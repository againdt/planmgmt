-------------------------------
11-Feb-2013
This is only one time task.

mvn archetype:create -DarchetypeGroupId=org.springframework.ws -DarchetypeArtifactId=spring-ws-archetype -DarchetypeVersion=2.1.2.RELEASE -DgroupId=com.getinsured.serff -DartifactId=ghix-serff

-------------------------------
mvn install

--This was given in soap project by SERFF
http://localhost:8080/exchange-hapi/services/ExchangePlanManagement

http://localhost:8080/serff/services/ExchangePlanManagement
 
--This is working in local system.
http://localhost:8080/exchange-hapi/ExchangePlanManagement?wsdl
============================================================================
Admin JSP

select * from serff_plan_mgmt

select * from serff_document

select serff_req_id,issuer_id,issuer_name,hios_product_id,plan_id, plan_name,serff_track_num,state_track_num,operation,process_ip, 
to_char(start_time,'DD-MON-YY HH24:MM:SS') as start1,
to_char(end_time,'DD-MON-YY HH24:MM:SS') as end1,
'<a href="getBottomRight?request="'||serff_req_id||' target="main">' as request_xml, 
response_xml, 
'attachments' as list1, request_status, remarks, attachments_list,request_state,request_state_desc
from serff_plan_mgmt
order by serff_req_id desc

select * from serff_document

select serff_doc_id, ecm_doc_id, doc_name, doc_type, doc_size,  
'<a href="getBottomRight?attachment='||ecm_doc_id||'" target="main">Show</a>' as attachment, 
to_char(created_on,'DD-MON-YY HH24:MM:SS') as createdon
from serff_document
where serff_req_id = 1
order by serff_doc_id
============================================================================
Steps for generate Java classes and merge when we receive new WSDL Files:
----------------------------------------------------------------------------
Note: Through following steps Java POJO classes will generate from WSDL and
its XSDs. But we will need to create end point class of WSDL explicitly.
----------------------------------------------------------------------------
Step 1: Create one java project with pom.xml file.
----------------------------------------------------------------------------
Step 2: Add WSDL file with all XSDs in one directory of your project.
----------------------------------------------------------------------------
Step 3: Add "org.apache.cxf" plugin in pom.xml file.
----------------------------------------------------------------------------
Step 4: Add source root path of the project in configuration tag.
----------------------------------------------------------------------------
Step 5: Add wsdl file path in configuration tag.
----------------------------------------------------------------------------
Step 6: Clean install meven pom.xml file from command promt or SST/Eclipse editor.
----------------------------------------------------------------------------
Step 7: All java classes will create after execution of pom.xml file.
----------------------------------------------------------------------------
EXAMPLE of POM file:
--------------------
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>serff-1.0</groupId>
	<artifactId>Serff</artifactId>
	<packaging>war</packaging>
	<version>1.0-SNAPSHOT</version>
	<build>
		<finalName>exchange-hapi</finalName>
		<plugins>
 			<plugin>
				<artifactId>maven-compiler-plugin</artifactId>
                <version>2.3.2</version>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
			<!-- "mvn generate-sources" To generate java from WSDL -->
			 <plugin>
				<groupId>org.apache.cxf</groupId>
				<artifactId>cxf-codegen-plugin</artifactId>
				<version>2.7.3</version>
				<executions>
					<execution>
						<id>generate-sources</id>
						<phase>generate-sources</phase>
						<configuration>
							<sourceRoot>${project.build.directory}/src/main/java/</sourceRoot>
							<sourceRoot>${basedir}/src/main/java/</sourceRoot>
							<wsdlOptions>
								<wsdlOption>
									<wsdl>${basedir}/src/main/webapp/WEB-INF/wsdl/WSDL_FILE_NAME.wsdl</wsdl>
								</wsdlOption>
						<!-- 		<extraargs>
									<extraarg>-xjc-Xts</extraarg>
								</extraargs> -->
								</wsdlOptions>
						</configuration>
						<goals>
							<goal>wsdl2java</goal>
						</goals>
					</execution>
				</executions>
         </plugin>
		</plugins>
	</build>
</project>
============================================================================