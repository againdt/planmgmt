
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCustomProduct.Plan.LookupSequence complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomProduct.Plan.LookupSequence">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomProduct.Plan.LookupSequence" type="{}CustomProduct.Plan.LookupSequence" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomProduct.Plan.LookupSequence", propOrder = {
    "customProductPlanLookupSequence"
})
public class ArrayOfCustomProductPlanLookupSequence {

    @XmlElement(name = "CustomProduct.Plan.LookupSequence", nillable = true)
    protected List<CustomProductPlanLookupSequence> customProductPlanLookupSequence;

    /**
     * Gets the value of the customProductPlanLookupSequence property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customProductPlanLookupSequence property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomProductPlanLookupSequence().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomProductPlanLookupSequence }
     * 
     * 
     */
    public List<CustomProductPlanLookupSequence> getCustomProductPlanLookupSequence() {
        if (customProductPlanLookupSequence == null) {
            customProductPlanLookupSequence = new ArrayList<CustomProductPlanLookupSequence>();
        }
        return this.customProductPlanLookupSequence;
    }

}
