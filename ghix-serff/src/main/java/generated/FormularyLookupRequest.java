
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FormularyLookup.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FormularyLookup.Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccessKeys" type="{}FormularyLookup.Request.AccessKey"/>
 *         &lt;element name="Inputs" type="{}FormularyLookup.Request.Input"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FormularyLookup.Request", propOrder = {
    "accessKeys",
    "inputs"
})
public class FormularyLookupRequest {

    @XmlElement(name = "AccessKeys", required = true, nillable = true)
    protected FormularyLookupRequestAccessKey accessKeys;
    @XmlElement(name = "Inputs", required = true, nillable = true)
    protected FormularyLookupRequestInput inputs;

    /**
     * Gets the value of the accessKeys property.
     * 
     * @return
     *     possible object is
     *     {@link FormularyLookupRequestAccessKey }
     *     
     */
    public FormularyLookupRequestAccessKey getAccessKeys() {
        return accessKeys;
    }

    /**
     * Sets the value of the accessKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormularyLookupRequestAccessKey }
     *     
     */
    public void setAccessKeys(FormularyLookupRequestAccessKey value) {
        this.accessKeys = value;
    }

    /**
     * Gets the value of the inputs property.
     * 
     * @return
     *     possible object is
     *     {@link FormularyLookupRequestInput }
     *     
     */
    public FormularyLookupRequestInput getInputs() {
        return inputs;
    }

    /**
     * Sets the value of the inputs property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormularyLookupRequestInput }
     *     
     */
    public void setInputs(FormularyLookupRequestInput value) {
        this.inputs = value;
    }

}
