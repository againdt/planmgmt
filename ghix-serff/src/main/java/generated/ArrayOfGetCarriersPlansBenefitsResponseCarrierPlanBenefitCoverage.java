
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage" type="{}GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage", propOrder = {
    "getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage"
})
public class ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage> getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage> getGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage() {
        if (getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage == null) {
            getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage = new ArrayList<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage>();
        }
        return this.getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage;
    }

}
