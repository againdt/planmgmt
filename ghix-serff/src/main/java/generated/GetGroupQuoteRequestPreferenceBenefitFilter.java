
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Request.Preference.BenefitFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Request.Preference.BenefitFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BenefitEnum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CoverageType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ViewPointType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ServiceType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LevelOfDetails" type="{}LevelOfDetails" minOccurs="0"/>
 *         &lt;element name="AttributeFilters" type="{}ArrayOfGetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Request.Preference.BenefitFilter", propOrder = {
    "benefitEnum",
    "coverageType",
    "viewPointType",
    "serviceType",
    "levelOfDetails",
    "attributeFilters"
})
public class GetGroupQuoteRequestPreferenceBenefitFilter {

    @XmlElementRef(name = "BenefitEnum", type = JAXBElement.class, required = false)
    protected JAXBElement<String> benefitEnum;
    @XmlElementRef(name = "CoverageType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> coverageType;
    @XmlElementRef(name = "ViewPointType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> viewPointType;
    @XmlElementRef(name = "ServiceType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serviceType;
    @XmlElement(name = "LevelOfDetails")
    protected LevelOfDetails levelOfDetails;
    @XmlElementRef(name = "AttributeFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter> attributeFilters;

    /**
     * Gets the value of the benefitEnum property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBenefitEnum() {
        return benefitEnum;
    }

    /**
     * Sets the value of the benefitEnum property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBenefitEnum(JAXBElement<String> value) {
        this.benefitEnum = value;
    }

    /**
     * Gets the value of the coverageType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCoverageType() {
        return coverageType;
    }

    /**
     * Sets the value of the coverageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCoverageType(JAXBElement<String> value) {
        this.coverageType = value;
    }

    /**
     * Gets the value of the viewPointType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getViewPointType() {
        return viewPointType;
    }

    /**
     * Sets the value of the viewPointType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setViewPointType(JAXBElement<String> value) {
        this.viewPointType = value;
    }

    /**
     * Gets the value of the serviceType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getServiceType() {
        return serviceType;
    }

    /**
     * Sets the value of the serviceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setServiceType(JAXBElement<String> value) {
        this.serviceType = value;
    }

    /**
     * Gets the value of the levelOfDetails property.
     * 
     * @return
     *     possible object is
     *     {@link LevelOfDetails }
     *     
     */
    public LevelOfDetails getLevelOfDetails() {
        return levelOfDetails;
    }

    /**
     * Sets the value of the levelOfDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link LevelOfDetails }
     *     
     */
    public void setLevelOfDetails(LevelOfDetails value) {
        this.levelOfDetails = value;
    }

    /**
     * Gets the value of the attributeFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter> getAttributeFilters() {
        return attributeFilters;
    }

    /**
     * Sets the value of the attributeFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter }{@code >}
     *     
     */
    public void setAttributeFilters(JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter> value) {
        this.attributeFilters = value;
    }

}
