
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InAreaAction.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="InAreaAction">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="QuoteThisBase"/>
 *     &lt;enumeration value="QuotePreviousBase"/>
 *     &lt;enumeration value="CheckNextBase"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "InAreaAction")
@XmlEnum
public enum InAreaAction {

    @XmlEnumValue("QuoteThisBase")
    QUOTE_THIS_BASE("QuoteThisBase"),
    @XmlEnumValue("QuotePreviousBase")
    QUOTE_PREVIOUS_BASE("QuotePreviousBase"),
    @XmlEnumValue("CheckNextBase")
    CHECK_NEXT_BASE("CheckNextBase");
    private final String value;

    InAreaAction(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static InAreaAction fromValue(String v) {
        for (InAreaAction c: InAreaAction.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
