
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetZipCodeInfo.Response.ZipCodeInfo.City complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetZipCodeInfo.Response.ZipCodeInfo.City">
 *   &lt;complexContent>
 *     &lt;extension base="{}CityBase">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetZipCodeInfo.Response.ZipCodeInfo.City")
public class GetZipCodeInfoResponseZipCodeInfoCity
    extends CityBase
{


}
