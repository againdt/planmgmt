
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmissionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SubmissionType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SetupClient"/>
 *     &lt;enumeration value="ResetClient"/>
 *     &lt;enumeration value="AppendData"/>
 *     &lt;enumeration value="EditData"/>
 *     &lt;enumeration value="DeleteClient"/>
 *     &lt;enumeration value="DeleteCarriers"/>
 *     &lt;enumeration value="DeletePlans"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SubmissionType")
@XmlEnum
public enum SubmissionType {

    @XmlEnumValue("SetupClient")
    SETUP_CLIENT("SetupClient"),
    @XmlEnumValue("ResetClient")
    RESET_CLIENT("ResetClient"),
    @XmlEnumValue("AppendData")
    APPEND_DATA("AppendData"),
    @XmlEnumValue("EditData")
    EDIT_DATA("EditData"),
    @XmlEnumValue("DeleteClient")
    DELETE_CLIENT("DeleteClient"),
    @XmlEnumValue("DeleteCarriers")
    DELETE_CARRIERS("DeleteCarriers"),
    @XmlEnumValue("DeletePlans")
    DELETE_PLANS("DeletePlans");
    private final String value;

    SubmissionType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SubmissionType fromValue(String v) {
        for (SubmissionType c: SubmissionType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
