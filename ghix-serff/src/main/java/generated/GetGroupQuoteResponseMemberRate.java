
package generated;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response.MemberRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response.MemberRate">
 *   &lt;complexContent>
 *     &lt;extension base="{}MemberRateBase">
 *       &lt;sequence>
 *         &lt;element name="EHBRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response.MemberRate", propOrder = {
    "ehbRate"
})
public class GetGroupQuoteResponseMemberRate
    extends MemberRateBase
{

    @XmlElement(name = "EHBRate")
    protected BigDecimal ehbRate;

    /**
     * Gets the value of the ehbRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEHBRate() {
        return ehbRate;
    }

    /**
     * Sets the value of the ehbRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEHBRate(BigDecimal value) {
        this.ehbRate = value;
    }

}
