
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BestTimeToCall.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BestTimeToCall">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Morning"/>
 *     &lt;enumeration value="Afternoon"/>
 *     &lt;enumeration value="Evening"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BestTimeToCall")
@XmlEnum
public enum BestTimeToCall {

    @XmlEnumValue("Morning")
    MORNING("Morning"),
    @XmlEnumValue("Afternoon")
    AFTERNOON("Afternoon"),
    @XmlEnumValue("Evening")
    EVENING("Evening");
    private final String value;

    BestTimeToCall(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BestTimeToCall fromValue(String v) {
        for (BestTimeToCall c: BestTimeToCall.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
