
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCounties.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCounties.Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccessKeys" type="{}GetCounties.Request.AccessKey"/>
 *         &lt;element name="Inputs" type="{}GetCounties.Request.Input"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCounties.Request", propOrder = {
    "accessKeys",
    "inputs"
})
public class GetCountiesRequest {

    @XmlElement(name = "AccessKeys", required = true, nillable = true)
    protected GetCountiesRequestAccessKey accessKeys;
    @XmlElement(name = "Inputs", required = true, nillable = true)
    protected GetCountiesRequestInput inputs;

    /**
     * Gets the value of the accessKeys property.
     * 
     * @return
     *     possible object is
     *     {@link GetCountiesRequestAccessKey }
     *     
     */
    public GetCountiesRequestAccessKey getAccessKeys() {
        return accessKeys;
    }

    /**
     * Sets the value of the accessKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCountiesRequestAccessKey }
     *     
     */
    public void setAccessKeys(GetCountiesRequestAccessKey value) {
        this.accessKeys = value;
    }

    /**
     * Gets the value of the inputs property.
     * 
     * @return
     *     possible object is
     *     {@link GetCountiesRequestInput }
     *     
     */
    public GetCountiesRequestInput getInputs() {
        return inputs;
    }

    /**
     * Sets the value of the inputs property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCountiesRequestInput }
     *     
     */
    public void setInputs(GetCountiesRequestInput value) {
        this.inputs = value;
    }

}
