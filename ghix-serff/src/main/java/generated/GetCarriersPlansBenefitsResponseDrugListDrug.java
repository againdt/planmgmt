
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarriersPlansBenefits.Response.DrugList.Drug complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarriersPlansBenefits.Response.DrugList.Drug">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RxCUI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TierLevel" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *         &lt;element name="AuthorizationRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="StepTherapyRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarriersPlansBenefits.Response.DrugList.Drug", propOrder = {
    "rxCUI",
    "tierLevel",
    "authorizationRequired",
    "stepTherapyRequired"
})
public class GetCarriersPlansBenefitsResponseDrugListDrug {

    @XmlElement(name = "RxCUI", required = true, nillable = true)
    protected String rxCUI;
    @XmlElement(name = "TierLevel")
    @XmlSchemaType(name = "unsignedByte")
    protected short tierLevel;
    @XmlElement(name = "AuthorizationRequired")
    protected boolean authorizationRequired;
    @XmlElement(name = "StepTherapyRequired")
    protected boolean stepTherapyRequired;

    /**
     * Gets the value of the rxCUI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRxCUI() {
        return rxCUI;
    }

    /**
     * Sets the value of the rxCUI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRxCUI(String value) {
        this.rxCUI = value;
    }

    /**
     * Gets the value of the tierLevel property.
     * 
     */
    public short getTierLevel() {
        return tierLevel;
    }

    /**
     * Sets the value of the tierLevel property.
     * 
     */
    public void setTierLevel(short value) {
        this.tierLevel = value;
    }

    /**
     * Gets the value of the authorizationRequired property.
     * 
     */
    public boolean isAuthorizationRequired() {
        return authorizationRequired;
    }

    /**
     * Sets the value of the authorizationRequired property.
     * 
     */
    public void setAuthorizationRequired(boolean value) {
        this.authorizationRequired = value;
    }

    /**
     * Gets the value of the stepTherapyRequired property.
     * 
     */
    public boolean isStepTherapyRequired() {
        return stepTherapyRequired;
    }

    /**
     * Sets the value of the stepTherapyRequired property.
     * 
     */
    public void setStepTherapyRequired(boolean value) {
        this.stepTherapyRequired = value;
    }

}
