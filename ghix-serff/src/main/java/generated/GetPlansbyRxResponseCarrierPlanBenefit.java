
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPlansbyRx.Response.Carrier.Plan.Benefit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPlansbyRx.Response.Carrier.Plan.Benefit">
 *   &lt;complexContent>
 *     &lt;extension base="{}BenefitBase">
 *       &lt;sequence>
 *         &lt;element name="Coverages" type="{}ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPlansbyRx.Response.Carrier.Plan.Benefit", propOrder = {
    "coverages"
})
public class GetPlansbyRxResponseCarrierPlanBenefit
    extends BenefitBase
{

    @XmlElementRef(name = "Coverages", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage> coverages;

    /**
     * Gets the value of the coverages property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage> getCoverages() {
        return coverages;
    }

    /**
     * Sets the value of the coverages property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage }{@code >}
     *     
     */
    public void setCoverages(JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage> value) {
        this.coverages = value;
    }

}
