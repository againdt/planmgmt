
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Request.GroupRateFactor.RAFOverride complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Request.GroupRateFactor.RAFOverride">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Request.GroupRateFactor.RAFOverride" type="{}GetGroupQuote.Request.GroupRateFactor.RAFOverride" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Request.GroupRateFactor.RAFOverride", propOrder = {
    "getGroupQuoteRequestGroupRateFactorRAFOverride"
})
public class ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride {

    @XmlElement(name = "GetGroupQuote.Request.GroupRateFactor.RAFOverride", nillable = true)
    protected List<GetGroupQuoteRequestGroupRateFactorRAFOverride> getGroupQuoteRequestGroupRateFactorRAFOverride;

    /**
     * Gets the value of the getGroupQuoteRequestGroupRateFactorRAFOverride property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteRequestGroupRateFactorRAFOverride property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteRequestGroupRateFactorRAFOverride().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteRequestGroupRateFactorRAFOverride }
     * 
     * 
     */
    public List<GetGroupQuoteRequestGroupRateFactorRAFOverride> getGetGroupQuoteRequestGroupRateFactorRAFOverride() {
        if (getGroupQuoteRequestGroupRateFactorRAFOverride == null) {
            getGroupQuoteRequestGroupRateFactorRAFOverride = new ArrayList<GetGroupQuoteRequestGroupRateFactorRAFOverride>();
        }
        return this.getGroupQuoteRequestGroupRateFactorRAFOverride;
    }

}
