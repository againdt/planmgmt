
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier.VersionedPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier.VersionedPlan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarrierLoadHistory.Response.LoadEvent.Carrier.VersionedPlan" type="{}GetCarrierLoadHistory.Response.LoadEvent.Carrier.VersionedPlan" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier.VersionedPlan", propOrder = {
    "getCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan"
})
public class ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan {

    @XmlElement(name = "GetCarrierLoadHistory.Response.LoadEvent.Carrier.VersionedPlan", nillable = true)
    protected List<GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan> getCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan;

    /**
     * Gets the value of the getCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan }
     * 
     * 
     */
    public List<GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan> getGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan() {
        if (getCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan == null) {
            getCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan = new ArrayList<GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan>();
        }
        return this.getCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan;
    }

}
