
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Response.Quote.EmployeePlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Response.Quote.EmployeePlan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Response.Quote.EmployeePlan" type="{}GetGroupQuote.Response.Quote.EmployeePlan" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Response.Quote.EmployeePlan", propOrder = {
    "getGroupQuoteResponseQuoteEmployeePlan"
})
public class ArrayOfGetGroupQuoteResponseQuoteEmployeePlan {

    @XmlElement(name = "GetGroupQuote.Response.Quote.EmployeePlan", nillable = true)
    protected List<GetGroupQuoteResponseQuoteEmployeePlan> getGroupQuoteResponseQuoteEmployeePlan;

    /**
     * Gets the value of the getGroupQuoteResponseQuoteEmployeePlan property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteResponseQuoteEmployeePlan property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteResponseQuoteEmployeePlan().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteResponseQuoteEmployeePlan }
     * 
     * 
     */
    public List<GetGroupQuoteResponseQuoteEmployeePlan> getGetGroupQuoteResponseQuoteEmployeePlan() {
        if (getGroupQuoteResponseQuoteEmployeePlan == null) {
            getGroupQuoteResponseQuoteEmployeePlan = new ArrayList<GetGroupQuoteResponseQuoteEmployeePlan>();
        }
        return this.getGroupQuoteResponseQuoteEmployeePlan;
    }

}
