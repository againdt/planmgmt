
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response.Quote.CarrierRate.PlanRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response.Quote.CarrierRate.PlanRate">
 *   &lt;complexContent>
 *     &lt;extension base="{}PlanRateBase">
 *       &lt;sequence>
 *         &lt;element name="IsShortPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsMixedPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PlanDetails" type="{}GetGroupQuote.Response.PlanDetail" minOccurs="0"/>
 *         &lt;element name="EmployeeRates" type="{}ArrayOfGetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response.Quote.CarrierRate.PlanRate", propOrder = {
    "isShortPlan",
    "isMixedPlan",
    "planDetails",
    "employeeRates"
})
public class GetGroupQuoteResponseQuoteCarrierRatePlanRate
    extends PlanRateBase
{

    @XmlElement(name = "IsShortPlan")
    protected boolean isShortPlan;
    @XmlElement(name = "IsMixedPlan")
    protected boolean isMixedPlan;
    @XmlElementRef(name = "PlanDetails", type = JAXBElement.class, required = false)
    protected JAXBElement<GetGroupQuoteResponsePlanDetail> planDetails;
    @XmlElementRef(name = "EmployeeRates", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate> employeeRates;

    /**
     * Gets the value of the isShortPlan property.
     * 
     */
    public boolean isIsShortPlan() {
        return isShortPlan;
    }

    /**
     * Sets the value of the isShortPlan property.
     * 
     */
    public void setIsShortPlan(boolean value) {
        this.isShortPlan = value;
    }

    /**
     * Gets the value of the isMixedPlan property.
     * 
     */
    public boolean isIsMixedPlan() {
        return isMixedPlan;
    }

    /**
     * Sets the value of the isMixedPlan property.
     * 
     */
    public void setIsMixedPlan(boolean value) {
        this.isMixedPlan = value;
    }

    /**
     * Gets the value of the planDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetail }{@code >}
     *     
     */
    public JAXBElement<GetGroupQuoteResponsePlanDetail> getPlanDetails() {
        return planDetails;
    }

    /**
     * Sets the value of the planDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetail }{@code >}
     *     
     */
    public void setPlanDetails(JAXBElement<GetGroupQuoteResponsePlanDetail> value) {
        this.planDetails = value;
    }

    /**
     * Gets the value of the employeeRates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate> getEmployeeRates() {
        return employeeRates;
    }

    /**
     * Sets the value of the employeeRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate }{@code >}
     *     
     */
    public void setEmployeeRates(JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate> value) {
        this.employeeRates = value;
    }

}
