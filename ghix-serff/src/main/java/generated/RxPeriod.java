
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RxPeriod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RxPeriod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OneMonth"/>
 *     &lt;enumeration value="ThreeMonths"/>
 *     &lt;enumeration value="ThirtyDays"/>
 *     &lt;enumeration value="NinetyDays"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RxPeriod")
@XmlEnum
public enum RxPeriod {

    @XmlEnumValue("OneMonth")
    ONE_MONTH("OneMonth"),
    @XmlEnumValue("ThreeMonths")
    THREE_MONTHS("ThreeMonths"),
    @XmlEnumValue("ThirtyDays")
    THIRTY_DAYS("ThirtyDays"),
    @XmlEnumValue("NinetyDays")
    NINETY_DAYS("NinetyDays");
    private final String value;

    RxPeriod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RxPeriod fromValue(String v) {
        for (RxPeriod c: RxPeriod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
