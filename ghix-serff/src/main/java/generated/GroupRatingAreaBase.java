
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GroupRatingAreaBase.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GroupRatingAreaBase">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EEResidence"/>
 *     &lt;enumeration value="EEWorksite"/>
 *     &lt;enumeration value="ERLocation"/>
 *     &lt;enumeration value="SpecialArea"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GroupRatingAreaBase")
@XmlEnum
public enum GroupRatingAreaBase {

    @XmlEnumValue("EEResidence")
    EE_RESIDENCE("EEResidence"),
    @XmlEnumValue("EEWorksite")
    EE_WORKSITE("EEWorksite"),
    @XmlEnumValue("ERLocation")
    ER_LOCATION("ERLocation"),
    @XmlEnumValue("SpecialArea")
    SPECIAL_AREA("SpecialArea");
    private final String value;

    GroupRatingAreaBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupRatingAreaBase fromValue(String v) {
        for (GroupRatingAreaBase c: GroupRatingAreaBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
