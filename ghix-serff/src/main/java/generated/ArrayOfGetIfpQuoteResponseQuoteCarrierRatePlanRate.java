
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.Response.Quote.CarrierRate.PlanRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.Response.Quote.CarrierRate.PlanRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.Response.Quote.CarrierRate.PlanRate" type="{}GetIfpQuote.Response.Quote.CarrierRate.PlanRate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.Response.Quote.CarrierRate.PlanRate", propOrder = {
    "getIfpQuoteResponseQuoteCarrierRatePlanRate"
})
public class ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate {

    @XmlElement(name = "GetIfpQuote.Response.Quote.CarrierRate.PlanRate", nillable = true)
    protected List<GetIfpQuoteResponseQuoteCarrierRatePlanRate> getIfpQuoteResponseQuoteCarrierRatePlanRate;

    /**
     * Gets the value of the getIfpQuoteResponseQuoteCarrierRatePlanRate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteResponseQuoteCarrierRatePlanRate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteResponseQuoteCarrierRatePlanRate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteResponseQuoteCarrierRatePlanRate }
     * 
     * 
     */
    public List<GetIfpQuoteResponseQuoteCarrierRatePlanRate> getGetIfpQuoteResponseQuoteCarrierRatePlanRate() {
        if (getIfpQuoteResponseQuoteCarrierRatePlanRate == null) {
            getIfpQuoteResponseQuoteCarrierRatePlanRate = new ArrayList<GetIfpQuoteResponseQuoteCarrierRatePlanRate>();
        }
        return this.getIfpQuoteResponseQuoteCarrierRatePlanRate;
    }

}
