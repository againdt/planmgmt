
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response.Quote.Carrier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response.Quote.Carrier">
 *   &lt;complexContent>
 *     &lt;extension base="{}CarrierRateBase">
 *       &lt;sequence>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Rates" type="{}ArrayOfGetIfpQuote.Response.Quote.Carrier.Rate.Modified"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response.Quote.Carrier", propOrder = {
    "version",
    "rates"
})
public class GetIfpQuoteResponseQuoteCarrier
    extends CarrierRateBase
{

    @XmlElement(name = "Version", required = true, nillable = true)
    protected String version;
    @XmlElement(name = "Rates", required = true, nillable = true)
    protected ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified rates;

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the rates property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified }
     *     
     */
    public ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified getRates() {
        return rates;
    }

    /**
     * Sets the value of the rates property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified }
     *     
     */
    public void setRates(ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified value) {
        this.rates = value;
    }

}
