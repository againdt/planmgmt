
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GroupPlanEligibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GroupPlanEligibility">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ShortPlansOK"/>
 *     &lt;enumeration value="MixedPlansOK"/>
 *     &lt;enumeration value="EEPartialEligibilityOK"/>
 *     &lt;enumeration value="IneligibleEEOK"/>
 *     &lt;enumeration value="FullEligibilityOnly"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GroupPlanEligibility")
@XmlEnum
public enum GroupPlanEligibility {

    @XmlEnumValue("ShortPlansOK")
    SHORT_PLANS_OK("ShortPlansOK"),
    @XmlEnumValue("MixedPlansOK")
    MIXED_PLANS_OK("MixedPlansOK"),
    @XmlEnumValue("EEPartialEligibilityOK")
    EE_PARTIAL_ELIGIBILITY_OK("EEPartialEligibilityOK"),
    @XmlEnumValue("IneligibleEEOK")
    INELIGIBLE_EEOK("IneligibleEEOK"),
    @XmlEnumValue("FullEligibilityOnly")
    FULL_ELIGIBILITY_ONLY("FullEligibilityOnly");
    private final String value;

    GroupPlanEligibility(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GroupPlanEligibility fromValue(String v) {
        for (GroupPlanEligibility c: GroupPlanEligibility.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
