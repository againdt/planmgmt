
package generated;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Request.Preference.DrugFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Request.Preference.DrugFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MinRxMatch" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="RxCUIs" type="{}RxCUIsBase"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Request.Preference.DrugFilter", propOrder = {
    "minRxMatch",
    "rxCUIs"
})
public class GetIfpQuoteRequestPreferenceDrugFilter {

    @XmlElement(name = "MinRxMatch", required = true)
    protected BigDecimal minRxMatch;
    @XmlElement(name = "RxCUIs", required = true, nillable = true)
    protected RxCUIsBase rxCUIs;

    /**
     * Gets the value of the minRxMatch property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinRxMatch() {
        return minRxMatch;
    }

    /**
     * Sets the value of the minRxMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinRxMatch(BigDecimal value) {
        this.minRxMatch = value;
    }

    /**
     * Gets the value of the rxCUIs property.
     * 
     * @return
     *     possible object is
     *     {@link RxCUIsBase }
     *     
     */
    public RxCUIsBase getRxCUIs() {
        return rxCUIs;
    }

    /**
     * Sets the value of the rxCUIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link RxCUIsBase }
     *     
     */
    public void setRxCUIs(RxCUIsBase value) {
        this.rxCUIs = value;
    }

}
