
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfListRx.Request.PlanFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfListRx.Request.PlanFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ListRx.Request.PlanFilter" type="{}ListRx.Request.PlanFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfListRx.Request.PlanFilter", propOrder = {
    "listRxRequestPlanFilter"
})
public class ArrayOfListRxRequestPlanFilter {

    @XmlElement(name = "ListRx.Request.PlanFilter", nillable = true)
    protected List<ListRxRequestPlanFilter> listRxRequestPlanFilter;

    /**
     * Gets the value of the listRxRequestPlanFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listRxRequestPlanFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListRxRequestPlanFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListRxRequestPlanFilter }
     * 
     * 
     */
    public List<ListRxRequestPlanFilter> getListRxRequestPlanFilter() {
        if (listRxRequestPlanFilter == null) {
            listRxRequestPlanFilter = new ArrayList<ListRxRequestPlanFilter>();
        }
        return this.listRxRequestPlanFilter;
    }

}
