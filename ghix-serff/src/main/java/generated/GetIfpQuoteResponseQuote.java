
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response.Quote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response.Quote">
 *   &lt;complexContent>
 *     &lt;extension base="{}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="QuoteFormat" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EligibleCostShareViewPoint" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Carriers" type="{}ArrayOfGetIfpQuote.Response.Quote.CarrierRate" minOccurs="0"/>
 *         &lt;element name="MemberPlans" type="{}ArrayOfGetIfpQuote.Response.Quote.MemberPlan" minOccurs="0"/>
 *         &lt;element name="DroppedRates" type="{}ArrayOfGetIfpQuote.Response.Quote.DroppedCarrier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response.Quote", propOrder = {
    "quoteFormat",
    "eligibleCostShareViewPoint",
    "carriers",
    "memberPlans",
    "droppedRates"
})
public class GetIfpQuoteResponseQuote
    extends ResponseBase
{

    @XmlElement(name = "QuoteFormat", required = true, nillable = true)
    protected String quoteFormat;
    @XmlElement(name = "EligibleCostShareViewPoint", required = true, nillable = true)
    protected String eligibleCostShareViewPoint;
    @XmlElementRef(name = "Carriers", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRate> carriers;
    @XmlElementRef(name = "MemberPlans", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponseQuoteMemberPlan> memberPlans;
    @XmlElementRef(name = "DroppedRates", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier> droppedRates;

    /**
     * Gets the value of the quoteFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuoteFormat() {
        return quoteFormat;
    }

    /**
     * Sets the value of the quoteFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuoteFormat(String value) {
        this.quoteFormat = value;
    }

    /**
     * Gets the value of the eligibleCostShareViewPoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEligibleCostShareViewPoint() {
        return eligibleCostShareViewPoint;
    }

    /**
     * Sets the value of the eligibleCostShareViewPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEligibleCostShareViewPoint(String value) {
        this.eligibleCostShareViewPoint = value;
    }

    /**
     * Gets the value of the carriers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrierRate }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRate> getCarriers() {
        return carriers;
    }

    /**
     * Sets the value of the carriers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrierRate }{@code >}
     *     
     */
    public void setCarriers(JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRate> value) {
        this.carriers = value;
    }

    /**
     * Gets the value of the memberPlans property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteMemberPlan }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteMemberPlan> getMemberPlans() {
        return memberPlans;
    }

    /**
     * Sets the value of the memberPlans property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteMemberPlan }{@code >}
     *     
     */
    public void setMemberPlans(JAXBElement<ArrayOfGetIfpQuoteResponseQuoteMemberPlan> value) {
        this.memberPlans = value;
    }

    /**
     * Gets the value of the droppedRates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier> getDroppedRates() {
        return droppedRates;
    }

    /**
     * Sets the value of the droppedRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier }{@code >}
     *     
     */
    public void setDroppedRates(JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier> value) {
        this.droppedRates = value;
    }

}
