
package generated;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IfpRateBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="IfpRateBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlanId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContractStatus" type="{}ContractStatus"/>
 *         &lt;element name="Visibility" type="{}PlanVisibility"/>
 *         &lt;element name="RatingMethod" type="{}RatingMethod"/>
 *         &lt;element name="RatingAreaBase" type="{}IfpRatingAreaBase" minOccurs="0"/>
 *         &lt;element name="RatingAreaId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SmokerSurcharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="EHBRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Age" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="IsBenchmarkPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsFeaturedPlan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NumberOfDays" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BaseRateUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IfpRateBase", propOrder = {
    "planId",
    "contractStatus",
    "visibility",
    "ratingMethod",
    "ratingAreaBase",
    "ratingAreaId",
    "smokerSurcharge",
    "rate",
    "ehbRate",
    "age",
    "isBenchmarkPlan",
    "isFeaturedPlan",
    "numberOfDays",
    "baseRateUnit"
})
@XmlSeeAlso({
    GetIfpQuoteResponseQuoteCarrierRateModified.class
})
public class IfpRateBase {

    @XmlElement(name = "PlanId", required = true, nillable = true)
    protected String planId;
    @XmlList
    @XmlElement(name = "ContractStatus", required = true)
    protected List<String> contractStatus;
    @XmlElement(name = "Visibility", required = true)
    protected PlanVisibility visibility;
    @XmlElement(name = "RatingMethod", required = true)
    protected RatingMethod ratingMethod;
    @XmlElement(name = "RatingAreaBase")
    protected IfpRatingAreaBase ratingAreaBase;
    @XmlElement(name = "RatingAreaId")
    protected Integer ratingAreaId;
    @XmlElement(name = "SmokerSurcharge")
    protected BigDecimal smokerSurcharge;
    @XmlElement(name = "Rate")
    protected BigDecimal rate;
    @XmlElement(name = "EHBRate")
    protected BigDecimal ehbRate;
    @XmlElement(name = "Age", required = true)
    protected BigDecimal age;
    @XmlElement(name = "IsBenchmarkPlan")
    protected boolean isBenchmarkPlan;
    @XmlElement(name = "IsFeaturedPlan")
    protected boolean isFeaturedPlan;
    @XmlElementRef(name = "NumberOfDays", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numberOfDays;
    @XmlElementRef(name = "BaseRateUnit", type = JAXBElement.class, required = false)
    protected JAXBElement<String> baseRateUnit;

    /**
     * Gets the value of the planId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanId() {
        return planId;
    }

    /**
     * Sets the value of the planId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanId(String value) {
        this.planId = value;
    }

    /**
     * Gets the value of the contractStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getContractStatus() {
        if (contractStatus == null) {
            contractStatus = new ArrayList<String>();
        }
        return this.contractStatus;
    }

    /**
     * Gets the value of the visibility property.
     * 
     * @return
     *     possible object is
     *     {@link PlanVisibility }
     *     
     */
    public PlanVisibility getVisibility() {
        return visibility;
    }

    /**
     * Sets the value of the visibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanVisibility }
     *     
     */
    public void setVisibility(PlanVisibility value) {
        this.visibility = value;
    }

    /**
     * Gets the value of the ratingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link RatingMethod }
     *     
     */
    public RatingMethod getRatingMethod() {
        return ratingMethod;
    }

    /**
     * Sets the value of the ratingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link RatingMethod }
     *     
     */
    public void setRatingMethod(RatingMethod value) {
        this.ratingMethod = value;
    }

    /**
     * Gets the value of the ratingAreaBase property.
     * 
     * @return
     *     possible object is
     *     {@link IfpRatingAreaBase }
     *     
     */
    public IfpRatingAreaBase getRatingAreaBase() {
        return ratingAreaBase;
    }

    /**
     * Sets the value of the ratingAreaBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link IfpRatingAreaBase }
     *     
     */
    public void setRatingAreaBase(IfpRatingAreaBase value) {
        this.ratingAreaBase = value;
    }

    /**
     * Gets the value of the ratingAreaId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRatingAreaId() {
        return ratingAreaId;
    }

    /**
     * Sets the value of the ratingAreaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRatingAreaId(Integer value) {
        this.ratingAreaId = value;
    }

    /**
     * Gets the value of the smokerSurcharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSmokerSurcharge() {
        return smokerSurcharge;
    }

    /**
     * Sets the value of the smokerSurcharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSmokerSurcharge(BigDecimal value) {
        this.smokerSurcharge = value;
    }

    /**
     * Gets the value of the rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * Gets the value of the ehbRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEHBRate() {
        return ehbRate;
    }

    /**
     * Sets the value of the ehbRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEHBRate(BigDecimal value) {
        this.ehbRate = value;
    }

    /**
     * Gets the value of the age property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAge() {
        return age;
    }

    /**
     * Sets the value of the age property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAge(BigDecimal value) {
        this.age = value;
    }

    /**
     * Gets the value of the isBenchmarkPlan property.
     * 
     */
    public boolean isIsBenchmarkPlan() {
        return isBenchmarkPlan;
    }

    /**
     * Sets the value of the isBenchmarkPlan property.
     * 
     */
    public void setIsBenchmarkPlan(boolean value) {
        this.isBenchmarkPlan = value;
    }

    /**
     * Gets the value of the isFeaturedPlan property.
     * 
     */
    public boolean isIsFeaturedPlan() {
        return isFeaturedPlan;
    }

    /**
     * Sets the value of the isFeaturedPlan property.
     * 
     */
    public void setIsFeaturedPlan(boolean value) {
        this.isFeaturedPlan = value;
    }

    /**
     * Gets the value of the numberOfDays property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumberOfDays() {
        return numberOfDays;
    }

    /**
     * Sets the value of the numberOfDays property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumberOfDays(JAXBElement<String> value) {
        this.numberOfDays = value;
    }

    /**
     * Gets the value of the baseRateUnit property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBaseRateUnit() {
        return baseRateUnit;
    }

    /**
     * Sets the value of the baseRateUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBaseRateUnit(JAXBElement<String> value) {
        this.baseRateUnit = value;
    }

}
