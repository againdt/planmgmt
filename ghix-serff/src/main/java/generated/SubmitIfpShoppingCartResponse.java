
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmitIfpShoppingCart.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitIfpShoppingCart.Response">
 *   &lt;complexContent>
 *     &lt;extension base="{}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="ShoppingCart" type="{}IfpShoppingCart" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitIfpShoppingCart.Response", propOrder = {
    "shoppingCart"
})
public class SubmitIfpShoppingCartResponse
    extends ResponseBase
{

    @XmlElementRef(name = "ShoppingCart", type = JAXBElement.class, required = false)
    protected JAXBElement<IfpShoppingCart> shoppingCart;

    /**
     * Gets the value of the shoppingCart property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link IfpShoppingCart }{@code >}
     *     
     */
    public JAXBElement<IfpShoppingCart> getShoppingCart() {
        return shoppingCart;
    }

    /**
     * Sets the value of the shoppingCart property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link IfpShoppingCart }{@code >}
     *     
     */
    public void setShoppingCart(JAXBElement<IfpShoppingCart> value) {
        this.shoppingCart = value;
    }

}
