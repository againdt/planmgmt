
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier.DroppedPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier.DroppedPlan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlanId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RatingMethod" type="{}RatingMethod"/>
 *         &lt;element name="PlanName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DroppedReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier.DroppedPlan", propOrder = {
    "planId",
    "ratingMethod",
    "planName",
    "droppedReason"
})
public class GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan {

    @XmlElement(name = "PlanId", required = true, nillable = true)
    protected String planId;
    @XmlElement(name = "RatingMethod", required = true)
    protected RatingMethod ratingMethod;
    @XmlElementRef(name = "PlanName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planName;
    @XmlElement(name = "DroppedReason", required = true, nillable = true)
    protected String droppedReason;

    /**
     * Gets the value of the planId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanId() {
        return planId;
    }

    /**
     * Sets the value of the planId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanId(String value) {
        this.planId = value;
    }

    /**
     * Gets the value of the ratingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link RatingMethod }
     *     
     */
    public RatingMethod getRatingMethod() {
        return ratingMethod;
    }

    /**
     * Sets the value of the ratingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link RatingMethod }
     *     
     */
    public void setRatingMethod(RatingMethod value) {
        this.ratingMethod = value;
    }

    /**
     * Gets the value of the planName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanName() {
        return planName;
    }

    /**
     * Sets the value of the planName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanName(JAXBElement<String> value) {
        this.planName = value;
    }

    /**
     * Gets the value of the droppedReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDroppedReason() {
        return droppedReason;
    }

    /**
     * Sets the value of the droppedReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDroppedReason(String value) {
        this.droppedReason = value;
    }

}
