
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuoteRequestBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuoteRequestBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemoteAccessKey" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid"/>
 *         &lt;element name="WebsiteAccessKey" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid"/>
 *         &lt;element name="BrokerId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteRequestBase", propOrder = {
    "remoteAccessKey",
    "websiteAccessKey",
    "brokerId"
})
@XmlSeeAlso({
    GetGroupQuoteRequest.class,
    GetIfpShoppingCartsRequest.class,
    GetGroupRequest.class,
    GetFamilyRequest.class,
    SubmitFamilyRequest.class,
    SubmitGroupRequest.class,
    GetIfpQuoteRequest.class
})
public class QuoteRequestBase {

    @XmlElement(name = "RemoteAccessKey", required = true)
    protected String remoteAccessKey;
    @XmlElement(name = "WebsiteAccessKey", required = true)
    protected String websiteAccessKey;
    @XmlElementRef(name = "BrokerId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> brokerId;

    /**
     * Gets the value of the remoteAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteAccessKey() {
        return remoteAccessKey;
    }

    /**
     * Sets the value of the remoteAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteAccessKey(String value) {
        this.remoteAccessKey = value;
    }

    /**
     * Gets the value of the websiteAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebsiteAccessKey() {
        return websiteAccessKey;
    }

    /**
     * Sets the value of the websiteAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebsiteAccessKey(String value) {
        this.websiteAccessKey = value;
    }

    /**
     * Gets the value of the brokerId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getBrokerId() {
        return brokerId;
    }

    /**
     * Sets the value of the brokerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setBrokerId(JAXBElement<Integer> value) {
        this.brokerId = value;
    }

}
