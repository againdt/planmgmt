
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.Request.Preference.DrugFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.Request.Preference.DrugFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.Request.Preference.DrugFilter" type="{}GetIfpQuote.Request.Preference.DrugFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.Request.Preference.DrugFilter", propOrder = {
    "getIfpQuoteRequestPreferenceDrugFilter"
})
public class ArrayOfGetIfpQuoteRequestPreferenceDrugFilter {

    @XmlElement(name = "GetIfpQuote.Request.Preference.DrugFilter", nillable = true)
    protected List<GetIfpQuoteRequestPreferenceDrugFilter> getIfpQuoteRequestPreferenceDrugFilter;

    /**
     * Gets the value of the getIfpQuoteRequestPreferenceDrugFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteRequestPreferenceDrugFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteRequestPreferenceDrugFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteRequestPreferenceDrugFilter }
     * 
     * 
     */
    public List<GetIfpQuoteRequestPreferenceDrugFilter> getGetIfpQuoteRequestPreferenceDrugFilter() {
        if (getIfpQuoteRequestPreferenceDrugFilter == null) {
            getIfpQuoteRequestPreferenceDrugFilter = new ArrayList<GetIfpQuoteRequestPreferenceDrugFilter>();
        }
        return this.getIfpQuoteRequestPreferenceDrugFilter;
    }

}
