
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomProductsQuote.Request.CustomRateFactor.Family" type="{}GetCustomProductsQuote.Request.CustomRateFactor.Family" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family", propOrder = {
    "getCustomProductsQuoteRequestCustomRateFactorFamily"
})
public class ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily {

    @XmlElement(name = "GetCustomProductsQuote.Request.CustomRateFactor.Family", nillable = true)
    protected List<GetCustomProductsQuoteRequestCustomRateFactorFamily> getCustomProductsQuoteRequestCustomRateFactorFamily;

    /**
     * Gets the value of the getCustomProductsQuoteRequestCustomRateFactorFamily property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCustomProductsQuoteRequestCustomRateFactorFamily property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCustomProductsQuoteRequestCustomRateFactorFamily().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCustomProductsQuoteRequestCustomRateFactorFamily }
     * 
     * 
     */
    public List<GetCustomProductsQuoteRequestCustomRateFactorFamily> getGetCustomProductsQuoteRequestCustomRateFactorFamily() {
        if (getCustomProductsQuoteRequestCustomRateFactorFamily == null) {
            getCustomProductsQuoteRequestCustomRateFactorFamily = new ArrayList<GetCustomProductsQuoteRequestCustomRateFactorFamily>();
        }
        return this.getCustomProductsQuoteRequestCustomRateFactorFamily;
    }

}
