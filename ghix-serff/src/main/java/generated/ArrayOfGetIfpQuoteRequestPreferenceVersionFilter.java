
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.Request.Preference.VersionFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.Request.Preference.VersionFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.Request.Preference.VersionFilter" type="{}GetIfpQuote.Request.Preference.VersionFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.Request.Preference.VersionFilter", propOrder = {
    "getIfpQuoteRequestPreferenceVersionFilter"
})
public class ArrayOfGetIfpQuoteRequestPreferenceVersionFilter {

    @XmlElement(name = "GetIfpQuote.Request.Preference.VersionFilter", nillable = true)
    protected List<GetIfpQuoteRequestPreferenceVersionFilter> getIfpQuoteRequestPreferenceVersionFilter;

    /**
     * Gets the value of the getIfpQuoteRequestPreferenceVersionFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteRequestPreferenceVersionFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteRequestPreferenceVersionFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteRequestPreferenceVersionFilter }
     * 
     * 
     */
    public List<GetIfpQuoteRequestPreferenceVersionFilter> getGetIfpQuoteRequestPreferenceVersionFilter() {
        if (getIfpQuoteRequestPreferenceVersionFilter == null) {
            getIfpQuoteRequestPreferenceVersionFilter = new ArrayList<GetIfpQuoteRequestPreferenceVersionFilter>();
        }
        return this.getIfpQuoteRequestPreferenceVersionFilter;
    }

}
