
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCustomProductsQuote.Request.CustomRateFactor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomProductsQuote.Request.CustomRateFactor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ClientZipCode" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ClientCountyFIPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Families" type="{}ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomProductsQuote.Request.CustomRateFactor", propOrder = {
    "clientId",
    "clientZipCode",
    "clientCountyFIPS",
    "clientState",
    "families"
})
public class GetCustomProductsQuoteRequestCustomRateFactor {

    @XmlElement(name = "ClientId", required = true, nillable = true)
    protected String clientId;
    @XmlElement(name = "ClientZipCode")
    protected int clientZipCode;
    @XmlElementRef(name = "ClientCountyFIPS", type = JAXBElement.class, required = false)
    protected JAXBElement<String> clientCountyFIPS;
    @XmlElementRef(name = "ClientState", type = JAXBElement.class, required = false)
    protected JAXBElement<String> clientState;
    @XmlElement(name = "Families", required = true, nillable = true)
    protected ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily families;

    /**
     * Gets the value of the clientId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Sets the value of the clientId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Gets the value of the clientZipCode property.
     * 
     */
    public int getClientZipCode() {
        return clientZipCode;
    }

    /**
     * Sets the value of the clientZipCode property.
     * 
     */
    public void setClientZipCode(int value) {
        this.clientZipCode = value;
    }

    /**
     * Gets the value of the clientCountyFIPS property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClientCountyFIPS() {
        return clientCountyFIPS;
    }

    /**
     * Sets the value of the clientCountyFIPS property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClientCountyFIPS(JAXBElement<String> value) {
        this.clientCountyFIPS = value;
    }

    /**
     * Gets the value of the clientState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getClientState() {
        return clientState;
    }

    /**
     * Sets the value of the clientState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setClientState(JAXBElement<String> value) {
        this.clientState = value;
    }

    /**
     * Gets the value of the families property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily }
     *     
     */
    public ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily getFamilies() {
        return families;
    }

    /**
     * Sets the value of the families property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily }
     *     
     */
    public void setFamilies(ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily value) {
        this.families = value;
    }

}
