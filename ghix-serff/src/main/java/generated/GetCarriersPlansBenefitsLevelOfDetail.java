
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarriersPlansBenefits.LevelOfDetail.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GetCarriersPlansBenefits.LevelOfDetail">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Carriers"/>
 *     &lt;enumeration value="Plans"/>
 *     &lt;enumeration value="Benefits"/>
 *     &lt;enumeration value="Coverages"/>
 *     &lt;enumeration value="ViewPoints"/>
 *     &lt;enumeration value="Services"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GetCarriersPlansBenefits.LevelOfDetail")
@XmlEnum
public enum GetCarriersPlansBenefitsLevelOfDetail {

    @XmlEnumValue("Carriers")
    CARRIERS("Carriers"),
    @XmlEnumValue("Plans")
    PLANS("Plans"),
    @XmlEnumValue("Benefits")
    BENEFITS("Benefits"),
    @XmlEnumValue("Coverages")
    COVERAGES("Coverages"),
    @XmlEnumValue("ViewPoints")
    VIEW_POINTS("ViewPoints"),
    @XmlEnumValue("Services")
    SERVICES("Services");
    private final String value;

    GetCarriersPlansBenefitsLevelOfDetail(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GetCarriersPlansBenefitsLevelOfDetail fromValue(String v) {
        for (GetCarriersPlansBenefitsLevelOfDetail c: GetCarriersPlansBenefitsLevelOfDetail.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
