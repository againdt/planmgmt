
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response.PlanDetail.Benefit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response.PlanDetail.Benefit">
 *   &lt;complexContent>
 *     &lt;extension base="{}BenefitBase">
 *       &lt;sequence>
 *         &lt;element name="BenefitData" type="{}ArrayOfGetIfpQuote.Response.Item" minOccurs="0"/>
 *         &lt;element name="Coverages" type="{}ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response.PlanDetail.Benefit", propOrder = {
    "benefitData",
    "coverages"
})
public class GetIfpQuoteResponsePlanDetailBenefit
    extends BenefitBase
{

    @XmlElementRef(name = "BenefitData", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponseItem> benefitData;
    @XmlElementRef(name = "Coverages", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage> coverages;

    /**
     * Gets the value of the benefitData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponseItem> getBenefitData() {
        return benefitData;
    }

    /**
     * Sets the value of the benefitData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseItem }{@code >}
     *     
     */
    public void setBenefitData(JAXBElement<ArrayOfGetIfpQuoteResponseItem> value) {
        this.benefitData = value;
    }

    /**
     * Gets the value of the coverages property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage> getCoverages() {
        return coverages;
    }

    /**
     * Sets the value of the coverages property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage }{@code >}
     *     
     */
    public void setCoverages(JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage> value) {
        this.coverages = value;
    }

}
