
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate" type="{}GetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate", propOrder = {
    "getGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate"
})
public class ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate {

    @XmlElement(name = "GetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate", nillable = true)
    protected List<GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate> getGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate;

    /**
     * Gets the value of the getGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate }
     * 
     * 
     */
    public List<GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate> getGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate() {
        if (getGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate == null) {
            getGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate = new ArrayList<GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate>();
        }
        return this.getGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate;
    }

}
