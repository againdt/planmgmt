
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response.PlanDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response.PlanDetail">
 *   &lt;complexContent>
 *     &lt;extension base="{}PlanDetailsBase">
 *       &lt;sequence>
 *         &lt;element name="PlanData" type="{}ArrayOfGetIfpQuote.Response.Item" minOccurs="0"/>
 *         &lt;element name="Benefits" type="{}ArrayOfGetIfpQuote.Response.PlanDetail.Benefit" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response.PlanDetail", propOrder = {
    "planData",
    "benefits"
})
public class GetIfpQuoteResponsePlanDetail
    extends PlanDetailsBase
{

    @XmlElementRef(name = "PlanData", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponseItem> planData;
    @XmlElementRef(name = "Benefits", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefit> benefits;

    /**
     * Gets the value of the planData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponseItem> getPlanData() {
        return planData;
    }

    /**
     * Sets the value of the planData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseItem }{@code >}
     *     
     */
    public void setPlanData(JAXBElement<ArrayOfGetIfpQuoteResponseItem> value) {
        this.planData = value;
    }

    /**
     * Gets the value of the benefits property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefit }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefit> getBenefits() {
        return benefits;
    }

    /**
     * Sets the value of the benefits property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefit }{@code >}
     *     
     */
    public void setBenefits(JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefit> value) {
        this.benefits = value;
    }

}
