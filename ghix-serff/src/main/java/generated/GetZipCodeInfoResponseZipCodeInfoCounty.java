
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetZipCodeInfo.Response.ZipCodeInfo.County complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetZipCodeInfo.Response.ZipCodeInfo.County">
 *   &lt;complexContent>
 *     &lt;extension base="{}CountyBase">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetZipCodeInfo.Response.ZipCodeInfo.County")
public class GetZipCodeInfoResponseZipCodeInfoCounty
    extends CountyBase
{


}
