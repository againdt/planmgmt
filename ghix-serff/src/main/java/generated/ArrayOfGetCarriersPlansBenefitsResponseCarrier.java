
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.Carrier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.Carrier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.Carrier" type="{}GetCarriersPlansBenefits.Response.Carrier" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier", propOrder = {
    "getCarriersPlansBenefitsResponseCarrier"
})
public class ArrayOfGetCarriersPlansBenefitsResponseCarrier {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.Carrier", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseCarrier> getCarriersPlansBenefitsResponseCarrier;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseCarrier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseCarrier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseCarrier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseCarrier }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseCarrier> getGetCarriersPlansBenefitsResponseCarrier() {
        if (getCarriersPlansBenefitsResponseCarrier == null) {
            getCarriersPlansBenefitsResponseCarrier = new ArrayList<GetCarriersPlansBenefitsResponseCarrier>();
        }
        return this.getCarriersPlansBenefitsResponseCarrier;
    }

}
