
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomProductsQuote.Response.Quote.FamilyQuote" type="{}GetCustomProductsQuote.Response.Quote.FamilyQuote" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote", propOrder = {
    "getCustomProductsQuoteResponseQuoteFamilyQuote"
})
public class ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote {

    @XmlElement(name = "GetCustomProductsQuote.Response.Quote.FamilyQuote", nillable = true)
    protected List<GetCustomProductsQuoteResponseQuoteFamilyQuote> getCustomProductsQuoteResponseQuoteFamilyQuote;

    /**
     * Gets the value of the getCustomProductsQuoteResponseQuoteFamilyQuote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCustomProductsQuoteResponseQuoteFamilyQuote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCustomProductsQuoteResponseQuoteFamilyQuote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCustomProductsQuoteResponseQuoteFamilyQuote }
     * 
     * 
     */
    public List<GetCustomProductsQuoteResponseQuoteFamilyQuote> getGetCustomProductsQuoteResponseQuoteFamilyQuote() {
        if (getCustomProductsQuoteResponseQuoteFamilyQuote == null) {
            getCustomProductsQuoteResponseQuoteFamilyQuote = new ArrayList<GetCustomProductsQuoteResponseQuoteFamilyQuote>();
        }
        return this.getCustomProductsQuoteResponseQuoteFamilyQuote;
    }

}
