
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPlansbyRx.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPlansbyRx.Response">
 *   &lt;complexContent>
 *     &lt;extension base="{}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="Carriers" type="{}ArrayOfGetPlansbyRx.Response.Carrier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPlansbyRx.Response", propOrder = {
    "carriers"
})
public class GetPlansbyRxResponse
    extends ResponseBase
{

    @XmlElementRef(name = "Carriers", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetPlansbyRxResponseCarrier> carriers;

    /**
     * Gets the value of the carriers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrier }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrier> getCarriers() {
        return carriers;
    }

    /**
     * Sets the value of the carriers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrier }{@code >}
     *     
     */
    public void setCarriers(JAXBElement<ArrayOfGetPlansbyRxResponseCarrier> value) {
        this.carriers = value;
    }

}
