
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate" type="{}GetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate", propOrder = {
    "getGroupQuoteResponseQuoteEmployeePlanCarrierRate"
})
public class ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate {

    @XmlElement(name = "GetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate", nillable = true)
    protected List<GetGroupQuoteResponseQuoteEmployeePlanCarrierRate> getGroupQuoteResponseQuoteEmployeePlanCarrierRate;

    /**
     * Gets the value of the getGroupQuoteResponseQuoteEmployeePlanCarrierRate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteResponseQuoteEmployeePlanCarrierRate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteResponseQuoteEmployeePlanCarrierRate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteResponseQuoteEmployeePlanCarrierRate }
     * 
     * 
     */
    public List<GetGroupQuoteResponseQuoteEmployeePlanCarrierRate> getGetGroupQuoteResponseQuoteEmployeePlanCarrierRate() {
        if (getGroupQuoteResponseQuoteEmployeePlanCarrierRate == null) {
            getGroupQuoteResponseQuoteEmployeePlanCarrierRate = new ArrayList<GetGroupQuoteResponseQuoteEmployeePlanCarrierRate>();
        }
        return this.getGroupQuoteResponseQuoteEmployeePlanCarrierRate;
    }

}
