
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate" type="{}GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate", propOrder = {
    "getCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate"
})
public class ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate {

    @XmlElement(name = "GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate", nillable = true)
    protected List<GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate> getCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate;

    /**
     * Gets the value of the getCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate }
     * 
     * 
     */
    public List<GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate> getGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate() {
        if (getCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate == null) {
            getCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate = new ArrayList<GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate>();
        }
        return this.getCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate;
    }

}
