
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier" type="{}GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier", propOrder = {
    "getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier"
})
public class ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier {

    @XmlElement(name = "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier", nillable = true)
    protected List<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier> getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier;

    /**
     * Gets the value of the getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier }
     * 
     * 
     */
    public List<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier> getGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier() {
        if (getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier == null) {
            getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier = new ArrayList<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier>();
        }
        return this.getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier;
    }

}
