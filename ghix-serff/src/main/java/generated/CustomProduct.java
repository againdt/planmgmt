
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CustomProduct complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomProduct">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Market" type="{}Market" minOccurs="0"/>
 *         &lt;element name="ProductType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="CarrierId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubmissionId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="RatingAreas" type="{}ArrayOfCustomProduct.RatingArea" minOccurs="0"/>
 *         &lt;element name="Plans" type="{}ArrayOfCustomProduct.Plan" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomProduct", propOrder = {
    "state",
    "market",
    "productType",
    "effectiveDate",
    "carrierId",
    "companyName",
    "submissionId",
    "ratingAreas",
    "plans"
})
public class CustomProduct {

    @XmlElement(name = "State", required = true, nillable = true)
    protected String state;
    @XmlElementRef(name = "Market", type = JAXBElement.class, required = false)
    protected JAXBElement<Market> market;
    @XmlElement(name = "ProductType", required = true, nillable = true)
    protected String productType;
    @XmlElement(name = "EffectiveDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "CarrierId", required = true, nillable = true)
    protected String carrierId;
    @XmlElement(name = "CompanyName", required = true, nillable = true)
    protected String companyName;
    @XmlElementRef(name = "SubmissionId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> submissionId;
    @XmlElementRef(name = "RatingAreas", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomProductRatingArea> ratingAreas;
    @XmlElementRef(name = "Plans", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomProductPlan> plans;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the market property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Market }{@code >}
     *     
     */
    public JAXBElement<Market> getMarket() {
        return market;
    }

    /**
     * Sets the value of the market property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Market }{@code >}
     *     
     */
    public void setMarket(JAXBElement<Market> value) {
        this.market = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the carrierId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrierId() {
        return carrierId;
    }

    /**
     * Sets the value of the carrierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrierId(String value) {
        this.carrierId = value;
    }

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the submissionId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSubmissionId() {
        return submissionId;
    }

    /**
     * Sets the value of the submissionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSubmissionId(JAXBElement<Integer> value) {
        this.submissionId = value;
    }

    /**
     * Gets the value of the ratingAreas property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProductRatingArea }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomProductRatingArea> getRatingAreas() {
        return ratingAreas;
    }

    /**
     * Sets the value of the ratingAreas property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProductRatingArea }{@code >}
     *     
     */
    public void setRatingAreas(JAXBElement<ArrayOfCustomProductRatingArea> value) {
        this.ratingAreas = value;
    }

    /**
     * Gets the value of the plans property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlan }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomProductPlan> getPlans() {
        return plans;
    }

    /**
     * Sets the value of the plans property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlan }{@code >}
     *     
     */
    public void setPlans(JAXBElement<ArrayOfCustomProductPlan> value) {
        this.plans = value;
    }

}
