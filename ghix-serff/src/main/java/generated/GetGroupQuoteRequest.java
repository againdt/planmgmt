
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Request">
 *   &lt;complexContent>
 *     &lt;extension base="{}QuoteRequestBase">
 *       &lt;sequence>
 *         &lt;element name="GroupRateKeys" type="{}GetGroupQuote.Request.GroupRateKey" minOccurs="0"/>
 *         &lt;element name="GroupRateFactors" type="{}GetGroupQuote.Request.GroupRateFactor" minOccurs="0"/>
 *         &lt;element name="Preferences" type="{}GetGroupQuote.Request.Preference"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Request", propOrder = {
    "groupRateKeys",
    "groupRateFactors",
    "preferences"
})
public class GetGroupQuoteRequest
    extends QuoteRequestBase
{

    @XmlElementRef(name = "GroupRateKeys", type = JAXBElement.class, required = false)
    protected JAXBElement<GetGroupQuoteRequestGroupRateKey> groupRateKeys;
    @XmlElementRef(name = "GroupRateFactors", type = JAXBElement.class, required = false)
    protected JAXBElement<GetGroupQuoteRequestGroupRateFactor> groupRateFactors;
    @XmlElement(name = "Preferences", required = true, nillable = true)
    protected GetGroupQuoteRequestPreference preferences;

    /**
     * Gets the value of the groupRateKeys property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateKey }{@code >}
     *     
     */
    public JAXBElement<GetGroupQuoteRequestGroupRateKey> getGroupRateKeys() {
        return groupRateKeys;
    }

    /**
     * Sets the value of the groupRateKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateKey }{@code >}
     *     
     */
    public void setGroupRateKeys(JAXBElement<GetGroupQuoteRequestGroupRateKey> value) {
        this.groupRateKeys = value;
    }

    /**
     * Gets the value of the groupRateFactors property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactor }{@code >}
     *     
     */
    public JAXBElement<GetGroupQuoteRequestGroupRateFactor> getGroupRateFactors() {
        return groupRateFactors;
    }

    /**
     * Sets the value of the groupRateFactors property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactor }{@code >}
     *     
     */
    public void setGroupRateFactors(JAXBElement<GetGroupQuoteRequestGroupRateFactor> value) {
        this.groupRateFactors = value;
    }

    /**
     * Gets the value of the preferences property.
     * 
     * @return
     *     possible object is
     *     {@link GetGroupQuoteRequestPreference }
     *     
     */
    public GetGroupQuoteRequestPreference getPreferences() {
        return preferences;
    }

    /**
     * Sets the value of the preferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetGroupQuoteRequestPreference }
     *     
     */
    public void setPreferences(GetGroupQuoteRequestPreference value) {
        this.preferences = value;
    }

}
