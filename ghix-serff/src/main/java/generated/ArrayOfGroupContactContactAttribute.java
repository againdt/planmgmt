
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGroup.Contact.ContactAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGroup.Contact.ContactAttribute">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Group.Contact.ContactAttribute" type="{}Group.Contact.ContactAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGroup.Contact.ContactAttribute", propOrder = {
    "groupContactContactAttribute"
})
public class ArrayOfGroupContactContactAttribute {

    @XmlElement(name = "Group.Contact.ContactAttribute", nillable = true)
    protected List<GroupContactContactAttribute> groupContactContactAttribute;

    /**
     * Gets the value of the groupContactContactAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupContactContactAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupContactContactAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupContactContactAttribute }
     * 
     * 
     */
    public List<GroupContactContactAttribute> getGroupContactContactAttribute() {
        if (groupContactContactAttribute == null) {
            groupContactContactAttribute = new ArrayList<GroupContactContactAttribute>();
        }
        return this.groupContactContactAttribute;
    }

}
