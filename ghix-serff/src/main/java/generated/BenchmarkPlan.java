
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BenchmarkPlan.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BenchmarkPlan">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NoDesignation"/>
 *     &lt;enumeration value="Designate"/>
 *     &lt;enumeration value="Filter"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "BenchmarkPlan")
@XmlEnum
public enum BenchmarkPlan {

    @XmlEnumValue("NoDesignation")
    NO_DESIGNATION("NoDesignation"),
    @XmlEnumValue("Designate")
    DESIGNATE("Designate"),
    @XmlEnumValue("Filter")
    FILTER("Filter");
    private final String value;

    BenchmarkPlan(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static BenchmarkPlan fromValue(String v) {
        for (BenchmarkPlan c: BenchmarkPlan.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
