
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.Response.Quote.MemberPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.Response.Quote.MemberPlan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.Response.Quote.MemberPlan" type="{}GetIfpQuote.Response.Quote.MemberPlan" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.Response.Quote.MemberPlan", propOrder = {
    "getIfpQuoteResponseQuoteMemberPlan"
})
public class ArrayOfGetIfpQuoteResponseQuoteMemberPlan {

    @XmlElement(name = "GetIfpQuote.Response.Quote.MemberPlan", nillable = true)
    protected List<GetIfpQuoteResponseQuoteMemberPlan> getIfpQuoteResponseQuoteMemberPlan;

    /**
     * Gets the value of the getIfpQuoteResponseQuoteMemberPlan property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteResponseQuoteMemberPlan property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteResponseQuoteMemberPlan().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteResponseQuoteMemberPlan }
     * 
     * 
     */
    public List<GetIfpQuoteResponseQuoteMemberPlan> getGetIfpQuoteResponseQuoteMemberPlan() {
        if (getIfpQuoteResponseQuoteMemberPlan == null) {
            getIfpQuoteResponseQuoteMemberPlan = new ArrayList<GetIfpQuoteResponseQuoteMemberPlan>();
        }
        return this.getIfpQuoteResponseQuoteMemberPlan;
    }

}
