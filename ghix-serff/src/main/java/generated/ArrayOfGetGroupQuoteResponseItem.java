
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Response.Item complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Response.Item">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Response.Item" type="{}GetGroupQuote.Response.Item" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Response.Item", propOrder = {
    "getGroupQuoteResponseItem"
})
public class ArrayOfGetGroupQuoteResponseItem {

    @XmlElement(name = "GetGroupQuote.Response.Item", nillable = true)
    protected List<GetGroupQuoteResponseItem> getGroupQuoteResponseItem;

    /**
     * Gets the value of the getGroupQuoteResponseItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteResponseItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteResponseItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteResponseItem }
     * 
     * 
     */
    public List<GetGroupQuoteResponseItem> getGetGroupQuoteResponseItem() {
        if (getGroupQuoteResponseItem == null) {
            getGroupQuoteResponseItem = new ArrayList<GetGroupQuoteResponseItem>();
        }
        return this.getGroupQuoteResponseItem;
    }

}
