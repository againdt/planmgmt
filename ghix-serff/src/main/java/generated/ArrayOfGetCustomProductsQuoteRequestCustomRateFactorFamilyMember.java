
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family.Member complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family.Member">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomProductsQuote.Request.CustomRateFactor.Family.Member" type="{}GetCustomProductsQuote.Request.CustomRateFactor.Family.Member" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family.Member", propOrder = {
    "getCustomProductsQuoteRequestCustomRateFactorFamilyMember"
})
public class ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember {

    @XmlElement(name = "GetCustomProductsQuote.Request.CustomRateFactor.Family.Member", nillable = true)
    protected List<GetCustomProductsQuoteRequestCustomRateFactorFamilyMember> getCustomProductsQuoteRequestCustomRateFactorFamilyMember;

    /**
     * Gets the value of the getCustomProductsQuoteRequestCustomRateFactorFamilyMember property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCustomProductsQuoteRequestCustomRateFactorFamilyMember property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCustomProductsQuoteRequestCustomRateFactorFamilyMember().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCustomProductsQuoteRequestCustomRateFactorFamilyMember }
     * 
     * 
     */
    public List<GetCustomProductsQuoteRequestCustomRateFactorFamilyMember> getGetCustomProductsQuoteRequestCustomRateFactorFamilyMember() {
        if (getCustomProductsQuoteRequestCustomRateFactorFamilyMember == null) {
            getCustomProductsQuoteRequestCustomRateFactorFamilyMember = new ArrayList<GetCustomProductsQuoteRequestCustomRateFactorFamilyMember>();
        }
        return this.getCustomProductsQuoteRequestCustomRateFactorFamilyMember;
    }

}
