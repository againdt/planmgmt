
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Response.Quote.EmployeePlan.Carrier" type="{}GetGroupQuote.Response.Quote.EmployeePlan.Carrier" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier", propOrder = {
    "getGroupQuoteResponseQuoteEmployeePlanCarrier"
})
public class ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier {

    @XmlElement(name = "GetGroupQuote.Response.Quote.EmployeePlan.Carrier", nillable = true)
    protected List<GetGroupQuoteResponseQuoteEmployeePlanCarrier> getGroupQuoteResponseQuoteEmployeePlanCarrier;

    /**
     * Gets the value of the getGroupQuoteResponseQuoteEmployeePlanCarrier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteResponseQuoteEmployeePlanCarrier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteResponseQuoteEmployeePlanCarrier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteResponseQuoteEmployeePlanCarrier }
     * 
     * 
     */
    public List<GetGroupQuoteResponseQuoteEmployeePlanCarrier> getGetGroupQuoteResponseQuoteEmployeePlanCarrier() {
        if (getGroupQuoteResponseQuoteEmployeePlanCarrier == null) {
            getGroupQuoteResponseQuoteEmployeePlanCarrier = new ArrayList<GetGroupQuoteResponseQuoteEmployeePlanCarrier>();
        }
        return this.getGroupQuoteResponseQuoteEmployeePlanCarrier;
    }

}
