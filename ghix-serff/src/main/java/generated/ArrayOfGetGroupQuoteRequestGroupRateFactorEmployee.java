
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Request.GroupRateFactor.Employee" type="{}GetGroupQuote.Request.GroupRateFactor.Employee" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee", propOrder = {
    "getGroupQuoteRequestGroupRateFactorEmployee"
})
public class ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee {

    @XmlElement(name = "GetGroupQuote.Request.GroupRateFactor.Employee", nillable = true)
    protected List<GetGroupQuoteRequestGroupRateFactorEmployee> getGroupQuoteRequestGroupRateFactorEmployee;

    /**
     * Gets the value of the getGroupQuoteRequestGroupRateFactorEmployee property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteRequestGroupRateFactorEmployee property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteRequestGroupRateFactorEmployee().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteRequestGroupRateFactorEmployee }
     * 
     * 
     */
    public List<GetGroupQuoteRequestGroupRateFactorEmployee> getGetGroupQuoteRequestGroupRateFactorEmployee() {
        if (getGroupQuoteRequestGroupRateFactorEmployee == null) {
            getGroupQuoteRequestGroupRateFactorEmployee = new ArrayList<GetGroupQuoteRequestGroupRateFactorEmployee>();
        }
        return this.getGroupQuoteRequestGroupRateFactorEmployee;
    }

}
