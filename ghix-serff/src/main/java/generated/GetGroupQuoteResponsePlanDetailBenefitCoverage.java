
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response.PlanDetail.Benefit.Coverage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response.PlanDetail.Benefit.Coverage">
 *   &lt;complexContent>
 *     &lt;extension base="{}CoverageBase">
 *       &lt;sequence>
 *         &lt;element name="ViewPoints" type="{}ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response.PlanDetail.Benefit.Coverage", propOrder = {
    "viewPoints"
})
public class GetGroupQuoteResponsePlanDetailBenefitCoverage
    extends CoverageBase
{

    @XmlElementRef(name = "ViewPoints", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint> viewPoints;

    /**
     * Gets the value of the viewPoints property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint> getViewPoints() {
        return viewPoints;
    }

    /**
     * Sets the value of the viewPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint }{@code >}
     *     
     */
    public void setViewPoints(JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint> value) {
        this.viewPoints = value;
    }

}
