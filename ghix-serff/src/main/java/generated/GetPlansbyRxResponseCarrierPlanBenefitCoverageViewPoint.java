
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint">
 *   &lt;complexContent>
 *     &lt;extension base="{}ViewPointBase">
 *       &lt;sequence>
 *         &lt;element name="Services" type="{}ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint", propOrder = {
    "services"
})
public class GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint
    extends ViewPointBase
{

    @XmlElementRef(name = "Services", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService> services;

    /**
     * Gets the value of the services property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService> getServices() {
        return services;
    }

    /**
     * Sets the value of the services property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService }{@code >}
     *     
     */
    public void setServices(JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService> value) {
        this.services = value;
    }

}
