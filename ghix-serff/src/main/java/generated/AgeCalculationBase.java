
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AgeCalculationBase.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AgeCalculationBase">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="EffectiveDate"/>
 *     &lt;enumeration value="Jan1st"/>
 *     &lt;enumeration value="Quarter1st"/>
 *     &lt;enumeration value="SemiYear1st"/>
 *     &lt;enumeration value="InsuranceDate"/>
 *     &lt;enumeration value="EffectiveMonthEnd"/>
 *     &lt;enumeration value="EffectiveJan1st"/>
 *     &lt;enumeration value="EffectiveQuarter1st"/>
 *     &lt;enumeration value="EffectiveSemiYear1st"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AgeCalculationBase")
@XmlEnum
public enum AgeCalculationBase {

    @XmlEnumValue("EffectiveDate")
    EFFECTIVE_DATE("EffectiveDate"),
    @XmlEnumValue("Jan1st")
    JAN_1_ST("Jan1st"),
    @XmlEnumValue("Quarter1st")
    QUARTER_1_ST("Quarter1st"),
    @XmlEnumValue("SemiYear1st")
    SEMI_YEAR_1_ST("SemiYear1st"),
    @XmlEnumValue("InsuranceDate")
    INSURANCE_DATE("InsuranceDate"),
    @XmlEnumValue("EffectiveMonthEnd")
    EFFECTIVE_MONTH_END("EffectiveMonthEnd"),
    @XmlEnumValue("EffectiveJan1st")
    EFFECTIVE_JAN_1_ST("EffectiveJan1st"),
    @XmlEnumValue("EffectiveQuarter1st")
    EFFECTIVE_QUARTER_1_ST("EffectiveQuarter1st"),
    @XmlEnumValue("EffectiveSemiYear1st")
    EFFECTIVE_SEMI_YEAR_1_ST("EffectiveSemiYear1st");
    private final String value;

    AgeCalculationBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AgeCalculationBase fromValue(String v) {
        for (AgeCalculationBase c: AgeCalculationBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
