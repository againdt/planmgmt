
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Request">
 *   &lt;complexContent>
 *     &lt;extension base="{}QuoteRequestBase">
 *       &lt;sequence>
 *         &lt;element name="IfpRateKeys" type="{}GetIfpQuote.Request.IfpRateKey" minOccurs="0"/>
 *         &lt;element name="IfpRateFactors" type="{}GetIfpQuote.Request.IfpRateFactor" minOccurs="0"/>
 *         &lt;element name="Preferences" type="{}GetIfpQuote.Request.Preference"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Request", propOrder = {
    "ifpRateKeys",
    "ifpRateFactors",
    "preferences"
})
public class GetIfpQuoteRequest
    extends QuoteRequestBase
{

    @XmlElementRef(name = "IfpRateKeys", type = JAXBElement.class, required = false)
    protected JAXBElement<GetIfpQuoteRequestIfpRateKey> ifpRateKeys;
    @XmlElementRef(name = "IfpRateFactors", type = JAXBElement.class, required = false)
    protected JAXBElement<GetIfpQuoteRequestIfpRateFactor> ifpRateFactors;
    @XmlElement(name = "Preferences", required = true, nillable = true)
    protected GetIfpQuoteRequestPreference preferences;

    /**
     * Gets the value of the ifpRateKeys property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestIfpRateKey }{@code >}
     *     
     */
    public JAXBElement<GetIfpQuoteRequestIfpRateKey> getIfpRateKeys() {
        return ifpRateKeys;
    }

    /**
     * Sets the value of the ifpRateKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestIfpRateKey }{@code >}
     *     
     */
    public void setIfpRateKeys(JAXBElement<GetIfpQuoteRequestIfpRateKey> value) {
        this.ifpRateKeys = value;
    }

    /**
     * Gets the value of the ifpRateFactors property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestIfpRateFactor }{@code >}
     *     
     */
    public JAXBElement<GetIfpQuoteRequestIfpRateFactor> getIfpRateFactors() {
        return ifpRateFactors;
    }

    /**
     * Sets the value of the ifpRateFactors property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestIfpRateFactor }{@code >}
     *     
     */
    public void setIfpRateFactors(JAXBElement<GetIfpQuoteRequestIfpRateFactor> value) {
        this.ifpRateFactors = value;
    }

    /**
     * Gets the value of the preferences property.
     * 
     * @return
     *     possible object is
     *     {@link GetIfpQuoteRequestPreference }
     *     
     */
    public GetIfpQuoteRequestPreference getPreferences() {
        return preferences;
    }

    /**
     * Sets the value of the preferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetIfpQuoteRequestPreference }
     *     
     */
    public void setPreferences(GetIfpQuoteRequestPreference value) {
        this.preferences = value;
    }

}
