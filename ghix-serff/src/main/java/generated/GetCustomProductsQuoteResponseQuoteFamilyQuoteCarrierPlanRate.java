
package generated;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlanId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RatingMethod" type="{}RatingMethod"/>
 *         &lt;element name="RatingAreaBase" type="{}RatingAreaBase"/>
 *         &lt;element name="Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SmokerSurcharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepSmokerSurcharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SSRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SD1Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SD2Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SD3Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="F1Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="F2Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="F3Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="IsShortCoverage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CoverageTier" type="{}CoverageTier"/>
 *         &lt;element name="PlanName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MemberRates" type="{}ArrayOfGetCustomProductsQuote.Response.MemberRate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate", propOrder = {
    "planId",
    "ratingMethod",
    "ratingAreaBase",
    "rate",
    "depRate",
    "smokerSurcharge",
    "depSmokerSurcharge",
    "sRate",
    "ssRate",
    "sd1Rate",
    "sd2Rate",
    "sd3Rate",
    "f1Rate",
    "f2Rate",
    "f3Rate",
    "isShortCoverage",
    "coverageTier",
    "planName",
    "memberRates"
})
public class GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate {

    @XmlElement(name = "PlanId", required = true, nillable = true)
    protected String planId;
    @XmlElement(name = "RatingMethod", required = true)
    protected RatingMethod ratingMethod;
    @XmlElement(name = "RatingAreaBase", required = true)
    protected RatingAreaBase ratingAreaBase;
    @XmlElementRef(name = "Rate", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> rate;
    @XmlElementRef(name = "DepRate", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> depRate;
    @XmlElementRef(name = "SmokerSurcharge", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> smokerSurcharge;
    @XmlElementRef(name = "DepSmokerSurcharge", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> depSmokerSurcharge;
    @XmlElementRef(name = "SRate", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> sRate;
    @XmlElementRef(name = "SSRate", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> ssRate;
    @XmlElementRef(name = "SD1Rate", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> sd1Rate;
    @XmlElementRef(name = "SD2Rate", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> sd2Rate;
    @XmlElementRef(name = "SD3Rate", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> sd3Rate;
    @XmlElementRef(name = "F1Rate", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> f1Rate;
    @XmlElementRef(name = "F2Rate", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> f2Rate;
    @XmlElementRef(name = "F3Rate", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> f3Rate;
    @XmlElement(name = "IsShortCoverage")
    protected boolean isShortCoverage;
    @XmlElement(name = "CoverageTier", required = true)
    protected CoverageTier coverageTier;
    @XmlElementRef(name = "PlanName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planName;
    @XmlElementRef(name = "MemberRates", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCustomProductsQuoteResponseMemberRate> memberRates;

    /**
     * Gets the value of the planId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanId() {
        return planId;
    }

    /**
     * Sets the value of the planId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanId(String value) {
        this.planId = value;
    }

    /**
     * Gets the value of the ratingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link RatingMethod }
     *     
     */
    public RatingMethod getRatingMethod() {
        return ratingMethod;
    }

    /**
     * Sets the value of the ratingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link RatingMethod }
     *     
     */
    public void setRatingMethod(RatingMethod value) {
        this.ratingMethod = value;
    }

    /**
     * Gets the value of the ratingAreaBase property.
     * 
     * @return
     *     possible object is
     *     {@link RatingAreaBase }
     *     
     */
    public RatingAreaBase getRatingAreaBase() {
        return ratingAreaBase;
    }

    /**
     * Sets the value of the ratingAreaBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link RatingAreaBase }
     *     
     */
    public void setRatingAreaBase(RatingAreaBase value) {
        this.ratingAreaBase = value;
    }

    /**
     * Gets the value of the rate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setRate(JAXBElement<BigDecimal> value) {
        this.rate = value;
    }

    /**
     * Gets the value of the depRate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getDepRate() {
        return depRate;
    }

    /**
     * Sets the value of the depRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setDepRate(JAXBElement<BigDecimal> value) {
        this.depRate = value;
    }

    /**
     * Gets the value of the smokerSurcharge property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getSmokerSurcharge() {
        return smokerSurcharge;
    }

    /**
     * Sets the value of the smokerSurcharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setSmokerSurcharge(JAXBElement<BigDecimal> value) {
        this.smokerSurcharge = value;
    }

    /**
     * Gets the value of the depSmokerSurcharge property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getDepSmokerSurcharge() {
        return depSmokerSurcharge;
    }

    /**
     * Sets the value of the depSmokerSurcharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setDepSmokerSurcharge(JAXBElement<BigDecimal> value) {
        this.depSmokerSurcharge = value;
    }

    /**
     * Gets the value of the sRate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getSRate() {
        return sRate;
    }

    /**
     * Sets the value of the sRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setSRate(JAXBElement<BigDecimal> value) {
        this.sRate = value;
    }

    /**
     * Gets the value of the ssRate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getSSRate() {
        return ssRate;
    }

    /**
     * Sets the value of the ssRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setSSRate(JAXBElement<BigDecimal> value) {
        this.ssRate = value;
    }

    /**
     * Gets the value of the sd1Rate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getSD1Rate() {
        return sd1Rate;
    }

    /**
     * Sets the value of the sd1Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setSD1Rate(JAXBElement<BigDecimal> value) {
        this.sd1Rate = value;
    }

    /**
     * Gets the value of the sd2Rate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getSD2Rate() {
        return sd2Rate;
    }

    /**
     * Sets the value of the sd2Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setSD2Rate(JAXBElement<BigDecimal> value) {
        this.sd2Rate = value;
    }

    /**
     * Gets the value of the sd3Rate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getSD3Rate() {
        return sd3Rate;
    }

    /**
     * Sets the value of the sd3Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setSD3Rate(JAXBElement<BigDecimal> value) {
        this.sd3Rate = value;
    }

    /**
     * Gets the value of the f1Rate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getF1Rate() {
        return f1Rate;
    }

    /**
     * Sets the value of the f1Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setF1Rate(JAXBElement<BigDecimal> value) {
        this.f1Rate = value;
    }

    /**
     * Gets the value of the f2Rate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getF2Rate() {
        return f2Rate;
    }

    /**
     * Sets the value of the f2Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setF2Rate(JAXBElement<BigDecimal> value) {
        this.f2Rate = value;
    }

    /**
     * Gets the value of the f3Rate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getF3Rate() {
        return f3Rate;
    }

    /**
     * Sets the value of the f3Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setF3Rate(JAXBElement<BigDecimal> value) {
        this.f3Rate = value;
    }

    /**
     * Gets the value of the isShortCoverage property.
     * 
     */
    public boolean isIsShortCoverage() {
        return isShortCoverage;
    }

    /**
     * Sets the value of the isShortCoverage property.
     * 
     */
    public void setIsShortCoverage(boolean value) {
        this.isShortCoverage = value;
    }

    /**
     * Gets the value of the coverageTier property.
     * 
     * @return
     *     possible object is
     *     {@link CoverageTier }
     *     
     */
    public CoverageTier getCoverageTier() {
        return coverageTier;
    }

    /**
     * Sets the value of the coverageTier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoverageTier }
     *     
     */
    public void setCoverageTier(CoverageTier value) {
        this.coverageTier = value;
    }

    /**
     * Gets the value of the planName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanName() {
        return planName;
    }

    /**
     * Sets the value of the planName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanName(JAXBElement<String> value) {
        this.planName = value;
    }

    /**
     * Gets the value of the memberRates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseMemberRate }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseMemberRate> getMemberRates() {
        return memberRates;
    }

    /**
     * Sets the value of the memberRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseMemberRate }{@code >}
     *     
     */
    public void setMemberRates(JAXBElement<ArrayOfGetCustomProductsQuoteResponseMemberRate> value) {
        this.memberRates = value;
    }

}
