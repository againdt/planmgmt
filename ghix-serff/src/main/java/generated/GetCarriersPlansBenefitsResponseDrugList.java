
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarriersPlansBenefits.Response.DrugList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarriersPlansBenefits.Response.DrugList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DrugListId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Drugs" type="{}ArrayOfGetCarriersPlansBenefits.Response.DrugList.Drug" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarriersPlansBenefits.Response.DrugList", propOrder = {
    "drugListId",
    "drugs"
})
public class GetCarriersPlansBenefitsResponseDrugList {

    @XmlElement(name = "DrugListId")
    protected int drugListId;
    @XmlElementRef(name = "Drugs", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug> drugs;

    /**
     * Gets the value of the drugListId property.
     * 
     */
    public int getDrugListId() {
        return drugListId;
    }

    /**
     * Sets the value of the drugListId property.
     * 
     */
    public void setDrugListId(int value) {
        this.drugListId = value;
    }

    /**
     * Gets the value of the drugs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug> getDrugs() {
        return drugs;
    }

    /**
     * Sets the value of the drugs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug }{@code >}
     *     
     */
    public void setDrugs(JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug> value) {
        this.drugs = value;
    }

}
