
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListRx.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListRx.Response">
 *   &lt;complexContent>
 *     &lt;extension base="{}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="Drugs" type="{}ArrayOfListRx.Response.Drug" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListRx.Response", propOrder = {
    "drugs"
})
public class ListRxResponse
    extends ResponseBase
{

    @XmlElementRef(name = "Drugs", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfListRxResponseDrug> drugs;

    /**
     * Gets the value of the drugs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfListRxResponseDrug }{@code >}
     *     
     */
    public JAXBElement<ArrayOfListRxResponseDrug> getDrugs() {
        return drugs;
    }

    /**
     * Sets the value of the drugs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfListRxResponseDrug }{@code >}
     *     
     */
    public void setDrugs(JAXBElement<ArrayOfListRxResponseDrug> value) {
        this.drugs = value;
    }

}
