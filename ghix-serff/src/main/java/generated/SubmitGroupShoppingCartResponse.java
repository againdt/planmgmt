
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmitGroupShoppingCart.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitGroupShoppingCart.Response">
 *   &lt;complexContent>
 *     &lt;extension base="{}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="ShoppingCart" type="{}GroupShoppingCart" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitGroupShoppingCart.Response", propOrder = {
    "shoppingCart"
})
public class SubmitGroupShoppingCartResponse
    extends ResponseBase
{

    @XmlElementRef(name = "ShoppingCart", type = JAXBElement.class, required = false)
    protected JAXBElement<GroupShoppingCart> shoppingCart;

    /**
     * Gets the value of the shoppingCart property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GroupShoppingCart }{@code >}
     *     
     */
    public JAXBElement<GroupShoppingCart> getShoppingCart() {
        return shoppingCart;
    }

    /**
     * Sets the value of the shoppingCart property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GroupShoppingCart }{@code >}
     *     
     */
    public void setShoppingCart(JAXBElement<GroupShoppingCart> value) {
        this.shoppingCart = value;
    }

}
