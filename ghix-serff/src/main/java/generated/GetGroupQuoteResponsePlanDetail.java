
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response.PlanDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response.PlanDetail">
 *   &lt;complexContent>
 *     &lt;extension base="{}PlanDetailsBase">
 *       &lt;sequence>
 *         &lt;element name="PlanData" type="{}ArrayOfGetGroupQuote.Response.Item" minOccurs="0"/>
 *         &lt;element name="Benefits" type="{}ArrayOfGetGroupQuote.Response.PlanDetail.Benefit" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response.PlanDetail", propOrder = {
    "planData",
    "benefits"
})
public class GetGroupQuoteResponsePlanDetail
    extends PlanDetailsBase
{

    @XmlElementRef(name = "PlanData", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteResponseItem> planData;
    @XmlElementRef(name = "Benefits", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefit> benefits;

    /**
     * Gets the value of the planData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteResponseItem> getPlanData() {
        return planData;
    }

    /**
     * Sets the value of the planData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseItem }{@code >}
     *     
     */
    public void setPlanData(JAXBElement<ArrayOfGetGroupQuoteResponseItem> value) {
        this.planData = value;
    }

    /**
     * Gets the value of the benefits property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefit }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefit> getBenefits() {
        return benefits;
    }

    /**
     * Sets the value of the benefits property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefit }{@code >}
     *     
     */
    public void setBenefits(JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefit> value) {
        this.benefits = value;
    }

}
