
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoverageType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CoverageType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="InNetwork"/>
 *     &lt;enumeration value="OutOfNetwork"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CoverageType")
@XmlEnum
public enum CoverageType {

    @XmlEnumValue("InNetwork")
    IN_NETWORK("InNetwork"),
    @XmlEnumValue("OutOfNetwork")
    OUT_OF_NETWORK("OutOfNetwork");
    private final String value;

    CoverageType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CoverageType fromValue(String v) {
        for (CoverageType c: CoverageType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
