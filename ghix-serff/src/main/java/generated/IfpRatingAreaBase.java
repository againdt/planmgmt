
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IfpRatingAreaBase.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IfpRatingAreaBase">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FamilyResidence"/>
 *     &lt;enumeration value="MemberResidence"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IfpRatingAreaBase")
@XmlEnum
public enum IfpRatingAreaBase {

    @XmlEnumValue("FamilyResidence")
    FAMILY_RESIDENCE("FamilyResidence"),
    @XmlEnumValue("MemberResidence")
    MEMBER_RESIDENCE("MemberResidence");
    private final String value;

    IfpRatingAreaBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IfpRatingAreaBase fromValue(String v) {
        for (IfpRatingAreaBase c: IfpRatingAreaBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
