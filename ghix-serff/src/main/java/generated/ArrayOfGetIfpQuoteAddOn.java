
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.AddOn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.AddOn">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.AddOn" type="{}GetIfpQuote.AddOn" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.AddOn", propOrder = {
    "getIfpQuoteAddOn"
})
public class ArrayOfGetIfpQuoteAddOn {

    @XmlElement(name = "GetIfpQuote.AddOn")
    protected List<GetIfpQuoteAddOn> getIfpQuoteAddOn;

    /**
     * Gets the value of the getIfpQuoteAddOn property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteAddOn property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteAddOn().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteAddOn }
     * 
     * 
     */
    public List<GetIfpQuoteAddOn> getGetIfpQuoteAddOn() {
        if (getIfpQuoteAddOn == null) {
            getIfpQuoteAddOn = new ArrayList<GetIfpQuoteAddOn>();
        }
        return this.getIfpQuoteAddOn;
    }

}
