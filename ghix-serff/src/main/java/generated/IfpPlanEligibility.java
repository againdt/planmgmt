
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IfpPlanEligibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IfpPlanEligibility">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FullEligibilityOnly"/>
 *     &lt;enumeration value="ShortPlansOK"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IfpPlanEligibility")
@XmlEnum
public enum IfpPlanEligibility {

    @XmlEnumValue("FullEligibilityOnly")
    FULL_ELIGIBILITY_ONLY("FullEligibilityOnly"),
    @XmlEnumValue("ShortPlansOK")
    SHORT_PLANS_OK("ShortPlansOK");
    private final String value;

    IfpPlanEligibility(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IfpPlanEligibility fromValue(String v) {
        for (IfpPlanEligibility c: IfpPlanEligibility.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
