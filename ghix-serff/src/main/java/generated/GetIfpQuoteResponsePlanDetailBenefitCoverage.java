
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response.PlanDetail.Benefit.Coverage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response.PlanDetail.Benefit.Coverage">
 *   &lt;complexContent>
 *     &lt;extension base="{}CoverageBase">
 *       &lt;sequence>
 *         &lt;element name="ViewPoints" type="{}ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response.PlanDetail.Benefit.Coverage", propOrder = {
    "viewPoints"
})
public class GetIfpQuoteResponsePlanDetailBenefitCoverage
    extends CoverageBase
{

    @XmlElementRef(name = "ViewPoints", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint> viewPoints;

    /**
     * Gets the value of the viewPoints property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint> getViewPoints() {
        return viewPoints;
    }

    /**
     * Sets the value of the viewPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint }{@code >}
     *     
     */
    public void setViewPoints(JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint> value) {
        this.viewPoints = value;
    }

}
