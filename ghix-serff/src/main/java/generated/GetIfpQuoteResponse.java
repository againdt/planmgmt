
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IfpQuote" type="{}GetIfpQuote.Response.Quote"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response", propOrder = {
    "ifpQuote"
})
public class GetIfpQuoteResponse {

    @XmlElement(name = "IfpQuote", required = true, nillable = true)
    protected GetIfpQuoteResponseQuote ifpQuote;

    /**
     * Gets the value of the ifpQuote property.
     * 
     * @return
     *     possible object is
     *     {@link GetIfpQuoteResponseQuote }
     *     
     */
    public GetIfpQuoteResponseQuote getIfpQuote() {
        return ifpQuote;
    }

    /**
     * Sets the value of the ifpQuote property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetIfpQuoteResponseQuote }
     *     
     */
    public void setIfpQuote(GetIfpQuoteResponseQuote value) {
        this.ifpQuote = value;
    }

}
