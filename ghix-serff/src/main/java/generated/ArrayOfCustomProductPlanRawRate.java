
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCustomProduct.Plan.RawRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomProduct.Plan.RawRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomProduct.Plan.RawRate" type="{}CustomProduct.Plan.RawRate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomProduct.Plan.RawRate", propOrder = {
    "customProductPlanRawRate"
})
public class ArrayOfCustomProductPlanRawRate {

    @XmlElement(name = "CustomProduct.Plan.RawRate", nillable = true)
    protected List<CustomProductPlanRawRate> customProductPlanRawRate;

    /**
     * Gets the value of the customProductPlanRawRate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customProductPlanRawRate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomProductPlanRawRate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomProductPlanRawRate }
     * 
     * 
     */
    public List<CustomProductPlanRawRate> getCustomProductPlanRawRate() {
        if (customProductPlanRawRate == null) {
            customProductPlanRawRate = new ArrayList<CustomProductPlanRawRate>();
        }
        return this.customProductPlanRawRate;
    }

}
