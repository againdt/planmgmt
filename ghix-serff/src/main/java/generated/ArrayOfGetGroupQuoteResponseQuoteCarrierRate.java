
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Response.Quote.CarrierRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Response.Quote.CarrierRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Response.Quote.CarrierRate" type="{}GetGroupQuote.Response.Quote.CarrierRate" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Response.Quote.CarrierRate", propOrder = {
    "getGroupQuoteResponseQuoteCarrierRate"
})
public class ArrayOfGetGroupQuoteResponseQuoteCarrierRate {

    @XmlElement(name = "GetGroupQuote.Response.Quote.CarrierRate", nillable = true)
    protected List<GetGroupQuoteResponseQuoteCarrierRate> getGroupQuoteResponseQuoteCarrierRate;

    /**
     * Gets the value of the getGroupQuoteResponseQuoteCarrierRate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteResponseQuoteCarrierRate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteResponseQuoteCarrierRate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteResponseQuoteCarrierRate }
     * 
     * 
     */
    public List<GetGroupQuoteResponseQuoteCarrierRate> getGetGroupQuoteResponseQuoteCarrierRate() {
        if (getGroupQuoteResponseQuoteCarrierRate == null) {
            getGroupQuoteResponseQuoteCarrierRate = new ArrayList<GetGroupQuoteResponseQuoteCarrierRate>();
        }
        return this.getGroupQuoteResponseQuoteCarrierRate;
    }

}
