
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LevelOfBenefitAddOns.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LevelOfBenefitAddOns">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Benefits"/>
 *     &lt;enumeration value="Coverages"/>
 *     &lt;enumeration value="ViewPoints"/>
 *     &lt;enumeration value="Services"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LevelOfBenefitAddOns")
@XmlEnum
public enum LevelOfBenefitAddOns {

    @XmlEnumValue("Benefits")
    BENEFITS("Benefits"),
    @XmlEnumValue("Coverages")
    COVERAGES("Coverages"),
    @XmlEnumValue("ViewPoints")
    VIEW_POINTS("ViewPoints"),
    @XmlEnumValue("Services")
    SERVICES("Services");
    private final String value;

    LevelOfBenefitAddOns(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LevelOfBenefitAddOns fromValue(String v) {
        for (LevelOfBenefitAddOns c: LevelOfBenefitAddOns.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
