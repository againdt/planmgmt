
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.Contact complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.Contact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.Contact" type="{}GetCarriersPlansBenefits.Response.Contact" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.Contact", propOrder = {
    "getCarriersPlansBenefitsResponseContact"
})
public class ArrayOfGetCarriersPlansBenefitsResponseContact {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.Contact", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseContact> getCarriersPlansBenefitsResponseContact;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseContact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseContact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseContact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseContact }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseContact> getGetCarriersPlansBenefitsResponseContact() {
        if (getCarriersPlansBenefitsResponseContact == null) {
            getCarriersPlansBenefitsResponseContact = new ArrayList<GetCarriersPlansBenefitsResponseContact>();
        }
        return this.getCarriersPlansBenefitsResponseContact;
    }

}
