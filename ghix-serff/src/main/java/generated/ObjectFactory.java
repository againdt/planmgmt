
package generated;

import java.math.BigDecimal;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfint;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GroupLocationAddress_QNAME = new QName("", "Group.LocationAddress");
    private final static QName _CityBase_QNAME = new QName("", "CityBase");
    private final static QName _FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier_QNAME = new QName("", "FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier");
    private final static QName _GetCountiesRequest_QNAME = new QName("", "GetCounties.Request");
    private final static QName _IsPreferred_QNAME = new QName("", "IsPreferred");
    private final static QName _ArrayOfGetCarriersPlansBenefitsRequestVersionFilter_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Request.VersionFilter");
    private final static QName _ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO_QNAME = new QName("", "ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO");
    private final static QName _FormularyLookupSortBy_QNAME = new QName("", "FormularyLookup.SortBy");
    private final static QName _ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier_QNAME = new QName("", "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier");
    private final static QName _GroupShoppingCartProductChoicePlanChoices_QNAME = new QName("", "GroupShoppingCart.ProductChoice.PlanChoices");
    private final static QName _QuoteRequestBase_QNAME = new QName("", "QuoteRequestBase");
    private final static QName _GetGroupQuoteRequestGroupRateFactor_QNAME = new QName("", "GetGroupQuote.Request.GroupRateFactor");
    private final static QName _GetIfpQuoteResponse_QNAME = new QName("", "GetIfpQuote.Response");
    private final static QName _GetGroupQuoteRequestPreferenceDrugFilter_QNAME = new QName("", "GetGroupQuote.Request.Preference.DrugFilter");
    private final static QName _ArrayOfGetGroupQuoteResponseMemberRate_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.MemberRate");
    private final static QName _ListRxRequest_QNAME = new QName("", "ListRx.Request");
    private final static QName _DataSet_QNAME = new QName("", "DataSet");
    private final static QName _GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate_QNAME = new QName("", "GetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate");
    private final static QName _ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter");
    private final static QName _ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate");
    private final static QName _GetIfpQuoteResponsePlanDetailBenefitCoverage_QNAME = new QName("", "GetIfpQuote.Response.PlanDetail.Benefit.Coverage");
    private final static QName _GetPlansbyRxRequest_QNAME = new QName("", "GetPlansbyRx.Request");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseContact_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Contact");
    private final static QName _FormularyLookupRequestPlanFilter_QNAME = new QName("", "FormularyLookup.Request.PlanFilter");
    private final static QName _GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService_QNAME = new QName("", "GetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint.Service");
    private final static QName _ArrayOfGroupEmployeeMember_QNAME = new QName("", "ArrayOfGroup.Employee.Member");
    private final static QName _ClassCarveOut_QNAME = new QName("", "ClassCarveOut");
    private final static QName _GetGroupQuoteRequestPlanVisibility_QNAME = new QName("", "GetGroupQuote.Request.PlanVisibility");
    private final static QName _FetchCustomProductsRequestAccessKey_QNAME = new QName("", "FetchCustomProducts.Request.AccessKey");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink");
    private final static QName _GetCarriersPlansBenefitsRequestInput_QNAME = new QName("", "GetCarriersPlansBenefits.Request.Input");
    private final static QName _ScenarioBase_QNAME = new QName("", "ScenarioBase");
    private final static QName _ArrayOfGroupScenario_QNAME = new QName("", "ArrayOfGroup.Scenario");
    private final static QName _RateUnit_QNAME = new QName("", "RateUnit");
    private final static QName _ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.Quote.CarrierRate.PlanRate");
    private final static QName _GetCarriersPlansBenefitsResponseContact_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Contact");
    private final static QName _GetCustomProductsQuoteResponseMemberRate_QNAME = new QName("", "GetCustomProductsQuote.Response.MemberRate");
    private final static QName _ArrayOfGetPlansbyRxResponseCarrier_QNAME = new QName("", "ArrayOfGetPlansbyRx.Response.Carrier");
    private final static QName _ArrayOfGetIfpQuoteRequestProductChoice_QNAME = new QName("", "ArrayOfGetIfpQuote.Request.ProductChoice");
    private final static QName _ArrayOfFormularyLookupResponseDrugAttribute_QNAME = new QName("", "ArrayOfFormularyLookup.Response.DrugAttribute");
    private final static QName _GetGroupQuoteRequestGroupRateFactorEmployee_QNAME = new QName("", "GetGroupQuote.Request.GroupRateFactor.Employee");
    private final static QName _GetIfpQuoteRequestMember_QNAME = new QName("", "GetIfpQuote.Request.Member");
    private final static QName _GetGroupQuoteResponseQuoteCarrierRatePlanRate_QNAME = new QName("", "GetGroupQuote.Response.Quote.CarrierRate.PlanRate");
    private final static QName _GetIfpQuoteRequestPreferenceBenchmarkRules_QNAME = new QName("", "GetIfpQuote.Request.Preference.BenchmarkRules");
    private final static QName _GetIfpQuoteRequestPreference_QNAME = new QName("", "GetIfpQuote.Request.Preference");
    private final static QName _GroupContactContactAttribute_QNAME = new QName("", "Group.Contact.ContactAttribute");
    private final static QName _ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint.Service");
    private final static QName _GroupScenario_QNAME = new QName("", "Group.Scenario");
    private final static QName _CarrierRateBase_QNAME = new QName("", "CarrierRateBase");
    private final static QName _GetCarriersPlansBenefitsResponseDrugList_QNAME = new QName("", "GetCarriersPlansBenefits.Response.DrugList");
    private final static QName _PlanVisibility_QNAME = new QName("", "PlanVisibility");
    private final static QName _FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug_QNAME = new QName("", "FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug");
    private final static QName _PlanChoiceBase_QNAME = new QName("", "PlanChoiceBase");
    private final static QName _GetIfpQuoteResponseItem_QNAME = new QName("", "GetIfpQuote.Response.Item");
    private final static QName _GetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter_QNAME = new QName("", "GetIfpQuote.Request.Preference.BenefitFilter.AttributeFilter");
    private final static QName _ChangeType_QNAME = new QName("", "ChangeType");
    private final static QName _PharmacyType_QNAME = new QName("", "PharmacyType");
    private final static QName _Towards_QNAME = new QName("", "Towards");
    private final static QName _GetIfpQuoteResponsePlanDetailBenefit_QNAME = new QName("", "GetIfpQuote.Response.PlanDetail.Benefit");
    private final static QName _SubmitFamilyResponse_QNAME = new QName("", "SubmitFamily.Response");
    private final static QName _GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan_QNAME = new QName("", "GetIfpQuote.Response.Quote.DroppedCarrier.DroppedPlan");
    private final static QName _SubmitGroupShoppingCartResponse_QNAME = new QName("", "SubmitGroupShoppingCart.Response");
    private final static QName _ArrayOfGetIfpQuoteResponseQuoteCarrier_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.Quote.Carrier");
    private final static QName _ArrayOfCustomProduct_QNAME = new QName("", "ArrayOfCustomProduct");
    private final static QName _ArrayOfGetGroupQuoteRequestPreferenceVersionFilter_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.Preference.VersionFilter");
    private final static QName _GetGroupQuoteResponsePlanDetailBenefitCoverage_QNAME = new QName("", "GetGroupQuote.Response.PlanDetail.Benefit.Coverage");
    private final static QName _ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint");
    private final static QName _GroupShoppingCartsBase_QNAME = new QName("", "GroupShoppingCartsBase");
    private final static QName _GetGroupQuoteRequestGroupRateKey_QNAME = new QName("", "GetGroupQuote.Request.GroupRateKey");
    private final static QName _LevelOfBenefitAddOns_QNAME = new QName("", "LevelOfBenefitAddOns");
    private final static QName _ArrayOfGetGroupQuoteRequestPreferenceDrugFilter_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.Preference.DrugFilter");
    private final static QName _GetIfpQuoteResponseCarrierDetail_QNAME = new QName("", "GetIfpQuote.Response.CarrierDetail");
    private final static QName _FetchCustomProductsRequestInput_QNAME = new QName("", "FetchCustomProducts.Request.Input");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan_QNAME = new QName("", "GetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier.DroppedPlan");
    private final static QName _FormularyLookupResponseDrugAttribute_QNAME = new QName("", "FormularyLookup.Response.DrugAttribute");
    private final static QName _RatingMethod_QNAME = new QName("", "RatingMethod");
    private final static QName _CustomProductPlanRawRate_QNAME = new QName("", "CustomProduct.Plan.RawRate");
    private final static QName _MemberType_QNAME = new QName("", "MemberType");
    private final static QName _GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice_QNAME = new QName("", "GetGroupQuote.Request.GroupRateFactor.Employee.ProductChoice");
    private final static QName _ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate");
    private final static QName _GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent_QNAME = new QName("", "GetIfpQuote.Request.Preference.BenchmarkRules.PlanComponent");
    private final static QName _RelationshipType_QNAME = new QName("", "RelationshipType");
    private final static QName _GetIfpQuoteResponseQuoteCarrierRate_QNAME = new QName("", "GetIfpQuote.Response.Quote.CarrierRate");
    private final static QName _ArrayOfGetPlansbyRxItem_QNAME = new QName("", "ArrayOfGetPlansbyRx.Item");
    private final static QName _LevelOfDetails_QNAME = new QName("", "LevelOfDetails");
    private final static QName _GetPlansbyRxResponseCarrierPlanBenefitCoverage_QNAME = new QName("", "GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage");
    private final static QName _FamilyLocationAddress_QNAME = new QName("", "Family.LocationAddress");
    private final static QName _GetCountiesRequestAccessKey_QNAME = new QName("", "GetCounties.Request.AccessKey");
    private final static QName _CustomProductPlanBusinessRuleAllowedRelation_QNAME = new QName("", "CustomProduct.Plan.BusinessRule.AllowedRelation");
    private final static QName _PaymentOption_QNAME = new QName("", "PaymentOption");
    private final static QName _InsuranceType_QNAME = new QName("", "InsuranceType");
    private final static QName _EmployeeRateBase_QNAME = new QName("", "EmployeeRateBase");
    private final static QName _GetCarrierLoadHistoryResponse_QNAME = new QName("", "GetCarrierLoadHistory.Response");
    private final static QName _PlanFilterBase_QNAME = new QName("", "PlanFilterBase");
    private final static QName _GetGroupQuoteResponseQuoteDroppedCarrier_QNAME = new QName("", "GetGroupQuote.Response.Quote.DroppedCarrier");
    private final static QName _FamilyContact_QNAME = new QName("", "Family.Contact");
    private final static QName _GetCarriersPlansBenefitsRequestDrugFilters_QNAME = new QName("", "GetCarriersPlansBenefits.Request.DrugFilters");
    private final static QName _GetCarriersPlansBenefitsRequestAccessKey_QNAME = new QName("", "GetCarriersPlansBenefits.Request.AccessKey");
    private final static QName _OutOfAreaAction_QNAME = new QName("", "OutOfAreaAction");
    private final static QName _AllowanceType_QNAME = new QName("", "AllowanceType");
    private final static QName _ServiceBase_QNAME = new QName("", "ServiceBase");
    private final static QName _ArrayOfError_QNAME = new QName("", "ArrayOfError");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier_QNAME = new QName("", "GetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier");
    private final static QName _Error_QNAME = new QName("", "Error");
    private final static QName _GetZipCodeInfoResponseZipCodeInfoCity_QNAME = new QName("", "GetZipCodeInfo.Response.ZipCodeInfo.City");
    private final static QName _ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug_QNAME = new QName("", "ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug");
    private final static QName _ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.Quote.CarrierRate.MemberRate");
    private final static QName _PlanComponentBase_QNAME = new QName("", "PlanComponentBase");
    private final static QName _ZipCodeInfoBase_QNAME = new QName("", "ZipCodeInfoBase");
    private final static QName _ArrayOfFamilyMember_QNAME = new QName("", "ArrayOfFamily.Member");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint");
    private final static QName _VersionFilterBase_QNAME = new QName("", "VersionFilterBase");
    private final static QName _GetGroupQuoteResponsePlanDetailBenefit_QNAME = new QName("", "GetGroupQuote.Response.PlanDetail.Benefit");
    private final static QName _GetCarriersPlansBenefitsResponseFormulary_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Formulary");
    private final static QName _ArrayOfFormularyLookupResponseCarrier_QNAME = new QName("", "ArrayOfFormularyLookup.Response.Carrier");
    private final static QName _GetGroupQuoteRequestGroupRateFactorEmployeeMember_QNAME = new QName("", "GetGroupQuote.Request.GroupRateFactor.Employee.Member");
    private final static QName _RxPeriod_QNAME = new QName("", "RxPeriod");
    private final static QName _GetPlansbyRxResponseCarrierPlanBenefit_QNAME = new QName("", "GetPlansbyRx.Response.Carrier.Plan.Benefit");
    private final static QName _ArrayOfGetCustomProductsQuoteResponseMemberRate_QNAME = new QName("", "ArrayOfGetCustomProductsQuote.Response.MemberRate");
    private final static QName _GetGroupQuoteRequestPreferenceChoiceFilter_QNAME = new QName("", "GetGroupQuote.Request.Preference.ChoiceFilter");
    private final static QName _ArrayOfGetPlansbyRxResponseCarrierPlanBenefit_QNAME = new QName("", "ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit");
    private final static QName _ArrayOfGetIfpQuoteRequestMember_QNAME = new QName("", "ArrayOfGetIfpQuote.Request.Member");
    private final static QName _GroupEmployeeMember_QNAME = new QName("", "Group.Employee.Member");
    private final static QName _ArrayOfGetIfpQuoteResponsePlanDetailBenefit_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.PlanDetail.Benefit");
    private final static QName _GetIfpQuoteResponseQuote_QNAME = new QName("", "GetIfpQuote.Response.Quote");
    private final static QName _ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan_QNAME = new QName("", "ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier.VersionedPlan");
    private final static QName _GetGroupRequest_QNAME = new QName("", "GetGroup.Request");
    private final static QName _Eligibility_QNAME = new QName("", "Eligibility");
    private final static QName _GetIfpQuoteResponseQuoteCarrierRatePlanRate_QNAME = new QName("", "GetIfpQuote.Response.Quote.CarrierRate.PlanRate");
    private final static QName _BenchmarkPlan_QNAME = new QName("", "BenchmarkPlan");
    private final static QName _GetGroupQuoteResponseQuote_QNAME = new QName("", "GetGroupQuote.Response.Quote");
    private final static QName _IfpShoppingCartsBase_QNAME = new QName("", "IfpShoppingCartsBase");
    private final static QName _ArrayOfGetGroupQuoteResponseQuoteCarrierRate_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.Quote.CarrierRate");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier_QNAME = new QName("", "GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier");
    private final static QName _ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.GroupRateFactor.RAFOverride");
    private final static QName _ArrayOfGetPlansbyRxAddOn_QNAME = new QName("", "ArrayOfGetPlansbyRx.AddOn");
    private final static QName _FormularyLookupRequestInput_QNAME = new QName("", "FormularyLookup.Request.Input");
    private final static QName _SubmitCustomProductsResponse_QNAME = new QName("", "SubmitCustomProducts.Response");
    private final static QName _GetGroupQuoteResponseMemberRate_QNAME = new QName("", "GetGroupQuote.Response.MemberRate");
    private final static QName _ViewPointBase_QNAME = new QName("", "ViewPointBase");
    private final static QName _ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee.Member");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug_QNAME = new QName("", "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug");
    private final static QName _ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange_QNAME = new QName("", "ArrayOfIfpShoppingCart.ProductChoice.PlanChoices.StatusChange");
    private final static QName _GetGroupQuoteResponseQuoteEmployeePlanCarrierRate_QNAME = new QName("", "GetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate");
    private final static QName _GetGroupQuoteResponsePlanDetail_QNAME = new QName("", "GetGroupQuote.Response.PlanDetail");
    private final static QName _FormularyLookupRequest_QNAME = new QName("", "FormularyLookup.Request");
    private final static QName _CountyBase_QNAME = new QName("", "CountyBase");
    private final static QName _ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier_QNAME = new QName("", "ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier");
    private final static QName _GroupQuoteFormat_QNAME = new QName("", "GroupQuoteFormat");
    private final static QName _FetchCustomProductsRequest_QNAME = new QName("", "FetchCustomProducts.Request");
    private final static QName _GetZipCodeInfoResponse_QNAME = new QName("", "GetZipCodeInfo.Response");
    private final static QName _ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier_QNAME = new QName("", "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier");
    private final static QName _GetPlansbyRxResponseCarrier_QNAME = new QName("", "GetPlansbyRx.Response.Carrier");
    private final static QName _GetIfpQuoteResponseQuoteCarrierRateModified_QNAME = new QName("", "GetIfpQuote.Response.Quote.Carrier.Rate.Modified");
    private final static QName _ArrayOfGroupRAFOverride_QNAME = new QName("", "ArrayOfGroup.RAFOverride");
    private final static QName _ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint");
    private final static QName _CustomProductRatingAreaZipRange_QNAME = new QName("", "CustomProduct.RatingArea.ZipRange");
    private final static QName _IfpQuoteType_QNAME = new QName("", "IfpQuoteType");
    private final static QName _GetGroupQuoteResponse_QNAME = new QName("", "GetGroupQuote.Response");
    private final static QName _ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint.Service");
    private final static QName _ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.Quote.DroppedCarrier");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan");
    private final static QName _EventOperation_QNAME = new QName("", "EventOperation");
    private final static QName _GetCustomProductsQuoteRequestPreference_QNAME = new QName("", "GetCustomProductsQuote.Request.Preference");
    private final static QName _GetIfpQuoteRequestProductChoicePlanChoices_QNAME = new QName("", "GetIfpQuote.Request.ProductChoice.PlanChoices");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierPlan_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Carrier.Plan");
    private final static QName _GetIfpQuoteRequestPreferenceChoiceFilter_QNAME = new QName("", "GetIfpQuote.Request.Preference.ChoiceFilter");
    private final static QName _CustomProductRatingAreaZipCounty_QNAME = new QName("", "CustomProduct.RatingArea.ZipCounty");
    private final static QName _RatingAreaBase_QNAME = new QName("", "RatingAreaBase");
    private final static QName _ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.Quote.DroppedCarrier");
    private final static QName _ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter_QNAME = new QName("", "ArrayOfGetIfpQuote.Request.Preference.BenefitFilter.AttributeFilter");
    private final static QName _RAFOverrideBase_QNAME = new QName("", "RAFOverrideBase");
    private final static QName _ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage");
    private final static QName _ArrayOfGetPlansbyRxResponseCarrierPlan_QNAME = new QName("", "ArrayOfGetPlansbyRx.Response.Carrier.Plan");
    private final static QName _ArrayOfGetGroupQuoteResponseItem_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.Item");
    private final static QName _GetCarriersPlansBenefitsResponse_QNAME = new QName("", "GetCarriersPlansBenefits.Response");
    private final static QName _DroppedPlanBase_QNAME = new QName("", "DroppedPlanBase");
    private final static QName _GetCarriersPlansBenefitsPlanVisibility_QNAME = new QName("", "GetCarriersPlansBenefits.PlanVisibility");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service");
    private final static QName _GroupLifeSelector_QNAME = new QName("", "GroupLifeSelector");
    private final static QName _ResponseBase_QNAME = new QName("", "ResponseBase");
    private final static QName _ItemBase_QNAME = new QName("", "ItemBase");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseItem_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Item");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.DrugList.Drug");
    private final static QName _FetchCustomProductsRequestInputProductFilter_QNAME = new QName("", "FetchCustomProducts.Request.Input.ProductFilter");
    private final static QName _GroupRatingAreaBase_QNAME = new QName("", "GroupRatingAreaBase");
    private final static QName _ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO_QNAME = new QName("", "ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO");
    private final static QName _Market_QNAME = new QName("", "Market");
    private final static QName _GetGroupQuoteRequestPreferencePlanFilter_QNAME = new QName("", "GetGroupQuote.Request.Preference.PlanFilter");
    private final static QName _ArrayOfGetIfpQuoteRequestPreferenceDrugFilter_QNAME = new QName("", "ArrayOfGetIfpQuote.Request.Preference.DrugFilter");
    private final static QName _FormularyLookupResponseCarrier_QNAME = new QName("", "FormularyLookup.Response.Carrier");
    private final static QName _ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.Preference.ChoiceFilter");
    private final static QName _GetCustomProductsQuoteRequestCustomRateFactorFamily_QNAME = new QName("", "GetCustomProductsQuote.Request.CustomRateFactor.Family");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormulary_QNAME = new QName("", "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary");
    private final static QName _ContactBase_QNAME = new QName("", "ContactBase");
    private final static QName _GetIfpQuoteRequestIfpRateFactor_QNAME = new QName("", "GetIfpQuote.Request.IfpRateFactor");
    private final static QName _GetCustomProductsQuoteRequest_QNAME = new QName("", "GetCustomProductsQuote.Request");
    private final static QName _GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint_QNAME = new QName("", "GetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint");
    private final static QName _GroupRAFOverride_QNAME = new QName("", "Group.RAFOverride");
    private final static QName _ArrayOfGetGroupQuoteResponsePlanDetailBenefit_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.PlanDetail.Benefit");
    private final static QName _LinkType_QNAME = new QName("", "LinkType");
    private final static QName _ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee.ProductChoice");
    private final static QName _GetGroupQuoteRequestGroupRateFactorRAFOverride_QNAME = new QName("", "GetGroupQuote.Request.GroupRateFactor.RAFOverride");
    private final static QName _FormularyLookupResponseCarrierPlanPlanFormulary_QNAME = new QName("", "FormularyLookup.Response.Carrier.Plan.PlanFormulary");
    private final static QName _IfpRateBase_QNAME = new QName("", "IfpRateBase");
    private final static QName _LocationBase_QNAME = new QName("", "LocationBase");
    private final static QName _GroupShoppingCartProductChoice_QNAME = new QName("", "GroupShoppingCart.ProductChoice");
    private final static QName _GetGroupQuoteResponseQuoteCarrierRate_QNAME = new QName("", "GetGroupQuote.Response.Quote.CarrierRate");
    private final static QName _MemberRateBase_QNAME = new QName("", "MemberRateBase");
    private final static QName _ValidationType_QNAME = new QName("", "ValidationType");
    private final static QName _GetFamilyResponse_QNAME = new QName("", "GetFamily.Response");
    private final static QName _FamilyContactContactAttribute_QNAME = new QName("", "Family.Contact.ContactAttribute");
    private final static QName _GetZipCodeInfoRequestInput_QNAME = new QName("", "GetZipCodeInfo.Request.Input");
    private final static QName _ListRxRequestInput_QNAME = new QName("", "ListRx.Request.Input");
    private final static QName _GetCustomProductsQuoteRequestPreferenceProductFilter_QNAME = new QName("", "GetCustomProductsQuote.Request.Preference.ProductFilter");
    private final static QName _GetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService_QNAME = new QName("", "GetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint.Service");
    private final static QName _ArrayOfGroupEmployee_QNAME = new QName("", "ArrayOfGroup.Employee");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit");
    private final static QName _ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.Quote.DroppedCarrier.DroppedPlan");
    private final static QName _GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService_QNAME = new QName("", "GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service");
    private final static QName _ArrayOfFormularyLookupRequestPlanFilter_QNAME = new QName("", "ArrayOfFormularyLookup.Request.PlanFilter");
    private final static QName _ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier_QNAME = new QName("", "ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier");
    private final static QName _GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate_QNAME = new QName("", "GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate");
    private final static QName _ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily_QNAME = new QName("", "ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family");
    private final static QName _GetZipCodeInfoRequestAccessKey_QNAME = new QName("", "GetZipCodeInfo.Request.AccessKey");
    private final static QName _GetGroupQuoteResponseCarrierDetail_QNAME = new QName("", "GetGroupQuote.Response.CarrierDetail");
    private final static QName _ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint_QNAME = new QName("", "ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint");
    private final static QName _GetGroupQuoteAddOn_QNAME = new QName("", "GetGroupQuote.AddOn");
    private final static QName _ArrayOfCustomProductRatingArea_QNAME = new QName("", "ArrayOfCustomProduct.RatingArea");
    private final static QName _ArrayOfInsuranceType_QNAME = new QName("", "ArrayOfInsuranceType");
    private final static QName _GetCarriersPlansBenefitsResponseFormularyDrugTierTierType_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Formulary.DrugTier.TierType");
    private final static QName _FetchCustomProductsResponse_QNAME = new QName("", "FetchCustomProducts.Response");
    private final static QName _IfpShoppingCart_QNAME = new QName("", "IfpShoppingCart");
    private final static QName _ArrayOfGetGroupQuoteRequestGroupRateFactorScenario_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.GroupRateFactor.Scenario");
    private final static QName _FormularyLookupFilterType_QNAME = new QName("", "FormularyLookup.FilterType");
    private final static QName _FlagValue_QNAME = new QName("", "FlagValue");
    private final static QName _GetCarriersPlansBenefitsResponseContactContactPoint_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Contact.ContactPoint");
    private final static QName _ListRxResponse_QNAME = new QName("", "ListRx.Response");
    private final static QName _IfpPlanEligibility_QNAME = new QName("", "IfpPlanEligibility");
    private final static QName _GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan_QNAME = new QName("", "GetCarrierLoadHistory.Response.LoadEvent.Carrier.VersionedPlan");
    private final static QName _DroppedCarrierBase_QNAME = new QName("", "DroppedCarrierBase");
    private final static QName _GetCustomProductsQuoteResponseQuote_QNAME = new QName("", "GetCustomProductsQuote.Response.Quote");
    private final static QName _ArrayOfCustomProductPlanRawRate_QNAME = new QName("", "ArrayOfCustomProduct.Plan.RawRate");
    private final static QName _LocationAddressBase_QNAME = new QName("", "LocationAddressBase");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO_QNAME = new QName("", "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO");
    private final static QName _ArrayOfGroupShoppingCartProductChoice_QNAME = new QName("", "ArrayOfGroupShoppingCart.ProductChoice");
    private final static QName _GetIfpQuoteRequestPreferenceDrugFilter_QNAME = new QName("", "GetIfpQuote.Request.Preference.DrugFilter");
    private final static QName _FormularyLookupResponse_QNAME = new QName("", "FormularyLookup.Response");
    private final static QName _ArrayOfCustomProductRatingAreaZipCounty_QNAME = new QName("", "ArrayOfCustomProduct.RatingArea.ZipCounty");
    private final static QName _ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService_QNAME = new QName("", "ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service");
    private final static QName _PlanDetailsBase_QNAME = new QName("", "PlanDetailsBase");
    private final static QName _GetIfpQuoteAddOn_QNAME = new QName("", "GetIfpQuote.AddOn");
    private final static QName _GetCarriersPlansBenefitsLevelOfDetail_QNAME = new QName("", "GetCarriersPlansBenefits.LevelOfDetail");
    private final static QName _RateBase_QNAME = new QName("", "RateBase");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseFormulary_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Formulary");
    private final static QName _ChoiceFilterBase_QNAME = new QName("", "ChoiceFilterBase");
    private final static QName _SubmitCustomProductsRequest_QNAME = new QName("", "SubmitCustomProducts.Request");
    private final static QName _ArrayOfListRxResponseDrug_QNAME = new QName("", "ArrayOfListRx.Response.Drug");
    private final static QName _GroupContact_QNAME = new QName("", "Group.Contact");
    private final static QName _GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter_QNAME = new QName("", "GetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter");
    private final static QName _ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier_QNAME = new QName("", "ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier");
    private final static QName _ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember_QNAME = new QName("", "ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family.Member");
    private final static QName _SubmitCustomProductsRequestSetting_QNAME = new QName("", "SubmitCustomProducts.Request.Setting");
    private final static QName _GetGroupQuoteResponseQuoteEmployeePlan_QNAME = new QName("", "GetGroupQuote.Response.Quote.EmployeePlan");
    private final static QName _ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.Quote.CarrierRate.PlanRate");
    private final static QName _CustomProductRatingAreaCounty_QNAME = new QName("", "CustomProduct.RatingArea.County");
    private final static QName _GetGroupQuoteRequestGroupRateFactorScenario_QNAME = new QName("", "GetGroupQuote.Request.GroupRateFactor.Scenario");
    private final static QName _AttributeBase_QNAME = new QName("", "AttributeBase");
    private final static QName _IfpShoppingCartProductChoicePlanChoices_QNAME = new QName("", "IfpShoppingCart.ProductChoice.PlanChoices");
    private final static QName _ArrayOfGetGroupQuoteRequestPreferencePlanFilter_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.Preference.PlanFilter");
    private final static QName _Family_QNAME = new QName("", "Family");
    private final static QName _SubmitCustomProductsRequestAccessKey_QNAME = new QName("", "SubmitCustomProducts.Request.AccessKey");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint");
    private final static QName _ContractStatus_QNAME = new QName("", "ContractStatus");
    private final static QName _ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty_QNAME = new QName("", "ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.County");
    private final static QName _ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote_QNAME = new QName("", "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote");
    private final static QName _ArrayOfGetIfpQuoteRequestPreferencePlanFilter_QNAME = new QName("", "ArrayOfGetIfpQuote.Request.Preference.PlanFilter");
    private final static QName _GetPlansbyRxAddOn_QNAME = new QName("", "GetPlansbyRx.AddOn");
    private final static QName _ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan_QNAME = new QName("", "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier.DroppedPlan");
    private final static QName _BenefitBase_QNAME = new QName("", "BenefitBase");
    private final static QName _GetCarriersPlansBenefitsResponseFormularyDrugTier_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Formulary.DrugTier");
    private final static QName _GetIfpShoppingCartsResponse_QNAME = new QName("", "GetIfpShoppingCarts.Response");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuote_QNAME = new QName("", "GetCustomProductsQuote.Response.Quote.FamilyQuote");
    private final static QName _ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier");
    private final static QName _CustomProductPlan_QNAME = new QName("", "CustomProduct.Plan");
    private final static QName _ArrayOfGetCarriersPlansBenefitsAddOn_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.AddOn");
    private final static QName _GetIfpQuoteResponsePlanDetail_QNAME = new QName("", "GetIfpQuote.Response.PlanDetail");
    private final static QName _ArrayOfGetGroupQuoteResponseQuoteEmployeePlan_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.Quote.EmployeePlan");
    private final static QName _CustomProductPlanBusinessRule_QNAME = new QName("", "CustomProduct.Plan.BusinessRule");
    private final static QName _ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Request.CarrierPlanFilter");
    private final static QName _ArrayOfGetIfpQuoteResponseQuoteCarrierRate_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.Quote.CarrierRate");
    private final static QName _ArrayOfCustomProductPlan_QNAME = new QName("", "ArrayOfCustomProduct.Plan");
    private final static QName _GetCustomProductsQuoteRequestCustomRateFactorFamilyMember_QNAME = new QName("", "GetCustomProductsQuote.Request.CustomRateFactor.Family.Member");
    private final static QName _ArrayOfCustomProductRatingAreaCounty_QNAME = new QName("", "ArrayOfCustomProduct.RatingArea.County");
    private final static QName _ArrayOfGetCountiesResponseCounty_QNAME = new QName("", "ArrayOfGetCounties.Response.County");
    private final static QName _Gender_QNAME = new QName("", "Gender");
    private final static QName _BestTimeToCall_QNAME = new QName("", "BestTimeToCall");
    private final static QName _GetCustomProductsQuoteResponse_QNAME = new QName("", "GetCustomProductsQuote.Response");
    private final static QName _SubmissionStatus_QNAME = new QName("", "SubmissionStatus");
    private final static QName _ListRxResponseDrug_QNAME = new QName("", "ListRx.Response.Drug");
    private final static QName _SubmitIfpShoppingCartResponse_QNAME = new QName("", "SubmitIfpShoppingCart.Response");
    private final static QName _GetIfpQuoteRequestPreferencePlanFilter_QNAME = new QName("", "GetIfpQuote.Request.Preference.PlanFilter");
    private final static QName _FormularyLookupResponseCarrierPlan_QNAME = new QName("", "FormularyLookup.Response.Carrier.Plan");
    private final static QName _GetCarriersPlansBenefitsResponseCarrier_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Carrier");
    private final static QName _GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices_QNAME = new QName("", "GetGroupQuote.Request.GroupRateFactor.Employee.ProductChoice.PlanChoices");
    private final static QName _GetCountiesResponseCounty_QNAME = new QName("", "GetCounties.Response.County");
    private final static QName _GetCarrierLoadHistoryRequestInput_QNAME = new QName("", "GetCarrierLoadHistory.Request.Input");
    private final static QName _ContactPointBase_QNAME = new QName("", "ContactPointBase");
    private final static QName _GetGroupQuoteResponseItem_QNAME = new QName("", "GetGroupQuote.Response.Item");
    private final static QName _GetPlansbyRxRequestAccessKey_QNAME = new QName("", "GetPlansbyRx.Request.AccessKey");
    private final static QName _IfpShoppingCartProductChoice_QNAME = new QName("", "IfpShoppingCart.ProductChoice");
    private final static QName _PlanSortDir_QNAME = new QName("", "PlanSortDir");
    private final static QName _GetIfpQuoteRequest_QNAME = new QName("", "GetIfpQuote.Request");
    private final static QName _GetIfpQuoteResponseQuoteDroppedCarrier_QNAME = new QName("", "GetIfpQuote.Response.Quote.DroppedCarrier");
    private final static QName _ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage_QNAME = new QName("", "ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage");
    private final static QName _GetCustomProductsQuoteRequestCustomRateFactor_QNAME = new QName("", "GetCustomProductsQuote.Request.CustomRateFactor");
    private final static QName _ArrayOfGetIfpQuoteRequestPreferenceVersionFilter_QNAME = new QName("", "ArrayOfGetIfpQuote.Request.Preference.VersionFilter");
    private final static QName _CommandFault_QNAME = new QName("", "CommandFault");
    private final static QName _RxCUIsBase_QNAME = new QName("", "RxCUIsBase");
    private final static QName _GetZipCodeInfoResponseZipCodeInfoCounty_QNAME = new QName("", "GetZipCodeInfo.Response.ZipCodeInfo.County");
    private final static QName _GetCountiesRequestInput_QNAME = new QName("", "GetCounties.Request.Input");
    private final static QName _InAreaAction_QNAME = new QName("", "InAreaAction");
    private final static QName _ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate_QNAME = new QName("", "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate");
    private final static QName _EmployerContributionTowards_QNAME = new QName("", "EmployerContributionTowards");
    private final static QName _RateSet_QNAME = new QName("", "RateSet");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance");
    private final static QName _GetGroupQuoteRequestPreferenceVersionFilter_QNAME = new QName("", "GetGroupQuote.Request.Preference.VersionFilter");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseDrugList_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.DrugList");
    private final static QName _SubmitGroupResponse_QNAME = new QName("", "SubmitGroup.Response");
    private final static QName _GetCarriersPlansBenefitsRequestBenefitFilter_QNAME = new QName("", "GetCarriersPlansBenefits.Request.BenefitFilter");
    private final static QName _GetGroupQuoteRequest_QNAME = new QName("", "GetGroupQuote.Request");
    private final static QName _GetCarrierLoadHistoryRequestAccessKey_QNAME = new QName("", "GetCarrierLoadHistory.Request.AccessKey");
    private final static QName _GetIfpQuoteResponseQuoteCarrierRateMemberRate_QNAME = new QName("", "GetIfpQuote.Response.Quote.CarrierRate.MemberRate");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service");
    private final static QName _GetGroupQuoteRequestPreference_QNAME = new QName("", "GetGroupQuote.Request.Preference");
    private final static QName _ListRxRequestPlanFilter_QNAME = new QName("", "ListRx.Request.PlanFilter");
    private final static QName _GetZipCodeInfoResponseZipCodeInfo_QNAME = new QName("", "GetZipCodeInfo.Response.ZipCodeInfo");
    private final static QName _ArrayOfFamilyContactContactAttribute_QNAME = new QName("", "ArrayOfFamily.Contact.ContactAttribute");
    private final static QName _CustomProductRatingArea_QNAME = new QName("", "CustomProduct.RatingArea");
    private final static QName _IfpPlanChoiceBase_QNAME = new QName("", "IfpPlanChoiceBase");
    private final static QName _GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint_QNAME = new QName("", "GetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint");
    private final static QName _GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint_QNAME = new QName("", "GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint");
    private final static QName _PlanBase_QNAME = new QName("", "PlanBase");
    private final static QName _ArrayOfGetZipCodeInfoResponseZipCodeInfoCity_QNAME = new QName("", "ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.City");
    private final static QName _Group_QNAME = new QName("", "Group");
    private final static QName _PlanRateBase_QNAME = new QName("", "PlanRateBase");
    private final static QName _IfpQuoteFormat_QNAME = new QName("", "IfpQuoteFormat");
    private final static QName _GetIfpShoppingCartsRequest_QNAME = new QName("", "GetIfpShoppingCarts.Request");
    private final static QName _SubmitFamilyRequest_QNAME = new QName("", "SubmitFamily.Request");
    private final static QName _ArrayOfGetZipCodeInfoResponseZipCodeInfo_QNAME = new QName("", "ArrayOfGetZipCodeInfo.Response.ZipCodeInfo");
    private final static QName _CostSharingType_QNAME = new QName("", "CostSharingType");
    private final static QName _FeaturedPlan_QNAME = new QName("", "FeaturedPlan");
    private final static QName _ArrayOfCustomProductRatingAreaZipRange_QNAME = new QName("", "ArrayOfCustomProduct.RatingArea.ZipRange");
    private final static QName _GetPlansbyRxItem_QNAME = new QName("", "GetPlansbyRx.Item");
    private final static QName _GetCarriersPlansBenefitsAddOn_QNAME = new QName("", "GetCarriersPlansBenefits.AddOn");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierPlanBenefit_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit");
    private final static QName _GetCarriersPlansBenefitsRequest_QNAME = new QName("", "GetCarriersPlansBenefits.Request");
    private final static QName _ArrayOfFormularyLookupResponseCarrierPlan_QNAME = new QName("", "ArrayOfFormularyLookup.Response.Carrier.Plan");
    private final static QName _GroupEmployee_QNAME = new QName("", "Group.Employee");
    private final static QName _MemberBase_QNAME = new QName("", "MemberBase");
    private final static QName _ArrayOfFetchCustomProductsRequestInputProductFilter_QNAME = new QName("", "ArrayOfFetchCustomProducts.Request.Input.ProductFilter");
    private final static QName _GetCarriersPlansBenefitsResponseLocation_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Location");
    private final static QName _GetIfpQuoteRequestProductChoice_QNAME = new QName("", "GetIfpQuote.Request.ProductChoice");
    private final static QName _GetZipCodeInfoRequestInputIsQuotableFilters_QNAME = new QName("", "GetZipCodeInfo.Request.Input.IsQuotableFilters");
    private final static QName _ArrayOfGetIfpQuoteResponseItem_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.Item");
    private final static QName _SubmitIfpShoppingCartRequest_QNAME = new QName("", "SubmitIfpShoppingCart.Request");
    private final static QName _ArrayOfGetIfpQuoteResponseQuoteMemberPlan_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.Quote.MemberPlan");
    private final static QName _ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter_QNAME = new QName("", "ArrayOfGetCustomProductsQuote.Request.Preference.ProductFilter");
    private final static QName _InsuranceCategory_QNAME = new QName("", "InsuranceCategory");
    private final static QName _GetCarrierLoadHistoryRequest_QNAME = new QName("", "GetCarrierLoadHistory.Request");
    private final static QName _GroupPlanEligibility_QNAME = new QName("", "GroupPlanEligibility");
    private final static QName _GetCarriersPlansBenefitsResponseDrugListDrug_QNAME = new QName("", "GetCarriersPlansBenefits.Response.DrugList.Drug");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierPlanPlanLink_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink");
    private final static QName _GetGroupResponse_QNAME = new QName("", "GetGroup.Response");
    private final static QName _FamilyMember_QNAME = new QName("", "Family.Member");
    private final static QName _CoverageBase_QNAME = new QName("", "CoverageBase");
    private final static QName _GetCarriersPlansBenefitsResponseItem_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Item");
    private final static QName _GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan_QNAME = new QName("", "GetGroupQuote.Response.Quote.DroppedCarrier.DroppedPlan");
    private final static QName _ArrayOfCustomProductPlanBusinessRuleAllowedRelation_QNAME = new QName("", "ArrayOfCustomProduct.Plan.BusinessRule.AllowedRelation");
    private final static QName _ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug_QNAME = new QName("", "ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug");
    private final static QName _CustomProduct_QNAME = new QName("", "CustomProduct");
    private final static QName _ArrayOfCustomProductPlanLookupSequence_QNAME = new QName("", "ArrayOfCustomProduct.Plan.LookupSequence");
    private final static QName _SubmitGroupShoppingCartRequest_QNAME = new QName("", "SubmitGroupShoppingCart.Request");
    private final static QName _RatesGender_QNAME = new QName("", "RatesGender");
    private final static QName _ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter_QNAME = new QName("", "ArrayOfGetIfpQuote.Request.Preference.BenefitFilter");
    private final static QName _GetGroupQuoteResponseQuoteEmployeePlanCarrier_QNAME = new QName("", "GetGroupQuote.Response.Quote.EmployeePlan.Carrier");
    private final static QName _GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter_QNAME = new QName("", "GetCarriersPlansBenefits.Request.BenefitFilter.AttributeFilter");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage");
    private final static QName _ListRxRequestAccessKey_QNAME = new QName("", "ListRx.Request.AccessKey");
    private final static QName _GetPlansbyRxResponse_QNAME = new QName("", "GetPlansbyRx.Response");
    private final static QName _CarrierDetailsBase_QNAME = new QName("", "CarrierDetailsBase");
    private final static QName _GetGroupQuoteRequestPreferenceBenefitFilter_QNAME = new QName("", "GetGroupQuote.Request.Preference.BenefitFilter");
    private final static QName _ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified_QNAME = new QName("", "ArrayOfGetIfpQuote.Response.Quote.Carrier.Rate.Modified");
    private final static QName _ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.Preference.BenefitFilter");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier");
    private final static QName _GetIfpQuoteResponseQuoteCarrier_QNAME = new QName("", "GetIfpQuote.Response.Quote.Carrier");
    private final static QName _CustomProductPlanLookupSequence_QNAME = new QName("", "CustomProduct.Plan.LookupSequence");
    private final static QName _ArrayOfGroupContactContactAttribute_QNAME = new QName("", "ArrayOfGroup.Contact.ContactAttribute");
    private final static QName _CoverageTier_QNAME = new QName("", "CoverageTier");
    private final static QName _CustomProductPlanBusinessRuleCappingRule_QNAME = new QName("", "CustomProduct.Plan.BusinessRule.CappingRule");
    private final static QName _IfpShoppingCartProductChoicePlanChoicesStatusChange_QNAME = new QName("", "IfpShoppingCart.ProductChoice.PlanChoices.StatusChange");
    private final static QName _GetIfpQuoteRequestIfpRateKey_QNAME = new QName("", "GetIfpQuote.Request.IfpRateKey");
    private final static QName _ArrayOfGetGroupQuoteAddOn_QNAME = new QName("", "ArrayOfGetGroupQuote.AddOn");
    private final static QName _GetFamilyRequest_QNAME = new QName("", "GetFamily.Request");
    private final static QName _AgeSort_QNAME = new QName("", "AgeSort");
    private final static QName _ContributionType_QNAME = new QName("", "ContributionType");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier_QNAME = new QName("", "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier");
    private final static QName _GetIfpQuoteRequestPlanVisibility_QNAME = new QName("", "GetIfpQuote.Request.PlanVisibility");
    private final static QName _ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage");
    private final static QName _AccessKeyBase_QNAME = new QName("", "AccessKeyBase");
    private final static QName _GetCarrierLoadHistoryResponseLoadEventCarrier_QNAME = new QName("", "GetCarrierLoadHistory.Response.LoadEvent.Carrier");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Contact.ContactPoint");
    private final static QName _GetPlansbyRxResponseCarrierPlan_QNAME = new QName("", "GetPlansbyRx.Response.Carrier.Plan");
    private final static QName _ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan_QNAME = new QName("", "ArrayOfGetGroupQuote.Response.Quote.DroppedCarrier.DroppedPlan");
    private final static QName _ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter.AttributeFilter");
    private final static QName _ArrayOfListRxRequestPlanFilter_QNAME = new QName("", "ArrayOfListRx.Request.PlanFilter");
    private final static QName _AgeCalculationBase_QNAME = new QName("", "AgeCalculationBase");
    private final static QName _ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter_QNAME = new QName("", "ArrayOfGetIfpQuote.Request.Preference.ChoiceFilter");
    private final static QName _IfpRatingAreaBase_QNAME = new QName("", "IfpRatingAreaBase");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseLocation_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Location");
    private final static QName _CoverageType_QNAME = new QName("", "CoverageType");
    private final static QName _GetCountiesResponse_QNAME = new QName("", "GetCounties.Response");
    private final static QName _GetIfpQuoteRequestPreferenceBenefitFilter_QNAME = new QName("", "GetIfpQuote.Request.Preference.BenefitFilter");
    private final static QName _GetZipCodeInfoRequest_QNAME = new QName("", "GetZipCodeInfo.Request");
    private final static QName _ArrayOfGetCarriersPlansBenefitsResponseCarrier_QNAME = new QName("", "ArrayOfGetCarriersPlansBenefits.Response.Carrier");
    private final static QName _GetIfpQuoteRequestPreferenceVersionFilter_QNAME = new QName("", "GetIfpQuote.Request.Preference.VersionFilter");
    private final static QName _ArrayOfGetIfpQuoteAddOn_QNAME = new QName("", "ArrayOfGetIfpQuote.AddOn");
    private final static QName _RatingAreaType_QNAME = new QName("", "RatingAreaType");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage_QNAME = new QName("", "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage");
    private final static QName _GetCarriersPlansBenefitsRequestCarrierPlanFilter_QNAME = new QName("", "GetCarriersPlansBenefits.Request.CarrierPlanFilter");
    private final static QName _FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO_QNAME = new QName("", "FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO");
    private final static QName _FormularyLookupRequestDrug_QNAME = new QName("", "FormularyLookup.Request.Drug");
    private final static QName _ArrayOfGetCarrierLoadHistoryResponseLoadEvent_QNAME = new QName("", "ArrayOfGetCarrierLoadHistory.Response.LoadEvent");
    private final static QName _ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent_QNAME = new QName("", "ArrayOfGetIfpQuote.Request.Preference.BenchmarkRules.PlanComponent");
    private final static QName _PlanOffering_QNAME = new QName("", "PlanOffering");
    private final static QName _CarrierBase_QNAME = new QName("", "CarrierBase");
    private final static QName _GetIfpQuoteResponseQuoteMemberPlan_QNAME = new QName("", "GetIfpQuote.Response.Quote.MemberPlan");
    private final static QName _GetCarrierLoadHistoryResponseLoadEvent_QNAME = new QName("", "GetCarrierLoadHistory.Response.LoadEvent");
    private final static QName _GetCarriersPlansBenefitsRequestVersionFilter_QNAME = new QName("", "GetCarriersPlansBenefits.Request.VersionFilter");
    private final static QName _GroupShoppingCart_QNAME = new QName("", "GroupShoppingCart");
    private final static QName _SubmissionType_QNAME = new QName("", "SubmissionType");
    private final static QName _ArrayOfIfpShoppingCartProductChoice_QNAME = new QName("", "ArrayOfIfpShoppingCart.ProductChoice");
    private final static QName _GetPlansbyRxRequestInput_QNAME = new QName("", "GetPlansbyRx.Request.Input");
    private final static QName _SubmitGroupRequest_QNAME = new QName("", "SubmitGroup.Request");
    private final static QName _ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee");
    private final static QName _ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter_QNAME = new QName("", "ArrayOfGetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter");
    private final static QName _FormularyLookupRequestAccessKey_QNAME = new QName("", "FormularyLookup.Request.AccessKey");
    private final static QName _ItemBaseValue_QNAME = new QName("", "Value");
    private final static QName _ItemBaseAttribute_QNAME = new QName("", "Attribute");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierSubmissionId_QNAME = new QName("", "SubmissionId");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRates_QNAME = new QName("", "PlanRates");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierCompanyName_QNAME = new QName("", "CompanyName");
    private final static QName _GetCarrierLoadHistoryResponseLoadEvents_QNAME = new QName("", "LoadEvents");
    private final static QName _GetGroupQuoteRequestPreferenceBenefitFilterViewPointType_QNAME = new QName("", "ViewPointType");
    private final static QName _GetGroupQuoteRequestPreferenceBenefitFilterBenefitEnum_QNAME = new QName("", "BenefitEnum");
    private final static QName _GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilters_QNAME = new QName("", "AttributeFilters");
    private final static QName _GetGroupQuoteRequestPreferenceBenefitFilterServiceType_QNAME = new QName("", "ServiceType");
    private final static QName _MemberBaseChangeCodes_QNAME = new QName("", "ChangeCodes");
    private final static QName _MemberBaseMembershipDate_QNAME = new QName("", "MembershipDate");
    private final static QName _MemberBaseDateLastSmoked_QNAME = new QName("", "DateLastSmoked");
    private final static QName _MemberBasePartnerData_QNAME = new QName("", "PartnerData");
    private final static QName _MemberBaseIsSmoker_QNAME = new QName("", "IsSmoker");
    private final static QName _MemberBaseFirstName_QNAME = new QName("", "FirstName");
    private final static QName _MemberBaseCountyName_QNAME = new QName("", "CountyName");
    private final static QName _MemberBaseLastName_QNAME = new QName("", "LastName");
    private final static QName _MemberBaseZipCode_QNAME = new QName("", "ZipCode");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVODrugPrescriptionPeriodType_QNAME = new QName("", "DrugPrescriptionPeriodType");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCoInsurance_QNAME = new QName("", "CoInsurance");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCoPayment_QNAME = new QName("", "CoPayment");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVONetworkCostType_QNAME = new QName("", "NetworkCostType");
    private final static QName _SubmitGroupResponseShoppingCarts_QNAME = new QName("", "ShoppingCarts");
    private final static QName _ListRxResponseDrugDrugName_QNAME = new QName("", "DrugName");
    private final static QName _GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME = new QName("", "EffectiveDate");
    private final static QName _GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoice_QNAME = new QName("", "PlanChoice");
    private final static QName _GetGroupQuoteResponseQuoteCarriers_QNAME = new QName("", "Carriers");
    private final static QName _GetGroupQuoteResponseQuoteDroppedRates_QNAME = new QName("", "DroppedRates");
    private final static QName _GetGroupQuoteResponseQuoteEmployeePlans_QNAME = new QName("", "EmployeePlans");
    private final static QName _GetGroupQuoteResponsePlanDetailBenefitBenefitData_QNAME = new QName("", "BenefitData");
    private final static QName _GetGroupQuoteResponsePlanDetailBenefitCoverages_QNAME = new QName("", "Coverages");
    private final static QName _ErrorHint_QNAME = new QName("", "Hint");
    private final static QName _GetCustomProductsQuoteRequestCustomRateFactorFamilyWorksiteCountyFIPS_QNAME = new QName("", "WorksiteCountyFIPS");
    private final static QName _FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugRxCUI_QNAME = new QName("", "RxCUI");
    private final static QName _GetZipCodeInfoRequestInputIsQuotableFiltersInsuranceTypes_QNAME = new QName("", "InsuranceTypes");
    private final static QName _GetZipCodeInfoRequestInputIsQuotableFiltersPlanIds_QNAME = new QName("", "PlanIds");
    private final static QName _GetZipCodeInfoRequestInputIsQuotableFiltersCarrierIds_QNAME = new QName("", "CarrierIds");
    private final static QName _RateBaseRatingAnnotation_QNAME = new QName("", "RatingAnnotation");
    private final static QName _GetGroupQuoteRequestPreferenceBenefitTemplate_QNAME = new QName("", "BenefitTemplate");
    private final static QName _GetGroupQuoteRequestPreferenceAsOfDate_QNAME = new QName("", "AsOfDate");
    private final static QName _GetGroupQuoteRequestPreferenceChoiceFilters_QNAME = new QName("", "ChoiceFilters");
    private final static QName _GetGroupQuoteRequestPreferenceAddOns_QNAME = new QName("", "AddOns");
    private final static QName _GetGroupQuoteRequestPreferenceDrugFilters_QNAME = new QName("", "DrugFilters");
    private final static QName _GetGroupQuoteRequestPreferencePlanFilters_QNAME = new QName("", "PlanFilters");
    private final static QName _GetGroupQuoteRequestPreferenceQuoteState_QNAME = new QName("", "QuoteState");
    private final static QName _GetGroupQuoteRequestPreferenceBenefitFilters_QNAME = new QName("", "BenefitFilters");
    private final static QName _GetGroupQuoteRequestPreferenceVersionFilters_QNAME = new QName("", "VersionFilters");
    private final static QName _LocationAddressBaseState_QNAME = new QName("", "State");
    private final static QName _LocationAddressBaseAddress_QNAME = new QName("", "Address");
    private final static QName _LocationAddressBaseCity_QNAME = new QName("", "City");
    private final static QName _GetGroupQuoteResponseQuoteEmployeePlanCarrierRates_QNAME = new QName("", "Rates");
    private final static QName _SubmitIfpShoppingCartResponseShoppingCart_QNAME = new QName("", "ShoppingCart");
    private final static QName _FetchCustomProductsRequestInputProductFilterProductType_QNAME = new QName("", "ProductType");
    private final static QName _FetchCustomProductsRequestInputProductFilterCarrierId_QNAME = new QName("", "CarrierId");
    private final static QName _ContactPointBaseValue2_QNAME = new QName("", "Value2");
    private final static QName _GetGroupQuoteResponseQuoteEmployeePlanCarrierRateMemberRates_QNAME = new QName("", "MemberRates");
    private final static QName _GetGroupQuoteResponseQuoteEmployeePlanCarrierRateCoveredRxCUIs_QNAME = new QName("", "CoveredRxCUIs");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoints_QNAME = new QName("", "ViewPoints");
    private final static QName _GetGroupQuoteResponseGroupQuote_QNAME = new QName("", "GroupQuote");
    private final static QName _FormularyLookupResponseDrugAttributes_QNAME = new QName("", "DrugAttributes");
    private final static QName _QuoteRequestBaseBrokerId_QNAME = new QName("", "BrokerId");
    private final static QName _GroupShoppingCartProductChoiceEmployeeId_QNAME = new QName("", "EmployeeId");
    private final static QName _GetGroupQuoteRequestGroupRateFactorScenarioNonSmokerRateOnly_QNAME = new QName("", "NonSmokerRateOnly");
    private final static QName _GetGroupQuoteRequestGroupRateFactorScenarioDepContributionType_QNAME = new QName("", "DepContributionType");
    private final static QName _GetGroupQuoteRequestGroupRateFactorScenarioSponsoredPlanId_QNAME = new QName("", "SponsoredPlanId");
    private final static QName _GetGroupQuoteRequestGroupRateFactorScenarioNonSmokerDepRateOnly_QNAME = new QName("", "NonSmokerDepRateOnly");
    private final static QName _GetGroupQuoteRequestGroupRateFactorScenarioNote_QNAME = new QName("", "Note");
    private final static QName _GetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel_QNAME = new QName("", "BenefitLevel");
    private final static QName _GetGroupQuoteRequestGroupRateFactorScenarioPlanType_QNAME = new QName("", "PlanType");
    private final static QName _GetGroupQuoteRequestGroupRateFactorScenarioSponsoredCarrierId_QNAME = new QName("", "SponsoredCarrierId");
    private final static QName _GetGroupQuoteRequestGroupRateFactorScenarioEHBRateOnly_QNAME = new QName("", "EHBRateOnly");
    private final static QName _GetCarriersPlansBenefitsResponseContactContactPoints_QNAME = new QName("", "ContactPoints");
    private final static QName _GetCarriersPlansBenefitsResponseContactTitle_QNAME = new QName("", "Title");
    private final static QName _GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointServices_QNAME = new QName("", "Services");
    private final static QName _CustomProductRatingAreaZipCountyCountyFIPS_QNAME = new QName("", "CountyFIPS");
    private final static QName _FormularyLookupResponseCarrierPlans_QNAME = new QName("", "Plans");
    private final static QName _FormularyLookupResponseCarrierIssuerId_QNAME = new QName("", "IssuerId");
    private final static QName _GetGroupRequestLeadId_QNAME = new QName("", "LeadId");
    private final static QName _GetGroupRequestCartId_QNAME = new QName("", "CartId");
    private final static QName _GetGroupRequestContactId_QNAME = new QName("", "ContactId");
    private final static QName _GetGroupRequestGroupId_QNAME = new QName("", "GroupId");
    private final static QName _GetIfpQuoteResponsePlanDetailPlanData_QNAME = new QName("", "PlanData");
    private final static QName _GetIfpQuoteResponsePlanDetailBenefits_QNAME = new QName("", "Benefits");
    private final static QName _GetCarriersPlansBenefitsResponseDrugListDrugs_QNAME = new QName("", "Drugs");
    private final static QName _GetGroupQuoteRequestGroupRateFactors_QNAME = new QName("", "GroupRateFactors");
    private final static QName _GetGroupQuoteRequestGroupRateKeys_QNAME = new QName("", "GroupRateKeys");
    private final static QName _IfpShoppingCartCreatedDate_QNAME = new QName("", "CreatedDate");
    private final static QName _IfpShoppingCartApplicationId_QNAME = new QName("", "ApplicationId");
    private final static QName _IfpShoppingCartProductChoices_QNAME = new QName("", "ProductChoices");
    private final static QName _GetGroupQuoteRequestGroupRateKeyProposalId_QNAME = new QName("", "ProposalId");
    private final static QName _GetIfpQuoteResponseQuoteCarrierRateCarrierDetails_QNAME = new QName("", "CarrierDetails");
    private final static QName _GetCarrierLoadHistoryRequestInputToLoadSequenceId_QNAME = new QName("", "ToLoadSequenceId");
    private final static QName _GetCarrierLoadHistoryRequestInputFromEffectiveDate_QNAME = new QName("", "FromEffectiveDate");
    private final static QName _GetCarrierLoadHistoryRequestInputToEffectiveDate_QNAME = new QName("", "ToEffectiveDate");
    private final static QName _FamilyComments_QNAME = new QName("", "Comments");
    private final static QName _FamilyFamilyId_QNAME = new QName("", "FamilyId");
    private final static QName _FamilySecondaryContact_QNAME = new QName("", "SecondaryContact");
    private final static QName _FamilyMembers_QNAME = new QName("", "Members");
    private final static QName _FamilyMailingAddress_QNAME = new QName("", "MailingAddress");
    private final static QName _FamilyOriginatingUserAgent_QNAME = new QName("", "OriginatingUserAgent");
    private final static QName _FamilyExpirationDate_QNAME = new QName("", "ExpirationDate");
    private final static QName _FamilyFamilySourceReferenceId_QNAME = new QName("", "FamilySourceReferenceId");
    private final static QName _FamilyTerminationDate_QNAME = new QName("", "TerminationDate");
    private final static QName _FamilyOESConsumerId_QNAME = new QName("", "OESConsumerId");
    private final static QName _FamilyFamilySource_QNAME = new QName("", "FamilySource");
    private final static QName _FamilyOriginatingIPAddress_QNAME = new QName("", "OriginatingIPAddress");
    private final static QName _FamilyPrimaryContact_QNAME = new QName("", "PrimaryContact");
    private final static QName _IfpPlanChoiceBasePremium_QNAME = new QName("", "Premium");
    private final static QName _IfpPlanChoiceBaseLifeAmount_QNAME = new QName("", "LifeAmount");
    private final static QName _GetCustomProductsQuoteRequestCustomRateFactorClientCountyFIPS_QNAME = new QName("", "ClientCountyFIPS");
    private final static QName _GetCustomProductsQuoteRequestCustomRateFactorClientState_QNAME = new QName("", "ClientState");
    private final static QName _FormularyLookupResponseDrugAttributeStrength_QNAME = new QName("", "Strength");
    private final static QName _FormularyLookupResponseDrugAttributeUnit_QNAME = new QName("", "Unit");
    private final static QName _FormularyLookupResponseDrugAttributeRxOTC_QNAME = new QName("", "RxOTC");
    private final static QName _PlanChoiceBasePlanId_QNAME = new QName("", "PlanId");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyURL_QNAME = new QName("", "FormularyURL");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyNumberTiers_QNAME = new QName("", "NumberTiers");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyID_QNAME = new QName("", "FormularyID");
    private final static QName _GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTiers_QNAME = new QName("", "FormularyTiers");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierPlanPlanLinkValidFromDate_QNAME = new QName("", "ValidFromDate");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierPlanPlanLinkLinkActiveText_QNAME = new QName("", "LinkActiveText");
    private final static QName _IfpRateBaseNumberOfDays_QNAME = new QName("", "NumberOfDays");
    private final static QName _IfpRateBaseBaseRateUnit_QNAME = new QName("", "BaseRateUnit");
    private final static QName _LocationBaseType_QNAME = new QName("", "Type");
    private final static QName _FamilyContactContactAttributes_QNAME = new QName("", "ContactAttributes");
    private final static QName _ServiceBaseCost_QNAME = new QName("", "Cost");
    private final static QName _ServiceBaseAmount_QNAME = new QName("", "Amount");
    private final static QName _GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRates_QNAME = new QName("", "EmployeeRates");
    private final static QName _GetGroupQuoteResponseQuoteCarrierRatePlanRatePlanDetails_QNAME = new QName("", "PlanDetails");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierDrugLists_QNAME = new QName("", "DrugLists");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierLocations_QNAME = new QName("", "Locations");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierFormularies_QNAME = new QName("", "Formularies");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierContacts_QNAME = new QName("", "Contacts");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierCarrierData_QNAME = new QName("", "CarrierData");
    private final static QName _GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlans_QNAME = new QName("", "DroppedPlans");
    private final static QName _GetIfpQuoteRequestIfpRateFactorHouseholdSize_QNAME = new QName("", "HouseholdSize");
    private final static QName _GetIfpQuoteRequestIfpRateFactorIsNativeAmerican_QNAME = new QName("", "IsNativeAmerican");
    private final static QName _GetIfpQuoteRequestIfpRateFactorHouseholdMAGI_QNAME = new QName("", "HouseholdMAGI");
    private final static QName _IfpShoppingCartProductChoiceCoverageWaived_QNAME = new QName("", "CoverageWaived");
    private final static QName _IfpShoppingCartProductChoiceMemberId_QNAME = new QName("", "MemberId");
    private final static QName _SubmitCustomProductsRequestCustomProducts_QNAME = new QName("", "CustomProducts");
    private final static QName _CustomProductPlanBusinessRuleTobaccoFreeMonths_QNAME = new QName("", "TobaccoFreeMonths");
    private final static QName _CustomProductPlanBusinessRuleChildrenCap_QNAME = new QName("", "ChildrenCap");
    private final static QName _CustomProductPlanBusinessRuleAllowedRelations_QNAME = new QName("", "AllowedRelations");
    private final static QName _CustomProductPlanBusinessRuleIgnoreMembershipDate_QNAME = new QName("", "IgnoreMembershipDate");
    private final static QName _CustomProductPlanBusinessRuleMinAdultAge_QNAME = new QName("", "MinAdultAge");
    private final static QName _CarrierBaseLogoFileSmall_QNAME = new QName("", "LogoFileSmall");
    private final static QName _CarrierBaseDisclaimerHtml_QNAME = new QName("", "DisclaimerHtml");
    private final static QName _CarrierBaseCompanyId_QNAME = new QName("", "CompanyId");
    private final static QName _CarrierBaseLogoFileLarge_QNAME = new QName("", "LogoFileLarge");
    private final static QName _CarrierBaseLogoFileMediumTransparent_QNAME = new QName("", "LogoFileMediumTransparent");
    private final static QName _CarrierBaseLogoFileMedium_QNAME = new QName("", "LogoFileMedium");
    private final static QName _FormularyLookupRequestDrugRxCUIs_QNAME = new QName("", "RxCUIs");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateF1Rate_QNAME = new QName("", "F1Rate");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSD3Rate_QNAME = new QName("", "SD3Rate");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSD2Rate_QNAME = new QName("", "SD2Rate");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRatePlanName_QNAME = new QName("", "PlanName");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSRate_QNAME = new QName("", "SRate");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateF3Rate_QNAME = new QName("", "F3Rate");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateDepSmokerSurcharge_QNAME = new QName("", "DepSmokerSurcharge");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSmokerSurcharge_QNAME = new QName("", "SmokerSurcharge");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateF2Rate_QNAME = new QName("", "F2Rate");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateDepRate_QNAME = new QName("", "DepRate");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSSRate_QNAME = new QName("", "SSRate");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSD1Rate_QNAME = new QName("", "SD1Rate");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateRate_QNAME = new QName("", "Rate");
    private final static QName _IfpShoppingCartProductChoicePlanChoicesStatusChangeMessage_QNAME = new QName("", "Message");
    private final static QName _GetCarrierLoadHistoryResponseLoadEventCarrierReleaseNote_QNAME = new QName("", "ReleaseNote");
    private final static QName _GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlans_QNAME = new QName("", "VersionedPlans");
    private final static QName _CustomProductPlanAreaLookupStrategy_QNAME = new QName("", "AreaLookupStrategy");
    private final static QName _CustomProductPlanRawRates_QNAME = new QName("", "RawRates");
    private final static QName _CustomProductPlanBusinessRules_QNAME = new QName("", "BusinessRules");
    private final static QName _GetGroupQuoteRequestGroupRateFactorRAFOverrides_QNAME = new QName("", "RAFOverrides");
    private final static QName _GetGroupQuoteRequestGroupRateFactorERContributionScenarios_QNAME = new QName("", "ERContributionScenarios");
    private final static QName _GetGroupQuoteRequestGroupRateFactorSIC_QNAME = new QName("", "SIC");
    private final static QName _FamilyMemberHeightInches_QNAME = new QName("", "HeightInches");
    private final static QName _FamilyMemberAnnualIncome_QNAME = new QName("", "AnnualIncome");
    private final static QName _FamilyMemberHeightFeet_QNAME = new QName("", "HeightFeet");
    private final static QName _FamilyMemberIncludeInQuote_QNAME = new QName("", "IncludeInQuote");
    private final static QName _FamilyMemberWeight_QNAME = new QName("", "Weight");
    private final static QName _FamilyMemberIncludeInHouseholdMAGI_QNAME = new QName("", "IncludeInHouseholdMAGI");
    private final static QName _FamilyMemberQHPEligible_QNAME = new QName("", "QHPEligible");
    private final static QName _ContactBaseContactType_QNAME = new QName("", "ContactType");
    private final static QName _GetIfpQuoteResponseQuoteMemberPlans_QNAME = new QName("", "MemberPlans");
    private final static QName _GetZipCodeInfoRequestInputIsQuotableFilter_QNAME = new QName("", "IsQuotableFilter");
    private final static QName _FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugs_QNAME = new QName("", "FormularyDrugs");
    private final static QName _FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierTierType_QNAME = new QName("", "TierType");
    private final static QName _FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOs_QNAME = new QName("", "CostSharingTypeVOs");
    private final static QName _CustomProductRatingAreas_QNAME = new QName("", "RatingAreas");
    private final static QName _GetIfpQuoteRequestIfpRateFactors_QNAME = new QName("", "IfpRateFactors");
    private final static QName _GetIfpQuoteRequestIfpRateKeys_QNAME = new QName("", "IfpRateKeys");
    private final static QName _GroupGroupSourceReferenceId_QNAME = new QName("", "GroupSourceReferenceId");
    private final static QName _GroupGroupSource_QNAME = new QName("", "GroupSource");
    private final static QName _GroupLifeMultiplier_QNAME = new QName("", "LifeMultiplier");
    private final static QName _PlanDetailsBaseBenefitsLink_QNAME = new QName("", "BenefitsLink");
    private final static QName _PlanDetailsBaseProviderLink_QNAME = new QName("", "ProviderLink");
    private final static QName _PlanDetailsBaseUnderwritingLink_QNAME = new QName("", "UnderwritingLink");
    private final static QName _PlanBaseFormularyId_QNAME = new QName("", "FormularyId");
    private final static QName _GetCountiesResponseCounties_QNAME = new QName("", "Counties");
    private final static QName _RAFOverrideBaseRAF_QNAME = new QName("", "RAF");
    private final static QName _FetchCustomProductsRequestInputProductFilters_QNAME = new QName("", "ProductFilters");
    private final static QName _GetGroupQuoteRequestGroupRateFactorEmployeeOnCobra_QNAME = new QName("", "OnCobra");
    private final static QName _GetGroupQuoteRequestGroupRateFactorEmployeeWorksiteZipCode_QNAME = new QName("", "WorksiteZipCode");
    private final static QName _GetGroupQuoteRequestGroupRateFactorEmployeeWorksiteCountyName_QNAME = new QName("", "WorksiteCountyName");
    private final static QName _GetZipCodeInfoResponseZipCodes_QNAME = new QName("", "ZipCodes");
    private final static QName _GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedReason_QNAME = new QName("", "DroppedReason");
    private final static QName _CustomProductRatingAreaZipRanges_QNAME = new QName("", "ZipRanges");
    private final static QName _CustomProductRatingAreaZipCounties_QNAME = new QName("", "ZipCounties");
    private final static QName _GetIfpQuoteRequestPreferenceBenchmarkRule_QNAME = new QName("", "BenchmarkRule");
    private final static QName _GetCarriersPlansBenefitsRequestInputCarrierPlanFilters_QNAME = new QName("", "CarrierPlanFilters");
    private final static QName _GetCarriersPlansBenefitsRequestInputLatestVersionOnly_QNAME = new QName("", "LatestVersionOnly");
    private final static QName _GetCarriersPlansBenefitsRequestInputDrugFilter_QNAME = new QName("", "DrugFilter");
    private final static QName _GetCarriersPlansBenefitsResponseCarrierPlanPlanLinks_QNAME = new QName("", "PlanLinks");
    private final static QName _BenefitBaseCategory_QNAME = new QName("", "Category");
    private final static QName _BenefitBaseTinyValue_QNAME = new QName("", "TinyValue");
    private final static QName _BenefitBaseFullValue_QNAME = new QName("", "FullValue");
    private final static QName _ZipCodeInfoBaseLatitude_QNAME = new QName("", "Latitude");
    private final static QName _ZipCodeInfoBaseLongitude_QNAME = new QName("", "Longitude");
    private final static QName _ZipCodeInfoBaseTimeZone_QNAME = new QName("", "TimeZone");
    private final static QName _ZipCodeInfoBaseAreaCodes_QNAME = new QName("", "AreaCodes");
    private final static QName _IfpShoppingCartProductChoicePlanChoicesStatusHistory_QNAME = new QName("", "StatusHistory");
    private final static QName _IfpShoppingCartProductChoicePlanChoicesLeadStatus_QNAME = new QName("", "LeadStatus");
    private final static QName _GroupEmployeeEmailAddress_QNAME = new QName("", "EmailAddress");
    private final static QName _GroupEmployeeSalary_QNAME = new QName("", "Salary");
    private final static QName _GroupEmployeeUserName_QNAME = new QName("", "UserName");
    private final static QName _GetCustomProductsQuoteResponseCustomProductsQuote_QNAME = new QName("", "CustomProductsQuote");
    private final static QName _GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariances_QNAME = new QName("", "CostShareVariances");
    private final static QName _GetCarriersPlansBenefitsResponseFormularyDrugTierTierTypes_QNAME = new QName("", "TierTypes");
    private final static QName _ViewPointBaseLongValue_QNAME = new QName("", "LongValue");
    private final static QName _ViewPointBaseShortValue_QNAME = new QName("", "ShortValue");
    private final static QName _GetPlansbyRxResponseCarrierPlanFormulary_QNAME = new QName("", "Formulary");
    private final static QName _GetCarriersPlansBenefitsResponseFormularyDrugTiers_QNAME = new QName("", "DrugTiers");
    private final static QName _GetCarriersPlansBenefitsResponseFormularyNumberOfTiers_QNAME = new QName("", "NumberOfTiers");
    private final static QName _FetchCustomProductsResponseRemarks_QNAME = new QName("", "Remarks");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponse }
     * 
     */
    public GetIfpQuoteResponse createGetIfpQuoteResponse() {
        return new GetIfpQuoteResponse();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteResponse }
     * 
     */
    public GetCustomProductsQuoteResponse createGetCustomProductsQuoteResponse() {
        return new GetCustomProductsQuoteResponse();
    }

    /**
     * Create an instance of {@link SubmitCustomProductsRequest }
     * 
     */
    public SubmitCustomProductsRequest createSubmitCustomProductsRequest() {
        return new SubmitCustomProductsRequest();
    }

    /**
     * Create an instance of {@link GetPlansbyRxRequest }
     * 
     */
    public GetPlansbyRxRequest createGetPlansbyRxRequest() {
        return new GetPlansbyRxRequest();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequest }
     * 
     */
    public GetGroupQuoteRequest createGetGroupQuoteRequest() {
        return new GetGroupQuoteRequest();
    }

    /**
     * Create an instance of {@link GetIfpShoppingCartsRequest }
     * 
     */
    public GetIfpShoppingCartsRequest createGetIfpShoppingCartsRequest() {
        return new GetIfpShoppingCartsRequest();
    }

    /**
     * Create an instance of {@link GetCountiesRequest }
     * 
     */
    public GetCountiesRequest createGetCountiesRequest() {
        return new GetCountiesRequest();
    }

    /**
     * Create an instance of {@link FormularyLookupRequest }
     * 
     */
    public FormularyLookupRequest createFormularyLookupRequest() {
        return new FormularyLookupRequest();
    }

    /**
     * Create an instance of {@link SubmitFamilyResponse }
     * 
     */
    public SubmitFamilyResponse createSubmitFamilyResponse() {
        return new SubmitFamilyResponse();
    }

    /**
     * Create an instance of {@link ListRxResponse }
     * 
     */
    public ListRxResponse createListRxResponse() {
        return new ListRxResponse();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponse }
     * 
     */
    public GetCarriersPlansBenefitsResponse createGetCarriersPlansBenefitsResponse() {
        return new GetCarriersPlansBenefitsResponse();
    }

    /**
     * Create an instance of {@link GetZipCodeInfoResponse }
     * 
     */
    public GetZipCodeInfoResponse createGetZipCodeInfoResponse() {
        return new GetZipCodeInfoResponse();
    }

    /**
     * Create an instance of {@link SubmitGroupShoppingCartResponse }
     * 
     */
    public SubmitGroupShoppingCartResponse createSubmitGroupShoppingCartResponse() {
        return new SubmitGroupShoppingCartResponse();
    }

    /**
     * Create an instance of {@link SubmitCustomProductsResponse }
     * 
     */
    public SubmitCustomProductsResponse createSubmitCustomProductsResponse() {
        return new SubmitCustomProductsResponse();
    }

    /**
     * Create an instance of {@link GetGroupRequest }
     * 
     */
    public GetGroupRequest createGetGroupRequest() {
        return new GetGroupRequest();
    }

    /**
     * Create an instance of {@link GetFamilyRequest }
     * 
     */
    public GetFamilyRequest createGetFamilyRequest() {
        return new GetFamilyRequest();
    }

    /**
     * Create an instance of {@link FormularyLookupResponse }
     * 
     */
    public FormularyLookupResponse createFormularyLookupResponse() {
        return new FormularyLookupResponse();
    }

    /**
     * Create an instance of {@link GetCarrierLoadHistoryResponse }
     * 
     */
    public GetCarrierLoadHistoryResponse createGetCarrierLoadHistoryResponse() {
        return new GetCarrierLoadHistoryResponse();
    }

    /**
     * Create an instance of {@link ListRxRequest }
     * 
     */
    public ListRxRequest createListRxRequest() {
        return new ListRxRequest();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteRequest }
     * 
     */
    public GetCustomProductsQuoteRequest createGetCustomProductsQuoteRequest() {
        return new GetCustomProductsQuoteRequest();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponse }
     * 
     */
    public GetPlansbyRxResponse createGetPlansbyRxResponse() {
        return new GetPlansbyRxResponse();
    }

    /**
     * Create an instance of {@link GetGroupResponse }
     * 
     */
    public GetGroupResponse createGetGroupResponse() {
        return new GetGroupResponse();
    }

    /**
     * Create an instance of {@link SubmitFamilyRequest }
     * 
     */
    public SubmitFamilyRequest createSubmitFamilyRequest() {
        return new SubmitFamilyRequest();
    }

    /**
     * Create an instance of {@link GetCarrierLoadHistoryRequest }
     * 
     */
    public GetCarrierLoadHistoryRequest createGetCarrierLoadHistoryRequest() {
        return new GetCarrierLoadHistoryRequest();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsRequest }
     * 
     */
    public GetCarriersPlansBenefitsRequest createGetCarriersPlansBenefitsRequest() {
        return new GetCarriersPlansBenefitsRequest();
    }

    /**
     * Create an instance of {@link SubmitGroupRequest }
     * 
     */
    public SubmitGroupRequest createSubmitGroupRequest() {
        return new SubmitGroupRequest();
    }

    /**
     * Create an instance of {@link SubmitIfpShoppingCartResponse }
     * 
     */
    public SubmitIfpShoppingCartResponse createSubmitIfpShoppingCartResponse() {
        return new SubmitIfpShoppingCartResponse();
    }

    /**
     * Create an instance of {@link SubmitIfpShoppingCartRequest }
     * 
     */
    public SubmitIfpShoppingCartRequest createSubmitIfpShoppingCartRequest() {
        return new SubmitIfpShoppingCartRequest();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequest }
     * 
     */
    public GetIfpQuoteRequest createGetIfpQuoteRequest() {
        return new GetIfpQuoteRequest();
    }

    /**
     * Create an instance of {@link SubmitGroupResponse }
     * 
     */
    public SubmitGroupResponse createSubmitGroupResponse() {
        return new SubmitGroupResponse();
    }

    /**
     * Create an instance of {@link GetZipCodeInfoRequest }
     * 
     */
    public GetZipCodeInfoRequest createGetZipCodeInfoRequest() {
        return new GetZipCodeInfoRequest();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponse }
     * 
     */
    public GetGroupQuoteResponse createGetGroupQuoteResponse() {
        return new GetGroupQuoteResponse();
    }

    /**
     * Create an instance of {@link GetFamilyResponse }
     * 
     */
    public GetFamilyResponse createGetFamilyResponse() {
        return new GetFamilyResponse();
    }

    /**
     * Create an instance of {@link GetCountiesResponse }
     * 
     */
    public GetCountiesResponse createGetCountiesResponse() {
        return new GetCountiesResponse();
    }

    /**
     * Create an instance of {@link FetchCustomProductsResponse }
     * 
     */
    public FetchCustomProductsResponse createFetchCustomProductsResponse() {
        return new FetchCustomProductsResponse();
    }

    /**
     * Create an instance of {@link FetchCustomProductsRequest }
     * 
     */
    public FetchCustomProductsRequest createFetchCustomProductsRequest() {
        return new FetchCustomProductsRequest();
    }

    /**
     * Create an instance of {@link SubmitGroupShoppingCartRequest }
     * 
     */
    public SubmitGroupShoppingCartRequest createSubmitGroupShoppingCartRequest() {
        return new SubmitGroupShoppingCartRequest();
    }

    /**
     * Create an instance of {@link GetIfpShoppingCartsResponse }
     * 
     */
    public GetIfpShoppingCartsResponse createGetIfpShoppingCartsResponse() {
        return new GetIfpShoppingCartsResponse();
    }

    /**
     * Create an instance of {@link GroupLocationAddress }
     * 
     */
    public GroupLocationAddress createGroupLocationAddress() {
        return new GroupLocationAddress();
    }

    /**
     * Create an instance of {@link CityBase }
     * 
     */
    public CityBase createCityBase() {
        return new CityBase();
    }

    /**
     * Create an instance of {@link FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier }
     * 
     */
    public FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier() {
        return new FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsRequestVersionFilter }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsRequestVersionFilter createArrayOfGetCarriersPlansBenefitsRequestVersionFilter() {
        return new ArrayOfGetCarriersPlansBenefitsRequestVersionFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }
     * 
     */
    public ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO createArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO() {
        return new ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO();
    }

    /**
     * Create an instance of {@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier }
     * 
     */
    public ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier createArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier() {
        return new ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier();
    }

    /**
     * Create an instance of {@link GroupShoppingCartProductChoicePlanChoices }
     * 
     */
    public GroupShoppingCartProductChoicePlanChoices createGroupShoppingCartProductChoicePlanChoices() {
        return new GroupShoppingCartProductChoicePlanChoices();
    }

    /**
     * Create an instance of {@link QuoteRequestBase }
     * 
     */
    public QuoteRequestBase createQuoteRequestBase() {
        return new QuoteRequestBase();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestGroupRateFactor }
     * 
     */
    public GetGroupQuoteRequestGroupRateFactor createGetGroupQuoteRequestGroupRateFactor() {
        return new GetGroupQuoteRequestGroupRateFactor();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestPreferenceDrugFilter }
     * 
     */
    public GetGroupQuoteRequestPreferenceDrugFilter createGetGroupQuoteRequestPreferenceDrugFilter() {
        return new GetGroupQuoteRequestPreferenceDrugFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponseMemberRate }
     * 
     */
    public ArrayOfGetGroupQuoteResponseMemberRate createArrayOfGetGroupQuoteResponseMemberRate() {
        return new ArrayOfGetGroupQuoteResponseMemberRate();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate }
     * 
     */
    public GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate createGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate() {
        return new GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter createArrayOfGetCarriersPlansBenefitsRequestBenefitFilter() {
        return new ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate }
     * 
     */
    public ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate createArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate() {
        return new ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponsePlanDetailBenefitCoverage }
     * 
     */
    public GetIfpQuoteResponsePlanDetailBenefitCoverage createGetIfpQuoteResponsePlanDetailBenefitCoverage() {
        return new GetIfpQuoteResponsePlanDetailBenefitCoverage();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseContact }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseContact createArrayOfGetCarriersPlansBenefitsResponseContact() {
        return new ArrayOfGetCarriersPlansBenefitsResponseContact();
    }

    /**
     * Create an instance of {@link FormularyLookupRequestPlanFilter }
     * 
     */
    public FormularyLookupRequestPlanFilter createFormularyLookupRequestPlanFilter() {
        return new FormularyLookupRequestPlanFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGroupEmployeeMember }
     * 
     */
    public ArrayOfGroupEmployeeMember createArrayOfGroupEmployeeMember() {
        return new ArrayOfGroupEmployeeMember();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService }
     * 
     */
    public GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService createGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService() {
        return new GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService();
    }

    /**
     * Create an instance of {@link FetchCustomProductsRequestAccessKey }
     * 
     */
    public FetchCustomProductsRequestAccessKey createFetchCustomProductsRequestAccessKey() {
        return new FetchCustomProductsRequestAccessKey();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink createArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink() {
        return new ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsRequestInput }
     * 
     */
    public GetCarriersPlansBenefitsRequestInput createGetCarriersPlansBenefitsRequestInput() {
        return new GetCarriersPlansBenefitsRequestInput();
    }

    /**
     * Create an instance of {@link ScenarioBase }
     * 
     */
    public ScenarioBase createScenarioBase() {
        return new ScenarioBase();
    }

    /**
     * Create an instance of {@link ArrayOfGroupScenario }
     * 
     */
    public ArrayOfGroupScenario createArrayOfGroupScenario() {
        return new ArrayOfGroupScenario();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate }
     * 
     */
    public ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate createArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate() {
        return new ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteResponseMemberRate }
     * 
     */
    public GetCustomProductsQuoteResponseMemberRate createGetCustomProductsQuoteResponseMemberRate() {
        return new GetCustomProductsQuoteResponseMemberRate();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseContact }
     * 
     */
    public GetCarriersPlansBenefitsResponseContact createGetCarriersPlansBenefitsResponseContact() {
        return new GetCarriersPlansBenefitsResponseContact();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteRequestProductChoice }
     * 
     */
    public ArrayOfGetIfpQuoteRequestProductChoice createArrayOfGetIfpQuoteRequestProductChoice() {
        return new ArrayOfGetIfpQuoteRequestProductChoice();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxResponseCarrier }
     * 
     */
    public ArrayOfGetPlansbyRxResponseCarrier createArrayOfGetPlansbyRxResponseCarrier() {
        return new ArrayOfGetPlansbyRxResponseCarrier();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestGroupRateFactorEmployee }
     * 
     */
    public GetGroupQuoteRequestGroupRateFactorEmployee createGetGroupQuoteRequestGroupRateFactorEmployee() {
        return new GetGroupQuoteRequestGroupRateFactorEmployee();
    }

    /**
     * Create an instance of {@link ArrayOfFormularyLookupResponseDrugAttribute }
     * 
     */
    public ArrayOfFormularyLookupResponseDrugAttribute createArrayOfFormularyLookupResponseDrugAttribute() {
        return new ArrayOfFormularyLookupResponseDrugAttribute();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestMember }
     * 
     */
    public GetIfpQuoteRequestMember createGetIfpQuoteRequestMember() {
        return new GetIfpQuoteRequestMember();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseQuoteCarrierRatePlanRate }
     * 
     */
    public GetGroupQuoteResponseQuoteCarrierRatePlanRate createGetGroupQuoteResponseQuoteCarrierRatePlanRate() {
        return new GetGroupQuoteResponseQuoteCarrierRatePlanRate();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestPreferenceBenchmarkRules }
     * 
     */
    public GetIfpQuoteRequestPreferenceBenchmarkRules createGetIfpQuoteRequestPreferenceBenchmarkRules() {
        return new GetIfpQuoteRequestPreferenceBenchmarkRules();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestPreference }
     * 
     */
    public GetIfpQuoteRequestPreference createGetIfpQuoteRequestPreference() {
        return new GetIfpQuoteRequestPreference();
    }

    /**
     * Create an instance of {@link GroupContactContactAttribute }
     * 
     */
    public GroupContactContactAttribute createGroupContactContactAttribute() {
        return new GroupContactContactAttribute();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService }
     * 
     */
    public ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService createArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService() {
        return new ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService();
    }

    /**
     * Create an instance of {@link GroupScenario }
     * 
     */
    public GroupScenario createGroupScenario() {
        return new GroupScenario();
    }

    /**
     * Create an instance of {@link CarrierRateBase }
     * 
     */
    public CarrierRateBase createCarrierRateBase() {
        return new CarrierRateBase();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseDrugList }
     * 
     */
    public GetCarriersPlansBenefitsResponseDrugList createGetCarriersPlansBenefitsResponseDrugList() {
        return new GetCarriersPlansBenefitsResponseDrugList();
    }

    /**
     * Create an instance of {@link FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }
     * 
     */
    public FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug() {
        return new FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug();
    }

    /**
     * Create an instance of {@link PlanChoiceBase }
     * 
     */
    public PlanChoiceBase createPlanChoiceBase() {
        return new PlanChoiceBase();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseItem }
     * 
     */
    public GetIfpQuoteResponseItem createGetIfpQuoteResponseItem() {
        return new GetIfpQuoteResponseItem();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter }
     * 
     */
    public GetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter createGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter() {
        return new GetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponsePlanDetailBenefit }
     * 
     */
    public GetIfpQuoteResponsePlanDetailBenefit createGetIfpQuoteResponsePlanDetailBenefit() {
        return new GetIfpQuoteResponsePlanDetailBenefit();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan }
     * 
     */
    public GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan createGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan() {
        return new GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponseQuoteCarrier }
     * 
     */
    public ArrayOfGetIfpQuoteResponseQuoteCarrier createArrayOfGetIfpQuoteResponseQuoteCarrier() {
        return new ArrayOfGetIfpQuoteResponseQuoteCarrier();
    }

    /**
     * Create an instance of {@link ArrayOfCustomProduct }
     * 
     */
    public ArrayOfCustomProduct createArrayOfCustomProduct() {
        return new ArrayOfCustomProduct();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestPreferenceVersionFilter }
     * 
     */
    public ArrayOfGetGroupQuoteRequestPreferenceVersionFilter createArrayOfGetGroupQuoteRequestPreferenceVersionFilter() {
        return new ArrayOfGetGroupQuoteRequestPreferenceVersionFilter();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponsePlanDetailBenefitCoverage }
     * 
     */
    public GetGroupQuoteResponsePlanDetailBenefitCoverage createGetGroupQuoteResponsePlanDetailBenefitCoverage() {
        return new GetGroupQuoteResponsePlanDetailBenefitCoverage();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint }
     * 
     */
    public ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint createArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint() {
        return new ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint();
    }

    /**
     * Create an instance of {@link GroupShoppingCartsBase }
     * 
     */
    public GroupShoppingCartsBase createGroupShoppingCartsBase() {
        return new GroupShoppingCartsBase();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestGroupRateKey }
     * 
     */
    public GetGroupQuoteRequestGroupRateKey createGetGroupQuoteRequestGroupRateKey() {
        return new GetGroupQuoteRequestGroupRateKey();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestPreferenceDrugFilter }
     * 
     */
    public ArrayOfGetGroupQuoteRequestPreferenceDrugFilter createArrayOfGetGroupQuoteRequestPreferenceDrugFilter() {
        return new ArrayOfGetGroupQuoteRequestPreferenceDrugFilter();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseCarrierDetail }
     * 
     */
    public GetIfpQuoteResponseCarrierDetail createGetIfpQuoteResponseCarrierDetail() {
        return new GetIfpQuoteResponseCarrierDetail();
    }

    /**
     * Create an instance of {@link FetchCustomProductsRequestInput }
     * 
     */
    public FetchCustomProductsRequestInput createFetchCustomProductsRequestInput() {
        return new FetchCustomProductsRequestInput();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan }
     * 
     */
    public GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan createGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan() {
        return new GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan();
    }

    /**
     * Create an instance of {@link FormularyLookupResponseDrugAttribute }
     * 
     */
    public FormularyLookupResponseDrugAttribute createFormularyLookupResponseDrugAttribute() {
        return new FormularyLookupResponseDrugAttribute();
    }

    /**
     * Create an instance of {@link CustomProductPlanRawRate }
     * 
     */
    public CustomProductPlanRawRate createCustomProductPlanRawRate() {
        return new CustomProductPlanRawRate();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice }
     * 
     */
    public GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice createGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice() {
        return new GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate }
     * 
     */
    public ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate createArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate() {
        return new ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent }
     * 
     */
    public GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent createGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent() {
        return new GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseQuoteCarrierRate }
     * 
     */
    public GetIfpQuoteResponseQuoteCarrierRate createGetIfpQuoteResponseQuoteCarrierRate() {
        return new GetIfpQuoteResponseQuoteCarrierRate();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxItem }
     * 
     */
    public ArrayOfGetPlansbyRxItem createArrayOfGetPlansbyRxItem() {
        return new ArrayOfGetPlansbyRxItem();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponseCarrierPlanBenefitCoverage }
     * 
     */
    public GetPlansbyRxResponseCarrierPlanBenefitCoverage createGetPlansbyRxResponseCarrierPlanBenefitCoverage() {
        return new GetPlansbyRxResponseCarrierPlanBenefitCoverage();
    }

    /**
     * Create an instance of {@link FamilyLocationAddress }
     * 
     */
    public FamilyLocationAddress createFamilyLocationAddress() {
        return new FamilyLocationAddress();
    }

    /**
     * Create an instance of {@link GetCountiesRequestAccessKey }
     * 
     */
    public GetCountiesRequestAccessKey createGetCountiesRequestAccessKey() {
        return new GetCountiesRequestAccessKey();
    }

    /**
     * Create an instance of {@link CustomProductPlanBusinessRuleAllowedRelation }
     * 
     */
    public CustomProductPlanBusinessRuleAllowedRelation createCustomProductPlanBusinessRuleAllowedRelation() {
        return new CustomProductPlanBusinessRuleAllowedRelation();
    }

    /**
     * Create an instance of {@link EmployeeRateBase }
     * 
     */
    public EmployeeRateBase createEmployeeRateBase() {
        return new EmployeeRateBase();
    }

    /**
     * Create an instance of {@link PlanFilterBase }
     * 
     */
    public PlanFilterBase createPlanFilterBase() {
        return new PlanFilterBase();
    }

    /**
     * Create an instance of {@link FamilyContact }
     * 
     */
    public FamilyContact createFamilyContact() {
        return new FamilyContact();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseQuoteDroppedCarrier }
     * 
     */
    public GetGroupQuoteResponseQuoteDroppedCarrier createGetGroupQuoteResponseQuoteDroppedCarrier() {
        return new GetGroupQuoteResponseQuoteDroppedCarrier();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsRequestDrugFilters }
     * 
     */
    public GetCarriersPlansBenefitsRequestDrugFilters createGetCarriersPlansBenefitsRequestDrugFilters() {
        return new GetCarriersPlansBenefitsRequestDrugFilters();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsRequestAccessKey }
     * 
     */
    public GetCarriersPlansBenefitsRequestAccessKey createGetCarriersPlansBenefitsRequestAccessKey() {
        return new GetCarriersPlansBenefitsRequestAccessKey();
    }

    /**
     * Create an instance of {@link ArrayOfError }
     * 
     */
    public ArrayOfError createArrayOfError() {
        return new ArrayOfError();
    }

    /**
     * Create an instance of {@link ServiceBase }
     * 
     */
    public ServiceBase createServiceBase() {
        return new ServiceBase();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier }
     * 
     */
    public GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier createGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier() {
        return new GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link GetZipCodeInfoResponseZipCodeInfoCity }
     * 
     */
    public GetZipCodeInfoResponseZipCodeInfoCity createGetZipCodeInfoResponseZipCodeInfoCity() {
        return new GetZipCodeInfoResponseZipCodeInfoCity();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }
     * 
     */
    public ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug createArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug() {
        return new ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug();
    }

    /**
     * Create an instance of {@link PlanComponentBase }
     * 
     */
    public PlanComponentBase createPlanComponentBase() {
        return new PlanComponentBase();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate }
     * 
     */
    public ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate createArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate() {
        return new ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate();
    }

    /**
     * Create an instance of {@link ZipCodeInfoBase }
     * 
     */
    public ZipCodeInfoBase createZipCodeInfoBase() {
        return new ZipCodeInfoBase();
    }

    /**
     * Create an instance of {@link ArrayOfFamilyMember }
     * 
     */
    public ArrayOfFamilyMember createArrayOfFamilyMember() {
        return new ArrayOfFamilyMember();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint createArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint() {
        return new ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint();
    }

    /**
     * Create an instance of {@link VersionFilterBase }
     * 
     */
    public VersionFilterBase createVersionFilterBase() {
        return new VersionFilterBase();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseFormulary }
     * 
     */
    public GetCarriersPlansBenefitsResponseFormulary createGetCarriersPlansBenefitsResponseFormulary() {
        return new GetCarriersPlansBenefitsResponseFormulary();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponsePlanDetailBenefit }
     * 
     */
    public GetGroupQuoteResponsePlanDetailBenefit createGetGroupQuoteResponsePlanDetailBenefit() {
        return new GetGroupQuoteResponsePlanDetailBenefit();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestGroupRateFactorEmployeeMember }
     * 
     */
    public GetGroupQuoteRequestGroupRateFactorEmployeeMember createGetGroupQuoteRequestGroupRateFactorEmployeeMember() {
        return new GetGroupQuoteRequestGroupRateFactorEmployeeMember();
    }

    /**
     * Create an instance of {@link ArrayOfFormularyLookupResponseCarrier }
     * 
     */
    public ArrayOfFormularyLookupResponseCarrier createArrayOfFormularyLookupResponseCarrier() {
        return new ArrayOfFormularyLookupResponseCarrier();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponseCarrierPlanBenefit }
     * 
     */
    public GetPlansbyRxResponseCarrierPlanBenefit createGetPlansbyRxResponseCarrierPlanBenefit() {
        return new GetPlansbyRxResponseCarrierPlanBenefit();
    }

    /**
     * Create an instance of {@link ArrayOfGetCustomProductsQuoteResponseMemberRate }
     * 
     */
    public ArrayOfGetCustomProductsQuoteResponseMemberRate createArrayOfGetCustomProductsQuoteResponseMemberRate() {
        return new ArrayOfGetCustomProductsQuoteResponseMemberRate();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestPreferenceChoiceFilter }
     * 
     */
    public GetGroupQuoteRequestPreferenceChoiceFilter createGetGroupQuoteRequestPreferenceChoiceFilter() {
        return new GetGroupQuoteRequestPreferenceChoiceFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefit }
     * 
     */
    public ArrayOfGetPlansbyRxResponseCarrierPlanBenefit createArrayOfGetPlansbyRxResponseCarrierPlanBenefit() {
        return new ArrayOfGetPlansbyRxResponseCarrierPlanBenefit();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteRequestMember }
     * 
     */
    public ArrayOfGetIfpQuoteRequestMember createArrayOfGetIfpQuoteRequestMember() {
        return new ArrayOfGetIfpQuoteRequestMember();
    }

    /**
     * Create an instance of {@link GroupEmployeeMember }
     * 
     */
    public GroupEmployeeMember createGroupEmployeeMember() {
        return new GroupEmployeeMember();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponsePlanDetailBenefit }
     * 
     */
    public ArrayOfGetIfpQuoteResponsePlanDetailBenefit createArrayOfGetIfpQuoteResponsePlanDetailBenefit() {
        return new ArrayOfGetIfpQuoteResponsePlanDetailBenefit();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan }
     * 
     */
    public ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan createArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan() {
        return new ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseQuote }
     * 
     */
    public GetIfpQuoteResponseQuote createGetIfpQuoteResponseQuote() {
        return new GetIfpQuoteResponseQuote();
    }

    /**
     * Create an instance of {@link IfpShoppingCartsBase }
     * 
     */
    public IfpShoppingCartsBase createIfpShoppingCartsBase() {
        return new IfpShoppingCartsBase();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseQuote }
     * 
     */
    public GetGroupQuoteResponseQuote createGetGroupQuoteResponseQuote() {
        return new GetGroupQuoteResponseQuote();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseQuoteCarrierRatePlanRate }
     * 
     */
    public GetIfpQuoteResponseQuoteCarrierRatePlanRate createGetIfpQuoteResponseQuoteCarrierRatePlanRate() {
        return new GetIfpQuoteResponseQuoteCarrierRatePlanRate();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponseQuoteCarrierRate }
     * 
     */
    public ArrayOfGetGroupQuoteResponseQuoteCarrierRate createArrayOfGetGroupQuoteResponseQuoteCarrierRate() {
        return new ArrayOfGetGroupQuoteResponseQuoteCarrierRate();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier }
     * 
     */
    public GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier() {
        return new GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride }
     * 
     */
    public ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride createArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride() {
        return new ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxAddOn }
     * 
     */
    public ArrayOfGetPlansbyRxAddOn createArrayOfGetPlansbyRxAddOn() {
        return new ArrayOfGetPlansbyRxAddOn();
    }

    /**
     * Create an instance of {@link FormularyLookupRequestInput }
     * 
     */
    public FormularyLookupRequestInput createFormularyLookupRequestInput() {
        return new FormularyLookupRequestInput();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseMemberRate }
     * 
     */
    public GetGroupQuoteResponseMemberRate createGetGroupQuoteResponseMemberRate() {
        return new GetGroupQuoteResponseMemberRate();
    }

    /**
     * Create an instance of {@link ViewPointBase }
     * 
     */
    public ViewPointBase createViewPointBase() {
        return new ViewPointBase();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember }
     * 
     */
    public ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember createArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember() {
        return new ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember();
    }

    /**
     * Create an instance of {@link ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange }
     * 
     */
    public ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange createArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange() {
        return new ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }
     * 
     */
    public GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug() {
        return new GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponsePlanDetail }
     * 
     */
    public GetGroupQuoteResponsePlanDetail createGetGroupQuoteResponsePlanDetail() {
        return new GetGroupQuoteResponsePlanDetail();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseQuoteEmployeePlanCarrierRate }
     * 
     */
    public GetGroupQuoteResponseQuoteEmployeePlanCarrierRate createGetGroupQuoteResponseQuoteEmployeePlanCarrierRate() {
        return new GetGroupQuoteResponseQuoteEmployeePlanCarrierRate();
    }

    /**
     * Create an instance of {@link CountyBase }
     * 
     */
    public CountyBase createCountyBase() {
        return new CountyBase();
    }

    /**
     * Create an instance of {@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier }
     * 
     */
    public ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier createArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier() {
        return new ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier();
    }

    /**
     * Create an instance of {@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier }
     * 
     */
    public ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier createArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier() {
        return new ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponseCarrier }
     * 
     */
    public GetPlansbyRxResponseCarrier createGetPlansbyRxResponseCarrier() {
        return new GetPlansbyRxResponseCarrier();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseQuoteCarrierRateModified }
     * 
     */
    public GetIfpQuoteResponseQuoteCarrierRateModified createGetIfpQuoteResponseQuoteCarrierRateModified() {
        return new GetIfpQuoteResponseQuoteCarrierRateModified();
    }

    /**
     * Create an instance of {@link ArrayOfGroupRAFOverride }
     * 
     */
    public ArrayOfGroupRAFOverride createArrayOfGroupRAFOverride() {
        return new ArrayOfGroupRAFOverride();
    }

    /**
     * Create an instance of {@link CustomProductRatingAreaZipRange }
     * 
     */
    public CustomProductRatingAreaZipRange createCustomProductRatingAreaZipRange() {
        return new CustomProductRatingAreaZipRange();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint }
     * 
     */
    public ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint createArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint() {
        return new ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService }
     * 
     */
    public ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService createArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService() {
        return new ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier }
     * 
     */
    public ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier createArrayOfGetGroupQuoteResponseQuoteDroppedCarrier() {
        return new ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan createArrayOfGetCarriersPlansBenefitsResponseCarrierPlan() {
        return new ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteRequestPreference }
     * 
     */
    public GetCustomProductsQuoteRequestPreference createGetCustomProductsQuoteRequestPreference() {
        return new GetCustomProductsQuoteRequestPreference();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseCarrierPlan }
     * 
     */
    public GetCarriersPlansBenefitsResponseCarrierPlan createGetCarriersPlansBenefitsResponseCarrierPlan() {
        return new GetCarriersPlansBenefitsResponseCarrierPlan();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestProductChoicePlanChoices }
     * 
     */
    public GetIfpQuoteRequestProductChoicePlanChoices createGetIfpQuoteRequestProductChoicePlanChoices() {
        return new GetIfpQuoteRequestProductChoicePlanChoices();
    }

    /**
     * Create an instance of {@link CustomProductRatingAreaZipCounty }
     * 
     */
    public CustomProductRatingAreaZipCounty createCustomProductRatingAreaZipCounty() {
        return new CustomProductRatingAreaZipCounty();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestPreferenceChoiceFilter }
     * 
     */
    public GetIfpQuoteRequestPreferenceChoiceFilter createGetIfpQuoteRequestPreferenceChoiceFilter() {
        return new GetIfpQuoteRequestPreferenceChoiceFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier }
     * 
     */
    public ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier createArrayOfGetIfpQuoteResponseQuoteDroppedCarrier() {
        return new ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter }
     * 
     */
    public ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter createArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter() {
        return new ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter();
    }

    /**
     * Create an instance of {@link RAFOverrideBase }
     * 
     */
    public RAFOverrideBase createRAFOverrideBase() {
        return new RAFOverrideBase();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage }
     * 
     */
    public ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage createArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage() {
        return new ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxResponseCarrierPlan }
     * 
     */
    public ArrayOfGetPlansbyRxResponseCarrierPlan createArrayOfGetPlansbyRxResponseCarrierPlan() {
        return new ArrayOfGetPlansbyRxResponseCarrierPlan();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponseItem }
     * 
     */
    public ArrayOfGetGroupQuoteResponseItem createArrayOfGetGroupQuoteResponseItem() {
        return new ArrayOfGetGroupQuoteResponseItem();
    }

    /**
     * Create an instance of {@link DroppedPlanBase }
     * 
     */
    public DroppedPlanBase createDroppedPlanBase() {
        return new DroppedPlanBase();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService createArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService() {
        return new ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService();
    }

    /**
     * Create an instance of {@link ResponseBase }
     * 
     */
    public ResponseBase createResponseBase() {
        return new ResponseBase();
    }

    /**
     * Create an instance of {@link ItemBase }
     * 
     */
    public ItemBase createItemBase() {
        return new ItemBase();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseItem }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseItem createArrayOfGetCarriersPlansBenefitsResponseItem() {
        return new ArrayOfGetCarriersPlansBenefitsResponseItem();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug createArrayOfGetCarriersPlansBenefitsResponseDrugListDrug() {
        return new ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug();
    }

    /**
     * Create an instance of {@link FetchCustomProductsRequestInputProductFilter }
     * 
     */
    public FetchCustomProductsRequestInputProductFilter createFetchCustomProductsRequestInputProductFilter() {
        return new FetchCustomProductsRequestInputProductFilter();
    }

    /**
     * Create an instance of {@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }
     * 
     */
    public ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO createArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO() {
        return new ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestPreferencePlanFilter }
     * 
     */
    public GetGroupQuoteRequestPreferencePlanFilter createGetGroupQuoteRequestPreferencePlanFilter() {
        return new GetGroupQuoteRequestPreferencePlanFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteRequestPreferenceDrugFilter }
     * 
     */
    public ArrayOfGetIfpQuoteRequestPreferenceDrugFilter createArrayOfGetIfpQuoteRequestPreferenceDrugFilter() {
        return new ArrayOfGetIfpQuoteRequestPreferenceDrugFilter();
    }

    /**
     * Create an instance of {@link FormularyLookupResponseCarrier }
     * 
     */
    public FormularyLookupResponseCarrier createFormularyLookupResponseCarrier() {
        return new FormularyLookupResponseCarrier();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter }
     * 
     */
    public ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter createArrayOfGetGroupQuoteRequestPreferenceChoiceFilter() {
        return new ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteRequestCustomRateFactorFamily }
     * 
     */
    public GetCustomProductsQuoteRequestCustomRateFactorFamily createGetCustomProductsQuoteRequestCustomRateFactorFamily() {
        return new GetCustomProductsQuoteRequestCustomRateFactorFamily();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponseCarrierPlanPlanFormulary }
     * 
     */
    public GetPlansbyRxResponseCarrierPlanPlanFormulary createGetPlansbyRxResponseCarrierPlanPlanFormulary() {
        return new GetPlansbyRxResponseCarrierPlanPlanFormulary();
    }

    /**
     * Create an instance of {@link ContactBase }
     * 
     */
    public ContactBase createContactBase() {
        return new ContactBase();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestIfpRateFactor }
     * 
     */
    public GetIfpQuoteRequestIfpRateFactor createGetIfpQuoteRequestIfpRateFactor() {
        return new GetIfpQuoteRequestIfpRateFactor();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint }
     * 
     */
    public GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint createGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint() {
        return new GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint();
    }

    /**
     * Create an instance of {@link GroupRAFOverride }
     * 
     */
    public GroupRAFOverride createGroupRAFOverride() {
        return new GroupRAFOverride();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponsePlanDetailBenefit }
     * 
     */
    public ArrayOfGetGroupQuoteResponsePlanDetailBenefit createArrayOfGetGroupQuoteResponsePlanDetailBenefit() {
        return new ArrayOfGetGroupQuoteResponsePlanDetailBenefit();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice }
     * 
     */
    public ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice createArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice() {
        return new ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestGroupRateFactorRAFOverride }
     * 
     */
    public GetGroupQuoteRequestGroupRateFactorRAFOverride createGetGroupQuoteRequestGroupRateFactorRAFOverride() {
        return new GetGroupQuoteRequestGroupRateFactorRAFOverride();
    }

    /**
     * Create an instance of {@link FormularyLookupResponseCarrierPlanPlanFormulary }
     * 
     */
    public FormularyLookupResponseCarrierPlanPlanFormulary createFormularyLookupResponseCarrierPlanPlanFormulary() {
        return new FormularyLookupResponseCarrierPlanPlanFormulary();
    }

    /**
     * Create an instance of {@link IfpRateBase }
     * 
     */
    public IfpRateBase createIfpRateBase() {
        return new IfpRateBase();
    }

    /**
     * Create an instance of {@link LocationBase }
     * 
     */
    public LocationBase createLocationBase() {
        return new LocationBase();
    }

    /**
     * Create an instance of {@link GroupShoppingCartProductChoice }
     * 
     */
    public GroupShoppingCartProductChoice createGroupShoppingCartProductChoice() {
        return new GroupShoppingCartProductChoice();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseQuoteCarrierRate }
     * 
     */
    public GetGroupQuoteResponseQuoteCarrierRate createGetGroupQuoteResponseQuoteCarrierRate() {
        return new GetGroupQuoteResponseQuoteCarrierRate();
    }

    /**
     * Create an instance of {@link MemberRateBase }
     * 
     */
    public MemberRateBase createMemberRateBase() {
        return new MemberRateBase();
    }

    /**
     * Create an instance of {@link FamilyContactContactAttribute }
     * 
     */
    public FamilyContactContactAttribute createFamilyContactContactAttribute() {
        return new FamilyContactContactAttribute();
    }

    /**
     * Create an instance of {@link GetZipCodeInfoRequestInput }
     * 
     */
    public GetZipCodeInfoRequestInput createGetZipCodeInfoRequestInput() {
        return new GetZipCodeInfoRequestInput();
    }

    /**
     * Create an instance of {@link ListRxRequestInput }
     * 
     */
    public ListRxRequestInput createListRxRequestInput() {
        return new ListRxRequestInput();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteRequestPreferenceProductFilter }
     * 
     */
    public GetCustomProductsQuoteRequestPreferenceProductFilter createGetCustomProductsQuoteRequestPreferenceProductFilter() {
        return new GetCustomProductsQuoteRequestPreferenceProductFilter();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService }
     * 
     */
    public GetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService createGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService() {
        return new GetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService();
    }

    /**
     * Create an instance of {@link ArrayOfGroupEmployee }
     * 
     */
    public ArrayOfGroupEmployee createArrayOfGroupEmployee() {
        return new ArrayOfGroupEmployee();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit createArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit() {
        return new ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan }
     * 
     */
    public ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan createArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan() {
        return new ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService }
     * 
     */
    public GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService createGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService() {
        return new GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService();
    }

    /**
     * Create an instance of {@link ArrayOfFormularyLookupRequestPlanFilter }
     * 
     */
    public ArrayOfFormularyLookupRequestPlanFilter createArrayOfFormularyLookupRequestPlanFilter() {
        return new ArrayOfFormularyLookupRequestPlanFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier }
     * 
     */
    public ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier createArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier() {
        return new ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance }
     * 
     */
    public GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance createGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance() {
        return new GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate }
     * 
     */
    public GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate() {
        return new GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate();
    }

    /**
     * Create an instance of {@link ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily }
     * 
     */
    public ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily createArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily() {
        return new ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily();
    }

    /**
     * Create an instance of {@link GetZipCodeInfoRequestAccessKey }
     * 
     */
    public GetZipCodeInfoRequestAccessKey createGetZipCodeInfoRequestAccessKey() {
        return new GetZipCodeInfoRequestAccessKey();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseCarrierDetail }
     * 
     */
    public GetGroupQuoteResponseCarrierDetail createGetGroupQuoteResponseCarrierDetail() {
        return new GetGroupQuoteResponseCarrierDetail();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint }
     * 
     */
    public ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint createArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint() {
        return new ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint();
    }

    /**
     * Create an instance of {@link ArrayOfCustomProductRatingArea }
     * 
     */
    public ArrayOfCustomProductRatingArea createArrayOfCustomProductRatingArea() {
        return new ArrayOfCustomProductRatingArea();
    }

    /**
     * Create an instance of {@link ArrayOfInsuranceType }
     * 
     */
    public ArrayOfInsuranceType createArrayOfInsuranceType() {
        return new ArrayOfInsuranceType();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseFormularyDrugTierTierType }
     * 
     */
    public GetCarriersPlansBenefitsResponseFormularyDrugTierTierType createGetCarriersPlansBenefitsResponseFormularyDrugTierTierType() {
        return new GetCarriersPlansBenefitsResponseFormularyDrugTierTierType();
    }

    /**
     * Create an instance of {@link IfpShoppingCart }
     * 
     */
    public IfpShoppingCart createIfpShoppingCart() {
        return new IfpShoppingCart();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestGroupRateFactorScenario }
     * 
     */
    public ArrayOfGetGroupQuoteRequestGroupRateFactorScenario createArrayOfGetGroupQuoteRequestGroupRateFactorScenario() {
        return new ArrayOfGetGroupQuoteRequestGroupRateFactorScenario();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseContactContactPoint }
     * 
     */
    public GetCarriersPlansBenefitsResponseContactContactPoint createGetCarriersPlansBenefitsResponseContactContactPoint() {
        return new GetCarriersPlansBenefitsResponseContactContactPoint();
    }

    /**
     * Create an instance of {@link GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan }
     * 
     */
    public GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan createGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan() {
        return new GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan();
    }

    /**
     * Create an instance of {@link DroppedCarrierBase }
     * 
     */
    public DroppedCarrierBase createDroppedCarrierBase() {
        return new DroppedCarrierBase();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteResponseQuote }
     * 
     */
    public GetCustomProductsQuoteResponseQuote createGetCustomProductsQuoteResponseQuote() {
        return new GetCustomProductsQuoteResponseQuote();
    }

    /**
     * Create an instance of {@link ArrayOfCustomProductPlanRawRate }
     * 
     */
    public ArrayOfCustomProductPlanRawRate createArrayOfCustomProductPlanRawRate() {
        return new ArrayOfCustomProductPlanRawRate();
    }

    /**
     * Create an instance of {@link LocationAddressBase }
     * 
     */
    public LocationAddressBase createLocationAddressBase() {
        return new LocationAddressBase();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }
     * 
     */
    public GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO() {
        return new GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO();
    }

    /**
     * Create an instance of {@link ArrayOfGroupShoppingCartProductChoice }
     * 
     */
    public ArrayOfGroupShoppingCartProductChoice createArrayOfGroupShoppingCartProductChoice() {
        return new ArrayOfGroupShoppingCartProductChoice();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestPreferenceDrugFilter }
     * 
     */
    public GetIfpQuoteRequestPreferenceDrugFilter createGetIfpQuoteRequestPreferenceDrugFilter() {
        return new GetIfpQuoteRequestPreferenceDrugFilter();
    }

    /**
     * Create an instance of {@link ArrayOfCustomProductRatingAreaZipCounty }
     * 
     */
    public ArrayOfCustomProductRatingAreaZipCounty createArrayOfCustomProductRatingAreaZipCounty() {
        return new ArrayOfCustomProductRatingAreaZipCounty();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService }
     * 
     */
    public ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService createArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService() {
        return new ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService();
    }

    /**
     * Create an instance of {@link PlanDetailsBase }
     * 
     */
    public PlanDetailsBase createPlanDetailsBase() {
        return new PlanDetailsBase();
    }

    /**
     * Create an instance of {@link RateBase }
     * 
     */
    public RateBase createRateBase() {
        return new RateBase();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseFormulary }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseFormulary createArrayOfGetCarriersPlansBenefitsResponseFormulary() {
        return new ArrayOfGetCarriersPlansBenefitsResponseFormulary();
    }

    /**
     * Create an instance of {@link ChoiceFilterBase }
     * 
     */
    public ChoiceFilterBase createChoiceFilterBase() {
        return new ChoiceFilterBase();
    }

    /**
     * Create an instance of {@link ArrayOfListRxResponseDrug }
     * 
     */
    public ArrayOfListRxResponseDrug createArrayOfListRxResponseDrug() {
        return new ArrayOfListRxResponseDrug();
    }

    /**
     * Create an instance of {@link GroupContact }
     * 
     */
    public GroupContact createGroupContact() {
        return new GroupContact();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter }
     * 
     */
    public GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter createGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter() {
        return new GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier }
     * 
     */
    public ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier createArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier() {
        return new ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier();
    }

    /**
     * Create an instance of {@link ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember }
     * 
     */
    public ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember createArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember() {
        return new ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember();
    }

    /**
     * Create an instance of {@link SubmitCustomProductsRequestSetting }
     * 
     */
    public SubmitCustomProductsRequestSetting createSubmitCustomProductsRequestSetting() {
        return new SubmitCustomProductsRequestSetting();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseQuoteEmployeePlan }
     * 
     */
    public GetGroupQuoteResponseQuoteEmployeePlan createGetGroupQuoteResponseQuoteEmployeePlan() {
        return new GetGroupQuoteResponseQuoteEmployeePlan();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate }
     * 
     */
    public ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate createArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate() {
        return new ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate();
    }

    /**
     * Create an instance of {@link CustomProductRatingAreaCounty }
     * 
     */
    public CustomProductRatingAreaCounty createCustomProductRatingAreaCounty() {
        return new CustomProductRatingAreaCounty();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestGroupRateFactorScenario }
     * 
     */
    public GetGroupQuoteRequestGroupRateFactorScenario createGetGroupQuoteRequestGroupRateFactorScenario() {
        return new GetGroupQuoteRequestGroupRateFactorScenario();
    }

    /**
     * Create an instance of {@link AttributeBase }
     * 
     */
    public AttributeBase createAttributeBase() {
        return new AttributeBase();
    }

    /**
     * Create an instance of {@link IfpShoppingCartProductChoicePlanChoices }
     * 
     */
    public IfpShoppingCartProductChoicePlanChoices createIfpShoppingCartProductChoicePlanChoices() {
        return new IfpShoppingCartProductChoicePlanChoices();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestPreferencePlanFilter }
     * 
     */
    public ArrayOfGetGroupQuoteRequestPreferencePlanFilter createArrayOfGetGroupQuoteRequestPreferencePlanFilter() {
        return new ArrayOfGetGroupQuoteRequestPreferencePlanFilter();
    }

    /**
     * Create an instance of {@link Family }
     * 
     */
    public Family createFamily() {
        return new Family();
    }

    /**
     * Create an instance of {@link SubmitCustomProductsRequestAccessKey }
     * 
     */
    public SubmitCustomProductsRequestAccessKey createSubmitCustomProductsRequestAccessKey() {
        return new SubmitCustomProductsRequestAccessKey();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint }
     * 
     */
    public GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint createGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint() {
        return new GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint();
    }

    /**
     * Create an instance of {@link ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty }
     * 
     */
    public ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty createArrayOfGetZipCodeInfoResponseZipCodeInfoCounty() {
        return new ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty();
    }

    /**
     * Create an instance of {@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote }
     * 
     */
    public ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote createArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote() {
        return new ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteRequestPreferencePlanFilter }
     * 
     */
    public ArrayOfGetIfpQuoteRequestPreferencePlanFilter createArrayOfGetIfpQuoteRequestPreferencePlanFilter() {
        return new ArrayOfGetIfpQuoteRequestPreferencePlanFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan }
     * 
     */
    public ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan createArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan() {
        return new ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan();
    }

    /**
     * Create an instance of {@link BenefitBase }
     * 
     */
    public BenefitBase createBenefitBase() {
        return new BenefitBase();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseFormularyDrugTier }
     * 
     */
    public GetCarriersPlansBenefitsResponseFormularyDrugTier createGetCarriersPlansBenefitsResponseFormularyDrugTier() {
        return new GetCarriersPlansBenefitsResponseFormularyDrugTier();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteResponseQuoteFamilyQuote }
     * 
     */
    public GetCustomProductsQuoteResponseQuoteFamilyQuote createGetCustomProductsQuoteResponseQuoteFamilyQuote() {
        return new GetCustomProductsQuoteResponseQuoteFamilyQuote();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier }
     * 
     */
    public ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier createArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier() {
        return new ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier();
    }

    /**
     * Create an instance of {@link CustomProductPlan }
     * 
     */
    public CustomProductPlan createCustomProductPlan() {
        return new CustomProductPlan();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsAddOn }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsAddOn createArrayOfGetCarriersPlansBenefitsAddOn() {
        return new ArrayOfGetCarriersPlansBenefitsAddOn();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponsePlanDetail }
     * 
     */
    public GetIfpQuoteResponsePlanDetail createGetIfpQuoteResponsePlanDetail() {
        return new GetIfpQuoteResponsePlanDetail();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlan }
     * 
     */
    public ArrayOfGetGroupQuoteResponseQuoteEmployeePlan createArrayOfGetGroupQuoteResponseQuoteEmployeePlan() {
        return new ArrayOfGetGroupQuoteResponseQuoteEmployeePlan();
    }

    /**
     * Create an instance of {@link CustomProductPlanBusinessRule }
     * 
     */
    public CustomProductPlanBusinessRule createCustomProductPlanBusinessRule() {
        return new CustomProductPlanBusinessRule();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter createArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter() {
        return new ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponseQuoteCarrierRate }
     * 
     */
    public ArrayOfGetIfpQuoteResponseQuoteCarrierRate createArrayOfGetIfpQuoteResponseQuoteCarrierRate() {
        return new ArrayOfGetIfpQuoteResponseQuoteCarrierRate();
    }

    /**
     * Create an instance of {@link ArrayOfCustomProductPlan }
     * 
     */
    public ArrayOfCustomProductPlan createArrayOfCustomProductPlan() {
        return new ArrayOfCustomProductPlan();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteRequestCustomRateFactorFamilyMember }
     * 
     */
    public GetCustomProductsQuoteRequestCustomRateFactorFamilyMember createGetCustomProductsQuoteRequestCustomRateFactorFamilyMember() {
        return new GetCustomProductsQuoteRequestCustomRateFactorFamilyMember();
    }

    /**
     * Create an instance of {@link ArrayOfCustomProductRatingAreaCounty }
     * 
     */
    public ArrayOfCustomProductRatingAreaCounty createArrayOfCustomProductRatingAreaCounty() {
        return new ArrayOfCustomProductRatingAreaCounty();
    }

    /**
     * Create an instance of {@link ArrayOfGetCountiesResponseCounty }
     * 
     */
    public ArrayOfGetCountiesResponseCounty createArrayOfGetCountiesResponseCounty() {
        return new ArrayOfGetCountiesResponseCounty();
    }

    /**
     * Create an instance of {@link ListRxResponseDrug }
     * 
     */
    public ListRxResponseDrug createListRxResponseDrug() {
        return new ListRxResponseDrug();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestPreferencePlanFilter }
     * 
     */
    public GetIfpQuoteRequestPreferencePlanFilter createGetIfpQuoteRequestPreferencePlanFilter() {
        return new GetIfpQuoteRequestPreferencePlanFilter();
    }

    /**
     * Create an instance of {@link FormularyLookupResponseCarrierPlan }
     * 
     */
    public FormularyLookupResponseCarrierPlan createFormularyLookupResponseCarrierPlan() {
        return new FormularyLookupResponseCarrierPlan();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseCarrier }
     * 
     */
    public GetCarriersPlansBenefitsResponseCarrier createGetCarriersPlansBenefitsResponseCarrier() {
        return new GetCarriersPlansBenefitsResponseCarrier();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices }
     * 
     */
    public GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices createGetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices() {
        return new GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices();
    }

    /**
     * Create an instance of {@link GetCountiesResponseCounty }
     * 
     */
    public GetCountiesResponseCounty createGetCountiesResponseCounty() {
        return new GetCountiesResponseCounty();
    }

    /**
     * Create an instance of {@link GetCarrierLoadHistoryRequestInput }
     * 
     */
    public GetCarrierLoadHistoryRequestInput createGetCarrierLoadHistoryRequestInput() {
        return new GetCarrierLoadHistoryRequestInput();
    }

    /**
     * Create an instance of {@link ContactPointBase }
     * 
     */
    public ContactPointBase createContactPointBase() {
        return new ContactPointBase();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseItem }
     * 
     */
    public GetGroupQuoteResponseItem createGetGroupQuoteResponseItem() {
        return new GetGroupQuoteResponseItem();
    }

    /**
     * Create an instance of {@link GetPlansbyRxRequestAccessKey }
     * 
     */
    public GetPlansbyRxRequestAccessKey createGetPlansbyRxRequestAccessKey() {
        return new GetPlansbyRxRequestAccessKey();
    }

    /**
     * Create an instance of {@link IfpShoppingCartProductChoice }
     * 
     */
    public IfpShoppingCartProductChoice createIfpShoppingCartProductChoice() {
        return new IfpShoppingCartProductChoice();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseQuoteDroppedCarrier }
     * 
     */
    public GetIfpQuoteResponseQuoteDroppedCarrier createGetIfpQuoteResponseQuoteDroppedCarrier() {
        return new GetIfpQuoteResponseQuoteDroppedCarrier();
    }

    /**
     * Create an instance of {@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage }
     * 
     */
    public ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage createArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage() {
        return new ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuoteRequestCustomRateFactor }
     * 
     */
    public GetCustomProductsQuoteRequestCustomRateFactor createGetCustomProductsQuoteRequestCustomRateFactor() {
        return new GetCustomProductsQuoteRequestCustomRateFactor();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteRequestPreferenceVersionFilter }
     * 
     */
    public ArrayOfGetIfpQuoteRequestPreferenceVersionFilter createArrayOfGetIfpQuoteRequestPreferenceVersionFilter() {
        return new ArrayOfGetIfpQuoteRequestPreferenceVersionFilter();
    }

    /**
     * Create an instance of {@link CommandFault }
     * 
     */
    public CommandFault createCommandFault() {
        return new CommandFault();
    }

    /**
     * Create an instance of {@link RxCUIsBase }
     * 
     */
    public RxCUIsBase createRxCUIsBase() {
        return new RxCUIsBase();
    }

    /**
     * Create an instance of {@link GetZipCodeInfoResponseZipCodeInfoCounty }
     * 
     */
    public GetZipCodeInfoResponseZipCodeInfoCounty createGetZipCodeInfoResponseZipCodeInfoCounty() {
        return new GetZipCodeInfoResponseZipCodeInfoCounty();
    }

    /**
     * Create an instance of {@link GetCountiesRequestInput }
     * 
     */
    public GetCountiesRequestInput createGetCountiesRequestInput() {
        return new GetCountiesRequestInput();
    }

    /**
     * Create an instance of {@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate }
     * 
     */
    public ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate createArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate() {
        return new ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance createArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance() {
        return new ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestPreferenceVersionFilter }
     * 
     */
    public GetGroupQuoteRequestPreferenceVersionFilter createGetGroupQuoteRequestPreferenceVersionFilter() {
        return new GetGroupQuoteRequestPreferenceVersionFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseDrugList }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseDrugList createArrayOfGetCarriersPlansBenefitsResponseDrugList() {
        return new ArrayOfGetCarriersPlansBenefitsResponseDrugList();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsRequestBenefitFilter }
     * 
     */
    public GetCarriersPlansBenefitsRequestBenefitFilter createGetCarriersPlansBenefitsRequestBenefitFilter() {
        return new GetCarriersPlansBenefitsRequestBenefitFilter();
    }

    /**
     * Create an instance of {@link GetCarrierLoadHistoryRequestAccessKey }
     * 
     */
    public GetCarrierLoadHistoryRequestAccessKey createGetCarrierLoadHistoryRequestAccessKey() {
        return new GetCarrierLoadHistoryRequestAccessKey();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseQuoteCarrierRateMemberRate }
     * 
     */
    public GetIfpQuoteResponseQuoteCarrierRateMemberRate createGetIfpQuoteResponseQuoteCarrierRateMemberRate() {
        return new GetIfpQuoteResponseQuoteCarrierRateMemberRate();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService }
     * 
     */
    public GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService createGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService() {
        return new GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestPreference }
     * 
     */
    public GetGroupQuoteRequestPreference createGetGroupQuoteRequestPreference() {
        return new GetGroupQuoteRequestPreference();
    }

    /**
     * Create an instance of {@link ListRxRequestPlanFilter }
     * 
     */
    public ListRxRequestPlanFilter createListRxRequestPlanFilter() {
        return new ListRxRequestPlanFilter();
    }

    /**
     * Create an instance of {@link GetZipCodeInfoResponseZipCodeInfo }
     * 
     */
    public GetZipCodeInfoResponseZipCodeInfo createGetZipCodeInfoResponseZipCodeInfo() {
        return new GetZipCodeInfoResponseZipCodeInfo();
    }

    /**
     * Create an instance of {@link ArrayOfFamilyContactContactAttribute }
     * 
     */
    public ArrayOfFamilyContactContactAttribute createArrayOfFamilyContactContactAttribute() {
        return new ArrayOfFamilyContactContactAttribute();
    }

    /**
     * Create an instance of {@link CustomProductRatingArea }
     * 
     */
    public CustomProductRatingArea createCustomProductRatingArea() {
        return new CustomProductRatingArea();
    }

    /**
     * Create an instance of {@link IfpPlanChoiceBase }
     * 
     */
    public IfpPlanChoiceBase createIfpPlanChoiceBase() {
        return new IfpPlanChoiceBase();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint }
     * 
     */
    public GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint createGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint() {
        return new GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint }
     * 
     */
    public GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint createGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint() {
        return new GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint();
    }

    /**
     * Create an instance of {@link PlanBase }
     * 
     */
    public PlanBase createPlanBase() {
        return new PlanBase();
    }

    /**
     * Create an instance of {@link ArrayOfGetZipCodeInfoResponseZipCodeInfoCity }
     * 
     */
    public ArrayOfGetZipCodeInfoResponseZipCodeInfoCity createArrayOfGetZipCodeInfoResponseZipCodeInfoCity() {
        return new ArrayOfGetZipCodeInfoResponseZipCodeInfoCity();
    }

    /**
     * Create an instance of {@link Group }
     * 
     */
    public Group createGroup() {
        return new Group();
    }

    /**
     * Create an instance of {@link PlanRateBase }
     * 
     */
    public PlanRateBase createPlanRateBase() {
        return new PlanRateBase();
    }

    /**
     * Create an instance of {@link ArrayOfGetZipCodeInfoResponseZipCodeInfo }
     * 
     */
    public ArrayOfGetZipCodeInfoResponseZipCodeInfo createArrayOfGetZipCodeInfoResponseZipCodeInfo() {
        return new ArrayOfGetZipCodeInfoResponseZipCodeInfo();
    }

    /**
     * Create an instance of {@link ArrayOfCustomProductRatingAreaZipRange }
     * 
     */
    public ArrayOfCustomProductRatingAreaZipRange createArrayOfCustomProductRatingAreaZipRange() {
        return new ArrayOfCustomProductRatingAreaZipRange();
    }

    /**
     * Create an instance of {@link GetPlansbyRxItem }
     * 
     */
    public GetPlansbyRxItem createGetPlansbyRxItem() {
        return new GetPlansbyRxItem();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseCarrierPlanBenefit }
     * 
     */
    public GetCarriersPlansBenefitsResponseCarrierPlanBenefit createGetCarriersPlansBenefitsResponseCarrierPlanBenefit() {
        return new GetCarriersPlansBenefitsResponseCarrierPlanBenefit();
    }

    /**
     * Create an instance of {@link ArrayOfFormularyLookupResponseCarrierPlan }
     * 
     */
    public ArrayOfFormularyLookupResponseCarrierPlan createArrayOfFormularyLookupResponseCarrierPlan() {
        return new ArrayOfFormularyLookupResponseCarrierPlan();
    }

    /**
     * Create an instance of {@link GroupEmployee }
     * 
     */
    public GroupEmployee createGroupEmployee() {
        return new GroupEmployee();
    }

    /**
     * Create an instance of {@link MemberBase }
     * 
     */
    public MemberBase createMemberBase() {
        return new MemberBase();
    }

    /**
     * Create an instance of {@link ArrayOfFetchCustomProductsRequestInputProductFilter }
     * 
     */
    public ArrayOfFetchCustomProductsRequestInputProductFilter createArrayOfFetchCustomProductsRequestInputProductFilter() {
        return new ArrayOfFetchCustomProductsRequestInputProductFilter();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseLocation }
     * 
     */
    public GetCarriersPlansBenefitsResponseLocation createGetCarriersPlansBenefitsResponseLocation() {
        return new GetCarriersPlansBenefitsResponseLocation();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestProductChoice }
     * 
     */
    public GetIfpQuoteRequestProductChoice createGetIfpQuoteRequestProductChoice() {
        return new GetIfpQuoteRequestProductChoice();
    }

    /**
     * Create an instance of {@link GetZipCodeInfoRequestInputIsQuotableFilters }
     * 
     */
    public GetZipCodeInfoRequestInputIsQuotableFilters createGetZipCodeInfoRequestInputIsQuotableFilters() {
        return new GetZipCodeInfoRequestInputIsQuotableFilters();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponseItem }
     * 
     */
    public ArrayOfGetIfpQuoteResponseItem createArrayOfGetIfpQuoteResponseItem() {
        return new ArrayOfGetIfpQuoteResponseItem();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponseQuoteMemberPlan }
     * 
     */
    public ArrayOfGetIfpQuoteResponseQuoteMemberPlan createArrayOfGetIfpQuoteResponseQuoteMemberPlan() {
        return new ArrayOfGetIfpQuoteResponseQuoteMemberPlan();
    }

    /**
     * Create an instance of {@link ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter }
     * 
     */
    public ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter createArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter() {
        return new ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseDrugListDrug }
     * 
     */
    public GetCarriersPlansBenefitsResponseDrugListDrug createGetCarriersPlansBenefitsResponseDrugListDrug() {
        return new GetCarriersPlansBenefitsResponseDrugListDrug();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseCarrierPlanPlanLink }
     * 
     */
    public GetCarriersPlansBenefitsResponseCarrierPlanPlanLink createGetCarriersPlansBenefitsResponseCarrierPlanPlanLink() {
        return new GetCarriersPlansBenefitsResponseCarrierPlanPlanLink();
    }

    /**
     * Create an instance of {@link FamilyMember }
     * 
     */
    public FamilyMember createFamilyMember() {
        return new FamilyMember();
    }

    /**
     * Create an instance of {@link CoverageBase }
     * 
     */
    public CoverageBase createCoverageBase() {
        return new CoverageBase();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseItem }
     * 
     */
    public GetCarriersPlansBenefitsResponseItem createGetCarriersPlansBenefitsResponseItem() {
        return new GetCarriersPlansBenefitsResponseItem();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan }
     * 
     */
    public GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan createGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan() {
        return new GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan();
    }

    /**
     * Create an instance of {@link ArrayOfCustomProductPlanBusinessRuleAllowedRelation }
     * 
     */
    public ArrayOfCustomProductPlanBusinessRuleAllowedRelation createArrayOfCustomProductPlanBusinessRuleAllowedRelation() {
        return new ArrayOfCustomProductPlanBusinessRuleAllowedRelation();
    }

    /**
     * Create an instance of {@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }
     * 
     */
    public ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug createArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug() {
        return new ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug();
    }

    /**
     * Create an instance of {@link CustomProduct }
     * 
     */
    public CustomProduct createCustomProduct() {
        return new CustomProduct();
    }

    /**
     * Create an instance of {@link ArrayOfCustomProductPlanLookupSequence }
     * 
     */
    public ArrayOfCustomProductPlanLookupSequence createArrayOfCustomProductPlanLookupSequence() {
        return new ArrayOfCustomProductPlanLookupSequence();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter }
     * 
     */
    public ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter createArrayOfGetIfpQuoteRequestPreferenceBenefitFilter() {
        return new ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter();
    }

    /**
     * Create an instance of {@link GetGroupQuoteResponseQuoteEmployeePlanCarrier }
     * 
     */
    public GetGroupQuoteResponseQuoteEmployeePlanCarrier createGetGroupQuoteResponseQuoteEmployeePlanCarrier() {
        return new GetGroupQuoteResponseQuoteEmployeePlanCarrier();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage createArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage() {
        return new ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter }
     * 
     */
    public GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter createGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter() {
        return new GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter();
    }

    /**
     * Create an instance of {@link CarrierDetailsBase }
     * 
     */
    public CarrierDetailsBase createCarrierDetailsBase() {
        return new CarrierDetailsBase();
    }

    /**
     * Create an instance of {@link ListRxRequestAccessKey }
     * 
     */
    public ListRxRequestAccessKey createListRxRequestAccessKey() {
        return new ListRxRequestAccessKey();
    }

    /**
     * Create an instance of {@link GetGroupQuoteRequestPreferenceBenefitFilter }
     * 
     */
    public GetGroupQuoteRequestPreferenceBenefitFilter createGetGroupQuoteRequestPreferenceBenefitFilter() {
        return new GetGroupQuoteRequestPreferenceBenefitFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified }
     * 
     */
    public ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified createArrayOfGetIfpQuoteResponseQuoteCarrierRateModified() {
        return new ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier createArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier() {
        return new ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter }
     * 
     */
    public ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter createArrayOfGetGroupQuoteRequestPreferenceBenefitFilter() {
        return new ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseQuoteCarrier }
     * 
     */
    public GetIfpQuoteResponseQuoteCarrier createGetIfpQuoteResponseQuoteCarrier() {
        return new GetIfpQuoteResponseQuoteCarrier();
    }

    /**
     * Create an instance of {@link CustomProductPlanLookupSequence }
     * 
     */
    public CustomProductPlanLookupSequence createCustomProductPlanLookupSequence() {
        return new CustomProductPlanLookupSequence();
    }

    /**
     * Create an instance of {@link ArrayOfGroupContactContactAttribute }
     * 
     */
    public ArrayOfGroupContactContactAttribute createArrayOfGroupContactContactAttribute() {
        return new ArrayOfGroupContactContactAttribute();
    }

    /**
     * Create an instance of {@link CustomProductPlanBusinessRuleCappingRule }
     * 
     */
    public CustomProductPlanBusinessRuleCappingRule createCustomProductPlanBusinessRuleCappingRule() {
        return new CustomProductPlanBusinessRuleCappingRule();
    }

    /**
     * Create an instance of {@link IfpShoppingCartProductChoicePlanChoicesStatusChange }
     * 
     */
    public IfpShoppingCartProductChoicePlanChoicesStatusChange createIfpShoppingCartProductChoicePlanChoicesStatusChange() {
        return new IfpShoppingCartProductChoicePlanChoicesStatusChange();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestIfpRateKey }
     * 
     */
    public GetIfpQuoteRequestIfpRateKey createGetIfpQuoteRequestIfpRateKey() {
        return new GetIfpQuoteRequestIfpRateKey();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteAddOn }
     * 
     */
    public ArrayOfGetGroupQuoteAddOn createArrayOfGetGroupQuoteAddOn() {
        return new ArrayOfGetGroupQuoteAddOn();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier }
     * 
     */
    public GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier() {
        return new GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage }
     * 
     */
    public ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage createArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage() {
        return new ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage();
    }

    /**
     * Create an instance of {@link AccessKeyBase }
     * 
     */
    public AccessKeyBase createAccessKeyBase() {
        return new AccessKeyBase();
    }

    /**
     * Create an instance of {@link GetCarrierLoadHistoryResponseLoadEventCarrier }
     * 
     */
    public GetCarrierLoadHistoryResponseLoadEventCarrier createGetCarrierLoadHistoryResponseLoadEventCarrier() {
        return new GetCarrierLoadHistoryResponseLoadEventCarrier();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint createArrayOfGetCarriersPlansBenefitsResponseContactContactPoint() {
        return new ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan }
     * 
     */
    public ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan createArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan() {
        return new ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan();
    }

    /**
     * Create an instance of {@link GetPlansbyRxResponseCarrierPlan }
     * 
     */
    public GetPlansbyRxResponseCarrierPlan createGetPlansbyRxResponseCarrierPlan() {
        return new GetPlansbyRxResponseCarrierPlan();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter createArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter() {
        return new ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter();
    }

    /**
     * Create an instance of {@link ArrayOfListRxRequestPlanFilter }
     * 
     */
    public ArrayOfListRxRequestPlanFilter createArrayOfListRxRequestPlanFilter() {
        return new ArrayOfListRxRequestPlanFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter }
     * 
     */
    public ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter createArrayOfGetIfpQuoteRequestPreferenceChoiceFilter() {
        return new ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseLocation }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseLocation createArrayOfGetCarriersPlansBenefitsResponseLocation() {
        return new ArrayOfGetCarriersPlansBenefitsResponseLocation();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestPreferenceBenefitFilter }
     * 
     */
    public GetIfpQuoteRequestPreferenceBenefitFilter createGetIfpQuoteRequestPreferenceBenefitFilter() {
        return new GetIfpQuoteRequestPreferenceBenefitFilter();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarriersPlansBenefitsResponseCarrier }
     * 
     */
    public ArrayOfGetCarriersPlansBenefitsResponseCarrier createArrayOfGetCarriersPlansBenefitsResponseCarrier() {
        return new ArrayOfGetCarriersPlansBenefitsResponseCarrier();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteAddOn }
     * 
     */
    public ArrayOfGetIfpQuoteAddOn createArrayOfGetIfpQuoteAddOn() {
        return new ArrayOfGetIfpQuoteAddOn();
    }

    /**
     * Create an instance of {@link GetIfpQuoteRequestPreferenceVersionFilter }
     * 
     */
    public GetIfpQuoteRequestPreferenceVersionFilter createGetIfpQuoteRequestPreferenceVersionFilter() {
        return new GetIfpQuoteRequestPreferenceVersionFilter();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage }
     * 
     */
    public GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage createGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage() {
        return new GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsRequestCarrierPlanFilter }
     * 
     */
    public GetCarriersPlansBenefitsRequestCarrierPlanFilter createGetCarriersPlansBenefitsRequestCarrierPlanFilter() {
        return new GetCarriersPlansBenefitsRequestCarrierPlanFilter();
    }

    /**
     * Create an instance of {@link FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }
     * 
     */
    public FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO() {
        return new FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO();
    }

    /**
     * Create an instance of {@link FormularyLookupRequestDrug }
     * 
     */
    public FormularyLookupRequestDrug createFormularyLookupRequestDrug() {
        return new FormularyLookupRequestDrug();
    }

    /**
     * Create an instance of {@link ArrayOfGetCarrierLoadHistoryResponseLoadEvent }
     * 
     */
    public ArrayOfGetCarrierLoadHistoryResponseLoadEvent createArrayOfGetCarrierLoadHistoryResponseLoadEvent() {
        return new ArrayOfGetCarrierLoadHistoryResponseLoadEvent();
    }

    /**
     * Create an instance of {@link ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent }
     * 
     */
    public ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent createArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent() {
        return new ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent();
    }

    /**
     * Create an instance of {@link CarrierBase }
     * 
     */
    public CarrierBase createCarrierBase() {
        return new CarrierBase();
    }

    /**
     * Create an instance of {@link GetCarrierLoadHistoryResponseLoadEvent }
     * 
     */
    public GetCarrierLoadHistoryResponseLoadEvent createGetCarrierLoadHistoryResponseLoadEvent() {
        return new GetCarrierLoadHistoryResponseLoadEvent();
    }

    /**
     * Create an instance of {@link GetIfpQuoteResponseQuoteMemberPlan }
     * 
     */
    public GetIfpQuoteResponseQuoteMemberPlan createGetIfpQuoteResponseQuoteMemberPlan() {
        return new GetIfpQuoteResponseQuoteMemberPlan();
    }

    /**
     * Create an instance of {@link GroupShoppingCart }
     * 
     */
    public GroupShoppingCart createGroupShoppingCart() {
        return new GroupShoppingCart();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefitsRequestVersionFilter }
     * 
     */
    public GetCarriersPlansBenefitsRequestVersionFilter createGetCarriersPlansBenefitsRequestVersionFilter() {
        return new GetCarriersPlansBenefitsRequestVersionFilter();
    }

    /**
     * Create an instance of {@link ArrayOfIfpShoppingCartProductChoice }
     * 
     */
    public ArrayOfIfpShoppingCartProductChoice createArrayOfIfpShoppingCartProductChoice() {
        return new ArrayOfIfpShoppingCartProductChoice();
    }

    /**
     * Create an instance of {@link GetPlansbyRxRequestInput }
     * 
     */
    public GetPlansbyRxRequestInput createGetPlansbyRxRequestInput() {
        return new GetPlansbyRxRequestInput();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee }
     * 
     */
    public ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee createArrayOfGetGroupQuoteRequestGroupRateFactorEmployee() {
        return new ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee();
    }

    /**
     * Create an instance of {@link ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter }
     * 
     */
    public ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter createArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter() {
        return new ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter();
    }

    /**
     * Create an instance of {@link FormularyLookupRequestAccessKey }
     * 
     */
    public FormularyLookupRequestAccessKey createFormularyLookupRequestAccessKey() {
        return new FormularyLookupRequestAccessKey();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupLocationAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Group.LocationAddress")
    public JAXBElement<GroupLocationAddress> createGroupLocationAddress(GroupLocationAddress value) {
        return new JAXBElement<GroupLocationAddress>(_GroupLocationAddress_QNAME, GroupLocationAddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CityBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CityBase")
    public JAXBElement<CityBase> createCityBase(CityBase value) {
        return new JAXBElement<CityBase>(_CityBase_QNAME, CityBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier")
    public JAXBElement<FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier(FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier value) {
        return new JAXBElement<FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier_QNAME, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountiesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCounties.Request")
    public JAXBElement<GetCountiesRequest> createGetCountiesRequest(GetCountiesRequest value) {
        return new JAXBElement<GetCountiesRequest>(_GetCountiesRequest_QNAME, GetCountiesRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IsPreferred")
    public JAXBElement<List<String>> createIsPreferred(List<String> value) {
        return new JAXBElement<List<String>>(_IsPreferred_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestVersionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Request.VersionFilter")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestVersionFilter> createArrayOfGetCarriersPlansBenefitsRequestVersionFilter(ArrayOfGetCarriersPlansBenefitsRequestVersionFilter value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestVersionFilter>(_ArrayOfGetCarriersPlansBenefitsRequestVersionFilter_QNAME, ArrayOfGetCarriersPlansBenefitsRequestVersionFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO")
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> createArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO(ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO>(_ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupSortBy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.SortBy")
    public JAXBElement<FormularyLookupSortBy> createFormularyLookupSortBy(FormularyLookupSortBy value) {
        return new JAXBElement<FormularyLookupSortBy>(_FormularyLookupSortBy_QNAME, FormularyLookupSortBy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier")
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier> createArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier(ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier>(_ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier_QNAME, ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupShoppingCartProductChoicePlanChoices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupShoppingCart.ProductChoice.PlanChoices")
    public JAXBElement<GroupShoppingCartProductChoicePlanChoices> createGroupShoppingCartProductChoicePlanChoices(GroupShoppingCartProductChoicePlanChoices value) {
        return new JAXBElement<GroupShoppingCartProductChoicePlanChoices>(_GroupShoppingCartProductChoicePlanChoices_QNAME, GroupShoppingCartProductChoicePlanChoices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuoteRequestBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "QuoteRequestBase")
    public JAXBElement<QuoteRequestBase> createQuoteRequestBase(QuoteRequestBase value) {
        return new JAXBElement<QuoteRequestBase>(_QuoteRequestBase_QNAME, QuoteRequestBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.GroupRateFactor")
    public JAXBElement<GetGroupQuoteRequestGroupRateFactor> createGetGroupQuoteRequestGroupRateFactor(GetGroupQuoteRequestGroupRateFactor value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateFactor>(_GetGroupQuoteRequestGroupRateFactor_QNAME, GetGroupQuoteRequestGroupRateFactor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response")
    public JAXBElement<GetIfpQuoteResponse> createGetIfpQuoteResponse(GetIfpQuoteResponse value) {
        return new JAXBElement<GetIfpQuoteResponse>(_GetIfpQuoteResponse_QNAME, GetIfpQuoteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestPreferenceDrugFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.Preference.DrugFilter")
    public JAXBElement<GetGroupQuoteRequestPreferenceDrugFilter> createGetGroupQuoteRequestPreferenceDrugFilter(GetGroupQuoteRequestPreferenceDrugFilter value) {
        return new JAXBElement<GetGroupQuoteRequestPreferenceDrugFilter>(_GetGroupQuoteRequestPreferenceDrugFilter_QNAME, GetGroupQuoteRequestPreferenceDrugFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseMemberRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.MemberRate")
    public JAXBElement<ArrayOfGetGroupQuoteResponseMemberRate> createArrayOfGetGroupQuoteResponseMemberRate(ArrayOfGetGroupQuoteResponseMemberRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseMemberRate>(_ArrayOfGetGroupQuoteResponseMemberRate_QNAME, ArrayOfGetGroupQuoteResponseMemberRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListRxRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ListRx.Request")
    public JAXBElement<ListRxRequest> createListRxRequest(ListRxRequest value) {
        return new JAXBElement<ListRxRequest>(_ListRxRequest_QNAME, ListRxRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DataSet")
    public JAXBElement<List<String>> createDataSet(List<String> value) {
        return new JAXBElement<List<String>>(_DataSet_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate")
    public JAXBElement<GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate> createGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate(GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate value) {
        return new JAXBElement<GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate>(_GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate_QNAME, GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter> createArrayOfGetCarriersPlansBenefitsRequestBenefitFilter(ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter>(_ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter_QNAME, ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.Quote.CarrierRate.PlanRate.EmployeeRate")
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate> createArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate(ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate>(_ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate_QNAME, ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponsePlanDetailBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.PlanDetail.Benefit.Coverage")
    public JAXBElement<GetIfpQuoteResponsePlanDetailBenefitCoverage> createGetIfpQuoteResponsePlanDetailBenefitCoverage(GetIfpQuoteResponsePlanDetailBenefitCoverage value) {
        return new JAXBElement<GetIfpQuoteResponsePlanDetailBenefitCoverage>(_GetIfpQuoteResponsePlanDetailBenefitCoverage_QNAME, GetIfpQuoteResponsePlanDetailBenefitCoverage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Request")
    public JAXBElement<GetPlansbyRxRequest> createGetPlansbyRxRequest(GetPlansbyRxRequest value) {
        return new JAXBElement<GetPlansbyRxRequest>(_GetPlansbyRxRequest_QNAME, GetPlansbyRxRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Contact")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseContact> createArrayOfGetCarriersPlansBenefitsResponseContact(ArrayOfGetCarriersPlansBenefitsResponseContact value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseContact>(_ArrayOfGetCarriersPlansBenefitsResponseContact_QNAME, ArrayOfGetCarriersPlansBenefitsResponseContact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupRequestPlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Request.PlanFilter")
    public JAXBElement<FormularyLookupRequestPlanFilter> createFormularyLookupRequestPlanFilter(FormularyLookupRequestPlanFilter value) {
        return new JAXBElement<FormularyLookupRequestPlanFilter>(_FormularyLookupRequestPlanFilter_QNAME, FormularyLookupRequestPlanFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint.Service")
    public JAXBElement<GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService> createGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService(GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService value) {
        return new JAXBElement<GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService>(_GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService_QNAME, GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGroupEmployeeMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGroup.Employee.Member")
    public JAXBElement<ArrayOfGroupEmployeeMember> createArrayOfGroupEmployeeMember(ArrayOfGroupEmployeeMember value) {
        return new JAXBElement<ArrayOfGroupEmployeeMember>(_ArrayOfGroupEmployeeMember_QNAME, ArrayOfGroupEmployeeMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ClassCarveOut")
    public JAXBElement<List<String>> createClassCarveOut(List<String> value) {
        return new JAXBElement<List<String>>(_ClassCarveOut_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestPlanVisibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.PlanVisibility")
    public JAXBElement<GetGroupQuoteRequestPlanVisibility> createGetGroupQuoteRequestPlanVisibility(GetGroupQuoteRequestPlanVisibility value) {
        return new JAXBElement<GetGroupQuoteRequestPlanVisibility>(_GetGroupQuoteRequestPlanVisibility_QNAME, GetGroupQuoteRequestPlanVisibility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchCustomProductsRequestAccessKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FetchCustomProducts.Request.AccessKey")
    public JAXBElement<FetchCustomProductsRequestAccessKey> createFetchCustomProductsRequestAccessKey(FetchCustomProductsRequestAccessKey value) {
        return new JAXBElement<FetchCustomProductsRequestAccessKey>(_FetchCustomProductsRequestAccessKey_QNAME, FetchCustomProductsRequestAccessKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink> createArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink>(_ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequestInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Request.Input")
    public JAXBElement<GetCarriersPlansBenefitsRequestInput> createGetCarriersPlansBenefitsRequestInput(GetCarriersPlansBenefitsRequestInput value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequestInput>(_GetCarriersPlansBenefitsRequestInput_QNAME, GetCarriersPlansBenefitsRequestInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ScenarioBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ScenarioBase")
    public JAXBElement<ScenarioBase> createScenarioBase(ScenarioBase value) {
        return new JAXBElement<ScenarioBase>(_ScenarioBase_QNAME, ScenarioBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGroupScenario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGroup.Scenario")
    public JAXBElement<ArrayOfGroupScenario> createArrayOfGroupScenario(ArrayOfGroupScenario value) {
        return new JAXBElement<ArrayOfGroupScenario>(_ArrayOfGroupScenario_QNAME, ArrayOfGroupScenario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RateUnit")
    public JAXBElement<List<String>> createRateUnit(List<String> value) {
        return new JAXBElement<List<String>>(_RateUnit_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.Quote.CarrierRate.PlanRate")
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate> createArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate(ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate>(_ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate_QNAME, ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Contact")
    public JAXBElement<GetCarriersPlansBenefitsResponseContact> createGetCarriersPlansBenefitsResponseContact(GetCarriersPlansBenefitsResponseContact value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseContact>(_GetCarriersPlansBenefitsResponseContact_QNAME, GetCarriersPlansBenefitsResponseContact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponseMemberRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Response.MemberRate")
    public JAXBElement<GetCustomProductsQuoteResponseMemberRate> createGetCustomProductsQuoteResponseMemberRate(GetCustomProductsQuoteResponseMemberRate value) {
        return new JAXBElement<GetCustomProductsQuoteResponseMemberRate>(_GetCustomProductsQuoteResponseMemberRate_QNAME, GetCustomProductsQuoteResponseMemberRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.Response.Carrier")
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrier> createArrayOfGetPlansbyRxResponseCarrier(ArrayOfGetPlansbyRxResponseCarrier value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrier>(_ArrayOfGetPlansbyRxResponseCarrier_QNAME, ArrayOfGetPlansbyRxResponseCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Request.ProductChoice")
    public JAXBElement<ArrayOfGetIfpQuoteRequestProductChoice> createArrayOfGetIfpQuoteRequestProductChoice(ArrayOfGetIfpQuoteRequestProductChoice value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestProductChoice>(_ArrayOfGetIfpQuoteRequestProductChoice_QNAME, ArrayOfGetIfpQuoteRequestProductChoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseDrugAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfFormularyLookup.Response.DrugAttribute")
    public JAXBElement<ArrayOfFormularyLookupResponseDrugAttribute> createArrayOfFormularyLookupResponseDrugAttribute(ArrayOfFormularyLookupResponseDrugAttribute value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseDrugAttribute>(_ArrayOfFormularyLookupResponseDrugAttribute_QNAME, ArrayOfFormularyLookupResponseDrugAttribute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactorEmployee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.GroupRateFactor.Employee")
    public JAXBElement<GetGroupQuoteRequestGroupRateFactorEmployee> createGetGroupQuoteRequestGroupRateFactorEmployee(GetGroupQuoteRequestGroupRateFactorEmployee value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateFactorEmployee>(_GetGroupQuoteRequestGroupRateFactorEmployee_QNAME, GetGroupQuoteRequestGroupRateFactorEmployee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.Member")
    public JAXBElement<GetIfpQuoteRequestMember> createGetIfpQuoteRequestMember(GetIfpQuoteRequestMember value) {
        return new JAXBElement<GetIfpQuoteRequestMember>(_GetIfpQuoteRequestMember_QNAME, GetIfpQuoteRequestMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuoteCarrierRatePlanRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.Quote.CarrierRate.PlanRate")
    public JAXBElement<GetGroupQuoteResponseQuoteCarrierRatePlanRate> createGetGroupQuoteResponseQuoteCarrierRatePlanRate(GetGroupQuoteResponseQuoteCarrierRatePlanRate value) {
        return new JAXBElement<GetGroupQuoteResponseQuoteCarrierRatePlanRate>(_GetGroupQuoteResponseQuoteCarrierRatePlanRate_QNAME, GetGroupQuoteResponseQuoteCarrierRatePlanRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPreferenceBenchmarkRules }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.Preference.BenchmarkRules")
    public JAXBElement<GetIfpQuoteRequestPreferenceBenchmarkRules> createGetIfpQuoteRequestPreferenceBenchmarkRules(GetIfpQuoteRequestPreferenceBenchmarkRules value) {
        return new JAXBElement<GetIfpQuoteRequestPreferenceBenchmarkRules>(_GetIfpQuoteRequestPreferenceBenchmarkRules_QNAME, GetIfpQuoteRequestPreferenceBenchmarkRules.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPreference }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.Preference")
    public JAXBElement<GetIfpQuoteRequestPreference> createGetIfpQuoteRequestPreference(GetIfpQuoteRequestPreference value) {
        return new JAXBElement<GetIfpQuoteRequestPreference>(_GetIfpQuoteRequestPreference_QNAME, GetIfpQuoteRequestPreference.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupContactContactAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Group.Contact.ContactAttribute")
    public JAXBElement<GroupContactContactAttribute> createGroupContactContactAttribute(GroupContactContactAttribute value) {
        return new JAXBElement<GroupContactContactAttribute>(_GroupContactContactAttribute_QNAME, GroupContactContactAttribute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint.Service")
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService> createArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService(ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService>(_ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService_QNAME, ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupScenario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Group.Scenario")
    public JAXBElement<GroupScenario> createGroupScenario(GroupScenario value) {
        return new JAXBElement<GroupScenario>(_GroupScenario_QNAME, GroupScenario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarrierRateBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierRateBase")
    public JAXBElement<CarrierRateBase> createCarrierRateBase(CarrierRateBase value) {
        return new JAXBElement<CarrierRateBase>(_CarrierRateBase_QNAME, CarrierRateBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseDrugList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.DrugList")
    public JAXBElement<GetCarriersPlansBenefitsResponseDrugList> createGetCarriersPlansBenefitsResponseDrugList(GetCarriersPlansBenefitsResponseDrugList value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseDrugList>(_GetCarriersPlansBenefitsResponseDrugList_QNAME, GetCarriersPlansBenefitsResponseDrugList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlanVisibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanVisibility")
    public JAXBElement<PlanVisibility> createPlanVisibility(PlanVisibility value) {
        return new JAXBElement<PlanVisibility>(_PlanVisibility_QNAME, PlanVisibility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug")
    public JAXBElement<FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug(FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug value) {
        return new JAXBElement<FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug_QNAME, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlanChoiceBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanChoiceBase")
    public JAXBElement<PlanChoiceBase> createPlanChoiceBase(PlanChoiceBase value) {
        return new JAXBElement<PlanChoiceBase>(_PlanChoiceBase_QNAME, PlanChoiceBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.Item")
    public JAXBElement<GetIfpQuoteResponseItem> createGetIfpQuoteResponseItem(GetIfpQuoteResponseItem value) {
        return new JAXBElement<GetIfpQuoteResponseItem>(_GetIfpQuoteResponseItem_QNAME, GetIfpQuoteResponseItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.Preference.BenefitFilter.AttributeFilter")
    public JAXBElement<GetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter> createGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter(GetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter value) {
        return new JAXBElement<GetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter>(_GetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter_QNAME, GetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChangeType")
    public JAXBElement<List<String>> createChangeType(List<String> value) {
        return new JAXBElement<List<String>>(_ChangeType_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PharmacyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PharmacyType")
    public JAXBElement<PharmacyType> createPharmacyType(PharmacyType value) {
        return new JAXBElement<PharmacyType>(_PharmacyType_QNAME, PharmacyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Towards")
    public JAXBElement<List<String>> createTowards(List<String> value) {
        return new JAXBElement<List<String>>(_Towards_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponsePlanDetailBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.PlanDetail.Benefit")
    public JAXBElement<GetIfpQuoteResponsePlanDetailBenefit> createGetIfpQuoteResponsePlanDetailBenefit(GetIfpQuoteResponsePlanDetailBenefit value) {
        return new JAXBElement<GetIfpQuoteResponsePlanDetailBenefit>(_GetIfpQuoteResponsePlanDetailBenefit_QNAME, GetIfpQuoteResponsePlanDetailBenefit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitFamilyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitFamily.Response")
    public JAXBElement<SubmitFamilyResponse> createSubmitFamilyResponse(SubmitFamilyResponse value) {
        return new JAXBElement<SubmitFamilyResponse>(_SubmitFamilyResponse_QNAME, SubmitFamilyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.Quote.DroppedCarrier.DroppedPlan")
    public JAXBElement<GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan> createGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan(GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan value) {
        return new JAXBElement<GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan>(_GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan_QNAME, GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitGroupShoppingCartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitGroupShoppingCart.Response")
    public JAXBElement<SubmitGroupShoppingCartResponse> createSubmitGroupShoppingCartResponse(SubmitGroupShoppingCartResponse value) {
        return new JAXBElement<SubmitGroupShoppingCartResponse>(_SubmitGroupShoppingCartResponse_QNAME, SubmitGroupShoppingCartResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.Quote.Carrier")
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrier> createArrayOfGetIfpQuoteResponseQuoteCarrier(ArrayOfGetIfpQuoteResponseQuoteCarrier value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrier>(_ArrayOfGetIfpQuoteResponseQuoteCarrier_QNAME, ArrayOfGetIfpQuoteResponseQuoteCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfCustomProduct")
    public JAXBElement<ArrayOfCustomProduct> createArrayOfCustomProduct(ArrayOfCustomProduct value) {
        return new JAXBElement<ArrayOfCustomProduct>(_ArrayOfCustomProduct_QNAME, ArrayOfCustomProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceVersionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.Preference.VersionFilter")
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceVersionFilter> createArrayOfGetGroupQuoteRequestPreferenceVersionFilter(ArrayOfGetGroupQuoteRequestPreferenceVersionFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceVersionFilter>(_ArrayOfGetGroupQuoteRequestPreferenceVersionFilter_QNAME, ArrayOfGetGroupQuoteRequestPreferenceVersionFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetailBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.PlanDetail.Benefit.Coverage")
    public JAXBElement<GetGroupQuoteResponsePlanDetailBenefitCoverage> createGetGroupQuoteResponsePlanDetailBenefitCoverage(GetGroupQuoteResponsePlanDetailBenefitCoverage value) {
        return new JAXBElement<GetGroupQuoteResponsePlanDetailBenefitCoverage>(_GetGroupQuoteResponsePlanDetailBenefitCoverage_QNAME, GetGroupQuoteResponsePlanDetailBenefitCoverage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint")
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint> createArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint(ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint>(_ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint_QNAME, ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupShoppingCartsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupShoppingCartsBase")
    public JAXBElement<GroupShoppingCartsBase> createGroupShoppingCartsBase(GroupShoppingCartsBase value) {
        return new JAXBElement<GroupShoppingCartsBase>(_GroupShoppingCartsBase_QNAME, GroupShoppingCartsBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.GroupRateKey")
    public JAXBElement<GetGroupQuoteRequestGroupRateKey> createGetGroupQuoteRequestGroupRateKey(GetGroupQuoteRequestGroupRateKey value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateKey>(_GetGroupQuoteRequestGroupRateKey_QNAME, GetGroupQuoteRequestGroupRateKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LevelOfBenefitAddOns }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LevelOfBenefitAddOns")
    public JAXBElement<LevelOfBenefitAddOns> createLevelOfBenefitAddOns(LevelOfBenefitAddOns value) {
        return new JAXBElement<LevelOfBenefitAddOns>(_LevelOfBenefitAddOns_QNAME, LevelOfBenefitAddOns.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceDrugFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.Preference.DrugFilter")
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceDrugFilter> createArrayOfGetGroupQuoteRequestPreferenceDrugFilter(ArrayOfGetGroupQuoteRequestPreferenceDrugFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceDrugFilter>(_ArrayOfGetGroupQuoteRequestPreferenceDrugFilter_QNAME, ArrayOfGetGroupQuoteRequestPreferenceDrugFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseCarrierDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.CarrierDetail")
    public JAXBElement<GetIfpQuoteResponseCarrierDetail> createGetIfpQuoteResponseCarrierDetail(GetIfpQuoteResponseCarrierDetail value) {
        return new JAXBElement<GetIfpQuoteResponseCarrierDetail>(_GetIfpQuoteResponseCarrierDetail_QNAME, GetIfpQuoteResponseCarrierDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchCustomProductsRequestInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FetchCustomProducts.Request.Input")
    public JAXBElement<FetchCustomProductsRequestInput> createFetchCustomProductsRequestInput(FetchCustomProductsRequestInput value) {
        return new JAXBElement<FetchCustomProductsRequestInput>(_FetchCustomProductsRequestInput_QNAME, FetchCustomProductsRequestInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier.DroppedPlan")
    public JAXBElement<GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan> createGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan(GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan value) {
        return new JAXBElement<GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan_QNAME, GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupResponseDrugAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Response.DrugAttribute")
    public JAXBElement<FormularyLookupResponseDrugAttribute> createFormularyLookupResponseDrugAttribute(FormularyLookupResponseDrugAttribute value) {
        return new JAXBElement<FormularyLookupResponseDrugAttribute>(_FormularyLookupResponseDrugAttribute_QNAME, FormularyLookupResponseDrugAttribute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RatingMethod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RatingMethod")
    public JAXBElement<RatingMethod> createRatingMethod(RatingMethod value) {
        return new JAXBElement<RatingMethod>(_RatingMethod_QNAME, RatingMethod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductPlanRawRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct.Plan.RawRate")
    public JAXBElement<CustomProductPlanRawRate> createCustomProductPlanRawRate(CustomProductPlanRawRate value) {
        return new JAXBElement<CustomProductPlanRawRate>(_CustomProductPlanRawRate_QNAME, CustomProductPlanRawRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MemberType")
    public JAXBElement<List<String>> createMemberType(List<String> value) {
        return new JAXBElement<List<String>>(_MemberType_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.GroupRateFactor.Employee.ProductChoice")
    public JAXBElement<GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice> createGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice(GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice_QNAME, GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate")
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate> createArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate(ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate>(_ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate_QNAME, ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.Preference.BenchmarkRules.PlanComponent")
    public JAXBElement<GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent> createGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent(GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent value) {
        return new JAXBElement<GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent>(_GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent_QNAME, GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RelationshipType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RelationshipType")
    public JAXBElement<RelationshipType> createRelationshipType(RelationshipType value) {
        return new JAXBElement<RelationshipType>(_RelationshipType_QNAME, RelationshipType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseQuoteCarrierRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.Quote.CarrierRate")
    public JAXBElement<GetIfpQuoteResponseQuoteCarrierRate> createGetIfpQuoteResponseQuoteCarrierRate(GetIfpQuoteResponseQuoteCarrierRate value) {
        return new JAXBElement<GetIfpQuoteResponseQuoteCarrierRate>(_GetIfpQuoteResponseQuoteCarrierRate_QNAME, GetIfpQuoteResponseQuoteCarrierRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.Item")
    public JAXBElement<ArrayOfGetPlansbyRxItem> createArrayOfGetPlansbyRxItem(ArrayOfGetPlansbyRxItem value) {
        return new JAXBElement<ArrayOfGetPlansbyRxItem>(_ArrayOfGetPlansbyRxItem_QNAME, ArrayOfGetPlansbyRxItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LevelOfDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LevelOfDetails")
    public JAXBElement<LevelOfDetails> createLevelOfDetails(LevelOfDetails value) {
        return new JAXBElement<LevelOfDetails>(_LevelOfDetails_QNAME, LevelOfDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrierPlanBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage")
    public JAXBElement<GetPlansbyRxResponseCarrierPlanBenefitCoverage> createGetPlansbyRxResponseCarrierPlanBenefitCoverage(GetPlansbyRxResponseCarrierPlanBenefitCoverage value) {
        return new JAXBElement<GetPlansbyRxResponseCarrierPlanBenefitCoverage>(_GetPlansbyRxResponseCarrierPlanBenefitCoverage_QNAME, GetPlansbyRxResponseCarrierPlanBenefitCoverage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FamilyLocationAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Family.LocationAddress")
    public JAXBElement<FamilyLocationAddress> createFamilyLocationAddress(FamilyLocationAddress value) {
        return new JAXBElement<FamilyLocationAddress>(_FamilyLocationAddress_QNAME, FamilyLocationAddress.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountiesRequestAccessKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCounties.Request.AccessKey")
    public JAXBElement<GetCountiesRequestAccessKey> createGetCountiesRequestAccessKey(GetCountiesRequestAccessKey value) {
        return new JAXBElement<GetCountiesRequestAccessKey>(_GetCountiesRequestAccessKey_QNAME, GetCountiesRequestAccessKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductPlanBusinessRuleAllowedRelation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct.Plan.BusinessRule.AllowedRelation")
    public JAXBElement<CustomProductPlanBusinessRuleAllowedRelation> createCustomProductPlanBusinessRuleAllowedRelation(CustomProductPlanBusinessRuleAllowedRelation value) {
        return new JAXBElement<CustomProductPlanBusinessRuleAllowedRelation>(_CustomProductPlanBusinessRuleAllowedRelation_QNAME, CustomProductPlanBusinessRuleAllowedRelation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PaymentOption")
    public JAXBElement<List<String>> createPaymentOption(List<String> value) {
        return new JAXBElement<List<String>>(_PaymentOption_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsuranceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "InsuranceType")
    public JAXBElement<InsuranceType> createInsuranceType(InsuranceType value) {
        return new JAXBElement<InsuranceType>(_InsuranceType_QNAME, InsuranceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmployeeRateBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EmployeeRateBase")
    public JAXBElement<EmployeeRateBase> createEmployeeRateBase(EmployeeRateBase value) {
        return new JAXBElement<EmployeeRateBase>(_EmployeeRateBase_QNAME, EmployeeRateBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarrierLoadHistoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarrierLoadHistory.Response")
    public JAXBElement<GetCarrierLoadHistoryResponse> createGetCarrierLoadHistoryResponse(GetCarrierLoadHistoryResponse value) {
        return new JAXBElement<GetCarrierLoadHistoryResponse>(_GetCarrierLoadHistoryResponse_QNAME, GetCarrierLoadHistoryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlanFilterBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanFilterBase")
    public JAXBElement<PlanFilterBase> createPlanFilterBase(PlanFilterBase value) {
        return new JAXBElement<PlanFilterBase>(_PlanFilterBase_QNAME, PlanFilterBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.Quote.DroppedCarrier")
    public JAXBElement<GetGroupQuoteResponseQuoteDroppedCarrier> createGetGroupQuoteResponseQuoteDroppedCarrier(GetGroupQuoteResponseQuoteDroppedCarrier value) {
        return new JAXBElement<GetGroupQuoteResponseQuoteDroppedCarrier>(_GetGroupQuoteResponseQuoteDroppedCarrier_QNAME, GetGroupQuoteResponseQuoteDroppedCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FamilyContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Family.Contact")
    public JAXBElement<FamilyContact> createFamilyContact(FamilyContact value) {
        return new JAXBElement<FamilyContact>(_FamilyContact_QNAME, FamilyContact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequestDrugFilters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Request.DrugFilters")
    public JAXBElement<GetCarriersPlansBenefitsRequestDrugFilters> createGetCarriersPlansBenefitsRequestDrugFilters(GetCarriersPlansBenefitsRequestDrugFilters value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequestDrugFilters>(_GetCarriersPlansBenefitsRequestDrugFilters_QNAME, GetCarriersPlansBenefitsRequestDrugFilters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequestAccessKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Request.AccessKey")
    public JAXBElement<GetCarriersPlansBenefitsRequestAccessKey> createGetCarriersPlansBenefitsRequestAccessKey(GetCarriersPlansBenefitsRequestAccessKey value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequestAccessKey>(_GetCarriersPlansBenefitsRequestAccessKey_QNAME, GetCarriersPlansBenefitsRequestAccessKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OutOfAreaAction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OutOfAreaAction")
    public JAXBElement<OutOfAreaAction> createOutOfAreaAction(OutOfAreaAction value) {
        return new JAXBElement<OutOfAreaAction>(_OutOfAreaAction_QNAME, OutOfAreaAction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AllowanceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AllowanceType")
    public JAXBElement<AllowanceType> createAllowanceType(AllowanceType value) {
        return new JAXBElement<AllowanceType>(_AllowanceType_QNAME, AllowanceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ServiceBase")
    public JAXBElement<ServiceBase> createServiceBase(ServiceBase value) {
        return new JAXBElement<ServiceBase>(_ServiceBase_QNAME, ServiceBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfError")
    public JAXBElement<ArrayOfError> createArrayOfError(ArrayOfError value) {
        return new JAXBElement<ArrayOfError>(_ArrayOfError_QNAME, ArrayOfError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier")
    public JAXBElement<GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier> createGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier(GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier value) {
        return new JAXBElement<GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier_QNAME, GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Error }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Error")
    public JAXBElement<Error> createError(Error value) {
        return new JAXBElement<Error>(_Error_QNAME, Error.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoResponseZipCodeInfoCity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetZipCodeInfo.Response.ZipCodeInfo.City")
    public JAXBElement<GetZipCodeInfoResponseZipCodeInfoCity> createGetZipCodeInfoResponseZipCodeInfoCity(GetZipCodeInfoResponseZipCodeInfoCity value) {
        return new JAXBElement<GetZipCodeInfoResponseZipCodeInfoCity>(_GetZipCodeInfoResponseZipCodeInfoCity_QNAME, GetZipCodeInfoResponseZipCodeInfoCity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug")
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> createArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug(ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug>(_ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.Quote.CarrierRate.MemberRate")
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate> createArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate(ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate>(_ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate_QNAME, ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlanComponentBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanComponentBase")
    public JAXBElement<PlanComponentBase> createPlanComponentBase(PlanComponentBase value) {
        return new JAXBElement<PlanComponentBase>(_PlanComponentBase_QNAME, PlanComponentBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ZipCodeInfoBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZipCodeInfoBase")
    public JAXBElement<ZipCodeInfoBase> createZipCodeInfoBase(ZipCodeInfoBase value) {
        return new JAXBElement<ZipCodeInfoBase>(_ZipCodeInfoBase_QNAME, ZipCodeInfoBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFamilyMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfFamily.Member")
    public JAXBElement<ArrayOfFamilyMember> createArrayOfFamilyMember(ArrayOfFamilyMember value) {
        return new JAXBElement<ArrayOfFamilyMember>(_ArrayOfFamilyMember_QNAME, ArrayOfFamilyMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint> createArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint>(_ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionFilterBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VersionFilterBase")
    public JAXBElement<VersionFilterBase> createVersionFilterBase(VersionFilterBase value) {
        return new JAXBElement<VersionFilterBase>(_VersionFilterBase_QNAME, VersionFilterBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetailBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.PlanDetail.Benefit")
    public JAXBElement<GetGroupQuoteResponsePlanDetailBenefit> createGetGroupQuoteResponsePlanDetailBenefit(GetGroupQuoteResponsePlanDetailBenefit value) {
        return new JAXBElement<GetGroupQuoteResponsePlanDetailBenefit>(_GetGroupQuoteResponsePlanDetailBenefit_QNAME, GetGroupQuoteResponsePlanDetailBenefit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseFormulary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Formulary")
    public JAXBElement<GetCarriersPlansBenefitsResponseFormulary> createGetCarriersPlansBenefitsResponseFormulary(GetCarriersPlansBenefitsResponseFormulary value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseFormulary>(_GetCarriersPlansBenefitsResponseFormulary_QNAME, GetCarriersPlansBenefitsResponseFormulary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfFormularyLookup.Response.Carrier")
    public JAXBElement<ArrayOfFormularyLookupResponseCarrier> createArrayOfFormularyLookupResponseCarrier(ArrayOfFormularyLookupResponseCarrier value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseCarrier>(_ArrayOfFormularyLookupResponseCarrier_QNAME, ArrayOfFormularyLookupResponseCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactorEmployeeMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.GroupRateFactor.Employee.Member")
    public JAXBElement<GetGroupQuoteRequestGroupRateFactorEmployeeMember> createGetGroupQuoteRequestGroupRateFactorEmployeeMember(GetGroupQuoteRequestGroupRateFactorEmployeeMember value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateFactorEmployeeMember>(_GetGroupQuoteRequestGroupRateFactorEmployeeMember_QNAME, GetGroupQuoteRequestGroupRateFactorEmployeeMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RxPeriod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RxPeriod")
    public JAXBElement<RxPeriod> createRxPeriod(RxPeriod value) {
        return new JAXBElement<RxPeriod>(_RxPeriod_QNAME, RxPeriod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrierPlanBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response.Carrier.Plan.Benefit")
    public JAXBElement<GetPlansbyRxResponseCarrierPlanBenefit> createGetPlansbyRxResponseCarrierPlanBenefit(GetPlansbyRxResponseCarrierPlanBenefit value) {
        return new JAXBElement<GetPlansbyRxResponseCarrierPlanBenefit>(_GetPlansbyRxResponseCarrierPlanBenefit_QNAME, GetPlansbyRxResponseCarrierPlanBenefit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseMemberRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCustomProductsQuote.Response.MemberRate")
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseMemberRate> createArrayOfGetCustomProductsQuoteResponseMemberRate(ArrayOfGetCustomProductsQuoteResponseMemberRate value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseMemberRate>(_ArrayOfGetCustomProductsQuoteResponseMemberRate_QNAME, ArrayOfGetCustomProductsQuoteResponseMemberRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestPreferenceChoiceFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.Preference.ChoiceFilter")
    public JAXBElement<GetGroupQuoteRequestPreferenceChoiceFilter> createGetGroupQuoteRequestPreferenceChoiceFilter(GetGroupQuoteRequestPreferenceChoiceFilter value) {
        return new JAXBElement<GetGroupQuoteRequestPreferenceChoiceFilter>(_GetGroupQuoteRequestPreferenceChoiceFilter_QNAME, GetGroupQuoteRequestPreferenceChoiceFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit")
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefit> createArrayOfGetPlansbyRxResponseCarrierPlanBenefit(ArrayOfGetPlansbyRxResponseCarrierPlanBenefit value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefit>(_ArrayOfGetPlansbyRxResponseCarrierPlanBenefit_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanBenefit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Request.Member")
    public JAXBElement<ArrayOfGetIfpQuoteRequestMember> createArrayOfGetIfpQuoteRequestMember(ArrayOfGetIfpQuoteRequestMember value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestMember>(_ArrayOfGetIfpQuoteRequestMember_QNAME, ArrayOfGetIfpQuoteRequestMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupEmployeeMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Group.Employee.Member")
    public JAXBElement<GroupEmployeeMember> createGroupEmployeeMember(GroupEmployeeMember value) {
        return new JAXBElement<GroupEmployeeMember>(_GroupEmployeeMember_QNAME, GroupEmployeeMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.PlanDetail.Benefit")
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefit> createArrayOfGetIfpQuoteResponsePlanDetailBenefit(ArrayOfGetIfpQuoteResponsePlanDetailBenefit value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefit>(_ArrayOfGetIfpQuoteResponsePlanDetailBenefit_QNAME, ArrayOfGetIfpQuoteResponsePlanDetailBenefit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseQuote }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.Quote")
    public JAXBElement<GetIfpQuoteResponseQuote> createGetIfpQuoteResponseQuote(GetIfpQuoteResponseQuote value) {
        return new JAXBElement<GetIfpQuoteResponseQuote>(_GetIfpQuoteResponseQuote_QNAME, GetIfpQuoteResponseQuote.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier.VersionedPlan")
    public JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan> createArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan(ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan value) {
        return new JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan>(_ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan_QNAME, ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroup.Request")
    public JAXBElement<GetGroupRequest> createGetGroupRequest(GetGroupRequest value) {
        return new JAXBElement<GetGroupRequest>(_GetGroupRequest_QNAME, GetGroupRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Eligibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Eligibility")
    public JAXBElement<Eligibility> createEligibility(Eligibility value) {
        return new JAXBElement<Eligibility>(_Eligibility_QNAME, Eligibility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseQuoteCarrierRatePlanRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.Quote.CarrierRate.PlanRate")
    public JAXBElement<GetIfpQuoteResponseQuoteCarrierRatePlanRate> createGetIfpQuoteResponseQuoteCarrierRatePlanRate(GetIfpQuoteResponseQuoteCarrierRatePlanRate value) {
        return new JAXBElement<GetIfpQuoteResponseQuoteCarrierRatePlanRate>(_GetIfpQuoteResponseQuoteCarrierRatePlanRate_QNAME, GetIfpQuoteResponseQuoteCarrierRatePlanRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BenchmarkPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenchmarkPlan")
    public JAXBElement<BenchmarkPlan> createBenchmarkPlan(BenchmarkPlan value) {
        return new JAXBElement<BenchmarkPlan>(_BenchmarkPlan_QNAME, BenchmarkPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuote }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.Quote")
    public JAXBElement<GetGroupQuoteResponseQuote> createGetGroupQuoteResponseQuote(GetGroupQuoteResponseQuote value) {
        return new JAXBElement<GetGroupQuoteResponseQuote>(_GetGroupQuoteResponseQuote_QNAME, GetGroupQuoteResponseQuote.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpShoppingCartsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpShoppingCartsBase")
    public JAXBElement<IfpShoppingCartsBase> createIfpShoppingCartsBase(IfpShoppingCartsBase value) {
        return new JAXBElement<IfpShoppingCartsBase>(_IfpShoppingCartsBase_QNAME, IfpShoppingCartsBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteCarrierRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.Quote.CarrierRate")
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRate> createArrayOfGetGroupQuoteResponseQuoteCarrierRate(ArrayOfGetGroupQuoteResponseQuoteCarrierRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRate>(_ArrayOfGetGroupQuoteResponseQuoteCarrierRate_QNAME, ArrayOfGetGroupQuoteResponseQuoteCarrierRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier")
    public JAXBElement<GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier(GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier value) {
        return new JAXBElement<GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier_QNAME, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.GroupRateFactor.RAFOverride")
    public JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride> createArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride(ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride>(_ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride_QNAME, ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.AddOn")
    public JAXBElement<ArrayOfGetPlansbyRxAddOn> createArrayOfGetPlansbyRxAddOn(ArrayOfGetPlansbyRxAddOn value) {
        return new JAXBElement<ArrayOfGetPlansbyRxAddOn>(_ArrayOfGetPlansbyRxAddOn_QNAME, ArrayOfGetPlansbyRxAddOn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupRequestInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Request.Input")
    public JAXBElement<FormularyLookupRequestInput> createFormularyLookupRequestInput(FormularyLookupRequestInput value) {
        return new JAXBElement<FormularyLookupRequestInput>(_FormularyLookupRequestInput_QNAME, FormularyLookupRequestInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitCustomProductsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitCustomProducts.Response")
    public JAXBElement<SubmitCustomProductsResponse> createSubmitCustomProductsResponse(SubmitCustomProductsResponse value) {
        return new JAXBElement<SubmitCustomProductsResponse>(_SubmitCustomProductsResponse_QNAME, SubmitCustomProductsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseMemberRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.MemberRate")
    public JAXBElement<GetGroupQuoteResponseMemberRate> createGetGroupQuoteResponseMemberRate(GetGroupQuoteResponseMemberRate value) {
        return new JAXBElement<GetGroupQuoteResponseMemberRate>(_GetGroupQuoteResponseMemberRate_QNAME, GetGroupQuoteResponseMemberRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ViewPointBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ViewPointBase")
    public JAXBElement<ViewPointBase> createViewPointBase(ViewPointBase value) {
        return new JAXBElement<ViewPointBase>(_ViewPointBase_QNAME, ViewPointBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee.Member")
    public JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember> createArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember(ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember>(_ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember_QNAME, ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug")
    public JAXBElement<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug(GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug value) {
        return new JAXBElement<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug_QNAME, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfIfpShoppingCart.ProductChoice.PlanChoices.StatusChange")
    public JAXBElement<ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange> createArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange(ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange value) {
        return new JAXBElement<ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange>(_ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange_QNAME, ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuoteEmployeePlanCarrierRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate")
    public JAXBElement<GetGroupQuoteResponseQuoteEmployeePlanCarrierRate> createGetGroupQuoteResponseQuoteEmployeePlanCarrierRate(GetGroupQuoteResponseQuoteEmployeePlanCarrierRate value) {
        return new JAXBElement<GetGroupQuoteResponseQuoteEmployeePlanCarrierRate>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRate_QNAME, GetGroupQuoteResponseQuoteEmployeePlanCarrierRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.PlanDetail")
    public JAXBElement<GetGroupQuoteResponsePlanDetail> createGetGroupQuoteResponsePlanDetail(GetGroupQuoteResponsePlanDetail value) {
        return new JAXBElement<GetGroupQuoteResponsePlanDetail>(_GetGroupQuoteResponsePlanDetail_QNAME, GetGroupQuoteResponsePlanDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Request")
    public JAXBElement<FormularyLookupRequest> createFormularyLookupRequest(FormularyLookupRequest value) {
        return new JAXBElement<FormularyLookupRequest>(_FormularyLookupRequest_QNAME, FormularyLookupRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountyBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyBase")
    public JAXBElement<CountyBase> createCountyBase(CountyBase value) {
        return new JAXBElement<CountyBase>(_CountyBase_QNAME, CountyBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier")
    public JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier> createArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier(ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier>(_ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier_QNAME, ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupQuoteFormat")
    public JAXBElement<List<String>> createGroupQuoteFormat(List<String> value) {
        return new JAXBElement<List<String>>(_GroupQuoteFormat_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchCustomProductsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FetchCustomProducts.Request")
    public JAXBElement<FetchCustomProductsRequest> createFetchCustomProductsRequest(FetchCustomProductsRequest value) {
        return new JAXBElement<FetchCustomProductsRequest>(_FetchCustomProductsRequest_QNAME, FetchCustomProductsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetZipCodeInfo.Response")
    public JAXBElement<GetZipCodeInfoResponse> createGetZipCodeInfoResponse(GetZipCodeInfoResponse value) {
        return new JAXBElement<GetZipCodeInfoResponse>(_GetZipCodeInfoResponse_QNAME, GetZipCodeInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier")
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier> createArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier(ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier>(_ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier_QNAME, ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response.Carrier")
    public JAXBElement<GetPlansbyRxResponseCarrier> createGetPlansbyRxResponseCarrier(GetPlansbyRxResponseCarrier value) {
        return new JAXBElement<GetPlansbyRxResponseCarrier>(_GetPlansbyRxResponseCarrier_QNAME, GetPlansbyRxResponseCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseQuoteCarrierRateModified }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.Quote.Carrier.Rate.Modified")
    public JAXBElement<GetIfpQuoteResponseQuoteCarrierRateModified> createGetIfpQuoteResponseQuoteCarrierRateModified(GetIfpQuoteResponseQuoteCarrierRateModified value) {
        return new JAXBElement<GetIfpQuoteResponseQuoteCarrierRateModified>(_GetIfpQuoteResponseQuoteCarrierRateModified_QNAME, GetIfpQuoteResponseQuoteCarrierRateModified.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGroupRAFOverride }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGroup.RAFOverride")
    public JAXBElement<ArrayOfGroupRAFOverride> createArrayOfGroupRAFOverride(ArrayOfGroupRAFOverride value) {
        return new JAXBElement<ArrayOfGroupRAFOverride>(_ArrayOfGroupRAFOverride_QNAME, ArrayOfGroupRAFOverride.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint")
    public JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint> createArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint(ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint>(_ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint_QNAME, ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductRatingAreaZipRange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct.RatingArea.ZipRange")
    public JAXBElement<CustomProductRatingAreaZipRange> createCustomProductRatingAreaZipRange(CustomProductRatingAreaZipRange value) {
        return new JAXBElement<CustomProductRatingAreaZipRange>(_CustomProductRatingAreaZipRange_QNAME, CustomProductRatingAreaZipRange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpQuoteType")
    public JAXBElement<List<String>> createIfpQuoteType(List<String> value) {
        return new JAXBElement<List<String>>(_IfpQuoteType_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response")
    public JAXBElement<GetGroupQuoteResponse> createGetGroupQuoteResponse(GetGroupQuoteResponse value) {
        return new JAXBElement<GetGroupQuoteResponse>(_GetGroupQuoteResponse_QNAME, GetGroupQuoteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint.Service")
    public JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService> createArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService(ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService>(_ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService_QNAME, ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.Quote.DroppedCarrier")
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier> createArrayOfGetGroupQuoteResponseQuoteDroppedCarrier(ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier>(_ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier_QNAME, ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan> createArrayOfGetCarriersPlansBenefitsResponseCarrierPlan(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan>(_ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EventOperation")
    public JAXBElement<List<String>> createEventOperation(List<String> value) {
        return new JAXBElement<List<String>>(_EventOperation_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteRequestPreference }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Request.Preference")
    public JAXBElement<GetCustomProductsQuoteRequestPreference> createGetCustomProductsQuoteRequestPreference(GetCustomProductsQuoteRequestPreference value) {
        return new JAXBElement<GetCustomProductsQuoteRequestPreference>(_GetCustomProductsQuoteRequestPreference_QNAME, GetCustomProductsQuoteRequestPreference.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestProductChoicePlanChoices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.ProductChoice.PlanChoices")
    public JAXBElement<GetIfpQuoteRequestProductChoicePlanChoices> createGetIfpQuoteRequestProductChoicePlanChoices(GetIfpQuoteRequestProductChoicePlanChoices value) {
        return new JAXBElement<GetIfpQuoteRequestProductChoicePlanChoices>(_GetIfpQuoteRequestProductChoicePlanChoices_QNAME, GetIfpQuoteRequestProductChoicePlanChoices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseCarrierPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Carrier.Plan")
    public JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlan> createGetCarriersPlansBenefitsResponseCarrierPlan(GetCarriersPlansBenefitsResponseCarrierPlan value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlan>(_GetCarriersPlansBenefitsResponseCarrierPlan_QNAME, GetCarriersPlansBenefitsResponseCarrierPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPreferenceChoiceFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.Preference.ChoiceFilter")
    public JAXBElement<GetIfpQuoteRequestPreferenceChoiceFilter> createGetIfpQuoteRequestPreferenceChoiceFilter(GetIfpQuoteRequestPreferenceChoiceFilter value) {
        return new JAXBElement<GetIfpQuoteRequestPreferenceChoiceFilter>(_GetIfpQuoteRequestPreferenceChoiceFilter_QNAME, GetIfpQuoteRequestPreferenceChoiceFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductRatingAreaZipCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct.RatingArea.ZipCounty")
    public JAXBElement<CustomProductRatingAreaZipCounty> createCustomProductRatingAreaZipCounty(CustomProductRatingAreaZipCounty value) {
        return new JAXBElement<CustomProductRatingAreaZipCounty>(_CustomProductRatingAreaZipCounty_QNAME, CustomProductRatingAreaZipCounty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RatingAreaBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RatingAreaBase")
    public JAXBElement<RatingAreaBase> createRatingAreaBase(RatingAreaBase value) {
        return new JAXBElement<RatingAreaBase>(_RatingAreaBase_QNAME, RatingAreaBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.Quote.DroppedCarrier")
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier> createArrayOfGetIfpQuoteResponseQuoteDroppedCarrier(ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier>(_ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier_QNAME, ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Request.Preference.BenefitFilter.AttributeFilter")
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter> createArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter(ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter>(_ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter_QNAME, ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RAFOverrideBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RAFOverrideBase")
    public JAXBElement<RAFOverrideBase> createRAFOverrideBase(RAFOverrideBase value) {
        return new JAXBElement<RAFOverrideBase>(_RAFOverrideBase_QNAME, RAFOverrideBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage")
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage> createArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage(ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage>(_ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage_QNAME, ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan")
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlan> createArrayOfGetPlansbyRxResponseCarrierPlan(ArrayOfGetPlansbyRxResponseCarrierPlan value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlan>(_ArrayOfGetPlansbyRxResponseCarrierPlan_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.Item")
    public JAXBElement<ArrayOfGetGroupQuoteResponseItem> createArrayOfGetGroupQuoteResponseItem(ArrayOfGetGroupQuoteResponseItem value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseItem>(_ArrayOfGetGroupQuoteResponseItem_QNAME, ArrayOfGetGroupQuoteResponseItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response")
    public JAXBElement<GetCarriersPlansBenefitsResponse> createGetCarriersPlansBenefitsResponse(GetCarriersPlansBenefitsResponse value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponse>(_GetCarriersPlansBenefitsResponse_QNAME, GetCarriersPlansBenefitsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DroppedPlanBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedPlanBase")
    public JAXBElement<DroppedPlanBase> createDroppedPlanBase(DroppedPlanBase value) {
        return new JAXBElement<DroppedPlanBase>(_DroppedPlanBase_QNAME, DroppedPlanBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsPlanVisibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.PlanVisibility")
    public JAXBElement<GetCarriersPlansBenefitsPlanVisibility> createGetCarriersPlansBenefitsPlanVisibility(GetCarriersPlansBenefitsPlanVisibility value) {
        return new JAXBElement<GetCarriersPlansBenefitsPlanVisibility>(_GetCarriersPlansBenefitsPlanVisibility_QNAME, GetCarriersPlansBenefitsPlanVisibility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService> createArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService>(_ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupLifeSelector")
    public JAXBElement<List<String>> createGroupLifeSelector(List<String> value) {
        return new JAXBElement<List<String>>(_GroupLifeSelector_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ResponseBase")
    public JAXBElement<ResponseBase> createResponseBase(ResponseBase value) {
        return new JAXBElement<ResponseBase>(_ResponseBase_QNAME, ResponseBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ItemBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ItemBase")
    public JAXBElement<ItemBase> createItemBase(ItemBase value) {
        return new JAXBElement<ItemBase>(_ItemBase_QNAME, ItemBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Item")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> createArrayOfGetCarriersPlansBenefitsResponseItem(ArrayOfGetCarriersPlansBenefitsResponseItem value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem>(_ArrayOfGetCarriersPlansBenefitsResponseItem_QNAME, ArrayOfGetCarriersPlansBenefitsResponseItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.DrugList.Drug")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug> createArrayOfGetCarriersPlansBenefitsResponseDrugListDrug(ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug>(_ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug_QNAME, ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchCustomProductsRequestInputProductFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FetchCustomProducts.Request.Input.ProductFilter")
    public JAXBElement<FetchCustomProductsRequestInputProductFilter> createFetchCustomProductsRequestInputProductFilter(FetchCustomProductsRequestInputProductFilter value) {
        return new JAXBElement<FetchCustomProductsRequestInputProductFilter>(_FetchCustomProductsRequestInputProductFilter_QNAME, FetchCustomProductsRequestInputProductFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupRatingAreaBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupRatingAreaBase")
    public JAXBElement<GroupRatingAreaBase> createGroupRatingAreaBase(GroupRatingAreaBase value) {
        return new JAXBElement<GroupRatingAreaBase>(_GroupRatingAreaBase_QNAME, GroupRatingAreaBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO")
    public JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> createArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO(ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO>(_ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO_QNAME, ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Market }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Market")
    public JAXBElement<Market> createMarket(Market value) {
        return new JAXBElement<Market>(_Market_QNAME, Market.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestPreferencePlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.Preference.PlanFilter")
    public JAXBElement<GetGroupQuoteRequestPreferencePlanFilter> createGetGroupQuoteRequestPreferencePlanFilter(GetGroupQuoteRequestPreferencePlanFilter value) {
        return new JAXBElement<GetGroupQuoteRequestPreferencePlanFilter>(_GetGroupQuoteRequestPreferencePlanFilter_QNAME, GetGroupQuoteRequestPreferencePlanFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceDrugFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Request.Preference.DrugFilter")
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceDrugFilter> createArrayOfGetIfpQuoteRequestPreferenceDrugFilter(ArrayOfGetIfpQuoteRequestPreferenceDrugFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceDrugFilter>(_ArrayOfGetIfpQuoteRequestPreferenceDrugFilter_QNAME, ArrayOfGetIfpQuoteRequestPreferenceDrugFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupResponseCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Response.Carrier")
    public JAXBElement<FormularyLookupResponseCarrier> createFormularyLookupResponseCarrier(FormularyLookupResponseCarrier value) {
        return new JAXBElement<FormularyLookupResponseCarrier>(_FormularyLookupResponseCarrier_QNAME, FormularyLookupResponseCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.Preference.ChoiceFilter")
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter> createArrayOfGetGroupQuoteRequestPreferenceChoiceFilter(ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter>(_ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter_QNAME, ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteRequestCustomRateFactorFamily }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Request.CustomRateFactor.Family")
    public JAXBElement<GetCustomProductsQuoteRequestCustomRateFactorFamily> createGetCustomProductsQuoteRequestCustomRateFactorFamily(GetCustomProductsQuoteRequestCustomRateFactorFamily value) {
        return new JAXBElement<GetCustomProductsQuoteRequestCustomRateFactorFamily>(_GetCustomProductsQuoteRequestCustomRateFactorFamily_QNAME, GetCustomProductsQuoteRequestCustomRateFactorFamily.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrierPlanPlanFormulary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary")
    public JAXBElement<GetPlansbyRxResponseCarrierPlanPlanFormulary> createGetPlansbyRxResponseCarrierPlanPlanFormulary(GetPlansbyRxResponseCarrierPlanPlanFormulary value) {
        return new JAXBElement<GetPlansbyRxResponseCarrierPlanPlanFormulary>(_GetPlansbyRxResponseCarrierPlanPlanFormulary_QNAME, GetPlansbyRxResponseCarrierPlanPlanFormulary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactBase")
    public JAXBElement<ContactBase> createContactBase(ContactBase value) {
        return new JAXBElement<ContactBase>(_ContactBase_QNAME, ContactBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestIfpRateFactor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.IfpRateFactor")
    public JAXBElement<GetIfpQuoteRequestIfpRateFactor> createGetIfpQuoteRequestIfpRateFactor(GetIfpQuoteRequestIfpRateFactor value) {
        return new JAXBElement<GetIfpQuoteRequestIfpRateFactor>(_GetIfpQuoteRequestIfpRateFactor_QNAME, GetIfpQuoteRequestIfpRateFactor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Request")
    public JAXBElement<GetCustomProductsQuoteRequest> createGetCustomProductsQuoteRequest(GetCustomProductsQuoteRequest value) {
        return new JAXBElement<GetCustomProductsQuoteRequest>(_GetCustomProductsQuoteRequest_QNAME, GetCustomProductsQuoteRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint")
    public JAXBElement<GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint> createGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint(GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint value) {
        return new JAXBElement<GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint>(_GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint_QNAME, GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupRAFOverride }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Group.RAFOverride")
    public JAXBElement<GroupRAFOverride> createGroupRAFOverride(GroupRAFOverride value) {
        return new JAXBElement<GroupRAFOverride>(_GroupRAFOverride_QNAME, GroupRAFOverride.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.PlanDetail.Benefit")
    public JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefit> createArrayOfGetGroupQuoteResponsePlanDetailBenefit(ArrayOfGetGroupQuoteResponsePlanDetailBenefit value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefit>(_ArrayOfGetGroupQuoteResponsePlanDetailBenefit_QNAME, ArrayOfGetGroupQuoteResponsePlanDetailBenefit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LinkType")
    public JAXBElement<List<String>> createLinkType(List<String> value) {
        return new JAXBElement<List<String>>(_LinkType_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee.ProductChoice")
    public JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice> createArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice(ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice>(_ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice_QNAME, ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactorRAFOverride }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.GroupRateFactor.RAFOverride")
    public JAXBElement<GetGroupQuoteRequestGroupRateFactorRAFOverride> createGetGroupQuoteRequestGroupRateFactorRAFOverride(GetGroupQuoteRequestGroupRateFactorRAFOverride value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateFactorRAFOverride>(_GetGroupQuoteRequestGroupRateFactorRAFOverride_QNAME, GetGroupQuoteRequestGroupRateFactorRAFOverride.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupResponseCarrierPlanPlanFormulary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Response.Carrier.Plan.PlanFormulary")
    public JAXBElement<FormularyLookupResponseCarrierPlanPlanFormulary> createFormularyLookupResponseCarrierPlanPlanFormulary(FormularyLookupResponseCarrierPlanPlanFormulary value) {
        return new JAXBElement<FormularyLookupResponseCarrierPlanPlanFormulary>(_FormularyLookupResponseCarrierPlanPlanFormulary_QNAME, FormularyLookupResponseCarrierPlanPlanFormulary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpRateBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpRateBase")
    public JAXBElement<IfpRateBase> createIfpRateBase(IfpRateBase value) {
        return new JAXBElement<IfpRateBase>(_IfpRateBase_QNAME, IfpRateBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocationBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LocationBase")
    public JAXBElement<LocationBase> createLocationBase(LocationBase value) {
        return new JAXBElement<LocationBase>(_LocationBase_QNAME, LocationBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupShoppingCartProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupShoppingCart.ProductChoice")
    public JAXBElement<GroupShoppingCartProductChoice> createGroupShoppingCartProductChoice(GroupShoppingCartProductChoice value) {
        return new JAXBElement<GroupShoppingCartProductChoice>(_GroupShoppingCartProductChoice_QNAME, GroupShoppingCartProductChoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuoteCarrierRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.Quote.CarrierRate")
    public JAXBElement<GetGroupQuoteResponseQuoteCarrierRate> createGetGroupQuoteResponseQuoteCarrierRate(GetGroupQuoteResponseQuoteCarrierRate value) {
        return new JAXBElement<GetGroupQuoteResponseQuoteCarrierRate>(_GetGroupQuoteResponseQuoteCarrierRate_QNAME, GetGroupQuoteResponseQuoteCarrierRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberRateBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MemberRateBase")
    public JAXBElement<MemberRateBase> createMemberRateBase(MemberRateBase value) {
        return new JAXBElement<MemberRateBase>(_MemberRateBase_QNAME, MemberRateBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ValidationType")
    public JAXBElement<ValidationType> createValidationType(ValidationType value) {
        return new JAXBElement<ValidationType>(_ValidationType_QNAME, ValidationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFamilyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetFamily.Response")
    public JAXBElement<GetFamilyResponse> createGetFamilyResponse(GetFamilyResponse value) {
        return new JAXBElement<GetFamilyResponse>(_GetFamilyResponse_QNAME, GetFamilyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FamilyContactContactAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Family.Contact.ContactAttribute")
    public JAXBElement<FamilyContactContactAttribute> createFamilyContactContactAttribute(FamilyContactContactAttribute value) {
        return new JAXBElement<FamilyContactContactAttribute>(_FamilyContactContactAttribute_QNAME, FamilyContactContactAttribute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoRequestInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetZipCodeInfo.Request.Input")
    public JAXBElement<GetZipCodeInfoRequestInput> createGetZipCodeInfoRequestInput(GetZipCodeInfoRequestInput value) {
        return new JAXBElement<GetZipCodeInfoRequestInput>(_GetZipCodeInfoRequestInput_QNAME, GetZipCodeInfoRequestInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListRxRequestInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ListRx.Request.Input")
    public JAXBElement<ListRxRequestInput> createListRxRequestInput(ListRxRequestInput value) {
        return new JAXBElement<ListRxRequestInput>(_ListRxRequestInput_QNAME, ListRxRequestInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteRequestPreferenceProductFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Request.Preference.ProductFilter")
    public JAXBElement<GetCustomProductsQuoteRequestPreferenceProductFilter> createGetCustomProductsQuoteRequestPreferenceProductFilter(GetCustomProductsQuoteRequestPreferenceProductFilter value) {
        return new JAXBElement<GetCustomProductsQuoteRequestPreferenceProductFilter>(_GetCustomProductsQuoteRequestPreferenceProductFilter_QNAME, GetCustomProductsQuoteRequestPreferenceProductFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint.Service")
    public JAXBElement<GetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService> createGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService(GetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService value) {
        return new JAXBElement<GetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService>(_GetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService_QNAME, GetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGroupEmployee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGroup.Employee")
    public JAXBElement<ArrayOfGroupEmployee> createArrayOfGroupEmployee(ArrayOfGroupEmployee value) {
        return new JAXBElement<ArrayOfGroupEmployee>(_ArrayOfGroupEmployee_QNAME, ArrayOfGroupEmployee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit> createArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit>(_ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.Quote.DroppedCarrier.DroppedPlan")
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan> createArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan(ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan>(_ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan_QNAME, ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service")
    public JAXBElement<GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService> createGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService(GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService value) {
        return new JAXBElement<GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService>(_GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService_QNAME, GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupRequestPlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfFormularyLookup.Request.PlanFilter")
    public JAXBElement<ArrayOfFormularyLookupRequestPlanFilter> createArrayOfFormularyLookupRequestPlanFilter(ArrayOfFormularyLookupRequestPlanFilter value) {
        return new JAXBElement<ArrayOfFormularyLookupRequestPlanFilter>(_ArrayOfFormularyLookupRequestPlanFilter_QNAME, ArrayOfFormularyLookupRequestPlanFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier")
    public JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier> createArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier(ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier value) {
        return new JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier>(_ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier_QNAME, ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance")
    public JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance> createGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance(GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance>(_GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance_QNAME, GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate")
    public JAXBElement<GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate(GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate value) {
        return new JAXBElement<GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate_QNAME, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family")
    public JAXBElement<ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily> createArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily(ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily>(_ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily_QNAME, ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamily.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoRequestAccessKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetZipCodeInfo.Request.AccessKey")
    public JAXBElement<GetZipCodeInfoRequestAccessKey> createGetZipCodeInfoRequestAccessKey(GetZipCodeInfoRequestAccessKey value) {
        return new JAXBElement<GetZipCodeInfoRequestAccessKey>(_GetZipCodeInfoRequestAccessKey_QNAME, GetZipCodeInfoRequestAccessKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseCarrierDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.CarrierDetail")
    public JAXBElement<GetGroupQuoteResponseCarrierDetail> createGetGroupQuoteResponseCarrierDetail(GetGroupQuoteResponseCarrierDetail value) {
        return new JAXBElement<GetGroupQuoteResponseCarrierDetail>(_GetGroupQuoteResponseCarrierDetail_QNAME, GetGroupQuoteResponseCarrierDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint")
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint> createArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint(ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint>(_ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.AddOn")
    public JAXBElement<GetGroupQuoteAddOn> createGetGroupQuoteAddOn(GetGroupQuoteAddOn value) {
        return new JAXBElement<GetGroupQuoteAddOn>(_GetGroupQuoteAddOn_QNAME, GetGroupQuoteAddOn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductRatingArea }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfCustomProduct.RatingArea")
    public JAXBElement<ArrayOfCustomProductRatingArea> createArrayOfCustomProductRatingArea(ArrayOfCustomProductRatingArea value) {
        return new JAXBElement<ArrayOfCustomProductRatingArea>(_ArrayOfCustomProductRatingArea_QNAME, ArrayOfCustomProductRatingArea.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfInsuranceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfInsuranceType")
    public JAXBElement<ArrayOfInsuranceType> createArrayOfInsuranceType(ArrayOfInsuranceType value) {
        return new JAXBElement<ArrayOfInsuranceType>(_ArrayOfInsuranceType_QNAME, ArrayOfInsuranceType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseFormularyDrugTierTierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Formulary.DrugTier.TierType")
    public JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTierTierType> createGetCarriersPlansBenefitsResponseFormularyDrugTierTierType(GetCarriersPlansBenefitsResponseFormularyDrugTierTierType value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTierTierType>(_GetCarriersPlansBenefitsResponseFormularyDrugTierTierType_QNAME, GetCarriersPlansBenefitsResponseFormularyDrugTierTierType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchCustomProductsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FetchCustomProducts.Response")
    public JAXBElement<FetchCustomProductsResponse> createFetchCustomProductsResponse(FetchCustomProductsResponse value) {
        return new JAXBElement<FetchCustomProductsResponse>(_FetchCustomProductsResponse_QNAME, FetchCustomProductsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpShoppingCart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpShoppingCart")
    public JAXBElement<IfpShoppingCart> createIfpShoppingCart(IfpShoppingCart value) {
        return new JAXBElement<IfpShoppingCart>(_IfpShoppingCart_QNAME, IfpShoppingCart.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestGroupRateFactorScenario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.GroupRateFactor.Scenario")
    public JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorScenario> createArrayOfGetGroupQuoteRequestGroupRateFactorScenario(ArrayOfGetGroupQuoteRequestGroupRateFactorScenario value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorScenario>(_ArrayOfGetGroupQuoteRequestGroupRateFactorScenario_QNAME, ArrayOfGetGroupQuoteRequestGroupRateFactorScenario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupFilterType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.FilterType")
    public JAXBElement<FormularyLookupFilterType> createFormularyLookupFilterType(FormularyLookupFilterType value) {
        return new JAXBElement<FormularyLookupFilterType>(_FormularyLookupFilterType_QNAME, FormularyLookupFilterType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FlagValue")
    public JAXBElement<List<String>> createFlagValue(List<String> value) {
        return new JAXBElement<List<String>>(_FlagValue_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseContactContactPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Contact.ContactPoint")
    public JAXBElement<GetCarriersPlansBenefitsResponseContactContactPoint> createGetCarriersPlansBenefitsResponseContactContactPoint(GetCarriersPlansBenefitsResponseContactContactPoint value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseContactContactPoint>(_GetCarriersPlansBenefitsResponseContactContactPoint_QNAME, GetCarriersPlansBenefitsResponseContactContactPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListRxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ListRx.Response")
    public JAXBElement<ListRxResponse> createListRxResponse(ListRxResponse value) {
        return new JAXBElement<ListRxResponse>(_ListRxResponse_QNAME, ListRxResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpPlanEligibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpPlanEligibility")
    public JAXBElement<IfpPlanEligibility> createIfpPlanEligibility(IfpPlanEligibility value) {
        return new JAXBElement<IfpPlanEligibility>(_IfpPlanEligibility_QNAME, IfpPlanEligibility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarrierLoadHistory.Response.LoadEvent.Carrier.VersionedPlan")
    public JAXBElement<GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan> createGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan(GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan value) {
        return new JAXBElement<GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan>(_GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan_QNAME, GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DroppedCarrierBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedCarrierBase")
    public JAXBElement<DroppedCarrierBase> createDroppedCarrierBase(DroppedCarrierBase value) {
        return new JAXBElement<DroppedCarrierBase>(_DroppedCarrierBase_QNAME, DroppedCarrierBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponseQuote }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Response.Quote")
    public JAXBElement<GetCustomProductsQuoteResponseQuote> createGetCustomProductsQuoteResponseQuote(GetCustomProductsQuoteResponseQuote value) {
        return new JAXBElement<GetCustomProductsQuoteResponseQuote>(_GetCustomProductsQuoteResponseQuote_QNAME, GetCustomProductsQuoteResponseQuote.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanRawRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfCustomProduct.Plan.RawRate")
    public JAXBElement<ArrayOfCustomProductPlanRawRate> createArrayOfCustomProductPlanRawRate(ArrayOfCustomProductPlanRawRate value) {
        return new JAXBElement<ArrayOfCustomProductPlanRawRate>(_ArrayOfCustomProductPlanRawRate_QNAME, ArrayOfCustomProductPlanRawRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocationAddressBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LocationAddressBase")
    public JAXBElement<LocationAddressBase> createLocationAddressBase(LocationAddressBase value) {
        return new JAXBElement<LocationAddressBase>(_LocationAddressBase_QNAME, LocationAddressBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO")
    public JAXBElement<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO(GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO value) {
        return new JAXBElement<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO_QNAME, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGroupShoppingCartProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGroupShoppingCart.ProductChoice")
    public JAXBElement<ArrayOfGroupShoppingCartProductChoice> createArrayOfGroupShoppingCartProductChoice(ArrayOfGroupShoppingCartProductChoice value) {
        return new JAXBElement<ArrayOfGroupShoppingCartProductChoice>(_ArrayOfGroupShoppingCartProductChoice_QNAME, ArrayOfGroupShoppingCartProductChoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPreferenceDrugFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.Preference.DrugFilter")
    public JAXBElement<GetIfpQuoteRequestPreferenceDrugFilter> createGetIfpQuoteRequestPreferenceDrugFilter(GetIfpQuoteRequestPreferenceDrugFilter value) {
        return new JAXBElement<GetIfpQuoteRequestPreferenceDrugFilter>(_GetIfpQuoteRequestPreferenceDrugFilter_QNAME, GetIfpQuoteRequestPreferenceDrugFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Response")
    public JAXBElement<FormularyLookupResponse> createFormularyLookupResponse(FormularyLookupResponse value) {
        return new JAXBElement<FormularyLookupResponse>(_FormularyLookupResponse_QNAME, FormularyLookupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductRatingAreaZipCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfCustomProduct.RatingArea.ZipCounty")
    public JAXBElement<ArrayOfCustomProductRatingAreaZipCounty> createArrayOfCustomProductRatingAreaZipCounty(ArrayOfCustomProductRatingAreaZipCounty value) {
        return new JAXBElement<ArrayOfCustomProductRatingAreaZipCounty>(_ArrayOfCustomProductRatingAreaZipCounty_QNAME, ArrayOfCustomProductRatingAreaZipCounty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service")
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService> createArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService(ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService>(_ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlanDetailsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanDetailsBase")
    public JAXBElement<PlanDetailsBase> createPlanDetailsBase(PlanDetailsBase value) {
        return new JAXBElement<PlanDetailsBase>(_PlanDetailsBase_QNAME, PlanDetailsBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.AddOn")
    public JAXBElement<GetIfpQuoteAddOn> createGetIfpQuoteAddOn(GetIfpQuoteAddOn value) {
        return new JAXBElement<GetIfpQuoteAddOn>(_GetIfpQuoteAddOn_QNAME, GetIfpQuoteAddOn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsLevelOfDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.LevelOfDetail")
    public JAXBElement<GetCarriersPlansBenefitsLevelOfDetail> createGetCarriersPlansBenefitsLevelOfDetail(GetCarriersPlansBenefitsLevelOfDetail value) {
        return new JAXBElement<GetCarriersPlansBenefitsLevelOfDetail>(_GetCarriersPlansBenefitsLevelOfDetail_QNAME, GetCarriersPlansBenefitsLevelOfDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RateBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RateBase")
    public JAXBElement<RateBase> createRateBase(RateBase value) {
        return new JAXBElement<RateBase>(_RateBase_QNAME, RateBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseFormulary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Formulary")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormulary> createArrayOfGetCarriersPlansBenefitsResponseFormulary(ArrayOfGetCarriersPlansBenefitsResponseFormulary value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormulary>(_ArrayOfGetCarriersPlansBenefitsResponseFormulary_QNAME, ArrayOfGetCarriersPlansBenefitsResponseFormulary.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChoiceFilterBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChoiceFilterBase")
    public JAXBElement<ChoiceFilterBase> createChoiceFilterBase(ChoiceFilterBase value) {
        return new JAXBElement<ChoiceFilterBase>(_ChoiceFilterBase_QNAME, ChoiceFilterBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitCustomProductsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitCustomProducts.Request")
    public JAXBElement<SubmitCustomProductsRequest> createSubmitCustomProductsRequest(SubmitCustomProductsRequest value) {
        return new JAXBElement<SubmitCustomProductsRequest>(_SubmitCustomProductsRequest_QNAME, SubmitCustomProductsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfListRxResponseDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfListRx.Response.Drug")
    public JAXBElement<ArrayOfListRxResponseDrug> createArrayOfListRxResponseDrug(ArrayOfListRxResponseDrug value) {
        return new JAXBElement<ArrayOfListRxResponseDrug>(_ArrayOfListRxResponseDrug_QNAME, ArrayOfListRxResponseDrug.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Group.Contact")
    public JAXBElement<GroupContact> createGroupContact(GroupContact value) {
        return new JAXBElement<GroupContact>(_GroupContact_QNAME, GroupContact.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter")
    public JAXBElement<GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter> createGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter(GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter value) {
        return new JAXBElement<GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter>(_GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter_QNAME, GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier")
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier> createArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier(ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier>(_ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCustomProductsQuote.Request.CustomRateFactor.Family.Member")
    public JAXBElement<ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember> createArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember(ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember>(_ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember_QNAME, ArrayOfGetCustomProductsQuoteRequestCustomRateFactorFamilyMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitCustomProductsRequestSetting }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitCustomProducts.Request.Setting")
    public JAXBElement<SubmitCustomProductsRequestSetting> createSubmitCustomProductsRequestSetting(SubmitCustomProductsRequestSetting value) {
        return new JAXBElement<SubmitCustomProductsRequestSetting>(_SubmitCustomProductsRequestSetting_QNAME, SubmitCustomProductsRequestSetting.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuoteEmployeePlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.Quote.EmployeePlan")
    public JAXBElement<GetGroupQuoteResponseQuoteEmployeePlan> createGetGroupQuoteResponseQuoteEmployeePlan(GetGroupQuoteResponseQuoteEmployeePlan value) {
        return new JAXBElement<GetGroupQuoteResponseQuoteEmployeePlan>(_GetGroupQuoteResponseQuoteEmployeePlan_QNAME, GetGroupQuoteResponseQuoteEmployeePlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.Quote.CarrierRate.PlanRate")
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate> createArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate(ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate>(_ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate_QNAME, ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductRatingAreaCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct.RatingArea.County")
    public JAXBElement<CustomProductRatingAreaCounty> createCustomProductRatingAreaCounty(CustomProductRatingAreaCounty value) {
        return new JAXBElement<CustomProductRatingAreaCounty>(_CustomProductRatingAreaCounty_QNAME, CustomProductRatingAreaCounty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactorScenario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.GroupRateFactor.Scenario")
    public JAXBElement<GetGroupQuoteRequestGroupRateFactorScenario> createGetGroupQuoteRequestGroupRateFactorScenario(GetGroupQuoteRequestGroupRateFactorScenario value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateFactorScenario>(_GetGroupQuoteRequestGroupRateFactorScenario_QNAME, GetGroupQuoteRequestGroupRateFactorScenario.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttributeBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AttributeBase")
    public JAXBElement<AttributeBase> createAttributeBase(AttributeBase value) {
        return new JAXBElement<AttributeBase>(_AttributeBase_QNAME, AttributeBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpShoppingCartProductChoicePlanChoices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpShoppingCart.ProductChoice.PlanChoices")
    public JAXBElement<IfpShoppingCartProductChoicePlanChoices> createIfpShoppingCartProductChoicePlanChoices(IfpShoppingCartProductChoicePlanChoices value) {
        return new JAXBElement<IfpShoppingCartProductChoicePlanChoices>(_IfpShoppingCartProductChoicePlanChoices_QNAME, IfpShoppingCartProductChoicePlanChoices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferencePlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.Preference.PlanFilter")
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferencePlanFilter> createArrayOfGetGroupQuoteRequestPreferencePlanFilter(ArrayOfGetGroupQuoteRequestPreferencePlanFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferencePlanFilter>(_ArrayOfGetGroupQuoteRequestPreferencePlanFilter_QNAME, ArrayOfGetGroupQuoteRequestPreferencePlanFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Family }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Family")
    public JAXBElement<Family> createFamily(Family value) {
        return new JAXBElement<Family>(_Family_QNAME, Family.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitCustomProductsRequestAccessKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitCustomProducts.Request.AccessKey")
    public JAXBElement<SubmitCustomProductsRequestAccessKey> createSubmitCustomProductsRequestAccessKey(SubmitCustomProductsRequestAccessKey value) {
        return new JAXBElement<SubmitCustomProductsRequestAccessKey>(_SubmitCustomProductsRequestAccessKey_QNAME, SubmitCustomProductsRequestAccessKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint")
    public JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint> createGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint(GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint>(_GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint_QNAME, GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContractStatus")
    public JAXBElement<List<String>> createContractStatus(List<String> value) {
        return new JAXBElement<List<String>>(_ContractStatus_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.County")
    public JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty> createArrayOfGetZipCodeInfoResponseZipCodeInfoCounty(ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty value) {
        return new JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty>(_ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty_QNAME, ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote")
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote> createArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote(ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote>(_ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote_QNAME, ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferencePlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Request.Preference.PlanFilter")
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferencePlanFilter> createArrayOfGetIfpQuoteRequestPreferencePlanFilter(ArrayOfGetIfpQuoteRequestPreferencePlanFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferencePlanFilter>(_ArrayOfGetIfpQuoteRequestPreferencePlanFilter_QNAME, ArrayOfGetIfpQuoteRequestPreferencePlanFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.AddOn")
    public JAXBElement<GetPlansbyRxAddOn> createGetPlansbyRxAddOn(GetPlansbyRxAddOn value) {
        return new JAXBElement<GetPlansbyRxAddOn>(_GetPlansbyRxAddOn_QNAME, GetPlansbyRxAddOn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier.DroppedPlan")
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan> createArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan(ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan>(_ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan_QNAME, ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BenefitBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitBase")
    public JAXBElement<BenefitBase> createBenefitBase(BenefitBase value) {
        return new JAXBElement<BenefitBase>(_BenefitBase_QNAME, BenefitBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseFormularyDrugTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Formulary.DrugTier")
    public JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTier> createGetCarriersPlansBenefitsResponseFormularyDrugTier(GetCarriersPlansBenefitsResponseFormularyDrugTier value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTier>(_GetCarriersPlansBenefitsResponseFormularyDrugTier_QNAME, GetCarriersPlansBenefitsResponseFormularyDrugTier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpShoppingCartsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpShoppingCarts.Response")
    public JAXBElement<GetIfpShoppingCartsResponse> createGetIfpShoppingCartsResponse(GetIfpShoppingCartsResponse value) {
        return new JAXBElement<GetIfpShoppingCartsResponse>(_GetIfpShoppingCartsResponse_QNAME, GetIfpShoppingCartsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponseQuoteFamilyQuote }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Response.Quote.FamilyQuote")
    public JAXBElement<GetCustomProductsQuoteResponseQuoteFamilyQuote> createGetCustomProductsQuoteResponseQuoteFamilyQuote(GetCustomProductsQuoteResponseQuoteFamilyQuote value) {
        return new JAXBElement<GetCustomProductsQuoteResponseQuoteFamilyQuote>(_GetCustomProductsQuoteResponseQuoteFamilyQuote_QNAME, GetCustomProductsQuoteResponseQuoteFamilyQuote.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier")
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier> createArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier(ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier>(_ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier_QNAME, ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct.Plan")
    public JAXBElement<CustomProductPlan> createCustomProductPlan(CustomProductPlan value) {
        return new JAXBElement<CustomProductPlan>(_CustomProductPlan_QNAME, CustomProductPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.AddOn")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsAddOn> createArrayOfGetCarriersPlansBenefitsAddOn(ArrayOfGetCarriersPlansBenefitsAddOn value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsAddOn>(_ArrayOfGetCarriersPlansBenefitsAddOn_QNAME, ArrayOfGetCarriersPlansBenefitsAddOn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponsePlanDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.PlanDetail")
    public JAXBElement<GetIfpQuoteResponsePlanDetail> createGetIfpQuoteResponsePlanDetail(GetIfpQuoteResponsePlanDetail value) {
        return new JAXBElement<GetIfpQuoteResponsePlanDetail>(_GetIfpQuoteResponsePlanDetail_QNAME, GetIfpQuoteResponsePlanDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.Quote.EmployeePlan")
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlan> createArrayOfGetGroupQuoteResponseQuoteEmployeePlan(ArrayOfGetGroupQuoteResponseQuoteEmployeePlan value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlan>(_ArrayOfGetGroupQuoteResponseQuoteEmployeePlan_QNAME, ArrayOfGetGroupQuoteResponseQuoteEmployeePlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductPlanBusinessRule }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct.Plan.BusinessRule")
    public JAXBElement<CustomProductPlanBusinessRule> createCustomProductPlanBusinessRule(CustomProductPlanBusinessRule value) {
        return new JAXBElement<CustomProductPlanBusinessRule>(_CustomProductPlanBusinessRule_QNAME, CustomProductPlanBusinessRule.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Request.CarrierPlanFilter")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter> createArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter(ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter>(_ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter_QNAME, ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrierRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.Quote.CarrierRate")
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRate> createArrayOfGetIfpQuoteResponseQuoteCarrierRate(ArrayOfGetIfpQuoteResponseQuoteCarrierRate value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRate>(_ArrayOfGetIfpQuoteResponseQuoteCarrierRate_QNAME, ArrayOfGetIfpQuoteResponseQuoteCarrierRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfCustomProduct.Plan")
    public JAXBElement<ArrayOfCustomProductPlan> createArrayOfCustomProductPlan(ArrayOfCustomProductPlan value) {
        return new JAXBElement<ArrayOfCustomProductPlan>(_ArrayOfCustomProductPlan_QNAME, ArrayOfCustomProductPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteRequestCustomRateFactorFamilyMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Request.CustomRateFactor.Family.Member")
    public JAXBElement<GetCustomProductsQuoteRequestCustomRateFactorFamilyMember> createGetCustomProductsQuoteRequestCustomRateFactorFamilyMember(GetCustomProductsQuoteRequestCustomRateFactorFamilyMember value) {
        return new JAXBElement<GetCustomProductsQuoteRequestCustomRateFactorFamilyMember>(_GetCustomProductsQuoteRequestCustomRateFactorFamilyMember_QNAME, GetCustomProductsQuoteRequestCustomRateFactorFamilyMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductRatingAreaCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfCustomProduct.RatingArea.County")
    public JAXBElement<ArrayOfCustomProductRatingAreaCounty> createArrayOfCustomProductRatingAreaCounty(ArrayOfCustomProductRatingAreaCounty value) {
        return new JAXBElement<ArrayOfCustomProductRatingAreaCounty>(_ArrayOfCustomProductRatingAreaCounty_QNAME, ArrayOfCustomProductRatingAreaCounty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCountiesResponseCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCounties.Response.County")
    public JAXBElement<ArrayOfGetCountiesResponseCounty> createArrayOfGetCountiesResponseCounty(ArrayOfGetCountiesResponseCounty value) {
        return new JAXBElement<ArrayOfGetCountiesResponseCounty>(_ArrayOfGetCountiesResponseCounty_QNAME, ArrayOfGetCountiesResponseCounty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Gender")
    public JAXBElement<List<String>> createGender(List<String> value) {
        return new JAXBElement<List<String>>(_Gender_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BestTimeToCall }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BestTimeToCall")
    public JAXBElement<BestTimeToCall> createBestTimeToCall(BestTimeToCall value) {
        return new JAXBElement<BestTimeToCall>(_BestTimeToCall_QNAME, BestTimeToCall.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Response")
    public JAXBElement<GetCustomProductsQuoteResponse> createGetCustomProductsQuoteResponse(GetCustomProductsQuoteResponse value) {
        return new JAXBElement<GetCustomProductsQuoteResponse>(_GetCustomProductsQuoteResponse_QNAME, GetCustomProductsQuoteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmissionStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmissionStatus")
    public JAXBElement<SubmissionStatus> createSubmissionStatus(SubmissionStatus value) {
        return new JAXBElement<SubmissionStatus>(_SubmissionStatus_QNAME, SubmissionStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListRxResponseDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ListRx.Response.Drug")
    public JAXBElement<ListRxResponseDrug> createListRxResponseDrug(ListRxResponseDrug value) {
        return new JAXBElement<ListRxResponseDrug>(_ListRxResponseDrug_QNAME, ListRxResponseDrug.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitIfpShoppingCartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitIfpShoppingCart.Response")
    public JAXBElement<SubmitIfpShoppingCartResponse> createSubmitIfpShoppingCartResponse(SubmitIfpShoppingCartResponse value) {
        return new JAXBElement<SubmitIfpShoppingCartResponse>(_SubmitIfpShoppingCartResponse_QNAME, SubmitIfpShoppingCartResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPreferencePlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.Preference.PlanFilter")
    public JAXBElement<GetIfpQuoteRequestPreferencePlanFilter> createGetIfpQuoteRequestPreferencePlanFilter(GetIfpQuoteRequestPreferencePlanFilter value) {
        return new JAXBElement<GetIfpQuoteRequestPreferencePlanFilter>(_GetIfpQuoteRequestPreferencePlanFilter_QNAME, GetIfpQuoteRequestPreferencePlanFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupResponseCarrierPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Response.Carrier.Plan")
    public JAXBElement<FormularyLookupResponseCarrierPlan> createFormularyLookupResponseCarrierPlan(FormularyLookupResponseCarrierPlan value) {
        return new JAXBElement<FormularyLookupResponseCarrierPlan>(_FormularyLookupResponseCarrierPlan_QNAME, FormularyLookupResponseCarrierPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Carrier")
    public JAXBElement<GetCarriersPlansBenefitsResponseCarrier> createGetCarriersPlansBenefitsResponseCarrier(GetCarriersPlansBenefitsResponseCarrier value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseCarrier>(_GetCarriersPlansBenefitsResponseCarrier_QNAME, GetCarriersPlansBenefitsResponseCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.GroupRateFactor.Employee.ProductChoice.PlanChoices")
    public JAXBElement<GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices> createGetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices(GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices_QNAME, GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountiesResponseCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCounties.Response.County")
    public JAXBElement<GetCountiesResponseCounty> createGetCountiesResponseCounty(GetCountiesResponseCounty value) {
        return new JAXBElement<GetCountiesResponseCounty>(_GetCountiesResponseCounty_QNAME, GetCountiesResponseCounty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarrierLoadHistoryRequestInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarrierLoadHistory.Request.Input")
    public JAXBElement<GetCarrierLoadHistoryRequestInput> createGetCarrierLoadHistoryRequestInput(GetCarrierLoadHistoryRequestInput value) {
        return new JAXBElement<GetCarrierLoadHistoryRequestInput>(_GetCarrierLoadHistoryRequestInput_QNAME, GetCarrierLoadHistoryRequestInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ContactPointBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactPointBase")
    public JAXBElement<ContactPointBase> createContactPointBase(ContactPointBase value) {
        return new JAXBElement<ContactPointBase>(_ContactPointBase_QNAME, ContactPointBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.Item")
    public JAXBElement<GetGroupQuoteResponseItem> createGetGroupQuoteResponseItem(GetGroupQuoteResponseItem value) {
        return new JAXBElement<GetGroupQuoteResponseItem>(_GetGroupQuoteResponseItem_QNAME, GetGroupQuoteResponseItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxRequestAccessKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Request.AccessKey")
    public JAXBElement<GetPlansbyRxRequestAccessKey> createGetPlansbyRxRequestAccessKey(GetPlansbyRxRequestAccessKey value) {
        return new JAXBElement<GetPlansbyRxRequestAccessKey>(_GetPlansbyRxRequestAccessKey_QNAME, GetPlansbyRxRequestAccessKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpShoppingCartProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpShoppingCart.ProductChoice")
    public JAXBElement<IfpShoppingCartProductChoice> createIfpShoppingCartProductChoice(IfpShoppingCartProductChoice value) {
        return new JAXBElement<IfpShoppingCartProductChoice>(_IfpShoppingCartProductChoice_QNAME, IfpShoppingCartProductChoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanSortDir")
    public JAXBElement<List<String>> createPlanSortDir(List<String> value) {
        return new JAXBElement<List<String>>(_PlanSortDir_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request")
    public JAXBElement<GetIfpQuoteRequest> createGetIfpQuoteRequest(GetIfpQuoteRequest value) {
        return new JAXBElement<GetIfpQuoteRequest>(_GetIfpQuoteRequest_QNAME, GetIfpQuoteRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.Quote.DroppedCarrier")
    public JAXBElement<GetIfpQuoteResponseQuoteDroppedCarrier> createGetIfpQuoteResponseQuoteDroppedCarrier(GetIfpQuoteResponseQuoteDroppedCarrier value) {
        return new JAXBElement<GetIfpQuoteResponseQuoteDroppedCarrier>(_GetIfpQuoteResponseQuoteDroppedCarrier_QNAME, GetIfpQuoteResponseQuoteDroppedCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage")
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage> createArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage(ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage>(_ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteRequestCustomRateFactor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCustomProductsQuote.Request.CustomRateFactor")
    public JAXBElement<GetCustomProductsQuoteRequestCustomRateFactor> createGetCustomProductsQuoteRequestCustomRateFactor(GetCustomProductsQuoteRequestCustomRateFactor value) {
        return new JAXBElement<GetCustomProductsQuoteRequestCustomRateFactor>(_GetCustomProductsQuoteRequestCustomRateFactor_QNAME, GetCustomProductsQuoteRequestCustomRateFactor.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceVersionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Request.Preference.VersionFilter")
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceVersionFilter> createArrayOfGetIfpQuoteRequestPreferenceVersionFilter(ArrayOfGetIfpQuoteRequestPreferenceVersionFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceVersionFilter>(_ArrayOfGetIfpQuoteRequestPreferenceVersionFilter_QNAME, ArrayOfGetIfpQuoteRequestPreferenceVersionFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CommandFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CommandFault")
    public JAXBElement<CommandFault> createCommandFault(CommandFault value) {
        return new JAXBElement<CommandFault>(_CommandFault_QNAME, CommandFault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RxCUIsBase")
    public JAXBElement<RxCUIsBase> createRxCUIsBase(RxCUIsBase value) {
        return new JAXBElement<RxCUIsBase>(_RxCUIsBase_QNAME, RxCUIsBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoResponseZipCodeInfoCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetZipCodeInfo.Response.ZipCodeInfo.County")
    public JAXBElement<GetZipCodeInfoResponseZipCodeInfoCounty> createGetZipCodeInfoResponseZipCodeInfoCounty(GetZipCodeInfoResponseZipCodeInfoCounty value) {
        return new JAXBElement<GetZipCodeInfoResponseZipCodeInfoCounty>(_GetZipCodeInfoResponseZipCodeInfoCounty_QNAME, GetZipCodeInfoResponseZipCodeInfoCounty.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountiesRequestInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCounties.Request.Input")
    public JAXBElement<GetCountiesRequestInput> createGetCountiesRequestInput(GetCountiesRequestInput value) {
        return new JAXBElement<GetCountiesRequestInput>(_GetCountiesRequestInput_QNAME, GetCountiesRequestInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InAreaAction }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "InAreaAction")
    public JAXBElement<InAreaAction> createInAreaAction(InAreaAction value) {
        return new JAXBElement<InAreaAction>(_InAreaAction_QNAME, InAreaAction.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate")
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate> createArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate(ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate>(_ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate_QNAME, ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EmployerContributionTowards")
    public JAXBElement<List<String>> createEmployerContributionTowards(List<String> value) {
        return new JAXBElement<List<String>>(_EmployerContributionTowards_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RateSet")
    public JAXBElement<List<String>> createRateSet(List<String> value) {
        return new JAXBElement<List<String>>(_RateSet_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance> createArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance(ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance>(_ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance_QNAME, ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestPreferenceVersionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.Preference.VersionFilter")
    public JAXBElement<GetGroupQuoteRequestPreferenceVersionFilter> createGetGroupQuoteRequestPreferenceVersionFilter(GetGroupQuoteRequestPreferenceVersionFilter value) {
        return new JAXBElement<GetGroupQuoteRequestPreferenceVersionFilter>(_GetGroupQuoteRequestPreferenceVersionFilter_QNAME, GetGroupQuoteRequestPreferenceVersionFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseDrugList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.DrugList")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugList> createArrayOfGetCarriersPlansBenefitsResponseDrugList(ArrayOfGetCarriersPlansBenefitsResponseDrugList value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugList>(_ArrayOfGetCarriersPlansBenefitsResponseDrugList_QNAME, ArrayOfGetCarriersPlansBenefitsResponseDrugList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitGroupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitGroup.Response")
    public JAXBElement<SubmitGroupResponse> createSubmitGroupResponse(SubmitGroupResponse value) {
        return new JAXBElement<SubmitGroupResponse>(_SubmitGroupResponse_QNAME, SubmitGroupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequestBenefitFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Request.BenefitFilter")
    public JAXBElement<GetCarriersPlansBenefitsRequestBenefitFilter> createGetCarriersPlansBenefitsRequestBenefitFilter(GetCarriersPlansBenefitsRequestBenefitFilter value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequestBenefitFilter>(_GetCarriersPlansBenefitsRequestBenefitFilter_QNAME, GetCarriersPlansBenefitsRequestBenefitFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request")
    public JAXBElement<GetGroupQuoteRequest> createGetGroupQuoteRequest(GetGroupQuoteRequest value) {
        return new JAXBElement<GetGroupQuoteRequest>(_GetGroupQuoteRequest_QNAME, GetGroupQuoteRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarrierLoadHistoryRequestAccessKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarrierLoadHistory.Request.AccessKey")
    public JAXBElement<GetCarrierLoadHistoryRequestAccessKey> createGetCarrierLoadHistoryRequestAccessKey(GetCarrierLoadHistoryRequestAccessKey value) {
        return new JAXBElement<GetCarrierLoadHistoryRequestAccessKey>(_GetCarrierLoadHistoryRequestAccessKey_QNAME, GetCarrierLoadHistoryRequestAccessKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseQuoteCarrierRateMemberRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.Quote.CarrierRate.MemberRate")
    public JAXBElement<GetIfpQuoteResponseQuoteCarrierRateMemberRate> createGetIfpQuoteResponseQuoteCarrierRateMemberRate(GetIfpQuoteResponseQuoteCarrierRateMemberRate value) {
        return new JAXBElement<GetIfpQuoteResponseQuoteCarrierRateMemberRate>(_GetIfpQuoteResponseQuoteCarrierRateMemberRate_QNAME, GetIfpQuoteResponseQuoteCarrierRateMemberRate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service")
    public JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService> createGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService(GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService>(_GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService_QNAME, GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestPreference }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.Preference")
    public JAXBElement<GetGroupQuoteRequestPreference> createGetGroupQuoteRequestPreference(GetGroupQuoteRequestPreference value) {
        return new JAXBElement<GetGroupQuoteRequestPreference>(_GetGroupQuoteRequestPreference_QNAME, GetGroupQuoteRequestPreference.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListRxRequestPlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ListRx.Request.PlanFilter")
    public JAXBElement<ListRxRequestPlanFilter> createListRxRequestPlanFilter(ListRxRequestPlanFilter value) {
        return new JAXBElement<ListRxRequestPlanFilter>(_ListRxRequestPlanFilter_QNAME, ListRxRequestPlanFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoResponseZipCodeInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetZipCodeInfo.Response.ZipCodeInfo")
    public JAXBElement<GetZipCodeInfoResponseZipCodeInfo> createGetZipCodeInfoResponseZipCodeInfo(GetZipCodeInfoResponseZipCodeInfo value) {
        return new JAXBElement<GetZipCodeInfoResponseZipCodeInfo>(_GetZipCodeInfoResponseZipCodeInfo_QNAME, GetZipCodeInfoResponseZipCodeInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFamilyContactContactAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfFamily.Contact.ContactAttribute")
    public JAXBElement<ArrayOfFamilyContactContactAttribute> createArrayOfFamilyContactContactAttribute(ArrayOfFamilyContactContactAttribute value) {
        return new JAXBElement<ArrayOfFamilyContactContactAttribute>(_ArrayOfFamilyContactContactAttribute_QNAME, ArrayOfFamilyContactContactAttribute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductRatingArea }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct.RatingArea")
    public JAXBElement<CustomProductRatingArea> createCustomProductRatingArea(CustomProductRatingArea value) {
        return new JAXBElement<CustomProductRatingArea>(_CustomProductRatingArea_QNAME, CustomProductRatingArea.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpPlanChoiceBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpPlanChoiceBase")
    public JAXBElement<IfpPlanChoiceBase> createIfpPlanChoiceBase(IfpPlanChoiceBase value) {
        return new JAXBElement<IfpPlanChoiceBase>(_IfpPlanChoiceBase_QNAME, IfpPlanChoiceBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint")
    public JAXBElement<GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint> createGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint(GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint value) {
        return new JAXBElement<GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint>(_GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint_QNAME, GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint")
    public JAXBElement<GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint> createGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint(GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint value) {
        return new JAXBElement<GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint>(_GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint_QNAME, GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlanBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanBase")
    public JAXBElement<PlanBase> createPlanBase(PlanBase value) {
        return new JAXBElement<PlanBase>(_PlanBase_QNAME, PlanBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetZipCodeInfoResponseZipCodeInfoCity }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.City")
    public JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfoCity> createArrayOfGetZipCodeInfoResponseZipCodeInfoCity(ArrayOfGetZipCodeInfoResponseZipCodeInfoCity value) {
        return new JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfoCity>(_ArrayOfGetZipCodeInfoResponseZipCodeInfoCity_QNAME, ArrayOfGetZipCodeInfoResponseZipCodeInfoCity.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Group }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Group")
    public JAXBElement<Group> createGroup(Group value) {
        return new JAXBElement<Group>(_Group_QNAME, Group.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlanRateBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanRateBase")
    public JAXBElement<PlanRateBase> createPlanRateBase(PlanRateBase value) {
        return new JAXBElement<PlanRateBase>(_PlanRateBase_QNAME, PlanRateBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpQuoteFormat")
    public JAXBElement<List<String>> createIfpQuoteFormat(List<String> value) {
        return new JAXBElement<List<String>>(_IfpQuoteFormat_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpShoppingCartsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpShoppingCarts.Request")
    public JAXBElement<GetIfpShoppingCartsRequest> createGetIfpShoppingCartsRequest(GetIfpShoppingCartsRequest value) {
        return new JAXBElement<GetIfpShoppingCartsRequest>(_GetIfpShoppingCartsRequest_QNAME, GetIfpShoppingCartsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitFamilyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitFamily.Request")
    public JAXBElement<SubmitFamilyRequest> createSubmitFamilyRequest(SubmitFamilyRequest value) {
        return new JAXBElement<SubmitFamilyRequest>(_SubmitFamilyRequest_QNAME, SubmitFamilyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetZipCodeInfoResponseZipCodeInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetZipCodeInfo.Response.ZipCodeInfo")
    public JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfo> createArrayOfGetZipCodeInfoResponseZipCodeInfo(ArrayOfGetZipCodeInfoResponseZipCodeInfo value) {
        return new JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfo>(_ArrayOfGetZipCodeInfoResponseZipCodeInfo_QNAME, ArrayOfGetZipCodeInfoResponseZipCodeInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CostSharingType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CostSharingType")
    public JAXBElement<CostSharingType> createCostSharingType(CostSharingType value) {
        return new JAXBElement<CostSharingType>(_CostSharingType_QNAME, CostSharingType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FeaturedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FeaturedPlan")
    public JAXBElement<FeaturedPlan> createFeaturedPlan(FeaturedPlan value) {
        return new JAXBElement<FeaturedPlan>(_FeaturedPlan_QNAME, FeaturedPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductRatingAreaZipRange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfCustomProduct.RatingArea.ZipRange")
    public JAXBElement<ArrayOfCustomProductRatingAreaZipRange> createArrayOfCustomProductRatingAreaZipRange(ArrayOfCustomProductRatingAreaZipRange value) {
        return new JAXBElement<ArrayOfCustomProductRatingAreaZipRange>(_ArrayOfCustomProductRatingAreaZipRange_QNAME, ArrayOfCustomProductRatingAreaZipRange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Item")
    public JAXBElement<GetPlansbyRxItem> createGetPlansbyRxItem(GetPlansbyRxItem value) {
        return new JAXBElement<GetPlansbyRxItem>(_GetPlansbyRxItem_QNAME, GetPlansbyRxItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.AddOn")
    public JAXBElement<GetCarriersPlansBenefitsAddOn> createGetCarriersPlansBenefitsAddOn(GetCarriersPlansBenefitsAddOn value) {
        return new JAXBElement<GetCarriersPlansBenefitsAddOn>(_GetCarriersPlansBenefitsAddOn_QNAME, GetCarriersPlansBenefitsAddOn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseCarrierPlanBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit")
    public JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlanBenefit> createGetCarriersPlansBenefitsResponseCarrierPlanBenefit(GetCarriersPlansBenefitsResponseCarrierPlanBenefit value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlanBenefit>(_GetCarriersPlansBenefitsResponseCarrierPlanBenefit_QNAME, GetCarriersPlansBenefitsResponseCarrierPlanBenefit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Request")
    public JAXBElement<GetCarriersPlansBenefitsRequest> createGetCarriersPlansBenefitsRequest(GetCarriersPlansBenefitsRequest value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequest>(_GetCarriersPlansBenefitsRequest_QNAME, GetCarriersPlansBenefitsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfFormularyLookup.Response.Carrier.Plan")
    public JAXBElement<ArrayOfFormularyLookupResponseCarrierPlan> createArrayOfFormularyLookupResponseCarrierPlan(ArrayOfFormularyLookupResponseCarrierPlan value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseCarrierPlan>(_ArrayOfFormularyLookupResponseCarrierPlan_QNAME, ArrayOfFormularyLookupResponseCarrierPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupEmployee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Group.Employee")
    public JAXBElement<GroupEmployee> createGroupEmployee(GroupEmployee value) {
        return new JAXBElement<GroupEmployee>(_GroupEmployee_QNAME, GroupEmployee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MemberBase")
    public JAXBElement<MemberBase> createMemberBase(MemberBase value) {
        return new JAXBElement<MemberBase>(_MemberBase_QNAME, MemberBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFetchCustomProductsRequestInputProductFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfFetchCustomProducts.Request.Input.ProductFilter")
    public JAXBElement<ArrayOfFetchCustomProductsRequestInputProductFilter> createArrayOfFetchCustomProductsRequestInputProductFilter(ArrayOfFetchCustomProductsRequestInputProductFilter value) {
        return new JAXBElement<ArrayOfFetchCustomProductsRequestInputProductFilter>(_ArrayOfFetchCustomProductsRequestInputProductFilter_QNAME, ArrayOfFetchCustomProductsRequestInputProductFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseLocation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Location")
    public JAXBElement<GetCarriersPlansBenefitsResponseLocation> createGetCarriersPlansBenefitsResponseLocation(GetCarriersPlansBenefitsResponseLocation value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseLocation>(_GetCarriersPlansBenefitsResponseLocation_QNAME, GetCarriersPlansBenefitsResponseLocation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.ProductChoice")
    public JAXBElement<GetIfpQuoteRequestProductChoice> createGetIfpQuoteRequestProductChoice(GetIfpQuoteRequestProductChoice value) {
        return new JAXBElement<GetIfpQuoteRequestProductChoice>(_GetIfpQuoteRequestProductChoice_QNAME, GetIfpQuoteRequestProductChoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoRequestInputIsQuotableFilters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetZipCodeInfo.Request.Input.IsQuotableFilters")
    public JAXBElement<GetZipCodeInfoRequestInputIsQuotableFilters> createGetZipCodeInfoRequestInputIsQuotableFilters(GetZipCodeInfoRequestInputIsQuotableFilters value) {
        return new JAXBElement<GetZipCodeInfoRequestInputIsQuotableFilters>(_GetZipCodeInfoRequestInputIsQuotableFilters_QNAME, GetZipCodeInfoRequestInputIsQuotableFilters.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.Item")
    public JAXBElement<ArrayOfGetIfpQuoteResponseItem> createArrayOfGetIfpQuoteResponseItem(ArrayOfGetIfpQuoteResponseItem value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseItem>(_ArrayOfGetIfpQuoteResponseItem_QNAME, ArrayOfGetIfpQuoteResponseItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitIfpShoppingCartRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitIfpShoppingCart.Request")
    public JAXBElement<SubmitIfpShoppingCartRequest> createSubmitIfpShoppingCartRequest(SubmitIfpShoppingCartRequest value) {
        return new JAXBElement<SubmitIfpShoppingCartRequest>(_SubmitIfpShoppingCartRequest_QNAME, SubmitIfpShoppingCartRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteMemberPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.Quote.MemberPlan")
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteMemberPlan> createArrayOfGetIfpQuoteResponseQuoteMemberPlan(ArrayOfGetIfpQuoteResponseQuoteMemberPlan value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteMemberPlan>(_ArrayOfGetIfpQuoteResponseQuoteMemberPlan_QNAME, ArrayOfGetIfpQuoteResponseQuoteMemberPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCustomProductsQuote.Request.Preference.ProductFilter")
    public JAXBElement<ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter> createArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter(ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter>(_ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter_QNAME, ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "InsuranceCategory")
    public JAXBElement<List<String>> createInsuranceCategory(List<String> value) {
        return new JAXBElement<List<String>>(_InsuranceCategory_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarrierLoadHistoryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarrierLoadHistory.Request")
    public JAXBElement<GetCarrierLoadHistoryRequest> createGetCarrierLoadHistoryRequest(GetCarrierLoadHistoryRequest value) {
        return new JAXBElement<GetCarrierLoadHistoryRequest>(_GetCarrierLoadHistoryRequest_QNAME, GetCarrierLoadHistoryRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupPlanEligibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupPlanEligibility")
    public JAXBElement<GroupPlanEligibility> createGroupPlanEligibility(GroupPlanEligibility value) {
        return new JAXBElement<GroupPlanEligibility>(_GroupPlanEligibility_QNAME, GroupPlanEligibility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseDrugListDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.DrugList.Drug")
    public JAXBElement<GetCarriersPlansBenefitsResponseDrugListDrug> createGetCarriersPlansBenefitsResponseDrugListDrug(GetCarriersPlansBenefitsResponseDrugListDrug value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseDrugListDrug>(_GetCarriersPlansBenefitsResponseDrugListDrug_QNAME, GetCarriersPlansBenefitsResponseDrugListDrug.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseCarrierPlanPlanLink }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink")
    public JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlanPlanLink> createGetCarriersPlansBenefitsResponseCarrierPlanPlanLink(GetCarriersPlansBenefitsResponseCarrierPlanPlanLink value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlanPlanLink>(_GetCarriersPlansBenefitsResponseCarrierPlanPlanLink_QNAME, GetCarriersPlansBenefitsResponseCarrierPlanPlanLink.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroup.Response")
    public JAXBElement<GetGroupResponse> createGetGroupResponse(GetGroupResponse value) {
        return new JAXBElement<GetGroupResponse>(_GetGroupResponse_QNAME, GetGroupResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FamilyMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Family.Member")
    public JAXBElement<FamilyMember> createFamilyMember(FamilyMember value) {
        return new JAXBElement<FamilyMember>(_FamilyMember_QNAME, FamilyMember.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CoverageBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoverageBase")
    public JAXBElement<CoverageBase> createCoverageBase(CoverageBase value) {
        return new JAXBElement<CoverageBase>(_CoverageBase_QNAME, CoverageBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Item")
    public JAXBElement<GetCarriersPlansBenefitsResponseItem> createGetCarriersPlansBenefitsResponseItem(GetCarriersPlansBenefitsResponseItem value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseItem>(_GetCarriersPlansBenefitsResponseItem_QNAME, GetCarriersPlansBenefitsResponseItem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.Quote.DroppedCarrier.DroppedPlan")
    public JAXBElement<GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan> createGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan(GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan value) {
        return new JAXBElement<GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan>(_GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan_QNAME, GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanBusinessRuleAllowedRelation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfCustomProduct.Plan.BusinessRule.AllowedRelation")
    public JAXBElement<ArrayOfCustomProductPlanBusinessRuleAllowedRelation> createArrayOfCustomProductPlanBusinessRuleAllowedRelation(ArrayOfCustomProductPlanBusinessRuleAllowedRelation value) {
        return new JAXBElement<ArrayOfCustomProductPlanBusinessRuleAllowedRelation>(_ArrayOfCustomProductPlanBusinessRuleAllowedRelation_QNAME, ArrayOfCustomProductPlanBusinessRuleAllowedRelation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug")
    public JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> createArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug(ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug>(_ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug_QNAME, ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct")
    public JAXBElement<CustomProduct> createCustomProduct(CustomProduct value) {
        return new JAXBElement<CustomProduct>(_CustomProduct_QNAME, CustomProduct.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanLookupSequence }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfCustomProduct.Plan.LookupSequence")
    public JAXBElement<ArrayOfCustomProductPlanLookupSequence> createArrayOfCustomProductPlanLookupSequence(ArrayOfCustomProductPlanLookupSequence value) {
        return new JAXBElement<ArrayOfCustomProductPlanLookupSequence>(_ArrayOfCustomProductPlanLookupSequence_QNAME, ArrayOfCustomProductPlanLookupSequence.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitGroupShoppingCartRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitGroupShoppingCart.Request")
    public JAXBElement<SubmitGroupShoppingCartRequest> createSubmitGroupShoppingCartRequest(SubmitGroupShoppingCartRequest value) {
        return new JAXBElement<SubmitGroupShoppingCartRequest>(_SubmitGroupShoppingCartRequest_QNAME, SubmitGroupShoppingCartRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RatesGender }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RatesGender")
    public JAXBElement<RatesGender> createRatesGender(RatesGender value) {
        return new JAXBElement<RatesGender>(_RatesGender_QNAME, RatesGender.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Request.Preference.BenefitFilter")
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter> createArrayOfGetIfpQuoteRequestPreferenceBenefitFilter(ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter>(_ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter_QNAME, ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuoteEmployeePlanCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Response.Quote.EmployeePlan.Carrier")
    public JAXBElement<GetGroupQuoteResponseQuoteEmployeePlanCarrier> createGetGroupQuoteResponseQuoteEmployeePlanCarrier(GetGroupQuoteResponseQuoteEmployeePlanCarrier value) {
        return new JAXBElement<GetGroupQuoteResponseQuoteEmployeePlanCarrier>(_GetGroupQuoteResponseQuoteEmployeePlanCarrier_QNAME, GetGroupQuoteResponseQuoteEmployeePlanCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Request.BenefitFilter.AttributeFilter")
    public JAXBElement<GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter> createGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter(GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter>(_GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter_QNAME, GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage> createArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage>(_ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListRxRequestAccessKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ListRx.Request.AccessKey")
    public JAXBElement<ListRxRequestAccessKey> createListRxRequestAccessKey(ListRxRequestAccessKey value) {
        return new JAXBElement<ListRxRequestAccessKey>(_ListRxRequestAccessKey_QNAME, ListRxRequestAccessKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response")
    public JAXBElement<GetPlansbyRxResponse> createGetPlansbyRxResponse(GetPlansbyRxResponse value) {
        return new JAXBElement<GetPlansbyRxResponse>(_GetPlansbyRxResponse_QNAME, GetPlansbyRxResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarrierDetailsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierDetailsBase")
    public JAXBElement<CarrierDetailsBase> createCarrierDetailsBase(CarrierDetailsBase value) {
        return new JAXBElement<CarrierDetailsBase>(_CarrierDetailsBase_QNAME, CarrierDetailsBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestPreferenceBenefitFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetGroupQuote.Request.Preference.BenefitFilter")
    public JAXBElement<GetGroupQuoteRequestPreferenceBenefitFilter> createGetGroupQuoteRequestPreferenceBenefitFilter(GetGroupQuoteRequestPreferenceBenefitFilter value) {
        return new JAXBElement<GetGroupQuoteRequestPreferenceBenefitFilter>(_GetGroupQuoteRequestPreferenceBenefitFilter_QNAME, GetGroupQuoteRequestPreferenceBenefitFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Response.Quote.Carrier.Rate.Modified")
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified> createArrayOfGetIfpQuoteResponseQuoteCarrierRateModified(ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified>(_ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified_QNAME, ArrayOfGetIfpQuoteResponseQuoteCarrierRateModified.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.Preference.BenefitFilter")
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter> createArrayOfGetGroupQuoteRequestPreferenceBenefitFilter(ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter>(_ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter_QNAME, ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier> createArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier(ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier>(_ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier_QNAME, ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseQuoteCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.Quote.Carrier")
    public JAXBElement<GetIfpQuoteResponseQuoteCarrier> createGetIfpQuoteResponseQuoteCarrier(GetIfpQuoteResponseQuoteCarrier value) {
        return new JAXBElement<GetIfpQuoteResponseQuoteCarrier>(_GetIfpQuoteResponseQuoteCarrier_QNAME, GetIfpQuoteResponseQuoteCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductPlanLookupSequence }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct.Plan.LookupSequence")
    public JAXBElement<CustomProductPlanLookupSequence> createCustomProductPlanLookupSequence(CustomProductPlanLookupSequence value) {
        return new JAXBElement<CustomProductPlanLookupSequence>(_CustomProductPlanLookupSequence_QNAME, CustomProductPlanLookupSequence.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGroupContactContactAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGroup.Contact.ContactAttribute")
    public JAXBElement<ArrayOfGroupContactContactAttribute> createArrayOfGroupContactContactAttribute(ArrayOfGroupContactContactAttribute value) {
        return new JAXBElement<ArrayOfGroupContactContactAttribute>(_ArrayOfGroupContactContactAttribute_QNAME, ArrayOfGroupContactContactAttribute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CoverageTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoverageTier")
    public JAXBElement<CoverageTier> createCoverageTier(CoverageTier value) {
        return new JAXBElement<CoverageTier>(_CoverageTier_QNAME, CoverageTier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductPlanBusinessRuleCappingRule }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProduct.Plan.BusinessRule.CappingRule")
    public JAXBElement<CustomProductPlanBusinessRuleCappingRule> createCustomProductPlanBusinessRuleCappingRule(CustomProductPlanBusinessRuleCappingRule value) {
        return new JAXBElement<CustomProductPlanBusinessRuleCappingRule>(_CustomProductPlanBusinessRuleCappingRule_QNAME, CustomProductPlanBusinessRuleCappingRule.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpShoppingCartProductChoicePlanChoicesStatusChange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpShoppingCart.ProductChoice.PlanChoices.StatusChange")
    public JAXBElement<IfpShoppingCartProductChoicePlanChoicesStatusChange> createIfpShoppingCartProductChoicePlanChoicesStatusChange(IfpShoppingCartProductChoicePlanChoicesStatusChange value) {
        return new JAXBElement<IfpShoppingCartProductChoicePlanChoicesStatusChange>(_IfpShoppingCartProductChoicePlanChoicesStatusChange_QNAME, IfpShoppingCartProductChoicePlanChoicesStatusChange.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestIfpRateKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.IfpRateKey")
    public JAXBElement<GetIfpQuoteRequestIfpRateKey> createGetIfpQuoteRequestIfpRateKey(GetIfpQuoteRequestIfpRateKey value) {
        return new JAXBElement<GetIfpQuoteRequestIfpRateKey>(_GetIfpQuoteRequestIfpRateKey_QNAME, GetIfpQuoteRequestIfpRateKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.AddOn")
    public JAXBElement<ArrayOfGetGroupQuoteAddOn> createArrayOfGetGroupQuoteAddOn(ArrayOfGetGroupQuoteAddOn value) {
        return new JAXBElement<ArrayOfGetGroupQuoteAddOn>(_ArrayOfGetGroupQuoteAddOn_QNAME, ArrayOfGetGroupQuoteAddOn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFamilyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetFamily.Request")
    public JAXBElement<GetFamilyRequest> createGetFamilyRequest(GetFamilyRequest value) {
        return new JAXBElement<GetFamilyRequest>(_GetFamilyRequest_QNAME, GetFamilyRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgeSort }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AgeSort")
    public JAXBElement<AgeSort> createAgeSort(AgeSort value) {
        return new JAXBElement<AgeSort>(_AgeSort_QNAME, AgeSort.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContributionType")
    public JAXBElement<List<String>> createContributionType(List<String> value) {
        return new JAXBElement<List<String>>(_ContributionType_QNAME, ((Class) List.class), null, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier")
    public JAXBElement<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier(GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier value) {
        return new JAXBElement<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier_QNAME, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPlanVisibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.PlanVisibility")
    public JAXBElement<GetIfpQuoteRequestPlanVisibility> createGetIfpQuoteRequestPlanVisibility(GetIfpQuoteRequestPlanVisibility value) {
        return new JAXBElement<GetIfpQuoteRequestPlanVisibility>(_GetIfpQuoteRequestPlanVisibility_QNAME, GetIfpQuoteRequestPlanVisibility.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage")
    public JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage> createArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage(ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage>(_ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage_QNAME, ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccessKeyBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AccessKeyBase")
    public JAXBElement<AccessKeyBase> createAccessKeyBase(AccessKeyBase value) {
        return new JAXBElement<AccessKeyBase>(_AccessKeyBase_QNAME, AccessKeyBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarrierLoadHistoryResponseLoadEventCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarrierLoadHistory.Response.LoadEvent.Carrier")
    public JAXBElement<GetCarrierLoadHistoryResponseLoadEventCarrier> createGetCarrierLoadHistoryResponseLoadEventCarrier(GetCarrierLoadHistoryResponseLoadEventCarrier value) {
        return new JAXBElement<GetCarrierLoadHistoryResponseLoadEventCarrier>(_GetCarrierLoadHistoryResponseLoadEventCarrier_QNAME, GetCarrierLoadHistoryResponseLoadEventCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Contact.ContactPoint")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint> createArrayOfGetCarriersPlansBenefitsResponseContactContactPoint(ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint>(_ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint_QNAME, ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrierPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Response.Carrier.Plan")
    public JAXBElement<GetPlansbyRxResponseCarrierPlan> createGetPlansbyRxResponseCarrierPlan(GetPlansbyRxResponseCarrierPlan value) {
        return new JAXBElement<GetPlansbyRxResponseCarrierPlan>(_GetPlansbyRxResponseCarrierPlan_QNAME, GetPlansbyRxResponseCarrierPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Response.Quote.DroppedCarrier.DroppedPlan")
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan> createArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan(ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan>(_ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan_QNAME, ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter.AttributeFilter")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter> createArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter(ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter>(_ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter_QNAME, ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfListRxRequestPlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfListRx.Request.PlanFilter")
    public JAXBElement<ArrayOfListRxRequestPlanFilter> createArrayOfListRxRequestPlanFilter(ArrayOfListRxRequestPlanFilter value) {
        return new JAXBElement<ArrayOfListRxRequestPlanFilter>(_ArrayOfListRxRequestPlanFilter_QNAME, ArrayOfListRxRequestPlanFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgeCalculationBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AgeCalculationBase")
    public JAXBElement<AgeCalculationBase> createAgeCalculationBase(AgeCalculationBase value) {
        return new JAXBElement<AgeCalculationBase>(_AgeCalculationBase_QNAME, AgeCalculationBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Request.Preference.ChoiceFilter")
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter> createArrayOfGetIfpQuoteRequestPreferenceChoiceFilter(ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter>(_ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter_QNAME, ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpRatingAreaBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpRatingAreaBase")
    public JAXBElement<IfpRatingAreaBase> createIfpRatingAreaBase(IfpRatingAreaBase value) {
        return new JAXBElement<IfpRatingAreaBase>(_IfpRatingAreaBase_QNAME, IfpRatingAreaBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseLocation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Location")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseLocation> createArrayOfGetCarriersPlansBenefitsResponseLocation(ArrayOfGetCarriersPlansBenefitsResponseLocation value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseLocation>(_ArrayOfGetCarriersPlansBenefitsResponseLocation_QNAME, ArrayOfGetCarriersPlansBenefitsResponseLocation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CoverageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoverageType")
    public JAXBElement<CoverageType> createCoverageType(CoverageType value) {
        return new JAXBElement<CoverageType>(_CoverageType_QNAME, CoverageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountiesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCounties.Response")
    public JAXBElement<GetCountiesResponse> createGetCountiesResponse(GetCountiesResponse value) {
        return new JAXBElement<GetCountiesResponse>(_GetCountiesResponse_QNAME, GetCountiesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPreferenceBenefitFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.Preference.BenefitFilter")
    public JAXBElement<GetIfpQuoteRequestPreferenceBenefitFilter> createGetIfpQuoteRequestPreferenceBenefitFilter(GetIfpQuoteRequestPreferenceBenefitFilter value) {
        return new JAXBElement<GetIfpQuoteRequestPreferenceBenefitFilter>(_GetIfpQuoteRequestPreferenceBenefitFilter_QNAME, GetIfpQuoteRequestPreferenceBenefitFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetZipCodeInfo.Request")
    public JAXBElement<GetZipCodeInfoRequest> createGetZipCodeInfoRequest(GetZipCodeInfoRequest value) {
        return new JAXBElement<GetZipCodeInfoRequest>(_GetZipCodeInfoRequest_QNAME, GetZipCodeInfoRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier")
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrier> createArrayOfGetCarriersPlansBenefitsResponseCarrier(ArrayOfGetCarriersPlansBenefitsResponseCarrier value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrier>(_ArrayOfGetCarriersPlansBenefitsResponseCarrier_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrier.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPreferenceVersionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Request.Preference.VersionFilter")
    public JAXBElement<GetIfpQuoteRequestPreferenceVersionFilter> createGetIfpQuoteRequestPreferenceVersionFilter(GetIfpQuoteRequestPreferenceVersionFilter value) {
        return new JAXBElement<GetIfpQuoteRequestPreferenceVersionFilter>(_GetIfpQuoteRequestPreferenceVersionFilter_QNAME, GetIfpQuoteRequestPreferenceVersionFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.AddOn")
    public JAXBElement<ArrayOfGetIfpQuoteAddOn> createArrayOfGetIfpQuoteAddOn(ArrayOfGetIfpQuoteAddOn value) {
        return new JAXBElement<ArrayOfGetIfpQuoteAddOn>(_ArrayOfGetIfpQuoteAddOn_QNAME, ArrayOfGetIfpQuoteAddOn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RatingAreaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RatingAreaType")
    public JAXBElement<RatingAreaType> createRatingAreaType(RatingAreaType value) {
        return new JAXBElement<RatingAreaType>(_RatingAreaType_QNAME, RatingAreaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage")
    public JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage> createGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage(GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage>(_GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage_QNAME, GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequestCarrierPlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Request.CarrierPlanFilter")
    public JAXBElement<GetCarriersPlansBenefitsRequestCarrierPlanFilter> createGetCarriersPlansBenefitsRequestCarrierPlanFilter(GetCarriersPlansBenefitsRequestCarrierPlanFilter value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequestCarrierPlanFilter>(_GetCarriersPlansBenefitsRequestCarrierPlanFilter_QNAME, GetCarriersPlansBenefitsRequestCarrierPlanFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO")
    public JAXBElement<FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO(FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO value) {
        return new JAXBElement<FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO_QNAME, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupRequestDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Request.Drug")
    public JAXBElement<FormularyLookupRequestDrug> createFormularyLookupRequestDrug(FormularyLookupRequestDrug value) {
        return new JAXBElement<FormularyLookupRequestDrug>(_FormularyLookupRequestDrug_QNAME, FormularyLookupRequestDrug.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarrierLoadHistoryResponseLoadEvent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetCarrierLoadHistory.Response.LoadEvent")
    public JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEvent> createArrayOfGetCarrierLoadHistoryResponseLoadEvent(ArrayOfGetCarrierLoadHistoryResponseLoadEvent value) {
        return new JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEvent>(_ArrayOfGetCarrierLoadHistoryResponseLoadEvent_QNAME, ArrayOfGetCarrierLoadHistoryResponseLoadEvent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetIfpQuote.Request.Preference.BenchmarkRules.PlanComponent")
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent> createArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent(ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent>(_ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent_QNAME, ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PlanOffering }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanOffering")
    public JAXBElement<PlanOffering> createPlanOffering(PlanOffering value) {
        return new JAXBElement<PlanOffering>(_PlanOffering_QNAME, PlanOffering.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarrierBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierBase")
    public JAXBElement<CarrierBase> createCarrierBase(CarrierBase value) {
        return new JAXBElement<CarrierBase>(_CarrierBase_QNAME, CarrierBase.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseQuoteMemberPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetIfpQuote.Response.Quote.MemberPlan")
    public JAXBElement<GetIfpQuoteResponseQuoteMemberPlan> createGetIfpQuoteResponseQuoteMemberPlan(GetIfpQuoteResponseQuoteMemberPlan value) {
        return new JAXBElement<GetIfpQuoteResponseQuoteMemberPlan>(_GetIfpQuoteResponseQuoteMemberPlan_QNAME, GetIfpQuoteResponseQuoteMemberPlan.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarrierLoadHistoryResponseLoadEvent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarrierLoadHistory.Response.LoadEvent")
    public JAXBElement<GetCarrierLoadHistoryResponseLoadEvent> createGetCarrierLoadHistoryResponseLoadEvent(GetCarrierLoadHistoryResponseLoadEvent value) {
        return new JAXBElement<GetCarrierLoadHistoryResponseLoadEvent>(_GetCarrierLoadHistoryResponseLoadEvent_QNAME, GetCarrierLoadHistoryResponseLoadEvent.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequestVersionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetCarriersPlansBenefits.Request.VersionFilter")
    public JAXBElement<GetCarriersPlansBenefitsRequestVersionFilter> createGetCarriersPlansBenefitsRequestVersionFilter(GetCarriersPlansBenefitsRequestVersionFilter value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequestVersionFilter>(_GetCarriersPlansBenefitsRequestVersionFilter_QNAME, GetCarriersPlansBenefitsRequestVersionFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupShoppingCart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupShoppingCart")
    public JAXBElement<GroupShoppingCart> createGroupShoppingCart(GroupShoppingCart value) {
        return new JAXBElement<GroupShoppingCart>(_GroupShoppingCart_QNAME, GroupShoppingCart.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmissionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmissionType")
    public JAXBElement<SubmissionType> createSubmissionType(SubmissionType value) {
        return new JAXBElement<SubmissionType>(_SubmissionType_QNAME, SubmissionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIfpShoppingCartProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfIfpShoppingCart.ProductChoice")
    public JAXBElement<ArrayOfIfpShoppingCartProductChoice> createArrayOfIfpShoppingCartProductChoice(ArrayOfIfpShoppingCartProductChoice value) {
        return new JAXBElement<ArrayOfIfpShoppingCartProductChoice>(_ArrayOfIfpShoppingCartProductChoice_QNAME, ArrayOfIfpShoppingCartProductChoice.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxRequestInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GetPlansbyRx.Request.Input")
    public JAXBElement<GetPlansbyRxRequestInput> createGetPlansbyRxRequestInput(GetPlansbyRxRequestInput value) {
        return new JAXBElement<GetPlansbyRxRequestInput>(_GetPlansbyRxRequestInput_QNAME, GetPlansbyRxRequestInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitGroupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmitGroup.Request")
    public JAXBElement<SubmitGroupRequest> createSubmitGroupRequest(SubmitGroupRequest value) {
        return new JAXBElement<SubmitGroupRequest>(_SubmitGroupRequest_QNAME, SubmitGroupRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee")
    public JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee> createArrayOfGetGroupQuoteRequestGroupRateFactorEmployee(ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee>(_ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee_QNAME, ArrayOfGetGroupQuoteRequestGroupRateFactorEmployee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArrayOfGetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter")
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter> createArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter(ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter>(_ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter_QNAME, ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupRequestAccessKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyLookup.Request.AccessKey")
    public JAXBElement<FormularyLookupRequestAccessKey> createFormularyLookupRequestAccessKey(FormularyLookupRequestAccessKey value) {
        return new JAXBElement<FormularyLookupRequestAccessKey>(_FormularyLookupRequestAccessKey_QNAME, FormularyLookupRequestAccessKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Value", scope = ItemBase.class)
    public JAXBElement<String> createItemBaseValue(String value) {
        return new JAXBElement<String>(_ItemBaseValue_QNAME, String.class, ItemBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Attribute", scope = ItemBase.class)
    public JAXBElement<String> createItemBaseAttribute(String value) {
        return new JAXBElement<String>(_ItemBaseAttribute_QNAME, String.class, ItemBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmissionId", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier.class)
    public JAXBElement<Integer> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierSubmissionId(Integer value) {
        return new JAXBElement<Integer>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierSubmissionId_QNAME, Integer.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanRates", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier.class)
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRates(ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRates_QNAME, ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CompanyName", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier.class)
    public JAXBElement<String> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierCompanyName(String value) {
        return new JAXBElement<String>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierCompanyName_QNAME, String.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarrierLoadHistoryResponseLoadEvent }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LoadEvents", scope = GetCarrierLoadHistoryResponse.class)
    public JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEvent> createGetCarrierLoadHistoryResponseLoadEvents(ArrayOfGetCarrierLoadHistoryResponseLoadEvent value) {
        return new JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEvent>(_GetCarrierLoadHistoryResponseLoadEvents_QNAME, ArrayOfGetCarrierLoadHistoryResponseLoadEvent.class, GetCarrierLoadHistoryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ViewPointType", scope = GetGroupQuoteRequestPreferenceBenefitFilter.class)
    public JAXBElement<String> createGetGroupQuoteRequestPreferenceBenefitFilterViewPointType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitFilterViewPointType_QNAME, String.class, GetGroupQuoteRequestPreferenceBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoverageType", scope = GetGroupQuoteRequestPreferenceBenefitFilter.class)
    public JAXBElement<String> createGetGroupQuoteRequestPreferenceBenefitFilterCoverageType(String value) {
        return new JAXBElement<String>(_CoverageType_QNAME, String.class, GetGroupQuoteRequestPreferenceBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitEnum", scope = GetGroupQuoteRequestPreferenceBenefitFilter.class)
    public JAXBElement<String> createGetGroupQuoteRequestPreferenceBenefitFilterBenefitEnum(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitFilterBenefitEnum_QNAME, String.class, GetGroupQuoteRequestPreferenceBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AttributeFilters", scope = GetGroupQuoteRequestPreferenceBenefitFilter.class)
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter> createGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilters(ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter>(_GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilters_QNAME, ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter.class, GetGroupQuoteRequestPreferenceBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ServiceType", scope = GetGroupQuoteRequestPreferenceBenefitFilter.class)
    public JAXBElement<String> createGetGroupQuoteRequestPreferenceBenefitFilterServiceType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitFilterServiceType_QNAME, String.class, GetGroupQuoteRequestPreferenceBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChangeCodes", scope = MemberBase.class)
    public JAXBElement<String> createMemberBaseChangeCodes(String value) {
        return new JAXBElement<String>(_MemberBaseChangeCodes_QNAME, String.class, MemberBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MembershipDate", scope = MemberBase.class)
    public JAXBElement<XMLGregorianCalendar> createMemberBaseMembershipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MemberBaseMembershipDate_QNAME, XMLGregorianCalendar.class, MemberBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DateLastSmoked", scope = MemberBase.class)
    public JAXBElement<XMLGregorianCalendar> createMemberBaseDateLastSmoked(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MemberBaseDateLastSmoked_QNAME, XMLGregorianCalendar.class, MemberBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PartnerData", scope = MemberBase.class)
    public JAXBElement<String> createMemberBasePartnerData(String value) {
        return new JAXBElement<String>(_MemberBasePartnerData_QNAME, String.class, MemberBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IsSmoker", scope = MemberBase.class)
    public JAXBElement<Boolean> createMemberBaseIsSmoker(Boolean value) {
        return new JAXBElement<Boolean>(_MemberBaseIsSmoker_QNAME, Boolean.class, MemberBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FirstName", scope = MemberBase.class)
    public JAXBElement<String> createMemberBaseFirstName(String value) {
        return new JAXBElement<String>(_MemberBaseFirstName_QNAME, String.class, MemberBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyName", scope = MemberBase.class)
    public JAXBElement<String> createMemberBaseCountyName(String value) {
        return new JAXBElement<String>(_MemberBaseCountyName_QNAME, String.class, MemberBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LastName", scope = MemberBase.class)
    public JAXBElement<String> createMemberBaseLastName(String value) {
        return new JAXBElement<String>(_MemberBaseLastName_QNAME, String.class, MemberBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZipCode", scope = MemberBase.class)
    public JAXBElement<Integer> createMemberBaseZipCode(Integer value) {
        return new JAXBElement<Integer>(_MemberBaseZipCode_QNAME, Integer.class, MemberBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugPrescriptionPeriodType", scope = GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVODrugPrescriptionPeriodType(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVODrugPrescriptionPeriodType_QNAME, String.class, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CostSharingType", scope = GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCostSharingType(String value) {
        return new JAXBElement<String>(_CostSharingType_QNAME, String.class, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoInsurance", scope = GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCoInsurance(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCoInsurance_QNAME, String.class, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoPayment", scope = GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCoPayment(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCoPayment_QNAME, String.class, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NetworkCostType", scope = GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVONetworkCostType(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVONetworkCostType_QNAME, String.class, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupShoppingCartsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ShoppingCarts", scope = SubmitGroupResponse.class)
    public JAXBElement<GroupShoppingCartsBase> createSubmitGroupResponseShoppingCarts(GroupShoppingCartsBase value) {
        return new JAXBElement<GroupShoppingCartsBase>(_SubmitGroupResponseShoppingCarts_QNAME, GroupShoppingCartsBase.class, SubmitGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Group }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Group", scope = GetGroupResponse.class)
    public JAXBElement<Group> createGetGroupResponseGroup(Group value) {
        return new JAXBElement<Group>(_Group_QNAME, Group.class, GetGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugName", scope = ListRxResponseDrug.class)
    public JAXBElement<String> createListRxResponseDrugDrugName(String value) {
        return new JAXBElement<String>(_ListRxResponseDrugDrugName_QNAME, String.class, ListRxResponseDrug.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice.class)
    public JAXBElement<XMLGregorianCalendar> createGetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanChoice", scope = GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice.class)
    public JAXBElement<GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices> createGetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoice(GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoice_QNAME, GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices.class, GetGroupQuoteRequestGroupRateFactorEmployeeProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteCarrierRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Carriers", scope = GetGroupQuoteResponseQuote.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRate> createGetGroupQuoteResponseQuoteCarriers(ArrayOfGetGroupQuoteResponseQuoteCarrierRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRate>(_GetGroupQuoteResponseQuoteCarriers_QNAME, ArrayOfGetGroupQuoteResponseQuoteCarrierRate.class, GetGroupQuoteResponseQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedRates", scope = GetGroupQuoteResponseQuote.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier> createGetGroupQuoteResponseQuoteDroppedRates(ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier>(_GetGroupQuoteResponseQuoteDroppedRates_QNAME, ArrayOfGetGroupQuoteResponseQuoteDroppedCarrier.class, GetGroupQuoteResponseQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EmployeePlans", scope = GetGroupQuoteResponseQuote.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlan> createGetGroupQuoteResponseQuoteEmployeePlans(ArrayOfGetGroupQuoteResponseQuoteEmployeePlan value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlan>(_GetGroupQuoteResponseQuoteEmployeePlans_QNAME, ArrayOfGetGroupQuoteResponseQuoteEmployeePlan.class, GetGroupQuoteResponseQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitData", scope = GetGroupQuoteResponsePlanDetailBenefit.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseItem> createGetGroupQuoteResponsePlanDetailBenefitBenefitData(ArrayOfGetGroupQuoteResponseItem value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseItem>(_GetGroupQuoteResponsePlanDetailBenefitBenefitData_QNAME, ArrayOfGetGroupQuoteResponseItem.class, GetGroupQuoteResponsePlanDetailBenefit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Coverages", scope = GetGroupQuoteResponsePlanDetailBenefit.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage> createGetGroupQuoteResponsePlanDetailBenefitCoverages(ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage>(_GetGroupQuoteResponsePlanDetailBenefitCoverages_QNAME, ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage.class, GetGroupQuoteResponsePlanDetailBenefit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Hint", scope = Error.class)
    public JAXBElement<String> createErrorHint(String value) {
        return new JAXBElement<String>(_ErrorHint_QNAME, String.class, Error.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WorksiteCountyFIPS", scope = GetCustomProductsQuoteRequestCustomRateFactorFamily.class)
    public JAXBElement<String> createGetCustomProductsQuoteRequestCustomRateFactorFamilyWorksiteCountyFIPS(String value) {
        return new JAXBElement<String>(_GetCustomProductsQuoteRequestCustomRateFactorFamilyWorksiteCountyFIPS_QNAME, String.class, GetCustomProductsQuoteRequestCustomRateFactorFamily.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RxCUI", scope = FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugRxCUI(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugRxCUI_QNAME, String.class, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfInsuranceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "InsuranceTypes", scope = GetZipCodeInfoRequestInputIsQuotableFilters.class)
    public JAXBElement<ArrayOfInsuranceType> createGetZipCodeInfoRequestInputIsQuotableFiltersInsuranceTypes(ArrayOfInsuranceType value) {
        return new JAXBElement<ArrayOfInsuranceType>(_GetZipCodeInfoRequestInputIsQuotableFiltersInsuranceTypes_QNAME, ArrayOfInsuranceType.class, GetZipCodeInfoRequestInputIsQuotableFilters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanIds", scope = GetZipCodeInfoRequestInputIsQuotableFilters.class)
    public JAXBElement<ArrayOfstring> createGetZipCodeInfoRequestInputIsQuotableFiltersPlanIds(ArrayOfstring value) {
        return new JAXBElement<ArrayOfstring>(_GetZipCodeInfoRequestInputIsQuotableFiltersPlanIds_QNAME, ArrayOfstring.class, GetZipCodeInfoRequestInputIsQuotableFilters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierIds", scope = GetZipCodeInfoRequestInputIsQuotableFilters.class)
    public JAXBElement<ArrayOfint> createGetZipCodeInfoRequestInputIsQuotableFiltersCarrierIds(ArrayOfint value) {
        return new JAXBElement<ArrayOfint>(_GetZipCodeInfoRequestInputIsQuotableFiltersCarrierIds_QNAME, ArrayOfint.class, GetZipCodeInfoRequestInputIsQuotableFilters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = GetZipCodeInfoRequestInputIsQuotableFilters.class)
    public JAXBElement<XMLGregorianCalendar> createGetZipCodeInfoRequestInputIsQuotableFiltersEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, GetZipCodeInfoRequestInputIsQuotableFilters.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RatingAnnotation", scope = RateBase.class)
    public JAXBElement<String> createRateBaseRatingAnnotation(String value) {
        return new JAXBElement<String>(_RateBaseRatingAnnotation_QNAME, String.class, RateBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitTemplate", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<String> createGetGroupQuoteRequestPreferenceBenefitTemplate(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitTemplate_QNAME, String.class, GetGroupQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AsOfDate", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<XMLGregorianCalendar> createGetGroupQuoteRequestPreferenceAsOfDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestPreferenceAsOfDate_QNAME, XMLGregorianCalendar.class, GetGroupQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RateSet", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<List<String>> createGetGroupQuoteRequestPreferenceRateSet(List<String> value) {
        return new JAXBElement<List<String>>(_RateSet_QNAME, ((Class) List.class), GetGroupQuoteRequestPreference.class, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChoiceFilters", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter> createGetGroupQuoteRequestPreferenceChoiceFilters(ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter>(_GetGroupQuoteRequestPreferenceChoiceFilters_QNAME, ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter.class, GetGroupQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AddOns", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetGroupQuoteAddOn> createGetGroupQuoteRequestPreferenceAddOns(ArrayOfGetGroupQuoteAddOn value) {
        return new JAXBElement<ArrayOfGetGroupQuoteAddOn>(_GetGroupQuoteRequestPreferenceAddOns_QNAME, ArrayOfGetGroupQuoteAddOn.class, GetGroupQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceDrugFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugFilters", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceDrugFilter> createGetGroupQuoteRequestPreferenceDrugFilters(ArrayOfGetGroupQuoteRequestPreferenceDrugFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceDrugFilter>(_GetGroupQuoteRequestPreferenceDrugFilters_QNAME, ArrayOfGetGroupQuoteRequestPreferenceDrugFilter.class, GetGroupQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferencePlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanFilters", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferencePlanFilter> createGetGroupQuoteRequestPreferencePlanFilters(ArrayOfGetGroupQuoteRequestPreferencePlanFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferencePlanFilter>(_GetGroupQuoteRequestPreferencePlanFilters_QNAME, ArrayOfGetGroupQuoteRequestPreferencePlanFilter.class, GetGroupQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "QuoteState", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<String> createGetGroupQuoteRequestPreferenceQuoteState(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceQuoteState_QNAME, String.class, GetGroupQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitFilters", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter> createGetGroupQuoteRequestPreferenceBenefitFilters(ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter>(_GetGroupQuoteRequestPreferenceBenefitFilters_QNAME, ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter.class, GetGroupQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LevelOfBenefitAddOns }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LevelOfBenefitAddOns", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<LevelOfBenefitAddOns> createGetGroupQuoteRequestPreferenceLevelOfBenefitAddOns(LevelOfBenefitAddOns value) {
        return new JAXBElement<LevelOfBenefitAddOns>(_LevelOfBenefitAddOns_QNAME, LevelOfBenefitAddOns.class, GetGroupQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceVersionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VersionFilters", scope = GetGroupQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceVersionFilter> createGetGroupQuoteRequestPreferenceVersionFilters(ArrayOfGetGroupQuoteRequestPreferenceVersionFilter value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceVersionFilter>(_GetGroupQuoteRequestPreferenceVersionFilters_QNAME, ArrayOfGetGroupQuoteRequestPreferenceVersionFilter.class, GetGroupQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Carriers", scope = GetGroupQuoteResponseQuoteEmployeePlan.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier> createGetGroupQuoteResponseQuoteEmployeePlanCarriers(ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier>(_GetGroupQuoteResponseQuoteCarriers_QNAME, ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier.class, GetGroupQuoteResponseQuoteEmployeePlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "State", scope = LocationAddressBase.class)
    public JAXBElement<String> createLocationAddressBaseState(String value) {
        return new JAXBElement<String>(_LocationAddressBaseState_QNAME, String.class, LocationAddressBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Address", scope = LocationAddressBase.class)
    public JAXBElement<String> createLocationAddressBaseAddress(String value) {
        return new JAXBElement<String>(_LocationAddressBaseAddress_QNAME, String.class, LocationAddressBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyName", scope = LocationAddressBase.class)
    public JAXBElement<String> createLocationAddressBaseCountyName(String value) {
        return new JAXBElement<String>(_MemberBaseCountyName_QNAME, String.class, LocationAddressBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "City", scope = LocationAddressBase.class)
    public JAXBElement<String> createLocationAddressBaseCity(String value) {
        return new JAXBElement<String>(_LocationAddressBaseCity_QNAME, String.class, LocationAddressBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RatingAnnotation", scope = EmployeeRateBase.class)
    public JAXBElement<String> createEmployeeRateBaseRatingAnnotation(String value) {
        return new JAXBElement<String>(_RateBaseRatingAnnotation_QNAME, String.class, EmployeeRateBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Rates", scope = GetGroupQuoteResponseQuoteEmployeePlanCarrier.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate> createGetGroupQuoteResponseQuoteEmployeePlanCarrierRates(ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRates_QNAME, ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrierRate.class, GetGroupQuoteResponseQuoteEmployeePlanCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpShoppingCart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ShoppingCart", scope = SubmitIfpShoppingCartResponse.class)
    public JAXBElement<IfpShoppingCart> createSubmitIfpShoppingCartResponseShoppingCart(IfpShoppingCart value) {
        return new JAXBElement<IfpShoppingCart>(_SubmitIfpShoppingCartResponseShoppingCart_QNAME, IfpShoppingCart.class, SubmitIfpShoppingCartResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "State", scope = FetchCustomProductsRequestInputProductFilter.class)
    public JAXBElement<String> createFetchCustomProductsRequestInputProductFilterState(String value) {
        return new JAXBElement<String>(_LocationAddressBaseState_QNAME, String.class, FetchCustomProductsRequestInputProductFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductType", scope = FetchCustomProductsRequestInputProductFilter.class)
    public JAXBElement<String> createFetchCustomProductsRequestInputProductFilterProductType(String value) {
        return new JAXBElement<String>(_FetchCustomProductsRequestInputProductFilterProductType_QNAME, String.class, FetchCustomProductsRequestInputProductFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierId", scope = FetchCustomProductsRequestInputProductFilter.class)
    public JAXBElement<String> createFetchCustomProductsRequestInputProductFilterCarrierId(String value) {
        return new JAXBElement<String>(_FetchCustomProductsRequestInputProductFilterCarrierId_QNAME, String.class, FetchCustomProductsRequestInputProductFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = FetchCustomProductsRequestInputProductFilter.class)
    public JAXBElement<XMLGregorianCalendar> createFetchCustomProductsRequestInputProductFilterEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, FetchCustomProductsRequestInputProductFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Carriers", scope = GetPlansbyRxResponse.class)
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrier> createGetPlansbyRxResponseCarriers(ArrayOfGetPlansbyRxResponseCarrier value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrier>(_GetGroupQuoteResponseQuoteCarriers_QNAME, ArrayOfGetPlansbyRxResponseCarrier.class, GetPlansbyRxResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Value2", scope = ContactPointBase.class)
    public JAXBElement<String> createContactPointBaseValue2(String value) {
        return new JAXBElement<String>(_ContactPointBaseValue2_QNAME, String.class, ContactPointBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseMemberRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MemberRates", scope = GetGroupQuoteResponseQuoteEmployeePlanCarrierRate.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseMemberRate> createGetGroupQuoteResponseQuoteEmployeePlanCarrierRateMemberRates(ArrayOfGetGroupQuoteResponseMemberRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseMemberRate>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRateMemberRates_QNAME, ArrayOfGetGroupQuoteResponseMemberRate.class, GetGroupQuoteResponseQuoteEmployeePlanCarrierRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoveredRxCUIs", scope = GetGroupQuoteResponseQuoteEmployeePlanCarrierRate.class)
    public JAXBElement<RxCUIsBase> createGetGroupQuoteResponseQuoteEmployeePlanCarrierRateCoveredRxCUIs(RxCUIsBase value) {
        return new JAXBElement<RxCUIsBase>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRateCoveredRxCUIs_QNAME, RxCUIsBase.class, GetGroupQuoteResponseQuoteEmployeePlanCarrierRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ViewPoints", scope = GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint> createGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoints(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint>(_GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoints_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint.class, GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuote }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupQuote", scope = GetGroupQuoteResponse.class)
    public JAXBElement<GetGroupQuoteResponseQuote> createGetGroupQuoteResponseGroupQuote(GetGroupQuoteResponseQuote value) {
        return new JAXBElement<GetGroupQuoteResponseQuote>(_GetGroupQuoteResponseGroupQuote_QNAME, GetGroupQuoteResponseQuote.class, GetGroupQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Carriers", scope = FormularyLookupResponse.class)
    public JAXBElement<ArrayOfFormularyLookupResponseCarrier> createFormularyLookupResponseCarriers(ArrayOfFormularyLookupResponseCarrier value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseCarrier>(_GetGroupQuoteResponseQuoteCarriers_QNAME, ArrayOfFormularyLookupResponseCarrier.class, FormularyLookupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseDrugAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugAttributes", scope = FormularyLookupResponse.class)
    public JAXBElement<ArrayOfFormularyLookupResponseDrugAttribute> createFormularyLookupResponseDrugAttributes(ArrayOfFormularyLookupResponseDrugAttribute value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseDrugAttribute>(_FormularyLookupResponseDrugAttributes_QNAME, ArrayOfFormularyLookupResponseDrugAttribute.class, FormularyLookupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BrokerId", scope = QuoteRequestBase.class)
    public JAXBElement<Integer> createQuoteRequestBaseBrokerId(Integer value) {
        return new JAXBElement<Integer>(_QuoteRequestBaseBrokerId_QNAME, Integer.class, QuoteRequestBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = GroupShoppingCartProductChoice.class)
    public JAXBElement<XMLGregorianCalendar> createGroupShoppingCartProductChoiceEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, GroupShoppingCartProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CoverageTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoverageTier", scope = GroupShoppingCartProductChoice.class)
    public JAXBElement<CoverageTier> createGroupShoppingCartProductChoiceCoverageTier(CoverageTier value) {
        return new JAXBElement<CoverageTier>(_CoverageTier_QNAME, CoverageTier.class, GroupShoppingCartProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupShoppingCartProductChoicePlanChoices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanChoice", scope = GroupShoppingCartProductChoice.class)
    public JAXBElement<GroupShoppingCartProductChoicePlanChoices> createGroupShoppingCartProductChoicePlanChoice(GroupShoppingCartProductChoicePlanChoices value) {
        return new JAXBElement<GroupShoppingCartProductChoicePlanChoices>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoice_QNAME, GroupShoppingCartProductChoicePlanChoices.class, GroupShoppingCartProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EmployeeId", scope = GroupShoppingCartProductChoice.class)
    public JAXBElement<String> createGroupShoppingCartProductChoiceEmployeeId(String value) {
        return new JAXBElement<String>(_GroupShoppingCartProductChoiceEmployeeId_QNAME, String.class, GroupShoppingCartProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NonSmokerRateOnly", scope = GetGroupQuoteRequestGroupRateFactorScenario.class)
    public JAXBElement<Boolean> createGetGroupQuoteRequestGroupRateFactorScenarioNonSmokerRateOnly(Boolean value) {
        return new JAXBElement<Boolean>(_GetGroupQuoteRequestGroupRateFactorScenarioNonSmokerRateOnly_QNAME, Boolean.class, GetGroupQuoteRequestGroupRateFactorScenario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DepContributionType", scope = GetGroupQuoteRequestGroupRateFactorScenario.class)
    public JAXBElement<List<String>> createGetGroupQuoteRequestGroupRateFactorScenarioDepContributionType(List<String> value) {
        return new JAXBElement<List<String>>(_GetGroupQuoteRequestGroupRateFactorScenarioDepContributionType_QNAME, ((Class) List.class), GetGroupQuoteRequestGroupRateFactorScenario.class, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SponsoredPlanId", scope = GetGroupQuoteRequestGroupRateFactorScenario.class)
    public JAXBElement<String> createGetGroupQuoteRequestGroupRateFactorScenarioSponsoredPlanId(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioSponsoredPlanId_QNAME, String.class, GetGroupQuoteRequestGroupRateFactorScenario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NonSmokerDepRateOnly", scope = GetGroupQuoteRequestGroupRateFactorScenario.class)
    public JAXBElement<Boolean> createGetGroupQuoteRequestGroupRateFactorScenarioNonSmokerDepRateOnly(Boolean value) {
        return new JAXBElement<Boolean>(_GetGroupQuoteRequestGroupRateFactorScenarioNonSmokerDepRateOnly_QNAME, Boolean.class, GetGroupQuoteRequestGroupRateFactorScenario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Note", scope = GetGroupQuoteRequestGroupRateFactorScenario.class)
    public JAXBElement<String> createGetGroupQuoteRequestGroupRateFactorScenarioNote(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioNote_QNAME, String.class, GetGroupQuoteRequestGroupRateFactorScenario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitLevel", scope = GetGroupQuoteRequestGroupRateFactorScenario.class)
    public JAXBElement<String> createGetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel_QNAME, String.class, GetGroupQuoteRequestGroupRateFactorScenario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanType", scope = GetGroupQuoteRequestGroupRateFactorScenario.class)
    public JAXBElement<String> createGetGroupQuoteRequestGroupRateFactorScenarioPlanType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioPlanType_QNAME, String.class, GetGroupQuoteRequestGroupRateFactorScenario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SponsoredCarrierId", scope = GetGroupQuoteRequestGroupRateFactorScenario.class)
    public JAXBElement<Integer> createGetGroupQuoteRequestGroupRateFactorScenarioSponsoredCarrierId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupQuoteRequestGroupRateFactorScenarioSponsoredCarrierId_QNAME, Integer.class, GetGroupQuoteRequestGroupRateFactorScenario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EHBRateOnly", scope = GetGroupQuoteRequestGroupRateFactorScenario.class)
    public JAXBElement<Boolean> createGetGroupQuoteRequestGroupRateFactorScenarioEHBRateOnly(Boolean value) {
        return new JAXBElement<Boolean>(_GetGroupQuoteRequestGroupRateFactorScenarioEHBRateOnly_QNAME, Boolean.class, GetGroupQuoteRequestGroupRateFactorScenario.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FirstName", scope = GetCarriersPlansBenefitsResponseContact.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsResponseContactFirstName(String value) {
        return new JAXBElement<String>(_MemberBaseFirstName_QNAME, String.class, GetCarriersPlansBenefitsResponseContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactPoints", scope = GetCarriersPlansBenefitsResponseContact.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint> createGetCarriersPlansBenefitsResponseContactContactPoints(ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint>(_GetCarriersPlansBenefitsResponseContactContactPoints_QNAME, ArrayOfGetCarriersPlansBenefitsResponseContactContactPoint.class, GetCarriersPlansBenefitsResponseContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LastName", scope = GetCarriersPlansBenefitsResponseContact.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsResponseContactLastName(String value) {
        return new JAXBElement<String>(_MemberBaseLastName_QNAME, String.class, GetCarriersPlansBenefitsResponseContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Title", scope = GetCarriersPlansBenefitsResponseContact.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsResponseContactTitle(String value) {
        return new JAXBElement<String>(_GetCarriersPlansBenefitsResponseContactTitle_QNAME, String.class, GetCarriersPlansBenefitsResponseContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseMemberRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MemberRates", scope = GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseMemberRate> createGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRateMemberRates(ArrayOfGetGroupQuoteResponseMemberRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseMemberRate>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRateMemberRates_QNAME, ArrayOfGetGroupQuoteResponseMemberRate.class, GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoveredRxCUIs", scope = GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate.class)
    public JAXBElement<RxCUIsBase> createGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRateCoveredRxCUIs(RxCUIsBase value) {
        return new JAXBElement<RxCUIsBase>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRateCoveredRxCUIs_QNAME, RxCUIsBase.class, GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Services", scope = GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService> createGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointServices(ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService>(_GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointServices_QNAME, ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService.class, GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyFIPS", scope = CustomProductRatingAreaZipCounty.class)
    public JAXBElement<String> createCustomProductRatingAreaZipCountyCountyFIPS(String value) {
        return new JAXBElement<String>(_CustomProductRatingAreaZipCountyCountyFIPS_QNAME, String.class, CustomProductRatingAreaZipCounty.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupShoppingCart }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ShoppingCart", scope = SubmitGroupShoppingCartResponse.class)
    public JAXBElement<GroupShoppingCart> createSubmitGroupShoppingCartResponseShoppingCart(GroupShoppingCart value) {
        return new JAXBElement<GroupShoppingCart>(_SubmitIfpShoppingCartResponseShoppingCart_QNAME, GroupShoppingCart.class, SubmitGroupShoppingCartResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Plans", scope = FormularyLookupResponseCarrier.class)
    public JAXBElement<ArrayOfFormularyLookupResponseCarrierPlan> createFormularyLookupResponseCarrierPlans(ArrayOfFormularyLookupResponseCarrierPlan value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseCarrierPlan>(_FormularyLookupResponseCarrierPlans_QNAME, ArrayOfFormularyLookupResponseCarrierPlan.class, FormularyLookupResponseCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssuerId", scope = FormularyLookupResponseCarrier.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierIssuerId(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierIssuerId_QNAME, String.class, FormularyLookupResponseCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Plans", scope = GetPlansbyRxResponseCarrier.class)
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlan> createGetPlansbyRxResponseCarrierPlans(ArrayOfGetPlansbyRxResponseCarrierPlan value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlan>(_FormularyLookupResponseCarrierPlans_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlan.class, GetPlansbyRxResponseCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssuerId", scope = GetPlansbyRxResponseCarrier.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierIssuerId(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierIssuerId_QNAME, String.class, GetPlansbyRxResponseCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LeadId", scope = GetGroupRequest.class)
    public JAXBElement<String> createGetGroupRequestLeadId(String value) {
        return new JAXBElement<String>(_GetGroupRequestLeadId_QNAME, String.class, GetGroupRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CartId", scope = GetGroupRequest.class)
    public JAXBElement<String> createGetGroupRequestCartId(String value) {
        return new JAXBElement<String>(_GetGroupRequestCartId_QNAME, String.class, GetGroupRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactId", scope = GetGroupRequest.class)
    public JAXBElement<Integer> createGetGroupRequestContactId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupRequestContactId_QNAME, Integer.class, GetGroupRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupId", scope = GetGroupRequest.class)
    public JAXBElement<Integer> createGetGroupRequestGroupId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupRequestGroupId_QNAME, Integer.class, GetGroupRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanData", scope = GetIfpQuoteResponsePlanDetail.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseItem> createGetIfpQuoteResponsePlanDetailPlanData(ArrayOfGetIfpQuoteResponseItem value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseItem>(_GetIfpQuoteResponsePlanDetailPlanData_QNAME, ArrayOfGetIfpQuoteResponseItem.class, GetIfpQuoteResponsePlanDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Benefits", scope = GetIfpQuoteResponsePlanDetail.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefit> createGetIfpQuoteResponsePlanDetailBenefits(ArrayOfGetIfpQuoteResponsePlanDetailBenefit value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefit>(_GetIfpQuoteResponsePlanDetailBenefits_QNAME, ArrayOfGetIfpQuoteResponsePlanDetailBenefit.class, GetIfpQuoteResponsePlanDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Carriers", scope = GetCustomProductsQuoteResponseQuoteFamilyQuote.class)
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarriers(ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier>(_GetGroupQuoteResponseQuoteCarriers_QNAME, ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier.class, GetCustomProductsQuoteResponseQuoteFamilyQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedRates", scope = GetCustomProductsQuoteResponseQuoteFamilyQuote.class)
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier> createGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedRates(ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier>(_GetGroupQuoteResponseQuoteDroppedRates_QNAME, ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier.class, GetCustomProductsQuoteResponseQuoteFamilyQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Drugs", scope = GetCarriersPlansBenefitsResponseDrugList.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug> createGetCarriersPlansBenefitsResponseDrugListDrugs(ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug>(_GetCarriersPlansBenefitsResponseDrugListDrugs_QNAME, ArrayOfGetCarriersPlansBenefitsResponseDrugListDrug.class, GetCarriersPlansBenefitsResponseDrugList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateFactor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupRateFactors", scope = GetGroupQuoteRequest.class)
    public JAXBElement<GetGroupQuoteRequestGroupRateFactor> createGetGroupQuoteRequestGroupRateFactors(GetGroupQuoteRequestGroupRateFactor value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateFactor>(_GetGroupQuoteRequestGroupRateFactors_QNAME, GetGroupQuoteRequestGroupRateFactor.class, GetGroupQuoteRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequestGroupRateKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupRateKeys", scope = GetGroupQuoteRequest.class)
    public JAXBElement<GetGroupQuoteRequestGroupRateKey> createGetGroupQuoteRequestGroupRateKeys(GetGroupQuoteRequestGroupRateKey value) {
        return new JAXBElement<GetGroupQuoteRequestGroupRateKey>(_GetGroupQuoteRequestGroupRateKeys_QNAME, GetGroupQuoteRequestGroupRateKey.class, GetGroupQuoteRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CartId", scope = IfpShoppingCart.class)
    public JAXBElement<String> createIfpShoppingCartCartId(String value) {
        return new JAXBElement<String>(_GetGroupRequestCartId_QNAME, String.class, IfpShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CreatedDate", scope = IfpShoppingCart.class)
    public JAXBElement<XMLGregorianCalendar> createIfpShoppingCartCreatedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_IfpShoppingCartCreatedDate_QNAME, XMLGregorianCalendar.class, IfpShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ApplicationId", scope = IfpShoppingCart.class)
    public JAXBElement<String> createIfpShoppingCartApplicationId(String value) {
        return new JAXBElement<String>(_IfpShoppingCartApplicationId_QNAME, String.class, IfpShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIfpShoppingCartProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductChoices", scope = IfpShoppingCart.class)
    public JAXBElement<ArrayOfIfpShoppingCartProductChoice> createIfpShoppingCartProductChoices(ArrayOfIfpShoppingCartProductChoice value) {
        return new JAXBElement<ArrayOfIfpShoppingCartProductChoice>(_IfpShoppingCartProductChoices_QNAME, ArrayOfIfpShoppingCartProductChoice.class, IfpShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProposalId", scope = GetGroupQuoteRequestGroupRateKey.class)
    public JAXBElement<Integer> createGetGroupQuoteRequestGroupRateKeyProposalId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupQuoteRequestGroupRateKeyProposalId_QNAME, Integer.class, GetGroupQuoteRequestGroupRateKey.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = GetGroupQuoteRequestGroupRateKey.class)
    public JAXBElement<XMLGregorianCalendar> createGetGroupQuoteRequestGroupRateKeyEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, GetGroupQuoteRequestGroupRateKey.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ViewPointType", scope = GetCarriersPlansBenefitsRequestBenefitFilter.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsRequestBenefitFilterViewPointType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitFilterViewPointType_QNAME, String.class, GetCarriersPlansBenefitsRequestBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoverageType", scope = GetCarriersPlansBenefitsRequestBenefitFilter.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsRequestBenefitFilterCoverageType(String value) {
        return new JAXBElement<String>(_CoverageType_QNAME, String.class, GetCarriersPlansBenefitsRequestBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitEnum", scope = GetCarriersPlansBenefitsRequestBenefitFilter.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsRequestBenefitFilterBenefitEnum(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitFilterBenefitEnum_QNAME, String.class, GetCarriersPlansBenefitsRequestBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AttributeFilters", scope = GetCarriersPlansBenefitsRequestBenefitFilter.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter> createGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilters(ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter>(_GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilters_QNAME, ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter.class, GetCarriersPlansBenefitsRequestBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ServiceType", scope = GetCarriersPlansBenefitsRequestBenefitFilter.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsRequestBenefitFilterServiceType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitFilterServiceType_QNAME, String.class, GetCarriersPlansBenefitsRequestBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseCarrierDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierDetails", scope = GetIfpQuoteResponseQuoteCarrierRate.class)
    public JAXBElement<GetIfpQuoteResponseCarrierDetail> createGetIfpQuoteResponseQuoteCarrierRateCarrierDetails(GetIfpQuoteResponseCarrierDetail value) {
        return new JAXBElement<GetIfpQuoteResponseCarrierDetail>(_GetIfpQuoteResponseQuoteCarrierRateCarrierDetails_QNAME, GetIfpQuoteResponseCarrierDetail.class, GetIfpQuoteResponseQuoteCarrierRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanRates", scope = GetIfpQuoteResponseQuoteCarrierRate.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate> createGetIfpQuoteResponseQuoteCarrierRatePlanRates(ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRates_QNAME, ArrayOfGetIfpQuoteResponseQuoteCarrierRatePlanRate.class, GetIfpQuoteResponseQuoteCarrierRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ToLoadSequenceId", scope = GetCarrierLoadHistoryRequestInput.class)
    public JAXBElement<Integer> createGetCarrierLoadHistoryRequestInputToLoadSequenceId(Integer value) {
        return new JAXBElement<Integer>(_GetCarrierLoadHistoryRequestInputToLoadSequenceId_QNAME, Integer.class, GetCarrierLoadHistoryRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FromEffectiveDate", scope = GetCarrierLoadHistoryRequestInput.class)
    public JAXBElement<XMLGregorianCalendar> createGetCarrierLoadHistoryRequestInputFromEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetCarrierLoadHistoryRequestInputFromEffectiveDate_QNAME, XMLGregorianCalendar.class, GetCarrierLoadHistoryRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ToEffectiveDate", scope = GetCarrierLoadHistoryRequestInput.class)
    public JAXBElement<XMLGregorianCalendar> createGetCarrierLoadHistoryRequestInputToEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetCarrierLoadHistoryRequestInputToEffectiveDate_QNAME, XMLGregorianCalendar.class, GetCarrierLoadHistoryRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Comments", scope = Family.class)
    public JAXBElement<String> createFamilyComments(String value) {
        return new JAXBElement<String>(_FamilyComments_QNAME, String.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FamilyId", scope = Family.class)
    public JAXBElement<Integer> createFamilyFamilyId(Integer value) {
        return new JAXBElement<Integer>(_FamilyFamilyId_QNAME, Integer.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FamilyContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SecondaryContact", scope = Family.class)
    public JAXBElement<FamilyContact> createFamilySecondaryContact(FamilyContact value) {
        return new JAXBElement<FamilyContact>(_FamilySecondaryContact_QNAME, FamilyContact.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = Family.class)
    public JAXBElement<XMLGregorianCalendar> createFamilyEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFamilyMember }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Members", scope = Family.class)
    public JAXBElement<ArrayOfFamilyMember> createFamilyMembers(ArrayOfFamilyMember value) {
        return new JAXBElement<ArrayOfFamilyMember>(_FamilyMembers_QNAME, ArrayOfFamilyMember.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FamilyLocationAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MailingAddress", scope = Family.class)
    public JAXBElement<FamilyLocationAddress> createFamilyMailingAddress(FamilyLocationAddress value) {
        return new JAXBElement<FamilyLocationAddress>(_FamilyMailingAddress_QNAME, FamilyLocationAddress.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OriginatingUserAgent", scope = Family.class)
    public JAXBElement<String> createFamilyOriginatingUserAgent(String value) {
        return new JAXBElement<String>(_FamilyOriginatingUserAgent_QNAME, String.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PaymentOption", scope = Family.class)
    public JAXBElement<List<String>> createFamilyPaymentOption(List<String> value) {
        return new JAXBElement<List<String>>(_PaymentOption_QNAME, ((Class) List.class), Family.class, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ExpirationDate", scope = Family.class)
    public JAXBElement<XMLGregorianCalendar> createFamilyExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FamilyExpirationDate_QNAME, XMLGregorianCalendar.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpShoppingCartsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ShoppingCarts", scope = Family.class)
    public JAXBElement<IfpShoppingCartsBase> createFamilyShoppingCarts(IfpShoppingCartsBase value) {
        return new JAXBElement<IfpShoppingCartsBase>(_SubmitGroupResponseShoppingCarts_QNAME, IfpShoppingCartsBase.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FamilySourceReferenceId", scope = Family.class)
    public JAXBElement<String> createFamilyFamilySourceReferenceId(String value) {
        return new JAXBElement<String>(_FamilyFamilySourceReferenceId_QNAME, String.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TerminationDate", scope = Family.class)
    public JAXBElement<XMLGregorianCalendar> createFamilyTerminationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FamilyTerminationDate_QNAME, XMLGregorianCalendar.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactId", scope = Family.class)
    public JAXBElement<Integer> createFamilyContactId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupRequestContactId_QNAME, Integer.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OESConsumerId", scope = Family.class)
    public JAXBElement<String> createFamilyOESConsumerId(String value) {
        return new JAXBElement<String>(_FamilyOESConsumerId_QNAME, String.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FamilySource", scope = Family.class)
    public JAXBElement<String> createFamilyFamilySource(String value) {
        return new JAXBElement<String>(_FamilyFamilySource_QNAME, String.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OriginatingIPAddress", scope = Family.class)
    public JAXBElement<String> createFamilyOriginatingIPAddress(String value) {
        return new JAXBElement<String>(_FamilyOriginatingIPAddress_QNAME, String.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FamilyContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PrimaryContact", scope = Family.class)
    public JAXBElement<FamilyContact> createFamilyPrimaryContact(FamilyContact value) {
        return new JAXBElement<FamilyContact>(_FamilyPrimaryContact_QNAME, FamilyContact.class, Family.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LeadId", scope = IfpPlanChoiceBase.class)
    public JAXBElement<String> createIfpPlanChoiceBaseLeadId(String value) {
        return new JAXBElement<String>(_GetGroupRequestLeadId_QNAME, String.class, IfpPlanChoiceBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Premium", scope = IfpPlanChoiceBase.class)
    public JAXBElement<BigDecimal> createIfpPlanChoiceBasePremium(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IfpPlanChoiceBasePremium_QNAME, BigDecimal.class, IfpPlanChoiceBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ApplicationId", scope = IfpPlanChoiceBase.class)
    public JAXBElement<String> createIfpPlanChoiceBaseApplicationId(String value) {
        return new JAXBElement<String>(_IfpShoppingCartApplicationId_QNAME, String.class, IfpPlanChoiceBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LifeAmount", scope = IfpPlanChoiceBase.class)
    public JAXBElement<BigDecimal> createIfpPlanChoiceBaseLifeAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IfpPlanChoiceBaseLifeAmount_QNAME, BigDecimal.class, IfpPlanChoiceBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ClientCountyFIPS", scope = GetCustomProductsQuoteRequestCustomRateFactor.class)
    public JAXBElement<String> createGetCustomProductsQuoteRequestCustomRateFactorClientCountyFIPS(String value) {
        return new JAXBElement<String>(_GetCustomProductsQuoteRequestCustomRateFactorClientCountyFIPS_QNAME, String.class, GetCustomProductsQuoteRequestCustomRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ClientState", scope = GetCustomProductsQuoteRequestCustomRateFactor.class)
    public JAXBElement<String> createGetCustomProductsQuoteRequestCustomRateFactorClientState(String value) {
        return new JAXBElement<String>(_GetCustomProductsQuoteRequestCustomRateFactorClientState_QNAME, String.class, GetCustomProductsQuoteRequestCustomRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Strength", scope = FormularyLookupResponseDrugAttribute.class)
    public JAXBElement<String> createFormularyLookupResponseDrugAttributeStrength(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseDrugAttributeStrength_QNAME, String.class, FormularyLookupResponseDrugAttribute.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugName", scope = FormularyLookupResponseDrugAttribute.class)
    public JAXBElement<String> createFormularyLookupResponseDrugAttributeDrugName(String value) {
        return new JAXBElement<String>(_ListRxResponseDrugDrugName_QNAME, String.class, FormularyLookupResponseDrugAttribute.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Unit", scope = FormularyLookupResponseDrugAttribute.class)
    public JAXBElement<String> createFormularyLookupResponseDrugAttributeUnit(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseDrugAttributeUnit_QNAME, String.class, FormularyLookupResponseDrugAttribute.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RxOTC", scope = FormularyLookupResponseDrugAttribute.class)
    public JAXBElement<String> createFormularyLookupResponseDrugAttributeRxOTC(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseDrugAttributeRxOTC_QNAME, String.class, FormularyLookupResponseDrugAttribute.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RxCUI", scope = FormularyLookupResponseDrugAttribute.class)
    public JAXBElement<String> createFormularyLookupResponseDrugAttributeRxCUI(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugRxCUI_QNAME, String.class, FormularyLookupResponseDrugAttribute.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanData", scope = GetGroupQuoteResponsePlanDetail.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseItem> createGetGroupQuoteResponsePlanDetailPlanData(ArrayOfGetGroupQuoteResponseItem value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseItem>(_GetIfpQuoteResponsePlanDetailPlanData_QNAME, ArrayOfGetGroupQuoteResponseItem.class, GetGroupQuoteResponsePlanDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Benefits", scope = GetGroupQuoteResponsePlanDetail.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefit> createGetGroupQuoteResponsePlanDetailBenefits(ArrayOfGetGroupQuoteResponsePlanDetailBenefit value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefit>(_GetIfpQuoteResponsePlanDetailBenefits_QNAME, ArrayOfGetGroupQuoteResponsePlanDetailBenefit.class, GetGroupQuoteResponsePlanDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanId", scope = PlanChoiceBase.class)
    public JAXBElement<String> createPlanChoiceBasePlanId(String value) {
        return new JAXBElement<String>(_PlanChoiceBasePlanId_QNAME, String.class, PlanChoiceBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierId", scope = PlanChoiceBase.class)
    public JAXBElement<Integer> createPlanChoiceBaseCarrierId(Integer value) {
        return new JAXBElement<Integer>(_FetchCustomProductsRequestInputProductFilterCarrierId_QNAME, Integer.class, PlanChoiceBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitLevel", scope = PlanChoiceBase.class)
    public JAXBElement<String> createPlanChoiceBaseBenefitLevel(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel_QNAME, String.class, PlanChoiceBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanType", scope = PlanChoiceBase.class)
    public JAXBElement<String> createPlanChoiceBasePlanType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioPlanType_QNAME, String.class, PlanChoiceBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyURL", scope = GetPlansbyRxResponseCarrierPlanPlanFormulary.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyURL(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyURL_QNAME, String.class, GetPlansbyRxResponseCarrierPlanPlanFormulary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NumberTiers", scope = GetPlansbyRxResponseCarrierPlanPlanFormulary.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierPlanPlanFormularyNumberTiers(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyNumberTiers_QNAME, String.class, GetPlansbyRxResponseCarrierPlanPlanFormulary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyID", scope = GetPlansbyRxResponseCarrierPlanPlanFormulary.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyID(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyID_QNAME, String.class, GetPlansbyRxResponseCarrierPlanPlanFormulary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyTiers", scope = GetPlansbyRxResponseCarrierPlanPlanFormulary.class)
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTiers(ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTiers_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier.class, GetPlansbyRxResponseCarrierPlanPlanFormulary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyFIPS", scope = CustomProductRatingAreaZipRange.class)
    public JAXBElement<String> createCustomProductRatingAreaZipRangeCountyFIPS(String value) {
        return new JAXBElement<String>(_CustomProductRatingAreaZipCountyCountyFIPS_QNAME, String.class, CustomProductRatingAreaZipRange.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LeadId", scope = GetIfpShoppingCartsRequest.class)
    public JAXBElement<String> createGetIfpShoppingCartsRequestLeadId(String value) {
        return new JAXBElement<String>(_GetGroupRequestLeadId_QNAME, String.class, GetIfpShoppingCartsRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CartId", scope = GetIfpShoppingCartsRequest.class)
    public JAXBElement<String> createGetIfpShoppingCartsRequestCartId(String value) {
        return new JAXBElement<String>(_GetGroupRequestCartId_QNAME, String.class, GetIfpShoppingCartsRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactId", scope = GetIfpShoppingCartsRequest.class)
    public JAXBElement<Integer> createGetIfpShoppingCartsRequestContactId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupRequestContactId_QNAME, Integer.class, GetIfpShoppingCartsRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ValidFromDate", scope = GetCarriersPlansBenefitsResponseCarrierPlanPlanLink.class)
    public JAXBElement<XMLGregorianCalendar> createGetCarriersPlansBenefitsResponseCarrierPlanPlanLinkValidFromDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetCarriersPlansBenefitsResponseCarrierPlanPlanLinkValidFromDate_QNAME, XMLGregorianCalendar.class, GetCarriersPlansBenefitsResponseCarrierPlanPlanLink.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LinkActiveText", scope = GetCarriersPlansBenefitsResponseCarrierPlanPlanLink.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsResponseCarrierPlanPlanLinkLinkActiveText(String value) {
        return new JAXBElement<String>(_GetCarriersPlansBenefitsResponseCarrierPlanPlanLinkLinkActiveText_QNAME, String.class, GetCarriersPlansBenefitsResponseCarrierPlanPlanLink.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NumberOfDays", scope = IfpRateBase.class)
    public JAXBElement<String> createIfpRateBaseNumberOfDays(String value) {
        return new JAXBElement<String>(_IfpRateBaseNumberOfDays_QNAME, String.class, IfpRateBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BaseRateUnit", scope = IfpRateBase.class)
    public JAXBElement<String> createIfpRateBaseBaseRateUnit(String value) {
        return new JAXBElement<String>(_IfpRateBaseBaseRateUnit_QNAME, String.class, IfpRateBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "State", scope = LocationBase.class)
    public JAXBElement<String> createLocationBaseState(String value) {
        return new JAXBElement<String>(_LocationAddressBaseState_QNAME, String.class, LocationBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Type", scope = LocationBase.class)
    public JAXBElement<String> createLocationBaseType(String value) {
        return new JAXBElement<String>(_LocationBaseType_QNAME, String.class, LocationBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Address", scope = LocationBase.class)
    public JAXBElement<String> createLocationBaseAddress(String value) {
        return new JAXBElement<String>(_LocationAddressBaseAddress_QNAME, String.class, LocationBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "City", scope = LocationBase.class)
    public JAXBElement<String> createLocationBaseCity(String value) {
        return new JAXBElement<String>(_LocationAddressBaseCity_QNAME, String.class, LocationBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AsOfDate", scope = GetCountiesRequestInput.class)
    public JAXBElement<XMLGregorianCalendar> createGetCountiesRequestInputAsOfDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestPreferenceAsOfDate_QNAME, XMLGregorianCalendar.class, GetCountiesRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "State", scope = GetCountiesRequestInput.class)
    public JAXBElement<String> createGetCountiesRequestInputState(String value) {
        return new JAXBElement<String>(_LocationAddressBaseState_QNAME, String.class, GetCountiesRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZipCode", scope = GetCountiesRequestInput.class)
    public JAXBElement<Integer> createGetCountiesRequestInputZipCode(Integer value) {
        return new JAXBElement<Integer>(_MemberBaseZipCode_QNAME, Integer.class, GetCountiesRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFamilyContactContactAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactAttributes", scope = FamilyContact.class)
    public JAXBElement<ArrayOfFamilyContactContactAttribute> createFamilyContactContactAttributes(ArrayOfFamilyContactContactAttribute value) {
        return new JAXBElement<ArrayOfFamilyContactContactAttribute>(_FamilyContactContactAttributes_QNAME, ArrayOfFamilyContactContactAttribute.class, FamilyContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BestTimeToCall }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BestTimeToCall", scope = FamilyContact.class)
    public JAXBElement<BestTimeToCall> createFamilyContactBestTimeToCall(BestTimeToCall value) {
        return new JAXBElement<BestTimeToCall>(_BestTimeToCall_QNAME, BestTimeToCall.class, FamilyContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProposalId", scope = GetIfpQuoteRequestIfpRateKey.class)
    public JAXBElement<Integer> createGetIfpQuoteRequestIfpRateKeyProposalId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupQuoteRequestGroupRateKeyProposalId_QNAME, Integer.class, GetIfpQuoteRequestIfpRateKey.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = GetIfpQuoteRequestIfpRateKey.class)
    public JAXBElement<XMLGregorianCalendar> createGetIfpQuoteRequestIfpRateKeyEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, GetIfpQuoteRequestIfpRateKey.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TerminationDate", scope = GetIfpQuoteRequestIfpRateKey.class)
    public JAXBElement<XMLGregorianCalendar> createGetIfpQuoteRequestIfpRateKeyTerminationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FamilyTerminationDate_QNAME, XMLGregorianCalendar.class, GetIfpQuoteRequestIfpRateKey.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Cost", scope = ServiceBase.class)
    public JAXBElement<BigDecimal> createServiceBaseCost(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ServiceBaseCost_QNAME, BigDecimal.class, ServiceBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Amount", scope = ServiceBase.class)
    public JAXBElement<BigDecimal> createServiceBaseAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ServiceBaseAmount_QNAME, BigDecimal.class, ServiceBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EmployeeRates", scope = GetGroupQuoteResponseQuoteCarrierRatePlanRate.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate> createGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRates(ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate>(_GetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRates_QNAME, ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRateEmployeeRate.class, GetGroupQuoteResponseQuoteCarrierRatePlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanDetails", scope = GetGroupQuoteResponseQuoteCarrierRatePlanRate.class)
    public JAXBElement<GetGroupQuoteResponsePlanDetail> createGetGroupQuoteResponseQuoteCarrierRatePlanRatePlanDetails(GetGroupQuoteResponsePlanDetail value) {
        return new JAXBElement<GetGroupQuoteResponsePlanDetail>(_GetGroupQuoteResponseQuoteCarrierRatePlanRatePlanDetails_QNAME, GetGroupQuoteResponsePlanDetail.class, GetGroupQuoteResponseQuoteCarrierRatePlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseDrugList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugLists", scope = GetCarriersPlansBenefitsResponseCarrier.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugList> createGetCarriersPlansBenefitsResponseCarrierDrugLists(ArrayOfGetCarriersPlansBenefitsResponseDrugList value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseDrugList>(_GetCarriersPlansBenefitsResponseCarrierDrugLists_QNAME, ArrayOfGetCarriersPlansBenefitsResponseDrugList.class, GetCarriersPlansBenefitsResponseCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Plans", scope = GetCarriersPlansBenefitsResponseCarrier.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan> createGetCarriersPlansBenefitsResponseCarrierPlans(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan>(_FormularyLookupResponseCarrierPlans_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan.class, GetCarriersPlansBenefitsResponseCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseLocation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Locations", scope = GetCarriersPlansBenefitsResponseCarrier.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseLocation> createGetCarriersPlansBenefitsResponseCarrierLocations(ArrayOfGetCarriersPlansBenefitsResponseLocation value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseLocation>(_GetCarriersPlansBenefitsResponseCarrierLocations_QNAME, ArrayOfGetCarriersPlansBenefitsResponseLocation.class, GetCarriersPlansBenefitsResponseCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseFormulary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Formularies", scope = GetCarriersPlansBenefitsResponseCarrier.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormulary> createGetCarriersPlansBenefitsResponseCarrierFormularies(ArrayOfGetCarriersPlansBenefitsResponseFormulary value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormulary>(_GetCarriersPlansBenefitsResponseCarrierFormularies_QNAME, ArrayOfGetCarriersPlansBenefitsResponseFormulary.class, GetCarriersPlansBenefitsResponseCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Contacts", scope = GetCarriersPlansBenefitsResponseCarrier.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseContact> createGetCarriersPlansBenefitsResponseCarrierContacts(ArrayOfGetCarriersPlansBenefitsResponseContact value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseContact>(_GetCarriersPlansBenefitsResponseCarrierContacts_QNAME, ArrayOfGetCarriersPlansBenefitsResponseContact.class, GetCarriersPlansBenefitsResponseCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierData", scope = GetCarriersPlansBenefitsResponseCarrier.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> createGetCarriersPlansBenefitsResponseCarrierCarrierData(ArrayOfGetCarriersPlansBenefitsResponseItem value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem>(_GetCarriersPlansBenefitsResponseCarrierCarrierData_QNAME, ArrayOfGetCarriersPlansBenefitsResponseItem.class, GetCarriersPlansBenefitsResponseCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierData", scope = GetGroupQuoteResponseCarrierDetail.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseItem> createGetGroupQuoteResponseCarrierDetailCarrierData(ArrayOfGetGroupQuoteResponseItem value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseItem>(_GetCarriersPlansBenefitsResponseCarrierCarrierData_QNAME, ArrayOfGetGroupQuoteResponseItem.class, GetGroupQuoteResponseCarrierDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseCarrierDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierDetails", scope = GetIfpQuoteResponseQuoteDroppedCarrier.class)
    public JAXBElement<GetIfpQuoteResponseCarrierDetail> createGetIfpQuoteResponseQuoteDroppedCarrierCarrierDetails(GetIfpQuoteResponseCarrierDetail value) {
        return new JAXBElement<GetIfpQuoteResponseCarrierDetail>(_GetIfpQuoteResponseQuoteCarrierRateCarrierDetails_QNAME, GetIfpQuoteResponseCarrierDetail.class, GetIfpQuoteResponseQuoteDroppedCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedPlans", scope = GetIfpQuoteResponseQuoteDroppedCarrier.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan> createGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlans(ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan>(_GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlans_QNAME, ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan.class, GetIfpQuoteResponseQuoteDroppedCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Services", scope = GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService> createGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointServices(ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService>(_GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointServices_QNAME, ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPointService.class, GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IsPreferred", scope = CountyBase.class)
    public JAXBElement<String> createCountyBaseIsPreferred(String value) {
        return new JAXBElement<String>(_IsPreferred_QNAME, String.class, CountyBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NonSmokerRateOnly", scope = ScenarioBase.class)
    public JAXBElement<Boolean> createScenarioBaseNonSmokerRateOnly(Boolean value) {
        return new JAXBElement<Boolean>(_GetGroupQuoteRequestGroupRateFactorScenarioNonSmokerRateOnly_QNAME, Boolean.class, ScenarioBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DepContributionType", scope = ScenarioBase.class)
    public JAXBElement<List<String>> createScenarioBaseDepContributionType(List<String> value) {
        return new JAXBElement<List<String>>(_GetGroupQuoteRequestGroupRateFactorScenarioDepContributionType_QNAME, ((Class) List.class), ScenarioBase.class, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SponsoredPlanId", scope = ScenarioBase.class)
    public JAXBElement<String> createScenarioBaseSponsoredPlanId(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioSponsoredPlanId_QNAME, String.class, ScenarioBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NonSmokerDepRateOnly", scope = ScenarioBase.class)
    public JAXBElement<Boolean> createScenarioBaseNonSmokerDepRateOnly(Boolean value) {
        return new JAXBElement<Boolean>(_GetGroupQuoteRequestGroupRateFactorScenarioNonSmokerDepRateOnly_QNAME, Boolean.class, ScenarioBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Note", scope = ScenarioBase.class)
    public JAXBElement<String> createScenarioBaseNote(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioNote_QNAME, String.class, ScenarioBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitLevel", scope = ScenarioBase.class)
    public JAXBElement<String> createScenarioBaseBenefitLevel(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel_QNAME, String.class, ScenarioBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanType", scope = ScenarioBase.class)
    public JAXBElement<String> createScenarioBasePlanType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioPlanType_QNAME, String.class, ScenarioBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SponsoredCarrierId", scope = ScenarioBase.class)
    public JAXBElement<Integer> createScenarioBaseSponsoredCarrierId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupQuoteRequestGroupRateFactorScenarioSponsoredCarrierId_QNAME, Integer.class, ScenarioBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "HouseholdSize", scope = GetIfpQuoteRequestIfpRateFactor.class)
    public JAXBElement<Short> createGetIfpQuoteRequestIfpRateFactorHouseholdSize(Short value) {
        return new JAXBElement<Short>(_GetIfpQuoteRequestIfpRateFactorHouseholdSize_QNAME, Short.class, GetIfpQuoteRequestIfpRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IsNativeAmerican", scope = GetIfpQuoteRequestIfpRateFactor.class)
    public JAXBElement<Boolean> createGetIfpQuoteRequestIfpRateFactorIsNativeAmerican(Boolean value) {
        return new JAXBElement<Boolean>(_GetIfpQuoteRequestIfpRateFactorIsNativeAmerican_QNAME, Boolean.class, GetIfpQuoteRequestIfpRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "HouseholdMAGI", scope = GetIfpQuoteRequestIfpRateFactor.class)
    public JAXBElement<BigDecimal> createGetIfpQuoteRequestIfpRateFactorHouseholdMAGI(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetIfpQuoteRequestIfpRateFactorHouseholdMAGI_QNAME, BigDecimal.class, GetIfpQuoteRequestIfpRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "State", scope = GetIfpQuoteRequestIfpRateFactor.class)
    public JAXBElement<String> createGetIfpQuoteRequestIfpRateFactorState(String value) {
        return new JAXBElement<String>(_LocationAddressBaseState_QNAME, String.class, GetIfpQuoteRequestIfpRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = GetIfpQuoteRequestIfpRateFactor.class)
    public JAXBElement<XMLGregorianCalendar> createGetIfpQuoteRequestIfpRateFactorEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, GetIfpQuoteRequestIfpRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TerminationDate", scope = GetIfpQuoteRequestIfpRateFactor.class)
    public JAXBElement<XMLGregorianCalendar> createGetIfpQuoteRequestIfpRateFactorTerminationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FamilyTerminationDate_QNAME, XMLGregorianCalendar.class, GetIfpQuoteRequestIfpRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyName", scope = GetIfpQuoteRequestIfpRateFactor.class)
    public JAXBElement<String> createGetIfpQuoteRequestIfpRateFactorCountyName(String value) {
        return new JAXBElement<String>(_MemberBaseCountyName_QNAME, String.class, GetIfpQuoteRequestIfpRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductChoices", scope = GetIfpQuoteRequestIfpRateFactor.class)
    public JAXBElement<ArrayOfGetIfpQuoteRequestProductChoice> createGetIfpQuoteRequestIfpRateFactorProductChoices(ArrayOfGetIfpQuoteRequestProductChoice value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestProductChoice>(_IfpShoppingCartProductChoices_QNAME, ArrayOfGetIfpQuoteRequestProductChoice.class, GetIfpQuoteRequestIfpRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = IfpShoppingCartProductChoice.class)
    public JAXBElement<XMLGregorianCalendar> createIfpShoppingCartProductChoiceEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, IfpShoppingCartProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TerminationDate", scope = IfpShoppingCartProductChoice.class)
    public JAXBElement<XMLGregorianCalendar> createIfpShoppingCartProductChoiceTerminationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FamilyTerminationDate_QNAME, XMLGregorianCalendar.class, IfpShoppingCartProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoverageWaived", scope = IfpShoppingCartProductChoice.class)
    public JAXBElement<Boolean> createIfpShoppingCartProductChoiceCoverageWaived(Boolean value) {
        return new JAXBElement<Boolean>(_IfpShoppingCartProductChoiceCoverageWaived_QNAME, Boolean.class, IfpShoppingCartProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CoverageTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoverageTier", scope = IfpShoppingCartProductChoice.class)
    public JAXBElement<CoverageTier> createIfpShoppingCartProductChoiceCoverageTier(CoverageTier value) {
        return new JAXBElement<CoverageTier>(_CoverageTier_QNAME, CoverageTier.class, IfpShoppingCartProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpShoppingCartProductChoicePlanChoices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanChoice", scope = IfpShoppingCartProductChoice.class)
    public JAXBElement<IfpShoppingCartProductChoicePlanChoices> createIfpShoppingCartProductChoicePlanChoice(IfpShoppingCartProductChoicePlanChoices value) {
        return new JAXBElement<IfpShoppingCartProductChoicePlanChoices>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoice_QNAME, IfpShoppingCartProductChoicePlanChoices.class, IfpShoppingCartProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PaymentOption", scope = IfpShoppingCartProductChoice.class)
    public JAXBElement<List<String>> createIfpShoppingCartProductChoicePaymentOption(List<String> value) {
        return new JAXBElement<List<String>>(_PaymentOption_QNAME, ((Class) List.class), IfpShoppingCartProductChoice.class, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MemberId", scope = IfpShoppingCartProductChoice.class)
    public JAXBElement<String> createIfpShoppingCartProductChoiceMemberId(String value) {
        return new JAXBElement<String>(_IfpShoppingCartProductChoiceMemberId_QNAME, String.class, IfpShoppingCartProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProducts", scope = SubmitCustomProductsRequest.class)
    public JAXBElement<ArrayOfCustomProduct> createSubmitCustomProductsRequestCustomProducts(ArrayOfCustomProduct value) {
        return new JAXBElement<ArrayOfCustomProduct>(_SubmitCustomProductsRequestCustomProducts_QNAME, ArrayOfCustomProduct.class, SubmitCustomProductsRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanId", scope = PlanFilterBase.class)
    public JAXBElement<String> createPlanFilterBasePlanId(String value) {
        return new JAXBElement<String>(_PlanChoiceBasePlanId_QNAME, String.class, PlanFilterBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssuerId", scope = PlanFilterBase.class)
    public JAXBElement<String> createPlanFilterBaseIssuerId(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierIssuerId_QNAME, String.class, PlanFilterBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierId", scope = PlanFilterBase.class)
    public JAXBElement<Integer> createPlanFilterBaseCarrierId(Integer value) {
        return new JAXBElement<Integer>(_FetchCustomProductsRequestInputProductFilterCarrierId_QNAME, Integer.class, PlanFilterBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitLevel", scope = PlanFilterBase.class)
    public JAXBElement<String> createPlanFilterBaseBenefitLevel(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel_QNAME, String.class, PlanFilterBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanType", scope = PlanFilterBase.class)
    public JAXBElement<String> createPlanFilterBasePlanType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioPlanType_QNAME, String.class, PlanFilterBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RxCUI", scope = GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugRxCUI(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugRxCUI_QNAME, String.class, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TobaccoFreeMonths", scope = CustomProductPlanBusinessRule.class)
    public JAXBElement<Short> createCustomProductPlanBusinessRuleTobaccoFreeMonths(Short value) {
        return new JAXBElement<Short>(_CustomProductPlanBusinessRuleTobaccoFreeMonths_QNAME, Short.class, CustomProductPlanBusinessRule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductPlanBusinessRuleCappingRule }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChildrenCap", scope = CustomProductPlanBusinessRule.class)
    public JAXBElement<CustomProductPlanBusinessRuleCappingRule> createCustomProductPlanBusinessRuleChildrenCap(CustomProductPlanBusinessRuleCappingRule value) {
        return new JAXBElement<CustomProductPlanBusinessRuleCappingRule>(_CustomProductPlanBusinessRuleChildrenCap_QNAME, CustomProductPlanBusinessRuleCappingRule.class, CustomProductPlanBusinessRule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanBusinessRuleAllowedRelation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AllowedRelations", scope = CustomProductPlanBusinessRule.class)
    public JAXBElement<ArrayOfCustomProductPlanBusinessRuleAllowedRelation> createCustomProductPlanBusinessRuleAllowedRelations(ArrayOfCustomProductPlanBusinessRuleAllowedRelation value) {
        return new JAXBElement<ArrayOfCustomProductPlanBusinessRuleAllowedRelation>(_CustomProductPlanBusinessRuleAllowedRelations_QNAME, ArrayOfCustomProductPlanBusinessRuleAllowedRelation.class, CustomProductPlanBusinessRule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IgnoreMembershipDate", scope = CustomProductPlanBusinessRule.class)
    public JAXBElement<Boolean> createCustomProductPlanBusinessRuleIgnoreMembershipDate(Boolean value) {
        return new JAXBElement<Boolean>(_CustomProductPlanBusinessRuleIgnoreMembershipDate_QNAME, Boolean.class, CustomProductPlanBusinessRule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MinAdultAge", scope = CustomProductPlanBusinessRule.class)
    public JAXBElement<Short> createCustomProductPlanBusinessRuleMinAdultAge(Short value) {
        return new JAXBElement<Short>(_CustomProductPlanBusinessRuleMinAdultAge_QNAME, Short.class, CustomProductPlanBusinessRule.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LogoFileSmall", scope = CarrierBase.class)
    public JAXBElement<String> createCarrierBaseLogoFileSmall(String value) {
        return new JAXBElement<String>(_CarrierBaseLogoFileSmall_QNAME, String.class, CarrierBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssuerId", scope = CarrierBase.class)
    public JAXBElement<String> createCarrierBaseIssuerId(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierIssuerId_QNAME, String.class, CarrierBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DisclaimerHtml", scope = CarrierBase.class)
    public JAXBElement<String> createCarrierBaseDisclaimerHtml(String value) {
        return new JAXBElement<String>(_CarrierBaseDisclaimerHtml_QNAME, String.class, CarrierBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CompanyId", scope = CarrierBase.class)
    public JAXBElement<Integer> createCarrierBaseCompanyId(Integer value) {
        return new JAXBElement<Integer>(_CarrierBaseCompanyId_QNAME, Integer.class, CarrierBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LogoFileLarge", scope = CarrierBase.class)
    public JAXBElement<String> createCarrierBaseLogoFileLarge(String value) {
        return new JAXBElement<String>(_CarrierBaseLogoFileLarge_QNAME, String.class, CarrierBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LogoFileMediumTransparent", scope = CarrierBase.class)
    public JAXBElement<String> createCarrierBaseLogoFileMediumTransparent(String value) {
        return new JAXBElement<String>(_CarrierBaseLogoFileMediumTransparent_QNAME, String.class, CarrierBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LogoFileMedium", scope = CarrierBase.class)
    public JAXBElement<String> createCarrierBaseLogoFileMedium(String value) {
        return new JAXBElement<String>(_CarrierBaseLogoFileMedium_QNAME, String.class, CarrierBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Carriers", scope = GetCarriersPlansBenefitsResponse.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrier> createGetCarriersPlansBenefitsResponseCarriers(ArrayOfGetCarriersPlansBenefitsResponseCarrier value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrier>(_GetGroupQuoteResponseQuoteCarriers_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrier.class, GetCarriersPlansBenefitsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RxCUIs", scope = FormularyLookupRequestDrug.class)
    public JAXBElement<RxCUIsBase> createFormularyLookupRequestDrugRxCUIs(RxCUIsBase value) {
        return new JAXBElement<RxCUIsBase>(_FormularyLookupRequestDrugRxCUIs_QNAME, RxCUIsBase.class, FormularyLookupRequestDrug.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugName", scope = FormularyLookupRequestDrug.class)
    public JAXBElement<String> createFormularyLookupRequestDrugDrugName(String value) {
        return new JAXBElement<String>(_ListRxResponseDrugDrugName_QNAME, String.class, FormularyLookupRequestDrug.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseMemberRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MemberRates", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseMemberRate> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateMemberRates(ArrayOfGetCustomProductsQuoteResponseMemberRate value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseMemberRate>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRateMemberRates_QNAME, ArrayOfGetCustomProductsQuoteResponseMemberRate.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "F1Rate", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateF1Rate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateF1Rate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SD3Rate", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSD3Rate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSD3Rate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SD2Rate", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSD2Rate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSD2Rate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanName", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<String> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRatePlanName(String value) {
        return new JAXBElement<String>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRatePlanName_QNAME, String.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SRate", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSRate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "F3Rate", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateF3Rate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateF3Rate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DepSmokerSurcharge", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateDepSmokerSurcharge(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateDepSmokerSurcharge_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SmokerSurcharge", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSmokerSurcharge(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSmokerSurcharge_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "F2Rate", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateF2Rate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateF2Rate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DepRate", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateDepRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateDepRate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SSRate", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSSRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSSRate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SD1Rate", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSD1Rate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSD1Rate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Rate", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateRate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MembershipDate", scope = GetGroupQuoteRequestGroupRateFactorEmployeeMember.class)
    public JAXBElement<XMLGregorianCalendar> createGetGroupQuoteRequestGroupRateFactorEmployeeMemberMembershipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MemberBaseMembershipDate_QNAME, XMLGregorianCalendar.class, GetGroupQuoteRequestGroupRateFactorEmployeeMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DateLastSmoked", scope = GetGroupQuoteRequestGroupRateFactorEmployeeMember.class)
    public JAXBElement<XMLGregorianCalendar> createGetGroupQuoteRequestGroupRateFactorEmployeeMemberDateLastSmoked(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MemberBaseDateLastSmoked_QNAME, XMLGregorianCalendar.class, GetGroupQuoteRequestGroupRateFactorEmployeeMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IsSmoker", scope = GetGroupQuoteRequestGroupRateFactorEmployeeMember.class)
    public JAXBElement<Boolean> createGetGroupQuoteRequestGroupRateFactorEmployeeMemberIsSmoker(Boolean value) {
        return new JAXBElement<Boolean>(_MemberBaseIsSmoker_QNAME, Boolean.class, GetGroupQuoteRequestGroupRateFactorEmployeeMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyName", scope = GetGroupQuoteRequestGroupRateFactorEmployeeMember.class)
    public JAXBElement<String> createGetGroupQuoteRequestGroupRateFactorEmployeeMemberCountyName(String value) {
        return new JAXBElement<String>(_MemberBaseCountyName_QNAME, String.class, GetGroupQuoteRequestGroupRateFactorEmployeeMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZipCode", scope = GetGroupQuoteRequestGroupRateFactorEmployeeMember.class)
    public JAXBElement<Integer> createGetGroupQuoteRequestGroupRateFactorEmployeeMemberZipCode(Integer value) {
        return new JAXBElement<Integer>(_MemberBaseZipCode_QNAME, Integer.class, GetGroupQuoteRequestGroupRateFactorEmployeeMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SRate", scope = GetCustomProductsQuoteResponseMemberRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseMemberRateSRate(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSRate_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseMemberRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SmokerSurcharge", scope = GetCustomProductsQuoteResponseMemberRate.class)
    public JAXBElement<BigDecimal> createGetCustomProductsQuoteResponseMemberRateSmokerSurcharge(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRateSmokerSurcharge_QNAME, BigDecimal.class, GetCustomProductsQuoteResponseMemberRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Message", scope = IfpShoppingCartProductChoicePlanChoicesStatusChange.class)
    public JAXBElement<String> createIfpShoppingCartProductChoicePlanChoicesStatusChangeMessage(String value) {
        return new JAXBElement<String>(_IfpShoppingCartProductChoicePlanChoicesStatusChangeMessage_QNAME, String.class, IfpShoppingCartProductChoicePlanChoicesStatusChange.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ReleaseNote", scope = GetCarrierLoadHistoryResponseLoadEventCarrier.class)
    public JAXBElement<String> createGetCarrierLoadHistoryResponseLoadEventCarrierReleaseNote(String value) {
        return new JAXBElement<String>(_GetCarrierLoadHistoryResponseLoadEventCarrierReleaseNote_QNAME, String.class, GetCarrierLoadHistoryResponseLoadEventCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VersionedPlans", scope = GetCarrierLoadHistoryResponseLoadEventCarrier.class)
    public JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan> createGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlans(ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan value) {
        return new JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan>(_GetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlans_QNAME, ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrierVersionedPlan.class, GetCarrierLoadHistoryResponseLoadEventCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanLookupSequence }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AreaLookupStrategy", scope = CustomProductPlan.class)
    public JAXBElement<ArrayOfCustomProductPlanLookupSequence> createCustomProductPlanAreaLookupStrategy(ArrayOfCustomProductPlanLookupSequence value) {
        return new JAXBElement<ArrayOfCustomProductPlanLookupSequence>(_CustomProductPlanAreaLookupStrategy_QNAME, ArrayOfCustomProductPlanLookupSequence.class, CustomProductPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanRawRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RawRates", scope = CustomProductPlan.class)
    public JAXBElement<ArrayOfCustomProductPlanRawRate> createCustomProductPlanRawRates(ArrayOfCustomProductPlanRawRate value) {
        return new JAXBElement<ArrayOfCustomProductPlanRawRate>(_CustomProductPlanRawRates_QNAME, ArrayOfCustomProductPlanRawRate.class, CustomProductPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CustomProductPlanBusinessRule }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BusinessRules", scope = CustomProductPlan.class)
    public JAXBElement<CustomProductPlanBusinessRule> createCustomProductPlanBusinessRules(CustomProductPlanBusinessRule value) {
        return new JAXBElement<CustomProductPlanBusinessRule>(_CustomProductPlanBusinessRules_QNAME, CustomProductPlanBusinessRule.class, CustomProductPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RAFOverrides", scope = GetGroupQuoteRequestGroupRateFactor.class)
    public JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride> createGetGroupQuoteRequestGroupRateFactorRAFOverrides(ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride>(_GetGroupQuoteRequestGroupRateFactorRAFOverrides_QNAME, ArrayOfGetGroupQuoteRequestGroupRateFactorRAFOverride.class, GetGroupQuoteRequestGroupRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestGroupRateFactorScenario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ERContributionScenarios", scope = GetGroupQuoteRequestGroupRateFactor.class)
    public JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorScenario> createGetGroupQuoteRequestGroupRateFactorERContributionScenarios(ArrayOfGetGroupQuoteRequestGroupRateFactorScenario value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorScenario>(_GetGroupQuoteRequestGroupRateFactorERContributionScenarios_QNAME, ArrayOfGetGroupQuoteRequestGroupRateFactorScenario.class, GetGroupQuoteRequestGroupRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SIC", scope = GetGroupQuoteRequestGroupRateFactor.class)
    public JAXBElement<Integer> createGetGroupQuoteRequestGroupRateFactorSIC(Integer value) {
        return new JAXBElement<Integer>(_GetGroupQuoteRequestGroupRateFactorSIC_QNAME, Integer.class, GetGroupQuoteRequestGroupRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "State", scope = GetGroupQuoteRequestGroupRateFactor.class)
    public JAXBElement<String> createGetGroupQuoteRequestGroupRateFactorState(String value) {
        return new JAXBElement<String>(_LocationAddressBaseState_QNAME, String.class, GetGroupQuoteRequestGroupRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = GetGroupQuoteRequestGroupRateFactor.class)
    public JAXBElement<XMLGregorianCalendar> createGetGroupQuoteRequestGroupRateFactorEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, GetGroupQuoteRequestGroupRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyName", scope = GetGroupQuoteRequestGroupRateFactor.class)
    public JAXBElement<String> createGetGroupQuoteRequestGroupRateFactorCountyName(String value) {
        return new JAXBElement<String>(_MemberBaseCountyName_QNAME, String.class, GetGroupQuoteRequestGroupRateFactor.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Coverages", scope = GetPlansbyRxResponseCarrierPlanBenefit.class)
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage> createGetPlansbyRxResponseCarrierPlanBenefitCoverages(ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage>(_GetGroupQuoteResponsePlanDetailBenefitCoverages_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverage.class, GetPlansbyRxResponseCarrierPlanBenefit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "HeightInches", scope = FamilyMember.class)
    public JAXBElement<Short> createFamilyMemberHeightInches(Short value) {
        return new JAXBElement<Short>(_FamilyMemberHeightInches_QNAME, Short.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MembershipDate", scope = FamilyMember.class)
    public JAXBElement<XMLGregorianCalendar> createFamilyMemberMembershipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MemberBaseMembershipDate_QNAME, XMLGregorianCalendar.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IsSmoker", scope = FamilyMember.class)
    public JAXBElement<Boolean> createFamilyMemberIsSmoker(Boolean value) {
        return new JAXBElement<Boolean>(_MemberBaseIsSmoker_QNAME, Boolean.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AnnualIncome", scope = FamilyMember.class)
    public JAXBElement<BigDecimal> createFamilyMemberAnnualIncome(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_FamilyMemberAnnualIncome_QNAME, BigDecimal.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LastName", scope = FamilyMember.class)
    public JAXBElement<String> createFamilyMemberLastName(String value) {
        return new JAXBElement<String>(_MemberBaseLastName_QNAME, String.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZipCode", scope = FamilyMember.class)
    public JAXBElement<Integer> createFamilyMemberZipCode(Integer value) {
        return new JAXBElement<Integer>(_MemberBaseZipCode_QNAME, Integer.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "HeightFeet", scope = FamilyMember.class)
    public JAXBElement<Short> createFamilyMemberHeightFeet(Short value) {
        return new JAXBElement<Short>(_FamilyMemberHeightFeet_QNAME, Short.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IncludeInQuote", scope = FamilyMember.class)
    public JAXBElement<Boolean> createFamilyMemberIncludeInQuote(Boolean value) {
        return new JAXBElement<Boolean>(_FamilyMemberIncludeInQuote_QNAME, Boolean.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DateLastSmoked", scope = FamilyMember.class)
    public JAXBElement<XMLGregorianCalendar> createFamilyMemberDateLastSmoked(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MemberBaseDateLastSmoked_QNAME, XMLGregorianCalendar.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Weight", scope = FamilyMember.class)
    public JAXBElement<Short> createFamilyMemberWeight(Short value) {
        return new JAXBElement<Short>(_FamilyMemberWeight_QNAME, Short.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IncludeInHouseholdMAGI", scope = FamilyMember.class)
    public JAXBElement<Boolean> createFamilyMemberIncludeInHouseholdMAGI(Boolean value) {
        return new JAXBElement<Boolean>(_FamilyMemberIncludeInHouseholdMAGI_QNAME, Boolean.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FirstName", scope = FamilyMember.class)
    public JAXBElement<String> createFamilyMemberFirstName(String value) {
        return new JAXBElement<String>(_MemberBaseFirstName_QNAME, String.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyName", scope = FamilyMember.class)
    public JAXBElement<String> createFamilyMemberCountyName(String value) {
        return new JAXBElement<String>(_MemberBaseCountyName_QNAME, String.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "QHPEligible", scope = FamilyMember.class)
    public JAXBElement<Boolean> createFamilyMemberQHPEligible(Boolean value) {
        return new JAXBElement<Boolean>(_FamilyMemberQHPEligible_QNAME, Boolean.class, FamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FirstName", scope = ContactBase.class)
    public JAXBElement<String> createContactBaseFirstName(String value) {
        return new JAXBElement<String>(_MemberBaseFirstName_QNAME, String.class, ContactBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LastName", scope = ContactBase.class)
    public JAXBElement<String> createContactBaseLastName(String value) {
        return new JAXBElement<String>(_MemberBaseLastName_QNAME, String.class, ContactBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactType", scope = ContactBase.class)
    public JAXBElement<String> createContactBaseContactType(String value) {
        return new JAXBElement<String>(_ContactBaseContactType_QNAME, String.class, ContactBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Title", scope = ContactBase.class)
    public JAXBElement<String> createContactBaseTitle(String value) {
        return new JAXBElement<String>(_GetCarriersPlansBenefitsResponseContactTitle_QNAME, String.class, ContactBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGroupContactContactAttribute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactAttributes", scope = GroupContact.class)
    public JAXBElement<ArrayOfGroupContactContactAttribute> createGroupContactContactAttributes(ArrayOfGroupContactContactAttribute value) {
        return new JAXBElement<ArrayOfGroupContactContactAttribute>(_FamilyContactContactAttributes_QNAME, ArrayOfGroupContactContactAttribute.class, GroupContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BestTimeToCall }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BestTimeToCall", scope = GroupContact.class)
    public JAXBElement<BestTimeToCall> createGroupContactBestTimeToCall(BestTimeToCall value) {
        return new JAXBElement<BestTimeToCall>(_BestTimeToCall_QNAME, BestTimeToCall.class, GroupContact.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrierRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Carriers", scope = GetIfpQuoteResponseQuote.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRate> createGetIfpQuoteResponseQuoteCarriers(ArrayOfGetIfpQuoteResponseQuoteCarrierRate value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRate>(_GetGroupQuoteResponseQuoteCarriers_QNAME, ArrayOfGetIfpQuoteResponseQuoteCarrierRate.class, GetIfpQuoteResponseQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteMemberPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MemberPlans", scope = GetIfpQuoteResponseQuote.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteMemberPlan> createGetIfpQuoteResponseQuoteMemberPlans(ArrayOfGetIfpQuoteResponseQuoteMemberPlan value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteMemberPlan>(_GetIfpQuoteResponseQuoteMemberPlans_QNAME, ArrayOfGetIfpQuoteResponseQuoteMemberPlan.class, GetIfpQuoteResponseQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedRates", scope = GetIfpQuoteResponseQuote.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier> createGetIfpQuoteResponseQuoteDroppedRates(ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier>(_GetGroupQuoteResponseQuoteDroppedRates_QNAME, ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier.class, GetIfpQuoteResponseQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AsOfDate", scope = GetZipCodeInfoRequestInput.class)
    public JAXBElement<XMLGregorianCalendar> createGetZipCodeInfoRequestInputAsOfDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestPreferenceAsOfDate_QNAME, XMLGregorianCalendar.class, GetZipCodeInfoRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "State", scope = GetZipCodeInfoRequestInput.class)
    public JAXBElement<String> createGetZipCodeInfoRequestInputState(String value) {
        return new JAXBElement<String>(_LocationAddressBaseState_QNAME, String.class, GetZipCodeInfoRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZipCode", scope = GetZipCodeInfoRequestInput.class)
    public JAXBElement<Integer> createGetZipCodeInfoRequestInputZipCode(Integer value) {
        return new JAXBElement<Integer>(_MemberBaseZipCode_QNAME, Integer.class, GetZipCodeInfoRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoRequestInputIsQuotableFilters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IsQuotableFilter", scope = GetZipCodeInfoRequestInput.class)
    public JAXBElement<GetZipCodeInfoRequestInputIsQuotableFilters> createGetZipCodeInfoRequestInputIsQuotableFilter(GetZipCodeInfoRequestInputIsQuotableFilters value) {
        return new JAXBElement<GetZipCodeInfoRequestInputIsQuotableFilters>(_GetZipCodeInfoRequestInputIsQuotableFilter_QNAME, GetZipCodeInfoRequestInputIsQuotableFilters.class, GetZipCodeInfoRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyDrugs", scope = FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier.class)
    public JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugs(ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugs_QNAME, ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug.class, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TierType", scope = FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierTierType(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierTierType_QNAME, String.class, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CostSharingTypeVOs", scope = FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier.class)
    public JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOs(ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOs_QNAME, ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LeadId", scope = GetFamilyRequest.class)
    public JAXBElement<String> createGetFamilyRequestLeadId(String value) {
        return new JAXBElement<String>(_GetGroupRequestLeadId_QNAME, String.class, GetFamilyRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CartId", scope = GetFamilyRequest.class)
    public JAXBElement<String> createGetFamilyRequestCartId(String value) {
        return new JAXBElement<String>(_GetGroupRequestCartId_QNAME, String.class, GetFamilyRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FamilyId", scope = GetFamilyRequest.class)
    public JAXBElement<Integer> createGetFamilyRequestFamilyId(Integer value) {
        return new JAXBElement<Integer>(_FamilyFamilyId_QNAME, Integer.class, GetFamilyRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactId", scope = GetFamilyRequest.class)
    public JAXBElement<Integer> createGetFamilyRequestContactId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupRequestContactId_QNAME, Integer.class, GetFamilyRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanName", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan.class)
    public JAXBElement<String> createGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlanPlanName(String value) {
        return new JAXBElement<String>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRatePlanName_QNAME, String.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ViewPoints", scope = GetIfpQuoteResponsePlanDetailBenefitCoverage.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint> createGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoints(ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint>(_GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoints_QNAME, ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint.class, GetIfpQuoteResponsePlanDetailBenefitCoverage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Plans", scope = CustomProduct.class)
    public JAXBElement<ArrayOfCustomProductPlan> createCustomProductPlans(ArrayOfCustomProductPlan value) {
        return new JAXBElement<ArrayOfCustomProductPlan>(_FormularyLookupResponseCarrierPlans_QNAME, ArrayOfCustomProductPlan.class, CustomProduct.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductRatingArea }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RatingAreas", scope = CustomProduct.class)
    public JAXBElement<ArrayOfCustomProductRatingArea> createCustomProductRatingAreas(ArrayOfCustomProductRatingArea value) {
        return new JAXBElement<ArrayOfCustomProductRatingArea>(_CustomProductRatingAreas_QNAME, ArrayOfCustomProductRatingArea.class, CustomProduct.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Market }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Market", scope = CustomProduct.class)
    public JAXBElement<Market> createCustomProductMarket(Market value) {
        return new JAXBElement<Market>(_Market_QNAME, Market.class, CustomProduct.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmissionId", scope = CustomProduct.class)
    public JAXBElement<Integer> createCustomProductSubmissionId(Integer value) {
        return new JAXBElement<Integer>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierSubmissionId_QNAME, Integer.class, CustomProduct.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyDrugs", scope = GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier.class)
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugs(ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrugs_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug.class, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TierType", scope = GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier.class)
    public JAXBElement<String> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierTierType(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierTierType_QNAME, String.class, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CostSharingTypeVOs", scope = GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier.class)
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> createGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOs(ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO>(_FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOs_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestIfpRateFactor }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpRateFactors", scope = GetIfpQuoteRequest.class)
    public JAXBElement<GetIfpQuoteRequestIfpRateFactor> createGetIfpQuoteRequestIfpRateFactors(GetIfpQuoteRequestIfpRateFactor value) {
        return new JAXBElement<GetIfpQuoteRequestIfpRateFactor>(_GetIfpQuoteRequestIfpRateFactors_QNAME, GetIfpQuoteRequestIfpRateFactor.class, GetIfpQuoteRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestIfpRateKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IfpRateKeys", scope = GetIfpQuoteRequest.class)
    public JAXBElement<GetIfpQuoteRequestIfpRateKey> createGetIfpQuoteRequestIfpRateKeys(GetIfpQuoteRequestIfpRateKey value) {
        return new JAXBElement<GetIfpQuoteRequestIfpRateKey>(_GetIfpQuoteRequestIfpRateKeys_QNAME, GetIfpQuoteRequestIfpRateKey.class, GetIfpQuoteRequest.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsuranceType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "InsuranceType", scope = GetCarriersPlansBenefitsRequestCarrierPlanFilter.class)
    public JAXBElement<InsuranceType> createGetCarriersPlansBenefitsRequestCarrierPlanFilterInsuranceType(InsuranceType value) {
        return new JAXBElement<InsuranceType>(_InsuranceType_QNAME, InsuranceType.class, GetCarriersPlansBenefitsRequestCarrierPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanId", scope = GetCarriersPlansBenefitsRequestCarrierPlanFilter.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsRequestCarrierPlanFilterPlanId(String value) {
        return new JAXBElement<String>(_PlanChoiceBasePlanId_QNAME, String.class, GetCarriersPlansBenefitsRequestCarrierPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssuerId", scope = GetCarriersPlansBenefitsRequestCarrierPlanFilter.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsRequestCarrierPlanFilterIssuerId(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierIssuerId_QNAME, String.class, GetCarriersPlansBenefitsRequestCarrierPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "State", scope = GetCarriersPlansBenefitsRequestCarrierPlanFilter.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsRequestCarrierPlanFilterState(String value) {
        return new JAXBElement<String>(_LocationAddressBaseState_QNAME, String.class, GetCarriersPlansBenefitsRequestCarrierPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CompanyId", scope = GetCarriersPlansBenefitsRequestCarrierPlanFilter.class)
    public JAXBElement<Integer> createGetCarriersPlansBenefitsRequestCarrierPlanFilterCompanyId(Integer value) {
        return new JAXBElement<Integer>(_CarrierBaseCompanyId_QNAME, Integer.class, GetCarriersPlansBenefitsRequestCarrierPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierId", scope = GetCarriersPlansBenefitsRequestCarrierPlanFilter.class)
    public JAXBElement<Integer> createGetCarriersPlansBenefitsRequestCarrierPlanFilterCarrierId(Integer value) {
        return new JAXBElement<Integer>(_FetchCustomProductsRequestInputProductFilterCarrierId_QNAME, Integer.class, GetCarriersPlansBenefitsRequestCarrierPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitLevel", scope = GetCarriersPlansBenefitsRequestCarrierPlanFilter.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsRequestCarrierPlanFilterBenefitLevel(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel_QNAME, String.class, GetCarriersPlansBenefitsRequestCarrierPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanType", scope = GetCarriersPlansBenefitsRequestCarrierPlanFilter.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsRequestCarrierPlanFilterPlanType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioPlanType_QNAME, String.class, GetCarriersPlansBenefitsRequestCarrierPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsPlanVisibility }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanVisibility", scope = GetCarriersPlansBenefitsRequestCarrierPlanFilter.class)
    public JAXBElement<GetCarriersPlansBenefitsPlanVisibility> createGetCarriersPlansBenefitsRequestCarrierPlanFilterPlanVisibility(GetCarriersPlansBenefitsPlanVisibility value) {
        return new JAXBElement<GetCarriersPlansBenefitsPlanVisibility>(_PlanVisibility_QNAME, GetCarriersPlansBenefitsPlanVisibility.class, GetCarriersPlansBenefitsRequestCarrierPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AddOns", scope = GetPlansbyRxRequestInput.class)
    public JAXBElement<ArrayOfGetPlansbyRxAddOn> createGetPlansbyRxRequestInputAddOns(ArrayOfGetPlansbyRxAddOn value) {
        return new JAXBElement<ArrayOfGetPlansbyRxAddOn>(_GetGroupQuoteRequestPreferenceAddOns_QNAME, ArrayOfGetPlansbyRxAddOn.class, GetPlansbyRxRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RxCUIs", scope = GetPlansbyRxRequestInput.class)
    public JAXBElement<RxCUIsBase> createGetPlansbyRxRequestInputRxCUIs(RxCUIsBase value) {
        return new JAXBElement<RxCUIsBase>(_FormularyLookupRequestDrugRxCUIs_QNAME, RxCUIsBase.class, GetPlansbyRxRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupSourceReferenceId", scope = Group.class)
    public JAXBElement<String> createGroupGroupSourceReferenceId(String value) {
        return new JAXBElement<String>(_GroupGroupSourceReferenceId_QNAME, String.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGroupRAFOverride }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RAFOverrides", scope = Group.class)
    public JAXBElement<ArrayOfGroupRAFOverride> createGroupRAFOverrides(ArrayOfGroupRAFOverride value) {
        return new JAXBElement<ArrayOfGroupRAFOverride>(_GetGroupQuoteRequestGroupRateFactorRAFOverrides_QNAME, ArrayOfGroupRAFOverride.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChangeCodes", scope = Group.class)
    public JAXBElement<String> createGroupChangeCodes(String value) {
        return new JAXBElement<String>(_MemberBaseChangeCodes_QNAME, String.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupSource", scope = Group.class)
    public JAXBElement<String> createGroupGroupSource(String value) {
        return new JAXBElement<String>(_GroupGroupSource_QNAME, String.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Comments", scope = Group.class)
    public JAXBElement<String> createGroupComments(String value) {
        return new JAXBElement<String>(_FamilyComments_QNAME, String.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PartnerData", scope = Group.class)
    public JAXBElement<String> createGroupPartnerData(String value) {
        return new JAXBElement<String>(_MemberBasePartnerData_QNAME, String.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupContact }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SecondaryContact", scope = Group.class)
    public JAXBElement<GroupContact> createGroupSecondaryContact(GroupContact value) {
        return new JAXBElement<GroupContact>(_FamilySecondaryContact_QNAME, GroupContact.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = Group.class)
    public JAXBElement<XMLGregorianCalendar> createGroupEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupLocationAddress }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MailingAddress", scope = Group.class)
    public JAXBElement<GroupLocationAddress> createGroupMailingAddress(GroupLocationAddress value) {
        return new JAXBElement<GroupLocationAddress>(_FamilyMailingAddress_QNAME, GroupLocationAddress.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OriginatingUserAgent", scope = Group.class)
    public JAXBElement<String> createGroupOriginatingUserAgent(String value) {
        return new JAXBElement<String>(_FamilyOriginatingUserAgent_QNAME, String.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GroupId", scope = Group.class)
    public JAXBElement<Integer> createGroupGroupId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupRequestGroupId_QNAME, Integer.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ExpirationDate", scope = Group.class)
    public JAXBElement<XMLGregorianCalendar> createGroupExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FamilyExpirationDate_QNAME, XMLGregorianCalendar.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GroupShoppingCartsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ShoppingCarts", scope = Group.class)
    public JAXBElement<GroupShoppingCartsBase> createGroupShoppingCarts(GroupShoppingCartsBase value) {
        return new JAXBElement<GroupShoppingCartsBase>(_SubmitGroupResponseShoppingCarts_QNAME, GroupShoppingCartsBase.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGroupScenario }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ERContributionScenarios", scope = Group.class)
    public JAXBElement<ArrayOfGroupScenario> createGroupERContributionScenarios(ArrayOfGroupScenario value) {
        return new JAXBElement<ArrayOfGroupScenario>(_GetGroupQuoteRequestGroupRateFactorERContributionScenarios_QNAME, ArrayOfGroupScenario.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SIC", scope = Group.class)
    public JAXBElement<Integer> createGroupSIC(Integer value) {
        return new JAXBElement<Integer>(_GetGroupQuoteRequestGroupRateFactorSIC_QNAME, Integer.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ContactId", scope = Group.class)
    public JAXBElement<Integer> createGroupContactId(Integer value) {
        return new JAXBElement<Integer>(_GetGroupRequestContactId_QNAME, Integer.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OESConsumerId", scope = Group.class)
    public JAXBElement<String> createGroupOESConsumerId(String value) {
        return new JAXBElement<String>(_FamilyOESConsumerId_QNAME, String.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OriginatingIPAddress", scope = Group.class)
    public JAXBElement<String> createGroupOriginatingIPAddress(String value) {
        return new JAXBElement<String>(_FamilyOriginatingIPAddress_QNAME, String.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LifeMultiplier", scope = Group.class)
    public JAXBElement<BigDecimal> createGroupLifeMultiplier(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GroupLifeMultiplier_QNAME, BigDecimal.class, Group.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitsLink", scope = PlanDetailsBase.class)
    public JAXBElement<String> createPlanDetailsBaseBenefitsLink(String value) {
        return new JAXBElement<String>(_PlanDetailsBaseBenefitsLink_QNAME, String.class, PlanDetailsBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitLevel", scope = PlanDetailsBase.class)
    public JAXBElement<String> createPlanDetailsBaseBenefitLevel(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel_QNAME, String.class, PlanDetailsBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProviderLink", scope = PlanDetailsBase.class)
    public JAXBElement<String> createPlanDetailsBaseProviderLink(String value) {
        return new JAXBElement<String>(_PlanDetailsBaseProviderLink_QNAME, String.class, PlanDetailsBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "UnderwritingLink", scope = PlanDetailsBase.class)
    public JAXBElement<String> createPlanDetailsBaseUnderwritingLink(String value) {
        return new JAXBElement<String>(_PlanDetailsBaseUnderwritingLink_QNAME, String.class, PlanDetailsBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ViewPoints", scope = GetPlansbyRxResponseCarrierPlanBenefitCoverage.class)
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint> createGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoints(ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint>(_GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoints_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint.class, GetPlansbyRxResponseCarrierPlanBenefitCoverage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LeadId", scope = GroupShoppingCartProductChoicePlanChoices.class)
    public JAXBElement<String> createGroupShoppingCartProductChoicePlanChoicesLeadId(String value) {
        return new JAXBElement<String>(_GetGroupRequestLeadId_QNAME, String.class, GroupShoppingCartProductChoicePlanChoices.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Premium", scope = GroupShoppingCartProductChoicePlanChoices.class)
    public JAXBElement<BigDecimal> createGroupShoppingCartProductChoicePlanChoicesPremium(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IfpPlanChoiceBasePremium_QNAME, BigDecimal.class, GroupShoppingCartProductChoicePlanChoices.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ApplicationId", scope = GroupShoppingCartProductChoicePlanChoices.class)
    public JAXBElement<String> createGroupShoppingCartProductChoicePlanChoicesApplicationId(String value) {
        return new JAXBElement<String>(_IfpShoppingCartApplicationId_QNAME, String.class, GroupShoppingCartProductChoicePlanChoices.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LifeAmount", scope = GroupShoppingCartProductChoicePlanChoices.class)
    public JAXBElement<BigDecimal> createGroupShoppingCartProductChoicePlanChoicesLifeAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IfpPlanChoiceBaseLifeAmount_QNAME, BigDecimal.class, GroupShoppingCartProductChoicePlanChoices.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MemberRates", scope = GetIfpQuoteResponseQuoteCarrierRatePlanRate.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate> createGetIfpQuoteResponseQuoteCarrierRatePlanRateMemberRates(ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRateMemberRates_QNAME, ArrayOfGetIfpQuoteResponseQuoteCarrierRateMemberRate.class, GetIfpQuoteResponseQuoteCarrierRatePlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NumberOfDays", scope = GetIfpQuoteResponseQuoteCarrierRatePlanRate.class)
    public JAXBElement<String> createGetIfpQuoteResponseQuoteCarrierRatePlanRateNumberOfDays(String value) {
        return new JAXBElement<String>(_IfpRateBaseNumberOfDays_QNAME, String.class, GetIfpQuoteResponseQuoteCarrierRatePlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponsePlanDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanDetails", scope = GetIfpQuoteResponseQuoteCarrierRatePlanRate.class)
    public JAXBElement<GetIfpQuoteResponsePlanDetail> createGetIfpQuoteResponseQuoteCarrierRatePlanRatePlanDetails(GetIfpQuoteResponsePlanDetail value) {
        return new JAXBElement<GetIfpQuoteResponsePlanDetail>(_GetGroupQuoteResponseQuoteCarrierRatePlanRatePlanDetails_QNAME, GetIfpQuoteResponsePlanDetail.class, GetIfpQuoteResponseQuoteCarrierRatePlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoveredRxCUIs", scope = GetIfpQuoteResponseQuoteCarrierRatePlanRate.class)
    public JAXBElement<RxCUIsBase> createGetIfpQuoteResponseQuoteCarrierRatePlanRateCoveredRxCUIs(RxCUIsBase value) {
        return new JAXBElement<RxCUIsBase>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRateCoveredRxCUIs_QNAME, RxCUIsBase.class, GetIfpQuoteResponseQuoteCarrierRatePlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BaseRateUnit", scope = GetIfpQuoteResponseQuoteCarrierRatePlanRate.class)
    public JAXBElement<String> createGetIfpQuoteResponseQuoteCarrierRatePlanRateBaseRateUnit(String value) {
        return new JAXBElement<String>(_IfpRateBaseBaseRateUnit_QNAME, String.class, GetIfpQuoteResponseQuoteCarrierRatePlanRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssuerId", scope = CarrierDetailsBase.class)
    public JAXBElement<String> createCarrierDetailsBaseIssuerId(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierIssuerId_QNAME, String.class, CarrierDetailsBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DisclaimerHtml", scope = CarrierDetailsBase.class)
    public JAXBElement<String> createCarrierDetailsBaseDisclaimerHtml(String value) {
        return new JAXBElement<String>(_CarrierBaseDisclaimerHtml_QNAME, String.class, CarrierDetailsBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseCarrierDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierDetails", scope = GetGroupQuoteResponseQuoteCarrierRate.class)
    public JAXBElement<GetGroupQuoteResponseCarrierDetail> createGetGroupQuoteResponseQuoteCarrierRateCarrierDetails(GetGroupQuoteResponseCarrierDetail value) {
        return new JAXBElement<GetGroupQuoteResponseCarrierDetail>(_GetIfpQuoteResponseQuoteCarrierRateCarrierDetails_QNAME, GetGroupQuoteResponseCarrierDetail.class, GetGroupQuoteResponseQuoteCarrierRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanRates", scope = GetGroupQuoteResponseQuoteCarrierRate.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate> createGetGroupQuoteResponseQuoteCarrierRatePlanRates(ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRates_QNAME, ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate.class, GetGroupQuoteResponseQuoteCarrierRate.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitsLink", scope = PlanBase.class)
    public JAXBElement<String> createPlanBaseBenefitsLink(String value) {
        return new JAXBElement<String>(_PlanDetailsBaseBenefitsLink_QNAME, String.class, PlanBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyId", scope = PlanBase.class)
    public JAXBElement<String> createPlanBaseFormularyId(String value) {
        return new JAXBElement<String>(_PlanBaseFormularyId_QNAME, String.class, PlanBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitLevel", scope = PlanBase.class)
    public JAXBElement<String> createPlanBaseBenefitLevel(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel_QNAME, String.class, PlanBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProviderLink", scope = PlanBase.class)
    public JAXBElement<String> createPlanBaseProviderLink(String value) {
        return new JAXBElement<String>(_PlanDetailsBaseProviderLink_QNAME, String.class, PlanBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "UnderwritingLink", scope = PlanBase.class)
    public JAXBElement<String> createPlanBaseUnderwritingLink(String value) {
        return new JAXBElement<String>(_PlanDetailsBaseUnderwritingLink_QNAME, String.class, PlanBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCountiesResponseCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Counties", scope = GetCountiesResponse.class)
    public JAXBElement<ArrayOfGetCountiesResponseCounty> createGetCountiesResponseCounties(ArrayOfGetCountiesResponseCounty value) {
        return new JAXBElement<ArrayOfGetCountiesResponseCounty>(_GetCountiesResponseCounties_QNAME, ArrayOfGetCountiesResponseCounty.class, GetCountiesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierId", scope = RAFOverrideBase.class)
    public JAXBElement<Integer> createRAFOverrideBaseCarrierId(Integer value) {
        return new JAXBElement<Integer>(_FetchCustomProductsRequestInputProductFilterCarrierId_QNAME, Integer.class, RAFOverrideBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RAF", scope = RAFOverrideBase.class)
    public JAXBElement<BigDecimal> createRAFOverrideBaseRAF(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_RAFOverrideBaseRAF_QNAME, BigDecimal.class, RAFOverrideBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Services", scope = GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService> createGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointServices(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService>(_GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointServices_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService.class, GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmissionId", scope = FetchCustomProductsRequestInput.class)
    public JAXBElement<Integer> createFetchCustomProductsRequestInputSubmissionId(Integer value) {
        return new JAXBElement<Integer>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierSubmissionId_QNAME, Integer.class, FetchCustomProductsRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFetchCustomProductsRequestInputProductFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductFilters", scope = FetchCustomProductsRequestInput.class)
    public JAXBElement<ArrayOfFetchCustomProductsRequestInputProductFilter> createFetchCustomProductsRequestInputProductFilters(ArrayOfFetchCustomProductsRequestInputProductFilter value) {
        return new JAXBElement<ArrayOfFetchCustomProductsRequestInputProductFilter>(_FetchCustomProductsRequestInputProductFilters_QNAME, ArrayOfFetchCustomProductsRequestInputProductFilter.class, FetchCustomProductsRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanDetails", scope = GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan.class)
    public JAXBElement<GetGroupQuoteResponsePlanDetail> createGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlanPlanDetails(GetGroupQuoteResponsePlanDetail value) {
        return new JAXBElement<GetGroupQuoteResponsePlanDetail>(_GetGroupQuoteResponseQuoteCarrierRatePlanRatePlanDetails_QNAME, GetGroupQuoteResponsePlanDetail.class, GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OnCobra", scope = GetGroupQuoteRequestGroupRateFactorEmployee.class)
    public JAXBElement<Boolean> createGetGroupQuoteRequestGroupRateFactorEmployeeOnCobra(Boolean value) {
        return new JAXBElement<Boolean>(_GetGroupQuoteRequestGroupRateFactorEmployeeOnCobra_QNAME, Boolean.class, GetGroupQuoteRequestGroupRateFactorEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WorksiteZipCode", scope = GetGroupQuoteRequestGroupRateFactorEmployee.class)
    public JAXBElement<Integer> createGetGroupQuoteRequestGroupRateFactorEmployeeWorksiteZipCode(Integer value) {
        return new JAXBElement<Integer>(_GetGroupQuoteRequestGroupRateFactorEmployeeWorksiteZipCode_QNAME, Integer.class, GetGroupQuoteRequestGroupRateFactorEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WorksiteCountyName", scope = GetGroupQuoteRequestGroupRateFactorEmployee.class)
    public JAXBElement<String> createGetGroupQuoteRequestGroupRateFactorEmployeeWorksiteCountyName(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorEmployeeWorksiteCountyName_QNAME, String.class, GetGroupQuoteRequestGroupRateFactorEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductChoices", scope = GetGroupQuoteRequestGroupRateFactorEmployee.class)
    public JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice> createGetGroupQuoteRequestGroupRateFactorEmployeeProductChoices(ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice value) {
        return new JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice>(_IfpShoppingCartProductChoices_QNAME, ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice.class, GetGroupQuoteRequestGroupRateFactorEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IncludeInQuote", scope = GetGroupQuoteRequestGroupRateFactorEmployee.class)
    public JAXBElement<Boolean> createGetGroupQuoteRequestGroupRateFactorEmployeeIncludeInQuote(Boolean value) {
        return new JAXBElement<Boolean>(_FamilyMemberIncludeInQuote_QNAME, Boolean.class, GetGroupQuoteRequestGroupRateFactorEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedRates", scope = GetIfpQuoteResponseQuoteCarrierRateModified.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier> createGetIfpQuoteResponseQuoteCarrierRateModifiedDroppedRates(ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier>(_GetGroupQuoteResponseQuoteDroppedRates_QNAME, ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier.class, GetIfpQuoteResponseQuoteCarrierRateModified.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoveredRxCUIs", scope = GetIfpQuoteResponseQuoteCarrierRateModified.class)
    public JAXBElement<RxCUIsBase> createGetIfpQuoteResponseQuoteCarrierRateModifiedCoveredRxCUIs(RxCUIsBase value) {
        return new JAXBElement<RxCUIsBase>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRateCoveredRxCUIs_QNAME, RxCUIsBase.class, GetIfpQuoteResponseQuoteCarrierRateModified.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ViewPoints", scope = GetGroupQuoteResponsePlanDetailBenefitCoverage.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint> createGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoints(ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint>(_GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoints_QNAME, ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint.class, GetGroupQuoteResponsePlanDetailBenefitCoverage.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpShoppingCartsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ShoppingCarts", scope = GetIfpShoppingCartsResponse.class)
    public JAXBElement<IfpShoppingCartsBase> createGetIfpShoppingCartsResponseShoppingCarts(IfpShoppingCartsBase value) {
        return new JAXBElement<IfpShoppingCartsBase>(_SubmitGroupResponseShoppingCarts_QNAME, IfpShoppingCartsBase.class, GetIfpShoppingCartsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupRequestDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugFilters", scope = FormularyLookupRequestInput.class)
    public JAXBElement<FormularyLookupRequestDrug> createFormularyLookupRequestInputDrugFilters(FormularyLookupRequestDrug value) {
        return new JAXBElement<FormularyLookupRequestDrug>(_GetGroupQuoteRequestPreferenceDrugFilters_QNAME, FormularyLookupRequestDrug.class, FormularyLookupRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupRequestPlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanFilters", scope = FormularyLookupRequestInput.class)
    public JAXBElement<ArrayOfFormularyLookupRequestPlanFilter> createFormularyLookupRequestInputPlanFilters(ArrayOfFormularyLookupRequestPlanFilter value) {
        return new JAXBElement<ArrayOfFormularyLookupRequestPlanFilter>(_GetGroupQuoteRequestPreferencePlanFilters_QNAME, ArrayOfFormularyLookupRequestPlanFilter.class, FormularyLookupRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IfpShoppingCartsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ShoppingCarts", scope = SubmitFamilyResponse.class)
    public JAXBElement<IfpShoppingCartsBase> createSubmitFamilyResponseShoppingCarts(IfpShoppingCartsBase value) {
        return new JAXBElement<IfpShoppingCartsBase>(_SubmitGroupResponseShoppingCarts_QNAME, IfpShoppingCartsBase.class, SubmitFamilyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetZipCodeInfoResponseZipCodeInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZipCodes", scope = GetZipCodeInfoResponse.class)
    public JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfo> createGetZipCodeInfoResponseZipCodes(ArrayOfGetZipCodeInfoResponseZipCodeInfo value) {
        return new JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfo>(_GetZipCodeInfoResponseZipCodes_QNAME, ArrayOfGetZipCodeInfoResponseZipCodeInfo.class, GetZipCodeInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedReason", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier.class)
    public JAXBElement<String> createGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedReason(String value) {
        return new JAXBElement<String>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedReason_QNAME, String.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CompanyName", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier.class)
    public JAXBElement<String> createGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierCompanyName(String value) {
        return new JAXBElement<String>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierCompanyName_QNAME, String.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedPlans", scope = GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier.class)
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan> createGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlans(ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan>(_GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlans_QNAME, ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedPlan.class, GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductRatingAreaZipRange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZipRanges", scope = CustomProductRatingArea.class)
    public JAXBElement<ArrayOfCustomProductRatingAreaZipRange> createCustomProductRatingAreaZipRanges(ArrayOfCustomProductRatingAreaZipRange value) {
        return new JAXBElement<ArrayOfCustomProductRatingAreaZipRange>(_CustomProductRatingAreaZipRanges_QNAME, ArrayOfCustomProductRatingAreaZipRange.class, CustomProductRatingArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductRatingAreaZipCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZipCounties", scope = CustomProductRatingArea.class)
    public JAXBElement<ArrayOfCustomProductRatingAreaZipCounty> createCustomProductRatingAreaZipCounties(ArrayOfCustomProductRatingAreaZipCounty value) {
        return new JAXBElement<ArrayOfCustomProductRatingAreaZipCounty>(_CustomProductRatingAreaZipCounties_QNAME, ArrayOfCustomProductRatingAreaZipCounty.class, CustomProductRatingArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProductRatingAreaCounty }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Counties", scope = CustomProductRatingArea.class)
    public JAXBElement<ArrayOfCustomProductRatingAreaCounty> createCustomProductRatingAreaCounties(ArrayOfCustomProductRatingAreaCounty value) {
        return new JAXBElement<ArrayOfCustomProductRatingAreaCounty>(_GetCountiesResponseCounties_QNAME, ArrayOfCustomProductRatingAreaCounty.class, CustomProductRatingArea.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitTemplate", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<String> createGetIfpQuoteRequestPreferenceBenefitTemplate(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitTemplate_QNAME, String.class, GetIfpQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AsOfDate", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<XMLGregorianCalendar> createGetIfpQuoteRequestPreferenceAsOfDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestPreferenceAsOfDate_QNAME, XMLGregorianCalendar.class, GetIfpQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RateSet", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<List<String>> createGetIfpQuoteRequestPreferenceRateSet(List<String> value) {
        return new JAXBElement<List<String>>(_RateSet_QNAME, ((Class) List.class), GetIfpQuoteRequestPreference.class, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChoiceFilters", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter> createGetIfpQuoteRequestPreferenceChoiceFilters(ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter>(_GetGroupQuoteRequestPreferenceChoiceFilters_QNAME, ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter.class, GetIfpQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AddOns", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetIfpQuoteAddOn> createGetIfpQuoteRequestPreferenceAddOns(ArrayOfGetIfpQuoteAddOn value) {
        return new JAXBElement<ArrayOfGetIfpQuoteAddOn>(_GetGroupQuoteRequestPreferenceAddOns_QNAME, ArrayOfGetIfpQuoteAddOn.class, GetIfpQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceDrugFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugFilters", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceDrugFilter> createGetIfpQuoteRequestPreferenceDrugFilters(ArrayOfGetIfpQuoteRequestPreferenceDrugFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceDrugFilter>(_GetGroupQuoteRequestPreferenceDrugFilters_QNAME, ArrayOfGetIfpQuoteRequestPreferenceDrugFilter.class, GetIfpQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferencePlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanFilters", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferencePlanFilter> createGetIfpQuoteRequestPreferencePlanFilters(ArrayOfGetIfpQuoteRequestPreferencePlanFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferencePlanFilter>(_GetGroupQuoteRequestPreferencePlanFilters_QNAME, ArrayOfGetIfpQuoteRequestPreferencePlanFilter.class, GetIfpQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestPreferenceBenchmarkRules }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenchmarkRule", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<GetIfpQuoteRequestPreferenceBenchmarkRules> createGetIfpQuoteRequestPreferenceBenchmarkRule(GetIfpQuoteRequestPreferenceBenchmarkRules value) {
        return new JAXBElement<GetIfpQuoteRequestPreferenceBenchmarkRules>(_GetIfpQuoteRequestPreferenceBenchmarkRule_QNAME, GetIfpQuoteRequestPreferenceBenchmarkRules.class, GetIfpQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitFilters", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter> createGetIfpQuoteRequestPreferenceBenefitFilters(ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter>(_GetGroupQuoteRequestPreferenceBenefitFilters_QNAME, ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter.class, GetIfpQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LevelOfBenefitAddOns }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LevelOfBenefitAddOns", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<LevelOfBenefitAddOns> createGetIfpQuoteRequestPreferenceLevelOfBenefitAddOns(LevelOfBenefitAddOns value) {
        return new JAXBElement<LevelOfBenefitAddOns>(_LevelOfBenefitAddOns_QNAME, LevelOfBenefitAddOns.class, GetIfpQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceVersionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VersionFilters", scope = GetIfpQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceVersionFilter> createGetIfpQuoteRequestPreferenceVersionFilters(ArrayOfGetIfpQuoteRequestPreferenceVersionFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceVersionFilter>(_GetGroupQuoteRequestPreferenceVersionFilters_QNAME, ArrayOfGetIfpQuoteRequestPreferenceVersionFilter.class, GetIfpQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssuerId", scope = FormularyLookupRequestPlanFilter.class)
    public JAXBElement<String> createFormularyLookupRequestPlanFilterIssuerId(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierIssuerId_QNAME, String.class, FormularyLookupRequestPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanId", scope = FormularyLookupRequestPlanFilter.class)
    public JAXBElement<String> createFormularyLookupRequestPlanFilterPlanId(String value) {
        return new JAXBElement<String>(_PlanChoiceBasePlanId_QNAME, String.class, FormularyLookupRequestPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierId", scope = FormularyLookupRequestPlanFilter.class)
    public JAXBElement<Integer> createFormularyLookupRequestPlanFilterCarrierId(Integer value) {
        return new JAXBElement<Integer>(_FetchCustomProductsRequestInputProductFilterCarrierId_QNAME, Integer.class, FormularyLookupRequestPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AsOfDate", scope = GetCarriersPlansBenefitsRequestInput.class)
    public JAXBElement<XMLGregorianCalendar> createGetCarriersPlansBenefitsRequestInputAsOfDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestPreferenceAsOfDate_QNAME, XMLGregorianCalendar.class, GetCarriersPlansBenefitsRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitTemplate", scope = GetCarriersPlansBenefitsRequestInput.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsRequestInputBenefitTemplate(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitTemplate_QNAME, String.class, GetCarriersPlansBenefitsRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierPlanFilters", scope = GetCarriersPlansBenefitsRequestInput.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter> createGetCarriersPlansBenefitsRequestInputCarrierPlanFilters(ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter>(_GetCarriersPlansBenefitsRequestInputCarrierPlanFilters_QNAME, ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter.class, GetCarriersPlansBenefitsRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsAddOn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AddOns", scope = GetCarriersPlansBenefitsRequestInput.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsAddOn> createGetCarriersPlansBenefitsRequestInputAddOns(ArrayOfGetCarriersPlansBenefitsAddOn value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsAddOn>(_GetGroupQuoteRequestPreferenceAddOns_QNAME, ArrayOfGetCarriersPlansBenefitsAddOn.class, GetCarriersPlansBenefitsRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitFilters", scope = GetCarriersPlansBenefitsRequestInput.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter> createGetCarriersPlansBenefitsRequestInputBenefitFilters(ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter>(_GetGroupQuoteRequestPreferenceBenefitFilters_QNAME, ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter.class, GetCarriersPlansBenefitsRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LatestVersionOnly", scope = GetCarriersPlansBenefitsRequestInput.class)
    public JAXBElement<Boolean> createGetCarriersPlansBenefitsRequestInputLatestVersionOnly(Boolean value) {
        return new JAXBElement<Boolean>(_GetCarriersPlansBenefitsRequestInputLatestVersionOnly_QNAME, Boolean.class, GetCarriersPlansBenefitsRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DataSet", scope = GetCarriersPlansBenefitsRequestInput.class)
    public JAXBElement<List<String>> createGetCarriersPlansBenefitsRequestInputDataSet(List<String> value) {
        return new JAXBElement<List<String>>(_DataSet_QNAME, ((Class) List.class), GetCarriersPlansBenefitsRequestInput.class, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequestDrugFilters }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugFilter", scope = GetCarriersPlansBenefitsRequestInput.class)
    public JAXBElement<GetCarriersPlansBenefitsRequestDrugFilters> createGetCarriersPlansBenefitsRequestInputDrugFilter(GetCarriersPlansBenefitsRequestDrugFilters value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequestDrugFilters>(_GetCarriersPlansBenefitsRequestInputDrugFilter_QNAME, GetCarriersPlansBenefitsRequestDrugFilters.class, GetCarriersPlansBenefitsRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestVersionFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VersionFilters", scope = GetCarriersPlansBenefitsRequestInput.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestVersionFilter> createGetCarriersPlansBenefitsRequestInputVersionFilters(ArrayOfGetCarriersPlansBenefitsRequestVersionFilter value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestVersionFilter>(_GetGroupQuoteRequestPreferenceVersionFilters_QNAME, ArrayOfGetCarriersPlansBenefitsRequestVersionFilter.class, GetCarriersPlansBenefitsRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Carriers", scope = GetIfpQuoteResponseQuoteMemberPlan.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrier> createGetIfpQuoteResponseQuoteMemberPlanCarriers(ArrayOfGetIfpQuoteResponseQuoteCarrier value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrier>(_GetGroupQuoteResponseQuoteCarriers_QNAME, ArrayOfGetIfpQuoteResponseQuoteCarrier.class, GetIfpQuoteResponseQuoteMemberPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedRates", scope = GetIfpQuoteResponseQuoteMemberPlan.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier> createGetIfpQuoteResponseQuoteMemberPlanDroppedRates(ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier>(_GetGroupQuoteResponseQuoteDroppedRates_QNAME, ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier.class, GetIfpQuoteResponseQuoteMemberPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanData", scope = GetCarriersPlansBenefitsResponseCarrierPlan.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> createGetCarriersPlansBenefitsResponseCarrierPlanPlanData(ArrayOfGetCarriersPlansBenefitsResponseItem value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem>(_GetIfpQuoteResponsePlanDetailPlanData_QNAME, ArrayOfGetCarriersPlansBenefitsResponseItem.class, GetCarriersPlansBenefitsResponseCarrierPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoveredRxCUIs", scope = GetCarriersPlansBenefitsResponseCarrierPlan.class)
    public JAXBElement<RxCUIsBase> createGetCarriersPlansBenefitsResponseCarrierPlanCoveredRxCUIs(RxCUIsBase value) {
        return new JAXBElement<RxCUIsBase>(_GetGroupQuoteResponseQuoteEmployeePlanCarrierRateCoveredRxCUIs_QNAME, RxCUIsBase.class, GetCarriersPlansBenefitsResponseCarrierPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Benefits", scope = GetCarriersPlansBenefitsResponseCarrierPlan.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit> createGetCarriersPlansBenefitsResponseCarrierPlanBenefits(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit>(_GetIfpQuoteResponsePlanDetailBenefits_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit.class, GetCarriersPlansBenefitsResponseCarrierPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanLinks", scope = GetCarriersPlansBenefitsResponseCarrierPlan.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink> createGetCarriersPlansBenefitsResponseCarrierPlanPlanLinks(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink>(_GetCarriersPlansBenefitsResponseCarrierPlanPlanLinks_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink.class, GetCarriersPlansBenefitsResponseCarrierPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanId", scope = ListRxRequestPlanFilter.class)
    public JAXBElement<String> createListRxRequestPlanFilterPlanId(String value) {
        return new JAXBElement<String>(_PlanChoiceBasePlanId_QNAME, String.class, ListRxRequestPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssuerId", scope = ListRxRequestPlanFilter.class)
    public JAXBElement<String> createListRxRequestPlanFilterIssuerId(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierIssuerId_QNAME, String.class, ListRxRequestPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierId", scope = ListRxRequestPlanFilter.class)
    public JAXBElement<Integer> createListRxRequestPlanFilterCarrierId(Integer value) {
        return new JAXBElement<Integer>(_FetchCustomProductsRequestInputProductFilterCarrierId_QNAME, Integer.class, ListRxRequestPlanFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Cost", scope = BenefitBase.class)
    public JAXBElement<BigDecimal> createBenefitBaseCost(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ServiceBaseCost_QNAME, BigDecimal.class, BenefitBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Amount", scope = BenefitBase.class)
    public JAXBElement<BigDecimal> createBenefitBaseAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ServiceBaseAmount_QNAME, BigDecimal.class, BenefitBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Category", scope = BenefitBase.class)
    public JAXBElement<String> createBenefitBaseCategory(String value) {
        return new JAXBElement<String>(_BenefitBaseCategory_QNAME, String.class, BenefitBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TinyValue", scope = BenefitBase.class)
    public JAXBElement<String> createBenefitBaseTinyValue(String value) {
        return new JAXBElement<String>(_BenefitBaseTinyValue_QNAME, String.class, BenefitBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FullValue", scope = BenefitBase.class)
    public JAXBElement<String> createBenefitBaseFullValue(String value) {
        return new JAXBElement<String>(_BenefitBaseFullValue_QNAME, String.class, BenefitBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FlagValue", scope = BenefitBase.class)
    public JAXBElement<List<String>> createBenefitBaseFlagValue(List<String> value) {
        return new JAXBElement<List<String>>(_FlagValue_QNAME, ((Class) List.class), BenefitBase.class, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AsOfDate", scope = GetCustomProductsQuoteRequestPreference.class)
    public JAXBElement<XMLGregorianCalendar> createGetCustomProductsQuoteRequestPreferenceAsOfDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestPreferenceAsOfDate_QNAME, XMLGregorianCalendar.class, GetCustomProductsQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "QuoteState", scope = GetCustomProductsQuoteRequestPreference.class)
    public JAXBElement<String> createGetCustomProductsQuoteRequestPreferenceQuoteState(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceQuoteState_QNAME, String.class, GetCustomProductsQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = GetCustomProductsQuoteRequestPreference.class)
    public JAXBElement<XMLGregorianCalendar> createGetCustomProductsQuoteRequestPreferenceEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, GetCustomProductsQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductFilters", scope = GetCustomProductsQuoteRequestPreference.class)
    public JAXBElement<ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter> createGetCustomProductsQuoteRequestPreferenceProductFilters(ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter value) {
        return new JAXBElement<ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter>(_FetchCustomProductsRequestInputProductFilters_QNAME, ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter.class, GetCustomProductsQuoteRequestPreference.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DateLastSmoked", scope = GetIfpQuoteRequestMember.class)
    public JAXBElement<XMLGregorianCalendar> createGetIfpQuoteRequestMemberDateLastSmoked(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MemberBaseDateLastSmoked_QNAME, XMLGregorianCalendar.class, GetIfpQuoteRequestMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IsSmoker", scope = GetIfpQuoteRequestMember.class)
    public JAXBElement<Boolean> createGetIfpQuoteRequestMemberIsSmoker(Boolean value) {
        return new JAXBElement<Boolean>(_MemberBaseIsSmoker_QNAME, Boolean.class, GetIfpQuoteRequestMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyName", scope = GetIfpQuoteRequestMember.class)
    public JAXBElement<String> createGetIfpQuoteRequestMemberCountyName(String value) {
        return new JAXBElement<String>(_MemberBaseCountyName_QNAME, String.class, GetIfpQuoteRequestMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ZipCode", scope = GetIfpQuoteRequestMember.class)
    public JAXBElement<Integer> createGetIfpQuoteRequestMemberZipCode(Integer value) {
        return new JAXBElement<Integer>(_MemberBaseZipCode_QNAME, Integer.class, GetIfpQuoteRequestMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestProductChoice }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductChoices", scope = GetIfpQuoteRequestMember.class)
    public JAXBElement<ArrayOfGetIfpQuoteRequestProductChoice> createGetIfpQuoteRequestMemberProductChoices(ArrayOfGetIfpQuoteRequestProductChoice value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestProductChoice>(_IfpShoppingCartProductChoices_QNAME, ArrayOfGetIfpQuoteRequestProductChoice.class, GetIfpQuoteRequestMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IncludeInQuote", scope = GetIfpQuoteRequestMember.class)
    public JAXBElement<Boolean> createGetIfpQuoteRequestMemberIncludeInQuote(Boolean value) {
        return new JAXBElement<Boolean>(_FamilyMemberIncludeInQuote_QNAME, Boolean.class, GetIfpQuoteRequestMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Latitude", scope = ZipCodeInfoBase.class)
    public JAXBElement<Float> createZipCodeInfoBaseLatitude(Float value) {
        return new JAXBElement<Float>(_ZipCodeInfoBaseLatitude_QNAME, Float.class, ZipCodeInfoBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Float }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Longitude", scope = ZipCodeInfoBase.class)
    public JAXBElement<Float> createZipCodeInfoBaseLongitude(Float value) {
        return new JAXBElement<Float>(_ZipCodeInfoBaseLongitude_QNAME, Float.class, ZipCodeInfoBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TimeZone", scope = ZipCodeInfoBase.class)
    public JAXBElement<String> createZipCodeInfoBaseTimeZone(String value) {
        return new JAXBElement<String>(_ZipCodeInfoBaseTimeZone_QNAME, String.class, ZipCodeInfoBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AreaCodes", scope = ZipCodeInfoBase.class)
    public JAXBElement<String> createZipCodeInfoBaseAreaCodes(String value) {
        return new JAXBElement<String>(_ZipCodeInfoBaseAreaCodes_QNAME, String.class, ZipCodeInfoBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedReason", scope = GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan.class)
    public JAXBElement<String> createGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlanDroppedReason(String value) {
        return new JAXBElement<String>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedReason_QNAME, String.class, GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteResponsePlanDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanDetails", scope = GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan.class)
    public JAXBElement<GetIfpQuoteResponsePlanDetail> createGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlanPlanDetails(GetIfpQuoteResponsePlanDetail value) {
        return new JAXBElement<GetIfpQuoteResponsePlanDetail>(_GetGroupQuoteResponseQuoteCarrierRatePlanRatePlanDetails_QNAME, GetIfpQuoteResponsePlanDetail.class, GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseCarrierDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierDetails", scope = GetGroupQuoteResponseQuoteDroppedCarrier.class)
    public JAXBElement<GetGroupQuoteResponseCarrierDetail> createGetGroupQuoteResponseQuoteDroppedCarrierCarrierDetails(GetGroupQuoteResponseCarrierDetail value) {
        return new JAXBElement<GetGroupQuoteResponseCarrierDetail>(_GetIfpQuoteResponseQuoteCarrierRateCarrierDetails_QNAME, GetGroupQuoteResponseCarrierDetail.class, GetGroupQuoteResponseQuoteDroppedCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedPlans", scope = GetGroupQuoteResponseQuoteDroppedCarrier.class)
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan> createGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlans(ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan value) {
        return new JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan>(_GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlans_QNAME, ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan.class, GetGroupQuoteResponseQuoteDroppedCarrier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfListRxResponseDrug }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Drugs", scope = ListRxResponse.class)
    public JAXBElement<ArrayOfListRxResponseDrug> createListRxResponseDrugs(ArrayOfListRxResponseDrug value) {
        return new JAXBElement<ArrayOfListRxResponseDrug>(_GetCarriersPlansBenefitsResponseDrugListDrugs_QNAME, ArrayOfListRxResponseDrug.class, ListRxResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfListRxRequestPlanFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanFilters", scope = ListRxRequestInput.class)
    public JAXBElement<ArrayOfListRxRequestPlanFilter> createListRxRequestInputPlanFilters(ArrayOfListRxRequestPlanFilter value) {
        return new JAXBElement<ArrayOfListRxRequestPlanFilter>(_GetGroupQuoteRequestPreferencePlanFilters_QNAME, ArrayOfListRxRequestPlanFilter.class, ListRxRequestInput.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "StatusHistory", scope = IfpShoppingCartProductChoicePlanChoices.class)
    public JAXBElement<ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange> createIfpShoppingCartProductChoicePlanChoicesStatusHistory(ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange value) {
        return new JAXBElement<ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange>(_IfpShoppingCartProductChoicePlanChoicesStatusHistory_QNAME, ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange.class, IfpShoppingCartProductChoicePlanChoices.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LeadStatus", scope = IfpShoppingCartProductChoicePlanChoices.class)
    public JAXBElement<String> createIfpShoppingCartProductChoicePlanChoicesLeadStatus(String value) {
        return new JAXBElement<String>(_IfpShoppingCartProductChoicePlanChoicesLeadStatus_QNAME, String.class, IfpShoppingCartProductChoicePlanChoices.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugPrescriptionPeriodType", scope = FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVODrugPrescriptionPeriodType(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVODrugPrescriptionPeriodType_QNAME, String.class, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CostSharingType", scope = FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCostSharingType(String value) {
        return new JAXBElement<String>(_CostSharingType_QNAME, String.class, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoInsurance", scope = FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCoInsurance(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCoInsurance_QNAME, String.class, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoPayment", scope = FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCoPayment(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVOCoPayment_QNAME, String.class, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NetworkCostType", scope = FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVONetworkCostType(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVONetworkCostType_QNAME, String.class, FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ChangeCodes", scope = GroupEmployee.class)
    public JAXBElement<String> createGroupEmployeeChangeCodes(String value) {
        return new JAXBElement<String>(_MemberBaseChangeCodes_QNAME, String.class, GroupEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OnCobra", scope = GroupEmployee.class)
    public JAXBElement<Boolean> createGroupEmployeeOnCobra(Boolean value) {
        return new JAXBElement<Boolean>(_GetGroupQuoteRequestGroupRateFactorEmployeeOnCobra_QNAME, Boolean.class, GroupEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PartnerData", scope = GroupEmployee.class)
    public JAXBElement<String> createGroupEmployeePartnerData(String value) {
        return new JAXBElement<String>(_MemberBasePartnerData_QNAME, String.class, GroupEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EmailAddress", scope = GroupEmployee.class)
    public JAXBElement<String> createGroupEmployeeEmailAddress(String value) {
        return new JAXBElement<String>(_GroupEmployeeEmailAddress_QNAME, String.class, GroupEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Salary", scope = GroupEmployee.class)
    public JAXBElement<BigDecimal> createGroupEmployeeSalary(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_GroupEmployeeSalary_QNAME, BigDecimal.class, GroupEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "UserName", scope = GroupEmployee.class)
    public JAXBElement<String> createGroupEmployeeUserName(String value) {
        return new JAXBElement<String>(_GroupEmployeeUserName_QNAME, String.class, GroupEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WorksiteZipCode", scope = GroupEmployee.class)
    public JAXBElement<Integer> createGroupEmployeeWorksiteZipCode(Integer value) {
        return new JAXBElement<Integer>(_GetGroupQuoteRequestGroupRateFactorEmployeeWorksiteZipCode_QNAME, Integer.class, GroupEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "WorksiteCountyName", scope = GroupEmployee.class)
    public JAXBElement<String> createGroupEmployeeWorksiteCountyName(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorEmployeeWorksiteCountyName_QNAME, String.class, GroupEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LifeAmount", scope = GroupEmployee.class)
    public JAXBElement<BigDecimal> createGroupEmployeeLifeAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IfpPlanChoiceBaseLifeAmount_QNAME, BigDecimal.class, GroupEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IncludeInQuote", scope = GroupEmployee.class)
    public JAXBElement<Boolean> createGroupEmployeeIncludeInQuote(Boolean value) {
        return new JAXBElement<Boolean>(_FamilyMemberIncludeInQuote_QNAME, Boolean.class, GroupEmployee.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanId", scope = GetCustomProductsQuoteRequestPreferenceProductFilter.class)
    public JAXBElement<String> createGetCustomProductsQuoteRequestPreferenceProductFilterPlanId(String value) {
        return new JAXBElement<String>(_PlanChoiceBasePlanId_QNAME, String.class, GetCustomProductsQuoteRequestPreferenceProductFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ProductType", scope = GetCustomProductsQuoteRequestPreferenceProductFilter.class)
    public JAXBElement<String> createGetCustomProductsQuoteRequestPreferenceProductFilterProductType(String value) {
        return new JAXBElement<String>(_FetchCustomProductsRequestInputProductFilterProductType_QNAME, String.class, GetCustomProductsQuoteRequestPreferenceProductFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierId", scope = GetCustomProductsQuoteRequestPreferenceProductFilter.class)
    public JAXBElement<String> createGetCustomProductsQuoteRequestPreferenceProductFilterCarrierId(String value) {
        return new JAXBElement<String>(_FetchCustomProductsRequestInputProductFilterCarrierId_QNAME, String.class, GetCustomProductsQuoteRequestPreferenceProductFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponseQuote }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProductsQuote", scope = GetCustomProductsQuoteResponse.class)
    public JAXBElement<GetCustomProductsQuoteResponseQuote> createGetCustomProductsQuoteResponseCustomProductsQuote(GetCustomProductsQuoteResponseQuote value) {
        return new JAXBElement<GetCustomProductsQuoteResponseQuote>(_GetCustomProductsQuoteResponseCustomProductsQuote_QNAME, GetCustomProductsQuoteResponseQuote.class, GetCustomProductsQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CostShareVariances", scope = GetCarriersPlansBenefitsResponseFormularyDrugTier.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance> createGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariances(ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance>(_GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariances_QNAME, ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance.class, GetCarriersPlansBenefitsResponseFormularyDrugTier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseFormularyDrugTierTierType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TierTypes", scope = GetCarriersPlansBenefitsResponseFormularyDrugTier.class)
    public JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTierTierType> createGetCarriersPlansBenefitsResponseFormularyDrugTierTierTypes(GetCarriersPlansBenefitsResponseFormularyDrugTierTierType value) {
        return new JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTierTierType>(_GetCarriersPlansBenefitsResponseFormularyDrugTierTierTypes_QNAME, GetCarriersPlansBenefitsResponseFormularyDrugTierTierType.class, GetCarriersPlansBenefitsResponseFormularyDrugTier.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmissionId", scope = SubmitCustomProductsResponse.class)
    public JAXBElement<Integer> createSubmitCustomProductsResponseSubmissionId(Integer value) {
        return new JAXBElement<Integer>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierSubmissionId_QNAME, Integer.class, SubmitCustomProductsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Cost", scope = ViewPointBase.class)
    public JAXBElement<BigDecimal> createViewPointBaseCost(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ServiceBaseCost_QNAME, BigDecimal.class, ViewPointBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Amount", scope = ViewPointBase.class)
    public JAXBElement<BigDecimal> createViewPointBaseAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ServiceBaseAmount_QNAME, BigDecimal.class, ViewPointBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TinyValue", scope = ViewPointBase.class)
    public JAXBElement<String> createViewPointBaseTinyValue(String value) {
        return new JAXBElement<String>(_BenefitBaseTinyValue_QNAME, String.class, ViewPointBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FlagValue", scope = ViewPointBase.class)
    public JAXBElement<List<String>> createViewPointBaseFlagValue(List<String> value) {
        return new JAXBElement<List<String>>(_FlagValue_QNAME, ((Class) List.class), ViewPointBase.class, ((List<String> ) value));
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LongValue", scope = ViewPointBase.class)
    public JAXBElement<String> createViewPointBaseLongValue(String value) {
        return new JAXBElement<String>(_ViewPointBaseLongValue_QNAME, String.class, ViewPointBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ShortValue", scope = ViewPointBase.class)
    public JAXBElement<String> createViewPointBaseShortValue(String value) {
        return new JAXBElement<String>(_ViewPointBaseShortValue_QNAME, String.class, ViewPointBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ViewPointType", scope = GetIfpQuoteRequestPreferenceBenefitFilter.class)
    public JAXBElement<String> createGetIfpQuoteRequestPreferenceBenefitFilterViewPointType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitFilterViewPointType_QNAME, String.class, GetIfpQuoteRequestPreferenceBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CoverageType", scope = GetIfpQuoteRequestPreferenceBenefitFilter.class)
    public JAXBElement<String> createGetIfpQuoteRequestPreferenceBenefitFilterCoverageType(String value) {
        return new JAXBElement<String>(_CoverageType_QNAME, String.class, GetIfpQuoteRequestPreferenceBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitEnum", scope = GetIfpQuoteRequestPreferenceBenefitFilter.class)
    public JAXBElement<String> createGetIfpQuoteRequestPreferenceBenefitFilterBenefitEnum(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitFilterBenefitEnum_QNAME, String.class, GetIfpQuoteRequestPreferenceBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AttributeFilters", scope = GetIfpQuoteRequestPreferenceBenefitFilter.class)
    public JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter> createGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilters(ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter value) {
        return new JAXBElement<ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter>(_GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilters_QNAME, ArrayOfGetIfpQuoteRequestPreferenceBenefitFilterAttributeFilter.class, GetIfpQuoteRequestPreferenceBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ServiceType", scope = GetIfpQuoteRequestPreferenceBenefitFilter.class)
    public JAXBElement<String> createGetIfpQuoteRequestPreferenceBenefitFilterServiceType(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestPreferenceBenefitFilterServiceType_QNAME, String.class, GetIfpQuoteRequestPreferenceBenefitFilter.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitData", scope = GetCarriersPlansBenefitsResponseCarrierPlanBenefit.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> createGetCarriersPlansBenefitsResponseCarrierPlanBenefitBenefitData(ArrayOfGetCarriersPlansBenefitsResponseItem value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem>(_GetGroupQuoteResponsePlanDetailBenefitBenefitData_QNAME, ArrayOfGetCarriersPlansBenefitsResponseItem.class, GetCarriersPlansBenefitsResponseCarrierPlanBenefit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Coverages", scope = GetCarriersPlansBenefitsResponseCarrierPlanBenefit.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage> createGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverages(ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage>(_GetGroupQuoteResponsePlanDetailBenefitCoverages_QNAME, ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage.class, GetCarriersPlansBenefitsResponseCarrierPlanBenefit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanData", scope = GetPlansbyRxResponseCarrierPlan.class)
    public JAXBElement<ArrayOfGetPlansbyRxItem> createGetPlansbyRxResponseCarrierPlanPlanData(ArrayOfGetPlansbyRxItem value) {
        return new JAXBElement<ArrayOfGetPlansbyRxItem>(_GetIfpQuoteResponsePlanDetailPlanData_QNAME, ArrayOfGetPlansbyRxItem.class, GetPlansbyRxResponseCarrierPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxResponseCarrierPlanPlanFormulary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Formulary", scope = GetPlansbyRxResponseCarrierPlan.class)
    public JAXBElement<GetPlansbyRxResponseCarrierPlanPlanFormulary> createGetPlansbyRxResponseCarrierPlanFormulary(GetPlansbyRxResponseCarrierPlanPlanFormulary value) {
        return new JAXBElement<GetPlansbyRxResponseCarrierPlanPlanFormulary>(_GetPlansbyRxResponseCarrierPlanFormulary_QNAME, GetPlansbyRxResponseCarrierPlanPlanFormulary.class, GetPlansbyRxResponseCarrierPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Benefits", scope = GetPlansbyRxResponseCarrierPlan.class)
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefit> createGetPlansbyRxResponseCarrierPlanBenefits(ArrayOfGetPlansbyRxResponseCarrierPlanBenefit value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefit>(_GetIfpQuoteResponsePlanDetailBenefits_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanBenefit.class, GetPlansbyRxResponseCarrierPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DrugTiers", scope = GetCarriersPlansBenefitsResponseFormulary.class)
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier> createGetCarriersPlansBenefitsResponseFormularyDrugTiers(ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier value) {
        return new JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier>(_GetCarriersPlansBenefitsResponseFormularyDrugTiers_QNAME, ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier.class, GetCarriersPlansBenefitsResponseFormulary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyURL", scope = GetCarriersPlansBenefitsResponseFormulary.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsResponseFormularyFormularyURL(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyURL_QNAME, String.class, GetCarriersPlansBenefitsResponseFormulary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NumberOfTiers", scope = GetCarriersPlansBenefitsResponseFormulary.class)
    public JAXBElement<String> createGetCarriersPlansBenefitsResponseFormularyNumberOfTiers(String value) {
        return new JAXBElement<String>(_GetCarriersPlansBenefitsResponseFormularyNumberOfTiers_QNAME, String.class, GetCarriersPlansBenefitsResponseFormulary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitData", scope = GetIfpQuoteResponsePlanDetailBenefit.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseItem> createGetIfpQuoteResponsePlanDetailBenefitBenefitData(ArrayOfGetIfpQuoteResponseItem value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseItem>(_GetGroupQuoteResponsePlanDetailBenefitBenefitData_QNAME, ArrayOfGetIfpQuoteResponseItem.class, GetIfpQuoteResponsePlanDetailBenefit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Coverages", scope = GetIfpQuoteResponsePlanDetailBenefit.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage> createGetIfpQuoteResponsePlanDetailBenefitCoverages(ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage>(_GetGroupQuoteResponsePlanDetailBenefitCoverages_QNAME, ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverage.class, GetIfpQuoteResponsePlanDetailBenefit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmissionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmissionType", scope = FetchCustomProductsResponse.class)
    public JAXBElement<SubmissionType> createFetchCustomProductsResponseSubmissionType(SubmissionType value) {
        return new JAXBElement<SubmissionType>(_SubmissionType_QNAME, SubmissionType.class, FetchCustomProductsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Remarks", scope = FetchCustomProductsResponse.class)
    public JAXBElement<String> createFetchCustomProductsResponseRemarks(String value) {
        return new JAXBElement<String>(_FetchCustomProductsResponseRemarks_QNAME, String.class, FetchCustomProductsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubmissionId", scope = FetchCustomProductsResponse.class)
    public JAXBElement<Integer> createFetchCustomProductsResponseSubmissionId(Integer value) {
        return new JAXBElement<Integer>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierSubmissionId_QNAME, Integer.class, FetchCustomProductsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfCustomProduct }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CustomProducts", scope = FetchCustomProductsResponse.class)
    public JAXBElement<ArrayOfCustomProduct> createFetchCustomProductsResponseCustomProducts(ArrayOfCustomProduct value) {
        return new JAXBElement<ArrayOfCustomProduct>(_SubmitCustomProductsRequestCustomProducts_QNAME, ArrayOfCustomProduct.class, FetchCustomProductsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CartId", scope = GroupShoppingCart.class)
    public JAXBElement<String> createGroupShoppingCartCartId(String value) {
        return new JAXBElement<String>(_GetGroupRequestCartId_QNAME, String.class, GroupShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CreatedDate", scope = GroupShoppingCart.class)
    public JAXBElement<XMLGregorianCalendar> createGroupShoppingCartCreatedDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_IfpShoppingCartCreatedDate_QNAME, XMLGregorianCalendar.class, GroupShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ApplicationId", scope = GroupShoppingCart.class)
    public JAXBElement<String> createGroupShoppingCartApplicationId(String value) {
        return new JAXBElement<String>(_IfpShoppingCartApplicationId_QNAME, String.class, GroupShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseItem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CarrierData", scope = GetIfpQuoteResponseCarrierDetail.class)
    public JAXBElement<ArrayOfGetIfpQuoteResponseItem> createGetIfpQuoteResponseCarrierDetailCarrierData(ArrayOfGetIfpQuoteResponseItem value) {
        return new JAXBElement<ArrayOfGetIfpQuoteResponseItem>(_GetCarriersPlansBenefitsResponseCarrierCarrierData_QNAME, ArrayOfGetIfpQuoteResponseItem.class, GetIfpQuoteResponseCarrierDetail.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "MembershipDate", scope = GetCustomProductsQuoteRequestCustomRateFactorFamilyMember.class)
    public JAXBElement<XMLGregorianCalendar> createGetCustomProductsQuoteRequestCustomRateFactorFamilyMemberMembershipDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MemberBaseMembershipDate_QNAME, XMLGregorianCalendar.class, GetCustomProductsQuoteRequestCustomRateFactorFamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DateLastSmoked", scope = GetCustomProductsQuoteRequestCustomRateFactorFamilyMember.class)
    public JAXBElement<XMLGregorianCalendar> createGetCustomProductsQuoteRequestCustomRateFactorFamilyMemberDateLastSmoked(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_MemberBaseDateLastSmoked_QNAME, XMLGregorianCalendar.class, GetCustomProductsQuoteRequestCustomRateFactorFamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CountyFIPS", scope = GetCustomProductsQuoteRequestCustomRateFactorFamilyMember.class)
    public JAXBElement<String> createGetCustomProductsQuoteRequestCustomRateFactorFamilyMemberCountyFIPS(String value) {
        return new JAXBElement<String>(_CustomProductRatingAreaZipCountyCountyFIPS_QNAME, String.class, GetCustomProductsQuoteRequestCustomRateFactorFamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IsSmoker", scope = GetCustomProductsQuoteRequestCustomRateFactorFamilyMember.class)
    public JAXBElement<Boolean> createGetCustomProductsQuoteRequestCustomRateFactorFamilyMemberIsSmoker(Boolean value) {
        return new JAXBElement<Boolean>(_MemberBaseIsSmoker_QNAME, Boolean.class, GetCustomProductsQuoteRequestCustomRateFactorFamilyMember.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssuerId", scope = DroppedCarrierBase.class)
    public JAXBElement<String> createDroppedCarrierBaseIssuerId(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierIssuerId_QNAME, String.class, DroppedCarrierBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DroppedReason", scope = DroppedCarrierBase.class)
    public JAXBElement<String> createDroppedCarrierBaseDroppedReason(String value) {
        return new JAXBElement<String>(_GetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrierDroppedReason_QNAME, String.class, DroppedCarrierBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupResponseCarrierPlanPlanFormulary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Formulary", scope = FormularyLookupResponseCarrierPlan.class)
    public JAXBElement<FormularyLookupResponseCarrierPlanPlanFormulary> createFormularyLookupResponseCarrierPlanFormulary(FormularyLookupResponseCarrierPlanPlanFormulary value) {
        return new JAXBElement<FormularyLookupResponseCarrierPlanPlanFormulary>(_GetPlansbyRxResponseCarrierPlanFormulary_QNAME, FormularyLookupResponseCarrierPlanPlanFormulary.class, FormularyLookupResponseCarrierPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitLevel", scope = FormularyLookupResponseCarrierPlan.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanBenefitLevel(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel_QNAME, String.class, FormularyLookupResponseCarrierPlan.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IssuerId", scope = CarrierRateBase.class)
    public JAXBElement<String> createCarrierRateBaseIssuerId(String value) {
        return new JAXBElement<String>(_FormularyLookupResponseCarrierIssuerId_QNAME, String.class, CarrierRateBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BenefitLevel", scope = PlanComponentBase.class)
    public JAXBElement<String> createPlanComponentBaseBenefitLevel(String value) {
        return new JAXBElement<String>(_GetGroupQuoteRequestGroupRateFactorScenarioBenefitLevel_QNAME, String.class, PlanComponentBase.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Services", scope = GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint.class)
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService> createGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointServices(ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService value) {
        return new JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService>(_GetIfpQuoteResponsePlanDetailBenefitCoverageViewPointServices_QNAME, ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService.class, GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EffectiveDate", scope = GetIfpQuoteRequestProductChoice.class)
    public JAXBElement<XMLGregorianCalendar> createGetIfpQuoteRequestProductChoiceEffectiveDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoiceEffectiveDate_QNAME, XMLGregorianCalendar.class, GetIfpQuoteRequestProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequestProductChoicePlanChoices }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "PlanChoice", scope = GetIfpQuoteRequestProductChoice.class)
    public JAXBElement<GetIfpQuoteRequestProductChoicePlanChoices> createGetIfpQuoteRequestProductChoicePlanChoice(GetIfpQuoteRequestProductChoicePlanChoices value) {
        return new JAXBElement<GetIfpQuoteRequestProductChoicePlanChoices>(_GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoice_QNAME, GetIfpQuoteRequestProductChoicePlanChoices.class, GetIfpQuoteRequestProductChoice.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyURL", scope = FormularyLookupResponseCarrierPlanPlanFormulary.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyURL(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyURL_QNAME, String.class, FormularyLookupResponseCarrierPlanPlanFormulary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "NumberTiers", scope = FormularyLookupResponseCarrierPlanPlanFormulary.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanPlanFormularyNumberTiers(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyNumberTiers_QNAME, String.class, FormularyLookupResponseCarrierPlanPlanFormulary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyID", scope = FormularyLookupResponseCarrierPlanPlanFormulary.class)
    public JAXBElement<String> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyID(String value) {
        return new JAXBElement<String>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyID_QNAME, String.class, FormularyLookupResponseCarrierPlanPlanFormulary.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FormularyTiers", scope = FormularyLookupResponseCarrierPlanPlanFormulary.class)
    public JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier> createFormularyLookupResponseCarrierPlanPlanFormularyFormularyTiers(ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier value) {
        return new JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier>(_GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTiers_QNAME, ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTier.class, FormularyLookupResponseCarrierPlanPlanFormulary.class, value);
    }

}
