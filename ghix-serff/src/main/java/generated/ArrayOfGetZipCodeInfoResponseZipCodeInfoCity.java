
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.City complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.City">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetZipCodeInfo.Response.ZipCodeInfo.City" type="{}GetZipCodeInfo.Response.ZipCodeInfo.City" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.City", propOrder = {
    "getZipCodeInfoResponseZipCodeInfoCity"
})
public class ArrayOfGetZipCodeInfoResponseZipCodeInfoCity {

    @XmlElement(name = "GetZipCodeInfo.Response.ZipCodeInfo.City", nillable = true)
    protected List<GetZipCodeInfoResponseZipCodeInfoCity> getZipCodeInfoResponseZipCodeInfoCity;

    /**
     * Gets the value of the getZipCodeInfoResponseZipCodeInfoCity property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getZipCodeInfoResponseZipCodeInfoCity property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetZipCodeInfoResponseZipCodeInfoCity().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetZipCodeInfoResponseZipCodeInfoCity }
     * 
     * 
     */
    public List<GetZipCodeInfoResponseZipCodeInfoCity> getGetZipCodeInfoResponseZipCodeInfoCity() {
        if (getZipCodeInfoResponseZipCodeInfoCity == null) {
            getZipCodeInfoResponseZipCodeInfoCity = new ArrayList<GetZipCodeInfoResponseZipCodeInfoCity>();
        }
        return this.getZipCodeInfoResponseZipCodeInfoCity;
    }

}
