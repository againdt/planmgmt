
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGroup.Scenario complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGroup.Scenario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Group.Scenario" type="{}Group.Scenario" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGroup.Scenario", propOrder = {
    "groupScenario"
})
public class ArrayOfGroupScenario {

    @XmlElement(name = "Group.Scenario", nillable = true)
    protected List<GroupScenario> groupScenario;

    /**
     * Gets the value of the groupScenario property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupScenario property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupScenario().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupScenario }
     * 
     * 
     */
    public List<GroupScenario> getGroupScenario() {
        if (groupScenario == null) {
            groupScenario = new ArrayList<GroupScenario>();
        }
        return this.groupScenario;
    }

}
