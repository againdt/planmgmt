
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Request.Preference.PlanFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Request.Preference.PlanFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Request.Preference.PlanFilter" type="{}GetGroupQuote.Request.Preference.PlanFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Request.Preference.PlanFilter", propOrder = {
    "getGroupQuoteRequestPreferencePlanFilter"
})
public class ArrayOfGetGroupQuoteRequestPreferencePlanFilter {

    @XmlElement(name = "GetGroupQuote.Request.Preference.PlanFilter", nillable = true)
    protected List<GetGroupQuoteRequestPreferencePlanFilter> getGroupQuoteRequestPreferencePlanFilter;

    /**
     * Gets the value of the getGroupQuoteRequestPreferencePlanFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteRequestPreferencePlanFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteRequestPreferencePlanFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteRequestPreferencePlanFilter }
     * 
     * 
     */
    public List<GetGroupQuoteRequestPreferencePlanFilter> getGetGroupQuoteRequestPreferencePlanFilter() {
        if (getGroupQuoteRequestPreferencePlanFilter == null) {
            getGroupQuoteRequestPreferencePlanFilter = new ArrayList<GetGroupQuoteRequestPreferencePlanFilter>();
        }
        return this.getGroupQuoteRequestPreferencePlanFilter;
    }

}
