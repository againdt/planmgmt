
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarrierLoadHistory.Request.AccessKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarrierLoadHistory.Request.AccessKey">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemoteAccessKey" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid"/>
 *         &lt;element name="WebsiteAccessKey" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid"/>
 *         &lt;element name="BrokerId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarrierLoadHistory.Request.AccessKey", propOrder = {
    "remoteAccessKey",
    "websiteAccessKey",
    "brokerId"
})
public class GetCarrierLoadHistoryRequestAccessKey {

    @XmlElement(name = "RemoteAccessKey", required = true)
    protected String remoteAccessKey;
    @XmlElement(name = "WebsiteAccessKey", required = true)
    protected String websiteAccessKey;
    @XmlElement(name = "BrokerId")
    protected int brokerId;

    /**
     * Gets the value of the remoteAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteAccessKey() {
        return remoteAccessKey;
    }

    /**
     * Sets the value of the remoteAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteAccessKey(String value) {
        this.remoteAccessKey = value;
    }

    /**
     * Gets the value of the websiteAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebsiteAccessKey() {
        return websiteAccessKey;
    }

    /**
     * Sets the value of the websiteAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebsiteAccessKey(String value) {
        this.websiteAccessKey = value;
    }

    /**
     * Gets the value of the brokerId property.
     * 
     */
    public int getBrokerId() {
        return brokerId;
    }

    /**
     * Sets the value of the brokerId property.
     * 
     */
    public void setBrokerId(int value) {
        this.brokerId = value;
    }

}
