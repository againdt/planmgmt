
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGroup.Employee.Member complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGroup.Employee.Member">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Group.Employee.Member" type="{}Group.Employee.Member" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGroup.Employee.Member", propOrder = {
    "groupEmployeeMember"
})
public class ArrayOfGroupEmployeeMember {

    @XmlElement(name = "Group.Employee.Member", nillable = true)
    protected List<GroupEmployeeMember> groupEmployeeMember;

    /**
     * Gets the value of the groupEmployeeMember property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupEmployeeMember property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupEmployeeMember().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupEmployeeMember }
     * 
     * 
     */
    public List<GroupEmployeeMember> getGroupEmployeeMember() {
        if (groupEmployeeMember == null) {
            groupEmployeeMember = new ArrayList<GroupEmployeeMember>();
        }
        return this.groupEmployeeMember;
    }

}
