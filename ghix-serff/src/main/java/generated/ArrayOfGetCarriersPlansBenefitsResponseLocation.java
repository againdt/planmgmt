
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.Location complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.Location">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.Location" type="{}GetCarriersPlansBenefits.Response.Location" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.Location", propOrder = {
    "getCarriersPlansBenefitsResponseLocation"
})
public class ArrayOfGetCarriersPlansBenefitsResponseLocation {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.Location", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseLocation> getCarriersPlansBenefitsResponseLocation;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseLocation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseLocation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseLocation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseLocation }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseLocation> getGetCarriersPlansBenefitsResponseLocation() {
        if (getCarriersPlansBenefitsResponseLocation == null) {
            getCarriersPlansBenefitsResponseLocation = new ArrayList<GetCarriersPlansBenefitsResponseLocation>();
        }
        return this.getCarriersPlansBenefitsResponseLocation;
    }

}
