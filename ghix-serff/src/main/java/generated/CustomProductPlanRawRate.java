
package generated;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomProduct.Plan.RawRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomProduct.Plan.RawRate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RatingAreaId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MinAge" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="MaxAge" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Gender" type="{}RatesGender"/>
 *         &lt;element name="CoverageTier" type="{}CoverageTier"/>
 *         &lt;element name="Rate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="SmokerRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomProduct.Plan.RawRate", propOrder = {
    "ratingAreaId",
    "minAge",
    "maxAge",
    "gender",
    "coverageTier",
    "rate",
    "smokerRate"
})
public class CustomProductPlanRawRate {

    @XmlElement(name = "RatingAreaId")
    protected int ratingAreaId;
    @XmlElement(name = "MinAge", required = true)
    protected BigDecimal minAge;
    @XmlElement(name = "MaxAge", required = true)
    protected BigDecimal maxAge;
    @XmlElement(name = "Gender", required = true)
    protected RatesGender gender;
    @XmlElement(name = "CoverageTier", required = true)
    protected CoverageTier coverageTier;
    @XmlElement(name = "Rate", required = true)
    protected BigDecimal rate;
    @XmlElement(name = "SmokerRate", required = true)
    protected BigDecimal smokerRate;

    /**
     * Gets the value of the ratingAreaId property.
     * 
     */
    public int getRatingAreaId() {
        return ratingAreaId;
    }

    /**
     * Sets the value of the ratingAreaId property.
     * 
     */
    public void setRatingAreaId(int value) {
        this.ratingAreaId = value;
    }

    /**
     * Gets the value of the minAge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinAge() {
        return minAge;
    }

    /**
     * Sets the value of the minAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinAge(BigDecimal value) {
        this.minAge = value;
    }

    /**
     * Gets the value of the maxAge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMaxAge() {
        return maxAge;
    }

    /**
     * Sets the value of the maxAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMaxAge(BigDecimal value) {
        this.maxAge = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link RatesGender }
     *     
     */
    public RatesGender getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link RatesGender }
     *     
     */
    public void setGender(RatesGender value) {
        this.gender = value;
    }

    /**
     * Gets the value of the coverageTier property.
     * 
     * @return
     *     possible object is
     *     {@link CoverageTier }
     *     
     */
    public CoverageTier getCoverageTier() {
        return coverageTier;
    }

    /**
     * Sets the value of the coverageTier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoverageTier }
     *     
     */
    public void setCoverageTier(CoverageTier value) {
        this.coverageTier = value;
    }

    /**
     * Gets the value of the rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * Gets the value of the smokerRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSmokerRate() {
        return smokerRate;
    }

    /**
     * Sets the value of the smokerRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSmokerRate(BigDecimal value) {
        this.smokerRate = value;
    }

}
