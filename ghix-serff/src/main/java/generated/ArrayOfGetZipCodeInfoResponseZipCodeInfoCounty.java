
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.County complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.County">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetZipCodeInfo.Response.ZipCodeInfo.County" type="{}GetZipCodeInfo.Response.ZipCodeInfo.County" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.County", propOrder = {
    "getZipCodeInfoResponseZipCodeInfoCounty"
})
public class ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty {

    @XmlElement(name = "GetZipCodeInfo.Response.ZipCodeInfo.County", nillable = true)
    protected List<GetZipCodeInfoResponseZipCodeInfoCounty> getZipCodeInfoResponseZipCodeInfoCounty;

    /**
     * Gets the value of the getZipCodeInfoResponseZipCodeInfoCounty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getZipCodeInfoResponseZipCodeInfoCounty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetZipCodeInfoResponseZipCodeInfoCounty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetZipCodeInfoResponseZipCodeInfoCounty }
     * 
     * 
     */
    public List<GetZipCodeInfoResponseZipCodeInfoCounty> getGetZipCodeInfoResponseZipCodeInfoCounty() {
        if (getZipCodeInfoResponseZipCodeInfoCounty == null) {
            getZipCodeInfoResponseZipCodeInfoCounty = new ArrayList<GetZipCodeInfoResponseZipCodeInfoCounty>();
        }
        return this.getZipCodeInfoResponseZipCodeInfoCounty;
    }

}
