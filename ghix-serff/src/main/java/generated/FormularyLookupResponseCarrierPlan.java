
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FormularyLookup.Response.Carrier.Plan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FormularyLookup.Response.Carrier.Plan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlanId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PlanType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InsuranceType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BenefitLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Formulary" type="{}FormularyLookup.Response.Carrier.Plan.PlanFormulary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FormularyLookup.Response.Carrier.Plan", propOrder = {
    "planId",
    "planType",
    "name",
    "state",
    "insuranceType",
    "benefitLevel",
    "formulary"
})
public class FormularyLookupResponseCarrierPlan {

    @XmlElement(name = "PlanId", required = true, nillable = true)
    protected String planId;
    @XmlElement(name = "PlanType", required = true, nillable = true)
    protected String planType;
    @XmlElement(name = "Name", required = true, nillable = true)
    protected String name;
    @XmlElement(name = "State", required = true, nillable = true)
    protected String state;
    @XmlElement(name = "InsuranceType", required = true, nillable = true)
    protected String insuranceType;
    @XmlElementRef(name = "BenefitLevel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> benefitLevel;
    @XmlElementRef(name = "Formulary", type = JAXBElement.class, required = false)
    protected JAXBElement<FormularyLookupResponseCarrierPlanPlanFormulary> formulary;

    /**
     * Gets the value of the planId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanId() {
        return planId;
    }

    /**
     * Sets the value of the planId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanId(String value) {
        this.planId = value;
    }

    /**
     * Gets the value of the planType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanType() {
        return planType;
    }

    /**
     * Sets the value of the planType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanType(String value) {
        this.planType = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the insuranceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceType() {
        return insuranceType;
    }

    /**
     * Sets the value of the insuranceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceType(String value) {
        this.insuranceType = value;
    }

    /**
     * Gets the value of the benefitLevel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBenefitLevel() {
        return benefitLevel;
    }

    /**
     * Sets the value of the benefitLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBenefitLevel(JAXBElement<String> value) {
        this.benefitLevel = value;
    }

    /**
     * Gets the value of the formulary property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FormularyLookupResponseCarrierPlanPlanFormulary }{@code >}
     *     
     */
    public JAXBElement<FormularyLookupResponseCarrierPlanPlanFormulary> getFormulary() {
        return formulary;
    }

    /**
     * Sets the value of the formulary property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FormularyLookupResponseCarrierPlanPlanFormulary }{@code >}
     *     
     */
    public void setFormulary(JAXBElement<FormularyLookupResponseCarrierPlanPlanFormulary> value) {
        this.formulary = value;
    }

}
