
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCustomProductsQuote.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomProductsQuote.Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemoteAccessKey" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid"/>
 *         &lt;element name="WebsiteAccessKey" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid"/>
 *         &lt;element name="BrokerId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CustomRateFactors" type="{}GetCustomProductsQuote.Request.CustomRateFactor"/>
 *         &lt;element name="Preferences" type="{}GetCustomProductsQuote.Request.Preference"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomProductsQuote.Request", propOrder = {
    "remoteAccessKey",
    "websiteAccessKey",
    "brokerId",
    "customRateFactors",
    "preferences"
})
public class GetCustomProductsQuoteRequest {

    @XmlElement(name = "RemoteAccessKey", required = true)
    protected String remoteAccessKey;
    @XmlElement(name = "WebsiteAccessKey", required = true)
    protected String websiteAccessKey;
    @XmlElement(name = "BrokerId")
    protected int brokerId;
    @XmlElement(name = "CustomRateFactors", required = true, nillable = true)
    protected GetCustomProductsQuoteRequestCustomRateFactor customRateFactors;
    @XmlElement(name = "Preferences", required = true, nillable = true)
    protected GetCustomProductsQuoteRequestPreference preferences;

    /**
     * Gets the value of the remoteAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteAccessKey() {
        return remoteAccessKey;
    }

    /**
     * Sets the value of the remoteAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteAccessKey(String value) {
        this.remoteAccessKey = value;
    }

    /**
     * Gets the value of the websiteAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebsiteAccessKey() {
        return websiteAccessKey;
    }

    /**
     * Sets the value of the websiteAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebsiteAccessKey(String value) {
        this.websiteAccessKey = value;
    }

    /**
     * Gets the value of the brokerId property.
     * 
     */
    public int getBrokerId() {
        return brokerId;
    }

    /**
     * Sets the value of the brokerId property.
     * 
     */
    public void setBrokerId(int value) {
        this.brokerId = value;
    }

    /**
     * Gets the value of the customRateFactors property.
     * 
     * @return
     *     possible object is
     *     {@link GetCustomProductsQuoteRequestCustomRateFactor }
     *     
     */
    public GetCustomProductsQuoteRequestCustomRateFactor getCustomRateFactors() {
        return customRateFactors;
    }

    /**
     * Sets the value of the customRateFactors property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCustomProductsQuoteRequestCustomRateFactor }
     *     
     */
    public void setCustomRateFactors(GetCustomProductsQuoteRequestCustomRateFactor value) {
        this.customRateFactors = value;
    }

    /**
     * Gets the value of the preferences property.
     * 
     * @return
     *     possible object is
     *     {@link GetCustomProductsQuoteRequestPreference }
     *     
     */
    public GetCustomProductsQuoteRequestPreference getPreferences() {
        return preferences;
    }

    /**
     * Sets the value of the preferences property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCustomProductsQuoteRequestPreference }
     *     
     */
    public void setPreferences(GetCustomProductsQuoteRequestPreference value) {
        this.preferences = value;
    }

}
