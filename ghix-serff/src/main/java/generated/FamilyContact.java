
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Family.Contact complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Family.Contact">
 *   &lt;complexContent>
 *     &lt;extension base="{}ContactBase">
 *       &lt;sequence>
 *         &lt;element name="BestTimeToCall" type="{}BestTimeToCall" minOccurs="0"/>
 *         &lt;element name="ContactAttributes" type="{}ArrayOfFamily.Contact.ContactAttribute" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Family.Contact", propOrder = {
    "bestTimeToCall",
    "contactAttributes"
})
public class FamilyContact
    extends ContactBase
{

    @XmlElementRef(name = "BestTimeToCall", type = JAXBElement.class, required = false)
    protected JAXBElement<BestTimeToCall> bestTimeToCall;
    @XmlElementRef(name = "ContactAttributes", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfFamilyContactContactAttribute> contactAttributes;

    /**
     * Gets the value of the bestTimeToCall property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BestTimeToCall }{@code >}
     *     
     */
    public JAXBElement<BestTimeToCall> getBestTimeToCall() {
        return bestTimeToCall;
    }

    /**
     * Sets the value of the bestTimeToCall property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BestTimeToCall }{@code >}
     *     
     */
    public void setBestTimeToCall(JAXBElement<BestTimeToCall> value) {
        this.bestTimeToCall = value;
    }

    /**
     * Gets the value of the contactAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFamilyContactContactAttribute }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFamilyContactContactAttribute> getContactAttributes() {
        return contactAttributes;
    }

    /**
     * Sets the value of the contactAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFamilyContactContactAttribute }{@code >}
     *     
     */
    public void setContactAttributes(JAXBElement<ArrayOfFamilyContactContactAttribute> value) {
        this.contactAttributes = value;
    }

}
