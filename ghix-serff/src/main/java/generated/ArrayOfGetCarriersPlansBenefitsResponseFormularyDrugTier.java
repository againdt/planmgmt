
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.Formulary.DrugTier" type="{}GetCarriersPlansBenefits.Response.Formulary.DrugTier" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier", propOrder = {
    "getCarriersPlansBenefitsResponseFormularyDrugTier"
})
public class ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.Formulary.DrugTier", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseFormularyDrugTier> getCarriersPlansBenefitsResponseFormularyDrugTier;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseFormularyDrugTier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseFormularyDrugTier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseFormularyDrugTier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseFormularyDrugTier }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseFormularyDrugTier> getGetCarriersPlansBenefitsResponseFormularyDrugTier() {
        if (getCarriersPlansBenefitsResponseFormularyDrugTier == null) {
            getCarriersPlansBenefitsResponseFormularyDrugTier = new ArrayList<GetCarriersPlansBenefitsResponseFormularyDrugTier>();
        }
        return this.getCarriersPlansBenefitsResponseFormularyDrugTier;
    }

}
