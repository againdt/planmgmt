
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmitFamily.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitFamily.Request">
 *   &lt;complexContent>
 *     &lt;extension base="{}QuoteRequestBase">
 *       &lt;sequence>
 *         &lt;element name="Family" type="{}Family"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitFamily.Request", propOrder = {
    "family"
})
public class SubmitFamilyRequest
    extends QuoteRequestBase
{

    @XmlElement(name = "Family", required = true, nillable = true)
    protected Family family;

    /**
     * Gets the value of the family property.
     * 
     * @return
     *     possible object is
     *     {@link Family }
     *     
     */
    public Family getFamily() {
        return family;
    }

    /**
     * Sets the value of the family property.
     * 
     * @param value
     *     allowed object is
     *     {@link Family }
     *     
     */
    public void setFamily(Family value) {
        this.family = value;
    }

}
