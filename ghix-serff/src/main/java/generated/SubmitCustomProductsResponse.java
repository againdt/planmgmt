
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmitCustomProducts.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitCustomProducts.Response">
 *   &lt;complexContent>
 *     &lt;extension base="{}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="SubmissionStatus" type="{}SubmissionStatus"/>
 *         &lt;element name="SubmissionId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Errors" type="{}ArrayOfError"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitCustomProducts.Response", propOrder = {
    "submissionStatus",
    "submissionId",
    "errors"
})
public class SubmitCustomProductsResponse
    extends ResponseBase
{

    @XmlElement(name = "SubmissionStatus", required = true)
    protected SubmissionStatus submissionStatus;
    @XmlElementRef(name = "SubmissionId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> submissionId;
    @XmlElement(name = "Errors", required = true, nillable = true)
    protected ArrayOfError errors;

    /**
     * Gets the value of the submissionStatus property.
     * 
     * @return
     *     possible object is
     *     {@link SubmissionStatus }
     *     
     */
    public SubmissionStatus getSubmissionStatus() {
        return submissionStatus;
    }

    /**
     * Sets the value of the submissionStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmissionStatus }
     *     
     */
    public void setSubmissionStatus(SubmissionStatus value) {
        this.submissionStatus = value;
    }

    /**
     * Gets the value of the submissionId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSubmissionId() {
        return submissionId;
    }

    /**
     * Sets the value of the submissionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSubmissionId(JAXBElement<Integer> value) {
        this.submissionId = value;
    }

    /**
     * Gets the value of the errors property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfError }
     *     
     */
    public ArrayOfError getErrors() {
        return errors;
    }

    /**
     * Sets the value of the errors property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfError }
     *     
     */
    public void setErrors(ArrayOfError value) {
        this.errors = value;
    }

}
