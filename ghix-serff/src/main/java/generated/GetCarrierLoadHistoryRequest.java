
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarrierLoadHistory.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarrierLoadHistory.Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccessKeys" type="{}GetCarrierLoadHistory.Request.AccessKey"/>
 *         &lt;element name="Inputs" type="{}GetCarrierLoadHistory.Request.Input"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarrierLoadHistory.Request", propOrder = {
    "accessKeys",
    "inputs"
})
public class GetCarrierLoadHistoryRequest {

    @XmlElement(name = "AccessKeys", required = true, nillable = true)
    protected GetCarrierLoadHistoryRequestAccessKey accessKeys;
    @XmlElement(name = "Inputs", required = true, nillable = true)
    protected GetCarrierLoadHistoryRequestInput inputs;

    /**
     * Gets the value of the accessKeys property.
     * 
     * @return
     *     possible object is
     *     {@link GetCarrierLoadHistoryRequestAccessKey }
     *     
     */
    public GetCarrierLoadHistoryRequestAccessKey getAccessKeys() {
        return accessKeys;
    }

    /**
     * Sets the value of the accessKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCarrierLoadHistoryRequestAccessKey }
     *     
     */
    public void setAccessKeys(GetCarrierLoadHistoryRequestAccessKey value) {
        this.accessKeys = value;
    }

    /**
     * Gets the value of the inputs property.
     * 
     * @return
     *     possible object is
     *     {@link GetCarrierLoadHistoryRequestInput }
     *     
     */
    public GetCarrierLoadHistoryRequestInput getInputs() {
        return inputs;
    }

    /**
     * Sets the value of the inputs property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCarrierLoadHistoryRequestInput }
     *     
     */
    public void setInputs(GetCarrierLoadHistoryRequestInput value) {
        this.inputs = value;
    }

}
