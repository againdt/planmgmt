
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomProduct.Plan.LookupSequence complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomProduct.Plan.LookupSequence">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RatingAreaBase" type="{}RatingAreaBase"/>
 *         &lt;element name="InAreaAction" type="{}InAreaAction"/>
 *         &lt;element name="OutOfAreaAction" type="{}OutOfAreaAction"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomProduct.Plan.LookupSequence", propOrder = {
    "ratingAreaBase",
    "inAreaAction",
    "outOfAreaAction"
})
public class CustomProductPlanLookupSequence {

    @XmlElement(name = "RatingAreaBase", required = true)
    protected RatingAreaBase ratingAreaBase;
    @XmlElement(name = "InAreaAction", required = true)
    protected InAreaAction inAreaAction;
    @XmlElement(name = "OutOfAreaAction", required = true)
    protected OutOfAreaAction outOfAreaAction;

    /**
     * Gets the value of the ratingAreaBase property.
     * 
     * @return
     *     possible object is
     *     {@link RatingAreaBase }
     *     
     */
    public RatingAreaBase getRatingAreaBase() {
        return ratingAreaBase;
    }

    /**
     * Sets the value of the ratingAreaBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link RatingAreaBase }
     *     
     */
    public void setRatingAreaBase(RatingAreaBase value) {
        this.ratingAreaBase = value;
    }

    /**
     * Gets the value of the inAreaAction property.
     * 
     * @return
     *     possible object is
     *     {@link InAreaAction }
     *     
     */
    public InAreaAction getInAreaAction() {
        return inAreaAction;
    }

    /**
     * Sets the value of the inAreaAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link InAreaAction }
     *     
     */
    public void setInAreaAction(InAreaAction value) {
        this.inAreaAction = value;
    }

    /**
     * Gets the value of the outOfAreaAction property.
     * 
     * @return
     *     possible object is
     *     {@link OutOfAreaAction }
     *     
     */
    public OutOfAreaAction getOutOfAreaAction() {
        return outOfAreaAction;
    }

    /**
     * Sets the value of the outOfAreaAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutOfAreaAction }
     *     
     */
    public void setOutOfAreaAction(OutOfAreaAction value) {
        this.outOfAreaAction = value;
    }

}
