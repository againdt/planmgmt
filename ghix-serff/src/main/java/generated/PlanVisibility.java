
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlanVisibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlanVisibility">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OnWebsite"/>
 *     &lt;enumeration value="OnProposals"/>
 *     &lt;enumeration value="OnBoth"/>
 *     &lt;enumeration value="OffBoth"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PlanVisibility")
@XmlEnum
public enum PlanVisibility {

    @XmlEnumValue("OnWebsite")
    ON_WEBSITE("OnWebsite"),
    @XmlEnumValue("OnProposals")
    ON_PROPOSALS("OnProposals"),
    @XmlEnumValue("OnBoth")
    ON_BOTH("OnBoth"),
    @XmlEnumValue("OffBoth")
    OFF_BOTH("OffBoth");
    private final String value;

    PlanVisibility(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlanVisibility fromValue(String v) {
        for (PlanVisibility c: PlanVisibility.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
