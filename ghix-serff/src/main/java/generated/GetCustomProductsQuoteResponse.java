
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCustomProductsQuote.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomProductsQuote.Response">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomProductsQuote" type="{}GetCustomProductsQuote.Response.Quote" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomProductsQuote.Response", propOrder = {
    "customProductsQuote"
})
public class GetCustomProductsQuoteResponse {

    @XmlElementRef(name = "CustomProductsQuote", type = JAXBElement.class, required = false)
    protected JAXBElement<GetCustomProductsQuoteResponseQuote> customProductsQuote;

    /**
     * Gets the value of the customProductsQuote property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponseQuote }{@code >}
     *     
     */
    public JAXBElement<GetCustomProductsQuoteResponseQuote> getCustomProductsQuote() {
        return customProductsQuote;
    }

    /**
     * Sets the value of the customProductsQuote property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteResponseQuote }{@code >}
     *     
     */
    public void setCustomProductsQuote(JAXBElement<GetCustomProductsQuoteResponseQuote> value) {
        this.customProductsQuote = value;
    }

}
