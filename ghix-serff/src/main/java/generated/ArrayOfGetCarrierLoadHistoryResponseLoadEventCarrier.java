
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarrierLoadHistory.Response.LoadEvent.Carrier" type="{}GetCarrierLoadHistory.Response.LoadEvent.Carrier" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier", propOrder = {
    "getCarrierLoadHistoryResponseLoadEventCarrier"
})
public class ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier {

    @XmlElement(name = "GetCarrierLoadHistory.Response.LoadEvent.Carrier", nillable = true)
    protected List<GetCarrierLoadHistoryResponseLoadEventCarrier> getCarrierLoadHistoryResponseLoadEventCarrier;

    /**
     * Gets the value of the getCarrierLoadHistoryResponseLoadEventCarrier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarrierLoadHistoryResponseLoadEventCarrier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarrierLoadHistoryResponseLoadEventCarrier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarrierLoadHistoryResponseLoadEventCarrier }
     * 
     * 
     */
    public List<GetCarrierLoadHistoryResponseLoadEventCarrier> getGetCarrierLoadHistoryResponseLoadEventCarrier() {
        if (getCarrierLoadHistoryResponseLoadEventCarrier == null) {
            getCarrierLoadHistoryResponseLoadEventCarrier = new ArrayList<GetCarrierLoadHistoryResponseLoadEventCarrier>();
        }
        return this.getCarrierLoadHistoryResponseLoadEventCarrier;
    }

}
