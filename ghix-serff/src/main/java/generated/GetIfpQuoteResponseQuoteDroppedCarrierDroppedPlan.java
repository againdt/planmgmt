
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response.Quote.DroppedCarrier.DroppedPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response.Quote.DroppedCarrier.DroppedPlan">
 *   &lt;complexContent>
 *     &lt;extension base="{}DroppedPlanBase">
 *       &lt;sequence>
 *         &lt;element name="ContractStatus" type="{}ContractStatus"/>
 *         &lt;element name="Visibility" type="{}PlanVisibility"/>
 *         &lt;element name="RatingMethod" type="{}RatingMethod"/>
 *         &lt;element name="DroppedReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanDetails" type="{}GetIfpQuote.Response.PlanDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response.Quote.DroppedCarrier.DroppedPlan", propOrder = {
    "contractStatus",
    "visibility",
    "ratingMethod",
    "droppedReason",
    "planDetails"
})
public class GetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan
    extends DroppedPlanBase
{

    @XmlList
    @XmlElement(name = "ContractStatus", required = true)
    protected List<String> contractStatus;
    @XmlElement(name = "Visibility", required = true)
    protected PlanVisibility visibility;
    @XmlElement(name = "RatingMethod", required = true)
    protected RatingMethod ratingMethod;
    @XmlElementRef(name = "DroppedReason", type = JAXBElement.class, required = false)
    protected JAXBElement<String> droppedReason;
    @XmlElementRef(name = "PlanDetails", type = JAXBElement.class, required = false)
    protected JAXBElement<GetIfpQuoteResponsePlanDetail> planDetails;

    /**
     * Gets the value of the contractStatus property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contractStatus property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContractStatus().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getContractStatus() {
        if (contractStatus == null) {
            contractStatus = new ArrayList<String>();
        }
        return this.contractStatus;
    }

    /**
     * Gets the value of the visibility property.
     * 
     * @return
     *     possible object is
     *     {@link PlanVisibility }
     *     
     */
    public PlanVisibility getVisibility() {
        return visibility;
    }

    /**
     * Sets the value of the visibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanVisibility }
     *     
     */
    public void setVisibility(PlanVisibility value) {
        this.visibility = value;
    }

    /**
     * Gets the value of the ratingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link RatingMethod }
     *     
     */
    public RatingMethod getRatingMethod() {
        return ratingMethod;
    }

    /**
     * Sets the value of the ratingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link RatingMethod }
     *     
     */
    public void setRatingMethod(RatingMethod value) {
        this.ratingMethod = value;
    }

    /**
     * Gets the value of the droppedReason property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDroppedReason() {
        return droppedReason;
    }

    /**
     * Sets the value of the droppedReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDroppedReason(JAXBElement<String> value) {
        this.droppedReason = value;
    }

    /**
     * Gets the value of the planDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetIfpQuoteResponsePlanDetail }{@code >}
     *     
     */
    public JAXBElement<GetIfpQuoteResponsePlanDetail> getPlanDetails() {
        return planDetails;
    }

    /**
     * Sets the value of the planDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetIfpQuoteResponsePlanDetail }{@code >}
     *     
     */
    public void setPlanDetails(JAXBElement<GetIfpQuoteResponsePlanDetail> value) {
        this.planDetails = value;
    }

}
