
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Request.Preference.DrugFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Request.Preference.DrugFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Request.Preference.DrugFilter" type="{}GetGroupQuote.Request.Preference.DrugFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Request.Preference.DrugFilter", propOrder = {
    "getGroupQuoteRequestPreferenceDrugFilter"
})
public class ArrayOfGetGroupQuoteRequestPreferenceDrugFilter {

    @XmlElement(name = "GetGroupQuote.Request.Preference.DrugFilter", nillable = true)
    protected List<GetGroupQuoteRequestPreferenceDrugFilter> getGroupQuoteRequestPreferenceDrugFilter;

    /**
     * Gets the value of the getGroupQuoteRequestPreferenceDrugFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteRequestPreferenceDrugFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteRequestPreferenceDrugFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteRequestPreferenceDrugFilter }
     * 
     * 
     */
    public List<GetGroupQuoteRequestPreferenceDrugFilter> getGetGroupQuoteRequestPreferenceDrugFilter() {
        if (getGroupQuoteRequestPreferenceDrugFilter == null) {
            getGroupQuoteRequestPreferenceDrugFilter = new ArrayList<GetGroupQuoteRequestPreferenceDrugFilter>();
        }
        return this.getGroupQuoteRequestPreferenceDrugFilter;
    }

}
