
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarrierLoadHistory.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarrierLoadHistory.Response">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LatestLoadSequenceId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CutOffLoadSequenceId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="LoadEvents" type="{}ArrayOfGetCarrierLoadHistory.Response.LoadEvent" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarrierLoadHistory.Response", propOrder = {
    "latestLoadSequenceId",
    "cutOffLoadSequenceId",
    "loadEvents"
})
public class GetCarrierLoadHistoryResponse {

    @XmlElement(name = "LatestLoadSequenceId")
    protected int latestLoadSequenceId;
    @XmlElement(name = "CutOffLoadSequenceId")
    protected Integer cutOffLoadSequenceId;
    @XmlElementRef(name = "LoadEvents", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEvent> loadEvents;

    /**
     * Gets the value of the latestLoadSequenceId property.
     * 
     */
    public int getLatestLoadSequenceId() {
        return latestLoadSequenceId;
    }

    /**
     * Sets the value of the latestLoadSequenceId property.
     * 
     */
    public void setLatestLoadSequenceId(int value) {
        this.latestLoadSequenceId = value;
    }

    /**
     * Gets the value of the cutOffLoadSequenceId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCutOffLoadSequenceId() {
        return cutOffLoadSequenceId;
    }

    /**
     * Sets the value of the cutOffLoadSequenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCutOffLoadSequenceId(Integer value) {
        this.cutOffLoadSequenceId = value;
    }

    /**
     * Gets the value of the loadEvents property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarrierLoadHistoryResponseLoadEvent }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEvent> getLoadEvents() {
        return loadEvents;
    }

    /**
     * Sets the value of the loadEvents property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarrierLoadHistoryResponseLoadEvent }{@code >}
     *     
     */
    public void setLoadEvents(JAXBElement<ArrayOfGetCarrierLoadHistoryResponseLoadEvent> value) {
        this.loadEvents = value;
    }

}
