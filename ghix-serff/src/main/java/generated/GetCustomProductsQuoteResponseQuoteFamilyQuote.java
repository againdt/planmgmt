
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCustomProductsQuote.Response.Quote.FamilyQuote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomProductsQuote.Response.Quote.FamilyQuote">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FamilyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Carriers" type="{}ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier" minOccurs="0"/>
 *         &lt;element name="DroppedRates" type="{}ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.DroppedCarrier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomProductsQuote.Response.Quote.FamilyQuote", propOrder = {
    "familyId",
    "carriers",
    "droppedRates"
})
public class GetCustomProductsQuoteResponseQuoteFamilyQuote {

    @XmlElement(name = "FamilyId", required = true, nillable = true)
    protected String familyId;
    @XmlElementRef(name = "Carriers", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier> carriers;
    @XmlElementRef(name = "DroppedRates", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier> droppedRates;

    /**
     * Gets the value of the familyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFamilyId() {
        return familyId;
    }

    /**
     * Sets the value of the familyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFamilyId(String value) {
        this.familyId = value;
    }

    /**
     * Gets the value of the carriers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier> getCarriers() {
        return carriers;
    }

    /**
     * Sets the value of the carriers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier }{@code >}
     *     
     */
    public void setCarriers(JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier> value) {
        this.carriers = value;
    }

    /**
     * Gets the value of the droppedRates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier> getDroppedRates() {
        return droppedRates;
    }

    /**
     * Sets the value of the droppedRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier }{@code >}
     *     
     */
    public void setDroppedRates(JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteDroppedCarrier> value) {
        this.droppedRates = value;
    }

}
