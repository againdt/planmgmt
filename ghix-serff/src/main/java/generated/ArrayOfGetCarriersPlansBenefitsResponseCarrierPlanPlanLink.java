
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink" type="{}GetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink", propOrder = {
    "getCarriersPlansBenefitsResponseCarrierPlanPlanLink"
})
public class ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseCarrierPlanPlanLink> getCarriersPlansBenefitsResponseCarrierPlanPlanLink;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseCarrierPlanPlanLink property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseCarrierPlanPlanLink property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseCarrierPlanPlanLink().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseCarrierPlanPlanLink }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseCarrierPlanPlanLink> getGetCarriersPlansBenefitsResponseCarrierPlanPlanLink() {
        if (getCarriersPlansBenefitsResponseCarrierPlanPlanLink == null) {
            getCarriersPlansBenefitsResponseCarrierPlanPlanLink = new ArrayList<GetCarriersPlansBenefitsResponseCarrierPlanPlanLink>();
        }
        return this.getCarriersPlansBenefitsResponseCarrierPlanPlanLink;
    }

}
