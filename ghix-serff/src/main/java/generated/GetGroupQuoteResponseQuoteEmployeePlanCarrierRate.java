
package generated;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate">
 *   &lt;complexContent>
 *     &lt;extension base="{}RateBase">
 *       &lt;sequence>
 *         &lt;element name="IsShortCoverage" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CoverageTier" type="{}CoverageTier"/>
 *         &lt;element name="RxMatch" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CoveredRxCUIs" type="{}RxCUIsBase" minOccurs="0"/>
 *         &lt;element name="MemberRates" type="{}ArrayOfGetGroupQuote.Response.MemberRate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response.Quote.EmployeePlan.Carrier.Rate", propOrder = {
    "isShortCoverage",
    "coverageTier",
    "rxMatch",
    "coveredRxCUIs",
    "memberRates"
})
public class GetGroupQuoteResponseQuoteEmployeePlanCarrierRate
    extends RateBase
{

    @XmlElement(name = "IsShortCoverage")
    protected boolean isShortCoverage;
    @XmlElement(name = "CoverageTier", required = true)
    protected CoverageTier coverageTier;
    @XmlElement(name = "RxMatch")
    protected BigDecimal rxMatch;
    @XmlElementRef(name = "CoveredRxCUIs", type = JAXBElement.class, required = false)
    protected JAXBElement<RxCUIsBase> coveredRxCUIs;
    @XmlElementRef(name = "MemberRates", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteResponseMemberRate> memberRates;

    /**
     * Gets the value of the isShortCoverage property.
     * 
     */
    public boolean isIsShortCoverage() {
        return isShortCoverage;
    }

    /**
     * Sets the value of the isShortCoverage property.
     * 
     */
    public void setIsShortCoverage(boolean value) {
        this.isShortCoverage = value;
    }

    /**
     * Gets the value of the coverageTier property.
     * 
     * @return
     *     possible object is
     *     {@link CoverageTier }
     *     
     */
    public CoverageTier getCoverageTier() {
        return coverageTier;
    }

    /**
     * Sets the value of the coverageTier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoverageTier }
     *     
     */
    public void setCoverageTier(CoverageTier value) {
        this.coverageTier = value;
    }

    /**
     * Gets the value of the rxMatch property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRxMatch() {
        return rxMatch;
    }

    /**
     * Sets the value of the rxMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRxMatch(BigDecimal value) {
        this.rxMatch = value;
    }

    /**
     * Gets the value of the coveredRxCUIs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}
     *     
     */
    public JAXBElement<RxCUIsBase> getCoveredRxCUIs() {
        return coveredRxCUIs;
    }

    /**
     * Sets the value of the coveredRxCUIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}
     *     
     */
    public void setCoveredRxCUIs(JAXBElement<RxCUIsBase> value) {
        this.coveredRxCUIs = value;
    }

    /**
     * Gets the value of the memberRates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseMemberRate }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteResponseMemberRate> getMemberRates() {
        return memberRates;
    }

    /**
     * Sets the value of the memberRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseMemberRate }{@code >}
     *     
     */
    public void setMemberRates(JAXBElement<ArrayOfGetGroupQuoteResponseMemberRate> value) {
        this.memberRates = value;
    }

}
