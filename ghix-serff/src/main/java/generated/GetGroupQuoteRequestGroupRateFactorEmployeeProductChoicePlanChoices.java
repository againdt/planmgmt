
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Request.GroupRateFactor.Employee.ProductChoice.PlanChoices complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Request.GroupRateFactor.Employee.ProductChoice.PlanChoices">
 *   &lt;complexContent>
 *     &lt;extension base="{}PlanChoiceBase">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Request.GroupRateFactor.Employee.ProductChoice.PlanChoices")
public class GetGroupQuoteRequestGroupRateFactorEmployeeProductChoicePlanChoices
    extends PlanChoiceBase
{


}
