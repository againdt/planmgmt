
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance" type="{}GetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance", propOrder = {
    "getCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance"
})
public class ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance> getCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance> getGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance() {
        if (getCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance == null) {
            getCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance = new ArrayList<GetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance>();
        }
        return this.getCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance;
    }

}
