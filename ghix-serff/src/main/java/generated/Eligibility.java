
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Eligibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Eligibility">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Ineligible"/>
 *     &lt;enumeration value="AddOn"/>
 *     &lt;enumeration value="Included"/>
 *     &lt;enumeration value="Capped"/>
 *     &lt;enumeration value="Unrated"/>
 *     &lt;enumeration value="Waived"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Eligibility")
@XmlEnum
public enum Eligibility {

    @XmlEnumValue("Ineligible")
    INELIGIBLE("Ineligible"),
    @XmlEnumValue("AddOn")
    ADD_ON("AddOn"),
    @XmlEnumValue("Included")
    INCLUDED("Included"),
    @XmlEnumValue("Capped")
    CAPPED("Capped"),
    @XmlEnumValue("Unrated")
    UNRATED("Unrated"),
    @XmlEnumValue("Waived")
    WAIVED("Waived");
    private final String value;

    Eligibility(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Eligibility fromValue(String v) {
        for (Eligibility c: Eligibility.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
