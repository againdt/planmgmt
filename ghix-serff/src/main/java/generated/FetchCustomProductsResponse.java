
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FetchCustomProducts.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FetchCustomProducts.Response">
 *   &lt;complexContent>
 *     &lt;extension base="{}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="SubmissionId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SubmissionType" type="{}SubmissionType" minOccurs="0"/>
 *         &lt;element name="Remarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustomProducts" type="{}ArrayOfCustomProduct" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FetchCustomProducts.Response", propOrder = {
    "submissionId",
    "submissionType",
    "remarks",
    "customProducts"
})
public class FetchCustomProductsResponse
    extends ResponseBase
{

    @XmlElementRef(name = "SubmissionId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> submissionId;
    @XmlElementRef(name = "SubmissionType", type = JAXBElement.class, required = false)
    protected JAXBElement<SubmissionType> submissionType;
    @XmlElementRef(name = "Remarks", type = JAXBElement.class, required = false)
    protected JAXBElement<String> remarks;
    @XmlElementRef(name = "CustomProducts", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomProduct> customProducts;

    /**
     * Gets the value of the submissionId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSubmissionId() {
        return submissionId;
    }

    /**
     * Sets the value of the submissionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSubmissionId(JAXBElement<Integer> value) {
        this.submissionId = value;
    }

    /**
     * Gets the value of the submissionType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SubmissionType }{@code >}
     *     
     */
    public JAXBElement<SubmissionType> getSubmissionType() {
        return submissionType;
    }

    /**
     * Sets the value of the submissionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SubmissionType }{@code >}
     *     
     */
    public void setSubmissionType(JAXBElement<SubmissionType> value) {
        this.submissionType = value;
    }

    /**
     * Gets the value of the remarks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRemarks() {
        return remarks;
    }

    /**
     * Sets the value of the remarks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRemarks(JAXBElement<String> value) {
        this.remarks = value;
    }

    /**
     * Gets the value of the customProducts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProduct }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomProduct> getCustomProducts() {
        return customProducts;
    }

    /**
     * Sets the value of the customProducts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProduct }{@code >}
     *     
     */
    public void setCustomProducts(JAXBElement<ArrayOfCustomProduct> value) {
        this.customProducts = value;
    }

}
