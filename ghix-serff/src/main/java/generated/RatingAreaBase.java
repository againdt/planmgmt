
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RatingAreaBase.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RatingAreaBase">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FamilyResidence"/>
 *     &lt;enumeration value="MemberResidence"/>
 *     &lt;enumeration value="EEWorksite"/>
 *     &lt;enumeration value="ERLocation"/>
 *     &lt;enumeration value="SpecialArea"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RatingAreaBase")
@XmlEnum
public enum RatingAreaBase {

    @XmlEnumValue("FamilyResidence")
    FAMILY_RESIDENCE("FamilyResidence"),
    @XmlEnumValue("MemberResidence")
    MEMBER_RESIDENCE("MemberResidence"),
    @XmlEnumValue("EEWorksite")
    EE_WORKSITE("EEWorksite"),
    @XmlEnumValue("ERLocation")
    ER_LOCATION("ERLocation"),
    @XmlEnumValue("SpecialArea")
    SPECIAL_AREA("SpecialArea");
    private final String value;

    RatingAreaBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RatingAreaBase fromValue(String v) {
        for (RatingAreaBase c: RatingAreaBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
