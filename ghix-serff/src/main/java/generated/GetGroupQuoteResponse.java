
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupQuote" type="{}GetGroupQuote.Response.Quote" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response", propOrder = {
    "groupQuote"
})
public class GetGroupQuoteResponse {

    @XmlElementRef(name = "GroupQuote", type = JAXBElement.class, required = false)
    protected JAXBElement<GetGroupQuoteResponseQuote> groupQuote;

    /**
     * Gets the value of the groupQuote property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuote }{@code >}
     *     
     */
    public JAXBElement<GetGroupQuoteResponseQuote> getGroupQuote() {
        return groupQuote;
    }

    /**
     * Sets the value of the groupQuote property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseQuote }{@code >}
     *     
     */
    public void setGroupQuote(JAXBElement<GetGroupQuoteResponseQuote> value) {
        this.groupQuote = value;
    }

}
