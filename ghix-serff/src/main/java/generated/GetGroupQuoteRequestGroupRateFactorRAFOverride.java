
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Request.GroupRateFactor.RAFOverride complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Request.GroupRateFactor.RAFOverride">
 *   &lt;complexContent>
 *     &lt;extension base="{}RAFOverrideBase">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Request.GroupRateFactor.RAFOverride")
public class GetGroupQuoteRequestGroupRateFactorRAFOverride
    extends RAFOverrideBase
{


}
