
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.Request.Preference.ChoiceFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.Request.Preference.ChoiceFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.Request.Preference.ChoiceFilter" type="{}GetIfpQuote.Request.Preference.ChoiceFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.Request.Preference.ChoiceFilter", propOrder = {
    "getIfpQuoteRequestPreferenceChoiceFilter"
})
public class ArrayOfGetIfpQuoteRequestPreferenceChoiceFilter {

    @XmlElement(name = "GetIfpQuote.Request.Preference.ChoiceFilter", nillable = true)
    protected List<GetIfpQuoteRequestPreferenceChoiceFilter> getIfpQuoteRequestPreferenceChoiceFilter;

    /**
     * Gets the value of the getIfpQuoteRequestPreferenceChoiceFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteRequestPreferenceChoiceFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteRequestPreferenceChoiceFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteRequestPreferenceChoiceFilter }
     * 
     * 
     */
    public List<GetIfpQuoteRequestPreferenceChoiceFilter> getGetIfpQuoteRequestPreferenceChoiceFilter() {
        if (getIfpQuoteRequestPreferenceChoiceFilter == null) {
            getIfpQuoteRequestPreferenceChoiceFilter = new ArrayList<GetIfpQuoteRequestPreferenceChoiceFilter>();
        }
        return this.getIfpQuoteRequestPreferenceChoiceFilter;
    }

}
