
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response.Quote.DroppedCarrier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response.Quote.DroppedCarrier">
 *   &lt;complexContent>
 *     &lt;extension base="{}DroppedCarrierBase">
 *       &lt;sequence>
 *         &lt;element name="CarrierDetails" type="{}GetGroupQuote.Response.CarrierDetail" minOccurs="0"/>
 *         &lt;element name="DroppedPlans" type="{}ArrayOfGetGroupQuote.Response.Quote.DroppedCarrier.DroppedPlan" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response.Quote.DroppedCarrier", propOrder = {
    "carrierDetails",
    "droppedPlans"
})
public class GetGroupQuoteResponseQuoteDroppedCarrier
    extends DroppedCarrierBase
{

    @XmlElementRef(name = "CarrierDetails", type = JAXBElement.class, required = false)
    protected JAXBElement<GetGroupQuoteResponseCarrierDetail> carrierDetails;
    @XmlElementRef(name = "DroppedPlans", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan> droppedPlans;

    /**
     * Gets the value of the carrierDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseCarrierDetail }{@code >}
     *     
     */
    public JAXBElement<GetGroupQuoteResponseCarrierDetail> getCarrierDetails() {
        return carrierDetails;
    }

    /**
     * Sets the value of the carrierDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseCarrierDetail }{@code >}
     *     
     */
    public void setCarrierDetails(JAXBElement<GetGroupQuoteResponseCarrierDetail> value) {
        this.carrierDetails = value;
    }

    /**
     * Gets the value of the droppedPlans property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan> getDroppedPlans() {
        return droppedPlans;
    }

    /**
     * Sets the value of the droppedPlans property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan }{@code >}
     *     
     */
    public void setDroppedPlans(JAXBElement<ArrayOfGetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan> value) {
        this.droppedPlans = value;
    }

}
