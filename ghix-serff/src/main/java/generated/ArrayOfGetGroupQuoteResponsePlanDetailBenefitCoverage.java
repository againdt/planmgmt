
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Response.PlanDetail.Benefit.Coverage" type="{}GetGroupQuote.Response.PlanDetail.Benefit.Coverage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage", propOrder = {
    "getGroupQuoteResponsePlanDetailBenefitCoverage"
})
public class ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverage {

    @XmlElement(name = "GetGroupQuote.Response.PlanDetail.Benefit.Coverage", nillable = true)
    protected List<GetGroupQuoteResponsePlanDetailBenefitCoverage> getGroupQuoteResponsePlanDetailBenefitCoverage;

    /**
     * Gets the value of the getGroupQuoteResponsePlanDetailBenefitCoverage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteResponsePlanDetailBenefitCoverage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteResponsePlanDetailBenefitCoverage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteResponsePlanDetailBenefitCoverage }
     * 
     * 
     */
    public List<GetGroupQuoteResponsePlanDetailBenefitCoverage> getGetGroupQuoteResponsePlanDetailBenefitCoverage() {
        if (getGroupQuoteResponsePlanDetailBenefitCoverage == null) {
            getGroupQuoteResponsePlanDetailBenefitCoverage = new ArrayList<GetGroupQuoteResponsePlanDetailBenefitCoverage>();
        }
        return this.getGroupQuoteResponsePlanDetailBenefitCoverage;
    }

}
