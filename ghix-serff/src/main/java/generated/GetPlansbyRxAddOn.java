
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPlansbyRx.AddOn.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GetPlansbyRx.AddOn">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CarrierDetails"/>
 *     &lt;enumeration value="CarrierData"/>
 *     &lt;enumeration value="PlanDetails"/>
 *     &lt;enumeration value="PlanData"/>
 *     &lt;enumeration value="BenefitsFlag"/>
 *     &lt;enumeration value="BenefitsNumeric"/>
 *     &lt;enumeration value="BenefitsTiny"/>
 *     &lt;enumeration value="BenefitsShort"/>
 *     &lt;enumeration value="BenefitsLong"/>
 *     &lt;enumeration value="BenefitsFull"/>
 *     &lt;enumeration value="BenefitsServices"/>
 *     &lt;enumeration value="BenefitsDefinition"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GetPlansbyRx.AddOn")
@XmlEnum
public enum GetPlansbyRxAddOn {

    @XmlEnumValue("CarrierDetails")
    CARRIER_DETAILS("CarrierDetails"),
    @XmlEnumValue("CarrierData")
    CARRIER_DATA("CarrierData"),
    @XmlEnumValue("PlanDetails")
    PLAN_DETAILS("PlanDetails"),
    @XmlEnumValue("PlanData")
    PLAN_DATA("PlanData"),
    @XmlEnumValue("BenefitsFlag")
    BENEFITS_FLAG("BenefitsFlag"),
    @XmlEnumValue("BenefitsNumeric")
    BENEFITS_NUMERIC("BenefitsNumeric"),
    @XmlEnumValue("BenefitsTiny")
    BENEFITS_TINY("BenefitsTiny"),
    @XmlEnumValue("BenefitsShort")
    BENEFITS_SHORT("BenefitsShort"),
    @XmlEnumValue("BenefitsLong")
    BENEFITS_LONG("BenefitsLong"),
    @XmlEnumValue("BenefitsFull")
    BENEFITS_FULL("BenefitsFull"),
    @XmlEnumValue("BenefitsServices")
    BENEFITS_SERVICES("BenefitsServices"),
    @XmlEnumValue("BenefitsDefinition")
    BENEFITS_DEFINITION("BenefitsDefinition");
    private final String value;

    GetPlansbyRxAddOn(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GetPlansbyRxAddOn fromValue(String v) {
        for (GetPlansbyRxAddOn c: GetPlansbyRxAddOn.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
