
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage">
 *   &lt;complexContent>
 *     &lt;extension base="{}CoverageBase">
 *       &lt;sequence>
 *         &lt;element name="ViewPoints" type="{}ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage", propOrder = {
    "viewPoints"
})
public class GetPlansbyRxResponseCarrierPlanBenefitCoverage
    extends CoverageBase
{

    @XmlElementRef(name = "ViewPoints", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint> viewPoints;

    /**
     * Gets the value of the viewPoints property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint> getViewPoints() {
        return viewPoints;
    }

    /**
     * Sets the value of the viewPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint }{@code >}
     *     
     */
    public void setViewPoints(JAXBElement<ArrayOfGetPlansbyRxResponseCarrierPlanBenefitCoverageViewPoint> value) {
        this.viewPoints = value;
    }

}
