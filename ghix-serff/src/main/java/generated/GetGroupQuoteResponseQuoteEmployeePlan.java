
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response.Quote.EmployeePlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response.Quote.EmployeePlan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmployeeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Carriers" type="{}ArrayOfGetGroupQuote.Response.Quote.EmployeePlan.Carrier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response.Quote.EmployeePlan", propOrder = {
    "employeeId",
    "carriers"
})
public class GetGroupQuoteResponseQuoteEmployeePlan {

    @XmlElement(name = "EmployeeId", required = true, nillable = true)
    protected String employeeId;
    @XmlElementRef(name = "Carriers", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier> carriers;

    /**
     * Gets the value of the employeeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * Sets the value of the employeeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeId(String value) {
        this.employeeId = value;
    }

    /**
     * Gets the value of the carriers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier> getCarriers() {
        return carriers;
    }

    /**
     * Sets the value of the carriers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier }{@code >}
     *     
     */
    public void setCarriers(JAXBElement<ArrayOfGetGroupQuoteResponseQuoteEmployeePlanCarrier> value) {
        this.carriers = value;
    }

}
