
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfFamily.Contact.ContactAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfFamily.Contact.ContactAttribute">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Family.Contact.ContactAttribute" type="{}Family.Contact.ContactAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfFamily.Contact.ContactAttribute", propOrder = {
    "familyContactContactAttribute"
})
public class ArrayOfFamilyContactContactAttribute {

    @XmlElement(name = "Family.Contact.ContactAttribute", nillable = true)
    protected List<FamilyContactContactAttribute> familyContactContactAttribute;

    /**
     * Gets the value of the familyContactContactAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the familyContactContactAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFamilyContactContactAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FamilyContactContactAttribute }
     * 
     * 
     */
    public List<FamilyContactContactAttribute> getFamilyContactContactAttribute() {
        if (familyContactContactAttribute == null) {
            familyContactContactAttribute = new ArrayList<FamilyContactContactAttribute>();
        }
        return this.familyContactContactAttribute;
    }

}
