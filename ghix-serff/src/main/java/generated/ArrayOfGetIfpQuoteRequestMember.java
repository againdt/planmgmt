
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.Request.Member complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.Request.Member">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.Request.Member" type="{}GetIfpQuote.Request.Member" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.Request.Member", propOrder = {
    "getIfpQuoteRequestMember"
})
public class ArrayOfGetIfpQuoteRequestMember {

    @XmlElement(name = "GetIfpQuote.Request.Member", nillable = true)
    protected List<GetIfpQuoteRequestMember> getIfpQuoteRequestMember;

    /**
     * Gets the value of the getIfpQuoteRequestMember property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteRequestMember property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteRequestMember().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteRequestMember }
     * 
     * 
     */
    public List<GetIfpQuoteRequestMember> getGetIfpQuoteRequestMember() {
        if (getIfpQuoteRequestMember == null) {
            getIfpQuoteRequestMember = new ArrayList<GetIfpQuoteRequestMember>();
        }
        return this.getIfpQuoteRequestMember;
    }

}
