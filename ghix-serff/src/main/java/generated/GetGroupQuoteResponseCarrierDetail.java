
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response.CarrierDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response.CarrierDetail">
 *   &lt;complexContent>
 *     &lt;extension base="{}CarrierDetailsBase">
 *       &lt;sequence>
 *         &lt;element name="CarrierData" type="{}ArrayOfGetGroupQuote.Response.Item" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response.CarrierDetail", propOrder = {
    "carrierData"
})
public class GetGroupQuoteResponseCarrierDetail
    extends CarrierDetailsBase
{

    @XmlElementRef(name = "CarrierData", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteResponseItem> carrierData;

    /**
     * Gets the value of the carrierData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteResponseItem> getCarrierData() {
        return carrierData;
    }

    /**
     * Sets the value of the carrierData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseItem }{@code >}
     *     
     */
    public void setCarrierData(JAXBElement<ArrayOfGetGroupQuoteResponseItem> value) {
        this.carrierData = value;
    }

}
