
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service">
 *   &lt;complexContent>
 *     &lt;extension base="{}ServiceBase">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPlansbyRx.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service")
public class GetPlansbyRxResponseCarrierPlanBenefitCoverageViewPointService
    extends ServiceBase
{


}
