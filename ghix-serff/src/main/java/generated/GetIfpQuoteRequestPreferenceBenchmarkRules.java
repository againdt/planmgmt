
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Request.Preference.BenchmarkRules complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Request.Preference.BenchmarkRules">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Rank" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *         &lt;element name="Towards" type="{}Towards"/>
 *         &lt;element name="PlanComponents" type="{}ArrayOfGetIfpQuote.Request.Preference.BenchmarkRules.PlanComponent"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Request.Preference.BenchmarkRules", propOrder = {
    "rank",
    "towards",
    "planComponents"
})
public class GetIfpQuoteRequestPreferenceBenchmarkRules {

    @XmlElement(name = "Rank")
    @XmlSchemaType(name = "unsignedByte")
    protected short rank;
    @XmlList
    @XmlElement(name = "Towards", required = true)
    protected List<String> towards;
    @XmlElement(name = "PlanComponents", required = true, nillable = true)
    protected ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent planComponents;

    /**
     * Gets the value of the rank property.
     * 
     */
    public short getRank() {
        return rank;
    }

    /**
     * Sets the value of the rank property.
     * 
     */
    public void setRank(short value) {
        this.rank = value;
    }

    /**
     * Gets the value of the towards property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the towards property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTowards().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTowards() {
        if (towards == null) {
            towards = new ArrayList<String>();
        }
        return this.towards;
    }

    /**
     * Gets the value of the planComponents property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent }
     *     
     */
    public ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent getPlanComponents() {
        return planComponents;
    }

    /**
     * Sets the value of the planComponents property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent }
     *     
     */
    public void setPlanComponents(ArrayOfGetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent value) {
        this.planComponents = value;
    }

}
