
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GetCarrierLoadHistory.Response.LoadEvent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarrierLoadHistory.Response.LoadEvent">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoadSequenceId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="EventStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="EventOperation" type="{}EventOperation"/>
 *         &lt;element name="Carriers" type="{}ArrayOfGetCarrierLoadHistory.Response.LoadEvent.Carrier"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarrierLoadHistory.Response.LoadEvent", propOrder = {
    "loadSequenceId",
    "eventStartDate",
    "effectiveDate",
    "eventOperation",
    "carriers"
})
public class GetCarrierLoadHistoryResponseLoadEvent {

    @XmlElement(name = "LoadSequenceId")
    protected int loadSequenceId;
    @XmlElement(name = "EventStartDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar eventStartDate;
    @XmlElement(name = "EffectiveDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlList
    @XmlElement(name = "EventOperation", required = true)
    protected List<String> eventOperation;
    @XmlElement(name = "Carriers", required = true, nillable = true)
    protected ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier carriers;

    /**
     * Gets the value of the loadSequenceId property.
     * 
     */
    public int getLoadSequenceId() {
        return loadSequenceId;
    }

    /**
     * Sets the value of the loadSequenceId property.
     * 
     */
    public void setLoadSequenceId(int value) {
        this.loadSequenceId = value;
    }

    /**
     * Gets the value of the eventStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEventStartDate() {
        return eventStartDate;
    }

    /**
     * Sets the value of the eventStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEventStartDate(XMLGregorianCalendar value) {
        this.eventStartDate = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the eventOperation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eventOperation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventOperation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getEventOperation() {
        if (eventOperation == null) {
            eventOperation = new ArrayList<String>();
        }
        return this.eventOperation;
    }

    /**
     * Gets the value of the carriers property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier }
     *     
     */
    public ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier getCarriers() {
        return carriers;
    }

    /**
     * Sets the value of the carriers property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier }
     *     
     */
    public void setCarriers(ArrayOfGetCarrierLoadHistoryResponseLoadEventCarrier value) {
        this.carriers = value;
    }

}
