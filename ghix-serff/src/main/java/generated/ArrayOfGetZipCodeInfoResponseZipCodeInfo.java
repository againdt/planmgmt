
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetZipCodeInfo.Response.ZipCodeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetZipCodeInfo.Response.ZipCodeInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetZipCodeInfo.Response.ZipCodeInfo" type="{}GetZipCodeInfo.Response.ZipCodeInfo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetZipCodeInfo.Response.ZipCodeInfo", propOrder = {
    "getZipCodeInfoResponseZipCodeInfo"
})
public class ArrayOfGetZipCodeInfoResponseZipCodeInfo {

    @XmlElement(name = "GetZipCodeInfo.Response.ZipCodeInfo", nillable = true)
    protected List<GetZipCodeInfoResponseZipCodeInfo> getZipCodeInfoResponseZipCodeInfo;

    /**
     * Gets the value of the getZipCodeInfoResponseZipCodeInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getZipCodeInfoResponseZipCodeInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetZipCodeInfoResponseZipCodeInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetZipCodeInfoResponseZipCodeInfo }
     * 
     * 
     */
    public List<GetZipCodeInfoResponseZipCodeInfo> getGetZipCodeInfoResponseZipCodeInfo() {
        if (getZipCodeInfoResponseZipCodeInfo == null) {
            getZipCodeInfoResponseZipCodeInfo = new ArrayList<GetZipCodeInfoResponseZipCodeInfo>();
        }
        return this.getZipCodeInfoResponseZipCodeInfo;
    }

}
