
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGroupShoppingCart.ProductChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGroupShoppingCart.ProductChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GroupShoppingCart.ProductChoice" type="{}GroupShoppingCart.ProductChoice" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGroupShoppingCart.ProductChoice", propOrder = {
    "groupShoppingCartProductChoice"
})
public class ArrayOfGroupShoppingCartProductChoice {

    @XmlElement(name = "GroupShoppingCart.ProductChoice", nillable = true)
    protected List<GroupShoppingCartProductChoice> groupShoppingCartProductChoice;

    /**
     * Gets the value of the groupShoppingCartProductChoice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the groupShoppingCartProductChoice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGroupShoppingCartProductChoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GroupShoppingCartProductChoice }
     * 
     * 
     */
    public List<GroupShoppingCartProductChoice> getGroupShoppingCartProductChoice() {
        if (groupShoppingCartProductChoice == null) {
            groupShoppingCartProductChoice = new ArrayList<GroupShoppingCartProductChoice>();
        }
        return this.groupShoppingCartProductChoice;
    }

}
