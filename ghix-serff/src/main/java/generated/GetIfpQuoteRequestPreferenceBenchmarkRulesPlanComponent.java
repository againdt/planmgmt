
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Request.Preference.BenchmarkRules.PlanComponent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Request.Preference.BenchmarkRules.PlanComponent">
 *   &lt;complexContent>
 *     &lt;extension base="{}PlanComponentBase">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Request.Preference.BenchmarkRules.PlanComponent")
public class GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent
    extends PlanComponentBase
{


}
