
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Request.VersionFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Request.VersionFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Request.VersionFilter" type="{}GetCarriersPlansBenefits.Request.VersionFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Request.VersionFilter", propOrder = {
    "getCarriersPlansBenefitsRequestVersionFilter"
})
public class ArrayOfGetCarriersPlansBenefitsRequestVersionFilter {

    @XmlElement(name = "GetCarriersPlansBenefits.Request.VersionFilter", nillable = true)
    protected List<GetCarriersPlansBenefitsRequestVersionFilter> getCarriersPlansBenefitsRequestVersionFilter;

    /**
     * Gets the value of the getCarriersPlansBenefitsRequestVersionFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsRequestVersionFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsRequestVersionFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsRequestVersionFilter }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsRequestVersionFilter> getGetCarriersPlansBenefitsRequestVersionFilter() {
        if (getCarriersPlansBenefitsRequestVersionFilter == null) {
            getCarriersPlansBenefitsRequestVersionFilter = new ArrayList<GetCarriersPlansBenefitsRequestVersionFilter>();
        }
        return this.getCarriersPlansBenefitsRequestVersionFilter;
    }

}
