
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Request.Preference.BenefitFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Request.Preference.BenefitFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Request.Preference.BenefitFilter" type="{}GetGroupQuote.Request.Preference.BenefitFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Request.Preference.BenefitFilter", propOrder = {
    "getGroupQuoteRequestPreferenceBenefitFilter"
})
public class ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter {

    @XmlElement(name = "GetGroupQuote.Request.Preference.BenefitFilter", nillable = true)
    protected List<GetGroupQuoteRequestPreferenceBenefitFilter> getGroupQuoteRequestPreferenceBenefitFilter;

    /**
     * Gets the value of the getGroupQuoteRequestPreferenceBenefitFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteRequestPreferenceBenefitFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteRequestPreferenceBenefitFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteRequestPreferenceBenefitFilter }
     * 
     * 
     */
    public List<GetGroupQuoteRequestPreferenceBenefitFilter> getGetGroupQuoteRequestPreferenceBenefitFilter() {
        if (getGroupQuoteRequestPreferenceBenefitFilter == null) {
            getGroupQuoteRequestPreferenceBenefitFilter = new ArrayList<GetGroupQuoteRequestPreferenceBenefitFilter>();
        }
        return this.getGroupQuoteRequestPreferenceBenefitFilter;
    }

}
