
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmitIfpShoppingCart.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitIfpShoppingCart.Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RemoteAccessKey" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid"/>
 *         &lt;element name="WebsiteAccessKey" type="{http://schemas.microsoft.com/2003/10/Serialization/}guid"/>
 *         &lt;element name="FamilyId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ShoppingCart" type="{}IfpShoppingCart"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitIfpShoppingCart.Request", propOrder = {
    "remoteAccessKey",
    "websiteAccessKey",
    "familyId",
    "shoppingCart"
})
public class SubmitIfpShoppingCartRequest {

    @XmlElement(name = "RemoteAccessKey", required = true)
    protected String remoteAccessKey;
    @XmlElement(name = "WebsiteAccessKey", required = true)
    protected String websiteAccessKey;
    @XmlElement(name = "FamilyId")
    protected int familyId;
    @XmlElement(name = "ShoppingCart", required = true, nillable = true)
    protected IfpShoppingCart shoppingCart;

    /**
     * Gets the value of the remoteAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemoteAccessKey() {
        return remoteAccessKey;
    }

    /**
     * Sets the value of the remoteAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemoteAccessKey(String value) {
        this.remoteAccessKey = value;
    }

    /**
     * Gets the value of the websiteAccessKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebsiteAccessKey() {
        return websiteAccessKey;
    }

    /**
     * Sets the value of the websiteAccessKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebsiteAccessKey(String value) {
        this.websiteAccessKey = value;
    }

    /**
     * Gets the value of the familyId property.
     * 
     */
    public int getFamilyId() {
        return familyId;
    }

    /**
     * Sets the value of the familyId property.
     * 
     */
    public void setFamilyId(int value) {
        this.familyId = value;
    }

    /**
     * Gets the value of the shoppingCart property.
     * 
     * @return
     *     possible object is
     *     {@link IfpShoppingCart }
     *     
     */
    public IfpShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    /**
     * Sets the value of the shoppingCart property.
     * 
     * @param value
     *     allowed object is
     *     {@link IfpShoppingCart }
     *     
     */
    public void setShoppingCart(IfpShoppingCart value) {
        this.shoppingCart = value;
    }

}
