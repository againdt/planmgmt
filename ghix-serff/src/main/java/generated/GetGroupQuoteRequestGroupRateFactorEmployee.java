
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Request.GroupRateFactor.Employee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Request.GroupRateFactor.Employee">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmployeeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OnCobra" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WorksiteZipCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="WorksiteCountyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IncludeInQuote" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="ProductChoices" type="{}ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee.ProductChoice" minOccurs="0"/>
 *         &lt;element name="Members" type="{}ArrayOfGetGroupQuote.Request.GroupRateFactor.Employee.Member"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Request.GroupRateFactor.Employee", propOrder = {
    "employeeId",
    "onCobra",
    "worksiteZipCode",
    "worksiteCountyName",
    "includeInQuote",
    "productChoices",
    "members"
})
public class GetGroupQuoteRequestGroupRateFactorEmployee {

    @XmlElement(name = "EmployeeId", required = true, nillable = true)
    protected String employeeId;
    @XmlElementRef(name = "OnCobra", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> onCobra;
    @XmlElementRef(name = "WorksiteZipCode", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> worksiteZipCode;
    @XmlElementRef(name = "WorksiteCountyName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> worksiteCountyName;
    @XmlElementRef(name = "IncludeInQuote", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> includeInQuote;
    @XmlElementRef(name = "ProductChoices", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice> productChoices;
    @XmlElement(name = "Members", required = true, nillable = true)
    protected ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember members;

    /**
     * Gets the value of the employeeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * Sets the value of the employeeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeId(String value) {
        this.employeeId = value;
    }

    /**
     * Gets the value of the onCobra property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getOnCobra() {
        return onCobra;
    }

    /**
     * Sets the value of the onCobra property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setOnCobra(JAXBElement<Boolean> value) {
        this.onCobra = value;
    }

    /**
     * Gets the value of the worksiteZipCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getWorksiteZipCode() {
        return worksiteZipCode;
    }

    /**
     * Sets the value of the worksiteZipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setWorksiteZipCode(JAXBElement<Integer> value) {
        this.worksiteZipCode = value;
    }

    /**
     * Gets the value of the worksiteCountyName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWorksiteCountyName() {
        return worksiteCountyName;
    }

    /**
     * Sets the value of the worksiteCountyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWorksiteCountyName(JAXBElement<String> value) {
        this.worksiteCountyName = value;
    }

    /**
     * Gets the value of the includeInQuote property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIncludeInQuote() {
        return includeInQuote;
    }

    /**
     * Sets the value of the includeInQuote property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIncludeInQuote(JAXBElement<Boolean> value) {
        this.includeInQuote = value;
    }

    /**
     * Gets the value of the productChoices property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice> getProductChoices() {
        return productChoices;
    }

    /**
     * Sets the value of the productChoices property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice }{@code >}
     *     
     */
    public void setProductChoices(JAXBElement<ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeProductChoice> value) {
        this.productChoices = value;
    }

    /**
     * Gets the value of the members property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember }
     *     
     */
    public ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember getMembers() {
        return members;
    }

    /**
     * Sets the value of the members property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember }
     *     
     */
    public void setMembers(ArrayOfGetGroupQuoteRequestGroupRateFactorEmployeeMember value) {
        this.members = value;
    }

}
