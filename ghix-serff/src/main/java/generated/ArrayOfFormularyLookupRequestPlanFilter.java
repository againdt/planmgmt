
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfFormularyLookup.Request.PlanFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfFormularyLookup.Request.PlanFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormularyLookup.Request.PlanFilter" type="{}FormularyLookup.Request.PlanFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfFormularyLookup.Request.PlanFilter", propOrder = {
    "formularyLookupRequestPlanFilter"
})
public class ArrayOfFormularyLookupRequestPlanFilter {

    @XmlElement(name = "FormularyLookup.Request.PlanFilter", nillable = true)
    protected List<FormularyLookupRequestPlanFilter> formularyLookupRequestPlanFilter;

    /**
     * Gets the value of the formularyLookupRequestPlanFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formularyLookupRequestPlanFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormularyLookupRequestPlanFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormularyLookupRequestPlanFilter }
     * 
     * 
     */
    public List<FormularyLookupRequestPlanFilter> getFormularyLookupRequestPlanFilter() {
        if (formularyLookupRequestPlanFilter == null) {
            formularyLookupRequestPlanFilter = new ArrayList<FormularyLookupRequestPlanFilter>();
        }
        return this.formularyLookupRequestPlanFilter;
    }

}
