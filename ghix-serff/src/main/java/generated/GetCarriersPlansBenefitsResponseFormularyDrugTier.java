
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarriersPlansBenefits.Response.Formulary.DrugTier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarriersPlansBenefits.Response.Formulary.DrugTier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TierLevel" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TierTypes" type="{}GetCarriersPlansBenefits.Response.Formulary.DrugTier.TierType" minOccurs="0"/>
 *         &lt;element name="CostShareVariances" type="{}ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier.CostShareVariance" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarriersPlansBenefits.Response.Formulary.DrugTier", propOrder = {
    "tierLevel",
    "tierTypes",
    "costShareVariances"
})
public class GetCarriersPlansBenefitsResponseFormularyDrugTier {

    @XmlElement(name = "TierLevel")
    protected Integer tierLevel;
    @XmlElementRef(name = "TierTypes", type = JAXBElement.class, required = false)
    protected JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTierTierType> tierTypes;
    @XmlElementRef(name = "CostShareVariances", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance> costShareVariances;

    /**
     * Gets the value of the tierLevel property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTierLevel() {
        return tierLevel;
    }

    /**
     * Sets the value of the tierLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTierLevel(Integer value) {
        this.tierLevel = value;
    }

    /**
     * Gets the value of the tierTypes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseFormularyDrugTierTierType }{@code >}
     *     
     */
    public JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTierTierType> getTierTypes() {
        return tierTypes;
    }

    /**
     * Sets the value of the tierTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsResponseFormularyDrugTierTierType }{@code >}
     *     
     */
    public void setTierTypes(JAXBElement<GetCarriersPlansBenefitsResponseFormularyDrugTierTierType> value) {
        this.tierTypes = value;
    }

    /**
     * Gets the value of the costShareVariances property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance> getCostShareVariances() {
        return costShareVariances;
    }

    /**
     * Sets the value of the costShareVariances property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance }{@code >}
     *     
     */
    public void setCostShareVariances(JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTierCostShareVariance> value) {
        this.costShareVariances = value;
    }

}
