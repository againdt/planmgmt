
package generated;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarriersPlansBenefits.Response.Carrier.Plan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarriersPlansBenefits.Response.Carrier.Plan">
 *   &lt;complexContent>
 *     &lt;extension base="{}PlanBase">
 *       &lt;sequence>
 *         &lt;element name="RxMatch" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="CoveredRxCUIs" type="{}RxCUIsBase" minOccurs="0"/>
 *         &lt;element name="PlanData" type="{}ArrayOfGetCarriersPlansBenefits.Response.Item" minOccurs="0"/>
 *         &lt;element name="PlanLinks" type="{}ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.PlanLink" minOccurs="0"/>
 *         &lt;element name="Benefits" type="{}ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarriersPlansBenefits.Response.Carrier.Plan", propOrder = {
    "rxMatch",
    "coveredRxCUIs",
    "planData",
    "planLinks",
    "benefits"
})
public class GetCarriersPlansBenefitsResponseCarrierPlan
    extends PlanBase
{

    @XmlElement(name = "RxMatch")
    protected BigDecimal rxMatch;
    @XmlElementRef(name = "CoveredRxCUIs", type = JAXBElement.class, required = false)
    protected JAXBElement<RxCUIsBase> coveredRxCUIs;
    @XmlElementRef(name = "PlanData", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> planData;
    @XmlElementRef(name = "PlanLinks", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink> planLinks;
    @XmlElementRef(name = "Benefits", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit> benefits;

    /**
     * Gets the value of the rxMatch property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRxMatch() {
        return rxMatch;
    }

    /**
     * Sets the value of the rxMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRxMatch(BigDecimal value) {
        this.rxMatch = value;
    }

    /**
     * Gets the value of the coveredRxCUIs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}
     *     
     */
    public JAXBElement<RxCUIsBase> getCoveredRxCUIs() {
        return coveredRxCUIs;
    }

    /**
     * Sets the value of the coveredRxCUIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}
     *     
     */
    public void setCoveredRxCUIs(JAXBElement<RxCUIsBase> value) {
        this.coveredRxCUIs = value;
    }

    /**
     * Gets the value of the planData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> getPlanData() {
        return planData;
    }

    /**
     * Sets the value of the planData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseItem }{@code >}
     *     
     */
    public void setPlanData(JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> value) {
        this.planData = value;
    }

    /**
     * Gets the value of the planLinks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink> getPlanLinks() {
        return planLinks;
    }

    /**
     * Sets the value of the planLinks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink }{@code >}
     *     
     */
    public void setPlanLinks(JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanPlanLink> value) {
        this.planLinks = value;
    }

    /**
     * Gets the value of the benefits property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit> getBenefits() {
        return benefits;
    }

    /**
     * Sets the value of the benefits property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit }{@code >}
     *     
     */
    public void setBenefits(JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit> value) {
        this.benefits = value;
    }

}
