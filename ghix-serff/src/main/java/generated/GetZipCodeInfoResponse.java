
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetZipCodeInfo.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetZipCodeInfo.Response">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ZipCodes" type="{}ArrayOfGetZipCodeInfo.Response.ZipCodeInfo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetZipCodeInfo.Response", propOrder = {
    "zipCodes"
})
public class GetZipCodeInfoResponse {

    @XmlElementRef(name = "ZipCodes", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfo> zipCodes;

    /**
     * Gets the value of the zipCodes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetZipCodeInfoResponseZipCodeInfo }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfo> getZipCodes() {
        return zipCodes;
    }

    /**
     * Sets the value of the zipCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetZipCodeInfoResponseZipCodeInfo }{@code >}
     *     
     */
    public void setZipCodes(JAXBElement<ArrayOfGetZipCodeInfoResponseZipCodeInfo> value) {
        this.zipCodes = value;
    }

}
