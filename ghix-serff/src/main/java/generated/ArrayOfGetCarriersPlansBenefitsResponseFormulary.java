
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.Formulary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.Formulary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.Formulary" type="{}GetCarriersPlansBenefits.Response.Formulary" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.Formulary", propOrder = {
    "getCarriersPlansBenefitsResponseFormulary"
})
public class ArrayOfGetCarriersPlansBenefitsResponseFormulary {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.Formulary", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseFormulary> getCarriersPlansBenefitsResponseFormulary;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseFormulary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseFormulary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseFormulary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseFormulary }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseFormulary> getGetCarriersPlansBenefitsResponseFormulary() {
        if (getCarriersPlansBenefitsResponseFormulary == null) {
            getCarriersPlansBenefitsResponseFormulary = new ArrayList<GetCarriersPlansBenefitsResponseFormulary>();
        }
        return this.getCarriersPlansBenefitsResponseFormulary;
    }

}
