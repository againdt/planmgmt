
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter" type="{}GetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter", propOrder = {
    "getGroupQuoteRequestPreferenceBenefitFilterAttributeFilter"
})
public class ArrayOfGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter {

    @XmlElement(name = "GetGroupQuote.Request.Preference.BenefitFilter.AttributeFilter", nillable = true)
    protected List<GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter> getGroupQuoteRequestPreferenceBenefitFilterAttributeFilter;

    /**
     * Gets the value of the getGroupQuoteRequestPreferenceBenefitFilterAttributeFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteRequestPreferenceBenefitFilterAttributeFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter }
     * 
     * 
     */
    public List<GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter> getGetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter() {
        if (getGroupQuoteRequestPreferenceBenefitFilterAttributeFilter == null) {
            getGroupQuoteRequestPreferenceBenefitFilterAttributeFilter = new ArrayList<GetGroupQuoteRequestPreferenceBenefitFilterAttributeFilter>();
        }
        return this.getGroupQuoteRequestPreferenceBenefitFilterAttributeFilter;
    }

}
