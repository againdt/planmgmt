
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response.Quote.MemberPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response.Quote.MemberPlan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Carriers" type="{}ArrayOfGetIfpQuote.Response.Quote.Carrier" minOccurs="0"/>
 *         &lt;element name="DroppedRates" type="{}ArrayOfGetIfpQuote.Response.Quote.DroppedCarrier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response.Quote.MemberPlan", propOrder = {
    "memberId",
    "carriers",
    "droppedRates"
})
public class GetIfpQuoteResponseQuoteMemberPlan {

    @XmlElement(name = "MemberId", required = true, nillable = true)
    protected String memberId;
    @XmlElementRef(name = "Carriers", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrier> carriers;
    @XmlElementRef(name = "DroppedRates", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier> droppedRates;

    /**
     * Gets the value of the memberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * Sets the value of the memberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberId(String value) {
        this.memberId = value;
    }

    /**
     * Gets the value of the carriers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrier }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrier> getCarriers() {
        return carriers;
    }

    /**
     * Sets the value of the carriers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteCarrier }{@code >}
     *     
     */
    public void setCarriers(JAXBElement<ArrayOfGetIfpQuoteResponseQuoteCarrier> value) {
        this.carriers = value;
    }

    /**
     * Gets the value of the droppedRates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier> getDroppedRates() {
        return droppedRates;
    }

    /**
     * Sets the value of the droppedRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier }{@code >}
     *     
     */
    public void setDroppedRates(JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrier> value) {
        this.droppedRates = value;
    }

}
