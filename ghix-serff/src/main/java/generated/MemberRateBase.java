
package generated;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MemberRateBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MemberRateBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Age" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="Eligibility" type="{}Eligibility"/>
 *         &lt;element name="SRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SmokerSurcharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="RatingSortOrder" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MemberRateBase", propOrder = {
    "memberId",
    "age",
    "eligibility",
    "sRate",
    "smokerSurcharge",
    "ratingSortOrder"
})
@XmlSeeAlso({
    GetGroupQuoteResponseMemberRate.class,
    GetIfpQuoteResponseQuoteCarrierRateMemberRate.class
})
public class MemberRateBase {

    @XmlElement(name = "MemberId", required = true, nillable = true)
    protected String memberId;
    @XmlElement(name = "Age", required = true)
    protected BigDecimal age;
    @XmlElement(name = "Eligibility", required = true)
    protected Eligibility eligibility;
    @XmlElement(name = "SRate")
    protected BigDecimal sRate;
    @XmlElement(name = "SmokerSurcharge")
    protected BigDecimal smokerSurcharge;
    @XmlElement(name = "RatingSortOrder")
    @XmlSchemaType(name = "unsignedByte")
    protected short ratingSortOrder;

    /**
     * Gets the value of the memberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * Sets the value of the memberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberId(String value) {
        this.memberId = value;
    }

    /**
     * Gets the value of the age property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAge() {
        return age;
    }

    /**
     * Sets the value of the age property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAge(BigDecimal value) {
        this.age = value;
    }

    /**
     * Gets the value of the eligibility property.
     * 
     * @return
     *     possible object is
     *     {@link Eligibility }
     *     
     */
    public Eligibility getEligibility() {
        return eligibility;
    }

    /**
     * Sets the value of the eligibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link Eligibility }
     *     
     */
    public void setEligibility(Eligibility value) {
        this.eligibility = value;
    }

    /**
     * Gets the value of the sRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRate() {
        return sRate;
    }

    /**
     * Sets the value of the sRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRate(BigDecimal value) {
        this.sRate = value;
    }

    /**
     * Gets the value of the smokerSurcharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSmokerSurcharge() {
        return smokerSurcharge;
    }

    /**
     * Sets the value of the smokerSurcharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSmokerSurcharge(BigDecimal value) {
        this.smokerSurcharge = value;
    }

    /**
     * Gets the value of the ratingSortOrder property.
     * 
     */
    public short getRatingSortOrder() {
        return ratingSortOrder;
    }

    /**
     * Sets the value of the ratingSortOrder property.
     * 
     */
    public void setRatingSortOrder(short value) {
        this.ratingSortOrder = value;
    }

}
