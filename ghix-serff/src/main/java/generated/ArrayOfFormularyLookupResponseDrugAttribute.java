
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfFormularyLookup.Response.DrugAttribute complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfFormularyLookup.Response.DrugAttribute">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormularyLookup.Response.DrugAttribute" type="{}FormularyLookup.Response.DrugAttribute" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfFormularyLookup.Response.DrugAttribute", propOrder = {
    "formularyLookupResponseDrugAttribute"
})
public class ArrayOfFormularyLookupResponseDrugAttribute {

    @XmlElement(name = "FormularyLookup.Response.DrugAttribute", nillable = true)
    protected List<FormularyLookupResponseDrugAttribute> formularyLookupResponseDrugAttribute;

    /**
     * Gets the value of the formularyLookupResponseDrugAttribute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formularyLookupResponseDrugAttribute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormularyLookupResponseDrugAttribute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormularyLookupResponseDrugAttribute }
     * 
     * 
     */
    public List<FormularyLookupResponseDrugAttribute> getFormularyLookupResponseDrugAttribute() {
        if (formularyLookupResponseDrugAttribute == null) {
            formularyLookupResponseDrugAttribute = new ArrayList<FormularyLookupResponseDrugAttribute>();
        }
        return this.formularyLookupResponseDrugAttribute;
    }

}
