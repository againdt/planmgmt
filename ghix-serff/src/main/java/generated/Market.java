
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Market.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Market">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SHOP"/>
 *     &lt;enumeration value="IFP"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Market")
@XmlEnum
public enum Market {

    SHOP,
    IFP;

    public String value() {
        return name();
    }

    public static Market fromValue(String v) {
        return valueOf(v);
    }

}
