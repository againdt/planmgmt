
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FormularyLookup.SortBy.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FormularyLookup.SortBy">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Description"/>
 *     &lt;enumeration value="Code"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FormularyLookup.SortBy")
@XmlEnum
public enum FormularyLookupSortBy {

    @XmlEnumValue("Description")
    DESCRIPTION("Description"),
    @XmlEnumValue("Code")
    CODE("Code");
    private final String value;

    FormularyLookupSortBy(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FormularyLookupSortBy fromValue(String v) {
        for (FormularyLookupSortBy c: FormularyLookupSortBy.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
