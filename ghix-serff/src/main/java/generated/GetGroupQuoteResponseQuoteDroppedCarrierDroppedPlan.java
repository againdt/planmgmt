
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response.Quote.DroppedCarrier.DroppedPlan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response.Quote.DroppedCarrier.DroppedPlan">
 *   &lt;complexContent>
 *     &lt;extension base="{}DroppedPlanBase">
 *       &lt;sequence>
 *         &lt;element name="Visibility" type="{}PlanVisibility"/>
 *         &lt;element name="RatingMethod" type="{}RatingMethod"/>
 *         &lt;element name="DroppedReason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PlanDetails" type="{}GetGroupQuote.Response.PlanDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response.Quote.DroppedCarrier.DroppedPlan", propOrder = {
    "visibility",
    "ratingMethod",
    "droppedReason",
    "planDetails"
})
public class GetGroupQuoteResponseQuoteDroppedCarrierDroppedPlan
    extends DroppedPlanBase
{

    @XmlElement(name = "Visibility", required = true)
    protected PlanVisibility visibility;
    @XmlElement(name = "RatingMethod", required = true)
    protected RatingMethod ratingMethod;
    @XmlElement(name = "DroppedReason", required = true, nillable = true)
    protected String droppedReason;
    @XmlElementRef(name = "PlanDetails", type = JAXBElement.class, required = false)
    protected JAXBElement<GetGroupQuoteResponsePlanDetail> planDetails;

    /**
     * Gets the value of the visibility property.
     * 
     * @return
     *     possible object is
     *     {@link PlanVisibility }
     *     
     */
    public PlanVisibility getVisibility() {
        return visibility;
    }

    /**
     * Sets the value of the visibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanVisibility }
     *     
     */
    public void setVisibility(PlanVisibility value) {
        this.visibility = value;
    }

    /**
     * Gets the value of the ratingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link RatingMethod }
     *     
     */
    public RatingMethod getRatingMethod() {
        return ratingMethod;
    }

    /**
     * Sets the value of the ratingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link RatingMethod }
     *     
     */
    public void setRatingMethod(RatingMethod value) {
        this.ratingMethod = value;
    }

    /**
     * Gets the value of the droppedReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDroppedReason() {
        return droppedReason;
    }

    /**
     * Sets the value of the droppedReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDroppedReason(String value) {
        this.droppedReason = value;
    }

    /**
     * Gets the value of the planDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetail }{@code >}
     *     
     */
    public JAXBElement<GetGroupQuoteResponsePlanDetail> getPlanDetails() {
        return planDetails;
    }

    /**
     * Sets the value of the planDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteResponsePlanDetail }{@code >}
     *     
     */
    public void setPlanDetails(JAXBElement<GetGroupQuoteResponsePlanDetail> value) {
        this.planDetails = value;
    }

}
