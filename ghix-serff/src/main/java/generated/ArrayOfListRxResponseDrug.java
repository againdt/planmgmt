
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfListRx.Response.Drug complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfListRx.Response.Drug">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ListRx.Response.Drug" type="{}ListRx.Response.Drug" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfListRx.Response.Drug", propOrder = {
    "listRxResponseDrug"
})
public class ArrayOfListRxResponseDrug {

    @XmlElement(name = "ListRx.Response.Drug", nillable = true)
    protected List<ListRxResponseDrug> listRxResponseDrug;

    /**
     * Gets the value of the listRxResponseDrug property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listRxResponseDrug property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListRxResponseDrug().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListRxResponseDrug }
     * 
     * 
     */
    public List<ListRxResponseDrug> getListRxResponseDrug() {
        if (listRxResponseDrug == null) {
            listRxResponseDrug = new ArrayList<ListRxResponseDrug>();
        }
        return this.listRxResponseDrug;
    }

}
