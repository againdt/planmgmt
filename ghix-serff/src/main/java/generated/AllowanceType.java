
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AllowanceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AllowanceType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ToRateAsSpouse"/>
 *     &lt;enumeration value="ForChildrenOnly"/>
 *     &lt;enumeration value="ForSubscriber"/>
 *     &lt;enumeration value="ForDependents"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AllowanceType")
@XmlEnum
public enum AllowanceType {

    @XmlEnumValue("ToRateAsSpouse")
    TO_RATE_AS_SPOUSE("ToRateAsSpouse"),
    @XmlEnumValue("ForChildrenOnly")
    FOR_CHILDREN_ONLY("ForChildrenOnly"),
    @XmlEnumValue("ForSubscriber")
    FOR_SUBSCRIBER("ForSubscriber"),
    @XmlEnumValue("ForDependents")
    FOR_DEPENDENTS("ForDependents");
    private final String value;

    AllowanceType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AllowanceType fromValue(String v) {
        for (AllowanceType c: AllowanceType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
