
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCustomProduct.Plan.BusinessRule.AllowedRelation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomProduct.Plan.BusinessRule.AllowedRelation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomProduct.Plan.BusinessRule.AllowedRelation" type="{}CustomProduct.Plan.BusinessRule.AllowedRelation" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomProduct.Plan.BusinessRule.AllowedRelation", propOrder = {
    "customProductPlanBusinessRuleAllowedRelation"
})
public class ArrayOfCustomProductPlanBusinessRuleAllowedRelation {

    @XmlElement(name = "CustomProduct.Plan.BusinessRule.AllowedRelation", nillable = true)
    protected List<CustomProductPlanBusinessRuleAllowedRelation> customProductPlanBusinessRuleAllowedRelation;

    /**
     * Gets the value of the customProductPlanBusinessRuleAllowedRelation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customProductPlanBusinessRuleAllowedRelation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomProductPlanBusinessRuleAllowedRelation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomProductPlanBusinessRuleAllowedRelation }
     * 
     * 
     */
    public List<CustomProductPlanBusinessRuleAllowedRelation> getCustomProductPlanBusinessRuleAllowedRelation() {
        if (customProductPlanBusinessRuleAllowedRelation == null) {
            customProductPlanBusinessRuleAllowedRelation = new ArrayList<CustomProductPlanBusinessRuleAllowedRelation>();
        }
        return this.customProductPlanBusinessRuleAllowedRelation;
    }

}
