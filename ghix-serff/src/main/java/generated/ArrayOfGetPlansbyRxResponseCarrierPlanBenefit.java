
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPlansbyRx.Response.Carrier.Plan.Benefit" type="{}GetPlansbyRx.Response.Carrier.Plan.Benefit" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan.Benefit", propOrder = {
    "getPlansbyRxResponseCarrierPlanBenefit"
})
public class ArrayOfGetPlansbyRxResponseCarrierPlanBenefit {

    @XmlElement(name = "GetPlansbyRx.Response.Carrier.Plan.Benefit", nillable = true)
    protected List<GetPlansbyRxResponseCarrierPlanBenefit> getPlansbyRxResponseCarrierPlanBenefit;

    /**
     * Gets the value of the getPlansbyRxResponseCarrierPlanBenefit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getPlansbyRxResponseCarrierPlanBenefit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetPlansbyRxResponseCarrierPlanBenefit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetPlansbyRxResponseCarrierPlanBenefit }
     * 
     * 
     */
    public List<GetPlansbyRxResponseCarrierPlanBenefit> getGetPlansbyRxResponseCarrierPlanBenefit() {
        if (getPlansbyRxResponseCarrierPlanBenefit == null) {
            getPlansbyRxResponseCarrierPlanBenefit = new ArrayList<GetPlansbyRxResponseCarrierPlanBenefit>();
        }
        return this.getPlansbyRxResponseCarrierPlanBenefit;
    }

}
