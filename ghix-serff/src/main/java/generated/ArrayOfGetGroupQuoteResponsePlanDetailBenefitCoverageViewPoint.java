
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint" type="{}GetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint", propOrder = {
    "getGroupQuoteResponsePlanDetailBenefitCoverageViewPoint"
})
public class ArrayOfGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint {

    @XmlElement(name = "GetGroupQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint", nillable = true)
    protected List<GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint> getGroupQuoteResponsePlanDetailBenefitCoverageViewPoint;

    /**
     * Gets the value of the getGroupQuoteResponsePlanDetailBenefitCoverageViewPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getGroupQuoteResponsePlanDetailBenefitCoverageViewPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint }
     * 
     * 
     */
    public List<GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint> getGetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint() {
        if (getGroupQuoteResponsePlanDetailBenefitCoverageViewPoint == null) {
            getGroupQuoteResponsePlanDetailBenefitCoverageViewPoint = new ArrayList<GetGroupQuoteResponsePlanDetailBenefitCoverageViewPoint>();
        }
        return this.getGroupQuoteResponsePlanDetailBenefitCoverageViewPoint;
    }

}
