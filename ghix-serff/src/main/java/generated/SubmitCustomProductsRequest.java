
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmitCustomProducts.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitCustomProducts.Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccessKeys" type="{}SubmitCustomProducts.Request.AccessKey"/>
 *         &lt;element name="Settings" type="{}SubmitCustomProducts.Request.Setting"/>
 *         &lt;element name="CustomProducts" type="{}ArrayOfCustomProduct" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitCustomProducts.Request", propOrder = {
    "accessKeys",
    "settings",
    "customProducts"
})
public class SubmitCustomProductsRequest {

    @XmlElement(name = "AccessKeys", required = true, nillable = true)
    protected SubmitCustomProductsRequestAccessKey accessKeys;
    @XmlElement(name = "Settings", required = true, nillable = true)
    protected SubmitCustomProductsRequestSetting settings;
    @XmlElementRef(name = "CustomProducts", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomProduct> customProducts;

    /**
     * Gets the value of the accessKeys property.
     * 
     * @return
     *     possible object is
     *     {@link SubmitCustomProductsRequestAccessKey }
     *     
     */
    public SubmitCustomProductsRequestAccessKey getAccessKeys() {
        return accessKeys;
    }

    /**
     * Sets the value of the accessKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmitCustomProductsRequestAccessKey }
     *     
     */
    public void setAccessKeys(SubmitCustomProductsRequestAccessKey value) {
        this.accessKeys = value;
    }

    /**
     * Gets the value of the settings property.
     * 
     * @return
     *     possible object is
     *     {@link SubmitCustomProductsRequestSetting }
     *     
     */
    public SubmitCustomProductsRequestSetting getSettings() {
        return settings;
    }

    /**
     * Sets the value of the settings property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubmitCustomProductsRequestSetting }
     *     
     */
    public void setSettings(SubmitCustomProductsRequestSetting value) {
        this.settings = value;
    }

    /**
     * Gets the value of the customProducts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProduct }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomProduct> getCustomProducts() {
        return customProducts;
    }

    /**
     * Sets the value of the customProducts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProduct }{@code >}
     *     
     */
    public void setCustomProducts(JAXBElement<ArrayOfCustomProduct> value) {
        this.customProducts = value;
    }

}
