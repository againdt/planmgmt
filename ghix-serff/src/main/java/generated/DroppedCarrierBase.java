
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DroppedCarrierBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DroppedCarrierBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IssuerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CarrierId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DroppedReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DroppedCarrierBase", propOrder = {
    "issuerId",
    "carrierId",
    "droppedReason"
})
@XmlSeeAlso({
    GetGroupQuoteResponseQuoteDroppedCarrier.class,
    GetIfpQuoteResponseQuoteDroppedCarrier.class
})
public class DroppedCarrierBase {

    @XmlElementRef(name = "IssuerId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> issuerId;
    @XmlElement(name = "CarrierId")
    protected int carrierId;
    @XmlElementRef(name = "DroppedReason", type = JAXBElement.class, required = false)
    protected JAXBElement<String> droppedReason;

    /**
     * Gets the value of the issuerId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIssuerId() {
        return issuerId;
    }

    /**
     * Sets the value of the issuerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIssuerId(JAXBElement<String> value) {
        this.issuerId = value;
    }

    /**
     * Gets the value of the carrierId property.
     * 
     */
    public int getCarrierId() {
        return carrierId;
    }

    /**
     * Sets the value of the carrierId property.
     * 
     */
    public void setCarrierId(int value) {
        this.carrierId = value;
    }

    /**
     * Gets the value of the droppedReason property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDroppedReason() {
        return droppedReason;
    }

    /**
     * Sets the value of the droppedReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDroppedReason(JAXBElement<String> value) {
        this.droppedReason = value;
    }

}
