
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPlansbyRx.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPlansbyRx.Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccessKeys" type="{}GetPlansbyRx.Request.AccessKey"/>
 *         &lt;element name="Inputs" type="{}GetPlansbyRx.Request.Input"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPlansbyRx.Request", propOrder = {
    "accessKeys",
    "inputs"
})
public class GetPlansbyRxRequest {

    @XmlElement(name = "AccessKeys", required = true, nillable = true)
    protected GetPlansbyRxRequestAccessKey accessKeys;
    @XmlElement(name = "Inputs", required = true, nillable = true)
    protected GetPlansbyRxRequestInput inputs;

    /**
     * Gets the value of the accessKeys property.
     * 
     * @return
     *     possible object is
     *     {@link GetPlansbyRxRequestAccessKey }
     *     
     */
    public GetPlansbyRxRequestAccessKey getAccessKeys() {
        return accessKeys;
    }

    /**
     * Sets the value of the accessKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPlansbyRxRequestAccessKey }
     *     
     */
    public void setAccessKeys(GetPlansbyRxRequestAccessKey value) {
        this.accessKeys = value;
    }

    /**
     * Gets the value of the inputs property.
     * 
     * @return
     *     possible object is
     *     {@link GetPlansbyRxRequestInput }
     *     
     */
    public GetPlansbyRxRequestInput getInputs() {
        return inputs;
    }

    /**
     * Sets the value of the inputs property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetPlansbyRxRequestInput }
     *     
     */
    public void setInputs(GetPlansbyRxRequestInput value) {
        this.inputs = value;
    }

}
