
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ValidationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ValidationType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Format"/>
 *     &lt;enumeration value="Length"/>
 *     &lt;enumeration value="Limit"/>
 *     &lt;enumeration value="Range"/>
 *     &lt;enumeration value="Uniqueness"/>
 *     &lt;enumeration value="Integrity"/>
 *     &lt;enumeration value="Other"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ValidationType")
@XmlEnum
public enum ValidationType {

    @XmlEnumValue("Format")
    FORMAT("Format"),
    @XmlEnumValue("Length")
    LENGTH("Length"),
    @XmlEnumValue("Limit")
    LIMIT("Limit"),
    @XmlEnumValue("Range")
    RANGE("Range"),
    @XmlEnumValue("Uniqueness")
    UNIQUENESS("Uniqueness"),
    @XmlEnumValue("Integrity")
    INTEGRITY("Integrity"),
    @XmlEnumValue("Other")
    OTHER("Other");
    private final String value;

    ValidationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ValidationType fromValue(String v) {
        for (ValidationType c: ValidationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
