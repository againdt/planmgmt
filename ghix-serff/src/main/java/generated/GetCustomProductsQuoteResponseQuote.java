
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCustomProductsQuote.Response.Quote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomProductsQuote.Response.Quote">
 *   &lt;complexContent>
 *     &lt;extension base="{}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="FamilyQuotes" type="{}ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomProductsQuote.Response.Quote", propOrder = {
    "familyQuotes"
})
public class GetCustomProductsQuoteResponseQuote
    extends ResponseBase
{

    @XmlElement(name = "FamilyQuotes", required = true, nillable = true)
    protected ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote familyQuotes;

    /**
     * Gets the value of the familyQuotes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote }
     *     
     */
    public ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote getFamilyQuotes() {
        return familyQuotes;
    }

    /**
     * Sets the value of the familyQuotes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote }
     *     
     */
    public void setFamilyQuotes(ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuote value) {
        this.familyQuotes = value;
    }

}
