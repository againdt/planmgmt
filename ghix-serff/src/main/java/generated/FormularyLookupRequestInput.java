
package generated;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for FormularyLookup.Request.Input complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FormularyLookup.Request.Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DrugFilters" type="{}FormularyLookup.Request.Drug" minOccurs="0"/>
 *         &lt;element name="PlanFilters" type="{}ArrayOfFormularyLookup.Request.PlanFilter" minOccurs="0"/>
 *         &lt;element name="RxMatch" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SortBy" type="{}FormularyLookup.SortBy" minOccurs="0"/>
 *         &lt;element name="SortDir" type="{}PlanSortDir" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FormularyLookup.Request.Input", propOrder = {
    "effectiveDate",
    "drugFilters",
    "planFilters",
    "rxMatch",
    "sortBy",
    "sortDir"
})
public class FormularyLookupRequestInput {

    @XmlElement(name = "EffectiveDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElementRef(name = "DrugFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<FormularyLookupRequestDrug> drugFilters;
    @XmlElementRef(name = "PlanFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfFormularyLookupRequestPlanFilter> planFilters;
    @XmlElement(name = "RxMatch")
    protected BigDecimal rxMatch;
    @XmlElement(name = "SortBy")
    protected FormularyLookupSortBy sortBy;
    @XmlList
    @XmlElement(name = "SortDir")
    protected List<String> sortDir;

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the drugFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FormularyLookupRequestDrug }{@code >}
     *     
     */
    public JAXBElement<FormularyLookupRequestDrug> getDrugFilters() {
        return drugFilters;
    }

    /**
     * Sets the value of the drugFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FormularyLookupRequestDrug }{@code >}
     *     
     */
    public void setDrugFilters(JAXBElement<FormularyLookupRequestDrug> value) {
        this.drugFilters = value;
    }

    /**
     * Gets the value of the planFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupRequestPlanFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFormularyLookupRequestPlanFilter> getPlanFilters() {
        return planFilters;
    }

    /**
     * Sets the value of the planFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupRequestPlanFilter }{@code >}
     *     
     */
    public void setPlanFilters(JAXBElement<ArrayOfFormularyLookupRequestPlanFilter> value) {
        this.planFilters = value;
    }

    /**
     * Gets the value of the rxMatch property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRxMatch() {
        return rxMatch;
    }

    /**
     * Sets the value of the rxMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRxMatch(BigDecimal value) {
        this.rxMatch = value;
    }

    /**
     * Gets the value of the sortBy property.
     * 
     * @return
     *     possible object is
     *     {@link FormularyLookupSortBy }
     *     
     */
    public FormularyLookupSortBy getSortBy() {
        return sortBy;
    }

    /**
     * Sets the value of the sortBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormularyLookupSortBy }
     *     
     */
    public void setSortBy(FormularyLookupSortBy value) {
        this.sortBy = value;
    }

    /**
     * Gets the value of the sortDir property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sortDir property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSortDir().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSortDir() {
        if (sortDir == null) {
            sortDir = new ArrayList<String>();
        }
        return this.sortDir;
    }

}
