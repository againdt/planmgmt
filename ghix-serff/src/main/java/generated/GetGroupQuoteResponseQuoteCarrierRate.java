
package generated;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Response.Quote.CarrierRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Response.Quote.CarrierRate">
 *   &lt;complexContent>
 *     &lt;extension base="{}CarrierRateBase">
 *       &lt;sequence>
 *         &lt;element name="CarrierRAF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CarrierDetails" type="{}GetGroupQuote.Response.CarrierDetail" minOccurs="0"/>
 *         &lt;element name="PlanRates" type="{}ArrayOfGetGroupQuote.Response.Quote.CarrierRate.PlanRate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Response.Quote.CarrierRate", propOrder = {
    "carrierRAF",
    "version",
    "carrierDetails",
    "planRates"
})
public class GetGroupQuoteResponseQuoteCarrierRate
    extends CarrierRateBase
{

    @XmlElement(name = "CarrierRAF")
    protected BigDecimal carrierRAF;
    @XmlElement(name = "Version", required = true, nillable = true)
    protected String version;
    @XmlElementRef(name = "CarrierDetails", type = JAXBElement.class, required = false)
    protected JAXBElement<GetGroupQuoteResponseCarrierDetail> carrierDetails;
    @XmlElementRef(name = "PlanRates", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate> planRates;

    /**
     * Gets the value of the carrierRAF property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCarrierRAF() {
        return carrierRAF;
    }

    /**
     * Sets the value of the carrierRAF property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCarrierRAF(BigDecimal value) {
        this.carrierRAF = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the carrierDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseCarrierDetail }{@code >}
     *     
     */
    public JAXBElement<GetGroupQuoteResponseCarrierDetail> getCarrierDetails() {
        return carrierDetails;
    }

    /**
     * Sets the value of the carrierDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetGroupQuoteResponseCarrierDetail }{@code >}
     *     
     */
    public void setCarrierDetails(JAXBElement<GetGroupQuoteResponseCarrierDetail> value) {
        this.carrierDetails = value;
    }

    /**
     * Gets the value of the planRates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate> getPlanRates() {
        return planRates;
    }

    /**
     * Sets the value of the planRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate }{@code >}
     *     
     */
    public void setPlanRates(JAXBElement<ArrayOfGetGroupQuoteResponseQuoteCarrierRatePlanRate> value) {
        this.planRates = value;
    }

}
