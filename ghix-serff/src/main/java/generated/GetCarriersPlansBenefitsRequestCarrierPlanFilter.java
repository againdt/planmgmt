
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarriersPlansBenefits.Request.CarrierPlanFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarriersPlansBenefits.Request.CarrierPlanFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CompanyId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InsuranceType" type="{}InsuranceType" minOccurs="0"/>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanVisibility" type="{}GetCarriersPlansBenefits.PlanVisibility" minOccurs="0"/>
 *         &lt;element name="PlanType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BenefitLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IssuerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CarrierId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PlanId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarriersPlansBenefits.Request.CarrierPlanFilter", propOrder = {
    "companyId",
    "insuranceType",
    "state",
    "planVisibility",
    "planType",
    "benefitLevel",
    "issuerId",
    "carrierId",
    "planId"
})
public class GetCarriersPlansBenefitsRequestCarrierPlanFilter {

    @XmlElementRef(name = "CompanyId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> companyId;
    @XmlElementRef(name = "InsuranceType", type = JAXBElement.class, required = false)
    protected JAXBElement<InsuranceType> insuranceType;
    @XmlElementRef(name = "State", type = JAXBElement.class, required = false)
    protected JAXBElement<String> state;
    @XmlElementRef(name = "PlanVisibility", type = JAXBElement.class, required = false)
    protected JAXBElement<GetCarriersPlansBenefitsPlanVisibility> planVisibility;
    @XmlElementRef(name = "PlanType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planType;
    @XmlElementRef(name = "BenefitLevel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> benefitLevel;
    @XmlElementRef(name = "IssuerId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> issuerId;
    @XmlElementRef(name = "CarrierId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> carrierId;
    @XmlElementRef(name = "PlanId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planId;

    /**
     * Gets the value of the companyId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCompanyId() {
        return companyId;
    }

    /**
     * Sets the value of the companyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCompanyId(JAXBElement<Integer> value) {
        this.companyId = value;
    }

    /**
     * Gets the value of the insuranceType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link InsuranceType }{@code >}
     *     
     */
    public JAXBElement<InsuranceType> getInsuranceType() {
        return insuranceType;
    }

    /**
     * Sets the value of the insuranceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link InsuranceType }{@code >}
     *     
     */
    public void setInsuranceType(JAXBElement<InsuranceType> value) {
        this.insuranceType = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setState(JAXBElement<String> value) {
        this.state = value;
    }

    /**
     * Gets the value of the planVisibility property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsPlanVisibility }{@code >}
     *     
     */
    public JAXBElement<GetCarriersPlansBenefitsPlanVisibility> getPlanVisibility() {
        return planVisibility;
    }

    /**
     * Sets the value of the planVisibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsPlanVisibility }{@code >}
     *     
     */
    public void setPlanVisibility(JAXBElement<GetCarriersPlansBenefitsPlanVisibility> value) {
        this.planVisibility = value;
    }

    /**
     * Gets the value of the planType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanType() {
        return planType;
    }

    /**
     * Sets the value of the planType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanType(JAXBElement<String> value) {
        this.planType = value;
    }

    /**
     * Gets the value of the benefitLevel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBenefitLevel() {
        return benefitLevel;
    }

    /**
     * Sets the value of the benefitLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBenefitLevel(JAXBElement<String> value) {
        this.benefitLevel = value;
    }

    /**
     * Gets the value of the issuerId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIssuerId() {
        return issuerId;
    }

    /**
     * Sets the value of the issuerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIssuerId(JAXBElement<String> value) {
        this.issuerId = value;
    }

    /**
     * Gets the value of the carrierId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCarrierId() {
        return carrierId;
    }

    /**
     * Sets the value of the carrierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCarrierId(JAXBElement<Integer> value) {
        this.carrierId = value;
    }

    /**
     * Gets the value of the planId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanId() {
        return planId;
    }

    /**
     * Sets the value of the planId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanId(JAXBElement<String> value) {
        this.planId = value;
    }

}
