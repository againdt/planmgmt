
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCustomProductsQuote.Request.Preference.ProductFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCustomProductsQuote.Request.Preference.ProductFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCustomProductsQuote.Request.Preference.ProductFilter" type="{}GetCustomProductsQuote.Request.Preference.ProductFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCustomProductsQuote.Request.Preference.ProductFilter", propOrder = {
    "getCustomProductsQuoteRequestPreferenceProductFilter"
})
public class ArrayOfGetCustomProductsQuoteRequestPreferenceProductFilter {

    @XmlElement(name = "GetCustomProductsQuote.Request.Preference.ProductFilter", nillable = true)
    protected List<GetCustomProductsQuoteRequestPreferenceProductFilter> getCustomProductsQuoteRequestPreferenceProductFilter;

    /**
     * Gets the value of the getCustomProductsQuoteRequestPreferenceProductFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCustomProductsQuoteRequestPreferenceProductFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCustomProductsQuoteRequestPreferenceProductFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCustomProductsQuoteRequestPreferenceProductFilter }
     * 
     * 
     */
    public List<GetCustomProductsQuoteRequestPreferenceProductFilter> getGetCustomProductsQuoteRequestPreferenceProductFilter() {
        if (getCustomProductsQuoteRequestPreferenceProductFilter == null) {
            getCustomProductsQuoteRequestPreferenceProductFilter = new ArrayList<GetCustomProductsQuoteRequestPreferenceProductFilter>();
        }
        return this.getCustomProductsQuoteRequestPreferenceProductFilter;
    }

}
