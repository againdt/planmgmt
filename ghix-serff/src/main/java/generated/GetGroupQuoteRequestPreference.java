
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GetGroupQuote.Request.Preference complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Request.Preference">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InsuranceTypes" type="{}ArrayOfInsuranceType"/>
 *         &lt;element name="PlanVisibility" type="{}GetGroupQuote.Request.PlanVisibility"/>
 *         &lt;element name="PlanEligibility" type="{}GroupPlanEligibility" minOccurs="0"/>
 *         &lt;element name="QuoteFormat" type="{}GroupQuoteFormat"/>
 *         &lt;element name="QuoteState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RateUnit" type="{}RateUnit"/>
 *         &lt;element name="RateSet" type="{}RateSet" minOccurs="0"/>
 *         &lt;element name="AsOfDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ProductChoicesOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="CalculateERContributions" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AddOns" type="{}ArrayOfGetGroupQuote.AddOn" minOccurs="0"/>
 *         &lt;element name="LevelOfBenefitAddOns" type="{}LevelOfBenefitAddOns" minOccurs="0"/>
 *         &lt;element name="BenefitTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VersionFilters" type="{}ArrayOfGetGroupQuote.Request.Preference.VersionFilter" minOccurs="0"/>
 *         &lt;element name="DrugFilters" type="{}ArrayOfGetGroupQuote.Request.Preference.DrugFilter" minOccurs="0"/>
 *         &lt;element name="ChoiceFilters" type="{}ArrayOfGetGroupQuote.Request.Preference.ChoiceFilter" minOccurs="0"/>
 *         &lt;element name="PlanFilters" type="{}ArrayOfGetGroupQuote.Request.Preference.PlanFilter" minOccurs="0"/>
 *         &lt;element name="BenefitFilters" type="{}ArrayOfGetGroupQuote.Request.Preference.BenefitFilter" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Request.Preference", propOrder = {
    "insuranceTypes",
    "planVisibility",
    "planEligibility",
    "quoteFormat",
    "quoteState",
    "rateUnit",
    "rateSet",
    "asOfDate",
    "productChoicesOnly",
    "calculateERContributions",
    "addOns",
    "levelOfBenefitAddOns",
    "benefitTemplate",
    "versionFilters",
    "drugFilters",
    "choiceFilters",
    "planFilters",
    "benefitFilters"
})
public class GetGroupQuoteRequestPreference {

    @XmlElement(name = "InsuranceTypes", required = true, nillable = true)
    protected ArrayOfInsuranceType insuranceTypes;
    @XmlElement(name = "PlanVisibility", required = true)
    protected GetGroupQuoteRequestPlanVisibility planVisibility;
    @XmlElement(name = "PlanEligibility")
    protected GroupPlanEligibility planEligibility;
    @XmlList
    @XmlElement(name = "QuoteFormat", required = true)
    protected List<String> quoteFormat;
    @XmlElementRef(name = "QuoteState", type = JAXBElement.class, required = false)
    protected JAXBElement<String> quoteState;
    @XmlList
    @XmlElement(name = "RateUnit", required = true)
    protected List<String> rateUnit;
    @XmlElementRef(name = "RateSet", type = JAXBElement.class, required = false)
    protected List<JAXBElement<List<String>>> rateSet;
    @XmlElementRef(name = "AsOfDate", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> asOfDate;
    @XmlElement(name = "ProductChoicesOnly")
    protected Boolean productChoicesOnly;
    @XmlElement(name = "CalculateERContributions")
    protected Boolean calculateERContributions;
    @XmlElementRef(name = "AddOns", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteAddOn> addOns;
    @XmlElementRef(name = "LevelOfBenefitAddOns", type = JAXBElement.class, required = false)
    protected JAXBElement<LevelOfBenefitAddOns> levelOfBenefitAddOns;
    @XmlElementRef(name = "BenefitTemplate", type = JAXBElement.class, required = false)
    protected JAXBElement<String> benefitTemplate;
    @XmlElementRef(name = "VersionFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceVersionFilter> versionFilters;
    @XmlElementRef(name = "DrugFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceDrugFilter> drugFilters;
    @XmlElementRef(name = "ChoiceFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter> choiceFilters;
    @XmlElementRef(name = "PlanFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteRequestPreferencePlanFilter> planFilters;
    @XmlElementRef(name = "BenefitFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter> benefitFilters;

    /**
     * Gets the value of the insuranceTypes property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfInsuranceType }
     *     
     */
    public ArrayOfInsuranceType getInsuranceTypes() {
        return insuranceTypes;
    }

    /**
     * Sets the value of the insuranceTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfInsuranceType }
     *     
     */
    public void setInsuranceTypes(ArrayOfInsuranceType value) {
        this.insuranceTypes = value;
    }

    /**
     * Gets the value of the planVisibility property.
     * 
     * @return
     *     possible object is
     *     {@link GetGroupQuoteRequestPlanVisibility }
     *     
     */
    public GetGroupQuoteRequestPlanVisibility getPlanVisibility() {
        return planVisibility;
    }

    /**
     * Sets the value of the planVisibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetGroupQuoteRequestPlanVisibility }
     *     
     */
    public void setPlanVisibility(GetGroupQuoteRequestPlanVisibility value) {
        this.planVisibility = value;
    }

    /**
     * Gets the value of the planEligibility property.
     * 
     * @return
     *     possible object is
     *     {@link GroupPlanEligibility }
     *     
     */
    public GroupPlanEligibility getPlanEligibility() {
        return planEligibility;
    }

    /**
     * Sets the value of the planEligibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupPlanEligibility }
     *     
     */
    public void setPlanEligibility(GroupPlanEligibility value) {
        this.planEligibility = value;
    }

    /**
     * Gets the value of the quoteFormat property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the quoteFormat property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuoteFormat().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getQuoteFormat() {
        if (quoteFormat == null) {
            quoteFormat = new ArrayList<String>();
        }
        return this.quoteFormat;
    }

    /**
     * Gets the value of the quoteState property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getQuoteState() {
        return quoteState;
    }

    /**
     * Sets the value of the quoteState property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setQuoteState(JAXBElement<String> value) {
        this.quoteState = value;
    }

    /**
     * Gets the value of the rateUnit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rateUnit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRateUnit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRateUnit() {
        if (rateUnit == null) {
            rateUnit = new ArrayList<String>();
        }
        return this.rateUnit;
    }

    /**
     * Gets the value of the rateSet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rateSet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRateSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}
     * 
     * 
     */
    public List<JAXBElement<List<String>>> getRateSet() {
        if (rateSet == null) {
            rateSet = new ArrayList<JAXBElement<List<String>>>();
        }
        return this.rateSet;
    }

    /**
     * Gets the value of the asOfDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAsOfDate() {
        return asOfDate;
    }

    /**
     * Sets the value of the asOfDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAsOfDate(JAXBElement<XMLGregorianCalendar> value) {
        this.asOfDate = value;
    }

    /**
     * Gets the value of the productChoicesOnly property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProductChoicesOnly() {
        return productChoicesOnly;
    }

    /**
     * Sets the value of the productChoicesOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProductChoicesOnly(Boolean value) {
        this.productChoicesOnly = value;
    }

    /**
     * Gets the value of the calculateERContributions property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCalculateERContributions() {
        return calculateERContributions;
    }

    /**
     * Sets the value of the calculateERContributions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCalculateERContributions(Boolean value) {
        this.calculateERContributions = value;
    }

    /**
     * Gets the value of the addOns property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteAddOn }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteAddOn> getAddOns() {
        return addOns;
    }

    /**
     * Sets the value of the addOns property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteAddOn }{@code >}
     *     
     */
    public void setAddOns(JAXBElement<ArrayOfGetGroupQuoteAddOn> value) {
        this.addOns = value;
    }

    /**
     * Gets the value of the levelOfBenefitAddOns property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link LevelOfBenefitAddOns }{@code >}
     *     
     */
    public JAXBElement<LevelOfBenefitAddOns> getLevelOfBenefitAddOns() {
        return levelOfBenefitAddOns;
    }

    /**
     * Sets the value of the levelOfBenefitAddOns property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link LevelOfBenefitAddOns }{@code >}
     *     
     */
    public void setLevelOfBenefitAddOns(JAXBElement<LevelOfBenefitAddOns> value) {
        this.levelOfBenefitAddOns = value;
    }

    /**
     * Gets the value of the benefitTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBenefitTemplate() {
        return benefitTemplate;
    }

    /**
     * Sets the value of the benefitTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBenefitTemplate(JAXBElement<String> value) {
        this.benefitTemplate = value;
    }

    /**
     * Gets the value of the versionFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceVersionFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceVersionFilter> getVersionFilters() {
        return versionFilters;
    }

    /**
     * Sets the value of the versionFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceVersionFilter }{@code >}
     *     
     */
    public void setVersionFilters(JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceVersionFilter> value) {
        this.versionFilters = value;
    }

    /**
     * Gets the value of the drugFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceDrugFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceDrugFilter> getDrugFilters() {
        return drugFilters;
    }

    /**
     * Sets the value of the drugFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceDrugFilter }{@code >}
     *     
     */
    public void setDrugFilters(JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceDrugFilter> value) {
        this.drugFilters = value;
    }

    /**
     * Gets the value of the choiceFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter> getChoiceFilters() {
        return choiceFilters;
    }

    /**
     * Sets the value of the choiceFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter }{@code >}
     *     
     */
    public void setChoiceFilters(JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceChoiceFilter> value) {
        this.choiceFilters = value;
    }

    /**
     * Gets the value of the planFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferencePlanFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferencePlanFilter> getPlanFilters() {
        return planFilters;
    }

    /**
     * Sets the value of the planFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferencePlanFilter }{@code >}
     *     
     */
    public void setPlanFilters(JAXBElement<ArrayOfGetGroupQuoteRequestPreferencePlanFilter> value) {
        this.planFilters = value;
    }

    /**
     * Gets the value of the benefitFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter> getBenefitFilters() {
        return benefitFilters;
    }

    /**
     * Sets the value of the benefitFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter }{@code >}
     *     
     */
    public void setBenefitFilters(JAXBElement<ArrayOfGetGroupQuoteRequestPreferenceBenefitFilter> value) {
        this.benefitFilters = value;
    }

}
