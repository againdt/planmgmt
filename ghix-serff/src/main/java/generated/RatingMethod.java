
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RatingMethod.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="RatingMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PerMember"/>
 *     &lt;enumeration value="PerSubscriber"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "RatingMethod")
@XmlEnum
public enum RatingMethod {

    @XmlEnumValue("PerMember")
    PER_MEMBER("PerMember"),
    @XmlEnumValue("PerSubscriber")
    PER_SUBSCRIBER("PerSubscriber");
    private final String value;

    RatingMethod(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RatingMethod fromValue(String v) {
        for (RatingMethod c: RatingMethod.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
