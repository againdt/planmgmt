
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.Carrier.Plan" type="{}GetCarriersPlansBenefits.Response.Carrier.Plan" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan", propOrder = {
    "getCarriersPlansBenefitsResponseCarrierPlan"
})
public class ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.Carrier.Plan", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseCarrierPlan> getCarriersPlansBenefitsResponseCarrierPlan;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseCarrierPlan property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseCarrierPlan property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseCarrierPlan().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseCarrierPlan }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseCarrierPlan> getGetCarriersPlansBenefitsResponseCarrierPlan() {
        if (getCarriersPlansBenefitsResponseCarrierPlan == null) {
            getCarriersPlansBenefitsResponseCarrierPlan = new ArrayList<GetCarriersPlansBenefitsResponseCarrierPlan>();
        }
        return this.getCarriersPlansBenefitsResponseCarrierPlan;
    }

}
