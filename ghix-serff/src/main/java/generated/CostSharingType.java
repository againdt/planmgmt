
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CostSharingType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CostSharingType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Copayment"/>
 *     &lt;enumeration value="Coinsurance"/>
 *     &lt;enumeration value="WhicheverIsLesser"/>
 *     &lt;enumeration value="WhicheverIsGreater"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CostSharingType")
@XmlEnum
public enum CostSharingType {

    @XmlEnumValue("Copayment")
    COPAYMENT("Copayment"),
    @XmlEnumValue("Coinsurance")
    COINSURANCE("Coinsurance"),
    @XmlEnumValue("WhicheverIsLesser")
    WHICHEVER_IS_LESSER("WhicheverIsLesser"),
    @XmlEnumValue("WhicheverIsGreater")
    WHICHEVER_IS_GREATER("WhicheverIsGreater");
    private final String value;

    CostSharingType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CostSharingType fromValue(String v) {
        for (CostSharingType c: CostSharingType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
