
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetGroupQuote.Request.PlanVisibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GetGroupQuote.Request.PlanVisibility">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="All"/>
 *     &lt;enumeration value="OnWebsite"/>
 *     &lt;enumeration value="OnProposals"/>
 *     &lt;enumeration value="OnBoth"/>
 *     &lt;enumeration value="OnAny"/>
 *     &lt;enumeration value="OffBoth"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GetGroupQuote.Request.PlanVisibility")
@XmlEnum
public enum GetGroupQuoteRequestPlanVisibility {

    @XmlEnumValue("All")
    ALL("All"),
    @XmlEnumValue("OnWebsite")
    ON_WEBSITE("OnWebsite"),
    @XmlEnumValue("OnProposals")
    ON_PROPOSALS("OnProposals"),
    @XmlEnumValue("OnBoth")
    ON_BOTH("OnBoth"),
    @XmlEnumValue("OnAny")
    ON_ANY("OnAny"),
    @XmlEnumValue("OffBoth")
    OFF_BOTH("OffBoth");
    private final String value;

    GetGroupQuoteRequestPlanVisibility(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GetGroupQuoteRequestPlanVisibility fromValue(String v) {
        for (GetGroupQuoteRequestPlanVisibility c: GetGroupQuoteRequestPlanVisibility.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
