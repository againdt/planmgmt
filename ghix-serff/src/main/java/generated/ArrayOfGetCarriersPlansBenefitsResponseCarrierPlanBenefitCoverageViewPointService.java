
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service" type="{}GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service", propOrder = {
    "getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService"
})
public class ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage.ViewPoint.Service", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService> getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService> getGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService() {
        if (getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService == null) {
            getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService = new ArrayList<GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService>();
        }
        return this.getCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPointService;
    }

}
