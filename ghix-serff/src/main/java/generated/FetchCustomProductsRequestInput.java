
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FetchCustomProducts.Request.Input complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FetchCustomProducts.Request.Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SubmissionId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ProductFilters" type="{}ArrayOfFetchCustomProducts.Request.Input.ProductFilter" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FetchCustomProducts.Request.Input", propOrder = {
    "clientId",
    "submissionId",
    "productFilters"
})
public class FetchCustomProductsRequestInput {

    @XmlElement(name = "ClientId", required = true, nillable = true)
    protected String clientId;
    @XmlElementRef(name = "SubmissionId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> submissionId;
    @XmlElementRef(name = "ProductFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfFetchCustomProductsRequestInputProductFilter> productFilters;

    /**
     * Gets the value of the clientId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Sets the value of the clientId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Gets the value of the submissionId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSubmissionId() {
        return submissionId;
    }

    /**
     * Sets the value of the submissionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSubmissionId(JAXBElement<Integer> value) {
        this.submissionId = value;
    }

    /**
     * Gets the value of the productFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFetchCustomProductsRequestInputProductFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFetchCustomProductsRequestInputProductFilter> getProductFilters() {
        return productFilters;
    }

    /**
     * Sets the value of the productFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFetchCustomProductsRequestInputProductFilter }{@code >}
     *     
     */
    public void setProductFilters(JAXBElement<ArrayOfFetchCustomProductsRequestInputProductFilter> value) {
        this.productFilters = value;
    }

}
