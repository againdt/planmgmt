
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCustomProduct.RatingArea.ZipCounty complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomProduct.RatingArea.ZipCounty">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomProduct.RatingArea.ZipCounty" type="{}CustomProduct.RatingArea.ZipCounty" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomProduct.RatingArea.ZipCounty", propOrder = {
    "customProductRatingAreaZipCounty"
})
public class ArrayOfCustomProductRatingAreaZipCounty {

    @XmlElement(name = "CustomProduct.RatingArea.ZipCounty", nillable = true)
    protected List<CustomProductRatingAreaZipCounty> customProductRatingAreaZipCounty;

    /**
     * Gets the value of the customProductRatingAreaZipCounty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customProductRatingAreaZipCounty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomProductRatingAreaZipCounty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomProductRatingAreaZipCounty }
     * 
     * 
     */
    public List<CustomProductRatingAreaZipCounty> getCustomProductRatingAreaZipCounty() {
        if (customProductRatingAreaZipCounty == null) {
            customProductRatingAreaZipCounty = new ArrayList<CustomProductRatingAreaZipCounty>();
        }
        return this.customProductRatingAreaZipCounty;
    }

}
