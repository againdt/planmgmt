
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCounties.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCounties.Response">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Counties" type="{}ArrayOfGetCounties.Response.County" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCounties.Response", propOrder = {
    "counties"
})
public class GetCountiesResponse {

    @XmlElementRef(name = "Counties", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCountiesResponseCounty> counties;

    /**
     * Gets the value of the counties property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCountiesResponseCounty }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCountiesResponseCounty> getCounties() {
        return counties;
    }

    /**
     * Sets the value of the counties property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCountiesResponseCounty }{@code >}
     *     
     */
    public void setCounties(JAXBElement<ArrayOfGetCountiesResponseCounty> value) {
        this.counties = value;
    }

}
