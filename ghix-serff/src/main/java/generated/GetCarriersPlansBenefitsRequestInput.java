
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GetCarriersPlansBenefits.Request.Input complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarriersPlansBenefits.Request.Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="InsuranceCategory" type="{}InsuranceCategory"/>
 *         &lt;element name="LevelOfDetails" type="{}GetCarriersPlansBenefits.LevelOfDetail"/>
 *         &lt;element name="BenefitTemplate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataSet" type="{}RateSet" minOccurs="0"/>
 *         &lt;element name="AsOfDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LatestVersionOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="AddOns" type="{}ArrayOfGetCarriersPlansBenefits.AddOn" minOccurs="0"/>
 *         &lt;element name="VersionFilters" type="{}ArrayOfGetCarriersPlansBenefits.Request.VersionFilter" minOccurs="0"/>
 *         &lt;element name="DrugFilter" type="{}GetCarriersPlansBenefits.Request.DrugFilters" minOccurs="0"/>
 *         &lt;element name="CarrierPlanFilters" type="{}ArrayOfGetCarriersPlansBenefits.Request.CarrierPlanFilter" minOccurs="0"/>
 *         &lt;element name="BenefitFilters" type="{}ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarriersPlansBenefits.Request.Input", propOrder = {
    "effectiveDate",
    "insuranceCategory",
    "levelOfDetails",
    "benefitTemplate",
    "dataSet",
    "asOfDate",
    "latestVersionOnly",
    "addOns",
    "versionFilters",
    "drugFilter",
    "carrierPlanFilters",
    "benefitFilters"
})
public class GetCarriersPlansBenefitsRequestInput {

    @XmlElement(name = "EffectiveDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlList
    @XmlElement(name = "InsuranceCategory", required = true)
    protected List<String> insuranceCategory;
    @XmlElement(name = "LevelOfDetails", required = true)
    protected GetCarriersPlansBenefitsLevelOfDetail levelOfDetails;
    @XmlElementRef(name = "BenefitTemplate", type = JAXBElement.class, required = false)
    protected JAXBElement<String> benefitTemplate;
    @XmlElementRef(name = "DataSet", type = JAXBElement.class, required = false)
    protected List<JAXBElement<List<String>>> dataSet;
    @XmlElementRef(name = "AsOfDate", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> asOfDate;
    @XmlElementRef(name = "LatestVersionOnly", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> latestVersionOnly;
    @XmlElementRef(name = "AddOns", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsAddOn> addOns;
    @XmlElementRef(name = "VersionFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestVersionFilter> versionFilters;
    @XmlElementRef(name = "DrugFilter", type = JAXBElement.class, required = false)
    protected JAXBElement<GetCarriersPlansBenefitsRequestDrugFilters> drugFilter;
    @XmlElementRef(name = "CarrierPlanFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter> carrierPlanFilters;
    @XmlElementRef(name = "BenefitFilters", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter> benefitFilters;

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the insuranceCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the insuranceCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInsuranceCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getInsuranceCategory() {
        if (insuranceCategory == null) {
            insuranceCategory = new ArrayList<String>();
        }
        return this.insuranceCategory;
    }

    /**
     * Gets the value of the levelOfDetails property.
     * 
     * @return
     *     possible object is
     *     {@link GetCarriersPlansBenefitsLevelOfDetail }
     *     
     */
    public GetCarriersPlansBenefitsLevelOfDetail getLevelOfDetails() {
        return levelOfDetails;
    }

    /**
     * Sets the value of the levelOfDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetCarriersPlansBenefitsLevelOfDetail }
     *     
     */
    public void setLevelOfDetails(GetCarriersPlansBenefitsLevelOfDetail value) {
        this.levelOfDetails = value;
    }

    /**
     * Gets the value of the benefitTemplate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBenefitTemplate() {
        return benefitTemplate;
    }

    /**
     * Sets the value of the benefitTemplate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBenefitTemplate(JAXBElement<String> value) {
        this.benefitTemplate = value;
    }

    /**
     * Gets the value of the dataSet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dataSet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDataSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}
     * 
     * 
     */
    public List<JAXBElement<List<String>>> getDataSet() {
        if (dataSet == null) {
            dataSet = new ArrayList<JAXBElement<List<String>>>();
        }
        return this.dataSet;
    }

    /**
     * Gets the value of the asOfDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getAsOfDate() {
        return asOfDate;
    }

    /**
     * Sets the value of the asOfDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setAsOfDate(JAXBElement<XMLGregorianCalendar> value) {
        this.asOfDate = value;
    }

    /**
     * Gets the value of the latestVersionOnly property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getLatestVersionOnly() {
        return latestVersionOnly;
    }

    /**
     * Sets the value of the latestVersionOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setLatestVersionOnly(JAXBElement<Boolean> value) {
        this.latestVersionOnly = value;
    }

    /**
     * Gets the value of the addOns property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsAddOn }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsAddOn> getAddOns() {
        return addOns;
    }

    /**
     * Sets the value of the addOns property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsAddOn }{@code >}
     *     
     */
    public void setAddOns(JAXBElement<ArrayOfGetCarriersPlansBenefitsAddOn> value) {
        this.addOns = value;
    }

    /**
     * Gets the value of the versionFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestVersionFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestVersionFilter> getVersionFilters() {
        return versionFilters;
    }

    /**
     * Sets the value of the versionFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestVersionFilter }{@code >}
     *     
     */
    public void setVersionFilters(JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestVersionFilter> value) {
        this.versionFilters = value;
    }

    /**
     * Gets the value of the drugFilter property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequestDrugFilters }{@code >}
     *     
     */
    public JAXBElement<GetCarriersPlansBenefitsRequestDrugFilters> getDrugFilter() {
        return drugFilter;
    }

    /**
     * Sets the value of the drugFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequestDrugFilters }{@code >}
     *     
     */
    public void setDrugFilter(JAXBElement<GetCarriersPlansBenefitsRequestDrugFilters> value) {
        this.drugFilter = value;
    }

    /**
     * Gets the value of the carrierPlanFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter> getCarrierPlanFilters() {
        return carrierPlanFilters;
    }

    /**
     * Sets the value of the carrierPlanFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter }{@code >}
     *     
     */
    public void setCarrierPlanFilters(JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter> value) {
        this.carrierPlanFilters = value;
    }

    /**
     * Gets the value of the benefitFilters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter> getBenefitFilters() {
        return benefitFilters;
    }

    /**
     * Sets the value of the benefitFilters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter }{@code >}
     *     
     */
    public void setBenefitFilters(JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter> value) {
        this.benefitFilters = value;
    }

}
