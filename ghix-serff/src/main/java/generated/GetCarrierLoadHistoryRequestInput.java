
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GetCarrierLoadHistory.Request.Input complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarrierLoadHistory.Request.Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InsuranceCategory" type="{}InsuranceCategory"/>
 *         &lt;element name="FromLoadSequenceId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ToLoadSequenceId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FromEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ToEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarrierLoadHistory.Request.Input", propOrder = {
    "insuranceCategory",
    "fromLoadSequenceId",
    "toLoadSequenceId",
    "fromEffectiveDate",
    "toEffectiveDate"
})
public class GetCarrierLoadHistoryRequestInput {

    @XmlList
    @XmlElement(name = "InsuranceCategory", required = true)
    protected List<String> insuranceCategory;
    @XmlElement(name = "FromLoadSequenceId")
    protected int fromLoadSequenceId;
    @XmlElementRef(name = "ToLoadSequenceId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> toLoadSequenceId;
    @XmlElementRef(name = "FromEffectiveDate", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> fromEffectiveDate;
    @XmlElementRef(name = "ToEffectiveDate", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> toEffectiveDate;

    /**
     * Gets the value of the insuranceCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the insuranceCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInsuranceCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getInsuranceCategory() {
        if (insuranceCategory == null) {
            insuranceCategory = new ArrayList<String>();
        }
        return this.insuranceCategory;
    }

    /**
     * Gets the value of the fromLoadSequenceId property.
     * 
     */
    public int getFromLoadSequenceId() {
        return fromLoadSequenceId;
    }

    /**
     * Sets the value of the fromLoadSequenceId property.
     * 
     */
    public void setFromLoadSequenceId(int value) {
        this.fromLoadSequenceId = value;
    }

    /**
     * Gets the value of the toLoadSequenceId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getToLoadSequenceId() {
        return toLoadSequenceId;
    }

    /**
     * Sets the value of the toLoadSequenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setToLoadSequenceId(JAXBElement<Integer> value) {
        this.toLoadSequenceId = value;
    }

    /**
     * Gets the value of the fromEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getFromEffectiveDate() {
        return fromEffectiveDate;
    }

    /**
     * Sets the value of the fromEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setFromEffectiveDate(JAXBElement<XMLGregorianCalendar> value) {
        this.fromEffectiveDate = value;
    }

    /**
     * Gets the value of the toEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getToEffectiveDate() {
        return toEffectiveDate;
    }

    /**
     * Sets the value of the toEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setToEffectiveDate(JAXBElement<XMLGregorianCalendar> value) {
        this.toEffectiveDate = value;
    }

}
