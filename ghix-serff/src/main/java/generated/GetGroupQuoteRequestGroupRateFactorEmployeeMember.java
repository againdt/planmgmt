
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GetGroupQuote.Request.GroupRateFactor.Employee.Member complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetGroupQuote.Request.GroupRateFactor.Employee.Member">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MemberType" type="{}MemberType"/>
 *         &lt;element name="RelationshipType" type="{}RelationshipType"/>
 *         &lt;element name="MembershipDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DateOfBirth" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Gender" type="{}Gender"/>
 *         &lt;element name="IsSmoker" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DateLastSmoked" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="LivesInHousehold" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ZipCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="CountyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetGroupQuote.Request.GroupRateFactor.Employee.Member", propOrder = {
    "memberId",
    "memberType",
    "relationshipType",
    "membershipDate",
    "dateOfBirth",
    "gender",
    "isSmoker",
    "dateLastSmoked",
    "livesInHousehold",
    "zipCode",
    "countyName"
})
public class GetGroupQuoteRequestGroupRateFactorEmployeeMember {

    @XmlElement(name = "MemberId", required = true, nillable = true)
    protected String memberId;
    @XmlList
    @XmlElement(name = "MemberType", required = true)
    protected List<String> memberType;
    @XmlElement(name = "RelationshipType", required = true)
    protected RelationshipType relationshipType;
    @XmlElementRef(name = "MembershipDate", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> membershipDate;
    @XmlElement(name = "DateOfBirth", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlList
    @XmlElement(name = "Gender", required = true)
    protected List<String> gender;
    @XmlElementRef(name = "IsSmoker", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> isSmoker;
    @XmlElementRef(name = "DateLastSmoked", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dateLastSmoked;
    @XmlElement(name = "LivesInHousehold")
    protected boolean livesInHousehold;
    @XmlElementRef(name = "ZipCode", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> zipCode;
    @XmlElementRef(name = "CountyName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> countyName;

    /**
     * Gets the value of the memberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberId() {
        return memberId;
    }

    /**
     * Sets the value of the memberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberId(String value) {
        this.memberId = value;
    }

    /**
     * Gets the value of the memberType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the memberType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMemberType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMemberType() {
        if (memberType == null) {
            memberType = new ArrayList<String>();
        }
        return this.memberType;
    }

    /**
     * Gets the value of the relationshipType property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipType }
     *     
     */
    public RelationshipType getRelationshipType() {
        return relationshipType;
    }

    /**
     * Sets the value of the relationshipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipType }
     *     
     */
    public void setRelationshipType(RelationshipType value) {
        this.relationshipType = value;
    }

    /**
     * Gets the value of the membershipDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getMembershipDate() {
        return membershipDate;
    }

    /**
     * Sets the value of the membershipDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setMembershipDate(JAXBElement<XMLGregorianCalendar> value) {
        this.membershipDate = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gender property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGender().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getGender() {
        if (gender == null) {
            gender = new ArrayList<String>();
        }
        return this.gender;
    }

    /**
     * Gets the value of the isSmoker property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIsSmoker() {
        return isSmoker;
    }

    /**
     * Sets the value of the isSmoker property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIsSmoker(JAXBElement<Boolean> value) {
        this.isSmoker = value;
    }

    /**
     * Gets the value of the dateLastSmoked property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDateLastSmoked() {
        return dateLastSmoked;
    }

    /**
     * Sets the value of the dateLastSmoked property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDateLastSmoked(JAXBElement<XMLGregorianCalendar> value) {
        this.dateLastSmoked = value;
    }

    /**
     * Gets the value of the livesInHousehold property.
     * 
     */
    public boolean isLivesInHousehold() {
        return livesInHousehold;
    }

    /**
     * Sets the value of the livesInHousehold property.
     * 
     */
    public void setLivesInHousehold(boolean value) {
        this.livesInHousehold = value;
    }

    /**
     * Gets the value of the zipCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getZipCode() {
        return zipCode;
    }

    /**
     * Sets the value of the zipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setZipCode(JAXBElement<Integer> value) {
        this.zipCode = value;
    }

    /**
     * Gets the value of the countyName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCountyName() {
        return countyName;
    }

    /**
     * Sets the value of the countyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCountyName(JAXBElement<String> value) {
        this.countyName = value;
    }

}
