
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO" type="{}GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO", propOrder = {
    "getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO"
})
public class ArrayOfGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO {

    @XmlElement(name = "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO", nillable = true)
    protected List<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO;

    /**
     * Gets the value of the getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }
     * 
     * 
     */
    public List<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> getGetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO() {
        if (getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO == null) {
            getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO = new ArrayList<GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO>();
        }
        return this.getPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO;
    }

}
