
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomProduct.Plan.BusinessRule complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomProduct.Plan.BusinessRule">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RatingMethod" type="{}RatingMethod"/>
 *         &lt;element name="MinAdultAge" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="AgeCalculationBase" type="{}AgeCalculationBase"/>
 *         &lt;element name="IgnoreMembershipDate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TobaccoFreeMonths" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/>
 *         &lt;element name="PlanOffering" type="{}PlanOffering"/>
 *         &lt;element name="ChildrenCap" type="{}CustomProduct.Plan.BusinessRule.CappingRule" minOccurs="0"/>
 *         &lt;element name="AllowedRelations" type="{}ArrayOfCustomProduct.Plan.BusinessRule.AllowedRelation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomProduct.Plan.BusinessRule", propOrder = {
    "ratingMethod",
    "minAdultAge",
    "ageCalculationBase",
    "ignoreMembershipDate",
    "tobaccoFreeMonths",
    "planOffering",
    "childrenCap",
    "allowedRelations"
})
public class CustomProductPlanBusinessRule {

    @XmlElement(name = "RatingMethod", required = true)
    protected RatingMethod ratingMethod;
    @XmlElementRef(name = "MinAdultAge", type = JAXBElement.class, required = false)
    protected JAXBElement<Short> minAdultAge;
    @XmlElement(name = "AgeCalculationBase", required = true)
    protected AgeCalculationBase ageCalculationBase;
    @XmlElementRef(name = "IgnoreMembershipDate", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> ignoreMembershipDate;
    @XmlElementRef(name = "TobaccoFreeMonths", type = JAXBElement.class, required = false)
    protected JAXBElement<Short> tobaccoFreeMonths;
    @XmlElement(name = "PlanOffering", required = true)
    protected PlanOffering planOffering;
    @XmlElementRef(name = "ChildrenCap", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomProductPlanBusinessRuleCappingRule> childrenCap;
    @XmlElementRef(name = "AllowedRelations", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomProductPlanBusinessRuleAllowedRelation> allowedRelations;

    /**
     * Gets the value of the ratingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link RatingMethod }
     *     
     */
    public RatingMethod getRatingMethod() {
        return ratingMethod;
    }

    /**
     * Sets the value of the ratingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link RatingMethod }
     *     
     */
    public void setRatingMethod(RatingMethod value) {
        this.ratingMethod = value;
    }

    /**
     * Gets the value of the minAdultAge property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Short }{@code >}
     *     
     */
    public JAXBElement<Short> getMinAdultAge() {
        return minAdultAge;
    }

    /**
     * Sets the value of the minAdultAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Short }{@code >}
     *     
     */
    public void setMinAdultAge(JAXBElement<Short> value) {
        this.minAdultAge = value;
    }

    /**
     * Gets the value of the ageCalculationBase property.
     * 
     * @return
     *     possible object is
     *     {@link AgeCalculationBase }
     *     
     */
    public AgeCalculationBase getAgeCalculationBase() {
        return ageCalculationBase;
    }

    /**
     * Sets the value of the ageCalculationBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link AgeCalculationBase }
     *     
     */
    public void setAgeCalculationBase(AgeCalculationBase value) {
        this.ageCalculationBase = value;
    }

    /**
     * Gets the value of the ignoreMembershipDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIgnoreMembershipDate() {
        return ignoreMembershipDate;
    }

    /**
     * Sets the value of the ignoreMembershipDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIgnoreMembershipDate(JAXBElement<Boolean> value) {
        this.ignoreMembershipDate = value;
    }

    /**
     * Gets the value of the tobaccoFreeMonths property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Short }{@code >}
     *     
     */
    public JAXBElement<Short> getTobaccoFreeMonths() {
        return tobaccoFreeMonths;
    }

    /**
     * Sets the value of the tobaccoFreeMonths property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Short }{@code >}
     *     
     */
    public void setTobaccoFreeMonths(JAXBElement<Short> value) {
        this.tobaccoFreeMonths = value;
    }

    /**
     * Gets the value of the planOffering property.
     * 
     * @return
     *     possible object is
     *     {@link PlanOffering }
     *     
     */
    public PlanOffering getPlanOffering() {
        return planOffering;
    }

    /**
     * Sets the value of the planOffering property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanOffering }
     *     
     */
    public void setPlanOffering(PlanOffering value) {
        this.planOffering = value;
    }

    /**
     * Gets the value of the childrenCap property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomProductPlanBusinessRuleCappingRule }{@code >}
     *     
     */
    public JAXBElement<CustomProductPlanBusinessRuleCappingRule> getChildrenCap() {
        return childrenCap;
    }

    /**
     * Sets the value of the childrenCap property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomProductPlanBusinessRuleCappingRule }{@code >}
     *     
     */
    public void setChildrenCap(JAXBElement<CustomProductPlanBusinessRuleCappingRule> value) {
        this.childrenCap = value;
    }

    /**
     * Gets the value of the allowedRelations property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanBusinessRuleAllowedRelation }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomProductPlanBusinessRuleAllowedRelation> getAllowedRelations() {
        return allowedRelations;
    }

    /**
     * Sets the value of the allowedRelations property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanBusinessRuleAllowedRelation }{@code >}
     *     
     */
    public void setAllowedRelations(JAXBElement<ArrayOfCustomProductPlanBusinessRuleAllowedRelation> value) {
        this.allowedRelations = value;
    }

}
