
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.Request.Preference.BenefitFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.Request.Preference.BenefitFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.Request.Preference.BenefitFilter" type="{}GetIfpQuote.Request.Preference.BenefitFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.Request.Preference.BenefitFilter", propOrder = {
    "getIfpQuoteRequestPreferenceBenefitFilter"
})
public class ArrayOfGetIfpQuoteRequestPreferenceBenefitFilter {

    @XmlElement(name = "GetIfpQuote.Request.Preference.BenefitFilter", nillable = true)
    protected List<GetIfpQuoteRequestPreferenceBenefitFilter> getIfpQuoteRequestPreferenceBenefitFilter;

    /**
     * Gets the value of the getIfpQuoteRequestPreferenceBenefitFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteRequestPreferenceBenefitFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteRequestPreferenceBenefitFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteRequestPreferenceBenefitFilter }
     * 
     * 
     */
    public List<GetIfpQuoteRequestPreferenceBenefitFilter> getGetIfpQuoteRequestPreferenceBenefitFilter() {
        if (getIfpQuoteRequestPreferenceBenefitFilter == null) {
            getIfpQuoteRequestPreferenceBenefitFilter = new ArrayList<GetIfpQuoteRequestPreferenceBenefitFilter>();
        }
        return this.getIfpQuoteRequestPreferenceBenefitFilter;
    }

}
