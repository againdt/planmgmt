
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PharmacyType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PharmacyType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Retail"/>
 *     &lt;enumeration value="MailOrder"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PharmacyType")
@XmlEnum
public enum PharmacyType {

    @XmlEnumValue("Retail")
    RETAIL("Retail"),
    @XmlEnumValue("MailOrder")
    MAIL_ORDER("MailOrder");
    private final String value;

    PharmacyType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PharmacyType fromValue(String v) {
        for (PharmacyType c: PharmacyType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
