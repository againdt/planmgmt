
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Response.DrugList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Response.DrugList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Response.DrugList" type="{}GetCarriersPlansBenefits.Response.DrugList" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Response.DrugList", propOrder = {
    "getCarriersPlansBenefitsResponseDrugList"
})
public class ArrayOfGetCarriersPlansBenefitsResponseDrugList {

    @XmlElement(name = "GetCarriersPlansBenefits.Response.DrugList", nillable = true)
    protected List<GetCarriersPlansBenefitsResponseDrugList> getCarriersPlansBenefitsResponseDrugList;

    /**
     * Gets the value of the getCarriersPlansBenefitsResponseDrugList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsResponseDrugList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsResponseDrugList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsResponseDrugList }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsResponseDrugList> getGetCarriersPlansBenefitsResponseDrugList() {
        if (getCarriersPlansBenefitsResponseDrugList == null) {
            getCarriersPlansBenefitsResponseDrugList = new ArrayList<GetCarriersPlansBenefitsResponseDrugList>();
        }
        return this.getCarriersPlansBenefitsResponseDrugList;
    }

}
