
package generated;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Group.Employee complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Group.Employee">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EmployeeId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OnCobra" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="WorksiteZipCode" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="WorksiteCountyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IncludeInQuote" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="UserName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Salary" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="LifeAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ChangeCodes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PartnerData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Members" type="{}ArrayOfGroup.Employee.Member"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Group.Employee", propOrder = {
    "employeeId",
    "onCobra",
    "worksiteZipCode",
    "worksiteCountyName",
    "includeInQuote",
    "userName",
    "emailAddress",
    "salary",
    "lifeAmount",
    "changeCodes",
    "partnerData",
    "members"
})
public class GroupEmployee {

    @XmlElement(name = "EmployeeId", required = true, nillable = true)
    protected String employeeId;
    @XmlElementRef(name = "OnCobra", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> onCobra;
    @XmlElementRef(name = "WorksiteZipCode", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> worksiteZipCode;
    @XmlElementRef(name = "WorksiteCountyName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> worksiteCountyName;
    @XmlElementRef(name = "IncludeInQuote", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> includeInQuote;
    @XmlElementRef(name = "UserName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> userName;
    @XmlElementRef(name = "EmailAddress", type = JAXBElement.class, required = false)
    protected JAXBElement<String> emailAddress;
    @XmlElementRef(name = "Salary", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> salary;
    @XmlElementRef(name = "LifeAmount", type = JAXBElement.class, required = false)
    protected JAXBElement<BigDecimal> lifeAmount;
    @XmlElementRef(name = "ChangeCodes", type = JAXBElement.class, required = false)
    protected JAXBElement<String> changeCodes;
    @XmlElementRef(name = "PartnerData", type = JAXBElement.class, required = false)
    protected JAXBElement<String> partnerData;
    @XmlElement(name = "Members", required = true, nillable = true)
    protected ArrayOfGroupEmployeeMember members;

    /**
     * Gets the value of the employeeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * Sets the value of the employeeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeId(String value) {
        this.employeeId = value;
    }

    /**
     * Gets the value of the onCobra property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getOnCobra() {
        return onCobra;
    }

    /**
     * Sets the value of the onCobra property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setOnCobra(JAXBElement<Boolean> value) {
        this.onCobra = value;
    }

    /**
     * Gets the value of the worksiteZipCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getWorksiteZipCode() {
        return worksiteZipCode;
    }

    /**
     * Sets the value of the worksiteZipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setWorksiteZipCode(JAXBElement<Integer> value) {
        this.worksiteZipCode = value;
    }

    /**
     * Gets the value of the worksiteCountyName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getWorksiteCountyName() {
        return worksiteCountyName;
    }

    /**
     * Sets the value of the worksiteCountyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setWorksiteCountyName(JAXBElement<String> value) {
        this.worksiteCountyName = value;
    }

    /**
     * Gets the value of the includeInQuote property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getIncludeInQuote() {
        return includeInQuote;
    }

    /**
     * Sets the value of the includeInQuote property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setIncludeInQuote(JAXBElement<Boolean> value) {
        this.includeInQuote = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setUserName(JAXBElement<String> value) {
        this.userName = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setEmailAddress(JAXBElement<String> value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the salary property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getSalary() {
        return salary;
    }

    /**
     * Sets the value of the salary property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setSalary(JAXBElement<BigDecimal> value) {
        this.salary = value;
    }

    /**
     * Gets the value of the lifeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public JAXBElement<BigDecimal> getLifeAmount() {
        return lifeAmount;
    }

    /**
     * Sets the value of the lifeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}
     *     
     */
    public void setLifeAmount(JAXBElement<BigDecimal> value) {
        this.lifeAmount = value;
    }

    /**
     * Gets the value of the changeCodes property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getChangeCodes() {
        return changeCodes;
    }

    /**
     * Sets the value of the changeCodes property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setChangeCodes(JAXBElement<String> value) {
        this.changeCodes = value;
    }

    /**
     * Gets the value of the partnerData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPartnerData() {
        return partnerData;
    }

    /**
     * Sets the value of the partnerData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPartnerData(JAXBElement<String> value) {
        this.partnerData = value;
    }

    /**
     * Gets the value of the members property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGroupEmployeeMember }
     *     
     */
    public ArrayOfGroupEmployeeMember getMembers() {
        return members;
    }

    /**
     * Sets the value of the members property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGroupEmployeeMember }
     *     
     */
    public void setMembers(ArrayOfGroupEmployeeMember value) {
        this.members = value;
    }

}
