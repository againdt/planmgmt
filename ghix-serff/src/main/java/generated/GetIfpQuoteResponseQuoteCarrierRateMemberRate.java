
package generated;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response.Quote.CarrierRate.MemberRate complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response.Quote.CarrierRate.MemberRate">
 *   &lt;complexContent>
 *     &lt;extension base="{}MemberRateBase">
 *       &lt;sequence>
 *         &lt;element name="RatingAreaBase" type="{}IfpRatingAreaBase" minOccurs="0"/>
 *         &lt;element name="RatingAreaId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="EHBRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response.Quote.CarrierRate.MemberRate", propOrder = {
    "ratingAreaBase",
    "ratingAreaId",
    "ehbRate"
})
public class GetIfpQuoteResponseQuoteCarrierRateMemberRate
    extends MemberRateBase
{

    @XmlElement(name = "RatingAreaBase")
    protected IfpRatingAreaBase ratingAreaBase;
    @XmlElement(name = "RatingAreaId")
    protected Integer ratingAreaId;
    @XmlElement(name = "EHBRate")
    protected BigDecimal ehbRate;

    /**
     * Gets the value of the ratingAreaBase property.
     * 
     * @return
     *     possible object is
     *     {@link IfpRatingAreaBase }
     *     
     */
    public IfpRatingAreaBase getRatingAreaBase() {
        return ratingAreaBase;
    }

    /**
     * Sets the value of the ratingAreaBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link IfpRatingAreaBase }
     *     
     */
    public void setRatingAreaBase(IfpRatingAreaBase value) {
        this.ratingAreaBase = value;
    }

    /**
     * Gets the value of the ratingAreaId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRatingAreaId() {
        return ratingAreaId;
    }

    /**
     * Sets the value of the ratingAreaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRatingAreaId(Integer value) {
        this.ratingAreaId = value;
    }

    /**
     * Gets the value of the ehbRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEHBRate() {
        return ehbRate;
    }

    /**
     * Sets the value of the ehbRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEHBRate(BigDecimal value) {
        this.ehbRate = value;
    }

}
