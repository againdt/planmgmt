
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmitGroup.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitGroup.Response">
 *   &lt;complexContent>
 *     &lt;extension base="{}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="GroupId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ContactId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ShoppingCarts" type="{}GroupShoppingCartsBase" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitGroup.Response", propOrder = {
    "groupId",
    "contactId",
    "shoppingCarts"
})
public class SubmitGroupResponse
    extends ResponseBase
{

    @XmlElement(name = "GroupId")
    protected int groupId;
    @XmlElement(name = "ContactId")
    protected int contactId;
    @XmlElementRef(name = "ShoppingCarts", type = JAXBElement.class, required = false)
    protected JAXBElement<GroupShoppingCartsBase> shoppingCarts;

    /**
     * Gets the value of the groupId property.
     * 
     */
    public int getGroupId() {
        return groupId;
    }

    /**
     * Sets the value of the groupId property.
     * 
     */
    public void setGroupId(int value) {
        this.groupId = value;
    }

    /**
     * Gets the value of the contactId property.
     * 
     */
    public int getContactId() {
        return contactId;
    }

    /**
     * Sets the value of the contactId property.
     * 
     */
    public void setContactId(int value) {
        this.contactId = value;
    }

    /**
     * Gets the value of the shoppingCarts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GroupShoppingCartsBase }{@code >}
     *     
     */
    public JAXBElement<GroupShoppingCartsBase> getShoppingCarts() {
        return shoppingCarts;
    }

    /**
     * Sets the value of the shoppingCarts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GroupShoppingCartsBase }{@code >}
     *     
     */
    public void setShoppingCarts(JAXBElement<GroupShoppingCartsBase> value) {
        this.shoppingCarts = value;
    }

}
