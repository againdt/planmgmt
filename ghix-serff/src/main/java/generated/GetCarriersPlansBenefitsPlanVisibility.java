
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarriersPlansBenefits.PlanVisibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GetCarriersPlansBenefits.PlanVisibility">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OnWebsite"/>
 *     &lt;enumeration value="OnProposals"/>
 *     &lt;enumeration value="OnBoth"/>
 *     &lt;enumeration value="OnAny"/>
 *     &lt;enumeration value="OffBoth"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GetCarriersPlansBenefits.PlanVisibility")
@XmlEnum
public enum GetCarriersPlansBenefitsPlanVisibility {

    @XmlEnumValue("OnWebsite")
    ON_WEBSITE("OnWebsite"),
    @XmlEnumValue("OnProposals")
    ON_PROPOSALS("OnProposals"),
    @XmlEnumValue("OnBoth")
    ON_BOTH("OnBoth"),
    @XmlEnumValue("OnAny")
    ON_ANY("OnAny"),
    @XmlEnumValue("OffBoth")
    OFF_BOTH("OffBoth");
    private final String value;

    GetCarriersPlansBenefitsPlanVisibility(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static GetCarriersPlansBenefitsPlanVisibility fromValue(String v) {
        for (GetCarriersPlansBenefitsPlanVisibility c: GetCarriersPlansBenefitsPlanVisibility.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
