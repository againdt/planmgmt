
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit">
 *   &lt;complexContent>
 *     &lt;extension base="{}BenefitBase">
 *       &lt;sequence>
 *         &lt;element name="BenefitData" type="{}ArrayOfGetCarriersPlansBenefits.Response.Item" minOccurs="0"/>
 *         &lt;element name="Coverages" type="{}ArrayOfGetCarriersPlansBenefits.Response.Carrier.Plan.Benefit.Coverage" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarriersPlansBenefits.Response.Carrier.Plan.Benefit", propOrder = {
    "benefitData",
    "coverages"
})
public class GetCarriersPlansBenefitsResponseCarrierPlanBenefit
    extends BenefitBase
{

    @XmlElementRef(name = "BenefitData", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> benefitData;
    @XmlElementRef(name = "Coverages", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage> coverages;

    /**
     * Gets the value of the benefitData property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseItem }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> getBenefitData() {
        return benefitData;
    }

    /**
     * Sets the value of the benefitData property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseItem }{@code >}
     *     
     */
    public void setBenefitData(JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> value) {
        this.benefitData = value;
    }

    /**
     * Gets the value of the coverages property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage> getCoverages() {
        return coverages;
    }

    /**
     * Sets the value of the coverages property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage }{@code >}
     *     
     */
    public void setCoverages(JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage> value) {
        this.coverages = value;
    }

}
