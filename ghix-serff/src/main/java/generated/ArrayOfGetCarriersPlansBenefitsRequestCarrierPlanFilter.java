
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Request.CarrierPlanFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Request.CarrierPlanFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Request.CarrierPlanFilter" type="{}GetCarriersPlansBenefits.Request.CarrierPlanFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Request.CarrierPlanFilter", propOrder = {
    "getCarriersPlansBenefitsRequestCarrierPlanFilter"
})
public class ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter {

    @XmlElement(name = "GetCarriersPlansBenefits.Request.CarrierPlanFilter", nillable = true)
    protected List<GetCarriersPlansBenefitsRequestCarrierPlanFilter> getCarriersPlansBenefitsRequestCarrierPlanFilter;

    /**
     * Gets the value of the getCarriersPlansBenefitsRequestCarrierPlanFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsRequestCarrierPlanFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsRequestCarrierPlanFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsRequestCarrierPlanFilter }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsRequestCarrierPlanFilter> getGetCarriersPlansBenefitsRequestCarrierPlanFilter() {
        if (getCarriersPlansBenefitsRequestCarrierPlanFilter == null) {
            getCarriersPlansBenefitsRequestCarrierPlanFilter = new ArrayList<GetCarriersPlansBenefitsRequestCarrierPlanFilter>();
        }
        return this.getCarriersPlansBenefitsRequestCarrierPlanFilter;
    }

}
