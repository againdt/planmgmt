
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CarrierBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CarrierBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IssuerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CarrierId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="InsuranceType" type="{}InsuranceType"/>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LogoFileLarge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LogoFileMedium" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LogoFileSmall" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LogoFileMediumTransparent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DisclaimerHtml" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CarrierBase", propOrder = {
    "issuerId",
    "carrierId",
    "version",
    "name",
    "companyId",
    "insuranceType",
    "state",
    "logoFileLarge",
    "logoFileMedium",
    "logoFileSmall",
    "logoFileMediumTransparent",
    "disclaimerHtml"
})
@XmlSeeAlso({
    GetCarriersPlansBenefitsResponseCarrier.class
})
public class CarrierBase {

    @XmlElementRef(name = "IssuerId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> issuerId;
    @XmlElement(name = "CarrierId")
    protected int carrierId;
    @XmlElement(name = "Version", required = true, nillable = true)
    protected String version;
    @XmlElement(name = "Name", required = true, nillable = true)
    protected String name;
    @XmlElementRef(name = "CompanyId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> companyId;
    @XmlElement(name = "InsuranceType", required = true)
    protected InsuranceType insuranceType;
    @XmlElement(name = "State", required = true, nillable = true)
    protected String state;
    @XmlElementRef(name = "LogoFileLarge", type = JAXBElement.class, required = false)
    protected JAXBElement<String> logoFileLarge;
    @XmlElementRef(name = "LogoFileMedium", type = JAXBElement.class, required = false)
    protected JAXBElement<String> logoFileMedium;
    @XmlElementRef(name = "LogoFileSmall", type = JAXBElement.class, required = false)
    protected JAXBElement<String> logoFileSmall;
    @XmlElementRef(name = "LogoFileMediumTransparent", type = JAXBElement.class, required = false)
    protected JAXBElement<String> logoFileMediumTransparent;
    @XmlElementRef(name = "DisclaimerHtml", type = JAXBElement.class, required = false)
    protected JAXBElement<String> disclaimerHtml;

    /**
     * Gets the value of the issuerId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIssuerId() {
        return issuerId;
    }

    /**
     * Sets the value of the issuerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIssuerId(JAXBElement<String> value) {
        this.issuerId = value;
    }

    /**
     * Gets the value of the carrierId property.
     * 
     */
    public int getCarrierId() {
        return carrierId;
    }

    /**
     * Sets the value of the carrierId property.
     * 
     */
    public void setCarrierId(int value) {
        this.carrierId = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the companyId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getCompanyId() {
        return companyId;
    }

    /**
     * Sets the value of the companyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setCompanyId(JAXBElement<Integer> value) {
        this.companyId = value;
    }

    /**
     * Gets the value of the insuranceType property.
     * 
     * @return
     *     possible object is
     *     {@link InsuranceType }
     *     
     */
    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    /**
     * Sets the value of the insuranceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuranceType }
     *     
     */
    public void setInsuranceType(InsuranceType value) {
        this.insuranceType = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the logoFileLarge property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLogoFileLarge() {
        return logoFileLarge;
    }

    /**
     * Sets the value of the logoFileLarge property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLogoFileLarge(JAXBElement<String> value) {
        this.logoFileLarge = value;
    }

    /**
     * Gets the value of the logoFileMedium property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLogoFileMedium() {
        return logoFileMedium;
    }

    /**
     * Sets the value of the logoFileMedium property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLogoFileMedium(JAXBElement<String> value) {
        this.logoFileMedium = value;
    }

    /**
     * Gets the value of the logoFileSmall property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLogoFileSmall() {
        return logoFileSmall;
    }

    /**
     * Sets the value of the logoFileSmall property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLogoFileSmall(JAXBElement<String> value) {
        this.logoFileSmall = value;
    }

    /**
     * Gets the value of the logoFileMediumTransparent property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLogoFileMediumTransparent() {
        return logoFileMediumTransparent;
    }

    /**
     * Sets the value of the logoFileMediumTransparent property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLogoFileMediumTransparent(JAXBElement<String> value) {
        this.logoFileMediumTransparent = value;
    }

    /**
     * Gets the value of the disclaimerHtml property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisclaimerHtml() {
        return disclaimerHtml;
    }

    /**
     * Sets the value of the disclaimerHtml property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisclaimerHtml(JAXBElement<String> value) {
        this.disclaimerHtml = value;
    }

}
