
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlanComponentBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PlanComponentBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InsuranceType" type="{}InsuranceType"/>
 *         &lt;element name="BenefitLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlanComponentBase", propOrder = {
    "insuranceType",
    "benefitLevel"
})
@XmlSeeAlso({
    GetIfpQuoteRequestPreferenceBenchmarkRulesPlanComponent.class
})
public class PlanComponentBase {

    @XmlElement(name = "InsuranceType", required = true)
    protected InsuranceType insuranceType;
    @XmlElementRef(name = "BenefitLevel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> benefitLevel;

    /**
     * Gets the value of the insuranceType property.
     * 
     * @return
     *     possible object is
     *     {@link InsuranceType }
     *     
     */
    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    /**
     * Sets the value of the insuranceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuranceType }
     *     
     */
    public void setInsuranceType(InsuranceType value) {
        this.insuranceType = value;
    }

    /**
     * Gets the value of the benefitLevel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBenefitLevel() {
        return benefitLevel;
    }

    /**
     * Sets the value of the benefitLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBenefitLevel(JAXBElement<String> value) {
        this.benefitLevel = value;
    }

}
