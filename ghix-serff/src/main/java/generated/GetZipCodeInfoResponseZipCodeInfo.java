
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetZipCodeInfo.Response.ZipCodeInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetZipCodeInfo.Response.ZipCodeInfo">
 *   &lt;complexContent>
 *     &lt;extension base="{}ZipCodeInfoBase">
 *       &lt;sequence>
 *         &lt;element name="Counties" type="{}ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.County"/>
 *         &lt;element name="Cities" type="{}ArrayOfGetZipCodeInfo.Response.ZipCodeInfo.City"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetZipCodeInfo.Response.ZipCodeInfo", propOrder = {
    "counties",
    "cities"
})
public class GetZipCodeInfoResponseZipCodeInfo
    extends ZipCodeInfoBase
{

    @XmlElement(name = "Counties", required = true, nillable = true)
    protected ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty counties;
    @XmlElement(name = "Cities", required = true, nillable = true)
    protected ArrayOfGetZipCodeInfoResponseZipCodeInfoCity cities;

    /**
     * Gets the value of the counties property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty }
     *     
     */
    public ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty getCounties() {
        return counties;
    }

    /**
     * Sets the value of the counties property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty }
     *     
     */
    public void setCounties(ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty value) {
        this.counties = value;
    }

    /**
     * Gets the value of the cities property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGetZipCodeInfoResponseZipCodeInfoCity }
     *     
     */
    public ArrayOfGetZipCodeInfoResponseZipCodeInfoCity getCities() {
        return cities;
    }

    /**
     * Sets the value of the cities property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGetZipCodeInfoResponseZipCodeInfoCity }
     *     
     */
    public void setCities(ArrayOfGetZipCodeInfoResponseZipCodeInfoCity value) {
        this.cities = value;
    }

}
