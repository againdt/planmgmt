
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Request.BenefitFilter" type="{}GetCarriersPlansBenefits.Request.BenefitFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter", propOrder = {
    "getCarriersPlansBenefitsRequestBenefitFilter"
})
public class ArrayOfGetCarriersPlansBenefitsRequestBenefitFilter {

    @XmlElement(name = "GetCarriersPlansBenefits.Request.BenefitFilter", nillable = true)
    protected List<GetCarriersPlansBenefitsRequestBenefitFilter> getCarriersPlansBenefitsRequestBenefitFilter;

    /**
     * Gets the value of the getCarriersPlansBenefitsRequestBenefitFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsRequestBenefitFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsRequestBenefitFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsRequestBenefitFilter }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsRequestBenefitFilter> getGetCarriersPlansBenefitsRequestBenefitFilter() {
        if (getCarriersPlansBenefitsRequestBenefitFilter == null) {
            getCarriersPlansBenefitsRequestBenefitFilter = new ArrayList<GetCarriersPlansBenefitsRequestBenefitFilter>();
        }
        return this.getCarriersPlansBenefitsRequestBenefitFilter;
    }

}
