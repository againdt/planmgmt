
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfCustomProduct.RatingArea.County complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfCustomProduct.RatingArea.County">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CustomProduct.RatingArea.County" type="{}CustomProduct.RatingArea.County" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfCustomProduct.RatingArea.County", propOrder = {
    "customProductRatingAreaCounty"
})
public class ArrayOfCustomProductRatingAreaCounty {

    @XmlElement(name = "CustomProduct.RatingArea.County", nillable = true)
    protected List<CustomProductRatingAreaCounty> customProductRatingAreaCounty;

    /**
     * Gets the value of the customProductRatingAreaCounty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customProductRatingAreaCounty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomProductRatingAreaCounty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CustomProductRatingAreaCounty }
     * 
     * 
     */
    public List<CustomProductRatingAreaCounty> getCustomProductRatingAreaCounty() {
        if (customProductRatingAreaCounty == null) {
            customProductRatingAreaCounty = new ArrayList<CustomProductRatingAreaCounty>();
        }
        return this.customProductRatingAreaCounty;
    }

}
