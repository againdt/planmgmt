
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint" type="{}GetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint", propOrder = {
    "getIfpQuoteResponsePlanDetailBenefitCoverageViewPoint"
})
public class ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint {

    @XmlElement(name = "GetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint", nillable = true)
    protected List<GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint> getIfpQuoteResponsePlanDetailBenefitCoverageViewPoint;

    /**
     * Gets the value of the getIfpQuoteResponsePlanDetailBenefitCoverageViewPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteResponsePlanDetailBenefitCoverageViewPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint }
     * 
     * 
     */
    public List<GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint> getGetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint() {
        if (getIfpQuoteResponsePlanDetailBenefitCoverageViewPoint == null) {
            getIfpQuoteResponsePlanDetailBenefitCoverageViewPoint = new ArrayList<GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint>();
        }
        return this.getIfpQuoteResponsePlanDetailBenefitCoverageViewPoint;
    }

}
