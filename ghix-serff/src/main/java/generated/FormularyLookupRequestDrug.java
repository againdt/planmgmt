
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FormularyLookup.Request.Drug complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FormularyLookup.Request.Drug">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FilterType" type="{}FormularyLookup.FilterType"/>
 *         &lt;element name="DrugName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RxCUIs" type="{}RxCUIsBase" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FormularyLookup.Request.Drug", propOrder = {
    "filterType",
    "drugName",
    "rxCUIs"
})
public class FormularyLookupRequestDrug {

    @XmlElement(name = "FilterType", required = true)
    protected FormularyLookupFilterType filterType;
    @XmlElementRef(name = "DrugName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> drugName;
    @XmlElementRef(name = "RxCUIs", type = JAXBElement.class, required = false)
    protected JAXBElement<RxCUIsBase> rxCUIs;

    /**
     * Gets the value of the filterType property.
     * 
     * @return
     *     possible object is
     *     {@link FormularyLookupFilterType }
     *     
     */
    public FormularyLookupFilterType getFilterType() {
        return filterType;
    }

    /**
     * Sets the value of the filterType property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormularyLookupFilterType }
     *     
     */
    public void setFilterType(FormularyLookupFilterType value) {
        this.filterType = value;
    }

    /**
     * Gets the value of the drugName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDrugName() {
        return drugName;
    }

    /**
     * Sets the value of the drugName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDrugName(JAXBElement<String> value) {
        this.drugName = value;
    }

    /**
     * Gets the value of the rxCUIs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}
     *     
     */
    public JAXBElement<RxCUIsBase> getRxCUIs() {
        return rxCUIs;
    }

    /**
     * Sets the value of the rxCUIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}
     *     
     */
    public void setRxCUIs(JAXBElement<RxCUIsBase> value) {
        this.rxCUIs = value;
    }

}
