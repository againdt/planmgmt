
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint">
 *   &lt;complexContent>
 *     &lt;extension base="{}ViewPointBase">
 *       &lt;sequence>
 *         &lt;element name="Services" type="{}ArrayOfGetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint.Service" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response.PlanDetail.Benefit.Coverage.ViewPoint", propOrder = {
    "services"
})
public class GetIfpQuoteResponsePlanDetailBenefitCoverageViewPoint
    extends ViewPointBase
{

    @XmlElementRef(name = "Services", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService> services;

    /**
     * Gets the value of the services property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService> getServices() {
        return services;
    }

    /**
     * Sets the value of the services property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService }{@code >}
     *     
     */
    public void setServices(JAXBElement<ArrayOfGetIfpQuoteResponsePlanDetailBenefitCoverageViewPointService> value) {
        this.services = value;
    }

}
