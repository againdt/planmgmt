
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpQuote.Response.Quote.DroppedCarrier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpQuote.Response.Quote.DroppedCarrier">
 *   &lt;complexContent>
 *     &lt;extension base="{}DroppedCarrierBase">
 *       &lt;sequence>
 *         &lt;element name="CarrierDetails" type="{}GetIfpQuote.Response.CarrierDetail" minOccurs="0"/>
 *         &lt;element name="DroppedPlans" type="{}ArrayOfGetIfpQuote.Response.Quote.DroppedCarrier.DroppedPlan" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpQuote.Response.Quote.DroppedCarrier", propOrder = {
    "carrierDetails",
    "droppedPlans"
})
public class GetIfpQuoteResponseQuoteDroppedCarrier
    extends DroppedCarrierBase
{

    @XmlElementRef(name = "CarrierDetails", type = JAXBElement.class, required = false)
    protected JAXBElement<GetIfpQuoteResponseCarrierDetail> carrierDetails;
    @XmlElementRef(name = "DroppedPlans", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan> droppedPlans;

    /**
     * Gets the value of the carrierDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseCarrierDetail }{@code >}
     *     
     */
    public JAXBElement<GetIfpQuoteResponseCarrierDetail> getCarrierDetails() {
        return carrierDetails;
    }

    /**
     * Sets the value of the carrierDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetIfpQuoteResponseCarrierDetail }{@code >}
     *     
     */
    public void setCarrierDetails(JAXBElement<GetIfpQuoteResponseCarrierDetail> value) {
        this.carrierDetails = value;
    }

    /**
     * Gets the value of the droppedPlans property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan> getDroppedPlans() {
        return droppedPlans;
    }

    /**
     * Sets the value of the droppedPlans property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan }{@code >}
     *     
     */
    public void setDroppedPlans(JAXBElement<ArrayOfGetIfpQuoteResponseQuoteDroppedCarrierDroppedPlan> value) {
        this.droppedPlans = value;
    }

}
