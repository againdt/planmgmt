
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CarrierId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProductType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubmissionId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PlanRates" type="{}ArrayOfGetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier.PlanRate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCustomProductsQuote.Response.Quote.FamilyQuote.Carrier", propOrder = {
    "carrierId",
    "productType",
    "companyName",
    "submissionId",
    "planRates"
})
public class GetCustomProductsQuoteResponseQuoteFamilyQuoteCarrier {

    @XmlElement(name = "CarrierId", required = true, nillable = true)
    protected String carrierId;
    @XmlElement(name = "ProductType", required = true, nillable = true)
    protected String productType;
    @XmlElementRef(name = "CompanyName", type = JAXBElement.class, required = false)
    protected JAXBElement<String> companyName;
    @XmlElementRef(name = "SubmissionId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> submissionId;
    @XmlElementRef(name = "PlanRates", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate> planRates;

    /**
     * Gets the value of the carrierId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarrierId() {
        return carrierId;
    }

    /**
     * Sets the value of the carrierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarrierId(String value) {
        this.carrierId = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompanyName(JAXBElement<String> value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the submissionId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSubmissionId() {
        return submissionId;
    }

    /**
     * Sets the value of the submissionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSubmissionId(JAXBElement<Integer> value) {
        this.submissionId = value;
    }

    /**
     * Gets the value of the planRates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate> getPlanRates() {
        return planRates;
    }

    /**
     * Sets the value of the planRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate }{@code >}
     *     
     */
    public void setPlanRates(JAXBElement<ArrayOfGetCustomProductsQuoteResponseQuoteFamilyQuoteCarrierPlanRate> value) {
        this.planRates = value;
    }

}
