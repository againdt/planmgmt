
package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetZipCodeInfo.Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetZipCodeInfo.Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccessKeys" type="{}GetZipCodeInfo.Request.AccessKey"/>
 *         &lt;element name="Inputs" type="{}GetZipCodeInfo.Request.Input"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetZipCodeInfo.Request", propOrder = {
    "accessKeys",
    "inputs"
})
public class GetZipCodeInfoRequest {

    @XmlElement(name = "AccessKeys", required = true, nillable = true)
    protected GetZipCodeInfoRequestAccessKey accessKeys;
    @XmlElement(name = "Inputs", required = true, nillable = true)
    protected GetZipCodeInfoRequestInput inputs;

    /**
     * Gets the value of the accessKeys property.
     * 
     * @return
     *     possible object is
     *     {@link GetZipCodeInfoRequestAccessKey }
     *     
     */
    public GetZipCodeInfoRequestAccessKey getAccessKeys() {
        return accessKeys;
    }

    /**
     * Sets the value of the accessKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetZipCodeInfoRequestAccessKey }
     *     
     */
    public void setAccessKeys(GetZipCodeInfoRequestAccessKey value) {
        this.accessKeys = value;
    }

    /**
     * Gets the value of the inputs property.
     * 
     * @return
     *     possible object is
     *     {@link GetZipCodeInfoRequestInput }
     *     
     */
    public GetZipCodeInfoRequestInput getInputs() {
        return inputs;
    }

    /**
     * Sets the value of the inputs property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetZipCodeInfoRequestInput }
     *     
     */
    public void setInputs(GetZipCodeInfoRequestInput value) {
        this.inputs = value;
    }

}
