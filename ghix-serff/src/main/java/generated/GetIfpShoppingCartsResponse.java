
package generated;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetIfpShoppingCarts.Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetIfpShoppingCarts.Response">
 *   &lt;complexContent>
 *     &lt;extension base="{}ResponseBase">
 *       &lt;sequence>
 *         &lt;element name="ShoppingCarts" type="{}IfpShoppingCartsBase" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetIfpShoppingCarts.Response", propOrder = {
    "shoppingCarts"
})
public class GetIfpShoppingCartsResponse
    extends ResponseBase
{

    @XmlElementRef(name = "ShoppingCarts", type = JAXBElement.class, required = false)
    protected JAXBElement<IfpShoppingCartsBase> shoppingCarts;

    /**
     * Gets the value of the shoppingCarts property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link IfpShoppingCartsBase }{@code >}
     *     
     */
    public JAXBElement<IfpShoppingCartsBase> getShoppingCarts() {
        return shoppingCarts;
    }

    /**
     * Sets the value of the shoppingCarts property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link IfpShoppingCartsBase }{@code >}
     *     
     */
    public void setShoppingCarts(JAXBElement<IfpShoppingCartsBase> value) {
        this.shoppingCarts = value;
    }

}
