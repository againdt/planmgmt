
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.Request.Preference.PlanFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.Request.Preference.PlanFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.Request.Preference.PlanFilter" type="{}GetIfpQuote.Request.Preference.PlanFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.Request.Preference.PlanFilter", propOrder = {
    "getIfpQuoteRequestPreferencePlanFilter"
})
public class ArrayOfGetIfpQuoteRequestPreferencePlanFilter {

    @XmlElement(name = "GetIfpQuote.Request.Preference.PlanFilter", nillable = true)
    protected List<GetIfpQuoteRequestPreferencePlanFilter> getIfpQuoteRequestPreferencePlanFilter;

    /**
     * Gets the value of the getIfpQuoteRequestPreferencePlanFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteRequestPreferencePlanFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteRequestPreferencePlanFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteRequestPreferencePlanFilter }
     * 
     * 
     */
    public List<GetIfpQuoteRequestPreferencePlanFilter> getGetIfpQuoteRequestPreferencePlanFilter() {
        if (getIfpQuoteRequestPreferencePlanFilter == null) {
            getIfpQuoteRequestPreferencePlanFilter = new ArrayList<GetIfpQuoteRequestPreferencePlanFilter>();
        }
        return this.getIfpQuoteRequestPreferencePlanFilter;
    }

}
