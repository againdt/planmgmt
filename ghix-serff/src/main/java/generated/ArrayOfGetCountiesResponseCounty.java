
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCounties.Response.County complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCounties.Response.County">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCounties.Response.County" type="{}GetCounties.Response.County" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCounties.Response.County", propOrder = {
    "getCountiesResponseCounty"
})
public class ArrayOfGetCountiesResponseCounty {

    @XmlElement(name = "GetCounties.Response.County", nillable = true)
    protected List<GetCountiesResponseCounty> getCountiesResponseCounty;

    /**
     * Gets the value of the getCountiesResponseCounty property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCountiesResponseCounty property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCountiesResponseCounty().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCountiesResponseCounty }
     * 
     * 
     */
    public List<GetCountiesResponseCounty> getGetCountiesResponseCounty() {
        if (getCountiesResponseCounty == null) {
            getCountiesResponseCounty = new ArrayList<GetCountiesResponseCounty>();
        }
        return this.getCountiesResponseCounty;
    }

}
