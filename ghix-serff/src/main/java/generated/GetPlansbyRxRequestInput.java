
package generated;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for GetPlansbyRx.Request.Input complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPlansbyRx.Request.Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="RxCUIs" type="{}RxCUIsBase" minOccurs="0"/>
 *         &lt;element name="RxMatch" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="Viewpoint" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DropPlans" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="AddOns" type="{}ArrayOfGetPlansbyRx.AddOn" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPlansbyRx.Request.Input", propOrder = {
    "state",
    "effectiveDate",
    "rxCUIs",
    "rxMatch",
    "viewpoint",
    "dropPlans",
    "addOns"
})
public class GetPlansbyRxRequestInput {

    @XmlElement(name = "State", required = true, nillable = true)
    protected String state;
    @XmlElement(name = "EffectiveDate", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElementRef(name = "RxCUIs", type = JAXBElement.class, required = false)
    protected JAXBElement<RxCUIsBase> rxCUIs;
    @XmlElement(name = "RxMatch")
    protected BigDecimal rxMatch;
    @XmlElement(name = "Viewpoint", required = true, nillable = true)
    protected String viewpoint;
    @XmlElement(name = "DropPlans")
    protected boolean dropPlans;
    @XmlElementRef(name = "AddOns", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetPlansbyRxAddOn> addOns;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the rxCUIs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}
     *     
     */
    public JAXBElement<RxCUIsBase> getRxCUIs() {
        return rxCUIs;
    }

    /**
     * Sets the value of the rxCUIs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RxCUIsBase }{@code >}
     *     
     */
    public void setRxCUIs(JAXBElement<RxCUIsBase> value) {
        this.rxCUIs = value;
    }

    /**
     * Gets the value of the rxMatch property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRxMatch() {
        return rxMatch;
    }

    /**
     * Sets the value of the rxMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRxMatch(BigDecimal value) {
        this.rxMatch = value;
    }

    /**
     * Gets the value of the viewpoint property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getViewpoint() {
        return viewpoint;
    }

    /**
     * Sets the value of the viewpoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setViewpoint(String value) {
        this.viewpoint = value;
    }

    /**
     * Gets the value of the dropPlans property.
     * 
     */
    public boolean isDropPlans() {
        return dropPlans;
    }

    /**
     * Sets the value of the dropPlans property.
     * 
     */
    public void setDropPlans(boolean value) {
        this.dropPlans = value;
    }

    /**
     * Gets the value of the addOns property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxAddOn }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetPlansbyRxAddOn> getAddOns() {
        return addOns;
    }

    /**
     * Sets the value of the addOns property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetPlansbyRxAddOn }{@code >}
     *     
     */
    public void setAddOns(JAXBElement<ArrayOfGetPlansbyRxAddOn> value) {
        this.addOns = value;
    }

}
