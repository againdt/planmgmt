
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfIfpShoppingCart.ProductChoice.PlanChoices.StatusChange complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfIfpShoppingCart.ProductChoice.PlanChoices.StatusChange">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IfpShoppingCart.ProductChoice.PlanChoices.StatusChange" type="{}IfpShoppingCart.ProductChoice.PlanChoices.StatusChange" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfIfpShoppingCart.ProductChoice.PlanChoices.StatusChange", propOrder = {
    "ifpShoppingCartProductChoicePlanChoicesStatusChange"
})
public class ArrayOfIfpShoppingCartProductChoicePlanChoicesStatusChange {

    @XmlElement(name = "IfpShoppingCart.ProductChoice.PlanChoices.StatusChange", nillable = true)
    protected List<IfpShoppingCartProductChoicePlanChoicesStatusChange> ifpShoppingCartProductChoicePlanChoicesStatusChange;

    /**
     * Gets the value of the ifpShoppingCartProductChoicePlanChoicesStatusChange property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ifpShoppingCartProductChoicePlanChoicesStatusChange property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIfpShoppingCartProductChoicePlanChoicesStatusChange().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IfpShoppingCartProductChoicePlanChoicesStatusChange }
     * 
     * 
     */
    public List<IfpShoppingCartProductChoicePlanChoicesStatusChange> getIfpShoppingCartProductChoicePlanChoicesStatusChange() {
        if (ifpShoppingCartProductChoicePlanChoicesStatusChange == null) {
            ifpShoppingCartProductChoicePlanChoicesStatusChange = new ArrayList<IfpShoppingCartProductChoicePlanChoicesStatusChange>();
        }
        return this.ifpShoppingCartProductChoicePlanChoicesStatusChange;
    }

}
