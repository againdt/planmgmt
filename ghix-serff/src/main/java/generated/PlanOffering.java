
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PlanOffering.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PlanOffering">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NoRestriction"/>
 *     &lt;enumeration value="OneMemberOnly"/>
 *     &lt;enumeration value="OneAdultOnly"/>
 *     &lt;enumeration value="OneChildOnly"/>
 *     &lt;enumeration value="MultiMemberOnly"/>
 *     &lt;enumeration value="AdultsOnly"/>
 *     &lt;enumeration value="ChildrenOnly"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PlanOffering")
@XmlEnum
public enum PlanOffering {

    @XmlEnumValue("NoRestriction")
    NO_RESTRICTION("NoRestriction"),
    @XmlEnumValue("OneMemberOnly")
    ONE_MEMBER_ONLY("OneMemberOnly"),
    @XmlEnumValue("OneAdultOnly")
    ONE_ADULT_ONLY("OneAdultOnly"),
    @XmlEnumValue("OneChildOnly")
    ONE_CHILD_ONLY("OneChildOnly"),
    @XmlEnumValue("MultiMemberOnly")
    MULTI_MEMBER_ONLY("MultiMemberOnly"),
    @XmlEnumValue("AdultsOnly")
    ADULTS_ONLY("AdultsOnly"),
    @XmlEnumValue("ChildrenOnly")
    CHILDREN_ONLY("ChildrenOnly");
    private final String value;

    PlanOffering(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PlanOffering fromValue(String v) {
        for (PlanOffering c: PlanOffering.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
