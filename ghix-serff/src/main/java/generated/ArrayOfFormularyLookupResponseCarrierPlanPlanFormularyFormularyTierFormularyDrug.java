
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug" type="{}FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug", propOrder = {
    "formularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug"
})
public class ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug {

    @XmlElement(name = "FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug", nillable = true)
    protected List<FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> formularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug;

    /**
     * Gets the value of the formularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }
     * 
     * 
     */
    public List<FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> getFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug() {
        if (formularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug == null) {
            formularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug = new ArrayList<FormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug>();
        }
        return this.formularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug;
    }

}
