
package generated;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScenarioBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ScenarioBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InsuranceType" type="{}InsuranceType"/>
 *         &lt;element name="ContributionType" type="{}ContributionType"/>
 *         &lt;element name="DepContributionType" type="{}ContributionType" minOccurs="0"/>
 *         &lt;element name="EmpAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="DepAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="ApplyExcessToDep" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Towards" type="{}EmployerContributionTowards"/>
 *         &lt;element name="EHBRateOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="NonSmokerRateOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="NonSmokerDepRateOnly" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SameCarrierOnly" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="PlanType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BenefitLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SponsoredCarrierId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SponsoredPlanId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ScenarioBase", propOrder = {
    "insuranceType",
    "contributionType",
    "depContributionType",
    "empAmount",
    "depAmount",
    "applyExcessToDep",
    "towards",
    "ehbRateOnly",
    "nonSmokerRateOnly",
    "nonSmokerDepRateOnly",
    "sameCarrierOnly",
    "planType",
    "benefitLevel",
    "sponsoredCarrierId",
    "sponsoredPlanId",
    "note"
})
@XmlSeeAlso({
    GroupScenario.class
})
public class ScenarioBase {

    @XmlElement(name = "InsuranceType", required = true)
    protected InsuranceType insuranceType;
    @XmlList
    @XmlElement(name = "ContributionType", required = true)
    protected List<String> contributionType;
    @XmlElementRef(name = "DepContributionType", type = JAXBElement.class, required = false)
    protected List<JAXBElement<List<String>>> depContributionType;
    @XmlElement(name = "EmpAmount", required = true)
    protected BigDecimal empAmount;
    @XmlElement(name = "DepAmount", required = true)
    protected BigDecimal depAmount;
    @XmlElement(name = "ApplyExcessToDep")
    protected boolean applyExcessToDep;
    @XmlList
    @XmlElement(name = "Towards", required = true)
    protected List<String> towards;
    @XmlElement(name = "EHBRateOnly")
    protected boolean ehbRateOnly;
    @XmlElementRef(name = "NonSmokerRateOnly", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> nonSmokerRateOnly;
    @XmlElementRef(name = "NonSmokerDepRateOnly", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> nonSmokerDepRateOnly;
    @XmlElement(name = "SameCarrierOnly")
    protected boolean sameCarrierOnly;
    @XmlElementRef(name = "PlanType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planType;
    @XmlElementRef(name = "BenefitLevel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> benefitLevel;
    @XmlElementRef(name = "SponsoredCarrierId", type = JAXBElement.class, required = false)
    protected JAXBElement<Integer> sponsoredCarrierId;
    @XmlElementRef(name = "SponsoredPlanId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> sponsoredPlanId;
    @XmlElementRef(name = "Note", type = JAXBElement.class, required = false)
    protected JAXBElement<String> note;

    /**
     * Gets the value of the insuranceType property.
     * 
     * @return
     *     possible object is
     *     {@link InsuranceType }
     *     
     */
    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    /**
     * Sets the value of the insuranceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link InsuranceType }
     *     
     */
    public void setInsuranceType(InsuranceType value) {
        this.insuranceType = value;
    }

    /**
     * Gets the value of the contributionType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contributionType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContributionType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getContributionType() {
        if (contributionType == null) {
            contributionType = new ArrayList<String>();
        }
        return this.contributionType;
    }

    /**
     * Gets the value of the depContributionType property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the depContributionType property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDepContributionType().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link JAXBElement }{@code <}{@link List }{@code <}{@link String }{@code >}{@code >}
     * 
     * 
     */
    public List<JAXBElement<List<String>>> getDepContributionType() {
        if (depContributionType == null) {
            depContributionType = new ArrayList<JAXBElement<List<String>>>();
        }
        return this.depContributionType;
    }

    /**
     * Gets the value of the empAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getEmpAmount() {
        return empAmount;
    }

    /**
     * Sets the value of the empAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setEmpAmount(BigDecimal value) {
        this.empAmount = value;
    }

    /**
     * Gets the value of the depAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepAmount() {
        return depAmount;
    }

    /**
     * Sets the value of the depAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepAmount(BigDecimal value) {
        this.depAmount = value;
    }

    /**
     * Gets the value of the applyExcessToDep property.
     * 
     */
    public boolean isApplyExcessToDep() {
        return applyExcessToDep;
    }

    /**
     * Sets the value of the applyExcessToDep property.
     * 
     */
    public void setApplyExcessToDep(boolean value) {
        this.applyExcessToDep = value;
    }

    /**
     * Gets the value of the towards property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the towards property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTowards().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTowards() {
        if (towards == null) {
            towards = new ArrayList<String>();
        }
        return this.towards;
    }

    /**
     * Gets the value of the ehbRateOnly property.
     * 
     */
    public boolean isEHBRateOnly() {
        return ehbRateOnly;
    }

    /**
     * Sets the value of the ehbRateOnly property.
     * 
     */
    public void setEHBRateOnly(boolean value) {
        this.ehbRateOnly = value;
    }

    /**
     * Gets the value of the nonSmokerRateOnly property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getNonSmokerRateOnly() {
        return nonSmokerRateOnly;
    }

    /**
     * Sets the value of the nonSmokerRateOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setNonSmokerRateOnly(JAXBElement<Boolean> value) {
        this.nonSmokerRateOnly = value;
    }

    /**
     * Gets the value of the nonSmokerDepRateOnly property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getNonSmokerDepRateOnly() {
        return nonSmokerDepRateOnly;
    }

    /**
     * Sets the value of the nonSmokerDepRateOnly property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setNonSmokerDepRateOnly(JAXBElement<Boolean> value) {
        this.nonSmokerDepRateOnly = value;
    }

    /**
     * Gets the value of the sameCarrierOnly property.
     * 
     */
    public boolean isSameCarrierOnly() {
        return sameCarrierOnly;
    }

    /**
     * Sets the value of the sameCarrierOnly property.
     * 
     */
    public void setSameCarrierOnly(boolean value) {
        this.sameCarrierOnly = value;
    }

    /**
     * Gets the value of the planType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanType() {
        return planType;
    }

    /**
     * Sets the value of the planType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanType(JAXBElement<String> value) {
        this.planType = value;
    }

    /**
     * Gets the value of the benefitLevel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBenefitLevel() {
        return benefitLevel;
    }

    /**
     * Sets the value of the benefitLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBenefitLevel(JAXBElement<String> value) {
        this.benefitLevel = value;
    }

    /**
     * Gets the value of the sponsoredCarrierId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public JAXBElement<Integer> getSponsoredCarrierId() {
        return sponsoredCarrierId;
    }

    /**
     * Sets the value of the sponsoredCarrierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Integer }{@code >}
     *     
     */
    public void setSponsoredCarrierId(JAXBElement<Integer> value) {
        this.sponsoredCarrierId = value;
    }

    /**
     * Gets the value of the sponsoredPlanId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSponsoredPlanId() {
        return sponsoredPlanId;
    }

    /**
     * Sets the value of the sponsoredPlanId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSponsoredPlanId(JAXBElement<String> value) {
        this.sponsoredPlanId = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNote(JAXBElement<String> value) {
        this.note = value;
    }

}
