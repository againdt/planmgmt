
package generated;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RateBase complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RateBase">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlanId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Visibility" type="{}PlanVisibility"/>
 *         &lt;element name="RatingMethod" type="{}RatingMethod"/>
 *         &lt;element name="RatingAnnotation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RatingAreaBase" type="{}GroupRatingAreaBase"/>
 *         &lt;element name="RatingAreaId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Rate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="DepRate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="SmokerSurcharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepSmokerSurcharge" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SSRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SD1Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SD2Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SD3Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="F1Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="F2Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="F3Rate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="SRateER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepRateER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepSD1RateER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepSD2RateER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepSD3RateER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepSSRateER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepF1RateER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepF2RateER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="DepF3RateER" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateBase", propOrder = {
    "planId",
    "visibility",
    "ratingMethod",
    "ratingAnnotation",
    "ratingAreaBase",
    "ratingAreaId",
    "rate",
    "depRate",
    "smokerSurcharge",
    "depSmokerSurcharge",
    "sRate",
    "ssRate",
    "sd1Rate",
    "sd2Rate",
    "sd3Rate",
    "f1Rate",
    "f2Rate",
    "f3Rate",
    "sRateER",
    "depRateER",
    "depSD1RateER",
    "depSD2RateER",
    "depSD3RateER",
    "depSSRateER",
    "depF1RateER",
    "depF2RateER",
    "depF3RateER"
})
@XmlSeeAlso({
    GetGroupQuoteResponseQuoteEmployeePlanCarrierRate.class
})
public class RateBase {

    @XmlElement(name = "PlanId", required = true, nillable = true)
    protected String planId;
    @XmlElement(name = "Visibility", required = true)
    protected PlanVisibility visibility;
    @XmlElement(name = "RatingMethod", required = true)
    protected RatingMethod ratingMethod;
    @XmlElementRef(name = "RatingAnnotation", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ratingAnnotation;
    @XmlElement(name = "RatingAreaBase", required = true)
    protected GroupRatingAreaBase ratingAreaBase;
    @XmlElement(name = "RatingAreaId")
    protected int ratingAreaId;
    @XmlElement(name = "Rate", required = true)
    protected BigDecimal rate;
    @XmlElement(name = "DepRate", required = true)
    protected BigDecimal depRate;
    @XmlElement(name = "SmokerSurcharge")
    protected BigDecimal smokerSurcharge;
    @XmlElement(name = "DepSmokerSurcharge")
    protected BigDecimal depSmokerSurcharge;
    @XmlElement(name = "SRate")
    protected BigDecimal sRate;
    @XmlElement(name = "SSRate")
    protected BigDecimal ssRate;
    @XmlElement(name = "SD1Rate")
    protected BigDecimal sd1Rate;
    @XmlElement(name = "SD2Rate")
    protected BigDecimal sd2Rate;
    @XmlElement(name = "SD3Rate")
    protected BigDecimal sd3Rate;
    @XmlElement(name = "F1Rate")
    protected BigDecimal f1Rate;
    @XmlElement(name = "F2Rate")
    protected BigDecimal f2Rate;
    @XmlElement(name = "F3Rate")
    protected BigDecimal f3Rate;
    @XmlElement(name = "SRateER")
    protected BigDecimal sRateER;
    @XmlElement(name = "DepRateER")
    protected BigDecimal depRateER;
    @XmlElement(name = "DepSD1RateER")
    protected BigDecimal depSD1RateER;
    @XmlElement(name = "DepSD2RateER")
    protected BigDecimal depSD2RateER;
    @XmlElement(name = "DepSD3RateER")
    protected BigDecimal depSD3RateER;
    @XmlElement(name = "DepSSRateER")
    protected BigDecimal depSSRateER;
    @XmlElement(name = "DepF1RateER")
    protected BigDecimal depF1RateER;
    @XmlElement(name = "DepF2RateER")
    protected BigDecimal depF2RateER;
    @XmlElement(name = "DepF3RateER")
    protected BigDecimal depF3RateER;

    /**
     * Gets the value of the planId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanId() {
        return planId;
    }

    /**
     * Sets the value of the planId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanId(String value) {
        this.planId = value;
    }

    /**
     * Gets the value of the visibility property.
     * 
     * @return
     *     possible object is
     *     {@link PlanVisibility }
     *     
     */
    public PlanVisibility getVisibility() {
        return visibility;
    }

    /**
     * Sets the value of the visibility property.
     * 
     * @param value
     *     allowed object is
     *     {@link PlanVisibility }
     *     
     */
    public void setVisibility(PlanVisibility value) {
        this.visibility = value;
    }

    /**
     * Gets the value of the ratingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link RatingMethod }
     *     
     */
    public RatingMethod getRatingMethod() {
        return ratingMethod;
    }

    /**
     * Sets the value of the ratingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link RatingMethod }
     *     
     */
    public void setRatingMethod(RatingMethod value) {
        this.ratingMethod = value;
    }

    /**
     * Gets the value of the ratingAnnotation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRatingAnnotation() {
        return ratingAnnotation;
    }

    /**
     * Sets the value of the ratingAnnotation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRatingAnnotation(JAXBElement<String> value) {
        this.ratingAnnotation = value;
    }

    /**
     * Gets the value of the ratingAreaBase property.
     * 
     * @return
     *     possible object is
     *     {@link GroupRatingAreaBase }
     *     
     */
    public GroupRatingAreaBase getRatingAreaBase() {
        return ratingAreaBase;
    }

    /**
     * Sets the value of the ratingAreaBase property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupRatingAreaBase }
     *     
     */
    public void setRatingAreaBase(GroupRatingAreaBase value) {
        this.ratingAreaBase = value;
    }

    /**
     * Gets the value of the ratingAreaId property.
     * 
     */
    public int getRatingAreaId() {
        return ratingAreaId;
    }

    /**
     * Sets the value of the ratingAreaId property.
     * 
     */
    public void setRatingAreaId(int value) {
        this.ratingAreaId = value;
    }

    /**
     * Gets the value of the rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRate(BigDecimal value) {
        this.rate = value;
    }

    /**
     * Gets the value of the depRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepRate() {
        return depRate;
    }

    /**
     * Sets the value of the depRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepRate(BigDecimal value) {
        this.depRate = value;
    }

    /**
     * Gets the value of the smokerSurcharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSmokerSurcharge() {
        return smokerSurcharge;
    }

    /**
     * Sets the value of the smokerSurcharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSmokerSurcharge(BigDecimal value) {
        this.smokerSurcharge = value;
    }

    /**
     * Gets the value of the depSmokerSurcharge property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepSmokerSurcharge() {
        return depSmokerSurcharge;
    }

    /**
     * Sets the value of the depSmokerSurcharge property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepSmokerSurcharge(BigDecimal value) {
        this.depSmokerSurcharge = value;
    }

    /**
     * Gets the value of the sRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRate() {
        return sRate;
    }

    /**
     * Sets the value of the sRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRate(BigDecimal value) {
        this.sRate = value;
    }

    /**
     * Gets the value of the ssRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSSRate() {
        return ssRate;
    }

    /**
     * Sets the value of the ssRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSSRate(BigDecimal value) {
        this.ssRate = value;
    }

    /**
     * Gets the value of the sd1Rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSD1Rate() {
        return sd1Rate;
    }

    /**
     * Sets the value of the sd1Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSD1Rate(BigDecimal value) {
        this.sd1Rate = value;
    }

    /**
     * Gets the value of the sd2Rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSD2Rate() {
        return sd2Rate;
    }

    /**
     * Sets the value of the sd2Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSD2Rate(BigDecimal value) {
        this.sd2Rate = value;
    }

    /**
     * Gets the value of the sd3Rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSD3Rate() {
        return sd3Rate;
    }

    /**
     * Sets the value of the sd3Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSD3Rate(BigDecimal value) {
        this.sd3Rate = value;
    }

    /**
     * Gets the value of the f1Rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getF1Rate() {
        return f1Rate;
    }

    /**
     * Sets the value of the f1Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setF1Rate(BigDecimal value) {
        this.f1Rate = value;
    }

    /**
     * Gets the value of the f2Rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getF2Rate() {
        return f2Rate;
    }

    /**
     * Sets the value of the f2Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setF2Rate(BigDecimal value) {
        this.f2Rate = value;
    }

    /**
     * Gets the value of the f3Rate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getF3Rate() {
        return f3Rate;
    }

    /**
     * Sets the value of the f3Rate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setF3Rate(BigDecimal value) {
        this.f3Rate = value;
    }

    /**
     * Gets the value of the sRateER property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSRateER() {
        return sRateER;
    }

    /**
     * Sets the value of the sRateER property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSRateER(BigDecimal value) {
        this.sRateER = value;
    }

    /**
     * Gets the value of the depRateER property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepRateER() {
        return depRateER;
    }

    /**
     * Sets the value of the depRateER property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepRateER(BigDecimal value) {
        this.depRateER = value;
    }

    /**
     * Gets the value of the depSD1RateER property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepSD1RateER() {
        return depSD1RateER;
    }

    /**
     * Sets the value of the depSD1RateER property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepSD1RateER(BigDecimal value) {
        this.depSD1RateER = value;
    }

    /**
     * Gets the value of the depSD2RateER property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepSD2RateER() {
        return depSD2RateER;
    }

    /**
     * Sets the value of the depSD2RateER property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepSD2RateER(BigDecimal value) {
        this.depSD2RateER = value;
    }

    /**
     * Gets the value of the depSD3RateER property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepSD3RateER() {
        return depSD3RateER;
    }

    /**
     * Sets the value of the depSD3RateER property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepSD3RateER(BigDecimal value) {
        this.depSD3RateER = value;
    }

    /**
     * Gets the value of the depSSRateER property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepSSRateER() {
        return depSSRateER;
    }

    /**
     * Sets the value of the depSSRateER property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepSSRateER(BigDecimal value) {
        this.depSSRateER = value;
    }

    /**
     * Gets the value of the depF1RateER property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepF1RateER() {
        return depF1RateER;
    }

    /**
     * Sets the value of the depF1RateER property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepF1RateER(BigDecimal value) {
        this.depF1RateER = value;
    }

    /**
     * Gets the value of the depF2RateER property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepF2RateER() {
        return depF2RateER;
    }

    /**
     * Sets the value of the depF2RateER property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepF2RateER(BigDecimal value) {
        this.depF2RateER = value;
    }

    /**
     * Gets the value of the depF3RateER property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepF3RateER() {
        return depF3RateER;
    }

    /**
     * Sets the value of the depF3RateER property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepF3RateER(BigDecimal value) {
        this.depF3RateER = value;
    }

}
