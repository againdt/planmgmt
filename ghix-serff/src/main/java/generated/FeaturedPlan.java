
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FeaturedPlan.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FeaturedPlan">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NoDesignation"/>
 *     &lt;enumeration value="Designate"/>
 *     &lt;enumeration value="Filter"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FeaturedPlan")
@XmlEnum
public enum FeaturedPlan {

    @XmlEnumValue("NoDesignation")
    NO_DESIGNATION("NoDesignation"),
    @XmlEnumValue("Designate")
    DESIGNATE("Designate"),
    @XmlEnumValue("Filter")
    FILTER("Filter");
    private final String value;

    FeaturedPlan(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static FeaturedPlan fromValue(String v) {
        for (FeaturedPlan c: FeaturedPlan.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
