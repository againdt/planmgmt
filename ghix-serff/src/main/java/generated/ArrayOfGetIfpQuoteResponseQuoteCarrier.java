
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetIfpQuote.Response.Quote.Carrier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetIfpQuote.Response.Quote.Carrier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetIfpQuote.Response.Quote.Carrier" type="{}GetIfpQuote.Response.Quote.Carrier" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetIfpQuote.Response.Quote.Carrier", propOrder = {
    "getIfpQuoteResponseQuoteCarrier"
})
public class ArrayOfGetIfpQuoteResponseQuoteCarrier {

    @XmlElement(name = "GetIfpQuote.Response.Quote.Carrier", nillable = true)
    protected List<GetIfpQuoteResponseQuoteCarrier> getIfpQuoteResponseQuoteCarrier;

    /**
     * Gets the value of the getIfpQuoteResponseQuoteCarrier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getIfpQuoteResponseQuoteCarrier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetIfpQuoteResponseQuoteCarrier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetIfpQuoteResponseQuoteCarrier }
     * 
     * 
     */
    public List<GetIfpQuoteResponseQuoteCarrier> getGetIfpQuoteResponseQuoteCarrier() {
        if (getIfpQuoteResponseQuoteCarrier == null) {
            getIfpQuoteResponseQuoteCarrier = new ArrayList<GetIfpQuoteResponseQuoteCarrier>();
        }
        return this.getIfpQuoteResponseQuoteCarrier;
    }

}
