
package generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LevelOfDetails.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LevelOfDetails">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Benefits"/>
 *     &lt;enumeration value="CoverageTypes"/>
 *     &lt;enumeration value="ViewPoints"/>
 *     &lt;enumeration value="Services"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "LevelOfDetails")
@XmlEnum
public enum LevelOfDetails {

    @XmlEnumValue("Benefits")
    BENEFITS("Benefits"),
    @XmlEnumValue("CoverageTypes")
    COVERAGE_TYPES("CoverageTypes"),
    @XmlEnumValue("ViewPoints")
    VIEW_POINTS("ViewPoints"),
    @XmlEnumValue("Services")
    SERVICES("Services");
    private final String value;

    LevelOfDetails(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LevelOfDetails fromValue(String v) {
        for (LevelOfDetails c: LevelOfDetails.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
