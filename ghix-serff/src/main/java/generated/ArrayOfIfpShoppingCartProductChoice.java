
package generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfIfpShoppingCart.ProductChoice complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfIfpShoppingCart.ProductChoice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IfpShoppingCart.ProductChoice" type="{}IfpShoppingCart.ProductChoice" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfIfpShoppingCart.ProductChoice", propOrder = {
    "ifpShoppingCartProductChoice"
})
public class ArrayOfIfpShoppingCartProductChoice {

    @XmlElement(name = "IfpShoppingCart.ProductChoice", nillable = true)
    protected List<IfpShoppingCartProductChoice> ifpShoppingCartProductChoice;

    /**
     * Gets the value of the ifpShoppingCartProductChoice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ifpShoppingCartProductChoice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIfpShoppingCartProductChoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IfpShoppingCartProductChoice }
     * 
     * 
     */
    public List<IfpShoppingCartProductChoice> getIfpShoppingCartProductChoice() {
        if (ifpShoppingCartProductChoice == null) {
            ifpShoppingCartProductChoice = new ArrayList<IfpShoppingCartProductChoice>();
        }
        return this.ifpShoppingCartProductChoice;
    }

}
