
package com.quotit.services.actws.aca._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetFamilyResult" type="{}GetFamily.Response" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getFamilyResult"
})
@XmlRootElement(name = "GetFamilyResponse")
public class GetFamilyResponse {

    @XmlElementRef(name = "GetFamilyResult", namespace = "http://www.quotit.com/Services/ActWS/ACA/2", type = JAXBElement.class, required = false)
    protected JAXBElement<generated.GetFamilyResponse> getFamilyResult;

    /**
     * Gets the value of the getFamilyResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link generated.GetFamilyResponse }{@code >}
     *     
     */
    public JAXBElement<generated.GetFamilyResponse> getGetFamilyResult() {
        return getFamilyResult;
    }

    /**
     * Sets the value of the getFamilyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link generated.GetFamilyResponse }{@code >}
     *     
     */
    public void setGetFamilyResult(JAXBElement<generated.GetFamilyResponse> value) {
        this.getFamilyResult = value;
    }

}
