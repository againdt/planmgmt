
package com.quotit.services.actws.aca._2;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.3
 * 2015-01-29T15:16:09.808+05:30
 * Generated source version: 2.7.3
 */

@WebFault(name = "CommandFault", targetNamespace = "http://www.quotit.com/Services/ActWS/ACA/2")
public class IACAFetchCustomProductsCommandFaultFaultFaultMessage extends Exception {
    
    private generated.CommandFault commandFault;

    public IACAFetchCustomProductsCommandFaultFaultFaultMessage() {
        super();
    }
    
    public IACAFetchCustomProductsCommandFaultFaultFaultMessage(String message) {
        super(message);
    }
    
    public IACAFetchCustomProductsCommandFaultFaultFaultMessage(String message, Throwable cause) {
        super(message, cause);
    }

    public IACAFetchCustomProductsCommandFaultFaultFaultMessage(String message, generated.CommandFault commandFault) {
        super(message);
        this.commandFault = commandFault;
    }

    public IACAFetchCustomProductsCommandFaultFaultFaultMessage(String message, generated.CommandFault commandFault, Throwable cause) {
        super(message, cause);
        this.commandFault = commandFault;
    }

    public generated.CommandFault getFaultInfo() {
        return this.commandFault;
    }
}
