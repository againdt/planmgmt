
package com.quotit.services.actws.aca._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import generated.GetCustomProductsQuoteRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="QuoteRequest" type="{}GetCustomProductsQuote.Request" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "quoteRequest"
})
@XmlRootElement(name = "GetCustomProductsQuote")
public class GetCustomProductsQuote {

    @XmlElementRef(name = "QuoteRequest", namespace = "http://www.quotit.com/Services/ActWS/ACA/2", type = JAXBElement.class, required = false)
    protected JAXBElement<GetCustomProductsQuoteRequest> quoteRequest;

    /**
     * Gets the value of the quoteRequest property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteRequest }{@code >}
     *     
     */
    public JAXBElement<GetCustomProductsQuoteRequest> getQuoteRequest() {
        return quoteRequest;
    }

    /**
     * Sets the value of the quoteRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteRequest }{@code >}
     *     
     */
    public void setQuoteRequest(JAXBElement<GetCustomProductsQuoteRequest> value) {
        this.quoteRequest = value;
    }

}
