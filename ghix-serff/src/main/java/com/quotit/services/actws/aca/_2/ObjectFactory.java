
package com.quotit.services.actws.aca._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import generated.FetchCustomProductsRequest;
import generated.FormularyLookupRequest;
import generated.GetCarrierLoadHistoryRequest;
import generated.GetCarriersPlansBenefitsRequest;
import generated.GetCountiesRequest;
import generated.GetCustomProductsQuoteRequest;
import generated.GetFamilyRequest;
import generated.GetGroupQuoteRequest;
import generated.GetGroupRequest;
import generated.GetIfpQuoteRequest;
import generated.GetIfpShoppingCartsRequest;
import generated.GetPlansbyRxRequest;
import generated.GetZipCodeInfoRequest;
import generated.ListRxRequest;
import generated.SubmitCustomProductsRequest;
import generated.SubmitFamilyRequest;
import generated.SubmitGroupRequest;
import generated.SubmitGroupShoppingCartRequest;
import generated.SubmitIfpShoppingCartRequest;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.quotit.services.actws.aca._2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListRxRequest_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "request");
    private final static QName _SubmitCustomProductsRequest_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "Request");
    private final static QName _SubmitGroupShoppingCartResponseSubmitGroupShoppingCartResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "SubmitGroupShoppingCartResult");
    private final static QName _FetchCustomProductsResponseFetchCustomProductsResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "FetchCustomProductsResult");
    private final static QName _SubmitFamilyResponseSubmitFamilyResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "SubmitFamilyResult");
    private final static QName _GetGroupQuoteResponseGetGroupQuoteResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetGroupQuoteResult");
    private final static QName _GetGroupQuoteQuoteRequest_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "QuoteRequest");
    private final static QName _ListRxResponseListRxResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "ListRxResult");
    private final static QName _SubmitIfpShoppingCartResponseSubmitIfpShoppingCartResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "SubmitIfpShoppingCartResult");
    private final static QName _GetCountiesGetCountiesRequest_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetCountiesRequest");
    private final static QName _SubmitCustomProductsResponseSubmitCustomProductsResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "SubmitCustomProductsResult");
    private final static QName _GetCountiesResponseGetCountiesResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetCountiesResult");
    private final static QName _GetIfpQuoteResponseGetIfpQuoteResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetIfpQuoteResult");
    private final static QName _GetZipCodeInfoResponseGetZipCodeInfoResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetZipCodeInfoResult");
    private final static QName _GetPlansbyRxResponseGetPlansbyRxResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetPlansbyRxResult");
    private final static QName _GetGroupResponseGetGroupResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetGroupResult");
    private final static QName _SubmitGroupResponseSubmitGroupResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "SubmitGroupResult");
    private final static QName _FormularyLookupResponseFormularyLookupResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "FormularyLookupResult");
    private final static QName _GetCarriersPlansBenefitsResponseGetCarriersPlansBenefitsResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetCarriersPlansBenefitsResult");
    private final static QName _GetIfpShoppingCartsResponseGetIfpShoppingCartsResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetIfpShoppingCartsResult");
    private final static QName _GetCustomProductsQuoteResponseGetCustomProductsQuoteResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetCustomProductsQuoteResult");
    private final static QName _GetZipCodeInfoGetZipCodeInfoRequest_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetZipCodeInfoRequest");
    private final static QName _GetFamilyResponseGetFamilyResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetFamilyResult");
    private final static QName _GetCarrierLoadHistoryResponseGetCarrierLoadHistoryResult_QNAME = new QName("http://www.quotit.com/Services/ActWS/ACA/2", "GetCarrierLoadHistoryResult");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.quotit.services.actws.aca._2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetIfpQuoteResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetIfpQuoteResponse createGetIfpQuoteResponse() {
        return new com.quotit.services.actws.aca._2.GetIfpQuoteResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetCustomProductsQuoteResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetCustomProductsQuoteResponse createGetCustomProductsQuoteResponse() {
        return new com.quotit.services.actws.aca._2.GetCustomProductsQuoteResponse();
    }

    /**
     * Create an instance of {@link SubmitCustomProducts }
     * 
     */
    public SubmitCustomProducts createSubmitCustomProducts() {
        return new SubmitCustomProducts();
    }

    /**
     * Create an instance of {@link GetPlansbyRx }
     * 
     */
    public GetPlansbyRx createGetPlansbyRx() {
        return new GetPlansbyRx();
    }

    /**
     * Create an instance of {@link GetGroupQuote }
     * 
     */
    public GetGroupQuote createGetGroupQuote() {
        return new GetGroupQuote();
    }

    /**
     * Create an instance of {@link GetIfpShoppingCarts }
     * 
     */
    public GetIfpShoppingCarts createGetIfpShoppingCarts() {
        return new GetIfpShoppingCarts();
    }

    /**
     * Create an instance of {@link GetCounties }
     * 
     */
    public GetCounties createGetCounties() {
        return new GetCounties();
    }

    /**
     * Create an instance of {@link FormularyLookup }
     * 
     */
    public FormularyLookup createFormularyLookup() {
        return new FormularyLookup();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.SubmitFamilyResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.SubmitFamilyResponse createSubmitFamilyResponse() {
        return new com.quotit.services.actws.aca._2.SubmitFamilyResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.ListRxResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.ListRxResponse createListRxResponse() {
        return new com.quotit.services.actws.aca._2.ListRxResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse createGetCarriersPlansBenefitsResponse() {
        return new com.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetZipCodeInfoResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetZipCodeInfoResponse createGetZipCodeInfoResponse() {
        return new com.quotit.services.actws.aca._2.GetZipCodeInfoResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.SubmitGroupShoppingCartResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.SubmitGroupShoppingCartResponse createSubmitGroupShoppingCartResponse() {
        return new com.quotit.services.actws.aca._2.SubmitGroupShoppingCartResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.SubmitCustomProductsResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.SubmitCustomProductsResponse createSubmitCustomProductsResponse() {
        return new com.quotit.services.actws.aca._2.SubmitCustomProductsResponse();
    }

    /**
     * Create an instance of {@link GetGroup }
     * 
     */
    public GetGroup createGetGroup() {
        return new GetGroup();
    }

    /**
     * Create an instance of {@link GetFamily }
     * 
     */
    public GetFamily createGetFamily() {
        return new GetFamily();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.FormularyLookupResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.FormularyLookupResponse createFormularyLookupResponse() {
        return new com.quotit.services.actws.aca._2.FormularyLookupResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetCarrierLoadHistoryResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetCarrierLoadHistoryResponse createGetCarrierLoadHistoryResponse() {
        return new com.quotit.services.actws.aca._2.GetCarrierLoadHistoryResponse();
    }

    /**
     * Create an instance of {@link ListRx }
     * 
     */
    public ListRx createListRx() {
        return new ListRx();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuote }
     * 
     */
    public GetCustomProductsQuote createGetCustomProductsQuote() {
        return new GetCustomProductsQuote();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetPlansbyRxResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetPlansbyRxResponse createGetPlansbyRxResponse() {
        return new com.quotit.services.actws.aca._2.GetPlansbyRxResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetGroupResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetGroupResponse createGetGroupResponse() {
        return new com.quotit.services.actws.aca._2.GetGroupResponse();
    }

    /**
     * Create an instance of {@link SubmitFamily }
     * 
     */
    public SubmitFamily createSubmitFamily() {
        return new SubmitFamily();
    }

    /**
     * Create an instance of {@link GetCarrierLoadHistory }
     * 
     */
    public GetCarrierLoadHistory createGetCarrierLoadHistory() {
        return new GetCarrierLoadHistory();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefits }
     * 
     */
    public GetCarriersPlansBenefits createGetCarriersPlansBenefits() {
        return new GetCarriersPlansBenefits();
    }

    /**
     * Create an instance of {@link SubmitGroup }
     * 
     */
    public SubmitGroup createSubmitGroup() {
        return new SubmitGroup();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.SubmitIfpShoppingCartResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.SubmitIfpShoppingCartResponse createSubmitIfpShoppingCartResponse() {
        return new com.quotit.services.actws.aca._2.SubmitIfpShoppingCartResponse();
    }

    /**
     * Create an instance of {@link SubmitIfpShoppingCart }
     * 
     */
    public SubmitIfpShoppingCart createSubmitIfpShoppingCart() {
        return new SubmitIfpShoppingCart();
    }

    /**
     * Create an instance of {@link GetIfpQuote }
     * 
     */
    public GetIfpQuote createGetIfpQuote() {
        return new GetIfpQuote();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.SubmitGroupResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.SubmitGroupResponse createSubmitGroupResponse() {
        return new com.quotit.services.actws.aca._2.SubmitGroupResponse();
    }

    /**
     * Create an instance of {@link GetZipCodeInfo }
     * 
     */
    public GetZipCodeInfo createGetZipCodeInfo() {
        return new GetZipCodeInfo();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetGroupQuoteResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetGroupQuoteResponse createGetGroupQuoteResponse() {
        return new com.quotit.services.actws.aca._2.GetGroupQuoteResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetFamilyResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetFamilyResponse createGetFamilyResponse() {
        return new com.quotit.services.actws.aca._2.GetFamilyResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetCountiesResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetCountiesResponse createGetCountiesResponse() {
        return new com.quotit.services.actws.aca._2.GetCountiesResponse();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.FetchCustomProductsResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.FetchCustomProductsResponse createFetchCustomProductsResponse() {
        return new com.quotit.services.actws.aca._2.FetchCustomProductsResponse();
    }

    /**
     * Create an instance of {@link FetchCustomProducts }
     * 
     */
    public FetchCustomProducts createFetchCustomProducts() {
        return new FetchCustomProducts();
    }

    /**
     * Create an instance of {@link SubmitGroupShoppingCart }
     * 
     */
    public SubmitGroupShoppingCart createSubmitGroupShoppingCart() {
        return new SubmitGroupShoppingCart();
    }

    /**
     * Create an instance of {@link com.quotit.services.actws.aca._2.GetIfpShoppingCartsResponse }
     * 
     */
    public com.quotit.services.actws.aca._2.GetIfpShoppingCartsResponse createGetIfpShoppingCartsResponse() {
        return new com.quotit.services.actws.aca._2.GetIfpShoppingCartsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListRxRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = ListRx.class)
    public JAXBElement<ListRxRequest> createListRxRequest(ListRxRequest value) {
        return new JAXBElement<ListRxRequest>(_ListRxRequest_QNAME, ListRxRequest.class, ListRx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitCustomProductsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "Request", scope = SubmitCustomProducts.class)
    public JAXBElement<SubmitCustomProductsRequest> createSubmitCustomProductsRequest(SubmitCustomProductsRequest value) {
        return new JAXBElement<SubmitCustomProductsRequest>(_SubmitCustomProductsRequest_QNAME, SubmitCustomProductsRequest.class, SubmitCustomProducts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.SubmitGroupShoppingCartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "SubmitGroupShoppingCartResult", scope = com.quotit.services.actws.aca._2.SubmitGroupShoppingCartResponse.class)
    public JAXBElement<generated.SubmitGroupShoppingCartResponse> createSubmitGroupShoppingCartResponseSubmitGroupShoppingCartResult(generated.SubmitGroupShoppingCartResponse value) {
        return new JAXBElement<generated.SubmitGroupShoppingCartResponse>(_SubmitGroupShoppingCartResponseSubmitGroupShoppingCartResult_QNAME, generated.SubmitGroupShoppingCartResponse.class, com.quotit.services.actws.aca._2.SubmitGroupShoppingCartResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.FetchCustomProductsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "FetchCustomProductsResult", scope = com.quotit.services.actws.aca._2.FetchCustomProductsResponse.class)
    public JAXBElement<generated.FetchCustomProductsResponse> createFetchCustomProductsResponseFetchCustomProductsResult(generated.FetchCustomProductsResponse value) {
        return new JAXBElement<generated.FetchCustomProductsResponse>(_FetchCustomProductsResponseFetchCustomProductsResult_QNAME, generated.FetchCustomProductsResponse.class, com.quotit.services.actws.aca._2.FetchCustomProductsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.SubmitFamilyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "SubmitFamilyResult", scope = com.quotit.services.actws.aca._2.SubmitFamilyResponse.class)
    public JAXBElement<generated.SubmitFamilyResponse> createSubmitFamilyResponseSubmitFamilyResult(generated.SubmitFamilyResponse value) {
        return new JAXBElement<generated.SubmitFamilyResponse>(_SubmitFamilyResponseSubmitFamilyResult_QNAME, generated.SubmitFamilyResponse.class, com.quotit.services.actws.aca._2.SubmitFamilyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetGroupQuoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetGroupQuoteResult", scope = com.quotit.services.actws.aca._2.GetGroupQuoteResponse.class)
    public JAXBElement<generated.GetGroupQuoteResponse> createGetGroupQuoteResponseGetGroupQuoteResult(generated.GetGroupQuoteResponse value) {
        return new JAXBElement<generated.GetGroupQuoteResponse>(_GetGroupQuoteResponseGetGroupQuoteResult_QNAME, generated.GetGroupQuoteResponse.class, com.quotit.services.actws.aca._2.GetGroupQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "QuoteRequest", scope = GetGroupQuote.class)
    public JAXBElement<GetGroupQuoteRequest> createGetGroupQuoteQuoteRequest(GetGroupQuoteRequest value) {
        return new JAXBElement<GetGroupQuoteRequest>(_GetGroupQuoteQuoteRequest_QNAME, GetGroupQuoteRequest.class, GetGroupQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.ListRxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "ListRxResult", scope = com.quotit.services.actws.aca._2.ListRxResponse.class)
    public JAXBElement<generated.ListRxResponse> createListRxResponseListRxResult(generated.ListRxResponse value) {
        return new JAXBElement<generated.ListRxResponse>(_ListRxResponseListRxResult_QNAME, generated.ListRxResponse.class, com.quotit.services.actws.aca._2.ListRxResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.SubmitIfpShoppingCartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "SubmitIfpShoppingCartResult", scope = com.quotit.services.actws.aca._2.SubmitIfpShoppingCartResponse.class)
    public JAXBElement<generated.SubmitIfpShoppingCartResponse> createSubmitIfpShoppingCartResponseSubmitIfpShoppingCartResult(generated.SubmitIfpShoppingCartResponse value) {
        return new JAXBElement<generated.SubmitIfpShoppingCartResponse>(_SubmitIfpShoppingCartResponseSubmitIfpShoppingCartResult_QNAME, generated.SubmitIfpShoppingCartResponse.class, com.quotit.services.actws.aca._2.SubmitIfpShoppingCartResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitIfpShoppingCartRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = SubmitIfpShoppingCart.class)
    public JAXBElement<SubmitIfpShoppingCartRequest> createSubmitIfpShoppingCartRequest(SubmitIfpShoppingCartRequest value) {
        return new JAXBElement<SubmitIfpShoppingCartRequest>(_ListRxRequest_QNAME, SubmitIfpShoppingCartRequest.class, SubmitIfpShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpShoppingCartsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = GetIfpShoppingCarts.class)
    public JAXBElement<GetIfpShoppingCartsRequest> createGetIfpShoppingCartsRequest(GetIfpShoppingCartsRequest value) {
        return new JAXBElement<GetIfpShoppingCartsRequest>(_ListRxRequest_QNAME, GetIfpShoppingCartsRequest.class, GetIfpShoppingCarts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountiesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetCountiesRequest", scope = GetCounties.class)
    public JAXBElement<GetCountiesRequest> createGetCountiesGetCountiesRequest(GetCountiesRequest value) {
        return new JAXBElement<GetCountiesRequest>(_GetCountiesGetCountiesRequest_QNAME, GetCountiesRequest.class, GetCounties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitGroupShoppingCartRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = SubmitGroupShoppingCart.class)
    public JAXBElement<SubmitGroupShoppingCartRequest> createSubmitGroupShoppingCartRequest(SubmitGroupShoppingCartRequest value) {
        return new JAXBElement<SubmitGroupShoppingCartRequest>(_ListRxRequest_QNAME, SubmitGroupShoppingCartRequest.class, SubmitGroupShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.SubmitCustomProductsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "SubmitCustomProductsResult", scope = com.quotit.services.actws.aca._2.SubmitCustomProductsResponse.class)
    public JAXBElement<generated.SubmitCustomProductsResponse> createSubmitCustomProductsResponseSubmitCustomProductsResult(generated.SubmitCustomProductsResponse value) {
        return new JAXBElement<generated.SubmitCustomProductsResponse>(_SubmitCustomProductsResponseSubmitCustomProductsResult_QNAME, generated.SubmitCustomProductsResponse.class, com.quotit.services.actws.aca._2.SubmitCustomProductsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetCountiesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetCountiesResult", scope = com.quotit.services.actws.aca._2.GetCountiesResponse.class)
    public JAXBElement<generated.GetCountiesResponse> createGetCountiesResponseGetCountiesResult(generated.GetCountiesResponse value) {
        return new JAXBElement<generated.GetCountiesResponse>(_GetCountiesResponseGetCountiesResult_QNAME, generated.GetCountiesResponse.class, com.quotit.services.actws.aca._2.GetCountiesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetIfpQuoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetIfpQuoteResult", scope = com.quotit.services.actws.aca._2.GetIfpQuoteResponse.class)
    public JAXBElement<generated.GetIfpQuoteResponse> createGetIfpQuoteResponseGetIfpQuoteResult(generated.GetIfpQuoteResponse value) {
        return new JAXBElement<generated.GetIfpQuoteResponse>(_GetIfpQuoteResponseGetIfpQuoteResult_QNAME, generated.GetIfpQuoteResponse.class, com.quotit.services.actws.aca._2.GetIfpQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = GetPlansbyRx.class)
    public JAXBElement<GetPlansbyRxRequest> createGetPlansbyRxRequest(GetPlansbyRxRequest value) {
        return new JAXBElement<GetPlansbyRxRequest>(_ListRxRequest_QNAME, GetPlansbyRxRequest.class, GetPlansbyRx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetZipCodeInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetZipCodeInfoResult", scope = com.quotit.services.actws.aca._2.GetZipCodeInfoResponse.class)
    public JAXBElement<generated.GetZipCodeInfoResponse> createGetZipCodeInfoResponseGetZipCodeInfoResult(generated.GetZipCodeInfoResponse value) {
        return new JAXBElement<generated.GetZipCodeInfoResponse>(_GetZipCodeInfoResponseGetZipCodeInfoResult_QNAME, generated.GetZipCodeInfoResponse.class, com.quotit.services.actws.aca._2.GetZipCodeInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitFamilyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = SubmitFamily.class)
    public JAXBElement<SubmitFamilyRequest> createSubmitFamilyRequest(SubmitFamilyRequest value) {
        return new JAXBElement<SubmitFamilyRequest>(_ListRxRequest_QNAME, SubmitFamilyRequest.class, SubmitFamily.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetPlansbyRxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetPlansbyRxResult", scope = com.quotit.services.actws.aca._2.GetPlansbyRxResponse.class)
    public JAXBElement<generated.GetPlansbyRxResponse> createGetPlansbyRxResponseGetPlansbyRxResult(generated.GetPlansbyRxResponse value) {
        return new JAXBElement<generated.GetPlansbyRxResponse>(_GetPlansbyRxResponseGetPlansbyRxResult_QNAME, generated.GetPlansbyRxResponse.class, com.quotit.services.actws.aca._2.GetPlansbyRxResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetGroupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetGroupResult", scope = com.quotit.services.actws.aca._2.GetGroupResponse.class)
    public JAXBElement<generated.GetGroupResponse> createGetGroupResponseGetGroupResult(generated.GetGroupResponse value) {
        return new JAXBElement<generated.GetGroupResponse>(_GetGroupResponseGetGroupResult_QNAME, generated.GetGroupResponse.class, com.quotit.services.actws.aca._2.GetGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.SubmitGroupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "SubmitGroupResult", scope = com.quotit.services.actws.aca._2.SubmitGroupResponse.class)
    public JAXBElement<generated.SubmitGroupResponse> createSubmitGroupResponseSubmitGroupResult(generated.SubmitGroupResponse value) {
        return new JAXBElement<generated.SubmitGroupResponse>(_SubmitGroupResponseSubmitGroupResult_QNAME, generated.SubmitGroupResponse.class, com.quotit.services.actws.aca._2.SubmitGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.FormularyLookupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "FormularyLookupResult", scope = com.quotit.services.actws.aca._2.FormularyLookupResponse.class)
    public JAXBElement<generated.FormularyLookupResponse> createFormularyLookupResponseFormularyLookupResult(generated.FormularyLookupResponse value) {
        return new JAXBElement<generated.FormularyLookupResponse>(_FormularyLookupResponseFormularyLookupResult_QNAME, generated.FormularyLookupResponse.class, com.quotit.services.actws.aca._2.FormularyLookupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetCarriersPlansBenefitsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetCarriersPlansBenefitsResult", scope = com.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse.class)
    public JAXBElement<generated.GetCarriersPlansBenefitsResponse> createGetCarriersPlansBenefitsResponseGetCarriersPlansBenefitsResult(generated.GetCarriersPlansBenefitsResponse value) {
        return new JAXBElement<generated.GetCarriersPlansBenefitsResponse>(_GetCarriersPlansBenefitsResponseGetCarriersPlansBenefitsResult_QNAME, generated.GetCarriersPlansBenefitsResponse.class, com.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitGroupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = SubmitGroup.class)
    public JAXBElement<SubmitGroupRequest> createSubmitGroupRequest(SubmitGroupRequest value) {
        return new JAXBElement<SubmitGroupRequest>(_ListRxRequest_QNAME, SubmitGroupRequest.class, SubmitGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = GetCarriersPlansBenefits.class)
    public JAXBElement<GetCarriersPlansBenefitsRequest> createGetCarriersPlansBenefitsRequest(GetCarriersPlansBenefitsRequest value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequest>(_ListRxRequest_QNAME, GetCarriersPlansBenefitsRequest.class, GetCarriersPlansBenefits.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetIfpShoppingCartsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetIfpShoppingCartsResult", scope = com.quotit.services.actws.aca._2.GetIfpShoppingCartsResponse.class)
    public JAXBElement<generated.GetIfpShoppingCartsResponse> createGetIfpShoppingCartsResponseGetIfpShoppingCartsResult(generated.GetIfpShoppingCartsResponse value) {
        return new JAXBElement<generated.GetIfpShoppingCartsResponse>(_GetIfpShoppingCartsResponseGetIfpShoppingCartsResult_QNAME, generated.GetIfpShoppingCartsResponse.class, com.quotit.services.actws.aca._2.GetIfpShoppingCartsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetCustomProductsQuoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetCustomProductsQuoteResult", scope = com.quotit.services.actws.aca._2.GetCustomProductsQuoteResponse.class)
    public JAXBElement<generated.GetCustomProductsQuoteResponse> createGetCustomProductsQuoteResponseGetCustomProductsQuoteResult(generated.GetCustomProductsQuoteResponse value) {
        return new JAXBElement<generated.GetCustomProductsQuoteResponse>(_GetCustomProductsQuoteResponseGetCustomProductsQuoteResult_QNAME, generated.GetCustomProductsQuoteResponse.class, com.quotit.services.actws.aca._2.GetCustomProductsQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "QuoteRequest", scope = GetCustomProductsQuote.class)
    public JAXBElement<GetCustomProductsQuoteRequest> createGetCustomProductsQuoteQuoteRequest(GetCustomProductsQuoteRequest value) {
        return new JAXBElement<GetCustomProductsQuoteRequest>(_GetGroupQuoteQuoteRequest_QNAME, GetCustomProductsQuoteRequest.class, GetCustomProductsQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarrierLoadHistoryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = GetCarrierLoadHistory.class)
    public JAXBElement<GetCarrierLoadHistoryRequest> createGetCarrierLoadHistoryRequest(GetCarrierLoadHistoryRequest value) {
        return new JAXBElement<GetCarrierLoadHistoryRequest>(_ListRxRequest_QNAME, GetCarrierLoadHistoryRequest.class, GetCarrierLoadHistory.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetZipCodeInfoRequest", scope = GetZipCodeInfo.class)
    public JAXBElement<GetZipCodeInfoRequest> createGetZipCodeInfoGetZipCodeInfoRequest(GetZipCodeInfoRequest value) {
        return new JAXBElement<GetZipCodeInfoRequest>(_GetZipCodeInfoGetZipCodeInfoRequest_QNAME, GetZipCodeInfoRequest.class, GetZipCodeInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFamilyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = GetFamily.class)
    public JAXBElement<GetFamilyRequest> createGetFamilyRequest(GetFamilyRequest value) {
        return new JAXBElement<GetFamilyRequest>(_ListRxRequest_QNAME, GetFamilyRequest.class, GetFamily.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchCustomProductsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "Request", scope = FetchCustomProducts.class)
    public JAXBElement<FetchCustomProductsRequest> createFetchCustomProductsRequest(FetchCustomProductsRequest value) {
        return new JAXBElement<FetchCustomProductsRequest>(_SubmitCustomProductsRequest_QNAME, FetchCustomProductsRequest.class, FetchCustomProducts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "QuoteRequest", scope = GetIfpQuote.class)
    public JAXBElement<GetIfpQuoteRequest> createGetIfpQuoteQuoteRequest(GetIfpQuoteRequest value) {
        return new JAXBElement<GetIfpQuoteRequest>(_GetGroupQuoteQuoteRequest_QNAME, GetIfpQuoteRequest.class, GetIfpQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetFamilyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetFamilyResult", scope = com.quotit.services.actws.aca._2.GetFamilyResponse.class)
    public JAXBElement<generated.GetFamilyResponse> createGetFamilyResponseGetFamilyResult(generated.GetFamilyResponse value) {
        return new JAXBElement<generated.GetFamilyResponse>(_GetFamilyResponseGetFamilyResult_QNAME, generated.GetFamilyResponse.class, com.quotit.services.actws.aca._2.GetFamilyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = FormularyLookup.class)
    public JAXBElement<FormularyLookupRequest> createFormularyLookupRequest(FormularyLookupRequest value) {
        return new JAXBElement<FormularyLookupRequest>(_ListRxRequest_QNAME, FormularyLookupRequest.class, FormularyLookup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "request", scope = GetGroup.class)
    public JAXBElement<GetGroupRequest> createGetGroupRequest(GetGroupRequest value) {
        return new JAXBElement<GetGroupRequest>(_ListRxRequest_QNAME, GetGroupRequest.class, GetGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link generated.GetCarrierLoadHistoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.quotit.com/Services/ActWS/ACA/2", name = "GetCarrierLoadHistoryResult", scope = com.quotit.services.actws.aca._2.GetCarrierLoadHistoryResponse.class)
    public JAXBElement<generated.GetCarrierLoadHistoryResponse> createGetCarrierLoadHistoryResponseGetCarrierLoadHistoryResult(generated.GetCarrierLoadHistoryResponse value) {
        return new JAXBElement<generated.GetCarrierLoadHistoryResponse>(_GetCarrierLoadHistoryResponseGetCarrierLoadHistoryResult_QNAME, generated.GetCarrierLoadHistoryResponse.class, com.quotit.services.actws.aca._2.GetCarrierLoadHistoryResponse.class, value);
    }

}
