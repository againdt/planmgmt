
package com.quotit.services.actws.aca._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupQuoteResult" type="{}GetGroupQuote.Response" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGroupQuoteResult"
})
@XmlRootElement(name = "GetGroupQuoteResponse")
public class GetGroupQuoteResponse {

    @XmlElementRef(name = "GetGroupQuoteResult", namespace = "http://www.quotit.com/Services/ActWS/ACA/2", type = JAXBElement.class, required = false)
    protected JAXBElement<generated.GetGroupQuoteResponse> getGroupQuoteResult;

    /**
     * Gets the value of the getGroupQuoteResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link generated.GetGroupQuoteResponse }{@code >}
     *     
     */
    public JAXBElement<generated.GetGroupQuoteResponse> getGetGroupQuoteResult() {
        return getGroupQuoteResult;
    }

    /**
     * Sets the value of the getGroupQuoteResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link generated.GetGroupQuoteResponse }{@code >}
     *     
     */
    public void setGetGroupQuoteResult(JAXBElement<generated.GetGroupQuoteResponse> value) {
        this.getGroupQuoteResult = value;
    }

}
