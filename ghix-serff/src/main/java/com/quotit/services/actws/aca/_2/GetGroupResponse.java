
package com.quotit.services.actws.aca._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetGroupResult" type="{}GetGroup.Response" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getGroupResult"
})
@XmlRootElement(name = "GetGroupResponse")
public class GetGroupResponse {

    @XmlElementRef(name = "GetGroupResult", namespace = "http://www.quotit.com/Services/ActWS/ACA/2", type = JAXBElement.class, required = false)
    protected JAXBElement<generated.GetGroupResponse> getGroupResult;

    /**
     * Gets the value of the getGroupResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link generated.GetGroupResponse }{@code >}
     *     
     */
    public JAXBElement<generated.GetGroupResponse> getGetGroupResult() {
        return getGroupResult;
    }

    /**
     * Sets the value of the getGroupResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link generated.GetGroupResponse }{@code >}
     *     
     */
    public void setGetGroupResult(JAXBElement<generated.GetGroupResponse> value) {
        this.getGroupResult = value;
    }

}
