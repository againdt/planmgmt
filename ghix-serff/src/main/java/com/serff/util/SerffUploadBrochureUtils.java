package com.serff.util;

import static com.serff.util.SerffConstants.POS_APPLICABLE_YEAR;
import static com.serff.util.SerffConstants.POS_BROCHURE_ECM;
import static com.serff.util.SerffConstants.POS_BROCHURE_FILE;
import static com.serff.util.SerffConstants.POS_EOC_ECM;
import static com.serff.util.SerffConstants.POS_EOC_FILE;
import static com.serff.util.SerffConstants.POS_PLAN_ISSUER_NUM;
import static com.serff.util.SerffConstants.POS_SBC_ECM;
import static com.serff.util.SerffConstants.POS_SBC_FILE;
import static com.serff.util.SerffConstants.SERF_ECM_BROCHURE_BASE_PATH;
import static com.serff.util.SerffConstants.UNDERSCORE;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.hix.model.PlanDocumentsJob;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch.FTP_STATUS;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.serff.service.templates.PlanDocumentsJobService;
import com.serff.service.validation.SerffValidator;

/**
 * 
 * This is a singleton class for upload brochure utility to restrict more than
 * one simultaneous access to upload process.
 * 
 * @author vardekar_s
 * 
 */
@Component
public class SerffUploadBrochureUtils extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(SerffUploadBrochureUtils.class);
	private static SerffUploadBrochureUtils instance;
	private static volatile boolean isUplaodRunning = false;
	private static final String DATABASE_ENTRY_NOT_FOUND = "Document entry not found in the database. ";
	private static final String DATABASE_UPDATE_ERROR = "Document uploaded in ECM. Database update error : ";
	private static final String APPLICABLE_ENTRY_NOT_FOUND = "Applicable year is not present in file name. ";
	private static final String FTP_FILE_MOVE_FAILED = "Moving file in ftp host failed. ";
	private static final String MSG_UPLOAD_ANCILLARY_FAIL = "Failed to upload Ancillary Document(PDF) at ECM server. ";
	
	private static final String SUCCESS = "success";
	private static final String ERROR = "error";
	private static final String SBC = "sbc";
	private static final String BROCHURE = "brochure";
	private static final String EOC = "eoc";
	private static final String PDF = "pdf";
	private static final String SBC_INITIAL = "S";
	private static final String BROCHURE_INITIAL = "B";
	private static final String EOC_INITIAL = "E";
	private static final String PATH_SEPERATOR = "/";
	private static final String EMPTY_STRING = "";
	public static final int APPLICABLE_YEAR_INDEX = 1;
	public static final int DOCUMENT_ARRAY_SIZE = 9;
	private static final int ERROR_MAX_LEN = 1019;

	private SerffUploadBrochureUtils() {

	}

	public static boolean isUploadRunning() {
		return isUplaodRunning;
	}

	private static void setUploadRunning(boolean flag) {
		isUplaodRunning = flag;
	}

	/**
	 * 
	 * This method returns singleton instance of SerffUploadBrochureUtils class.
	 * 
	 * @return SerffUploadBrochureUtils
	 */
	public static SerffUploadBrochureUtils getSerffUploadBrochureUtilsSingleton() {
		if (null == instance) {
			instance = new SerffUploadBrochureUtils();
		}
		return instance;
	}

	/**
	 * 
	 * This method extracts IssuerPlanNumber from remote file name.
	 * 
	 * 
	 * @return
	 */
	private String[] getIssuerPlanNumberFromFileName(String remoteFileName) {

		// null check remoteFileName
		String[] filenameChunks = remoteFileName.split("[.]");
		String[] fileNameArray = new String[POS_SBC_ECM];

		if (null != filenameChunks && filenameChunks.length > 0) {
			String fileExtension = filenameChunks[filenameChunks.length - 1];

			if (!fileExtension.equalsIgnoreCase(PDF)) {
				LOGGER.info("Not Supported FTP file : " + remoteFileName);
				return null;
			}
			else if (filenameChunks[0].indexOf(UNDERSCORE) > -1) {
				
				String[] fileNameData = filenameChunks[0].split("[_]");
				String issuerPlanNumber = null;
				String variance = null;
				
				if (fileNameData.length > POS_BROCHURE_FILE) {
				
					LOGGER.info("Supported FTP file : " + remoteFileName);
					String planIdNameComponent = (fileNameData)[0];
					String applicableYear = (fileNameData)[POS_BROCHURE_FILE];
					
					// Check for variance.
					if (planIdNameComponent.contains("-")) {
						LOGGER.info("Issuer plan number with variance : " + planIdNameComponent);
						String[] planIdComponentArray = planIdNameComponent.split("[-]");
						issuerPlanNumber = planIdComponentArray[0];
						variance = planIdComponentArray[POS_BROCHURE_FILE];
						fileNameArray [0] = issuerPlanNumber + variance;
						fileNameArray [POS_BROCHURE_FILE] = applicableYear;
						LOGGER.debug("Issuer plan number with variance : Issuer plan number : " + issuerPlanNumber);
						LOGGER.debug("Issuer plan number with variance : Variance : " + variance);
					}
					else {
						issuerPlanNumber = planIdNameComponent;
						fileNameArray [0] = issuerPlanNumber;
						fileNameArray [POS_BROCHURE_FILE] = applicableYear;
						LOGGER.debug("Issuer plan number without variance : " + issuerPlanNumber);
					}
				}

				// Validate planId
				if (!isValidateStandardComponentId(issuerPlanNumber)) {
					LOGGER.debug("Plan id is not valid " + issuerPlanNumber);
					return null;
				}

			//	return (null != variance) ? issuerPlanNumber + variance : issuerPlanNumber;
				return fileNameArray;
			}
		}

		return null;
	}

	/**
	 * This method will download files from designated FTP location and upload
	 * them into the ECM. After file upload is done file entry will be updated
	 * in database table.
	 * 
	 * 
	 * @throws Exception
	 */
	public synchronized boolean uploadBrochure(ContentManagementService ecmService,
			PlanDocumentsJobService planDocumentsJobService, GHIXSFTPClient ftpClient , String dropboxDirectory) throws Exception {
		boolean isECMUploadSuccessful = true, uploadSuccess = true;

		try {
			// Upload process is not running. Start upload process here.
			setUploadRunning(true);
			// Get files from ftp location.
		/*	String dropboxDirectory = SerffConstants.getSerffCsrHomePath() + SerffConstants.getFtpServerUploadSubpath()
					+ PATH_SEPERATOR + SerffConstants.FTP_BROCHURE_PATH;*/
			LOGGER.debug("Dropbox directory path :" + dropboxDirectory);
			Map<String, InputStream> fileList = ftpClient.getFilesFromDirectory(dropboxDirectory + PATH_SEPERATOR);
			// Returning false for no file list.
			if (CollectionUtils.isEmpty(fileList)) {
				LOGGER.error("There is no file at FTP location for upload process.");
				return false;
			}
			// Check and create error and success directories before processing.
			// As absence of error and success directory will halt file move
			// functionality and show wrong count on UI.
			checkSuccessAndErrorDirectories(ftpClient);

			// Map of pland id and brochure values.
			Map<String, String[]> planIdDocumentsMap = new HashMap<String, String[]>();

			// Download from FTP location.
			for (Entry<String, InputStream> entry : fileList.entrySet()) {

				if (null == entry) {
					continue;
				}
				isECMUploadSuccessful = processFileForRecordUpdate(entry, ecmService, planDocumentsJobService, ftpClient, planIdDocumentsMap);
				uploadSuccess = (uploadSuccess && isECMUploadSuccessful);
				// Fixed HP-FOD issue.
				SerffResourceUtil.closeInputStream(entry.getValue());
			}
			// Check whether planIdDocumentsMap is not null and is not empty.
			// e.g when all documents are failed in ECM upload etc. No need to process further.
			if (!CollectionUtils.isEmpty(planIdDocumentsMap)) {
				// Update values stored in map in single go.
				Map<String, String> failedPlanIdsAndError = updatePlanDocuments(planIdDocumentsMap,
						planDocumentsJobService);
				LOGGER.debug("Failed plan ids map : " + failedPlanIdsAndError);
				// Create document job table entries by using failedPlanIds list.
				createDocumentJobEntries(planIdDocumentsMap, failedPlanIdsAndError, planDocumentsJobService, ftpClient);
			}

		} finally {
			// Upload process finished/exception. Reset flag to false.
			setUploadRunning(false);
		}

		return uploadSuccess;
	}

	private void updateStatusRecord(String documentNewName, String documentOldName, String documentType,
			String errorMessage, String issuerPlanNumber, String identifier, String ucmNewId,
			String ucmOldId, FTP_STATUS status, PlanDocumentsJobService planDocumentsJobService, GHIXSFTPClient ftpClient) {
		
		LOGGER.debug("Creating job entry : Document type is : " + documentType + " FileName:" + documentNewName + " newDocumentECMId: " + ucmNewId);
		
		int jobId = updatePlanDocumentJobTable(documentNewName, documentOldName, documentType,
				errorMessage, issuerPlanNumber.substring(0, SerffConstants.LEN_HIOS_PRODUCT_ID), issuerPlanNumber, identifier, ucmNewId, ucmOldId,
				status, planDocumentsJobService);
	
		moveFileToDirectory(jobId, (status == FTP_STATUS.COMPLETED), documentNewName, ftpClient);	
	}

	private boolean processFileForRecordUpdate(Entry<String, InputStream> entry, ContentManagementService ecmService,
			PlanDocumentsJobService planDocumentsJobService, GHIXSFTPClient ftpClient, Map<String, String[]> planIdDocumentsMap) {
		String documentType = EMPTY_STRING;
		boolean isECMUploadSuccessful = true;
		String fileName = entry.getKey();
		LOGGER.debug("Processing file for plan document: " + fileName);
		// Extract IssuerPlanNumber from filename.
		String[] issuerPlanNumberArray = getIssuerPlanNumberFromFileName(fileName);
		String issuerPlanNumber = null, mapKey = null, applicableYear = null;
		if(null!= issuerPlanNumberArray){
			issuerPlanNumber = issuerPlanNumberArray[0];
			applicableYear = issuerPlanNumberArray[APPLICABLE_YEAR_INDEX];
			
			if(!StringUtils.isNumeric(applicableYear)){
				mapKey = issuerPlanNumber + SerffConstants.UNDERSCORE + applicableYear;
				updateStatusRecord(fileName, EMPTY_STRING, documentType, APPLICABLE_ENTRY_NOT_FOUND, issuerPlanNumber, 
						mapKey, null, EMPTY_STRING, FTP_STATUS.FAILED, planDocumentsJobService, ftpClient);
				return false;
			}
		}

		if (null != issuerPlanNumber && !issuerPlanNumber.isEmpty()) {
			LOGGER.debug("Plan id/Issuer Plan Number from filename : " + issuerPlanNumber);
			// Check document existence in database.
			long planCount = 0;

			if (fileName.toLowerCase().contains(SBC)) {
				documentType = SBC_INITIAL;
			} else if (fileName.toLowerCase().contains(BROCHURE)) {
				documentType = BROCHURE_INITIAL;
			} else if(fileName.toLowerCase().contains(EOC)){
				documentType = EOC_INITIAL;
			}else{
				LOGGER.error("Valid document type not found in " + fileName);
				return false;
			}
			
			planCount = planDocumentsJobService.getPlanCount(issuerPlanNumber , Integer.parseInt(applicableYear));
			LOGGER.debug("Plan count for " + mapKey + " is " + planCount);
			if (planCount > 0) {
				try {
					// Document is present in database. Upload in ECM.
					String documentId = uploadDocumentInEcm(SERF_ECM_BROCHURE_BASE_PATH, fileName, entry.getValue(), ecmService);
					// Update map values with uploaded document id and document name.
					addOrUpdateBrochureValuesInMap(issuerPlanNumber, applicableYear, documentType, documentId, fileName,
							planIdDocumentsMap);
					LOGGER.debug("Map size after manipulation : " + planIdDocumentsMap.size());
				} catch (Exception e) {
					LOGGER.error("Exception occurred while uploading " + fileName
							+ " in to ECM. Error is " + e.getMessage(), e);
					isECMUploadSuccessful = false;
					updateStatusRecord(fileName, EMPTY_STRING, documentType, e.getMessage(), issuerPlanNumber, 
							mapKey, null, EMPTY_STRING, FTP_STATUS.FAILED, planDocumentsJobService, ftpClient);
				}
			} else {
				LOGGER.error(" Database entry not found for : " + fileName);
				isECMUploadSuccessful = false;
				updateStatusRecord(fileName, EMPTY_STRING, documentType, DATABASE_ENTRY_NOT_FOUND, issuerPlanNumber, 
						mapKey, null, EMPTY_STRING, FTP_STATUS.FAILED, planDocumentsJobService, ftpClient);
			}
		} else {
			LOGGER.error("Excluding file since file type not supported. " + fileName);
		}
		return isECMUploadSuccessful;
	}
	
	/**
	 * This method inserts records for all the documents those are successfully
	 * uploaded in ECM.
	 * 
	 * @param planIdDocumentsMap
	 * @param failedPlanIds
	 * @param planDocumentsJobService
	 */
	private void createDocumentJobEntries(Map<String, String[]> planIdDocumentsMap,
			Map<String, String> failedPlanIdsAndError, PlanDocumentsJobService planDocumentsJobService,
			GHIXSFTPClient ftpClient) {
		// failedPlanIds list will not be null in ideal case. When all update
		// successful then this map will be of size zero.
		if (null != planIdDocumentsMap && null != failedPlanIdsAndError) {
			
			FTP_STATUS status = null;
			StringBuilder errorMessage = null;
			String[] planDocumentValues = null;
			boolean isDBUpdateSuccess = false;
			
			for (Entry<String, String[]> entryData : planIdDocumentsMap.entrySet()) {
				// Extract required values to update job table.
				status = null;
				errorMessage = new StringBuilder();
				isDBUpdateSuccess = false;
				LOGGER.debug("Creating job entry : planId from planIdDocumentsMap : " + entryData.getKey());
				
				// Check whether planId is in failed plan ids.
				if (failedPlanIdsAndError.containsKey(entryData.getKey())) {
					LOGGER.debug("Creating job entry : failed plan id found : " + entryData.getKey());
					isDBUpdateSuccess = false;
					status = FTP_STATUS.FAILED;
					errorMessage.append(DATABASE_UPDATE_ERROR);
					errorMessage.append(failedPlanIdsAndError.get(entryData.getKey()));
				}
				else {
					LOGGER.debug("Creating job entry : success plan id found : " + entryData.getKey());
					// Setting success flag to true.
					isDBUpdateSuccess = true;
					status = FTP_STATUS.COMPLETED;
				}
				LOGGER.debug("Creating job entry : isSuccess : " + isDBUpdateSuccess + " status : " + status + " errorMessage : " + errorMessage);
				
				planDocumentValues = entryData.getValue();
				if (null == planDocumentValues || planDocumentValues.length == 0) {
					continue;
				}
				
				LOGGER.debug("Creating job entry : hiosProductId from planId : " + planDocumentValues[POS_PLAN_ISSUER_NUM].substring(0, LEN_HIOS_PRODUCT_ID));

				if (planDocumentValues[POS_BROCHURE_ECM] != null && planDocumentValues[POS_BROCHURE_FILE] != null) {
					
					// Update document job table and move file.
					updateStatusRecord(planDocumentValues[POS_BROCHURE_FILE], EMPTY_STRING, BROCHURE_INITIAL, errorMessage.toString(), planDocumentValues[POS_PLAN_ISSUER_NUM], 
							entryData.getKey(), planDocumentValues[POS_BROCHURE_ECM], EMPTY_STRING, status, planDocumentsJobService, ftpClient);
				}
				
				if (planDocumentValues[POS_SBC_ECM] != null && planDocumentValues[POS_SBC_FILE] != null) {
					
					// Update document job table and move file.
					updateStatusRecord(planDocumentValues[POS_SBC_FILE], EMPTY_STRING, SBC_INITIAL, errorMessage.toString(), planDocumentValues[POS_PLAN_ISSUER_NUM], 
							entryData.getKey(), planDocumentValues[POS_SBC_ECM], EMPTY_STRING, status, planDocumentsJobService, ftpClient);
				}
				if (planDocumentValues[POS_EOC_ECM] != null && planDocumentValues[POS_EOC_FILE] != null) {
					
					// Update document job table and move file.
					updateStatusRecord(planDocumentValues[POS_EOC_FILE], EMPTY_STRING, EOC_INITIAL, errorMessage.toString(), planDocumentValues[POS_PLAN_ISSUER_NUM], 
							entryData.getKey(), planDocumentValues[POS_EOC_ECM], EMPTY_STRING, status, planDocumentsJobService, ftpClient);
				}
				
			}
		}
		else {
			LOGGER.error("Incorrect map values created.");
		}
	}

	/**
	 * This method updates values in plan and plan health table.
	 * 
	 * @param planIdBrochureMap
	 * @param planDocumentsJobService
	 * @return
	 */
	private Map<String, String> updatePlanDocuments(Map<String, String[]> planIdBrochureMap,
			PlanDocumentsJobService planDocumentsJobService) {

		LOGGER.info("Map size after manipulation : " + planIdBrochureMap.size()
				+ ". Updating database tables.");
		return planDocumentsJobService.updatePlanDocuments(planIdBrochureMap);
	}

	/**
	 * This is utility method to hold all the plans and uploaded document
	 * information to update those in single go.
	 * 
	 * 
	 * @param issuerPlanNumber
	 * @param documentType
	 * @param documentId
	 * @param fileName
	 * @param idValueMap
	 */
	private void addOrUpdateBrochureValuesInMap(String issuerPlanNumber, String applicableYear, String documentType, String documentId,
			String fileName, Map<String, String[]> idValueMap) {
		// Check whether entry is present in the map for the existing plan.
		String mapKey = issuerPlanNumber + UNDERSCORE + applicableYear;
		String[] brochureValueArr = idValueMap.get(mapKey);

		if (null == brochureValueArr || brochureValueArr.length == 0) {
			// There is no previous entry in the map. Create new entry.
			LOGGER.info("Creating new value array for " + mapKey);
			brochureValueArr = new String[DOCUMENT_ARRAY_SIZE];
			brochureValueArr[POS_PLAN_ISSUER_NUM] = issuerPlanNumber;
			brochureValueArr[POS_APPLICABLE_YEAR] = applicableYear;
		}

		// Either value array is new or already existing , set values and put in
		// the map.
		if (documentType.equals(BROCHURE_INITIAL)) {
			LOGGER.debug("Entry in map for brochure : key : " + mapKey + " , Values Before : "
					+ Arrays.toString(brochureValueArr));
			brochureValueArr[POS_BROCHURE_ECM] = documentId;
			brochureValueArr[POS_BROCHURE_FILE] = fileName;
			LOGGER.debug("Entry in map for brochure  : key : " + mapKey + " , Values After : "
					+ Arrays.toString(brochureValueArr));
		}

		if (documentType.equals(SBC_INITIAL)) {
			LOGGER.debug("Entry in map for SBC : key : " + mapKey + " , Values Before : "
					+ Arrays.toString(brochureValueArr));
			brochureValueArr[POS_SBC_ECM] = documentId;
			brochureValueArr[POS_SBC_FILE] = fileName;
			LOGGER.debug("Entry in map for SBC : key : " + mapKey + " , Values After : "
					+ Arrays.toString(brochureValueArr));
		}if(documentType.equals(EOC_INITIAL)){
			LOGGER.debug("Entry in map for EOC : key : " + mapKey + " , Values Before : "
					+ Arrays.toString(brochureValueArr));
			brochureValueArr[POS_EOC_ECM] = documentId;
			brochureValueArr[POS_EOC_FILE] = fileName;
			LOGGER.debug("Entry in map for SBC : key : " + mapKey + " , Values After : "
					+ Arrays.toString(brochureValueArr));
		}

		// Adding will have no effect when key is present, it will update array
		// values in previous steps.
		idValueMap.put(mapKey, brochureValueArr);
	}

	/**
	 * This method checks whether error and success directories exist in dropbox
	 * directory.
	 * 
	 * @param successDirectoryPath
	 * @param errorDirectoryPath
	 * @param ftpClient
	 * @throws Exception
	 */
	public void checkSuccessAndErrorDirectories(GHIXSFTPClient ftpClient) {

		String dropBoxDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getFtpServerUploadSubpath()
				+ PATH_SEPERATOR + SerffConstants.FTP_BROCHURE_PATH;
		String successDirectoryPath = dropBoxDirectoryPath + PATH_SEPERATOR + SUCCESS;
		String errorDirectoryPath = dropBoxDirectoryPath + PATH_SEPERATOR + ERROR;

		try {
			if (!ftpClient.isRemoteDirectoryExist(successDirectoryPath + PATH_SEPERATOR)) {
				LOGGER.info(" success directory does not exist. Creating success directory in path "
						+ successDirectoryPath);
				ftpClient.createDirectory(successDirectoryPath + PATH_SEPERATOR);
			} else {
				LOGGER.info(" Success directory exist. " + successDirectoryPath);
			}
		} catch (Exception e) {
			LOGGER.error("Error while creating success directory in path " + successDirectoryPath, e);
		}

		try {
			if (!ftpClient.isRemoteDirectoryExist(errorDirectoryPath + PATH_SEPERATOR)) {
				LOGGER.info(" error directory does not exist. Creating error directory in path "
						+ errorDirectoryPath);
				ftpClient.createDirectory(errorDirectoryPath + PATH_SEPERATOR);
			} else {
				LOGGER.info(" Error directory exist. " + errorDirectoryPath);
			}
		} catch (Exception e) {
			LOGGER.error("Error while creating error directory in path " + errorDirectoryPath, e);
		}
	}

	/**
	 * This method will move files in success/error directory in ftp server.
	 * 
	 * @param jobId
	 * @param isSuccess
	 * @param fileName
	 * @param ftpClient
	 * @return
	 */
	public String moveFileToDirectory(int jobId, boolean isSuccess, String fileName, GHIXSFTPClient ftpClient) {
		
		StringBuffer message = new StringBuffer();
		
		try {
			String ftpDropboxPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getFtpServerUploadSubpath() + PATH_SEPERATOR + SerffConstants.FTP_BROCHURE_PATH;
			LOGGER.info("FTP dropbox path while moving file : " + ftpDropboxPath);
			
			if (isSuccess) {
				String ftpSuccessPathWithJobId = ftpDropboxPath + PATH_SEPERATOR + SerffConstants.FTP_BROCHURE_SUCCESS_PATH + jobId;
				LOGGER.debug("Moving file to success directory. Path - " + ftpSuccessPathWithJobId + " File -" + fileName);
				ftpClient.moveFile(ftpDropboxPath + PATH_SEPERATOR, fileName, ftpSuccessPathWithJobId + PATH_SEPERATOR, fileName);
				LOGGER.info("Moving file to success directory completed." + fileName);
			}
			else {
				String ftpErrorPathWithJobId = ftpDropboxPath + PATH_SEPERATOR + SerffConstants.FTP_BROCHURE_ERROR_PATH + jobId;
				LOGGER.debug("Moving file to error directory. Path - " + ftpErrorPathWithJobId + " File -" + fileName);
				ftpClient.moveFile(ftpDropboxPath + PATH_SEPERATOR, fileName, ftpErrorPathWithJobId + PATH_SEPERATOR, fileName);
				LOGGER.info("Moving file to error directory completed." + fileName);
			}
		}
		catch (Exception ex) {
			LOGGER.error("SFTP Error: " + ex.getMessage(), ex);
			message.append(FTP_FILE_MOVE_FAILED);
			message.append(ex.getMessage());
		}
		return message.toString();
	}

	/**
	 * This method creates entry in database for serff_plan_documents_job table.
	 * 
	 * @param documentNewName
	 * @param documentOldName
	 * @param documentType
	 * @param errorMessage
	 * @param hiosProductId
	 * @param issuerPlanNumber
	 * @param planId
	 * @param ucmNewId
	 * @param ucmOldId
	 * @param status
	 * @param planDocumentsJobService
	 * @return
	 */
	private int updatePlanDocumentJobTable(String documentNewName, String documentOldName, String documentType,
			String errorMessage, String hiosProductId, String issuerPlanNumber, String planId, String ucmNewId,
			String ucmOldId, FTP_STATUS status, PlanDocumentsJobService planDocumentsJobService) {
		PlanDocumentsJob planDocumentJob = new PlanDocumentsJob();
		planDocumentJob.setDocumentNewName(documentNewName);
		planDocumentJob.setDocumentOldName(documentOldName);
		planDocumentJob.setDocumentType(documentType);
		planDocumentJob.setErrorMessage(errorMessage);
		planDocumentJob.setHiosProductId(hiosProductId);
		planDocumentJob.setIssuerPlanNumber(issuerPlanNumber);
		planDocumentJob.setLastUpdated(new Date());
		planDocumentJob.setPlanId(planId);
		planDocumentJob.setUcmNewId(ucmNewId);
		planDocumentJob.setUcmOldId(ucmOldId);
		planDocumentJob.setFtpStatus(status);
		return planDocumentsJobService.persistPlanDocumentRecord(planDocumentJob);
	}
	
	/**
	 * Method is used to add data in PlanDocumentJob table.
	 * @return return PlanDocumentsJob created Object.
	 */
	private PlanDocumentsJob createPlanDocumentsJob(String documentNewName, String documentOldName, String documentType,
			String errorMessage, String hiosProductId, String issuerPlanNumber, String planId, String ucmNewId,
			String ucmOldId, FTP_STATUS status, PlanDocumentsJobService planDocumentsJobService) {
		
		PlanDocumentsJob planDocumentJob = new PlanDocumentsJob();
		planDocumentJob.setDocumentNewName(documentNewName);
		planDocumentJob.setDocumentOldName(documentOldName);
		planDocumentJob.setDocumentType(documentType);
		planDocumentJob.setErrorMessage(errorMessage);
		planDocumentJob.setHiosProductId(hiosProductId);
		planDocumentJob.setIssuerPlanNumber(issuerPlanNumber);
		planDocumentJob.setLastUpdated(new Date());
		planDocumentJob.setPlanId(planId);
		planDocumentJob.setUcmNewId(ucmNewId);
		planDocumentJob.setUcmOldId(ucmOldId);
		planDocumentJob.setFtpStatus(status);
		planDocumentsJobService.persistPlanDocumentRecord(planDocumentJob);
		return planDocumentJob;
	}

	/**
	 * 
	 * This method uploads brochure documents from ftp to ecm location.
	 * @return Document Id
	 */
	private String uploadDocumentInEcm(String basePath, String fileName, InputStream docStream, ContentManagementService ecmService)
			throws ContentManagementServiceException, IOException {

		LOGGER.info("uploadDocumentInEcm() Start");
		String documentId = null;
		
		try {
			if (null == ecmService) {
				LOGGER.error("ECM Service object is null.");
				return documentId;
			}
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("uploading file " + SecurityUtil.sanitizeForLogging(fileName) + " in ECM.");
			}
			byte[] fileContent = IOUtils.toByteArray(docStream);
			
				documentId = ecmService.createContent(basePath, fileName, fileContent, ECMConstants.Serff.DOC_CATEGORY, ECMConstants.Serff.DOC_SUB_CATEGORY, null);
			LOGGER.debug("file stored in ECM as " + documentId);
		}
		catch (IOException ioException) {
			LOGGER.error("IOException : ", ioException);
			throw ioException;
		}
		catch (ContentManagementServiceException exception) {
			LOGGER.error("ContentManagementServiceException : ", exception);
			throw exception;
		}
		catch (RuntimeException exception) {
			LOGGER.error("RuntimeException : ",exception);
			LOGGER.error(SerffConstants.EMSG_ECM_RESPONSE);
			throw exception;
		}
		/*catch (Exception exp) {
			LOGGER.error(fileName + " upload to ECM failed. " + exp.getMessage());
			throw exp;
		}*/
		finally {
			LOGGER.info("uploadDocumentInEcm() End");
		}
		return documentId;
	}

	/**
	 * This method returns count of files that are present in ftp location.
	 * 
	 * @param workDirectoryPath
	 * @return
	 * @throws Exception
	 */
	public int getFileCount(String workDirectoryPath, GHIXSFTPClient ftpClient) throws Exception {
		LOGGER.debug("Getting file count from: " + workDirectoryPath);
		int count = 0;

		try {
			Map<String, InputStream> fileList = ftpClient.getFilesFromDirectory(workDirectoryPath + PATH_SEPERATOR);
			if (CollectionUtils.isEmpty(fileList)) {
				LOGGER.warn("There is no file at FTP location.");
				return count;
			}

			for (Entry<String, InputStream> entry : fileList.entrySet()) {

				if (null == entry) {
					continue;
				}
				// Add only valid files to the total file count.
				if (null != getIssuerPlanNumberFromFileName(entry.getKey())) {
					count++;
				}
				// Fixed HP-FOD issue.
				SerffResourceUtil.closeInputStream(entry.getValue());
			}
		}
		catch (Exception ex) {
			LOGGER.info("Exception while getting files count.");
			throw ex;
		}
		return count;
	}
	
	/**
	 * Method is used to Upload Ancillary Document(PDF) to ECM Server. 
	 * @return Upload Document Status Message.
	 */
	@RequestMapping(value = "/admin/uploadAncillaryDocs", method = RequestMethod.POST)
	public String uploadAncillaryDocumentToECM(InputStream ancillaryDocument,
			String fileName, PlanDocumentsJobService planDocumentsJobService,
			ContentManagementService ecmService) {
		
		LOGGER.info("uploadAncillaryDocumentToECM() Begin");
		PlanDocumentsJob planDocumentJob = null;
		String uploadDocumentMessage = MSG_UPLOAD_ANCILLARY_FAIL;
		
		try {
			planDocumentJob = createPlanDocumentsJob(null, null, "A", null, null, null, null, null, null,
					SerffPlanMgmtBatch.FTP_STATUS.IN_PROGRESS, planDocumentsJobService);
			
			if (null == ancillaryDocument) {
				uploadDocumentMessage = "Ancillary Document file is empty.";
				return uploadDocumentMessage;
			}
			
//			if (null != planDocumentJob) {
			planDocumentJob.setDocumentNewName(fileName);
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Ancillary Document Name: " + SecurityUtil.sanitizeForLogging(fileName));
			}
			
			String ecmFolderName = SerffConstants.SERF_ECM_ANCILLARY_BASE_PATH + Calendar.getInstance().getTimeInMillis() + SerffConstants.PATH_SEPERATOR;
			LOGGER.info("ECM Base Folder path: " + ecmFolderName);
			
			String documentId = uploadDocumentInEcm(ecmFolderName, fileName, ancillaryDocument, ecmService);
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Ancillary Document ECM/UCM ID: " + SecurityUtil.sanitizeForLogging(documentId));
			}
			
			if (StringUtils.isNotBlank(documentId)) {
				planDocumentJob.setUcmNewId(documentId);
				planDocumentJob.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.COMPLETED);
				uploadDocumentMessage = null;
			}
			/*}
			else {
				uploadDocumentMessage = "Failed to create record in PlanDocumentsJob table.";
			}*/
		}
		catch (Exception ex) {
			
			if (null != planDocumentJob) {
				uploadDocumentMessage = MSG_UPLOAD_ANCILLARY_FAIL + ex.getMessage();
			}
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			
			if (StringUtils.isNotBlank(uploadDocumentMessage)) {
				
				if (null != planDocumentJob) {
					planDocumentJob.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.FAILED);
					
					if (ERROR_MAX_LEN < uploadDocumentMessage.length()) {
						uploadDocumentMessage = uploadDocumentMessage.substring(0, ERROR_MAX_LEN);
					}
					planDocumentJob.setErrorMessage(uploadDocumentMessage);
				}
				LOGGER.error(uploadDocumentMessage);
			}
			
			if (null != planDocumentJob) {
				planDocumentsJobService.persistPlanDocumentRecord(planDocumentJob);
			}
			LOGGER.info("uploadAncillaryDocumentToECM() End");
		}
		return uploadDocumentMessage;
	}
}
