package com.serff.util;


import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffDocument.DOC_TYPE;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.ecm.MimeConfig;
import com.getinsured.hix.platform.external.cloud.service.ExternalCloudService;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.exception.GIException;
import com.serff.service.SerffService;

/**
 * Class is used to provide Input Output services of ECM or FTP servers.
 * 
 * @author Bhavin Parmar
 * @since January 27, 2015
 */
@Component
public class SerffResourceUtil {

	private static final Logger LOGGER = Logger.getLogger(SerffResourceUtil.class);
	private static final String EMSG_ERROR_ECM = "Failed to upload File at ECM. Reason: ";
	private static final String DISK_SERFF_FILES_PATH = System.getProperty("GHIX_HOME")+File.separatorChar+"ghix-docs"+File.separatorChar;

	@Autowired private SerffService serffService;
	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private ContentManagementService ecmService;
	@Autowired private ExternalCloudService externalCloudService;

	/**
	 * This method adds attachment to ECM
	 * @throws ContentManagementServiceException
	 * 
	 * @param ecmService Autowired ecmService instance which handles operations related to ECM.
	 * @param record SerffPlanMgmt instance saved in database. 		  
	 * @param basePath Path where document is to be saved in ECM.
	 * @param fileName Name of the document.
	 * @param content Byte representation of the document.
	 * @return SerffDocument that is saved in ECM.
	 * @throws GI Exception
	 */
	public SerffDocument addAttachmentInECM(ContentManagementService ecmService, SerffPlanMgmt record,
			String basePath, String folderName, String fileName, byte[] content, boolean bypassable) throws GIException {

		LOGGER.info("Executing addAttachmentInECM() Start");

		try {
			String ecmDocID = uploadDocumentAtECM(ecmService, record, basePath, folderName, fileName, content, bypassable);

			if (StringUtils.isNotEmpty(ecmDocID)) {
				SerffDocument attachment = new SerffDocument();
				attachment.setSerffReqId(record);
				attachment.setEcmDocId(ecmDocID);
				attachment.setDocName(fileName);
				attachment.setDocType(getFileExtension(fileName));
				attachment.setDocSize(content.length);
				return attachment;
			}
		}
		finally {
			LOGGER.info("Executing addAttachmentInECM() End");
		}
		return null;
	}

	/**
	 * This method adds attachment to CDN Server.
	 * @throws GI Exception
	 */
	public SerffDocument addAttachmentInCDN(SerffPlanMgmt record, String fileName, byte[] content) throws GIException {

		LOGGER.info("Executing addAttachmentInCDN() Start");
		SerffDocument attachment = null;
		boolean failedToSetLogo = false;
		String errorMessage = null;

		try {
			
			final String COMPANY_LOGO_MIME_TYPE = "image/gif,image/jpeg,image/png";
			String fileExtension = getFileExtension(fileName).name();

			String actualMimetype = MimeConfig.getMimeConfig().validateIncomingContent(content, fileName);
			//If actual Mime type and the file extension does not match OR actual mime type is not in allowed mime type list
			if(!actualMimetype.contains(fileExtension) || !Arrays.asList(COMPANY_LOGO_MIME_TYPE.split(",")).contains(actualMimetype)) {
				failedToSetLogo = true;
				LOGGER.error("Invalid Company Logo file type: "+actualMimetype+" ,Allowed logo file types : " + COMPANY_LOGO_MIME_TYPE);
			}
			
			Long companyLogoSize = 0L;
			String cdnURL = null;
			String companyLogoStr = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.COMPANYLOGOFILESIZE);

			if (StringUtils.isNotBlank(companyLogoStr)) {
				companyLogoSize = Long.parseLong(companyLogoStr);
			}
			else {
				companyLogoSize = SerffConstants.COMPANY_LOGO_FILE_SIZE;
			}

			if (content.length < companyLogoSize) {
				final String CDN_ENABLED_URL = DynamicPropertiesUtil.getPropertyValue(SerffConstants.CDN_GLOBAL_URL);

				if (StringUtils.isNotBlank(CDN_ENABLED_URL)) {
					URL docUrl = externalCloudService.upload(SerffConstants.CDN_ISSUER_LOGO_PATH, fileName, content);

					if (null != docUrl) {
						cdnURL = docUrl.toString();
						LOGGER.debug("CDN Logo URL: " + cdnURL);
					}
					else {
						failedToSetLogo = true;
						errorMessage = "Failed to upload Company Logo at CDN.";
						LOGGER.error(errorMessage);
					}
				}
				else {
					LOGGER.info("Skipping to upload logo to CDN as CDN is not enable.");
				}
			}
			else {
				failedToSetLogo = true;
				errorMessage = "Company logo size("+ content.length +") is exceeding maximum size " + companyLogoSize;
				LOGGER.error(errorMessage);
			}

			if (!failedToSetLogo) {
				attachment = new SerffDocument();
				attachment.setSerffReqId(record);

				if (StringUtils.isNotBlank(cdnURL)) {
					attachment.setEcmDocId(cdnURL);
				}
				attachment.setDocName(fileName);
				attachment.setDocType(getFileExtension(fileName));
				attachment.setDocSize(content.length);
			}
		}
		catch (IllegalArgumentException ex) {
			throw new GIException(Integer.parseInt(SerffConstants.APP_EXCEPTION_CODE), SerffConstants.APP_EXCEPTION_MESSAGE_FAILED_TO_PUT_DOCUMENT_TO_CDN_MESSAGE + ex.getMessage(), StringUtils.EMPTY);
		}
		catch (Exception ex) {
			throw new GIException(Integer.parseInt(SerffConstants.APP_EXCEPTION_CODE), SerffConstants.APP_EXCEPTION_MESSAGE_FAILED_TO_PUT_DOCUMENT_TO_CDN_MESSAGE + ex.getMessage(), StringUtils.EMPTY);
		}
		finally {

			if (failedToSetLogo) {
				throw new GIException(Integer.parseInt(SerffConstants.APP_EXCEPTION_CODE), SerffConstants.APP_EXCEPTION_MESSAGE_FAILED_TO_PUT_DOCUMENT_TO_CDN_MESSAGE + errorMessage, StringUtils.EMPTY);
			}
			LOGGER.info("Executing addAttachmentInCDN() End");
		}
		return attachment;
	}

	/**
	 * This method adds attachment to ECM
	 * @throws ContentManagementServiceException
	 * 
	 * @param ecmService, Autowired ecmService instance which handles operations related to ECM.
	 * @param basePath, Path where document is to be saved in ECM.
	 * @param fileName, Name of the document.
	 * @param content, Byte representation of the document.
	 * @return ECM Document ID in String format.
	 * @throws GI Exception
	 */
	public String uploadDocumentAtECM(ContentManagementService ecmService, SerffPlanMgmt record,
			String basePath, String folderName, String fileName, byte[] content, boolean bypassable) throws GIException {

		LOGGER.info("Executing uploadDocumentAtECM() Start");
		String ecmDocID = null;
		long currentTime = System.currentTimeMillis();
		try {

			if (null == content || 0 == content.length) {
				LOGGER.error("Content is empty.");
			}

			if (bypassable && "true".equalsIgnoreCase(SerffConstants.getSerffECMBypassEnabled())) {
				ecmDocID = currentTime + "_" + content.length + "_" + fileName ;
				if("true".equalsIgnoreCase(SerffConstants.getSerffDiskLogEnabled()) 
						&& System.getProperty("GHIX_HOME") != null){
					try {
						String diskFilePath = DISK_SERFF_FILES_PATH + basePath + folderName;
						File diskFileDirectory = new File(diskFilePath);
						if(!diskFileDirectory.exists()){
							diskFileDirectory.mkdirs();
						}
						FileUtils.writeByteArrayToFile(new File(diskFilePath + File.separatorChar + ecmDocID), content);
					} catch (IOException e) {
						LOGGER.info("Failed to log document to Disk for " + ecmDocID);
					}
				} else {
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Skippping logging document to Disk for " + fileName + " GHIX_HOME: " + System.getProperty("GHIX_HOME") + " Enable LogToDisk:" + SerffConstants.getSerffDiskLogEnabled());
					}
				}
			}
			else {
				String ecmBasePath = basePath + record.getSerffReqId() + folderName + currentTime + File.separatorChar;
				ecmDocID = ecmService.createContent(ecmBasePath, fileName, content, ECMConstants.Serff.DOC_CATEGORY, ECMConstants.Serff.DOC_SUB_CATEGORY, null);
			}
		}
		catch (ContentManagementServiceException e) {
			LOGGER.error("ContentManagementServiceException: ", e);
			throw new GIException(Integer.parseInt(SerffConstants.APP_EXCEPTION_CODE), SerffConstants.APP_EXCEPTION_MESSAGE_FAILED_TO_PUT_DOCUMENT_TO_ECM_MESSAGE + " : " + e.getMessage(), StringUtils.EMPTY);
		}
		catch (RuntimeException exception) {
			LOGGER.error("untimeException: ", exception);
			throw new GIException(Integer.parseInt(SerffConstants.APP_EXCEPTION_CODE), SerffConstants.APP_EXCEPTION_MESSAGE_FAILED_TO_PUT_DOCUMENT_TO_ECM_MESSAGE + " : " + exception.getMessage(), StringUtils.EMPTY);
		}
		finally {
			LOGGER.info("Executing uploadDocumentAtECM() End");
		}
		return ecmDocID;
	}

	/**
	 * This method returns the file extension for a file
	 * 
	 * @param fileName Name of file whose extension is to be extracted.
	 * @return File extension. 		  
	 */
	private DOC_TYPE getFileExtension(String fileName) {

		LOGGER.info("Executing getFileExtension() with file name: " + fileName);
		DOC_TYPE docType = null;

		if (StringUtils.isBlank(fileName)) {
			return docType;
		}

		int indexOfDot = fileName.lastIndexOf('.');
		if (indexOfDot != -1) {
			String extension = fileName.substring(indexOfDot + 1);
			LOGGER.debug("File Extension: " + extension);

			try {
				docType = DOC_TYPE.valueOf(extension.toLowerCase());
			}
			catch (IllegalArgumentException ex) {
				LOGGER.error("Invalid Document Type: " + extension, ex);
			}
		}
		return docType;
	}

	/**
	 * Method is used to get List of File Name from FTP server.
	 */
	public List<String> getFileNameListFromFTP(String folderPath) {
		LOGGER.debug("getFileNameListFromFTP() Start with FTP Folder Path: " + folderPath);
		List<String> fileNameList = null;

		if (StringUtils.isNotBlank(folderPath)) {
			fileNameList = ftpClient.getFileNames(folderPath);
			LOGGER.debug("File Name List is NULL: " + CollectionUtils.isEmpty(fileNameList));
		}
		LOGGER.debug("getFileNameListFromFTP() End");
		return fileNameList;
	}

	/**
	 * Method is used to upload Excel at ECM server.
	 */
	public boolean uploadFileFromFTPToECM(SerffPlanMgmt trackingRecord, String ftpFolderPath, String templateType, String fileName, boolean bypassable) throws IOException {

		LOGGER.debug("uploadFileFromFTPToECM() Strat");
		boolean isSuccess = false;
		InputStream inputStream = null;

		try {
			inputStream = ftpClient.getFileInputStream(ftpFolderPath + fileName);
			LOGGER.debug("Uploading File Name with path to ECM:" + SerffConstants.SERF_ECM_BASE_PATH + templateType + fileName);
			// Uploading Excel at ECM.
			SerffDocument attachment = addAttachmentInECM(ecmService, trackingRecord, SerffConstants.SERF_ECM_BASE_PATH, templateType, fileName, IOUtils.toByteArray(inputStream), bypassable);
			serffService.saveSerffDocument(attachment);
			isSuccess = true;
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(EMSG_ERROR_ECM + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			// Fixed HP-FOD issue.
			releaseInputStream(inputStream);
			LOGGER.debug("uploadFileFromFTPToECM() End");
		}
		return isSuccess;
	}

	/**
	 * Method is used to Move File from one directory to success/error directory.
	 */
	public void moveFileToDirectory(boolean isSuccess, String fileName, String baseDirectoryPath) {

		LOGGER.debug("moveFileToDirectory() Start");
		String moveToDirectoryPath = null;

		try {
			moveToDirectoryPath = baseDirectoryPath;

			if (isSuccess) {
				moveToDirectoryPath += SerffConstants.FTP_SUCCESS + SerffConstants.PATH_SEPERATOR;
			}
			else {
				moveToDirectoryPath += SerffConstants.FTP_ERROR + SerffConstants.PATH_SEPERATOR;
			}
			moveFileToDirectory(fileName, baseDirectoryPath, moveToDirectoryPath);
		}
		catch (Exception ex) {
			LOGGER.info("Skipping file beacause it is already exist in directory: " + moveToDirectoryPath);
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("moveFileToDirectory() End");
		}
	}

	/**
	 * Method is used to Move File from directory to another directory.
	 */
	public void moveFileToDirectory(String fileName, String baseDirectoryPath, String moveToDirectoryPath) {

		LOGGER.debug("moveFileToDirectory() Start");

		try {
			if (!ftpClient.isRemoteDirectoryExist(moveToDirectoryPath)) {
				ftpClient.createDirectory(moveToDirectoryPath);
			}
			LOGGER.debug("FTP Folder Path: " + baseDirectoryPath);
			LOGGER.info("Move To Directory Path: " + moveToDirectoryPath);
			ftpClient.moveFile(baseDirectoryPath, fileName, moveToDirectoryPath, fileName);
		}
		catch (Exception ex) {
			LOGGER.error("Skipping file beacause it is already exist in directory: " + moveToDirectoryPath);
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("moveFileToDirectory() End");
		}
	}

	/**
	 * Method is used to close InputStream object.
	 */
	public void releaseInputStream(InputStream inputStream) {
		// Fixed HP-FOD issue.
		if (null != inputStream) {
			try {
				inputStream.close();
			}
			catch (IOException e) {
				LOGGER.error("Exception occured while closing File Input Stream.");
			}
		}
	}

	/**
	 * Method is used to close InputStream object.
	 */
	public static void closeInputStream(InputStream inputStream) {
		// Fixed HP-FOD issue.
		if (null != inputStream) {
			try {
				inputStream.close();
			}
			catch (IOException e) {
				LOGGER.error("Exception occured while closing File Input Stream.");
			}
		}
	}
}
