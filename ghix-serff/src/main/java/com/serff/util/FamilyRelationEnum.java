package com.serff.util;

public enum FamilyRelationEnum {

	SPOUSE("Spouse"),

	PARENT("Parent"),

	GRANDPARENT("Grandparent"),

	GRANDCHILD("Grandchild"),

	PARENT_SIBLING("ParentSibling"),

	SIBLING_CHILD("SiblingChild"),

	COUSIN("Cousin"),

	ADOPTED_CHILD("AdoptedChild"),

	FOSTER_CHILD("FosterChild"),

	CHILD_IN_LAW("ChildInLaw"),

	SIBLING_IN_LAW("SiblingInLaw"),

	SIBLING("Sibling"),

	WARD("Ward"),

	STEP_PARENT("StepParent"),

	STEP_CHILD("StepChild"),

	CHILD("Child"),

	SPONSORED_DEPENDENT("SponsoredDependent"),

	DEPENDENT_OF_MINOR_DEPENDENT("DependentOfMinorDependent"),

	EX_SPOUSE("ExSpouse"),

	GUARDIAN("Guardian"),

	COURT_APPOINTED_GUARDIAN("CourtAppointedGuardian"),

	COLLATERAL_DEPENDENT("CollateralDependent"),

	LIFE_PARTNER("LifePartner"),

	ANNULTANT("Annultant"),

	TRUSTEE("Trustee"),

	SELF("Self"),

	PARENT_IN_LAW("ParentInLaw"),

	OTHER_RELATIONSHIP("OtherRelationship"),

	OTHER_RELATIVE("OtherRelative");
	private final String value;

	FamilyRelationEnum(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

}
