package com.serff.util;

/**
 * @author kothari_r This class is used to collect statistics on the count of
 *         plans that got added / updated / deleted / skipped / failed during a plan load
 *         process.
 */
public class PlanUpdateStatistics {

	private int insertedCount;
	private int updatedCount;
	private int totalCount;
	private int skipCount;
	private int skipCertifiedCount;
	private int deletedCount;
	private int failedCount;

	/**
	 * @return count of plans that got Inserted in DB
	 */
	public int getInsertedCount() {
		return insertedCount;
	}

	/**
	 * @return count of plans that got updated in DB
	 */
	public int getUpdatedCount() {
		return updatedCount;
	}

	/* 
	 * @return formatted string containing all statistics in # separated format
	 * I - Inserted, U - Updated, T - Total, S - Skipped, C - Certified Skipped, D - Deleted/Decertify, F - Failed
	 */
	@Override
	public String toString() {
		return "I:" + insertedCount + "#U:" + updatedCount + "#T:" + totalCount + "#S:" + skipCount + "#C:"
				+ skipCertifiedCount + "#D:" + deletedCount + "#F:" + failedCount;
	}

	/**
	 * @return count of total plans that got processed
	 */
	public int getTotalCount() {
		return totalCount;
	}

	/**
	 * @return count of plans that got deleted from DB
	 */
	public int getDeletedCount() {
		return deletedCount;
	}

	/**
	 * @return count of plans that got failed while processing
	 */
	public int getFailedCount() {
		return failedCount;
	}

	/**
	 * @return count of plans that got skipped while processing
	 */
	public int getSkipCount() {
		return skipCount;
	}

	/**
	 * @return count of plans that got skipped as existing plan are certified
	 */
	public int getSkipCertifiedCount() {
		return skipCertifiedCount;
	}

	/**
	 * 
	 */
	public PlanUpdateStatistics() {
		super();
		insertedCount = updatedCount = totalCount = skipCount = skipCertifiedCount = deletedCount = failedCount = 0;
	}

	/**
	 * Adjust statistics when a single plan got added to DB
	 */
	public void planAdded() {
		insertedCount++;
		totalCount++;
	}

	/**
	 * Adjust statistics when a single plan got deleted from DB
	 */
	public void planDeleted() {
		deletedCount++;
	}

	/**
	 * Adjust statistics when multiple plans got deleted from DB
	 */
	public void planDeleted(int deletedCount) {
		this.deletedCount += deletedCount;
	}

	/**
	 * Adjust statistics when a single plan got failed from DB
	 */
	public void planFailed() {
		failedCount++;
		totalCount++;
	}

	/**
	 * Adjust statistics when multiple plans got failed from DB
	 */
	public void planFailed(int failedCount) {
		this.failedCount += failedCount;
		this.totalCount += failedCount;
	}

	/**
	 * Adjust statistics when a single plan got updated to DB
	 * @param decrementSkipCount
	 *  boolean flag to specify if skip count need to be adjusted in
	 *  case of new variant of a CERTIFIED plan 
	 * 
	 */
	public void planUpdated(boolean decrementSkipCount) {
		updatedCount++;
		if (decrementSkipCount) {
			skipCount--;
		} else {
			totalCount++;
		}
	}

	/**
	 * Adjust statistics when a single plan got skipped
	 */
	public void planSkipped() {
		totalCount++;
		skipCount++;
	}

	/**
	 * Adjust statistics when a single plan got skipped as PLAN is already
	 * present in CERTIFIED state
	 */
	public void planSkippedCertified() {
		totalCount++;
		skipCertifiedCount++;
	}
}
