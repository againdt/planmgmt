package com.serff.util;

import java.math.BigInteger;

/**
 * This class is the plan management report record type for Data-wise statistics
 * HIX-26726
 * 
 * @author Nikhil Talreja
 * @since 08 January, 2013
 *
 */

public class PlanManagmentReportsRecord {
	
	private String date;
	private BigInteger createdCount;
	private BigInteger startDateCount;
	private BigInteger endDateCount;
	private BigInteger updationCount;
	private BigInteger certifiedDateCount;
	private BigInteger enrollmentEffectiveDateCount;
	
	public PlanManagmentReportsRecord() {
	}
	
	public PlanManagmentReportsRecord(String date, BigInteger createdCount,
			BigInteger startDateCount, BigInteger endDateCount,
			BigInteger updationCount, BigInteger certifiedDateCount,
			BigInteger enrollmentEffectiveDateCount) {
		this.date = date;
		this.createdCount = createdCount == null ? BigInteger.ZERO : createdCount;
		this.startDateCount = startDateCount == null ? BigInteger.ZERO : startDateCount;
		this.endDateCount = endDateCount == null ? BigInteger.ZERO : endDateCount;
		this.updationCount = updationCount == null ? BigInteger.ZERO : updationCount;
		this.certifiedDateCount = certifiedDateCount == null ? BigInteger.ZERO : certifiedDateCount;
		this.enrollmentEffectiveDateCount = enrollmentEffectiveDateCount == null ? BigInteger.ZERO : enrollmentEffectiveDateCount;
	}

	@Override
	public String toString() {
		return "PlanManagmentReportsRecord [date=" + date + ", createdCount="
				+ createdCount + ", startDateCount=" + startDateCount
				+ ", endDateCount=" + endDateCount + ", updationCount="
				+ updationCount + ", certifiedDateCount=" + certifiedDateCount
				+ ", enrollmentEffectiveDateCount="
				+ enrollmentEffectiveDateCount + "]";
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public BigInteger getCreatedCount() {
		return createdCount;
	}
	public void setCreatedCount(BigInteger createdCount) {
		this.createdCount = createdCount;
	}
	public BigInteger getStartDateCount() {
		return startDateCount;
	}
	public void setStartDateCount(BigInteger startDateCount) {
		this.startDateCount = startDateCount;
	}
	public BigInteger getEndDateCount() {
		return endDateCount;
	}
	public void setEndDateCount(BigInteger endDateCount) {
		this.endDateCount = endDateCount;
	}
	public BigInteger getUpdationCount() {
		return updationCount;
	}
	public void setUpdationCount(BigInteger updationCount) {
		this.updationCount = updationCount;
	}

	public BigInteger getCertifiedDateCount() {
		return certifiedDateCount;
	}

	public void setCertifiedDateCount(BigInteger certifiedDateCount) {
		this.certifiedDateCount = certifiedDateCount;
	}

	public BigInteger getEnrollmentEffectiveDateCount() {
		return enrollmentEffectiveDateCount;
	}

	public void setEnrollmentEffectiveDateCount(
			BigInteger enrollmentEffectiveDateCount) {
		this.enrollmentEffectiveDateCount = enrollmentEffectiveDateCount;
	}
	
}
