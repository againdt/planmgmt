package com.serff.util;

import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

public final class ExcelReportGenerator {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExcelReportGenerator.class);

	private static String SHEET_NAME_PREFIX = "Drug List ";
	private static String TITLE_DRUG_LIST_OLD_COUNT = "Total Drug records in previous list : ";
	private static String TITLE_DRUG_LIST_NEW_COUNT = "Total Drug records in new list : ";
	private static String TITLE_DRUG_LIST_ADDED1 = "Drugs added to the list (";
	private static String TITLE_DRUG_LIST_ADDED2 = " records)";
	private static String TITLE_DRUG_LIST_REMOVED1 = "Drugs removed from the list (";
	private static String TITLE_DRUG_LIST_MODIFIED1 = "Drugs modified in the list (";

	private static String HEADER_DRUG_LIST_DRUG_NAME = "Name";
	private static String HEADER_DRUG_LIST_STRENGTH = "Strength";
	private static String HEADER_DRUG_LIST_FULL_NAME = "Full Name";
	private static String HEADER_DRUG_LIST_RXCUI = "RXCUI";
	private static String HEADER_DRUG_LIST_AUTH_REQD = "Authentication Required";
	private static String HEADER_DRUG_LIST_STEP_THERAPY_REQD = "Step Therapy Required";
	private static String HEADER_DRUG_LIST_TIER_TYPE1 = "Tier Type 1";
	private static String HEADER_DRUG_LIST_TIER_TYPE2 = "Tier Type 2";
	private static String HEADER_DRUG_LIST_OLD = "Previous Value";
	private static String HEADER_DRUG_LIST_NEW = "Updated Value";

	public static void createFormularyDrugListDiffReport(HSSFWorkbook workbook, Map<String, List<String[]>> diffRecordsMap, Long oldDrugCount, Long newDrugCount, int sheetNum) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Executing createFormularyDrugListDiffReport for sheet:" + sheetNum + " OldCount:" + oldDrugCount + " newCount:" + newDrugCount );
		}
		HSSFSheet drugSheet = workbook.createSheet(SHEET_NAME_PREFIX + sheetNum);
		int[] colWidth = {25, 35, 25, 8, 25, 25, 25, 25, 25, 25, 25, 25};
		if(!CollectionUtils.isEmpty(diffRecordsMap)) {
			//drugSheet.setColumnWidth(ZERO, FIFTEEN * TWO_FIFTY_SIX);
			HSSFFont font = workbook.createFont();
			font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			HSSFCellStyle styleNormalBold = workbook.createCellStyle();
			styleNormalBold.setFont(font);
			styleNormalBold.setBorderRight(CellStyle.BORDER_THIN);
			styleNormalBold.setBorderTop(CellStyle.BORDER_THIN);
			styleNormalBold.setBorderBottom(CellStyle.BORDER_THIN);
			styleNormalBold.setBorderLeft(CellStyle.BORDER_THIN);
			styleNormalBold.setAlignment(CellStyle.ALIGN_LEFT);
			
			HSSFCellStyle styleYellowBG = workbook.createCellStyle();
			styleYellowBG.setFont(font);
			styleYellowBG.setFillForegroundColor(HSSFColor.LIGHT_YELLOW.index);
			styleYellowBG.setFillPattern(CellStyle.SOLID_FOREGROUND);
			styleYellowBG.setBorderRight(CellStyle.BORDER_THIN);
			styleYellowBG.setBorderTop(CellStyle.BORDER_THIN);
			styleYellowBG.setBorderBottom(CellStyle.BORDER_THIN);
			styleYellowBG.setBorderLeft(CellStyle.BORDER_THIN);
			styleYellowBG.setAlignment(CellStyle.ALIGN_LEFT);
			
			HSSFCellStyle style2 = workbook.createCellStyle();
			style2.setFont(font);
			style2.setFillForegroundColor(HSSFColor.ROYAL_BLUE.index);
			style2.setFillPattern(CellStyle.SOLID_FOREGROUND);
			style2.setBorderRight(CellStyle.BORDER_THIN);
			style2.setBorderBottom(CellStyle.BORDER_THIN);
			style2.setBorderLeft(CellStyle.BORDER_THIN);
			style2.setBorderTop(CellStyle.BORDER_THIN);
			style2.setAlignment(CellStyle.ALIGN_CENTER);
			
			HSSFCellStyle stylePaleBlueBG = workbook.createCellStyle();
			stylePaleBlueBG.setFont(font);
			stylePaleBlueBG.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
			stylePaleBlueBG.setFillPattern(CellStyle.SOLID_FOREGROUND);
			stylePaleBlueBG.setBorderRight(CellStyle.BORDER_THIN);
			stylePaleBlueBG.setBorderBottom(CellStyle.BORDER_THIN);
			stylePaleBlueBG.setBorderLeft(CellStyle.BORDER_THIN);
			stylePaleBlueBG.setBorderTop(CellStyle.BORDER_THIN);
			stylePaleBlueBG.setAlignment(CellStyle.ALIGN_CENTER);

			HSSFCellStyle styleNormal = workbook.createCellStyle();
			styleNormal.setAlignment(CellStyle.ALIGN_LEFT);
			styleNormal.setBorderRight(CellStyle.BORDER_THIN);
			styleNormal.setBorderBottom(CellStyle.BORDER_THIN);
			styleNormal.setBorderLeft(CellStyle.BORDER_THIN);
			styleNormal.setBorderTop(CellStyle.BORDER_THIN);
			LOGGER.debug("Styles created for sheet");

			List<String[]> addList = diffRecordsMap.get(SerffConstants.DRUG_LIST_REC_ADDED); 
			List<String[]> removeList = diffRecordsMap.get(SerffConstants.DRUG_LIST_REC_REMOVED);
			List<String[]> modifiedList = diffRecordsMap.get(SerffConstants.DRUG_LIST_REC_MODIFIED);
			for(int i = 0; i < colWidth.length; i++) {
				drugSheet.setColumnWidth(i, colWidth[i] * 256);
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Count for record Added:" + addList.size() + " Removed:" + removeList.size() + " Modified:" + modifiedList.size() );
			}
			
			LOGGER.debug("Adding section for count");
			String[] title = new String[1] ; 
			String[] header = new String[12] ; 
			int currRowNum = 0;
			title[0] = TITLE_DRUG_LIST_OLD_COUNT + oldDrugCount;
			drugSheet.addMergedRegion(new CellRangeAddress(currRowNum, currRowNum, SerffConstants.DRUG_LIST_OLD_RX_NAME, SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2));
			createRow(currRowNum++, title.length, 0, title, 0, drugSheet, workbook, styleYellowBG);
			currRowNum++;
			
			title[0] = TITLE_DRUG_LIST_NEW_COUNT + newDrugCount;
			drugSheet.addMergedRegion(new CellRangeAddress(currRowNum, currRowNum, SerffConstants.DRUG_LIST_OLD_RX_NAME, SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2));
			createRow(currRowNum++, title.length, 0, title, 0, drugSheet, workbook, styleYellowBG);
			currRowNum++;
			
			LOGGER.debug("Adding section for records added");
			title[0] = TITLE_DRUG_LIST_ADDED1 + addList.size() + TITLE_DRUG_LIST_ADDED2;
			drugSheet.addMergedRegion(new CellRangeAddress(currRowNum, currRowNum, SerffConstants.DRUG_LIST_OLD_RX_NAME, SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2));
			createRow(currRowNum++, title.length, 0, title, 0, drugSheet, workbook, styleYellowBG);
			header[SerffConstants.DRUG_LIST_OLD_RX_NAME] = HEADER_DRUG_LIST_DRUG_NAME;
			header[SerffConstants.DRUG_LIST_OLD_RX_FULL_NAME] = HEADER_DRUG_LIST_FULL_NAME;
			header[SerffConstants.DRUG_LIST_OLD_RX_STRENGTH] = HEADER_DRUG_LIST_STRENGTH;
			header[SerffConstants.DRUG_LIST_OLD_RXCUI] = HEADER_DRUG_LIST_RXCUI;
			header[SerffConstants.DRUG_LIST_OLD_AUTH_REQUIRED] = HEADER_DRUG_LIST_AUTH_REQD;
			header[SerffConstants.DRUG_LIST_OLD_STEP_THERAPY_REQUIRED] = HEADER_DRUG_LIST_STEP_THERAPY_REQD;
			header[SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE1] = HEADER_DRUG_LIST_TIER_TYPE1;
			header[SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2] = HEADER_DRUG_LIST_TIER_TYPE2;
			createRow(currRowNum++, SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2 + 1, 0, header, 0, drugSheet, workbook, stylePaleBlueBG);
			for (String[] drugData : addList) {
				createRow(currRowNum, SerffConstants.DRUG_LIST_OLD_RX_STRENGTH+1, SerffConstants.DRUG_LIST_OLD_RX_NAME, drugData, 0, drugSheet, workbook, styleNormal);
				createRow(currRowNum++, (SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2 - SerffConstants.DRUG_LIST_OLD_RX_STRENGTH), 
								SerffConstants.DRUG_LIST_NEW_RXCUI, drugData, SerffConstants.DRUG_LIST_OLD_RX_STRENGTH+1, drugSheet, workbook, styleNormal);
			}
			
			currRowNum++;
			LOGGER.debug("Adding section for records removed");
			title[0] = TITLE_DRUG_LIST_REMOVED1 + removeList.size() + TITLE_DRUG_LIST_ADDED2;
			drugSheet.addMergedRegion(new CellRangeAddress(currRowNum, currRowNum, SerffConstants.DRUG_LIST_OLD_RX_NAME, SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2));
			createRow(currRowNum++, title.length, 0, title, 0, drugSheet, workbook, styleYellowBG);
			createRow(currRowNum++, SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2 + 1, 0, header, 0, drugSheet, workbook, stylePaleBlueBG);
			for (String[] drugData : removeList) {
				createRow(currRowNum, SerffConstants.DRUG_LIST_OLD_RX_STRENGTH + 1, SerffConstants.DRUG_LIST_OLD_RX_NAME, drugData, 0, drugSheet, workbook, styleNormal);
				createRow(currRowNum++, (SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2 - SerffConstants.DRUG_LIST_OLD_RX_STRENGTH),
								SerffConstants.DRUG_LIST_OLD_RXCUI, drugData, SerffConstants.DRUG_LIST_OLD_RX_STRENGTH+1, drugSheet, workbook, styleNormal);
			}
			
			
			currRowNum++;
			LOGGER.debug("Adding section for records modified");
			title[0] = TITLE_DRUG_LIST_MODIFIED1 + modifiedList.size() + TITLE_DRUG_LIST_ADDED2;
			drugSheet.addMergedRegion(new CellRangeAddress(currRowNum, currRowNum, SerffConstants.DRUG_LIST_OLD_RX_NAME, SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2));
			createRow(currRowNum++, title.length, 0, title, 0, drugSheet, workbook, styleYellowBG);
			drugSheet.addMergedRegion(new CellRangeAddress(currRowNum, currRowNum, SerffConstants.DRUG_LIST_OLD_RX_NAME, SerffConstants.DRUG_LIST_OLD_RXCUI));
			drugSheet.addMergedRegion(new CellRangeAddress(currRowNum, currRowNum, SerffConstants.DRUG_LIST_OLD_AUTH_REQUIRED, (SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2)));
			drugSheet.addMergedRegion(new CellRangeAddress(currRowNum, currRowNum, SerffConstants.DRUG_LIST_NEW_RXCUI, SerffConstants.DRUG_LIST_NEW_DRUG_TIER_TYPE2 - 1));
			String[] subheader = new String[12] ; 
			subheader[SerffConstants.DRUG_LIST_OLD_AUTH_REQUIRED] = HEADER_DRUG_LIST_OLD;
			subheader[SerffConstants.DRUG_LIST_NEW_RXCUI] = HEADER_DRUG_LIST_NEW;
			createRow(currRowNum++, SerffConstants.DRUG_LIST_NEW_DRUG_TIER_TYPE2, 0, subheader, 0, drugSheet, workbook, stylePaleBlueBG);
			
			header[SerffConstants.DRUG_LIST_NEW_AUTH_REQUIRED-1] = HEADER_DRUG_LIST_AUTH_REQD;
			header[SerffConstants.DRUG_LIST_NEW_STEP_THERAPY_REQUIRED-1] = HEADER_DRUG_LIST_STEP_THERAPY_REQD;
			header[SerffConstants.DRUG_LIST_NEW_DRUG_TIER_TYPE1-1] = HEADER_DRUG_LIST_TIER_TYPE1;
			header[SerffConstants.DRUG_LIST_NEW_DRUG_TIER_TYPE2-1] = HEADER_DRUG_LIST_TIER_TYPE2;

			createRow(currRowNum++, SerffConstants.DRUG_LIST_NEW_DRUG_TIER_TYPE2, 0, header, 0, drugSheet, workbook, stylePaleBlueBG);
			for (String[] drugData : modifiedList) {
				createRow(currRowNum, SerffConstants.DRUG_LIST_OLD_RX_STRENGTH + 1, SerffConstants.DRUG_LIST_OLD_RX_NAME, drugData, 0, drugSheet, workbook, styleNormal);
				createRow(currRowNum, (SerffConstants.DRUG_LIST_OLD_DRUG_TIER_TYPE2 - SerffConstants.DRUG_LIST_OLD_RX_STRENGTH),
								SerffConstants.DRUG_LIST_OLD_RXCUI, drugData, SerffConstants.DRUG_LIST_OLD_RX_STRENGTH+1, drugSheet, workbook, styleNormal);

				createRow(currRowNum++, (SerffConstants.DRUG_LIST_NEW_DRUG_TIER_TYPE2 - SerffConstants.DRUG_LIST_NEW_RXCUI), 
								SerffConstants.DRUG_LIST_NEW_AUTH_REQUIRED, drugData, SerffConstants.DRUG_LIST_NEW_RXCUI, drugSheet, workbook, styleNormal);
			}
		}
	}
	
	private static void createRow(int rowNum, int colCount, int colStartIndex, String[] colDataList, int dataStartIndex, HSSFSheet sheet, HSSFWorkbook workbook, HSSFCellStyle cellStyle) {
		Row row = sheet.getRow(rowNum);
		if(null == row) {
			row = sheet.createRow(rowNum);
		}
		for(int i = 0; i < colCount; i++) {
			Cell cellA = row.createCell(dataStartIndex+i);
			cellA.setCellValue(colDataList[colStartIndex + i]);
			cellA.setCellStyle(cellStyle);
		}
	}
}
