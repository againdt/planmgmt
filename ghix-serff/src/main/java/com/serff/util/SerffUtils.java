package com.serff.util;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.util.SerffConstants.DATA_ADMIN_DATA;
import static com.serff.util.SerffConstants.DATA_BENEFITS;
import static com.serff.util.SerffConstants.DATA_DENTAL_BENEFITS;
import static com.serff.util.SerffConstants.DATA_ECP_DATA;
import static com.serff.util.SerffConstants.DATA_NCQA_DATA;
import static com.serff.util.SerffConstants.DATA_NETWORK_ID;
import static com.serff.util.SerffConstants.DATA_PRESCRIPTION_DRUG;
import static com.serff.util.SerffConstants.DATA_RATING_RULES;
import static com.serff.util.SerffConstants.DATA_RATING_TABLE;
import static com.serff.util.SerffConstants.DATA_SERVICE_AREA;
import static com.serff.util.SerffConstants.DATA_UNIFIED_RATE;
import static com.serff.util.SerffConstants.DATA_URAC_DATA;
import static com.serff.util.SerffConstants.HIOS_ISSUER_ID;
import static com.serff.util.SerffConstants.INITIAL_INDEX;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.ConcurrentSkipListMap;

import javax.activation.DataHandler;
import javax.persistence.EntityManager;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws.PlanMgmtRequest;
import com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws.PlanMgmtResponse;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.PlanRate;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.TenantPlan;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration.PlanmgmtConfigurationEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.HIXHTTPClient;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.StateHelper.State;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.serff.config.SerffConfiguration.SerffConfigurationEnum;
import com.google.gson.Gson;
import com.rits.cloning.Cloner;
import com.serff.planmanagementexchangeapi.common.model.pm.Attachment;
import com.serff.planmanagementexchangeapi.common.model.pm.Attachments;
import com.serff.planmanagementexchangeapi.common.model.pm.SupportingDocument;
import com.serff.planmanagementexchangeapi.common.model.pm.SupportingDocuments;
import com.serff.planmanagementexchangeapi.common.model.pm.ValidationError;
import com.serff.planmanagementexchangeapi.common.model.pm.ValidationErrors;
import com.serff.planmanagementexchangeapi.exchange.model.pm.DataTemplate;
import com.serff.planmanagementexchangeapi.exchange.model.pm.DataTemplates;
import com.serff.planmanagementexchangeapi.exchange.model.pm.Insurer;
import com.serff.planmanagementexchangeapi.exchange.model.pm.Plan;
import com.serff.planmanagementexchangeapi.exchange.model.pm.TransferDataTemplate;
import com.serff.planmanagementexchangeapi.exchange.model.pm.TransferDataTemplates;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlan;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlanResponse;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlanResponseEx;
import com.serff.planmanagementexchangeapi.exchange.model.service.ValidateAndTransformDataTemplate;
import com.serff.repository.templates.ITenantRepository;
import com.serff.service.SerffService;
import com.serff.service.SerffTemplateService;
import com.serff.service.validation.BusinessRuleValidator;
import com.serff.service.validation.FailedValidation;
import com.serff.service.validation.IPrescriptionDrugValidator;
import com.serff.service.validation.IssuerDataValidator;
import com.serff.service.validation.NetworkValidator;
import com.serff.service.validation.PlanValidator;
import com.serff.service.validation.RatesValidator;
import com.serff.service.validation.SerffValidator;
import com.serff.service.validation.ServiceAreaValidator;
import com.serff.service.validation.UnifiedRateValidator;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.HeaderVO;
import com.serff.template.plan.PlanBenefitTemplateVO;
import com.serff.template.plan.PrescriptionDrugTemplateVO;
import com.serff.template.plan.QhpApplicationRateGroupListVO;
import com.serff.template.plan.QhpApplicationRateGroupVO;
import com.serff.template.plan.QhpApplicationRateItemVO;
import com.serff.template.svcarea.hix.pm.GeographicAreaType;
import com.serff.template.svcarea.niem.usps.states.USStateCodeSimpleType;
import com.serff.template.urrt.InsuranceRateForecastTempVO;

/**
 * Utility class for SERFF service
 * @author Nikhil Talreja
 * @since 08 March, 2013
 */
@Component
public class SerffUtils {

	private static final Logger LOGGER = Logger.getLogger(SerffUtils.class);
	private static final int TEMPLATE_COUNT = 4;
	private static final String FAMILY_TIER_RATES = "Family-Tier Rates";
	private static final String FAMILY_OPTION = "Family Option";
	
	@Qualifier("hIXHTTPClient")
	@Autowired private HIXHTTPClient hixHttpClient;

	@Autowired private PlanValidator planValidatorV6;
	@Autowired private PlanValidator planValidatorV5;
	@Autowired private BusinessRuleValidator businessRuleValidator ;
	@Autowired private UnifiedRateValidator unifiedRateValidator ;
	@Autowired private IPrescriptionDrugValidator prescriptionDrugValidatorV5;
	@Autowired private ServiceAreaValidator serviceAreaValidator;
	@Autowired private IssuerDataValidator issuerDataValidator;
	@Autowired private NetworkValidator networkValidator;
	@Autowired private RatesValidator ratesValidator;
	@Autowired private SerffTemplateService serffTemplateService;
	@Autowired private ITenantRepository iTenantRepository;
	@Autowired private SerffResourceUtil serffResourceUtil;
	@Autowired private Gson platformGson;

	private static final String RAW_TYPES = "rawtypes";

	//This map hold the mapping for Object unmarshalling of templates
	@SuppressWarnings(RAW_TYPES)
	private static Map<String, Class> templateTypeMap = new ConcurrentSkipListMap<String, Class>();
	@SuppressWarnings(RAW_TYPES)
	private static Map<String, Class> templateTypeMap2018 = new ConcurrentSkipListMap<String, Class>();
	@SuppressWarnings(RAW_TYPES)
	private static Map<String, Class> templateTypeMap2019 = new ConcurrentSkipListMap<String, Class>();

	//Change this to true while Unit Testing
	private static final boolean IS_TEST = false;
	private static final long SLEEP_TIME = 1;

	private List<FailedValidation> validationErrors = new ArrayList<FailedValidation>();
	private static final int CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_TYPE = -1;
	private static final int NOT_COVERED_TYPE = 0; 
	private static final int CO_INS_ATTR_EX_TYPE = 1; 
	private static final int CO_INS_VAL_ATTR_TYPE = 2; 
	private static final int CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_TYPE_EX = 3;
	private static final int CO_PAY_ATTR_EX_TYPE = 4; 
	private static final int CO_PAY_VAL_ATTR_EX_TYPE = 5;
	private static final int CO_PAY_VAL_ATTR_CO_INS_VAL_ATTR_TYPE = 6;
	private static final int NOT_COVERED_CO_PAY_VAL_ATTR_TYPE = 7;
	private static final int NO_CHARGE_TYPE = 11;
	private static final int NO_CHARGE_EX_TYPE = 12;
	private static final int COPAY_ONLY_TYPE = 13;
	private static final int COINS_ONLY_TYPE = 14;
	private static final int COINS_COPAY_NO_CHARGE_AFTER_DEDT_TYPE = 15;
	private static final int COPAY_COINS_NO_CHARGE_AFTER_DEDT_TYPE = 16;
	private static final int COPAY_COINS_TYPE = 17;
	
	private static final String LINE_BREAK = "<br>";
	private static final String NOT_APPLICABLE_TEXT = "n/a";
	private static final String COINS_AFTER_DEDUCTIBLE = "Coinsurance after deductible";
	private static final String COINS_BEFORE_DEDUCTIBLE = "Coinsurance before deductible";
	private static final String COINSURANCE = "Coinsurance";
	private static final String PERCENT_COINSURANCE = "Percent co-insurance";
	private static final String COPAY = "Copay";
	private static final String COPAY_BEFORE_DEDUCTIBLE = "Copay before deductible";
	private static final String COPAY_AFTER_DEDUCTIBLE = "Copay after deductible";
	private static final String NO_CHARGE_AFTER_DEDUCTIBLE = "No Charge after deductible";
	private static final String DOLLAR_AMOUNT_COPAY = "Dollar Amount Copay";
	private static final String DOLLAR_AMOUNT_PREFIX = "Dollar Amount ";
	public static final String COPAY_PER_DAY = "Copay per day";
	private static final String COPAY_PER_STAY = "Copay per stay";
	private static final String REQUEST_STATUS = "Request completed successfully";
	private static final String SAME_HIOS_ID_MSG = "HIOS Issuer Id must be same in all templates";
	private static final String REQUIRED_TEMPLATE_MSG =" templates are required" ; 
	private static final String ERR_RESPONSE_MSG = "Error response from Plan Management service";
	private static final String ZERO_STRING = "0";
	private static final String FIRST_PREFIX = "First ";
	private static final String SPACE = " ";
	private static final String SPACE_SLASH_SPACE = " / ";
	private static final String NOT_COVERED_TEXT = "Not covered";
	private static final String NO_CHARGE_TEXT = "No Charge";
	//private static final String BEFORE_DEDUCTIBLE_SUFFIX = " before deductible";
	private static final String AFTER_DEDUCTIBLE_SUFFIX = " after deductible";
	private static final String DEDUCTIBLE_TEXT = "deductible";
	private static final String PER_STAY_SUFFIX = " per stay";
	private static final String PER_DAY_SUFFIX = " per day";
	private static final String COPAYS_AT = " visits at ";
	private static final String VISIT_AT_NO_CHARGE = " visits at no charge";
	private static final String PRIMARY_CARE_LINE_BREAK_1 = ", <br> then additional visits at ";
	private static final String PRIMARY_CARE_LINE_BREAK = ", <br> then ";
	private static final Double HUNDRED = 100d;
	private static final int ATTACHMENT_LIST_MAXLEN = 1000;

	private static final String[] COINSURANCE_ATTR_LIST = {NO_CHARGE_TEXT, COINSURANCE, PERCENT_COINSURANCE, NO_CHARGE_AFTER_DEDUCTIBLE, COINS_BEFORE_DEDUCTIBLE, COINS_AFTER_DEDUCTIBLE};
	private static final String[] COPAY_ATTR_LIST = {NO_CHARGE_TEXT, DOLLAR_AMOUNT_COPAY, NO_CHARGE_AFTER_DEDUCTIBLE, COPAY_BEFORE_DEDUCTIBLE, COPAY_AFTER_DEDUCTIBLE, COPAY_PER_STAY, COPAY_PER_DAY};
	
	private static final String[] COINSURANCE_ATTR_LIST_FOR_STATE = {NO_CHARGE_TEXT, COINSURANCE, PERCENT_COINSURANCE, NO_CHARGE_AFTER_DEDUCTIBLE, COINS_AFTER_DEDUCTIBLE};
	private static final String[] COPAY_ATTR_LIST_FOR_STATE = {NO_CHARGE_TEXT, NO_CHARGE_AFTER_DEDUCTIBLE, DOLLAR_AMOUNT_COPAY, COPAY_AFTER_DEDUCTIBLE, COPAY_BEFORE_DEDUCTIBLE, COPAY_PER_STAY, COPAY_PER_DAY};

	public static final String PRIMARY_CARE_BENEFIT = "PRIMARY_VISIT";
	public static final String INPATIENT_HOSPITAL_SERVICE_BENEFIT = "INPATIENT_HOSPITAL_SERVICE";
	public static final String SPECIALTY_DRUGS_BENEFIT = "SPECIALTY_DRUGS";

	
	static{
		templateTypeMap.put(DATA_BENEFITS, com.serff.template.plan.PlanBenefitTemplateVO.class);
		templateTypeMap.put(DATA_ADMIN_DATA, com.serff.template.admin.extension.PayloadType.class);
		templateTypeMap.put(DATA_PRESCRIPTION_DRUG, com.serff.template.plan.PrescriptionDrugTemplateVO.class);
		templateTypeMap.put(DATA_ECP_DATA, com.serff.template.plan.QhpApplicationEcpVO.class);
		templateTypeMap.put(DATA_RATING_RULES, com.serff.template.rbrules.extension.PayloadType.class);
		templateTypeMap.put(DATA_RATING_TABLE, com.serff.template.plan.QhpApplicationRateGroupVO.class);
		templateTypeMap.put(DATA_SERVICE_AREA, com.serff.template.svcarea.extension.PayloadType.class);
		templateTypeMap.put(DATA_NETWORK_ID, com.serff.template.networkProv.extension.PayloadType.class);
		templateTypeMap.put(DATA_DENTAL_BENEFITS, com.serff.template.plan.PlanBenefitTemplateVO.class);
		templateTypeMap.put(DATA_UNIFIED_RATE, com.serff.template.urrt.InsuranceRateForecastTempVO.class);
		templateTypeMap.put(DATA_URAC_DATA, com.serff.template.urac.IssuerType.class);
		templateTypeMap.put(DATA_NCQA_DATA, com.serff.template.ncqa.ObjectFactory.class);
		
		templateTypeMap2018.put(DATA_BENEFITS, com.serff.template.plan.PlanBenefitTemplateVO.class);
		templateTypeMap2018.put(DATA_ADMIN_DATA, com.serff.template.admin.extension.PayloadType.class);
		templateTypeMap2018.put(DATA_PRESCRIPTION_DRUG, com.serff.template.plan.PrescriptionDrugTemplateVO.class);
		templateTypeMap2018.put(DATA_ECP_DATA, com.serff.template.plan.QhpApplicationEcpVO.class);
		templateTypeMap2018.put(DATA_RATING_RULES, com.serff.template.rbrules.extension.PayloadType.class);
		templateTypeMap2018.put(DATA_RATING_TABLE, com.serff.template.plan.QhpApplicationRateGroupListVO.class);
		templateTypeMap2018.put(DATA_SERVICE_AREA, com.serff.template.svcarea.extension.PayloadType.class);
		templateTypeMap2018.put(DATA_NETWORK_ID, com.serff.template.networkProv.extension.PayloadType.class);
		templateTypeMap2018.put(DATA_DENTAL_BENEFITS, com.serff.template.plan.PlanBenefitTemplateVO.class);
		templateTypeMap2018.put(DATA_UNIFIED_RATE, com.serff.template.urrt.InsuranceRateForecastTempVO.class);
		templateTypeMap2018.put(DATA_URAC_DATA, com.serff.template.urac.IssuerType.class);
		templateTypeMap2018.put(DATA_NCQA_DATA, com.serff.template.ncqa.ObjectFactory.class);

		templateTypeMap2019.put(DATA_BENEFITS, com.serff.template.plan.PlanBenefitTemplateVO.class);
		templateTypeMap2019.put(DATA_ADMIN_DATA, com.serff.template.admin.extension.PayloadType.class);
		templateTypeMap2019.put(DATA_PRESCRIPTION_DRUG, com.serff.template.plan.PrescriptionDrugTemplateVO.class);
		templateTypeMap2019.put(DATA_ECP_DATA, com.serff.template.plan.QhpApplicationEcpVO.class);
		templateTypeMap2019.put(DATA_RATING_RULES, com.serff.template.rbrules.extension.PayloadType.class);
		templateTypeMap2019.put(DATA_RATING_TABLE, com.serff.template.plan.QhpApplicationRateGroupListVO.class);
		templateTypeMap2019.put(DATA_SERVICE_AREA, com.serff.template.plan.BenefitServiceAreaTemplateVO.class);
		templateTypeMap2019.put(DATA_NETWORK_ID, com.serff.template.networkProv.extension.PayloadType.class);
		templateTypeMap2019.put(DATA_DENTAL_BENEFITS, com.serff.template.plan.PlanBenefitTemplateVO.class);
		templateTypeMap2019.put(DATA_UNIFIED_RATE, com.serff.template.urrt.InsuranceRateForecastTempVO.class);
		templateTypeMap2019.put(DATA_URAC_DATA, com.serff.template.urac.IssuerType.class);
		templateTypeMap2019.put(DATA_NCQA_DATA, com.serff.template.ncqa.ObjectFactory.class);
	}

	enum BenefitDisplayType {
		CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR(CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_TYPE),
		NOT_COVERED(NOT_COVERED_TYPE), 
		CO_INS_ATTR_EX(CO_INS_ATTR_EX_TYPE), 
		CO_INS_VAL_ATTR(CO_INS_VAL_ATTR_TYPE), 
		CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_EX(CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_TYPE_EX),
		CO_PAY_ATTR_EX(CO_PAY_ATTR_EX_TYPE), 
		CO_PAY_VAL_ATTR_EX(CO_PAY_VAL_ATTR_EX_TYPE),
		CO_PAY_VAL_ATTR_CO_INS_VAL_ATTR(CO_PAY_VAL_ATTR_CO_INS_VAL_ATTR_TYPE),
		NOT_COVERED_CO_PAY_VAL_ATTR(NOT_COVERED_CO_PAY_VAL_ATTR_TYPE);
		
		private int dispType;

		private BenefitDisplayType(int dispType) {
			this.dispType = dispType;
		}

		public String getDispTileText(String coInsVal, String coInsAttr, String coPayVal, String coPayAttr, boolean isPHIXProfile) {
			StringBuffer tileText = new StringBuffer();
			LOGGER.debug("Tile Display coInsurance Val:" + coInsVal + " coIns Attr:" + coInsAttr + " coPay Val:" + coPayVal + " coPay Attribute:" + coPayAttr + " disp Type:" + dispType);
			switch(dispType) {
				case NOT_COVERED_TYPE: {
					tileText.append(isPHIXProfile ? NOT_COVERED_TEXT : StringUtils.EMPTY);
					break;
				}
				case CO_INS_ATTR_EX_TYPE: 
				case CO_INS_VAL_ATTR_TYPE: { 
					boolean isAttrCoInsOrNoCharge = false;
					if(coInsAttr != null) {
						isAttrCoInsOrNoCharge = coInsAttr.equalsIgnoreCase(NO_CHARGE_TEXT) || coInsAttr.equalsIgnoreCase(COINSURANCE) || coInsAttr.equalsIgnoreCase(PERCENT_COINSURANCE);
					}
					if(coPayAttr == null && isAttrCoInsOrNoCharge && coInsVal.equals(ZERO_STRING)) {
						tileText.append(SerffConstants.DOLLARSIGN);
						tileText.append(coInsVal);
					} else {
						tileText.append(coInsVal);
						tileText.append(SerffConstants.PERCENTAGE);
					}
					break;
				}
				case CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_TYPE:
				case CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_TYPE_EX: { 
					tileText.append(coInsVal);
					tileText.append(SerffConstants.PERCENTAGE);
					tileText.append(SPACE_SLASH_SPACE);
					tileText.append(SerffConstants.DOLLARSIGN );
					tileText.append(coPayVal); 
					break;
				}
				case CO_PAY_ATTR_EX_TYPE: 
				case CO_PAY_VAL_ATTR_EX_TYPE: { 
					tileText.append(SerffConstants.DOLLARSIGN );
					tileText.append(coPayVal); 
					if(coInsVal != null && !ZERO_STRING.equals(coPayVal) && COINS_AFTER_DEDUCTIBLE.equalsIgnoreCase(coInsAttr)) {
						LOGGER.debug("Coins not null and copay not null and CoIns is COINS_AFTER_DEDUCTIBLE");
						if(COPAY_BEFORE_DEDUCTIBLE.equalsIgnoreCase(coPayAttr) || COPAY_AFTER_DEDUCTIBLE.equalsIgnoreCase(coPayAttr)) {
							tileText.append(SPACE_SLASH_SPACE);
							//tileText.append(SerffConstants.DOLLARSIGN );
							tileText.append(ZERO_STRING); 
							tileText.append(SerffConstants.PERCENTAGE);
						}
					}
					break;
				}
				case CO_PAY_VAL_ATTR_CO_INS_VAL_ATTR_TYPE: { 
					tileText.append(SerffConstants.DOLLARSIGN);
					tileText.append(coPayVal);
					tileText.append(SPACE_SLASH_SPACE);
					tileText.append(coInsVal);
					tileText.append(SerffConstants.PERCENTAGE); 
					break;
				}
				case NOT_COVERED_CO_PAY_VAL_ATTR_TYPE: { 
					tileText.append(NOT_COVERED_TEXT);
					tileText.append(SPACE_SLASH_SPACE);
					tileText.append(SerffConstants.DOLLARSIGN);
					tileText.append(coPayVal); 
					break;
				}
				default : LOGGER.info("case did not match to create tile text:");
			}		
			LOGGER.debug("Tile Display value :" + tileText.toString());
			return tileText.toString();
		}
	
		public String getDisplayText(String coInsVal, String coInsAttr, String coPayVal, String coPayAttr) {
			String standardText = null;
			int switchValue = dispType;
			LOGGER.debug("Benefit Display coInsVal:" + coInsVal + " coInsAttr:" + coInsAttr + " coPayVal:" + coPayVal + " coPayAttr:" + coPayAttr + " dispType:" + dispType + " switch :" + switchValue);
			switch(switchValue) {
				case NOT_COVERED_TYPE: {
					standardText = NOT_COVERED_TEXT;
					break;
				}
				case CO_INS_ATTR_EX_TYPE: { 
					standardText = coInsAttr;
					//<B>(replace Coinsurance with 'No Charge')
					if(standardText != null) {
						standardText = standardText.replace(COINSURANCE, NO_CHARGE_TEXT);
					}
					break;
				}
				case CO_INS_VAL_ATTR_TYPE: { 
					//<A> <B> 
					standardText = coInsVal + SerffConstants.PERCENTAGE + SPACE + coInsAttr; 
					break;
				}
				case CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_TYPE_EX: { 
					standardText = SerffConstants.DOLLARSIGN + coPayVal + SPACE + coPayAttr; 
					if(standardText != null) {
						standardText = standardText.replace(DOLLAR_AMOUNT_PREFIX, StringUtils.EMPTY);
					}
					standardText = coInsVal + SerffConstants.PERCENTAGE + SPACE + coInsAttr  + LINE_BREAK + standardText;
					//<A><B><C> <D> (replace Dollar Amount with '')
					break;
				}
				case CO_PAY_ATTR_EX_TYPE: { 
					//<D> (replace Dollar Amount with ''), (replace Copay with 'No Charge'), [(replace ' before deductible' with ''),], (replace 'per stay'/'per day' with ''),  
					standardText = coPayAttr; 
					if(standardText != null) {
						standardText = standardText.replace(DOLLAR_AMOUNT_PREFIX, StringUtils.EMPTY);
						if(coInsAttr != null) {
							if(coInsAttr.equalsIgnoreCase(COINS_AFTER_DEDUCTIBLE)) {
								//standardText = standardText.replace(BEFORE_DEDUCTIBLE_SUFFIX, StringUtils.EMPTY);
								if(NO_CHARGE_AFTER_DEDUCTIBLE.equalsIgnoreCase(coPayAttr) || COPAY_AFTER_DEDUCTIBLE.equalsIgnoreCase(coPayAttr) ) {
									standardText = NO_CHARGE_AFTER_DEDUCTIBLE;
								} else {
									standardText = coInsVal + SerffConstants.PERCENTAGE + SPACE + coInsAttr;
								}
							} else {
								standardText = standardText.replace(PER_STAY_SUFFIX, StringUtils.EMPTY);
								standardText = standardText.replace(PER_DAY_SUFFIX, StringUtils.EMPTY);
								if(coInsVal == null && ZERO_STRING.equals(coPayVal) && COPAY_AFTER_DEDUCTIBLE.equalsIgnoreCase(coPayAttr)) {
									standardText = standardText.replace(COPAY, SerffConstants.DOLLARSIGN + coPayVal + SPACE + COPAY);
								} else {
									standardText = standardText.replace(COPAY, NO_CHARGE_TEXT);
								}
							}
						}
					}
					break;
				}
				case CO_PAY_VAL_ATTR_EX_TYPE: { 
					//<C> <D> (replace Dollar Amount with '')
					standardText = SerffConstants.DOLLARSIGN + coPayVal + SPACE + coPayAttr; 
					standardText = standardText.replace(DOLLAR_AMOUNT_PREFIX, StringUtils.EMPTY);
					if(coInsAttr != null && coInsAttr.equalsIgnoreCase(COINS_AFTER_DEDUCTIBLE)) {
						LOGGER.debug("Coins not null and CoIns is COINS_AFTER_DEDUCTIBLE");
						if(coPayAttr.equalsIgnoreCase(COPAY_BEFORE_DEDUCTIBLE)) {
							standardText = standardText + LINE_BREAK + NO_CHARGE_AFTER_DEDUCTIBLE;
						}
					}
					break;
				}
				case CO_PAY_VAL_ATTR_CO_INS_VAL_ATTR_TYPE: { 
					//<C> <D> (replace Dollar Amount with '')<A><B>
					standardText = SerffConstants.DOLLARSIGN + coPayVal + SPACE + coPayAttr; 
					standardText = standardText.replace(DOLLAR_AMOUNT_PREFIX, StringUtils.EMPTY);
					standardText = standardText  + LINE_BREAK + coInsVal + SerffConstants.PERCENTAGE + SPACE + coInsAttr;
					break;
				}
				case NOT_COVERED_CO_PAY_VAL_ATTR_TYPE: { 
					//Not covered <br><C> <D> (replace Dollar Amount with '')
					standardText = SerffConstants.DOLLARSIGN + coPayVal + SPACE + coPayAttr; 
					standardText = standardText.replace(DOLLAR_AMOUNT_PREFIX, StringUtils.EMPTY);
					standardText = NOT_COVERED_TEXT + LINE_BREAK + standardText;
					break;
				}
				case CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_TYPE: {
					if(coInsVal != null) {
						standardText = coInsVal + SPACE + coInsAttr ;
					} else {
						standardText = "";
					}
					if(coPayVal != null) {
						if(standardText.length() > 1) {
							standardText += LINE_BREAK;
						}
						standardText += coPayVal + SPACE + coPayAttr;
					}
					break;
				}
				default :  LOGGER.info("No case matched to create standard Text :");
			}		
			LOGGER.debug("Benefit Display value :" + standardText);
			return standardText;
		}
	}
	
	
	enum BenefitDisplayTypeForState {
		NOT_COVERED(NOT_COVERED_TYPE), 
		NO_CHARGE(NO_CHARGE_TYPE),
		NO_CHARGE_EX(NO_CHARGE_EX_TYPE),
		COPAY_ONLY(COPAY_ONLY_TYPE),
		COINS_ONLY(COINS_ONLY_TYPE),
		COINS_COPAY_NO_CHARGE_AFTER_DEDT(COINS_COPAY_NO_CHARGE_AFTER_DEDT_TYPE),
		COPAY_COINS_NO_CHARGE_AFTER_DEDT(COPAY_COINS_NO_CHARGE_AFTER_DEDT_TYPE),
		COPAY_COINS(COPAY_COINS_TYPE);
		
		private int dispType;

		private BenefitDisplayTypeForState(int dispType) {
			this.dispType = dispType;
		}

		public String getDispTileText(String coInsVal, String coInsAttr, String coPayVal, String coPayAttr) {
			StringBuffer tileText = new StringBuffer();
			LOGGER.debug("Tile Display coInsVal:" + coInsVal + " coInsAttr:" + coInsAttr + " coPayVal:" + coPayVal + " coPayAttr:" + coPayAttr + " dispType:" + dispType);
			switch(dispType) {
				case NOT_COVERED_TYPE: {
					tileText.append(StringUtils.EMPTY);
					break;
				}
				case NO_CHARGE_TYPE: 
				case NO_CHARGE_EX_TYPE: {
					tileText.append(NO_CHARGE_TEXT);
					break;
				}
				case COPAY_COINS_TYPE: { 
					tileText.append(coInsVal);
					tileText.append(SerffConstants.PERCENTAGE);
					tileText.append(SPACE_SLASH_SPACE);
					tileText.append(SerffConstants.DOLLARSIGN );
					tileText.append(coPayVal); 
					break;
				}
				case COPAY_COINS_NO_CHARGE_AFTER_DEDT_TYPE: 
				case COPAY_ONLY_TYPE: { 
					tileText.append(SerffConstants.DOLLARSIGN );
					tileText.append(coPayVal); 
					break;
				}
				case COINS_COPAY_NO_CHARGE_AFTER_DEDT_TYPE:
				case COINS_ONLY_TYPE: { 
					tileText.append(coInsVal);
					tileText.append(SerffConstants.PERCENTAGE); 
					break;
				}
				default : LOGGER.info("No case matched to create tile text:");
			}		
			LOGGER.debug("Tile Display value:" + tileText.toString());
			return tileText.toString();
		}
	
		public String getDisplayText(String coInsVal, String coInsAttr, String coPayVal, String coPayAttr) {
			String standardText = null;
			int switchValue = dispType;
			LOGGER.debug("Benefit Display coInsVal:" + coInsVal + " coInsAttr:" + coInsAttr + " coPayVal:" + coPayVal + " coPayAttr:" + coPayAttr + " dispType:" + dispType + " switch:" + switchValue);
			switch(switchValue) {
				case NOT_COVERED_TYPE: {
					standardText = StringUtils.EMPTY;
					break;
				}
				case NO_CHARGE_TYPE: { 
					standardText = NO_CHARGE_TEXT;
					break;
				}
				case NO_CHARGE_EX_TYPE: { 
					standardText = NO_CHARGE_AFTER_DEDUCTIBLE; 
					break;
				}
				case COPAY_ONLY_TYPE: { 
					standardText = SerffConstants.DOLLARSIGN + coPayVal + SPACE + coPayAttr; 
					break;
				}
				case COINS_ONLY_TYPE: { 
					standardText = coInsVal + SerffConstants.PERCENTAGE + SPACE + coInsAttr; 
					break;
				}
				case COINS_COPAY_NO_CHARGE_AFTER_DEDT_TYPE: { 
					standardText = coInsVal + SerffConstants.PERCENTAGE + SPACE + coInsAttr  + LINE_BREAK + coPayAttr ; 
					break;
				}
				case COPAY_COINS_NO_CHARGE_AFTER_DEDT_TYPE: { 
					standardText = SerffConstants.DOLLARSIGN + coPayVal + SPACE + coPayAttr; 
					standardText = standardText  + LINE_BREAK + coInsAttr;
					break;
				}
				case COPAY_COINS_TYPE: { 
					standardText = SerffConstants.DOLLARSIGN + coPayVal + SPACE + coPayAttr; 
					standardText = standardText + LINE_BREAK + coInsVal + SerffConstants.PERCENTAGE + SPACE + coInsAttr ;
					break;
				}
				default :  LOGGER.info("No case matched to create standard Text:");
			}		
			LOGGER.debug("Benefit Display value:" + standardText);
			return standardText;
		}
	}

	public SerffUtils() {
	}

	/**
	 * 
	 * @deprecated  A new implementation is present in class com.serff.service.PlanDisplayMapping</br>
	 *              New implementation is more flexible and allows to define rules in DB</br>
	 *              use PlanDisplayMapping.getBenefitDisplayTileText() instead   
	 */
	@Deprecated
	public String getBenefitDisplayTileText(String coInsVal, String coInsAttr, String coPayValue, String coPayAttr, boolean isPHIXProfile) {
		//For now we assume that logic for tile text is same for PHIX and STATE
		BenefitDisplayType dispType = null;
		if(isPHIXProfile) {
			dispType = getBenefitDisplayTypeForPHIX(coInsVal, coInsAttr, coPayValue, coPayAttr, true);
			//return dispType.getDispTileText(coInsVal, coInsAttr, coPayValue, coPayAttr, isPHIXProfile);
		} else {
			dispType = getBenefitDisplayTypeForStateTile(coInsVal, coInsAttr, coPayValue, coPayAttr);
		}
		return dispType.getDispTileText(coInsVal, coInsAttr, coPayValue, coPayAttr, isPHIXProfile);
	}
	
	/**
	 * @param beginPmrycareDedtOrCoinsAfterNumOfCopays
	 * @param beginPmryCareCsAfterNumOfVisits
	 * @param coInsVal
	 * @param coInsAttribute
	 * @param coPayValue
	 * @param coPayAttribute
	 * @param benefitName
	 * @param isTier1
	 * @param isPHIXProfile
	 * @return
	 * 
	 * @deprecated  A new implementation is present in class com.serff.service.PlanDisplayMapping</br>
	 *              New implementation is more flexible and allows to define rules in DB</br>
	 *              use PlanDisplayMapping.getBenefitDisplayText() instead   
	 */
	@Deprecated 
	public String getBenefitDisplayText(Long beginPmrycareDedtOrCoinsAfterNumOfCopays, Long beginPmryCareCsAfterNumOfVisits, String coInsVal, String coInsAttribute, 
			String coPayValue, String coPayAttribute, String benefitName , boolean isTier1, boolean isPHIXProfile) {
		if(isPHIXProfile) {
			return getBenefitDisplayTextForPHIX(beginPmrycareDedtOrCoinsAfterNumOfCopays, beginPmryCareCsAfterNumOfVisits, coInsVal, coInsAttribute, 
					coPayValue, coPayAttribute, benefitName , isTier1);
		} else {
			return getBenefitDisplayTextForSTATE(beginPmrycareDedtOrCoinsAfterNumOfCopays, beginPmryCareCsAfterNumOfVisits, coInsVal, coInsAttribute, 
					coPayValue, coPayAttribute, benefitName , isTier1);
		}
	}
	
	private String getBenefitDisplayTextForSTATE(Long beginPmrycareDedtOrCoinsAfterNumOfCopays, Long beginPmryCareCsAfterNumOfVisits, String coInsVal, String coInsAttribute, 
			String coPayValue, String coPayAttribute, String benefitName , boolean isTier1) {
		String coInsAttr = null, coPayAttr = null;
		BenefitDisplayTypeForState dispType = null;
		if(coInsAttribute != null) {
			coInsAttr = coInsAttribute.trim();
			if(coInsVal != null) {
				boolean found = false;
				for (String coIns : COINSURANCE_ATTR_LIST_FOR_STATE) {
					if(coIns.equalsIgnoreCase(coInsAttr)) {
						if(PERCENT_COINSURANCE.equals(coIns)) {
							//percent co-insurance should be treated as Coinsurance
							coInsAttr = COINSURANCE;
						} else {
							coInsAttr = coIns;
						}
						found = true;
						break;
					}
				}
				
				if(!found) {
					//Set default custom type benefit
					dispType = BenefitDisplayTypeForState.COPAY_COINS;
				}
			}
		} else {
			coInsAttr = "";
		}
		if(coPayAttribute != null) { 
			coPayAttr = coPayAttribute.trim();
			if(dispType != BenefitDisplayTypeForState.COPAY_COINS && coPayValue != null) {
				boolean found = false;
				for (String copay : COPAY_ATTR_LIST_FOR_STATE) {
					if(copay.equalsIgnoreCase(coPayAttr)) {
						coPayAttr = copay;
						found = true;
						break;
					}
				}
				
				if(!found) {
					//Set default custom type benefit
					dispType = BenefitDisplayTypeForState.COPAY_COINS;
				} else {
					coPayAttr = coPayAttr.replace(DOLLAR_AMOUNT_PREFIX, StringUtils.EMPTY);
				}
			}
		} else {
			coPayAttr = "";
		}

		//Get how to display benefit for STATE
		if(dispType == null) {
			dispType = getBenefitDisplayTypeForState(coInsVal, coInsAttr, coPayValue, coPayAttr);
		}
		
		String stdDisplayValue = dispType.getDisplayText(coInsVal, coInsAttr, coPayValue, coPayAttr);
		StringBuffer value = new StringBuffer();
		if(PRIMARY_CARE_BENEFIT.equalsIgnoreCase(benefitName) && isTier1) {
			if (beginPmrycareDedtOrCoinsAfterNumOfCopays > 0  && StringUtils.isNotBlank(stdDisplayValue)) {
				value.append(FIRST_PREFIX);
				value.append(beginPmrycareDedtOrCoinsAfterNumOfCopays);
				value.append(COPAYS_AT);
				if(coPayAttr != null && coInsAttr != null) {
					if(coPayAttr.toLowerCase().startsWith(NO_CHARGE_TEXT.toLowerCase()) && dispType != BenefitDisplayTypeForState.NO_CHARGE) {
						value.append(NO_CHARGE_TEXT + PRIMARY_CARE_LINE_BREAK);
					} else if(coPayAttr.equalsIgnoreCase(COPAY_AFTER_DEDUCTIBLE)){
						value.append(SerffConstants.DOLLARSIGN + coPayValue + SPACE + COPAY + SPACE + PRIMARY_CARE_LINE_BREAK);
					}
				} else {
					value.append(VISIT_AT_NO_CHARGE);
					value.append(PRIMARY_CARE_LINE_BREAK);
				}
			} else if (beginPmryCareCsAfterNumOfVisits > 0 && beginPmrycareDedtOrCoinsAfterNumOfCopays <= 0) {
				value.append(FIRST_PREFIX);
				value.append(beginPmryCareCsAfterNumOfVisits);
				value.append(VISIT_AT_NO_CHARGE);
				if(StringUtils.isNotBlank(stdDisplayValue)) {
					value.append(PRIMARY_CARE_LINE_BREAK);
				}
			}
		}
		value.append(stdDisplayValue); 
		return value.toString();
	}
	
	
	private String getBenefitDisplayTextForPHIX(Long beginPmrycareDedtOrCoinsAfterNumOfCopays, Long beginPmryCareCsAfterNumOfVisits, String coInsVal, String coInsAttribute, 
			String coPayValue, String coPayAttribute, String benefitName , boolean isTier1) {
		String coInsAttr = null, coPayAttr = null;
		BenefitDisplayType dispType = null;
		if(coInsAttribute != null) {
			coInsAttr = coInsAttribute.trim();
			if(coInsVal != null) {
				boolean found = false;
				for (String coIns : COINSURANCE_ATTR_LIST) {
					if(coIns.equalsIgnoreCase(coInsAttr)) {
						if(PERCENT_COINSURANCE.equals(coIns)) {
							//percent co-insurance should be treated as Coinsurance
							coInsAttr = COINSURANCE;
						} else {
							coInsAttr = coIns;
						}
						found = true;
						break;
					}
				}
				
				if(!found) {
					dispType = BenefitDisplayType.CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR;
				}
			}
		} else {
			coInsAttr = "";
		}
		if(coPayAttribute != null) { 
			coPayAttr = coPayAttribute.trim();
			if(dispType != BenefitDisplayType.CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR && coPayValue != null) {
				boolean found = false;
				for (String copay : COPAY_ATTR_LIST) {
					if(copay.equalsIgnoreCase(coPayAttr)) {
						coPayAttr = copay;
						found = true;
						break;
					}
				}
				
				if(!found) {
					dispType = BenefitDisplayType.CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR;
				}
			}
		} else {
			coPayAttr = "";
		}
		if(dispType == null) {
			dispType = getBenefitDisplayTypeForPHIX(coInsVal, coInsAttr, coPayValue, coPayAttr, false);
		}
		String stdDisplayValue = dispType.getDisplayText(coInsVal, coInsAttr, coPayValue, coPayAttr);
		StringBuffer value = new StringBuffer();
		boolean appendAD = false;
		if(PRIMARY_CARE_BENEFIT.equalsIgnoreCase(benefitName) && isTier1) {
			if(beginPmrycareDedtOrCoinsAfterNumOfCopays > 0 && beginPmryCareCsAfterNumOfVisits > 0) {
				value.append(FIRST_PREFIX);
				value.append(beginPmryCareCsAfterNumOfVisits);
				value.append(VISIT_AT_NO_CHARGE);
				value.append(PRIMARY_CARE_LINE_BREAK);
				//String coPayAmt = (coPayValue == null) ? NOT_APPLICABLE : SerffConstants.DOLLARSIGN + coPayValue;
				value.append(FIRST_PREFIX);
				value.append(beginPmrycareDedtOrCoinsAfterNumOfCopays);
				value.append(COPAYS_AT);
				value.append(stdDisplayValue);
				value.append(PRIMARY_CARE_LINE_BREAK);
				value.append(stdDisplayValue); 
			} else if (beginPmrycareDedtOrCoinsAfterNumOfCopays > 0 ) {
				if(!stdDisplayValue.equalsIgnoreCase(NOT_COVERED_TEXT)) {
					boolean bIfUseGenericRule = true;
					value.append(FIRST_PREFIX);
					value.append(beginPmrycareDedtOrCoinsAfterNumOfCopays);
					value.append(COPAYS_AT);
					if(NO_CHARGE_AFTER_DEDUCTIBLE.equalsIgnoreCase(coInsAttr)) {
						if(coPayValue == null) {
							value.append(NOT_APPLICABLE_TEXT);
						} else {
							if(ZERO_STRING.equals(coPayValue)) {
								value.append(NO_CHARGE_TEXT);
							} else {
								value.append(stdDisplayValue);
							}
						}
						value.append(PRIMARY_CARE_LINE_BREAK_1);
						value.append(NO_CHARGE_AFTER_DEDUCTIBLE);
						bIfUseGenericRule = false;
					} else if(COINS_AFTER_DEDUCTIBLE.equalsIgnoreCase(coInsAttr)) {
						if(ZERO_STRING.equals(coPayValue)) {
							value.append(NO_CHARGE_TEXT);
							value.append(PRIMARY_CARE_LINE_BREAK_1);
							value.append(stdDisplayValue);
							bIfUseGenericRule = false;
						} else if(coPayValue != null && coPayAttr != null){
							String copayStrVal = SerffConstants.DOLLARSIGN + coPayValue + SPACE + coPayAttr.replace(DOLLAR_AMOUNT_COPAY, StringUtils.EMPTY); 
							value.append(copayStrVal.trim());
							value.append(PRIMARY_CARE_LINE_BREAK_1);
							value.append(coInsVal + SerffConstants.PERCENTAGE + SPACE + coInsAttr);
							bIfUseGenericRule = false;
						}
					} else if(COINSURANCE.equalsIgnoreCase(coInsAttr) && coInsVal != null) {
						Double coInsDoubleVal = getNumericValue(coInsVal);
						if(coInsDoubleVal != 0 && !coInsDoubleVal.equals(HUNDRED)) {
							if(coPayValue == null || ZERO_STRING.equals(coPayValue)) {
								if(coPayValue == null) {
									value.append(NOT_APPLICABLE_TEXT);
								} else {
									value.append(NO_CHARGE_TEXT);
								}
								value.append(PRIMARY_CARE_LINE_BREAK_1);
								value.append(stdDisplayValue);
							} else {
								String copayStrVal = SerffConstants.DOLLARSIGN + coPayValue + SPACE + coPayAttr.replace(DOLLAR_AMOUNT_COPAY, StringUtils.EMPTY); 
								value.append(copayStrVal.trim());
								value.append(PRIMARY_CARE_LINE_BREAK_1);
								value.append(coInsVal + SerffConstants.PERCENTAGE + SPACE + coInsAttr);
							}
							bIfUseGenericRule = false;
						}
					} else if(COINS_BEFORE_DEDUCTIBLE.equalsIgnoreCase(coInsAttr) && coInsVal != null) {
						Double coInsDoubleVal = getNumericValue(coInsVal);
						if(coInsDoubleVal == 0) {
							if(coPayAttr == null || coPayValue == null ) {
								value.append(NOT_APPLICABLE_TEXT);
								value.append(PRIMARY_CARE_LINE_BREAK_1);
								value.append(stdDisplayValue);
								bIfUseGenericRule = false;
							} else if(ZERO_STRING.equals(coPayValue) && (NO_CHARGE_TEXT.equalsIgnoreCase(coPayAttr) || DOLLAR_AMOUNT_COPAY.equalsIgnoreCase(coPayAttr))) {
								value.append(NO_CHARGE_TEXT);
								value.append(PRIMARY_CARE_LINE_BREAK_1);
								value.append(stdDisplayValue);
								bIfUseGenericRule = false;
							}
						}
					} 
					if(bIfUseGenericRule) {
						value.append(stdDisplayValue);
						value.append(PRIMARY_CARE_LINE_BREAK_1);
						value.append(stdDisplayValue); 
						appendAD = true;
					}
				} else {
					value.append(stdDisplayValue); 
				}
			} else if (beginPmryCareCsAfterNumOfVisits > 0 ) {
				value.append(FIRST_PREFIX);
				value.append(beginPmryCareCsAfterNumOfVisits);
				value.append(VISIT_AT_NO_CHARGE);
				value.append(PRIMARY_CARE_LINE_BREAK_1);
				value.append(stdDisplayValue); 
				if(!NO_CHARGE_AFTER_DEDUCTIBLE.equalsIgnoreCase(coInsAttr)) {
					appendAD = true;
				}
				
				if(COINS_BEFORE_DEDUCTIBLE.equalsIgnoreCase(coInsAttr) && coInsVal != null) {
					Double coInsDoubleVal = getNumericValue(coInsVal);
					if(coInsDoubleVal == 0) {
						if(coPayAttr == null || coPayValue == null ) {
							appendAD = false;
						} else if(ZERO_STRING.equals(coPayValue) && (NO_CHARGE_TEXT.equalsIgnoreCase(coPayAttr) || DOLLAR_AMOUNT_COPAY.equalsIgnoreCase(coPayAttr))) {
							appendAD = false;
						}
					}
				} else if(COINSURANCE.equalsIgnoreCase(coInsAttr) && coInsVal != null) {
					Double coInsDoubleVal = getNumericValue(coInsVal);
					if(coInsDoubleVal.equals(HUNDRED)) {
						appendAD = false;
					}
				}
			} else {
				value.append(stdDisplayValue); 
			}
		} else {
			value.append(stdDisplayValue); 
		}
		if(appendAD && !stdDisplayValue.endsWith(DEDUCTIBLE_TEXT) && !stdDisplayValue.endsWith(COINSURANCE)) {
			value.append(AFTER_DEDUCTIBLE_SUFFIX); 
		}
		return value.toString();
	}
	
	private boolean isNoChargeAfterDeductible(boolean coPayIsZero, String coInsAttr) {
		return (coPayIsZero && coInsAttr != null && coInsAttr.equalsIgnoreCase(NO_CHARGE_AFTER_DEDUCTIBLE));
	}

	private boolean isCoinsBeforeCopay(String coPayAttr, String coInsAttr) {
		return (coInsAttr.equalsIgnoreCase(COINSURANCE) && coPayAttr.equalsIgnoreCase(COPAY_AFTER_DEDUCTIBLE)) 
		|| (coInsAttr.equalsIgnoreCase(COINS_BEFORE_DEDUCTIBLE) && !coPayAttr.equalsIgnoreCase(COPAY_BEFORE_DEDUCTIBLE));
	}
	
	private boolean isCoinsAfterCopay(String coPayAttr, String coInsAttr) {
		boolean isCoinsAndCopayNotAfter = (coInsAttr.equalsIgnoreCase(COINSURANCE) || coInsAttr.equalsIgnoreCase(PERCENT_COINSURANCE) ) && !coPayAttr.equalsIgnoreCase(COPAY_AFTER_DEDUCTIBLE);
		boolean isCoinsAndCopayBeforeDedt = coInsAttr.equalsIgnoreCase(COINS_BEFORE_DEDUCTIBLE) && coPayAttr.equalsIgnoreCase(COPAY_BEFORE_DEDUCTIBLE);
		return coInsAttr.equalsIgnoreCase(COINS_AFTER_DEDUCTIBLE)  
				|| isCoinsAndCopayNotAfter
				|| isCoinsAndCopayBeforeDedt;
	}

	private BenefitDisplayTypeForState getBenefitDisplayTypeForState(String coInsVal, String coInsAttr, String coPayVal, String coPayAttr) {
		BenefitDisplayTypeForState returnValue = null;
		Double coPayDoubleVal = getNumericValue(coPayVal);
		Double coInsDoubleVal = getNumericValue(coInsVal);
		boolean bothCopayCoinsNull = (coInsDoubleVal == null && coPayDoubleVal == null);
		
		if(!bothCopayCoinsNull) {
			boolean copayIsNoCharge = (coPayAttr != null && coPayAttr.equalsIgnoreCase(NO_CHARGE_TEXT));
			boolean coInsIsNoCharge = (coInsAttr != null && coInsAttr.equalsIgnoreCase(NO_CHARGE_TEXT));
			boolean copayIsNoChargeAfterDedt = (coPayAttr != null && coPayAttr.equalsIgnoreCase(NO_CHARGE_AFTER_DEDUCTIBLE));
			boolean coInsIsNoChargeAfterDedt = (coInsAttr != null && coInsAttr.equalsIgnoreCase(NO_CHARGE_AFTER_DEDUCTIBLE));
			boolean coInsStartsWithNoCharge =  coInsIsNoCharge || coInsIsNoChargeAfterDedt;
			boolean copayStartsWithNoCharge =  copayIsNoCharge || copayIsNoChargeAfterDedt;
			
			if(copayIsNoCharge && coInsIsNoCharge) {
				returnValue = BenefitDisplayTypeForState.NO_CHARGE;
			} else if (coInsStartsWithNoCharge && copayStartsWithNoCharge) {
				returnValue = BenefitDisplayTypeForState.NO_CHARGE_EX;
			} else if (coInsIsNoCharge && !copayStartsWithNoCharge) {
				returnValue = BenefitDisplayTypeForState.COPAY_ONLY;
			} else if (copayIsNoCharge && !coInsStartsWithNoCharge) {
				returnValue = BenefitDisplayTypeForState.COINS_ONLY;
			} else if (copayIsNoChargeAfterDedt && !coInsStartsWithNoCharge) {
				returnValue = BenefitDisplayTypeForState.COINS_COPAY_NO_CHARGE_AFTER_DEDT;
			} else if (coInsIsNoChargeAfterDedt && !copayStartsWithNoCharge) {
				returnValue = BenefitDisplayTypeForState.COPAY_COINS_NO_CHARGE_AFTER_DEDT;
			} else {
				returnValue = BenefitDisplayTypeForState.COPAY_COINS;
			}			
		} else {
			returnValue = BenefitDisplayTypeForState.NOT_COVERED;
		}
		
		return returnValue;
	}
	
	private BenefitDisplayType getBenefitDisplayTypeForPHIX(String coInsVal, String coInsAttr, String coPayVal, String coPayAttr, boolean isTile) {
		BenefitDisplayType returnValue = null;
		Double coPayDoubleVal = getNumericValue(coPayVal);
		Double coInsDoubleVal = getNumericValue(coInsVal);
		boolean bothCopayCoinsNull = (coInsDoubleVal == null && coPayDoubleVal == null);
		boolean copayIsNonZero = (coPayDoubleVal != null && coPayDoubleVal.doubleValue() > 0);
		boolean copayIsZero = (coPayDoubleVal != null && coPayDoubleVal.equals(0.0));
		boolean coInsIsZero = (coInsDoubleVal != null && coInsDoubleVal.equals(0.0));
		boolean coInsIsNonZero = (coInsDoubleVal != null && coInsDoubleVal.doubleValue() > 0);
		boolean coinsIsHundredPercent = (coInsDoubleVal != null && coInsDoubleVal.equals(HUNDRED));
		if(bothCopayCoinsNull || coinsIsHundredPercent) {
			returnValue = BenefitDisplayType.NOT_COVERED;
		} else if(coinsIsHundredPercent && copayIsNonZero) {
			returnValue = BenefitDisplayType.NOT_COVERED_CO_PAY_VAL_ATTR;
		} else if (coInsIsZero && (coPayDoubleVal == null || isNoChargeAfterDeductible(copayIsZero, coInsAttr))) {
			returnValue = BenefitDisplayType.CO_INS_ATTR_EX;
		} else if (coInsIsNonZero && coInsDoubleVal.compareTo(HUNDRED) < 0 && !copayIsNonZero) {
			returnValue = BenefitDisplayType.CO_INS_VAL_ATTR;
		} else if ((coInsDoubleVal == null || coInsDoubleVal.equals(0.0)) && copayIsNonZero) {
			if(!isTile && COINS_AFTER_DEDUCTIBLE.equalsIgnoreCase(coInsAttr) && !COPAY_AFTER_DEDUCTIBLE.equalsIgnoreCase(coPayAttr)) {
				returnValue = BenefitDisplayType.CO_PAY_VAL_ATTR_CO_INS_VAL_ATTR;
			} else {
				returnValue = BenefitDisplayType.CO_PAY_VAL_ATTR_EX;
			}
		} else if (coInsIsNonZero && copayIsNonZero 
				&& isCoinsBeforeCopay(coPayAttr, coInsAttr)) {
			returnValue = BenefitDisplayType.CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_EX;
		} else if (coInsIsNonZero && copayIsNonZero && isCoinsAfterCopay(coPayAttr, coInsAttr)){
			returnValue = BenefitDisplayType.CO_PAY_VAL_ATTR_CO_INS_VAL_ATTR;
		} else if (coInsIsZero && copayIsZero 
				&& !coInsAttr.equalsIgnoreCase(NO_CHARGE_AFTER_DEDUCTIBLE)) {
			if(isTile) {
				if(COINS_AFTER_DEDUCTIBLE.equalsIgnoreCase(coInsAttr) && coPayAttr != null && (coPayAttr.contains(DEDUCTIBLE_TEXT) || NO_CHARGE_TEXT.equalsIgnoreCase(coPayAttr))) {
					returnValue = BenefitDisplayType.CO_INS_ATTR_EX;
				} else if(COINS_BEFORE_DEDUCTIBLE.equalsIgnoreCase(coInsAttr) && coPayAttr != null && (DOLLAR_AMOUNT_COPAY.contains(coPayAttr) || NO_CHARGE_TEXT.equalsIgnoreCase(coPayAttr))) {
					returnValue = BenefitDisplayType.CO_INS_ATTR_EX;
				} 
			} 
			if(returnValue == null) {
				returnValue = BenefitDisplayType.CO_PAY_ATTR_EX;
			}
		}  else if (coInsDoubleVal == null && copayIsZero) {
			returnValue = BenefitDisplayType.CO_PAY_ATTR_EX;
		} else {
			returnValue = BenefitDisplayType.CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_EX;
		}
		return returnValue;
	}
	
	private BenefitDisplayType getBenefitDisplayTypeForStateTile(String coInsVal, String coInsAttr, String coPayVal, String coPayAttr) {
		BenefitDisplayType returnValue = null;
		Double coPayDoubleVal = getNumericValue(coPayVal);
		Double coInsDoubleVal = getNumericValue(coInsVal);
		boolean bothCopayCoinsNull = (coInsDoubleVal == null && coPayDoubleVal == null);
		boolean copayIsNonZero = (coPayDoubleVal != null && coPayDoubleVal.doubleValue() > 0);
		boolean copayIsZero = (coPayDoubleVal != null && coPayDoubleVal.equals(0.0));
		boolean coInsIsZero = (coInsDoubleVal != null && coInsDoubleVal.equals(0.0));
		boolean coInsIsNonZero = (coInsDoubleVal != null && coInsDoubleVal.doubleValue() > 0);
		boolean coinsIsHundredPercent = (coInsDoubleVal != null && coInsDoubleVal.equals(HUNDRED));
		if(bothCopayCoinsNull || (coinsIsHundredPercent && !copayIsNonZero)) {
			returnValue = BenefitDisplayType.NOT_COVERED;
		} else if(coinsIsHundredPercent && copayIsNonZero) {
			returnValue = BenefitDisplayType.NOT_COVERED_CO_PAY_VAL_ATTR;
		} else if (coInsIsZero && (coPayDoubleVal == null || isNoChargeAfterDeductible(copayIsZero, coInsAttr))) {
			returnValue = BenefitDisplayType.CO_INS_ATTR_EX;
		} else if (coInsIsNonZero && coInsDoubleVal.compareTo(HUNDRED) < 0 && !copayIsNonZero) {
			returnValue = BenefitDisplayType.CO_INS_VAL_ATTR;
		} else if ((coInsDoubleVal == null || coInsDoubleVal.equals(0.0)) && copayIsNonZero) {
			returnValue = BenefitDisplayType.CO_PAY_VAL_ATTR_EX;
		} else if (coInsIsNonZero && copayIsNonZero 
				&& isCoinsBeforeCopay(coPayAttr, coInsAttr)) {
			returnValue = BenefitDisplayType.CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_EX;
		} else if (coInsIsNonZero && copayIsNonZero && isCoinsAfterCopay(coPayAttr, coInsAttr)){
			returnValue = BenefitDisplayType.CO_PAY_VAL_ATTR_CO_INS_VAL_ATTR;
		} else if (coInsIsZero && copayIsZero 
				&& !coInsAttr.equalsIgnoreCase(NO_CHARGE_AFTER_DEDUCTIBLE)) {
			returnValue = BenefitDisplayType.CO_PAY_ATTR_EX;
		}  else if (coInsDoubleVal == null && copayIsZero) {
			returnValue = BenefitDisplayType.CO_PAY_ATTR_EX;
		} else {
			returnValue = BenefitDisplayType.CO_INS_VAL_ATTR_CO_PAY_VAL_ATTR_EX;
		}
		return returnValue;
	}

	public String getLimitAndExclusionText(String limit, String limitAttr, String exclusion) {
		StringBuffer limitAndExclusionText = null;
		
		if(limit != null || limitAttr != null || exclusion != null) {
			limitAndExclusionText = new StringBuffer();
			
			if(limit == null && limitAttr == null) {
				limitAndExclusionText.append(exclusion);
			} else {
				if(limit != null) {
					limitAndExclusionText.append(limit);
					limitAndExclusionText.append(" ");
				}
				if(limitAttr != null) {
					limitAndExclusionText.append(limitAttr);
				}
				if(exclusion != null) {
					limitAndExclusionText.append(LINE_BREAK);
					limitAndExclusionText.append(exclusion);
				}			
			}
			return limitAndExclusionText.toString();
		} 

		return null;
	}

	private Double getNumericValue(String value) {
		if(value != null) {
			String val = value.trim().replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY).replace(SerffConstants.PERCENTAGE, StringUtils.EMPTY);
			return Double.valueOf(val);
		}
		return null;
	}
	
	/**
	 * This method validates the plan object for transfer plan operation
	 * @author Nikhil Talreja
	 * @since 08 March, 2013
	 *  
	 * @param plan
	 *        Plan instance.
	 *        
	 * @param request
	 *        TransferPlan instance.
	 *        
	 * @param response
	 * 		  TransferPlanResponse instance
	 * 
	 * @param errorCodes
	 * 		  Error codes list in String format.
	 *  	
	 * @param errorMessages
	 *        Error messages list in String format.
	 *        
	 * @return TransferPlanResponse instance.
	 */
	public TransferPlanResponse validatePlan(boolean isPHIXProfile, Plan plan, TransferPlan request, TransferPlanResponse response, List<String> errorCodes, List<String> errorMessages){

		LOGGER.info("validatePlan() : Validating plan for transfer plan operation");

		if(request == null || request.getPlan() == null){
			errorCodes.add(SerffConstants.NO_OBJECT_FOUND_CODE);
			errorMessages.add(SerffConstants.NO_OBJECT_FOUND_MESSAGE + ": Plan");
			generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
			return response;
		}

		//Validating plan Id
		LOGGER.debug("validatePlan() : Validating Standard Component Id : " + plan.getStandardComponentId());
		SerffValidator serffValidator = new SerffValidator();
		serffValidator.validateStandardComponentId(plan.getStandardComponentId(), getGlobalStateCode(), errorCodes, errorMessages);

		if(!errorCodes.isEmpty()) {
			generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
			if(isPHIXProfile && response instanceof TransferPlanResponseEx) {
				LOGGER.debug("validatePlan() : Returning response : RequestId : " + ((TransferPlanResponseEx) response).getRequestId());
			}
			return response;
		}
		/*String planId = plan.getStandardComponentId();
		TransferPlanResponse returnResponse = validatePlanId(planId, response,  plan, errorMessages, errorMessages);
		if(returnResponse != null){
			return returnResponse;
		}*/
		LOGGER.debug("validatePlan() : Returning null.");
		return null;
	}
	
	/**
	 * This method creates a record in SERFF_PLAN_MGMT table for any operation
	 * Only the generic fields are created in this method. Operation specific fields are updated in other method
	 * @author Nikhil Talreja
	 * @since 08 March, 2013
	 * 
	 * @param operation Operation String like validate, update passed by using constants. 
	 * @return SerffPlanMgmt instance.
	 * @throws UnknownHostException
	 */
	public SerffPlanMgmt createSerffPlanMgmtRecord(String operation) throws UnknownHostException {
		return createSerffPlanMgmtRecord(operation, SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.B);
	}
	
	/**
	 * @param operation
	 * @param requestStatus
	 * @param requestState
	 * @return
	 * @throws UnknownHostException
	 */
	public SerffPlanMgmt createSerffPlanMgmtRecord(String operation, SerffPlanMgmt.REQUEST_STATUS requestStatus,
			SerffPlanMgmt.REQUEST_STATE requestState) throws UnknownHostException {

		LOGGER.info("createSerffPlanMgmtRecord() : Creating record in SERFF_PLAN_MGMT table: operation : " + operation);

		SerffPlanMgmt record = new SerffPlanMgmt();
		String serverList = SerffConstants.getServiceServerList();
		String[] servers = null;
		
		if (null != serverList) {
			servers = serverList.split(SerffConstants.COMMA);
			LOGGER.debug("createSerffPlanMgmtRecord() : Servers List Size : " + servers.length);
		}
		
		record.setCallerType(SerffPlanMgmt.CALLER_TYPE.O);
		record.setCallerIp(SerffConstants.LOCALHOST_IP);
		
		if (null != servers && servers.length > 0 && null != servers[0]) {
			
			LOGGER.debug("server: " + servers[0]);
			String server = servers[0].substring(servers[0].indexOf(SerffConstants.AT_THE_RATE) + 1);
			
			if (server.length() > SerffConstants.PROCESS_IP_LENGTH) {
				LOGGER.info("Process Ip: " + server);
				record.setProcessIp(server.substring(0, SerffConstants.PROCESS_IP_LENGTH - 1));
			}
			else {
				LOGGER.debug("ProcessIp: " + server);
				record.setProcessIp(server);
			}
		}
		record.setOperation(operation);
		record.setEnv(SerffPlanMgmt.ENV.DEV);
		record.setStartTime(new Date());
		record.setRequestStatus(requestStatus);
		
		InetAddress processIp = InetAddress.getLocalHost();
		
		if (null != processIp) {
			LOGGER.debug("createSerffPlanMgmtRecord()  : " + processIp.getHostAddress());
			record.setCreatedBy(processIp.getHostAddress());
		}

		//If request came from UI, set created by as user's id
		if(SerffPlanMgmt.CALLER_TYPE.U.equals(record.getCallerType())){
			record.setCreatedBy("USER ID");
		}

		record.setRemarks(SerffConstants.PLANS_LOADING_MESSAGE);
		record.setRequestState(requestState);
		record.setRequestStateDesc("New request recieved");

		return record;
	}

	/**
	 * Method is used to Create new record for SERFF_PLAN_MGMT_BATCH table.
	 * @param request HttpServletRequest Object
	 * @return new Saved SerffPlanMgmtBatch Object
	 */
	public SerffPlanMgmtBatch createBatchRecord(String issuerId, String carrier, String stateCode, String exchangeType,
			String tenants, String operation, int userId) {
		
		LOGGER.info("createBatchRecord Begin");
		SerffPlanMgmtBatch serffPlanMgmtBatch = null;
		
		try {
			serffPlanMgmtBatch = new SerffPlanMgmtBatch();
			serffPlanMgmtBatch.setIsDeleted(SerffPlanMgmtBatch.IS_DELETED.N);
			serffPlanMgmtBatch.setFtpStartTime(new Date());
			serffPlanMgmtBatch.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.IN_PROGRESS);
			serffPlanMgmtBatch.setIssuerId(issuerId);
			serffPlanMgmtBatch.setCareer(carrier);
			serffPlanMgmtBatch.setState(stateCode);
			serffPlanMgmtBatch.setExchangeType(exchangeType);
			serffPlanMgmtBatch.setDefaultTenant(tenants);
			serffPlanMgmtBatch.setOperationType(operation);

			if (0 < userId) {
				serffPlanMgmtBatch.setUserId(userId);
			}
		}
		finally {
			LOGGER.debug("createBatchRecord object End");
		}
		return serffPlanMgmtBatch;
	}
	
	/**
	 * 
	* This method adds Transfer plan specific fields to SERFF_PLAN_MGMT record
	 * @author Nikhil Talreja
	 * @since 08 March, 2013
	 * 
	 * @param record
	 *        SerffPlanMgmt instance that is fetched from table.
	 *        
	 * @param request
	 *        TransferPlan request instance.
	 *        
	 * @param exchangeTypeFilter
	 *        Filter selected by user i.e. ON or OFF
	 * 
	 * @return SerffPlanMgmt instance.
	 * 
	 * @throws GIException 
	 */
	public  SerffPlanMgmt updateRecordForTransferPlan(SerffPlanMgmt record, TransferPlan request, String exchangeTypeFilter) throws GIException{

		LOGGER.info("updateRecordForTransferPlan() : Updating record for transfer plan");

		SerffPlanMgmt newRecord = null;
		newRecord = (SerffPlanMgmt)clone(record);

		TransferPlan newRequest = null;

		Cloner cloner = new Cloner();
		newRequest = cloner.deepClone(request);
		LOGGER.debug("Cloned new record : RequestId : " + newRequest.getRequestId());

		/*
		 * Sending attachment data as null before marshalling
		 * Marshalling binary data was causing request to time out
		 */
		SupportingDocuments supportingDocs =  newRequest.getPlan().getSupportingDocuments();
		TransferDataTemplates templates = newRequest.getPlan()
				.getDataTemplates();
		removeBinaryData(supportingDocs, templates);

		newRecord.setRequestXml(objectToXmlString(newRequest));

		Plan plan = request.getPlan();
		if (null != plan) {
			LOGGER.debug("Plan HiosProductId : " + plan.getHiosProductId());
			
			Insurer insurer = plan.getInsurer();
			if (insurer != null) {
				newRecord.setIssuerId(Integer.valueOf(insurer.getIssuerId())
						.toString());
				if (insurer.getCompanyInfo() != null) {
					newRecord.setIssuerName(insurer.getCompanyInfo()
							.getCompanyName());
				}
			}
			if (templates != null || supportingDocs != null) {
				String attachmentList = createAttachmentList(templates, supportingDocs);
				if(ATTACHMENT_LIST_MAXLEN < attachmentList.length()) {
					attachmentList = attachmentList.substring(0, ATTACHMENT_LIST_MAXLEN);
				}
				newRecord.setAttachmentsList(attachmentList);
			}
			newRecord.setPlanId(plan.getStandardComponentId());
			newRecord.setPlanName(plan.getPlanName());
			newRecord.setHiosProductId(plan.getHiosProductId());
			newRecord.setSerffTrackNum(plan.getSerffTrackingNumber());
			newRecord.setStateTrackNum(plan.getStateTrackingNumber());
			//newRecord.setExchangeType(exchangeType);
		}
		return newRecord;
	}
	
	/**
	 * 
	 * This method saves the request xml to SERFF_PLAN_MGMT record for
	 * validateAndTransformDataTemplate
	 * @author Geetha Chandran
	 * @since 14 March, 2013
	 * 
	 * @param record
	 *        SerffPlanMgmt instance that saved in table.
	 *         
	 * @param request
	 * 		  ValidateAndTransformDataTemplate instance.
	 * 
	 * @return SerffPlanMgmt instance after saving in the database.
	 * 
	 * @throws GIException 
	 */
	public  SerffPlanMgmt updateRecordForValidatePlan(SerffPlanMgmt record, ValidateAndTransformDataTemplate request) throws GIException{
		
		LOGGER.info("updateRecordForValidatePlan() : Updating record for validated plan.");
		
		SerffPlanMgmt newRecord = (SerffPlanMgmt)clone(record);
		ValidateAndTransformDataTemplate newRequest = null;

		Cloner cloner = new Cloner();
		newRequest = cloner.deepClone(request);

		DataTemplates templates = newRequest.getDataTemplates();		
		if(templates != null){
			List<DataTemplate> dataTemplates = templates.getDataTemplate();
			if(!CollectionUtils.isEmpty(dataTemplates)){				
				LOGGER.info("updateRecordForValidatePlan() : dataTemplates size : " + dataTemplates.size());				
				for(DataTemplate template : dataTemplates ){
					template.setRequiredInsurerSuppliedData(null);
				}
			}
		}

		newRecord.setRequestXml(objectToXmlString(newRequest));
		LOGGER.info("updateRecordForValidatePlan() : Updated SERFF_PLAN_MGMT table with request xml for validateAndTransformDataTemplate operation.");

		return newRecord;
	}

	
	/**
	 * 
	 * This method saves the request xml to SERFF_PLAN_MGMT record for
	 * updatePlanStatus and retrievePlanStatus
	 * @author Geetha Chandran 
	 * @since 14 March, 2013
	 * 
	 * @param record
	 * 		  SerffPlanMgmt instance that saved in table.
	 * 
	 * @param request
	 * 		  ValidateAndTransformDataTemplate instance.
	 * 
	 * @return SerffPlanMgmt after updating in the database.
	 * 
	 * @throws GIException 
	 */
	public  SerffPlanMgmt updateClobData(SerffPlanMgmt record, Object request) throws GIException{
		LOGGER.info("Executing updateClobData() : issuer id for record : " + record.getIssuerId());
		SerffPlanMgmt newRecord = (SerffPlanMgmt)clone(record);
		Object newRequest = null;

		Cloner cloner = new Cloner();
		newRequest = cloner.deepClone(request);

		newRecord.setRequestXml(objectToXmlString(newRequest));
		LOGGER.info("updateClobData() : Updated SERFF_PLAN_MGMT table with request xml");

		return newRecord;
	}

	
	/**
	 * 
	 * This method removes the binary data from attachments in Transfer plan request
	 * This is done to create request xml only
	 * @author Nikhil Talreja
	 * @since 14 March, 2013
	 * 
	 * @param supportingDocs
	 * 		  All the supporting docs. 
	 * 
	 * @param templates
	 *        Templates present.
	 */
	public void removeBinaryData(SupportingDocuments supportingDocs, TransferDataTemplates templates){

		LOGGER.info("Executing removeBinaryData() .");
		removeBinaryDataForSupportingDocs(supportingDocs);
		removeBinaryDataForTemplates(templates);
	}

	
	
	
	/**
	 * This is utility method to remove binary data present within Supporting Docs.
	 * 
	 * @author Nikhil Talreja
	 * @since 14 March, 2013
	 * 
	 * @param supportingDocs
	 * 		  All the supporting docs.
	 * 
	 */
	private void removeBinaryDataForSupportingDocs(SupportingDocuments supportingDocs) {
		if(supportingDocs != null){
			LOGGER.info("Executing removeBinaryDataForSupportingDocs().");
			List<SupportingDocument> supportingDoc = supportingDocs.getSupportingDocument();
			LOGGER.debug("removeBinaryDataForSupportingDocs() : supportingDoc : " + supportingDoc);
			if (!CollectionUtils.isEmpty(supportingDoc)){				
				LOGGER.debug("removeBinaryDataForSupportingDocs() : supportingDoc size : " + supportingDoc.size());				
				for (SupportingDocument document : supportingDoc){
					Attachments attachments = document.getAttachments();                    
					List<Attachment> attachment = attachments.getAttachment();
					if(!CollectionUtils.isEmpty(attachment)){
						for (Attachment doc : attachment) {
							doc.setAttachmentData(null);
						}
					}
				}
			}
		}
	}
	
	
	
	/**
	 * This is utility method to remove binary data present within templates.
	 * 
	 * @author Nikhil Talreja
	 * @since 14 March, 2013
	 * 
	 * @param templates
	 *		  TransferDataTemplates present in request.
	 * 
	 */
	private void removeBinaryDataForTemplates(TransferDataTemplates templates) {
		if (templates != null) {
			LOGGER.info("Executing removeBinaryDataForTemplates()");
			List<TransferDataTemplate> dataTemplates = templates.getDataTemplate();
			if (!CollectionUtils.isEmpty(dataTemplates)) {				
				LOGGER.debug("removeBinaryDataForTemplates() : dataTemplates size : " + dataTemplates.size());	
				for (TransferDataTemplate template : dataTemplates) {
					/*template.setInsurerSuppliedData(null);
					template.setExchangeSuppliedData(null);*/

					if (null != template.getInsurerAttachment()) {
						template.getInsurerAttachment().setInsurerSuppliedData(null);
					}

					if (null != template.getExchangeAttachment()) {
						template.getExchangeAttachment().setExchangeSuppliedData(null);
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * This method created record in SERFF_TEMPLATE_STATUS table and adds the templates to Alfresco
	 * @author Nikhil Talreja
	 * @since 14 March, 2013
	 *  
	 * @param templates
	 *        Templates present in request.
	 *        
	 * @param record
	 *        SerffPlanMgmt instance saved in database.
	 *        
	 * @param serffService
	 * 		  Service used for persistance logic. 
	 * 
	 * @param ecmService
	 * 		  Service used for ECM upload purpose.
	 * 
	 * @param supportingDocuments
	 *        Supporting documents to the plan.
	 * 
	 * @return Map of document id and name that is saved in ECM.
	 * 
	 * @throws GIException 
	 */
	public Map <String, String> createSerffTemplateRecords(TransferDataTemplates templates,
			SerffPlanMgmt record, SerffService serffService,
			ContentManagementService ecmService,Map <String, String> supportingDocuments) throws GIException {

		LOGGER.debug("Executing createSerffTemplateRecords().");
		

		if (templates != null) {

			List<TransferDataTemplate> dataTemplates = templates.getDataTemplate();

			if (!CollectionUtils.isEmpty(dataTemplates)) {

				LOGGER.debug("createSerffTemplateRecords() : dataTemplates size : " + dataTemplates.size());
				
				for (TransferDataTemplate template : dataTemplates) {
					//Adding template in ECM
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("createSerffTemplateRecords() : Adding template " + SecurityUtil.sanitizeForLogging(template.getDataTemplateId()) + " in ECM inside folder " + "_DataTemplates_");
					}
					byte[] docBytes = createECMdata(template.getInsurerAttachment().getInsurerSuppliedData());
					SerffDocument attachment = null;
					SerffPlanMgmt planmgmtRecord = serffService.getSerffPlanMgmtById(record.getSerffReqId());
					attachment = addAttachmentInECM(ecmService, planmgmtRecord,
								SerffConstants.SERF_ECM_BASE_PATH,
								"_DataTemplates_",
								template.getDataTemplateType() + "_" + template.getDataTemplateId() + ".xml", docBytes, true);
					//attachment.setSerffTemplateId(templateStatus.getId());
					serffService.saveSerffDocument(attachment);
					supportingDocuments.put(template.getDataTemplateType(), attachment.getEcmDocId());
				}
			}
		}
		
		LOGGER.info("createSerffTemplateRecords() : supportingDocuments : " + supportingDocuments);
		return supportingDocuments;
	}
	
	/**
	 * 
	 * This method creates the attachment list string to be stored in SERFF_PLAN_MGMT record
	 * @author Nikhil Talreja
	 * @since 13 March, 2013
	 *  
	 * @param templates
	 *        Templates present within Plan.
	 *       
	 * @param supportingDocs
	 * 		  Supporting docs present for Plan. 
	 * 
	 * @return String representation of attachments.
	 */
	public  String createAttachmentList(TransferDataTemplates templates, SupportingDocuments supportingDocs){

		LOGGER.info("Executing createAttachmentList().");
		
		StringBuffer sb = new StringBuffer();

		if(templates != null){
			List<TransferDataTemplate> dataTemplates = templates.getDataTemplate();
			LOGGER.debug("createAttachmentList() : dataTemplates sise : " + dataTemplates.size());
			
			if(!CollectionUtils.isEmpty(dataTemplates)){
				LOGGER.debug("createAttachmentList() : dataTemplates size : " + dataTemplates.size());
				for(TransferDataTemplate template : dataTemplates ){
					sb.append(template.getDataTemplateType());
					sb.append(",");
				}
			}
		}

		if(supportingDocs != null){
			List<SupportingDocument> supportingDoc = supportingDocs.getSupportingDocument();

			if (!CollectionUtils.isEmpty(supportingDoc)){
				LOGGER.debug("createAttachmentList() : supportingDoc size : " + supportingDoc.size());
				for (SupportingDocument document : supportingDoc){
					Attachments attachments = document.getAttachments();                
					List<Attachment> attachment = attachments.getAttachment();
					if(!CollectionUtils.isEmpty(attachment)){
						for (Attachment doc : attachment) {
							sb.append(doc.getFileName());
							sb.append(",");
						}
					}
				}
			}
		}

		LOGGER.debug("createAttachmentList() : Attachment list string : " + sb.toString());
		return sb.toString();
	}
		
	/**
	 * 
	 * This method updates error details of an operation in SERFF_PLAN_MGMT record
	 * @author Nikhil Talreja
	 * @since 08 March, 2013
	 * 	
	 * @param record
	 *        SerffPlanMgmt instance saved in database.
	 *        
	 * @param response
	 *        TransferPlanResponse instance.
	 * 
	 * @param remarks
	 * 		  Remarks in String format.
	 * 
	 * @param errorDescription
	 *        String representation of error.
	 *           
	 * @return SerffPlanMgmt instance after saving into database.
	 * @throws GIException 
	 */
	public  SerffPlanMgmt updateErrorDetailsForRecord(SerffPlanMgmt record,TransferPlanResponse response, String remarks, String errorDescription) throws GIException{

		LOGGER.info("updateErrorDetailsForRecord() : Updating record with Error details : remarks : " + remarks);

		SerffPlanMgmt newRecord = null;
		
		newRecord = (SerffPlanMgmt)clone(record);

		newRecord.setEndTime(new Date());
		newRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
		newRecord.setRemarks(remarks);
		newRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
		newRecord.setRequestStateDesc(errorDescription);
		newRecord.setResponseXml(objectToXmlString(response));

		return newRecord;
	}
	
	/**
	 * 
	 * This method updates success details of an operation in SERFF_PLAN_MGMT record
	 * @author Nikhil Talreja	 * 
	 * @since 08 March, 2013
	 * 
	 * @param record
	 *        SerffPlanMgmt instance saved in database.
	 *        
	 * @param response
	 *        TransferPlanResponse instance.
	 * 
	 * @param remarks
	 * 		  Remarks in String format.
	 * 
	 * @return SerffPlanMgmt instance after saving into database.
	 * 
	 * @throws GIException 
	 */
	public  SerffPlanMgmt updateSuccessDetailsForRecord(SerffPlanMgmt record,TransferPlanResponse response, String remarks) throws GIException{

		LOGGER.info("updateSuccessDetailsForRecord() : Updating record with Success details : remarks : " + remarks);

		SerffPlanMgmt newRecord = null;
		newRecord = (SerffPlanMgmt)clone(record);
		newRecord.setEndTime(new Date());
		newRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
		newRecord.setRemarks(remarks);
		newRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
		newRecord.setRequestStateDesc(REQUEST_STATUS);
		newRecord.setResponseXml(objectToXmlString(response));

		return newRecord;
	}
		
	/**
	 * 
	 * This method creates data to be added to ECM
	 * @author Nikhil Talreja
	 * @since 08 March, 2013
	 * 
	 * @param data
	 *        Document data that is in stream.
	 *        
	 * @return Byte stream of the document.
	 * @throws GIException 
	 * @throws NumberFormatException 
	 * 
	 * @throws IOException
	 */
	public byte[] createECMdata(DataHandler data) throws GIException {

		LOGGER.info("Execution of createECMdata() Begin");
		byte bytes[] = null;

		try {
			final InputStream stream = data.getDataSource().getInputStream();
			bytes = IOUtils.toByteArray(stream);
			LOGGER.info("createECMdata() : Recieved " + bytes.length + " bytes for attachment.");

			if (stream != null) {
				stream.close();
			}
		}
		catch (IOException e) {
			LOGGER.error("Exception occurred while creating ECM data", e);
			throw new GIException(Integer.parseInt(SerffConstants.APP_EXCEPTION_CODE),
					SerffConstants.APP_EXCEPTION_MESSAGE_FAILED_TO_PUT_DOCUMENT_TO_ECM_MESSAGE
					+ " : Error while creating data stream for ECM."
					+ e.getMessage(), "");
		}
		finally {
			LOGGER.info("Execution of createECMdata() End");
		}
		return bytes;
	}
	/*public static String getUniqueSystemInfo() {
        try {
            InetAddress thisIPConfig = InetAddress.getLocalHost();

            LOGGER.info("Host Name==>"+thisIPConfig.getHostName());
            LOGGER.info("IP Address==>"+thisIPConfig.getHostAddress());

            return thisIPConfig.getHostName();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "";
    }*/
		
	/**
	 * This method adds attachment to ECM
	 * @author Nikhil Talreja
	 * @throws ContentManagementServiceException
	 * @since 12 March, 2013
	 * 
	 * @param ecmService Autowired ecmService instance which handles operations related to ECM.
	 * @param record SerffPlanMgmt instance saved in database. 		  
	 * @param basePath Path where document is to be saved in ECM.
	 * @param fileName Name of the document.
	 * @param content Byte representation of the document.
	 * @return SerffDocument that is saved in ECM.
	 * @throws  
	 */
	public SerffDocument addAttachmentInECM(
			ContentManagementService ecmService, SerffPlanMgmt record,
			String basePath, String folderName, String fileName, byte[] content, boolean bypassable) throws GIException{

		return serffResourceUtil.addAttachmentInECM(ecmService, record, basePath, folderName, fileName, content, bypassable);
	}

	/*
	 * @param record SerffPlanMgmt instance saved in database. 		  
	 * @param fileName Name of the document.
	 * @param content Byte representation of the document.
	 * @return SerffDocument that is saved in CDN
	 * @throws  
	 */
	public SerffDocument addAttachmentInCDN(SerffPlanMgmt record, String fileName, byte[] content) throws GIException{
		return serffResourceUtil.addAttachmentInCDN(record, fileName, content);
	}

	/**
	 * This method parses Data Templates inside a request.
	 *
	 * @author Nikhil Talreja
	 * @since 08 March, 2013
	 * 
	 * @param templates
	 *        Templates of the plan object.
	 *        
	 * @param response
	 *        TransferPlanResponse instance created after submitting request to webservice.
	 *        
	 * @param templateObjects
	 *        List of templates List in plain Object format.
	 * @throws GIException 
	 */
	@SuppressWarnings(RAW_TYPES)
	public void parseTransferDataTemplates(TransferDataTemplates templates,
			TransferPlanResponse response, List<Object> templateObjects) throws GIException
	{
		Map<String, Class> templateTypeMapping = templateTypeMap;
		String templateType = null;
		Class templateClass = null;

		LOGGER.info("parseTransferDataTemplates() : Parsing templates for the request.");

		String templateVersion = null;
		List<TransferDataTemplate> transDataTempList = templates.getDataTemplate();
		//Parse Plan Benefits template first
		for (TransferDataTemplate dataTemplate : transDataTempList) {
			templateType = dataTemplate.getDataTemplateType();

			if (DATA_BENEFITS.equals(templateType)) {
				templateClass = templateTypeMapping.get(templateType);
				Object planTemplateObject = parseTemplate(dataTemplate, templateClass);
				String version = getTemplateVersion(planTemplateObject);

				if (null != version) {

					if (version.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_9)) {
						templateTypeMapping = templateTypeMap2019;
						templateVersion = PlanValidator.TEMPLATE_VERSION_8; // There are no changes in v9 so v8 would work here
					}
					else if (version.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_8)) {
						templateTypeMapping = templateTypeMap2019;
						templateVersion = PlanValidator.TEMPLATE_VERSION_8;
					}
					else if (version.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_7)) {
						templateTypeMapping = templateTypeMap2018;
						templateVersion = PlanValidator.TEMPLATE_VERSION_7;
					}
					else if (version.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_6)) {
						templateVersion = PlanValidator.TEMPLATE_VERSION_6;
					}
					else if (version.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_5)) {
						templateVersion = PlanValidator.TEMPLATE_VERSION_5;
					}
					else {
						templateVersion = version;
					}
				}

				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("parseTransferDataTemplates() : Transformation successful for Plan Benefits Template,  version: " + version);
				}

				if (null != planTemplateObject) {
					templateObjects.add(planTemplateObject);
				}
				break;
			}
		}

		for (TransferDataTemplate dataTemplate : transDataTempList) {

			templateType = dataTemplate.getDataTemplateType();

			if (DATA_BENEFITS.equals(templateType)) {
				//already processed, so skip
				continue;
			}

			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Transformation for template " + SecurityUtil.sanitizeForLogging(templateType));
			}

			if (StringUtils.isNotEmpty(templateType)) {
				templateClass = templateTypeMapping.get(templateType);
			}
			try {
				Object templateObject = parseTemplate(dataTemplate, templateClass);

				if (null != templateObject) {

					if (PlanValidator.TEMPLATE_VERSION_8.equals(templateVersion)
							&& DATA_SERVICE_AREA.equals(templateType)
							&& templateObject instanceof com.serff.template.plan.BenefitServiceAreaTemplateVO) {
						com.serff.template.plan.BenefitServiceAreaTemplateVO benefitServiceAreaTemplateVO = (com.serff.template.plan.BenefitServiceAreaTemplateVO) templateObject;
						templateObject = convertToPayloadTypeVo(benefitServiceAreaTemplateVO);
					}
					templateObjects.add(templateObject);
				}
				else if (LOGGER.isInfoEnabled()) {
					LOGGER.info("parseTransferDataTemplates() : Transformation successful for template " + SecurityUtil.sanitizeForLogging(templateType));
				}
			}
			catch (Exception e) {
				LOGGER.error("parseTransferDataTemplates() : Error parsing SERFF template TYPE: " + templateType + " The template would be not be processed. Plan load process would continue if all mandatory templates are parsed succesfully.");
			}
		}
	}

	/**
	 * Method is used for Template Adaptor to create Identification Type DTO.
	 * XML Element: <IdentificationID>---</IdentificationID>
	 */
	private com.serff.template.svcarea.niem.core.IdentificationType convertToIdentificationTypeVo(String value,
			com.serff.template.svcarea.niem.core.ObjectFactory coreFactory,
			com.serff.template.svcarea.niem.proxy.xsd.ObjectFactory xsdFactory) {

		com.serff.template.svcarea.niem.core.IdentificationType identificationType = null;

		if (StringUtils.isBlank(value)) {
			return identificationType;
		}
		identificationType = coreFactory.createIdentificationType();
		com.serff.template.svcarea.niem.proxy.xsd.String serviceAreaIDStr = xsdFactory.createString();
		serviceAreaIDStr.setValue(value);
		identificationType.setIdentificationID(serviceAreaIDStr);
		return identificationType;
	}

	/**
	 * Method is used for Service Area Adaptor to create Payload Type DTO using BenefitServiceAreaTemplateVO DTO (version 8).
	 * XML Element: <Payload>---</Payload>
	 */
	private com.serff.template.svcarea.extension.PayloadType convertToPayloadTypeVo(
			com.serff.template.plan.BenefitServiceAreaTemplateVO benefitServiceAreaTemplateVO) {

		LOGGER.debug("convertToPayloadTypeVo() Start");
		com.serff.template.svcarea.extension.PayloadType payloadType = null;

		try {

			if (null == benefitServiceAreaTemplateVO) {
				return payloadType;
			}
			com.serff.template.svcarea.extension.ObjectFactory extensionFactory = new com.serff.template.svcarea.extension.ObjectFactory();
			com.serff.template.svcarea.hix.pm.ObjectFactory pmFactory = new com.serff.template.svcarea.hix.pm.ObjectFactory();
			com.serff.template.svcarea.niem.core.ObjectFactory coreFactory = new com.serff.template.svcarea.niem.core.ObjectFactory();
			com.serff.template.svcarea.niem.proxy.xsd.ObjectFactory xsdFactory = new com.serff.template.svcarea.niem.proxy.xsd.ObjectFactory();
			com.serff.template.svcarea.niem.fips.ObjectFactory fipsFactory = new com.serff.template.svcarea.niem.fips.ObjectFactory();

			// XML Element: <Payload>---</Payload>
			payloadType = extensionFactory.createPayloadType();
			// XML Element: <Issuer>---</Issuer>
			com.serff.template.svcarea.hix.pm.IssuerType issuerType = convertToIssuerTypeVo(
					benefitServiceAreaTemplateVO.getHeader(), coreFactory, xsdFactory, pmFactory);
			payloadType.setIssuer(issuerType);

			// XML Element: <ServiceArea>---</ServiceArea> List
			List<com.serff.template.svcarea.hix.pm.ServiceAreaType> serviceAreaTypeList = issuerType.getServiceArea();
			List<com.serff.template.plan.BenefitServiceAreaVO> serviceAreaList = benefitServiceAreaTemplateVO.getServiceArea();

			if (!CollectionUtils.isEmpty(serviceAreaList)) {
				convertToServiceAreaTypeVo(serviceAreaTypeList, serviceAreaList, pmFactory, coreFactory, xsdFactory, fipsFactory);
			}
		}
		finally {
			LOGGER.debug("convertToPayloadTypeVo() End");
		}
		return payloadType;
	}

	/**
	 * Method is used for Service Area Adaptor to create Issuer Type DTO using HeaderVO DTO (version 8).
	 * XML Element: <Issuer>---</Issuer>
	 */
	private com.serff.template.svcarea.hix.pm.IssuerType convertToIssuerTypeVo(com.serff.template.plan.HeaderVO headerVO,
			com.serff.template.svcarea.niem.core.ObjectFactory coreFactory, com.serff.template.svcarea.niem.proxy.xsd.ObjectFactory xsdFactory,
			com.serff.template.svcarea.hix.pm.ObjectFactory pmFactory) {

		LOGGER.debug("convertToIssuerTypeVo() Start");
		com.serff.template.svcarea.hix.pm.IssuerType issuerType = pmFactory.createIssuerType();

		try {

			if (null != headerVO) {
				com.serff.template.svcarea.niem.usps.states.ObjectFactory statesFactory = new com.serff.template.svcarea.niem.usps.states.ObjectFactory();

				// XML Element: <IdentificationID>61589</IdentificationID>
				if (null != headerVO.getIssuerId()) {
					com.serff.template.svcarea.niem.core.IdentificationType identificationType = convertToIdentificationTypeVo(
							headerVO.getIssuerId().getCellValue(), coreFactory, xsdFactory);
	
					if (null != identificationType) {
						issuerType.setIssuerIdentification(identificationType);
					}
				}
				// XML Element: <IssuerStateCode>ID</IssuerStateCode>
				if (null != headerVO.getStatePostalCode() && StringUtils
						.isNotBlank(headerVO.getStatePostalCode().getCellValue())) {
					com.serff.template.svcarea.niem.usps.states.USStateCodeType stateCodeType = statesFactory.createUSStateCodeType();
					stateCodeType.setValue(USStateCodeSimpleType.fromValue(headerVO.getStatePostalCode().getCellValue()));
					issuerType.setIssuerStateCode(stateCodeType);
				}
			}
		}
		finally {
			LOGGER.debug("convertToIssuerTypeVo() End");
		}
		return issuerType;
	}

	/**
	 * Method is used for Service Area Adaptor to create Service Area Type DTO using BenefitServiceAreaVO DTO (version 8).
	 * XML Element: <ServiceArea>---</ServiceArea> List
	 */
	private void convertToServiceAreaTypeVo(List<com.serff.template.svcarea.hix.pm.ServiceAreaType> serviceAreaTypeList,
			List<com.serff.template.plan.BenefitServiceAreaVO> serviceAreaList,
			com.serff.template.svcarea.hix.pm.ObjectFactory pmFactory,
			com.serff.template.svcarea.niem.core.ObjectFactory coreFactory,
			com.serff.template.svcarea.niem.proxy.xsd.ObjectFactory xsdFactory,
			com.serff.template.svcarea.niem.fips.ObjectFactory fipsFactory) {

		LOGGER.debug("convertToServiceAreaTypeVo() Start");

		try {

			for (com.serff.template.plan.BenefitServiceAreaVO serviceAreaVO : serviceAreaList) {

				if (null == serviceAreaVO) {
					continue;
				}
				// XML Element: <ServiceAreaIdentification>---</ServiceAreaIdentification>
				com.serff.template.svcarea.hix.pm.ServiceAreaType serviceAreaType = pmFactory.createServiceAreaType();

				// XML Element: <IdentificationID>IDS001</IdentificationID>
				if (null != serviceAreaVO.getServiceAreaId()) {
					com.serff.template.svcarea.niem.core.IdentificationType serviceAreaID = convertToIdentificationTypeVo(
							serviceAreaVO.getServiceAreaId().getCellValue(), coreFactory, xsdFactory);
	
					if (null != serviceAreaID) {
						serviceAreaType.setServiceAreaIdentification(serviceAreaID);
					}
				}

				// XML Element: <ServiceAreaName>Statewide</ServiceAreaName>
				if (StringUtils.isNotBlank(serviceAreaVO.getServiceAreaName().getCellValue())) {
					com.serff.template.svcarea.niem.core.ProperNameTextType serviceAreaName = coreFactory.createProperNameTextType();
					serviceAreaName.setValue(serviceAreaVO.getServiceAreaName().getCellValue());
					serviceAreaType.setServiceAreaName(serviceAreaName);
				}
				// XML Element: <GeographicArea>---</GeographicArea>
				List<GeographicAreaType> geographicArea = serviceAreaType.getGeographicArea();

				// XML Element: <GeographicAreaEntireStateIndicator>false</GeographicAreaEntireStateIndicator>
				com.serff.template.svcarea.hix.pm.GeographicAreaType geographicAreaType = convertToGeographicAreaTypeVo(
						pmFactory, coreFactory, xsdFactory, fipsFactory, serviceAreaVO);

				geographicArea.add(geographicAreaType);
				serviceAreaTypeList.add(serviceAreaType);
			}
		}
		finally {
			LOGGER.debug("convertToServiceAreaTypeVo() End");
		}
	}

	/**
	 * Method is used for Service Area Adaptor to create Geographic Area Type DTO using BenefitServiceAreaVO DTO (version 8).
	 * XML Element: <GeographicArea>---</GeographicArea>
	 */
	private com.serff.template.svcarea.hix.pm.GeographicAreaType convertToGeographicAreaTypeVo(com.serff.template.svcarea.hix.pm.ObjectFactory pmFactory,
			com.serff.template.svcarea.niem.core.ObjectFactory coreFactory,
			com.serff.template.svcarea.niem.proxy.xsd.ObjectFactory xsdFactory,
			com.serff.template.svcarea.niem.fips.ObjectFactory fipsFactory,
			com.serff.template.plan.BenefitServiceAreaVO serviceAreaVO) {

		LOGGER.debug("convertToGeographicAreaTypeVo() Start");
		com.serff.template.svcarea.hix.pm.GeographicAreaType geographicAreaType = pmFactory.createGeographicAreaType();

		try {
			final String trueValue = "true";
			final String falseValue = "false";

			// XML Element: <GeographicAreaEntireStateIndicator>false</GeographicAreaEntireStateIndicator>
			boolean isStatewide = trueValue.equalsIgnoreCase(serviceAreaVO.getIsStatewide().getCellValue());
			com.serff.template.svcarea.niem.proxy.xsd.Boolean xsdBoolean = xsdFactory.createBoolean();
			xsdBoolean.setValue(isStatewide);
			geographicAreaType.setGeographicAreaEntireStateIndicator(xsdBoolean);

			if (!isStatewide) {

				// XML Element: <LocationCountyCode>007</LocationCountyCode>
				if (StringUtils.isNotBlank(serviceAreaVO.getCountyFips().getCellValue())) {

					String countyFips = serviceAreaVO.getCountyFips().getCellValue().trim();
					int countyFipsLength = countyFips.length() - SerffConstants.LEN_COUNTY_FIPS;

					if (SerffConstants.INITIAL_INDEX <= countyFipsLength) {
						com.serff.template.svcarea.niem.fips.USCountyCodeType countyCodeType = fipsFactory.createUSCountyCodeType();
						countyCodeType.setValue(countyFips.substring(countyFipsLength));
						JAXBElement<com.serff.template.svcarea.niem.fips.USCountyCodeType> countyCodeValue = coreFactory.createLocationCountyCode(countyCodeType);
						geographicAreaType.setLocationCounty(countyCodeValue);
					}
				}
				com.serff.template.plan.PartialCountyVO partialCountyVO = serviceAreaVO.getPartialCounty();

				if (null != partialCountyVO && null != partialCountyVO.getPartialCountyCovered()) {

					String partialCountyCovered = partialCountyVO.getPartialCountyCovered().getCellValue();

					// XML Element: <GeographicAreaPartialCountyIndicator>false</GeographicAreaPartialCountyIndicator>
					if (StringUtils.isNotBlank(partialCountyCovered)) {

						if (trueValue.equalsIgnoreCase(partialCountyCovered)) {
							xsdBoolean = xsdFactory.createBoolean();
							xsdBoolean.setValue(Boolean.TRUE);
							geographicAreaType.setGeographicAreaPartialCountyIndicator(xsdBoolean);
							// Create Partial County data DTOs when it is TRUE
							convertToPartialCountyDataVo(partialCountyVO, geographicAreaType, coreFactory);
						}
						else if (falseValue.equalsIgnoreCase(partialCountyCovered)) {
							xsdBoolean = xsdFactory.createBoolean();
							xsdBoolean.setValue(Boolean.FALSE);
							geographicAreaType.setGeographicAreaPartialCountyIndicator(xsdBoolean);
						}
					}
				}
			}
		}
		finally {
			LOGGER.debug("convertToGeographicAreaTypeVo() End");
		}
		return geographicAreaType;
	}

	/**
	 * Method is used for Service Area Adaptor to create Partial County data DTOs when it is TRUE using BenefitServiceAreaVO DTO (version 8).
	 * XML Element: <LocationPostalCode>95613</LocationPostalCode> List
	 * XML Element: <GeographicAreaPartialCountyJustificationText>Justification for partial county</GeographicAreaPartialCountyJustificationText>
	 */
	private void convertToPartialCountyDataVo(com.serff.template.plan.PartialCountyVO partialCountyVO,
			com.serff.template.svcarea.hix.pm.GeographicAreaType geographicAreaType,
			com.serff.template.svcarea.niem.core.ObjectFactory coreFactory) {

		LOGGER.debug("convertToPartialCountyDataVo() End");

		try {
			// XML Element: <LocationPostalCode>95613</LocationPostalCode> List
			if (null != partialCountyVO.getZipCode()
					&& StringUtils.isNotBlank(partialCountyVO.getZipCode().getCellValue())) {

				String[] zipCodes = partialCountyVO.getZipCode().getCellValue().replaceAll(SerffConstants.SPACE, StringUtils.EMPTY).split(SerffConstants.COMMA);

				for (String zipCode : zipCodes) {

					if (StringUtils.isBlank(zipCode)) {
						continue;
					}
					List<com.serff.template.svcarea.niem.proxy.xsd.String> locationPostalCode = geographicAreaType.getLocationPostalCode();
					com.serff.template.svcarea.niem.proxy.xsd.String zipCodeStr = new com.serff.template.svcarea.niem.proxy.xsd.String();
					zipCodeStr.setValue(zipCode);
					locationPostalCode.add(zipCodeStr);
				}
			}

			// XML Element: <GeographicAreaPartialCountyJustificationText>Justification for partial county</GeographicAreaPartialCountyJustificationText>
			if (null != partialCountyVO.getPartialCountyJustification()
					&& StringUtils.isNotBlank(partialCountyVO.getPartialCountyJustification().getCellValue())) {
				com.serff.template.svcarea.niem.core.TextType textType = coreFactory.createTextType();
				textType.setValue(partialCountyVO.getPartialCountyJustification().getCellValue());
				geographicAreaType.setGeographicAreaPartialCountyJustificationText(textType);
			}
		}
		finally {
			LOGGER.debug("convertToPartialCountyDataVo() End");
		}
	}

	@SuppressWarnings(RAW_TYPES)
	private Object parseTemplate(TransferDataTemplate dataTemplate, Class templateClass) throws GIException {
		Object templateObject = null;
			if (templateClass != null) {
				InputStream stream = null;
				try {
					stream = dataTemplate.getInsurerAttachment().getInsurerSuppliedData().getDataSource().getInputStream();
				templateObject = inputStreamToObject(stream, templateClass);
				}
				catch (IOException e) {
				LOGGER.error("parseTemplate() :IO Exception occurred :  " + e.getMessage(), e);
					throw new GIException(Integer.parseInt(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE), SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE_IO_ERROR + " : Exception occurred :" + e.getMessage(), "");
				}
				catch (Exception e) {
				LOGGER.error("parseTemplate() : JAXB Exception occurred :  " + e.getMessage(), e);
					throw new GIException(Integer.parseInt(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE), SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE_XML_PARSING_ERROR + " : Exception occurred :" + e.getMessage(), "");
				}
				finally {
					// Fixed HP-FOD issue.
					serffResourceUtil.releaseInputStream(stream);
				}
			} else {
				LOGGER.error("parseTemplate() : No Class found to parse: " + dataTemplate.getDataTemplateType());
			}
		return templateObject;
			}

	/**
	 * 
	 * This method parses Data Templates inside a request.
	 * @author Bhavin Parmar
	 * 
	 * @param templates
	 *        Templates that are present with plan.
	 *        
	 * @return Returns true when parsing templates succeeds else false.
	 * 
	 * @throws IOException
	 *  
	 * @throws JAXBException
	 */
	@SuppressWarnings(RAW_TYPES)
	public boolean parseDataTemplates(DataTemplates templates) throws IOException, Exception {

		LOGGER.info("parseTransferDataTemplates() : Parsing Data Templates for the request begine");
		boolean isParse = false;

		// Step 1 : Data transformation
		LOGGER.debug("parseTransferDataTemplates() : Data transformation");
		List<DataTemplate> transDataTempList = templates.getDataTemplate();

			for (DataTemplate dataTemplate : transDataTempList) {

				String templateType = dataTemplate.getDataTemplateType();
				Class templateClass = null;

				if (StringUtils.isNotEmpty(templateType)) {
					templateClass = templateTypeMap.get(templateType);
				}

				if (templateClass != null) {
					InputStream stream = dataTemplate
							.getRequiredInsurerSuppliedData().getDataSource()
							.getInputStream();
					inputStreamToObject(stream, templateClass);
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("parseTransferDataTemplates() : Transformation successful for the template "
							+ SecurityUtil.sanitizeForLogging(templateType));
					}
				}
			}
		// Step 2 : Data Mapping using Dozer

		// Step 3 : Call Plan Management service
		isParse = true;
		LOGGER.debug("parseTransferDataTemplates() : Calling Plan Management service");
		LOGGER.debug("parseTransferDataTemplates() : isParse: " + isParse);
		LOGGER.debug("parseTransferDataTemplates() : Parsing Data Templates for the request end");

		return isParse;
	}

	
	/**
	 * 
	 * This method generates an error response for a failed transfer plan operation
	 * @author Nikhil Talreja
	 * @since 08 March, 2013
	 * 
	 * @param errorResponse
	 *        TransferPlanResponse representing errors occurred.
	 * 
	 * @param errorCodes
	 *        List of different error codes in String format.
	 *         
	 * @param errorMessages
	 *         List of different error messages in String format.
	 *  
	 */
	public void generateErrorResponseForTransferPlan(TransferPlanResponse errorResponse, List<String> errorCodes, List<String> errorMessages){

		LOGGER.info("parseTransferDataTemplates() : Generating error response for transer plan operation");

		/*errorResponse.setSuccessful(false);
		errorResponse.setPlanStatus(PlanStatus.DOES_NOT_EXIST);*/

		// errorResponse.setExchangeWorkflowStatus("Failed");

		ValidationErrors errorList = new ValidationErrors();
		List<ValidationError> errors =   errorList.getErrors();

		//Adding validation errors to response
		if(!CollectionUtils.isEmpty(errorCodes) && !CollectionUtils.isEmpty(errorMessages)){
			LOGGER.info("parseTransferDataTemplates() : Total errors in response : " + errorCodes.size());
			for(int i=0; i<errorCodes.size(); i++){
				ValidationError error = new ValidationError();
				error.setCode(errorCodes.get(i));
				error.setMessage(errorMessages.get(i));
				errors.add(error);
			}
		}
		errorResponse.setValidationErrors(errorList);
	}
	
	/**
	 * 
	 * Clones an object using serialization
	 * @author Nikhil Talreja
	 * @since 08 March 2013
	 * 
	 * @param obj 
	 *        Object to be cloned.
	 *        
	 * @return Cloned object.
	 * @throws GIException 
	 */
	public  Object clone(Object obj) throws GIException{

		Object clonedObj = null;

		ByteArrayOutputStream baos = null;
		ObjectOutputStream oos = null;

		ByteArrayInputStream bais = null;
		ObjectInputStream ois = null;


		baos = new ByteArrayOutputStream();
		try {
			oos = new ObjectOutputStream(baos);
		
		oos.writeObject(obj);

		bais = new ByteArrayInputStream(baos.toByteArray());
		ois = new ObjectInputStream(bais);
		clonedObj = ois.readObject();

		oos.close();
		ois.close();

		baos = null;
		oos = null;
		bais = null;
		ois = null;
		} catch (ClassNotFoundException | IOException e) {
			LOGGER.error("clone() : Exception occurred while cloning an object : " + e.getMessage(), e);
			throw new GIException(Integer.parseInt(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE), SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE_IO_ERROR + " : Exception occurred while cloning an object : " + e.getMessage(), "");
		}

		return clonedObj;
	}
	
	/**
	 * Converts an object to xml string
	 * @author Nikhil Talreja
	 * @since 08 March 2013
	 * 
	 * @param object  Object to be converted into XML string. 
	 * @return XML String representation of the passed object.
	 * @throws GIException 
	 */
	public String objectToXmlString(Object object) throws GIException {

		LOGGER.info("Executing objectToXmlString() Start");
		String xmlContent = null;
		
		try {
			if (null == object) {
				return xmlContent;
			}
			JAXBContext context = JAXBContext.newInstance(object.getClass());
			JAXBIntrospector introspector = context.createJAXBIntrospector();
			Marshaller marshaller = context.createMarshaller();
			//marshaller.setProperty("jaxb.encoding", "US-ASCII");
			//marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE); 
			//marshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\"?>");
		    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		    StringWriter stringWriter = new StringWriter();

			if (null == introspector.getElementName(object)) {
				JAXBElement<Object> jaxbElement = new JAXBElement<Object>(new QName("soap"), Object.class, object);
				marshaller.marshal(jaxbElement, stringWriter);
			}
			else {
				marshaller.marshal(object, stringWriter);
			}
			xmlContent = stringWriter.toString();
		}
		catch (JAXBException e) {
			LOGGER.error("objectToXmlString() : Error creating request xml.",e);
			throw new GIException(Integer.parseInt(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE), SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE_XML_PARSING_ERROR + " : Error creating request xml : " + e.getMessage(), "");
		}
		finally {

			if (StringUtils.isBlank(xmlContent)) {
				LOGGER.error("XML content is empty.");
			}
			LOGGER.info("objectToXmlString() End");
		}
		return xmlContent;
	}

	/**
	 * Converts an object to JSON.
	 * @since 10 February 2016
	 * 
	 * @param object  Object to be converted into JSON string. 
	 * @return JSON String representation of the passed object.
	 */
	public String objectToJSONString(Object object) {

		LOGGER.info("Executing objectToJSONString() Start");
		String jsonContent = null;
		
		try {
			if (null == object) {
				return jsonContent;
			}
			jsonContent = platformGson.toJson(object);
		}
		catch (Exception ex) {
			jsonContent = "Error in converting Object to JSON.";
			LOGGER.error(jsonContent, ex);
		}
		finally {

			if (StringUtils.isBlank(jsonContent)) {
				LOGGER.error("JSON content is empty.");
			}
			LOGGER.info("objectToJSONString() End");
		}
		return jsonContent;
	}

	/**
	 * Converts XML InputStream to Object.
	 * 
	 * @since 08 March 2013
	 * @author Nikhil Talreja
	 * 
	 * @param in
	 *        Input stream to be converted into object.
	 *           
	 * @param c
	 *        Class to which above stream is to be converted.
	 * 
	 * @return Object representation of input stream.
	 * 
	 * @throws JAXBException
	 */
	@SuppressWarnings(RAW_TYPES)
	public Object inputStreamToObject(InputStream in, Class c) throws Exception {
		LOGGER.debug("Executing inputStreamToObject()");
		return GhixUtils.inputStreamToObject(in, c);
	}
	
	/**
	 * Converts Stream Reader to Object
	 * 
	 * 
	 * @param xmlData
	 *        XML representation of stream data.
	 *        
	 * @param c
	 *        Class to which stream is to be converted as an object.
	 *        
	 * @return Object representation of XMLStream.
	 * 
	 * @throws JAXBException
	 */
	@SuppressWarnings(RAW_TYPES)
	public Object streamReaderToObject(XMLStreamReader xmlData, Class c) throws JAXBException {
		
		LOGGER.info("Executing streamReaderToObject()");
		
		if (null == c) {
			return null;
		}
		LOGGER.debug("streamReaderToObject() : Creating Unmarshaller for class " + c.getName());		
		JAXBContext jaxbContext = JAXBContext.newInstance(c);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		return unmarshaller.unmarshal(xmlData);
	}
	
	/**
	 * 
	* This method causes the application to sleep for some time
	 * Used in JUnit testing
	 * @author Nikhil Talreja
	 * @since Apr 8, 2013
	 * 
	 * @throws InterruptedException
	 */
	public  void sleep() throws InterruptedException{

		if(SerffUtils.IS_TEST){
			Thread.sleep(SLEEP_TIME);
		}
	}

	
	/**
	 * 
	 * This method validates the incoming request object for Validate And Transform DataTemplate.
	 * @author Bhavin Parmar
	 * 
	 * @param request ValidateAndTransformDataTemplate instance.
	 * 
	 * @return
	 */
	public  boolean validateTransformDataTemplate(ValidateAndTransformDataTemplate request) {

		boolean isValidate = false;
		LOGGER.info("validateTransformDataTemplate() : Validating incoming request for Validate And Transform DataTemplate.");

		if (null != request && StringUtils.isNotEmpty(request.getRequestId())
				&& null != request.getDataTemplates()) {
			isValidate = true;
		}
		return isValidate;
	}
	
	
	/**
	 * 
	 * This method updates error details of an operation in SERFF_PLAN_MGMT record.
	 * @author Bhavin Parmar
	 * 
	 * @param record
	 *        SerffPlanMgmt instance saved in database.
	 *        
	 * @param response
	 *        TransferPlanResponse instance.
	 * 
	 * @param remarks
	 * 		  Remarks in String format.
	 * 
	 * @param errorDescription
	 *        String describing error occurred.
	 *       
	 * @return SerffPlanMgmt instance that saved in the database.
	 * @throws GIException 
	 */
	public  SerffPlanMgmt updateErrorDetailsForRecord(SerffPlanMgmt record, Object response, String remarks, String errorDescription) throws GIException {

		LOGGER.info("updateErrorDetailsForRecord() : Updating record with Error details.");
		SerffPlanMgmt newRecord = (SerffPlanMgmt) clone(record);
		newRecord.setEndTime(new Date());
		newRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
		newRecord.setRemarks(remarks);
		newRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
		newRecord.setRequestStateDesc(errorDescription);
		newRecord.setResponseXml(objectToXmlString(response));
		return newRecord;
	}

	/**
	 * This method updates success details of an operation in SERFF_PLAN_MGMT record.
	 * @author Bhavin Parmar
	 * 
	  * @param record
	 *        SerffPlanMgmt instance saved in database.
	 *        
	 * @param response
	 *        TransferPlanResponse instance.
	 * 
	 * @param remarks
	 * 		  Remarks in String format.
	 * 
	 * @return SerffPlanMgmt instance that saved in the database.
	 * 
	 * @throws IOException
	 * 
	 * @throws ClassNotFoundException
	 * @throws GIException 
	 */
	public  SerffPlanMgmt updateSuccessDetailsForRecord(SerffPlanMgmt record, Object response, String remarks) throws GIException {

		LOGGER.info("updateSuccessDetailsForRecord() : Updating record with Success details.");
		SerffPlanMgmt newRecord = (SerffPlanMgmt) clone(record);
		newRecord.setEndTime(new Date());
		newRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
		newRecord.setRemarks(remarks);
		newRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
		newRecord.setRequestStateDesc(REQUEST_STATUS);
		newRecord.setResponseXml(objectToXmlString(response));
		return newRecord;
	}
	
	/**
	 * 
	 * Calls Plan Management service to transfer Plan Benefit template
	 * @author Nikhil Talreja
	 * @since 15 March 2013
	 * 
	 * @param req
	 * @param url
	 * @return
	 */
	public String invokeRestCall(PlanMgmtRequest req, String url) {

		LOGGER.info("invokeRestCall() : REST call to Plan Management starts.");
		String response = null;

		try {

			if (null != req && StringUtils.isNotEmpty(url)) {
				//ObjectMapper jacksonMapper = new ObjectMapper();
				//jacksonMapper.setVisibility(com.fasterxml.jackson.annotation.PropertyAccessor.FIELD, Visibility.ANY);
				//String postParams = jacksonMapper.writeValueAsString(req);
				
				ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PlanMgmtRequest.class);
				String postParams = writer.writeValueAsString(req);

				LOGGER.debug("invokeRestCall() : Parameters : " + postParams);
				LOGGER.debug("invokeRestCall() : url : " + url);
				response = hixHttpClient.getPOSTData(url, postParams, SerffConstants.CONTENT_TYPE);
			}
		}
		catch (RuntimeException exception) {
			LOGGER.info("invokeRestCall() : " + SerffConstants.EMSG_PM_RESPONSE);
			throw exception;
		}
		catch (Exception exception) {
			LOGGER.error("invokeRestCall() : An exception occured in Plan Management module.",exception);
		}
		finally {
			LOGGER.info("invokeRestCall() : REST call to Plan Management ends.");
		}
		return response;
	}

	/**
	 * 
	 * Transforms CSRAdvancePaymentDeterminationType from payload.
	 *  
	 * @param payload
	 *        CSRAdvancePaymentDeterminationType instance.
	 * 
	 * @return CSRAdvancePaymentDeterminationType instance.
	 */
	public com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType mapCSRVO (com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType payload){
		LOGGER.error("Not implemented.");
		/*payload.setBenefitYearNumeric(null);
		payload.setSubmissionFileGenerationDateTime(null);
		return mapper.map(payload, com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType.class);*/
		return null;
	}

	/**
	 * This method creates error response for the serffPlanMgmt instance.
	 * 
	 * 
	 * @param serffPlanMgmt
	 * 		  serffPlanMgmt instance whos error response is to be created.
	 * 
	 * @param response
	 *        TransferPlanResponse instance.
	 *        
	 * @param serffService
	 *        Autowired service.
	 *         
	 * @param errorCodes
	 *        List of generated error codes. 
	 * 
	 * @param errorMessages
	 *        List of generated error messages. 
	 * 
	 * @return SerffPlanMgmt insnace whose error codes are upadated in database.
	 * 
	 * @throws GIException 
	 */
	private SerffPlanMgmt generateErrorResponse(SerffPlanMgmt serffPlanMgmt,
			TransferPlanResponse response, SerffService serffService,
			List<String> errorCodes, List<String> errorMessages)
			throws GIException {
		LOGGER.info("Executing generateErrorResponse().");
		SerffPlanMgmt record = serffPlanMgmt;
		generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
		record = updateErrorDetailsForRecord(record,response,SerffConstants.VALIDATION_ERROR,"Plan management call has not been made due to validation errors in the templates");
		record = serffService.saveSerffPlanMgmt(record);
		return record;
	}

	
	/**
	 * This method validates HIOS ID as per the standards.
	 * 
	 * @param templatePlanData Map of plan data.
	 * @param templateIssuerData Map of template data.
	 * @param templateDrugData Map of template drug data.
	 * @param templateRatesData Map of template rates data.
	 * @param templateServiceData Map of templates service data.
	 * @param templateNetworkData Map of template network data. 
	 * 
	 * @return true if valid HIOS id else false.
	 */
	private boolean validateHIOSIssuerID(
			Map<String, List<String>> templatePlanData,
			Map<String, List<String>> templateIssuerData,
			Map<String, List<String>> templateDrugData,
			Map<String, List<String>> templateRatesData,
			Map<String, List<String>> templateServiceData,
			Map<String, List<String>> templateNetworkData,
			Map<String, List<String>> templateBusinessRuleData,
			Map<String, List<String>> templateUnifiedRateData) {

		LOGGER.info("validateHIOSIssuerID() :  START");
		boolean flag = false;

		try {
			if (null == templatePlanData
					|| null == templateRatesData
					|| null == templateServiceData
					|| null == templateNetworkData) {
				return flag;
			}

			if(null != templatePlanData.get(HIOS_ISSUER_ID)
					&& null != templateRatesData.get(HIOS_ISSUER_ID)
					&& null != templateServiceData.get(HIOS_ISSUER_ID)
					&& null != templateNetworkData.get(HIOS_ISSUER_ID)
					&& (null == templateIssuerData ? true : null != templateIssuerData.get(HIOS_ISSUER_ID))
					&& (null == templateDrugData ? true : null != templateDrugData.get(HIOS_ISSUER_ID))
					&& (null == templateBusinessRuleData ? true : null != templateBusinessRuleData.get(HIOS_ISSUER_ID))
					&& (null == templateUnifiedRateData ? true : null != templateUnifiedRateData.get(HIOS_ISSUER_ID))) {

				if(!templatePlanData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX).equalsIgnoreCase(templateRatesData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX))
						|| !templatePlanData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX).equalsIgnoreCase(templateServiceData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX))
						|| !templatePlanData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX).equalsIgnoreCase(templateNetworkData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX))
						|| (null == templateIssuerData ? false : !templatePlanData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX).equalsIgnoreCase(templateIssuerData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX)))
						|| (null == templateDrugData ? false : !templatePlanData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX).equalsIgnoreCase(templateDrugData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX)))
						|| (null == templateBusinessRuleData ? false : !templatePlanData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX).equalsIgnoreCase(templateBusinessRuleData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX)))
						|| (null == templateUnifiedRateData ? false : !templatePlanData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX).equalsIgnoreCase(templateUnifiedRateData.get(HIOS_ISSUER_ID).get(INITIAL_INDEX)))) {
					
					LOGGER.error(SAME_HIOS_ID_MSG);
					validationErrors.add(getFailedValidation("HIOS Issuer ID", null, SAME_HIOS_ID_MSG, ECODE_DEFAULT, "All Templates"));
				}
				else {
					LOGGER.info("HIOS Issuer Id matched successfully for all templates.");
					flag = true;
				}
			}
		}
		finally {
			LOGGER.debug("validateHIOSIssuerID() : END.");
		}
		return flag;
	}

	
	/**
	 * This method validates templates type.
	 * 
	 * @param templateObjects
	 *        List of template objects.
	 *           
	 * @return true if all templates are valid else false.
	 */
	private boolean isTemplatesAvailable(List<Object> templateObjects, boolean adminTemplateRequired) {

		LOGGER.debug("Executing isTemplatesAvailable() : ");
		
		boolean flag = false;
		int count = 0;

		for (Object template : templateObjects) {
			if(null != template) {
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Template type : " + template.getClass().getName());
				}
	
				if (template instanceof com.serff.template.plan.PlanBenefitTemplateVO) {
					count++;
				}
				else if (template instanceof com.serff.template.admin.extension.PayloadType
						&& adminTemplateRequired) {
					count++;
				}
				/*else if (template instanceof com.serff.template.plan.PrescriptionDrugTemplateVO) {
					count++;
				}*/
				else if (template instanceof com.serff.template.plan.QhpApplicationRateGroupVO || template instanceof com.serff.template.plan.QhpApplicationRateGroupListVO) {
					count++;
				}
				/*else if (template instanceof com.serff.template.plan.QhpApplicationEcpVO) {
					count++;
				}*/
				else if (template instanceof com.serff.template.svcarea.extension.PayloadType) {
					count++;
				}
				else if (template instanceof com.serff.template.networkProv.extension.PayloadType) {
					count++;
				}
				/*else if (template instanceof com.serff.template.rbrules.extension.PayloadType) {
					count++;
				}*/
			}
		}

		if ((adminTemplateRequired && (TEMPLATE_COUNT + 1) == count)
				|| (!adminTemplateRequired && TEMPLATE_COUNT == count)) {
			flag = true;
		}
		else {
			String missingTemplateEMsg = "Missing template. Total of ";
			
			if (adminTemplateRequired) {
				missingTemplateEMsg += (TEMPLATE_COUNT + 1) + REQUIRED_TEMPLATE_MSG;
			}
			else {
				missingTemplateEMsg += TEMPLATE_COUNT + REQUIRED_TEMPLATE_MSG;
			}
			validationErrors.clear();
			validationErrors.add(getFailedValidation(null, null, missingTemplateEMsg, ECODE_DEFAULT, null));
		}
		return flag;
	}

	
	/**
	 * This method add templates to the  PlanMgmtRequest instance.
	 * 
	 * @param templateObjects Available templates list.
	 * @param supportingDocuments Available supporting dicuments list.  
	 * @param request TransferPlan instance of the plan instance.
	 * @param response TransferPlanResponse instance of the plan instance.
	 * @param serffPlanMgmtRec SerffPlanMgmt instance of corresponding Plan instance in  database.   
	 * @param serffService Autowired service.
	 * @param mapper Objectmapper utility to map fields.
	 * @param errorCodes List of error codes in String format.
	 * @param errorMessages List of error messages in String format.
	 * @param exchangeTypeFilter Selected exchange type filter, either On/OFF. 
	 * @param tenantIds Selected list of tenant id for this Plan.
	 * @return SerffPlanMgmt instance after updating into database.
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws GIException 
	 */	
	public SerffPlanMgmt addTemplatesToPlanManagementRequest(
			List<Object> templateObjects, Map<String, String> supportingDocuments,TransferPlan request,
			TransferPlanResponse response, SerffPlanMgmt serffPlanMgmtRec,
			SerffService serffService, 
			List<String> errorCodes, List<String> errorMessages, String exchangeTypeFilter, String tenantIds, byte[] logoBytes)
			throws GIException {

		LOGGER.info("addTemplatesToPlanManagementRequest(): Adding templates to Plan Management Request.");
		PlanMgmtRequest req = new PlanMgmtRequest();
		SerffPlanMgmt record = serffPlanMgmtRec;
		req.setAppID(SerffConstants.SERFF_APP_ID);
		req.setRequestTime(null);
		req.setSupportingDocuments(supportingDocuments);

		if (null != request && null != request.getPlan()) {

			// removing supporting docs and templates
			SupportingDocuments supportingDocs = null;
			TransferDataTemplates templates = null;

			if(null != request.getPlan().getSupportingDocuments()) {
				supportingDocs = request.getPlan().getSupportingDocuments();
			}

			if(null != request.getPlan().getDataTemplates()) {
				templates = request.getPlan().getDataTemplates();
			}
			removeBinaryData(supportingDocs, templates);
		}

		if (null != templateObjects) {

			// Validate NCQA and URAC data
			//NCQA And URAC objects are made optional for now, as SERFF is not pushing this data
			//Below code need to be uncommented if validation is required, TO BE REVISITED FOR NEXT RELEASE
			//validateNCQAAndURAC(request, errorCodes, errorMessages);

			if (!CollectionUtils.isEmpty(errorCodes)) {
				return generateErrorResponse(record, response, serffService, errorCodes, errorMessages);
			}
			req.setTransferPlan((request));
			Map<String, List<String>> templatePlanData = null;
			Map<String, List<String>> templateIssuerData = null;
			Map<String, List<String>> templateDrugData = null;
			Map<String, List<String>> templateRatesData = null;
			Map<String, List<String>> templateServiceData = null;
			Map<String, List<String>> templateNetworkData = null;
			Map<String, List<String>> templateDataRule = null;
			Map<String, List<String>> templateUnifiedRate = null;
			/*Map<String, List<String>> templateUrac = null;
			Map<String, List<String>> templateNcqa = null;*/
			Map<String, Object> reqTemplates = new HashMap<String, Object>();
			Object template = null;

			for (Object templateObj : templateObjects) {
				if (templateObj instanceof com.serff.template.admin.extension.PayloadType) {
					reqTemplates.put(SerffConstants.DATA_ADMIN_DATA, templateObj);
				} 
				else if (templateObj instanceof com.serff.template.plan.PrescriptionDrugTemplateVO) {
					reqTemplates.put(SerffConstants.DATA_PRESCRIPTION_DRUG, templateObj);
				}
				else if (templateObj instanceof com.serff.template.rbrules.extension.PayloadType ) {
					reqTemplates.put(SerffConstants.DATA_RATING_RULES, templateObj);
				}
				else if (templateObj instanceof com.serff.template.plan.QhpApplicationRateGroupVO) {
					reqTemplates.put(SerffConstants.DATA_RATING_TABLE, templateObj);
				}
				else if (templateObj instanceof com.serff.template.plan.QhpApplicationRateGroupListVO) {
					reqTemplates.put(SerffConstants.DATA_RATING_TABLE, preProcessRatesTemplateForYear2018((QhpApplicationRateGroupListVO) templateObj));
				}
				else if (templateObj instanceof com.serff.template.svcarea.extension.PayloadType) {
					reqTemplates.put(SerffConstants.DATA_SERVICE_AREA, templateObj);
				}
				else if (templateObj instanceof com.serff.template.networkProv.extension.PayloadType) {
					reqTemplates.put(SerffConstants.DATA_NETWORK_ID, templateObj);
				}
				else if (templateObj instanceof PlanBenefitTemplateVO) {
					reqTemplates.put(SerffConstants.DATA_BENEFITS, templateObj);
				}
				else if (templateObj instanceof InsuranceRateForecastTempVO) {
					reqTemplates.put(SerffConstants.DATA_UNIFIED_RATE, templateObj);
				}
				else if (templateObj instanceof JAXBElement) {
					if (((JAXBElement)templateObj).getValue() instanceof com.serff.template.urac.IssuerType) {
						reqTemplates.put(SerffConstants.DATA_URAC_DATA, ((JAXBElement) templateObj).getValue());
					 }
					 if (((JAXBElement)templateObj).getValue() instanceof  com.serff.template.ncqa.IssuerType) {
						 reqTemplates.put(SerffConstants.DATA_NCQA_DATA, ((JAXBElement) templateObj).getValue());
					 }
				}
			}

			boolean adminTemplateRequired = false; //from 2017 onwards admin template is not required
			String missingTemplateEMsg = "Missing template. Total of " + TEMPLATE_COUNT + REQUIRED_TEMPLATE_MSG;
			// Validate Missing template.
			if (!isTemplatesAvailable(templateObjects, adminTemplateRequired) && !CollectionUtils.isEmpty(validationErrors)) {
				LOGGER.error(missingTemplateEMsg);
				addValidationErrorsToResponse(errorCodes, errorMessages);
				generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
				record = updateErrorDetailsForRecord(record, response, SerffConstants.MISSING_TEMPLATE_ERROR, missingTemplateEMsg);
				record = serffService.saveSerffPlanMgmt(record);
				return record;
			}
			//validationErrors needs to be cleared to consolidate errors from all templates
			validationErrors.clear();

			template = reqTemplates.get(SerffConstants.DATA_ADMIN_DATA);

			if (template instanceof com.serff.template.admin.extension.PayloadType) {
				LOGGER.debug("Template type " + template.getClass().getName());
				templateIssuerData = issuerDataValidator.validateIssuerData((com.serff.template.admin.extension.PayloadType) template, validationErrors);

				if (CollectionUtils.isEmpty(validationErrors)) {
					req.setAdminTemplate((com.serff.template.admin.extension.PayloadType) template);
					LOGGER.info("Added issuer template details to Plan Management request");
				}
				else {
					LOGGER.debug("Validation errors exist in Issuer template");
					addValidationErrorsToResponse(errorCodes, errorMessages);
				}
				validationErrors.clear();
			}

			template = reqTemplates.get(SerffConstants.DATA_PRESCRIPTION_DRUG);

			if (template instanceof com.serff.template.plan.PrescriptionDrugTemplateVO) {

				templateDrugData = prescriptionDrugValidatorV5.validatePrescriptionDrug(
						(com.serff.template.plan.PrescriptionDrugTemplateVO) template, validationErrors);

				if (CollectionUtils.isEmpty(validationErrors)) {
					req.setPrescriptionDrug((com.serff.template.plan.PrescriptionDrugTemplateVO) template);
					LOGGER.info("Added Prescription Drug template details to Plan Management request");
				}
				else {
					LOGGER.debug("Validation errors exist in Prescription and Drug template");
					addValidationErrorsToResponse(errorCodes, errorMessages);
				}
				validationErrors.clear();
			}

			template = reqTemplates.get(SerffConstants.DATA_RATING_TABLE);
			if (template instanceof com.serff.template.plan.QhpApplicationRateGroupVO) {

				templateRatesData = ratesValidator.validateRates((com.serff.template.plan.QhpApplicationRateGroupVO) template, validationErrors);

				if (CollectionUtils.isEmpty(validationErrors)) {
					req.setRates((com.serff.template.plan.QhpApplicationRateGroupVO) template);
					LOGGER.info("Added Rates template details to Plan Management request");
				}
				else {
					LOGGER.debug("Validation errors exist in Rates Rule template");
					addValidationErrorsToResponse(errorCodes, errorMessages);
				}
				validationErrors.clear();
			}

			template = reqTemplates.get(SerffConstants.DATA_SERVICE_AREA);
			if (template instanceof com.serff.template.svcarea.extension.PayloadType) {

				templateServiceData = serviceAreaValidator.validateServiceArea((com.serff.template.svcarea.extension.PayloadType) template, validationErrors);

				if (CollectionUtils.isEmpty(validationErrors)) {
					req.setServiceArea((com.serff.template.svcarea.extension.PayloadType) template);
					LOGGER.info("Added Service Area template details to Plan Management request");
				}
				else {
					LOGGER.debug("Validation errors exist in Service Area template");
					addValidationErrorsToResponse(errorCodes, errorMessages);
				}
				validationErrors.clear();
			}

			template = reqTemplates.get(SerffConstants.DATA_NETWORK_ID);
			if (template instanceof com.serff.template.networkProv.extension.PayloadType) {

				templateNetworkData = networkValidator.validateNetwork((com.serff.template.networkProv.extension.PayloadType) template, validationErrors);

				if (CollectionUtils.isEmpty(validationErrors)) {
					req.setNetworkProvider((com.serff.template.networkProv.extension.PayloadType) template);
					LOGGER.info("Added Network Provider template details to Plan Management request");
				}
				else {
					LOGGER.debug("Validation errors exist in Network Provider template");
					addValidationErrorsToResponse(errorCodes, errorMessages);
				}
				validationErrors.clear();
			}

			template = reqTemplates.get(SerffConstants.DATA_BENEFITS);
			if (template instanceof PlanBenefitTemplateVO) {

				String templateVersion = getTemplateVersion(template);

				if (StringUtils.isNotBlank(templateVersion)
						&& (templateVersion.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_6)
								|| templateVersion.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_7)
								|| templateVersion.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_8)
								|| templateVersion.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_9))) {
					templatePlanData = planValidatorV6.validatePlan((PlanBenefitTemplateVO) template, templateRatesData, templateNetworkData,
							templateServiceData, validationErrors, exchangeTypeFilter);
				}
				else {
					templatePlanData = planValidatorV5.validatePlan((PlanBenefitTemplateVO) template, templateRatesData, templateNetworkData,
							templateServiceData, validationErrors, exchangeTypeFilter);
				}

				if (CollectionUtils.isEmpty(validationErrors)) {
					req.setPlanBenefit((PlanBenefitTemplateVO)template);
					LOGGER.debug("Added plan details to Plan Management request");
				}
				else {
					LOGGER.debug("Validation errors exist in Plan and Benefits template");
					addValidationErrorsToResponse(errorCodes, errorMessages);
				}
			}
			template = reqTemplates.get(SerffConstants.DATA_RATING_RULES);
			if (template instanceof com.serff.template.rbrules.extension.PayloadType) {
				
				templateDataRule = businessRuleValidator.validateBusinessRule( (com.serff.template.rbrules.extension.PayloadType)template, validationErrors);
				
					
					if (CollectionUtils.isEmpty(validationErrors)) {
						req.setRateBussinessRules((com.serff.template.rbrules.extension.PayloadType)template);
						LOGGER.debug("Added Business Rule details to Plan Management request");
					}
					else {
						LOGGER.debug("Validation errors exist in Business Rule template");
						addValidationErrorsToResponse(errorCodes, errorMessages);
					}
				
			}

			
			template = reqTemplates.get(SerffConstants.DATA_UNIFIED_RATE);
			if (template instanceof com.serff.template.urrt.InsuranceRateForecastTempVO) {
				
				templateUnifiedRate = unifiedRateValidator.validateUnifiedRate((com.serff.template.urrt.InsuranceRateForecastTempVO)template, validationErrors);
									
					if (CollectionUtils.isEmpty(validationErrors)) {
						req.setUnifiedRate((com.serff.template.urrt.InsuranceRateForecastTempVO)template);
						LOGGER.debug("Added Unified Rate to Plan Management request");
					}
					else {
						LOGGER.debug("Validation errors exist in Unified Rate template");
						addValidationErrorsToResponse(errorCodes, errorMessages);
					}
			}
			
			template = reqTemplates.get(SerffConstants.DATA_URAC_DATA);
			if(template instanceof com.serff.template.urac.IssuerType){
				req.setUracTemplate((com.serff.template.urac.IssuerType)template);
				
			}
			
			template = reqTemplates.get(SerffConstants.DATA_NCQA_DATA);
			if(template instanceof com.serff.template.ncqa.IssuerType ){
				req.setNcqaTemplate((com.serff.template.ncqa.IssuerType)template);
			}
			
			
			LOGGER.debug("Total number of templates received : " + templateObjects.size());

			if (!CollectionUtils.isEmpty(errorCodes)) {
				return generateErrorResponse(record, response, serffService, errorCodes, errorMessages);
			}
			else if (!validateHIOSIssuerID(templatePlanData, templateIssuerData, templateDrugData, templateRatesData,
					templateServiceData, templateNetworkData ,templateDataRule, templateUnifiedRate)) {
				addValidationErrorsToResponse(errorCodes, errorMessages);
				generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
				record = updateErrorDetailsForRecord(record, response, SerffConstants.ISSUER_ID_MISMATCH, SAME_HIOS_ID_MSG);
				record = serffService.saveSerffPlanMgmt(record);
				return record;
			}
		}

		LOGGER.debug("Invoking REST call to Plan Management");
		PlanUpdateStatistics uploadStatistics = new PlanUpdateStatistics();
		PlanMgmtResponse planMgmtResponse = serffTemplateService.tranferPlan(req, exchangeTypeFilter, tenantIds, uploadStatistics, logoBytes);
		LOGGER.info("Updating Upload Statistics as: " + uploadStatistics.toString());
		record.setPlanStats(uploadStatistics.toString());

		if (null != planMgmtResponse) {
			
			if (!StringUtils.equalsIgnoreCase(planMgmtResponse.getStatus(),SerffConstants.SUCCESS)) {
				String remark = null;

				if (planMgmtResponse.getErrCode() == 0) {
					planMgmtResponse.setErrCode(Integer.parseInt(SerffConstants.APP_EXCEPTION_CODE));
				}

				if (planMgmtResponse.getErrMsg() == null || planMgmtResponse.getErrMsg().length() == 0) {
					planMgmtResponse.setErrMsg(SerffConstants.APP_EXCEPTION_MESSAGE);
				}

				if(planMgmtResponse.getErrMsg().contains(SerffConstants.PLAN_CERTIFIED_MESSAGE_NO_FUTURE_RATES) || 
						planMgmtResponse.getErrMsg().contains(SerffConstants.PLAN_CERTIFIED_MESSAGE_RATES_NOT_MODIFIED)){
					remark = SerffConstants.NOUPDATE_ERROR;
				} else if (planMgmtResponse.getErrCode() == Integer.parseInt(SerffConstants.APP_EXCEPTION_CODE)) {
					remark = SerffConstants.LOADING_ERROR;
				} else {
					remark = SerffConstants.INVALID_REQUEST;
				}
				errorCodes.add(String.valueOf(planMgmtResponse.getErrCode()));
				errorMessages.add(planMgmtResponse.getErrMsg());
				generateErrorResponseForTransferPlan(response,errorCodes, errorMessages);
				record = updateErrorDetailsForRecord(record, response, remark,ERR_RESPONSE_MSG);
			}
			else {
				record = generateResponseFromRESTResponse(record, serffService, planMgmtResponse, response, errorMessages, errorMessages);
			}
		}
		record = serffService.saveSerffPlanMgmt(record);
		return record;
	}

	private QhpApplicationRateGroupVO preProcessRatesTemplateForYear2018(QhpApplicationRateGroupListVO qhpApplicationRateGroupListVO) {
		QhpApplicationRateGroupVO combinedRateGroupVO = null;
		
		if(null != qhpApplicationRateGroupListVO && null != qhpApplicationRateGroupListVO.getQhpApplicationRateGroupVO()) {
			List<QhpApplicationRateGroupVO> ratesGroupVOList = qhpApplicationRateGroupListVO.getQhpApplicationRateGroupVO();
			ExcelCellVO familyOptionAgeNumber = new ExcelCellVO();
			familyOptionAgeNumber.setCellValue(FAMILY_OPTION);
			for (QhpApplicationRateGroupVO qhpApplicationRateGroupVO : ratesGroupVOList) {
				if(FAMILY_TIER_RATES.equals(qhpApplicationRateGroupVO.getRatingMethod().getCellValue())) {
					for (QhpApplicationRateItemVO rateItemVO : qhpApplicationRateGroupVO.getItems()) {
						rateItemVO.setAgeNumber(familyOptionAgeNumber);
					}
				}
				if(null == combinedRateGroupVO) {
					combinedRateGroupVO = qhpApplicationRateGroupVO;
				} else {
					combinedRateGroupVO.getItems().addAll(qhpApplicationRateGroupVO.getItems());
				}
			}
		}
		return combinedRateGroupVO;
	}
	
	/**
	 * Method is used to get Template Version from template.
	 * @return Template Version
	 */
	public String getTemplateVersion(Object template) {

		LOGGER.debug("getTemplateVersion() Start");
		String version = null;
		HeaderVO header = null;
		String templateName = null;

		try {
			if (null == template) {
				LOGGER.error("Template object has been empty.");
				return version;
			}

			if (template instanceof PrescriptionDrugTemplateVO) {
				templateName = "PrescriptionDrugTemplateVO";
				PrescriptionDrugTemplateVO drugVO = (PrescriptionDrugTemplateVO) template;
				header = drugVO.getHeader();
			}
			else if (template instanceof PlanBenefitTemplateVO) {
				templateName = "PlanBenefitTemplateVO";
				PlanBenefitTemplateVO planBenefitVO = (PlanBenefitTemplateVO) template;

				if (null != planBenefitVO.getPackagesList()
						&& !CollectionUtils.isEmpty(planBenefitVO.getPackagesList().getPackages())) {
					header = planBenefitVO.getPackagesList().getPackages().get(INITIAL_INDEX).getHeader();
				}
				else {
					LOGGER.error("Packages List is empty.");
				}
			}
			else {
				LOGGER.error("Template object is not found.");
			}

			if (null != header) {
				version = header.getTemplateVersion();
			}
			else {
				LOGGER.warn("Template["+ templateName +"] Header has been empty.");
			}
		}
		catch (Exception e) {
			LOGGER.error("Validating Template Version [" + version + "] Error: " + e.getMessage(), e);
		}
		finally {
			LOGGER.debug("getTemplateVersion() End with Template["+ templateName +"] Version: " + version);
		}
		return version;
	}

	/**
	 * Method is used to return FailedValidation with passed by arguments value.
	 * 
	 * @param fieldName Field Name
	 * @param fieldValue Field Value
	 * @param errorMsg Error Message
	 * @param errorCode Error Code
	 * @param xPath XML Path
	 *        
	 * @return Object of FailedValidation
	 */
	public FailedValidation getFailedValidation(String fieldName, String fieldValue, String errorMsg, String errorCode, String templateName) {

		LOGGER.debug("Executing getFailedValidation()");		
		FailedValidation failedValidation = new FailedValidation();
		failedValidation.setFieldName(fieldName);
		failedValidation.setFieldValue(fieldValue);
		failedValidation.setErrorMsg(errorMsg);
		failedValidation.setErrorCode(errorCode);
		failedValidation.setTemplateName(templateName);
		return failedValidation;
	}

	/**
	 * This method maps exceptions to SERFF error codes
	 * @author Nikhil Talreja
	 * @since Apr 2, 2013
	 * @param
	 * @return void
	 */
	public void addExceptionCodeToResponse(Exception e,
			List<String> errorCodes, List<String> errorMessages) {
		
		LOGGER.debug("Executing addExceptionCodeToResponse()");

	    if (e instanceof RuntimeException){
			errorCodes.add(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE);
			errorMessages.add(SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE + e.getMessage());
		}
		else if (e instanceof GIException){
			errorCodes.add(String.valueOf(((GIException)e).getErrorCode()));
			errorMessages.add(((GIException)e).getErrorMsg());
		}
		else {
			errorCodes.add(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE);
			errorMessages.add(SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE+ e.getMessage());
		}
	}

	/**
	 * This methods adds validation errors in templates to Web Service repsonse
	 * @author Nikhil Talreja
	 * @since Apr 2, 2013
	 * @param
	 * @return void
	 */
	private void addValidationErrorsToResponse(List<String> errorCodes, List<String> errorMessages) {		
				
		LOGGER.info("Total errors till now  : " + errorCodes.size());		
		LOGGER.debug("Total validation errors in current template : " + validationErrors.size());

		for (FailedValidation failedValidation : validationErrors) {

			errorCodes.add(failedValidation.getErrorCode());
			errorMessages.add(failedValidation.getErrorMsg());
			errorCodes.add(failedValidation.getErrorCode());

			if (null != failedValidation.getFieldValue()) {
				errorMessages.add("Template name: " + failedValidation.getTemplateName()
						+ ", Field name: " + failedValidation.getFieldName()
						+ ", Field value: " + failedValidation.getFieldValue());
			}
			else {
				errorMessages.add("Template name: " + failedValidation.getTemplateName()
						+ ", Field name: " + failedValidation.getFieldName());
			}
		}
	}
	
	/**
	 * 
	 * This method converts Plan Management response to Transfer Plan response
	 * @author Nikhil Talreja
	 * @since Apr 8, 2013
	 * 
	 * @param serffPlanMgmt 
	 *        serffPlanMgmt instance to be converted into Transfer plan.
	 * 
	 * @param serffService
	 *        Autowired service instance.   
	 * 
	 * @param planMgmtResponse
	 *        PlanMgmtResponse instance.
	 * 
	 * @param response
	 *        TransferPlanResponse instance.
	 * 
	 * @param errorCodes
	 *        List of error codes generated.
	 * 
	 * @param errorMessages
	 *        List of error messages generated.
	 * 
	 * @return SerffPlanMgmt instance that has been updated in the database.
	 * 
	 * @throws IOException
	 * 
	 * @throws ClassNotFoundException
	 * @throws GIException 
	 */
	public SerffPlanMgmt generateResponseFromRESTResponse(
			SerffPlanMgmt serffPlanMgmt, SerffService serffService,
			PlanMgmtResponse planMgmtResponse, TransferPlanResponse response, List<String> errorCodes, List<String> errorMessages) throws GIException {

		// Creating response based on the REST response		
		LOGGER.info("Executing generateResponseFromRESTResponse() : Generating response.");
		
		SerffPlanMgmt record = serffPlanMgmt;
		if (StringUtils.equalsIgnoreCase(planMgmtResponse.getStatus(), SerffConstants.SUCCESS)) {

			record.setPmResponseXml(objectToXmlString(planMgmtResponse));
			record = updateSuccessDetailsForRecord(record, response,
					SerffConstants.VALID_REQUEST);
			record = serffService.saveSerffPlanMgmt(record);

			/*response.setSuccessful(true);
			response.setExchangeWorkflowStatus("Successful");

			if (null != planMgmtResponse.getPlanStatus()) {
				response.setPlanStatus(PlanStatus.fromValue(planMgmtResponse.getPlanStatus().value()));
			}*/
		}
		else if (!StringUtils.equalsIgnoreCase(planMgmtResponse.getStatus(), SerffConstants.SUCCESS)) {

			if(planMgmtResponse.getErrCode() == 0) {
				planMgmtResponse.setErrCode(Integer.parseInt(SerffConstants.APP_EXCEPTION_CODE));
			}
			if(planMgmtResponse.getErrMsg() == null || planMgmtResponse.getErrMsg().length() == 0) {
				planMgmtResponse.setErrMsg(SerffConstants.APP_EXCEPTION_MESSAGE);
			}
			errorCodes.add(String.valueOf(planMgmtResponse.getErrCode()));
			errorMessages.add(planMgmtResponse.getErrMsg());
			generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
			record = updateErrorDetailsForRecord(record, response, SerffConstants.LOADING_ERROR,ERR_RESPONSE_MSG);
		}
		return record;
	}

	/**
	 * Method is used to get Original File Name from ECM server using ECM-ID.
	 */
	public String getECMFileName(String ecmId, ContentManagementService ecmService) {

		LOGGER.debug("getECMFileName() Start");
		String ecmFileName = null;

		try {

			if (StringUtils.isBlank(ecmId)) {
				return ecmFileName;
			}

			Content content = ecmService.getContentById(ecmId);
			if (null != content) {
				ecmFileName = content.getOriginalFileName();
			}
			else {
				LOGGER.error("File is not exist at ECM.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("getECMFileName() Exception occurred: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("getECMFileName() End");
		}
		return ecmFileName;
	}

	/**
	 * Method is used to get State Code from global-configuration.xml file.
	 * 
	 * @return State code in String format.
	 */
	public String getGlobalStateCode() {
		
		LOGGER.info("Executing getGlobalStateCode()");
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		LOGGER.info("GlobalConfigurationEnum.STATE_CODE: " + stateCode);

		if (SerffConstants.PHIX_PROFILE.equalsIgnoreCase(stateCode)) {
			stateCode = SerffConstants.DEFAULT_PHIX_STATE_CODE;
		}
		return stateCode;
	}
	
	
	/**
	 * This method checks if url starts with http or https id not then append http://.
	 * @author Vani Sharma
	 * @param url string
	 * @return String
	 */
	public static String checkAndcorrectURL(String url) {
		String value = null;
		String valueToCompare = null;
		String rval= null;
		if (null != url
				&& StringUtils.isNotEmpty(url)) {
			value = StringUtils.trim(url);
			valueToCompare = value.substring(0, value.length() > 15 ? 15 : value.length()-1).replaceAll(SerffConstants.SPACE, StringUtils.EMPTY); 
			if(valueToCompare.startsWith(SerffConstants.HTTP_STRING)||valueToCompare.startsWith(SerffConstants.HTTPS_STRING)){
				rval= value;
			}else{
				rval =SerffConstants.HTTP_STRING+value;
			}
		}
		return rval;
	}

	/**
	 * Method is used to get Property List from GI_APP_CONFIG table.
	 * @param propertyKeyPrefix Prefix of Property Key [e.g planManagement.HealthCosts.Cost.]
	 * @return Map <propertyValue, propertyKey (exclude propertyKeyPrefix value)>
	 */
	public Map<String, String> getPropertyList(PlanmgmtConfigurationEnum propertiesEnumMarker) {

		LOGGER.info("getPropertyList() Started");
		Map<String, String> propertiesMap = null;

		try {
			Map<String, String> propertiesListMap = DynamicPropertiesUtil.getPropertyValueList(propertiesEnumMarker);
			if (null == propertiesListMap || propertiesListMap.isEmpty()) {
				LOGGER.error("propertiesListMap is empty or null");
				return propertiesMap;
			}

			String entryKey = null;
			propertiesMap = new HashMap<String, String>();

			for (Map.Entry<String, String> entry : propertiesListMap.entrySet()) {

				entryKey = entry.getKey().substring(propertiesEnumMarker.getValue().length());

				if (StringUtils.isNotBlank(entryKey)) {
					// Convert Map Key in lowercase.
					propertiesMap.put(entry.getValue().toLowerCase(), entryKey.substring(entryKey.indexOf(SerffConstants.DOT) + 1));
				}
			}
			LOGGER.info("Size for "+ propertiesEnumMarker.getValue() +": " + propertiesMap.size());
		}
		finally {
			LOGGER.info("getPropertyList() End");
		}
		return propertiesMap;
	}
	
	/**
	 * Will process plan rates for Idaho state when there is max child age limit configured by property 'serff.state.dental.maxChildAgeLimit'
	 * in GI_APP_CONFIG table.
	 * 
	 * @param dentalPlanRates - Dental plan rates before processing for max child age limit.
	 * @return - Dental plan rates after processing for max child age limit.
	 */
	public List<PlanRate> preProcessDentalPlanRates(List<PlanRate> dentalPlanRates){
		
		LOGGER.info("preProcessDentalPlanRates() start.");
		//HIX-63650 : Change rates persistence code to override child max age for rates in the template.
		boolean isIdahoMaxChildAgeEnabled = false;
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		String idahoMaxChildAgeLimitString = DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.STATE_DENTAL_MAX_CHILD_AGE_LIMIT);
		idahoMaxChildAgeLimitString = idahoMaxChildAgeLimitString == null ? null : idahoMaxChildAgeLimitString.trim();
		int idahoMaxChildAgeLimit = 0;
		LOGGER.debug("StateCode : " + stateCode + ". Max age limit for Idaho. Configuration value :" + idahoMaxChildAgeLimitString);
		
		if(StringUtils.isNumeric(idahoMaxChildAgeLimitString) && SerffConstants.IDAHO_STATE_CODE.equals(stateCode)){
			
			idahoMaxChildAgeLimit = Integer.parseInt(idahoMaxChildAgeLimitString);
			
			if((idahoMaxChildAgeLimit >= SerffConstants.MIN_STARTING_AGE_FOR_DENTAL_ADULT) && (idahoMaxChildAgeLimit < SerffConstants.STANDARD_MAX_AGE_FOR_DENTAL_CHILD)){
				isIdahoMaxChildAgeEnabled = true;
				LOGGER.info("Max age limit enabled for Idaho. Max age limit :" + idahoMaxChildAgeLimit);
			}else{
				LOGGER.error("Invalid configurationg for 'serff.state.dental.maxChildAgeLimit'. Max child age for Idaho state : " + idahoMaxChildAgeLimit);
			}
		}
		
		//Process plan rates.
		if(isIdahoMaxChildAgeEnabled){
			
			for(PlanRate rate : dentalPlanRates){
				if(rate.getMinAge() == 0 && rate.getMaxAge() == SerffConstants.STANDARD_MAX_AGE_FOR_DENTAL_CHILD){
					//Change age range of rate range 0-20 to 0 - idahoMaxChildAgeLimit.
					rate.setMaxAge(idahoMaxChildAgeLimit);
					LOGGER.debug("Child age range changed from 0-20  to " + rate.getMinAge() + "-" + rate.getMaxAge() + " Rate value : " + rate.getRate());
				}else if(rate.getMinAge() == SerffConstants.STANDARD_MIN_AGE_FOR_DENTAL_ADULT && rate.getMaxAge() == SerffConstants.STANDARD_MIN_AGE_FOR_DENTAL_ADULT){
					//Change age range of rate range 21-21 to (idahoMaxChildAgeLimit + 1) - 21.
					rate.setMinAge(idahoMaxChildAgeLimit + 1);
					LOGGER.debug("Age range changed from 21-21 to " + rate.getMinAge() + "-" + rate.getMaxAge() + " Rate value : " + rate.getRate());
				}
			}
			
		}
		
		LOGGER.info("preProcessDentalPlanRates() end.");
		
		return dentalPlanRates;
	}
	
	
	/**
	 * 
	 * @param prescriptionDrugTemplateVO
	 * @param errorCodes
	 * @param errorMessages
	 */
	public void validatePrescriptionDrugJobTemplate(PrescriptionDrugTemplateVO prescriptionDrugTemplateVO,
			List<String> errorCodes, List<String> errorMessages) {

		LOGGER.info("validatePrescriptionDrugJobTemplates() Start.");

		prescriptionDrugValidatorV5.validatePrescriptionDrug(prescriptionDrugTemplateVO, validationErrors);

		if (CollectionUtils.isEmpty(validationErrors)) {
			LOGGER.info("Valid prescription drug template.");
		}
		else {
			LOGGER.debug("Validation errors exist in Prescription and Drug template");
			addValidationErrorsToResponse(errorCodes, errorMessages);
		}
		validationErrors.clear();
		LOGGER.info("validatePrescriptionDrugJobTemplates() End.");
	}

	/**
	 * Method is used to get state code using state name.
	 */
	public String getStateCode(String stateName) {

		String stateCode = null;

		if (StringUtils.isBlank(stateName)) {
			return stateCode;
		}

		for (State state : new StateHelper().getAllStates()) {
			if (state.getName().equalsIgnoreCase(stateName)) {
				stateCode = state.getCode();
				break;
			}
		}
		return stateCode;
	}
	
	/**
	 * This method will save TenantPlan record for Plan and Tenant.
	 * 
	 * @param plan
	 * @param tenantList
	 * @param entityManager
	 * @return
	 */
	public void saveTenantPlan(com.getinsured.hix.model.Plan plan, List<Tenant> tenantList, EntityManager entityManager){
		
		if(CollectionUtils.isEmpty(tenantList) || null == plan || null == entityManager) {
			LOGGER.error("Recieved Invalid params while saving plan tenants");
			return;
		}
		
		for (Tenant tenant : tenantList) {
			if(null == tenant) {
				LOGGER.warn("Got null tenant for plan : " + plan.getId());
				continue;
			} 

			LOGGER.info("Saving tenant plan : " + plan.getId() + " , " + tenant.getCode());		
			//As Plan is created in case of update also. Always there will be new record in PM_TENANT_PLAN.
			//Save planid and tenantId in PM_TENANT_PLAN.
			TenantPlan tenantPlan = new TenantPlan();
			tenantPlan.setPlan(plan);
			tenantPlan.setTenant(tenant);
			Date currentTimeStamp = new Date();
			tenantPlan.setCreatedDate(currentTimeStamp);
			tenantPlan.setUpdatedDate(currentTimeStamp);
			
			if (entityManager.isOpen()) {
				entityManager.merge(tenantPlan);
			} else {
				LOGGER.error("Error Saving tenant plan: " + plan.getId() + " , " + tenant.getCode());		
				return;
			}
		}
	}

	public List<Tenant> getTenantList(String tenantIds) {
		List<Tenant> tenantList = null;
		
		if(StringUtils.isNotBlank(tenantIds)){
			List<Long> tenantIdList = new ArrayList<Long>();
			String[] tenantIdArr = tenantIds.split(",");
			for (String tenantIdStr : tenantIdArr) {
				if(StringUtils.isNumeric(tenantIdStr)) {
					tenantIdList.add(Long.parseLong(tenantIdStr));
				} else {
					LOGGER.warn("Invalid tenant ID passed " + SecurityUtil.sanitizeForLogging(tenantIdStr));
				}
			}
			
			if(!CollectionUtils.isEmpty(tenantIdList)) {
				tenantList = iTenantRepository.getTenantInList(tenantIdList);
			}
		}
		return tenantList;
	}

	/**
	   * Replaces all newlines in the given String 'sourceStr' with the replacement
	   * SPACE. Each line is trimmed from leading and trailing whitespaces,
	   * then the new line-delimiter is added.
	   *
	   * @param sourceStr the source string.
	   * @return the resulting string.
	   */
	public String removeNewLines(final String sourceStr) {

		if (StringUtils.isBlank(sourceStr)) {
			return null;
		}

		StringBuilder result = new StringBuilder();

		StringTokenizer t = new StringTokenizer(sourceStr, SerffConstants.NEW_LINE);
		while (t.hasMoreTokens()) {
			result.append(t.nextToken().trim()).append(SerffConstants.SPACE);
		}
		return result.toString().trim();
	}

	/**
	 * Method is used to generate Support Document Key using Document Name and HIOS Plan ID.
	 */
	public String generateSupportDocumentKey(final String originalDocumentName, String hiosIssuerIdWithState, SerffValidator serffValidator) {

		LOGGER.debug("generateSupportDocumentKey() Start");
		String documentKey = originalDocumentName;

		try {
			boolean hasDocumentWithPlanId = false;
			String issuerPlanNumber = null;
			final int lastIndexOfExtension = originalDocumentName.lastIndexOf(SerffConstants.DOT);

			// Validate Support Document extension should be PDF.
			if (-1 < lastIndexOfExtension
					&& !SerffConstants.PDF_EXTENSION.equalsIgnoreCase(originalDocumentName.substring(lastIndexOfExtension))) {
				LOGGER.debug("Support Document(SBC/EOC/BROCHURE) should be PDF only.");
			}
			else if (-1 < lastIndexOfExtension && SerffConstants.LEN_HIOS_PLAN_ID_WITH_VARIANT < originalDocumentName.length()) {

				// Validate Support Document Name with HIOS Plan ID with variant.
				issuerPlanNumber = originalDocumentName.substring(SerffConstants.PLAN_INITIAL_INDEX, SerffConstants.LEN_HIOS_PLAN_ID_WITH_VARIANT).toUpperCase();
				if (serffValidator.isValidHIOSPlanNumberWithVariant(issuerPlanNumber)) {
					hasDocumentWithPlanId = true;
				}
				else {
					// Validate Support Document Name with HIOS Plan ID without variant.
					issuerPlanNumber = originalDocumentName.substring(SerffConstants.PLAN_INITIAL_INDEX, SerffConstants.LEN_HIOS_PLAN_ID).toUpperCase();
					if (serffValidator.isValidHIOSPlanNumber(issuerPlanNumber)) {
						hasDocumentWithPlanId = true;
					}
				}

				if (hasDocumentWithPlanId) {
					// Validate Support Document Name start with Selected Issuer ID and State Code
					if (!issuerPlanNumber.startsWith(hiosIssuerIdWithState)) {
						hasDocumentWithPlanId = false;
						LOGGER.warn("Invalid HIOS Issuer ID or State code in Supporting Document Name: " + originalDocumentName);
					}
				}
			}

			// If Support Document is not start with HIOS Plan ID
			if (!hasDocumentWithPlanId) {
				LOGGER.debug("Document Name is not associated with valid HIOS Plan ID.");

				if (SerffConstants.DATA_BENEFITS.equalsIgnoreCase(originalDocumentName)) {
					documentKey = SerffConstants.DATA_BENEFITS;
				}
				// FIX for HIX-17152, DATA_DENTAL_BENEFITS will be sent as DATA_BENEFITS
				else if (SerffConstants.DATA_DENTAL_BENEFITS.equalsIgnoreCase(originalDocumentName)) {
					documentKey = SerffConstants.DATA_DENTAL_BENEFITS;
				}
				else if (SerffConstants.DATA_RATING_TABLE.equalsIgnoreCase(originalDocumentName)) {
					documentKey = SerffConstants.DATA_RATING_TABLE;
				}
			}
			// Else Support Document is start with HIOS Plan ID
			else {

				String documentNameToUpperCase = originalDocumentName.toUpperCase();
				String prefixSbcFile = issuerPlanNumber + SerffConstants.UNDERSCORE + SerffConstants.DATA_SBC;
				String prefixEocFile = issuerPlanNumber + SerffConstants.UNDERSCORE + SerffConstants.DATA_EOC;
				String prefixBrochureFile = issuerPlanNumber + SerffConstants.UNDERSCORE + SerffConstants.DATA_BROCHURE;

				// SBC document check
				if (documentNameToUpperCase.startsWith(prefixSbcFile)) {
					// e.g 12345CA1234567-00_SBC.pdf OR 12345CA1234567_SBC.pdf
					documentKey = prefixSbcFile + SerffConstants.PDF_EXTENSION;
				}
				// EOC document check
				else if (documentNameToUpperCase.startsWith(prefixEocFile)) {
					// e.g 12345CA1234567-00_EOC.pdf OR 12345CA1234567_EOC.pdf
					documentKey = prefixEocFile + SerffConstants.PDF_EXTENSION;
				}
				// BROCHURE document check
				else if (documentNameToUpperCase.startsWith(prefixBrochureFile)) {
					// e.g 12345CA1234567-00_BROCHURE.pdf OR 12345CA1234567_BROCHURE.pdf
					documentKey = prefixBrochureFile + SerffConstants.PDF_EXTENSION;
				}
			}
		}
		finally {

			if (LOGGER.isDebugEnabled()) { 
				LOGGER.debug("generateSupportDocumentKey() End with Supporting Document Key: " + documentKey + ", And Supporting Document Name: " + originalDocumentName);
			}
		}
		return documentKey;
	}
}
