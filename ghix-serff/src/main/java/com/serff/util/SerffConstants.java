/**
 *
 */
package com.serff.util;

import java.io.File;
import java.net.InetAddress;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * This class contains constants used for SERFF services
 * 
 * @author Nikhil Talreja
 * @since 08 March, 2013
 */
@Component
public final class SerffConstants {

	// SERFF Client Exception error codes : Series 100X	
	public static final String NO_ERROR_CODE = "0";
	public static final String EMSG_PM_RESPONSE = "No response from Plan Management service.";
	public static final String EMSG_ECM_RESPONSE = "Failed to put document to ECM.";
	public static final String UNEXPECTED_GENERAL_ERROR_CODE = "1000";
	public static final String UNEXPECTED_GENERAL_ERROR_MESSAGE = "Unexpected general error.";
	public static final String UNEXPECTED_GENERAL_ERROR_MESSAGE_GENERATE_RESPONSE = "Exception occurred while generating response or by application failure.";
	public static final String UNEXPECTED_GENERAL_ERROR_MESSAGE_NULL_POINTER = "Code throws NULL Pointer exception.";
	public static final String UNEXPECTED_GENERAL_ERROR_MESSAGE_DB_TRANSACTION = "Exception occurred while committing transactions to database.";	
	public static final String UNEXPECTED_GENERAL_ERROR_MESSAGE_IO_ERROR = "I/O error in reading/writing data.";
	public static final String UNEXPECTED_GENERAL_ERROR_MESSAGE_XML_PARSING_ERROR = "Error while parsing request templates.";
	public static final String REQUIRED_FIELD_MISSING_CODE = "1001";
	public static final String REQUIRED_FIELD_MISSING_MESSAGE = "Required fields missing.";
	public static final String REQUIRED_FIELD_MISSING_MESSAGE_XML_SCHEMA = "Required data elements missing in the xml schema.";	
	public static final String INVALID_PARAMETER_CODE = "1002";
	public static final String INVALID_PARAMETER_MESSAGE = "Invalid parameter.";
	public static final String INVALID_PARAMETER_MESSAGE_ISSUER_NOT_SETUP = "Issuer is not set up correctly.";
	public static final String INVALID_PARAMETER_MESSAGE_PARSING_REQUEST = "Error while parsing request templates.";
	public static final String INVALID_PARAMETER_MESSAGE_INVALID_CHARACTERS = "Required/Essential fields have Invalid characters.";
	public static final String INVALID_PARAMETER_MESSAGE_DATA_TYPE_MISMATCH = "Data type mismatch.";
	public static final String INVALID_PARAMETER_MESSAGE_DATA_FORMAT_INVALID = "Data not in valid format.";
	public static final String INVALID_PARAMETER_MESSAGE_DATA_VALIDATION_ERROR = "Other data validation error.";
	public static final String NO_OBJECT_FOUND_CODE = "1003";
	public static final String NO_OBJECT_FOUND_MESSAGE = "No objects were found.";
	public static final String NO_OBJECT_FOUND_MESSAGE_INVALID_PLAN = "Plan does not contain any valid on-exchange cost share variances.";
	public static final String NO_OBJECT_FOUND_MESSAGE_RATES_MISSING = "Rates missing.";
	public static final String NO_OBJECT_FOUND_MESSAGE_SERVICE_AREA_MISSING = "Provider service area missing.";	
	public static final String NO_DATA_CODE = "1004";
	public static final String NO_DATA_MESSAGE = "The fields contain no data.";
	public static final String NO_DATA_MESSAGE_EMPTY_DATA = "Required/Essential fields have empty, null, or blank values.";
	public static final String INVALID_OPERATION_CODE = "1005";
	public static final String INVALID_OPERATION_MESSAGE = "The operation is not implemented and should not be called.";
	public static final int PLAN_LOADED_WITH_WARNING_CODE = 1006;
	public static final String PLAN_CERTIFIED_MESSAGE_NO_FUTURE_RATES = "Update failed because plan status is Certified and no future rates were found.";
	public static final String PLAN_CERTIFIED_MESSAGE_RATES_NOT_MODIFIED = "Update failed because plan status is Certified and rates are not modified.";
	public static final String INVALID_OPERATION_MESSAGE_INVALID_USER_DETAILS = "Username/password is incorrect.";	
	public static final String INVALID_OPERATION_MESSAGE_SCHEDULED_DOWNTIME = "Scheduled downtime.";
	public static final String INVALID_OPERATION_MESSAGE_SERVICE_INACTIVE = "Service is in inactive state.";	
	public static final String PROCESS_ALREADY_RUNNING_MESSAGE_TRANSFER_PLAN_RUNNING = "Transfer Plan is called again before finishing first process.";	
	
	//Server error codes : series 200X
	public static final String UNEXPECTED_GENERAL_SERVER_ERROR_CODE = "2000";
	public static final String UNEXPECTED_GENERAL_SERVER_ERROR_MESSAGE = "Unexpected general error.";
	
	public static final String DB_EXCEPTION_CODE = "2001";
	public static final String DB_EXCEPTION_MESSAGE = "Internal database error";
	
	public static final String APP_EXCEPTION_CODE = "2002";
	public static final String APP_EXCEPTION_MESSAGE = "Internal application error.";
	public static final String APP_EXCEPTION_MESSAGE_FAILED_TO_PUT_DOCUMENT_TO_ECM_MESSAGE = "Failed to put document to ECM.";
	public static final String APP_EXCEPTION_MESSAGE_FAILED_TO_PUT_DOCUMENT_TO_CDN_MESSAGE = "Failed to put document to CDN.";
	
	public static final String IO_ERROR_CODE = "2003";
	public static final String IO_ERROR_MESSAGE = "IO error occurred.";
	public static final String IO_ERROR_MESSAGE_PROCESS_ALREADY_RUNNING = "Unable to process request at this time. Please retry after some time.";
	public static final String IO_ERROR_MESSAGE_FTP_ERROR_IN_READING_DATA_MESSAGE = "Error occurred in reading or writing ftp data files.";
	
	public static final String NPE_EXCEPTION_CODE = "2004";
	public static final String NPE_EXCEPTION_MESSAGE = "Null pointer exception.";
	public static final String TEMPLATES_NOT_MODIFIED_MESSAGE = "Data from following templates is not updated because there is no change in templates since last load :";

	public static final String EXCEPTION_ERROR_CODE = "9999";
	public static final String RUNTIME_ERROR_CODE = "9000";

	public static final String BENEFIT_COVERED = "Covered";
	public static final String BENEFIT_NOT_COVERED = "Not Covered";
	public static final String INVALID_REQUEST = "Invalid request";
	public static final String VALIDATION_ERROR = "Validation Errors";
	public static final String ISSUER_ID_MISMATCH = "Issuer ID Mismatch";
	public static final String LOADING_ERROR = "Loading Error";
	public static final String NOUPDATE_ERROR = "No Plan Update";
	public static final String MISSING_TEMPLATE_ERROR = "Missing Template";
	public static final String PLANS_LOADING_MESSAGE = "Plan Loading in progress";
	public static final String PLANS_LOADED_MESSAGE = "Plans Loaded.";
	public static final String VALID_REQUEST = "Valid request";
	public static final String CSR_REQUEST = "CSR request. Processing file : ";
	public static final String NULL_REQUEST_OR_PLAN_OBJECT = "Request/Plan object was null";

	public static final String AT_THE_RATE = "@";
	public static final int PROCESS_IP_LENGTH = 40;
	public static final String PHIX_PROFILE = "PHIX";
	public static final String COMMA = ",";
	public static final String DECIMAL_POINT = "\\.";
	public static final String DECIMAL_POINT_AND_ANYTHING_ELSE = "\\..*";
	public static final String DOLLAR = "\\$";
	public static final String PERCENTAGE = "%";
	public static final String SPACE = " ";
	public static final String DOLLARSIGN = "$";
	public static final String UNDERSCORE = "_";
	public static final String HYPHEN = "-";
	public static final String DOT = ".";
	public static final String NEW_LINE = "\n";
	public static final String BREAK_LINE = "<br>";
	public static final String PATH_SEPERATOR = "/";
	public static final String SPECIAL_CHARS_REGEX = "[^a-zA-Z0-9]";
	public static final String UNSECURE_CHARS_REGEX = "[\n\r]";
	public static final String MALE = "M";
	public static final String FEMALE = "F";
	public static final String ASTERISK = "*";
	public static final int INITIAL_INDEX = 0;
	public static final int NEGATIVE_INDEX_OF = -1;
	public static final String SINGLE_QUOTE = "'";
	public static final String TENANT_GINS = "GINS";

	// Need to decide base path to store files on ECM
	public static final String SERF_ECM_BASE_PATH = "serff/docs/";
	public static final String SERF_ECM_BROCHURE_BASE_PATH = "serff/brochure/";
	public static final String SERF_ECM_ANCILLARY_BASE_PATH = "serff/ancillary/";
	public static final int PLAN_INITIAL_INDEX = 0;
	public static final int LEN_HIOS_PRODUCT_ID = 10;				// HIOS Issuer ID + State Code + Product ID [e.g 12345CA123]
	public static final int LEN_HIOS_PLAN_ID = 14;					// HIOS Issuer ID + State Code + Plan Number [e.g 12345CA1234567]
	public static final int LEN_HIOS_PLAN_ID_WITH_VARIANT = 17;		// HIOS Issuer ID + State Code + Plan Number + Variant [e.g 12345CA1234567-00]
	public static final int LEN_HIOS_ISSUER_ID = 5;					// HIOS Issuer ID [e.g 12345]
	public static final int LEN_HIOS_ISSUER_ID_WITH_STATE = 7;		// HIOS Issuer ID + State Code [e.g 12345CA]
	public static final int LEN_ISSUER_STATE = 2;					// State Code [e.g CA]
	public static final int LEN_COUNTY_FIPS = 3;					// County Fips [e.g 001]
	public static final int LEN_STATE_FIPS = 2;						// State Fips [e.g 01]
	public static final String REQUEST_MODE_API = "API";
	public static final String REQUEST_MODE = "requestMode";
	public static final String LOCALHOST_IP = InetAddress.getLoopbackAddress().getHostAddress();
	public static final int DEFAULT_HTTPS_PORT = 443;
	public static final String SERFF_CSR_PASSKEY = "SERFF_CSR_passkey";
    public static final String SERFF_CSR_PASSKEY_VALUE = "serff94303";
    public static final int ORC_COLUMN_MAX_LEN = 4000;
	public static final String POSTGRES = "POSTGRESQL";
	public static final String PDF_EXTENSION = ".pdf";

	public static final int POS_BROCHURE_ECM = 0;
	public static final int POS_BROCHURE_FILE = 1;
	public static final int POS_SBC_ECM = 2;
	public static final int POS_SBC_FILE = 3;
	public static final int POS_PLAN_ISSUER_NUM = 4;
	public static final int POS_APPLICABLE_YEAR = 5;
	public static final int POS_EOC_ECM = 6;
	public static final int POS_EOC_FILE = 7;
	
	public static final int MIN_STARTING_AGE_FOR_DENTAL_ADULT = 15;
	public static final int STANDARD_MAX_AGE_FOR_DENTAL_CHILD = 20;
	public static final int STANDARD_MIN_AGE_FOR_DENTAL_ADULT = 21;

	/**
	 * Need to merge SERFF_REQ_ID with value of FTP path.
	 */
    public static final String DISK_SERFF_FILES_PATH = System.getProperty("GHIX_HOME") + File.separatorChar + "ghix-docs";
    public static final String FTP_CSR_INPROCESS_PATH = "inprocess/ID_";
	public static final String FTP_CSR_ERRORS_PATH = "error/ID_";
	public static final String FTP_CSR_SUCCESS_PATH = "success/ID_";
	public static final String FTP_RESULT = "/Result.txt";
	public static final String FTP_UPLOAD_PLAN_PATH = File.separatorChar + "uploadPlans" + File.separatorChar;
	public static final long FTP_UPLOAD_MAX_SIZE = 314572800;
	public static final Long COMPANY_LOGO_FILE_SIZE = 512000L;
	public static final long BYTE_1MB_SIZE = 1048576;
	public static final int BUGGER_BYTE_SIZE = 1024;
	/*public static final int FILE_LIST_BY4 = 4;
	public static final int FILE_LIST_BY5 = 5;
	public static final int FILE_LIST_BY6 = 6;*/
	public static final int FILE_LIST_MIN_LENGTH = 4;
	public static final int FILE_LIST_MAX_LENGTH = 9;
	
	public static final int APPLICABLE_YEAR_INDEX = 2;
	public static final int PRESCRIPTION_DRUG_TEMPLATES_COUNT = 2;

	public static enum PUF_STATUS {
		IN_PROGRESS, COMPLETED, FAILED;
	}

	public static final String CDN_ISSUER_LOGO_PATH = "issuers/logo/";
	public static final String CDN_GLOBAL_URL = "global.CdnUrl";
	public static final String FTP_BROCHURE_PATH = "dropbox";
	public static final String FTP_BROCHURE_SUCCESS_PATH = "success/ID_";
	public static final String FTP_BROCHURE_ERROR_PATH = "error/ID_";
	public static final String FTP_UPLOAD_STM_PATH = "/uploadPlansSTM/";
	public static final String FTP_UPLOAD_AME_PATH = "/uploadPlansAME/";
	public static final String FTP_UPLOAD_HCC_PATH = "/uploadPlansHCC/";
	public static final String FTP_UPLOAD_PLAN_VISION_PATH = "/uploadPlansVision/";
	public static final String FTP_UPLOAD_PLAN_LIFE_PATH = "/uploadPlansLife/";
	public static final String FTP_UPLOAD_PLAN_DOCS_PATH = "/uploadPlansDocs/";
	public static final String FTP_UPLOAD_PLAN_MEDICAID_PATH = "/uploadMedicaidDocs/";
	public static final String FTP_UPLOAD_PLAN_MEDICARE_PATH = "/uploadMedicareDocs/";
	public static final String FTP_UPLOAD_ISSUER_LOGO_PATH ="/uploadLogos";
	public static final String FTP_UPLOAD_PLAN_BROCHURE_PATH ="/uploadBrochures";
	public static final String FTP_SUCCESS = "success";
	public static final String FTP_ERROR = "error";
	public static final String FTP_STM_FILE = "_STM_";
	public static final String FTP_ANCILLARY_FILEID_SUFFIX = "_ID";
	public static final String FTP_AME_FILE_PREFIX = "_AME_";
	public static final String FTP_AME_FAMILY_FILE = "_AME_FAMILY_";
	public static final String FTP_AME_GENDER_FILE = "_AME_GENDER_";
	public static final String FTP_AME_AGE_FILE = "_AME_AGE_";
	public static final String FTP_HCC_FILE = "_HCC_";
	public static final String FTP_LOGO_FILE = "_LOGO_";
	public static final String FTP_BROCHURE_FILE = "_BROCHURE_";
	public static final String FTP_AREA_FACTOR_FILE = "_AREA_FACTOR_";
	public static final String FTP_PLAN_DOCS_FILE = "_PLAN_DOCS_";
	public static final String FTP_PLAN_MEDICAID_FILE = "_MEDICAID_";
	public static final String FTP_PLAN_MEDICARE_FILE = "_MEDICARE_";
	public static final String FTP_VISION_FILE = "_VISION_";
	public static final String FTP_LIFE_FILE = "_LIFE_";
	public static final String FTP_DRUG_LIST_DIFF_REPORT = "_DRUG_LIST_DIFF_REPORT_";
	
	public static enum PLAN_TYPE {
		STM, AME_FAMILY, HCC, AME_AGE, PLAN_DOCS, PLAN_MEDICAID, VISION, AME_GENDER, AREA_FACTOR, LIFE;
	}
	
	public static enum AME_PLAN_TYPE {
		AME_FAMILY, AME_AGE, AME_GENDER;
	}
	
	public static final String TRANSFER_PLAN = "TRANSFER_PLAN";
	public static final String TRANSFER_PRESCRIPTION_DRUG = "TRANSFER_PRESCRIPTION_DRUG";
	public static final String TRANSFER_DRUGS_JSON = "TRANSFER_DRUGS_JSON";
	public static final String TRANSFER_DRUGS_XML = "TRANSFER_DRUGS_XML";
	public static final String TRANSFER_STM = "TRANSFER_STM";
	public static final String TRANSFER_AME_FAMILY = "TRANSFER_AME";
	public static final String TRANSFER_AME_AGE = "TRANSFER_AME_AGE";
	public static final String TRANSFER_AME_GENDER = "TRANSFER_AME_GENDER";
	public static final String TRANSFER_HCC = "TRANSFER_HCC";
	public static final String TRANSFER_AREA_FACTOR = "TRANSFER_AREA_FACTOR";
	public static final String TRANSFER_PLAN_DOCS = "TRANSFER_PLAN_DOCS";
	public static final String TRANSFER_PLAN_MEDICAID = "TRANSFER_PLAN_MEDICAID";
	public static final String TRANSFER_VISION = "TRANSFER_VISION";
	public static final String TRANSFER_LIFE = "TRANSFER_LIFE";
	public static final String TRANSFER_MEDICARE_AD = "TRANSFER_MEDICARE_AD";
	public static final String TRANSFER_MEDICARE_SP = "TRANSFER_MEDICARE_SP";
	public static final String TRANSFER_MEDICARE_RX = "TRANSFER_MEDICARE_RX";
	public static final String TRANSFER_ISSUER_LOGO = "TRANSFER_ISSUER_LOGO";

	public static final String PUF_DATASOURCE = "PUF";
	public static final String RETRIEVE_PLAN_STATUS = "RETRIEVE_PLAN_STATUS";
	public static final String UPDATE_PLAN_STATUS = "UPDATE_PLAN_STATUS";
	public static final String VALIDATE_AND_TRANSFORM_DATA_TEMPLATE = "VALIDATE_AND_TRANSFORM_DATA_TEMPLATE";
	public static final String CSR = "CSR";
	public static final String SUCCESS = "success";
	public static final String PM_STATUS = "Status from PM module will be incorporated here";
	public static final String FAILURE = "Failure";

	// Template types
	public static final String DATA_ADMIN_DATA = "DATA_ADMIN_DATA";
	public static final String DATA_ECP_DATA = "DATA_ECP_DATA";
	public static final String DATA_BENEFITS = "DATA_BENEFITS";
	public static final String DATA_PRESCRIPTION_DRUG = "DATA_PRESCRIPTION_DRUG";
	public static final String DATA_SERVICE_AREA = "DATA_SERVICE_AREA";
	public static final String DATA_RATING_TABLE = "DATA_RATING_TABLE";
	public static final String DATA_RATING_RULES = "DATA_RATING_RULES";
	public static final String DATA_NETWORK_ID = "DATA_NETWORK_ID";
	public static final String DATA_BUSINESS_RULE ="DATA_BUSINESS_RULE";
	public static final String DATA_UNIFIED_RATE ="DATA_UNIFIED_RATE";
	public static final String DATA_DENTAL_BENEFITS = "DATA_DENTAL_BENEFITS";
	public static final String DATA_SBC = "SBC";
	public static final String DATA_EOC = "EOC";
	public static final String DATA_BROCHURE = "BROCHURE";
	public static final String DATA_NCQA_DATA = "DATA_NCQA_DATA";
	public static final String DATA_URAC_DATA = "DATA_URAC_DATA";
	public static final String SUPPORTING_DOC = "SUPPORTING_DOC";
	public static final String PRESCRIPTION_DRUG_FILENAME_SUFFIX = "drug.xml";
	public static final String NETWORK_ID_FILENAME_SUFFIX = "network.xml";
	public static final String BENEFITS_FILENAME_SUFFIX = "plans.xml";
	public static final String RATING_TABLE_FILENAME_SUFFIX = "rate.xml";
	public static final String SERVICE_AREA_FILENAME_SUFFIX = "servicearea.xml";
	public static final String BUSINESS_RULE_FILENAME_SUFFIX = "businessrule.xml";
	public static final String UNIFIED_RATE_FILENAME_SUFFIX = "urrt.xml";
	public static final String URAC_FILENAME_SUFFIX = "urac.xml";
	public static final String NCQA_FILENAME_SUFFIX = "ncqa.xml";
	public static final String POSTFIX_SBC = SerffConstants.UNDERSCORE + SerffConstants.DATA_SBC + SerffConstants.PDF_EXTENSION;
	public static final String POSTFIX_EOC = SerffConstants.UNDERSCORE + SerffConstants.DATA_EOC + SerffConstants.PDF_EXTENSION;
	public static final String POSTFIX_BROCHURE = SerffConstants.UNDERSCORE + SerffConstants.DATA_BROCHURE + SerffConstants.PDF_EXTENSION;

	public static final int DEFAULT_USER_ID = 1;
	public static final String HIOS_ISSUER_ID = "HIOS_ISSUER_ID";
	public static final String SERVICE_AREA_ID = "SERVICE_AREA_ID";
	public static final String TOBACCO = "Tobacco User";
	public static final String NON_TOBACCO = "Non-Tobacco User";
	public static final String TOBACCO_AND_NON_TOBACCO = "tobacco user/non-tobacco user";
	public static final String COMPLETED = "COMPLETE";
	public static final String PARTIAL = "PARTIAL";
	public static final String YES = "YES";
	public static final String NO = "NO";
	public static final String YES_ABBR = "Y";
	public static final String NO_ABBR = "N";
	public static final String RATES = "RATES";
	public static final String SERVICE_AREAS = "SERVICE_AREAS";
	public static final String NETWORKS = "NETWORKS";

	public static final String DEFAULT_PHIX_STATE_CODE = "XX";
	public static final String IDAHO_STATE_CODE = "ID";
	public static final String CA_STATE_CODE = "CA";
	public static final String CONNECTICUT_STATE_CODE = "CT";
	public static final String OFF_EXCHANGE_SUFFIX = "-00";
	public static final String OFF_EXCHANGE_VARIANT_SUFFIX = "00";
	public static final String NOT_APPLICABLE = "N/A";

	public static final String DECERTIFIED = "DECERTIFIED";
	
	// Plan & Benefit Template values
	public static final String ALLOWS_ADULT_AND_CHILD = "allows adult and child-only";
	public static final String ALLOWS_ADULT_ONLY = "allows adult-only";
	public static final String ALLOWS_CHILD_ONLY = "allows child-only";

	// Plan Management REST services URLs
	public static final String CONTENT_TYPE = "application/json";
	public static final String SERFF_APP_ID = "SERFF";
	public static final String HEALTH = "HEALTH";
	public static final String DENTAL = "DENTAL";

	// LABEL for SERFF_FAMILY_OPTION
	public static final String SERFF_FAMILY_OPTION_VALUE = "SERFF_FAMILY_OPTION";
	public static final String FAMILY_OPTION_PRIMARY_ENROLLEE = "Individual Rate";
	public static final String FAMILY_OPTION_COUPLE_ENROLLEE = "Couple";
	public static final String FAMILY_OPTION_PRIMARY_ENROLLEE_ONE_DEPENDENT = "Primary Subscriber and One Dependent";
	public static final String FAMILY_OPTION_PRIMARY_ENROLLEE_TWO_DEPENDENT = "Primary Subscriber and Two Dependents";
	public static final String FAMILY_OPTION_PRIMARY_ENROLLEE_MANY_DEPENDENT = "Primary Subscriber and Three or More Dependents";
	public static final String FAMILY_OPTION_COUPLE_ENROLLEE_ONE_DEPENDENT = "Couple and One Dependent";
	public static final String FAMILY_OPTION_COUPLE_ENROLLEE_TWO_DEPENDENT = "Couple and Two Dependents";
	public static final String FAMILY_OPTION_COUPLE_ENROLLEE_MANY_DEPENDENT = "Couple and Three or More Dependents";
	public static final int FAMILY_OPTION_PRIMARY_ENROLLEE_CODE = 1008;
	public static final int FAMILY_OPTION_COUPLE_ENROLLEE_CODE = 1001;
	public static final int FAMILY_OPTION_PRIMARY_ENROLLEE_ONE_DEPENDENT_CODE = 1002;
	public static final int FAMILY_OPTION_PRIMARY_ENROLLEE_TWO_DEPENDENT_CODE = 1003;
	public static final int FAMILY_OPTION_PRIMARY_ENROLLEE_MANY_DEPENDENT_CODE = 1004;
	public static final int FAMILY_OPTION_COUPLE_ENROLLEE_ONE_DEPENDENT_CODE = 1005;
	public static final int FAMILY_OPTION_COUPLE_ENROLLEE_TWO_DEPENDENT_CODE = 1006;
	public static final int FAMILY_OPTION_COUPLE_ENROLLEE_MANY_DEPENDENT_CODE = 1007;
	public static final String HTTP_STRING = "http://";
	public static final String HTTPS_STRING = "https://";
	
	public static final String DRUG_LIST_REC_ADDED = "ADD";
	public static final String DRUG_LIST_REC_REMOVED = "REMOVED";
	public static final String DRUG_LIST_REC_MODIFIED = "MODIFIED";
	
	public static final int DRUG_LIST_OLD_RX_NAME = 0;
	public static final int DRUG_LIST_OLD_RX_FULL_NAME = 1;
	public static final int DRUG_LIST_OLD_RX_STRENGTH = 2;
	public static final int DRUG_LIST_OLD_RXCUI = 3;
	public static final int DRUG_LIST_OLD_AUTH_REQUIRED = 4;
	public static final int DRUG_LIST_OLD_STEP_THERAPY_REQUIRED = 5; 
	public static final int DRUG_LIST_OLD_DRUG_TIER_TYPE1 = 6;
	public static final int DRUG_LIST_OLD_DRUG_TIER_TYPE2 = 7;
	//public static final int DRUG_LIST_NEW_RX_NAME = 7;
	//public static final int DRUG_LIST_NEW_RX_STRENGTH = 8;
	public static final int DRUG_LIST_NEW_RXCUI = 8;
	public static final int DRUG_LIST_NEW_AUTH_REQUIRED = 9;
	public static final int DRUG_LIST_NEW_STEP_THERAPY_REQUIRED = 10;
	public static final int DRUG_LIST_NEW_DRUG_TIER_TYPE1 = 11;
	public static final int DRUG_LIST_NEW_DRUG_TIER_TYPE2  = 12;
	public static final int DRUG_QUERY_COL_COUNT  = 10;

	
	private static String serverIP;
	private static int serverMonitorPort;
	private static String serffMonitorUser;
	private static String serffMonitorPassword;
	private static String serffCsrFTPhost;
	private static String serffCsrFTPUser;
	private static String serffCsrFTPpassword;
	private static String serffCsrFilePollingURI;
	private static String serffCsrIsPollingEnabled;
	private static String serffCsrHomePath;
	private static String serffEcmUserName;
	private static String serffEcmPassword;
	private static String serffEcmAtompuburl;
	private static String serffEcmType;
	private static String serffEcmBasepath;
	private static String serfEcmStaticContentPath;
	private static String ecmServiceWSDL;
	private static String serfEcmBypassEnabled;
	private static String serffDiskLogEnabled;
	private static String serffEnablePufLoad;
	private static String serviceServerList;
	private static String serviceServerHTTPSPort;
	private static String ftpServerUploadSubpath;
	private static String serffBatchWSCallTimeout;
	private static String serffBatchServerIdleTime;
	private static String databaseType;

	public static String getServerIP() {
		return serverIP;
	}

	@Value("#{configProp['serverIP']}")
	public void setServerIP(String serverIP) {
		setStaticServerIP(serverIP);
	}

	private static void setStaticServerIP(String serverIP) {
		SerffConstants.serverIP = serverIP;
	}

	public static int getServerMonitorPort() {
		return serverMonitorPort;
	}

	@Value("#{configProp['serverMonitorPort']}")
	public void setServerMonitorPort(String serverMonitorPort) {
		
		if (StringUtils.isNotBlank(serverMonitorPort) && StringUtils.isNumeric(serverMonitorPort.trim())) {
			setStaticServerMonitorPort(Integer.valueOf(serverMonitorPort));
		}
	}

	private static void setStaticServerMonitorPort(int serverMonitorPort) {
		SerffConstants.serverMonitorPort = serverMonitorPort;
	}

	public static String getSerffMonitorUser() {
		return serffMonitorUser;
	}

	@Value("#{configProp['serffMonitorUser']}")
	public void setSerffMonitorUser(String serffMonitorUser) {
		setStaticSerffMonitorUser(serffMonitorUser);
	}

	private static void setStaticSerffMonitorUser(String serffMonitorUser) {
		SerffConstants.serffMonitorUser = serffMonitorUser;
	}

	public static String getSerffMonitorPassword() {
		return serffMonitorPassword;
	}

	@Value("#{configProp['serffMonitorPassword']}")
	public void setSerffMonitorPassword(String serffMonitorPassword) {
		setStaticSerffMonitorPassword(serffMonitorPassword);
	}

	private static void setStaticSerffMonitorPassword(String serffMonitorPassword) {
		SerffConstants.serffMonitorPassword = serffMonitorPassword;
	}

	@Value("#{configProp.ecmServiceWSDL}")
	public void setEcmServiceWSDL(String ecmServiceWSDL) {
		setStaticEcmServiceWSDL(ecmServiceWSDL);
	}

	private static void setStaticEcmServiceWSDL(String ecmServiceWSDL) {
		SerffConstants.ecmServiceWSDL = ecmServiceWSDL;
	}

	public static String getEcmServiceWSDL() {
		return ecmServiceWSDL;
	}

	@Value("#{configProp.SERFF_CSR_ftpHost}")
	public void setSerffCsrFtpHost(String serfCsrftpHost) {
		setStaticSerffCsrFtpHost(serfCsrftpHost);
	}

	private static void setStaticSerffCsrFtpHost(String serfCsrftpHost) {
		SerffConstants.serffCsrFTPhost = serfCsrftpHost;
	}

	public static String getSerffCsrFtpHost() {
		return serffCsrFTPhost;
	}

	@Value("#{configProp.SERFF_CSR_ftpUser}")
	public void setSerffCsrFtpUser(String serfCsrFtpUser) {
		setStaticSerffCsrFtpUser(serfCsrFtpUser);
	}

	private static void setStaticSerffCsrFtpUser(String serfCsrFtpUser) {
		SerffConstants.serffCsrFTPUser = serfCsrFtpUser;
	}

	public static String getSerffCsrFtpUser() {
		return serffCsrFTPUser;
	}

	@Value("#{configProp.SERFF_CSR_ftpPassword}")
	public void setSerffCsrFtpPassword(String serfCsrFtpPassword) {
		setStaticSerffCsrFtpPassword(serfCsrFtpPassword);
	}

	private static void setStaticSerffCsrFtpPassword(String serfCsrFtpPassword) {
		SerffConstants.serffCsrFTPpassword = serfCsrFtpPassword;
	}

	public static String getSerffCsrFtpPassword() {
		return serffCsrFTPpassword;
	}

	@Value("#{configProp.SERFF_CSR_File_Polling_URI}")
	public void setSerffCsrFilePollingURI(String serfCsrFilePollingURI) {
		setStaticSerffCsrFilePollingURI(serfCsrFilePollingURI);
	}

	private static void setStaticSerffCsrFilePollingURI(String serfCsrFilePollingURI) {
		SerffConstants.serffCsrFilePollingURI = serfCsrFilePollingURI;
	}

	public static String getSerffCsrFilePollingURI() {
		return serffCsrFilePollingURI;
	}

	@Value("#{configProp.SERFF_CSR_isPollingEnabled}")
	public void setSerffCsrIsPolling(String serfCsrisPollingEnabled) {
		setStaticSerffCsrIsPolling(serfCsrisPollingEnabled);
	}

	private static void setStaticSerffCsrIsPolling(String serfCsrisPollingEnabled) {
		SerffConstants.serffCsrIsPollingEnabled = serfCsrisPollingEnabled;
	}

	public static String getSerffCsrIsPolling() {
		return serffCsrIsPollingEnabled;
	}

	@Value("#{configProp.SERFF_CSR_homePath}")
	public void setSerffCsrHomePath(String serfCsrHomePath) {
		setStaticSerffCsrHomePath(serfCsrHomePath);
	}

	private static void setStaticSerffCsrHomePath(String serfCsrHomePath) {
		SerffConstants.serffCsrHomePath = serfCsrHomePath;
	}

	public static String getSerffCsrHomePath() {
		return serffCsrHomePath;
	}

	@Value("#{configProp['ecm.username']}")
	public void setSerffECMUserName(String serfEcmUsername) {
		setStaticSerffECMUserName(serfEcmUsername);
	}

	private static void setStaticSerffECMUserName(String serfEcmUsername) {
		SerffConstants.serffEcmUserName = serfEcmUsername;
	}

	public static String getSerffECMUserName() {
		return serffEcmUserName;
	}

	@Value("#{configProp['ecm.password']}")
	public void setSerffECMPassword(String serfEcmPassword) {
		setStaticSerffECMPassword(serfEcmPassword);
	}

	private static void setStaticSerffECMPassword(String serfEcmPassword) {
		SerffConstants.serffEcmPassword = serfEcmPassword;
	}

	public static String getSerffECMPassword() {
		return serffEcmPassword;
	}

	@Value("#{configProp['ecm.atompuburl']}")
	public void setSerfEcmAtompuburl(String serfEcmatomPubUrl) {
		setStaticSerfEcmAtompuburl(serfEcmatomPubUrl);
	}

	private static void setStaticSerfEcmAtompuburl(String serfEcmatomPubUrl) {
		SerffConstants.serffEcmAtompuburl = serfEcmatomPubUrl;
	}

	public static String getSerfEcmAtompuburl() {
		return serffEcmAtompuburl;
	}

	@Value("#{configProp['ecm.type']}")
	public void setSerfEcmType(String serfEcmType) {
		setStaticSerfEcmType(serfEcmType);
	}

	private static void setStaticSerfEcmType(String serfEcmType) {
		SerffConstants.serffEcmType = serfEcmType;
	}

	public static String getSerfEcmType() {
		return serffEcmType;
	}

	@Value("#{configProp['ecm.basepath']}")
	public void setSerfEcmBasePath(String serfEcmBasePath) {
		setStaticSerfEcmBasePath(serfEcmBasePath);
	}

	private static void setStaticSerfEcmBasePath(String serfEcmBasePath) {
		SerffConstants.serffEcmBasepath = serfEcmBasePath;
	}

	public static String getSerfEcmBasePath() {
		return serffEcmBasepath;
	}

	@Value("#{configProp['ecm.staticcontentpath']}")
	public void setSerfEcmStaticContentPath(String serfEcmStaticContentPath) {
		setStaticSerfEcmStaticContentPath(serfEcmStaticContentPath);
	}

	private static void setStaticSerfEcmStaticContentPath(String serfEcmStaticContentPath) {
		SerffConstants.serfEcmStaticContentPath = serfEcmStaticContentPath;
	}

	public static String getSerfEcmStaticContentPath() {
		return serfEcmStaticContentPath;
	}

	@Value("#{configProp['ecm.bypass_enabled']}")
	public void setSerffECMBypassEnabled(String serfEcmBypassEnabled) {
		setStaticSerffECMBypassEnabled(serfEcmBypassEnabled);
	}

	private static void setStaticSerffECMBypassEnabled(String serfEcmBypassEnabled) {
		SerffConstants.serfEcmBypassEnabled = serfEcmBypassEnabled;
	}

	public static String getSerffECMBypassEnabled() {
		return serfEcmBypassEnabled;
	}

	@Value("#{configProp['serff.disklogging.enabled']}")
	public void setSerffDiskLogEnabled(String serffDiskLogEnabled) {
		setStaticSerffDiskLogEnabled(serffDiskLogEnabled);
	}

	private static void setStaticSerffDiskLogEnabled(String serffDiskLogEnabled) {
		SerffConstants.serffDiskLogEnabled = serffDiskLogEnabled;
	}

	public static String getSerffDiskLogEnabled() {
		if (StringUtils.isNotBlank(serffDiskLogEnabled)) {
			return serffDiskLogEnabled;
		} else {
			return Boolean.TRUE.toString();
		}
	}
	
	@Value("#{configProp['Serff_Enable_PUF_Load']}")
	public void setSerffPUFLoadEnabled(String serffEnablePufLoad) {
		setStaticSerffPUFLoadEnabled(serffEnablePufLoad);
	}

	private static void setStaticSerffPUFLoadEnabled(String serffEnablePufLoad) {
		SerffConstants.serffEnablePufLoad = serffEnablePufLoad;
	}

	public static String getSerffPUFLoadEnabled() {
		return serffEnablePufLoad;
	}

	@Value("#{configProp['SERFF_ServiceServerList']}")
	public void setServiceServerList(String serverList) {
		setStaticServiceServerList(serverList);
	}

	private static void setStaticServiceServerList(String serverList) {
		SerffConstants.serviceServerList = serverList;
	}

	public static String getServiceServerList() {
		return serviceServerList;
	}

	@Value("#{configProp['SERFF_ServiceServerHTTPSPort']}")
	public void setServiceServerHTTPSPort(String serverHTTPSPort) {
		setStaticServiceServerHTTPSPort(serverHTTPSPort);
	}

	private static void setStaticServiceServerHTTPSPort(String serverHTTPSPort) {
		SerffConstants.serviceServerHTTPSPort = serverHTTPSPort;
	}

	public static String getServiceServerHTTPSPort() {
		return serviceServerHTTPSPort;
	}

	@Value("#{configProp['Serff_Ftp_Server_Subpath']}")
	public void setFTPServerSubpath(String ftpServerPath) {
		setStaticFTPServerSubpath(ftpServerPath);
	}

	private static void setStaticFTPServerSubpath(String ftpServerPath) {
		SerffConstants.ftpServerUploadSubpath = ftpServerPath;
	}

	@Value("#{configProp['SERFF_BatchWSCallTimeout']}")
	public void setSerffBatchWSCallTimeout(String serffBatchWSCallTimeout) {
		setStaticSerffBatchWSCallTimeout(serffBatchWSCallTimeout);
	}

	private static void setStaticSerffBatchWSCallTimeout(String serffBatchWSCallTimeout) {
		SerffConstants.serffBatchWSCallTimeout = serffBatchWSCallTimeout;
	}

	@Value("#{configProp['SERFF_BatchServerIdleTime']}")
	public void setSerffBatchServerIdleTime(String serffBatchServerIdleTime) {
		setStaticSerffBatchServerIdleTime(serffBatchServerIdleTime);
	}

	private static void setStaticSerffBatchServerIdleTime(String serffBatchServerIdleTime) {
		SerffConstants.serffBatchServerIdleTime = serffBatchServerIdleTime;
	}

	public static String getFtpServerUploadSubpath() {
		return ftpServerUploadSubpath;
	}

	public static String getFTPUploadPlanPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_PLAN_PATH;
	}

	public static String getSTMExcelUploadPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_STM_PATH;
	}

	public static String getAMEExcelUploadPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_AME_PATH;
	}

	public static String getHCCExcelUploadPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_HCC_PATH;
	}

	public static String getPlansDocUploadPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_PLAN_DOCS_PATH;
	}

	public static String getPlansMedicaidUploadPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_PLAN_MEDICAID_PATH;
	}

	public static String getPlansVisionUploadPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_PLAN_VISION_PATH;
	}

	public static String getPlansLifeUploadPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_PLAN_LIFE_PATH;
	}

	public static String getPlansMedicareUploadPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_PLAN_MEDICARE_PATH;
	}
	
	public static String getIssuerLogoUploadPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_ISSUER_LOGO_PATH;
	}

	public static String getPlanBrochureUploadPath() {
		return ftpServerUploadSubpath + FTP_UPLOAD_PLAN_BROCHURE_PATH;
	}
		
	public static String getSerffBatchWSCallTimeout() {
		return serffBatchWSCallTimeout;
	}

	public static String getSerffBatchServerIdleTime() {
		return serffBatchServerIdleTime;
	}

	public static String getDatabaseType() {
		return databaseType;
	}

	@Value("#{configProp['database.type']}")
	public void setDatabaseType(String databaseType) {
		setStaticDatabaseType(databaseType);
	}

	private static void setStaticDatabaseType(String databaseType) {
		SerffConstants.databaseType = databaseType;
	}

	/**
	 * Default Constructor
	 */
	private SerffConstants() {
	}
}
