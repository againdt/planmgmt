package com.serff.util.unixshell;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.serff.util.SerffConstants;

/**
 * SSHService acts as proxy to run Unix commands in given environment. This
 * helps developers to get required information from Unix where SERFF was
 * deployed. This won't give opportunity to users to issues any commands
 * directly. All are predefined tested commands associated with buttons in JSP.
 * 
 * @author Bhavani Polimetla
 * @since 21-May-2014
 * 
 */
@Component
public class SSHServices {

	private static final Logger LOGGER = Logger.getLogger(SSHServices.class);
	private static final String RESP_ERROR = "Command has not been successfully run.";
	private static final String CMD_UNZIP = "unzip -p ";
	private static final String CMD_PING = "ping -c 20 ";
//	private static final String USER = "tomcat";
//	private static final String PASSWORD = "tomcat1";
//	private static final String URL = "23.253.112.223";
//	private static final int PORT = 22;

	private static Session sessionSSH = null;
	private String dbURL = null;

	private static void resetSession() {
		sessionSSH = null;
	}

	private static Session getSSHSession() throws JSchException {

		if (null != sessionSSH) {
			return sessionSSH;
		}

		JSch jsch = new JSch();
		Session session = jsch.getSession(SerffConstants.getSerffMonitorUser(), SerffConstants.getServerIP(), SerffConstants.getServerMonitorPort());
//		Session session = jsch.getSession(USER, URL, PORT);
		session.setPassword(SerffConstants.getSerffMonitorPassword());
//		session.setPassword(PASSWORD);
		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);
		session.connect();
		sessionSSH = session;
		return sessionSSH;
	}

	public boolean validSERFFCredentials() {
		
		LOGGER.debug("validSERFFCredentials Begin");
		boolean isValid = Boolean.TRUE;
		
		try {
			
			LOGGER.info("ServerIP: " + SerffConstants.getServerIP());
            LOGGER.info("SerffMonitorUser: " + SerffConstants.getSerffMonitorUser());
            LOGGER.info("ServerMonitorPort: " + SerffConstants.getServerMonitorPort());
			
			if (StringUtils.isBlank(SerffConstants.getServerIP())) {
				isValid = Boolean.FALSE;
			}
			else if (0 == SerffConstants.getServerMonitorPort()) {
				isValid = Boolean.FALSE;
			}
			else if (StringUtils.isBlank(SerffConstants.getSerffMonitorUser())) {
				isValid = Boolean.FALSE;
			}
			else if (StringUtils.isBlank(SerffConstants.getSerffMonitorPassword())) {
				isValid = Boolean.FALSE;
			}
			
			if (isValid) {
				Session session = getSSHSession();
				
				if (null != session) {
					isValid = session.isConnected();
				}
				else {
					isValid = Boolean.FALSE;
				}
			}
		}
		catch (JSchException ex) {
			isValid = Boolean.FALSE;
			resetSession();
			LOGGER.error(ex.getMessage(), ex);
		}
		catch (Exception ex) {
			isValid = Boolean.FALSE;
			resetSession();
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			
			if (isValid) {
				LOGGER.debug("SERFF Monitoring user credentials are valid.");
			}
			else {
				LOGGER.error("Please provide SERFF Monitoring user credentials at configuration.properties.");
			}
			LOGGER.debug("validSERFFCredentials End");
		}
		return isValid;
	}

	public Connection getDBConnection() {

		LOGGER.debug("getDBConnection Begin");
		
		try {
			InitialContext ic = new InitialContext();
			Context xmlContext = (Context) ic.lookup("java:comp/env");
			DataSource dataSource = (DataSource) xmlContext.lookup("jdbc/ghixDS");
			return dataSource.getConnection();
		}
		catch(Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("getDBConnection End");
		}
		return null;
	}

	public String getDatabaseURL() {
		
		LOGGER.debug("getDatabaseURL Begin");
		Connection dbConn = null;
		
		try {
			if (StringUtils.isNotBlank(dbURL)) {
				LOGGER.info("Available Database Connection URL: " + dbURL);
				return dbURL;
			}
			// Example: jdbc:oracle:thin:@localhost:1521:XE
			dbConn = getDBConnection();
			dbURL = dbConn.getMetaData().getURL();
			LOGGER.info("Database Connection URL: " + dbURL);
		}
		catch(SQLException ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			
			try {
				if (null != dbConn && !dbConn.isClosed()) {
					dbConn.close();
				}
				dbConn = null;
			}
			catch (SQLException e) {
				LOGGER.error(e.getMessage(), e);
			}
			LOGGER.debug("getDatabaseURL End");
		}
		return dbURL;
	}

	public static void main(String[] arg) {

		SSHServices ssh = new SSHServices();
		/*
		 * ssh.getTotalWaitThreads(); ssh.getTotalEstablishedConnection();
		 * ssh.getTotalDBConnections();
		 * 
		 * ssh.getTraceRouteOracle(); ssh.getTraceRouteECM();
		 * 
		 * ssh.getCPU(); ssh.getCPUDetails(); ssh.getOSInfo();
		 * ssh.getSystemDate();
		 * 
		 * ssh.getMemory(); ssh.getFullMemoryDetails();
		 * ssh.getFullMemoryProfile(); ssh.getFreeDiskSpace();
		 * 
		 * ssh.getJavaProcessList(); ssh.getDaemonDetails();
		 * 
		 * ssh.getHead("/home/ghixdev/logs/dict.txt");
		 * ssh.getTail("/home/ghixdev/logs/dict.txt");
		 * ssh.getGrep("z","/home/ghixdev/logs/dict.txt");
		 */
		// ssh.getIPConfig();
		ssh.getHeaderIPInfo();

		// Pass path of serff war file
		// ssh.getSERFFWarInfo("/home/qauser/30may14/");
		// ssh.getPlanMgmtWarInfo("/home/qauser/30may14/");

		disConnectionSSHSession();
	}

	public SSHVo getCPUDetails() {
		SSHVo vo = executeCommand("cat /proc/cpuinfo");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getSERFFWarInfo(String path) {

		SSHVo vo = executeCommand("ls -l " + path
				+ "exchange-hapi.war | awk '{print $5,$6,$7,$8,$9}'");
		SSHVo vo2 = executeCommand(CMD_UNZIP + path
				+ "exchange-hapi.war WEB-INF/classes/build-number.properties");
		SSHVo vo3 = executeCommand(CMD_UNZIP
				+ path
				+ "exchange-hapi.war META-INF/maven/com.getinsured.serffservices/ghix-serff/pom.properties");
		vo.appendVO(vo2);
		vo.appendVO(vo3);
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getPlanMgmtWarInfo(String path) {

		SSHVo vo = executeCommand("ls -l " + path
				+ "ghix-planmgmt.war | awk '{print $5,$6,$7,$8,$9}'");
		SSHVo vo2 = executeCommand(CMD_UNZIP + path
				+ "ghix-planmgmt.war WEB-INF/classes/build-number.properties");
		SSHVo vo3 = executeCommand(CMD_UNZIP
				+ path
				+ "ghix-planmgmt.war META-INF/maven/com.getinsured.planmgmt/ghix-planmgmt/pom.properties");
		vo.appendVO(vo2);
		vo.appendVO(vo3);
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getIPConfig() {
		SSHVo vo = executeCommand("hostname");
		SSHVo vo2 = executeCommand("hostname -i");
		vo.appendVO(vo2);
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getServerIP() {
		
		/*try {
			SSHVo vo = new SSHVo();
			vo.setCommand("hostname -i");
			InetAddress processIp = InetAddress.getLocalHost();
			vo.setResponse(processIp.getHostAddress());
			return vo;
		} catch (UnknownHostException ex) {
			LOGGER.error(ex.getMessage(), ex);
		}*/
		
		SSHVo vo = executeCommand("hostname -i");
		String ipAddress = vo.getResponse();
		LOGGER.debug("IP Address List: " + ipAddress);
		
		if (StringUtils.isNotBlank(ipAddress)) {
			
			String ipList[] = ipAddress.trim().split(SerffConstants.SPACE);
			
			if (ipList.length > 0 && StringUtils.isNotBlank(ipList[0])) {
				vo.setResponse(ipList[0]);
				LOGGER.debug("IP Address: " + ipList[0]);
			}
		}
		return vo;
	}

	public SSHVo getServerName() {
		SSHVo vo = executeCommand("hostname");
		vo.printSSHInfo();
		return vo;
	}

	/**
	 * This is used to display on top of the page.
	 * 
	 * @return hostname/ipaddress
	 */
	public String getHeaderIPInfo() {

		StringBuilder headerIPInfo = new StringBuilder();

		SSHVo vo = executeCommand("hostname");
		SSHVo vo2 = executeCommand("hostname -i | awk '{print $1}'");
		headerIPInfo.append(vo.getResponse()).append("/");
		headerIPInfo.append(vo2.getResponse());

		//System.out.println("Header==>" + headerIPInfo.toString());
		return headerIPInfo.toString();
	}

	public SSHVo getDaemonDetails() {
		SSHVo vo = executeCommand("ps axo pid,ppid,pgrp,tty,tpgid,sess,comm |awk '$2==1' |awk '$1==$3'");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getHead(String fileFullPath, int lines) {

		// make sure that there is no pipe in given file
		SSHVo vo = executeCommand("head -"+ lines + SerffConstants.SPACE + fileFullPath);
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getTail(String fileFullPath, int lines) {

		// make sure that there is no pipe in given file
		SSHVo vo = executeCommand("tail -"+ lines + SerffConstants.SPACE + fileFullPath);
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getFileInfo(String fileFullPath) {

		SSHVo vo = executeCommand("ls -ltr " + fileFullPath);
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getGrep(String word, String fileFullPath) {

		// make sure that there is no pipe in given file
		SSHVo vo = executeCommand("grep " + word + " " + fileFullPath);
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getJavaProcessList() {
		SSHVo vo = executeCommand("ps -ef | grep java");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getOSInfo() {
		SSHVo vo = executeCommand("cat /proc/version");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getFullMemoryDetails() {
		SSHVo vo = executeCommand("cat /proc/meminfo");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getFullMemoryProfile() {
		SSHVo vo = executeCommand("free -m");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getCPU() {
		SSHVo vo = executeCommand("top -b -n 1 | grep java | awk '{sum +=$9}; END {print sum}'");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getMemory() {

		SSHVo vo = executeCommand("top -b -n 1 | grep java | awk '{sum +=$10}; END {print sum}'");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getTotalWaitThreads() {

		SSHVo vo = executeCommand("netstat -an | grep TIME_WAIT | wc -l");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getTotalEstablishedConnection() {
		SSHVo vo = executeCommand("netstat -an | grep ESTABLISHED | wc -l");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getTotalDBConnections() {

		SSHVo vo = executeCommand("netstat -anp | grep 1521 | wc -l");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getFreeDiskSpace() {

		SSHVo vo = executeCommand("df -h");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getSystemDate() {
		SSHVo vo = executeCommand("date");
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getPingOracle() {
		
		// Example: jdbc:oracle:thin:@localhost:1521:XE
		String databaseURL = getDatabaseURL();
		LOGGER.debug("Database Connection URL: " + databaseURL);
		databaseURL = databaseURL.substring(databaseURL.indexOf('@'));
		
		SSHVo vo = executeCommand(CMD_PING + databaseURL.substring(1, databaseURL.indexOf(':')));
		vo.printSSHInfo();
		return vo;
	}

	public SSHVo getPingECM() {
		// change server name
		String ecmURL = SerffConstants.getSerfEcmAtompuburl();
		SSHVo vo = executeCommand(CMD_PING + ecmURL.substring(ecmURL.indexOf("//") + 2, ecmURL.indexOf("/alfresco")));
		vo.printSSHInfo();
		return vo;
	}

	/**
	 * Method is not use.
	 */
	public SSHVo getTraceRouteOracle() {
		
		// Example: jdbc:oracle:thin:@localhost:1521:XE
		String databaseURL = getDatabaseURL();
		LOGGER.debug("Database Connection URL: " + databaseURL);
		databaseURL = databaseURL.substring(databaseURL.indexOf('@'));
		SSHVo vo = executeCommand("traceroute " + databaseURL.substring(1, databaseURL.indexOf(':')));
		vo.printSSHInfo();
		return vo;
	}

	/**
	 * Method is not use.
	 */
	public SSHVo getTraceRouteECM() {
		// change server name
		String ecmURL = SerffConstants.getSerfEcmAtompuburl();
		SSHVo vo = executeCommand("traceroute " + ecmURL.substring(ecmURL.indexOf("//") + 2, ecmURL.indexOf("/alfresco")));
		vo.printSSHInfo();
		return vo;
	}

	private static void disConnectionSSHSession() {
		try {
			Session session = getSSHSession();
			session.disconnect();
		} catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
	}

	private SSHVo executeCommand(String command) {

		SSHVo sshVo = new SSHVo();
		try {
			sshVo.setCommand(command);
			Session session = getSSHSession();

			sshVo.setResponse(executeCommand(sshVo, session, sshVo.getCommand()));

		} catch (Exception ex) {
			sshVo.setResponse(RESP_ERROR);
			sshVo.setErrorMsg(ex.getMessage());
			resetSession();
			LOGGER.error(ex.getMessage(), ex);
		}
		return sshVo;
	}

	private String executeCommand(SSHVo sshVo, Session session, String command) {

		StringBuilder sb = new StringBuilder();
		ChannelExec channel = null;
		InputStream in = null;
		BufferedReader br = null;
		boolean invalidCmd = false;

		try {
			channel = (ChannelExec) session.openChannel("exec");

			if (null != channel) {
				channel.setCommand(command);
				channel.setInputStream(null);
				// channel.setErrStream(System.err);
				channel.connect();

				if (!channel.isConnected()) {
					invalidCmd = true;
				}

				in = channel.getInputStream();
				if (null == in) {
					invalidCmd = true;
				}
			}
			else {
				invalidCmd = true;
			}

			if (invalidCmd) {
				return null;
			}

			br = new BufferedReader(new InputStreamReader(in));
			if (null != br) {
				String line;
				boolean newLine = false;

				while ((line = br.readLine()) != null) {

					if (newLine) {
						sb.append(line).append('\n');
					}
					else {
						sb.append(line);
					}
					newLine = true;
				}
			}
		}
		catch (JSchException e) {
			sshVo.setResponse(RESP_ERROR);
			sshVo.setErrorMsg(e.getMessage());
			resetSession();
			LOGGER.error(e.getMessage(), e);
		}
		catch (IOException e) {
			sshVo.setResponse(RESP_ERROR);
			sshVo.setErrorMsg(e.getMessage());
			resetSession();
			LOGGER.error(e.getMessage(), e);
		}
		finally {

			try {
				if (br != null) {
					br.close();
				}

				if (in != null) {
					in.close();
				}

				if (null != channel && channel.isConnected()) {
					channel.disconnect();
					channel = null;
				}
			}
			catch (IOException e) {
				sshVo.setResponse(RESP_ERROR);
				sshVo.setErrorMsg(e.getMessage());
				resetSession();
				LOGGER.error(e.getMessage(), e);
			}
		}
		return sb.toString();
	}
	
	public InputStream getFileInputStream(String remoteFileName) {
		
		LOGGER.debug("getFileInputStream() START");
		LOGGER.debug("Getting file content for " + remoteFileName);
		
		InputStream inputStream = null;
		
		try {
			
			Session session = getSSHSession();
			if (null == session) {
				return inputStream;
			}
			
			Channel channel = session.openChannel("sftp");
			
			if (null != channel) {
				channel.connect();
				ChannelSftp sftpChannel = (ChannelSftp) channel;
				LOGGER.debug("Connection establised.");
				inputStream = sftpChannel.get(remoteFileName);
				LOGGER.debug("Input stream received is null: " + (null == inputStream));
			}
		}
		catch (Exception ex) {
			LOGGER.error("Exception while reading file.");
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("getFileInputStream() END");
		}
		return inputStream;
	}
}
