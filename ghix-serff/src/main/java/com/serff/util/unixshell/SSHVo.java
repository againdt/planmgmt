package com.serff.util.unixshell;

import org.apache.log4j.Logger;

/**
 * Represents input/output parameters and exceptions for given Unix Session.
 * 
 * @author Bhavani Polimetla
 * @since 21-May-2014
 */
public class SSHVo {
	
	private static final Logger LOGGER = Logger.getLogger(SSHVo.class);
	
	public static enum COMMAND_NAME {
		TOTAL_WAIT_THREADS, ESTABLISHED_CONNECTIONS, ORACLE_CONNECTIONS, PING_ORACLE, PING_ECM, TRACERT_ORACLE, TRACERT_ECM, DOWNLOAD,
		HEAD, TAIL, FILE_INFO, GREP, CPU, CPU_DETAILS, OS, DATE, MEMORY, MEMORY_DETAILS, MEMORY_PROFILE, FREE_DISK_SPACE, JAVA_PROCESSES,
		DEAMON_PROCESSES, SERFF_WAR, PM_WAR;
	}

	private String command;
	private String response;
	private String errorMsg;

	public void printSSHInfo() {
		LOGGER.debug("Command:" + command);
		LOGGER.debug("Response:" + response);
		LOGGER.debug("Error Msg:" + errorMsg);
		
		// Comment them after completion of testing
		//System.out.println("Command:" + command);
		//System.out.println("Response:" + response);
		//System.out.println("Error Msg:" + errorMsg);
	}

	public void appendVO(SSHVo vo) {
		this.command = this.command + " and " + vo.getCommand();
		this.response = this.response + " and \n" + vo.getResponse();
		this.errorMsg = this.errorMsg + " and \n" + vo.getResponse();
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
}
