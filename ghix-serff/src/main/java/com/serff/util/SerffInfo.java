package com.serff.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.SerffEndPoints;

/**
 * SerffInfo class provides required information for developers.
 * <p
 * Provides data through JSP, Web Services and JMX
 * </p>
 * <p>
 * This is module specific class and not supposed to copy to other modules.
 * </p>
 * HIX-6234 - Startup Java Code
 * 
 * @author Vani Sharma
 * @since 04/23/2013
 * 
 */
public class SerffInfo implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger LOGGER = LoggerFactory.getLogger(SerffInfo.class);
	
	private List<SerffInfoRec> records; 
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		LOGGER.info(""+getSerffInfo());
	}

	/**
	 * <p>
	 * This method calls all the individual methods. Every method loads specific
	 * details as indicated in each method.
	 * </p>
	 * 
	 */
	public void loadSerffInfo() {
		
		LOGGER.info("Loading SERFF info");
		loadSerffconfigConfigDetails();
	}

	private void loadSerffconfigConfigDetails() {

		if (!CollectionUtils.isEmpty(records)) {
			return;
		}
		else{
			records = new ArrayList<SerffInfoRec>(); 
		}
		
		final String ecmProperties = "ECM Properties";
		final String csrProperties = "CSR Properties";
		final String serviceEndPoints = "Service Endpoints";
		final String ftpServerProperties = "FTP Server Properties";

		//ECM
		records.add(new SerffInfoRec(ecmProperties, "ECM User Name", SerffConstants.getSerffECMUserName()));
		records.add(new SerffInfoRec(ecmProperties,"ECM AtomPubURL", SerffConstants.getSerfEcmAtompuburl()));
		records.add(new SerffInfoRec(ecmProperties,"ECM Type", SerffConstants.getSerfEcmType()));
		records.add(new SerffInfoRec(ecmProperties,"ECM Base Path", SerffConstants.getSerfEcmBasePath()));
		records.add(new SerffInfoRec(ecmProperties,"ECM Static Content Path", SerffConstants.getSerfEcmStaticContentPath()));
		records.add(new SerffInfoRec(ecmProperties,"ECM Service WSDL", SerffConstants.getEcmServiceWSDL()));	
		records.add(new SerffInfoRec(ecmProperties,"ECM Bypass Enabled", SerffConstants.getSerffECMBypassEnabled()));
		
		//CSR
		records.add(new SerffInfoRec(csrProperties,"Serff CSR FTP Host", SerffConstants.getSerffCsrFtpHost()));
		records.add(new SerffInfoRec(csrProperties,"Serff CSR FTP User", SerffConstants.getSerffCsrFtpUser()));

		final String csrURLArgu = "password=";
		String csrURL = SerffConstants.getSerffCsrFilePollingURI();
		
		if (StringUtils.isNotBlank(csrURL) && csrURL.indexOf(csrURLArgu) > -1) {
			csrURL = csrURL.replace(csrURL.substring(csrURL.indexOf(csrURLArgu), csrURL.indexOf('&')), csrURLArgu + "[Ask Admin]");
		}
		records.add(new SerffInfoRec(csrProperties,"Serff CSR File Polling URI", csrURL));
		records.add(new SerffInfoRec(csrProperties,"Serff CSR Polling ",SerffConstants.getSerffCsrIsPolling()));
		records.add(new SerffInfoRec(csrProperties,"Serff CSR Home Path", SerffConstants.getSerffCsrHomePath()));
		
		//Endpoints
		records.add(new SerffInfoRec(serviceEndPoints,"SERFF Service", GhixEndPoints.GHIX_SERFF_SERVICE_URL));
		records.add(new SerffInfoRec(serviceEndPoints,"Ping Service", SerffEndPoints.PING_SERFF_MODULE));
		//records.add(new SerffInfoRec(serviceEndPoints,"Transfer Plan URL", PlanMgmtEndPoints.TRANSFER_PLAN_URL));
		records.add(new SerffInfoRec(serviceEndPoints,"Plan Status update URL", GhixConstants.GHIX_SERFF_UPDATE_PLAN_STATUS_WS_URL));
		
		//PHIX server Path
		records.add(new SerffInfoRec(ftpServerProperties,"Serff FTP subfolder",  SerffConstants.getFTPUploadPlanPath()));
		records.add(new SerffInfoRec(ftpServerProperties,"Serff Server List",  SerffConstants.getServiceServerList()));
	}
	
	/**
	 * <p>
	 * This method returns records for SERFF info
	 * </p>
	 * 
	 * 
	 * @return the List of records for SERFF information
	 */
	public List<SerffInfoRec> getSerffInfo() {
		
		loadSerffInfo();
		return records;
	}

	/**
	 * This is for Quick Testing Purpose
	 * 
	 * @param str
	 */
	public static void main(String str[]) {
		// System.out.println("In side main");
		SerffInfo serffInfo = new SerffInfo();
		serffInfo.loadSerffInfo();
		// System.out.println(serffInfo.getSerffInfo("txt"));
		// LOGGER.info(serffInfo.getSerffInfo("txt"));
	}

}
