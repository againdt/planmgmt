package com.serff.dto;

/**
 * Class is used to provide getter-setter methods for support documents for respective plan.  
 * @since 03 May, 2018
 */
public class SupportingDocumentDTO {

	private String sbcDocumentName;
	private String sbcDocumentId;
	private String eocDocumentName;
	private String eocDocumentId;
	private String brochureDocumentName;
	private String brochureDocumentId;
	private String benefitFile;
	private String rateFile;

	public String getSbcDocumentName() {
		return sbcDocumentName;
	}

	public void setSbcDocumentName(String sbcDocumentName) {
		this.sbcDocumentName = sbcDocumentName;
	}

	public String getSbcDocumentId() {
		return sbcDocumentId;
	}

	public void setSbcDocumentId(String sbcDocumentId) {
		this.sbcDocumentId = sbcDocumentId;
	}

	public String getEocDocumentName() {
		return eocDocumentName;
	}

	public void setEocDocumentName(String eocDocumentName) {
		this.eocDocumentName = eocDocumentName;
	}

	public String getEocDocumentId() {
		return eocDocumentId;
	}

	public void setEocDocumentId(String eocDocumentId) {
		this.eocDocumentId = eocDocumentId;
	}

	public String getBrochureDocumentName() {
		return brochureDocumentName;
	}

	public void setBrochureDocumentName(String brochureDocumentName) {
		this.brochureDocumentName = brochureDocumentName;
	}

	public String getBrochureDocumentId() {
		return brochureDocumentId;
	}

	public void setBrochureDocumentId(String brochureDocumentId) {
		this.brochureDocumentId = brochureDocumentId;
	}

	public String getBenefitFile() {
		return benefitFile;
	}

	public void setBenefitFile(String benefitFile) {
		this.benefitFile = benefitFile;
	}

	public String getRateFile() {
		return rateFile;
	}

	public void setRateFile(String rateFile) {
		this.rateFile = rateFile;
	}
}
