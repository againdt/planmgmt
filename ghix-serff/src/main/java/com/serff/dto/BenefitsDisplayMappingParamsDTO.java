package com.serff.dto;

public class BenefitsDisplayMappingParamsDTO {
	public static enum NetworkType {
		IN_NETWORK_TIER1, IN_NETWORK_TIER2, OUT_OF_NETWORK;
	}

	private String benefitName;							//Benefit name
	private NetworkType networkType;					//Network type
	private long maxNumDaysForChargingInpatientCopay; 	//Max num of days…
	private long maximumCoinsuranceForSpecialtyDrugs; 	//Max coins for sp drug
	private boolean medicalAndDrugIntegrated;			//Med & drug integrated
	private String planType;							//PlanType (Not Reqd For Now)
	
	public String getBenefitName() {
		return benefitName;
	}
	public void setBenefitName(String benefitName) {
		this.benefitName = benefitName;
	}
	public NetworkType getNetworkType() {
		return networkType;
	}
	public void setNetworkType(NetworkType networkType) {
		this.networkType = networkType;
	}
	public long getMaxNumDaysForChargingInpatientCopay() {
		return maxNumDaysForChargingInpatientCopay;
	}
	public void setMaxNumDaysForChargingInpatientCopay(long maxNumDaysForChargingInpatientCopay) {
		this.maxNumDaysForChargingInpatientCopay = maxNumDaysForChargingInpatientCopay;
	}
	public long getMaximumCoinsuranceForSpecialtyDrugs() {
		return maximumCoinsuranceForSpecialtyDrugs;
	}
	public void setMaximumCoinsuranceForSpecialtyDrugs(long maximumCoinsuranceForSpecialtyDrugs) {
		this.maximumCoinsuranceForSpecialtyDrugs = maximumCoinsuranceForSpecialtyDrugs;
	}
	public boolean isMedicalAndDrugIntegrated() {
		return medicalAndDrugIntegrated;
	}
	public void setMedicalAndDrugIntegrated(boolean medicalAndDrugIntegrated) {
		this.medicalAndDrugIntegrated = medicalAndDrugIntegrated;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
}
