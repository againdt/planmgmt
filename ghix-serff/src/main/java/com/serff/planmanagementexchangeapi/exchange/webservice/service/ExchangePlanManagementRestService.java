package com.serff.planmanagementexchangeapi.exchange.webservice.service;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.serff.planmanagementserffapi.common.model.service.BasicResponse;
import com.serff.planmanagementserffapi.serff.model.service.DataTemplateOutcomes;
import com.serff.planmanagementserffapi.serff.model.service.TransferPlanReply;
import com.serff.planmanagementserffapi.serff.model.service.UpdateExchangeWorkflowStatus;
import com.serff.planmanagementserffapi.serff.model.service.UpdateExchangeWorkflowStatusResponse;

@Controller
@RequestMapping("/exchangePlanManagementRestService")
public class ExchangePlanManagementRestService {

	@RequestMapping(value = { "/validateAndTransformDataTemplateReply" }, method = RequestMethod.POST)
	@ResponseBody
	public boolean validateAndTransformDataTemplateReply(String requestId,
			DataTemplateOutcomes dataTemplateOutcomes) {
		return true;
	}

	@RequestMapping(value = { "/transferPlanReply" }, method = RequestMethod.POST)
	@ResponseBody
	public BasicResponse transferPlanReply(TransferPlanReply request) {
		BasicResponse res = new BasicResponse();
		res.setSuccessful(true);
		return res;
	}

	@RequestMapping(value = { "/updateExchangeWorkflowStatus" }, method = RequestMethod.POST)
	@ResponseBody
	public UpdateExchangeWorkflowStatusResponse updateExchangeWorkflowStatus(
			UpdateExchangeWorkflowStatus request) {
		UpdateExchangeWorkflowStatusResponse res = new UpdateExchangeWorkflowStatusResponse();
		res.setExchangePlanId("1");
		res.setExchangeWorkflowStatus("Success");
		res.setStatus("UPDATED");
		return res;
	}
	
	@RequestMapping(value = { "/serffResponse" }, method = RequestMethod.POST)
	@ResponseBody
	public boolean serffResponse() {
		return true;
	}
}
