package com.serff.planmanagementexchangeapi.exchange.webservice.service;

import static com.serff.util.SerffConstants.INVALID_REQUEST;
import static com.serff.util.SerffConstants.NULL_REQUEST_OR_PLAN_OBJECT;
import static com.serff.util.SerffConstants.SERF_ECM_BASE_PATH;
import static com.serff.util.SerffConstants.TRANSFER_PLAN;
import static com.serff.util.SerffConstants.VALIDATE_AND_TRANSFORM_DATA_TEMPLATE;
import static com.serff.util.SerffConstants.VALID_REQUEST;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.util.exception.GIException;
import com.serff.planmanagementexchangeapi.common.model.pm.Attachment;
import com.serff.planmanagementexchangeapi.common.model.pm.Attachments;
import com.serff.planmanagementexchangeapi.common.model.pm.SupportingDocument;
import com.serff.planmanagementexchangeapi.exchange.model.pm.DataTemplate;
import com.serff.planmanagementexchangeapi.exchange.model.pm.DataTemplates;
import com.serff.planmanagementexchangeapi.exchange.model.pm.TransferDataTemplates;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlan;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlanResponse;
import com.serff.planmanagementexchangeapi.exchange.model.service.ValidateAndTransformDataTemplate;
import com.serff.planmanagementexchangeapi.exchange.model.service.ValidateAndTransformDataTemplateResponse;
import com.serff.service.SerffService;
import com.serff.service.validation.SerffValidator;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * @author kothari_r
 *
 */
@Service("exchangePlanManagementServices")
public class ExchangePlanManagementServiceImp implements ExchangePlanManagementServices{

	public static final Logger LOGGER = Logger.getLogger(ExchangePlanManagementServiceImp.class);

	@Autowired private ContentManagementService ecmService;
	@Autowired private SerffService serffService;
	@Autowired private SerffUtils serffUtils;

	//Code added for transfer plan checking.
	private static final Object lock = new Object();
	private static boolean isTransferPlanRunning = false;
	
	private void setProcessRunningFlag() {
		synchronized (lock) {
			LOGGER.debug("Setting flag to ON.");
			isTransferPlanRunning = true;
		}		
	}
		
	private void resetProcessRunningFlag(){
		synchronized (lock) {
		LOGGER.debug("Setting flag to OFF.");
		isTransferPlanRunning = false;
		}
	}

	/**
	 * @param batch
	 * @param status
	 * @param startTime
	 * @param endTime
	 * @return
	 * 
	 * This method updates the SerffPlanMgmtBatch record with the passed Status and start / End time
	 * When the STATUS is WAITING, start / end time is set as null 
	 */
	private SerffPlanMgmtBatch updateBatchJobStatus(SerffPlanMgmtBatch batch, SerffPlanMgmtBatch.BATCH_STATUS status, Date startTime, Date endTime) {
		if(null != batch && null != serffService) {
			batch.setBatchStatus(status);
			if(status.equals(SerffPlanMgmtBatch.BATCH_STATUS.WAITING)) {
				batch.setBatchStartTime(null);
				batch.setBatchEndTime(null);
			} else {
				if(null != startTime) {
					batch.setBatchStartTime(startTime);
				}
				if(null != endTime) {
					batch.setBatchEndTime(endTime);
				}
			}
			
			return serffService.saveSerffPlanMgmtBatch(batch);
		} else {
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.serff.planmanagementexchangeapi.exchange.webservice.service.ExchangePlanManagementServices#transferPlan(com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlan)
	 */
	@Override
	public TransferPlanResponse transferPlan(final TransferPlan request) {
		//These lists will be populated in case of any errors in operation
		List<String> errorCodes = new ArrayList<String>();
		List<String> errorMessages = new ArrayList<String>();
		boolean isPHIXProfile = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE).equals(SerffConstants.PHIX_PROFILE);
		LOGGER.info("isPHIXProfile" + isPHIXProfile);
		long batchJobID = 0;
		boolean status = false;
		SerffPlanMgmtBatch runningBatch = null;
		TransferPlanResponse response = new TransferPlanResponse();
		SerffPlanMgmt record = null;
		byte[] logoBytes = null;
		//Added null check before validating Plan object.
		if(null == request || null == request.getPlan()){
			errorCodes.add(SerffConstants.NO_OBJECT_FOUND_CODE);
			errorMessages.add(SerffConstants.NO_OBJECT_FOUND_MESSAGE + ": Plan");
			serffUtils.generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
			return response;
		}
		
		boolean isBatchRequest = request.getPlan().getSerffTrackingNumber().startsWith("SERFF_BT-");
		//If this is request coming from batch, Extract the Batch ID from PlanTracking attribute. 
		//Batch ID will be used to get record from DB and update status as processing proceed
		//In case on Non-batch, the request is sent from SOAP-UI or directly from SERFF So there would be no BatchID  
		if(isBatchRequest) {
			String[] planTrackingArr = request.getPlan().getSerffTrackingNumber().split("-");
			if(null != planTrackingArr && planTrackingArr.length > 1) {
				batchJobID = Long.parseLong(planTrackingArr[planTrackingArr.length-1]);
				LOGGER.debug("Transfer Plan called from batch for JobID : " + batchJobID);
				runningBatch = serffService.getSerffPlanMgmtBatchById(batchJobID);
			}
		} 
		
		LOGGER.info("Transfer plan is called. serff request id :  " + request.getRequestId() + " Already Running : " + isTransferPlanRunning);		
		//Check whether transfer plan is running.
		synchronized (lock) {
			if(isTransferPlanRunning){
				LOGGER.info("Transfer plan is running. Sending error response back to calling web-service. " + request.getRequestId());
				errorCodes.add(SerffConstants.IO_ERROR_CODE);
				errorMessages.add(SerffConstants.IO_ERROR_MESSAGE);
				LOGGER.info("Returning error code : " + SerffConstants.IO_ERROR_CODE + " : error message : " + SerffConstants.IO_ERROR_MESSAGE_PROCESS_ALREADY_RUNNING) ;
				serffUtils.generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
				//If another Job is already running, set the Batch STATUS as WAITING< so it could be reprocessed by Batch
				if(null != runningBatch) {
					runningBatch = updateBatchJobStatus(runningBatch, SerffPlanMgmtBatch.BATCH_STATUS.WAITING, null, null);
				}
				return response;
			} else {
				setProcessRunningFlag();
				LOGGER.info("Transfer plan is not already running. Setting flag to continue process.");
			}
		}
		
		try{
			
			//Step 1 :  Create record in SERFF_PLAN_MGMT table
			record = serffUtils.createSerffPlanMgmtRecord(TRANSFER_PLAN);
			record = serffService.saveSerffPlanMgmt(record);
			LOGGER.info("Created record in SERFF_PLAN_MGMT table");
			//Set Batch Status as IN_PROGRESS
			if(null != runningBatch) {
				runningBatch.setSerffReqId(record);
				runningBatch = updateBatchJobStatus(runningBatch, SerffPlanMgmtBatch.BATCH_STATUS.IN_PROGRESS, null, null);
			}
			String tenantIds = null;
			//If in Unit testing mode, a delay of 5 seconds will be introduced after every step
			serffUtils.sleep();
			String exchangeTypeFilter = SerffPlanMgmtBatch.EXCHANGE_TYPE.ON.toString();
			//If this is PHIX profile, get tenant code and Exchange type from Batch record. 
			if(isPHIXProfile && null != runningBatch) {
				tenantIds = runningBatch.getDefaultTenant();
				LOGGER.debug("Tenant code received : " + tenantIds);
				exchangeTypeFilter = runningBatch.getExchangeType();
				LOGGER.debug("Exchange type filter received : " + exchangeTypeFilter);
			}
			if(record != null){
				if(exchangeTypeFilter.equalsIgnoreCase(SerffPlanMgmtBatch.EXCHANGE_TYPE.PUF.toString()) && !"true".equalsIgnoreCase(SerffConstants.getSerffPUFLoadEnabled()) ) {
					//Update SERFF_PLAN_MGMT table with error details
					record = serffUtils.updateErrorDetailsForRecord(record,response,"PUF plans could not be loaded","System is not configured to load PUF plans. Please contact site admin.");
					record = serffService.saveSerffPlanMgmt(record);
					return response;
				}
				record = serffUtils.updateRecordForTransferPlan(record, request, exchangeTypeFilter);
				record = serffService.saveSerffPlanMgmt(record);
			}

			/*
			 * Step 2: Validate incoming request
			 * In case of failure, return an error response
			 */
			if (serffUtils.validatePlan(isPHIXProfile, request.getPlan(),request, response, errorCodes, errorMessages) != null) {

				serffUtils.generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
				//Step 2.1 : Update SERFF_PLAN_MGMT table with error details
				record = serffUtils.updateErrorDetailsForRecord(record,response,SerffConstants.VALIDATION_ERROR,NULL_REQUEST_OR_PLAN_OBJECT);
				record = serffService.saveSerffPlanMgmt(record);
				return response;
			}

			//Step 3 : Storing incoming files in ECM and SERFF_DOCUMENT table
//			Attachments supportingDocs = request.getPlan().getSupportingDocumentation();
			Map <String, String> supportingDocuments = new HashMap<String, String>();
			SerffValidator serffValidator = new SerffValidator();
			String hiosIssuerIdWithState = record.getPlanId().substring(SerffConstants.PLAN_INITIAL_INDEX, SerffConstants.LEN_HIOS_ISSUER_ID_WITH_STATE);

			for (SupportingDocument supportingDocument : request.getPlan().getSupportingDocuments().getSupportingDocument()) {

				Attachments supportingDocs = supportingDocument.getAttachments();

				if (supportingDocs != null && !CollectionUtils.isEmpty(supportingDocs.getAttachment())) {
					List<Attachment> attachments = supportingDocs.getAttachment();

					for (Attachment doc : attachments) {

						if (null == doc || null == doc.getAttachmentData()) {
							continue;
						}

						LOGGER.info("Getting attachment data to put into ECM ");

						byte[] docBytes = serffUtils.createECMdata(doc.getAttachmentData());

						if (docBytes != null && docBytes.length > 0) {

							SerffDocument attachment = null;

							if (doc.getFileName().toLowerCase().contains("logo")) {
								attachment = serffUtils.addAttachmentInCDN(record, doc.getFileName(), docBytes);
								logoBytes = docBytes.clone();
							}
							else {
								LOGGER.info("Adding attachment in ECM in folder " + SERF_ECM_BASE_PATH + "_SupportingDocs_");
								attachment = serffUtils.addAttachmentInECM(ecmService, record, SERF_ECM_BASE_PATH,
										"_SupportingDocs_", doc.getFileName(), docBytes, true);
							}
							serffService.saveSerffDocument(attachment);

							if (null != attachment) {

								String documentKey = serffUtils.generateSupportDocumentKey(attachment.getDocName(), hiosIssuerIdWithState, serffValidator);

								if (null != documentKey) {
									supportingDocuments.put(documentKey, attachment.getEcmDocId());
								}
							}
						}
					}
				}
			}

			//Step 4 : Parse Data Templates
			List<Object> templateObjects = new ArrayList<Object>();
			serffUtils.parseTransferDataTemplates(request.getPlan().getDataTemplates(), response, templateObjects);
			
			//Step 5 : Saving template on ECM and creating record in SERFF_TEMPLATE_STATUS
			LOGGER.info("Creating records in SERFF_TEMPLATE_STATUS table");
			TransferDataTemplates templates = request.getPlan().getDataTemplates();
			supportingDocuments = serffUtils.createSerffTemplateRecords(templates, record, serffService, ecmService,supportingDocuments);

			//Step 6 : Call Plan Management service
			record = serffUtils.addTemplatesToPlanManagementRequest(templateObjects, supportingDocuments,
					request, response, record, serffService, errorCodes, errorMessages, exchangeTypeFilter, tenantIds, logoBytes);

			// LOGGER.info("Soap Response ExchangeWorkflowStatus is " + response.getExchangeWorkflowStatus());

			if (response.getValidationErrors() == null
					|| response.getValidationErrors().getErrors() == null
					|| response.getValidationErrors().getErrors().isEmpty()) {
				record = serffUtils.updateSuccessDetailsForRecord(record, response, SerffConstants.PLANS_LOADED_MESSAGE);
				record = serffService.saveSerffPlanMgmt(record);
				status = true;
			} 
		}
		catch(GIException ge){
			LOGGER.error("GIException occurred in transferPlan() : " + ge.getMessage(),ge);
			response = createResponseXML(ge, record, response, errorCodes, errorMessages);			
		}
		catch(RuntimeException re){
			LOGGER.error("RuntimeException occurred in transferPlan() : " + re.getMessage(),re);
			response = createResponseXML(re, record, response, errorCodes, errorMessages);				
		}
		catch(Exception e){
			LOGGER.error("Exception occurred in transferPlan() : " + e.getMessage(),e);
			response =  createResponseXML(e, record, response, errorCodes, errorMessages);			
		}
		finally{
			LOGGER.info("Resetting process running flag.");
			resetProcessRunningFlag();
			//If this is PHIX profile the update the status in SERFF_PLAN_MGMT_BATCH table
			//In case of non-PHIX, the Batch doe snot exist
			if(isBatchRequest) {
				runningBatch.setSerffReqId(record);
				if(status) { 
					updateBatchJobStatus(runningBatch, SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
				} else if(runningBatch.getBatchStatus().equals(SerffPlanMgmtBatch.BATCH_STATUS.IN_PROGRESS)) {
					updateBatchJobStatus(runningBatch, SerffPlanMgmtBatch.BATCH_STATUS.FAILED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
				}
			}
		}		
		return response;
	}

	/**
	 * This is utility method to generate response XML if exception occurred in transferPlan().
	 * @param e Exception.
	 * @param record Record from SERFF_PLAN_MGMT table.
	 * @param response TransferPlanResponse object.
	 * @param errorCodes Error codes.
	 * @param errorMessages Error messages.
	 * @return TransferPlanResponse object.
	 */
	private TransferPlanResponse createResponseXML(Exception e, SerffPlanMgmt record, TransferPlanResponse response, List<String> errorCodes, List<String> errorMessages){
		try {
			serffUtils.addExceptionCodeToResponse(e, errorCodes, errorMessages);
			serffUtils.generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
			record = serffUtils.updateErrorDetailsForRecord(record,response,INVALID_REQUEST,e.getMessage());
			record = serffService.saveSerffPlanMgmt(record);
		} catch (Exception e1) {
			LOGGER.error(e1.getMessage(),e1);
			errorCodes.add(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE);
			errorMessages.add(SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE_DB_TRANSACTION);
			serffUtils.generateErrorResponseForTransferPlan(response, errorCodes, errorMessages);
			return response;
		}		
		return response;
	}
	
	@Override
	public ValidateAndTransformDataTemplateResponse validateAndTransformDataTemplate(ValidateAndTransformDataTemplate request) {

		LOGGER.info("Start validateAndTransformDataTemplate");
		ValidateAndTransformDataTemplateResponse response = new ValidateAndTransformDataTemplateResponse();
		SerffPlanMgmt record = null;
		response.setSuccessful(false);
		List<String> errorCodes = new ArrayList<String>();
		List<String> errorMessages = new ArrayList<String>();

		try {
			// Step 1:  Create record in SERFF_PLAN_MGMT table.
			record = serffUtils.createSerffPlanMgmtRecord(VALIDATE_AND_TRANSFORM_DATA_TEMPLATE);
			record = serffService.saveSerffPlanMgmt(record);
			LOGGER.info("Created record in SERFF_PLAN_MGMT table");
			//Saving request xml in CLOB
			if(record != null){
				record = serffUtils.updateRecordForValidatePlan(record,request);
				record = serffService.saveSerffPlanMgmt(record);
			}


			// Step 2: Validate incoming request In case of failure, return an error response.
			if (!serffUtils.validateTransformDataTemplate(request)) {
				record = serffUtils.updateErrorDetailsForRecord(record, response, INVALID_REQUEST,NULL_REQUEST_OR_PLAN_OBJECT);
				serffService.saveSerffPlanMgmt(record);
				LOGGER.info("Invalid Request for update plan status.");
			}
			else {


				// Step 3: Parse Data Templates In case of failure, return an error response.
				if (!serffUtils.parseDataTemplates(request.getDataTemplates())) {
					record = serffUtils.updateErrorDetailsForRecord(record,response,INVALID_REQUEST,"Error parsing Data Templates");
					record = serffService.saveSerffPlanMgmt(record);
					LOGGER.info("Invalid Request for update plan status.");
					return response;
				}

				//Step 4 : Parse Data Templates and store them in ECM.
				LOGGER.info("Parse Data Templates and store them in ECM.");
				DataTemplates templates = request.getDataTemplates();

				if (templates != null) {

					List<DataTemplate> dataTemplates = templates.getDataTemplate();

					if (!dataTemplates.isEmpty()) {
						LOGGER.info("Creating records in SERFF_TEMPLATE_STATUS and SERFF_DOCUMENT table.");

						for (DataTemplate template : dataTemplates) {

							try {
								// Adding template in ECM.
								LOGGER.info("Adding template in ECM");
								byte[] docBytes = serffUtils.createECMdata(template.getRequiredInsurerSuppliedData());
								SerffDocument attachment = serffUtils
										.addAttachmentInECM(ecmService, record,
												SERF_ECM_BASE_PATH, record.getSerffReqId() + "_Template",
												template.getDataTemplateType() + "_" + template.getDataTemplateId() + ".xml", docBytes, true);
							//	attachment.setSerffTemplateId(templateStatus.getId());
								// Saving data in SERFF_DOCUMENT table
								serffService.saveSerffDocument(attachment);
							}
							catch (RuntimeException rte) {

								try {
									LOGGER.error(SerffConstants.APP_EXCEPTION_MESSAGE_FAILED_TO_PUT_DOCUMENT_TO_ECM_MESSAGE,rte);
									errorCodes.add(SerffConstants.APP_EXCEPTION_CODE);
									errorMessages.add(SerffConstants.APP_EXCEPTION_MESSAGE);
									record = serffUtils.updateErrorDetailsForRecord(record, response, SerffConstants.APP_EXCEPTION_MESSAGE_FAILED_TO_PUT_DOCUMENT_TO_ECM_MESSAGE, rte.getMessage());
									record = serffService.saveSerffPlanMgmt(record);
								} catch (Exception exception) {
									LOGGER.error(exception.getMessage(), exception);
								}
								return response;
							}
						}
					}
				}

				// Step 5: REST call for plan management.
				LOGGER.info("REST call to Plan Management starts");
				// Need to make REST call to the plan management module.
				LOGGER.info("REST call to Plan Management ends");

				// Step 6: Set response based on response from Plan Management module
				LOGGER.info("Settting response.");
				response.setSuccessful(true);

				// Step 7: Update Success request in SERFF_PLAN_MGMT table
				LOGGER.info("Update Success request in SERFF_PLAN_MGMT table");
				record = serffUtils.updateSuccessDetailsForRecord(record, response, VALID_REQUEST);
				serffService.saveSerffPlanMgmt(record);
				return response;
			}
		}
		catch (Exception e) {

			try {
				serffUtils.addExceptionCodeToResponse(e, errorCodes, errorMessages);
				record = serffUtils.updateErrorDetailsForRecord(record, response, INVALID_REQUEST,e.getMessage());
				record = serffService.saveSerffPlanMgmt(record);
			} catch (Exception e1) {
				serffUtils.addExceptionCodeToResponse(e1, errorCodes, errorMessages);
			}
			LOGGER.error(e.getMessage(),e);
			return response;
		}
		finally {
			LOGGER.info("Return success response: " + response.isSuccessful());
			LOGGER.info("End validateAndTransformDataTemplate");
		}
		return response;
	}

	/*public PlanStatus retrievePlanStatus(RetrievePlanStatus request) {

		LOGGER.info("Start retrievePlanStatus");
		PlanStatus planStatus = PlanStatus.DOES_NOT_EXIST;
		SerffPlanMgmt record = null;
		errorCodes.clear();
		errorMessages.clear();

		try {
			// Step 1:  Create record in SERFF_PLAN_MGMT table.
			record = serffUtils.createSerffPlanMgmtRecord(RETRIEVE_PLAN_STATUS);
			record = serffService.saveSerffPlanMgmt(record);
			LOGGER.info("Created record in SERFF_PLAN_MGMT table.");

			if(record != null){
				record = serffUtils.updateClobData(record,request);
				record.setPlanId(request.getPlanId());
				record = serffService.saveSerffPlanMgmt(record);
			}


			// Step 2: Validate incoming request In case of failure, return an error response.
			if (!serffUtils.validateRetrievePlanStatus(request)) {
				record = serffUtils.updateErrorDetailsForRecord(record, PlanStatus.DOES_NOT_EXIST, INVALID_REQUEST,NULL_REQUEST_OR_PLAN_OBJECT);
				record = serffService.saveSerffPlanMgmt(record);
				LOGGER.info("Invalid Request for retrieve plan status.");
				return planStatus;
			}
			else {

				// Step 3: REST call for plan management.
				LOGGER.info("REST call to Plan Management starts");
				PlanMgmtRequest pmRequest = new PlanMgmtRequest();
				com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.service.RetrievePlanStatus retrievePlanStatus = new com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.service.RetrievePlanStatus();

				retrievePlanStatus.setPlanId(request.getPlanId());

				pmRequest.setRetrievePlanStatus(retrievePlanStatus);
				pmRequest.setAppID(SERFF_APP_ID);
				pmRequest.setRequestTime(null);
				String response = null;
				try {
					response = serffUtils.invokeRestCall(pmRequest, RETRIEVE_PLAN_STATUS_URL);
				}
				catch (RuntimeException rte) {
//					LOGGER.error(rte.getMessage(), rte);
					try {

						if (null != record) {
							record = serffUtils.updateErrorDetailsForRecord(record, PlanStatus.DOES_NOT_EXIST, EMSG_PM_RESPONSE, rte.getMessage());
							record = serffService.saveSerffPlanMgmt(record);
						}
						else {
							LOGGER.error(rte.getMessage(), rte);
						}
					}
					catch (Exception e1) {
						LOGGER.error(e1.getMessage(),e1);
					}
					return planStatus;
				}
				LOGGER.info("REST call to Plan Management ends");

				LOGGER.info("Setting response based on PM response");
				if (response != null && StringUtils.isNotEmpty(response)) {

					PlanMgmtResponse pmResponse = mapper.readValue(response,
							PlanMgmtResponse.class);
					if (null != pmResponse
							&& null != pmResponse.getPlanStatus()) {
						planStatus = PlanStatus.fromValue(pmResponse
								.getPlanStatus().value());
					}

				}
				// Step 5: Update Success request in SERFF_PLAN_MGMT table.
				LOGGER.info("Updating SERFF_PLAN_MGMT with response");
				record = serffUtils.updateSuccessDetailsForRecord(record, planStatus, VALID_REQUEST);
				serffService.saveSerffPlanMgmt(record);
			}
		}
		catch(Exception e){
			try {
				record = serffUtils.updateErrorDetailsForRecord(record, PlanStatus.DOES_NOT_EXIST, INVALID_REQUEST,e.getMessage());
				record = serffService.saveSerffPlanMgmt(record);
			} catch (Exception e1) {
				LOGGER.error(e1.getMessage(),e1);
			}
			LOGGER.error(e.getMessage(),e);
			return planStatus;
		}
		finally {
			LOGGER.info("Return response plan status: " + planStatus);
			LOGGER.info("End retrievePlanStatus");
		}
		return planStatus;
	}*/

	/* public UpdatePlanStatusResponse updatePlanStatus(UpdatePlanStatus request) {

		LOGGER.info("Start updatePlanStatus");
		UpdatePlanStatusResponse response = new UpdatePlanStatusResponse();
		SerffPlanMgmt record = null;
		errorCodes.clear();
		errorMessages.clear();

		try {
			// Step 1:  Create record in SERFF_PLAN_MGMT table.
			record = serffUtils.createSerffPlanMgmtRecord(UPDATE_PLAN_STATUS);
			record = serffService.saveSerffPlanMgmt(record);
			LOGGER.info("Created record in SERFF_PLAN_MGMT table");

			//Saving request xml in CLOB
			if(record != null){
				record = serffUtils.updateClobData(record,request);
				record.setPlanId(request.getPlanId());
				record = serffService.saveSerffPlanMgmt(record);
			}

			// Step 2: Validate incoming request In case of failure, return an error response.
			if (!serffUtils.validateUpdatePlanStatus(request)) {
				response.setPlanId(request.getPlanId());
				response.setPlanStatus(PlanStatus.DOES_NOT_EXIST);

				record = serffUtils.updateErrorDetailsForRecord(record, response, INVALID_REQUEST,NULL_REQUEST_OR_PLAN_OBJECT);
				serffService.saveSerffPlanMgmt(record);
				LOGGER.info("Invalid Request for update plan status.");
			}
			else {

				// Step 3: REST call for plan management.
				LOGGER.info("REST call to Plan Management starts");

				PlanMgmtRequest pmRequest = new PlanMgmtRequest();
				com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.service.UpdatePlanStatus updatePlanStatus = new com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.exchange.model.service.UpdatePlanStatus();


				com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.common.model.pm.PlanStatus pmPlanStatus = com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.common.model.pm.PlanStatus.DOES_NOT_EXIST;
				pmPlanStatus = com.getinsured.hix.dto.planmgmt.planmanagementexchangeapi.common.model.pm.PlanStatus.fromValue(request.getPlanStatus().value());

				updatePlanStatus.setPlanId(request.getPlanId());
				updatePlanStatus.setPlanStatus(pmPlanStatus);

				pmRequest.setUpdatePlanStatus(updatePlanStatus);
				pmRequest.setAppID(SERFF_APP_ID);
				pmRequest.setRequestTime(null);

				String restResponse = null;
				try {
					restResponse = serffUtils.invokeRestCall(pmRequest, UPDATE_PLAN_STATUS_URL);
				}
				catch (RuntimeException rte) {

					try {

						if (null != record) {
							response.setPlanId(request.getPlanId());
							response.setPlanStatus(PlanStatus.DOES_NOT_EXIST);
							record = serffUtils.updateErrorDetailsForRecord(record, PlanStatus.DOES_NOT_EXIST, EMSG_PM_RESPONSE, rte.getMessage());
							record = serffService.saveSerffPlanMgmt(record);
						}
						else {
							LOGGER.error(rte.getMessage(), rte);
						}
					}
					catch (Exception e1) {
						LOGGER.error(e1.getMessage(),e1);
					}
					return response;
				}
				LOGGER.info("REST call to Plan Management ends");


				LOGGER.info("Setting response based on PM response");
				if (response != null && StringUtils.isNotEmpty(restResponse)) {

					PlanMgmtResponse pmResponse = mapper.readValue(
							restResponse, PlanMgmtResponse.class);
					if (null != pmResponse
							&& null != pmResponse.getUpdatePlanStatusResponse()) {
						response.setPlanId(pmResponse
								.getUpdatePlanStatusResponse().getPlanId());

						response.getPlanStatus();
						response.setPlanStatus(PlanStatus.fromValue(pmResponse
								.getUpdatePlanStatusResponse().getPlanStatus()
								.value()));
					}

				}
				// Step 4: Update Success request in SERFF_PLAN_MGMT table
				record = serffUtils.updateSuccessDetailsForRecord(record, response, VALID_REQUEST);
				serffService.saveSerffPlanMgmt(record);
				LOGGER.info("Return success response with update plan status: " + request.getPlanStatus());
			}
		}
		catch(Exception e){
			try {

				if (null != record) {
					record = serffUtils.updateErrorDetailsForRecord(record, response, INVALID_REQUEST,e.getMessage());
					record = serffService.saveSerffPlanMgmt(record);
				}
				else {
					LOGGER.error(e.getMessage(), e);
				}
			} catch (Exception e1) {
				LOGGER.error(e1.getMessage(),e1);
			}
			LOGGER.error(e.getMessage(),e);
			return response;
		}
		finally {
			LOGGER.info("End updatePlanStatus");
		}
		return response;
	}*/
}
