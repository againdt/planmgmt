
package com.serff.planmanagementexchangeapi.exchange.webservice.service;

/**
 * Service interface for CSR
 * @author Nikhil Talreja
 * @since 03 April , 2013
 */
public interface CSRService {
	
	/**
	 * This method reads CSR payment files from FTP location and processes them
	 * This method is called by a PERL script using a Web Service call
	 * @author Nikhil Talreja
	 * @since Apr 3, 2013
	 * @param 
	 * @return void
	 */
	void readCSRFiles(String fileName);
	
}
