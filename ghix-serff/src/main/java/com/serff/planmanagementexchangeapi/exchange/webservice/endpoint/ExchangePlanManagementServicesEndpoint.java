package com.serff.planmanagementexchangeapi.exchange.webservice.endpoint;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

//import com.serff.planmanagementexchangeapi.common.model.pm.PlanStatus;
//import com.serff.planmanagementexchangeapi.exchange.model.service.RetrievePlanStatus;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlan;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlanResponse;
import com.serff.planmanagementexchangeapi.exchange.model.service.ValidateAndTransformDataTemplate;
import com.serff.planmanagementexchangeapi.exchange.model.service.ValidateAndTransformDataTemplateResponse;
import com.serff.planmanagementexchangeapi.exchange.webservice.service.ExchangePlanManagementServices;

@Endpoint
public class ExchangePlanManagementServicesEndpoint {

	private static final String TARGET_NAMESPACE = "http://www.serff.com/planManagementExchangeApi/exchange/model/service";
	public static final Logger LOGGER = Logger
			.getLogger(ExchangePlanManagementServicesEndpoint.class);

	@Autowired
	private ExchangePlanManagementServices exchangePlanManagementServiceInterface;

/*	@PayloadRoot(localPart = "retrievePlanStatus", namespace = TARGET_NAMESPACE)
	@ResponsePayload
	public PlanStatus retrievePlanStatus(
			@RequestPayload RetrievePlanStatus request) {
		return exchangePlanManagementServiceInterface
				.retrievePlanStatus(request);

	}*/

	@PayloadRoot(localPart = "transferPlan", namespace = TARGET_NAMESPACE)
	@ResponsePayload
	public TransferPlanResponse transferPlan(
			@RequestPayload TransferPlan request) {

		return exchangePlanManagementServiceInterface.transferPlan(request);
	}

/*	@PayloadRoot(localPart = "updatePlanStatus", namespace = TARGET_NAMESPACE)
	@ResponsePayload
	public com.serff.planmanagementexchangeapi.exchange.model.service.UpdatePlanStatusResponse updatePlanStatus(
			@RequestPayload com.serff.planmanagementexchangeapi.exchange.model.service.UpdatePlanStatus request) {
		return exchangePlanManagementServiceInterface.updatePlanStatus(request);
	}*/

	@PayloadRoot(localPart = "validateAndTransformDataTemplate", namespace = TARGET_NAMESPACE)
	@ResponsePayload
	public ValidateAndTransformDataTemplateResponse validateAndTransformDataTemplate(
			@RequestPayload ValidateAndTransformDataTemplate request) {
		return exchangePlanManagementServiceInterface
				.validateAndTransformDataTemplate(request);
	}

	public void setExchangePlanManagementServiceInterface(
			ExchangePlanManagementServices exchangePlanManagementServiceInterface) {
		// this.exchangePlanManagementServiceInterface =
		// exchangePlanManagementServiceInterface;
	}
}
