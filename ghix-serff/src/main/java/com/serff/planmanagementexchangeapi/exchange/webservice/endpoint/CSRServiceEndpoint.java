package com.serff.planmanagementexchangeapi.exchange.webservice.endpoint;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.hix.csr.model.CsrRequest;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.serff.planmanagementexchangeapi.exchange.webservice.service.CSRService;

/**
 * Web Service end point for CSR
 * @author Nikhil Talreja
 * @since 03 April , 2013
 */
@Endpoint
public class CSRServiceEndpoint {
	
	private static final String TARGET_NAMESPACE = "http://endpoint.webservice.exchange.planmanagementexchangeapi.serff.com";
	public static final Logger LOGGER = Logger
			.getLogger(CSRServiceEndpoint.class);
	
	@Autowired private CSRService csrService;
	
	/**
	 * This method reads calls CSR Web Service implementation to read CSR file
	 * @author Nikhil Talreja
	 * @since Apr 1, 2013
	 * @param 
	 * @return void
	 */
	@PayloadRoot(localPart = "csrRequest", namespace = TARGET_NAMESPACE)
	@ResponsePayload
	public void readCSRFiles(@RequestPayload final CsrRequest request){
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Reading file " + SecurityUtil.sanitizeForLogging(request.getFileName()));
		}
		csrService.readCSRFiles(request.getFileName());
		
		LOGGER.info("CSR processing completed");
	}
	
}
