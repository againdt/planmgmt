package com.serff.planmanagementexchangeapi.exchange.webservice.service;


import static com.serff.util.SerffConstants.APP_EXCEPTION_CODE;
import static com.serff.util.SerffConstants.APP_EXCEPTION_MESSAGE;
import static com.serff.util.SerffConstants.CSR;
import static com.serff.util.SerffConstants.CSR_REQUEST;
import static com.serff.util.SerffConstants.EXCEPTION_ERROR_CODE;
import static com.serff.util.SerffConstants.INVALID_REQUEST;
import static com.serff.util.SerffConstants.RUNTIME_ERROR_CODE;
import static com.serff.util.SerffConstants.SERF_ECM_BASE_PATH;
import static com.serff.util.SerffConstants.VALID_REQUEST;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.csr.model.CsrResponse;
import com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws.PlanMgmtRequest;
import com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws.PlanMgmtResponse;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.serff.planmanagementexchangeapi.common.model.pm.ValidationError;
import com.serff.planmanagementexchangeapi.common.model.pm.ValidationErrors;
import com.serff.service.SerffService;
import com.serff.service.SerffTemplateService;
import com.serff.service.validation.CSRValidator;
import com.serff.service.validation.FailedValidation;
import com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;
/**
 * Class is used to implements method of CSR
 * @author Bhavin Parmar
 */
@Service("csrService")
public class CSRServiceImp implements CSRService {
	
	private static final Logger LOGGER = Logger.getLogger(CSRServiceImp.class);
	private static final String MOVING_FILE_ERROR_FTP = "Start: Moving file to error/ID using FTP";
	
	@Autowired private ContentManagementService ecmService;
	@Autowired private SerffService serffService;
	@Autowired private SerffUtils serffUtils;
	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private CSRValidator csrValidator;
	
	@Autowired private SerffTemplateService serffTemplateService;
	
	
	//These lists will be populated in case of any errors in operation
	private List<String> errorCodes = new ArrayList<String>();
	private List<String> errorMessages = new ArrayList<String>();
	
	private List<FailedValidation> validationErrors = new ArrayList<FailedValidation>();
	
	@Override
	public void readCSRFiles(String fileName) {

		LOGGER.info("readCSRFiles() Start");
		validationErrors.clear();
		SerffPlanMgmt record = null;
		CsrResponse csrResponse = new CsrResponse();
		
		try {
			errorCodes.clear();
			errorMessages.clear();
			
			// Step 1: Fetching file from FTP location.
			byte[] fileContent = ftpClient.getFileContent(fileName);
			CSRAdvancePaymentDeterminationType csrVO = getCSRAdvancePaymentVO(fileName, fileContent);
			
			// Step 2: Create record in SERFF_PLAN_MGMT table.
			record = serffUtils.createSerffPlanMgmtRecord(CSR);
			record = serffService.saveSerffPlanMgmt(record);
			LOGGER.info("Created record in SERFF_PLAN_MGMT table");
			
			// Saving request xml in CLOB
			if (record != null) {
				record = serffUtils.updateClobData(record, CSR_REQUEST + fileName);
				record = serffService.saveSerffPlanMgmt(record);
			}

			// Step 3: Call ECM and save SERFF Document.
			LOGGER.info("Start: Call ECM and store file.");
			try {
				SerffDocument attachment = serffUtils.addAttachmentInECM(
						ecmService, record, SERF_ECM_BASE_PATH,
								Long.toString(record.getSerffReqId()), fileName, fileContent, true);
				attachment.setSerffReqId(record);
				serffService.saveSerffDocument(attachment);
				LOGGER.info("Save SERFF Document");
			}
			catch (RuntimeException rte) {
				
				try {
					
					if (null != record) {
						errorCodes.add(APP_EXCEPTION_CODE);
						errorMessages.add(APP_EXCEPTION_MESSAGE);
						generateRuntimeErrors(csrResponse, SerffConstants.EMSG_ECM_RESPONSE, RUNTIME_ERROR_CODE, rte.getMessage(), record, fileName);
					}
				}
				catch (Exception exception) {
					LOGGER.error(exception.getMessage(), exception);
				}
				return;
			}
			LOGGER.info("End: Call ECM and store file.");
			
			// Step 4: Moving file to inprocess/ID using FTP
			LOGGER.info("Start: Moving file to inprocess/ID using FTP");
			ftpClient.moveFile("", fileName, SerffConstants.FTP_CSR_INPROCESS_PATH + record.getSerffReqId(), fileName);
			LOGGER.info("End: Moving file to inprocess/ID using FTP");

			// Step 7: Validation
			LOGGER.info("Start: Validation");
			csrValidator.validateCSR(csrVO, validationErrors);
			LOGGER.info("End: Validation");
			
			if (CollectionUtils.isEmpty(validationErrors)) {
				
				// Step 8: Data Mapping(Dozer) and Call Plan management request. 
				LOGGER.info("Start: Data Mapping(Dozer) and Call Plan management request");
				PlanMgmtRequest pmRequest = new PlanMgmtRequest();
				pmRequest.setAppID(SerffConstants.SERFF_APP_ID);
				pmRequest.setRequestTime(null);
				
				if(null != csrVO){
					
					// String sBenefitYear = null;
					// String sSubmissionDate = null;
					
					CSRAdvancePaymentDeterminationType csrVoDto = serffUtils.mapCSRVO(csrVO);
					
					if (null != csrVO.getBenefitYearNumeric()) {
						csrVoDto.setBenefitYearNumeric(csrVO.getBenefitYearNumeric());
					}
					
					if (null != csrVO.getSubmissionFileGenerationDateTime()) {
						csrVoDto.setSubmissionFileGenerationDateTime(csrVO.getSubmissionFileGenerationDateTime());
					}
					
					/*com.serff.template.csr.niem.core.DateType txtBenefitYear = new com.serff.template.csr.niem.core.DateType();
					txtBenefitYear.setValue(sBenefitYear);
					csrVoDto.setBenefitYearNumeric(txtBenefitYear);*/
					
					/*com.serff.template.csr.niem.core.DateType txtSubmissionDate = new com.serff.template.csr.niem.core.DateType();
					txtSubmissionDate.setValue(sSubmissionDate);
					csrVoDto.setSubmissionFileGenerationDateTime(txtSubmissionDate);*/
					
					pmRequest.setCsrTemplate(csrVoDto);
				}
				
				PlanMgmtResponse planMgmtResponse = serffTemplateService.processCSR(pmRequest);
				
				/*String pmResponse = null;
				try {
					pmResponse = serffUtils.invokeRestCall(pmRequest, PROCESS_CSR_URL);
				}
				catch (Throwable rte) {
					
					if (rte instanceof Exception) {
						
						try {
							
							if (null != record) {
								errorCodes.add(APP_EXCEPTION_CODE);
								errorMessages.add(APP_EXCEPTION_MESSAGE);
								generateRuntimeErrors(csrResponse, SerffConstants.EMSG_PM_RESPONSE, RUNTIME_ERROR_CODE, rte.getMessage(), record, fileName);
							}
						}
						catch (Exception exception) {
							LOGGER.error(exception.getMessage(), exception);
						}
						return;
					}
					else if (rte instanceof Error) {
						generateRuntimeErrors(csrResponse, SerffConstants.EMSG_PM_RESPONSE, RUNTIME_ERROR_CODE, rte.getMessage(), record, fileName);
						return;
					}
				}
				finally {
					LOGGER.info("End: Data Mapping(Dozer) and Call Plan management request");
				}*/
				
				csrResponse = validatePlanMgmtResponse(planMgmtResponse, record, fileName);
				
				if(null == csrResponse.getValidationErrors()){
					// Step 12: Update valid response to SerffPlanMgmt
					LOGGER.info("Start: Update valid response to SerffPlanMgmt");
					record = serffUtils.updateSuccessDetailsForRecord(record, planMgmtResponse, VALID_REQUEST);
					serffService.saveSerffPlanMgmt(record);
					
					//Moving file to success folder
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("End: Update valid response to SerffPlanMgmt");
						LOGGER.info("Start: Moving file to success/ID using FTP "+SerffConstants.FTP_CSR_INPROCESS_PATH + record.getSerffReqId()+"/"+SecurityUtil.sanitizeForLogging(fileName));
					}
					ftpClient.moveFile(SerffConstants.FTP_CSR_INPROCESS_PATH + record.getSerffReqId(), fileName, SerffConstants.FTP_CSR_SUCCESS_PATH + record.getSerffReqId(), fileName);
					csrResponse.setSuccessful(true);
					csrResponse.setStatus("CSR processing completed.");
					csrResponse.setValidationErrors(null);
					//Writing response at FTP location
					writeResponseToFTP(csrResponse, SerffConstants.FTP_CSR_SUCCESS_PATH + record.getSerffReqId());
				}
			}
			else {
				
				LOGGER.info("Validation has been failed.");
				generateValidationErrors(csrResponse, record, fileName);
			}
		}
		catch (Exception exception1) {
			
			try {
				
				if (null != record) {
					generateRuntimeErrors(csrResponse, INVALID_REQUEST, EXCEPTION_ERROR_CODE, exception1.getMessage(), record, fileName);
				}
			}
			catch (Exception exception2) {
				LOGGER.error(exception2.getMessage(), exception2);
			}
			LOGGER.error(exception1.getMessage(), exception1);
		}
		finally {
			LOGGER.info("readCSRFiles() End");	
			if(null!= ftpClient){
				ftpClient.close();
			}
		}
	}
	
	private void generateRuntimeErrors(CsrResponse csrResponse, String remarks,
			String errorCode, String errorMsg, SerffPlanMgmt record, String fileName)
			throws Exception {
		
		errorCodes.add(errorCode);
		errorMessages.add(errorMsg);
		csrResponse.setStatus(SerffConstants.FAILURE);
		csrResponse = generatefailureCsrResponse(csrResponse, errorCodes);
		
		record = serffUtils.updateErrorDetailsForRecord(record, csrResponse, remarks, errorMsg);
		record = serffService.saveSerffPlanMgmt(record);
		
		LOGGER.info(MOVING_FILE_ERROR_FTP);
		
		//If an ECM exception occurs the sample file is still under root folder rather
		// than the inprocess folder.
		if(remarks.equalsIgnoreCase(SerffConstants.EMSG_ECM_RESPONSE)){
			ftpClient.moveFile("", fileName, SerffConstants.FTP_CSR_ERRORS_PATH + record.getSerffReqId(), fileName);
		}else{
			ftpClient.moveFile(SerffConstants.FTP_CSR_INPROCESS_PATH + record.getSerffReqId(), fileName, SerffConstants.FTP_CSR_ERRORS_PATH + record.getSerffReqId(), fileName);
		}
		
		//Writing response at FTP location
		writeResponseToFTP(csrResponse, SerffConstants.FTP_CSR_ERRORS_PATH + record.getSerffReqId());
	}
	
	/**
	 * Method is used to generate validation errors from ValidationError List.
	 * 
	 * @param csrResponse, Object of CsrResponse
	 * @param record, Object of SerffPlanMgmt
	 * @param fileNam, File Name
	 * @throws Exception
	 */
	private void generateValidationErrors(CsrResponse csrResponse,
			SerffPlanMgmt record, String fileName) throws Exception {
		
		ValidationErrors errorList = new ValidationErrors();
		List<ValidationError> errors = errorList.getErrors();
		ValidationError error = null;
		
		for (FailedValidation validationerror : validationErrors) {
			
			error = new ValidationError();
			error.setCode(validationerror.getErrorCode());
			error.setMessage(validationerror.getErrorMsg());
			errors.add(error);
			
			error = new ValidationError();
			if (null != validationerror.getFieldValue()) {
				error.setCode(validationerror.getErrorCode());
				error.setMessage("Template name: " + validationerror.getTemplateName()
						+ ", Field name:" + validationerror.getFieldName()
						+ ", Field value:" + validationerror.getFieldValue());
			}
			else {
				error.setCode(validationerror.getErrorCode());
				error.setMessage("Template name: " + validationerror.getTemplateName()
						+ ", Field name:" + validationerror.getFieldName());
			}
			errors.add(error);
		}
		csrResponse.setStatus(SerffConstants.FAILURE);
		csrResponse = generatefailureCsrResponse(csrResponse, errorCodes);
		csrResponse.setValidationErrors(errorList);
		
		record = serffUtils.updateErrorDetailsForRecord(record, csrResponse, INVALID_REQUEST, "Validation of template failed");
		record = serffService.saveSerffPlanMgmt(record);
		
		LOGGER.info(MOVING_FILE_ERROR_FTP);
		ftpClient.moveFile(SerffConstants.FTP_CSR_INPROCESS_PATH + record.getSerffReqId(), fileName, SerffConstants.FTP_CSR_ERRORS_PATH + record.getSerffReqId(), fileName);
		
		//Writing response at FTP location
		writeResponseToFTP(csrResponse, SerffConstants.FTP_CSR_ERRORS_PATH + record.getSerffReqId());
	}
	
	/**
	 * Method is used to validate Plan Management Response and return CsrResponse
	 * @param pmResponse, Plan Management Response
	 * @param record, Object of SerffPlanMgmt
	 * @param fileName, File Name
	 * @return CsrResponse
	 * @throws Exception
	 */
	private CsrResponse validatePlanMgmtResponse(PlanMgmtResponse planMgmtResponse,
			SerffPlanMgmt record, String fileName) throws Exception {
		
		CsrResponse csrResponse = new CsrResponse();
		
//		PlanMgmtResponse planMgmtResponse = mapper.readValue(pmResponse, PlanMgmtResponse.class);
		if (null != planMgmtResponse
				&& !StringUtils.equalsIgnoreCase(
						planMgmtResponse.getStatus(),
						SerffConstants.SUCCESS)) {
			errorCodes.add(String.valueOf(planMgmtResponse.getErrCode()));
			errorMessages.add(planMgmtResponse.getErrMsg());
			csrResponse.setStatus(planMgmtResponse.getStatus());
			csrResponse = generatefailureCsrResponse(csrResponse, errorCodes);
			record.setPmResponseXml(serffUtils.objectToXmlString(planMgmtResponse));
			record = serffUtils.updateErrorDetailsForRecord(record, csrResponse, INVALID_REQUEST, "Error response from Plan management");
			record = serffService.saveSerffPlanMgmt(record);
			LOGGER.info(MOVING_FILE_ERROR_FTP);
			ftpClient.moveFile(SerffConstants.FTP_CSR_INPROCESS_PATH + record.getSerffReqId(), fileName, SerffConstants.FTP_CSR_ERRORS_PATH + record.getSerffReqId(), fileName);
			
			//Writing response at FTP location
			writeResponseToFTP(csrResponse, SerffConstants.FTP_CSR_ERRORS_PATH + record.getSerffReqId());
		}
		return csrResponse;
	}
	
	/**
	 * Method is used to Unmarshalle CSRAdvancePaymentDeterminationType Object
	 * @param fileName, File Name
	 * @return Object of CSRAdvancePaymentDeterminationType
	 */
	private CSRAdvancePaymentDeterminationType getCSRAdvancePaymentVO(
			String fileName, byte[] fileContent) throws IOException {
		
		LOGGER.info("Start: Fetching file from FTP location");
		CSRAdvancePaymentDeterminationType csrVO = new CSRAdvancePaymentDeterminationType();
		InputStream in = null;
		XMLStreamReader xsr = null;
		try {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Got file " + SecurityUtil.sanitizeForLogging(fileName));
			}
			in = new ByteArrayInputStream(fileContent); 
			csrVO = (CSRAdvancePaymentDeterminationType) GhixUtils.inputStreamToObject(in, CSRAdvancePaymentDeterminationType.class);
		} catch (Exception e) {
			LOGGER.info("Error while unmarshalling FTP file");
			LOGGER.error(e.getMessage(),e);
		}
		finally {
			if(null != in) {
				in.close();
			}
			if(null != xsr) {
				try {
					xsr.close();
				} catch (XMLStreamException e) {
					LOGGER.error("Failed to close XML Stream : " + e.getMessage(), e);
				}
			}
			LOGGER.info("End: Fetching file from FTP location");
		}
		return csrVO;
	}
	
	/**
	 * This method generates CsrResponse in case of any error/failure
	 * @author Geetha Chandran
	 * @since 5 April, 2013
	 */	
	private CsrResponse generatefailureCsrResponse(CsrResponse response,
			List<String> errorCodes) {
		
		if(null != response){
			response.setSuccessful(false);
			
			ValidationErrors errorList = new ValidationErrors();
			List<ValidationError> errors =   errorList.getErrors();
			
			if(!CollectionUtils.isEmpty(errorCodes) && !CollectionUtils.isEmpty(errorMessages)){
				LOGGER.info("Total errors in response : " + errorCodes.size());
				for(int i=0; i<errorCodes.size(); i++){
					ValidationError error = new ValidationError();
					error.setCode(errorCodes.get(i));
					error.setMessage(errorMessages.get(i));
					errors.add(error);
				}
			}
			
			response.setValidationErrors(errorList);
		}
		
		return response;
	}
	
	/**
	 * This method writes response at FTP root level and move to target folder defined.
	 * @author Geetha Chandran
	 * @since 9 April, 2013
	 */	
	private void writeResponseToFTP (CsrResponse csrResponse, String remoteTargetFolder) throws Exception{
		//Writing response at FTP location
		String response = serffUtils.objectToXmlString(csrResponse);
		InputStream localFile = IOUtils.toInputStream(response, "UTF-8");
		ftpClient.uplodadFile(localFile, remoteTargetFolder + SerffConstants.FTP_RESULT);
	}
}
