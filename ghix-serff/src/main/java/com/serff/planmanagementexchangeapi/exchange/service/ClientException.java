
package com.serff.planmanagementexchangeapi.exchange.service;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 2.7.3
 * 2013-02-26T18:42:12.044+05:30
 * Generated source version: 2.7.3
 */

@WebFault(name = "clientException", targetNamespace = "http://www.serff.com/planManagementExchangeApi/common/model/service")
public class ClientException extends Exception {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -6779992265070471743L;
	private com.serff.planmanagementexchangeapi.common.model.service.ClientException clientException;

    public ClientException() {
        super();
    }
    
    public ClientException(String message) {
        super(message);
    }
    
    public ClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClientException(String message, com.serff.planmanagementexchangeapi.common.model.service.ClientException clientException) {
        super(message);
        this.clientException = clientException;
    }

    public ClientException(String message, com.serff.planmanagementexchangeapi.common.model.service.ClientException clientException, Throwable cause) {
        super(message, cause);
        this.clientException = clientException;
    }

    public com.serff.planmanagementexchangeapi.common.model.service.ClientException getFaultInfo() {
        return this.clientException;
    }
}
