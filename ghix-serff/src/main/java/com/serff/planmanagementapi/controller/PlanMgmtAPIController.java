package com.serff.planmanagementapi.controller;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpServletRequest;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.AmePlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.CrossSellPlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.EmployerRequest;
import com.getinsured.hix.dto.planmgmt.IssuerRequest;
import com.getinsured.hix.dto.planmgmt.MedicaidPlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.VisionPlanRequestDTO;
import com.getinsured.hix.dto.shop.EmployerCoverageDTO;
import com.getinsured.hix.dto.shop.employer.eps.EmployerQuotingDTO;
import com.getinsured.hix.dto.shop.mlec.MLECRestRequest;
import com.getinsured.hix.model.planmgmt.EligibilityRequest;
import com.getinsured.hix.model.planmgmt.HouseHoldRequest;
import com.serff.planmanagementapi.util.BenchMarkPlanAPIUtil;
import com.serff.planmanagementapi.util.BenefitsAndCostAPIUtil;
import com.serff.planmanagementapi.util.CheckIndvHealthPlanAvailabilityAPIUtil;
import com.serff.planmanagementapi.util.CheckMetalTierAvailabilityAPIUtil;
import com.serff.planmanagementapi.util.CheckPlanAvailabilityForEmployeesAPIUtil;
import com.serff.planmanagementapi.util.FindLowestPremiumPlanRateAPIUtil;
import com.serff.planmanagementapi.util.GetAMEPlansAPIUtil;
import com.serff.planmanagementapi.util.GetAllAMEPlansAPIUtil;
import com.serff.planmanagementapi.util.GetBenchmarkRateAPIUtil;
import com.serff.planmanagementapi.util.GetBenefitCostDataAPIUtil;
import com.serff.planmanagementapi.util.GetBenefitsAndCostByPlanIdAPIUtil;
import com.serff.planmanagementapi.util.GetDentalPlansAPIUtil;
import com.serff.planmanagementapi.util.GetDisclaimerInfoUtil;
import com.serff.planmanagementapi.util.GetEmployerMinMaxRateAPIUtil;
import com.serff.planmanagementapi.util.GetEmployerPlanRateAPIUtil;
import com.serff.planmanagementapi.util.GetIssuerInfoByZipcodeUtil;
import com.serff.planmanagementapi.util.GetIssuerInfobyId;
import com.serff.planmanagementapi.util.GetLowestAMEPremiumAPIUtil;
import com.serff.planmanagementapi.util.GetLowestDentalPremiumAPIUtil;
import com.serff.planmanagementapi.util.GetMedicaidPlansAPIUtil;
import com.serff.planmanagementapi.util.GetPlanAvailabilityUtil;
import com.serff.planmanagementapi.util.GetPlanDetailsUtil;
import com.serff.planmanagementapi.util.GetPlanInfoAPIUtil;
import com.serff.planmanagementapi.util.GetPlanInfoByZipAPIUtil;
import com.serff.planmanagementapi.util.GetPlanIssuerInfoUtil;
import com.serff.planmanagementapi.util.GetPlansDataUtil;
import com.serff.planmanagementapi.util.GetPlansForUniversalCartAPIUtil;
import com.serff.planmanagementapi.util.GetProviderNetworkAPIUtil;
import com.serff.planmanagementapi.util.GetRefPlanUtil;
import com.serff.planmanagementapi.util.GetVisionPlansAPIUtil;
import com.serff.planmanagementapi.util.HouseholdAPIUtil;
import com.serff.planmanagementapi.util.HouseholdReqAPIUtil;
import com.serff.planmanagementapi.util.MemberLevelEmployeeRateAPIUtil;
import com.serff.planmanagementapi.util.MemberLevelPremiumAPIUtil;
import com.serff.planmanagementapi.util.PlanByIssuerUtil;
import com.serff.planmanagementapi.util.PlanMgmtAPIInput;
import com.serff.planmanagementapi.util.PlanMgmtAPIOutput;
import com.serff.planmanagementapi.util.STMPlanAPIUtil;
import com.serff.planmanagementapi.util.TeaserPlanEligibilityRequestUtil;

/**
 * This is controller to dispatch all the Plan management api related urls to respective pages.
 * 
 * @author vardekar_s
 *
 */
@Controller
public class PlanMgmtAPIController {


	private static final Logger LOGGER = LoggerFactory.getLogger(PlanMgmtAPIController.class);
	private static final String PLAN_MGMT_API_PAGE = "pmAPIRequestResponse";
	private static final String PLAN_MGMT_API_HELP_PAGE = "pmAPIHelp";
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	
	/**
	 * Returns Plan Management API page.
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "admin/getPlanMgmtAPI", method = RequestMethod.GET)
	public String getPlanMgmtAPI() {
		
		LOGGER.info("getPlanMgmtAPI Begin.");
		
		return PLAN_MGMT_API_PAGE;
	}
	
	/**
	 * Return Plan Management API Help page.
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "admin/getPlanMgmtAPIHelp", method = RequestMethod.GET)
	public String getPlanMgmtAPIHelp() {
		
		LOGGER.info("getPlanMgmtAPIHelp Begin.");
		
		return PLAN_MGMT_API_HELP_PAGE;
	}
	/**
	 * Returns JSON request text for PlanInfo API.
	 * 
	 * @return
	 */
	@RequestMapping(value = "admin/getPlanInfoJSONRequest", method = RequestMethod.GET)
	public@ResponseBody GetPlanInfoAPIUtil.PlanRequest getPlanInfoJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getPlanInfoJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetPlanInfoAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * 
	 * @param HttpServletRequest
	 * @return JSON request text for planAvailability API
	 */
	@RequestMapping(value = "admin/getPlanAvailabilityJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRequest getPlanAvailabilityJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getPlanInfoJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);	
		return GetPlanAvailabilityUtil.createJSONRequestForPlanAvailability(inputs, entityManagerFactory);
		
	}
	
	/**
	 * 
	 * @param HttpServletRequest
	 * @return JSON request text for planAvailability API
	 */
	@RequestMapping(value = "admin/getPlansByIssuerJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRequest getplansbyissuerJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getPlanInfoJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);	
		return PlanByIssuerUtil.createJSONRequestForPlanByIssuer(inputs, entityManagerFactory);
		
	}
	
	/**
	 * 
	 * @param request
	 * @return JSON request text
	 */
	@RequestMapping(value = "admin/getPlansDetailsJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRequest getPlansDetailsJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getplansDetailsJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);	
		return GetPlanDetailsUtil.createJSONRequestForPlanDetails(inputs, entityManagerFactory);
		
	}
	
	/**
	 * 
	 * @param request
	 * @return JSON request text
	 */
	@RequestMapping(value = "admin/getIssuerInfoByZipcodeJSONRequest", method = RequestMethod.GET)
	public@ResponseBody IssuerRequest getIssuerInfoByZipcodeJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getIssuerInfoByZipcodeJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);	
		return GetIssuerInfoByZipcodeUtil.createJSONRequestForIssuerInfoByZipcode(inputs, entityManagerFactory);
		
	}
	
	/**
	 * 
	 * @param request
	 * @return JSON request text
	 */
	@RequestMapping(value = "admin/getIssuerInfobyIdJSONRequest", method = RequestMethod.GET)
	public@ResponseBody IssuerRequest getIssuerInfobyIdJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getIssuerInfobyIdJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);	
		return GetIssuerInfobyId.createJSONRequestForgetIssuerInfobyId(inputs, entityManagerFactory);
		
	}
	
	/**
	 * 
	 * @param request
	 * @return JSON request text
	 */
	@RequestMapping(value = "admin/getPlansDataJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRequest getPlansDataJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getIssuerInfobyIdJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);	
		return GetPlansDataUtil.createJSONRequestForPlansdataJSONRequest(inputs, entityManagerFactory);
		
	}
	
	/**
	 * 
	 * @param request
	 * @return JSON request text
	 */
	@RequestMapping(value = "admin/getPlanIssuerInfoJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRequest getPlanIssuerInfoJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getplansDetailsJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);	
		return GetPlanIssuerInfoUtil.createJSONRequestForPlanIssuerInfo(inputs, entityManagerFactory);
		
	}
	
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getFindLowestPremiumPlanRateJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRateBenefitRequest findLowestPremiumPlanRate(HttpServletRequest request){
		
		LOGGER.info("findLowestPremiumPlanRate() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return FindLowestPremiumPlanRateAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSOn request to retrieve issuer disclaimer info.
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getDisclaimerInfoJSONRequest", method = RequestMethod.GET)
	public@ResponseBody IssuerRequest getDisclaimerInfoJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getDisclaimerInfo() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetDisclaimerInfoUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for individual health plan availability.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getCheckIndvHealthPlanAvailabilityJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRequest getCheckIndvHealthPlanAvailability(HttpServletRequest request){
		
		LOGGER.info("getCheckIndvHealthPlanAvailability() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return CheckIndvHealthPlanAvailabilityAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}

	/**
	 * Returns ref plan details for the given input.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getRefplanJSONRequest", method = RequestMethod.GET)
	public@ResponseBody EmployerQuotingDTO getRefPlanPlan(HttpServletRequest request){
		
		LOGGER.info("getRefPlanPlan() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetRefPlanUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request fro Employer Plan Rate.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getEmployerPlanRateJSONRequest", method = RequestMethod.GET)
	public@ResponseBody EmployerQuotingDTO getEmployerPlanRateJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getCheckIndvHealthPlanAvailability() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetEmployerPlanRateAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}	
	
	/**
	 *  Returns JSON request for Household api.
	 *  
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getHouseholdJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRateBenefitRequest getHouseholdJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getHouseholdJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return HouseholdAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON to call Get Dental Plans API.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getDentalPlansJSONRequest", method = RequestMethod.GET)
	public@ResponseBody CrossSellPlanRequestDTO getDentalPlansJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getDentalPlansJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetDentalPlansAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * This method returns valid states list for the plans.
	 */
	@RequestMapping(value = "admin/getValidStates", method = RequestMethod.GET)
	public@ResponseBody Map<String, String> getValidStates(HttpServletRequest request){

		LOGGER.info("getValidStates() Begin.");
		// Check whether State list is saved there in session.
		Map<String, String> validStates = null;
		Object sessionStates = request.getSession().getAttribute("validStates");

		if (null == sessionStates) {
			LOGGER.info("Valid states not found in session. Fetching from database.");
			validStates = GetPlanInfoAPIUtil.getValidStates(entityManagerFactory);
			// Store fetched states in session.
			request.getSession().setAttribute("validStates", validStates);
			return validStates;
		}
		else {
			LOGGER.info("Returning valid states from session : " + validStates);
			return (TreeMap<String, String>) sessionStates;
		}
	}
	
	/**
	 *  Returns JSON request for TeaserPlan Eligibility Request api.
	 *  
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getEligiblityJSONRequest", method = RequestMethod.GET)
	public@ResponseBody EligibilityRequest getTeaserPlanEligReqJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getTeaserPlanEligReqJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return TeaserPlanEligibilityRequestUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for Benefit Cost data API.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getBenefitCostDataJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRequest getBenefitCostDataJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getBenefitCostDataJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetBenefitCostDataAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	
	/**
	 * Returns JSON request for Get Lowest Dental Premium API.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getLowestDentalPremiumJSONRequest", method = RequestMethod.GET)
	public@ResponseBody CrossSellPlanRequestDTO getLowestDentalPremiumJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getLowestDentalPremiumJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetLowestDentalPremiumAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	

	/**
	 * Returns JSON request for Get Member Level Premium API.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getMemberLevelPremiumJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRateBenefitRequest getMemberLevelPremiumJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getMemberLevelPremiumJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return MemberLevelPremiumAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for Get Member Level Employee Rate API.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getMemberLevelEmployeeRateJSONRequest", method = RequestMethod.GET)
	public@ResponseBody MLECRestRequest getMemberLevelEmployeeRateJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getMemberLevelEmployeeRateJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return MemberLevelEmployeeRateAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for Get Employer Min Max Rate API.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getEmployerMinMaxRateJSONRequest", method = RequestMethod.GET)
	public@ResponseBody EmployerRequest getEmployerMinMaxRate(HttpServletRequest request){
		
		LOGGER.info("getMemberLevelEmployeeRateJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);
		return GetEmployerMinMaxRateAPIUtil.createJSONRequest(inputs, entityManagerFactory);
		
	}
	
	/**
	 * Returns JSON request for Get PLan Availability For Employees API.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getCheckPlanAvailabilityForEmployeesJSONRequest", method = RequestMethod.GET)
	public@ResponseBody List<EmployerCoverageDTO> getCheckPlanAvailabilityForEmployeesJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getCheckPlanAvailabilityForEmployeesJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return CheckPlanAvailabilityForEmployeesAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for Get Household Req API.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getHouseholdReqJSONRequest", method = RequestMethod.GET)
	public@ResponseBody HouseHoldRequest getHouseholdReqJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getHouseholdReqJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return HouseholdReqAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for STM Plan API.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getStmplanJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRateBenefitRequest getSTMPlanJSONRequest(HttpServletRequest request){
		
		LOGGER.info("getSTMPlanJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return STMPlanAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns XML request for 	http://webservice.hix.getinsured.com/planmgmt/benchMarkPlan webservice.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getBenchmarkPlanRequestXML", method = RequestMethod.GET)
	public@ResponseBody String getBenchmarkPlanRequestXML(HttpServletRequest request){
		LOGGER.info("getBenchmarkPlanRequestXML() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return BenchMarkPlanAPIUtil.createRequestXML(inputs, entityManagerFactory);
	}
	
	
	/**
	 * Returns JSON request for Plan Details By Zip API.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getPlanInfoByZipJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRequest getPlanInfoByZipJSONRequest(HttpServletRequest request){
		LOGGER.info("getPlanInfoByZipJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetPlanInfoByZipAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for getting Plan Details for Universal Cart.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getPlansForUniversalCartJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRequest getPlansForUniversalCartJSONRequest(HttpServletRequest request){
		LOGGER.info("getPlansForUniversalCartJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetPlansForUniversalCartAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for Benefits and Cost data of given plan ids.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getBenefitsAndCostByPlanIdJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRateBenefitRequest getBenefitsAndCostByPlanIdJSONRequest(HttpServletRequest request){
		LOGGER.info("getBenchmarkPlanRequestXML() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetBenefitsAndCostByPlanIdAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for Benchmark Rate.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getBenchmarkRateJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRateBenefitRequest getBenchmarkRateJSONRequest(HttpServletRequest request){
		LOGGER.info("getBenchmarkRateJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);				
		return GetBenchmarkRateAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for Provider Network.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/getProviderNetworkJSONRequest", method = RequestMethod.GET)
	public@ResponseBody PlanRequest getProviderNetworkJSONRequest(HttpServletRequest request){
		LOGGER.info("getSecondPlanJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);		
		return GetProviderNetworkAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/**
	 * Returns JSON request for Get AME Plans.
	 * @param request
	 * @return 
	 * @return
	 */
	@RequestMapping(value = "admin/getAllAMEplansJSONRequest", method = RequestMethod.GET)
	public@ResponseBody  AmePlanRequestDTO getAMEPlansJSONRequest(HttpServletRequest request){
		LOGGER.info("getAMEPlansJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);		
		return GetAllAMEPlansAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	
	/**
	 * Returns JSON request for Get Vision Plans.
	 * @param request
	 * @return 
	 * @return
	 */
	@RequestMapping(value = "admin/getVisionPlansJSONRequest", method = RequestMethod.GET)
	public@ResponseBody  VisionPlanRequestDTO getVisionPlansJSONRequest(HttpServletRequest request){
		LOGGER.info("getVisionPlansJSONRequest() Begin.");		
		PlanMgmtAPIInput inputs = new PlanMgmtAPIInput(request);	
		return GetVisionPlansAPIUtil.createJSONRequest(inputs, entityManagerFactory);
	}
	
	/* This method will post the JSON to PM API and return rsponse with all other attributes like..size, time required to execute etc.
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "admin/postPMAPIRequest", method = RequestMethod.GET)
	public @ResponseBody PlanMgmtAPIOutput postPMAPIRequest(HttpServletRequest request){
		
		PlanMgmtAPIOutput apiResponse = new PlanMgmtAPIOutput();
		LOGGER.info("postPMAPIRequest() Begin.");	
			try {
				//Retrieve parameters from request object.
				String host = request.getParameter("host");
				String apiName = request.getParameter("apiName");
				String payLoad = request.getParameter("payLoad");
				CloseableHttpClient httpClient = HttpClients.createDefault();
				StringEntity requestEntity = new StringEntity(payLoad, 
						"application/json");
				//Set web service URLS.
				if("planmgmt/benchMarkPlan".equals(apiName)){
					apiName = "webservice/planmgmt/endpoints";
					requestEntity = new StringEntity(payLoad, "text/xml");
				}
				String apiURL = host + "/ghix-planmgmt/" + apiName;
				HttpPost postMethod = new HttpPost(apiURL);
				//PostMethod postMethod = new PostMethod(apiURL);
				postMethod.addHeader("SERFF_CSR_PASSKEY_HEADER","serff94303");
				postMethod.setEntity(requestEntity);
				long startTime = System.currentTimeMillis();
				CloseableHttpResponse response = httpClient.execute(postMethod);
				LOGGER.debug("Status code after calling PM API :" + response.getStatusLine().getStatusCode());	
				String apiResponseString = EntityUtils.toString(response.getEntity());
				long apiExecutionInterval = System.currentTimeMillis() - startTime;
				int size = 0;
				if(null != apiResponseString){
					size = apiResponseString.getBytes("UTF-8").length;
				}
				//Set response parameters.
				apiResponse.setDuration(String.valueOf(apiExecutionInterval));
				apiResponse.setJsonResponse(apiResponseString);
				apiResponse.setMethod("POST");
				apiResponse.setSize(String.valueOf(size));
				apiResponse.setUrl(apiURL);

			} catch (Exception e) {
				LOGGER.error("Error while calling PMAPI " , e);	
				apiResponse.setErrorMessage("Error occurred in PM API : "  +e.getMessage());
			}

		return apiResponse;
	}

	/**
	 * Method is used to get input parameters of planratebenefit/getBenefitsAndCost API.
	 */
	@RequestMapping(value = "admin/getBenefitsAndCostJSONRequest", method = RequestMethod.GET)
	public @ResponseBody PlanRateBenefitRequest getBenefitsAndCost(HttpServletRequest request) {

		LOGGER.info("Beginning executing getBenefitsAndCost().");
		try {
			return BenefitsAndCostAPIUtil.createJSONRequest(new PlanMgmtAPIInput(request), entityManagerFactory);
		}
		finally {
			LOGGER.info("End execution getBenefitsAndCost().");
		}
	}

	/**
	 * Method is used to get input parameters of plan/checkMetalTierAvailability API.
	 */
	@RequestMapping(value = "admin/checkMetalTierAvailabilityJSONRequest", method = RequestMethod.GET)
	public @ResponseBody EmployerQuotingDTO checkMetalTierAvailability(HttpServletRequest request) {

		LOGGER.info("Beginning executing checkMetalTierAvailability().");
		try {
			return CheckMetalTierAvailabilityAPIUtil.createJSONRequest(new PlanMgmtAPIInput(request), entityManagerFactory);
		}
		finally {
			LOGGER.info("End execution checkMetalTierAvailability().");
		}
	}

	/**
	 * Method is used to get input parameters of crosssell/getlowestamepremium API.
	 */
	@RequestMapping(value = "admin/getlowestamepremiumJSONRequest", method = RequestMethod.GET)
	public @ResponseBody CrossSellPlanRequestDTO getLowestAMEPremium(HttpServletRequest request) {

		LOGGER.info("Beginning executing getLowestAMEPremium().");
		try {
			return GetLowestAMEPremiumAPIUtil.createJSONRequest(new PlanMgmtAPIInput(request), entityManagerFactory);
		}
		finally {
			LOGGER.info("End execution getLowestAMEPremium().");
		}
	}

	/**
	 * Method is used to get input parameters of crosssell/getameplans API.
	 */
	@RequestMapping(value = "admin/getameplansJSONRequest", method = RequestMethod.GET)
	public @ResponseBody CrossSellPlanRequestDTO getAMEPlans(HttpServletRequest request) {

		LOGGER.info("Beginning executing getAMEPlans().");
		try {
			return GetAMEPlansAPIUtil.createJSONRequest(new PlanMgmtAPIInput(request), entityManagerFactory);
		}
		finally {
			LOGGER.info("End execution getAMEPlans().");
		}
	}
	
	
	/**
	 * Method is used to create input parameters to get Medicaid Plans API.
	 * 
	 */
	@RequestMapping(value = "admin/getMedicaidPlansJSONRequest", method = RequestMethod.GET)
	public @ResponseBody MedicaidPlanRequestDTO getMedicaidPlansJSONRequest(HttpServletRequest request) {

		LOGGER.info("Beginning executing getMedicaidPlansJSONRequest().");
		try {
			return GetMedicaidPlansAPIUtil.createJSONRequest(new PlanMgmtAPIInput(request), entityManagerFactory);
		}
		finally {
			LOGGER.info("End execution getMedicaidPlansJSONRequest().");
		}
	}
}
