package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class BenchMarkPlanAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(BenchMarkPlanAPIUtil.class);
	public static final String VALID_PLANS_QUERY = "SELECT P.ID FROM PLAN P, PLAN_HEALTH PH, PM_PLAN_RATE PRT, PM_SERVICE_AREA PMS, PM_ZIP_COUNTY_RATING_AREA PMSC"  
			+ " WHERE P.ID = PRT.PLAN_ID " 
			+ " AND PRT.RATING_AREA_ID = PMSC.RATING_AREA_ID "  
			+ " AND PMSC.ZIP = PMS.ZIP "   
			+ " AND PMS.FIPS = PMSC.COUNTY_FIPS "   
			+ " AND P.SERVICE_AREA_ID = PMS.SERVICE_AREA_ID "   
			+ " AND P.INSURANCE_TYPE='HEALTH' "  
			+ " AND P.IS_DELETED='N' "  
			+ " AND P.MARKET='INDIVIDUAL' "  
			+ " AND P.STATUS IN ('CERTIFIED','RECERTIFIED') " 
			+ " AND P.ENROLLMENT_AVAIL = 'AVAILABLE' "  
			+ " AND P.ISSUER_VERIFICATION_STATUS = 'VERIFIED' " 
			+ " AND PH.PLAN_LEVEL = 'SILVER'";

	/**
	 * Returns JSON to retrieve individual health plan availability.
	 * 
	 * @return
	 */
	public static String createRequestXML(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		StringBuilder soapRequest = new StringBuilder();
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("BenchMarkPlanAPIUtil : createJSONRequest() begin.");
		}
		//Get list of valid plans.
		Plan randomPlan = null;
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		
		if(null != entityManager){
			
		try{
			     List<Object> validPlanIds = entityManager.createNativeQuery(VALID_PLANS_QUERY + createWhereClause(inputs)).setMaxResults(TWENTY).getResultList();
				 if(!CollectionUtils.isEmpty(validPlanIds)){
					 int randomPlanIndex = getRandomNumber(0, validPlanIds.size()-1);
					 String planId = (validPlanIds.get(randomPlanIndex)).toString();
					 randomPlan = (Plan)entityManager.find(Plan.class, Integer.valueOf(planId));
				 }else{					
					 LOGGER.error("No valid plans found.");
					 
				 }
			 }catch(Exception e){				
					LOGGER.error("createRequestXML() : Error occurred : " + e.getMessage(), e);				 
			 }finally{
				 	if(entityManager.isOpen()){
						entityManager.clear();
						entityManager.close();
						entityManager = null;
					}
			 }
		 }
		
		if(null != randomPlan){
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			if(null != randomServiceArea){

				soapRequest.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ben=\"http://webservice.hix.getinsured.com/planmgmt/benchMarkPlan\"> ").append("\n");
				soapRequest.append("<soapenv:Header/>").append("\n");
				soapRequest.append("<soapenv:Body>").append("\n");
				soapRequest.append("<ben:getBenchmarkPlanRequest>").append("\n");
				soapRequest.append("<ben:householdCaseId>1234567890</ben:householdCaseId>").append("\n");
				soapRequest.append("<ben:coverageStartDate>" + dateFormat.format(getEffectiveDate(randomPlan)) + "</ben:coverageStartDate>").append("\n");
				soapRequest.append("<ben:members>").append("\n");
				 
				ArrayList<Map<String, String>> members = createMemberList(randomPlan, randomServiceArea);
				
				for(Map<String, String> member : members){
					soapRequest.append("<ben:member>").append("\n");
					soapRequest.append("<ben:relation>"+member.get(RELATION)+"</ben:relation>").append("\n");
					//Get DOB.
					int age = Integer.parseInt(member.get(AGE));
					Calendar cal = Calendar.getInstance();
					cal.setTime (new Date()); 
					cal.add(Calendar.YEAR, -age);
					soapRequest.append("<ben:dob>"+dateFormat.format(cal.getTime())+"</ben:dob>").append("\n");
					soapRequest.append("<ben:zip>"+member.get(ZIP)+"</ben:zip>").append("\n");
					soapRequest.append("<ben:countyCode>"+member.get(COUNTYCODE)+"</ben:countyCode>").append("\n");
					soapRequest.append("<ben:tobacco>"+member.get(TOBACCO)+"</ben:tobacco>").append("\n");
					soapRequest.append("</ben:member>").append("\n");
				}
				
				soapRequest.append("</ben:members>").append("\n");
				soapRequest.append("</ben:getBenchmarkPlanRequest>").append("\n");
				soapRequest.append("</soapenv:Body>").append("\n");
				soapRequest.append("</soapenv:Envelope>");
				
			}
		
		} else {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("No valid plans found for selected criteria : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
		}

			return soapRequest.toString();
	}

	public static String createWhereClause(PlanMgmtAPIInput input) {

		String whereClause = StringUtils.EMPTY;

		if (StringUtils.isNotBlank(input.getAvailableFor())
				&& AVAILABLE_FOR.contains(input.getAvailableFor().toUpperCase())) {
			whereClause += " AND P.AVAILABLE_FOR ='" + input.getAvailableFor() + "' ";
		}

		if (StringUtils.isNotBlank(input.getExchangeType())
				&& EXCHANGE_TYPES.contains(input.getExchangeType().toUpperCase())) {
			whereClause += " AND P.EXCHANGE_TYPE ='" + input.getExchangeType() + "' ";
		}

		if (StringUtils.isNotBlank(input.getHealthSavingAccount())
				&& HSA.contains(input.getHealthSavingAccount().toUpperCase())) {
			whereClause += " AND P.HSA ='" + input.getHealthSavingAccount() + "' ";
		}

		if (StringUtils.isNotBlank(input.getNetworkType())
				&& NETWORK_TYPES.contains(input.getNetworkType().toUpperCase())) {
			whereClause += " AND P.NETWORK_TYPE ='" + input.getNetworkType() + "' ";
		}

		if (StringUtils.isNotBlank(input.getPlanYear())
				&& PLAN_YEARS.contains(input.getPlanYear().toUpperCase())) {
			whereClause += " AND P.APPLICABLE_YEAR ='" + input.getPlanYear() + "' ";
		}

		if (StringUtils.isNotBlank(input.getState())
				&& validateState(input.getState())) {
			whereClause += " AND P.STATE ='" + input.getState() + "' ";
		}
		return whereClause;
	}
}
