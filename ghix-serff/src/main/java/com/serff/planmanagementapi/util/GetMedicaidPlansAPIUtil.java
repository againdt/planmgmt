package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.MedicaidPlanRequestDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * --------------------------------------------------------------------------------------
 * HIX-58979 : SERFF API Test Page - Update
 * --------------------------------------------------------------------------------------
 * 
 * Utility class to generate sample JSON request to test ghix-planmgmt/medicaidplan/getmedicaidplans API.
 * 
 */
public class GetMedicaidPlansAPIUtil extends PlanMgmtAPIUtil{

	private static final Logger LOGGER = LoggerFactory.getLogger(GetMedicaidPlansAPIUtil.class);

	/**
	 * Returns JSON to retrieve Medicaid Plans availability.
	 * 
	 * @param inputs - Input parameters selected from UI screen
	 * @param entityManagerFactory - The entity manager factory instance
	 * @return - MedicaidPlanRequestDTO instnace with sample values populated from database.
	 */
	public static MedicaidPlanRequestDTO createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		
		MedicaidPlanRequestDTO medicaidPlanRequestDTO = null;
		LOGGER.debug("GetMedicaidPlansAPIUtil : createJSONRequest() begin.");
		//Select only MEDICAID plans.
		inputs.setPlanType("MEDICAID");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found Medicaid plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			if(null != randomServiceArea){
				
				medicaidPlanRequestDTO = new MedicaidPlanRequestDTO();
				medicaidPlanRequestDTO.setCountyCode(randomServiceArea.getFips());
				medicaidPlanRequestDTO.setEffectiveDate(dateFormat.format(getEffectiveDate(randomPlan)));
				medicaidPlanRequestDTO.setHouseholdCaseId("id1");
				medicaidPlanRequestDTO.setZipCode(randomServiceArea.getZip());
				
			}
		}
		
		return medicaidPlanRequestDTO;
	}

}
