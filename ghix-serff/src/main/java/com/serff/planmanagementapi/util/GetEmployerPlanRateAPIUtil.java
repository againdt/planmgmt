package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.shop.employer.eps.EmployerQuotingDTO;
import com.getinsured.hix.dto.shop.employer.eps.PlanDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class GetEmployerPlanRateAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetEmployerPlanRateAPIUtil.class);
	private static final String RELATION = "relation";
	private static final String AGE = "age";
	private static final String TOBACCO = "tobacco";
	private static final int TWENTY_ONE = 21;
	private static final int FIFTY = 50;
	private static final String ID="id";
	
	/**
	 * Returns JSON to retrieve Employer plan rate.
	 * 
	 * @return
	 */
	public static EmployerQuotingDTO createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		
		EmployerQuotingDTO employerQuotingDTO = null;
		LOGGER.debug("GetEmployerPlanRateAPIUtil : createJSONRequest() begin.");
		//HEALTH plans only.
		inputs.setPlanType("HEALTH");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			if(null != randomServiceArea){
				employerQuotingDTO = new EmployerQuotingDTO();
				//Set parameters.//YYYY-MM-DD
				employerQuotingDTO.setEffectiveDate(dateFormat.format(getEffectiveDate(randomPlan)));
				employerQuotingDTO.setEmpCountyCode(randomServiceArea.getFips());
				employerQuotingDTO.setEmpPrimaryZip(randomServiceArea.getZip());
				ArrayList<ArrayList<ArrayList<Map<String, String>>>> censusList = new ArrayList<ArrayList<ArrayList<Map<String, String>>>>();
				ArrayList<ArrayList<Map<String, String>>> list = new ArrayList<ArrayList<Map<String, String>>>();
				ArrayList<PlanDTO> planDTOList = new ArrayList<PlanDTO>();
				//Create PlanDTO
				PlanDTO planDTO = new PlanDTO();
				planDTO.setPlanId(randomPlan.getId());
				planDTO.setTierName("TestTierName");
				//Add in list.
				planDTOList.add(planDTO);
				//Set list attributes.
				employerQuotingDTO.setPlanDTOList(planDTOList);
				list.add(createMemberList(randomPlan));
				censusList.add(list);
				employerQuotingDTO.setCensusList(censusList);
			}
		}
		
		return employerQuotingDTO;
	}
	
	/**
	 * Returns member list created using Plan attributes.
	 * 
	 * @param randomPlan
	 * @return
	 */
	public static ArrayList<Map<String, String>> createMemberList(Plan randomPlan){
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Random Plan: " + randomPlan);
		}
		ArrayList<Map<String, String>> memberList = new ArrayList<Map<String, String>>();
		//Create Adult member only.
		Map<String, String> adultMember = new HashMap<String, String>();
		adultMember.put(String.valueOf(AGE), String.valueOf(getRandomNumber(TWENTY_ONE, FIFTY)));
		adultMember.put(TOBACCO, "N");
		adultMember.put(RELATION, "self");
		adultMember.put(ID, "1111");
		
		memberList.add(adultMember);
		
		return memberList;
	}
	
	
}
