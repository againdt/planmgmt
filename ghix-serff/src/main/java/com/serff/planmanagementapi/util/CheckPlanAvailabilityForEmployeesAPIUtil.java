package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.shop.EmployeeCoverageDTO;
import com.getinsured.hix.dto.shop.EmployerCoverageDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class CheckPlanAvailabilityForEmployeesAPIUtil extends PlanMgmtAPIUtil{


	private static final Logger LOGGER = LoggerFactory.getLogger(CheckPlanAvailabilityForEmployeesAPIUtil.class);
	private static final int EMP_ID = 1234;

	/**
	 * Returns JSON request for Check Plan Availability For Employees API.
	 *
	 * @param inputs
	 * @param entityManagerFactory
	 * @return
	 */
	public static List<EmployerCoverageDTO> createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		List<EmployerCoverageDTO> employerCoverageDTOList = null;
		LOGGER.debug("CheckPlanAvailabilityForEmployeesAPIUtil : createJSONRequest() begin.");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);

		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : "
					+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			
			if(null != randomServiceArea){

				employerCoverageDTOList = new ArrayList<EmployerCoverageDTO>();

				EmployerCoverageDTO employerCoverageDTO = new EmployerCoverageDTO();
				employerCoverageDTO.setCoverageStartDate(dateFormat.format(getEffectiveDate(randomPlan)));

				List<EmployeeCoverageDTO> employeeCoverageDTOList = new ArrayList<EmployeeCoverageDTO>();

				EmployeeCoverageDTO employeeCoverageDTO = new EmployeeCoverageDTO();
				employeeCoverageDTO.setCounty(randomServiceArea.getFips());
				employeeCoverageDTO.setCountyCode(randomServiceArea.getCounty());
				employeeCoverageDTO.setEmployeeId(EMP_ID);
				employeeCoverageDTO.setHiosPlanNumber(randomPlan.getIssuerPlanNumber());
				employeeCoverageDTO.setZip(randomServiceArea.getZip());

				employeeCoverageDTOList.add(employeeCoverageDTO);

				employerCoverageDTO.setEmployeeCoverage(employeeCoverageDTOList);

				employerCoverageDTOList.add(employerCoverageDTO);
			}
		}

		return employerCoverageDTOList;
	}

}
