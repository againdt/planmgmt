package com.serff.planmanagementapi.util;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.util.SecurityUtil;

public class GetPlanInfoAPIUtil extends PlanMgmtAPIUtil implements Serializable {

	private static final long serialVersionUID = -6828981742592281492L;
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetPlanInfoAPIUtil.class);

	/**
	 *
	 * @return
	 */
	public static PlanRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		LOGGER.debug("createGetPlanInfoJSONRequest() begin.");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);

		GetPlanInfoAPIUtil.PlanRequest plan = new GetPlanInfoAPIUtil().new PlanRequest();
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
//			plan = new GetPlanInfoAPIUtil().new PlanRequest();
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			plan.setPlanId(String.valueOf(validPlans.get(randomPlanIndex).getId()));
		} else {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("No valid plans found for selected state : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			return null;
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Plan id in request : " + plan.getPlanId());
		}
		return plan;
	}

	/**
	 * Class to create Planrequest JSON.
	 *
	 * @author Shashikant
	 *
	 */
	public class PlanRequest implements Serializable {

		private static final long serialVersionUID = 562459696612151167L;

		private String planId;

		public String getPlanId() {
			return planId;
		}

		public void setPlanId(String planId) {
			this.planId = planId;
		}

	}

}
