package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.EmployerRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class GetEmployerMinMaxRateAPIUtil extends PlanMgmtAPIUtil{

	
	private static final Logger LOGGER = LoggerFactory.getLogger(GetEmployerMinMaxRateAPIUtil.class);
	
	/**
	 * Returns JSON request for Household API. 
	 * 
	 * @param inputs
	 * @param entityManagerFactory
	 * @return
	 */
	public static EmployerRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		EmployerRequest employerRequest = null;
		LOGGER.debug("GetEmployerPlanRateAPIUtil : createJSONRequest() begin.");
		//SHOP only plans required.
		inputs.setPlanType("DENTAL");
		inputs.setMarket("SHOP");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			if(null != randomServiceArea){
				employerRequest = new EmployerRequest();
				//Set Employer level fields.
				employerRequest.setEmpCountyCode(randomServiceArea.getFips());
				employerRequest.setEmpPrimaryZip(randomServiceArea.getZip());
				employerRequest.setEffectiveDate(dateFormat.format(getEffectiveDate(randomPlan)));
				employerRequest.setInsType(randomPlan.getInsuranceType());
				//Add Employee data.
				ArrayList<ArrayList<ArrayList<Map<String, String>>>> employeeList = new ArrayList<ArrayList<ArrayList<Map<String, String>>>>();
				ArrayList<Map<String, String>> employeeData = createMemberList(randomPlan, randomServiceArea);
				//First level ArrayList.
				ArrayList<ArrayList<Map<String, String>>> employeeFirstLevelList = new ArrayList<ArrayList<Map<String, String>>>();
				employeeFirstLevelList.add(employeeData);
				//Second level ArrayList.
				employeeList.add(employeeFirstLevelList);
				employerRequest.setEmployeeList(employeeList);
			}
		}
		
		return employerRequest;
	}

}
