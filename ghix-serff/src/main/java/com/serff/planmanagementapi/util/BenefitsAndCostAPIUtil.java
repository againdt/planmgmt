package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * Class is used to provide utility inputs to planratebenefit/getBenefitsAndCost API.
 * 
 * @author Bhavin Parmar
 * @since December 09, 2014
 */
public class BenefitsAndCostAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(BenefitsAndCostAPIUtil.class);
	private static final int MAX_SIZE = 5;

	/**
	 * Returns JSON to retrieve issuer disclaimer information.
	 */
	public static PlanRateBenefitRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		LOGGER.info("Beginning executing createJSONRequest().");
		PlanRateBenefitRequest request = null;
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		try {
			List<Plan> validPlansList = getValidPlans(inputs, entityManagerFactory);

			if (CollectionUtils.isEmpty(validPlansList)) {
				LOGGER.error("No valid plans found for selected criteria : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
				return request;
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans("+ validPlansList.size() +") for selected state: " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}

			Plan plan = null;
			String startDate = null;
			List<Integer> planIdList = new ArrayList<Integer>();
			for (int i = 0; MAX_SIZE > i && i < validPlansList.size(); i++) {
				plan = validPlansList.get(i);
				planIdList.add(plan.getId());

				// Get start date from Plan
				if (i == 0) {
					startDate = dateFormat.format(plan.getStartDate());
				}
			}

			Map<String, Object> requestParameters = new HashMap<String, Object>();
			requestParameters.put("planIds", planIdList);
			requestParameters.put("insuranceType", inputs.getPlanType());
			requestParameters.put("coverageStartDate", startDate);
			
			request = new PlanRateBenefitRequest();
			request.setRequestParameters(requestParameters);
		}
		finally {
			LOGGER.info("End execution createJSONRequest().");
		}
		return request;
	}
}
