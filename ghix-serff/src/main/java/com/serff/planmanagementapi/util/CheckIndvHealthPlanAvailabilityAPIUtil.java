package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class CheckIndvHealthPlanAvailabilityAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CheckIndvHealthPlanAvailabilityAPIUtil.class);
	private static final int exchanegTypeIndex = 1;

	/**
	 * Returns JSON to retrieve individual health plan availability.
	 * 
	 * @return
	 */
	public static PlanRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		PlanRequest planRequest = null;
		LOGGER.debug("CheckIndvHealthPlanAvailabilityAPIUtil : createJSONRequest() begin.");
		//Only helath plans and Individual market should be selected irrespective of user selection.
		inputs.setPlanType("HEALTH");
		inputs.setMarket("INDIVIDUAL");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			if(null != randomServiceArea){
				planRequest = new PlanRequest();
				//Set parameters.
				planRequest.setCountyFIPS(randomServiceArea.getFips());
				planRequest.setZip(randomServiceArea.getZip());
				Date effectiveDate = getEffectiveDate(randomPlan);
				//Format date to format YYYY-MM-DD.
				planRequest.setEffectiveDate(String.valueOf(dateFormat.format(effectiveDate)));
				if(StringUtils.isEmpty(inputs.getExchangeType())){
					if(getRandomNumber(0, exchanegTypeIndex) == 0){
						planRequest.setExchangeType("ON");
					}else{
						planRequest.setExchangeType("OFF");
					}
				}else{
					planRequest.setExchangeType(inputs.getExchangeType());
				}
				//using default csr value. API will set csr value as "CS1" when passed null from here.
				planRequest.setCsrValue(null);
			}
		
		} else {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("No valid plans found for selected criteria : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
		}

			return planRequest;
	}
	
}
