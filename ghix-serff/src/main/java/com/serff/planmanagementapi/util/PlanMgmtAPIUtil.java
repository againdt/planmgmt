package com.serff.planmanagementapi.util;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.getinsured.hix.model.IssuerServiceArea;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.template.admin.niem.usps.states.USStateCodeSimpleType;
import com.serff.util.SerffConstants;

@Service
public class PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanMgmtAPIUtil.class);
	
	/**
	 * This query fetches distinct states for all valid plans.
	 */
	private static final String STATE_NAME_LIST_QUERY = "select distinct state, DECODE(state_name, NULL, state, state_name) "
	        + "from zipcodes where state in (select distinct state from plan where status in ('CERTIFIED','RECERTIFIED') "
	        + "and enrollment_avail = 'AVAILABLE' and issuer_verification_status = 'VERIFIED' and is_deleted='N') order by state ";
	private static final String jpaQuery = "Select plan from Plan plan "
			+ "LEFT JOIN fetch plan.planHealth ph "
			+ "LEFT JOIN fetch plan.planDental pd "
			+ "LEFT JOIN fetch plan.issuer planIssuer "
			+ "where plan.isDeleted='N' "
			+ "and plan.issuerVerificationStatus='VERIFIED' ";
	private static final String jpaQueryForServiceArea = "select svcArea from ServiceArea svcArea "
			+ "where svcArea.serviceAreaId=:serviceAreaId";
	private static final String jpaQueryForIssuerServiceArea = "select issuerSvcArea from IssuerServiceArea issuerSvcArea "
			+ "where issuerSvcArea.id=:id";
	private static final String PM_SERVICE_AREA_QUERY = "select zip, county, fips from pm_service_area where zip in "
			+ "(select zipcode from zipcodes where state = '%%') and is_deleted='N' ";
	private static final String ENROLL_AVAIL_PLAN_TYPES = "_ANY_HEALTH_DENTAL_";
	protected static final String EXCHANGE_TYPES = "_ANY_ON_OFF_";
	protected static final String PLAN_TYPES = "_ANY_HEALTH_DENTAL_STM_AME_";
	protected static final String PLAN_LEVELS = "_ANY_PLATINUM_CATASTROPHIC_GOLD_SILVER_BRONZE_";
	protected static final String MARKETS = "_ANY_INDIVIDUAL_SHOP_";
	protected static final String NETWORK_TYPES = "_ANY_EPO_POS_HMO_PPO_INDEMNITY_";
	protected static final String HSA = "_ANY_YES_NO_";
	protected static final String AVAILABLE_FOR = "_ANY_ADULTONLY_CHILDONLY_ADULTANDCHILD_";
	protected static final String PLAN_YEARS = "_2014_2015_2016_";
	
	@Autowired
	private ISerffPlanRepository iPlanRepository;

	public static final int TWENTY = 20;
	public static final int FIFTY = 50;
	public static final int TEN = 10;
	public static final int SEVENTEEN = 17;
	public static final int MAX_RESULTS = 20;
	public static final int ONE = 1;
	public static final String RELATION = "relation";
	public static final String AGE = "age";
	public static final String TOBACCO = "tobacco";
	public static final int TWENTY_ONE = 21;
	public static final String ID="id";
	public static final String ZIP="zip";
	public static final String COUNTYCODE="countycode";
	public static final String GENDER="gender";
	
	/**
	 * Gives effective start date for given plan. If enrollment available date is available for current plan then it returns that date.
	 * Else it returns last day i.e. 31 Dec of applicable year.
	 * 
	 * @param plan
	 * @return
	 */
	public static Date getEffectiveDate(Plan plan)
	{
		Calendar effectiveDate = Calendar.getInstance();
		if(null != plan.getEnrollmentAvailEffDate()){
			Date currentDate = new Date();
			if(currentDate.after(plan.getEnrollmentAvailEffDate()) 
					&& (plan.getEndDate() != null && plan.getEndDate().after(currentDate))){
				//If current date is after getEnrollmentAvailEffDate and before end date
				//return today + 1
				effectiveDate.setTime(currentDate);
				effectiveDate.add(Calendar.DAY_OF_MONTH, ONE);
				return effectiveDate.getTime();
			}else{
				//if current date is before getEnrollmentAvailEffDate
				//return getEnrollmentAvailEffDate
				return plan.getEnrollmentAvailEffDate();
			}
			
		}else{
			//Not possible condition as every AVAILABLE plan has EnrollmentAvailEffDate.
			return new Date();
		}
	}
	
	public static boolean validateState(String stateCode) {
		
		boolean flag = false;
		
		try {
			USStateCodeSimpleType.valueOf(stateCode);
			flag = true;
		}
		catch (Exception ex) {
			LOGGER.debug("Invalid State: " + ex.getMessage(), ex);
		}
		return flag;
	}
	
	/**
	 * 
	 * @return
	 */
	public static Date getParentDOB() {

		int randomParentAge = getRandomNumber(TWENTY, FIFTY);

		Calendar currentDate = Calendar.getInstance();
		LOGGER.debug("Current date : " + (currentDate.get(Calendar.MONTH) + 1)
				+ "-" + currentDate.get(Calendar.DATE) + "-"
				+ currentDate.get(Calendar.YEAR));

		currentDate = Calendar.getInstance();
		currentDate.add(Calendar.YEAR, -randomParentAge);

		return currentDate.getTime();
	}

	/**
	 * 
	 * @return
	 */
	public static Date getChildDOB() {

		int randomChildAge = getRandomNumber(TEN, SEVENTEEN);

		Calendar currentDate = Calendar.getInstance();
		LOGGER.debug("Current date : " + (currentDate.get(Calendar.MONTH) + 1)
				+ "-" + currentDate.get(Calendar.DATE) + "-"
				+ currentDate.get(Calendar.YEAR));

		currentDate = Calendar.getInstance();
		currentDate.add(Calendar.YEAR, -randomChildAge);

		return currentDate.getTime();
	}

	/**
	 * This gives random number between two given numbers.
	 * 
	 * @param start range
	 * @param end range
	 * @return
	 */
	public static int getRandomNumber(int start, int end) {
		SecureRandom random = new SecureRandom();
		int begin = start;
		int stop = end;

		if (begin > stop) {
			begin = begin + stop;
			stop = begin - stop;
			begin = begin - stop;
		}
		long range = (long) stop - (long) begin + 1;
		long fraction = (long) (range * random.nextDouble());
		return (int) (fraction + begin);
	}

	/**
	 * Get Plan ID for given input data.
	 * 
	 * @param state
	 * @return
	 */
	public String getPlanID(PlanMgmtAPIInput input) {

		List<Plan> validPlans = iPlanRepository.findByStateAndIsDeleted(
				input.getState(), SerffConstants.NO_ABBR);
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found valid plans for selected state : " + SecurityUtil.sanitizeForLogging(input.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			return String.valueOf(validPlans.get(randomPlanIndex).getId());
		} else {
			LOGGER.error("No valid plans found for selected state : "
					+ SecurityUtil.sanitizeForLogging(input.getState()));
			return null;
		}
	}

	/**
	 * This method returns list of valid states where plans are available.
	 * 
	 * @param entityManagerFactory
	 * @return
	 */
	public static Map<String, String> getValidStates(EntityManagerFactory entityManagerFactory){
		
		LOGGER.debug("getValidStates() begin. : ");
		 
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		Map<String, String> stateNames = null;
		
		try {
			if (null == entityManager) {
				LOGGER.error("Entity Manager is null");
				return stateNames;
			}
			List<Object[]> states = entityManager.createNativeQuery(STATE_NAME_LIST_QUERY).getResultList();
			
			if (null == states) {
				LOGGER.error("No valid states found.");
				return stateNames;
			}
			stateNames = new TreeMap<String, String>();
			
			for (Object[] state : states) {
				if (null != state[0]) {
					stateNames.put(state[0].toString(),
							null == state[1] ? "" : state[1].toString());
				}
			}
			LOGGER.debug("State names : " + stateNames);
		
		}
		catch (Exception e) {
			LOGGER.error("getValidStates() : Error occurred : " + e.getMessage(), e);
		}
		finally {
			if (null != entityManager) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}
		return stateNames;
	}

	/**
	 * Prepare query to get valid plans for given inputs.
	 * @param input
	 * @return
	 */
	public static List<Plan> getValidPlans(PlanMgmtAPIInput input, EntityManagerFactory entityManagerFactory){
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		List<Plan> validPlans = null;
		
		try{
			if (null == entityManager) {
				LOGGER.error("Entity Manager is null");
				return validPlans;
			}
			
			String jpaQuery = createParameterizedJPAQuery(input);
			LOGGER.info("Dynamic JPA query created : " + jpaQuery);
			Query jpqlQuery = entityManager.createQuery(jpaQuery);
			// set parameters to JPA query as per user selection.
			setParametersInJPAQuery(jpqlQuery, input);
			// This saves time. No need to go for full table scan.
			jpqlQuery.setMaxResults(MAX_RESULTS);
			// Execute JPA query and return plans.
			validPlans = jpqlQuery.getResultList();
			LOGGER.info("Valid plans size = " + validPlans.size());
		}
		catch(Exception e) {
			LOGGER.error("getValidPlans() : Error occurred :" + e.getMessage(), e);
		}
		finally {
			if (null != entityManager) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}
		return validPlans;
	}

	
	/**
	 * This method adds parameters dynamically to JPA query.
	 * @param input
	 * @return
	 */
	private static String createParameterizedJPAQuery(PlanMgmtAPIInput input){
		
		String parameterizedJPAQuery = jpaQuery;
		//Set main Query clause.
		if(!StringUtils.isEmpty(input.getPlanType()) && ENROLL_AVAIL_PLAN_TYPES.contains(input.getPlanType())){
			parameterizedJPAQuery += " and plan.enrollmentAvail='AVAILABLE' "; 
		}				
		//Add parameters dynamically to JPA query as per user selection.
		if(!StringUtils.isEmpty(input.getAvailableFor())){
			parameterizedJPAQuery += " and plan.availableFor=:availableFor " ;
		}
		if(!StringUtils.isEmpty(input.getExchangeType())){
			parameterizedJPAQuery += " and plan.exchangeType=:exchangeType " ;
		}
		if(!StringUtils.isEmpty(input.getHealthSavingAccount())){
			parameterizedJPAQuery += " and plan.hsa=:hsa " ;
		}
		if(!StringUtils.isEmpty(input.getMarket())){
			parameterizedJPAQuery += " and plan.market=:market " ;
		}
		if(!StringUtils.isEmpty(input.getNetworkType())){
			parameterizedJPAQuery += " and plan.networkType=:networkType " ;
		}
		if(!StringUtils.isEmpty(input.getPlanLevel())){
			parameterizedJPAQuery += " and (ph.planLevel=:healthPlanLevel or pd.planLevel=:dentalPlanLevel) " ;
		}
		if(!StringUtils.isEmpty(input.getPlanType())){
			parameterizedJPAQuery += " and plan.insuranceType=:insuranceType " ;
		}
		if(!StringUtils.isEmpty(input.getPlanYear())){
			parameterizedJPAQuery += " and plan.applicableYear=:applicableYear " ;
		}
		if(!StringUtils.isEmpty(input.getState())){
			parameterizedJPAQuery += " and plan.state=:state " ;
		}	
		
		return parameterizedJPAQuery;
	}

	/**
	 * This method sets selected parameters in the query.
	 * 
	 * @param jpqlQuery
	 * @param input
	 * @return
	 */
	private static void setParametersInJPAQuery(Query jpqlQuery, PlanMgmtAPIInput input){
		
		//Set parameters to JPA query as per user selection.
		if(!StringUtils.isEmpty(input.getAvailableFor())){
			
			if("ADULTONLY".equalsIgnoreCase(input.getAvailableFor())){
				jpqlQuery.setParameter("availableFor", Plan.AVAILABLE_FOR.ADULTONLY);
			}
			else if("CHILDONLY".equalsIgnoreCase(input.getAvailableFor())){
				jpqlQuery.setParameter("availableFor", Plan.AVAILABLE_FOR.CHILDONLY);
			}
			else if("ADULTANDCHILD".equalsIgnoreCase(input.getAvailableFor())){
				jpqlQuery.setParameter("availableFor", Plan.AVAILABLE_FOR.ADULTANDCHILD);
			}
		}
		if(!StringUtils.isEmpty(input.getExchangeType())){
			jpqlQuery.setParameter("exchangeType",input.getExchangeType());
		}
		if(!StringUtils.isEmpty(input.getHealthSavingAccount())){
			jpqlQuery.setParameter("hsa", input.getHealthSavingAccount());
		}
		if(!StringUtils.isEmpty(input.getMarket())){
			jpqlQuery.setParameter("market", input.getMarket());
		}
		if(!StringUtils.isEmpty(input.getNetworkType())){
			jpqlQuery.setParameter("networkType", input.getNetworkType());
		}
		if(!StringUtils.isEmpty(input.getPlanLevel())){
			jpqlQuery.setParameter("healthPlanLevel", input.getPlanLevel());
			jpqlQuery.setParameter("dentalPlanLevel", input.getPlanLevel());
		}
		if(!StringUtils.isEmpty(input.getPlanType())){
			jpqlQuery.setParameter("insuranceType", input.getPlanType());
		}
		if(!StringUtils.isEmpty(input.getPlanYear())){
			jpqlQuery.setParameter("applicableYear", Integer.parseInt(input.getPlanYear().trim()));
		}
		if(!StringUtils.isEmpty(input.getState())){
			jpqlQuery.setParameter("state", input.getState());
		}
		
	}
	
	/**
	 * Returns randomly selected service area details for provided plan. Fetches all records where plan service area id matches and 
	 * returns one randomly selected service area.
	 * 
	 * @param plan
	 * @return
	 */
	public static ServiceArea getServiceAreaDetailsForPlan(Plan plan, EntityManagerFactory entityManagerFactory) {
		
		LOGGER.info("getServiceAreaDetailsForPlan() begin.");
		ServiceArea serviceArea = null;
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		try {
			if (null == entityManager) {
				LOGGER.error("Entity Manager is null");
				return serviceArea;
			}
			//For STM and AME Plans ServiceArea will be null.
			if (null == plan.getServiceAreaId()) {
				ServiceArea svcArea = new ServiceArea();
				
				List<Object[]> serviceAreaList = entityManager.createNativeQuery(PM_SERVICE_AREA_QUERY.replace("%%", plan.getState())).setMaxResults(ONE).getResultList();
				if(null != serviceAreaList){
					for (Object[] area : serviceAreaList) {
						svcArea.setZip(area[0] == null ? "" : area[0].toString());
						svcArea.setCounty(area[1] == null ? "" : area[1].toString());
						svcArea.setFips(area[2] == null ? "" : area[2].toString());
						
						return svcArea;
					}
				}else{
					LOGGER.error("Invalid data. Service area not found for plan state.");
				}
				
			}    
			
			//For other plans ServiceArea Id will never be null.
			Query jpqlQueryForIssuerSvc = entityManager.createQuery(jpaQueryForIssuerServiceArea);
			jpqlQueryForIssuerSvc.setParameter("id", plan.getServiceAreaId());
			IssuerServiceArea issuerSvcArea = (IssuerServiceArea)jpqlQueryForIssuerSvc.getSingleResult();
			
			if (null != issuerSvcArea) {
				Query jpqlQuery = entityManager.createQuery(jpaQueryForServiceArea);
				jpqlQuery.setParameter("serviceAreaId", issuerSvcArea);
				//Execute JPA query.
				List<ServiceArea> serviceAreas = jpqlQuery.getResultList();
				
				if (!CollectionUtils.isEmpty(serviceAreas)) {
					int randomIndex = getRandomNumber(0, serviceAreas.size() - 1);
					serviceArea = serviceAreas.get(randomIndex);
				}
				else {
					// Not possible condition.
					LOGGER.error("Service area not found for plan.");
				}
			}
		}
		catch(Exception e) {
			LOGGER.error("getServiceAreaDetailsForPlan() : Error occurred  : " + e.getMessage(), e);
		}
		finally {
			if (null != entityManager) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}
		return serviceArea;
	}
	
	/**
	 * Returns member list created using Plan attributes.
	 * 
	 * @param randomPlan
	 * @return
	 */
	public static ArrayList<Map<String, String>> createMemberList(Plan randomPlan, ServiceArea serviceArea){
		
		ArrayList<Map<String, String>> memberList = new ArrayList<Map<String, String>>();
		
		Map<String, String> adultMember = new HashMap<String, String>();
		adultMember.put(AGE, String.valueOf(getRandomNumber(TWENTY_ONE, FIFTY)));
		adultMember.put(TOBACCO, "N");
		adultMember.put(RELATION, "self");
		adultMember.put(ID, "1111");
		adultMember.put(ZIP, serviceArea.getZip());
		adultMember.put(COUNTYCODE, serviceArea.getFips());
		adultMember.put(GENDER, "Male");
		
		Map<String, String> childMember = new HashMap<String, String>();
		childMember.put(AGE, String.valueOf(getRandomNumber(0, TWENTY_ONE)));
		childMember.put(TOBACCO, "N");
		childMember.put(RELATION, "child");
		childMember.put(ID, "2222");
		childMember.put(ZIP, serviceArea.getZip());
		childMember.put(COUNTYCODE, serviceArea.getFips());
		adultMember.put(GENDER, "Male");
		
		if(randomPlan.getAvailableFor() == Plan.AVAILABLE_FOR.ADULTONLY){
			memberList.add(adultMember);
		}else if(randomPlan.getAvailableFor() == Plan.AVAILABLE_FOR.CHILDONLY){ 
			memberList.add(childMember);
		}else{
			memberList.add(adultMember);
			memberList.add(childMember);
		}
		
		return memberList;
	}
	
}
