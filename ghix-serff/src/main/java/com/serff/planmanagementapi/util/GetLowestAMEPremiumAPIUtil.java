package com.serff.planmanagementapi.util;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.CrossSellPlanRequestDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * Class is used to provide utility inputs to crosssell/getlowestamepremium API.
 * 
 * @author Bhavin Parmar
 * @since December 11, 2014
 */
public class GetLowestAMEPremiumAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetLowestAMEPremiumAPIUtil.class);

	/**
	 * Returns JSON to retrieve issuer disclaimer information.
	 */
	public static CrossSellPlanRequestDTO createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		LOGGER.info("Beginning executing createJSONRequest().");
		CrossSellPlanRequestDTO request = null;

		try {
			inputs.setPlanType(Plan.PlanInsuranceType.AME.name());
			inputs.setExchangeType(Plan.EXCHANGE_TYPE.OFF.name());
			List<Plan> validPlansList = getValidPlans(inputs, entityManagerFactory);

			if (CollectionUtils.isEmpty(validPlansList)) {
				if(LOGGER.isErrorEnabled()) {
					LOGGER.error("No valid AME plans found for selected criteria : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
				}
				return request;
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found AME plans("+ validPlansList.size() +") for selected state: " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}

			int randomPlanIndex = getRandomNumber(0, validPlansList.size() - 1);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Random AME Plan Index is: " + randomPlanIndex);
			}
			Plan randomPlan = validPlansList.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);

			if (null != randomServiceArea) {
				request = new CrossSellPlanRequestDTO();
				request.setExchangeType(randomPlan.getExchangeType());
				request.setMemberList(createMemberList(randomPlan, randomServiceArea));
			}
			else {
				if(LOGGER.isErrorEnabled()) {
					LOGGER.error("Service Area is not available.");
				}
			}
		}
		finally {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("End execution createJSONRequest().");
			}
		}
		return request;
	}
}
