package com.serff.planmanagementapi.util;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class FindLowestPremiumPlanRateAPIUtil extends PlanMgmtAPIUtil{
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(FindLowestPremiumPlanRateAPIUtil.class);
	private static final long DAY_IN_MILLIS = 86400000;
	private static final int ONE = 1;

	/**
	 * Returns JSON to retrieve issuer disclaimer information.
	 * 
	 * @return
	 */
	public static PlanRateBenefitRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		LOGGER.debug("FindLowestPremiumPlanRateAPIUtil : createJSONRequest() begin.");
		//Select only HEALTH plans.
		inputs.setPlanType("HEALTH");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		PlanRateBenefitRequest planRateBenefitRequest = null;
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea serviceArea= getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			if(null != serviceArea){
				planRateBenefitRequest = new PlanRateBenefitRequest();
				Calendar c = Calendar.getInstance(); 
				//Set issuer parameters.
				Map<String, Object> requestParameters = new HashMap<String, Object>();
				//Set random DOB.
				c.setTime(getParentDOB());
				requestParameters.put("dob", c.get(Calendar.MONTH) + "/" + c.get(Calendar.DATE) + "/" +  c.get(Calendar.YEAR));
				requestParameters.put("zip", serviceArea.getZip());
				requestParameters.put("employerPrimaryZip", serviceArea.getZip());
				Date startDate = randomPlan.getStartDate();
				Date endDate = randomPlan.getEndDate();
				
				if(null != startDate && null!= endDate){
					//MM-DD-YYYY
					long timeDifference = endDate.getTime() - startDate.getTime();
					int timeDifferenceInDays = (int)(timeDifference/DAY_IN_MILLIS);
					c.setTime(startDate); 
					c.add(Calendar.DATE, getRandomNumber(ONE, timeDifferenceInDays));
				}else{
					requestParameters.put("coverageStartDate", "");
					c.setTime(new Date()); 
				}
				requestParameters.put("coverageStartDate", c.get(Calendar.MONTH) + "-" + c.get(Calendar.DATE) + "-" +  c.get(Calendar.YEAR));
				//Tobacco will always 'NO'
				requestParameters.put("tobacco", "N");
				if(null != randomPlan.getPlanHealth()){
					requestParameters.put("planLevel", randomPlan.getPlanHealth().getPlanLevel());
				}else if(null != randomPlan.getPlanDental()){
					requestParameters.put("planLevel", randomPlan.getPlanDental().getPlanLevel());
				}
				requestParameters.put("planMarket", randomPlan.getMarket());
				requestParameters.put("countyCode", serviceArea.getFips());
				planRateBenefitRequest.setRequestParameters(requestParameters);
			}
			
		} else {
			LOGGER.error("No valid plans found for selected criteria : "
					+ SecurityUtil.sanitizeForLogging(inputs.getState()));
		}

		return planRateBenefitRequest;
	}

	
}
