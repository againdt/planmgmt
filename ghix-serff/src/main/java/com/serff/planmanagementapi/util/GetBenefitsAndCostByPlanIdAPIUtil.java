package com.serff.planmanagementapi.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.model.Plan;

public class GetBenefitsAndCostByPlanIdAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetBenefitsAndCostByPlanIdAPIUtil.class);
	
	public static PlanRateBenefitRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		PlanRateBenefitRequest planRateBenefitRequest;
		LOGGER.debug("GetBenefitsAndCostByPlanIdAPIUtil : createJSONRequest() begin.");
		
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		if(CollectionUtils.isEmpty(validPlans)){
			return null;
		}else{
			planRateBenefitRequest = new PlanRateBenefitRequest();
			
			Map<String, Object> requestParameters = new HashMap<String, Object>();
			List<Integer> planIds = new ArrayList<Integer>();
			String insuranceType = validPlans.get(0).getInsuranceType();
			for(Plan plan : validPlans){
				planIds.add(plan.getId());
			}
     
			requestParameters.put("planIds", planIds);
			requestParameters.put("insuranceType", insuranceType);
			
			planRateBenefitRequest.setRequestParameters(requestParameters);
			
			return planRateBenefitRequest;			
		}
	}
}
