/**
 * 
 */
package com.serff.planmanagementapi.util;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * @author sharma_va
 *
 */
public class GetPlanIssuerInfoUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetPlanIssuerInfoUtil.class);
		
		/**
		 * 
		 * @param inputs
		 * @param entityManagerFactory
		 * @return
		 */
		public static PlanRequest createJSONRequestForPlanIssuerInfo(
				PlanMgmtAPIInput inputs, EntityManagerFactory entityManagerFactory) {
			PlanRequest planRequest = null;
			LOGGER.debug("createJSONRequestForPlanIssuerInfo() begin.");
			
			List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
			

			if (!CollectionUtils.isEmpty(validPlans)) {
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
				}
				planRequest = new PlanRequest();
				int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
				Plan validPlan =  validPlans.get(randomPlanIndex);
				planRequest.setPlanId(String.valueOf(validPlan.getId()));
				
			} else {
				LOGGER.error("No valid plans found for selected state : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
				return null;
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Plan id in request : " + planRequest.getPlanId());
			}
			
			return planRequest;
		}

}
