package com.serff.planmanagementapi.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.shop.mlec.MLECRestRequest;
import com.getinsured.hix.dto.shop.mlec.Member;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;

public class MemberLevelEmployeeRateAPIUtil extends PlanMgmtAPIUtil{


	private static final Logger LOGGER = LoggerFactory.getLogger(MemberLevelEmployeeRateAPIUtil.class);
	private static final String VALID_PLANS_QUERY=" SELECT DISTINCT P.ID FROM PLAN P, PM_PLAN_RATE PRT, PM_SERVICE_AREA PMS, PM_ZIP_COUNTY_RATING_AREA PMSC "
			+ "WHERE P.ID = PRT.PLAN_ID "
			+ "AND PRT.RATING_AREA_ID = PMSC.RATING_AREA_ID "
			+ "AND PMSC.ZIP = PMS.ZIP "
			+ "AND PMS.FIPS = PMSC.COUNTY_FIPS "
			+ "AND P.SERVICE_AREA_ID = PMS.SERVICE_AREA_ID "
			+ "AND P.INSURANCE_TYPE='HEALTH' "
			+ "AND P.IS_DELETED='N' "
			+ "AND P.MARKET='SHOP' "
			+ "AND P.STATUS IN ('CERTIFIED','RECERTIFIED') "
			+ "AND P.ENROLLMENT_AVAIL = 'AVAILABLE' "
			+ "AND P.ISSUER_VERIFICATION_STATUS = 'VERIFIED' ";

	/**
	 * Returns JSON request for MemberLevelEmployeeRate API.
	 *
	 * @param inputs
	 * @param entityManagerFactory
	 * @return
	 */
	public static MLECRestRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		MLECRestRequest mLECRestRequest = null;
		LOGGER.debug("MemberLevelEmployeeRateAPIUtil : createJSONRequest() begin.");

			//Get list of valid plans.
			Plan randomPlan = null;
			EntityManager entityManager = entityManagerFactory.createEntityManager();
			if(null != entityManager){
				try{
				 List<Object> validPlanIds = entityManager.createNativeQuery(VALID_PLANS_QUERY + BenchMarkPlanAPIUtil.createWhereClause(inputs)).getResultList();
				 if(!CollectionUtils.isEmpty(validPlanIds)){
					 int randomPlanIndex = getRandomNumber(0, validPlanIds.size()-1);
					 String planId = (validPlanIds.get(randomPlanIndex)).toString();
					 randomPlan = (Plan)entityManager.find(Plan.class, Integer.valueOf(planId));
				 }else{
					 LOGGER.error("No valid plans found.");
				 }
				}catch(Exception e){
				 LOGGER.error("createRequestXML() : Error occurred : " + e.getMessage(), e);
				}finally{
				 	if(entityManager.isOpen()){
						entityManager.clear();
						entityManager.close();
						entityManager = null;
					}
			 }
			 }

			//Find service Area details for plan.
			if(null != randomPlan){
				ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
				if(null != randomServiceArea){
					mLECRestRequest = new MLECRestRequest();

					mLECRestRequest.setBenchmarkPlanId(randomPlan.getId());
					mLECRestRequest.setEffectiveDate(getEffectiveDate(randomPlan));
					mLECRestRequest.setEmployeeQuotingZip(randomServiceArea.getZip());
					mLECRestRequest.setEmployerQuotingZip(randomServiceArea.getZip());
					mLECRestRequest.setEmployeeQuotingCountyCode(randomServiceArea.getFips());
					mLECRestRequest.setEmployerQuotingCountyCode(randomServiceArea.getFips());
					List<Member> members = new ArrayList<Member>();
					List<Map<String, String>> membersValueList = createMemberList(randomPlan, randomServiceArea);

					for(Map<String, String> memberAttributes : membersValueList){

						Member member = new Member();
						member.setMemberID(memberAttributes.get(ID));

						if ("self".equals(memberAttributes.get(RELATION))) {
							member.setRelationship(Member.Relationship.SE);
						}
						else {
							member.setRelationship(Member.Relationship.CH);
						}
						member.setAge(Integer.parseInt(memberAttributes.get(AGE)));
						member.setSmoker(false);

						members.add(member);
						}
					mLECRestRequest.setMembers(members);
					}
			}

		return mLECRestRequest;
	}

}