package com.serff.planmanagementapi.util;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;

/**
 * This DTO class transfers all Input Params from UI to Controller
 * 
 * @author polimetla_b
 * @since 08/20/2014
 * 
 */
public final class PlanMgmtAPIInput implements Serializable {

	private static final long serialVersionUID = -6968128917436981069L;

	private String state = null;
	private String exchangeType = null; // On Exchange / Off
	private String planType = null; // Dental, Health, STM, AME, ....etc
	private String planLevel = null; // Gold,Silver,...etc
	private String planYear = null; // 2014,2015,...etc
	private String market = null; // Individual,SHOP
	private String networkType = null; // Network Type
	private String healthSavingAccount = null; // HSA: Health Saving Account
	private String availableFor = null; // Adults, Children and Both

	public PlanMgmtAPIInput(HttpServletRequest request) {

		if (!isNull(request.getParameter("state"))) {
			setState(request.getParameter("state"));
		}

		if (!isNull(request.getParameter("exchangeType"))) {
			setExchangeType(request.getParameter("exchangeType"));
		}

		if (!isNull(request.getParameter("planType"))) {
			setPlanType(request.getParameter("planType"));
		}

		if (!isNull(request.getParameter("planLevel"))) {
			setPlanLevel(request.getParameter("planLevel"));
		}

		if (!isNull(request.getParameter("planYear"))) {
			setPlanYear(request.getParameter("planYear"));
		}

		if (!isNull(request.getParameter("market"))) {
			setMarket(request.getParameter("market"));
		}

		if (!isNull(request.getParameter("networkType"))) {
			setNetworkType(request.getParameter("networkType"));
		}

		if (!isNull(request.getParameter("healthSavingsAccount"))) {
			setHealthSavingAccount(request.getParameter("healthSavingsAccount"));
		}

		if (!isNull(request.getParameter("availableFor"))) {
			setAvailableFor(request.getParameter("availableFor"));
		}
	}

	public boolean isNull(String data) {
		if (null == data) {
			return true;
		}

		if (data.trim().length() <= 0) {
			return true;
		}
		return false;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getPlanLevel() {
		return planLevel;
	}

	public void setPlanLevel(String planLevel) {
		this.planLevel = planLevel;
	}

	public String getPlanYear() {
		return planYear;
	}

	public void setPlanYear(String planYear) {
		this.planYear = planYear;
	}

	public String getMarket() {
		return market;
	}

	public void setMarket(String market) {
		this.market = market;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getHealthSavingAccount() {
		return healthSavingAccount;
	}

	public void setHealthSavingAccount(String healthSavingAccount) {
		this.healthSavingAccount = healthSavingAccount;
	}

	public String getAvailableFor() {
		return availableFor;
	}

	public void setAvailableFor(String availableFor) {
		this.availableFor = availableFor;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

}
