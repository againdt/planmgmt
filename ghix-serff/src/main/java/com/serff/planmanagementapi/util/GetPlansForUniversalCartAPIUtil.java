package com.serff.planmanagementapi.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.model.Plan;

public class GetPlansForUniversalCartAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetPlansForUniversalCartAPIUtil.class);
	
	public static PlanRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		PlanRequest planRequest = null;
		LOGGER.debug("GetPlansForUniversalCartAPIUtil : createJSONRequest() begin.");
		
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		if(CollectionUtils.isEmpty(validPlans)){
			return null;
		}else{
			planRequest = new PlanRequest();
			List<Integer> planIds = new ArrayList<Integer>();
			for(Plan plan : validPlans){
				planIds.add(plan.getId());
			}
			planRequest.setPlanIds(planIds);
			return planRequest;			
		}
	}
}
