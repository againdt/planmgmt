package com.serff.planmanagementapi.util;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.CrossSellPlanRequestDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;

public class GetDentalPlansAPIUtil extends PlanMgmtAPIUtil{

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetDentalPlansAPIUtil.class);
	
	/**
	 * Returns JSON request to call Get Dental Plans API.
	 * 
	 * @param inputs
	 * @param entityManagerFactory
	 * @return
	 */
	public static CrossSellPlanRequestDTO createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		
		CrossSellPlanRequestDTO crossSellPlanRequestDTO = null;
		LOGGER.debug("GetEmployerPlanRateAPIUtil : createJSONRequest() begin.");
		//Dental plans only.
		inputs.setPlanType("DENTAL");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		if(CollectionUtils.isEmpty(validPlans)){
			return null;
		}
		int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
		Plan randomPlan = validPlans.get(randomPlanIndex);
		ServiceArea serviceArea= getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
		if(null != serviceArea){
			crossSellPlanRequestDTO = new CrossSellPlanRequestDTO();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(getEffectiveDate(randomPlan));
			crossSellPlanRequestDTO.setEffectiveDate(calendar.get(Calendar.YEAR) + "-" + calendar.get(Calendar.MONTH) + "-" + calendar.get(Calendar.DATE));
			String exchangeType = "ON";
			if(null == inputs.getExchangeType()){
					int exchangeTypeIndex = getRandomNumber(0, 1);
					if(exchangeTypeIndex == 0){
						inputs.setExchangeType("ON");
					}else{
						inputs.setExchangeType("OFF");
					}
			}
			crossSellPlanRequestDTO.setExchangeType(exchangeType);
			crossSellPlanRequestDTO.setMemberList(createMemberList(randomPlan, serviceArea));
		}
		
		return crossSellPlanRequestDTO;
	}
}
