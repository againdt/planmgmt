package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.AmePlanRequestDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class GetAllAMEPlansAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetAllAMEPlansAPIUtil.class);

	/**
	 * Returns JSON to retrieve AME Plans availability.
	 * 
	 * @return
	 */
	public static AmePlanRequestDTO createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		AmePlanRequestDTO amePlanRequestDTO = null;
		LOGGER.debug("GetAMEPlansAPIUtil : createJSONRequest() begin.");
		inputs.setPlanType("AME");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		
		if (!CollectionUtils.isEmpty(validPlans)) {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			if(null != randomServiceArea){
				amePlanRequestDTO = new AmePlanRequestDTO();
				amePlanRequestDTO.setEffectiveDate(dateFormat.format(getEffectiveDate(randomPlan)));
				amePlanRequestDTO.setMemberList(createMemberList(randomPlan, randomServiceArea));
			}
		
		} else {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("No valid plans found for selected criteria : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
		}

		return amePlanRequestDTO;
	}
	
}

