package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;

public class GetPlanInfoByZipAPIUtil  extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetPlanInfoByZipAPIUtil.class);
	
	
	public static PlanRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		
		PlanRequest planRequest = null;
		LOGGER.debug("GetPlanInfoByZipAPIUtil : createJSONRequest() begin.");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		if(CollectionUtils.isEmpty(validPlans)){
			return null;
		}
		int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
		Plan randomPlan = validPlans.get(randomPlanIndex);
		ServiceArea serviceArea= getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		if(null != serviceArea){
			planRequest = new PlanRequest();
			//set request fields.
			planRequest.setInsuranceType(randomPlan.getInsuranceType());
			planRequest.setExchangeType(randomPlan.getExchangeType());
			planRequest.setZip(serviceArea.getZip());
			planRequest.setCountyFIPS(serviceArea.getFips());
			Date effectiveDate = getEffectiveDate(randomPlan);
			planRequest.setEffectiveDate(dateFormat.format(effectiveDate));
		}
		
		return planRequest;
	}
	
}
