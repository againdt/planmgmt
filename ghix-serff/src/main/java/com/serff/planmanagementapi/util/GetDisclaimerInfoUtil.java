package com.serff.planmanagementapi.util;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.getinsured.hix.dto.planmgmt.IssuerRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.util.SecurityUtil;

public class GetDisclaimerInfoUtil extends PlanMgmtAPIUtil{

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetDisclaimerInfoUtil.class);
    private static final int exchanegTypeIndex = 1;
    
	/**
	 * Returns JSON to retrieve issuer disclaimer information.
	 * 
	 * @return
	 */
	public static IssuerRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		LOGGER.debug("GetDisclaimerInfoUtil : createJSONRequest() begin.");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		
		IssuerRequest issuerRequest = null;
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			issuerRequest = new IssuerRequest();
			//Set issuer parameters.
			issuerRequest.setId(String.valueOf(randomPlan.getIssuer().getId()));
			if(StringUtils.isEmpty(inputs.getExchangeType())){
				if(getRandomNumber(0, exchanegTypeIndex) == 0){
					issuerRequest.setExchangeType("ON");
				}else{
					issuerRequest.setExchangeType("OFF");
				}
			}else{
				issuerRequest.setExchangeType(inputs.getExchangeType());
			}
		} else {
			LOGGER.error("No valid plans found for selected criteria : "
					+ SecurityUtil.sanitizeForLogging(inputs.getState()));
		}

		return issuerRequest;
	}

}
