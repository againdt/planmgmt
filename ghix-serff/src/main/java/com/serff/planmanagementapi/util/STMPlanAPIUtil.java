package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class STMPlanAPIUtil  extends PlanMgmtAPIUtil{


	private static final Logger LOGGER = LoggerFactory.getLogger(STMPlanAPIUtil.class);

	/**
	 * Returns JSON request for Household API.
	 *
	 * @param inputs
	 * @param entityManagerFactory
	 * @return
	 */
	public static PlanRateBenefitRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		PlanRateBenefitRequest planRateBenefitRequest = null;
		LOGGER.debug("STMPlanAPIUtil : createJSONRequest() begin.");
		//SHOP only plans required.
		inputs.setPlanType("STM");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);

		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			if(null != randomServiceArea){
				planRateBenefitRequest = new PlanRateBenefitRequest();
				Map<String, Object> requestParams = new HashMap<String, Object>();
				requestParams.put("effectiveDate", dateFormat.format(getEffectiveDate(randomPlan)));
				requestParams.put("memberList", createMemberList(randomPlan, randomServiceArea));
				planRateBenefitRequest.setRequestParameters(requestParams);
			}
		}
		return planRateBenefitRequest;
	}

}
