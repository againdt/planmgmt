/**
 * 
 */
package com.serff.planmanagementapi.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * @author sharma_va
 *
 */
public class GetPlansDataUtil extends PlanMgmtAPIUtil{

	private static final Logger LOGGER = LoggerFactory.getLogger(GetPlansDataUtil.class);
	public static PlanRequest createJSONRequestForPlansdataJSONRequest(
			PlanMgmtAPIInput inputs, EntityManagerFactory entityManagerFactory) {
		
		PlanRequest planRequest = null;
		
		LOGGER.debug("createJSONRequestForPlansdataJSONRequest() begin.");
		
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			planRequest= new PlanRequest();
			List<Integer> planIdList = new ArrayList<Integer>();
			int firstRandomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan firstValidPlan =  validPlans.get(firstRandomPlanIndex);
			int secondRandomIndex = getRandomNumber(0, validPlans.size()-1);
			Plan secondValidPlan = validPlans.get(secondRandomIndex);
			Integer firstPlanId = firstValidPlan.getId();
			planIdList.add(firstPlanId);
			Integer secondPlanId = secondValidPlan.getId();
			planIdList.add(secondPlanId);
			planRequest.setPlanIds(planIdList);
			
		} else {
			LOGGER.error("No valid plans found for selected state : "
					+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			return null;
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Plan id in request : " + planRequest.getPlanId());
		}
		
		return planRequest;
	}

	
	
	
}
