package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.shop.employer.eps.EmployerQuotingDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * Class is used to provide utility inputs to plan/checkMetalTierAvailability API.
 * 
 * @author Bhavin Parmar
 * @since December 10, 2014
 */
public class CheckMetalTierAvailabilityAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(CheckMetalTierAvailabilityAPIUtil.class);

	/**
	 * Returns JSON to retrieve issuer disclaimer information.
	 */
	public static EmployerQuotingDTO createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		LOGGER.info("Beginning executing createJSONRequest().");
		EmployerQuotingDTO employerQuotingDTO = null;

		try {
			inputs.setPlanType(Plan.PlanInsuranceType.HEALTH.name());
			List<Plan> validPlansList = getValidPlans(inputs, entityManagerFactory);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

			if (CollectionUtils.isEmpty(validPlansList)) {
				LOGGER.error("No valid plans found for selected criteria : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
				return employerQuotingDTO;
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans("+ validPlansList.size() +") for selected state: " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}

			int randomPlanIndex = getRandomNumber(0, validPlansList.size() - 1);
			LOGGER.debug("Random Plan Index is: " + randomPlanIndex);
			Plan randomPlan = validPlansList.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);

			if (null != randomServiceArea) {
				employerQuotingDTO = new EmployerQuotingDTO();
				Date effectiveDate = getEffectiveDate(randomPlan);
				// Get Effective Date in 'YYYY-MM-DD' format
				employerQuotingDTO.setEffectiveDate(dateFormat.format(effectiveDate));

				if (null != randomPlan.getPlanHealth()) {
					employerQuotingDTO.setEhbCovered(randomPlan.getPlanHealth().getEhbCovered());
				}
				employerQuotingDTO.setEmpCountyCode(randomServiceArea.getFips());
				employerQuotingDTO.setEmpPrimaryZip(randomServiceArea.getZip());
				employerQuotingDTO.setInsType(randomPlan.getInsuranceType());
			}
			else {
				LOGGER.error("Service Area is not available.");
			}
		}
		finally {
			LOGGER.info("End execution createJSONRequest().");
		}
		return employerQuotingDTO;
	}
}
