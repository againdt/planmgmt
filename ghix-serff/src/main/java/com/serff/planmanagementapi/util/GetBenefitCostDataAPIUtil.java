package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;

public class GetBenefitCostDataAPIUtil extends PlanMgmtAPIUtil{

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetBenefitCostDataAPIUtil.class);
	
	/**
	 * Returns JSON to retrieve benefit cost data.
	 * 
	 * @return
	 */
	public static PlanRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		
		PlanRequest planRequest = null;
		LOGGER.debug("GetBenefitCostDataAPIUtil : createJSONRequest() begin.");
		
		//Either Health or Dental plan is mandatory for this API.
		if(null == inputs.getPlanType()){
			int randomPlanType = getRandomNumber(0, 1);
			if(randomPlanType == 0){
				inputs.setPlanType("HEALTH");
			}else{
				inputs.setPlanType("DENTAL");
			}
		}
		
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		if (!CollectionUtils.isEmpty(validPlans)) {
			LOGGER.debug("Found plans for selected state : "
					+ inputs.getState());
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			if(null != randomServiceArea){
				planRequest = new PlanRequest();
				//Set parameters.
				List<Integer> planIds = new ArrayList<Integer>();
				planIds.add(randomPlan.getId());
				planRequest.setPlanIds(planIds);
				planRequest.setInsuranceType(randomPlan.getInsuranceType());
				planRequest.setEffectiveDate(dateFormat.format(getEffectiveDate(randomPlan)));
			}
		}
		return planRequest;
	}
}
