/**
 *
 */
package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * @author sharma_va
 *
 */
public class GetPlanDetailsUtil extends PlanMgmtAPIUtil{

	private static final Logger LOGGER = LoggerFactory.getLogger(GetPlanDetailsUtil.class);


	/**
	 *
	 * @param PlanMgmtAPIInput
	 * @param entityManagerFactory
	 * @return
	 */
	public static PlanRequest createJSONRequestForPlanDetails(
			PlanMgmtAPIInput inputs, EntityManagerFactory entityManagerFactory) {

		PlanRequest planRequest = null;
		LOGGER.debug("createGetPlanAvailabilityJSONRequest() begin.");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : "+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			planRequest = new PlanRequest();
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan validPlan =  validPlans.get(randomPlanIndex);
			planRequest.setPlanId(String.valueOf(validPlan.getId()));
			try {
				Date effectiveDate = getEffectiveDate(validPlan);
			    planRequest.setEffectiveDate(String.valueOf(dateFormat.format(effectiveDate)));
			} catch (Exception e) {
				LOGGER.error("Exception occured while parsing date :" , e);
			}

		} else {
			LOGGER.error("No valid plans found for selected state : "
					+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			return null;
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Plan id in request : " + planRequest.getPlanId());
		}
		return planRequest;
	}



}
