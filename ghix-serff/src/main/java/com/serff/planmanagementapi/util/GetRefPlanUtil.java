package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.shop.employer.eps.EmployerQuotingDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;

public class GetRefPlanUtil extends PlanMgmtAPIUtil{

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetRefPlanUtil.class);
	
	/**
	 * Returns JSON request to call getrefplan API.
	 * 
	 * @return
	 */
	public static EmployerQuotingDTO createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		LOGGER.debug("GetRefPlanUtil : createGetPlanInfoJSONRequest() begin.");

		EmployerQuotingDTO employerQuotingDTO = null;
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		if (!CollectionUtils.isEmpty(validPlans)) {
			LOGGER.debug("Found plans for selected criteria : " + validPlans.size());
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			if(null != randomServiceArea){
				//'YYYY-MM-DD'
				employerQuotingDTO = new EmployerQuotingDTO();
				Date effectiveDate = getEffectiveDate(randomPlan);
				employerQuotingDTO.setEffectiveDate(dateFormat.format(effectiveDate));
				if(null != randomPlan.getPlanHealth()){
					employerQuotingDTO.setEhbCovered(randomPlan.getPlanHealth().getEhbCovered());
				}
				employerQuotingDTO.setEmpCountyCode(randomServiceArea.getFips());
				employerQuotingDTO.setEmpPrimaryZip(randomServiceArea.getZip());
				employerQuotingDTO.setInsType(randomPlan.getInsuranceType());
			}
			}
		
		return employerQuotingDTO;
	}

}
