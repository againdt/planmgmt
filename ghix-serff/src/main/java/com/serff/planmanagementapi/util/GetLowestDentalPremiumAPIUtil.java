package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.CrossSellPlanRequestDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class GetLowestDentalPremiumAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetLowestDentalPremiumAPIUtil.class);
	
	/**
	 * Returns JSON to retrieve benefit cost data.
	 * 
	 * @return
	 */
	public static CrossSellPlanRequestDTO createJSONRequest(
			PlanMgmtAPIInput inputs, EntityManagerFactory entityManagerFactory) {

		LOGGER.debug("GetBenefitCostDataAPIUtil : createJSONRequest() begin.");
		CrossSellPlanRequestDTO crossSellPlanRequestDTO = null;
		if(null == inputs.getExchangeType()){
			if(getRandomNumber(0, 1) == 0){
				inputs.setExchangeType("ON");
			}else{
				inputs.setPlanType("OFF");
			}
		}//Plan type is default : Dental
		inputs.setPlanType("DENTAL");
		
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);

		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size() - 1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			if (null != randomServiceArea) {
				crossSellPlanRequestDTO = new CrossSellPlanRequestDTO();
				crossSellPlanRequestDTO.setExchangeType(randomPlan.getExchangeType());
				crossSellPlanRequestDTO.setEffectiveDate(dateFormat.format(getEffectiveDate(randomPlan)));
				crossSellPlanRequestDTO.setMemberList(createMemberList(randomPlan, randomServiceArea));
			}
		}
		return crossSellPlanRequestDTO;
	}
	
}
