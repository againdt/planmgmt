package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.VisionPlanRequestDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * --------------------------------------------------------------------------------------
 * HIX-59894 : Create SERFF Test Bench API
 * --------------------------------------------------------------------------------------
 * 
 * Utility class to generate sample JSON request to test ghix-planmgmt/visionplan/getvisionplans API.
 * 
 */
public class GetVisionPlansAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetVisionPlansAPIUtil.class);

	/**
	 * Returns JSON to retrieve Vision Plans availability.
	 * 
	 * @param inputs - Input parameters selected from UI screen
	 * @param entityManagerFactory - The entity manager factory instance
	 * @return - VisionPlanRequestDTO with sample values populated from database
	 */
	public static VisionPlanRequestDTO createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		VisionPlanRequestDTO visionPlanRequestDTO = null;
		LOGGER.debug("GetVisionPlansAPIUtil : createJSONRequest() begin.");
		inputs.setPlanType("VISION");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found Vision plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			if(null != randomServiceArea){
				visionPlanRequestDTO = new VisionPlanRequestDTO();
				visionPlanRequestDTO.setEffectiveDate(dateFormat.format(getEffectiveDate(randomPlan)));
				visionPlanRequestDTO.setMemberList(createMemberList(randomPlan, randomServiceArea));
			}
		
		} else {
			LOGGER.error("No valid Vision plans found for selected criteria : "
					+ SecurityUtil.sanitizeForLogging(inputs.getState()));
		}

		return visionPlanRequestDTO;
	}
	
}