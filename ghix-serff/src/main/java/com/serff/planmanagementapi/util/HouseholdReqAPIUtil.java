package com.serff.planmanagementapi.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.planmgmt.HouseHoldRequest;
import com.getinsured.hix.model.planmgmt.Member;
import com.getinsured.hix.platform.util.SecurityUtil;

public class HouseholdReqAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(HouseholdReqAPIUtil.class);


	/**
	 * Returns JSON to retrieve Employer plan rate.
	 *
	 * @return
	 */
	public static HouseHoldRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		HouseHoldRequest houseHoldRequest = null;
		LOGGER.debug("HouseholdReqAPIUtil : createJSONRequest() begin.");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);

		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);

			houseHoldRequest = new HouseHoldRequest();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(getEffectiveDate(randomPlan));
			houseHoldRequest.setEffectiveDate(calendar);
			houseHoldRequest.setState(randomPlan.getState());
			houseHoldRequest.setMembers(new ArrayList<Member>());

		}
		return houseHoldRequest;
	}
}
