package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class GetBenchmarkRateAPIUtil extends PlanMgmtAPIUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(GetBenchmarkRateAPIUtil.class);

	/**
	 * Returns JSON request to call Benchmark Rate API.
	 * 
	 * @param inputs
	 * @param entityManagerFactory
	 * @return
	 */
	public static PlanRateBenefitRequest createJSONRequest(
			PlanMgmtAPIInput inputs, EntityManagerFactory entityManagerFactory) {

		PlanRateBenefitRequest planRateBenefitRequest = null;
		LOGGER.debug("GetBenchmarkRateAPIUtil : createJSONRequest() begin.");
		//Get list of valid plans.
		Plan randomPlan = null;
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		
		if(null != entityManager){
			
		try{
			     List<Object> validPlanIds = entityManager.createNativeQuery(BenchMarkPlanAPIUtil.VALID_PLANS_QUERY + 
			    		 BenchMarkPlanAPIUtil.createWhereClause(inputs)).setMaxResults(TWENTY).getResultList();
				 if(!CollectionUtils.isEmpty(validPlanIds)){
					 int randomPlanIndex = getRandomNumber(0, validPlanIds.size()-1);
					 String planId = (validPlanIds.get(randomPlanIndex)).toString();
					 randomPlan = (Plan)entityManager.find(Plan.class, Integer.valueOf(planId));
				 }else{
					 LOGGER.error("No valid plans found.");
				 }
			 }catch(Exception e){
				 LOGGER.error("createRequestXML() : Error occurred : " + e.getMessage(), e);
			 }finally{
				 	if(entityManager.isOpen()){
						entityManager.clear();
						entityManager.close();
						entityManager = null;
					}
			 }
		 }
		
		if(null != randomPlan){
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			if(null != randomServiceArea){
				planRateBenefitRequest = new PlanRateBenefitRequest();

				Map<String, Object> requestParams = new HashMap<String, Object>();
				requestParams.put("censusData", createMemberList(randomPlan, randomServiceArea));
				requestParams.put("coverageStartDate", dateFormat.format(getEffectiveDate(randomPlan)));
				
				planRateBenefitRequest.setRequestParameters(requestParams);
			}
		
		} else {
			LOGGER.error("No valid plans found for selected criteria : "
					+ SecurityUtil.sanitizeForLogging(inputs.getState()));
		}

		return planRateBenefitRequest;
	}
}