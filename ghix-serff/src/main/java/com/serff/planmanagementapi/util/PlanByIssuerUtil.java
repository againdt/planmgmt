/**
 * 
 */
package com.serff.planmanagementapi.util;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * @author sharma_va
 *
 */
public class PlanByIssuerUtil extends PlanMgmtAPIUtil{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanByIssuerUtil.class);
	
	/**
	 * 
	 * @param inputs
	 * @param entityManagerFactory
	 * @return
	 */
	public static PlanRequest createJSONRequestForPlanByIssuer(
			PlanMgmtAPIInput inputs, EntityManagerFactory entityManagerFactory) {
		
		PlanRequest planRequest = null;
		LOGGER.debug("createGetPlanAvailabilityJSONRequest() begin.");
		if(null != inputs.getPlanType() && ("STM".equals(inputs.getPlanType()) || "AME".equals(inputs.getPlanType()) )){
			LOGGER.error("PlanByIssuer is for Health or Dental plan.: ");
			return null;
		}else if(null == inputs.getPlanType()){
			//Randomly select Health or Dental plan if Plan type is null.
			if(getRandomNumber(0, 1) == 0){
				inputs.setPlanType("HEALTH");
			}else{
				inputs.setPlanType("DENTAL");
			}
		}//else continue with the plan type selected i.e. Health or Dental.
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			planRequest = new PlanRequest();
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan validPlan =  validPlans.get(randomPlanIndex);
			//zipCode, countyCode, insuranceType, issuerName, planLevel
			ServiceArea serviceArea = getServiceAreaDetailsForPlan(validPlan ,entityManagerFactory );
			
			planRequest.setZip(serviceArea.getZip());
			planRequest.setCountyFIPS(serviceArea.getFips());
			planRequest.setInsuranceType(validPlan.getInsuranceType());
			planRequest.setIssuerName(validPlan.getIssuer().getName());
			if(null != validPlan.getPlanHealth()){
				planRequest.setPlanLevel(validPlan.getPlanHealth().getPlanLevel());
			}else if(null != validPlan.getPlanDental()){
				planRequest.setPlanLevel(validPlan.getPlanDental().getPlanLevel());
			}
			
		} else {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("No valid plans found for selected state : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			return null;
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Plan id in request : " + planRequest.getPlanId());
		}
		return planRequest;
	}

}

