package com.serff.planmanagementapi.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

public class HouseholdAPIUtil extends PlanMgmtAPIUtil{

	
	private static final Logger LOGGER = LoggerFactory.getLogger(HouseholdAPIUtil.class);
	private static final int GRP_ID = 123;//Dummy value
	/**
	 * Returns JSON request for Household API. 
	 * 
	 * @param inputs
	 * @param entityManagerFactory
	 * @return
	 */
	public static PlanRateBenefitRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		//int groupId =123; //Dummy value
		PlanRateBenefitRequest planRateBenefitRequest = null;
		LOGGER.debug("GetEmployerPlanRateAPIUtil : createJSONRequest() begin.");
		//HEALTH plans only.
		inputs.setPlanType("HEALTH");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			
			if(null != randomServiceArea){
				planRateBenefitRequest = new PlanRateBenefitRequest();
				//Create Map of request parameters.
				Map<String, Object> requestParameters = new HashMap<String, Object>();
				requestParameters.put("memberList", createMemberList(randomPlan, randomServiceArea));
				requestParameters.put("insType", randomPlan.getInsuranceType() );
				requestParameters.put("effectiveDate", dateFormat.format(getEffectiveDate(randomPlan)));
				requestParameters.put("costSharing", "CS1" );//This will check for Plan Health with Parent Plan id 0. May not work in most dbs.
				requestParameters.put("planIdStr", "");
				requestParameters.put("showCatastrophicPlan", Boolean.FALSE);
				String planLevel = "";
				String ehbCovered = "";
				if(null != randomPlan.getPlanHealth()){
					planLevel = randomPlan.getPlanHealth().getPlanLevel();
					ehbCovered = randomPlan.getPlanHealth().getEhbCovered();
				}
				if(null != randomPlan.getPlanDental()){
					planLevel = randomPlan.getPlanDental().getPlanLevel();
				}
				requestParameters.put("planLevel", planLevel );
				requestParameters.put("ehbCovered", ehbCovered);
				requestParameters.put("groupId", Integer.valueOf(GRP_ID));//Not used, but need to pass as API expects this parameter.
				List<Map<String, List<String>>> providers = new ArrayList<Map<String, List<String>>>();
				requestParameters.put("providers",  providers);
				requestParameters.put("marketType",  randomPlan.getMarket());
				requestParameters.put("isSpecialEnrollment", randomPlan.getEnrollmentAvail());
				requestParameters.put("issuerId", randomPlan.getIssuer().getId());
				requestParameters.put("issuerVerifiedFlag", Boolean.valueOf(randomPlan.getIssuerVerificationStatus()));
				requestParameters.put("exchangeType", randomPlan.getExchangeType());
				requestParameters.put("tenant", "GINS");
				requestParameters.put("hiosPlanNumber", "");
				List<String> drugsNamesList = new ArrayList<String>();
				drugsNamesList.add("lipitor");	
				requestParameters.put("drugsNamesList",  drugsNamesList);
				List<String> drugsNamesListRxcode = new ArrayList<String>(); 
				drugsNamesListRxcode.add("861200");
				requestParameters.put("drugsNamesList_Rxcode", drugsNamesListRxcode);
								
				planRateBenefitRequest.setRequestParameters(requestParameters);
			}
		}
		
		return planRateBenefitRequest;
	}
	
}
