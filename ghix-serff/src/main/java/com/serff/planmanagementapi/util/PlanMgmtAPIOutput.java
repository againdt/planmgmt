package com.serff.planmanagementapi.util;

import java.io.Serializable;

/**
 * This is DTO and carries data from Util Classes to API Test JSP.
 * 
 * @author Shashikant
 * @since 08/21/2014
 */
public class PlanMgmtAPIOutput implements Serializable {
	
	private static final long serialVersionUID = -7816122492866854133L;
	
	private String duration;
	private String size;
	private String url;
	private String method;
	private String jsonResponse;
	private String errorMessage;

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getJsonResponse() {
		return jsonResponse;
	}

	public void setJsonResponse(String jsonResponse) {
		this.jsonResponse = jsonResponse;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
