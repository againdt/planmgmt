package com.serff.planmanagementapi.util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.model.planmgmt.EligibilityRequest;
import com.getinsured.hix.model.planmgmt.Member;
import com.getinsured.hix.platform.util.SecurityUtil;

public class TeaserPlanEligibilityRequestUtil extends PlanMgmtAPIUtil{


	private static final Logger LOGGER = LoggerFactory.getLogger(TeaserPlanEligibilityRequestUtil.class);

	/**
	 * Returns JSON request for Teaser Plan Eligibility Request API.
	 *
	 * @param inputs
	 * @param entityManagerFactory
	 * @return
	 */
	public static EligibilityRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {

		EligibilityRequest eligibilityRequest = null;
		LOGGER.debug("TeaserPlanEligibilityRequestUtil : createJSONRequest() begin.");
		//Health Plans Only.
		inputs.setPlanType("HEALTH");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);


		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			ServiceArea randomServiceArea = getServiceAreaDetailsForPlan(randomPlan, entityManagerFactory);

			if(null != randomServiceArea){
				eligibilityRequest = new EligibilityRequest();

				List<Map<String, String>> membersList = createMemberList(randomPlan, randomServiceArea);
				List<Member> members = new ArrayList<Member>();
				for(Map<String, String> memberValus : membersList){
					Member member = new Member();
					member.setAge(Integer.parseInt(memberValus.get(AGE)));
					member.setGender(memberValus.get(GENDER));
					member.setTobaccoUser(false);

					members.add(member);
				}
				eligibilityRequest.setMembers(members);
				eligibilityRequest.setState(randomPlan.getState());
				eligibilityRequest.setZipCode(randomServiceArea.getZip());
				eligibilityRequest.setCounty(randomServiceArea.getFips());
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(getEffectiveDate(randomPlan));
				eligibilityRequest.setEffectiveDate(calendar);
				eligibilityRequest.setExchangeFlowType(randomPlan.getExchangeType());
				eligibilityRequest.setCostSharingVariation(randomPlan.getPlanHealth().getCostSharing());
//				eligibilityRequest.setSendAllMetalTier(sendAllMetalTier);

			}
		}

		return eligibilityRequest;
	}
}
