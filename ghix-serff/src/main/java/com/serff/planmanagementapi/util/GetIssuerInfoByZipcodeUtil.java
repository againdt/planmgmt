/**
 * 
 */
package com.serff.planmanagementapi.util;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.IssuerRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * @author sharma_va
 *
 */
public class GetIssuerInfoByZipcodeUtil extends PlanMgmtAPIUtil{
	private static final Logger LOGGER = LoggerFactory.getLogger(GetIssuerInfoByZipcodeUtil.class);

	/**
	 * 
	 * @param planMgmtAPIInput
	 * @param entityManagerFactory
	 * @return issuerRequest
	 */
	public static IssuerRequest createJSONRequestForIssuerInfoByZipcode(PlanMgmtAPIInput inputs, EntityManagerFactory entityManagerFactory) {
		
		IssuerRequest issuerRequest = null;
		LOGGER.debug("createJSONRequestForIssuerInfoByZipcode() begin.");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		

		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found Issuer for selected state : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			issuerRequest = new IssuerRequest();
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan validPlan =  validPlans.get(randomPlanIndex);
			ServiceArea serviceArea = getServiceAreaDetailsForPlan(validPlan ,entityManagerFactory );
			
			issuerRequest.setZipCode(serviceArea.getZip());
			issuerRequest.setCountyCode(serviceArea.getFips());
			
		} else {
			if(LOGGER.isErrorEnabled()) {
				LOGGER.error("No valid Issuer found for selected state : "
						+ SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			return null;
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Issuer id in request : " + issuerRequest.getId());
		}
		
		return issuerRequest;
		
	}
	
	
}
