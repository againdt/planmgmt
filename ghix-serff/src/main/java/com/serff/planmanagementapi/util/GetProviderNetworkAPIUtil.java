package com.serff.planmanagementapi.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.util.SecurityUtil;

public class GetProviderNetworkAPIUtil extends PlanMgmtAPIUtil{

	
	private static final Logger LOGGER = LoggerFactory.getLogger(GetProviderNetworkAPIUtil.class);
	
	private static final String NETWORK_QUERY = "SELECT net.network_key, net.has_provider_data from plan p, "
			+ "network net WHERE net.id = p.provider_network_id AND p.id =";
	
	/**
	 * Returns JSON request for Get Provider Network  API. 
	 * 
	 * @param inputs
	 * @param entityManagerFactory
	 * @return
	 */
	public static PlanRequest createJSONRequest(PlanMgmtAPIInput inputs,
			EntityManagerFactory entityManagerFactory) {
		PlanRequest planRequest = null;
		LOGGER.debug("GetProviderNetworkAPIUtil : createJSONRequest() begin.");
		List<Plan> validPlans = getValidPlans(inputs, entityManagerFactory);
		
		if (!CollectionUtils.isEmpty(validPlans)) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Found plans for selected state : " + SecurityUtil.sanitizeForLogging(inputs.getState()));
			}
			int randomPlanIndex = getRandomNumber(0, validPlans.size()-1);
			Plan randomPlan = validPlans.get(randomPlanIndex);
			EntityManager entityManager = null;

			try{
				
				entityManager = entityManagerFactory.createEntityManager();				
				if (null == entityManager) {
					LOGGER.error("Entity Manager is null");
					return null;
				}else{
					List<Object[]> networkData = entityManager.createNativeQuery(NETWORK_QUERY + randomPlan.getId()).getResultList();
					if(!CollectionUtils.isEmpty(networkData)){
						planRequest = new PlanRequest();
						List<Map<String, List<String>>> providers = new ArrayList<Map<String, List<String>>>();
						String providerId = "20";
						List<String> networkKeys = new ArrayList<String>();
						networkKeys.add("22020-testkey1");
						networkKeys.add("22020-testkey2");
						for (Object[] network : networkData) {
							networkKeys.add(network[0] == null ? "" : network[0].toString());
						}
						Map<String, List<String>> networkProvider = new HashMap<String, List<String>>();
						networkProvider.put(providerId, networkKeys);
						providers.add(networkProvider);
						planRequest.setPlanId(String.valueOf(randomPlan.getId()));
						planRequest.setProviderList(providers);		
					}
				}
			}catch(Exception e) {
				LOGGER.error("createJSONRequest() : Error occurred  : " + e.getMessage(), e);
			}finally{
				if (null != entityManager) {
					entityManager.clear();
					entityManager.close();
					entityManager = null;
				}
			}
		}
		
		return planRequest;
	}
	
}
