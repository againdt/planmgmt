package com.serff.rest.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.serff.restServices.UpdateSerffPlanStatusRequest;
import com.serff.restServices.UpdateSerffPlanStatusResponse;

/**
 * This class act as the controller for all incoming REST call
 * to GHIX-SERFF.
 * 
 * @author Geetha Chandran
 * @since 11/04/2013
 */
@Controller
@RequestMapping(value = "/ghixSerffService")
public class GhixSerffRestController {

	private static final Logger LOGGER = Logger
			.getLogger(GhixSerffRestController.class);

	/**
	 * This method invokes call to SERFF to update plan status
	 * 
	 * @author Geetha Chandran
	 * @since 11/04/2013
	 */
	@RequestMapping(value = "/updateSerffPlanStatus", method = RequestMethod.POST)
	@ResponseBody
	public UpdateSerffPlanStatusResponse updateSerffPlanStatus(
			@RequestBody UpdateSerffPlanStatusRequest req) {

		LOGGER.info("updateSerffPlanStatus REST call invoked");
		//The ghixSerffResponse needs to be mapped to the response received from SERFF
		UpdateSerffPlanStatusResponse ghixSerffResponse = new UpdateSerffPlanStatusResponse();
		ghixSerffResponse.setSuccessful(true);
		
		if (null != req) {
			LOGGER.info("-----updateSerffPlanStatus parameters starts-----");
			LOGGER.info("Plan id : " + req.getPlanId());
			ghixSerffResponse.setPlanId(req.getPlanId());
//			ghixSerffResponse.setPlanStatus(req.getPlanStatus());
			/*if(null !=  req.getPlanStatus()){
				LOGGER.info("Status : " + req.getPlanStatus().value());
			}*/
			LOGGER.info("-----updateSerffPlanStatus parameters ends-----");
		}
		return ghixSerffResponse;
	}

}
