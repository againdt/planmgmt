package com.serff.service;

import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.PlanDisplayRules;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.serff.dto.BenefitsDisplayMappingParamsDTO;
import com.serff.repository.templates.IPlanDisplayRulesRepository;

/**
 * Class is used to map rules of Plan Display.
 * 
 * @author Bhavin Parmar
 * @since November 25, 2014
 */
@Service("planDisplayMapping")
public class PlanDisplayMappingImpl implements PlanDisplayMapping {

	private static final Logger LOGGER = Logger.getLogger(PlanDisplayMappingImpl.class);

	private static final String EMSG_INVALID_KEY = "Invalid Key Value for ID ";
	private static final String EMSG_EMPTY_SEED_DATA = "Seed data is not available in PlanDisplayRules table.";
	private static final String EMSG_EMPTY_MAP = "Plan Display Rules Map is empty.";

	private static final String MSG_RULES_LIST = "PlanDisplayRulesList is null: ";
	private static final String DOT_ZERO = ".0";
	private static final String DOT_ZERO_ZERO = ".00";

	@Autowired private IPlanDisplayRulesRepository iPlanDisplayRulesRepository;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;

	/**
	 * Method is used to load Plan Display Rules in Hashtable.
	 */
	@Override
	public PlanDisplayRulesMap loadPlanDisplayRulesInMap(final String hiosIssuerId) {

		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Beginning executing loadPlanDisplayRulesInMap() for HIOS ID["+ SecurityUtil.sanitizeForLogging(hiosIssuerId) +"].");
		}
		PlanDisplayRulesMap planDisplayRulesMap = null;

		try {
			//Load display rules from DB for given HIOS ID where isActive is set as Y 
			List<PlanDisplayRules> planDisplayRulesList = iPlanDisplayRulesRepository.findByHiosIssuerIdAndIsActive(hiosIssuerId, PlanDisplayRules.IS_ACTIVE.Y.name());
			LOGGER.debug(MSG_RULES_LIST + (null == planDisplayRulesList));
			
			//If there are no ISSUER specific rules, load default rules for ISSUER ID '00000'
			if (CollectionUtils.isEmpty(planDisplayRulesList)) {
				LOGGER.warn("For HIOS ID["+ SecurityUtil.sanitizeForLogging(hiosIssuerId) + "], " + EMSG_EMPTY_SEED_DATA);

				planDisplayRulesList = iPlanDisplayRulesRepository.findByHiosIssuerIdAndIsActive(PlanDisplayRulesMap.DEFAULT_HIOS_ID, PlanDisplayRules.IS_ACTIVE.Y.name());
				LOGGER.debug(MSG_RULES_LIST + (null == planDisplayRulesList));
			}

			if (CollectionUtils.isEmpty(planDisplayRulesList)) {
				// There are no Active rules 
				LOGGER.error(EMSG_EMPTY_SEED_DATA);
				return planDisplayRulesMap;
			}

			String key = null;
			planDisplayRulesMap = new PlanDisplayRulesMap();

			for (PlanDisplayRules planDisplayRules : planDisplayRulesList) {

				// Generate Key from copay & coinsurnace data.
				key = PlanDisplayRulesMap.generateKey(planDisplayRules.getCoinsuranceValue(),
						planDisplayRules.getCoinsuranceAttribute(), planDisplayRules.getCopayValue(),
						planDisplayRules.getCopayAttribute());

				if (StringUtils.isNotBlank(key)) {
					planDisplayRulesMap.put(key, planDisplayRules);
				}
				else {
					LOGGER.error(EMSG_INVALID_KEY + planDisplayRules.getId());
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (!CollectionUtils.isEmpty(planDisplayRulesMap)) {
				LOGGER.info("Size of PlanDisplayRulesMap: " + planDisplayRulesMap.size());

				/*for (Map.Entry<String, PlanDisplayRules> entryData : planDisplayRulesMap.entrySet()) {
					LOGGER.debug("======Record: planDisplayRulesMap.getKey("+ entryData.getKey() +"): " + entryData.getValue().toString());
				}*/
			}
			else {
				LOGGER.error(EMSG_EMPTY_MAP);
				planDisplayRulesMap = null;
			}
			LOGGER.info("End execution loadPlanDisplayRulesInMap().");
		}
		return planDisplayRulesMap;
	}

	/**
	 * Method is used to return Tile Display Text from CoPay and CoInsurance data.
	 */
	@Override
	public String getBenefitDisplayTileText(String coinsValue, String coinsAttr,
			String copayValue, String copayAttr, PlanDisplayRulesMap rulesMap) {

		LOGGER.debug("Beginning executing getBenefitDisplayTileText().");
		StringBuilder displayTileText = new StringBuilder();
		String coinsTrimmedValue = stripDecimalZero(coinsValue);
		String copayTrimmedValue = stripDecimalZero(copayValue);
		try {
			PlanDisplayRules rule = rulesMap.getPlanDisplayRules(coinsValue, coinsAttr, copayValue, copayAttr);
			if(null != rule) {
				Formatter formatter = new Formatter(displayTileText, Locale.US);
				String template = rule.getTileDisplayTemplate();
				formatter.format(template, coinsTrimmedValue, coinsAttr, copayTrimmedValue, copayAttr);
				formatter.close();
			} else {
				LOGGER.warn("No matching rule is available, so returning value as it is.");
				displayTileText.append(copayTrimmedValue);
				displayTileText.append(", ");
				displayTileText.append(coinsTrimmedValue );
			}
		}
		finally {
			LOGGER.debug("End execution getBenefitDisplayTileText(): Display Tile Text: " + displayTileText);
		}
		return displayTileText.toString();
	}

	/**
	 * Method is used to return Display Text from CoPay and CoInsurance data.
	 */
	@Override
	public String getBenefitDisplayText(String coinsValue, String coinsAttr,
			String copayValue, String copayAttr, long coinsAfterNoOffCopay,
			long afterNumberOfVisit, boolean isPrimaryCareT1,
			PlanDisplayRulesMap rulesMap, BenefitsDisplayMappingParamsDTO extendedParams) {

		LOGGER.debug("Beginning executing getBenefitDisplayText().");
		StringBuilder displayText = new StringBuilder();
		String template = null;
		String coinsTrimmedValue = stripDecimalZero(coinsValue);
		String copayTrimmedValue = stripDecimalZero(copayValue);

		try {
			PlanDisplayRules rule = rulesMap.getPlanDisplayRules(coinsValue, coinsAttr, copayValue, copayAttr);
			if(null != rule) {
				Formatter formatter = new Formatter(displayText, Locale.US);
	
				if (isPrimaryCareT1) {
	
					if (afterNumberOfVisit > 0 && coinsAfterNoOffCopay > 0) {
						template = rule.getPrimaryCareT1ForCopayAndVisits();
					}
					else if (afterNumberOfVisit > 0) {
						template = rule.getPrimaryCareT1ForNumVisits();
					}
					else if (coinsAfterNoOffCopay > 0) {
						template = rule.getPrimaryCareT1ForNumCopay();
					}
					else {
						template = rule.getStandardDisplayTemplate();
					}
				}
				else {
					template = rule.getStandardDisplayTemplate();
				}

				formatter.format(template, coinsTrimmedValue, coinsAttr, copayTrimmedValue, copayAttr, afterNumberOfVisit, coinsAfterNoOffCopay);
				formatter.close();
			} else {
				LOGGER.warn("No matching rule is available, so returning value as it is.");
				displayText.append(copayTrimmedValue);
				displayText.append(" ");
				displayText.append(copayAttr);
				displayText.append(", ");
				displayText.append(coinsTrimmedValue);
				displayText.append(" ");
				displayText.append(coinsAttr );
			}
		}
		finally {
			LOGGER.debug("End execution getBenefitDisplayText(): Display Text: " + displayText);
		}
		return displayText.toString();
	}

	@Override
	public boolean copyActiveDisplayRulesForIssuer(String copyFromIssuerId, String copyToIssuerId) {
		boolean copyDone = false;
		List<PlanDisplayRules> displayRules = null;
		if(StringUtils.isBlank(copyFromIssuerId) || StringUtils.isBlank(copyToIssuerId) ) {
			LOGGER.error("Invalid input params passed to copy benefit display rules.");
		} else {
			displayRules = iPlanDisplayRulesRepository.findByHiosIssuerIdAndIsActive(copyFromIssuerId, "Y");
		}
		if(CollectionUtils.isEmpty(displayRules)) {
			LOGGER.error("Benefit display rules does not exist for copying for HIOS ID:" + copyFromIssuerId);
			return copyDone;
		} 

		EntityManager entityManager =  null;
		try {
			entityManager = entityManagerFactory.createEntityManager();
			if(null == entityManager || !entityManager.isOpen()){
				LOGGER.error("Failed to get entity manager while copying Benefit display rules from HIOS ID:" + copyFromIssuerId + " to issuer id " + copyToIssuerId);
			} else {
				entityManager.getTransaction().begin();
				for (PlanDisplayRules planDisplayRule : displayRules) {
					PlanDisplayRules newRule = makeCopyFromRule(planDisplayRule, copyToIssuerId);
					if(null != newRule) {
						entityManager.merge(newRule);
					}
				}
				copyDone = true;
			}
		} finally {
			if (null != entityManager && entityManager.isOpen()) {
				if(copyDone) {
					entityManager.getTransaction().commit();
				} else {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}
		return copyDone;	
	}
	
	private PlanDisplayRules makeCopyFromRule(PlanDisplayRules planDisplayRule, String hiosId) {
		if(null == planDisplayRule || StringUtils.isBlank(hiosId)) {
			LOGGER.error("Invalid input params passed to make a copy of benefit display rule.");
			return null;
		} else {
			PlanDisplayRules newRule = new PlanDisplayRules();
			Date curTime = new Date();
			newRule.setHiosIssuerId(hiosId);
			newRule.setCoinsuranceAttribute(planDisplayRule.getCoinsuranceAttribute());
			newRule.setCoinsuranceValue(planDisplayRule.getCoinsuranceValue());
			newRule.setCopayAttribute(planDisplayRule.getCopayAttribute());
			newRule.setCopayValue(planDisplayRule.getCopayValue());
			newRule.setPrimaryCareT1ForCopayAndVisits(planDisplayRule.getPrimaryCareT1ForCopayAndVisits());
			newRule.setPrimaryCareT1ForNumCopay(planDisplayRule.getPrimaryCareT1ForNumCopay());
			newRule.setPrimaryCareT1ForNumVisits(planDisplayRule.getPrimaryCareT1ForNumVisits());
			newRule.setStandardDisplayTemplate(planDisplayRule.getStandardDisplayTemplate());
			newRule.setTileDisplayTemplate(planDisplayRule.getTileDisplayTemplate());
			newRule.setCreatedBy(planDisplayRule.getCreatedBy());
			newRule.setCreationTimestamp(curTime);
			newRule.setIsActive(planDisplayRule.getIsActive());
			newRule.setLastUpdatedBy(planDisplayRule.getLastUpdatedBy());
			newRule.setLastUpdateTimestamp(curTime);
			return newRule;
		}
	}
	
	public String stripDecimalZero(String value) {
		String returnVal = null;
		if(StringUtils.isNotBlank(value)) {
			returnVal = value.trim();
			if(returnVal.endsWith(DOT_ZERO_ZERO)) {
				returnVal = returnVal.replace(DOT_ZERO_ZERO, StringUtils.EMPTY);
			} else if(returnVal.endsWith(DOT_ZERO)) {
				returnVal = returnVal.replace(DOT_ZERO, StringUtils.EMPTY);
			}
		} else {
			returnVal = value;
		}
		return returnVal;
	}

	@Override
	public boolean isExtendedParamSupported() {
		return false;
	}
}
