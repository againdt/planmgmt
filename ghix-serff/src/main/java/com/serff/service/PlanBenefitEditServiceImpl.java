package com.serff.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.serff.repository.templates.IPlanBenefitEditRepository;

/**
 * Interface Implementation Class used to provide services for plan benefit Edit.
 * @author Vani Sharma
 */
@Service("planBenefitEditService")
public class PlanBenefitEditServiceImpl implements PlanBenefitEditService{

	@Autowired private IPlanBenefitEditRepository iSerffPlanBenefitEditRepository;
	
	/**
	 * This method is used to fetch all eligible plans based on applicable year
	 */
	@Override
	public List<String> getAllPlansForApplicableYear(String applicableYear) {
		List<String> validPlans = new ArrayList<String>();
		if(null!= applicableYear){
			validPlans = iSerffPlanBenefitEditRepository.getAllPlansForApplicableYear(applicableYear);
		}
		return validPlans;
	}
	
	/**
	 * This method is used to fetch all distinct applicable years
	 */
	@Override
	public List<String> getAllApplicableYear() {
		return iSerffPlanBenefitEditRepository.getAllApplicableYear();
	}

}
