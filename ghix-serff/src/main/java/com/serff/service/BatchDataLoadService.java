package com.serff.service;

import java.util.List;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Tenant;

/**
 * All batch services with in SERFF need to implement this.
 * 
 * @author Parmar_b
 * @since 12/12/2013
 */
public interface BatchDataLoadService {
	
	String processSerffPlanMgmtBatch();
	
	List<Issuer> getIssuerNameList();
	
	List<Tenant> getTenantNameList();

	boolean checkWaitingPlansToLoad();
}
