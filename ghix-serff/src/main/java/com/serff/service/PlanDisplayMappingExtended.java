package com.serff.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.serff.dto.BenefitsDisplayMappingParamsDTO;
import com.serff.util.SerffUtils;

@Service("planDisplayMappingExtended")
public class PlanDisplayMappingExtended extends PlanDisplayMappingImpl {

	private static final Logger LOGGER = Logger.getLogger(PlanDisplayMappingExtended.class);

	/**
	 * Method is used to return Display Text from CoPay and CoInsurance data.
	 */
	@Override
	public String getBenefitDisplayText(String coinsValue, String coinsAttr,
			String copayValue, String copayAttr, long coinsAfterNoOffCopay,
			long afterNumberOfVisit, boolean isPrimaryCareT1,
			PlanDisplayRulesMap rulesMap, BenefitsDisplayMappingParamsDTO extendedParams) {
		StringBuffer displayText = new StringBuffer();
		//boolean ifGetText = true;
		String benefitName = null;
		if(null != extendedParams) {
			benefitName = extendedParams.getBenefitName();
			//if "Medical & drug deductibles integrated" is true and benefit name is "Prescription drug deductible", append 'Included in deductible'. 
			//Applicable to Ind-INN, ind-OON, Fam-INN and Fam-OON.
			//if(extendedParams.isMedicalAndDrugIntegrated() && null != benefitName && "".equals(benefitName)) { }
		} 
		//if(ifGetText)
		displayText.append(super.getBenefitDisplayText(coinsValue, coinsAttr, copayValue, copayAttr, coinsAfterNoOffCopay,
				afterNumberOfVisit, isPrimaryCareT1, rulesMap, extendedParams));
		
		if(null != extendedParams) {
			switch (extendedParams.getNetworkType()) {
				case IN_NETWORK_TIER1: {
					String postFix = processForInpatientHospitalSvc(benefitName, copayValue, copayAttr, extendedParams.getMaxNumDaysForChargingInpatientCopay());
					if(null != postFix) {
						displayText.append(postFix);
						if(LOGGER.isDebugEnabled()) {
							LOGGER.debug("INN-" + benefitName + " :copay: " + copayValue + " " + copayAttr 
									+ " :maxDaysForCharging: " + extendedParams.getMaxNumDaysForChargingInpatientCopay() + " ---- " + displayText.toString());
						}
					} else {
						displayText = processForSpecialtyDrugs(displayText, benefitName, coinsValue, coinsAttr, extendedParams.getMaximumCoinsuranceForSpecialtyDrugs(), extendedParams.isMedicalAndDrugIntegrated());
						if(LOGGER.isDebugEnabled()) {
							LOGGER.debug("INN-" + benefitName + " :coins: " + coinsValue + " " + coinsAttr + " :maxCoinsForSD: " 
									+ extendedParams.getMaximumCoinsuranceForSpecialtyDrugs() + " :medDrugIntgtd: " + extendedParams.isMedicalAndDrugIntegrated() + " ---- " + displayText.toString());
						}
					}
					break;
				}
				case IN_NETWORK_TIER2:
				case OUT_OF_NETWORK:
				default: {
					break;
				}
			}
		} 
		return displayText.toString();
	}
	
	private StringBuffer processForSpecialtyDrugs(StringBuffer displayText, String benefitName, String coinsValue,
			String coinsAttr, long maximumCoinsuranceForSpecialtyDrugs, boolean medicalAndDrugIntegrated) {
		// If "Maximum coinsurance for specialty drugs" is not null and Benefit name is "Specialty Drugs" and cost share type is coinsurance,  
		// additional text needs to be appended. Applicable only for INN, and depending on deductible is integrated or not the display text varies.
		StringBuffer newDisplayText = displayText;
		if(null != displayText && SerffUtils.SPECIALTY_DRUGS_BENEFIT.equalsIgnoreCase(benefitName) && maximumCoinsuranceForSpecialtyDrugs > 0) {
			double coInsuranceAmount = 0.0;
			if(!StringUtils.isBlank(coinsAttr) && !StringUtils.isBlank(coinsValue)) {
				coInsuranceAmount = Double.parseDouble(coinsValue);
			}
			if(coInsuranceAmount > 0.0) {
				String text = " to a maximum of $" + maximumCoinsuranceForSpecialtyDrugs + " per prescription";
				if(!medicalAndDrugIntegrated && displayText.indexOf("after deductible") > 0) {
					newDisplayText = displayText.insert(displayText.indexOf(" deductible"), " Prescription Drug");
				}
				newDisplayText.append(text);
			}
		}
		return newDisplayText;
	}

	private String processForInpatientHospitalSvc(String benefitName, String copayValue, String copayAttr, long maxNumDaysForChargingInpatientCopay) {
		String text = null;
		//"If Benefit Name is 'Inpatient Hospital Services (e.g., Hospital Stay)'; and have value like '$XX copay per day' for INN copay field 
		//and 'Maximum Number of Days for Charging an Inpatient Copay?' is present and > 0, append the text with the words 'to a maximum of $xxx per admission', 
		//where the 'xxx' value in this example equals the number of days times the per day copay. Value to be displayed only for INN. No Change to OON"
		if(SerffUtils.INPATIENT_HOSPITAL_SERVICE_BENEFIT.equalsIgnoreCase(benefitName) && maxNumDaysForChargingInpatientCopay > 0) {
			double copayAmount = 0.0;
			if(!StringUtils.isBlank(copayAttr) && copayAttr.toLowerCase().startsWith(SerffUtils.COPAY_PER_DAY.toLowerCase()) && !StringUtils.isBlank(copayValue)) {
				copayAmount = Double.parseDouble(copayValue);
			}
			if(copayAmount > 0) {
				text = " to a maximum of $" + stripDecimalZero(Double.toString(copayAmount * maxNumDaysForChargingInpatientCopay)) + " per admission";
			}
		}		
		return text;
	}
	
	@Override
	public boolean isExtendedParamSupported() {
		return true;
	}
}
