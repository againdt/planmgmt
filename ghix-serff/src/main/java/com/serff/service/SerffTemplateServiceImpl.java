/**
 * PlanMgmtSerffRestController.java
 * @author chalse_v
 * @version 1.0
 * @since Apr 4, 2013
 */
package com.serff.service;

import static com.getinsured.hix.platform.util.GhixConstants.EXCEPTION_INTERNAL_APPLICATION_ERROR_CODE;
import static com.getinsured.hix.platform.util.GhixConstants.EXCEPTION_INVALID_PARAMETER_CODE;
import static com.getinsured.hix.platform.util.GhixConstants.EXCEPTION_REQUIRED_FIELDS_MISSING_CODE;
import static com.getinsured.hix.platform.util.GhixConstants.RESPONSE_FAILURE;
import static com.getinsured.hix.platform.util.GhixConstants.RESPONSE_SUCCESS;
import static com.serff.planmanagementexchangeapi.common.model.pm.PlanStatus.RECEIVED;

import java.util.HashMap;
import java.util.Map;

import javax.validation.ValidationException;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws.PlanMgmtRequest;
import com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws.PlanMgmtResponse;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.serff.planmanagementexchangeapi.common.model.pm.PlanStatus;
import com.serff.service.templates.PlanMgmtSerffService;
import com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType;
import com.serff.template.plan.PlanBenefitTemplateVO;
import com.serff.template.plan.PrescriptionDrugTemplateVO;
import com.serff.template.plan.QhpApplicationRateGroupVO;
import com.serff.template.rbrules.extension.PayloadType;
import com.serff.template.urrt.InsuranceRateForecastTempVO;
import com.serff.util.PlanUpdateStatistics;
import com.serff.util.SerffConstants;

/**
 * REST web service for Transfer of Plan Details.
 *
 * @author sharma_v, Sunil Desu, chalse_v
 * @version 1.2
 * @since June 17, 2013
 *
 */
@Service("serffTemplateService")
public class SerffTemplateServiceImpl implements SerffTemplateService {

	private static final Logger LOGGER = Logger.getLogger(SerffTemplateServiceImpl.class);

	@Autowired
	@Qualifier("planMgmtSerffService")
	private PlanMgmtSerffService planMgmtSerffService;

	/**
	 * Constructor.
	 */
	public SerffTemplateServiceImpl() {
	}

	/**
	 * Handles the transferPlan operation. The six types of templates are sent
	 * as a part of a single request.
	 *
	 * @param planManagementRequest
	 * @return planManagementResponse
	 */
	@Override
	public PlanMgmtResponse tranferPlan(PlanMgmtRequest req, String exchangeTypeFilter, String tenantIds, PlanUpdateStatistics uploadStatistics, byte[] logoBytes) {

		LOGGER.info("transferPlan operation has been invoked.");
		LOGGER.info("tranferPlan() START");

		PlanMgmtResponse resp = new PlanMgmtResponse();

		try {

			Map<String, Object> reqTemplates = new HashMap<String, Object>();
			boolean status = Boolean.FALSE;

			resp.startResponse();

			LOGGER.info("ghixWeb : PlanMgmtSerffRestController : tranferPlan Invoked  ");

			Validate.notNull(req, "Invalid PlanMgmtRequest was associated with the request.");
			Validate.notNull(planMgmtSerffService, "PlanMgmtSerffSerivice instnace is not properly configured.");

			/** Extract and populate bean information/payload from SERFF request instance */
			//PodamFactory factory = new PodamFactoryImpl();

			//PlanMgmtRequest srcRequestObject = factory.manufacturePojo(PlanMgmtRequest.class);
			//System.out.println(srcRequestObject.getAdminTemplate().getIssuer().getIssuerIdentification().getIdentificationID().getValue());


			com.serff.template.admin.extension.PayloadType adminTemplate = req.getAdminTemplate();
			
			reqTemplates.put(SerffConstants.SUPPORTING_DOC, req.getSupportingDocuments());
			reqTemplates.put(SerffConstants.DATA_ADMIN_DATA, adminTemplate);
			
			com.serff.template.svcarea.extension.PayloadType serviceAreaTemplate = req.getServiceArea();
			Validate.notNull(serviceAreaTemplate, "Invalid serviceAreaTemplate Payload instance associated with PlanMgmtRequest.");

			reqTemplates.put(SerffConstants.DATA_SERVICE_AREA, serviceAreaTemplate);

			com.serff.template.networkProv.extension.PayloadType networkProvider = req.getNetworkProvider();
			Validate.notNull(networkProvider, "Invalid network provider Payload instance associated with PlanMgmtRequest.");
			reqTemplates.put(SerffConstants.DATA_NETWORK_ID, networkProvider);

			QhpApplicationRateGroupVO ratesTemplate = req.getRates();
			Validate.notNull(ratesTemplate, "Invalid Rate Template Payload instance associated with PlanMgmtRequest.");
			reqTemplates.put(SerffConstants.DATA_RATING_TABLE, ratesTemplate);

			PrescriptionDrugTemplateVO prescriptionDrug = req.getPrescriptionDrug();
			//Prescription Drug template is not a mandatory one
			//Validate.notNull(prescriptionDrug, "Invalid prescriptionDrug Payload instance associated with PlanMgmtRequest.");
			reqTemplates.put(SerffConstants.DATA_PRESCRIPTION_DRUG, prescriptionDrug);
			
			PayloadType businessRule = req.getRateBussinessRules();
			reqTemplates.put(SerffConstants.DATA_RATING_RULES, businessRule);

			reqTemplates.put(SerffConstants.TRANSFER_PLAN, req.getTransferPlan());

			PlanBenefitTemplateVO planBenefitTemplate = req.getPlanBenefit();
			Validate.notNull(planBenefitTemplate, "Invalid PlanBenefitTemplateVO Payload instance associated with PlanMgmtRequest.");
			reqTemplates.put(SerffConstants.DATA_BENEFITS, planBenefitTemplate);
			
			
			InsuranceRateForecastTempVO unifiedRateTemplate = req.getUnifiedRate();
			reqTemplates.put(SerffConstants.DATA_UNIFIED_RATE, unifiedRateTemplate);
			
			com.serff.template.urac.IssuerType uracType = req.getUracTemplate();
			reqTemplates.put(SerffConstants.DATA_URAC_DATA, uracType);
			
			com.serff.template.ncqa.IssuerType ncqaType = req.getNcqaTemplate();
			reqTemplates.put(SerffConstants.DATA_NCQA_DATA, ncqaType);
			
			
			/** Persist the beans in repository*/
			StringBuffer responseMessage = new StringBuffer();
			status = planMgmtSerffService.populateAndPersistAllBeansFromSERFF(reqTemplates, exchangeTypeFilter, responseMessage, tenantIds, uploadStatistics, logoBytes);
			LOGGER.debug(" tranferPlan() : status : " + status + " Response from PlanMgmtSerffService : " + responseMessage.toString());
			
			if(status){
				LOGGER.info("The SERFF data was successful persisted.");
				
				if(responseMessage.toString().contains(SerffConstants.PLAN_CERTIFIED_MESSAGE_NO_FUTURE_RATES) || 
						responseMessage.toString().contains(SerffConstants.PLAN_CERTIFIED_MESSAGE_RATES_NOT_MODIFIED) || 
						responseMessage.toString().contains(SerffConstants.TEMPLATES_NOT_MODIFIED_MESSAGE)){
					resp.setStatus(GhixConstants.RESPONSE_FAILURE);
					resp.setErrCode(SerffConstants.PLAN_LOADED_WITH_WARNING_CODE);
					resp.setErrMsg(responseMessage.toString());
				}else{
					resp.setStatus(GhixConstants.RESPONSE_SUCCESS);
					resp.setErrMsg(responseMessage.toString());
				}
				
			}
			else {
				LOGGER.error("The persistence of SERFF data was unsuccessful at this time. Kindly refer server logs for more information.");
				resp.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}catch(ValidationException vex){
			LOGGER.error("Validation exception occurred.", vex);
			resp.setStatus(RESPONSE_FAILURE);
			resp.setErrMsg((null == resp.getErrMsg())? vex.getMessage(): (resp.getErrMsg() + "," + vex.getMessage()));
			resp.setErrCode(GhixConstants.EXCEPTION_REQUIRED_FIELDS_MISSING_CODE.intValue());			
		}catch(IllegalArgumentException iex){
			LOGGER.error("Illegal argument exception occurred.", iex);
			resp.setStatus(RESPONSE_FAILURE);
			resp.setErrMsg((null == resp.getErrMsg())? iex.getMessage(): (resp.getErrMsg() + "," + iex.getMessage()));
			resp.setErrCode(GhixConstants.EXCEPTION_INVALID_PARAMETER_CODE.intValue());
		}catch(GIException ex){
			LOGGER.error("GI exception occurred.", ex);
			resp.setStatus(RESPONSE_FAILURE);
			resp.setErrMsg((null == resp.getErrMsg())? ex.getMessage(): (resp.getErrMsg() + "," + ex.getMessage()));
			resp.setErrCode(((GIException) ex).getErrorCode());
		}catch(Exception ex) {
			LOGGER.error("Exception occurred while persisting SERFF data. Exception:" + ex.getMessage(), ex);
			resp.setStatus(GhixConstants.RESPONSE_FAILURE);
			resp.setErrCode(GhixConstants.EXCEPTION_INTERNAL_APPLICATION_ERROR_CODE.intValue());
			resp.setErrMsg((null == resp.getErrMsg())? ((null != ex.getCause() ? ex.getCause().getMessage() : ex.getMessage())): (resp.getErrMsg() + "," + (null != ex.getCause() ? ex.getCause().getMessage() : ex.getMessage())));
		}
		/*catch(Error er) {
			LOGGER.error("Exception occurred while persisting SERFF data. Exception:" + er.getMessage(), er);

			resp.setStatus(GhixConstants.RESPONSE_FAILURE);
			resp.setErrMsg((null == resp.getErrMsg())? er.getMessage(): (resp.getErrMsg() + "," + er.getMessage()));

			if(er instanceof IOError) {
				resp.setErrCode(GhixConstants.EXCEPTION_IO_ERROR_CODE.intValue());
			}
			else {
				resp.setErrCode(GhixConstants.EXCEPTION_INTERNAL_APPLICATION_ERROR_CODE.intValue());
			}
		}*/
		finally {
			resp.setPlanStatus(PlanStatus.RECEIVED);
			resp.endResponse();
			LOGGER.info("tranferPlan() END");
		}
		return resp;
	}

	/**
	 * This method process the CSR templates.
	 * processCSR is returned in the response.
	 *
	 * @author Geetha Chandran, Vinayak Chalse
	 * @version 1.1
	 *
	 * @param PlanMgmtRequest
	 *            UpdatePlanStatus is sent in the request.
	 * @return PlanMgmtResponse
	 */
	@Override
	public PlanMgmtResponse processCSR(PlanMgmtRequest req) {
		LOGGER.info("Beginning processCSR operation.");
		PlanMgmtResponse response = new PlanMgmtResponse();

		try {
			LOGGER.info("ProcessCSR is called with the following request " + req.toString());
			CSRAdvancePaymentDeterminationType advancePaymentDeterminationType = req.getCsrTemplate();

			Validate.notNull(advancePaymentDeterminationType, "Invalid CSRAdvancePaymentDeterminationType found for request");

//			if(advancePaymentDeterminationType instanceof CSRAdvancePaymentDeterminationType) {
				Boolean booleanResponse = planMgmtSerffService.populateAndPersistCSRAmountFromSERFF(advancePaymentDeterminationType);

				if(booleanResponse) {
					response.setStatus(RESPONSE_SUCCESS);
				}
				else {
					response.setStatus(RESPONSE_FAILURE);
					response.setErrCode(EXCEPTION_INTERNAL_APPLICATION_ERROR_CODE.intValue());
					response.setErrMsg((null == response.getErrMsg())? booleanResponse.toString(): (response.getErrMsg() + "," + booleanResponse.toString()));
				}
//			}

		}catch(ValidationException vex){
			LOGGER.error("Validqation exception occurred.", vex);
			response.setStatus(RESPONSE_FAILURE);
			response.setErrMsg((null == response.getErrMsg())? vex.getMessage(): (response.getErrMsg() + "," + vex.getMessage()));
			if(null != vex.getMessage() && vex.getMessage().contains("improperly") && vex.getMessage().contains("configured")) {
				response.setErrCode(EXCEPTION_INTERNAL_APPLICATION_ERROR_CODE.intValue());
			}
			else {
				response.setErrCode(EXCEPTION_REQUIRED_FIELDS_MISSING_CODE.intValue());
			}
		}catch(IllegalArgumentException iex){
			LOGGER.error("IllegalArgumentException occurred.", iex);
			response.setStatus(RESPONSE_FAILURE);
			response.setErrMsg((null == response.getErrMsg())? iex.getMessage(): (response.getErrMsg() + "," + iex.getMessage()));
			response.setErrCode(EXCEPTION_INVALID_PARAMETER_CODE.intValue());
		}catch(Exception ex){
			LOGGER.error("Exception occurred.", ex);
			response.setStatus(RESPONSE_FAILURE);
			response.setErrMsg((null == response.getErrMsg())? ex.getMessage(): (response.getErrMsg() + "," + ex.getMessage()));
			response.setErrCode(EXCEPTION_INTERNAL_APPLICATION_ERROR_CODE.intValue());
		}
		/*catch(Error er) {
			LOGGER.error("Exception occurred while persisting SERFF CSR data. Exception:" + er.getMessage(), er);

			response.setStatus(RESPONSE_FAILURE);
			response.setErrMsg((null == response.getErrMsg())? er.getMessage(): (response.getErrMsg() + "," + er.getMessage()));

			if(er instanceof IOError) {
				response.setErrCode(EXCEPTION_IO_ERROR_CODE.intValue());
			}
			else {
				response.setErrCode(EXCEPTION_INTERNAL_APPLICATION_ERROR_CODE.intValue());
			}
		}*/
		finally {
			response.setPlanStatus(RECEIVED);
			response.endResponse();

			LOGGER.info("processCSR operation has been done.");
		}
		return response;
	}

}
