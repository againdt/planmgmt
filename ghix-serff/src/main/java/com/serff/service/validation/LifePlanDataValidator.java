package com.serff.service.validation;

import static com.serff.util.SerffConstants.NEW_LINE;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.serffadapter.life.LifeExcelRow.LifePlanDataRowEnum;
import com.getinsured.serffadapter.life.LifeExcelRow.LifePlanRatesColumnEnum;
import com.getinsured.serffadapter.life.LifePlanDataVO;
import com.getinsured.serffadapter.life.LifePlanRatesVO;
import com.serff.util.SerffConstants;

/**
 *-----------------------------------------------------------------------------
 * HIX-59162 PHIX - Load and persist life plans.
 *-----------------------------------------------------------------------------
 * 
 * This Class is used to validate Life Plan Excel.
 * 
 * @author Bhavin Parmar
 * @since January 27, 2015
 */
@Component
public class LifePlanDataValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(LifePlanDataValidator.class);

	private static final String EMSG_INVALID_URL = " Invalid URL." + NEW_LINE;
	private static final String EMSG_EMPTY = " Value should not be empty." + NEW_LINE;
	private static final String EMSG_TEXT_LENGTH_EXCEEDS = " Text length is greater than maximum length allowed." + NEW_LINE;
	private static final String EMSG_INVALID_STATE = " Invalid State." + NEW_LINE;
	private static final String EMSG_INVALID_PLAN_TYPE = " Invalid Plan Type." + NEW_LINE;
	private static final String EMSG_INVALID_DATE = " Invalid Start date format. Format must be DD/MM/YYYY." + NEW_LINE;
	private static final String EMSG_AMOUNT = "Amount must be numeric or positive value" + NEW_LINE;
	private static final String EMSG_WHOLE_AMOUNT = "Amount must be whole numeric and positive value" + NEW_LINE;

	private static final String PLAN_TYPE = "LIFE";

	/**
	 * Method is used to validate require data from validateLifeExcelVO object before persistence.
	 * 
	 * @param lifePlanDataVO - LifePlanDataVO instance
	 * @return - Whether LifePlanDataVO having valid data to persist
	 */
	public boolean validateLifeExcelVO(LifePlanDataVO lifePlanDataVO) {

		LOGGER.info("validateLifeExcelVO() Start");
		boolean isValid = true;

		try {
			if (null == lifePlanDataVO || CollectionUtils.isEmpty(lifePlanDataVO.getLifePlanRateList())) {
				LOGGER.error("LifePlanDataVO/LifePlanRatesVO-List is empty !!");
				lifePlanDataVO.setErrorMessages("LifePlanData/LifePlanRates is NULL !!");
				return false;
			}

			StringBuilder errorMessages = new StringBuilder();

			LOGGER.info("Validations for LifePlanDataVO: Start");
			// validateBlankData(lifePlanDataVO.getCarrierName(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.CARRIER_NAME.getRowName());
			validateTextWithLength(lifePlanDataVO.getCarrierHIOSId(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.CARRIER_HIOS_ID.getRowName(), SerffValidator.LEN_HIOS_ISSUER_ID);
			validateState(lifePlanDataVO, errorMessages, LifePlanDataRowEnum.STATE_LIST.getRowName());
			validateTextWithLength(lifePlanDataVO.getPlanId(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.PLAN_ID.getRowName(), SerffValidator.LEN_LIFE_PLAN_ID);
			validateBlankData(lifePlanDataVO.getPlanName(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.PLAN_NAME.getRowName());
			validateBlankData(lifePlanDataVO.getStartDate(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.START_DATE.getRowName());
			validateStartDate(lifePlanDataVO, errorMessages, LifePlanDataRowEnum.START_DATE.getRowName());
			validatePlanType(lifePlanDataVO, errorMessages, LifePlanDataRowEnum.PLAN_TYPE.getRowName());
			validateURL(lifePlanDataVO.getBenefitDocument(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.BENEFIT_DOCUMENT.getRowName());

			validateBlankData(lifePlanDataVO.getPlanDuration(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.PLAN_DURATION.getRowName());
			validateBlankData(lifePlanDataVO.getMinCoverageAmount(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.MIN_COVERAGE_AMOUNT.getRowName());
			validateBlankData(lifePlanDataVO.getMaxCoverageAmount(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.MAX_COVERAGE_AMOUNT.getRowName());
			validateWholeNumber(lifePlanDataVO.getPlanDuration(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.PLAN_DURATION.getRowName());
			validateWholeAmount(lifePlanDataVO.getMinCoverageAmount(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.MIN_COVERAGE_AMOUNT.getRowName());
			validateWholeAmount(lifePlanDataVO.getMaxCoverageAmount(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.MAX_COVERAGE_AMOUNT.getRowName());

			validateAmount(lifePlanDataVO.getAnnualPolicyFee(), lifePlanDataVO, errorMessages, LifePlanDataRowEnum.ANNUAL_POLICY_FEE.getRowName());
			LOGGER.info("Validations for LifePlanDataVO: End");

			LOGGER.info("Validations for LifePlanRatesVO - List: Start");
			for (LifePlanRatesVO lifePlanRatesVO : lifePlanDataVO.getLifePlanRateList()) {
				validateTextWithLength(lifePlanRatesVO.getCarrierHIOSId(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.CARRIER_HIOS_ID.getColumnName(), SerffValidator.LEN_HIOS_ISSUER_ID);
				validateTextWithLength(lifePlanRatesVO.getPlanID(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.PLAN_ID.getColumnName(), SerffValidator.LEN_LIFE_PLAN_ID);
				// validateBlankData(lifePlanRatesVO.getPlanName(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.PLAN_NAME.getColumnName());

				validateBlankData(lifePlanRatesVO.getMinimumAge(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.MIN_AGE.getColumnName());
				validateBlankData(lifePlanRatesVO.getMaximumAge(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.MAX_AGE.getColumnName());
				validateWholeNumber(lifePlanRatesVO.getMinimumAge(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.MIN_AGE.getColumnName());
				validateWholeNumber(lifePlanRatesVO.getMaximumAge(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.MAX_AGE.getColumnName());

				validateBlankData(lifePlanRatesVO.getNonTobaccoMale(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.NON_TOBACCO_MALE.getColumnName());
				validateBlankData(lifePlanRatesVO.getTobaccoMale(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.TOBACCO_MALE.getColumnName());
				validateBlankData(lifePlanRatesVO.getNonTobaccoFemale(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.NON_TOBACCO_FEMALE.getColumnName());
				validateBlankData(lifePlanRatesVO.getTobaccoFemale(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.TOBACCO_FEMALE.getColumnName());
				validateAmount(lifePlanRatesVO.getNonTobaccoMale(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.NON_TOBACCO_MALE.getColumnName());
				validateAmount(lifePlanRatesVO.getTobaccoMale(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.TOBACCO_MALE.getColumnName());
				validateAmount(lifePlanRatesVO.getNonTobaccoFemale(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.NON_TOBACCO_FEMALE.getColumnName());
				validateAmount(lifePlanRatesVO.getTobaccoFemale(), lifePlanDataVO, errorMessages, LifePlanRatesColumnEnum.TOBACCO_FEMALE.getColumnName());
			}
			LOGGER.info("Validations for LifePlanRatesVO - List: End");

			if (lifePlanDataVO.isNotValid()) {
				isValid = false;

				if (StringUtils.isNotBlank(lifePlanDataVO.getCarrierHIOSId()) &&
						StringUtils.isNotBlank(lifePlanDataVO.getPlanId())) {
					errorMessages.append("Skipping to persist Life Plans [");
					errorMessages.append(lifePlanDataVO.getCarrierHIOSId());
					errorMessages.append(SerffConstants.DEFAULT_PHIX_STATE_CODE);
					errorMessages.append(lifePlanDataVO.getPlanId());
					errorMessages.append("] due to validations error.");
					lifePlanDataVO.setErrorMessages(errorMessages.toString());
				}
				else {
					lifePlanDataVO.setErrorMessages(errorMessages.toString() + "Skipping to persist Life plan due to validations error.");
				}
				lifePlanDataVO.setErrorMessages(lifePlanDataVO.getErrorMessages() + "\n=========================================================");
			}
		}
		finally {
			LOGGER.info("validateLifeExcelVO() End");
		}
		return isValid;
	}

	/**
	 * Validates whether Start Date is in valid format.
	 * 	
	 * @param lifePlanDataVO - LifePlanDataVO instance
	 * @param errorMessages - Message to be returned after validating date.
	 * @param cellHeader - Start Date parameter name from enum.
	 */
	private void validateStartDate(LifePlanDataVO lifePlanDataVO, StringBuilder errorMessages, String cellHeader) {

		LOGGER.debug("validateStartDate() Start");
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		try {

			if (StringUtils.isNotBlank(lifePlanDataVO.getStartDate())){
				formatter.parse(lifePlanDataVO.getStartDate());
			}
		}
		catch (ParseException e) {
			errorMessages.append(cellHeader + "[" + lifePlanDataVO.getStartDate() + "]" + SerffConstants.COMMA + EMSG_INVALID_DATE);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
			LOGGER.error("Exception occured while parsing date", e);
		}
		LOGGER.debug("validateStartDate() End");
	}

	/**
	 * Method is used to validate Blank Data from Cell Value.
	 * 
	 * @param cellValue - The value read from excel cell.
	 * @param lifePlanDataVO - LifePlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateBlankData(String cellValue, LifePlanDataVO lifePlanDataVO, StringBuilder errorMessages, String cellHeader) {
		LOGGER.debug("validateBlankData() Start");

		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateBlankData() End");
	}

	/**
	 * Validates Plan Type.
	 * 
	 * @param lifePlanDataVO - LifePlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validatePlanType(LifePlanDataVO lifePlanDataVO, StringBuilder errorMessages, String cellHeader) {
		LOGGER.debug("validatePlanType() Start");

		if (StringUtils.isBlank(lifePlanDataVO.getPlanType())) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
		}
		else if (!PLAN_TYPE.equalsIgnoreCase(lifePlanDataVO.getPlanType())) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_INVALID_PLAN_TYPE);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validatePlanType() End");
	}

	/**
	 * Validates whether text is not exceeding permissible maxlength.
	 * 
	 * @param cellValue - The value read from excel cell.
	 * @param lifePlanDataVO - LifePlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateTextWithLength(String cellValue, LifePlanDataVO lifePlanDataVO, StringBuilder errorMessages, String cellHeader, int maxLength) {

		LOGGER.debug("validateTextWithLength() Start");

		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
		}
		else if(StringUtils.isNotBlank(cellValue) && (cellValue.length() > maxLength )){
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_TEXT_LENGTH_EXCEEDS);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateTextWithLength() End");
	}

	/**
	 * Method is used to validate Number value from Cell Value.
	 * 
	 * @param cellValue - The value read from excel cell.
	 * @param lifePlanDataVO - LifePlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateAmount(String cellValue, LifePlanDataVO lifePlanDataVO, StringBuilder errorMessages, String cellHeader) {

		LOGGER.debug("validateNumber() Start");
		if (StringUtils.isNotBlank(cellValue) && !validateAmount(cellValue)) {

			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_AMOUNT);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateNumber() End");
	}

	/**
	 * Method is used to validate Integer value from Cell Value.
	 * 
	 * @param cellValue - The value read from excel cell.
	 * @param lifePlanDataVO - LifePlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateWholeAmount(String cellValue, LifePlanDataVO lifePlanDataVO, StringBuilder errorMessages, String cellHeader) {

		LOGGER.debug("validateWholeAmount() Start");
		if (StringUtils.isNotBlank(cellValue) && !validateWholeAmount(cellValue)) {

			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_WHOLE_AMOUNT);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateWholeAmount() End");
	}

	/**
	 * Method is used to validate Integer value from Cell Value.
	 * 
	 * @param cellValue - The value read from excel cell.
	 * @param lifePlanDataVO - LifePlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateWholeNumber(String cellValue, LifePlanDataVO lifePlanDataVO, StringBuilder errorMessages, String cellHeader) {

		LOGGER.debug("validateWholeAmount() Start");
		if (!StringUtils.isNumeric(cellValue)) {

			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_WHOLE_AMOUNT);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateWholeAmount() End");
	}

	/**
	 * Validates state field.
	 * 
	 * @param lifePlanDataVO - LifePlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateState(LifePlanDataVO lifePlanDataVO, StringBuilder errorMessages, String cellHeader) {
		LOGGER.debug("validateState() Start");

		if (CollectionUtils.isEmpty(lifePlanDataVO.getStateList())) {
			errorMessages.append(cellHeader + "State list" + SerffConstants.COMMA + EMSG_EMPTY);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
			return;
		}

		for (String state : lifePlanDataVO.getStateList()) {

			if (!validateState(state)) {
	
				errorMessages.append(cellHeader + "[" + state + "]" + SerffConstants.COMMA + EMSG_INVALID_STATE);

				if (!lifePlanDataVO.isNotValid()) {
					lifePlanDataVO.setNotValid(true);
				}
			}
		}
		LOGGER.debug("validateState() End");
	}

	/**
	 * This method validates URL field value.
	 * 
	 * @param cellValue - The value read from excel cell.
	 * @param lifePlanDataVO - LifePlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateURL(String cellValue, LifePlanDataVO lifePlanDataVO, StringBuilder errorMessages, String cellHeader) {

		LOGGER.debug("validateURL() Start");

		if (StringUtils.isNotBlank(cellValue) && !(isValidPattern(PATTERN_URL, StringUtils.trim(cellValue)))) {

			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_INVALID_URL);

			if (!lifePlanDataVO.isNotValid()) {
				lifePlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateURL() End");
	}
}
