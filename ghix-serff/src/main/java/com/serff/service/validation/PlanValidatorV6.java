package com.serff.service.validation;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.serff.template.plan.CostShareVarianceVO;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.PlanAndBenefitsVO;
import com.serff.template.plan.SBCVO;

/**
 * Class is used to to implements all validations of Plan & Benefits Template for Version 4.
 * 
 * @author Bhavin Parmar
 * @since Mar 4, 2015
 */
@Component("planValidatorV6")
public class PlanValidatorV6 extends PlanValidator {

	private static final Logger LOGGER = Logger.getLogger(PlanValidatorV6.class);

	/**
	 * This method is used for validating plan attributes fields
	 * @param planAttributeVO, Object of PlanAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	@Override
	protected void validatePlanCostCalcAttributes(PlanAndBenefitsVO planAndBenefitsVO, List<FailedValidation> errors) {

		if(null != planAndBenefitsVO) {
			String fieldName = null;
			for (CostShareVarianceVO costShareVarianceVO : planAndBenefitsVO.getCostShareVariancesList().getCostShareVariance()) {

				if (null != costShareVarianceVO) {
					// validate costSharVariations fields
					fieldName = "Maximum Coinsurance For Specialty Drugs";
					LOGGER.debug(MSG_PLAN_ATTR + fieldName);
					validateCostCalculatorFields(fieldName,costShareVarianceVO.getAvCalculator().getMaximumCoinsuranceForSpecialtyDrugs(),errors);

					fieldName = "Max Num Days For Charging Inpatient Copay";
					LOGGER.debug(MSG_PLAN_ATTR + fieldName);
					validateCostCalculatorFields(fieldName,costShareVarianceVO.getAvCalculator().getMaxNumDaysForChargingInpatientCopay(),errors);

					fieldName="Begin Primary Care Cost Sharing After Set Number Visits";
					LOGGER.debug(MSG_PLAN_ATTR + fieldName);
					validateCostCalculatorFields(fieldName,costShareVarianceVO.getAvCalculator().getBeginPrimaryCareCostSharingAfterSetNumberVisits(),errors);

					fieldName="Begin Primary Care Deductible Or Coinsurance After Set Number Copays";
					LOGGER.debug(MSG_PLAN_ATTR + fieldName);
					validateCostCalculatorFields(fieldName,costShareVarianceVO.getAvCalculator().getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays(),errors);
				}
			}			
		}
	}
	
	@Override
	protected ExcelCellVO getIssuerActuarialValue(CostShareVarianceVO costShareVarianceVO) {
		if(null != costShareVarianceVO && null != costShareVarianceVO.getAvCalculator()) {
			return costShareVarianceVO.getAvCalculator().getIssuerActuarialValue();
		}
		return null;
	}

	/**
	 * Method is used to validate SBC data.
	 *
	 * @param sbcVO, Object of SBCVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateSBCData(SBCVO sbcVO, List<FailedValidation> errors, boolean isNullCheck, boolean isWholeNumberCheck) {

		super.validateSBCData(sbcVO, errors, isNullCheck, isWholeNumberCheck);

		String fieldName = "Having Simple Fracture Coinsurance";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingSimplefractureCoInsurance(), errors, isNullCheck, isWholeNumberCheck);

		fieldName = "Having Simple Fracture Copayment";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingSimplefractureCopayment(), errors, isNullCheck, isWholeNumberCheck);

		fieldName = "Having Simple Fracture Deductible";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingSimplefractureDeductible(), errors, isNullCheck, isWholeNumberCheck);

		fieldName = "Having Simple Fracture Limit";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingSimplefractureLimit(), errors, isNullCheck, isWholeNumberCheck);
	}
	
	/* (non-Javadoc)
	 * @see com.serff.service.validation.PlanValidator#validatePlanCSVMarketingName(com.serff.template.plan.CostShareVarianceVO, java.util.List)
	 */
	@Override
	protected void validatePlanCSVMarketingName(CostShareVarianceVO costShareVarianceVO,
			List<FailedValidation> errors) {
		if(null != costShareVarianceVO && null != errors) {
			validatePlanMarketingName(costShareVarianceVO.getPlanVariantMarketingName(), errors);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.serff.service.validation.PlanValidator#validateEhbApportionmentForPediatricDental(java.lang.String, com.serff.template.plan.ExcelCellVO, java.util.List)
	 */
	@Override
	protected void validateEhbApportionmentForPediatricDental(String fieldName, ExcelCellVO ehbApptForPediatricDental,
			List<FailedValidation> errors) {
		validatePercentAsFraction(fieldName, ehbApptForPediatricDental, errors, true);
	}
	
	/* (non-Javadoc)
	 * @see com.serff.service.validation.PlanValidator#validateDesignType(java.lang.String, com.serff.template.plan.ExcelCellVO, java.util.List)
	 */
	@Override
	protected void validateDesignType(String fieldName, ExcelCellVO designTypeExcelCellVO, List<FailedValidation> errors) {
		validatePlanAttributes(fieldName, designTypeExcelCellVO, errors);
	}
}
