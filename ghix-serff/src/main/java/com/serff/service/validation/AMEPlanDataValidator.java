package com.serff.service.validation;

import static com.serff.util.SerffConstants.NEW_LINE;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.serffadapter.ame.AMEAgeExcelColumn.AMEAgePremiumFactorColumnEnum;
import com.getinsured.serffadapter.ame.AMEAgeExcelColumn.AMEAgeRatesColumnEnum;
import com.getinsured.serffadapter.ame.AMEAgeGroupExcelVO;
import com.getinsured.serffadapter.ame.AMEExcelRow.AMEPlansExcelRowEnum;
import com.getinsured.serffadapter.ame.AMEExcelRow.AMERatesExcelRowEnum;
import com.getinsured.serffadapter.ame.AMEExcelVO;
import com.getinsured.serffadapter.ame.AMEPremiumFactorExcelVO;
import com.serff.util.SerffConstants;

/**
 * Class is used to validate AME Plan Excel.
 * 
 * @author Bhavin Parmar
 * @since August 18, 2014
 */
@Component
public class AMEPlanDataValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(AMEPlanDataValidator.class);

	private static final String EMSG_LENGTH = " Value must have length ";
	private static final String EMSG_EMPTY = " Value should not be empty." + NEW_LINE;
	private static final String EMSG_INVALID_STATE = " Invalid State." + NEW_LINE;
	private static final String EMSG_NUMERIC = " Value must be numeric." + NEW_LINE;
	private static final String EMSG_INVALID_URL = " Invalid URL/ECM-ID." + NEW_LINE;
	private static final String EMSG_INVALID_DATE = " Invalid Start date format. Format must be DD/MM/YYYY." + NEW_LINE;
	
	/**
	 * Method is used to validate require data from AMEExcelVO object before persistence.
	 */
	public boolean validateAMEExcelVO(String hiosPlanNumber, AMEExcelVO ameExcelVO, String planType) {
		
		LOGGER.debug("validateAMEExcelVO() Start");
		boolean isValid = Boolean.TRUE;
		
		try {
			
			if (StringUtils.isBlank(hiosPlanNumber) || null == ameExcelVO) {
				LOGGER.error("HIOSPlanNumber/AMEExcelVO is empty !!");
				return Boolean.FALSE;
			}
			
			StringBuilder errorMessages = new StringBuilder();
			/*validateBlankData(ameExcelVO.getCarrierHIOS(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowName());
			validateBlankData(ameExcelVO.getState(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.STATE.getRowName());
			validateBlankData(ameExcelVO.getPlanID(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.PLAN_ID.getRowName());*/
			LOGGER.info("ameExcelVO.getCarrierName(): " + ameExcelVO.getCarrierName());
			
			validateBlankData(ameExcelVO.getCarrierHIOS(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowName());
			validateBlankData(ameExcelVO.getCarrierName(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.CARRIER_NAME.getRowName());
			validateBlankData(ameExcelVO.getPlanName(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.PLAN_NAME.getRowName());
			validateBlankData(ameExcelVO.getPlanType(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.PLAN_TYPE.getRowName());
			validateBlankData(ameExcelVO.getDeductibleValue(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.DEDUCTIBLE_VALUE.getRowName());
			validateBlankData(ameExcelVO.getDeductibleDesc(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.DEDUCTIBLE_DESC.getRowName());
			validateBlankData(ameExcelVO.getMaxBenefitValue(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.MAX_BENEFIT_VALUE.getRowName());
			validateBlankData(ameExcelVO.getMaxBenefitDesc(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.MAX_BENEFIT_DESC.getRowName());
			validateBlankData(ameExcelVO.getSupplementalAccident(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.SUPPLEMENTAL_ACCIDENT.getRowName());
			validateBlankData(ameExcelVO.getEmergencyTreatment(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.EMERGENCY_TREATMENT.getRowName());
			validateBlankData(ameExcelVO.getFollowUpTreatment(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.FOLLOW_UP_TREATMENT.getRowName());
			validateBlankData(ameExcelVO.getAmbulance(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.AMBULANCE.getRowName());
			validateBlankData(ameExcelVO.getInitialHospitalization(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.INITIAL_HOSPITALIZATION.getRowName());
			validateBlankData(ameExcelVO.getHospitalStay(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.HOSPITAL_STAY.getRowName());
			validateBlankData(ameExcelVO.getSurgery(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.SURGERY.getRowName());
			validateBlankData(ameExcelVO.getAccidentalDeath(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.ACCIDENTAL_DEATH.getRowName());
			validateBlankData(ameExcelVO.getAccidentalDismemberment(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.ACCIDENTAL_DISMEMBERMENT.getRowName());
			validateBlankData(ameExcelVO.getOtherBenefits(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.OTHER_BENEFITS.getRowName());
			validateURL(ameExcelVO.getPlanBrochure(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.PLAN_BROCHURE.getRowName());
			validateURL(ameExcelVO.getExclusionsAndLimitations(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.EXCLUSIONS_AND_LIMITATIONS.getRowName());
			
			validateInteger(true, ameExcelVO.getDeductibleValue(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.DEDUCTIBLE_VALUE.getRowName());
			validateInteger(true, ameExcelVO.getMaxBenefitValue(), ameExcelVO, errorMessages, AMEPlansExcelRowEnum.MAX_BENEFIT_VALUE.getRowName());
			validateState(ameExcelVO, errorMessages, AMEPlansExcelRowEnum.STATE.getRowName());
			validateDate(ameExcelVO, errorMessages, AMEPlansExcelRowEnum.START_DATE.getRowName());
			
			if (SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType)) {
				LOGGER.info("Validations for AME Rates For Family");
				validateBlankData(ameExcelVO.getCoupleRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.COUPLE.getRowName());
				validateBlankData(ameExcelVO.getOneAdultOneDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.ONE_ADULT_ONE_KID.getRowName());
				validateBlankData(ameExcelVO.getOneAdultTwoDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.ONE_ADULT_TWO_KID.getRowName());
				validateBlankData(ameExcelVO.getOneAdultThreePlusDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowName());
				validateBlankData(ameExcelVO.getTwoAdultOneDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.TWO_ADULT_ONE_KID.getRowName());
				validateBlankData(ameExcelVO.getTwoAdultTwoDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.TWO_ADULT_TWO_KID.getRowName());
				validateBlankData(ameExcelVO.getTwoAdultThreePlusDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowName());
				validateBlankData(ameExcelVO.getIndividualRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.INDIVIDUAL.getRowName());
				
				validateNumber(ameExcelVO.getCoupleRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.COUPLE.getRowName());
				validateNumber(ameExcelVO.getOneAdultOneDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.ONE_ADULT_ONE_KID.getRowName());
				validateNumber(ameExcelVO.getOneAdultTwoDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.ONE_ADULT_TWO_KID.getRowName());
				validateNumber(ameExcelVO.getOneAdultThreePlusDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowName());
				validateNumber(ameExcelVO.getTwoAdultOneDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.TWO_ADULT_ONE_KID.getRowName());
				validateNumber(ameExcelVO.getTwoAdultTwoDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.TWO_ADULT_TWO_KID.getRowName());
				validateNumber(ameExcelVO.getTwoAdultThreePlusDependentRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowName());
				validateNumber(ameExcelVO.getIndividualRate(), ameExcelVO, errorMessages, AMERatesExcelRowEnum.INDIVIDUAL.getRowName());
			}
			else {
				LOGGER.info("Validations for AME Rates For Age Group");
				
				if (!CollectionUtils.isEmpty(ameExcelVO.getAmeAgeGroupExcelVO())) {
					
					for (AMEAgeGroupExcelVO ameAgeGroupVO : ameExcelVO.getAmeAgeGroupExcelVO()) {
						validateBlankData(ameAgeGroupVO.getMinAge(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.MIN_AGE.getColumnName());
						validateBlankData(ameAgeGroupVO.getMaxAge(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.MAX_AGE.getColumnName());
						
						validateInteger(false, ameAgeGroupVO.getMinAge(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.MIN_AGE.getColumnName());
						validateInteger(false, ameAgeGroupVO.getMaxAge(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.MAX_AGE.getColumnName());

						if(SerffConstants.PLAN_TYPE.AME_GENDER.name().equalsIgnoreCase(planType)) {
							validateBlankData(ameAgeGroupVO.getApplicant(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.APPLICANT_MALE.getColumnName());
							validateBlankData(ameAgeGroupVO.getApplicantFemale(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.APPLICANT_FEMALE.getColumnName());
							validateBlankData(ameAgeGroupVO.getSpouse(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.SPOUSE_MALE.getColumnName());
							validateBlankData(ameAgeGroupVO.getSpouseFemale(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.SPOUSE_FEMALE.getColumnName());
							validateNumber(ameAgeGroupVO.getApplicant(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.APPLICANT_MALE.getColumnName());
							validateNumber(ameAgeGroupVO.getApplicantFemale(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.APPLICANT_FEMALE.getColumnName());
							validateNumber(ameAgeGroupVO.getSpouse(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.SPOUSE_MALE.getColumnName());
							validateNumber(ameAgeGroupVO.getSpouseFemale(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.SPOUSE_FEMALE.getColumnName());
							validateNumber(ameAgeGroupVO.getChild(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.CHILD_GENDERBASED.getColumnName());
						} else {
							validateBlankData(ameAgeGroupVO.getApplicant(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.APPLICANT.getColumnName());
							validateBlankData(ameAgeGroupVO.getSpouse(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.SPOUSE.getColumnName());
							validateNumber(ameAgeGroupVO.getApplicant(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.APPLICANT.getColumnName());
							validateNumber(ameAgeGroupVO.getSpouse(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.SPOUSE.getColumnName());
							validateNumber(ameAgeGroupVO.getChild(), ameExcelVO, errorMessages, AMEAgeRatesColumnEnum.CHILD.getColumnName());
						}
					}
				}
				else {
					errorMessages.append("AME age group plan rates must not empty.");

					if (!ameExcelVO.isNotValid()) {
						ameExcelVO.setNotValid(Boolean.TRUE);
					}
				}
			}
			
			if (ameExcelVO.isNotValid()) {
				isValid = Boolean.FALSE;
				
				if (StringUtils.isNotBlank(ameExcelVO.getCarrierHIOS()) &&
						StringUtils.isNotBlank(ameExcelVO.getState()) &&
						StringUtils.isNotBlank(ameExcelVO.getPlanID())) {
					ameExcelVO.setErrorMessages(errorMessages.toString() + "Skipping to persist AME Plans ["
							+ hiosPlanNumber + "] due to validations error.");
				}
				else {
					ameExcelVO.setErrorMessages(errorMessages.toString() + "Skipping to persist AME plan due to validations error.");
				}
				ameExcelVO.setErrorMessages(ameExcelVO.getErrorMessages() + "\n=========================================================");
			}
		}
		finally {
			LOGGER.debug("validateAMEExcelVO() End");
		}
		return isValid;
	}

	private void validateDate(AMEExcelVO ameExcelVO, StringBuilder errorMessages, String cellHeader) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		try {
			if (!StringUtils.isEmpty(ameExcelVO.getStartDate())){
				formatter.parse(ameExcelVO.getStartDate());
			}
		} catch (ParseException e) {
			errorMessages.append(cellHeader + "[" + ameExcelVO.getStartDate() + "]" + SerffConstants.COMMA + EMSG_INVALID_DATE);
			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
			LOGGER.error("Exception occured while parsing date", e);
		}
	}
	

	/**
	 * Method is used to validate require data from AMEExcelVO object before persistence.
	 */
	public boolean validateAMEPremiumFactorExcelVO(AMEPremiumFactorExcelVO amePremiumFactorVO) {
		
		LOGGER.debug("validateAMEPremiumFactorExcelVO() Start");
		boolean isValid = Boolean.TRUE;
		
		try {
			
			if (null == amePremiumFactorVO) {
				LOGGER.error("AMEPremiumFactorExcelVO is empty !!");
				return isValid;
			}
			
			StringBuilder errorMessages = new StringBuilder();
			LOGGER.info("amePremiumFactorVO.getCarrierHIOS(): " + amePremiumFactorVO.getCarrierHIOS());
			LOGGER.info("amePremiumFactorVO.getState(): " + amePremiumFactorVO.getState());
			
			validateBlankData(amePremiumFactorVO.getCarrierHIOS(), amePremiumFactorVO, errorMessages, AMEAgePremiumFactorColumnEnum.CARRIER_HIOS_ID.getColumnName());
			validateBlankData(amePremiumFactorVO.getState(), amePremiumFactorVO, errorMessages, AMEAgePremiumFactorColumnEnum.STATE.getColumnName());
			validateBlankData(amePremiumFactorVO.getAnnual(), amePremiumFactorVO, errorMessages, AMEAgePremiumFactorColumnEnum.ANNUAL.getColumnName());
			validateBlankData(amePremiumFactorVO.getSemiAnnual(), amePremiumFactorVO, errorMessages, AMEAgePremiumFactorColumnEnum.SEMI_ANNUAL.getColumnName());
			validateBlankData(amePremiumFactorVO.getQuarterly(), amePremiumFactorVO, errorMessages, AMEAgePremiumFactorColumnEnum.QUARTERLY.getColumnName());
			validateBlankData(amePremiumFactorVO.getMonthly(), amePremiumFactorVO, errorMessages, AMEAgePremiumFactorColumnEnum.MONTHLY.getColumnName());
			
			validateState(amePremiumFactorVO, errorMessages, AMEPlansExcelRowEnum.STATE.getRowName());
			
			validateNumber(amePremiumFactorVO.getAnnual(), amePremiumFactorVO, errorMessages, AMEAgePremiumFactorColumnEnum.ANNUAL.getColumnName());
			validateNumber(amePremiumFactorVO.getSemiAnnual(), amePremiumFactorVO, errorMessages, AMEAgePremiumFactorColumnEnum.SEMI_ANNUAL.getColumnName());
			validateNumber(amePremiumFactorVO.getQuarterly(), amePremiumFactorVO, errorMessages, AMEAgePremiumFactorColumnEnum.QUARTERLY.getColumnName());
			validateNumber(amePremiumFactorVO.getMonthly(), amePremiumFactorVO, errorMessages, AMEAgePremiumFactorColumnEnum.MONTHLY.getColumnName());
			
			if (amePremiumFactorVO.isNotValid()) {
				isValid = Boolean.FALSE;
				
				if (StringUtils.isNotBlank(amePremiumFactorVO.getCarrierHIOS()) &&
						StringUtils.isNotBlank(amePremiumFactorVO.getState())) {
					amePremiumFactorVO.setErrorMessages(errorMessages.toString() + "Skipping to persist AME Premium Factor ["
							+ amePremiumFactorVO.getCarrierHIOS() + amePremiumFactorVO.getState() + "] due to validations error.");
				}
				else {
					amePremiumFactorVO.setErrorMessages(errorMessages.toString() + "Skipping to persist AME Premium Factor due to validations error.");
				}
				amePremiumFactorVO.setErrorMessages(amePremiumFactorVO.getErrorMessages() + "\n=========================================================");
			}
		}
		finally {
			LOGGER.debug("validateAMEPremiumFactorExcelVO() End");
		}
		return isValid;
	}

	/**
	 * Method is used to validate URL and ECM Id.
	 */
	private void validateURL(String cellValue, AMEExcelVO ameExcelVO,
			StringBuilder errorMessages, String cellHeader) {
		String url = StringUtils.trim(cellValue);
		if (StringUtils.isNotBlank(cellValue) &&
				!(isValidPattern(PATTERN_URL, url) || isValidPattern(PATTERN_ECM, url))) {
			
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_INVALID_URL);
			
			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Blank Data from Cell Value.
	 */
	private void validateBlankData(String cellValue, AMEExcelVO ameExcelVO, StringBuilder errorMessages, String cellHeader) {
		
		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Blank Data from Cell Value.
	 */
	private void validateBlankData(String cellValue, AMEPremiumFactorExcelVO amePremiumExcelVO, StringBuilder errorMessages, String cellHeader) {
		
		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!amePremiumExcelVO.isNotValid()) {
				amePremiumExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Number value from Cell Value.
	 */
	private void validateNumber(String cellValue, AMEExcelVO ameExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue) && !validateNumeric(cellValue)) {
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_NUMERIC);
			
			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Number value from Cell Value.
	 */
	private void validateNumber(String cellValue, AMEPremiumFactorExcelVO amePremiumExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (!validateNumeric(cellValue)) {
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_NUMERIC);
			
			if (!amePremiumExcelVO.isNotValid()) {
				amePremiumExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Integer value from Cell Value.
	 */
	private void validateInteger(boolean isNotAvailable, String cellValue, AMEExcelVO ameExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (!StringUtils.isNumeric(cellValue)) {
			
			if (isNotAvailable && StringUtils.isNotBlank(cellValue)
					&& SerffConstants.NOT_APPLICABLE.equalsIgnoreCase(cellValue)) {
				return;
			}
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_NUMERIC);
			
			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	public void validateState(AMEExcelVO ameExcelVO, StringBuilder errorMessages, String cellHeader) {
		
		if (!validateState(ameExcelVO.getState())) {

			errorMessages.append(cellHeader + "[" + ameExcelVO.getState() + "]" + SerffConstants.COMMA + EMSG_INVALID_STATE);
			
			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	public void validateState(AMEPremiumFactorExcelVO amePremiumExcelVO, StringBuilder errorMessages, String cellHeader) {
		
		if (!validateState(amePremiumExcelVO.getState())) {

			errorMessages.append(cellHeader + "[" + amePremiumExcelVO.getState() + "]" + SerffConstants.COMMA + EMSG_INVALID_STATE);
			
			if (!amePremiumExcelVO.isNotValid()) {
				amePremiumExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	public void validateCarrierHIOS(AMEExcelVO ameExcelVO) {
		
		if (StringUtils.isBlank(ameExcelVO.getCarrierHIOS())) {
			
			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
			ameExcelVO.setErrorMessages(ameExcelVO.getErrorMessages() + AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowName()
					+ SerffConstants.COMMA + EMSG_EMPTY);
		}
		else if (LEN_HIOS_ISSUER_ID != StringUtils.length(ameExcelVO.getCarrierHIOS())) {
			
			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
			ameExcelVO.setErrorMessages(AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowName() + "[" + ameExcelVO.getCarrierHIOS() + "]"
					+ SerffConstants.COMMA + EMSG_LENGTH + LEN_HIOS_ISSUER_ID + NEW_LINE);
		}
	}

	public void validatePlanId(AMEExcelVO ameExcelVO) {
		
		if (StringUtils.isBlank(ameExcelVO.getPlanID())) {
			
			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
			ameExcelVO.setErrorMessages(AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowName() + SerffConstants.COMMA + EMSG_EMPTY);
		}
		else if (LEN_AME_PLAN_ID != StringUtils.length(ameExcelVO.getPlanID())) {
			
			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
			ameExcelVO.setErrorMessages(AMEPlansExcelRowEnum.PLAN_ID.getRowName() + "[" + ameExcelVO.getPlanID() + "]"
					+ SerffConstants.COMMA + EMSG_LENGTH + LEN_AME_PLAN_ID + NEW_LINE);
		}
	}
}
