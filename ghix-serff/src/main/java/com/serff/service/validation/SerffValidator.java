package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_FORMAT_MISSMATCHED;
import static com.serff.service.validation.IFailedValidation.EMSG_INVALID_STANDARD_STATE_CODE;
import static com.serff.service.validation.IFailedValidation.EMSG_LENGTH;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC;

import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.serff.template.admin.extension.IssuerType;
import com.serff.template.admin.niem.usps.states.USStateCodeSimpleType;
import com.serff.util.SerffConstants;

/**
 * This class is going to have all generic validations required for SERFF.
 *
 * @author polimetla / Geetha / Sunil
 * @since 3/20/2013
 */
public class SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(SerffValidator.class);
	
	//HIOS issued Id for an issuer
	protected static final int LEN_HIOS_ISSUER_ID = 5;
	protected static final int LEN_ISSUER_STATE = 2;
	protected static final int LEN_PLAN_YEAR = 4;
	protected static final int LEN_MEDICAID_PLAN_ID = 16;
	protected static final int LEN_STM_PLAN_ID = 7;
	protected static final int LEN_AME_PLAN_ID = 7;
	protected static final int LEN_LIFE_PLAN_ID = 7;
	protected static final int LEN_FORMULARY_ID = 6;
	protected static final int LEN_ZIP_CODE_1 = 5;
	protected static final int LEN_ZIP_CODE_2 = 9;
	//Health Plan Id
	protected static final int LEN_HP_ID = 10;
	//A 6-digit number that is the issuer's company identifier provided by the National Association of Insurance Commissioners (NAIC)
	protected static final int LEN_NAIC_CODE = 6;
	protected static final int LEN_PHONE_1 = 10;
	protected static final int LEN_PHONE_2 = 11;
	protected static final int LEN_CHILD_ONLY_PLAN_ID = 14;
	protected static final int LEN_HIOS_PLAN_ID = 14;
	protected static final int LEN_PLAN_VARIANCE = 2;
	protected static final int LEN_HIOS_PRODUCT_ID = 10;
	protected static final int LET_STANDARD_COMPONENT_ID = 14;
	protected static final int LET_STANDARD_COMPONENT_ID_WITH_VARIANT = 17;
	protected static final int LET_NETWORK_ID = 6;
	protected static final int LET_SERVICE_AREA_ID = 6;
	protected static final int LET_APPLICATION_NUMBER = 9;
	protected static final int LET_TIN = 9;
	protected static final int LEN_HPID = 10;

	protected static final String ISSUER_ID = "Issuer ID";
	protected static final String COUNTY_NAME = "County Name";
	protected static final String EMPTY_VALUE = "Empty";
	protected static final String LOCATION_POSTAL_CODE = "Location Postal Code";

	//protected static final String PATTERN_URL = "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
	//protected static final String PATTERN_URL = "^(https?://)?[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9 _/\\&\\?\\=\\(\\)\\;\\-\\.\\~\\%\\!]*";
	//protected static final String PATTERN_URL = "^(https?://)?([a-zA-Z0-9_\\-])+([\\.][a-zA-Z0-9_/\\-]+)*\\.([A-Za-z0-9/]{1,5})([:][0-9]+[/]?)?([a-zA-Z0-9 _/\\(\\)\\-\\.\\~\\%\\!]+)*([a-zA-Z0-9 _\\&\\?\\=\\(\\)\\;\\-\\.\\~\\%\\!#]+)?";
	protected static final String PATTERN_URL = "^(https?://)?([a-zA-Z0-9_\\-])+([\\.][a-zA-Z0-9_/\\-]+)*\\.([A-Za-z0-9/]{1,5})([:][0-9]+[/]?)?([\\S ]+)?";
	protected static final String PATTERN_ECM = "^(?i:workspace)(:)(\\/\\/)(?i:SpacesStore)(\\/)([A-Za-z0-9\\-])+(;)([0-9\\.]{1,5})$";
	protected static final String PATTERN_EMAIL = "^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})";
	protected static final String PATTERN_TIN = "(^[0-9]{2})([\\-]{1})([0-9]{7})$";
	protected static final String PATTERN_PLAN_ID = "(^[0-9]{5})([A-Za-z]{2})([0-9]{7})$";
	protected static final String PATTERN_HIOS_PLAN_NUM = "(^[A-Za-z0-9]{5})([A-Za-z]{2})([0-9]{7})$";
	protected static final String PATTERN_HIOS_PLAN_NUM_WITH_VARIANT = "(^[A-Za-z0-9]{5})([A-Za-z]{2})([0-9]{7})([\\\\-]{1})([0]{1}[0-6]{1})+$";
	protected static final String PATTERN_STANDARD_COMPONENT_ID = "(^[0-9]{5})([A-Za-z]{2})([0-9]{7})$";
	protected static final String PATTERN_STANDARD_COMPONENT_ID_WITH_VARIANT = "(^[0-9]{5})([A-Za-z]{2})([0-9]{7})([\\-]{1})([0]{1}[0-6]{1})+$";
	protected static final String PATTERN_VARIANT_COMPONENT_ID = "(^[0]{1}[0-6]{1}$)";
	protected static final String PATTERN_FORMULARY_ID = "(^[A-Za-z]{2}[Ff])([0-9]{3})$";
	protected static final String PATTERN_NETWORK_ID = "(^[A-Za-z]{2}[Nn])([0-9]{3})$";
	protected static final String PATTERN_SERVICE_AREA_ID = "(^[A-Za-z]{2}[Ss])([0-9]{3})$";

	protected static final String MARKET_COVERAGE_TYPE_SHOP = "SHOP";
	protected static final String MARKET_COVERAGE_TYPE_INDIVIDUAL = "INDIVIDUAL";

	protected static final String MARKET_TYPE_COMMERCIAL = "commercial";
	protected static final String MARKET_TYPE_EXCHANGE = "exchange";
	protected static final String MARKET_TYPE_MEDICAID = "medicaid";

	protected static final String YES = "yes";
	protected static final String NO = "no";
	protected static final int DATE_START_AND_END_INVALID = 99;
	protected static final int DATE_START_AND_END_EQUAL = 0;
	protected static final int DATE_START_GREATER_THAN_END = 1;
	protected static final int DATE_START_LESS_THAN_END = -1;

	protected static final String BENEFIT_COVERAGE_COVERED = "covered";

	protected static final String CHILD_ONLY_OFFERING_ALLOWS_ADULT_CHILD = "Allows Adult and Child-Only";
	protected static final String CHILD_ONLY_OFFERING_ALLOWS_ADULT_ONLY = "Allows Adult-Only";
	protected static final String CHILD_ONLY_OFFERING_ALLOWS_CHILD_ONLY = "Allows Child-Only";
	
	protected static final DateTimeFormatter PLAN_DATE_FORMATTER = DateTimeFormat.forPattern("MM/dd/yyyy").withLocale(Locale.US);
	protected static final DateTimeFormatter RATE_DATE_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd").withLocale(Locale.US);

	private static final String TEMPLATE_NAME = "ISSUER_DATA_TEMPLATE. ";

	// @Autowired private SerffUtils serffUtils;

	/**
	 * Method is used to validate given value with passed argument pattern.
	 * @param pattern
	 * @return True for Valid otherwise False.
	 */
	protected boolean isValidPattern(final String pattern, final String givenValue) {

		boolean isValid = false;

		if (StringUtils.isNotEmpty(pattern)
				&& StringUtils.isNotEmpty(givenValue)) {

			// Assigning the regular expression
			return givenValue.matches(pattern);
		}
		return isValid;
	}

	/**
	 * Method is used to validate length of field.
	 * @param length
	 * @param value
	 * @return True for Valid URL otherwise False.
	 */
	protected boolean isValidLength(final int length, final String value) {
		return length == StringUtils.length(value);
	}
	/**
	 * Method is used to validate TIN format
	 * @param sTin
	 * @return boolean
	 */
	/*protected boolean isValidTin(String sTin){
		if (StringUtils.isNotEmpty(sTin)) {

			return sTin.matches(PATTERN_TIN);
		}

		return false;
	}*/

	/**
	 * Method is used to return FailedValidation with passed by arguments value.
	 * @param fieldName, Field Name
	 * @param fieldValue, Field Value
	 * @param errorMsg, Error Message
	 * @param errorCode, Error Code
	 * @param xPath, XML Path
	 * @return Object of FailedValidation
	 */
	protected FailedValidation getFailedValidation(String fieldName, String fieldValue, String errorMsg, String errorCode, String templateName) {

		FailedValidation failedValidation = new FailedValidation();
		failedValidation.setFieldName(fieldName);
		failedValidation.setFieldValue(fieldValue);
		failedValidation.setErrorMsg(errorMsg);
		failedValidation.setErrorCode(errorCode);
		failedValidation.setTemplateName(templateName);
		return failedValidation;
	}

	protected String validateIssuerId(IssuerType issuerType,
			List<FailedValidation> validationErrors) {

		String value = null;

		if (null != issuerType.getIssuerIdentification()
				&& null != issuerType.getIssuerIdentification().getIdentificationID()) {

			value = issuerType.getIssuerIdentification().getIdentificationID().getValue();

			if (StringUtils.isNotEmpty(value)) {

				if (!StringUtils.isNumeric(value)) {
					validationErrors.add(getFailedValidation(ISSUER_ID, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
				else if (!isValidLength(LEN_HIOS_ISSUER_ID, value)) {
					validationErrors.add(getFailedValidation(ISSUER_ID, value, EMSG_LENGTH + LEN_HIOS_ISSUER_ID, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
			}
			else{
				validationErrors.add(getFailedValidation(ISSUER_ID, EMPTY_VALUE, EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
		}
		else{
			validationErrors.add(getFailedValidation(ISSUER_ID, EMPTY_VALUE, EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
			value = null;
		}
		return value;
	}

	/**
	 * Validate the date
	 * @param day
	 * @param month
	 * @param year
	 * @return
	 */
	public boolean isValidDate(int day, int month, int year) {

		GregorianCalendar gc = new GregorianCalendar();
		gc.setLenient(false);
		gc.set(GregorianCalendar.YEAR, year);
		gc.set(GregorianCalendar.MONTH, month);
		gc.set(GregorianCalendar.DATE, day);

		try {
			gc.getTime();
		} catch (Exception e) {
			LOGGER.debug("Invalid Date: " + e.getMessage(), e);
			return false;
		}
		return true;
	}

	/**
	 * This method validates the plan Id for a plan
	 */
	public void validateStandardComponentId(String standardComponentId,
			String globalStateCode, List<String> errorCodes,
			List<String> errorMessages) {

		final String fieldName = "Standard Component ID";
		boolean flagError = true;

		if(StringUtils.isEmpty(standardComponentId)){
			errorCodes.add(SerffConstants.REQUIRED_FIELD_MISSING_CODE);
			errorMessages.add(SerffConstants.REQUIRED_FIELD_MISSING_MESSAGE);
		}
		else {

			if (!isValidLength(LEN_HIOS_PLAN_ID, standardComponentId)) {
				errorCodes.add(SerffConstants.INVALID_PARAMETER_CODE);
				errorMessages.add(EMSG_LENGTH + LEN_HIOS_PLAN_ID);
			}
			else if (!isValidPattern(PATTERN_PLAN_ID, standardComponentId)) {
				errorCodes.add(SerffConstants.INVALID_PARAMETER_CODE);
				errorMessages.add(EMSG_FORMAT_MISSMATCHED);
			}
			else if (!SerffConstants.DEFAULT_PHIX_STATE_CODE.equalsIgnoreCase(globalStateCode)
					&& !standardComponentId.substring(LEN_HIOS_ISSUER_ID, LEN_HIOS_ISSUER_ID + LEN_ISSUER_STATE).equals(globalStateCode)) {
				errorCodes.add(SerffConstants.INVALID_PARAMETER_CODE);
				errorMessages.add(EMSG_INVALID_STANDARD_STATE_CODE + globalStateCode);
			}
			else {
				flagError = false;
			}
		}

		if(flagError) {
			errorCodes.add(SerffConstants.INVALID_PARAMETER_CODE);
			errorMessages.add("Field name:" + fieldName + ", Field value:" + standardComponentId);
		}
	}

	/**
	  * This method validates the Standard Component Id.
	  * @param issuerPlanNumber String Format.
	  */
	public boolean isValidateStandardComponentId(String issuerPlanNumber) {

		boolean flag = false;

		if (StringUtils.isNotBlank(issuerPlanNumber) && isValidLength(LEN_HIOS_PLAN_ID, issuerPlanNumber)
				&& isValidPattern(PATTERN_PLAN_ID, issuerPlanNumber)) {
			flag = true;
		}
		return flag;
	}

	/**
	  * This method validates the Issuer Plan Number of HEALTH/DENTAL/AME/STM.
	  * @param issuerPlanNumber String Format.
	  */
	public boolean isValidHIOSPlanNumber(String issuerPlanNumber) {

		boolean flag = false;

		if (StringUtils.isNotBlank(issuerPlanNumber) && isValidLength(LEN_HIOS_PLAN_ID, issuerPlanNumber)
				&& isValidPattern(PATTERN_HIOS_PLAN_NUM, issuerPlanNumber)) {
			flag = true;
		}
		return flag;
	}

	/**
	  * This method validates the Issuer Plan Number of HEALTH/DENTAL/AME/STM.
	  * @param issuerPlanNumber String Format.
	  */
	public boolean isValidHIOSPlanNumberWithVariant(String issuerPlanNumber) {

		boolean flag = false;

		if (StringUtils.isNotBlank(issuerPlanNumber) && isValidLength(LET_STANDARD_COMPONENT_ID_WITH_VARIANT, issuerPlanNumber)
				&& isValidPattern(PATTERN_HIOS_PLAN_NUM_WITH_VARIANT, issuerPlanNumber)) {
			flag = true;
		}
		return flag;
	}

	public boolean validateState(String stateCode) {
		
		boolean flag = false;
		
		try {
			USStateCodeSimpleType.valueOf(stateCode);
			flag = true;
		}
		catch (Exception ex) {
			LOGGER.debug("Invalid State: " + ex.getMessage(), ex);
		}
		return flag;
	}

	public boolean validateCarrierHIOS(String carrierHIOS) {
		
		boolean flag = false;
		
		if (StringUtils.isNotBlank(carrierHIOS) && isValidLength(LEN_HIOS_ISSUER_ID, carrierHIOS)) {
			flag = true;
		}
		return flag;
	}

	public boolean validatePlanId(String planId) {
		
		boolean flag = false;
		
		if (StringUtils.isNotBlank(planId) && isValidLength(LEN_AME_PLAN_ID, planId)) {
			flag = true;
		}
		return flag;
	}

	/**
	 * Method is used to get Integer value from String.
	 */
	public Integer getIntegerValue(String value) {
		
		Integer intValue = null;
		
		if (validateNumeric(value.trim())) {
			intValue = Double.valueOf(value.trim()).intValue();
		}
		return intValue;
	}

	/**
	 * Method is used to get Integer Percentage value from String.
	 */
	public Integer getPercentageValue(String value) {
		
		Integer intValue = null;
		
		if (StringUtils.isNotBlank(value) && validateNumeric(value.trim().replace(SerffConstants.PERCENTAGE, StringUtils.EMPTY))) {
			intValue = Double.valueOf(value.trim().replace(SerffConstants.PERCENTAGE, StringUtils.EMPTY)).intValue();
		}
		return intValue;
	}

	/**
	 * Method is used to get Integer Percentage value from String.
	 */
	public Double getPercentageValueInDouble(String value) {
		
		Double doubleValue = null;
		
		if (StringUtils.isNotBlank(value) && validateNumeric(value.trim().replace(SerffConstants.PERCENTAGE, StringUtils.EMPTY))) {
			doubleValue = Double.valueOf(value.trim().replace(SerffConstants.PERCENTAGE, StringUtils.EMPTY));
		}
		return doubleValue;
	}

	/**
	 * Method is used to get Integer Dollar value from String.
	 */
	public Integer getDollarValue(String value) {
		
		Integer intValue = null;
		
		if (StringUtils.isNotBlank(value) && validateNumeric(value.trim().replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY))) {
			intValue = Double.valueOf(value.trim().replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY)).intValue();
		}
		return intValue;
	}

	/**
	 * Method is used to get Integer Dollar value from String.
	 */
	public Double getDollarValueInDouble(String value) {
		
		Double doubleValue = null;
		
		if (StringUtils.isNotBlank(value) && validateNumeric(value.trim().replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY))) {
			doubleValue = Double.valueOf(value.trim().replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY));
		}
		return doubleValue;
	}

	/**
	 * Method is used to get Float value from String.
	 */
	public Float getFloatValue(String value) {
		
		Float fltValue = null;
		
		if (StringUtils.isNotBlank(value) && validateNumeric(value.trim().replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY))) {
			fltValue = Float.valueOf(value.trim().replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY));
		}
		return fltValue;
	}

	/**
	 * Method is used to verified that Value is numeric or not.
	 */
	public boolean validateNumeric(String value) {
		
		try {
			LOGGER.debug("Numeric is " + Double.valueOf(value));
			return true;
		}
		catch (Exception ex) {
			LOGGER.debug("Invalid Decimal value: " + ex.getMessage(), ex);
		}
		return false;
	}

	/**
	 * Method is used to verified that Amount is valid or not.
	 */
	public boolean validateAmount(String amount) {

		boolean status = true;
		
		try {

			BigDecimal value = new BigDecimal(amount.replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));

			if (-1 == value.signum()) {
				status = false;
			}
		}
		catch (Exception ex) {
			status = false;
			LOGGER.debug("Invalid Amount value: " + ex.getMessage(), ex);
		}
		return status;
	}

	/**
	 * Method is used to verified that Whole Amount is valid or not.
	 */
	public boolean validateWholeAmount(String amount) {

		boolean status = true;

		try {

			BigDecimal value = new BigDecimal(amount.replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));

			if (-1 == value.signum() || !(0 == value.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO))) {
				status = false;
			}
		}
		catch (Exception ex) {
			status = false;
			LOGGER.debug("Invalid Amount value: " + ex.getMessage(), ex);
		}
		return status;
	}

	/**
	 * Method is used to compare Effective Date and Expiration Date.
	 * @param date, Date
	 * @return boolean value
	 */
	public boolean validateDate(String date, DateTimeFormatter dtFormatter) {

		try {
			dtFormatter.parseLocalDate(date);
			return true;
		}
		catch (Exception e) {
			LOGGER.debug("Validating Date [" + date + "] Error: " + e.getMessage(), e);
		}
		return false;
	}

	/**
	 * Method is used to compare Start Date and End Date.
	 */
	public int compareDates(String startDateText, String endDateText, DateTimeFormatter dateFormat) {

		LOGGER.debug("Begin compareDates with Start Date: " + startDateText + " & End Date: " + endDateText);
		int compareValue = DATE_START_AND_END_INVALID;

		try {

			if (StringUtils.isBlank(startDateText)
				|| StringUtils.isBlank(endDateText)) {
				return compareValue;
			}
			LocalDate startDate = dateFormat.parseDateTime(startDateText).toLocalDate();
			LocalDate endDate = dateFormat.parseDateTime(endDateText).toLocalDate();
			compareValue = startDate.compareTo(endDate);
		}
		finally {
			LOGGER.debug("End compareDates() with compareValue: " + compareValue);
		}
		return compareValue;
	}
}
