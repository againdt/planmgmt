package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_FORMAT;
import static com.serff.service.validation.IFailedValidation.EMSG_LENGTH;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC;
import static com.serff.service.validation.IFailedValidation.EMSG_SHOULD_NOT_EMPTY;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.getinsured.hix.serff.config.SerffConfiguration;
import com.getinsured.hix.serff.config.SerffConfiguration.SerffConfigurationEnum;
import com.serff.planmanagementexchangeapi.exchange.model.pm.NcqaInfo;

/**
 * Class is used to provide validation service of Issuer NCQA.
 * @author Bhavin Parmar
 */
@Component
public class IssuerNCQAValidator extends SerffValidator {
	
	private static final Logger LOGGER = Logger.getLogger(IssuerNCQAValidator.class);
	private static final String TEMPLATE_NAME ="ISSUER_NCQA_TEMPLATE. ";
	private static final int FIVE = 5;
	
	/**
	 * Method is used to validate each field of NcqaInfo.
	 * 
	 * @param ncqaInfo, Object of NcqaInfo
	 * @param errors, List<FailedValidation>
	 * @return FailedValidation objects in List.
	 */
	public void validateIssuerNCQA(final NcqaInfo ncqaInfo, List<FailedValidation> errors) {
		
		LOGGER.info("validateIssuerNCQA() Start");
		
		try {
			
			if (null == ncqaInfo) {
				LOGGER.warn("NcqaInfo complex type has been null.");
				return;
			}
			
			validateHIOSIssuerID(ncqaInfo.getHiosIssuerId(), errors);
			
			validateOrganizationID(ncqaInfo.getOrgId(), errors);
			
			validateMarketType(ncqaInfo.getMarketType(), ncqaInfo.getSubId(), errors);
			
			validateAccreditationStatus(ncqaInfo.getAccreditationStatus(), errors);
			
			validateExpirationDate(ncqaInfo.getExpirationDate(), errors);
		}
		finally {
			LOGGER.info("validateIssuerNCQA() End");
		}
	}
	
	/**
	 * Method is used validate HIOS Issuer ID.
	 * 
	 * @param hiosIssuerId, Integer value of HIOS Issuer ID
	 * @param errors, List Object of FailedValidation
	 */
	private void validateHIOSIssuerID(Integer hiosIssuerId,
			List<FailedValidation> errors) {
		
		String fieldName = "HIOS Issuer ID";
		String xPath = "transferPlan/plan/insurer/accreditationInfo/ncqaInfo/hiosIssuerId";
		String value = String.valueOf(hiosIssuerId);
		
		if (null != hiosIssuerId) {
			LOGGER.debug("Validation Header: HIOS Issuer ID : "+ value);
			if (!isValidLength(LEN_HIOS_ISSUER_ID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_HIOS_ISSUER_ID, ECODE_DEFAULT, xPath));
			}
		}
		else {
			LOGGER.debug("Validation Header: HIOS Issuer ID : "+ value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, xPath));
		}
	}
	
	/**
	 * Method is used validate NCQA Organization ID.
	 * 
	 * @param orgId, String value of NCQA Organization ID
	 * @param errors, List Object of FailedValidation
	 */
	private void validateOrganizationID(String organizationId,
			List<FailedValidation> errors) {
		
		String fieldName = "NCQA Org ID";
		String xPath = "transferPlan/plan/insurer/accreditationInfo/ncqaInfo/orgId";
		
		if (null != organizationId
				&& StringUtils.isNotEmpty(organizationId)) {
			LOGGER.debug("Validation Field: NCQA Organization ID : "+organizationId);
			if (!StringUtils.isNumeric(organizationId)) {
				errors.add(getFailedValidation(fieldName, organizationId, EMSG_NUMERIC, ECODE_DEFAULT, xPath));
			}
			else if (2 > StringUtils.length(organizationId)
					|| FIVE < StringUtils.length(organizationId)) {
				errors.add(getFailedValidation(fieldName, organizationId, EMSG_LENGTH + 2 + "-" + FIVE, ECODE_DEFAULT, xPath));
			}
		}
		else {
			LOGGER.debug("Validation Field: NCQA Organization ID : "+organizationId);
			errors.add(getFailedValidation(fieldName, organizationId, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, xPath));
		}
	}
	
	/**
	 * Method is used validate Market Type.
	 * 
	 * @param marketType, String value of Market Type
	 * @param subId, Integer value of NCQA Sub ID
	 * @param errors, List Object of FailedValidation
	 */
	private void validateMarketType(String marketType, String subId,
			List<FailedValidation> errors) {
		
		String fieldName = "Market Type";
		LOGGER.debug("Validation Field: Market Type : "+marketType);
		if (StringUtils.isNotBlank(marketType)) {
			
			if (!SerffConfiguration.hasPropertyInList(marketType, SerffConfigurationEnum.INV_MARKET_TYPE)) {
				errors.add(getFailedValidation(fieldName, marketType, EMSG_FORMAT, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else {
				
				if (MARKET_TYPE_COMMERCIAL.equalsIgnoreCase(marketType)
						|| MARKET_TYPE_MEDICAID.equalsIgnoreCase(marketType)) {
					LOGGER.info("Validation Field: NCQA Sub ID");
					validateNCQASubID(subId, errors);
				}
			}
		}
	}
	
	/**
	 * Method is used validate NCQA Sub ID.
	 * 
	 * @param subId, Integer value of NCQA Sub ID
	 * @param errors, List Object of FailedValidation
	 */
	private void validateNCQASubID(String subId, List<FailedValidation> errors) {
		
		String fieldName = "NCQA Sub ID";
		String xPath = "transferPlan/plan/insurer/accreditationInfo/ncqaInfo/subId";
		
		if (null != subId) {
			
			if (!StringUtils.isNumeric(subId)) {
				errors.add(getFailedValidation(fieldName, subId, EMSG_NUMERIC, ECODE_DEFAULT, xPath));
			}
			else if (2 > StringUtils.length(subId)
					|| FIVE < StringUtils.length(subId)) {
				errors.add(getFailedValidation(fieldName, subId, EMSG_LENGTH + 2 + "-" + FIVE, ECODE_DEFAULT, xPath));
			}
		}
		else {
			errors.add(getFailedValidation(fieldName, null, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, xPath));
		}
	}
	
	/**
	 * Method is used validate NCQA Accreditation Status.
	 * 
	 * @param accreditationStatus, String value of Accreditation Status
	 * @param errors, List Object of FailedValidation
	 */
	private void validateAccreditationStatus(String accreditationStatus,
			List<FailedValidation> errors) {
		
		String fieldName = "Accreditation Status";
		LOGGER.info("Validation Field: NCQA Accreditation Status : "+accreditationStatus);
		if (null != accreditationStatus
				&& !SerffConfiguration.hasPropertyInList(accreditationStatus, SerffConfigurationEnum.INV_ACCREDITATION_STATUS)) {
			errors.add(getFailedValidation(fieldName, accreditationStatus, EMSG_FORMAT, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		else {
			errors.add(getFailedValidation(fieldName, accreditationStatus, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}
	
	/**
	 * Method is used validate NCQA Accreditation Status.
	 * 
	 * @param accreditationStatus, String value of Accreditation Status
	 * @param errors, List Object of FailedValidation
	 */
	private void validateExpirationDate(XMLGregorianCalendar expirationDate,
			List<FailedValidation> errors) {
		
		String fieldName = "Expiration date";
		String xPath = "transferPlan/plan/insurer/accreditationInfo/ncqaInfo/expirationDate";
		LOGGER.info("Validation Field: NCQA Expiration date : "+expirationDate);
		if (null == expirationDate) {
			errors.add(getFailedValidation(fieldName, StringUtils.EMPTY, EMSG_FORMAT, ECODE_DEFAULT, xPath));
		}
	}
}
