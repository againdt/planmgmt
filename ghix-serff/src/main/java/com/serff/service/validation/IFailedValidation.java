package com.serff.service.validation;

/**
 * Interface is used to declare error codes and messages for FailedValidation.
 * @author Bhavin Parmar
 */
public interface IFailedValidation {

	String ECODE_DEFAULT = "1001";
	String EMSG_DEFAULT = "The field contains no data";

	String EMSG_NUMERIC = "Value must be numeric";
	String EMSG_NUMERIC_ZERO = "Value must be numeric and greater than zero";
	String EMSG_LENGTH = "Value must have length ";
	String EMSG_URL = "URL is not valid";
	String EMSG_FORMAT = "Value is not in valid format";
	String EMSG_ALPHA = "Value must be alphabetic";
	String EMSG_AMOUNT_VALUE = "Dollar($) amount must not have greater than ";
	String EMSG_FORMULARYID_PREFIX_VALUE = "Value is not matched with Issuer state code ";
	String EMSG_MEASURE_POINT_VALUE = "Measure Point must have 1 or 3 value";
	String EMSG_TIME_UNIT_MON = "Time Unit Code must have value 'MON'";
	String EMSG_RETAIL_PHARMACY = "Pharmacy Code Simple must have type 'RetailPharmacy'";
	String EMSG_MAIL_ORDER_PHARMACY = "Pharmacy Code Simple must have type 'MailOrderPharmacy'";

	String EMSG_DOLLAR_AMOUNT = "Whole Dollar($) amount must always be filled in no matter the cost sharing type";
	String EMSG_COPAYMENT_AMOUNT = "Must be $0 if Cost Sharing Type is Coinsurance";
	String EMSG_COST_VALUE = "Dollar($) amount must have positive value";
	String EMSG_COST_WHOLE_AMOUNT = "Dollar($) amount must have whole value";

	String EMSG_PERCENTAGE = "Whole Percentage(%) must always be filled in no matter the cost sharing type";
	String EMSG_COINSURANCE_PERC = "Must be 0% if Cost Sharing Type is Copayment";
	String EMSG_PERCENTAGE_VALUE = "Percentage(%) must be between 0 to 100";
	String EMSG_PERCENTAGE_WHOLE = "Percentage(%) must have whole value";

	String EMSG_NETWORK_ID = "Network Id is invalid";
	String EMSG_SERVICE_AREA_ID = "Service area Id is invalid";
	String EMSG_INVALID_TIN = "Invalid TIN value";
	String EMSG_INVALID_EMAIL = "Invalid Email Id";
	String EMSG_INVALID_PLAN_ID = "Invalid Plan Id";
	String EMSG_INVALID_VARIANT_ID = "Value must be 00 through 06";
	String EMSG_INVALID_STATE_CODE = "State Code does not match with Global Configure State Code: ";
	String EMSG_INVALID_STANDARD_STATE_CODE = "State Code in Standard Component ID does not match with Global Configure State Code: ";

	String EMSG_FORMAT_MISSMATCHED = "Value is not in valid format. ";
	String EMSG_INVALID_MARKET_COVERAGE_TYPE = "Invalid issuer market coverage type. ";
	String EMSG_HSA_HRA_EMPLOYER_CONTRIBUTION = "Invalid HSA/HRA Employer Contribution";
	String EMSG_HSA_HRA_EMPLOYER_CONTRIBUTION_AMOUNT_REQ = "Amount required.";
	String EMSG_SHOULD_NOT_EMPTY = "Value should not be empty. ";
	String EMSG_VALID_VALUE_SHOULD_YES_NO = "correct value should be YES or NO. ";
	String EMSG_INVALID_DATE = " invalid date. ";
	String EMSG_EMPTY_INVALID = "Empty/Invalid data";
	String EMSG_NUMERIC_OR_INVALID = EMSG_NUMERIC + " or Invalid Format";
}
