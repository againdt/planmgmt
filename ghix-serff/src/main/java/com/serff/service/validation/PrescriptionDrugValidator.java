package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_COINSURANCE_PERC;
import static com.serff.service.validation.IFailedValidation.EMSG_COPAYMENT_AMOUNT;
import static com.serff.service.validation.IFailedValidation.EMSG_COST_VALUE;
import static com.serff.service.validation.IFailedValidation.EMSG_COST_WHOLE_AMOUNT;
import static com.serff.service.validation.IFailedValidation.EMSG_EMPTY_INVALID;
import static com.serff.service.validation.IFailedValidation.EMSG_FORMAT;
import static com.serff.service.validation.IFailedValidation.EMSG_FORMULARYID_PREFIX_VALUE;
import static com.serff.service.validation.IFailedValidation.EMSG_LENGTH;
import static com.serff.service.validation.IFailedValidation.EMSG_MEASURE_POINT_VALUE;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC;
import static com.serff.service.validation.IFailedValidation.EMSG_PERCENTAGE_VALUE;
import static com.serff.service.validation.IFailedValidation.EMSG_PERCENTAGE_WHOLE;
import static com.serff.service.validation.IFailedValidation.EMSG_SHOULD_NOT_EMPTY;
import static com.serff.service.validation.IFailedValidation.EMSG_URL;
import static com.serff.util.SerffConstants.DOLLARSIGN;
import static com.serff.util.SerffConstants.PERCENTAGE;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.serff.config.SerffConfiguration;
import com.getinsured.hix.serff.config.SerffConfiguration.SerffConfigurationEnum;
import com.serff.template.plan.DrugListVO;
import com.serff.template.plan.DrugListVOList;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.PlanAndBenefitsPackageVO;
import com.serff.template.plan.PlanAndBenefitsVO;
import com.serff.util.SerffConstants;

/**
 * Class is used to provide validation service to Prescription Drug.
 * 
 * @author Bhavin Parmar
 * @since Mar 16, 2015
 */
@Component
public class PrescriptionDrugValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(PrescriptionDrugValidator.class);
	private static final String NO_CHARGE = "no charge";
	private static final String NO_CHARGE_AFTER_DEDUCTIBLE = "no charge after deductible";
	private static final String NOT_APPLICABLE = "not applicable";
	private static final String COPAYMENT_FIELD = "Copayment";
	private static final String COINSURANCE_FIELD = "CoInsurance";
	private static final String MSG_HIOS_ID_NOT_MATCHING = "Hios Id is not matching in plan and prescription drug template.";
	private static final String MSG_INVALID_HIOS_ID_PLAN = "Invalid Hios Id in plan template. planTemplateHiosId :";
	private static final String MSG_APPLICABLE_YEAR_NOT_MATCHING = "Applicable year is not matching in subsequent Plan Benefits list.";
	private static final String MSG_INVALID_HIOS_ID_PND =  "Invalid Hios Id in prescription drug template. prescriptionDrugTemplateHiosId :"; 

	public static final String NEW_TEMPLATE_VERSION = "v5";
	protected static final int PRESCRIPTION_PERIOD_1MONTH = 1;
	protected static final int PRESCRIPTION_PERIOD_3MONTH = 3;
	protected static final String TEMPLATE_NAME = "PRESCRIPTION_DRUG_DATA_TEMPLATE";
	protected static final int IN_NETWORK_DIGIT_SET = 10; //TENS
	protected static final int OUT_NETWORK = 0;		//00
	protected static final int COPAYMENT_DIGIT_SET = 100; //HUNDREDS
	protected static final int BOTH_COPAY_COINS_DIGIT_SET = 200; //HUNDREDS
	protected static final int COINSURANCE = 0;		//000

	/**
	 * Rule: Validate HIOS Issuer ID
	 * @param issuerIdCellVO, object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	protected String validationHIOSIssuerID(final ExcelCellVO issuerIdCellVO, List<FailedValidation> errors)  {

		String value = null;
		String fieldName = "HIOS Issuer ID";

		if (null != issuerIdCellVO) {

			value = issuerIdCellVO.getCellValue();
			LOGGER.debug("Validation Header 1: HIOS Issuer ID : "+value);
			if (StringUtils.isNotEmpty(value)) {

				if (!StringUtils.isNumeric(value)) {
					LOGGER.debug(fieldName + ": " + EMSG_NUMERIC);
					errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
				else if (!isValidLength(LEN_HIOS_ISSUER_ID, value)) {
					LOGGER.debug(fieldName + ": " + EMSG_LENGTH + LEN_HIOS_ISSUER_ID);
					errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_HIOS_ISSUER_ID, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
			}
			else {
				errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
		}
		else {
			LOGGER.debug("Validation Header 1: HIOS Issuer ID : "+value);
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		return value;
	}

	/**
	 * Rule: Validate Issuer State
	 *
	 * @param usStateCodeCellVO, object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @return value of Issuer State Code
	 */
	protected String validationIssuerState(final ExcelCellVO usStateCodeCellVO, List<FailedValidation> errors)  {

		String value = null;
		String fieldName = "Issuer State";

		if (null != usStateCodeCellVO) {

			value = usStateCodeCellVO.getCellValue();
			LOGGER.debug("Validation Header 2: Issuer State : "+value);
			if (StringUtils.isNotEmpty(value)) {

				if (!isValidLength(LEN_ISSUER_STATE, value)) {
					LOGGER.debug(fieldName + ": " + EMSG_LENGTH + LEN_ISSUER_STATE);
					errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_ISSUER_STATE, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
			else {
				errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else {
			LOGGER.debug("Validation Header 2: Issuer State : "+value);
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		return value;
	}

	/**
	 * Rules: Formulary IDs are template generated in the format of XXF001.
	 * XX is the state abbreviation. F is an indicator of "Formulary ID" and
	 * the last three digits will increase based on Number of Formularies.
	 * No Duplicates on template Once an ID has been selected,
	 * template will create 7 rows below it for the Tiers and grey out appropriate boxes
	 *
	 * @param formularyIdCellVO Object of ExcelCellVO
	 * @param issuerStateCode, String
	 * @param errors List<FailedValidation>
	 */
	protected void validateFormularyId(final ExcelCellVO formularyIdCellVO,
			String issuerStateCode, List<FailedValidation> errors) {

		String fieldName = "Formulary ID";
		String value =null;
		if (null != formularyIdCellVO) {
			value = formularyIdCellVO.getCellValue();
			LOGGER.debug("Formulary Validation 1: Formulary ID : "+value);
			if (StringUtils.isNotEmpty(value)) {

				if (!isValidLength(LEN_FORMULARY_ID, value)) {
					LOGGER.debug(fieldName + ": " + EMSG_LENGTH + LEN_FORMULARY_ID);
					errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_FORMULARY_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (!isValidPattern(PATTERN_FORMULARY_ID, value)) {
					LOGGER.debug(fieldName + ": " + EMSG_FORMAT);
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (!value.substring(0, LEN_ISSUER_STATE).equals(issuerStateCode)) {
					LOGGER.debug(fieldName + " : " + value + ": " + EMSG_FORMULARYID_PREFIX_VALUE + issuerStateCode);
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMULARYID_PREFIX_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
			else {
				errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else {
			LOGGER.debug("Formulary Validation 1: Formulary ID : "+value);
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Rule: Valid URL
	 *
	 * @param formularyUrlCellVO, ExcelCellVO
	 * @param errors, List<FailedValidation>
	 */
	protected void validateFormularyURI(final ExcelCellVO formularyUrlCellVO, List<FailedValidation> errors)  {

		String fieldName = "Formulary URL";
		if (null != formularyUrlCellVO) {
			String value = StringUtils.trim(formularyUrlCellVO.getCellValue());
			if (StringUtils.isNotBlank(value)) {

				if (!isValidPattern(PATTERN_URL, value)) {
					LOGGER.debug(fieldName + ": " + EMSG_URL);
					errors.add(getFailedValidation(fieldName, value, EMSG_URL, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
			else{
				errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		} else {
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	protected void validateCopayCoInsurance(String field, String copayOrCoinsuranceValue,
			boolean isCoPayment, List<FailedValidation> errors) {

		boolean isInvalid = false;
		String fieldName = isCoPayment ? COPAYMENT_FIELD : COINSURANCE_FIELD;
		SerffConfigurationEnum configData = isCoPayment ? SerffConfigurationEnum.DV_COPAYMENT_DATA : SerffConfigurationEnum.DV_COINSURANCE_DATA;

		if (!NO_CHARGE.equalsIgnoreCase(copayOrCoinsuranceValue)
				&& !NO_CHARGE_AFTER_DEDUCTIBLE.equalsIgnoreCase(copayOrCoinsuranceValue)
				&& !NOT_APPLICABLE.equalsIgnoreCase(copayOrCoinsuranceValue)) {

			String numericValue = null;
			String attrValue = null;

			if (copayOrCoinsuranceValue.indexOf(SerffConstants.SPACE) > -1) {
				numericValue = copayOrCoinsuranceValue.substring
					(0, copayOrCoinsuranceValue.indexOf(SerffConstants.SPACE)).replace((isCoPayment ? DOLLARSIGN : PERCENTAGE), StringUtils.EMPTY).trim();
				attrValue = copayOrCoinsuranceValue.substring(copayOrCoinsuranceValue.indexOf(SerffConstants.SPACE)).trim();
			}
			else {
				numericValue = copayOrCoinsuranceValue.replace((isCoPayment ? DOLLARSIGN : PERCENTAGE), StringUtils.EMPTY).trim();
			}

			if (isCoPayment) {
				numericValue = numericValue.replace(SerffConstants.COMMA, StringUtils.EMPTY).trim();
			}

			if (!validateNumeric(numericValue)) {
				isInvalid = true;
			}
			else if(StringUtils.isNotBlank(attrValue)
					&& !SerffConfiguration.hasPropertyInList(attrValue, configData)) {
				isInvalid = true;
			}
		}
		else if (StringUtils.isBlank(copayOrCoinsuranceValue)) {
			isInvalid = true;
		}

		if (isInvalid) {
			LOGGER.debug(field + " invalid " + fieldName + SerffConstants.COMMA + copayOrCoinsuranceValue);
			errors.add(getFailedValidation(field + fieldName, copayOrCoinsuranceValue, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used to validate CostSharing param values
	 *
	 * @param field, String
	 * @param isCoPayment, boolean
	 * @param coPaymentStr, String
	 * @param coInsuranceStr, String
	 * @param errors, List<FailedValidation>
	 */
	protected boolean isValidCostSharingValue(String field, boolean isCoPayment, boolean isCoinsurance, String coPaymentStr,
			String coInsuranceStr, List<FailedValidation> errors) {

		boolean valid = false;
		BigDecimal coPayment = null, coInsurance = null;
		BigDecimal temp = null;

		if (StringUtils.isNotBlank(coPaymentStr)
				&& validateNumeric(coPaymentStr.replace(DOLLARSIGN, StringUtils.EMPTY))) {
			coPayment = new BigDecimal(coPaymentStr.replace(DOLLARSIGN, StringUtils.EMPTY).replace(SerffConstants.COMMA, StringUtils.EMPTY).trim());
		}
		else {
			LOGGER.debug(field + " Copayment invalid value:" + coPaymentStr);
			errors.add(getFailedValidation(field + " Copayment", coPaymentStr, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
		}

		if (StringUtils.isNotBlank(coInsuranceStr)
				&& validateNumeric(coInsuranceStr.replace(PERCENTAGE, StringUtils.EMPTY))) {
			coInsurance = new BigDecimal(coInsuranceStr.replace(PERCENTAGE, StringUtils.EMPTY));
		}
		else {
			LOGGER.debug(field + " CoInsurance invalid value:" + coInsuranceStr);
			errors.add(getFailedValidation(field + " CoInsurance", coInsuranceStr, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
		}

		if (coPayment != null && coInsurance != null) {

			if (isCoPayment) {

				if (isCoinsurance) {
					temp = BigDecimal.ZERO;
				}
				else {
					temp = coInsurance;
				}
				validateCopayment(field, coPayment, temp, errors);
			}

			if (isCoinsurance) {

				if (isCoPayment) {
					temp = BigDecimal.ZERO;
				}
				else {
					temp = coPayment;
				}
				validateCoinsurance(field, coInsurance, temp, errors);
			}
			valid = true;
		}
		return valid;
	}

	/**
	 * Method is used to validate and get Drug Cost Sharing type code
	 *
	 * @param nwCostType, String object with value In Network or Out Network
	 * @param prescriptionPeriod, String object with value 1 or 3
	 * @param sharingType, String object with value Copayment or CoInsurance
	 * @param errors, List<FailedValidation>
 	 * @return int DCSTypeCode with 3 digit (HTU) set as below
 	 * H - 1 for copayment, 0 for coinsurance, 2 for both
 	 * T - 1 for in network, 0 for out network
 	 * U - 1 for one month, 3 for three month, 0 for invalid
	 */
	protected int getDCSTypeCode(String nwCostType, String prescriptionPeriod, String sharingType,
			boolean isNewVersion, List<FailedValidation> errors) {

		int sharingCode = 0;

		if (StringUtils.isNotBlank(nwCostType)
				&& StringUtils.isNotBlank(prescriptionPeriod)) {

			if (!StringUtils.isNumeric(prescriptionPeriod)) {
				errors.add(getFailedValidation("Prescription Period " + nwCostType, prescriptionPeriod, EMSG_FORMAT, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else {
				int presPeriod = Integer.parseInt(prescriptionPeriod);

				if (presPeriod == PRESCRIPTION_PERIOD_1MONTH || presPeriod == PRESCRIPTION_PERIOD_3MONTH) {
					sharingCode = presPeriod;
				}
				else {
					errors.add(getFailedValidation("Prescription Period " + nwCostType, String.valueOf(presPeriod), EMSG_MEASURE_POINT_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}

			if (SerffConfiguration.hasPropertyInList(nwCostType, SerffConfigurationEnum.DV_NETWORK_COST_TYPE)) {

				if (SerffConfiguration.hasPropertyValue(nwCostType, SerffConfigurationEnum.DV_IN_NETWORK_COST_TYPE)) {
					sharingCode += IN_NETWORK_DIGIT_SET;
				}
			}
			else {
				errors.add(getFailedValidation("Network Cost Type", nwCostType, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
			}

			if (!isNewVersion && StringUtils.isNotBlank(sharingType)) {

				if (SerffConfiguration.hasPropertyInList(sharingType, SerffConfigurationEnum.DV_COST_SHARING_TYPE)) {

					if (SerffConfiguration.hasPropertyValue(sharingType, SerffConfigurationEnum.DV_COPAYMENT_COST_SHARING_TYPE)) {
						sharingCode += COPAYMENT_DIGIT_SET;
					}
					else if(!SerffConfiguration.hasPropertyValue(sharingType, SerffConfigurationEnum.DV_COINSURANCE_COST_SHARING_TYPE)) {
						// it is either 'Lesser of Copayment or Coinsurance' or 'Greater of Copayment or Coinsurance'
						sharingCode += BOTH_COPAY_COINS_DIGIT_SET;
					}
				}
				else {
					errors.add(getFailedValidation("Cost Sharing Type", sharingType, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
		}
		return sharingCode;
	}

	/**
	 * Method is used to validate copayment
	 *
	 * @param fieldName, String field name
	 * @param coPayment, BigDecimal object containing copayment value
	 * @param coInsurance, BigDecimal object containing coInsurance value
	 * @param errors, List<FailedValidation>
	 */
	protected void validateCopayment(final String fieldName,
			final BigDecimal coPayment, final BigDecimal coInsurance,
			List<FailedValidation> errors) {

		if (0 != coInsurance.compareTo(BigDecimal.ZERO)) {
			LOGGER.debug(fieldName + ": " + EMSG_COINSURANCE_PERC);
			errors.add(getFailedValidation(fieldName, coInsurance.toString(),
				EMSG_COINSURANCE_PERC, ECODE_DEFAULT, TEMPLATE_NAME));
		}

		if (-1 == coPayment.signum()) {
			LOGGER.debug(fieldName + ": " + EMSG_COST_VALUE);
			errors.add(getFailedValidation(fieldName, coPayment.toString(),
				EMSG_COST_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		else if (!(0 == coPayment.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO))) {
			LOGGER.debug(fieldName + ": " + EMSG_COST_WHOLE_AMOUNT);
			errors.add(getFailedValidation(fieldName, coPayment.toString(),
				EMSG_COST_WHOLE_AMOUNT, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used validate coinsurance
	 *
	 * @param fieldName, String field name
	 * @param coInsurance, BigDecimal object containing coInsurance value
	 * @param coPayment, BigDecimal object containing copayment value
	 * @param errors, List<FailedValidation>
	 */
	protected void validateCoinsurance(final String fieldName,
			final BigDecimal coInsurance, final BigDecimal coPayment,
			List<FailedValidation> errors) {


		if (0 != coPayment.compareTo(BigDecimal.ZERO)) {
			LOGGER.debug(fieldName + ": " + EMSG_COPAYMENT_AMOUNT);
			errors.add(getFailedValidation(fieldName, coPayment.toString(),
				EMSG_COPAYMENT_AMOUNT, ECODE_DEFAULT, TEMPLATE_NAME));
		}

		if (-1 == coInsurance.signum() || 1 == coInsurance.compareTo(new BigDecimal("100"))) {
			LOGGER.debug(fieldName + ": " + EMSG_PERCENTAGE_VALUE);
			errors.add(getFailedValidation(fieldName, coInsurance.toString(),
				EMSG_PERCENTAGE_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		else if (!(0 == coInsurance.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO))) {
			LOGGER.debug(fieldName + ": " + EMSG_PERCENTAGE_WHOLE);
			errors.add(getFailedValidation(fieldName, coInsurance.toString(),
				EMSG_PERCENTAGE_WHOLE,ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used to validate Drug List data.
	 * @param drugList, Object of DrugListVOList.
	 * @param errors, List<FailedValidation>
	 * @return boolean value.
	 */
	protected boolean validateDrugList(DrugListVOList drugList, List<FailedValidation> errors) {

		LOGGER.debug("Validate Drug List Start");
		boolean isValid = true;

		try {
			if (null == drugList) {
				isValid = false;
				LOGGER.debug("PrescriptionDrug: Template has empty DruglistVO");
				errors.add(getFailedValidation("Drug List Element", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else if (CollectionUtils.isEmpty(drugList.getDrugListVO())) {
				isValid = false;
				LOGGER.debug("PrescriptionDrug: Template has empty Drug list");
				errors.add(getFailedValidation("Drug List", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}

			if (!isValid) {
				return isValid;
			}

			for (DrugListVO drugVO : drugList.getDrugListVO()) {

				if (null == drugVO) {
					isValid = false;
					LOGGER.debug("PrescriptionDrug: Template has empty DrugListVO");
					errors.add(getFailedValidation("DrugListVO", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else {

					if (null == drugVO.getDrugTierLevelList()
							|| CollectionUtils.isEmpty(drugVO.getDrugTierLevelList().getDrugTierLevelVO())) {
						isValid = false;
						LOGGER.debug("PrescriptionDrug: Template has empty Drug Tier Level List");
						errors.add(getFailedValidation("DrugTierLevelList", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
					}

					if (null == drugVO.getDrugVOList()
							|| CollectionUtils.isEmpty(drugVO.getDrugVOList().getDrugVO())) {
						isValid = false;
						LOGGER.debug("PrescriptionDrug: Template has empty DrugVOList");
						errors.add(getFailedValidation("DrugVOList", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
					}
				}

				if (!isValid) {
					break;
				}
			}
		}
		finally {
			LOGGER.debug("Validate Drug List End");
		}
		return isValid;
	}
	
	/**
	 * Validates prescription drug job templates.
	 * 
	 * @param stateCode - State code
	 * @param planTemplateInstance - Plan template
	 * @param prescriptionDrugTemplateInstance - prescription drug template
	 * @param componentIdsAndFormularyInTemplate - Map of plan id and formulary id
	 * @param errorCodes - Error codes list
	 * @param errorMessages - Error messages list
	 * @return - whether templates are valid
	 */
	public boolean isValidPrescriptionDrugJobTemplates(String stateCode, com.serff.template.plan.PlanBenefitTemplateVO planTemplateInstance, com.serff.template.plan.PrescriptionDrugTemplateVO prescriptionDrugTemplateInstance, 
			Map<String, String> componentIdsAndFormularyInTemplate, List<String> errorCodes, List<String> errorMessages){
		
		LOGGER.info("isValidPrescriptionDrugJobTemplates() Start");
		
		String prescriptionDrugTemplateHiosId = prescriptionDrugTemplateInstance.getHeader().getIssuerId().getCellValue().trim();
		
		if(validateCarrierHIOS(prescriptionDrugTemplateHiosId)){
			
			int applicableYear = 0;
			
			for(PlanAndBenefitsPackageVO planAndBenefitsPackageVO : planTemplateInstance.getPackagesList().getPackages()){
				
				String planTemplateHiosId = planAndBenefitsPackageVO.getHeader().getIssuerId().getCellValue();
				LOGGER.debug("isValidPrescriptionDrugJobTemplates() : prescriptionDrugTemplateHiosId : " + prescriptionDrugTemplateHiosId + " : planTemplateHiosId : " + planTemplateHiosId);
				if(validateCarrierHIOS(planTemplateHiosId)){
					
					if(!(planTemplateHiosId.equals(prescriptionDrugTemplateHiosId))){
						LOGGER.error("validatePrescriptionDrugTemplateData() : Hios Id is not matching in plan and prescription drug template.");
						errorMessages.add(MSG_HIOS_ID_NOT_MATCHING);
						componentIdsAndFormularyInTemplate.clear();
						return false;
					}else{
						for(PlanAndBenefitsVO planAndBenefitsVO : planAndBenefitsPackageVO.getPlansList().getPlans()){
							
							String effectiveStartdate = planAndBenefitsPackageVO.getPlansList().getPlans().get(0).getPlanAttributes().getPlanEffectiveDate().getCellValue();
							LOGGER.info("populateAndPersistPrescriptionDrugTemplates() : effectiveStartdate : " + effectiveStartdate);
							int planApplicableYear = Integer.parseInt(effectiveStartdate.split("['/']")[SerffConstants.APPLICABLE_YEAR_INDEX]);
							
							if((applicableYear != 0) && (applicableYear != planApplicableYear)){
								LOGGER.error("validatePrescriptionDrugTemplateData() : Applicable year is not matching in subsequent Plan Benefits list.");
								errorMessages.add(MSG_APPLICABLE_YEAR_NOT_MATCHING);
								return false;
							}else{
								applicableYear = planApplicableYear;
								String standardComponentId = planAndBenefitsVO.getPlanAttributes().getStandardComponentID().getCellValue();
								String formulary = planAndBenefitsVO.getPlanAttributes().getFormularyID().getCellValue();
								validateStandardComponentId(standardComponentId, stateCode, errorCodes, errorMessages);

								if(CollectionUtils.isEmpty(errorCodes) && CollectionUtils.isEmpty(errorMessages)){
									componentIdsAndFormularyInTemplate.put(standardComponentId,formulary);
								}else{
									LOGGER.error("validatePrescriptionDrugTemplateData() : Invalid standard component id. Skipped standardComponentId : " + standardComponentId);
								}
							}
						}
					}
					
				}else{
					LOGGER.error("validatePrescriptionDrugTemplateData() : Invalid Hios Id in plan template. planTemplateHiosId : " + planTemplateHiosId);
					errorMessages.add(MSG_INVALID_HIOS_ID_PLAN + planTemplateHiosId);
					return false;
				}
			}
			//Template data is valid. Return true.
			LOGGER.info("isValidPrescriptionDrugJobTemplates() End. Returning true.");
			return true;
			
		}else{
			LOGGER.error("validatePrescriptionDrugTemplateData() : Invalid Hios Id in prescription drug template. prescriptionDrugTemplateHiosId : " + prescriptionDrugTemplateHiosId);
			LOGGER.info("isValidPrescriptionDrugJobTemplates() End. Returning false.");
			errorMessages.add(MSG_INVALID_HIOS_ID_PND + prescriptionDrugTemplateHiosId);
			return false;
		}
	}
	
}
