package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_EMPTY_INVALID;
import static com.serff.service.validation.IFailedValidation.EMSG_SHOULD_NOT_EMPTY;
import static com.serff.util.SerffConstants.HIOS_ISSUER_ID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.serff.template.plan.CostSharingTypeVO;
import com.serff.template.plan.CostSharingTypeVOList;
import com.serff.template.plan.FormularyCostSharingTypeVO;
import com.serff.template.plan.FormularyVO;
import com.serff.template.plan.PrescriptionDrugTemplateVO;
import com.serff.util.SerffConstants;

/**
 * Class is used to to implements all validations of Prescription Drug Template for Version 5.
 * 
 * @author Bhavin Parmar
 * @since Mar 16, 2015
 */
@Component("prescriptionDrugValidatorV5")
public class PrescriptionDrugValidatorV5 extends PrescriptionDrugValidator implements IPrescriptionDrugValidator {

	private static final Logger LOGGER = Logger.getLogger(PrescriptionDrugValidatorV5.class);
	private static final int ONE_MONTH_IN_NETWORK = PRESCRIPTION_PERIOD_1MONTH + IN_NETWORK_DIGIT_SET;
	private static final int THREE_MONTH_IN_NETWORK = PRESCRIPTION_PERIOD_3MONTH + IN_NETWORK_DIGIT_SET;
	private static final int ONE_MONTH_OUT_NETWORK = PRESCRIPTION_PERIOD_1MONTH + OUT_NETWORK;
	private static final int THREE_MONTH_OUT_NETWORK = PRESCRIPTION_PERIOD_3MONTH + OUT_NETWORK;

	/**
	 * Method is used to validate each field of Prescription Drug.
	 * 
	 * @param templateVO, Object of PrescriptionDrugTemplateVO
	 * @return FailedValidation objects in List.
	 */
	@Override
	public Map<String, List<String>> validatePrescriptionDrug(final PrescriptionDrugTemplateVO templateVO, List<FailedValidation> errors) {

		LOGGER.info("validatePrescriptionDrug() Start");

		Map<String, List<String>> templateData = null;
		String issuerId = null;

		try {
			boolean isValid = true;

			if (null == templateVO) {
				isValid = false;
				LOGGER.debug("PrescriptionDrug: Template is empty ");
				errors.add(getFailedValidation("ROOT Element", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else {

				if (null == templateVO.getHeader()) {
					isValid = false;
					LOGGER.debug("PrescriptionDrug: Template has empty header");
					errors.add(getFailedValidation("Header Element", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
				}

				if (isValid && null == templateVO.getFormularyList()) {
					isValid = false;
					LOGGER.debug("PrescriptionDrug: Template has empty formulary list");
					errors.add(getFailedValidation("Formulary List Element", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				isValid = isValid && validateDrugList(templateVO.getDrugList(), errors);
			}

			if (isValid) {
				issuerId = validationHIOSIssuerID(templateVO.getHeader().getIssuerId(), errors);
				String issuerStateCode = validationIssuerState(templateVO.getHeader().getStatePostalCode(), errors);
				validateFormularySheet(templateVO.getFormularyList().getFormularyVO(), issuerStateCode, errors);
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error: ", ex);
		}
		finally {
			templateData = new HashMap<String, List<String>>();
			List<String> issuerIdList = new ArrayList<String>();
			issuerIdList.add(issuerId);
			templateData.put(HIOS_ISSUER_ID, issuerIdList);
			// templateData.put(SERVICE_AREA_ID, serviceAreaId);
			LOGGER.info("validatePrescriptionDrug() End");
		}
		return templateData;
	}

	/**
	 * Method is used to validate formulary sheet.
	 *
	 * @param issuerFormularyVOList, List<FormularyVO>
	 * @param issuerStateCode, String
	 * @param errors, List<FailedValidation>
	 */
	private void validateFormularySheet(final List<FormularyVO> issuerFormularyVOList, String issuerStateCode, List<FailedValidation> errors) {

		LOGGER.debug("Validation for Formulary Sheet Start");
		boolean found = false;

		try {
			if (CollectionUtils.isEmpty(issuerFormularyVOList)) {
				return;
			}
	
			for (FormularyVO formularyVO : issuerFormularyVOList) {
	
				if (null == formularyVO) {
					continue;
				}

				if (!found) {
					found = true;
				}
				validateFormularyId(formularyVO.getFormularyID(), issuerStateCode, errors);
				LOGGER.debug("Formulary Validation 2: Formulary URL");
				validateFormularyURI(formularyVO.getFormularyUrl(), errors);
	
				if (null != formularyVO.getFormularyCostSharingTypeList()) {
					LOGGER.debug("Formulary Validation 3: Formulary Drug Cost Sharing Type List");
					formularyDrugCostSharingTier(formularyVO.getFormularyCostSharingTypeList().getFormularyCostSharingTypeVO(), errors);
				}
				else {
					errors.add(getFailedValidation("FormularyCostSharingTypeList", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
		}
		finally {

			if (!found) {
				LOGGER.error("PrescriptionDrug: Formulary List is empty");
				errors.add(getFailedValidation("Formulary List", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			LOGGER.debug("Validation for Formulary Sheet End");
		}
	}

	/**
	 * Method is used to validate DrugCostSharingTier List
	 *
	 * @param drugCostSharingTierList, List<FormularyCostSharingTypeVO>
	 * @param errors, List<FailedValidation>
	 */
	private void formularyDrugCostSharingTier(final List<FormularyCostSharingTypeVO> formularyCostSharingTypeList, List<FailedValidation> errors) {

		LOGGER.debug("Validation for Drug cost sharing tier list Start");
		boolean found = false;

		try {
			if (CollectionUtils.isEmpty(formularyCostSharingTypeList)) {
				errors.add(getFailedValidation("FormularyCostSharingTypeList", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
				return;
			}

			for (FormularyCostSharingTypeVO formularyCostSharingTypeVO : formularyCostSharingTypeList) {

				if (null == formularyCostSharingTypeVO) {
					continue;
				}

				if (!found) {
					found = true;
				}
				validateFormularyCostSharingTypeVO(formularyCostSharingTypeVO, errors);
			}
		}
		finally {

			if (!found) {
				errors.add(getFailedValidation("FormularyCostSharingTypeList", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			LOGGER.debug("Validation for Drug cost sharing tier list End");
		}
	}

	/**
	 * Method is used to validate Formulary CostSharing 
	 * @param formularyCostSharingTypeVO, object of FormularyCostSharingTypeVO
	 * @param errors, List<FailedValidation>
	 */
	private void validateFormularyCostSharingTypeVO(FormularyCostSharingTypeVO formularyCostSharingTypeVO, List<FailedValidation> errors) {

		boolean oneMonthOutNetworkRetailCheckReqd = YES.equalsIgnoreCase(formularyCostSharingTypeVO.getOneMonthOutNetworkRetailOfferedIndicator().getCellValue());
		boolean threeMonthInNetworkMailCheckReqd = YES.equalsIgnoreCase(formularyCostSharingTypeVO.getThreeMonthInNetworkMailOfferedIndicator().getCellValue());
		boolean threeMonthOutNetworkMailCheckReqd = YES.equalsIgnoreCase(formularyCostSharingTypeVO.getThreeMonthOutNetworkMailOfferedIndicator().getCellValue());

		Map<Integer, Boolean> sharingCodeValidation = new HashMap<Integer, Boolean>();
		CostSharingTypeVOList drugCostSharingTypeList = formularyCostSharingTypeVO.getCostSharingTypeList();

		sharingCodeValidation.put(ONE_MONTH_IN_NETWORK, false);
		sharingCodeValidation.put(ONE_MONTH_OUT_NETWORK, !oneMonthOutNetworkRetailCheckReqd);
		sharingCodeValidation.put(THREE_MONTH_OUT_NETWORK, !threeMonthOutNetworkMailCheckReqd);
		sharingCodeValidation.put(THREE_MONTH_IN_NETWORK, !threeMonthInNetworkMailCheckReqd);

		if (null == drugCostSharingTypeList
				|| CollectionUtils.isEmpty(drugCostSharingTypeList.getCostSharingTypeVO())) {
			errors.add(getFailedValidation("CostSharingTypeList", EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			return;
		}

		for (CostSharingTypeVO drugCostSharingType : drugCostSharingTypeList.getCostSharingTypeVO()) {

			if (null == drugCostSharingType) {
				continue;
			}
			LOGGER.debug("Formulary Validation for: " + drugCostSharingType.getDrugPrescriptionPeriodType() + " month "
					+ drugCostSharingType.getNetworkCostType());

			validateCostSharingVO(drugCostSharingType.getNetworkCostType(), drugCostSharingType.getDrugPrescriptionPeriodType(),
					drugCostSharingType.getCoPayment().getCellValue(), drugCostSharingType.getCoInsurance().getCellValue(),
					sharingCodeValidation, errors);
		}

		/**
		 * Rules:
		 * Validation 11: Required if 1 Month Out-of-Network Retail Pharmacy Offered is Yes
		 * Validation 15: Required if 3 Month In-Network Mail Order Pharmacy Offered is Yes
		 * Validation 19: Required if 3 Month Out-of-Network Mail Order Pharmacy Offered is Yes
		 */
		if (threeMonthInNetworkMailCheckReqd) {
			LOGGER.debug("Validation 15: 3 Month In-Network Mail Order Pharmacy Cost Sharing Type");
			if (!sharingCodeValidation.get(THREE_MONTH_IN_NETWORK)) {
				errors.add(getFailedValidation("CostSharingTypeList threeMonthInNetwork", EMPTY_VALUE, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}

		if (oneMonthOutNetworkRetailCheckReqd) {
			LOGGER.debug("Validation 11: Up to 1 Month Out-of-Network Retail Pharmacy Cost Sharing Type");
			if (!sharingCodeValidation.get(ONE_MONTH_OUT_NETWORK) ) {
				errors.add(getFailedValidation("CostSharingTypeList oneMonthOutNetworkRetail", EMPTY_VALUE, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}

		if (threeMonthOutNetworkMailCheckReqd) {
			LOGGER.debug("Validation 19: 3 Month Out–of–Network Mail Order Pharmacy Cost–Sharing Type");
			if (!sharingCodeValidation.get(THREE_MONTH_OUT_NETWORK)) {
				errors.add(getFailedValidation("CostSharingTypeList threeMonthOutNetworkMail", EMPTY_VALUE, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}

		if (!sharingCodeValidation.get(ONE_MONTH_IN_NETWORK)) {
			errors.add(getFailedValidation("CostSharingTypeList oneMonthInNetwork", EMPTY_VALUE, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used to validate CostSharing 
	 * @param networkCostTypeStr, String value
	 * @param drugPrescriptionPeriodTypeStr , String value
	 * @param costSharingTypeStr, String value
	 * @param coPaymentStr, String value
	 * @param coInsuranceStr, String value
	 * @param sharingCodeValidation , Map<Integer, Boolean>
	 * @param errors, List<FailedValidation>
	 */
	private void validateCostSharingVO(String networkCostTypeStr, String drugPrescriptionPeriodTypeStr, String coPaymentStr,
			String coInsuranceStr, Map<Integer, Boolean> sharingCodeValidation, List<FailedValidation> errors) {

		int sharingCode = getDCSTypeCode(networkCostTypeStr, drugPrescriptionPeriodTypeStr, null, true, errors);
		boolean isValidateCostSharing = false;
		String field = null;

		if (sharingCode > 0) {
			field =  drugPrescriptionPeriodTypeStr + " month " + networkCostTypeStr + SerffConstants.SPACE;
			LOGGER.debug("CostSharing Validation for " + field + " sharingCode: " + sharingCode);
		}

		switch(sharingCode) {
			case ONE_MONTH_IN_NETWORK: {
				//Required always
				/**
				 * Rules:
				 * Validation 7: If Copayment is selected, the Coinsurance field defaults to 0%
				 * If Coinsurance is selected, the Copayment field defaults to $0
				 *
				 * Validation 8: Whole $ dollar amount. Must always be filled in no matter the cost sharing type.
				 * Must be $0 if Cost Sharing Type is Coinsurance
				 *
				 * Validation 9:Whole % percentage. Must always be filled in no matter the cost sharing type.
				 * Must be 0% if Cost Sharing Type is Copayment
				 */
				LOGGER.debug("Formulary Validation 7,8,9: 1 Month In-Network Retail Pharmacy Cost Sharing Type");
				isValidateCostSharing = true;
				sharingCodeValidation.put(ONE_MONTH_IN_NETWORK, true);
				break;
			}
			case THREE_MONTH_IN_NETWORK: {
				if(!sharingCodeValidation.get(THREE_MONTH_IN_NETWORK)) {
					/**
					 * Rules:
					 * Validation 14: If Copayment is selected, the Coinsurance field defaults to 0%
					 * If Coinsurance is selected, the Copayment field defaults to $0
					 *
					 * Validation 16: Required if 3 Month In-Network Mail Order Pharmacy Offered is Yes
					 * Whole $ dollar amount Must be $0 if Cost Sharing Type is Coinsurance
					 *
					 * Validation 17: Required if 3 Month In-Network Mail Order Pharmacy Offered is Yes
					 * Whole % percentageMust be 0% if Cost Sharing Type is Copayment
					 */
					LOGGER.debug("Validation 14,16,17: 3 Month In-Network Mail Order Pharmacy Benefit Offered");
					isValidateCostSharing = true;
					sharingCodeValidation.put(THREE_MONTH_IN_NETWORK, true);
				}
				break;
			}
			case ONE_MONTH_OUT_NETWORK: {
				if(!sharingCodeValidation.get(ONE_MONTH_OUT_NETWORK)) {
					/**
					 * Rules:
					 * Validation 10: If Copayment is selected, the Coinsurance field defaults to 0%
					 * If Coinsurance is selected, the Copayment field defaults to $0
					 *
					 * Validation 12: Required if 1 Month Out-of-Network Retail Pharmacy Offered is Yes
					 * Whole $ dollar amount Must be $0 if Cost Sharing Type is Coinsurance
					 *
					 * Validation 13: Required if 1 Month Out-of-Network Retail Pharmacy Offered is Yes
					 * Whole % percentage Must be 0% if Cost Sharing Type is Copayment
					 */
					LOGGER.debug("Validation 10,12,13: 1 Month Out-of-Network Retail Pharmacy Offered");
					isValidateCostSharing = true;
					sharingCodeValidation.put(ONE_MONTH_OUT_NETWORK, true);
				}
				break;
			}
			case THREE_MONTH_OUT_NETWORK: {
				if(!sharingCodeValidation.get(THREE_MONTH_OUT_NETWORK)) {
					/**
					 * Rules:
					 * Validation 18: If Copayment is selected, the Coinsurance field defaults to 0%
					 * If Coinsurance is selected, the Copayment field defaults to $0
					 *
					 * Validation 20: Required if 3 Month Out-of-Network Mail Order Pharmacy Offered is Yes
					 * Whole $ dollar amount Must be $0 if Cost Sharing Type is Coinsurance
					 *
					 * Validation 21: Required if 3 Month Out-of-Network Mail Order Pharmacy Offered is Yes
					 * Whole % percentage Must be 0% if Cost Sharing Type is Copayment
					 */
					LOGGER.debug("Validation 18,20,21: 3 Month Out-of-Network Mail Order Pharmacy Benefit Offered");
					isValidateCostSharing = true;
					sharingCodeValidation.put(THREE_MONTH_OUT_NETWORK, true);
				}
				break;
			}
			case 0:
				break;
			default: LOGGER.info("sharingCode did not matched");
		}

		if (isValidateCostSharing) { 
			validateCopayCoInsurance(field, coPaymentStr, true, errors);
			validateCopayCoInsurance(field, coInsuranceStr, false, errors);
		}
	}
}
