package com.serff.service.validation;

import static com.serff.util.SerffConstants.NEW_LINE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.getinsured.serffadapter.stm.STMExcelColumn.STMExcelColumnEnum;
import com.getinsured.serffadapter.stm.STMExcelVO;
import com.serff.template.admin.niem.usps.states.USStateCodeSimpleType;
import com.serff.util.SerffConstants;

/**
 * Class is used to validate STM Plan Excel.
 * 
 * @author Bhavin Parmar
 * @since September 9, 2014
 */
@Component
public class STMPlanValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(STMPlanValidator.class);

	private static final double HUNDRED = 100d;
	private static final String EMSG_EMPTY = " Value should not be empty." + NEW_LINE;
	private static final String EMSG_LENGTH = " Value must have length ";
	private static final String EMSG_PERCENTAGE = " Percentage(%) Value must be between 0 to 100." + NEW_LINE;
	private static final String EMSG_NUMERIC = " Value must be numeric." + NEW_LINE;
	private static final String EMSG_INVALID_VALUE = " Invalid value.";
	private static final String EMSG_INVALID_STATE = " Invalid State." + NEW_LINE;
	private static final String EMSG_WHOLE_NUMBER = " Value must have whole number." + NEW_LINE;
	private static final String EMSG_OPTION = " Correct value should be YES or NO or Y or N." + NEW_LINE;
	private static final String EMSG_UNIT = " Correct value should be months." + NEW_LINE;
	private static final String EMSG_INVALID_URL = " Invalid URL/ECM-ID." + NEW_LINE;
	private static final String OPTION_VALUE = "yes,no,y,n";
	private static final String UNIT_VALUE = "days,months";
	//private static final String EMSG_INVALID_DATE = " Invalid Dates Start date must be less then end date." + NEW_LINE;
	/**
	 * Method is used to validate require data from STMExcelVO object before persistence.
	 */
	public boolean validateSTMExcelVO(STMExcelVO stmExcelVO, List<STMExcelVO> failedPlanList) {
		
		LOGGER.debug("validateSTMExcelVO() Start");
		boolean isValid = Boolean.TRUE;
		
		try {
			StringBuilder errorMessages = new StringBuilder();
			validateLength(LEN_HIOS_ISSUER_ID, Boolean.TRUE, stmExcelVO.getHiosId(), stmExcelVO, errorMessages, STMExcelColumnEnum.HIOS_ID.getColumnName());
			
			if (!stmExcelVO.isNotValid()) {
				validateLength(LEN_STM_PLAN_ID, Boolean.TRUE, stmExcelVO.getPlanNumber(), stmExcelVO, errorMessages, STMExcelColumnEnum.ISSUER_PLAN_NUMBER.getColumnName());
				validateBlankData(stmExcelVO.getCarrierCode(), stmExcelVO, errorMessages, STMExcelColumnEnum.CARRIER_CODE.getColumnName());
				validateBlankData(stmExcelVO.getIssuerName(), stmExcelVO, errorMessages, STMExcelColumnEnum.ISSUER_NAME.getColumnName());
				validateBlankData(stmExcelVO.getPlanResponseName(), stmExcelVO, errorMessages, STMExcelColumnEnum.PLAN_RESPONSE_NAME.getColumnName());
				validateBlankData(stmExcelVO.getPlanName(), stmExcelVO, errorMessages, STMExcelColumnEnum.PLAN_NAME.getColumnName());
				validateBlankData(stmExcelVO.getNetworkType(), stmExcelVO, errorMessages, STMExcelColumnEnum.NETWORK_TYPE.getColumnName());
				validateBlankData(stmExcelVO.getPlanState(), stmExcelVO, errorMessages, STMExcelColumnEnum.PLAN_STATE.getColumnName());
				validateBlankData(stmExcelVO.getDurationNumber(), stmExcelVO, errorMessages, STMExcelColumnEnum.DURATION_NUMBER.getColumnName());
				validateBlankData(stmExcelVO.getDurationUnit(), stmExcelVO, errorMessages, STMExcelColumnEnum.DURATION_UNIT.getColumnName());
				validateBlankData(stmExcelVO.getCoinsurance(), stmExcelVO, errorMessages, STMExcelColumnEnum.COINSURANCE.getColumnName());
				validateBlankData(stmExcelVO.getOfficeVisits(), stmExcelVO, errorMessages, STMExcelColumnEnum.OFFICE_VISITS.getColumnName());
				validateBlankData(stmExcelVO.getOfficeVisitsNumber(), stmExcelVO, errorMessages, STMExcelColumnEnum.OFFICE_VISITS_NUMBER.getColumnName());
				validateBlankData(stmExcelVO.getOfficeVisitsForPrimDoct(), stmExcelVO, errorMessages, STMExcelColumnEnum.OFFICE_VISITS_FOR_PRIM_DOCT.getColumnName());
				validateBlankData(stmExcelVO.getOfficeVisitsForSepcialist(), stmExcelVO, errorMessages, STMExcelColumnEnum.OFFICE_VISITS_FOR_SEPCIALIST.getColumnName());
				validateBlankData(stmExcelVO.getDeductibleIndividual(), stmExcelVO, errorMessages, STMExcelColumnEnum.DEDUCTIBLE_INDIVIDUAL.getColumnName());
				validateBlankData(stmExcelVO.getDeductibleIndividualDesc(), stmExcelVO, errorMessages, STMExcelColumnEnum.DEDUCTIBLE_INDIVIDUAL_DESC.getColumnName());
				validateBlankData(stmExcelVO.getDeductibleFamilyDesc(), stmExcelVO, errorMessages, STMExcelColumnEnum.DEDUCTIBLE_FAMILY_DESC.getColumnName());
				validateBlankData(stmExcelVO.getOutOfPocketMaxIndividual(), stmExcelVO, errorMessages, STMExcelColumnEnum.OUT_OF_POCKET_MAX_INDIVIDUAL.getColumnName());
				validateBlankData(stmExcelVO.getOutOfPocketMaxIndividualDesc(), stmExcelVO, errorMessages, STMExcelColumnEnum.OUT_OF_POCKET_MAX_INDIVIDUAL_DESC.getColumnName());
				validateBlankData(stmExcelVO.getOutOfPocketLimitMaxFamily(), stmExcelVO, errorMessages, STMExcelColumnEnum.OUT_OF_POCKET_LIMIT_MAX_FAMILY_DESC.getColumnName());
				validateBlankData(stmExcelVO.getPolicyMaximum(), stmExcelVO, errorMessages, STMExcelColumnEnum.POLICY_MAXIMUM.getColumnName());
				validateBlankData(stmExcelVO.getOutNetwkCoverage(), stmExcelVO, errorMessages, STMExcelColumnEnum.OUT_NETWK_COVERAGE.getColumnName());
				validateBlankData(stmExcelVO.getOutCountyCoverage(), stmExcelVO, errorMessages, STMExcelColumnEnum.OUT_COUNTY_COVERAGE.getColumnName());
				validateBlankData(stmExcelVO.getGenericDrugs(), stmExcelVO, errorMessages, STMExcelColumnEnum.GENERIC_DRUGS.getColumnName());
				validateBlankData(stmExcelVO.getBrandDrugs(), stmExcelVO, errorMessages, STMExcelColumnEnum.BRAND_DRUGS.getColumnName());
				validateBlankData(stmExcelVO.getNonFormularyDrugsCoverage(), stmExcelVO, errorMessages, STMExcelColumnEnum.NON_FORMULARY_DRUGS_COVERAGE.getColumnName());
				validateBlankData(stmExcelVO.getSepDrugDeductible(), stmExcelVO, errorMessages, STMExcelColumnEnum.SEP_DRUG_DEDUCTIBLE.getColumnName());
				validateBlankData(stmExcelVO.getUrgentCare(), stmExcelVO, errorMessages, STMExcelColumnEnum.URGENT_CARE.getColumnName());
				validateBlankData(stmExcelVO.getEmergencyRoom(), stmExcelVO, errorMessages, STMExcelColumnEnum.EMERGENCY_ROOM.getColumnName());
				validateBlankData(stmExcelVO.getOutpatientLabXray(), stmExcelVO, errorMessages, STMExcelColumnEnum.OUTPATIENT_LAB_XRAY.getColumnName());
				validateBlankData(stmExcelVO.getOutpatientSurgery(), stmExcelVO, errorMessages, STMExcelColumnEnum.OUTPATIENT_SURGERY.getColumnName());
				validateBlankData(stmExcelVO.getHospitalization(), stmExcelVO, errorMessages, STMExcelColumnEnum.HOSPITALIZATION.getColumnName());
				validateBlankData(stmExcelVO.getChiropracticCoverage(), stmExcelVO, errorMessages, STMExcelColumnEnum.CHIROPRACTIC_COVERAGE.getColumnName());
				validateBlankData(stmExcelVO.getMentalHealthCoverage(), stmExcelVO, errorMessages, STMExcelColumnEnum.MENTAL_HEALTH_COVERAGE.getColumnName());
				validateBlankData(stmExcelVO.getPrePostnatalOfficeVisit(), stmExcelVO, errorMessages, STMExcelColumnEnum.PRE_POSTNATAL_OFFICE_VISIT.getColumnName());
				validateBlankData(stmExcelVO.getLaborDeliveryHospitalStay(), stmExcelVO, errorMessages, STMExcelColumnEnum.LABOR_DELIVERY_HOSPITAL_STAY.getColumnName());
				
				validateURL(stmExcelVO.getPlanBrochure(), stmExcelVO, errorMessages, STMExcelColumnEnum.PLAN_BROCHURE.getColumnName());
				validateURL(stmExcelVO.getExclusionsAndLimitations(), stmExcelVO, errorMessages, STMExcelColumnEnum.EXCLUSIONS_AND_LIMITATIONS.getColumnName());
				
				validateNumber(stmExcelVO.getDurationNumber(), stmExcelVO, errorMessages, STMExcelColumnEnum.DURATION_NUMBER.getColumnName());
				validateNumber(stmExcelVO.getOutOfPocketMaxIndividual(), stmExcelVO, errorMessages, STMExcelColumnEnum.OUT_OF_POCKET_MAX_INDIVIDUAL.getColumnName());
				validateNumber(stmExcelVO.getPolicyMaximum(), stmExcelVO, errorMessages, STMExcelColumnEnum.POLICY_MAXIMUM.getColumnName());
				
				validatePercentage(stmExcelVO.getCoinsurance(), stmExcelVO, errorMessages, STMExcelColumnEnum.COINSURANCE.getColumnName());
				validateDollarAndPercValue(stmExcelVO.getOfficeVisitsNumber(), stmExcelVO, errorMessages, STMExcelColumnEnum.OFFICE_VISITS_NUMBER.getColumnName());
				validateWholeValue(stmExcelVO.getOfficeVisitsNumber(), stmExcelVO, errorMessages, STMExcelColumnEnum.OFFICE_VISITS_NUMBER.getColumnName());
				
				validateAgainstAllowedValue(stmExcelVO.getIsHsa(), OPTION_VALUE, EMSG_OPTION, stmExcelVO, errorMessages, STMExcelColumnEnum.IS_HSA.getColumnName());
				validateAgainstAllowedValue(stmExcelVO.getDurationUnit(), UNIT_VALUE, EMSG_UNIT, stmExcelVO, errorMessages, STMExcelColumnEnum.DURATION_UNIT.getColumnName());
				
				validateDollarValue(stmExcelVO.getApplicationFee(), stmExcelVO, errorMessages, STMExcelColumnEnum.APPLICATION_FEE.getColumnName());
				validateDollarValue(stmExcelVO.getDeductibleIndividual(), stmExcelVO, errorMessages, STMExcelColumnEnum.DEDUCTIBLE_INDIVIDUAL.getColumnName());
				
				validateStateList(stmExcelVO.getPlanState(), stmExcelVO, errorMessages, STMExcelColumnEnum.PLAN_STATE.getColumnName());
			}
			
			if (stmExcelVO.isNotValid()) {
				isValid = Boolean.FALSE;
				
				if (StringUtils.isNotBlank(stmExcelVO.getHiosId()) && StringUtils.isNotBlank(stmExcelVO.getPlanNumber())) {
					stmExcelVO.setErrorMessages(errorMessages.toString() + "Skipping to persist STM Issuer["+ stmExcelVO.getHiosId() +"] plan["+ stmExcelVO.getPlanNumber() +"] due to validations error.");
				}
				else {
					stmExcelVO.setErrorMessages(errorMessages.toString() + "Skipping to persist STM plan due to validations error.");
				}
				stmExcelVO.setErrorMessages(stmExcelVO.getErrorMessages() + "\n=========================================================");
				failedPlanList.add(stmExcelVO);
			}
		}
		finally {
			LOGGER.debug("validateSTMExcelVO() End");
		}
		return isValid;
	}

	/**
	 * Method is used to remove duplicate States from STMExcelVO.getPlanState().
	 */
	public String[] removeDuplicateStates(String[] planStateArray) {
		
		String[] uniqueStateArray = null;
		
		if (null != planStateArray && 0 < planStateArray.length) {
			
			Set<String> stateList = new HashSet<String>();
			
			for (String value : planStateArray) {
				stateList.add(value.trim());
		    }
			uniqueStateArray = stateList.toArray(new String[]{});
			LOGGER.info("uniqueStateArray size: " + uniqueStateArray.length);
		}
		return uniqueStateArray;
	}

	/**
	 * Method is used to validate URL and ECM Id.
	 */
	private void validateURL(String cellValue, STMExcelVO stmExcelVO,
			StringBuilder errorMessages, String cellHeader) {
		String url = StringUtils.trim(cellValue);
		if (StringUtils.isNotBlank(url) &&
				!(isValidPattern(PATTERN_URL, url) || isValidPattern(PATTERN_ECM, url))) {
			
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_INVALID_URL);
			
			if (!stmExcelVO.isNotValid()) {
				stmExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Blank Data from Cell Value.
	 */
	private void validateBlankData(String cellValue, STMExcelVO stmExcelVO, StringBuilder errorMessages, String cellHeader) {
		
		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!stmExcelVO.isNotValid()) {
				stmExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Number value from Cell Value.
	 */
	private void validateNumber(String cellValue, STMExcelVO stmExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (!validateNumeric(cellValue)) {
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_NUMERIC);
			
			if (!stmExcelVO.isNotValid()) {
				stmExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Percentage value from Cell Value.
	 */
	private void validatePercentage(String cellValue, STMExcelVO stmExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue)) {
			
			String percValue = cellValue.replace(SerffConstants.PERCENTAGE, StringUtils.EMPTY);
			boolean percError = Boolean.FALSE;
			boolean isNumeric = validateNumeric(percValue);
			
			if (isNumeric && (Double.valueOf(percValue) < 0d || Double.valueOf(percValue) > HUNDRED)) {
				errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_PERCENTAGE);
				percError = Boolean.TRUE;
			}
			else if (!isNumeric) {
				errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_NUMERIC);
				percError = Boolean.TRUE;
			}
			
			if (percError && !stmExcelVO.isNotValid()) {
				stmExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Dollar value from Cell Value.
	 */
	private void validateDollarValue(String cellValue, STMExcelVO stmExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue)) {
			String dollarValue = cellValue.replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY);
			validateNumber(dollarValue, stmExcelVO, errorMessages, cellHeader);
		}
	}

	/**
	 * Method is used to validate whole number from Cell Value.
	 */
	private void validateWholeValue(String cellValue, STMExcelVO stmExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isBlank(cellValue)) {
			return;
		}
		
		String value = cellValue.replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY);
		value = value.replace(SerffConstants.PERCENTAGE, StringUtils.EMPTY);
		
		if (validateNumeric(value)) {
			
			BigDecimal decValue = new BigDecimal(value);
			
			if (!(0 == decValue.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO))) {
				errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_WHOLE_NUMBER);
				
				if (!stmExcelVO.isNotValid()) {
					stmExcelVO.setNotValid(Boolean.TRUE);
				}
			}
		}
	}

	/**
	 * Method is used to validate Dollar/Percentage value from Cell Value.
	 */
	private void validateDollarAndPercValue(String cellValue, STMExcelVO stmExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue)) {
			
			if (cellValue.startsWith(SerffConstants.DOLLARSIGN)) {
				validateDollarValue(cellValue, stmExcelVO, errorMessages, cellHeader);
			}
			else if (cellValue.endsWith(SerffConstants.PERCENTAGE)) {
				validatePercentage(cellValue, stmExcelVO, errorMessages, cellHeader);
			}
			else {
				validateNumber(cellValue, stmExcelVO, errorMessages, cellHeader);
			}
		}
	}

	/**
	 * Method is used to validate Allowed value from Cell Value.
	 */
	private void validateAgainstAllowedValue(String cellValue, String allowedValue, String errorMsg, STMExcelVO stmExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue)) {
			
			List<String> list = Arrays.asList(allowedValue.split(SerffConstants.COMMA));
			
			if (!list.contains(cellValue.toLowerCase())) {
				errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_INVALID_VALUE + errorMsg);
				
				if (!stmExcelVO.isNotValid()) {
					stmExcelVO.setNotValid(Boolean.TRUE);
				}
			}
		}
	}

	/**
	 * Method is used to validate State List from Cell Value.
	 */
	private void validateStateList(String cellValue, STMExcelVO stmExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue)) {
			
			String planStateArray[] = removeDuplicateStates(stmExcelVO.getPlanState().split(SerffConstants.COMMA));
			
			if (null == planStateArray || 0 == planStateArray.length) {
				return;
			}
			List<String> invalidStateList = null;
			
			for (String planState : planStateArray) {
				
				try {
					USStateCodeSimpleType.valueOf(planState);
				}
				catch (Exception ex) {
					
					if (null == invalidStateList) {
						invalidStateList = new ArrayList<String>();
					}
					invalidStateList.add(planState);
					LOGGER.debug(ex.getMessage(), ex);
				}
			}
			
			if (null != invalidStateList && !invalidStateList.isEmpty()) {
				LOGGER.error("Invalid State List: " + invalidStateList);
				errorMessages.append(cellHeader + "[" + invalidStateList + "]" + SerffConstants.COMMA + EMSG_INVALID_STATE);
				
				if (!stmExcelVO.isNotValid()) {
					stmExcelVO.setNotValid(Boolean.TRUE);
				}
			}
		}
	}

	/**
	 * Method is used to validate Length from Cell Value.
	 */
	private void validateLength(int length, boolean isRequire, String cellValue, STMExcelVO stmExcelVO, StringBuilder errorMessages, String cellHeader) {
		
		if (isRequire && StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!stmExcelVO.isNotValid()) {
				stmExcelVO.setNotValid(Boolean.TRUE);
			}
		}
		else if (StringUtils.isNotBlank(cellValue) && !isValidLength(length, cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_LENGTH + length + NEW_LINE);
			
			if (!stmExcelVO.isNotValid()) {
				stmExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}
	
}
