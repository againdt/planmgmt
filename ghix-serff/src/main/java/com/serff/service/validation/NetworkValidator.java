/**
 *
 */
package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_EMPTY_INVALID;
import static com.serff.service.validation.IFailedValidation.EMSG_LENGTH;
import static com.serff.service.validation.IFailedValidation.EMSG_NETWORK_ID;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC;
import static com.serff.util.SerffConstants.HIOS_ISSUER_ID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.serff.template.networkProv.extension.PayloadType;
import com.serff.template.networkProv.hix.core.OrganizationType;
import com.serff.template.networkProv.hix.pm.HealthcareProviderNetworkType;
import com.serff.template.networkProv.niem.core.IdentificationType;
import com.serff.template.networkProv.usps.states.USStateCodeType;
import com.serff.util.SerffConstants;


/**
 * This class validates Network data
 * @author Nikhil Talreja
 * @since 02 April, 2013
 *
 */

@Component
public class NetworkValidator extends SerffValidator {

	private static final Logger LOGGER = Logger
			.getLogger(NetworkValidator.class);

	private static final int NETWORK_ID_NUM_LEN = 3;
	private static final String TEMPLATE_NAME="NETWORK_PROV_DATA_TEMPLATE. ";
	/**
	 * This method validates Network Provider template
	 * @author Nikhil Talreja
	 * @since 02 April, 2013
	 * @param PayloadType
	 *            network,List<FailedValidation> validationErrors
	 * @return void
	 */
	public Map<String, List<String>> validateNetwork(PayloadType network,
			List<FailedValidation> validationErrors) {

		LOGGER.info("Validating Network Provider fields");

		Map<String, List<String>> templateData = null;
		List<String> networks = null;
		String issuerId = null;

		if (network != null && network.getIssuer() != null) {

			validateState(network.getIssuer().getIssuerStateCode(),validationErrors);

			issuerId = validateHiosIssuerId(network.getIssuer().getIssuerIdentification(),validationErrors);

			validateNetworkNameAndUrl(network.getOrganization(),validationErrors);

			String stateCode = null;

			if (null != network.getIssuer()
					&& null != network.getIssuer().getIssuerStateCode()
					&& null != network.getIssuer().getIssuerStateCode().getValue()){
				stateCode = network.getIssuer().getIssuerStateCode().getValue().value();
			}

			networks = validateNetworkId(stateCode,network.getIssuer().getHealthcareProviderNetwork(),validationErrors);
		}
		else{
			validationErrors.add(getFailedValidation("Network/Issuer", "", EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
		}

		templateData = new HashMap<String, List<String>>();
		List<String> issuerIdList = new ArrayList<String>();
		issuerIdList.add(issuerId);
		templateData.put(HIOS_ISSUER_ID, issuerIdList);
		templateData.put(SerffConstants.NETWORKS, networks);
		return templateData;
	}

	/**
	 * This method validates the Network IDs for a network
	 * @author Nikhil Talreja
	 * @param validationErrors
	 * @since April 02, 2013
	 * @param IdentificationType identificationType, List<FailedValidation> validationErrors
	 * @return void
	 */
	private List<String> validateNetworkId(
			String stateCode,
			List<HealthcareProviderNetworkType> healthcareProviderNetworks,
			List<FailedValidation> validationErrors) {

		List<String> networks = new ArrayList<String>();
		String networkId =null;
		if (!CollectionUtils.isEmpty(healthcareProviderNetworks)) {
			for (HealthcareProviderNetworkType healthcareProviderNetwork : healthcareProviderNetworks) {
				networkId = healthcareProviderNetwork.getProviderNetworkIdentification().getIdentificationID().getValue();
				networks.add(networkId);
				LOGGER.debug("Validating Network ID : "+networkId);
				if (!isValidLength(LET_NETWORK_ID, networkId)) {
					validationErrors.add(getFailedValidation("Network ID", networkId, EMSG_LENGTH
							+ LET_NETWORK_ID, ECODE_DEFAULT, TEMPLATE_NAME));

				}

				if (StringUtils.startsWithIgnoreCase(networkId, stateCode)
						&& networkId.charAt(2) == 'N') {

					final String networkNumber = StringUtils.substring(networkId,NETWORK_ID_NUM_LEN);
					if (StringUtils.isNumeric(networkNumber)) {
						continue;
					}
				}
				validationErrors
						.add(getFailedValidation("Network ID", networkId,
								EMSG_NETWORK_ID, ECODE_DEFAULT,
								TEMPLATE_NAME));
			}
		}
		else {
			LOGGER.debug("Validating Network ID : "+networkId);
			validationErrors
					.add(getFailedValidation(
							"Network Id",
							"",
							EMSG_DEFAULT,
							ECODE_DEFAULT,
							TEMPLATE_NAME));
		}
		return networks;
	}

	/**
	 * This method validates the Network Name and URL
	 * @author Nikhil Talreja
	 * @param validationErrors
	 * @since April 02, 2013
	 * @param IdentificationType identificationType, List<FailedValidation> validationErrors
	 * @return void
	 */
	private void validateNetworkNameAndUrl(List<OrganizationType> organizations, List<FailedValidation> validationErrors) {
		String providerName= null;
		if(!CollectionUtils.isEmpty(organizations)){
			for(OrganizationType organization : organizations){
				providerName = organization.getOrganizationAugmentation().getOrganizationMarketingName().getValue();
				LOGGER.debug("Validating Network name and URL : "+providerName);
				if(StringUtils.isBlank(providerName)){
					validationErrors
					.add(getFailedValidation(
							"Network Name",
							"",
							EMSG_DEFAULT,
							ECODE_DEFAULT,
							TEMPLATE_NAME));
				}
				organization.getOrganizationPrimaryContactInformation().getContactWebsiteURI().getValue();
			}
		}
		else {
			LOGGER.debug("Validating Network name and URL : "+providerName);
			validationErrors
					.add(getFailedValidation(
							"Payload/Organization[]",
							"",
							EMSG_DEFAULT,
							ECODE_DEFAULT,
							TEMPLATE_NAME));
		}

	}

	/**
	 * This method validates the Issuer Id for an issuer
	 * @author Nikhil Talreja
	 * @since April 02, 2013
	 * @param IdentificationType identificationType, List<FailedValidation> validationErrors
	 * @return void
	 */
	private String validateHiosIssuerId(IdentificationType issuerIdentification,
			List<FailedValidation> validationErrors) {

		String hiosIssuerId = null;
		String fieldName = "HIOS Issuer ID";

		if (issuerIdentification != null
				&& issuerIdentification.getIdentificationID() != null) {

			hiosIssuerId = issuerIdentification.getIdentificationID().getValue();
			LOGGER.debug("Validation Header 2: HIOS Issuer ID : "+	hiosIssuerId );
			if (!StringUtils.isNumeric(hiosIssuerId)) {
				validationErrors.add(getFailedValidation(fieldName, hiosIssuerId, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
				hiosIssuerId = null;
			}
			else if (!isValidLength(LEN_HIOS_ISSUER_ID, hiosIssuerId)) {
				validationErrors.add(getFailedValidation(fieldName, hiosIssuerId, EMSG_LENGTH + LEN_HIOS_ISSUER_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				hiosIssuerId = null;
			}
		}
		else {
			LOGGER.debug("Validation Header 2: HIOS Issuer ID : "+	hiosIssuerId );
			validationErrors.add(getFailedValidation(fieldName, "", EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		return hiosIssuerId;
	}

	/**
	 * This method validates the state for an issuer
	 * @author Nikhil Talreja
	 * @since April 02, 2013
	 * @param USStateCodeType stateCodeType, List<FailedValidation> validationErrors
	 * @return void
	 */
	private void validateState(USStateCodeType stateCodeType, List<FailedValidation> validationErrors){

		String stateCode = null;
		if(stateCodeType != null && stateCodeType.getValue() != null){
			stateCode = stateCodeType.getValue().value();
			LOGGER.debug("Validation Header 1: Issuer State : "+stateCode);
			if (!isValidLength(LEN_ISSUER_STATE, stateCode)) {
				validationErrors.add(getFailedValidation("Issuer State", stateCode, EMSG_LENGTH
						+ LEN_HIOS_ISSUER_ID, ECODE_DEFAULT, TEMPLATE_NAME));

			}
		}
		else{
			LOGGER.debug("Validation Header 1: Issuer State : "+stateCode);
			validationErrors.add(getFailedValidation("Issuer State", "", EMSG_EMPTY_INVALID
					, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

}
