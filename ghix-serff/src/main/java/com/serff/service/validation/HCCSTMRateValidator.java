package com.serff.service.validation;

import static com.serff.util.SerffConstants.NEW_LINE;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.getinsured.serffadapter.hcc.HCCRateExcelVO;
import com.getinsured.serffadapter.hcc.HCCRatesExcelColumn.HCCRatesExcelColumnEnum;
import com.serff.util.SerffConstants;

/**
 * @author kothari_r
 *
 */
@Component
public class HCCSTMRateValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(HCCSTMRateValidator.class);
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private static final String EMSG_EMPTY = " Value should not be empty." + NEW_LINE;
	private static final String EMSG_LENGTH = " Value must have length ";
	private static final String EMSG_NUMERIC = " Value must be numeric." + NEW_LINE;
	private static final String EMSG_WHOLE_NUMBER = " Value must have whole number." + NEW_LINE;
	private static final int MAX_CHILD_AGE = 18;
	//private static final String EMSG_INVALID_DATE = " Invalid Dates Start date must be less then end date." + NEW_LINE;

/*	
	private static final String EMSG_PERCENTAGE = " Percentage(%) Value must be between 0 to 100." + NEW_LINE;
	private static final String EMSG_INVALID_VALUE = " Invalid value.";
	private static final String EMSG_INVALID_STATE = " Invalid State." + NEW_LINE;
	private static final String EMSG_OPTION = " Correct value should be YES or NO or Y or N." + NEW_LINE;
	private static final String EMSG_UNIT = " Correct value should be months." + NEW_LINE;
	private static final String EMSG_INVALID_URL = " Invalid URL/ECM-ID." + NEW_LINE;
	private static final String OPTION_VALUE = "yes,no,y,n";
	private static final String UNIT_VALUE = "months";
*/	/**
	 * Method is used to validate require data from HCC Rate Row object before persistence.
	 */
	public boolean validateHCCRate(HCCRateExcelVO rateVO, List<HCCRateExcelVO> failedRateList) {
		
		LOGGER.debug("validateHCCRate() Start");
		boolean isValid = Boolean.TRUE;
		boolean adultAgeRequired = true;
		boolean childAgeRequired = false;
		int minAge, maxAge;
		try {
			StringBuilder errorMessages = new StringBuilder();
			validateLength(LEN_HIOS_ISSUER_ID, Boolean.TRUE, rateVO.getHiosId(), rateVO,  errorMessages, HCCRatesExcelColumnEnum.CARRIER_HIOS_ID.getColumnName());
			
			if (!rateVO.isNotValid()) {
				validateLength(LEN_STM_PLAN_ID, Boolean.TRUE, rateVO.getPlanNumber(), rateVO, errorMessages, HCCRatesExcelColumnEnum.PLAN_ID.getColumnName());
				validateLength(LEN_ISSUER_STATE, Boolean.TRUE, rateVO.getPlanState(), rateVO, errorMessages, HCCRatesExcelColumnEnum.STATE.getColumnName());
				validateNumber(rateVO.getDeductible(), rateVO, errorMessages, HCCRatesExcelColumnEnum.DEDUCTIBLE.getColumnName());
				validateWholeValue(rateVO.getMinAge(), rateVO, errorMessages, HCCRatesExcelColumnEnum.MIN_AGE.getColumnName(), true);
				validateWholeValue(rateVO.getMaxAge(), rateVO, errorMessages, HCCRatesExcelColumnEnum.MAX_AGE.getColumnName(), true);
				if(!rateVO.isNotValid()) {
					minAge = Integer.parseInt(rateVO.getMinAge());
					maxAge = Integer.parseInt(rateVO.getMaxAge());
					if(minAge > maxAge) {
						rateVO.setNotValid(Boolean.TRUE);
					} else {
						adultAgeRequired = MAX_CHILD_AGE < maxAge ;
						childAgeRequired = MAX_CHILD_AGE > minAge;
						validateDollarValue(rateVO.getMaleRate(), rateVO, errorMessages, HCCRatesExcelColumnEnum.MALE.getColumnName(), adultAgeRequired);
						validateDollarValue(rateVO.getFemaleRate(), rateVO, errorMessages, HCCRatesExcelColumnEnum.FEMALE.getColumnName(), adultAgeRequired);
						validateDollarValue(rateVO.getChildRate(), rateVO, errorMessages, HCCRatesExcelColumnEnum.CHILD.getColumnName(), childAgeRequired);
						validateDate(rateVO.getEffectiveStartDate(), rateVO, errorMessages, HCCRatesExcelColumnEnum.EFFECTIVE_START_DATE.getColumnName(), Boolean.TRUE);
					}
				}
			}
			
			if (rateVO.isNotValid()) {
				isValid = Boolean.FALSE;
				failedRateList.add(rateVO);
			}
		}
		finally {
			LOGGER.debug("validateHCCRate() End");
		}
		return isValid;
	}


	/**
	 * Method is used to validate Blank Data from Cell Value.
	 */
/*	private void validateBlankData(String cellValue, STMExcelVO stmExcelVO, StringBuilder errorMessages, String cellHeader) {
		
		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!stmExcelVO.isNotValid()) {
				stmExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}
*/
	/**
	 * Method is used to validate Number value from Cell Value.
	 */
	private void validateNumber(String cellValue, HCCRateExcelVO hccRateExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (!validateNumeric(cellValue)) {
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_NUMERIC);
			
			if (!hccRateExcelVO.isNotValid()) {
				hccRateExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}


	/**
	 * Method is used to validate Dollar value from Cell Value.
	 */
	private void validateDollarValue(String cellValue, HCCRateExcelVO hccRateExcelVO, StringBuilder errorMessages, String cellHeader, boolean isRequire) {

		if (isRequire && StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!hccRateExcelVO.isNotValid()) {
				hccRateExcelVO.setNotValid(Boolean.TRUE);
			}
		}
		else if (StringUtils.isNotBlank(cellValue)) {
			String dollarValue = cellValue.replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY);
			validateNumber(dollarValue, hccRateExcelVO, errorMessages, cellHeader);
		}
	}

	/**
	 * Method is used to validate whole number from Cell Value.
	 */
	private void validateWholeValue(String cellValue, HCCRateExcelVO hccRateExcelVO, StringBuilder errorMessages, String cellHeader, boolean isRequire) {

		if (isRequire && StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!hccRateExcelVO.isNotValid()) {
				hccRateExcelVO.setNotValid(Boolean.TRUE);
			}
		}
		else if (StringUtils.isNotBlank(cellValue) && validateNumeric(cellValue)) {
				
			BigDecimal decValue = new BigDecimal(cellValue);
			
			if (!(0 == decValue.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO))) {
				errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_WHOLE_NUMBER);
				
				if (!hccRateExcelVO.isNotValid()) {
					hccRateExcelVO.setNotValid(Boolean.TRUE);
				}
			}
		}
	}


	/**
	 * Method is used to validate Length from Cell Value.
	 */
	private void validateLength(int length, boolean isRequire, String cellValue, HCCRateExcelVO hccRateExcelVO, StringBuilder errorMessages, String cellHeader) {
		
		if (isRequire && StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!hccRateExcelVO.isNotValid()) {
				hccRateExcelVO.setNotValid(Boolean.TRUE);
			}
		}
		else if (StringUtils.isNotBlank(cellValue) && !isValidLength(length, cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_LENGTH + length + NEW_LINE);
			
			if (!hccRateExcelVO.isNotValid()) {
				hccRateExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}
	
	private void validateDate(String cellValue, HCCRateExcelVO hccRateExcelVO, StringBuilder errorMessages, String cellHeader, boolean isRequire) {
		try {
			if (isRequire && StringUtils.isBlank(cellValue)) {
				errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
				
				if (!hccRateExcelVO.isNotValid()) {
					hccRateExcelVO.setNotValid(Boolean.TRUE);
				}
			}
			else if (!StringUtils.isEmpty(cellValue)) {
				dateFormat.parse(cellValue);
			}
		} catch (ParseException e) {
			errorMessages.append("Invalid Date ");
			if (!hccRateExcelVO.isNotValid()) {
				hccRateExcelVO.setNotValid(Boolean.TRUE);
			}
			LOGGER.error("Exception occured while parsing dates", e);
		}
	}
	
}
