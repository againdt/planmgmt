package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_DEFAULT;
import static com.serff.util.SerffConstants.HIOS_ISSUER_ID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.serff.template.rbrules.extension.PayloadType;

@Component
public class BusinessRuleValidator extends SerffValidator{
	
	private static final Logger LOGGER = Logger.getLogger(BusinessRuleValidator.class);
	private static final String TEMPLATE_NAME="BUSINESS_RULE_DATA_TEMPLATE. ";

	public Map<String, List<String>> validateBusinessRule(PayloadType businessRule,
			List<FailedValidation> validationErrors) {
		Map<String, List<String>> templateData = null;
		String issuerId = null;
		
		
		if (businessRule != null && businessRule.getIssuer() != null) {
			
			if(null!= businessRule.getIssuer().getIssuerIdentification() && null!= businessRule.getIssuer().getIssuerIdentification().getIdentificationID()){
				issuerId = businessRule.getIssuer().getIssuerIdentification().getIdentificationID().getValue();
				LOGGER.info("Business rule hios id : " +issuerId );
			}
	
		}else{
			validationErrors.add(getFailedValidation("BusinessRule/Issuer", "", EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		templateData = new HashMap<String, List<String>>();
		List<String> issuerIdList = new ArrayList<String>();
		issuerIdList.add(issuerId);
		templateData.put(HIOS_ISSUER_ID, issuerIdList);
		
		return templateData;
	}
	
}
