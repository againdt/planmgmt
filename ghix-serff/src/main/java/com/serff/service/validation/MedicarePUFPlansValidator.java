package com.serff.service.validation;

import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.CONTRACT_ID;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.COUNTY;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.MEDICARE_TYPE;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.MONTHLY_CONSOLIDATED_PREMIUM;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.ORGANIZATION_NAME;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.PLAN_ID;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.PLAN_NAME;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.SEGMENT_ID;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.STATE;
import static com.serff.util.SerffConstants.NEW_LINE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.serffadapter.puf.medicare.MedicareVO;
import com.serff.util.SerffConstants;

/**
 * Class is used to validate Medicare-PUF Plans Excel.
 * 
 * @author Bhavin Parmar
 * @since Apr 20, 2015
 */
@Component
public class MedicarePUFPlansValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(MedicarePUFPlansValidator.class);

	private static final String EMSG_EMPTY = " Value should not be empty." + NEW_LINE;

	/**
	 * Method is used to validate require data from of Medicare-PUF plans before persistence.
	 */
	public List<MedicareVO> validateMedicarePlanList(Map<String, Map<String, MedicareVO>> medicareIssuerPlansMap, String medicarePlanType) {

		LOGGER.info("validateMedicarePlanList() Start");
		List<MedicareVO> failedMedicareList = new ArrayList<>();

		try {
			boolean firstRow, isValidMedicarePlan, isValidMedicareIssuer;
			Entry<String, MedicareVO> mapEntry;
			Map<String, MedicareVO> medicarePlansMap;
			MedicareVO firstMedicareIssuerVO = null;
			MedicareVO medicareVO;
			Iterator<Entry<String, Map<String, MedicareVO>>> mapEntries = medicareIssuerPlansMap.entrySet().iterator();

			while (mapEntries.hasNext()) {
				mapEntry = (Entry) mapEntries.next();
				LOGGER.debug("MainMap[HIOS Issuer ID]: " + mapEntry.getKey());
				medicarePlansMap = (Map<String, MedicareVO>) mapEntry.getValue();
				firstMedicareIssuerVO = null;
				firstRow = true;
				isValidMedicareIssuer = true;
				isValidMedicarePlan = false;

				if (CollectionUtils.isEmpty(medicarePlansMap)) {
					continue;
				}

				for (Map.Entry<String, MedicareVO> entryData : medicarePlansMap.entrySet()) {
					LOGGER.debug("ChildMap[IssuerPlanNumber]: " + entryData.getKey());
					medicareVO = entryData.getValue();

					if (null == medicareVO) {
						continue;
					}

					if (firstRow) {
						firstRow = false;
						firstMedicareIssuerVO = medicareVO;
					}
					isValidMedicarePlan = validateMedicarePlans(medicareVO, medicarePlanType);
					isValidMedicareIssuer = isValidMedicareIssuer && isValidMedicarePlan;

					if (!isValidMedicarePlan) {
						failedMedicareList.add(medicareVO);
					}
				}
				// Used while skipping Issuer.
				if (null != firstMedicareIssuerVO && !isValidMedicareIssuer) {
					firstMedicareIssuerVO.setNotValid(true);
				}
			}
		}
		finally {
			LOGGER.info("validateMedicarePlanList() End");
		}
		return failedMedicareList;
	}

	private boolean validateMedicarePlans(MedicareVO medicareVO, String medicarePlanType) {

		LOGGER.debug("validateMedicarePlans() Start");
		boolean isValid = false;
		StringBuilder errorMessages = new StringBuilder();

		try {
			validateBlankData(medicareVO.getStateCode(), medicareVO, errorMessages, STATE.getColumnName(medicarePlanType));
			validateBlankData(medicareVO.getContractID(), medicareVO, errorMessages, CONTRACT_ID.getColumnName(medicarePlanType));
			validateBlankData(medicareVO.getPlanID(), medicareVO, errorMessages, PLAN_ID.getColumnName(medicarePlanType));
			validateBlankData(medicareVO.getSegmentID(), medicareVO, errorMessages, SEGMENT_ID.getColumnName(medicarePlanType));
			validateBlankData(medicareVO.getOrganizationName(), medicareVO, errorMessages, ORGANIZATION_NAME.getColumnName(medicarePlanType));
			validateBlankData(medicareVO.getPlanName(), medicareVO, errorMessages, PLAN_NAME.getColumnName(medicarePlanType));
			validateBlankData(medicareVO.getMonthlyPremium(), medicareVO, errorMessages, MONTHLY_CONSOLIDATED_PREMIUM.getColumnName(medicarePlanType));

			if (MedicareVO.InsuranceType.MA.equals(medicareVO.getInsuranceType())
					|| MedicareVO.InsuranceType.SNP.equals(medicareVO.getInsuranceType())) {
				validateBlankData(medicareVO.getMedicareType(), medicareVO, errorMessages, MEDICARE_TYPE.getColumnName(medicarePlanType));

				if (CollectionUtils.isEmpty(medicareVO.getCountyList())) {
					errorMessages.append(COUNTY.getColumnName(medicarePlanType) + SerffConstants.COMMA + EMSG_EMPTY);

					if (!medicareVO.isNotValid()) {
						medicareVO.setNotValid(true);
					}
				}
				/*if (MedicareVO.InsuranceType.SNP.equals(medicareVO.getInsuranceType())) {
					
				}*/
			}
			/*else if (MedicareVO.InsuranceType.RX.equals(medicareVO.getInsuranceType())) {
				
			}*/

			if (medicareVO.isNotValid()) {

				if (StringUtils.isNotBlank(medicareVO.getIssuerPlanNumber())) {
					medicareVO.setErrorMessages(errorMessages.toString() + "Skipping to persist Medicaid Plans ["
							+ medicareVO.getIssuerPlanNumber() + "] due to validations error.");
				}
				medicareVO.setErrorMessages(medicareVO.getErrorMessages() + "\n=========================================================");
				LOGGER.error(medicareVO.getErrorMessages());
			}
			else {
				isValid = true;
			}
		}
		finally {
			LOGGER.debug("validateMedicarePlans() End");
		}
		return isValid;
	}

	/**
	 * Method is used to validate Blank Data from Cell Value.
	 */
	private void validateBlankData(String cellValue, MedicareVO medicareVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);

			if (!medicareVO.isNotValid()) {
				medicareVO.setNotValid(true);
			}
		}
	}
}
