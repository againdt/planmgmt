/**
 *
 */
package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_EMPTY_INVALID;
import static com.serff.service.validation.IFailedValidation.EMSG_LENGTH;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC;
import static com.serff.service.validation.IFailedValidation.EMSG_SERVICE_AREA_ID;
import static com.serff.service.validation.IFailedValidation.EMSG_SHOULD_NOT_EMPTY;
import static com.serff.util.SerffConstants.HIOS_ISSUER_ID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.serff.template.svcarea.extension.PayloadType;
import com.serff.template.svcarea.hix.pm.GeographicAreaType;
import com.serff.template.svcarea.hix.pm.ServiceAreaType;
import com.serff.template.svcarea.niem.core.IdentificationType;
import com.serff.template.svcarea.niem.usps.states.USStateCodeType;
import com.serff.util.SerffConstants;

/**
 * This class validates Service Area data
 * @author Nikhil Talreja
 * @since 22 March, 2013
 *
 */
@Component
public class ServiceAreaValidator extends SerffValidator {

	private static final Logger LOGGER = Logger
			.getLogger(ServiceAreaValidator.class);

	private static final String TEMPLATE_NAME="SERVICE_AREA_DATA_TEMPLATE. ";

	/**
	 * This method validates Service data template
	 * This method has been further modularized to validate different sections
	 * @author Nikhil Talreja
	 * @since Mar 29, 2013
	 * @param PayloadType
	 *            serviceArea,List<FailedValidation> validationErrors
	 * @return Map<String, String>
	 */
	public Map<String, List<String>> validateServiceArea(PayloadType serviceArea,
			List<FailedValidation> validationErrors) {

		LOGGER.info("Validating Service Area fields");
		HashMap<String, List<String>> templateData = null;
		List<String> serviceAreas = null;
		String issuerId = null;

		if (null != serviceArea && null != serviceArea.getIssuer()
				&& null != serviceArea.getIssuer().getIssuerStateCode()) {

			validateState(serviceArea.getIssuer().getIssuerStateCode(),validationErrors);

			issuerId = validateHiosIssuerId(serviceArea.getIssuer().getIssuerIdentification(),validationErrors);

			if (null != serviceArea.getIssuer().getIssuerStateCode().getValue()) {

				String stateCode = serviceArea.getIssuer().getIssuerStateCode().getValue().value();
				
				serviceAreas = validateServiceAreaIds(serviceArea.getIssuer().getServiceArea(), stateCode,validationErrors);
			}

			validateGeographicDetails(serviceArea.getIssuer().getServiceArea(),validationErrors);
		}
		else {
			validationErrors.add(getFailedValidation("Issuer", "", EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
		}

		templateData = new HashMap<String, List<String>>();
		List<String> issuerIdList = new ArrayList<String>();
		issuerIdList.add(issuerId);
		templateData.put(HIOS_ISSUER_ID, issuerIdList);
		templateData.put(SerffConstants.SERVICE_AREAS, serviceAreas);
		return templateData;
	}

	/**
	 * This method validates if postal codes are present for all partial counties included
	 * in a service area
	 * @author Nikhil Talreja
	 * @since Mar 29, 2013
	 * @param GeographicAreaType geographicArea, List<FailedValidation> validationErrors
	 * @return void
	 */
	private void validatePartialCounties(GeographicAreaType geographicArea, List<FailedValidation> validationErrors){

		List<com.serff.template.svcarea.niem.proxy.xsd.String> postalCodes = geographicArea
				.getLocationPostalCode();
		// Postal codes are compulsory if partial counties are included in the service area
		if (CollectionUtils.isEmpty(postalCodes)) {
			LOGGER.debug("Postal codes are compulsory if partial counties are included in the service area");
			validationErrors.add(getFailedValidation(LOCATION_POSTAL_CODE, EMPTY_VALUE,
					EMSG_EMPTY_INVALID, ECODE_DEFAULT,
					TEMPLATE_NAME));
			return;
		}
		for (com.serff.template.svcarea.niem.proxy.xsd.String postalCode : postalCodes) {
			String zip = postalCode.getValue();
			if (!StringUtils.isNumeric(zip)) {
				validationErrors
						.add(getFailedValidation(LOCATION_POSTAL_CODE,
								zip, EMSG_NUMERIC,
								ECODE_DEFAULT,
								TEMPLATE_NAME));
			} else if (!isValidLength(LEN_ZIP_CODE_1 , zip)

					&& !isValidLength(LEN_ZIP_CODE_2 , zip)) {

				validationErrors.add(getFailedValidation("Zip Code", zip, EMSG_LENGTH
						+ LEN_ZIP_CODE_1 + " or " + LEN_ZIP_CODE_2 , ECODE_DEFAULT,
						TEMPLATE_NAME));
			}
		}
}


	/**
	 * This method validates the geographic details of a service area
	 * @author Nikhil Talreja
	 * @since Mar 25, 2013
	 * @param List<ServiceAreaType> serviceAreas, List<FailedValidation> validationErrors
	 * @return void
	 */
	private void validateGeographicDetails(
			List<ServiceAreaType> serviceAreas, List<FailedValidation> validationErrors) {

		for (ServiceAreaType area : serviceAreas) {
			for (GeographicAreaType geographicArea : area.getGeographicArea()) {

				// Service area does not include entire state
				if (null != geographicArea.getGeographicAreaEntireStateIndicator() && !geographicArea.getGeographicAreaEntireStateIndicator()
						.isValue()) {

					LOGGER.debug("Service area "
							+ area.getServiceAreaIdentification()
									.getIdentificationID().getValue()
							+ " does not cover entire state");

					//commented out geographicArea country code validation, due to new xsd changes, later on need to add validation.

					/*com.serff.template.svcarea.niem.proxy.xsd.String countyCode = geographicArea
							.getGeographicAreaCountyCode();
					// County code is compulsory if service area does not cover
					// entire state
					if (countyCode == null
							|| StringUtils.isBlank(countyCode.getValue())) {
						LOGGER.info("County code is compulsory if service area does not cover entire state");
						validationErrors.add(getFailedValidation(COUNTY_NAME, EMPTY_VALUE,
								EMSG_EMPTY_INVALID, ECODE_DEFAULT,
								templateName));
					}*/
					// Check if any partial counties are contained in the
					// service area
					if (null != geographicArea
							.getGeographicAreaPartialCountyIndicator() && geographicArea
							.getGeographicAreaPartialCountyIndicator()
							.isValue()) {
						// Service area contains partial counties
						validatePartialCounties(geographicArea, validationErrors);

					}
				}
			}
		}
	}

	/**
	 * This method validates the Service Ids for an issuer
	 * @author Nikhil Talreja
	 * @since Mar 25, 2013
	 * @param List<ServiceAreaType> serviceAreas,
			String stateCode, List<FailedValidation> validationErrors
	 * @return void
	 */
	private List<String> validateServiceAreaIds(List<ServiceAreaType> serviceAreas,
			String stateCode, List<FailedValidation> validationErrors) {

		List<String> availableServiceAreas = new ArrayList<String>();

		for (ServiceAreaType area : serviceAreas) {

			if (null == area) {
				continue;
			}

			if (null != area.getServiceAreaIdentification()
					&& null != area.getServiceAreaIdentification().getIdentificationID()
					&& StringUtils.isNotEmpty(area.getServiceAreaIdentification().getIdentificationID().getValue())){
				availableServiceAreas.add(area.getServiceAreaIdentification().getIdentificationID().getValue());
			}

			validateServiceAreaID(stateCode, area.getServiceAreaIdentification(), validationErrors);

			String serviceAreaName = area.getServiceAreaName().getValue();

			if (StringUtils.isBlank(serviceAreaName)) {
				validationErrors.add(getFailedValidation("Service Area Name", EMPTY_VALUE, EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		return availableServiceAreas;
	}

	/**
	 * Method is used Service Area ID.
	 * Format = State Code + S + 001
	 *
	 * @param stateCode, String object State Code
	 * @param identificationType, Object of IdentificationType
	 * @param errors, List Object of FailedValidation
	 */
	private void validateServiceAreaID(String stateCode,
			IdentificationType identificationType, List<FailedValidation> errors) {

		String fieldName = "Service Area ID";
		String value = null;

		if (null != identificationType
				&& null != identificationType.getIdentificationID()
				&& StringUtils.isNotEmpty(identificationType.getIdentificationID().getValue())) {

			value = identificationType.getIdentificationID().getValue();
			LOGGER.debug("Validation for Service Area IDs : "+value);
			if (!isValidLength(LET_SERVICE_AREA_ID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LET_SERVICE_AREA_ID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else {

				if (!isValidPattern(PATTERN_SERVICE_AREA_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_SERVICE_AREA_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (!value.substring(0, LEN_ISSUER_STATE).equals(stateCode)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_SERVICE_AREA_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
		}
		else {
			LOGGER.debug("Validation for Service Area IDs : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * This method validates the Issuer Id for an issuer
	 * @author Nikhil Talreja
	 * @since Mar 25, 2013
	 * @param IdentificationType identificationType, List<FailedValidation> validationErrors
	 * @return void
	 */
	private String validateHiosIssuerId(IdentificationType identificationType,
			List<FailedValidation> validationErrors) {

		String hiosIssuerId = null;
		String fieldName = "HIOS Issuer ID";

		if (identificationType != null
				&& identificationType.getIdentificationID() != null) {

			hiosIssuerId = identificationType.getIdentificationID().getValue();
			LOGGER.debug("Validation Header 2: HIOS Issuer ID : "+hiosIssuerId);
			if (StringUtils.isBlank(hiosIssuerId)) {
				validationErrors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
				hiosIssuerId = null;
			}
			else if (!StringUtils.isNumeric(hiosIssuerId)) {
				validationErrors.add(getFailedValidation(fieldName, hiosIssuerId, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
				hiosIssuerId = null;
			}
			else if (!isValidLength(LEN_HIOS_ISSUER_ID, hiosIssuerId)) {
				validationErrors.add(getFailedValidation(fieldName, hiosIssuerId, EMSG_LENGTH + LEN_HIOS_ISSUER_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				hiosIssuerId = null;
			}
		}
		else {
			LOGGER.debug("Validation Header 2: HIOS Issuer ID : "+hiosIssuerId);
			validationErrors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		return hiosIssuerId;
	}

	/**
	 * This method validates the state for an issuer
	 * @author Nikhil Talreja
	 * @since Mar 25, 2013
	 * @param USStateCodeType stateCodeType, List<FailedValidation> validationErrors
	 * @return void
	 */
	private void validateState(USStateCodeType stateCodeType, List<FailedValidation> validationErrors){

		String stateCode = null;
		if(null!=stateCodeType  && null!=stateCodeType.getValue()){
			stateCode = stateCodeType.getValue().value();
			LOGGER.debug("Validating state code : "+stateCode);
			if(StringUtils.isBlank(stateCode)){

				validationErrors.add(getFailedValidation("Issuer State", EMPTY_VALUE, EMSG_DEFAULT
						, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else if (!isValidLength(LEN_ISSUER_STATE, stateCode)) {
				validationErrors.add(getFailedValidation("Issuer State", stateCode, EMSG_LENGTH
						+ LEN_HIOS_ISSUER_ID, ECODE_DEFAULT, TEMPLATE_NAME));

			}
		}
		else{
			LOGGER.debug("Validating state code : "+stateCode);
			validationErrors.add(getFailedValidation("Issuer State", "", EMSG_DEFAULT
					, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}
}

