package com.serff.service.validation;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.serff.config.SerffConfiguration;
import com.getinsured.hix.serff.config.SerffConfiguration.SerffConfigurationEnum;
import com.serff.util.SerffConstants;

/**
 * This class is going to have dropdown validations required for Plan and
 * Benefit.
 * 
 * @author Vani Sharma
 * @since 5/15/2013
 */
@Component
public class PlanValidatorOptions {

	public boolean getOptionValue(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_BOOLEAN_VALUE);
	}

	public boolean getMarketCoverageType(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_MARKET_COVERAGE_TYPE);
	}

	public boolean getPlan(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_PLAN);
	}

	public boolean getPlanType(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_PLAN_TYPE);
	}

	public boolean getCoverageLevel(String value, boolean isDentalPlan) {

		SerffConfigurationEnum coverageLevel;

		if (isDentalPlan) {
			coverageLevel = SerffConfigurationEnum.PV_COVERAGE_LEVEL_DENTAL;
		} else {
			coverageLevel = SerffConfigurationEnum.PV_COVERAGE_LEVEL;
		}
		return SerffConfiguration.hasPropertyInList(value, coverageLevel);
	}

	public boolean getPlanHealth(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_PLAN_HEALTH);
	}

	public boolean getDiseaseManagement(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_DISEASE_MANAGEMENT);
	}

	public boolean getChildOffering(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_CHILD_OFFERING);
	}

	public boolean getRates(String value) {
		return SerffConfiguration.hasPropertyInList(value.trim(), SerffConfigurationEnum.PV_RATES);
	}

	public boolean getBenefitCoverage(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_BENEFIT_COVERAGE);
	}

	public boolean getLimitUnitCatogery1(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_LIMIT_UNIT_CATOGERY1);
	}

	public boolean getLimitUnitCatogery2(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_LIMIT_UNIT_CATOGERY2);
	}

	public boolean getListValues(String value) {
		return SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_LIST_VALUES);
	}

	public boolean getCopayCoinsurance(String value) {

		boolean flag = false;

		if (StringUtils.isNotBlank(value)) {

			flag = SerffConfiguration.hasPropertyInList(value, SerffConfigurationEnum.PV_COPAY_COINSURANCE);

			if (!flag) {

				flag = SerffConfiguration.containsPropertyValue(value, SerffConfigurationEnum.PV_COPAY_COINSURANCE_AFTER);

				if (!flag) {
					flag = SerffConfiguration.containsPropertyValue(value, SerffConfigurationEnum.PV_COPAY_COINSURANCE_BEFORE);

					if (!flag) {
						flag = SerffConfiguration.containsPropertyValue(value, SerffConfigurationEnum.PV_COPAY_PER_DAY);

						if (!flag) {
							flag = SerffConfiguration.containsPropertyValue(value, SerffConfigurationEnum.PV_COPAY_PER_STAY);

							if (!flag) {
								flag = SerffConfiguration.containsPropertyValue(value, SerffConfigurationEnum.PV_COINSURANCE_AFTER);

								if (!flag) {
									flag = isOtherCoapyStrings(value);
								}
							}
						}
					}
				}
			}
		}
		return flag;
	}
	
	private boolean isOtherCoapyStrings(String value) {
		
		boolean flag = false;
		String propertyValue = DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.PV_OTHER_COPAY_STRINGS);

		if (StringUtils.isNotBlank(propertyValue)) {

			String[] propertyList = propertyValue.split(SerffConstants.COMMA);

			if (null != propertyList && propertyList.length > 0) {

				for (String copayVal : propertyList) {
					
					if (value.toLowerCase().indexOf(copayVal) > -1) {
						flag = true;
						break;
					}
				}
			}
		}
		return flag;
	}

	public boolean getCatastrophicPlan(String value) {
		return SerffConfiguration.hasPropertyValue(value, SerffConfigurationEnum.PV_CATASTROPHIC_PLAN);
	}
	
	/*private boolean isOtherCopayCoinsurance(String value) {
		final String copayCoinsuranceAfter = "serff.PlanValidatorOptions.CopayCoinsuranceAfter";
		final String copayCoinsuranceBefore = "serff.PlanValidatorOptions.CopayCoinsuranceBefore";
		final String copayPerDay = "serff.PlanValidatorOptions.CopayPerDay";
		final String copayPerStay = "serff.PlanValidatorOptions.CopayPerStay";
		boolean flag = false;
		
		if(StringUtils.isNotBlank(combinedConfig.getString(copayCoinsuranceAfter))
				&& value.toLowerCase().indexOf(combinedConfig.getString(copayCoinsuranceAfter)) > -1) {
			flag = true;	
		}
		else if(StringUtils.isNotBlank(combinedConfig.getString(copayCoinsuranceBefore))
				&& value.toLowerCase().indexOf(combinedConfig.getString(copayCoinsuranceBefore)) > -1) {
			flag = true;
		}
		else if(StringUtils.isNotBlank(combinedConfig.getString(copayPerDay))
				&& value.toLowerCase().indexOf(combinedConfig.getString(copayPerDay)) > -1) {
			flag = true;
		}
		else if(StringUtils.isNotBlank(combinedConfig.getString(copayPerStay))
				&& value.toLowerCase().indexOf(combinedConfig.getString(copayPerStay)) > -1) {
			flag = true;
		}
		return flag;
	}*/
}
