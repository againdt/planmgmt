package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_DEFAULT;
import static com.serff.util.SerffConstants.HIOS_ISSUER_ID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.serff.template.urrt.InsuranceRateForecastTempVO;
import com.serff.template.urrt.InsuranceRateForecastTempVO.ChangedInsuranceProductRateChange;
import com.serff.template.urrt.InsuranceRateForecastTempVO.ChangedInsuranceProductRateChange.AssociatingInsurancePlanRateChange;

@Component
public class UnifiedRateValidator extends SerffValidator {

	private static final Logger LOGGER = Logger
			.getLogger(UnifiedRateValidator.class);
	private static final String TEMPLATE_NAME = "UNIFIED_RATE_DATA_TEMPLATE. ";
	private static final int START_INDEX=0;
	private static final int END_INDEX=5;

	public Map<String, List<String>> validateUnifiedRate(InsuranceRateForecastTempVO unifiedRateTemplate,
			List<FailedValidation> validationErrors) {
		Map<String, List<String>> templateData = null;
		String biPlanId = null;
		
       if (unifiedRateTemplate != null && (!CollectionUtils.isEmpty(unifiedRateTemplate.getChangedInsuranceProductRateChange())) ) {
			
    	    ChangedInsuranceProductRateChange changedInsuranceProductRateChange = unifiedRateTemplate.getChangedInsuranceProductRateChange().get(0);
    	   
    	    if(!CollectionUtils.isEmpty(changedInsuranceProductRateChange.getAssociatingInsurancePlanRateChange())){
    	    	AssociatingInsurancePlanRateChange associatingInsurancePlanRateChange = changedInsuranceProductRateChange.getAssociatingInsurancePlanRateChange().get(0);
    	    	biPlanId = associatingInsurancePlanRateChange.getBasedInsurancePlanIdentifier().getCellValue();
    	    	if(null != associatingInsurancePlanRateChange.getBasedInsurancePlanIdentifier()){
    	    		LOGGER.info("Unified Rate BI Plan Id  : " + biPlanId);
    	    	}
    	    }    	    
			
		}else{
			validationErrors.add(getFailedValidation("Unified Rate/Issuer", "", EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		templateData = new HashMap<String, List<String>>();
		List<String> issuerIdList = new ArrayList<String>();
		
		//Fixed the HP FOD Null dereference issue
		if(StringUtils.isNotBlank(biPlanId)){
		   issuerIdList.add(biPlanId.substring(START_INDEX, END_INDEX));
		}
		templateData.put(HIOS_ISSUER_ID, issuerIdList);
		
		return templateData;
		
	}

}