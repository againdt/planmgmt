package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_FORMAT;
import static com.serff.service.validation.IFailedValidation.EMSG_LENGTH;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC;
import static com.serff.service.validation.IFailedValidation.EMSG_SHOULD_NOT_EMPTY;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.getinsured.hix.serff.config.SerffConfiguration;
import com.getinsured.hix.serff.config.SerffConfiguration.SerffConfigurationEnum;
import com.serff.planmanagementexchangeapi.exchange.model.pm.UracInfo;

/**
 * Class is used to provide validation service of Issuer URAC.
 * @author Bhavin Parmar
 */
@Component
public class IssuerURACValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(IssuerURACValidator.class);
	private static final String TEMPLATE_NAME ="ISSUER_URAC_TEMPLATE. ";

	/**
	 * Method is used to validate each field of UracInfo.
	 * 
	 * @param uracInfo, Object of UracInfo
	 * @param errors, List<FailedValidation>
	 * @return FailedValidation objects in List.
	 */
	public void validateIssuerURAC(final UracInfo uracInfo, List<FailedValidation> errors) {
		
		LOGGER.info("validateIssuerURAC() Start");
		
		try {
			
			if (null == uracInfo) {
				LOGGER.warn("UracInfo complex type has been null.");
				return;
			}
			
			validateHIOSIssuerID(uracInfo.getHiosIssuerId(), errors);
			
			validateApplicationNumber(uracInfo.getApplicationNumber(), errors);
			
			validateMarketType(uracInfo.getMarketType(), errors);

			validateAccreditationStatus(uracInfo.getAccreditationStatus(), errors);
			
			validateExpirationDate(uracInfo.getExpirationDate(), errors);
		}
		finally {
			LOGGER.info("validateIssuerURAC() End");
		}
	}
	
	/**
	 * Method is used validate HIOS Issuer ID.
	 * 
	 * @param hiosIssuerId, Integer value of HIOS Issuer ID
	 * @param errors, List Object of FailedValidation
	 */
	private void validateHIOSIssuerID(Integer hiosIssuerId,
			List<FailedValidation> errors) {
		
		String fieldName = "HIOS Issuer ID";
		String xPath = "transferPlan/plan/insurer/accreditationInfo/uracInfo/hiosIssuerId";
		String value = String.valueOf(hiosIssuerId);
		
		if (null != hiosIssuerId) {
			LOGGER.debug("Validation Header: HIOS Issuer ID : "+ hiosIssuerId);
			if (!StringUtils.isNumeric(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, xPath));
			}
			else if (!isValidLength(LEN_HIOS_ISSUER_ID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_HIOS_ISSUER_ID, ECODE_DEFAULT, xPath));
			}
		}
		else {
			LOGGER.debug("Validation Header: HIOS Issuer ID is null.");
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, xPath));
		}
	}
	
	/**
	 * Method is used validate Market Type.
	 * 
	 * @param marketType, String value of Market Type
	 * @param errors, List Object of FailedValidation
	 */
	@SuppressWarnings("rawtypes")
	private void validateMarketType(String marketType,
			List<FailedValidation> errors) {
		
		String fieldName = "Market Type";
		LOGGER.debug("Validation Field: Market Type : " + marketType);
		
		if (null != marketType
				&& !SerffConfiguration.hasPropertyInList(marketType, SerffConfigurationEnum.IUV_MARKET_TYPE)) {
			errors.add(getFailedValidation(fieldName, marketType, EMSG_FORMAT, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}
	
	/**
	 * Method is used validate URAC Application Number.
	 * 
	 * @param applicationNumber, String value of URAC Application Number
	 * @param errors, List Object of FailedValidation
	 */
	private void validateApplicationNumber(String applicationNumber,
			List<FailedValidation> errors) {
		
		String fieldName = "URAC Application Number";
		String xPath = "transferPlan/plan/insurer/accreditationInfo/uracInfo/applicationNumber";
		LOGGER.debug("Validation Field: URAC Application Number : "+applicationNumber);
		if (null != applicationNumber
				&& StringUtils.isNotEmpty(applicationNumber)) {
			
			if (!isValidLength(LET_APPLICATION_NUMBER, applicationNumber)
					&& !isValidLength(LET_APPLICATION_NUMBER + 1, applicationNumber)) {
				errors.add(getFailedValidation(fieldName, applicationNumber, EMSG_LENGTH + LET_APPLICATION_NUMBER + "/" + (LET_APPLICATION_NUMBER + 1), ECODE_DEFAULT, xPath));
			}
		}
		else {
			errors.add(getFailedValidation(fieldName, applicationNumber, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, xPath));
		}
	}
	
	/**
	 * Method is used validate URAC Accreditation Status.
	 * 
	 * @param accreditationStatus, String value of Accreditation Status
	 * @param errors, List Object of FailedValidation
	 */
	private void validateAccreditationStatus(String accreditationStatus,
			List<FailedValidation> errors) {
		
		String fieldName = "Accreditation Status";
		LOGGER.debug("Validation Field: URAC Accreditation Status : "+accreditationStatus);
		
		if (null != accreditationStatus
				&& !SerffConfiguration.hasPropertyInList(accreditationStatus, SerffConfigurationEnum.IUV_ACCREDITATION_STATUS)) {
			errors.add(getFailedValidation(fieldName, accreditationStatus, EMSG_FORMAT, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		else {
			errors.add(getFailedValidation(fieldName, accreditationStatus, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}
	
	/**
	 * Method is used validate Expiration date.
	 * 
	 * @param expirationDate, Object of XMLGregorianCalendar
	 * @param errors, List Object of FailedValidation
	 */
	private void validateExpirationDate(XMLGregorianCalendar expirationDate,
			List<FailedValidation> errors) {
		
		String fieldName = "Expiration date";
		String xPath = "transferPlan/plan/insurer/accreditationInfo/uracInfo/expirationDate";
		LOGGER.debug("Validation Field: URAC Expiration date : "+expirationDate);
		if (null == expirationDate) {
			errors.add(getFailedValidation(fieldName, StringUtils.EMPTY, EMSG_FORMAT, ECODE_DEFAULT, xPath));
		}
	}
}
