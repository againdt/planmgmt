package com.serff.service.validation;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.serff.template.plan.CostShareVarianceVO;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.PlanAndBenefitsVO;
import com.serff.template.plan.PlanAttributeVO;

/**
 * Class is used to to implements all validations of Plan & Benefits Template for Version 5.
 * 
 * @author Bhavin Parmar
 * @since Mar 4, 2015
 */
@Component("planValidatorV5")
public class PlanValidatorV5 extends PlanValidator {

	private static final Logger LOGGER = Logger.getLogger(PlanValidatorV5.class);

	/**
	 * This method is used for validating plan attributes fields
	 * @param planAttributeVO, Object of PlanAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	@Override
	protected void validatePlanCostCalcAttributes(PlanAndBenefitsVO planAndBenefitsVO, List<FailedValidation> errors) {

		String fieldName = null;
		PlanAttributeVO planAttributeVO = planAndBenefitsVO.getPlanAttributes();
		fieldName = "Maximum Coinsurance For Specialty Drugs";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		validateCostCalculatorFields(fieldName,planAttributeVO.getMaximumCoinsuranceForSpecialtyDrugs(),errors);

		fieldName = "Max Num Days For Charging Inpatient Copay";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		validateCostCalculatorFields(fieldName,planAttributeVO.getMaxNumDaysForChargingInpatientCopay(),errors);

		fieldName="Begin Primary Care Cost Sharing After Set Number Visits";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		validateCostCalculatorFields(fieldName,planAttributeVO.getBeginPrimaryCareCostSharingAfterSetNumberVisits(),errors);

		fieldName="Begin Primary Care Deductible Or Coinsurance After Set Number Copays";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		validateCostCalculatorFields(fieldName,planAttributeVO.getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays(),errors);
	}

	/* (non-Javadoc)
	 * @see com.serff.service.validation.PlanValidator#getIssuerActuarialValue(com.serff.template.plan.CostShareVarianceVO)
	 */
	@Override
	protected ExcelCellVO getIssuerActuarialValue(CostShareVarianceVO costShareVarianceVO) {
		if(null != costShareVarianceVO) {
			return costShareVarianceVO.getIssuerActuarialValue();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.serff.service.validation.PlanValidator#validatePlanCSVMarketingName(com.serff.template.plan.CostShareVarianceVO, java.util.List)
	 */
	@Override
	protected void validatePlanCSVMarketingName(CostShareVarianceVO costShareVarianceVO,
			List<FailedValidation> errors) {
		if(null != costShareVarianceVO && null != errors) {
			validatePlanMarketingName(costShareVarianceVO.getPlanMarketingName(), errors);
			}
		}

	/* (non-Javadoc)
	 * @see com.serff.service.validation.PlanValidator#validateEhbApportionmentForPediatricDental(java.lang.String, com.serff.template.plan.ExcelCellVO, java.util.List)
	 */
	@Override
	protected void validateEhbApportionmentForPediatricDental(String fieldName, ExcelCellVO ehbApptForPediatricDental,
			List<FailedValidation> errors) {
		validateAmount(fieldName, ehbApptForPediatricDental, errors, true);
	}

	/* (non-Javadoc)
	 * @see com.serff.service.validation.PlanValidator#validateDesignType(java.lang.String, com.serff.template.plan.ExcelCellVO, java.util.List)
	 */
	@Override
	protected void validateDesignType(String fieldName, ExcelCellVO designTypeExcelCellVO, List<FailedValidation> errors) {
		//Not required for Version 5
		return;
	}
}
