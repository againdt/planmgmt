package com.serff.service.validation;

import static com.serff.util.SerffConstants.NEW_LINE;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidBenefitColumnEnum;
import com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidOtherServiceColumnEnum;
import com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum;
import com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidServiceAreaColumnEnum;
import com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidServiceAreaRowEnum;
import com.getinsured.serffadapter.medicaid.MedicaidOtherServicesVO;
import com.getinsured.serffadapter.medicaid.MedicaidPlanBenefitsVO;
import com.getinsured.serffadapter.medicaid.MedicaidPlanInfoVO;
import com.getinsured.serffadapter.medicaid.MedicaidServiceAreaExtVO;
import com.getinsured.serffadapter.medicaid.MedicaidServiceAreaVO;
import com.serff.util.SerffConstants;

/**
 * Class is used to validate Medicaid Plans Excel.
 * 
 * @author Bhavin Parmar
 * @since January 19, 2015
 */
@Component
public class MedicaidPlanDataValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(MedicaidPlanDataValidator.class);
	private static DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("dd/MM/yyyy").withLocale(Locale.US);

	private static final String OPTION_VALUE = "yes,no,y,n";
	private static final String EMSG_LENGTH = " Value must have length ";
	private static final String EMSG_EMPTY = " Value should not be empty." + NEW_LINE;
	private static final String EMSG_INVALID_VALUE = " Invalid value." + NEW_LINE;
	private static final String EMSG_INVALID_STATE = " Invalid State." + NEW_LINE;
	private static final String EMSG_NUMERIC = " Value must be numeric." + NEW_LINE;
	private static final String EMSG_INVALID_URL = " Invalid URL." + NEW_LINE;
	private static final String EMSG_INVALID_DATE = " Invalid Start date format. Format must be MM/dd/yyyy." + NEW_LINE;
	private static final String EMSG_INVALID_DATES = "Start date must be less than End date." + NEW_LINE;

	/**
	 * Method is used to validate require data from MedicaidPlanInfoVO object before persistence.
	 */
	public boolean validateMedicaidPlanData(MedicaidPlanInfoVO planInfoVO) {

		LOGGER.info("validateMedicaidPlanData() Start");
		boolean isValid = Boolean.FALSE;
		StringBuilder errorMessages = new StringBuilder();

		try {

			if (null == planInfoVO) {
				return isValid;
			}
			validateBlankData(planInfoVO.getIssuerName(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.ISSUER_NAME.getRowName());
			validateBlankData(planInfoVO.getIssuerHiosId(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.ISSUER_HIOS_ID.getRowName());
			validateBlankData(planInfoVO.getPlanName(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.PLAN_NAME.getRowName());
			validateBlankData(planInfoVO.getPlanNumber(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.PLAN_NUMBER.getRowName());
			validateBlankData(planInfoVO.getPlanStartDate(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.PLAN_START_DATE.getRowName());
			validateBlankData(planInfoVO.getPlanEndDate(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.PLAN_END_DATE.getRowName());
			validateBlankData(planInfoVO.getMemberNumber(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.MEMBER_NUMBER.getRowName());
			validateBlankData(planInfoVO.getWebsiteUrl(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.WEBSITE_URL.getRowName());
			validateBlankData(planInfoVO.getQualityRating(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.QUALITY_RATING.getRowName());
			validateBlankData(planInfoVO.getCountiesServed(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.COUNTIES_SERVED.getRowName());
			validateBlankData(planInfoVO.getPlanState(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.PLAN_STATE.getRowName());

			validateNumeric(planInfoVO.getIssuerHiosId(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.ISSUER_HIOS_ID.getRowName());
			validateLength(LEN_HIOS_ISSUER_ID, planInfoVO.getIssuerHiosId(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.ISSUER_HIOS_ID.getRowName());
			validatePlanNumber(planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.PLAN_NUMBER.getRowName());
			validateURL(planInfoVO.getWebsiteUrl(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.WEBSITE_URL.getRowName());
			validateState(planInfoVO.getPlanState(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.PLAN_STATE.getRowName());

			validateDate(planInfoVO.getPlanStartDate(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.PLAN_START_DATE.getRowName());
			validateDate(planInfoVO.getPlanEndDate(), planInfoVO, errorMessages, MedicaidPlanInfoRowEnum.PLAN_END_DATE.getRowName());
			compareDates(planInfoVO, errorMessages);

			validatePlanBenefitsList(planInfoVO, errorMessages);
			validateOtherServicesList(planInfoVO, errorMessages);
			validateServiceAreaList(planInfoVO, errorMessages);

			if (planInfoVO.isNotValid()) {

				if (StringUtils.isNotBlank(planInfoVO.getPlanNumber())) {
					planInfoVO.setErrorMessages(errorMessages.toString() + "Skipping to persist Medicaid Plans ["
							+ planInfoVO.getPlanNumber() + "] due to validations error.");
				}
				else {
					planInfoVO.setErrorMessages(errorMessages.toString() + "Skipping to persist Medicaid plan due to validations error.");
				}
				planInfoVO.setErrorMessages(planInfoVO.getErrorMessages() + "\n=========================================================");
				LOGGER.error(planInfoVO.getErrorMessages());
			}
			else {
				isValid = Boolean.TRUE;
			}
		}
		catch (Exception ex) {
			LOGGER.error("validateMedicaidPlanData() Exception: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("validateMedicaidPlanData() End isValid: " + isValid);
		}
		return isValid;
	}

	/**
	 * This method validate benefits list attributes
	 */
	private void validatePlanBenefitsList(MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages) {

		if (CollectionUtils.isEmpty(planInfoVO.getPlanBenefitsList())) {
			errorMessages.append("Plan Benefits List");
			errorMessages.append(SerffConstants.COMMA);
			errorMessages.append(EMSG_EMPTY);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
			return;
		}

		for (MedicaidPlanBenefitsVO planBenefitsVO : planInfoVO.getPlanBenefitsList()) {
			if (null == planBenefitsVO) {
				continue;
			}
			validateBlankData(planBenefitsVO.getBenefits(), planInfoVO, errorMessages, MedicaidBenefitColumnEnum.BENEFITS.getColumnName());
//			validateBlankData(planBenefitsVO.getKeyName(), planInfoVO, errorMessages, MedicaidBenefitColumnEnum.KEY_NAME.getColumnName());
//			validateBlankData(planBenefitsVO.getChildren(), planInfoVO, errorMessages, MedicaidBenefitColumnEnum.CHILDREN.getColumnName());
//			validateBlankData(planBenefitsVO.getChildrenAgeNinteenToTwenty(), planInfoVO, errorMessages, MedicaidBenefitColumnEnum.CHILDREN_AGE_19_20.getColumnName());
//			validateBlankData(planBenefitsVO.getAdults(), planInfoVO, errorMessages, MedicaidBenefitColumnEnum.ADULTS.getColumnName());
		}
	}

	/**
	 * This method is used to validate Other service attributes
	 */
	private void validateOtherServicesList(MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages) {

		if (CollectionUtils.isEmpty(planInfoVO.getOtherServicesList())) {
			errorMessages.append("Other Services List");
			errorMessages.append(SerffConstants.COMMA);
			errorMessages.append(EMSG_EMPTY);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
			return;
		}

		for (MedicaidOtherServicesVO medicaidOtherServicesVO : planInfoVO.getOtherServicesList()) {
			if (null == medicaidOtherServicesVO) {
				continue;
			}
			validateBlankData(medicaidOtherServicesVO.getServiceType(), planInfoVO, errorMessages, MedicaidOtherServiceColumnEnum.SERVICE_TYPE.getColumnName());
			validateBlankData(medicaidOtherServicesVO.getServiceName(), planInfoVO, errorMessages, MedicaidOtherServiceColumnEnum.SERVICE_NAME.getColumnName());
		}
	}

	/**
	 * This method validates service area attributes
	 */
	private void validateServiceAreaList(MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages) {

		MedicaidServiceAreaVO medicaidServiceAreaVO = planInfoVO.getServiceAreaVO();
		if (null == medicaidServiceAreaVO) {
			errorMessages.append("Service Area VO");
			errorMessages.append(SerffConstants.COMMA);
			errorMessages.append(EMSG_EMPTY);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
			return;
		}
		validateServiceAreaVO(planInfoVO, errorMessages);
		validateServiceAreaExtList(planInfoVO, errorMessages);
	}

	/**
	 * This method validates MedicaidServiceAreaVO
	 */
	private void validateServiceAreaVO(MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages) {

		MedicaidServiceAreaVO medicaidServiceAreaVO = planInfoVO.getServiceAreaVO();
		validateBlankData(medicaidServiceAreaVO.getServiceAreaId(), planInfoVO, errorMessages, MedicaidServiceAreaRowEnum.SERVICE_AREA_ID.getRowConstName());
		validateBlankData(medicaidServiceAreaVO.getServiceAreaName(), planInfoVO, errorMessages, MedicaidServiceAreaRowEnum.SERVICE_AREA_NAME.getRowConstName());
		validateBlankData(medicaidServiceAreaVO.getEntireState(), planInfoVO, errorMessages, MedicaidServiceAreaRowEnum.ENTIRE_STATE.getRowConstName());
		validateAgainstAllowedValue(medicaidServiceAreaVO.getEntireState(), OPTION_VALUE, planInfoVO, errorMessages, MedicaidServiceAreaRowEnum.ENTIRE_STATE.getRowConstName());
	}

	/**
	 * This method validates ServiceAreaExtList
	 */
	private void validateServiceAreaExtList(MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages) {

		MedicaidServiceAreaVO medicaidServiceAreaVO = planInfoVO.getServiceAreaVO();
		List<MedicaidServiceAreaExtVO> serviceAreaExtList = medicaidServiceAreaVO.getServiceAreaExtList();

		if (SerffConstants.YES.equalsIgnoreCase(medicaidServiceAreaVO.getEntireState())
				&& !CollectionUtils.isEmpty(serviceAreaExtList)) {
			errorMessages.append("Service Area Ext List should be empty.");
			errorMessages.append(NEW_LINE);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
		}
		else if (SerffConstants.NO.equalsIgnoreCase(medicaidServiceAreaVO.getEntireState())
				&& CollectionUtils.isEmpty(serviceAreaExtList)) {
			errorMessages.append("Service Area Ext List should not be empty.");
			errorMessages.append(NEW_LINE);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
		}
		else if (!CollectionUtils.isEmpty(serviceAreaExtList)) {
			validateServiceAreaExtVO(planInfoVO, serviceAreaExtList, errorMessages);
		}
	}

	/**
	 * This method validates MedicaidServiceAreaExtVO
	 */
	private void validateServiceAreaExtVO(MedicaidPlanInfoVO planInfoVO, List<MedicaidServiceAreaExtVO> serviceAreaExtList, StringBuilder errorMessages) {

		for (MedicaidServiceAreaExtVO medicaidServiceAreaExtVO : serviceAreaExtList) {
			if (null == medicaidServiceAreaExtVO) {
				continue;
			}
			validateBlankData(medicaidServiceAreaExtVO.getCountyName(), planInfoVO, errorMessages, MedicaidServiceAreaColumnEnum.COUNTY_NAME.getColumnName());
			validateBlankData(medicaidServiceAreaExtVO.getPartialcounty(), planInfoVO, errorMessages, MedicaidServiceAreaColumnEnum.PARTIAL_COUNTY.getColumnName());
			validateAgainstAllowedValue(medicaidServiceAreaExtVO.getPartialcounty(), OPTION_VALUE, planInfoVO, errorMessages, MedicaidServiceAreaColumnEnum.PARTIAL_COUNTY.getColumnName());

			if (SerffConstants.YES.equalsIgnoreCase(medicaidServiceAreaExtVO.getPartialcounty())) {
				validateBlankData(medicaidServiceAreaExtVO.getZiplist(), planInfoVO, errorMessages, MedicaidServiceAreaColumnEnum.ZIPCODE_LIST.getColumnName());
			}
		}
	}

	/**
	 * Method is used to validate Blank Data from Cell Value.
	 */
	private void validateBlankData(String cellValue, MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Numeric value from Cell Value.
	 */
	private void validateNumeric(String cellValue, MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue) && !StringUtils.isNumeric(cellValue)) {
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_NUMERIC);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Allowed value from Cell Value.
	 */
	private void validateAgainstAllowedValue(String cellValue, String allowedValue, MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue)) {

			List<String> list = Arrays.asList(allowedValue.split(SerffConstants.COMMA));

			if (!list.contains(cellValue.toLowerCase())) {
				errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_INVALID_VALUE);

				if (!planInfoVO.isNotValid()) {
					planInfoVO.setNotValid(Boolean.TRUE);
				}
			}
		}
	}

	private void validateState(String cellValue, MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages, String cellHeader) {
		
		if (StringUtils.isNotBlank(cellValue) && !validateState(cellValue)) {

			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_INVALID_STATE);
			
			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate URL and ECM Id.
	 */
	private void validateURL(String cellValue, MedicaidPlanInfoVO ameExcelVO,
			StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue) && !isValidPattern(PATTERN_URL, StringUtils.trim(cellValue))) {
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_INVALID_URL);

			if (!ameExcelVO.isNotValid()) {
				ameExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Length of Cell Value.
	 */
	private void validateLength(int length, String cellValue, MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue) && !isValidLength(length, cellValue)) {
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_LENGTH + length + NEW_LINE);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Length of Plan Number.
	 */
	private void validatePlanNumber(MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages, String cellHeader) {

		if (LEN_MEDICAID_PLAN_ID < StringUtils.length(planInfoVO.getPlanNumber())) {
			errorMessages.append(cellHeader + "[" + planInfoVO.getPlanNumber() + "]" + SerffConstants.COMMA + EMSG_LENGTH + LEN_MEDICAID_PLAN_ID + NEW_LINE);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	private void validateDate(String cellValue, MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue) && !validateDate(cellValue, DATE_FORMATTER)) {
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_INVALID_DATE);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to compare Start Date and End Date of Plan.
	 */
	private void compareDates(MedicaidPlanInfoVO planInfoVO, StringBuilder errorMessages) {

		int compareValue = compareDates(planInfoVO.getPlanStartDate(), planInfoVO.getPlanEndDate(), DATE_FORMATTER);

		if (DATE_START_AND_END_INVALID != compareValue
				&& (DATE_START_AND_END_EQUAL == compareValue || DATE_START_GREATER_THAN_END == compareValue)) {

			errorMessages.append(EMSG_INVALID_DATES);

			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Issuer HIOS ID.
	 */
	public void validateCarrierHIOS(MedicaidPlanInfoVO planInfoVO) {
		
		if (StringUtils.isBlank(planInfoVO.getIssuerHiosId())) {
			
			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
			planInfoVO.setErrorMessages(planInfoVO.getErrorMessages() + MedicaidPlanInfoRowEnum.ISSUER_HIOS_ID.getRowName()
					+ SerffConstants.COMMA + EMSG_EMPTY);
		}
		else if (LEN_HIOS_ISSUER_ID != StringUtils.length(planInfoVO.getIssuerHiosId())) {
			
			if (!planInfoVO.isNotValid()) {
				planInfoVO.setNotValid(Boolean.TRUE);
			}
			planInfoVO.setErrorMessages(MedicaidPlanInfoRowEnum.ISSUER_HIOS_ID.getRowName() + "[" + planInfoVO.getIssuerHiosId() + "]"
					+ SerffConstants.COMMA + EMSG_LENGTH + LEN_HIOS_ISSUER_ID + NEW_LINE);
		}
	}
	
}
