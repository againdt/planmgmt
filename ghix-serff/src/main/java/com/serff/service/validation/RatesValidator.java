package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_EMPTY_INVALID;
import static com.serff.service.validation.IFailedValidation.EMSG_FORMAT_MISSMATCHED;
import static com.serff.service.validation.IFailedValidation.EMSG_INVALID_PLAN_ID;
import static com.serff.service.validation.IFailedValidation.EMSG_LENGTH;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC;
import static com.serff.service.validation.IFailedValidation.EMSG_SHOULD_NOT_EMPTY;
import static com.serff.util.SerffConstants.HIOS_ISSUER_ID;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.serff.config.SerffConfiguration;
import com.getinsured.hix.serff.config.SerffConfiguration.SerffConfigurationEnum;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.QhpApplicationRateGroupVO;
import com.serff.template.plan.QhpApplicationRateItemVO;
import com.serff.util.SerffConstants;

/**
 * Class is used to provide validation service For Rates template.
 *
 * @author Geetha Chandran
 * @since 21-March-2013
 */
@Component
public class RatesValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(RatesValidator.class);

	private static final String SIXTY_FIVE ="65";
	private static final String SIXTY_FOUR_AND ="64 and";
	private static final String TEMPLATENAME= "RATES_DATA_TEMPLATE";
	private static final String INDIVIDUAL_TABACCO_RATE = "Individual Tobacco Rate";
	private static final int SENIOR_AGE = 65;
	private static final int END_OF_TEEN_AGE = 20;
	private static final int BIRTH_AGE = 0;
	private static final int ALLOWED_AGE = 14;
	private static final String AGE_STRING = "Age" ;

	/**
	 * Method is used to validate Rates.
	 *
	 * @param ratesVO, Object of QhpApplicationRateGroupVO
	 * @param errors, List Object of FailedValidation
	 * @return Map<String , List<String>>
	 */
	public Map<String, List<String>> validateRates(QhpApplicationRateGroupVO ratesVO, List<FailedValidation> errors) {

		LOGGER.info("validateRates() Start");
		Map<String, List<String>> templateData = null;
		List<String> planRates = new ArrayList<String>();

		String issuerId = null;

		String value=null;
		String fieldName;

		if (null != ratesVO && null != ratesVO.getHeader()) {
			
			if (null != ratesVO.getHeader().getIssuerId()
					&& null != ratesVO.getHeader().getIssuerId()
					.getCellValue()) {
				value = ratesVO.getHeader().getIssuerId()
						.getCellValue();
				LOGGER.debug("Validating HIOS Issuer ID : "+value);
				if (StringUtils.isNotEmpty(value)) {

					fieldName = "HIOS Issuer ID";

					if (!StringUtils.isNumeric(value)) {
						errors.add(getFailedValidation(fieldName, value,
								EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATENAME));
					}
					else if (!isValidLength(LEN_HIOS_ISSUER_ID, value)) {
						errors.add(getFailedValidation(fieldName, value,
								EMSG_LENGTH + LEN_HIOS_ISSUER_ID,
								ECODE_DEFAULT, TEMPLATENAME));
					}
					else {
						issuerId = value;
					}
				}
			}else{
				LOGGER.debug("Validating HIOS Issuer ID : "+value);
			}
		}

		// Validating Federal TIN
		
		if (null != ratesVO && null != ratesVO.getHeader().getTin()
				&& null != ratesVO.getHeader()
				.getTin()
				.getCellValue()) {
			value = ratesVO.getHeader().getTin()
					.getCellValue();
			LOGGER.debug("Validating Federal TIN : "+value);
			if (StringUtils.isNotEmpty(value)) {

				fieldName = "Federal TIN";

				if (!isValidLength(LET_TIN, value.replace("-", ""))) {
					errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LET_TIN + " (Numeric only)", ECODE_DEFAULT, TEMPLATENAME));
					value = null;
				}
				else if (!isValidPattern(PATTERN_TIN, value)) {
					errors.add(getFailedValidation(fieldName, value,
							EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATENAME));
				}
			}
		}

		final String rateFamilyOption = DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.RV_RATE_FAMILY_OPTION);
		final String rateAreaPrefix = DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.RV_RATE_AREA_PREFIX);
		final String rateAreaMax = DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.RV_RATE_AREA_MAX_NUMBER);
		boolean hasNoConfigs = false;
		
		if (StringUtils.isBlank(rateFamilyOption)) {
			hasNoConfigs = true;
			errors.add(getFailedValidation(SerffConfigurationEnum.RV_RATE_FAMILY_OPTION.getValue(), rateFamilyOption,
					EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATENAME));
		}
		
		if (StringUtils.isBlank(rateAreaPrefix)) {
			hasNoConfigs = true;
			errors.add(getFailedValidation(SerffConfigurationEnum.RV_RATE_AREA_PREFIX.getValue(), rateAreaPrefix,
					EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATENAME));
		}
		
		if (!StringUtils.isNumeric(rateAreaMax)) {
			hasNoConfigs = true;
			errors.add(getFailedValidation(SerffConfigurationEnum.RV_RATE_AREA_MAX_NUMBER.getValue(), rateAreaPrefix,
					EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATENAME));
		}

		if (!hasNoConfigs) {

			for (QhpApplicationRateItemVO ratesItem : ratesVO.getItems()) {
	
				if(null == ratesItem) {
					continue;
				}
	
				String effectiveDate = null;
				String expirationDate = null;
				fieldName = "Effective Date";
				
				if (null != ratesItem.getEffectiveDate()
						&& null != ratesItem.getEffectiveDate().getCellValue() && StringUtils.isNotBlank(ratesItem.getEffectiveDate().getCellValue())
						) {
					effectiveDate = ratesItem.getEffectiveDate().getCellValue();
					LOGGER.debug("Validating Rate Effective Date : "+effectiveDate);
	
					if (StringUtils.isNotEmpty(effectiveDate) && !validateDate(effectiveDate, RATE_DATE_FORMATTER)){
						errors.add(getFailedValidation(
								fieldName,
								effectiveDate, "Invalid Effective Date", ECODE_DEFAULT,
								TEMPLATENAME));
					}
				}
				else{
					LOGGER.debug("Validating Rate Effective Date : "+effectiveDate);
					errors.add(getFailedValidation(
							fieldName,
							EMPTY_VALUE, EMSG_EMPTY_INVALID, ECODE_DEFAULT,
							TEMPLATENAME));
				}
	
				fieldName = "Expiration Date";
				if (null != ratesItem.getExpirationDate()
						&& null != ratesItem.getExpirationDate().getCellValue()
						) {
					expirationDate = ratesItem.getExpirationDate().getCellValue();
					LOGGER.debug("Validating Rate Expiration Date : "+expirationDate);
					if (StringUtils.isNotEmpty(expirationDate)) {
						if(!validateDate(expirationDate, RATE_DATE_FORMATTER)){
							errors.add(getFailedValidation(
									fieldName,
									expirationDate, "Invalid Expiration Date", ECODE_DEFAULT,
									TEMPLATENAME));
						}else{
	
							if(null != effectiveDate && null != expirationDate){
								LocalDate effdate = null;
								LocalDate expdate = null;
								DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd").withLocale(Locale.US);
	
								try {
									effdate = formatter.parseLocalDate(effectiveDate);
									expdate = formatter.parseLocalDate(expirationDate);
	
									if (expdate.compareTo(effdate) <= 0) {
										errors.add(getFailedValidation(
												"Effective date",
												effectiveDate, "Effective date should be less than Expiration date", ECODE_DEFAULT,
												TEMPLATENAME));
									}
								}
								catch (Exception e) {
									LOGGER.error("Rate template: Error occured while comparing effective and expiration dates", e);
								}
							}
						}
					}
				}else{
					LOGGER.debug("Validating Rate Expiration Date : "+expirationDate);
					errors.add(getFailedValidation(
							fieldName,
							"", EMSG_EMPTY_INVALID, ECODE_DEFAULT,
							TEMPLATENAME));
				}
	
				if (null != ratesItem.getPlanId()) {
					String planId = ratesItem.getPlanId().getCellValue();
					if (!isValidPattern(PATTERN_PLAN_ID, planId)) {
						errors.add(getFailedValidation("Plan ID", planId,
								EMSG_INVALID_PLAN_ID, ECODE_DEFAULT,
								TEMPLATENAME));
					}
					LOGGER.debug("Plan ID :" + planId);
					planRates.add(planId);
				}
	
				fieldName = "Exchange Rate Area ID";
				validateRatingArea(rateAreaPrefix, rateAreaMax, fieldName, ratesItem.getRateAreaId(), errors);
				
				String tobaccoUsage = null;
	
				if (null != ratesItem.getTobacco()) {
					tobaccoUsage = ratesItem.getTobacco().getCellValue();
					LOGGER.debug("Validating Tobacco : "+ tobaccoUsage);
					if(StringUtils.isBlank(tobaccoUsage)){
						errors.add(getFailedValidation("Tobacco Usage", EMPTY_VALUE,
								EMSG_EMPTY_INVALID, ECODE_DEFAULT,
								TEMPLATENAME));
	
					}
					else if (!SerffConfiguration.hasPropertyInList(tobaccoUsage, SerffConfigurationEnum.RV_RATE_TOBACCO)) {
						errors.add(getFailedValidation(
								INDIVIDUAL_TABACCO_RATE,
								EMPTY_VALUE,
								EMSG_EMPTY_INVALID,
								ECODE_DEFAULT,
								TEMPLATENAME));
					}
				}else{
					LOGGER.debug("Validating Tobacco : "+ tobaccoUsage);
				}
	
				//Determining min and max age
	
				String ageRange = null;
				boolean isRateFamilyOption = false;
				if (null != ratesItem.getAgeNumber()
						&& StringUtils.isNotBlank(ratesItem.getAgeNumber().getCellValue())) {
					ageRange = ratesItem.getAgeNumber().getCellValue();
	
					if (ageRange.equalsIgnoreCase(rateFamilyOption)) {
						isRateFamilyOption = true;
					}
				}
				else {
					errors.add(getFailedValidation(
							AGE_STRING,
							EMPTY_VALUE,
							EMSG_EMPTY_INVALID,
							ECODE_DEFAULT, TEMPLATENAME));
				}
	
				// Rate of the Individual Non-Tobacco or No Preference Enrollee on a Plan
				validateAmount("Primary Enrollee", ratesItem.getPrimaryEnrollee(), errors, true);
	
				// Required if Tobacco <> "No Preference" - Will never be required with Age = Family Option Rate of the Individual Tobacco Enrollee
				fieldName = "Primary Enrollee Tobacco";
				if(!isRateFamilyOption
						&& SerffConstants.TOBACCO_AND_NON_TOBACCO.equalsIgnoreCase(tobaccoUsage)) {
					validateAmount(fieldName, ratesItem.getPrimaryEnrolleeTobacco(), errors, true);
				}
	
				// Rate of Family Option is validate when Age = Family Option
				if (isRateFamilyOption) {
					validateRateFields(ratesItem, errors);
				}
				
				boolean isNullCheck = (null != ratesItem.getPrimaryEnrolleeTobacco()
						&& null != ratesItem.getPrimaryEnrolleeTobacco().getCellValue()
						&& null != ratesItem.getPrimaryEnrollee() && null != ratesItem.getPrimaryEnrollee().getCellValue());
	
				if (isNullCheck && StringUtils.isNotBlank(ageRange) && !ageRange.equalsIgnoreCase(rateFamilyOption)) {
	
					BigDecimal tobaccoRate = null;
					BigDecimal nonTobaccoRate = null;
	
					String primaryEnrolleeTobacco = ratesItem.getPrimaryEnrolleeTobacco().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
					String primaryEnrollee = ratesItem.getPrimaryEnrollee().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
	
					if(StringUtils.isNotBlank(primaryEnrolleeTobacco)
							&& StringUtils.isNotBlank(primaryEnrollee)
							&& validateNumeric(primaryEnrolleeTobacco)
							&& validateNumeric(primaryEnrollee)){
	
						tobaccoRate = new BigDecimal(primaryEnrolleeTobacco);
						nonTobaccoRate = new BigDecimal(primaryEnrollee);
	
						String minage = null;
						String maxage = null;
	
						if (ageRange.startsWith(SIXTY_FIVE) || ageRange.startsWith(SIXTY_FOUR_AND)) {
							minage = SIXTY_FIVE;
							maxage = SIXTY_FIVE;
						} else if (ageRange.contains("-")) {
							String[] token = ageRange.split("-");
							minage = token[0];
							maxage = token[1];
						}
						else if(!ageRange.equalsIgnoreCase(rateFamilyOption)) {
							minage = ageRange;
							maxage = ageRange;
						}
	
						if(validateNumeric(minage) && validateNumeric(maxage)) {
	
							Integer minAge = Integer.parseInt(minage);
							Integer maxAge = Integer.parseInt(maxage);
	
							if((minAge > SENIOR_AGE) || (minAge <= ALLOWED_AGE && minAge > BIRTH_AGE)){
								errors.add(getFailedValidation(
										AGE_STRING,
										minAge.toString()+"-"+maxAge.toString(),
										EMSG_EMPTY_INVALID,
										ECODE_DEFAULT, TEMPLATENAME));
							}
							
							if(minAge==BIRTH_AGE && (maxAge< ALLOWED_AGE || maxAge>END_OF_TEEN_AGE)){
								errors.add(getFailedValidation(
										AGE_STRING,
										minAge.toString()+"-"+maxAge.toString(),
										EMSG_EMPTY_INVALID,
										ECODE_DEFAULT, TEMPLATENAME));
							}
							if (null != tobaccoRate) {
	
								if(tobaccoRate.compareTo(BigDecimal.ZERO) < 0){
									errors.add(getFailedValidation(
											INDIVIDUAL_TABACCO_RATE,
											tobaccoRate.toString(),
											"Tobacco Rate cannot be negative",
											ECODE_DEFAULT,
											TEMPLATENAME));
								}
								//Saving tobacco rates for different age bands
	
							}
	
							if ("TobaccoUse".equalsIgnoreCase(tobaccoUsage)) {
								errors.add(getFailedValidation(
										INDIVIDUAL_TABACCO_RATE,
										EMPTY_VALUE,
										EMSG_EMPTY_INVALID,
										ECODE_DEFAULT,
										TEMPLATENAME));
							}
							//Saving tobacco rates for different age bands
							if (null != nonTobaccoRate) {
	
								if(nonTobaccoRate.compareTo(BigDecimal.ZERO) < 0){
									errors.add(getFailedValidation(
											"Individual Non Tobacco Rate",
											nonTobaccoRate.toString(),
											"Non Tobacco Rate cannot be negative",
											ECODE_DEFAULT,
											TEMPLATENAME));
								}
	
							}
						}
						else if(!ageRange.equalsIgnoreCase(rateFamilyOption)) {
							errors.add(getFailedValidation(
									AGE_STRING,
									ageRange,
									EMSG_EMPTY_INVALID,
									ECODE_DEFAULT, TEMPLATENAME));
						}
					}
				}
			}
		}
		///HIX-13287 : SERFF would be validating Rates at their end and sending data. Hence comparison between
		// rates is not required.
		//rateValidations(tobaccoRates, nonTobaccoRates, errors);

		templateData = new HashMap<String, List<String>>();
		List<String> issuerIdList = new ArrayList<String>();
		issuerIdList.add(issuerId);
		templateData.put(HIOS_ISSUER_ID, issuerIdList);
		templateData.put(SerffConstants.RATES, planRates);

		LOGGER.info("validateRates() End");
		return templateData;
	}
	
	/**
	 * Method is used to validate Rating Area.
	 */
	private void validateRatingArea(final String rateAreaPrefix, final String rateAreaMax, final String fieldName,
			final ExcelCellVO excelCellVO, List<FailedValidation> errors) {
		
		String value = null;
		if (null != excelCellVO && StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			String rateAreaPrefixData = rateAreaPrefix.toLowerCase() + SerffConstants.SPACE;
			value = excelCellVO.getCellValue().trim();
			LOGGER.debug("Rating Area Value: " + value);
			
			if (value.toLowerCase().startsWith(rateAreaPrefixData)) {

				int rateAreaMaxNumber = Integer.valueOf(rateAreaMax);
				String rateAreaNumber = value.toLowerCase().replace(rateAreaPrefixData, StringUtils.EMPTY);
				
				if (!StringUtils.isNumeric(rateAreaNumber) || Integer.valueOf(rateAreaNumber) < 1
						|| Integer.valueOf(rateAreaNumber) > rateAreaMaxNumber) {
					errors.add(getFailedValidation(fieldName, value, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATENAME));
				}
			}
			else {
				errors.add(getFailedValidation(fieldName, value, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATENAME));
			}
		}
		else {
			LOGGER.debug("Validating Rate Area Value is: " + value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATENAME));
		}
	}
	
	/**
	 * Method is used validate Specific Rate Fields.
	 * @param ratesItem, Object of QhpApplicationRateItemVO
	 * @param errors, List Object of FailedValidation
	 *
	 */
	private void validateRateFields(final QhpApplicationRateItemVO ratesItem, List<FailedValidation> errors) {

		validateAmount("Couple Enrollee", ratesItem.getCoupleEnrollee(), errors, true);
		validateAmount("Primary Enrollee One Dependent", ratesItem.getPrimaryEnrolleeOneDependent(), errors, true);
		validateAmount("Primary Enrollee Two Dependent", ratesItem.getPrimaryEnrolleeTwoDependent(), errors, true);
		validateAmount("Primary Enrollee Many Dependent", ratesItem.getPrimaryEnrolleeManyDependent(), errors, true);
		validateAmount("Couple Enrollee One Dependent", ratesItem.getCoupleEnrolleeOneDependent(), errors, true);
		validateAmount("Couple Enrollee Two Dependent", ratesItem.getCoupleEnrolleeTwoDependent(), errors, true);
		validateAmount("Couple Enrollee Many Dependent", ratesItem.getCoupleEnrolleeManyDependent(), errors, true);
	}
	
	/**
	 * Method is used validate Amount.
	 * @param fieldName ,String
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 *
	 */
	private void validateAmount(final String fieldName,
			final ExcelCellVO excelCellVO, List<FailedValidation> errors,
			boolean isNullCheck) {

		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotEmpty(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
			LOGGER.debug("Validating Amount : " + value);
			try {
				LOGGER.debug("Amount is " + Double.valueOf(value));

				if (Double.valueOf(value) < 0) {
					errors.add(getFailedValidation(fieldName, value, "Rate must have positive value", ECODE_DEFAULT, TEMPLATENAME));
				}
			}
			catch (Exception e) {
				LOGGER.error("Invalid value: " + value, e);
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATENAME));
			}
		}
		else if (isNullCheck) {
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATENAME));
		}
	}
}
