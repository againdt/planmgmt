/**
 *
 */
package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_FORMAT_MISSMATCHED;
import static com.serff.service.validation.IFailedValidation.EMSG_LENGTH;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC;
import static com.serff.util.SerffConstants.HIOS_ISSUER_ID;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.serff.template.admin.extension.InsuranceCompanyType;
import com.serff.template.admin.extension.IssuerType;
import com.serff.template.admin.extension.PayloadType;
import com.serff.template.admin.niem.core.ContactInformationType;
import com.serff.template.admin.niem.core.PersonNameType;
import com.serff.template.admin.niem.core.StructuredAddressType;
import com.serff.template.admin.niem.core.TelephoneNumberType;
import com.serff.template.admin.niem.usps.states.USStateCodeType;

/**
 * This class validates Issuer/Admin data
 * @author Nikhil Talreja
 * @since 22 March, 2013
 *
 */
@Component
public class IssuerDataValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(IssuerDataValidator.class);

	//private static final String NAIC_CODE = "NAIC code";
	private static final String TEMPLATE_NAME ="ISSUER_DATA_TEMPLATE. ";
	private static final String PHONE_NUMBER_TEXT = " :Phone Number";
	private static final String CONTACT_INFO = "Contact Information";
	private static final String CUSTOMER_SERVICE_SHOP = "Customer Service -SHOP";
	private static final String CUSTOMER_SERVICE_INDIVIDUAL = "Customer Service-Individual Market";
	private static final String SHOP = "SHOP";
	private static final String INDIVIDUAL = "Individual";
	private static final String BOTH = "Both";
	
	private String issuerId = null;

	/**
	 * This method validates Issuer/Administrative data template
	 * This method has been further modularized to validate different sections
	 * @author Nikhil Talreja
	 * @since Mar 25, 2013
	 * @param issuerData
	 * @param validationErrors
	 */
	public Map<String, List<String>> validateIssuerData(final PayloadType issuerData,
			final List<FailedValidation> validationErrors) {

		HashMap<String, List<String>> templateData = null;
		LOGGER.info("Validating Issuer data : start");

		if (issuerData == null) {
			LOGGER.warn("Issuer data was null");
			validationErrors.add(getFailedValidation("Issuer Data", "", EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
			return templateData;
		}
		validateIssuerRecord(issuerData.getIssuer(), validationErrors);
	
		validateAdminSection(issuerData.getInsuranceCompany(), issuerData, validationErrors);
		
		validateAddress(issuerData.getInsuranceCompanyOrganization()
				.getOrganizationLocation().getLocationAddress()
				.getStructuredAddress(), validationErrors , "Company");
		
		validateAddress(issuerData.getIssuerOrganization()
				.getOrganizationLocation().getLocationAddress()
				.getStructuredAddress(), validationErrors ,"Issuer");

		LOGGER.info("Validating Issuer data : End");
		//Validation for these fields is not required currently

		/*LOGGER.info("Validating Contact Information");
		final String proposedMarketCoverage = issuerData.getIssuer()
				.getIssuerProposedExchangeMarketCoverageCode().getValue()
				.value();
		if (StringUtils.equalsIgnoreCase(proposedMarketCoverage, INDIVIDUAL)
				|| StringUtils.equalsIgnoreCase(proposedMarketCoverage, BOTH)) {
			validateContactDetails(issuerData.getIssuer()
					.getIssuerIndividualMarketContact(),
					issuerData.getContactInformation(), validationErrors);
		} else if (StringUtils.equalsIgnoreCase(proposedMarketCoverage, SHOP)
				|| StringUtils.equalsIgnoreCase(proposedMarketCoverage, BOTH)) {
			validateContactDetails(issuerData.getIssuer()
					.getIssuerSmallGroupMarketContact(),
					issuerData.getContactInformation(), validationErrors);

		}*/

		//Validation for these fields is not required currently

		/*LOGGER.info("Validating CEO and CFO details");
		validateContactDetails(issuerData.getInsuranceCompanyOrganization()
				.getOrganizationCEO(), issuerData.getContactInformation(),
				validationErrors);

		validateContactDetails(issuerData.getInsuranceCompanyOrganization()
				.getOrganizationCFO(), issuerData.getContactInformation(),
				validationErrors);*/

		
		validateCustomerServiceDetail(issuerData.getContactInformation(),
				validationErrors );
	
		validateShopCustomerServiceDetail(issuerData,validationErrors);
		
		validateIssuerIndividualMarketCustomerService(issuerData, validationErrors);
	
		validateNames(issuerData.getIssuer().getIssuerIndividualMarketContact().getPersonName(),issuerData, validationErrors);
		
		validateShopNames(issuerData.getIssuer().getIssuerSmallGroupMarketContact().getPersonName(),issuerData, validationErrors);

		templateData = new HashMap<String, List<String>>();
		List<String> issuerIdList = new ArrayList<String>();
		issuerIdList.add(issuerId);
		templateData.put(HIOS_ISSUER_ID, issuerIdList);
		// templateData.put(SERVICE_AREA_ID, serviceAreaId);
		return templateData;
	}
	
	private void validateShopNames(PersonNameType personName, PayloadType issuerData,
			List<FailedValidation> validationErrors) {
		
		boolean isNullPerson = (null!= personName.getPersonGivenName() && null!= personName.getPersonSurName());
		boolean isNullIssuer = (null != issuerData && null != issuerData.getIssuer() && null != issuerData.getIssuer().getIssuerProposedExchangeMarketCoverageCode().getValue());

		if (isNullPerson && isNullIssuer) {

			final String proposedMarketCoverage = issuerData.getIssuer().getIssuerProposedExchangeMarketCoverageCode().getValue().value();

			String names =personName.getPersonGivenName().getValue();
			String surNames= personName.getPersonSurName().getValue();
			LOGGER.debug("Validating Issuer Small Group Market contact persons names : "+names+" SurNames : "+surNames);
			//final String surName
			
			if (StringUtils.equalsIgnoreCase(proposedMarketCoverage, SHOP)
					|| StringUtils.equalsIgnoreCase(proposedMarketCoverage, BOTH)) {
				if(StringUtils.isBlank(names)){
					validationErrors.add(getFailedValidation("First Name", EMPTY_VALUE,
							EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
				}


				if(StringUtils.isBlank(surNames)){
					validationErrors.add(getFailedValidation("Surname ", EMPTY_VALUE,
							EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}

		}else{
			LOGGER.debug("Issuer Small Group Market contact persons names is null ");
		}

	}
	private void validateNames(PersonNameType name ,PayloadType issuerData,List<FailedValidation> validationErrors) {
		
		boolean isNullPerson = (null!= name.getPersonGivenName() && null!= name.getPersonSurName());
		boolean isNullIssuer = (null != issuerData && null != issuerData.getIssuer() && null != issuerData.getIssuer().getIssuerProposedExchangeMarketCoverageCode().getValue());
		
		if (isNullPerson && isNullIssuer) {

			final String proposedMarketCoverage = issuerData.getIssuer().getIssuerProposedExchangeMarketCoverageCode().getValue().value();
			String names =name.getPersonGivenName().getValue();
			String surNames= name.getPersonSurName().getValue();
			LOGGER.debug("Validating Names : "+names+" SurNames : "+surNames);
			//final String surName
			if (StringUtils.equalsIgnoreCase(proposedMarketCoverage, INDIVIDUAL)
					|| StringUtils.equalsIgnoreCase(proposedMarketCoverage, BOTH)) {
				
				if (StringUtils.isBlank(names)) {
					validationErrors.add(getFailedValidation("First Name", EMPTY_VALUE, EMSG_DEFAULT, ECODE_DEFAULT,
							TEMPLATE_NAME));
				}

				if (StringUtils.isBlank(surNames)) {
					validationErrors.add(getFailedValidation("Surname", EMPTY_VALUE, EMSG_DEFAULT, ECODE_DEFAULT,
							TEMPLATE_NAME));
				}
			}
		}
		else {
			LOGGER.debug("Person name or surname is null ");
		}
	}


	/**
	 * This method validates Individual Market Customer Service Details for Issuer data,
	 *
	 * @author Vani Sharma
	 * @since May 17, 2013
	 * @param contactInformation
	 * @param validationErrors
	 * @return FailedValidation
	 */
	private void validateIssuerIndividualMarketCustomerService(
			PayloadType issuerData, List<FailedValidation> validationErrors ) {

		if (null != issuerData && null != issuerData.getIssuer().getIssuerIndividualMarketCustomerService()
				&& null != issuerData.getIssuer().getIssuerProposedExchangeMarketCoverageCode().getValue()) {

			final String customerServiceUrl = issuerData.getIssuer().getIssuerIndividualMarketCustomerService().getContactWebsiteURI().getValue();
			final String proposedMarketCoverage = issuerData.getIssuer().getIssuerProposedExchangeMarketCoverageCode().getValue().value();
			LOGGER.debug("proposedMarketCoverage : "+proposedMarketCoverage);
			
			if (StringUtils.equalsIgnoreCase(proposedMarketCoverage, INDIVIDUAL)
					|| StringUtils.equalsIgnoreCase(proposedMarketCoverage, BOTH)) {
				LOGGER.debug("market coverage is individual or both");
				validatePhoneNumber(issuerData.getIssuer().getIssuerIndividualMarketCustomerService().getContactMainTelephoneNumber(), validationErrors, true, CUSTOMER_SERVICE_INDIVIDUAL);
				validatePhoneNumber(issuerData.getIssuer().getIssuerIndividualMarketCustomerService().getContactTollFreeTelephoneNumber(), validationErrors, true , CUSTOMER_SERVICE_INDIVIDUAL);
				validatePhoneNumber(issuerData.getIssuer().getIssuerIndividualMarketCustomerService().getContactTTYTelephoneNumber(), validationErrors, true , CUSTOMER_SERVICE_INDIVIDUAL);
				validatePhoneNumber(issuerData.getIssuer().getIssuerIndividualMarketCustomerService().getContactTelephoneNumber(), validationErrors, true, CUSTOMER_SERVICE_INDIVIDUAL);
	
				if (StringUtils.isBlank(customerServiceUrl)) {
					validationErrors.add(getFailedValidation("Customer Service URL", customerServiceUrl, EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
			else {
				LOGGER.debug("market coverage is not individual or both");
				validatePhoneNumber(issuerData.getIssuer().getIssuerIndividualMarketCustomerService().getContactMainTelephoneNumber(),validationErrors, false, CUSTOMER_SERVICE_INDIVIDUAL);
				validatePhoneNumber(issuerData.getIssuer().getIssuerIndividualMarketCustomerService().getContactTollFreeTelephoneNumber(),validationErrors, false, CUSTOMER_SERVICE_INDIVIDUAL);
				validatePhoneNumber(issuerData.getIssuer().getIssuerIndividualMarketCustomerService().getContactTTYTelephoneNumber(),validationErrors, false, CUSTOMER_SERVICE_INDIVIDUAL);
				validatePhoneNumber(issuerData.getIssuer().getIssuerIndividualMarketCustomerService().getContactTelephoneNumber(),validationErrors, false, CUSTOMER_SERVICE_INDIVIDUAL);
			}
		}
	}

	/**
	 * This method validates SHOP Customer Service Details for Issuer data
	 *
	 * @author Vani Sharma
	 * @since May 14, 2013
	 * @param contactInformation
	 * @param validationErrors
	 * @return FailedValidation
	 */
	private void validateShopCustomerServiceDetail(PayloadType issuerData, List<FailedValidation> validationErrors) {

		if (null != issuerData && null != issuerData.getIssuer()
				&& null != issuerData.getIssuer().getIssuerSHOPCustomerService()
				&& null != issuerData.getIssuer().getIssuerProposedExchangeMarketCoverageCode().getValue()) {

			final String customerServiceUrl = issuerData.getIssuer().getIssuerSHOPCustomerService().getContactWebsiteURI().getValue();

			LOGGER.debug("proposedMarketCoverage : " + issuerData.getIssuer().getIssuerProposedExchangeMarketCoverageCode().getValue());
			final String proposedMarketCoverage = issuerData.getIssuer().getIssuerProposedExchangeMarketCoverageCode().getValue().value();

			if (StringUtils.equalsIgnoreCase(proposedMarketCoverage, SHOP)
					|| StringUtils.equalsIgnoreCase(proposedMarketCoverage, BOTH)) {

				validatePhoneNumber(issuerData.getIssuer().getIssuerSHOPCustomerService().getContactMainTelephoneNumber(), validationErrors, true, CUSTOMER_SERVICE_SHOP);
				validatePhoneNumber(issuerData.getIssuer().getIssuerSHOPCustomerService().getContactTollFreeTelephoneNumber(), validationErrors, true, CUSTOMER_SERVICE_SHOP);
				validatePhoneNumber(issuerData.getIssuer().getIssuerSHOPCustomerService().getContactTTYTelephoneNumber(), validationErrors, true, CUSTOMER_SERVICE_SHOP);
				validatePhoneNumber(issuerData.getIssuer().getIssuerSHOPCustomerService().getContactTelephoneNumber(), validationErrors, true, CUSTOMER_SERVICE_SHOP);

				if (StringUtils.isBlank(customerServiceUrl)) {
					validationErrors.add(getFailedValidation("Customer Service URL", customerServiceUrl, EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
			else {
				LOGGER.debug("other then shop or both : " + customerServiceUrl);
				validatePhoneNumber(issuerData.getIssuer().getIssuerSHOPCustomerService().getContactMainTelephoneNumber(), validationErrors, false, CUSTOMER_SERVICE_SHOP);
				validatePhoneNumber(issuerData.getIssuer().getIssuerSHOPCustomerService().getContactTollFreeTelephoneNumber(), validationErrors, false, CUSTOMER_SERVICE_SHOP);
				validatePhoneNumber(issuerData.getIssuer().getIssuerSHOPCustomerService().getContactTTYTelephoneNumber(), validationErrors, false, CUSTOMER_SERVICE_SHOP);
				validatePhoneNumber(issuerData.getIssuer().getIssuerSHOPCustomerService().getContactTelephoneNumber(), validationErrors, false, CUSTOMER_SERVICE_SHOP);
			}
		}
	}

	/**
	 * This method validates Customer Service Details for Issuer/Admin data
	 *
	 * @author Nikhil Talreja
	 * @since Mar 25, 2013
	 * @param contactInformation
	 * @param validationErrors
	 * @return FailedValidation
	 */
	private void validateCustomerServiceDetail(
			final List<ContactInformationType> contactInformation, List<FailedValidation> validationErrors ) {


		for (final ContactInformationType info : contactInformation) {

			if (info == null) {
				validationErrors.add(getFailedValidation(CONTACT_INFO, "", EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
				continue;
			}

			validatePhoneNumber(info.getContactTelephoneNumber(), validationErrors, false, CONTACT_INFO);
			validatePhoneNumber(info.getContactTollFreeTelephoneNumber(), validationErrors, false, CONTACT_INFO);
			validatePhoneNumber(info.getContactTTYTelephoneNumber(), validationErrors, false, CONTACT_INFO);
			validatePhoneNumber(info.getContactMainTelephoneNumber(), validationErrors, false, CONTACT_INFO);

			/*if(null != info.getContactWebsiteURI()){
				final String customerServiceUrl = info.getContactWebsiteURI()
						.getValue();

				if(StringUtils.isNotEmpty(customerServiceUrl)){
					if (!isValidPattern(PATTERN_URL, customerServiceUrl)) {
						validationErrors.add(getFailedValidation("Customer Service URL", customerServiceUrl,
								EMSG_URL, ECODE_DEFAULT, templateName));
					}
				}
			}*/
		}
	}

	/**
	 * This method performs the following validations on phone number: 1. Should
	 * be numeric and 2.Should be 10 or 11 digits
	 *
	 * @author Nikhil Talreja
	 * @param validationErrors
	 * @since Mar 25, 2013
	 * @param String
	 *            number
	 * @return FailedValidation
	 */
	private void validatePhoneNumber(final TelephoneNumberType number, List<FailedValidation> validationErrors ,boolean flag ,String identifier ) {

		if (null != number && null != number.getFullTelephoneNumber()
				&& null != number.getFullTelephoneNumber().getTelephoneNumberFullID()
				&& null != number.getFullTelephoneNumber().getTelephoneNumberFullID().getValue()) {
			
			String phoneNumber = number.getFullTelephoneNumber().getTelephoneNumberFullID().getValue();
			LOGGER.debug("Validating Phone number : " + phoneNumber);
			
			if (phoneNumber.contains("-")) {
				phoneNumber = phoneNumber.replace("-", "");
			}
			if(flag){
				if(StringUtils.isBlank(phoneNumber)){
					validationErrors.add(getFailedValidation(identifier + PHONE_NUMBER_TEXT, EMPTY_VALUE, EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (!StringUtils.isNumeric(phoneNumber)) {
					validationErrors.add(getFailedValidation(identifier + PHONE_NUMBER_TEXT, phoneNumber, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (!isValidLength(LEN_PHONE_1, phoneNumber) && !isValidLength(LEN_PHONE_2, phoneNumber)) {
					validationErrors.add(getFailedValidation(identifier + PHONE_NUMBER_TEXT, phoneNumber, EMSG_LENGTH
							+ LEN_PHONE_1 + " or " + LEN_PHONE_2, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
			else {
				
				if (StringUtils.isNotEmpty(phoneNumber)) {
					
					if (!StringUtils.isNumeric(phoneNumber)) {
						validationErrors.add(getFailedValidation(identifier + PHONE_NUMBER_TEXT, phoneNumber,
								EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
					}
					else if (!isValidLength(LEN_PHONE_1, phoneNumber) && !isValidLength(LEN_PHONE_2, phoneNumber)) {
						validationErrors.add(getFailedValidation(identifier + PHONE_NUMBER_TEXT, phoneNumber,
								EMSG_LENGTH + LEN_PHONE_1 + " or " + LEN_PHONE_2, ECODE_DEFAULT, TEMPLATE_NAME));
					}
				}
			}
		} else {
			LOGGER.debug("Phone number is null ");
		}
	}

	/**
	 * This method validates the contact details for a person
	 *
	 * @author Nikhil Talreja
	 * @param validationErrors
	 * @since Mar 25, 2013
	 * @param PersonType
	 *            personType, List<ContactInformationType> contactInfos
	 * @return FailedValidation
	 */
	/*private void validateContactDetails(
			final PersonType personType,
			final List<ContactInformationType> contactInfos, List<FailedValidation> validationErrors) {

		if (personType == null
				|| StringUtils.isBlank(personType.getPersonName()
						.getPersonGivenName().getValue())
				|| StringUtils.isBlank(personType.getPersonName()
						.getPersonSurName().getValue())) {
			validationErrors.add(getFailedValidation("Contact Name", "", EMSG_DEFAULT,
					ECODE_DEFAULT, "PersonName"));
		}

		LOGGER.info("Validating Email addresses and phone numbers");
		for (final ContactInformationType info : contactInfos) {
			final String email = info.getContactEmailID().getValue();
			if (!isValidPattern(PATTERN_EMAIL, email)) {
				validationErrors.add(getFailedValidation("Email Id", email,
						EMSG_INVALID_EMAIL, ECODE_DEFAULT,
						"Payload/ContactInformation/ContactEmailID"));
			}
			final String phoneNumber = info.getContactTelephoneNumber()
					.getFullTelephoneNumber().getTelephoneNumberFullID()
					.getValue();
			validatePhoneNumber(phoneNumber, validationErrors);

		}
	}*/

	/**
	 * This method validates the address line 1 and city for a person
	 *
	 * @author Nikhil Talreja
	 * @param validationErrors
	 * @since Mar 28, 2013
	 * @param StructuredAddressType
	 *            structuredAddressType
	 * @return FailedValidation
	 */
	private void validateAddressLinesAndCity(final StructuredAddressType structuredAddressType, List<FailedValidation> validationErrors , String addres) {

		String address = null;
		LOGGER.debug("Validating address line and city ");
		if (null != structuredAddressType.getLocationStreet()
				&& null!= structuredAddressType
						.getLocationStreet().getStreetFullText()) {
			address = structuredAddressType
					.getLocationStreet().getStreetFullText().getValue();
			LOGGER.debug("Validating Adresss Line : " +address);
			if(StringUtils.isBlank(address)){
				validationErrors.add(getFailedValidation(
						addres+" Address: Address",
						EMPTY_VALUE,EMSG_DEFAULT,
						ECODE_DEFAULT,
						TEMPLATE_NAME));
		}
//		else{
//			validationErrors.add(getFailedValidation(
//					"Company Address: Linee",
//					address,
//					EMSG_DEFAULT,
//					ECODE_DEFAULT,
//					templateName));
//		}
//
		
		String city = null;
		if (structuredAddressType.getLocationCityName() != null) {
			city = structuredAddressType
					.getLocationCityName().getValue();
			LOGGER.debug("Validating City : "+city);
			if(StringUtils.isBlank(city)){
				validationErrors.add(getFailedValidation(
						addres+" Address:City",
						EMPTY_VALUE,EMSG_DEFAULT,
						ECODE_DEFAULT,
						TEMPLATE_NAME));
			}
		}
		else{
			LOGGER.debug(" City is null ");
			validationErrors.add(getFailedValidation(
					addres+" Address: City",
					city,
					EMSG_DEFAULT,
					ECODE_DEFAULT,
					TEMPLATE_NAME));
		}
	}
	}
	/**
	 * This method validates the address for a person
	 *
	 * @author Nikhil Talreja
	 * @param validationErrors
	 * @since Mar 25, 2013
	 * @param StructuredAddressType
	 *            structuredAddressType
	 * @return FailedValidation
	 */
	private void validateAddress(
			final StructuredAddressType structuredAddressType, List<FailedValidation> validationErrors , String address) {

		if (null!= structuredAddressType) {

			validateAddressLinesAndCity(structuredAddressType, validationErrors ,address );

			
			String stateCode = null;
			if (structuredAddressType.getLocationStateUSPostalServiceCode() != null
					&& structuredAddressType
							.getLocationStateUSPostalServiceCode().getValue() != null) {
				stateCode = structuredAddressType
						.getLocationStateUSPostalServiceCode().getValue()
						.value();
				LOGGER.debug("Validating State code : "+stateCode);
				if(StringUtils.isBlank(stateCode)){
					validationErrors.add(getFailedValidation(
							address+" Address: State",
							EMPTY_VALUE,
							EMSG_DEFAULT,
							ECODE_DEFAULT,
							TEMPLATE_NAME));
				}

				else if (!isValidLength(LEN_ISSUER_STATE, stateCode)) {
					validationErrors.add(getFailedValidation(
							address+" Address: State",
							stateCode,
							EMSG_LENGTH + LEN_HIOS_ISSUER_ID,
							ECODE_DEFAULT,
							TEMPLATE_NAME));
				}
			}
			else{
				validationErrors.add(getFailedValidation(
						address+" Address: State",
						"",
						EMSG_DEFAULT,
						ECODE_DEFAULT,
						TEMPLATE_NAME));
			}

			if (null != structuredAddressType.getLocationPostalCode()
					&& null != structuredAddressType.getLocationPostalCode()
							.getValue()) {
				 String zip = structuredAddressType
						.getLocationPostalCode().getValue();
				 LOGGER.debug("Validating Zip : "+zip);
				if(zip.contains("-")){
					zip = zip.replace("-","");
				}
				if(StringUtils.isBlank(zip)){

					validationErrors.add(getFailedValidation(
							address+" Address: Zip Code", EMPTY_VALUE, EMSG_DEFAULT,
							ECODE_DEFAULT, TEMPLATE_NAME));
				}else if (!StringUtils.isNumeric(zip)) {
				validationErrors.add(getFailedValidation(
						address+" Address: Zip Code",
						zip,
						EMSG_NUMERIC,
						ECODE_DEFAULT,
						TEMPLATE_NAME));
				}else if (!isValidLength(LEN_ZIP_CODE_1 , zip)
	
						&& !isValidLength(LEN_ZIP_CODE_2 , zip)) {
	
					validationErrors.add(getFailedValidation(address+" Zip Code", zip, EMSG_LENGTH
							+ LEN_ZIP_CODE_1 + " or " + LEN_ZIP_CODE_2 , ECODE_DEFAULT,
							TEMPLATE_NAME));
	
				}
			}else{
				LOGGER.debug("Zip is null ");
			}
		}
	}

	/**
	 * This method validates the NAIC codes for a company
	 *
	 * @author Nikhil Talreja
	 * @param organizationType
	 * @param validationErrors
	 * @since Mar 25, 2013
	 * @param InsuranceCompanyType
	 *            insuranceCompanyType
	 * @return FailedValidation
	 */
	/*private void validateNaicCodes(final InsuranceCompanyType insuranceCompanyType, List<FailedValidation> validationErrors){

		if (StringUtils.isBlank(insuranceCompanyType
				.getInsuranceCompanyNAICID().getValue()) ) {
			validationErrors.add(getFailedValidation(NAIC_CODE,
					"EMPTY_VALUE", EMSG_DEFAULT, ECODE_DEFAULT,
					templateName));
		}
		else if(!StringUtils.isNumeric(insuranceCompanyType
				.getInsuranceCompanyNAICID().getValue())){
			validationErrors.add(getFailedValidation(NAIC_CODE,
					insuranceCompanyType.getInsuranceCompanyNAICID()
							.getValue(), EMSG_NUMERIC, ECODE_DEFAULT,
							templateName));
		}
		else if (!isValidLength(LEN_NAIC_CODE, insuranceCompanyType
				.getInsuranceCompanyNAICID().getValue())) {
			validationErrors
					.add(getFailedValidation(
							NAIC_CODE,
							insuranceCompanyType
									.getInsuranceCompanyNAICID().getValue(),
							EMSG_LENGTH + LEN_NAIC_CODE, ECODE_DEFAULT,
							templateName));

		}
		if (StringUtils.isBlank(insuranceCompanyType
				.getInsuranceCompanyNAICGroupID().getValue()) ) {
			validationErrors.add(getFailedValidation("NAIC Group Code",
					"", EMSG_DEFAULT, ECODE_DEFAULT,
					templateName));
		}
		else if(!StringUtils.isNumeric(insuranceCompanyType
				.getInsuranceCompanyNAICGroupID().getValue())){
			validationErrors.add(getFailedValidation("NAIC Group Code",
					insuranceCompanyType.getInsuranceCompanyNAICGroupID()
							.getValue(), EMSG_NUMERIC, ECODE_DEFAULT,
							templateName));
		}
		else if (!isValidLength(LEN_NAIC_CODE, insuranceCompanyType
				.getInsuranceCompanyNAICGroupID().getValue())) {
			validationErrors
					.add(getFailedValidation(
							"NAIC Group Code",
							insuranceCompanyType
									.getInsuranceCompanyNAICGroupID().getValue(),
							EMSG_LENGTH + LEN_NAIC_CODE, ECODE_DEFAULT,
							templateName));

		}

}	*/

	/**
	 * This method validates the Administrative data for an Issuer application
	 *
	 * @author Nikhil Talreja
	 * @param payloadType
	 * @param validationErrors
	 * @since Mar 25, 2013
	 * @param InsuranceCompanyType
	 *            insuranceCompanyType
	 * @return FailedValidation
	 */
	private void validateAdminSection(
			final InsuranceCompanyType insuranceCompanyType, PayloadType issuer,  List<FailedValidation> validationErrors) {
		
		if(issuer.getIssuerOrganization() == null){
			validationErrors.add(getFailedValidation("Insurance Company Organization",
					"",
					EMSG_DEFAULT, ECODE_DEFAULT,
					TEMPLATE_NAME));
		}
		else{
			
			if(issuer.getInsuranceCompanyOrganization().getOrganizationAugmentation() != null){

				if(null != issuer.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationLegalName()){
					String companyName = issuer.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationLegalName().getValue();
					LOGGER.debug("Validating company legal name : "+ companyName);
					if(StringUtils.isBlank(companyName)){
						validationErrors.add(getFailedValidation("Company legal name",
								EMPTY_VALUE,
								EMSG_DEFAULT, ECODE_DEFAULT,
								TEMPLATE_NAME));
					}
				}else{
					LOGGER.debug("company legal name is null ");
					validationErrors.add(getFailedValidation("Company legal name",
							"",
							EMSG_DEFAULT, ECODE_DEFAULT,
							TEMPLATE_NAME));
				}

				if(null != issuer.getIssuerOrganization().getOrganizationAugmentation().getOrganizationMarketingName()){
					String marketingName = issuer.getIssuerOrganization().getOrganizationAugmentation().getOrganizationMarketingName().getValue();
					LOGGER.debug("Validating marketingName name : "+ marketingName);
					if(StringUtils.isBlank(marketingName)){
						validationErrors.add(getFailedValidation("Organization marketing name",
								EMPTY_VALUE,
								EMSG_DEFAULT, ECODE_DEFAULT,
								TEMPLATE_NAME));
					}
				}else{
					LOGGER.debug("company marketingName is null ");
					validationErrors.add(getFailedValidation("Organization Marketing name",
							"",
							EMSG_DEFAULT, ECODE_DEFAULT,
							TEMPLATE_NAME));
				}
			}

			



		if (insuranceCompanyType != null) {

			//Validation for this field is not required currently

			/*LOGGER.info("Validating Associated Health Plan ID");
			if (!StringUtils.isNumeric(insuranceCompanyType.getHPID()
					.getValue())) {
				validationErrors.add(getFailedValidation("Health Plan ID",
						insuranceCompanyType.getHPID().getValue(),
						EMSG_NUMERIC, ECODE_DEFAULT,
						"Payload/InsuranceCompany/HPID"));
			}
			if (!isValidLength(LEN_HP_ID, insuranceCompanyType.getHPID()
					.getValue())) {
				validationErrors.add(getFailedValidation("Health Plan ID",
						insuranceCompanyType.getHPID().getValue(), EMSG_LENGTH
								+ LEN_HP_ID, ECODE_DEFAULT,
						"Payload/InsuranceCompany/HPID"));

			}*/
			
			if (null != insuranceCompanyType.getInsuranceCompanyTINID()) {

				String value = insuranceCompanyType.getInsuranceCompanyTINID().getValue();
				LOGGER.debug("Validating TIN : " +value);
				if (StringUtils.isNotEmpty(value)) {

					String fieldName = "TIN";

					if (!isValidLength(LET_TIN, value.replace("-", ""))) {
						validationErrors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LET_TIN + " (Numeric only)", ECODE_DEFAULT, TEMPLATE_NAME));
						value = null;
					}
					else if (!isValidPattern(PATTERN_TIN, value)) {
						validationErrors.add(getFailedValidation("TIN", value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
					}
				}else{
					validationErrors.add(getFailedValidation("TIN", EMPTY_VALUE, EMSG_DEFAULT, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}

			// LOGGER.info("Validating NAIC codes");
		    // validateNaicCodes(insuranceCompanyType,validationErrors);
		}
		else{
			validationErrors.add(getFailedValidation("Insurance Company",
					"",
					EMSG_DEFAULT, ECODE_DEFAULT,
					TEMPLATE_NAME));
		}

	}
	}

	/**
	 * This method does basic validations on the Issuer record
	 *
	 * @author Nikhil Talreja
	 * @param validationErrors
	 * @since Mar 25, 2013
	 * @param IssuerType
	 *            issuerType
	 * @return FailedValidation
	 */
	private void validateIssuerRecord(final IssuerType issuerType, List<FailedValidation> validationErrors) {
		issuerId = validateIssuerId(issuerType, validationErrors);
		LOGGER.debug("Validated Issuer ID :" + issuerId);
		//validateContactWebsiteURI(issuerType, validationErrors);
		
		final USStateCodeType stateCodeType = issuerType.getIssuerStateCode();
		if (stateCodeType != null && stateCodeType.getValue() != null) {
			final String stateCode = stateCodeType.getValue().value();
			LOGGER.debug("Validating Issuer State" +stateCode);
			if(StringUtils.isBlank(stateCode)){
				validationErrors.add(getFailedValidation("Issuer State", EMPTY_VALUE,
						EMSG_DEFAULT, ECODE_DEFAULT,
						TEMPLATE_NAME));
			}

			if (!isValidLength(LEN_ISSUER_STATE, stateCode)) {
				validationErrors.add(getFailedValidation("Issuer State", stateCode,
						EMSG_LENGTH + LEN_ISSUER_STATE, ECODE_DEFAULT,
						TEMPLATE_NAME));

			}
		}
		else{
			LOGGER.debug(" Issuer State is null");
			validationErrors.add(getFailedValidation("Issuer State", "", EMSG_DEFAULT
					, ECODE_DEFAULT, TEMPLATE_NAME));
		}

	}




}