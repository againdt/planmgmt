package com.serff.service.validation;

import java.util.List;
import java.util.Map;

import com.serff.template.plan.PrescriptionDrugTemplateVO;

/**
 * Interface is used to implements all validations of Prescription Drug Template.
 * 
 * @author Bhavin Parmar
 * @since Mar 16, 2015
 */
public interface IPrescriptionDrugValidator {

	Map<String, List<String>> validatePrescriptionDrug(final PrescriptionDrugTemplateVO templateVO,
			List<FailedValidation> errors);
}
