package com.serff.service.validation;

/**
 * This is VO Class to capture all failed validations.
 * 
 * @author polimetla_b
 * @since 3/20/2013
 */
public class FailedValidation implements IFailedValidation {

	private String fieldName;
	private String fieldValue;
	private String errorMsg = EMSG_DEFAULT;
	private String errorCode = ECODE_DEFAULT;
	private String templateName;

	private static final String SEPARATOR = "#";

	public String getMessage() {
		
		StringBuffer sb = new StringBuffer();

		sb.append(fieldName).append(SEPARATOR);
		sb.append(fieldValue).append(SEPARATOR);
		sb.append(errorMsg).append(SEPARATOR);
		sb.append(errorMsg);

		return sb.toString();
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}


	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
}
