package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_ALPHA;
import static com.serff.service.validation.IFailedValidation.EMSG_COST_VALUE;
import static com.serff.service.validation.IFailedValidation.EMSG_EMPTY_INVALID;
import static com.serff.service.validation.IFailedValidation.EMSG_FORMAT_MISSMATCHED;
import static com.serff.service.validation.IFailedValidation.EMSG_INVALID_DATE;
import static com.serff.service.validation.IFailedValidation.EMSG_INVALID_MARKET_COVERAGE_TYPE;
import static com.serff.service.validation.IFailedValidation.EMSG_INVALID_STATE_CODE;
import static com.serff.service.validation.IFailedValidation.EMSG_LENGTH;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC_OR_INVALID;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC_ZERO;
import static com.serff.service.validation.IFailedValidation.EMSG_PERCENTAGE_VALUE;
import static com.serff.service.validation.IFailedValidation.EMSG_SHOULD_NOT_EMPTY;
import static com.serff.service.validation.IFailedValidation.EMSG_URL;
import static com.serff.util.SerffConstants.COMMA;
import static com.serff.util.SerffConstants.DECIMAL_POINT;
import static com.serff.util.SerffConstants.DOLLAR;
import static com.serff.util.SerffConstants.DOLLARSIGN;
import static com.serff.util.SerffConstants.HIOS_ISSUER_ID;
import static com.serff.util.SerffConstants.PERCENTAGE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.serff.template.plan.BenefitAttributeVO;
import com.serff.template.plan.CostShareVarianceVO;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.HeaderVO;
import com.serff.template.plan.HsaVO;
import com.serff.template.plan.MoopVO;
import com.serff.template.plan.PackageListVO;
import com.serff.template.plan.PlanAndBenefitsPackageVO;
import com.serff.template.plan.PlanAndBenefitsVO;
import com.serff.template.plan.PlanAttributeVO;
import com.serff.template.plan.PlanBenefitTemplateVO;
import com.serff.template.plan.PlanDeductibleVO;
import com.serff.template.plan.SBCVO;
import com.serff.template.plan.ServiceVisitVO;
import com.serff.template.plan.UrlVO;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * This is going to have all validations regarding Plan&Benefits.
 *
 * @author polimetla_b
 * @since 3/20/2013
 */
public abstract class PlanValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(PlanValidator.class);

	public static final String TEMPLATE_VERSION_5 = "v5";
	public static final String TEMPLATE_VERSION_6 = "v6";
	public static final String TEMPLATE_VERSION_7 = "v7";
	public static final String TEMPLATE_VERSION_8 = "v8";
	public static final String TEMPLATE_VERSION_9 = "v9";
	protected static final String MSG_SBC = "Validation  SBC - ";
	protected static final String MSG_MOOP = "Validation  Moop list - ";
	protected static final String MSG_DEDUCTIBLE_LIST = "Validation Deductible List: ";
	protected static final String MSG_URLS = "Validation URLs: ";
	protected static final String MSG_PLAN_ATTR = "Validation Plan Attributes: ";
	protected static final String MSG_HSA = "Validation HSA: ";
	protected static final String MSG_PLAN_BENEFITS = "Validation Plan Benefits: ";
	protected static final String MSG_COST_SHARE = "Validation Cost Share Variation: ";
	protected static final String MSG_SERVICE_VISIT = "Validation Service Visit List: ";

	@Autowired protected PlanValidatorOptions planValidatorOptions;
	@Autowired protected SerffUtils serffUtils;

	protected String stateAbbr = null, issuerId = null,
			marketCoverageType = null, dentalOnlyIdicator = null,
			uniquePlanDesign = null, multipleInNetworkTiers = null,metalLevel =null; // serviceAreaId = null;
	protected boolean isDentalOnlyInd = false;

	protected static final String TEMPLATE_NAME ="PLAN_BENIFIT_DATA_TEMPLATE";
	protected static final String MSG_PLAN_ATTR_VALIDATION = "Validation Plan Attributes : ";
	private static final String FAMILY_PER_PERSON = "per person";
	private static final String FAMILY_PER_GROUP = "per group";

	protected static final int FIRST_DAY = 1, FIRST_MONTH = 1, LAST_MONTH = 12;
	protected static final double HUNDRED = 100d;
	protected static final double ONE = 1;

	/**
	 * This method is used for validating Plan and benefit - Benefits Package
	 *
	 * @param planAndBenefitsPkg, object of PlanAndBenefitsPackageVO
	 * @param errors, List object of FailedValidation
	 */
	public Map<String, List<String>> validatePlan(PlanBenefitTemplateVO planAndBenefitTemplate, Map<String, List<String>> templateRatesData, 
			Map<String, List<String>> templateNetworkData, Map<String, List<String>> templateServiceData, List<FailedValidation> errors, String exchangeTypeFilter) {

		Map<String, List<String>> templateData = null;
		LOGGER.info("validatePlan() Start");

		try {
			if (null == planAndBenefitTemplate) {
				LOGGER.info("planAndBenefitTemplate has been empty.");
				return templateData;
			}

			// HeaderVO validation
			PackageListVO packageListVO = planAndBenefitTemplate.getPackagesList();
			List<PlanAndBenefitsPackageVO> planAndBenefitsPkgList = packageListVO.getPackages();
			LOGGER.info("Validating Paln Header : Start");

			if (!CollectionUtils.isEmpty(planAndBenefitsPkgList)) {

				for (PlanAndBenefitsPackageVO planAndBenefitsPkg : planAndBenefitsPkgList) {
					HeaderVO headerVO = planAndBenefitsPkg.getHeader();

					if (null != headerVO) {
						issuerId = validateHIOSIssuerID(headerVO.getIssuerId(), errors);
						stateAbbr = validateIssuerState(headerVO.getStatePostalCode(), errors);
						marketCoverageType = validateMarketCoverageType(headerVO.getMarketCoverage(), errors);
						validateTIN(headerVO.getTin(), errors);
						dentalOnlyIdicator = validateDentalOnlyIdicator(headerVO.getDentalPlanOnlyInd(), errors);

						if (dentalOnlyIdicator != null) {
							isDentalOnlyInd = dentalOnlyIdicator.equalsIgnoreCase(YES);
						}
						LOGGER.info("Validating Paln Header : End");
					}
					// Validate list of PlanAndBenefitsVO
					LOGGER.info("Validate list of PlanAndBenefitsVO");
					validatePlanAndBenefitsVO(planAndBenefitsPkg.getPlansList().getPlans(), exchangeTypeFilter, templateRatesData,
							templateNetworkData, templateServiceData, errors);
					validateBenefitsVO(planAndBenefitsPkg.getBenefitsList().getBenefits(), errors);
					templateData = new HashMap<String, List<String>>();
					List<String> issuerIdList = new ArrayList<String>();
					issuerIdList.add(issuerId);
					templateData.put(HIOS_ISSUER_ID, issuerIdList);
				}
			}
		}
		finally {
			LOGGER.info("validatePlan() End");
		}
		return templateData;
	}

	protected void validateBenefitsVO(List<BenefitAttributeVO> benefitAttributeList,
			List<FailedValidation> errors) {

		if (CollectionUtils.isEmpty(benefitAttributeList)) {
			LOGGER.info("BenefitAttributeVO List has been empty.");
			return;
		}

		for (BenefitAttributeVO benefitAttributeVO : benefitAttributeList) {

			if (null == benefitAttributeVO) {
				continue;
			}
			// validate benefitAttributeVO fields
			validateBenefitInfo(benefitAttributeVO, errors);
		}
	}

	/**
	 * Method is used to validate List of PlanAndBenefitsVO.
	 *
	 * @param planAndBenefitsList, List object of PlanAndBenefitsVO
	 * @param errors, List Object of FailedValidation
	 */
	private void validatePlanAndBenefitsVO(
			List<PlanAndBenefitsVO> planAndBenefitsList, String exchangeTypeFilter, Map<String, List<String>> templateRatesData,
			Map<String, List<String>> templateNetworkData, Map<String, List<String>> templateServiceData, List<FailedValidation> errors) {

		if (CollectionUtils.isEmpty(planAndBenefitsList)) {
			LOGGER.warn("PlanAndBenefitsVO List has been empty.");
			return;
		}

		for (PlanAndBenefitsVO planAndBenefitsVO : planAndBenefitsList) {

			if (null == planAndBenefitsVO) {
				continue;
			}
			boolean validateRates = true;
			// iterate list of CostShareVarianceVO
			boolean isOffExchange = false, isOnExchange = false;

			if (null != planAndBenefitsVO.getPlanAttributes()) {

				if (StringUtils.isNotBlank(issuerId) && StringUtils.isNotBlank(stateAbbr)) {
					// Plan Identifiers Validation
					validatePlanIdentifiers(planAndBenefitsVO.getPlanAttributes(), validateRates, templateRatesData, templateNetworkData, templateServiceData, errors);
					//PlanAttributes validations
					validatePlanAttributes(planAndBenefitsVO.getPlanAttributes(), errors);
					//CostCalculator attributes
					validatePlanCostCalcAttributes(planAndBenefitsVO, errors);
				}
				// Validate Stand Alone Dental Only
				validateStandAloneDentalPlan(planAndBenefitsVO.getPlanAttributes(), errors);
				// Validate plan dates
				validatePlanDates(planAndBenefitsVO.getPlanAttributes(), errors);
				// validate Geographic Coverage fields
				validateGeographicCoverage(planAndBenefitsVO.getPlanAttributes(), errors);
				// validate URLs
				//validateURLs(planAndBenefitsVO.getPlanAttributes(), errors);
			}

			for (CostShareVarianceVO costShareVarianceVO : planAndBenefitsVO.getCostShareVariancesList().getCostShareVariance()) {

				if (null != costShareVarianceVO) {
					// validate costSharVariations fields
					if (validateCostShareVariations(costShareVarianceVO, errors)) {
						isOffExchange = true;
					}
					else {
						isOnExchange = true;
					}
				}
			}
			//only off-exchangePlan
			if (isOffExchange && !isOnExchange) {
				LOGGER.debug("Only OFF-Exchange plans exist");
				if (exchangeTypeFilter.equals(SerffPlanMgmtBatch.EXCHANGE_TYPE.ON.toString())
						|| exchangeTypeFilter.equals(SerffPlanMgmtBatch.EXCHANGE_TYPE.PUF.toString())) {
					validateRates = false;
				}
			}
			else if (!isOffExchange && isOnExchange) {
				LOGGER.debug("Only ON-Exchange plans exist");
				if (exchangeTypeFilter.equals(SerffPlanMgmtBatch.EXCHANGE_TYPE.OFF.toString())) {
					validateRates = false;
				}
			}
			// iterate list of BenefitAttributeVO
			if (null != planAndBenefitsVO.getBenefitAttributesList()) {
				for (BenefitAttributeVO benefitAttributeVO : planAndBenefitsVO.getBenefitAttributesList().getBenefitAttributes()) {
					if (null != benefitAttributeVO) {
						// validate benefitAttributeVO fields
						validateBenefitInfo(benefitAttributeVO, errors);
					}
				}
			}
		}
	}

	/**
	 * This method is used for validating plan identifiers fields
	 * @param planAttributeVO, Object of PlanAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	private void validatePlanIdentifiers(PlanAttributeVO planAttributeVO, boolean validateRates, Map<String, List<String>> templateRatesData, Map<String, List<String>> templateNetworkData, Map<String, List<String>> templateServiceData, List<FailedValidation> errors){
		LOGGER.info("Validate: planAttributeVO");
		String hiosProductIdValue = null;
		
		hiosProductIdValue = validateHIOSProductID(planAttributeVO.getHiosProductID(), errors);

		validateHPID(planAttributeVO.getHpid(), errors);

		validateHIOSPlanID(hiosProductIdValue, validateRates, planAttributeVO.getStandardComponentID(), templateRatesData, errors);
		
		validatePlanMarketingName(planAttributeVO.getPlanMarketingName(), errors);

		validateNetworkID(planAttributeVO.getNetworkID(),templateNetworkData, errors);

		// serviceAreaId = validateServiceAreaID(planAttributeVO.getServiceAreaID(), errors);
		validateServiceAreaID(planAttributeVO.getServiceAreaID(),templateServiceData, errors);
		
		validateFormularyID(planAttributeVO.getFormularyID(), errors , !isDentalOnlyInd);
	}

	/**
	 * This method is used for validating plan cost calc attributes fields
	 * @param planAttributeVO, Object of PlanAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	protected abstract void validatePlanCostCalcAttributes(PlanAndBenefitsVO planAndBenefitsVO, List<FailedValidation> errors);
	/**
	 * This method is used to get IssuerActuarial attribute value
	 * @param costShareVarianceVO, Object of CostShareVarianceVO
	 */
	protected abstract ExcelCellVO getIssuerActuarialValue(CostShareVarianceVO costShareVarianceVO);

	/**
	 * This method is used for validating plan CSV Marketing Name fields
	 * @param costShareVarianceVO, Object of CostShareVarianceVO
	 * @param errors, List Object of FailedValidation
	 */
	protected abstract void validatePlanCSVMarketingName(CostShareVarianceVO costShareVarianceVO, List<FailedValidation> errors);
	protected abstract void validateDesignType(String fieldName, ExcelCellVO designTypeExcelCellVO, List<FailedValidation> errors);

	/**
	 * Method is used validate HIOS Issuer ID.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @return value of HIOS Issuer ID
	 */
	protected String validateHIOSIssuerID(ExcelCellVO excelCellVO,
			List<FailedValidation> errors) {

		String fieldName = "HIOS Issuer ID";
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue().trim();
			LOGGER.debug("Validation Header : HIOS Issuer ID : "+value);
			if (!StringUtils.isNumeric(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC,ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
			else if (!isValidLength(LEN_HIOS_ISSUER_ID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_HIOS_ISSUER_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
		}
		else {
			LOGGER.debug("Validation Header : HIOS Issuer ID : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			value = null;
		}
		return value;
	}

	/**
	 * Method is used validate Issuer State.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @return value of Issuer State
	 */
	protected String validateIssuerState(ExcelCellVO excelCellVO,
			List<FailedValidation> errors) {

		String value = null;
		String fieldName = "Issuer State";
		String globalStateCode = serffUtils.getGlobalStateCode();

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			LOGGER.debug("Validation Header : Issuer State : "+value);
			if (!isValidLength(LEN_ISSUER_STATE, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_ISSUER_STATE, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
			else if (!StringUtils.isAlpha(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_ALPHA, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
			else if (StringUtils.isNotBlank(globalStateCode) && !globalStateCode.equalsIgnoreCase(SerffConstants.DEFAULT_PHIX_STATE_CODE)
					&& !value.equals(globalStateCode)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_INVALID_STATE_CODE + globalStateCode, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
		}
		else {
			LOGGER.debug("Validation Header : Issuer State : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			value = null;
		}
		return value;
	}

	/**
	 * Method is used validate Market Coverage Type.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @return value of Market Coverage Type
	 */
	protected String validateMarketCoverageType(ExcelCellVO excelCellVO,
			List<FailedValidation> errors) {

		String fieldName = "Market Coverage Type";
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			LOGGER.debug("Validation Header : Market Coverage Type : "+value);
			if (!planValidatorOptions.getMarketCoverageType(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_INVALID_MARKET_COVERAGE_TYPE, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
		}
		else {
			LOGGER.debug("Validation Header : Market Coverage Type : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			value = null;
		}
		return value;
	}

	/**
	 * Method is used validate Tax Identifiers Number(TIN).
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateTIN(ExcelCellVO excelCellVO,
			List<FailedValidation> errors) {

		String fieldName = "Tax Identifiers Number(TIN)";
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			LOGGER.debug("Validation Header : TIN : "+value);
			if (!isValidLength(LET_TIN, value.replace("-", ""))) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LET_TIN + " (Numeric only)", ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
			else if (!isValidPattern(PATTERN_TIN, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else {
			LOGGER.debug("Validation Header : TIN : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used validate Dental Only Plan.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @return value of DentalOnlyIdicator
	 */
	protected String validateDentalOnlyIdicator(ExcelCellVO excelCellVO,
			List<FailedValidation> errors) {

		String fieldName = "Dental Only Plan";
	//	String xPath = StringUtils.EMPTY;
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {
			value = excelCellVO.getCellValue();
			LOGGER.debug("Validation Header : Dental Only Plan : "+value);
		}
		else {
			LOGGER.debug("Validation Header : Dental Only Plan : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			value = null;
		}
		return value;
	}

	/**
	 * Method is used validate HIOS Product ID.
	 * Format: Issuer ID + State code + 999 (12345VA001)
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @return value of HIOS Product ID
	 */
	protected String validateHIOSProductID(ExcelCellVO excelCellVO, List<FailedValidation> errors) {

		String fieldName = "HIOS Product ID";
		//String xPath = StringUtils.EMPTY;
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			LOGGER.debug("Validate: HIOS Product ID : "+value);
			if (!isValidLength(LEN_HIOS_PRODUCT_ID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_HIOS_PRODUCT_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
			else {

				if (!value.substring(0, LEN_HIOS_ISSUER_ID).equals(issuerId)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
				else if (!value.substring(LEN_HIOS_ISSUER_ID, LEN_HIOS_ISSUER_ID + LEN_ISSUER_STATE).equals(stateAbbr)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
				else if (!StringUtils.isNumeric(value.substring(LEN_HIOS_ISSUER_ID + LEN_ISSUER_STATE))) {
					errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
			}
		}
		else {
			LOGGER.debug("Validate: HIOS Product ID : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			value = null;
		}
		return value;
	}

	/**
	 * Method is used validate HPID.
	 * Format: 9999999999
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateHPID(ExcelCellVO excelCellVO, List<FailedValidation> errors) {

		String fieldName = "HPID";
		//String xPath = StringUtils.EMPTY;
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			LOGGER.debug("Validate: HPID : "+value);
			if (!StringUtils.isNumeric(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
			else if (!isValidLength(LEN_HPID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_HPID, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
		}
	}

	/**
	 * Method is used validate HIOS Plan ID.
	 * Format = HIOS Product ID + 9999 (12345VA0019999)
	 *
	 * @param productID, HIOS Product ID
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateHIOSPlanID(String productID, boolean validateRates, 
			ExcelCellVO excelCellVO, Map<String, List<String>> templateRatesData, List<FailedValidation> errors) {

		String fieldName = "HIOS Plan ID";
		//String xPath = StringUtils.EMPTY;
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			LOGGER.debug("Validate: HIOS Plan ID : "+value);
			if (!isValidLength(LEN_HIOS_PLAN_ID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_HIOS_PLAN_ID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else {

				if (!isValidPattern(PATTERN_PLAN_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (!value.substring(0, LEN_HIOS_PRODUCT_ID).equals(productID)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}

			//Validating if the provided Network is also part of Network template
			if(validateRates && !CollectionUtils.isEmpty(templateRatesData)){
				List<String> providedRates = templateRatesData.get(SerffConstants.RATES);
				if(CollectionUtils.isEmpty(providedRates) || !providedRates.contains(value)){
					errors.add(getFailedValidation(fieldName, value, "Rates not available for provided plan id " + value, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
		}
		else {
			LOGGER.debug("Validate: HIOS Plan ID : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used validate Plan Marketing Name.
	 * Plan Marketing Name associated with an Issuer/State combination.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validatePlanMarketingName(ExcelCellVO excelCellVO, List<FailedValidation> errors) {

		String fieldName = "Plan Marketing Name";
		//String xPath = StringUtils.EMPTY;
		LOGGER.debug("Validate: Plan Marketing Name");
		if (null == excelCellVO || StringUtils.isEmpty(excelCellVO.getCellValue())) {
			errors.add(getFailedValidation(fieldName, StringUtils.EMPTY, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used validate Network ID.
	 * Format = State Abbr + N + 001
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateNetworkID(ExcelCellVO excelCellVO, Map<String, List<String>> templateNetworkData, List<FailedValidation> errors) {

		String fieldName = "Network ID";
		//String xPath = StringUtils.EMPTY;
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			LOGGER.debug("Validate: Network ID : "+value);
			if (!isValidLength(LET_NETWORK_ID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LET_NETWORK_ID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else {

				if (!isValidPattern(PATTERN_NETWORK_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (!value.substring(0, LEN_ISSUER_STATE).equals(stateAbbr)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}

			//Validating if the provided Network is also part of Network template
			if(!CollectionUtils.isEmpty(templateNetworkData)){
				List<String> providedNetworks = templateNetworkData.get(SerffConstants.NETWORKS);

				if(CollectionUtils.isEmpty(providedNetworks) || !providedNetworks.contains(value)){
					errors.add(getFailedValidation(fieldName, value, "Provided Network ID is not part of Network template.", ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
		}
		else {
			LOGGER.debug("Validate: Network ID : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used Service Area ID.
	 * Format = State Abbr + S + 001
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	protected String validateServiceAreaID(ExcelCellVO excelCellVO, Map<String, List<String>> templateServiceData, List<FailedValidation> errors) {

		String fieldName = "Service Area ID";
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			LOGGER.debug("Validate: Service Area ID : "+value);
			if (!isValidLength(LET_SERVICE_AREA_ID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LET_SERVICE_AREA_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
			else {

				if (!isValidPattern(PATTERN_SERVICE_AREA_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
				else if (!value.substring(0, LEN_ISSUER_STATE).equals(stateAbbr)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
			}

			//Validating if the provided Service Area is also part of Service Area template
			if(!CollectionUtils.isEmpty(templateServiceData)){
				List<String> providedSvcAreas = templateServiceData.get(SerffConstants.SERVICE_AREAS);

				if(CollectionUtils.isEmpty(providedSvcAreas) || !providedSvcAreas.contains(value)){
					errors.add(getFailedValidation(fieldName, value, "Provided Service Area is not part of Service Area template.", ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
		}
		else {
			LOGGER.debug("Validate: Service Area ID : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		return value;
	}

	/**
	 * Method is used Formulary ID.
	 * Format = State Abbr + F + 001
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateFormularyID(ExcelCellVO excelCellVO, List<FailedValidation> errors, boolean isNullCheck)  {

		String fieldName = "Formulary ID";
		//String xPath = StringUtils.EMPTY;
		String value = null;


		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			LOGGER.debug("Validate: Formulary ID : "+value);
			if (!isValidLength(LEN_FORMULARY_ID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_FORMULARY_ID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else {

				if (!isValidPattern(PATTERN_FORMULARY_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (!value.substring(0, LEN_ISSUER_STATE).equals(stateAbbr)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
		}
		else if(isNullCheck) {
			LOGGER.debug("Validate: Formulary ID : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}
	
	protected void validateCostCalculatorFields(String fieldName ,ExcelCellVO excelCellVO, List<FailedValidation> errors){

		String value = null;
		
		if (excelCellVO != null
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {
			
			value = excelCellVO.getCellValue();
			LOGGER.debug(MSG_PLAN_ATTR_VALIDATION+value);
			if(value.contains(DOLLARSIGN)){
				value=value.replaceAll(DOLLAR, StringUtils.EMPTY);
			}
			if (!StringUtils.isNumeric(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}else{
			LOGGER.debug("Validation Plan Attributes: "+value);
		}
	}

	/**
	 * Method is used validate Plan Attributes.
	 *
	 * @param fieldName, Field Name
	 * @param xPath, Path of template Attribute
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @param Cell Value of ExcelCellVO object
	 */
	protected String validatePlanAttributes(String fieldName,
			ExcelCellVO excelCellVO, List<FailedValidation> errors) {
	
		String value = null;

		if (null == excelCellVO || StringUtils.isBlank(excelCellVO.getCellValue())) {
			LOGGER.info(MSG_PLAN_ATTR_VALIDATION+value);
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		else {
			value = excelCellVO.getCellValue();
			LOGGER.info(MSG_PLAN_ATTR_VALIDATION + value);
		}
		return value;
	}

	/**
	 * Method is used validate Amount field.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @param isNullCheck, Is Null Check
	 * @param isWholeNumberCheck, Is Whole Number Check
	 */
	protected String validateAmount(String fieldName, ExcelCellVO excelCellVO,
			List<FailedValidation> errors, boolean isNullCheck, boolean isWholeNumberCheck) {

		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue().trim();
			LOGGER.debug("Validation Amount : "+value);
			value = value.replaceAll(COMMA, StringUtils.EMPTY).replaceAll(DOLLAR, StringUtils.EMPTY);

			if (isWholeNumberCheck) {

				if (!validateNumeric(value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
				else if ((Double.valueOf(value) % 1) != 0) {
					errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
				else if (Double.valueOf(value) < 0) {
					errors.add(getFailedValidation(fieldName, value, EMSG_COST_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
			}
			else {
				value = value.replaceFirst(DECIMAL_POINT, StringUtils.EMPTY);

				if (!StringUtils.isNumeric(value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
				else if (Integer.valueOf(value) < 0) {
					errors.add(getFailedValidation(fieldName, value, EMSG_COST_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
			}
		}
		else if (isNullCheck) {
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		else {
			LOGGER.debug("Validation Amount : " + value);
		}
		return value;
	}

	/**
	 * Method is used validate Amount field.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @param isNullCheck, Is Null Check
	 */
	protected String validateAmount(String fieldName, ExcelCellVO excelCellVO,
			List<FailedValidation> errors, boolean isNullCheck) {
		return validateAmount(fieldName, excelCellVO, errors, isNullCheck, false);
	}

	/**
	 * Method is used validate Numeric field.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @param isNullCheck, Is Null Check
	 */
	protected void validatePercentage(String fieldName, ExcelCellVO excelCellVO,
			List<FailedValidation> errors, boolean isNullCheck) {

		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue().trim();
			value = value.replaceAll(PERCENTAGE, StringUtils.EMPTY);
			boolean isNumeric = validateNumeric(value);

			if (isNumeric
					&& (Double.valueOf(value) < 0d || Double.valueOf(value) > HUNDRED)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_PERCENTAGE_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else if (!isNumeric) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else if (isNullCheck) {
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used validate EHB Percent Premium field.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @param isNullCheck, Is Null Check
	 */
	protected void validatePercentAsFraction(String fieldName, ExcelCellVO excelCellVO,
			List<FailedValidation> errors, boolean isNullCheck) {

		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			boolean isNumeric = validateNumeric(value);

			if (isNumeric
					&& (0d > Double.valueOf(value) || ONE < Double.valueOf(value))) {
				errors.add(getFailedValidation(fieldName, value, "Value must be between 0 to 1", ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else if (!isNumeric) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else if (isNullCheck) {
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used Child Only Plan ID.
	 * Rule: If child only offering = Allows Adult-only, then this field is required
	 * Must be valid plan ID from this benefits package but not the current row's plan id.
	 * Format - 12345VA0011234 (plan id format)
	 * Child only plan is on same benefits package as adult plan.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	/*protected void validateChildOnlyPlanID(ExcelCellVO excelCellVO,
			List<FailedValidation> errors) {

		String fieldName = "Child Only Plan ID";
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();

			if (!isValidLength(LEN_HIOS_PLAN_ID, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_HIOS_PLAN_ID, ECODE_DEFAULT, templateName));
			}
			else {

				if (!isValidPattern(PATTERN_PLAN_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, templateName));
				}
			}
		}
		else {
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, templateName));
		}
	}*/

	/**
	 * Method is used to validate Stand Alone Dental Only.
	 *
	 * @param planAttributeVO, Object of PlanAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateStandAloneDentalPlan(PlanAttributeVO planAttributeVO,
			List<FailedValidation> errors) {

		String fieldName = null;

		if (isDentalOnlyInd) {
			fieldName = "EHB Apportionment for Pediatric Dental";
			LOGGER.debug(MSG_PLAN_ATTR + fieldName);
			//validateAmount(fieldName, planAttributeVO.getEhbApportionmentForPediatricDental(), errors, true);
			validateEhbApportionmentForPediatricDental(fieldName, planAttributeVO.getEhbApportionmentForPediatricDental(), errors);
			fieldName = "Guaranteed Vs EstimatedRate";
			LOGGER.debug(MSG_PLAN_ATTR + fieldName);
			String value = validatePlanAttributes(fieldName, planAttributeVO.getGuaranteedVsEstimatedRate(), errors);
			LOGGER.debug("Validation Stand Alone Dental Only: Guaranteed Vs EstimatedRate : "+value);
			if (StringUtils.isNotBlank(value)
					&& !planValidatorOptions.getRates(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
	}

	/**
	 * @param fieldName
	 * @param ehbApptForPediatricDental
	 * @param errors
	 */
	abstract protected void validateEhbApportionmentForPediatricDental(String fieldName, ExcelCellVO ehbApptForPediatricDental, List<FailedValidation> errors);

	/**
	 * This method is used for validating plan dates
	 * Rule: Plan dates for Individual Market.
	 *
	 * @param planAttributeVO, Object of PlanAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validatePlanDates(PlanAttributeVO planAttributeVO,
			List<FailedValidation> errors) {

		String fieldName = "Plan Effective Date";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		validatePlanDate(fieldName, FIRST_DAY, FIRST_MONTH, planAttributeVO.getPlanEffectiveDate(), errors);

		/*LOGGER.debug("Validation Plan Dates : Plan Expiration Date");
		String expirationDate = validatePlanDate("Plan Expiration Date", LastDay, LastMonth,
				planAttributeVO.getPlanExpirationDate(), errors);*/

		/*if (StringUtils.isNotBlank(effectiveDate) && StringUtils.isNotBlank(expirationDate)) {

			if (!validateEffectiveDate(effectiveDate, expirationDate)) {
				errors.add(getFailedValidation("Plan Effective Date and Plan Expiration Date",
						effectiveDate + " and " + expirationDate,
						"Plan Effective Date for a year must be before Plan Expiration Date",
						ECODE_DEFAULT, templateName));
			}
		}*/
	}

	/**
	 * Method is used Child Plan Effective Date.
	 * Rule:
	 * 1) Plan effective date for Individual Market must be January 1 for a QHP Certification year for QHPs only
	 * 2) Plan effective date  for a year must  be before Plan Expiration Date.
	 * 3) Individual Market expiration date must be December 31 of QHP Certification year for QHPs only.
	 *
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	protected String validatePlanDate(String fieldName, int iDay, int iMonth,
			ExcelCellVO excelCellVO, List<FailedValidation> errors) {

		//String xPath = StringUtils.EMPTY;
		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			LOGGER.debug("Validation Plan Attributes: Plan Date : "+value);
			boolean isIndividualMarket = false;

			if (MARKET_COVERAGE_TYPE_INDIVIDUAL.equalsIgnoreCase(marketCoverageType)) {
				isIndividualMarket = true;
			}

			boolean validateDate = validateDate(value, PLAN_DATE_FORMATTER);

			if (validateDate &&
					!validateGivenMonthAndDate(value, iDay, iMonth, isIndividualMarket)) {

				String message = null;

				if (iMonth == FIRST_MONTH) {
					message = "Plan effective date for Individual Market must be January 1 for a QHP Certification year for QHPs only.";
				}
				else if (iMonth == LAST_MONTH) {
					message = "Individual Market expiration date must be December 31 of QHP Certification year for QHPs only.";
				}
				else {
					message = EMSG_INVALID_DATE;
				}
				errors.add(getFailedValidation(fieldName, value, message, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
			else if (!validateDate) {
				errors.add(getFailedValidation(fieldName, value, EMSG_INVALID_DATE, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
		}
		else {
			LOGGER.debug("Validation Plan Attributes: Plan Date : "+value);
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		return value;
	}

	/**
	 * Method is used validate URLs.
	 *
	 * @param fieldName, Field Name
	 * @param xPath, Path of template Attribute
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateURL(String fieldName, ExcelCellVO excelCellVO,
			List<FailedValidation> errors) {

		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {
			value = StringUtils.trim(excelCellVO.getCellValue());

			if (!isValidPattern(PATTERN_URL, value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_URL, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
	}

	/**
	 * Method is used validate Option Value [Yes/No].
	 *
	 * @param fieldName, Field Name
	 * @param xPath, Path of template Attribute
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @param isNullCheck, Is Null Check
	 * @param Cell Value of ExcelCellVO object
	 */
	protected String validateOptionValue(String fieldName,
			ExcelCellVO excelCellVO, List<FailedValidation> errors,
			boolean isNullCheck) {

		String value = null;
		LOGGER.debug(MSG_PLAN_ATTR_VALIDATION+value);
		if (null != excelCellVO && StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();

			if (!planValidatorOptions.getOptionValue(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
		}
		else if (isNullCheck) {
			LOGGER.debug(MSG_PLAN_ATTR_VALIDATION+value);
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		return value;
	}

	protected boolean validatePlanId(ExcelCellVO excelCellVO, List<FailedValidation> errors, boolean isNullCheck) {
		String value = null;
		String fieldName = "Variance Plan ID";
		boolean isOffExchange = false;
		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			String[] planIdComp = value.split("-");

			if(null != planIdComp && planIdComp.length == 2 && value.length() == (LEN_HIOS_PLAN_ID + LEN_PLAN_VARIANCE + 1)) {
				if (!isValidLength(LEN_HIOS_PLAN_ID, planIdComp[0])) {
					errors.add(getFailedValidation(fieldName + " Prefix", value, EMSG_LENGTH + LEN_HIOS_PLAN_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else {
					if (!isValidLength(LEN_PLAN_VARIANCE, planIdComp[1])) {
						errors.add(getFailedValidation(fieldName + " Variation", value, EMSG_LENGTH + LEN_PLAN_VARIANCE, ECODE_DEFAULT, TEMPLATE_NAME));
					}
					isOffExchange = planIdComp[1].equals(SerffConstants.OFF_EXCHANGE_VARIANT_SUFFIX);
				}
			} else {
				errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT_MISSMATCHED, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else if (isNullCheck) {
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		return isOffExchange;
	}


	/**
	 * Method is used to CSR Variation Type.
	 * Format: 00 through 06
	 *
	 * @param excelCellVO, object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 */
	/*protected void validationCSRVariantType(ExcelCellVO excelCellVO,
			List<FailedValidation> errors) {

		String value = null;
		String fieldName = "CSR Variation Type";

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value =  excelCellVO.getCellValue();

			if (StringUtils.isNotBlank(value)) {

				if (!isValidPattern(PATTERN_VARIANT_COMPONENT_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_INVALID_VARIANT_ID, ECODE_DEFAULT, templateName));
				}
			}
			else {
				errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, templateName));
			}
		}
		else {
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, templateName));
		}
	}*/

	/**
	 * Method is used validate Amount field.
	 *
	 * @param amount, amount in double
	 * @param fieldName, field Name
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @param isNullCheck, Is Null Check
	 */
	protected void validateAmountEHBBenefits(String fieldName,
			ExcelCellVO excelCellVO, List<FailedValidation> errors, boolean isNullCheck) {

		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue();
			value = value.replaceAll(COMMA, StringUtils.EMPTY).replaceAll(DOLLAR, StringUtils.EMPTY);
			boolean isNumeric = validateNumeric(value);

			if (isNumeric && Double.valueOf(value) < 0) {
				errors.add(getFailedValidation(fieldName, value, EMSG_COST_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
			else if (!isNumeric && !planValidatorOptions.getListValues(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC + " or Not Applicable", ECODE_DEFAULT, TEMPLATE_NAME));
			}

			/*Implement as per the Business reply
			if (StringUtils.isNotBlank(value)) {

				if (!planValidatorOptions.getListValues(value)
						&& Integer.valueOf(value) >= amount) {
					errors.add(getFailedValidation(fieldName, value, EMSG_AMOUNT_VALUE + amount, ECODE_DEFAULT, templateName));
				}
			}*/
		}
		else if (isNullCheck) {
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used validate Family Amount field.
	 *
	 * @param amount, amount in double
	 * @param fieldName, field Name
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @param isNullCheck, Is Null Check
	 */
	protected void validateFamilyAmount(String fieldName,
			ExcelCellVO excelCellVO, List<FailedValidation> errors, boolean isNullCheck) {

		String value = null;

		if (null != excelCellVO
				&& StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			value = excelCellVO.getCellValue().trim();
			value = value.replaceAll(COMMA, StringUtils.EMPTY).replaceAll(DOLLAR, StringUtils.EMPTY);
			boolean isNumeric = validateNumeric(value);

			if (isNumeric && Double.valueOf(value) < 0) {
				errors.add(getFailedValidation(fieldName, value, EMSG_COST_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
				value = null;
			}
			else if (!isNumeric && value.indexOf("|") > -1) {

				String familyData[] = value.split("\\|");

				if (null != familyData && 2 == familyData.length) {
					validateFamilyAmount(fieldName, familyData[0].toLowerCase(), FAMILY_PER_PERSON, errors);
					validateFamilyAmount(fieldName, familyData[1].toLowerCase(), FAMILY_PER_GROUP, errors);
				}
				else {
					errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC + " for per person/per group", ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
			else if (!isNumeric && !planValidatorOptions.getListValues(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC + " or Not Applicable", ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else if (isNullCheck) {
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used validate Family Amount field for per person/per group.
	 */
	private void validateFamilyAmount(String fieldName, String familyData, String familyAttr, List<FailedValidation> errors) {

		boolean isValid = true;

		if (StringUtils.isNotBlank(familyData)
				&& familyData.toLowerCase().indexOf(familyAttr) > -1) {
			String perPersonamount = familyData.toLowerCase().replaceAll(familyAttr, StringUtils.EMPTY).trim();

			if (!planValidatorOptions.getListValues(perPersonamount)) {
				perPersonamount = perPersonamount.replaceAll(SerffConstants.SPACE, StringUtils.EMPTY);
				perPersonamount = perPersonamount.replaceAll(COMMA, StringUtils.EMPTY).replaceAll(DOLLAR, StringUtils.EMPTY);
	
				if (!validateNumeric(perPersonamount) || Double.valueOf(perPersonamount) < 0) {
					isValid = false;
				}
			}
		}
		else {
			isValid = false;
		}

		if (!isValid) {
			errors.add(getFailedValidation(fieldName, familyData, EMSG_NUMERIC + " for " + familyAttr, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used validate Amount/Percentage fields for Copay/Coinsurance.
	 *
	 * @param fieldName, field Name
	 * @param excelCellVO, Object of ExcelCellVO
	 * @param errors, List Object of FailedValidation
	 * @param isNullCheck, Is Null Check
	 * @param isCoinsurance, is Coinsurance Check
	 */
	protected void validateCopayCoinsurance(String fieldName,
			ExcelCellVO excelCellVO, List<FailedValidation> errors,
			boolean isNullCheck, boolean isCoinsurance) {

		String value = null;

		if (null != excelCellVO && StringUtils.isNotBlank(excelCellVO.getCellValue())) {

			String doubleValueFormat = excelCellVO.getCellValue();
			value = excelCellVO.getCellValue();
			
			/*
			 * There is a possibility of value like "50% up to $150 copay"
			 * if (value.indexOf(DOLLARSIGN) > -1 && value.indexOf(PERCENTAGE) > -1) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC_OR_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
				return;
			}
			else*/ 
			if (value.indexOf(DOLLARSIGN) > -1) {
				doubleValueFormat = value.replaceAll(COMMA, StringUtils.EMPTY).replaceAll(DOLLAR, StringUtils.EMPTY).trim();
			} else if (value.indexOf(PERCENTAGE) > -1) {
				doubleValueFormat = value.replaceAll(COMMA, StringUtils.EMPTY).replaceAll(PERCENTAGE, StringUtils.EMPTY).trim();
			}
			
			boolean flag = validateAmountCopayCoinsurance(doubleValueFormat, fieldName, value, isCoinsurance, errors);
			if (!flag && !planValidatorOptions.getCopayCoinsurance(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC_OR_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			doubleValueFormat = null;
			value = null;
		}
		else if (isNullCheck) {
			errors.add(getFailedValidation(fieldName, value, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used to validate amount for copay and percentage for coinsurance.
	 * @return Boolean value
	 */
	protected boolean validateAmountCopayCoinsurance(String doubleValueFormat, String fieldName, String value,
			boolean isCoinsurance, List<FailedValidation> errors) {

		boolean flag = false;

		if (validateNumeric(doubleValueFormat)) {

			double doubleValue = Double.valueOf(doubleValueFormat);
			flag = true;

			// Validate percentage if value is Coinsurance
			if(isCoinsurance && (doubleValue < 0d || doubleValue > HUNDRED)) {
				flag = false;
				errors.add(getFailedValidation(fieldName, value, EMSG_PERCENTAGE_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			// Validate amount if value is Copay
			else if (!isCoinsurance && doubleValue < 0d) {
				flag = false;
				errors.add(getFailedValidation(fieldName, value, EMSG_COST_VALUE, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		return flag;
	}

	/**
	 * Utility method for date validation
	 */
	protected static boolean validateGivenMonthAndDate(String sDate, int iDay, int iMonth, boolean isIndividualMarket) {

		boolean validDate = false;
		LocalDate date = null;

		try {
			date = PLAN_DATE_FORMATTER.parseLocalDate(sDate);

			if (isIndividualMarket && date.getMonthOfYear() == iMonth && iDay == date.getDayOfMonth()) {
				validDate = true;
			}
			else if (!isIndividualMarket) {
				validDate = true;
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception occurred while validating Month and Date", e);
		}
		return validDate;
	}
	

	/**
	 * This method is used for validating plan attributes fields
	 * @param planAttributeVO, Object of PlanAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validatePlanAttributes(PlanAttributeVO planAttributeVO, List<FailedValidation> errors){

		String fieldName = null;
		String value = null;
	
		fieldName = "Plan Type";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		value = validatePlanAttributes(fieldName, planAttributeVO.getPlanType(), errors);

		if (StringUtils.isNotBlank(value)
				&& !planValidatorOptions.getPlanType(value)) {
			errors.add(getFailedValidation(fieldName, value, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		fieldName = "Level of Coverage/Metal Level";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		metalLevel = validatePlanAttributes(fieldName, planAttributeVO.getMetalLevel(), errors);

		if (StringUtils.isNotBlank(metalLevel)) {

			if (!planValidatorOptions.getCoverageLevel(metalLevel, isDentalOnlyInd)) {
				errors.add(getFailedValidation(fieldName, metalLevel, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			fieldName = "EHB Percent of Total Premium";
			LOGGER.debug(MSG_PLAN_ATTR + fieldName);
			validatePercentAsFraction(fieldName, planAttributeVO.getEhbPercentPremium(), errors, false);
		}
		fieldName = "Design Type";
		validateDesignType(fieldName, planAttributeVO.getPlanDesignType(), errors);

		fieldName = "Unique Plan Design";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		uniquePlanDesign = validateOptionValue(fieldName, planAttributeVO.getUniquePlanDesign(), errors, !isDentalOnlyInd);

		fieldName = "QHP/Non QHP";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		value = validatePlanAttributes(fieldName, planAttributeVO.getQhpOrNonQhp(), errors);

		if (StringUtils.isNotBlank(value)
				&& !planValidatorOptions.getPlanHealth(value)) {
			errors.add(getFailedValidation(fieldName, value, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		fieldName = "Notice Required for Pregnancy";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		validateOptionValue(fieldName, planAttributeVO.getInsurancePlanPregnancyNoticeReqInd(), errors, !isDentalOnlyInd);

		fieldName = "Is a Referral Required for Specialist";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		value = validateOptionValue(fieldName, planAttributeVO.getIsSpecialistReferralRequired(), errors, !isDentalOnlyInd);

		if (YES.equalsIgnoreCase(value)) {
			fieldName = "Specialist Requiring a Referral";
			LOGGER.debug(MSG_PLAN_ATTR + fieldName);
			validatePlanAttributes(fieldName, planAttributeVO.getHealthCareSpecialistReferralType(), errors);
		}

		// Validate HSC fields
//		validateHSC(planAttributeVO, errors);

		fieldName = "Child-Only Offering";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		value = validatePlanAttributes(fieldName, planAttributeVO.getChildOnlyOffering(), errors);

		if (StringUtils.isNotBlank(value)
				&& !planValidatorOptions.getChildOffering(value)) {
			errors.add(getFailedValidation(fieldName, value, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
		}
		/*else if (value.equalsIgnoreCase(CHILD_ONLY_OFFERING_ALLOWS_ADULT_ONLY)) {
			LOGGER.info("Validation Plan Attributes: Child Only Plan ID");
			validateChildOnlyPlanID(planAttributeVO.getChildOnlyPlanID(), errors);
		}*/
		fieldName = "Wellness Program Offered";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		validateOptionValue(fieldName, planAttributeVO.getIsWellnessProgramOffered(), errors, !isDentalOnlyInd);

		// LOGGER.info("Validation Plan Attributes: Disease Management Programs Offered");
		// fieldName = "Disease Management Programs Offered";
		// value = validatePlanAttributes(fieldName, planAttributeVO.getIsDiseaseMgmtProgramsOffered(), errors);

		/*if (StringUtils.isNotBlank(value)) {

			if (value.indexOf(',') > -1) {

				String diseaseValues[] = value.split(",");

				for (String diseaseValue : diseaseValues) {

					if(!planValidatorOptions.getDiseaseManagement(diseaseValue)) {
						errors.add(getFailedValidation(fieldName, diseaseValue, EMSG_EMPTY_INVALID, ECODE_DEFAULT, templateName));
					}
				}
			}
			else if(!planValidatorOptions.getDiseaseManagement(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_EMPTY_INVALID, ECODE_DEFAULT, templateName));
			}
		}*/
	}

	/**
	 * Method is used validate HSC.
	 *
	 * @param planAttributeVO, Object of PlanAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateHSC(HsaVO hsaVO, List<FailedValidation> errors) {

		String fieldName = "H.S.A. - Eligible";
		String value = null;
		
		validateOptionValue(fieldName, hsaVO.getHsaEligibility(), errors, !isDentalOnlyInd);

		if (MARKET_COVERAGE_TYPE_SHOP.equalsIgnoreCase(marketCoverageType)) {
			fieldName = "HSA/HRA Employer Contribution";
			LOGGER.debug(MSG_HSA + fieldName);
			value = validatePlanAttributes(fieldName, hsaVO.getEmployerHSAHRAContributionIndicator(), errors);

			if (YES.equalsIgnoreCase(value)) {
				fieldName = "HSA/HRA Employer Contribution Amount";
				LOGGER.debug(MSG_HSA + fieldName);
				validateAmount(fieldName, hsaVO.getEmpContributionAmountForHSAOrHRA(), errors, true);
			}
		}
	}

	/**
	 * This method is used for validating URLs
	 * @param planAttributeVO, Object of PlanAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateURLs(UrlVO urlVO,
			List<FailedValidation> errors) {

		String fieldName = null;
		fieldName = "URL for Summary of Benefits and Coverage";
		LOGGER.debug(MSG_URLS + fieldName);
		validateURL(fieldName, urlVO.getSummaryBenefitAndCoverageURL(), errors);

		fieldName = "Plan Brochure";
		LOGGER.debug(MSG_URLS + fieldName);
		validateURL(fieldName, urlVO.getPlanBrochure(), errors);
	}

	/**
	 * This method is used for validating GeographicCoverage fields
	 *
	 * @param planAttributeVO, Object of PlanAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateGeographicCoverage(PlanAttributeVO planAttributeVO,
			List<FailedValidation> errors) {

		String value = null;
		String fieldName = "Out of Country Coverage";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		value = validatePlanAttributes(fieldName, planAttributeVO.getOutOfCountryCoverage(), errors);

		if (YES.equalsIgnoreCase(value)) {
			fieldName = "Out of Country Coverage Description";
			LOGGER.debug(MSG_PLAN_ATTR + fieldName);
			validatePlanAttributes(fieldName, planAttributeVO.getOutOfCountryCoverageDescription(), errors);
		}
		fieldName = "Out of Service Area Coverage";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		value = validatePlanAttributes(fieldName, planAttributeVO.getOutOfServiceAreaCoverage(), errors);

		if (YES.equalsIgnoreCase(value)) {
			fieldName = "Out of Service Area Coverage Description";
			LOGGER.debug(MSG_PLAN_ATTR + fieldName);
			validatePlanAttributes(fieldName, planAttributeVO.getOutOfServiceAreaCoverageDescription(), errors);
		}
		fieldName = "National Network";
		LOGGER.debug(MSG_PLAN_ATTR + fieldName);
		validatePlanAttributes(fieldName, planAttributeVO.getNationalNetwork(), errors);
	}
	
	/**
	 * Method is used to validate Benefits informations.
	 * @param benefitAttributeVO, Object of BenefitAttributeVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateBenefitInfo(BenefitAttributeVO benefitAttributeVO,
			List<FailedValidation> errors) {

		String fieldName = "Benefits";
		String value = null;
		LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
		validatePlanAttributes(fieldName, benefitAttributeVO.getBenefitTypeCode(), errors);

		/* fieldName = "EHB";
		LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
		String isEHB = validateOptionValue(fieldName, benefitAttributeVO.getIsEHB(), errors, true);*/
		boolean isNullCheck = false;

		/*if (StringUtils.isNotBlank(isEHB) && isEHB.equalsIgnoreCase(YES)) {
			isNullCheck = true;
		}*/
		
		fieldName ="Is state Mandate";
		LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
		value = validateOptionValue(fieldName, benefitAttributeVO.getIsStateMandate(), errors, isNullCheck);
		
		fieldName = "Quantitative Limit on Service";
		LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
		value = validateOptionValue(fieldName, benefitAttributeVO.getServiceLimit(), errors, isNullCheck);

		/*if (isNullCheck) {

			fieldName = "Is this Benefit Covered";
			LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
			validatePlanAttributes(fieldName, benefitAttributeVO.getIsBenefitCovered(), errors);

			if (StringUtils.isNotBlank(value) && value.equalsIgnoreCase(YES)) {
				fieldName = "Limit Quantity";
				LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
				validatePlanAttributes(fieldName, benefitAttributeVO.getQuantityLimit(), errors);
			}
		}*/

		isNullCheck = false;
		if (null != benefitAttributeVO.getIsBenefitCovered()
				&& StringUtils.isNotBlank(benefitAttributeVO.getIsBenefitCovered().getCellValue())) {
			fieldName = "Is this Benefit Covered";
			LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
			value = benefitAttributeVO.getIsBenefitCovered().getCellValue();

			if(!planValidatorOptions.getBenefitCoverage(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			else if (BENEFIT_COVERAGE_COVERED.equalsIgnoreCase(value)) {
				isNullCheck = true;
			}
		}

		/*fieldName = "Subject to Deductible (Tier 1)";
		LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
		validateOptionValue(fieldName, benefitAttributeVO.getSubjectToDeductibleTier1(), errors, isNullCheck);
		
		fieldName = "Subject to Deductible (Tier 2)";
		LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
		validateOptionValue(fieldName, benefitAttributeVO.getSubjectToDeductibleTier2(), errors, isNullCheck);*/

		fieldName = "Excluded from In Network MOOP";
		LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
		validateOptionValue(fieldName, benefitAttributeVO.getExcludedInNetworkMOOP(), errors, isNullCheck);
	
		fieldName = "Excluded from Out of Network MOOP";
		LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
		validateOptionValue(fieldName, benefitAttributeVO.getExcludedOutOfNetworkMOOP(), errors, isNullCheck);

		if (null != benefitAttributeVO.getQuantityLimit()
				&& StringUtils.isNotBlank(benefitAttributeVO.getQuantityLimit().getCellValue())) {
			fieldName = "Limit Quantity";
			LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
			value = benefitAttributeVO.getQuantityLimit().getCellValue();

			if (!StringUtils.isNumeric(value) || Integer.valueOf(value) < 1) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC_ZERO, ECODE_DEFAULT, TEMPLATE_NAME));
			}
			/*else {
				LOGGER.debug("Validation Geographic Coverage: Limit Unit");
				fieldName = "Limit Unit";
				//validatePlanAttributes(fieldName, benefitAttributeVO.getUnitLimit(), errors);
			}*/
		}

		if (null != benefitAttributeVO.getUnitLimit()
				&& StringUtils.isNotBlank(benefitAttributeVO.getUnitLimit().getCellValue())) {
			fieldName = "Limit Unit";
			LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
			value = benefitAttributeVO.getUnitLimit().getCellValue();

			/*if(!planValidatorOptions.getLimitUnitCatogery1(value)
					&& !planValidatorOptions.getLimitUnitCatogery2(value)) {
				errors.add(getFailedValidation(fieldName, value, EMSG_EMPTY_INVALID, ECODE_DEFAULT, templateName));
			}*/
		}

		if (null != benefitAttributeVO.getMinimumStay()
				&& StringUtils.isNotBlank(benefitAttributeVO.getMinimumStay().getCellValue())) {
			fieldName = "Minimum Stay";
			LOGGER.debug(MSG_PLAN_BENEFITS + fieldName);
			value = benefitAttributeVO.getMinimumStay().getCellValue();

			if (!StringUtils.isNumeric(value) || Integer.valueOf(value) < 1) {
				errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC_ZERO, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
	}

	/**
	 * This method is used for validating cost share variations fields
	 * @param costShareVarianceVO, Object of CostShareVarianceVO
	 * @param errors, List Object of FailedValidation
	 * @return 
	 */
	private boolean validateCostShareVariations(
			CostShareVarianceVO costShareVarianceVO,
			List<FailedValidation> errors) {

		String fieldName = null;
		LOGGER.debug(MSG_COST_SHARE + "PlanId");
		//validationCSRVariantType(costShareVarianceVO.getCsrVariationType(), errors);
		boolean isOffExchange = validatePlanId(costShareVarianceVO.getPlanId(), errors, true);

		fieldName = "Issuer Actuarial Value";
		LOGGER.debug(MSG_COST_SHARE + fieldName);
		
		validatePlanCSVMarketingName(costShareVarianceVO, errors);
		
		if (YES.equalsIgnoreCase(uniquePlanDesign)
				&& !planValidatorOptions.getCatastrophicPlan(metalLevel)) {
			validatePercentage(fieldName, getIssuerActuarialValue(costShareVarianceVO), errors, true);
		}
		else {
			validatePercentage(fieldName, getIssuerActuarialValue(costShareVarianceVO), errors, false);
		}

//		fieldName = "AV Calculator Output Number";
//		LOGGER.debug(MSG_COST_SHARE + fieldName);
//		validatePercentage(fieldName, costShareVarianceVO.getAvCalculatorOutputNumber(), errors, !isDentalOnlyInd );
		
		fieldName = "Multiple In Network Tiers";
		LOGGER.debug(MSG_COST_SHARE + fieldName);
		multipleInNetworkTiers = validateOptionValue(fieldName, costShareVarianceVO.getMultipleProviderTiers(), errors, true);

		fieldName = "1st Tier Utilization";
		LOGGER.debug(MSG_COST_SHARE + fieldName);
		validatePercentage(fieldName, costShareVarianceVO.getFirstTierUtilization(), errors, true);

		fieldName = "2nd Tier Utilization";
		LOGGER.debug(MSG_COST_SHARE + fieldName);
		if (YES.equalsIgnoreCase(multipleInNetworkTiers)) {
			validatePercentage(fieldName, costShareVarianceVO.getSecondTierUtilization(), errors, true);
		}
		else {
			validatePercentage(fieldName, costShareVarianceVO.getSecondTierUtilization(), errors, false);
		}

		// Iterating list of Benefits Cost Sharing
		for (PlanDeductibleVO planDeductibleList : costShareVarianceVO.getPlanDeductibleList().getPlanDeductible()) {

			if (null == planDeductibleList) {
				continue;
			}
			validatePlanDeductible(planDeductibleList, errors);
		}

		// Iterating list of Benefits Cost Sharing
		if (null != costShareVarianceVO.getServiceVisitList()) {

			for (ServiceVisitVO serviceVisitVO : costShareVarianceVO.getServiceVisitList().getServiceVisit()) {

				if (null == serviceVisitVO) {
					continue;
				}
				validateBenefitsCostSharing(serviceVisitVO, errors);
			}
		}
		else if(!isOffExchange) {
			errors.add(getFailedValidation("ServiceVisitList", EMPTY_VALUE, "Plan is rejected due to ServiceVisitList is completely empty.", ECODE_DEFAULT, TEMPLATE_NAME));
		}
		
		// Validate HSA Data
		validateHSC(costShareVarianceVO.getHsa(), errors);
		
		// Validate URL Data - commented out for HIX-109333
		//validateURLs(costShareVarianceVO.getUrl(), errors);
		
		// Iterating Moop list
		if (null != costShareVarianceVO.getMoopList()) {

			for (MoopVO moopVo : costShareVarianceVO.getMoopList().getMoop()) {
				if (null == moopVo) {
					continue;
				}
				validateMoopsCostSharing(moopVo, errors);
			}
		}

		// Validate SBC data
		if (null != costShareVarianceVO.getSbc()) {
			final boolean isWholeNumberCheck = true;
			validateSBCData(costShareVarianceVO.getSbc(), errors, !isOffExchange, isWholeNumberCheck);
		}
		return isOffExchange;
	}	


	/**
	 * Method is used to validate SBC data.
	 *
	 * @param sbcVO, Object of SBCVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateSBCData(SBCVO sbcVO, List<FailedValidation> errors, boolean isNullCheck, boolean isWholeNumberCheck) {

		String fieldName = "Having a Baby Coinsurance";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingBabyCoInsurance(), errors, isNullCheck, isWholeNumberCheck);

		fieldName = "Having a Baby Copayment";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingBabyCoPayment(), errors, isNullCheck, isWholeNumberCheck);

		fieldName = "Having a Baby Deductible";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingBabyDeductible(), errors, isNullCheck, isWholeNumberCheck);

		fieldName = "Having a Baby Limit";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingBabyLimit(), errors, isNullCheck, isWholeNumberCheck);

		fieldName = "Having Diabetes Coinsurance";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingDiabetesCoInsurance(), errors, isNullCheck, isWholeNumberCheck);

		fieldName = "Having Diabetes Copayment";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingDiabetesCopay(), errors, isNullCheck, isWholeNumberCheck);

		fieldName = "Having Diabetes Deductible";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingDiabetesDeductible(), errors, isNullCheck, isWholeNumberCheck);

		fieldName = "Having Diabetes Limit";
		LOGGER.debug(MSG_SBC + fieldName);
		validateAmount(fieldName, sbcVO.getHavingDiabetesLimit(), errors, isNullCheck, isWholeNumberCheck);
	}

	/**
	 * This method is used for validating benefits cost sharing fields
	 *
	 * @param serviceVisitVO, Object of ServiceVisitVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validatePlanDeductible(PlanDeductibleVO planDeductibleList,
			List<FailedValidation> errors) {

		boolean isNullCheck = false;
//		final double amountIndividual = 6400;
//		final double amountFamily = 12800;

		String fieldName = "Deductible - In Network - Individual";
		LOGGER.debug(MSG_DEDUCTIBLE_LIST + fieldName);
		validateAmountEHBBenefits(fieldName, planDeductibleList.getInNetworkTier1Individual(), errors, isNullCheck);

		fieldName = "Deductible - In Network - Family";
		LOGGER.debug(MSG_DEDUCTIBLE_LIST + fieldName);
		validateFamilyAmount(fieldName, planDeductibleList.getInNetworkTier1Family(), errors, isNullCheck);

		fieldName = "Deductible - Medical EHB Default Coinsurance In Network (Tier 1)";
		LOGGER.debug(MSG_DEDUCTIBLE_LIST + fieldName);
		validatePercentage(fieldName, planDeductibleList.getCoinsuranceInNetworkTier1(), errors, isNullCheck);

		fieldName = "Deductible - Medical EHB Default Coinsurance In Network (Tier 2)";
		LOGGER.debug(MSG_DEDUCTIBLE_LIST + fieldName);
		validatePercentage(fieldName, planDeductibleList.getCoinsuranceInNetworkTier2(), errors, isNullCheck);

		fieldName = "Deductible - In Network Tier 2 - Individual";
		LOGGER.debug(MSG_DEDUCTIBLE_LIST + fieldName);
		validateAmountEHBBenefits(fieldName, planDeductibleList.getInNetworkTierTwoIndividual(), errors, isNullCheck);

		fieldName = "Deductible - In Network Tier 2 - Family";
		LOGGER.debug(MSG_DEDUCTIBLE_LIST + fieldName);
		validateFamilyAmount(fieldName, planDeductibleList.getInNetworkTierTwoFamily(), errors, isNullCheck);

		fieldName = "Deductible - Out of Network - Individual";
		LOGGER.debug(MSG_DEDUCTIBLE_LIST + fieldName);
		validateAmountEHBBenefits(fieldName, planDeductibleList.getOutOfNetworkIndividual(), errors, isNullCheck);

		fieldName = "Deductible - Out of Network - Family";
		LOGGER.debug(MSG_DEDUCTIBLE_LIST + fieldName);
		validateFamilyAmount(fieldName, planDeductibleList.getOutOfNetworkFamily(), errors, isNullCheck);

		fieldName = "Deductible - Combined In/Out of Network - Individual";
		LOGGER.debug(MSG_DEDUCTIBLE_LIST + fieldName);
		validateAmountEHBBenefits(fieldName, planDeductibleList.getCombinedInOrOutNetworkIndividual(), errors, isNullCheck);

		fieldName = "Deductible - Combined In/Out of Network - Family";
		LOGGER.debug(MSG_DEDUCTIBLE_LIST + fieldName);
		validateFamilyAmount(fieldName, planDeductibleList.getCombinedInOrOutNetworkFamily(), errors, isNullCheck);
	}

	/**
	 * This method is used for validating benefits cost sharing fields
	 *
	 * @param serviceVisitVO, Object of ServiceVisitVO
	 * @param errors, List Object of FailedValidation
	 */
	protected void validateBenefitsCostSharing(ServiceVisitVO serviceVisitVO, List<FailedValidation> errors) {

		boolean isCoinsurance = true;
		boolean isNullCheck = true;

		String fieldName = "Copay - In Network (Tier 1)";
		LOGGER.debug(MSG_SERVICE_VISIT + fieldName);
		validateCopayCoinsurance(fieldName, serviceVisitVO.getCopayInNetworkTier1(), errors, isNullCheck, !isCoinsurance);

		fieldName = "Copay - Out of Network";
		LOGGER.debug(MSG_SERVICE_VISIT + fieldName);
		validateCopayCoinsurance(fieldName, serviceVisitVO.getCopayOutOfNetwork(), errors, isNullCheck, !isCoinsurance);

		fieldName = "Coinsurance - In Network (Tier 1)";
		LOGGER.debug(MSG_SERVICE_VISIT + fieldName);
		validateCopayCoinsurance(fieldName, serviceVisitVO.getCoInsuranceInNetworkTier1(), errors, isNullCheck, isCoinsurance);

		fieldName = "Coinsurance - Out of Network";
		LOGGER.debug(MSG_SERVICE_VISIT + fieldName);
		validateCopayCoinsurance(fieldName, serviceVisitVO.getCoInsuranceOutOfNetwork(), errors, isNullCheck, isCoinsurance);

		if (YES.equalsIgnoreCase(multipleInNetworkTiers)) {

			fieldName = "Copay - In Network (Tier 2)";
			LOGGER.debug(MSG_SERVICE_VISIT + fieldName);
			validateCopayCoinsurance(fieldName, serviceVisitVO.getCopayInNetworkTier2(), errors, isNullCheck, !isCoinsurance);

			fieldName = "Coinsurance - In Network (Tier 2)";
			LOGGER.debug(MSG_SERVICE_VISIT + fieldName);
			validateCopayCoinsurance(fieldName, serviceVisitVO.getCoInsuranceInNetworkTier2(), errors, isNullCheck, isCoinsurance);
		}
	}

	protected void validateMoopsCostSharing(MoopVO moopVo, List<FailedValidation> errors) {

		String fieldName;
		boolean isNullCheck = false;

		if (YES.equalsIgnoreCase(multipleInNetworkTiers)) {
			isNullCheck = true;
			fieldName = "Moop - In Network Individual (Tier 2)";
			LOGGER.debug(MSG_MOOP + fieldName);
			validateAmountEHBBenefits(fieldName, moopVo.getInNetworkTier2IndividualAmount(), errors, isNullCheck);

			fieldName = "Moop - In Network family (Tier 2)";
			LOGGER.debug(MSG_MOOP + fieldName);
			validateFamilyAmount(fieldName, moopVo.getInNetworkTier2FamilyAmount(), errors, isNullCheck);
		}
		else {
			fieldName = "Moop - In Network family (Tier 2)";
			LOGGER.debug(MSG_MOOP + fieldName);
			validateFamilyAmount(fieldName, moopVo.getInNetworkTier2FamilyAmount(), errors, isNullCheck);
		}
		isNullCheck = false;
		fieldName = "Moop - In Network family (Tier 1)";
		LOGGER.debug(MSG_MOOP + fieldName);
		validateFamilyAmount(fieldName, moopVo.getInNetworkTier1FamilyAmount(), errors, isNullCheck);

		fieldName = "Moop - Out Of Network Family Amount";
		LOGGER.debug(MSG_MOOP + fieldName);
		validateFamilyAmount(fieldName, moopVo.getOutOfNetworkFamilyAmount(), errors, isNullCheck);

		fieldName = "Moop - Combined In Out Network Family Amount";
		LOGGER.debug(MSG_MOOP + fieldName);
		validateFamilyAmount(fieldName, moopVo.getCombinedInOutNetworkFamilyAmount(), errors, isNullCheck);
	}
}
