package com.serff.service.validation;

import static com.serff.util.SerffConstants.NEW_LINE;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.getinsured.serffadapter.vision.VisionExcelRow.VisionPlanDataRowEnum;
import com.getinsured.serffadapter.vision.VisionExcelRow.VisionPlanRatesRowEnum;
import com.getinsured.serffadapter.vision.VisionPlanDataVO;
import com.getinsured.serffadapter.vision.VisionPlanRatesVO;
import com.serff.util.SerffConstants;

/**
 *-----------------------------------------------------------------------------
 * HIX-59162 PHIX - Load and persist vision plans.
 *-----------------------------------------------------------------------------
 * 
 * This Class is used to validate Vision Plan Excel.
 * 
 * @author Bhavin Parmar
 * @since January 27, 2015
 */
@Component
public class VisionPlanDataValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(VisionPlanDataValidator.class);
	private static final String EMSG_LENGTH = " Value must have length ";
	private static final String EMSG_INVALID_URL = " Invalid URL." + NEW_LINE;
	private static final String EMSG_EMPTY = " Value should not be empty." + NEW_LINE;
	private static final String EMSG_TEXT_LENGTH_EXCEEDS = " Text length is greater than maximum length allowed." + NEW_LINE;
	private static final String EMSG_INVALID_STATE = " Invalid State." + NEW_LINE;
	private static final String EMSG_NUMERIC = " Value must be numeric." + NEW_LINE;
	//private static final String EMSG_INVALID_URL = " Invalid URL/ECM-ID." + NEW_LINE;
	private static final String EMSG_INVALID_DATE = " Invalid Start date format. Format must be DD/MM/YYYY." + NEW_LINE;
	private static final String OUT_OF_NETWORK_VALUE_MSG = " Must be Y/N " + NEW_LINE;
	private static int MAX_TEXT_LENGTH_FIVE_HUNDRED = 500;
	private static int MAX_TEXT_LENGTH_TWO_HUNDRED = 200;
	private static int MAX_TEXT_LENGTH_THOUSAND = 1000;
	private static int ISSUER_PLAN_NUMBER_LENGTH = 14;
	
	/**
	 * Method is used to validate require data from validateVisionExcelVO object before persistence.
	 * 
	 * @param hiosPlanNumber - Issuer plan number
	 * @param visionPlanDataVO - VisionPlanDataVO instance
	 * @return - Whether Vo having valid data to persist
	 */
	public boolean validateVisionExcelVO(String hiosPlanNumber, VisionPlanDataVO visionPlanDataVO) {
		
		LOGGER.debug("validateVisionExcelVO() Start");
		boolean isValid = true;
		
		try {
			
			if (StringUtils.isBlank(hiosPlanNumber) || null == visionPlanDataVO || null == visionPlanDataVO.getVisionPlanRatesVO()) {
				LOGGER.error("HIOSPlanNumber/VisionPlanDataVO/VisionPlanRatesVO is empty !!");
				visionPlanDataVO.setErrorMessages("HIOSPlanNumber [" + hiosPlanNumber + "] is not matching !! VisionPlanData/VisionPlanRates is NULL !!");
				return false;
			}
			
			StringBuilder errorMessages = new StringBuilder();
			LOGGER.debug("validateAMEExcelVO.getCarrierName(): " + visionPlanDataVO.getCarrierName());
			//5 digit HIOS ID
			validateCarrierHIOS(visionPlanDataVO);
			//State validation
			validateState(visionPlanDataVO.getState());
			//7 digit plan id
			validatePlanId(visionPlanDataVO);
			//Text fields
			validateStartDate(visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.START_DATE.getRowName());
			
			validateBlankData(visionPlanDataVO.getCarrierName(), visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.CARRIER_NAME.getRowName());
			validateBlankData(visionPlanDataVO.getPlanName() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.PLAN_NAME.getRowName());
			validateBlankData(visionPlanDataVO.getContactsCopayAttr(), visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.CONTACTS_COPAY_ATTR.getRowName());
			validateTextWithLength(visionPlanDataVO.getContactsCopayAttr(), visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.CONTACTS_COPAY_ATTR.getRowName(), MAX_TEXT_LENGTH_TWO_HUNDRED);
			validateInteger(visionPlanDataVO.getContactsCopayVal() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.CONTACTS_COPAY_VAL.getRowName());
			validateTextWithLength(visionPlanDataVO.getContactsDetails() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.CONTACTS_DETAILS.getRowName(), MAX_TEXT_LENGTH_FIVE_HUNDRED);
			validateBlankData(visionPlanDataVO.getEyeExamCopayAttr() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.EYE_EXAM_COPAY_ATTR.getRowName());
			validateTextWithLength(visionPlanDataVO.getEyeExamCopayAttr() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.EYE_EXAM_COPAY_ATTR.getRowName(), MAX_TEXT_LENGTH_TWO_HUNDRED);
			validateInteger(visionPlanDataVO.getEyeExamCopayVal() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.EYE_EXAM_COPAY_VAL.getRowName());
			validateTextWithLength(visionPlanDataVO.getEyeExamDetails() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.EYE_EXAM_DETAILS.getRowName(), MAX_TEXT_LENGTH_FIVE_HUNDRED);
			validateBlankData(visionPlanDataVO.getFramesCopayAttr() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.FRAMES_COPAY_ATTR.getRowName());
			validateTextWithLength(visionPlanDataVO.getFramesCopayAttr() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.FRAMES_COPAY_ATTR.getRowName(), MAX_TEXT_LENGTH_TWO_HUNDRED);
			validateInteger(visionPlanDataVO.getFramesCopayVal() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.FRAMES_COPAY_VAL.getRowName());
			validateTextWithLength(visionPlanDataVO.getFramesDetails() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.FRAMES_DETAILS.getRowName(), MAX_TEXT_LENGTH_FIVE_HUNDRED);
			validateBlankData(visionPlanDataVO.getGlassesCopayAttr() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.GLASSES_COPAY_ATTR.getRowName());
			validateTextWithLength(visionPlanDataVO.getGlassesCopayAttr() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.GLASSES_COPAY_ATTR.getRowName(), MAX_TEXT_LENGTH_TWO_HUNDRED);
			validateInteger(visionPlanDataVO.getGlassesCopayVal() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.GLASSES_COPAY_VAL.getRowName());
			validateTextWithLength(visionPlanDataVO.getGlassesDetails() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.GLASSES_DETAILS.getRowName(), MAX_TEXT_LENGTH_FIVE_HUNDRED);
			validateTextWithLength(visionPlanDataVO.getLaserVisionCorrectionDetails() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.LASER_VISION_CORRECTION_DETAILS.getRowName(), MAX_TEXT_LENGTH_FIVE_HUNDRED);
			validateOutOfnetworkAvailability(visionPlanDataVO);
			validateTextWithLength(visionPlanDataVO.getOutOfNetworkCoverage() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.OUT_OF_NETWORK_COVERAGE.getRowName(), MAX_TEXT_LENGTH_TWO_HUNDRED);
			validateTextWithLength(visionPlanDataVO.getPremiumPayment() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.PREMIUM_PAYMENT.getRowName(), MAX_TEXT_LENGTH_TWO_HUNDRED);
			validateTextWithLength(visionPlanDataVO.getSunglassesandExtraGlassesDetails() , visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.SUNGLASSESAND_EXTRA_GLASSES_DETAILS.getRowName(), MAX_TEXT_LENGTH_FIVE_HUNDRED);
			validateURL(visionPlanDataVO.getPlanBrochureURL(), visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.PLAN_BROCHURE_URL.getRowName());
			validateURL(visionPlanDataVO.getProviderNetworkURL(), visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.PROVIDER_NETWORK_URL.getRowName());
			validateTextWithLength(visionPlanDataVO.getProviderNetworkURL(), visionPlanDataVO, errorMessages, VisionPlanDataRowEnum.PROVIDER_NETWORK_URL.getRowName(), MAX_TEXT_LENGTH_THOUSAND);
			
			LOGGER.debug("Validations for VisionPlanRatesVO");
			
			VisionPlanRatesVO visionPlanRatesVO = visionPlanDataVO.getVisionPlanRatesVO();
						
			validateNumber(visionPlanRatesVO.getCouple(), visionPlanDataVO, errorMessages, VisionPlanRatesRowEnum.COUPLE.getRowName());
			validateNumber(visionPlanRatesVO.getIndividual(), visionPlanDataVO, errorMessages, VisionPlanRatesRowEnum.INDIVIDUAL.getRowName());
			validateNumber(visionPlanRatesVO.getOneAdultOneKid(), visionPlanDataVO, errorMessages, VisionPlanRatesRowEnum.ONE_ADULT_ONE_KID.getRowName());
			validateNumber(visionPlanRatesVO.getOneAdultThreePlusKid(), visionPlanDataVO, errorMessages, VisionPlanRatesRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowName());
			validateNumber(visionPlanRatesVO.getOneAdultTwoKid(), visionPlanDataVO, errorMessages, VisionPlanRatesRowEnum.ONE_ADULT_TWO_KID.getRowName());
			validateNumber(visionPlanRatesVO.getPlanID(), visionPlanDataVO, errorMessages, VisionPlanRatesRowEnum.PLAN_ID.getRowName());
			validateNumber(visionPlanRatesVO.getTwoAdultOneKid(), visionPlanDataVO, errorMessages, VisionPlanRatesRowEnum.TWO_ADULT_ONE_KID.getRowName());
			validateNumber(visionPlanRatesVO.getTwoAdultThreePlusKid(), visionPlanDataVO, errorMessages, VisionPlanRatesRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowName());
			validateNumber(visionPlanRatesVO.getTwoAdultTwoKid(), visionPlanDataVO, errorMessages, VisionPlanRatesRowEnum.TWO_ADULT_TWO_KID.getRowName());
					
			if (visionPlanDataVO.isNotValid()) {
				isValid = false;
				
				if (StringUtils.isNotBlank(visionPlanDataVO.getCarrierHIOSId()) &&
						StringUtils.isNotBlank(visionPlanDataVO.getState()) &&
						StringUtils.isNotBlank(visionPlanDataVO.getPlanId())) {
					visionPlanDataVO.setErrorMessages(errorMessages.toString() + "Skipping to persist Vision Plans ["
							+ hiosPlanNumber + "] due to validations error.");
				}
				else {
					visionPlanDataVO.setErrorMessages(errorMessages.toString() + "Skipping to persist Vision plan due to validations error.");
				}
				
				visionPlanDataVO.setErrorMessages(visionPlanDataVO.getErrorMessages() + "\n=========================================================");
			}
		}
		finally {
			LOGGER.debug("validateVisionExcelVO() End");
		}
		return isValid;
	}
	
	/**
	 * Validates whether Start Date is in valid format.
	 * 	
	 * @param visionPlanDataVO - VisionPlanDataVO instance
	 * @param errorMessages - Message to be returned after validating date.
	 * @param cellHeader - Start Date parameter name from enum.
	 */
	private void validateStartDate(VisionPlanDataVO visionPlanDataVO, StringBuilder errorMessages, String cellHeader) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		LOGGER.debug("validateStartDate() Start");
		try {
			if (!StringUtils.isEmpty(visionPlanDataVO.getStartDate())){
				formatter.parse(visionPlanDataVO.getStartDate());
			}
		} catch (ParseException e) {
			errorMessages.append(cellHeader + "[" + visionPlanDataVO.getStartDate() + "]" + SerffConstants.COMMA + EMSG_INVALID_DATE);
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
		LOGGER.error("Exception occured while parsing date", e);
			}
		LOGGER.debug("validateStartDate() End");
	}

	/**
	 * Method is used to validate Blank Data from Cell Value.
	 * 
	 * @param cellValue - The value read from excel cell.
	 * @param visionPlanDataVO - VisionPlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateBlankData(String cellValue, VisionPlanDataVO visionPlanDataVO, StringBuilder errorMessages, String cellHeader) {
		LOGGER.debug("validateBlankData() Start");
		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateBlankData() End");
	}
	
	/**
	 * Validates whether text is not exceeding permissible maxlength.
	 * 
	 * @param cellValue - The value read from excel cell.
	 * @param visionPlanDataVO - VisionPlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateTextWithLength(String cellValue, VisionPlanDataVO visionPlanDataVO, StringBuilder errorMessages, String cellHeader, int maxLength) {
		LOGGER.debug("validateTextWithLength() Start");
		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
		}else if(StringUtils.isNotBlank(cellValue) && (cellValue.length() > maxLength )){
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_TEXT_LENGTH_EXCEEDS);
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateTextWithLength() End");
	}
	
	/**
	 * Method is used to validate Number value from Cell Value.
	 * 
	 * @param cellValue - The value read from excel cell.
	 * @param visionPlanDataVO - VisionPlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateNumber(String cellValue, VisionPlanDataVO visionPlanDataVO, StringBuilder errorMessages, String cellHeader) {
		LOGGER.debug("validateNumber() Start");
		if(StringUtils.isBlank(cellValue) || !validateNumeric(cellValue)){
			
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_NUMERIC);
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateNumber() End");
	}

	/**
	 * Method is used to validate Integer value from Cell Value.
	 * 
	 * @param cellValue - The value read from excel cell.
	 * @param visionPlanDataVO - VisionPlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	private void validateInteger(String cellValue, VisionPlanDataVO visionPlanDataVO, StringBuilder errorMessages, String cellHeader) {
		LOGGER.debug("validateInteger() Start");
		if ((StringUtils.isEmpty(cellValue)) || (!StringUtils.isNumeric(cellValue))) {
			
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_NUMERIC);
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateInteger() End");
	}
	
	/**
	 * Validates state field.
	 * 
	 * @param visionPlanDataVO - VisionPlanDataVO instance
	 * @param errorMessages - Message to create if validation failed.
	 * @param cellHeader - parameter name from enum.
	 */
	public void validateState(VisionPlanDataVO visionPlanDataVO, StringBuilder errorMessages, String cellHeader) {
		LOGGER.debug("validateState() Start");
		if (!validateState(visionPlanDataVO.getState())) {

			errorMessages.append(cellHeader + "[" + visionPlanDataVO.getState() + "]" + SerffConstants.COMMA + EMSG_INVALID_STATE);
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateState() End");
	}

	/**
	 * This method validates Issuer HIOS Id.
	 * 
	 * @param visionPlanDataVO - VisionPlanDataVO instance
	 */
	public void validateCarrierHIOS(VisionPlanDataVO visionPlanDataVO) {
		LOGGER.debug("validateCarrierHIOS() Start");
		if (StringUtils.isBlank(visionPlanDataVO.getCarrierHIOSId())) {
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
			visionPlanDataVO.setErrorMessages(visionPlanDataVO.getErrorMessages() + VisionPlanDataRowEnum.CARRIER_HIOS_ID.getRowName()
					+ SerffConstants.COMMA + EMSG_EMPTY);
		}
		else if (LEN_HIOS_ISSUER_ID != StringUtils.length(visionPlanDataVO.getCarrierHIOSId())) {
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
			visionPlanDataVO.setErrorMessages(VisionPlanDataRowEnum.CARRIER_HIOS_ID.getRowName() + "[" + visionPlanDataVO.getCarrierHIOSId() + "]"
					+ SerffConstants.COMMA + EMSG_LENGTH + LEN_HIOS_ISSUER_ID + NEW_LINE);
		}
		LOGGER.debug("validateCarrierHIOS() End");
	}

	/**
	 * This method validates PLan Id.
	 * 
	 * @param visionPlanDataVO - VisionPlanDataVO instance
	 */
	public void validatePlanId(VisionPlanDataVO visionPlanDataVO) {
		LOGGER.debug("validatePlanId() Start");
		if (StringUtils.isBlank(visionPlanDataVO.getPlanId())) {
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
			visionPlanDataVO.setErrorMessages(VisionPlanDataRowEnum.PLAN_ID.getRowName() + SerffConstants.COMMA + EMSG_EMPTY);
		}
		else if (ISSUER_PLAN_NUMBER_LENGTH != StringUtils.length(visionPlanDataVO.getPlanId())) {
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
			visionPlanDataVO.setErrorMessages(VisionPlanDataRowEnum.PLAN_ID.getRowName() + "[" + visionPlanDataVO.getPlanId() + "]"
					+ SerffConstants.COMMA + EMSG_LENGTH + LEN_AME_PLAN_ID + NEW_LINE);
		}
		LOGGER.debug("validatePlanId() End");
	}
	
	
	/**
	 * This method validates OutOfnetworkAvailability field value.
	 * 
	 * @param visionPlanDataVO - VisionPlanDataVO instance
	 */
   public void validateOutOfnetworkAvailability(VisionPlanDataVO visionPlanDataVO) {
	   LOGGER.debug("validateOutOfnetworkAvailability() Start");
		if (StringUtils.isBlank(visionPlanDataVO.getOutOfNetworkAvailability())) {
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
			visionPlanDataVO.setErrorMessages(VisionPlanDataRowEnum.OUT_OF_NETWORK_AVAILABILITY.getRowName() + SerffConstants.COMMA + EMSG_EMPTY);
		}
		else if ((!visionPlanDataVO.getOutOfNetworkAvailability().equalsIgnoreCase(SerffConstants.YES_ABBR)) && 
				(!visionPlanDataVO.getOutOfNetworkAvailability().equalsIgnoreCase(SerffConstants.NO_ABBR))) {
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
			visionPlanDataVO.setErrorMessages(VisionPlanDataRowEnum.OUT_OF_NETWORK_AVAILABILITY.getRowName() + "[" + visionPlanDataVO.getOutOfNetworkAvailability() + "]"
					+ SerffConstants.COMMA + OUT_OF_NETWORK_VALUE_MSG );
		}
		LOGGER.debug("validateOutOfnetworkAvailability() End");
	}
   
   /**
    * This method validates URL field value.
    * @param cellValue - The value read from excel cell.
	* @param visionPlanDataVO - VisionPlanDataVO instance
	* @param errorMessages - Message to create if validation failed.
	* @param cellHeader - parameter name from enum.
	*/
	private void validateURL(String cellValue, VisionPlanDataVO visionPlanDataVO,
			StringBuilder errorMessages, String cellHeader) {
		LOGGER.debug("validateURL() Start");
		if (StringUtils.isNotBlank(cellValue) && !(isValidPattern(PATTERN_URL, StringUtils.trim(cellValue)))){
			
			errorMessages.append(cellHeader + "[" + cellValue + "]" + SerffConstants.COMMA + EMSG_INVALID_URL);
			
			if (!visionPlanDataVO.isNotValid()) {
				visionPlanDataVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateURL() End");
	}
}
