package com.serff.service.validation;

import static com.serff.service.validation.IFailedValidation.ECODE_DEFAULT;
import static com.serff.service.validation.IFailedValidation.EMSG_EMPTY_INVALID;
import static com.serff.service.validation.IFailedValidation.EMSG_FORMAT;
import static com.serff.service.validation.IFailedValidation.EMSG_INVALID_STANDARD_STATE_CODE;
import static com.serff.service.validation.IFailedValidation.EMSG_INVALID_VARIANT_ID;
import static com.serff.service.validation.IFailedValidation.EMSG_LENGTH;
import static com.serff.service.validation.IFailedValidation.EMSG_NUMERIC;
import static com.serff.service.validation.IFailedValidation.EMSG_SHOULD_NOT_EMPTY;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType;
import com.serff.template.csr.extension.InsurancePlanVariantType;
import com.serff.template.csr.hix.pm.InsurancePlanType;
import com.serff.template.csr.hix.pm.IssuerType;
import com.serff.template.csr.niem.core.DateType;
import com.serff.template.csr.niem.core.IdentificationType;
import com.serff.template.csr.proxy.GYear;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * Class is used to provide validation service of CSR.
 * @author Bhavin Parmar
 */
@Component
public class CSRValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(CSRValidator.class);
	private static final String TEMPLATE_NAME = "CSR_VALIDATOR_TEMPLATE";
	@Autowired private SerffUtils serffUtils;

	/**
	 * Method is used to validate each field of CSRAdvancePaymentDeterminationType complex type.
	 *
	 * @param csrVO, Object of CSRAdvancePaymentDeterminationType
	 * @param errors, List<FailedValidation>
	 * @return FailedValidation objects in List.
	 */
	public void validateCSR(final CSRAdvancePaymentDeterminationType csrVO, List<FailedValidation> errors) {

		LOGGER.info("validateCSR() Start");

		try {

			if (null == csrVO) {
				LOGGER.info("CSRAdvancePaymentDeterminationType complex type has been null.");
				return;
			}

			LOGGER.info("Validation1: Benefit Year");
			validationBenefitYear(csrVO.getBenefitYearNumeric(), errors);

			if (null != csrVO.getIssuer()
					&& !csrVO.getIssuer().isEmpty()) {

				for (IssuerType issuerType : csrVO.getIssuer()) {

					if (null != issuerType
							&& null != issuerType.getIssuerIdentification()
							&& null != issuerType.getIssuerIdentification().getIdentificationID()) {

						// Validation of Issuer ID
						LOGGER.info("Validation2: Issuer ID");
						validationIssuerID(issuerType.getIssuerIdentification(), errors);

						// Validation of Insurance Plan List
						validationInsurancePlanList(issuerType.getInsurancePlan(), errors);
					}
				}
			}
		}
		finally {
			LOGGER.info("validateCSR() End");
		}
	}

	/**
	 * Method is used to validate Benefit Year.
	 *
	 * @param dateType, object of DateType
	 * @param errors, List Object of FailedValidation
	 */
	private void validationBenefitYear(final DateType dateType, List<FailedValidation> errors)  {

		String fieldName = "Benefit Year";

		if (null != dateType
				&& null != dateType.getDateRepresentation()
				&& null != dateType.getDateRepresentation().getValue()) {

			GYear gYear = (com.serff.template.csr.proxy.GYear) dateType.getDateRepresentation().getValue();

			if (null == gYear
					|| null == gYear.getValue()
					|| 0 == gYear.getValue().getYear()) {
				errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else {
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_EMPTY_INVALID, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used to validate Issuer Id.
	 * Rule: Valid 5-digit Issuer ID
	 *
	 * @param identificationType, object of IdentificationType
	 * @param errors, List Object of FailedValidation
	 */
	private void validationIssuerID(final IdentificationType identificationType, List<FailedValidation> errors)  {

		String value = null;
		String fieldName = "Issuer ID";

		if (null != identificationType
				&& null != identificationType.getIdentificationID()) {

			value =  identificationType.getIdentificationID().getValue();
			LOGGER.info("issuerType.getIssuerIdentification().getIdentificationID(): " + value);

			if (StringUtils.isNotEmpty(value)) {

				if (!StringUtils.isNumeric(value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_NUMERIC, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (!isValidLength(LEN_HIOS_ISSUER_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LEN_HIOS_ISSUER_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
			else {
				errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else {
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used to Insurance Plan List.
	 *
	 * @param insurancePlanTypeList, List object of InsurancePlanType
	 * @param errors, List Object of FailedValidation
	 */
	private void validationInsurancePlanList(final List<InsurancePlanType> insurancePlanTypeList, List<FailedValidation> errors)  {

		if (null != insurancePlanTypeList
				&& !insurancePlanTypeList.isEmpty()) {

			for (InsurancePlanType planType : insurancePlanTypeList) {

				if (null != planType) {

					LOGGER.info("Validation3: Standard Component ID");
					validationStandardComponentId(planType, errors);
					// Validate: Variant Component ID and Cost Sharing Reduction Advance Payment Amount
					validationInsurancePlanVariant(planType, errors);
				}
			}
		}
	}

	/**
	 * Method is used to Standard Component ID.
	 * Format = HIOS Product ID + 9999 (12345VA0019999)
	 *
	 * @param planType, object of InsurancePlanType
	 * @param errors, List Object of FailedValidation
	 */
	private void validationStandardComponentId(final InsurancePlanType planType, List<FailedValidation> errors)  {

		String value = null;
		String fieldName = "Standard Component ID";
		String globalStateCode = serffUtils.getGlobalStateCode();

		if (null != planType.getInsurancePlanStandardComponentIdentification()
				&& null != planType.getInsurancePlanStandardComponentIdentification().getIdentificationID()) {

			value =  planType.getInsurancePlanStandardComponentIdentification().getIdentificationID().getValue();
			LOGGER.info("planType.getInsurancePlanStandardComponentIdentification().getIdentificationID(): " + value);

			if (StringUtils.isNotEmpty(value)) {

				if (!isValidLength(LET_STANDARD_COMPONENT_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_LENGTH + LET_STANDARD_COMPONENT_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (!isValidPattern(PATTERN_STANDARD_COMPONENT_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_FORMAT, ECODE_DEFAULT, TEMPLATE_NAME));
				}
				else if (StringUtils.isNotBlank(globalStateCode) && !globalStateCode.equalsIgnoreCase(SerffConstants.DEFAULT_PHIX_STATE_CODE)
						&& !value.substring(LEN_HIOS_ISSUER_ID, LEN_HIOS_ISSUER_ID + LEN_ISSUER_STATE).equals(globalStateCode)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_INVALID_STANDARD_STATE_CODE + globalStateCode, ECODE_DEFAULT, TEMPLATE_NAME));
					value = null;
				}
			}
			else {
				errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else {
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used to Insurance Plan Variant.
	 * Validate: Variant Component ID and Cost Sharing Reduction Advance Payment Amount
	 * Format: 00 through 06
	 *
	 * @param planType, object of InsurancePlanType
	 * @param errors, List Object of FailedValidation
	 */
	private void validationInsurancePlanVariant(final InsurancePlanType planType, List<FailedValidation> errors)  {

		if (null != planType.getInsurancePlanVariant()
				&& !planType.getInsurancePlanVariant().isEmpty()) {

			for (InsurancePlanVariantType variantType : planType.getInsurancePlanVariant()) {

				if (null != variantType) {

					LOGGER.info("Validation4: Variant Component ID");
					validationVariantComponentID(variantType, errors);

					LOGGER.info("Validation5: Cost Sharing Reduction Advance Payment Amount");
					validationVariantAdvancePaymentAmount(variantType, errors);
				}
			}
		}
	}

	/**
	 * Method is used to Variant Component ID.
	 * Format: 00 through 06
	 *
	 * @param variantType, object of InsurancePlanVariantType
	 * @param errors, List Object of FailedValidation
	 */
	private void validationVariantComponentID(
			final InsurancePlanVariantType variantType,
			List<FailedValidation> errors) {

		String value = null;
		String fieldName = "Variant Component ID";

		if (null != variantType.getInsurancePlanVariantIdentification()
				&& null != variantType.getInsurancePlanVariantIdentification().getIdentificationID()) {

			value =  variantType.getInsurancePlanVariantIdentification().getIdentificationID().getValue();
			LOGGER.info("variantType.getInsurancePlanVariantIdentification().getIdentificationID(): " + value);

			if (StringUtils.isNotEmpty(value)) {

				if (!isValidPattern(PATTERN_VARIANT_COMPONENT_ID, value)) {
					errors.add(getFailedValidation(fieldName, value, EMSG_INVALID_VARIANT_ID, ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
			else {
				errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else {
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}

	/**
	 * Method is used to Cost Sharing Reduction Advance Payment Amount.
	 * Format: 0 or higher decimal value.
	 *
	 * @param variantType, object of InsurancePlanVariantType
	 * @param errors, List Object of FailedValidation
	 */
	private void validationVariantAdvancePaymentAmount(final InsurancePlanVariantType variantType, List<FailedValidation> errors)  {

		BigDecimal value = null;
		String fieldName = "Cost Sharing Reduction Advance Payment Amount";

		if (null != variantType.getCostSharingReductionAdvancePaymentAmount()) {

			value =  variantType.getCostSharingReductionAdvancePaymentAmount().getValue();
			LOGGER.info("variantType.getInsurancePlanVariantIdentification().getIdentificationID(): " + value);

			if (null != value) {

				if (-1 == value.compareTo(BigDecimal.ZERO)) {
					errors.add(getFailedValidation(fieldName, value.toString(), "Value must be zero or higher decimal", ECODE_DEFAULT, TEMPLATE_NAME));
				}
			}
			else {
				errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
			}
		}
		else {
			errors.add(getFailedValidation(fieldName, EMPTY_VALUE, EMSG_SHOULD_NOT_EMPTY, ECODE_DEFAULT, TEMPLATE_NAME));
		}
	}
}
