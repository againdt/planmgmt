package com.serff.service.validation;

import static com.serff.util.SerffConstants.COMMA;
import static com.serff.util.SerffConstants.NEW_LINE;
import static com.serff.util.SerffConstants.SUCCESS;

import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.serffadapter.plandoc.PlanDocumnetExcelColumn.PlanDocumnetExcelColumnEnum;
import com.getinsured.serffadapter.plandoc.PlanDocumnetExcelVO;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * Class is used to validate Plan Documents Excel.
 * 
 * @author Bhavin Parmar
 * @since January 08, 2015
 */
@Component
public class PlanDocumentsValidator extends SerffValidator {

	private static final Logger LOGGER = Logger.getLogger(PlanDocumentsValidator.class);

	private static final String DOCS_TYPE_ECM = "ECM";
	private static final String DOCS_TYPE_URL = "URL";
	private static final String DOCUMENT_TYPES = "ECM,URL";
	private static final String EMSG_LENGTH = " Value must have length ";
	private static final String EMSG_EMPTY = " Value should not be empty." + NEW_LINE;
	private static final String EMSG_INVALID_PLAN_ID = " Invalid Plan Id." + NEW_LINE;
	private static final String EMSG_NUMERIC = " Value must be numeric." + NEW_LINE;
	private static final String EMSG_INVALID_URL = " Invalid URL." + NEW_LINE;
	private static final String EMSG_INVALID_ECM = " Invalid ECM-ID." + NEW_LINE;
	private static final String EMSG_REQUIRE_DOCU_TYPE = " Document Type is require." + NEW_LINE;

	@Autowired private SerffUtils serffUtils;
	@Autowired private ContentManagementService ecmService;

	/**
	 * Method is used to validate require data from PlanDocumnetExcelVO object before persistence.
	 */
	public boolean validatePlanDocumentsList(PlanDocumnetExcelVO planDocumnetExcelVO, StringBuilder messages) {

		LOGGER.info("validatePlanDocumentsList() Start");
		boolean isValid = Boolean.FALSE;
		StringBuilder errorMessages = new StringBuilder();

		try {

			if (null == planDocumnetExcelVO) {
				return isValid;
			}

			LOGGER.info("planDocumnetExcelVO.getPlanId(): " + planDocumnetExcelVO.getPlanId());
			validateBlankData(planDocumnetExcelVO.getPlanId(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.PLAN_ID.getColumnName());
			validateBlankData(planDocumnetExcelVO.getApplicableYear(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.APPLICABLE_YEAR.getColumnName());

			validateNumeric(planDocumnetExcelVO.getApplicableYear(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.APPLICABLE_YEAR.getColumnName());

			validateLength(LET_STANDARD_COMPONENT_ID_WITH_VARIANT, planDocumnetExcelVO.getPlanId(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.PLAN_ID.getColumnName());
			validateLength(LEN_PLAN_YEAR, planDocumnetExcelVO.getApplicableYear(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.APPLICABLE_YEAR.getColumnName());

			validateIssuerPlanNumber(planDocumnetExcelVO.getPlanId(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.PLAN_ID.getColumnName());

			validateDocumentId(planDocumnetExcelVO.getBrochureDocumentName(), planDocumnetExcelVO.getBrochureType(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.BROCHURE_DOC_NAME.getColumnName());
			String ecmFileName = validateEcmId(planDocumnetExcelVO.getBrochureDocumentName(), planDocumnetExcelVO.getBrochureType(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.BROCHURE_DOC_NAME.getColumnName());
			planDocumnetExcelVO.setBrochureDocumentFileName(ecmFileName);

			validateDocumentId(planDocumnetExcelVO.getSbcName(), planDocumnetExcelVO.getSbcType(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.SBC_DOCUMENT_NAME.getColumnName());
			ecmFileName = validateEcmId(planDocumnetExcelVO.getSbcName(), planDocumnetExcelVO.getSbcType(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.SBC_DOCUMENT_NAME.getColumnName());
			planDocumnetExcelVO.setSbcFileName(ecmFileName);

			validateDocumentId(planDocumnetExcelVO.getEocDocumentName(), planDocumnetExcelVO.getEocType(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.EOC_DOCUMENT_NAME.getColumnName());
			ecmFileName = validateEcmId(planDocumnetExcelVO.getEocDocumentName(), planDocumnetExcelVO.getEocType(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.EOC_DOCUMENT_NAME.getColumnName());
			planDocumnetExcelVO.setEocDocumentFileName(ecmFileName);

			validateDocumentType(planDocumnetExcelVO.getBrochureType(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.BROCHURE_TYPE.getColumnName());
			validateDocumentType(planDocumnetExcelVO.getSbcType(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.SBC_TYPE.getColumnName());
			validateDocumentType(planDocumnetExcelVO.getEocType(), planDocumnetExcelVO, errorMessages, PlanDocumnetExcelColumnEnum.EOC_TYPE.getColumnName());

			if (planDocumnetExcelVO.isNotValid()) {
//				planDocumnetExcelVO.setErrorMessages(planDocumnetExcelVO.getPlanId() + "," +errorMessages.toString());
				planDocumnetExcelVO.setErrorMessages(errorMessages.toString());
//				planDocumnetExcelVO.setErrorMessages(errorMessages.toString() + "Skipping to update plan document data due to validations error.");
//				planDocumnetExcelVO.setErrorMessages(planDocumnetExcelVO.getErrorMessages() + "\n=========================================================");
			}
			else {
				isValid = Boolean.TRUE;
				errorMessages.append(SUCCESS);
				planDocumnetExcelVO.setErrorMessages(SUCCESS);
			}
		}
		catch (Exception ex) {
			LOGGER.error("validatePlanDocumentsList() Exception: " + ex.getMessage(), ex);
		}
		finally {

			if (null != planDocumnetExcelVO
					&& StringUtils.isNotBlank(planDocumnetExcelVO.getPlanId())) {
				messages.append(planDocumnetExcelVO.getPlanId());
				messages.append("==> ");
				messages.append(errorMessages);
			}
			else {
				messages.append(errorMessages);
			}
			messages.append("\n");
			LOGGER.info("validatePlanDocumentsList() End isValid: " + isValid);
		}
		return isValid;
	}

	/**
	 * Method is used to validate Blank Data from Cell Value.
	 */
	private void validateBlankData(String cellValue, PlanDocumnetExcelVO planDocumnetExcelVO, StringBuilder errorMessages, String cellHeader) {
		
		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_EMPTY);
			
			if (!planDocumnetExcelVO.isNotValid()) {
				planDocumnetExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Numeric value from Cell Value.
	 */
	private void validateNumeric(String cellValue, PlanDocumnetExcelVO planDocumnetExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue) && !StringUtils.isNumeric(cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_NUMERIC);

			if (!planDocumnetExcelVO.isNotValid()) {
				planDocumnetExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Length of Cell Value.
	 */
	private void validateLength(int length, String cellValue, PlanDocumnetExcelVO planDocumnetExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue) && !isValidLength(length, cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_LENGTH + length);

			if (!planDocumnetExcelVO.isNotValid()) {
				planDocumnetExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Length of Cell Value.
	 */
	private void validateIssuerPlanNumber(String cellValue, PlanDocumnetExcelVO planDocumnetExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(cellValue) && !isValidPattern(PATTERN_STANDARD_COMPONENT_ID_WITH_VARIANT, cellValue)) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_INVALID_PLAN_ID);

			if (!planDocumnetExcelVO.isNotValid()) {
				planDocumnetExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}

	/**
	 * Method is used to validate Document URL or ECM Id.
	 */
	private void validateDocumentId(String documentName, String documentType, PlanDocumnetExcelVO planDocumnetExcelVO,
			StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isBlank(documentName)) {
			return;
		}
		boolean isError = false;

		if (DOCS_TYPE_URL.equalsIgnoreCase(documentType) && !isValidPattern(PATTERN_URL, StringUtils.trim(documentName))) {
			isError = true;
			errorMessages.append(cellHeader + "[" + documentName + "]" + SerffConstants.COMMA + EMSG_INVALID_URL);
		}
		/*else if (DOCS_TYPE_ECM.equalsIgnoreCase(documentType) && !isValidPattern(PATTERN_ECM, documentName)) {
			isError = true;
			errorMessages.append(cellHeader + "[" + documentName + "]" + SerffConstants.COMMA + EMSG_INVALID_ECM);
		}*/
		else if (StringUtils.isBlank(documentType)) {
			isError = true;
			errorMessages.append(cellHeader + "[" + documentName + "]" + SerffConstants.COMMA + EMSG_REQUIRE_DOCU_TYPE);
		}

		if (isError && !planDocumnetExcelVO.isNotValid()) {
			planDocumnetExcelVO.setNotValid(Boolean.TRUE);
		}
	}

	/**
	 * Method is used to validate ECM Id and get ECM File Name.
	 */
	private String validateEcmId(String documentName, String documentType, PlanDocumnetExcelVO planDocumnetExcelVO,
			StringBuilder errorMessages, String cellHeader) {

		String ecmFileName = null;

		if (StringUtils.isBlank(documentName) || StringUtils.isBlank(documentType)) {
			return ecmFileName;
		}

		if (DOCS_TYPE_ECM.equalsIgnoreCase(documentType)) {
			ecmFileName = serffUtils.getECMFileName(documentName, ecmService);

			if (StringUtils.isBlank(ecmFileName)) {
				errorMessages.append(cellHeader + "[" + documentName + "]" + SerffConstants.COMMA + EMSG_INVALID_ECM);

				if (!planDocumnetExcelVO.isNotValid()) {
					planDocumnetExcelVO.setNotValid(Boolean.TRUE);
				}
			}
		}
		return ecmFileName;
	}

	/**
	 * Method is used to validate Numeric value from Cell Value.
	 */
	private void validateDocumentType(String documentType, PlanDocumnetExcelVO planDocumnetExcelVO, StringBuilder errorMessages, String cellHeader) {

		if (StringUtils.isNotBlank(documentType) && !Arrays.asList(DOCUMENT_TYPES.split(COMMA)).contains(documentType.toUpperCase())) {
			errorMessages.append(cellHeader + SerffConstants.COMMA + EMSG_NUMERIC);

			if (!planDocumnetExcelVO.isNotValid()) {
				planDocumnetExcelVO.setNotValid(Boolean.TRUE);
			}
		}
	}
}
