package com.serff.service;

import com.serff.dto.BenefitsDisplayMappingParamsDTO;

/**
 * Interface is used to map rules of Plan Display.
 * @author Bhavin Parmar
 * @since November 25, 2014
 */
public interface PlanDisplayMapping {

	PlanDisplayRulesMap loadPlanDisplayRulesInMap(String hiosIssuerId);

	String getBenefitDisplayTileText(String coinsValue, String coinsAttr, String copayValue, String copayAttr, PlanDisplayRulesMap rulesMap);

	String getBenefitDisplayText(String coinsValue, String coinsAttr, String copayValue, String copayAttr, long coinsAfterNoOffCopay,
			long afterNumberOfVisit, boolean isPrimaryCareT1, PlanDisplayRulesMap rulesMap, BenefitsDisplayMappingParamsDTO extendedParams);
	
	boolean copyActiveDisplayRulesForIssuer(String copyFromIssuerId, String copyToIssuerId);
	boolean isExtendedParamSupported();

}
