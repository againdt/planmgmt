package com.serff.service;

import java.util.List;
/**
 * Interface is used to provide services for plan benefit Edit.
 * @author Vani Sharma
 */
public interface PlanBenefitEditService {

	public List<String> getAllApplicableYear();
	
	public List<String> getAllPlansForApplicableYear(String planId);
} 
