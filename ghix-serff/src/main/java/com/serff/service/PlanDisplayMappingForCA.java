package com.serff.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.serff.dto.BenefitsDisplayMappingParamsDTO;
import com.serff.dto.BenefitsDisplayMappingParamsDTO.NetworkType;

@Service("planDisplayMappingForCA")
public class PlanDisplayMappingForCA extends PlanDisplayMappingImpl {
	private static final Logger LOGGER = Logger.getLogger(PlanDisplayMappingForCA.class);

	/**
	 * Method is used to return Display Text from CoPay and CoInsurance data.
	 */
	@Override
	public String getBenefitDisplayText(String coinsValue, String coinsAttr,
			String copayValue, String copayAttr, long coinsAfterNoOffCopay,
			long afterNumberOfVisit, boolean isPrimaryCareT1,
			PlanDisplayRulesMap rulesMap, BenefitsDisplayMappingParamsDTO extendedParams) {
		StringBuffer displayText = new StringBuffer();
		displayText.append(super.getBenefitDisplayText(coinsValue, coinsAttr, copayValue, copayAttr, coinsAfterNoOffCopay,
				afterNumberOfVisit, isPrimaryCareT1, rulesMap, extendedParams));
		
		if(null != extendedParams) {
			String benefitName = extendedParams.getBenefitName();
			NetworkType networkType = extendedParams.getNetworkType();
			long days = extendedParams.getMaxNumDaysForChargingInpatientCopay();
			if(days > 0 && networkType == NetworkType.IN_NETWORK_TIER1 && StringUtils.isNotBlank(benefitName)) {
				if(benefitName.equals("INPATIENT_HOSPITAL_SERVICE") 
						|| benefitName.equals("MENTAL_HEALTH_IN") 
						|| benefitName.equals("SUBSTANCE_INPATIENT_USE") 
						|| benefitName.equals("DELIVERY_IMP_MATERNITY_SERVICES") ) {
					double copay=0, coins=0;
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Getting updated PlanDisplayText for benefitName:" + benefitName + " days=" + days + " copayValue=" + copayValue + " coinsValue=" +  coinsValue);
					}
					if(StringUtils.isNotBlank(copayValue)) {
						copay = Double.parseDouble(copayValue);
					}
					if(StringUtils.isNotBlank(coinsValue)) {
						coins = Double.parseDouble(coinsValue);
					}
					if((copay + coins) > 0 ) {
						if(benefitName.equals("DELIVERY_IMP_MATERNITY_SERVICES")) {
							//HIX-112261
							int copayIndex = displayText.indexOf("Copay");
							if(copayIndex >= 0) {
								displayText = displayText.insert(copayIndex + 5, " per day");
							}
						}
						displayText.append(" up to " + days + " days");
					}
				}
			}
		} 
		return displayText.toString();
	}
	
	@Override
	public boolean isExtendedParamSupported() {
		return true;
	}
}
