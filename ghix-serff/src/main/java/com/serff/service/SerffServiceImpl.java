package com.serff.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.STMHCCPlansAdminFees;
import com.getinsured.hix.model.STMHCCPlansAreaFactor;
import com.getinsured.hix.model.STMHCCPlansRate;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.repository.IZipCodeRepository;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.SerffTemplateGenerator;
import com.getinsured.serffadapter.puf.PUFAdapter;
import com.google.gson.Gson;
import com.serff.dto.BenefitsDisplayMappingParamsDTO;
import com.serff.repository.ISerffDocument;
import com.serff.repository.ISerffPlanMgmt;
import com.serff.repository.IserffPlamMgmtBatch;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.IPlanDentalBenefitRepository;
import com.serff.repository.templates.IPlanHealthBenefitRepository;
import com.serff.repository.templates.IPufHealthRepository;
import com.serff.repository.templates.IPufRatesRepository;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.repository.templates.IstmHCCAreaFactorRepository;
import com.serff.repository.templates.IstmHccAdminFees;
import com.serff.repository.templates.IstmHccRepository;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * Class is used to provide services of SERFF.
 * 
 * @author Bhavin Parmar
 */
@Service("serffService")
public class SerffServiceImpl implements SerffService {

	private static final Logger LOGGER = Logger.getLogger(SerffServiceImpl.class);
	//private static final boolean IS_LOCAL_PATH = true;
	
	//private static final String PUF_PUSH_URL = "/exchange-hapi/admin/postFTPPlans";
	// private static final String PROD_PUF_PUSH_URL = QA_PUF_PUSH_URL;
	//private static final String DEFAULT_TENANT = "GINS";
	//private static final String EXCEPTIONS = "Exception: ";
	//private static final String MIME_TYPE = "text/xml";
	//private static final String PARM_MULTIPART_ENTITY = "fileUploadPlans";
	//private static final String UPDATE_CLAUSE_QA ="UPDATE PUF_PLAN_STATUS SET QA_POST_STATUS = '";
	//private static final String UPDATE_CLAUSE_STAGE ="UPDATE PUF_PLAN_STATUS SET STAGE_POST_STATUS  = '";
	private static final String WHERE_CLAUSE ="' WHERE HIOS_ISSUER_ID = '";
	private static final String SUCCESS_MSG = "PUF status updated succesfully for ";
	private static final String FAIL_MSG ="PUF status update failed for ";
	private static final String EXCHANGE_USER = "exchange@ghix.com";
	
	@Autowired private ISerffPlanMgmt iSerffPlanMgmt;
	@Autowired private ISerffDocument iSerffDocument;
	@Autowired private IserffPlamMgmtBatch iSerffPlanMgmtBatch;
	@Autowired private IPufHealthRepository iPufHealthRepository;
	@Autowired private IPufRatesRepository iPufRatesRepository;
	@Autowired private IZipCodeRepository iZipCodeRepository;
//	@Autowired private IPlanHealthRepository iPlanHealthRepository;
//	@Autowired private IPlanDentalRepository iPlanDentalRepository;
	@Autowired private IPlanHealthBenefitRepository iSerffPlanHealthBenefitRepository;
	@Autowired private IPlanDentalBenefitRepository iSerffPlanDentalBenefitRepository;
//	@Autowired private IPlanBenefitEditRepository iPlanBenefitEditRepository;	
	//@Autowired private GHIXSFTPClient ftpClient;	
	@Autowired private SerffUtils serfUtil;	
	@Autowired private PlanDisplayMapping planDisplayMapping;
	@Autowired private PlanDisplayMapping planDisplayMappingExtended;
	@Autowired private ISerffPlanRepository planRepo;
	@Autowired private IstmHccRepository iStmHccRaterepo ;
	@Autowired private IstmHCCAreaFactorRepository iStmAreaFactorrepo;
	@Autowired private IstmHccAdminFees istmAdminFeeRepo;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private IIssuersRepository iIssuerRepository;
	
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired 
	private Gson platformGson;
	
	
	
	/**
	 * Method is used to get a record from SERFF_PLAN_MGMT table.
	 * @author Bhavin Parmar
	 * 
	 * @param long SerffPlanMgmt Id
	 * @return SerffPlanMgmt instance that is retrieved from database using 'id'.       
	 */
	@Override
	public SerffPlanMgmt getSerffPlanMgmtById(long id) {
		
		LOGGER.info("Executing getSerffPlanMgmtById() : id : " + id);
		
		if (id > 0) {
			LOGGER.debug("getSerffPlanMgmtById() : id : " + id);
			return iSerffPlanMgmt.findOne(id);
		}
		LOGGER.debug("getSerffPlanMgmtById() : returning null.");
		return null;
	}
	
	/**
	 * Method is used to save a record in SERFF_PLAN_MGMT table.
	 * @author Bhavin Parmar
	 * 
	 * @param SerffPlanMgmt POJO object.
	 * @return SerffPlanMgmt instance that saved in the database.
	 */
	@Override
	@Transactional
	public SerffPlanMgmt saveSerffPlanMgmt(final SerffPlanMgmt serffPlanMgmt) {

		
		if (null != serffPlanMgmt) {
			LOGGER.info("Executing saveSerffPlanMgmt() : serffReqId : " + serffPlanMgmt.getSerffReqId());
			return iSerffPlanMgmt.save(serffPlanMgmt);
		}
		LOGGER.debug("saveSerffPlanMgmt() : returning null.");
		return null;
	}
	
	/**
	 * Method is used to get a record from SERFF_DOCUMENT table.
	 * @author Bhavin Parmar
	 * 
	 * @param long SerffDocument Id
	 * @return SerffDocument instance that saved in the database.        
	 */
	@Override
	public SerffDocument getSerffDocumentById(long id) {
		
		LOGGER.info("Executing getSerffDocumentById() : id : " + id);
		
		if (id > 0) {
			LOGGER.debug("getSerffDocumentById() : id : " + id);
			return iSerffDocument.findOne(id);
		}
		LOGGER.debug("getSerffDocumentById() : returning null.");
		return null;
	}

	/**
	 * Method is used to save a record in SERFF_DOCUMENT table.
	 * @author Bhavin Parmar
	 * 
	 * @param SerffDocument POJO object.
	 * @return Saved instance of the SerffDocument.       
	 */
	@Override
	@Transactional
	public SerffDocument saveSerffDocument(final SerffDocument serffDocument) {
		
		
		if (null != serffDocument) {
			LOGGER.debug("Executing saveSerffDocument() : serffReqId : " + serffDocument.getSerffReqId());
			return iSerffDocument.save(serffDocument);
		}
		LOGGER.debug("saveSerffDocument() : returning null.");
		return null;
	}
	
	
	/**
	 * Method is used to fetch a record from SERFF_PLAN_MGMT_BATCH table.
	 * @author Vani Sharma
	 * 
	 * @param id of the requested batch.
	 * @return SerffPlanMgmtBatch instance fetched from database on the basis of 'id'
	 * 
	 */
	@Override
	public SerffPlanMgmtBatch getSerffPlanMgmtBatchById(long id) {
		
		LOGGER.info("Executing getSerffPlanMgmtBatchById() : id : " + id);
		
		if (id > 0) {
			LOGGER.debug("getSerffPlanMgmtBatchById() : id : " + id);
			return iSerffPlanMgmtBatch.findOne(id);
		}
		LOGGER.debug("getSerffPlanMgmtBatchById() : returning null.");
		return null;
	}	
	
	/**
	 * Method is used to save a record in SERFF_PLAN_MGMT_BATCH table.
	 * @author Vani Sharma
	 * 
	 * @param SerffPlanMgmtBatch POJO object.
	 * @return Saved instance of the SerffPlanMgmtBatch.       
	 */
	@Override
	public SerffPlanMgmtBatch saveSerffPlanMgmtBatch(
			SerffPlanMgmtBatch serffPlanMgmtBatch) {
		if (null != serffPlanMgmtBatch) {
			LOGGER.debug("Executing saveSerffPlanMgmtBatch() : id : " + serffPlanMgmtBatch.getId());
			return iSerffPlanMgmtBatch.save(serffPlanMgmtBatch);
		}
		LOGGER.debug("saveSerffPlanMgmtBatch() : returning null.");
		return null;
	}
	

	/**
	 * Method is used to upload templates from one FTP server to another FTP server.
	 * This feature is no longer supported
	 */
	//@Override
	/*public SerffConstants.PUF_STATUS doMultipartFormPost(boolean isLocalPath, String carrier, String carrierText,
			String stateCodes, String exchangeType, String tenant,
			String sourceFolderPath, String postURL) {
		String actualPostURL=null;
		LOGGER.info("doMultipartFormPost Begin");
		SerffConstants.PUF_STATUS pufStatus = SerffConstants.PUF_STATUS.FAILED;
		
		try {
			
			LOGGER.debug("ftpClient is null: " + (null == ftpClient));
			LOGGER.debug("sourceFolderPath: " + sourceFolderPath);
			LOGGER.debug("postURL: " + postURL);
			
			if (null != ftpClient && StringUtils.isNotBlank(sourceFolderPath)
					&& StringUtils.isNotBlank(postURL)) {
				
				actualPostURL = SerffConstants.HTTP_STRING + postURL + PUF_PUSH_URL;
				LOGGER.info("Full Post URL: " + actualPostURL);
				
				MultipartEntity requestEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
				requestEntity.addPart("carrier", new StringBody(carrier));
				requestEntity.addPart("carrierText", new StringBody(carrierText));
				requestEntity.addPart("stateCodes", new StringBody(stateCodes));
				requestEntity.addPart("exchangeType", new StringBody(exchangeType));
				requestEntity.addPart("tenant", new StringBody(tenant));
				requestEntity.addPart(SerffConstants.REQUEST_MODE, new StringBody(SerffConstants.REQUEST_MODE_API));
				
				HttpClient httpclient = new DefaultHttpClient();
				httpclient.getParams().setParameter("Connection", "Keep-Alive");
				httpclient.getParams().setParameter("Content-Type", "multipart/form-data;");
				// httpclient.getParams().setParameter("http.socket.timeout", Integer.valueOf(TIMEOUT_WAIT_TO_CONNECT));
				// httpclient.getParams().setParameter("http.connection.timeout", Integer.valueOf(TIMEOUT_WAIT_FOR_DATA));
				
				HttpPost httpPost = new HttpPost(actualPostURL);
				
				if (isLocalPath) {
					getInputStreamLocal(requestEntity, sourceFolderPath);
				}
				else {
					getInputStreamFTP(requestEntity, sourceFolderPath);
				}
				LOGGER.debug("MultipartEntity.requestEntity is null: " + (null == requestEntity));
				
				httpPost.setEntity(requestEntity);
				LOGGER.debug("HttpPost.httpPost is null: " + (null == httpPost));
				
				HttpResponse response = httpclient.execute(httpPost);
				LOGGER.debug("HttpResponse.response is null: " + (null == response));
				String responseData = null;
				
				if (null != response && null != response.getEntity()) {
					
					responseData = EntityUtils.toString(response.getEntity()).trim();
			        LOGGER.debug("Response Data: " + responseData);
				}
				else {
					LOGGER.debug("Response Data is null");
				}
				
				if (StringUtils.isNotBlank(responseData)
						&& responseData.equalsIgnoreCase(SerffConstants.PUF_STATUS.COMPLETED.name())) {
					pufStatus = SerffConstants.PUF_STATUS.COMPLETED;
				}
			}
		}
		catch (IOException e) {
			LOGGER.error("IOException: " + e.getMessage(), e);
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("PUF Status: " + pufStatus.name());
			LOGGER.info("doMultipartFormPost End");
			if(null!= ftpClient){
				ftpClient.close();
			}
		}
		return pufStatus;
	}
	
	private void getInputStreamFTP(MultipartEntity requestEntity, String sourceFolderPath) {
		
		LOGGER.info("getInputStreamFTP Begin");
		
		try {
			List<String> fileList = ftpClient.getFileNames(sourceFolderPath);
			InputStreamBody inputStreamBody = null;
			
			if (null != fileList) {
				InputStream inputStream = null;
				
				for (String fileType : fileList) {
					try {
						inputStream = ftpClient.getFileInputStream(sourceFolderPath + fileType);
						inputStreamBody = new InputStreamBody(inputStream, MIME_TYPE, fileType);
						requestEntity.addPart(PARM_MULTIPART_ENTITY, inputStreamBody);
						
						if (inputStream != null) {
							inputStream.close();
						}
					}
					catch (IOException e) {
						LOGGER.error("Exception occurred while downloading mail templates.", e);
					}
				}
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("getInputStreamFTP End");
		}
	}
	
	private void getInputStreamLocal(MultipartEntity requestEntity, String sourceFolderPath) {
		
		LOGGER.info("getInputStreamLocal Begin");
		
		try {
			List<String> fileList = new ArrayList<String>();
			File[] files = new File(sourceFolderPath).listFiles();
			
			if (null != files) {
				
				for (File file : files) {
					if (file.isFile()) {
						fileList.add(file.getName());
					}
				}
				// FileBody fileBodyBody = null;
				InputStreamBody inputStreamBody = null;
				
				if (null != fileList) {
					InputStream inputStream = null;
					
					for (String fileName : fileList) {
						try{
							inputStream = new FileInputStream(new File(sourceFolderPath + fileName));
							inputStreamBody = new InputStreamBody(inputStream, MIME_TYPE, fileName);
							// fileBodyBody = new FileBody(file, fileName, MIME_TYPE, CHAR_SET);
							requestEntity.addPart(PARM_MULTIPART_ENTITY, inputStreamBody);
							if (inputStream != null) {
								inputStream.close();
							}
						}
						catch (IOException e) {
							LOGGER.error("Exception occurred while getting input stream from local.", e);
						}
					}
				}
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("getInputStreamLocal End");
		}
	}

	@Override
	public void pushPUFToQA(String stateCode, String hiosId, String srcPath) {
		LOGGER.info("stateCode:"+stateCode +"hios Id :"+hiosId);
		EntityManager entityManager =  null;
		int result =0;
		try {
			// Covert native query to prepare statement. Validation to prevent sql injection.
			Integer.parseInt(hiosId);
			
			LOGGER.debug("Source Folder Path: " + srcPath);
			entityManager = entityManagerFactory.createEntityManager();
			if(null != entityManager){
				entityManager.getTransaction().begin();
				 result = entityManager.createNativeQuery(UPDATE_CLAUSE_QA +SerffConstants.PUF_STATUS.IN_PROGRESS+ WHERE_CLAUSE + hiosId+"'").executeUpdate();
				if(result > 0){
					LOGGER.debug(SUCCESS_MSG+hiosId);
					entityManager.getTransaction().commit();
				}else{
					LOGGER.debug(FAIL_MSG+hiosId);
				}
			
			SerffConstants.PUF_STATUS pufStatus = doMultipartFormPost(IS_LOCAL_PATH, hiosId, hiosId,
					stateCode, SerffPlanMgmtBatch.EXCHANGE_TYPE.PUF.toString(), DEFAULT_TENANT, srcPath, System.getenv("QA_SERVER"));
			entityManager.getTransaction().begin();
			result = entityManager.createNativeQuery(UPDATE_CLAUSE_QA + pufStatus.name() +WHERE_CLAUSE + hiosId+"'").executeUpdate();
			if(result > 0){
				LOGGER.debug(SUCCESS_MSG+hiosId);
				entityManager.getTransaction().commit();
			}else{
				LOGGER.debug(FAIL_MSG+hiosId);
			}
			
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("addFTPTemplateAttachments End");
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}
	}


	@Override
	public void pushPUFToStage(String stateCode, String hiosId, String srcPath) {
		LOGGER.info("state Code :"+stateCode +"Hios Id :"+hiosId);
		EntityManager entityManager = null;
		int result =0;
		try {
			// Covert native query to prepare statement. Validation to prevent sql injection.
			Integer.parseInt(hiosId);
			
			entityManager = entityManagerFactory.createEntityManager();
			
			if(null != entityManager){
				entityManager.getTransaction().begin();
				 result = entityManager.createNativeQuery(UPDATE_CLAUSE_STAGE+SerffConstants.PUF_STATUS.IN_PROGRESS+ WHERE_CLAUSE + hiosId+"'").executeUpdate();
				if(result > 0){
					LOGGER.debug(SUCCESS_MSG+hiosId);
					entityManager.getTransaction().commit();
				}else{
					LOGGER.debug(FAIL_MSG+hiosId);
				}
				
			LOGGER.debug("Source Folder Path: " + srcPath);
			SerffConstants.PUF_STATUS pufStatus = doMultipartFormPost(IS_LOCAL_PATH, hiosId, hiosId,
					stateCode, SerffPlanMgmtBatch.EXCHANGE_TYPE.PUF.toString(), DEFAULT_TENANT, srcPath, System.getenv("CARR_QA_SERVER"));
			
			entityManager.getTransaction().begin();
			result = entityManager.createNativeQuery(UPDATE_CLAUSE_STAGE+ pufStatus.name() +WHERE_CLAUSE + hiosId+"'").executeUpdate();
			
			if(result > 0){
				LOGGER.debug(SUCCESS_MSG+hiosId);
				entityManager.getTransaction().commit();
			}else{
				LOGGER.debug(FAIL_MSG+hiosId);
			}
			
			}
			
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("addFTPTemplateAttachments End");
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}		
	}
*/

	
	@Override
	public void generateTemplatesForPUF(String stateCode, String hiosId) throws GIException {

		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("stateCode : "+SecurityUtil.sanitizeForLogging(stateCode)+"hiosId: "+SecurityUtil.sanitizeForLogging(hiosId));
		}
		
		String planPrefix;
		
		if(hiosId == null) {
			planPrefix = "%" + stateCode;
		} else {
			planPrefix = hiosId + stateCode;
		}
		
		String destinationPath = System.getenv("PUF_FILES_BASE_PATH");
		if (StringUtils.isBlank(destinationPath)) {
			destinationPath = System.getProperty("java.io.tmpdir");
			LOGGER.debug("Destination Path [java.io.tmpdir]: " + destinationPath);
		}
		else {
			LOGGER.debug("Destination Path [PUF_FILES_BASE_PATH]: " + destinationPath);
		}
		destinationPath += SerffConstants.PATH_SEPERATOR;
		
		SerffTemplateGenerator generator = new SerffTemplateGenerator();
		PUFAdapter adptPuf = new PUFAdapter();
		adptPuf.loadTemplateData(planPrefix, iPufHealthRepository, iPufRatesRepository, iZipCodeRepository);
		Map<String, String> createdPathMap = generator.generateSerffTemplates(adptPuf, destinationPath);
		//String createdPath = generator.generateSerffTemplatesOnFTP(adptPuf, destPath);
		EntityManager entityManager = null;
		if(createdPathMap != null) {
			try{
				entityManager =  entityManagerFactory.createEntityManager();
				String createdPath = null;
				
				if (null != entityManager) {
					entityManager.getTransaction().begin();
	
					for (Map.Entry<String, String> entry : createdPathMap.entrySet()) {
						createdPath = entry.getValue();
						LOGGER.info("Template Files generated at " + createdPath);
	
						int result = entityManager.createNativeQuery(
								"UPDATE PUF_PLAN_STATUS SET TEMPLATE_PATH = '" + createdPath + WHERE_CLAUSE
								 + entry.getKey() + "'").executeUpdate();
						
						if (result > 0) {
							LOGGER.info(SUCCESS_MSG + entry.getKey());
	
						} else {
							LOGGER.info(FAIL_MSG + entry.getKey());
						}
	
					}
					entityManager.getTransaction().commit();
	
				} else {
					LOGGER.error("Entity manager is null.");
				}
				LOGGER.info("Template Files generated at "+createdPathMap.get(hiosId));
		}catch(Exception e){
			LOGGER.error("Exception occurred " , e);
		}finally{
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}
				
			
		}
	}
	
	/**
	 * Method is used to get all health benefits
	 * @param issuerIdArr
	 */
	@Override
	public void getAllHealthBenefits(String[] issuerIdArr) {
		
		EntityManager entityManager = null;
		LOGGER.info("getAllHealthBenefits() Begin");
		boolean transInProgress = false;
		
		try {
			
			if (issuerIdArr == null) {
				LOGGER.error("Arrary of Issuer Ids is empty.");
				return;
			}
			entityManager = entityManagerFactory.createEntityManager();
			List<PlanHealthBenefit> benefitList = null;
			Integer issuerId = 0;

			if (null != entityManager) {
				
				for (String issuerIdStr : issuerIdArr) {
					issuerId = Integer.valueOf(issuerIdStr);
					
					if (issuerId > 0) {
						benefitList = iSerffPlanHealthBenefitRepository.findAllPlanHealthBenefitsByIssuerId(issuerId);
						transInProgress = updateBenefitDisplayData(issuerIdStr, benefitList, entityManager , true);
					}
				}
			}
			else {
				LOGGER.error("EntityManager is Null! Failed to update Plan Health Benefits");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while getting Plan Health Benefits: " + ex.getMessage());
		}
		finally {

			if (transInProgress) {
				LOGGER.error("Executing getAllHealthBenefits() is done with errors. Transaction Rolledback");

				if (null != entityManager && entityManager.isOpen()) {
					entityManager.getTransaction().rollback();
				}
			}

			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			LOGGER.info("getAllHealthBenefits() End");
		}
	}

	/**
	 * Method is used to get all dental benefits
	 * @param issuerIdArr
	 * 
	 */
	@Override
	public void getAllDentalBenefits(String[] issuerIdArr){
		EntityManager entityManager = null;
		LOGGER.info("getAllDentalBenefits() Begin");
		boolean transInProgress = false;
		
		try {
			
			if (issuerIdArr == null) {
				LOGGER.error("Arrary of Issuer Ids is empty.");
				return;
			}
			entityManager = entityManagerFactory.createEntityManager();
			List<PlanDentalBenefit> dentalBenefitList = null;
			Integer issuerId = 0;

			if (null != entityManager) {
			
				for (String issuerIdStr : issuerIdArr) {
					issuerId = Integer.valueOf(issuerIdStr);
					
					if (issuerId > 0) {
						dentalBenefitList = iSerffPlanDentalBenefitRepository.findAllPlanDentalBenefitsByIssuerId(issuerId);
						transInProgress = updateDentalBenefitDisplayData(issuerIdStr, dentalBenefitList, entityManager , true);
					}
				}
			}
			else {
				LOGGER.error("EntityManager is Null! Failed to update Plan Dental Benefits.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while reading Dental Plans.");
		}
		finally {

			if (transInProgress) {
				LOGGER.error("Executing getAllDentalBenefits() is done with errors. Transaction Rolledback");

				if (null != entityManager && entityManager.isOpen()) {
					entityManager.getTransaction().rollback();
				}
			}

			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			LOGGER.info("getAllDentalBenefits() End");
		}
	}
	
	/**
	 * Method is used to update all health display benefits
	 * @param planOrIssuerId ,dentalBenefitList ,entityManager ,processAll
	 * 
	 */
	private boolean updateDentalBenefitDisplayData(String planOrIssuerId, List<PlanDentalBenefit> dentalBenefitList, EntityManager entityManager , boolean processAll) {

			LOGGER.debug("updateDentalBenefitDisplayData() Begin start Plan/Issuer with " + planOrIssuerId);
			boolean transInProgress = false;
			try{
				if (null != dentalBenefitList && !dentalBenefitList.isEmpty()) {
					
					transInProgress = true;
					entityManager.getTransaction().begin();
					updateDentalBenefitDisplayText(planOrIssuerId, dentalBenefitList, entityManager ,processAll );
					entityManager.getTransaction().commit();
					boolean isPHIXProfile = SerffConstants.PHIX_PROFILE.equals(DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE));
					if(isPHIXProfile) {
						refreshPlansInSOLR(SerffConstants.DENTAL, getDentalPlanIDList(dentalBenefitList));
					}
					transInProgress = false;
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("Updated Plan Dental Benefits records: " + dentalBenefitList.size() + " for Issuer Plan Number Or HIOS Issuer Id " + SecurityUtil.sanitizeForLogging(planOrIssuerId));
					}
				}
				else {
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("Plan Dental Benefits has been empty for Issuer Plan Number Or HIOS Issuer Id:"  + SecurityUtil.sanitizeForLogging(planOrIssuerId));
					}
				}
			}catch(Exception e){
				LOGGER.error("Exception :" , e);
			}finally{
				if (null != entityManager && entityManager.isOpen()) {
					entityManager.clear();
					entityManager.close();
				}
				LOGGER.debug("updateDentalBenefitDisplayData() End with return value transInProgress: " + transInProgress);
			}
		return transInProgress;
	}

	/**
	 * Method is used to get all health display benefits by plan id
	 * @param planOrIssuerId , processAll
	 */
	@Override
	public void getAllHealthBenefitsByPlans(String planOrIssuerId, Integer applicableYear, boolean processAll) {
		
		EntityManager entityManager = null;
		LOGGER.info("getAllHealthBenefitsByPlans Begin");
		boolean transInProgress = false;
		
		try {
			
			if (null == planOrIssuerId) {
				LOGGER.error("Plan/Issuer Id is null.");
				return;
			}
			entityManager = entityManagerFactory.createEntityManager();
			
			if (null != entityManager) {
				List<PlanHealthBenefit> benefitList = null;
				
				if (processAll) {
					 benefitList = iSerffPlanHealthBenefitRepository.findAllPlanHealthBenefitsByIssuerPlanNumberLike(planOrIssuerId, applicableYear);
				}
				else {
					 benefitList = iSerffPlanHealthBenefitRepository.findAllPlanHealthBenefitsByIssuerPlanNumberAndName(planOrIssuerId, applicableYear);
				}
				transInProgress = updateBenefitDisplayData(planOrIssuerId, benefitList, entityManager , processAll);
			}
			else {
				LOGGER.error("EntityManager is Null! Failed to update Plan Health Benefits for Issuer Plan Number Or HIOS Issuer Id " + SecurityUtil.sanitizeForLogging(planOrIssuerId));
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while getting Benefits of Health Plans: " + ex.getMessage(), ex);
		}
		finally {

			if (transInProgress) {
				LOGGER.error("Executing getAllHealthBenefitsByPlans() is done with errors. Transaction Rolledback");

				if (null != entityManager && entityManager.isOpen()) {
					entityManager.getTransaction().rollback();
				}
			}

			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			LOGGER.info("getAllHealthBenefitsByPlans() End");
		}
	}
	
	/**
	 * Method is used to get all dental display benefits by plan id
	 * @param planOrIssuerId ,processAll
	 * 
	 */
	@Override
	public void getAllDentalBenefitsByPlans(String planOrIssuerId, Integer applicableYear, boolean processAll) {
		
		EntityManager entityManager = null;
		LOGGER.info("getAllDentalBenefitsByPlans() Begin");
		boolean transInProgress = false;
		
		try {
			
			if (null == planOrIssuerId) {
				LOGGER.error("Plan/Issuer Id is null.");
				return;
			}
			entityManager = entityManagerFactory.createEntityManager();
			
			if (null != entityManager) {
				List<PlanDentalBenefit> dentalBenefitList = null;
				
				if (processAll) {
					dentalBenefitList = iSerffPlanDentalBenefitRepository.findAllPlanDentalBenefitsByIssuerPlanNumberLike(planOrIssuerId, applicableYear);
				}
				else {
					dentalBenefitList = iSerffPlanDentalBenefitRepository.findAllPlanDentalBenefitsByIssuerPlanNumberAndName(planOrIssuerId, applicableYear);
				}
				transInProgress = updateDentalBenefitDisplayData(planOrIssuerId, dentalBenefitList, entityManager , processAll);
			}
			else {
				LOGGER.error("EntityManager is Null! Failed to update Plan Dental Benefits for Issuer Plan Number Or HIOS Issuer Id " + SecurityUtil.sanitizeForLogging(planOrIssuerId));
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while getting Benefits of Dental Plans: " + ex.getMessage(), ex);
		}
		finally {

			if (transInProgress) {
				LOGGER.error("Executing getAllDentalBenefitsByPlans() is done with errors. Transaction Rolledback");

				if (null != entityManager && entityManager.isOpen()) {
					entityManager.getTransaction().rollback();
				}
			}

			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			LOGGER.info("getAllDentalBenefitsByPlans() End");
		}
	}
	
	/**
	 * Method is used to update display benefits by plan id or issuer id
	 * @param planOrIssuerId , benefitList ,entityManager ,processAll
	 * 
	 */
	private boolean updateBenefitDisplayData(String planOrIssuerId, List<PlanHealthBenefit> benefitList, EntityManager entityManager , boolean processAll) {

		LOGGER.debug("updateBenefitDisplayData Begin start with Plan/Issuer Id value: " + planOrIssuerId);
		boolean transInProgress = false;

		try {

			if (null != benefitList && !benefitList.isEmpty()) {
				transInProgress = true;
				entityManager.getTransaction().begin();
				updateBenefitDisplayText(planOrIssuerId, benefitList, entityManager ,processAll );
				entityManager.getTransaction().commit();
				boolean isPHIXProfile = SerffConstants.PHIX_PROFILE.equals(DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE));
				if(isPHIXProfile) {
					refreshPlansInSOLR(SerffConstants.HEALTH, getHealthPlanIDList(benefitList));
				}
				transInProgress = false;
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Updated Plan Health Benefits records: " + benefitList.size() + " for Issuer Plan Number Or HIOS Issuer Id " + SecurityUtil.sanitizeForLogging(planOrIssuerId));
				}
			}
			else if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Plan Health Benefits has been empty for Issuer Plan Number Or HIOS Issuer Id:"  + SecurityUtil.sanitizeForLogging(planOrIssuerId));
			}
		}
		finally {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("updateBenefitDisplayData End with return value transInProgress: " + transInProgress);
			}
		}
		return transInProgress;
	}
	
	/**
	 * Method is used to update health display benefits text
	 * @param benefitList ,entityManager ,processAll
	 * 
	 */
	private void updateBenefitDisplayText(String planOrIssuerId, List<PlanHealthBenefit> benefitList, EntityManager entityManager , boolean processAll) {
		Long coinsAfterNumOfCopay = null;
		Long afterNumOfVisit = null;
		String tileDisplayValue = null, t1Disptext = null, t2DispText = null, oonDispText = null;
		boolean isPHIXProfile = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE).equals(SerffConstants.PHIX_PROFILE);
		PlanDisplayRulesMap planDispRulesMap = null;
		
		if(!isPHIXProfile) {
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
			if(SerffConstants.CONNECTICUT_STATE_CODE.equalsIgnoreCase(stateCode)) {
				planDisplayMapping = planDisplayMappingExtended;
				//planDisplayMapping = (PlanDisplayMapping) GHIXApplicationContext.getBean("planDisplayMappingExtended");
				LOGGER.debug("Using Extended PlanDisplay Mappings");
			}
		}

		//Commented for HIX-63472 - use DB based display text rules 
		//if(SerffConstants.YES_ABBR.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.USE_BENEFIT_DISPLAY_RULES_FROM_DB))) {
			LOGGER.info("Fetching Benefit Display Rules for Issuer: " + planOrIssuerId.substring(0, SerffConstants.LEN_HIOS_ISSUER_ID));
			planDispRulesMap = planDisplayMapping.loadPlanDisplayRulesInMap(planOrIssuerId.substring(0, SerffConstants.LEN_HIOS_ISSUER_ID));
			if(!CollectionUtils.isEmpty(planDispRulesMap)) {
				LOGGER.info("Got " + planDispRulesMap.size() + " Benefit Display Rules for Issuer: " + planDispRulesMap.elements().nextElement().getHiosIssuerId());
			} else {
				LOGGER.warn("Got No Benefit Display Rules for Issuer: " + planDispRulesMap.elements().nextElement().getHiosIssuerId());
			}
		//}

		for (PlanHealthBenefit healthBenefit : benefitList) {
			
			LOGGER.debug("Updating Plan Health Benefit's Issuer Plan Number:"
					+ healthBenefit.getPlanHealth().getPlan().getIssuerPlanNumber() + " and Benefit Name "
					+ healthBenefit.getName());
			coinsAfterNumOfCopay = healthBenefit.getPlanHealth().getBeginPmrycareDedtOrCoinsAfterNumOfCopays();
			
			if (null == coinsAfterNumOfCopay) {
				coinsAfterNumOfCopay = 0l;
			}
			afterNumOfVisit = healthBenefit.getPlanHealth().getBeginPmryCareCsAfterNumOfVisits();
			
			if (null == afterNumOfVisit) {
				afterNumOfVisit = 0l;
			}

			if (LOGGER.isDebugEnabled()) {
				// if Debug mode than check this and log Warning
				if (afterNumOfVisit > 0 && coinsAfterNumOfCopay > 0) {
					LOGGER.warn("Both Num of Visit and Coinsurance after num of copay are greater than 0. Issuer Plan Number:"
							+ healthBenefit.getPlanHealth().getPlan().getIssuerPlanNumber());
				}
			}

			if(CollectionUtils.isEmpty(planDispRulesMap)) {
				t1Disptext = serfUtil.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit,
						healthBenefit.getNetworkT1CoinsurVal(), healthBenefit.getNetworkT1CoinsurAttr(),
						healthBenefit.getNetworkT1CopayVal(), healthBenefit.getNetworkT1CopayAttr(), healthBenefit.getName() ,true, isPHIXProfile);
	
				
				if(processAll){
					tileDisplayValue = serfUtil.getBenefitDisplayTileText(healthBenefit.getNetworkT1CoinsurVal(), healthBenefit.getNetworkT1CoinsurAttr(),
							healthBenefit.getNetworkT1CopayVal(), healthBenefit.getNetworkT1CopayAttr(), isPHIXProfile);
	
					t2DispText = serfUtil.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit,
							healthBenefit.getNetworkT2CoinsurVal(), healthBenefit.getNetworkT2CoinsurAttr(),
							healthBenefit.getNetworkT2CopayVal(), healthBenefit.getNetworkT2CopayAttr(), healthBenefit.getName() , false, isPHIXProfile);
	
					oonDispText = serfUtil.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit,
							healthBenefit.getOutOfNetworkCoinsurVal(), healthBenefit.getOutOfNetworkCoinsurAttr(),
							healthBenefit.getOutOfNetworkCopayVal(), healthBenefit.getOutOfNetworkCopayAttr(), healthBenefit.getName() , false, isPHIXProfile);
				}
			} else {
				BenefitsDisplayMappingParamsDTO benefitsDisplayMappingParamsDTO = null;
				PlanHealth planHealth = healthBenefit.getPlanHealth();
				if(planDisplayMapping.isExtendedParamSupported()) {
					benefitsDisplayMappingParamsDTO = new BenefitsDisplayMappingParamsDTO();
					benefitsDisplayMappingParamsDTO.setBenefitName(healthBenefit.getName());
					if(null != planHealth.getMaxCoinsuranceForSPDrug()) {
						benefitsDisplayMappingParamsDTO.setMaximumCoinsuranceForSpecialtyDrugs(planHealth.getMaxCoinsuranceForSPDrug());
					}
					if(null != planHealth.getMaxDaysForChargingInpatientCopay()) {
						benefitsDisplayMappingParamsDTO.setMaxNumDaysForChargingInpatientCopay(planHealth.getMaxDaysForChargingInpatientCopay());
					}
					benefitsDisplayMappingParamsDTO.setMedicalAndDrugIntegrated("YES".equalsIgnoreCase(planHealth.getIsMedDrugDeductibleIntegrated()));
					benefitsDisplayMappingParamsDTO.setPlanType(planHealth.getPlan().getNetworkType());
				}
				if(null != benefitsDisplayMappingParamsDTO) {
					benefitsDisplayMappingParamsDTO.setNetworkType(BenefitsDisplayMappingParamsDTO.NetworkType.IN_NETWORK_TIER1);
				}
				t1Disptext = planDisplayMapping.getBenefitDisplayText(healthBenefit.getNetworkT1CoinsurVal(), healthBenefit.getNetworkT1CoinsurAttr(),
						healthBenefit.getNetworkT1CopayVal(), healthBenefit.getNetworkT1CopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, 
						SerffUtils.PRIMARY_CARE_BENEFIT.equals(healthBenefit.getName()), planDispRulesMap, benefitsDisplayMappingParamsDTO);
	
				
				if(processAll){
					tileDisplayValue = planDisplayMapping.getBenefitDisplayTileText(healthBenefit.getNetworkT1CoinsurVal(), healthBenefit.getNetworkT1CoinsurAttr(),
							healthBenefit.getNetworkT1CopayVal(), healthBenefit.getNetworkT1CopayAttr(), planDispRulesMap);
	
					if(null != benefitsDisplayMappingParamsDTO) {
						benefitsDisplayMappingParamsDTO.setNetworkType(BenefitsDisplayMappingParamsDTO.NetworkType.IN_NETWORK_TIER2);
					}
					t2DispText = planDisplayMapping.getBenefitDisplayText(healthBenefit.getNetworkT2CoinsurVal(), healthBenefit.getNetworkT2CoinsurAttr(),
							healthBenefit.getNetworkT2CopayVal(), healthBenefit.getNetworkT2CopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, false, planDispRulesMap, benefitsDisplayMappingParamsDTO);
	
					if(null != benefitsDisplayMappingParamsDTO) {
						benefitsDisplayMappingParamsDTO.setNetworkType(BenefitsDisplayMappingParamsDTO.NetworkType.OUT_OF_NETWORK);
					}
					oonDispText = planDisplayMapping.getBenefitDisplayText(healthBenefit.getOutOfNetworkCoinsurVal(), healthBenefit.getOutOfNetworkCoinsurAttr(),
							healthBenefit.getOutOfNetworkCopayVal(), healthBenefit.getOutOfNetworkCopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, false, planDispRulesMap, benefitsDisplayMappingParamsDTO);
				}
			}
			healthBenefit.setNetworkT1display(t1Disptext);
			if(processAll){
				healthBenefit.setNetworkT1TileDisplay(tileDisplayValue);
				healthBenefit.setNetworkT2display(t2DispText);
				healthBenefit.setOutOfNetworkDisplay(oonDispText);
				healthBenefit.setLimitExcepDisplay(serfUtil.getLimitAndExclusionText(healthBenefit.getNetworkLimitation(), 
						healthBenefit.getNetworkLimitationAttribute(), healthBenefit.getNetworkExceptions()));
			}
			
			if(healthBenefit.getCreationTimestamp() == null) {
				healthBenefit.setCreationTimestamp(healthBenefit.getPlanHealth().getPlan().getCreationTimestamp());
			}
			entityManager.merge(healthBenefit);
		}
		
	}

	/**
	 * Method is used to update dental display benefits text
	 * @param dentalBenefitList ,entityManager ,processAll
	 * 
	 */
	private void updateDentalBenefitDisplayText(String planOrIssuerId, List<PlanDentalBenefit> dentalBenefitList, EntityManager entityManager , boolean processAll) {
		Long coinsAfterNumOfCopay = null;
		Long afterNumOfVisit = null;
		String tileDisplayValue = null, t1Disptext = null, t2DispText = null, oonDispText = null;
		boolean isPHIXProfile = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE).equals(SerffConstants.PHIX_PROFILE);
		PlanDisplayRulesMap planDispRulesMap = null;
		
		//Commented for HIX-63472 - use DB based display text rules 
		//if(SerffConstants.YES_ABBR.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.USE_BENEFIT_DISPLAY_RULES_FROM_DB))) {
			LOGGER.info("Fetching Benefit Display Rules for Issuer: " + planOrIssuerId.substring(0, SerffConstants.LEN_HIOS_ISSUER_ID));
			planDispRulesMap = planDisplayMapping.loadPlanDisplayRulesInMap(planOrIssuerId.substring(0, SerffConstants.LEN_HIOS_ISSUER_ID));
			if(!CollectionUtils.isEmpty(planDispRulesMap)) {
				LOGGER.info("Got " + planDispRulesMap.size() + " Benefit Display Rules for Issuer: " + planDispRulesMap.elements().nextElement().getHiosIssuerId());
			} else {
				LOGGER.warn("Got No Benefit Display Rules for Issuer: " + planDispRulesMap.elements().nextElement().getHiosIssuerId());
			}
		//}

		for (PlanDentalBenefit dentalBenefit : dentalBenefitList) {
			
			LOGGER.debug("Updating Plan Dental Benefit's Issuer Plan Number:"
					+ dentalBenefit.getPlanDental().getPlan().getIssuerPlanNumber() + " and Benefit Name "
					+ dentalBenefit.getName());
			/*coinsAfterNumOfCopay = dentalBenefit.getPlanDental().getBeginPmrycareDedtOrCoinsAfterNumOfCopays();*/
			
			if (null == coinsAfterNumOfCopay) {
				coinsAfterNumOfCopay = 0l;
			}
			/*afterNumOfVisit = dentalBenefit.getPlanDental().getBeginPmryCareCsAfterNumOfVisits();
			*/
			if (null == afterNumOfVisit) {
				afterNumOfVisit = 0l;
			}

			if(CollectionUtils.isEmpty(planDispRulesMap)) {
				t1Disptext = serfUtil.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit,
						dentalBenefit.getNetworkT1CoinsurVal(), dentalBenefit.getNetworkT1CoinsurAttr(),
						dentalBenefit.getNetworkT1CopayVal(), dentalBenefit.getNetworkT1CopayAttr(), dentalBenefit.getName() ,true, isPHIXProfile);
	
				
				if(processAll){
					tileDisplayValue = serfUtil.getBenefitDisplayTileText(dentalBenefit.getNetworkT1CoinsurVal(), dentalBenefit.getNetworkT1CoinsurAttr(),
							dentalBenefit.getNetworkT1CopayVal(), dentalBenefit.getNetworkT1CopayAttr(), isPHIXProfile);
	
	
					t2DispText = serfUtil.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit,
							dentalBenefit.getNetworkT2CoinsurVal(), dentalBenefit.getNetworkT2CoinsurAttr(),
							dentalBenefit.getNetworkT2CopayVal(), dentalBenefit.getNetworkT2CopayAttr(), dentalBenefit.getName() , false, isPHIXProfile);
	
	
					oonDispText = serfUtil.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit,
							dentalBenefit.getOutOfNetworkCoinsurVal(), dentalBenefit.getOutOfNetworkCoinsurAttr(),
							dentalBenefit.getOutOfNetworkCopayVal(), dentalBenefit.getOutOfNetworkCopayAttr(), dentalBenefit.getName() , false, isPHIXProfile);
				}
			} else {
				BenefitsDisplayMappingParamsDTO benefitsDisplayMappingParamsDTO = null;
				t1Disptext = planDisplayMapping.getBenefitDisplayText(dentalBenefit.getNetworkT1CoinsurVal(), dentalBenefit.getNetworkT1CoinsurAttr(),
						dentalBenefit.getNetworkT1CopayVal(), dentalBenefit.getNetworkT1CopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, 
						SerffUtils.PRIMARY_CARE_BENEFIT.equals(dentalBenefit.getName()), planDispRulesMap, benefitsDisplayMappingParamsDTO);
	
				
				if(processAll){
					tileDisplayValue = planDisplayMapping.getBenefitDisplayTileText(dentalBenefit.getNetworkT1CoinsurVal(), dentalBenefit.getNetworkT1CoinsurAttr(),
							dentalBenefit.getNetworkT1CopayVal(), dentalBenefit.getNetworkT1CopayAttr(), planDispRulesMap);
	
	
					t2DispText = planDisplayMapping.getBenefitDisplayText(dentalBenefit.getNetworkT2CoinsurVal(), dentalBenefit.getNetworkT2CoinsurAttr(),
							dentalBenefit.getNetworkT2CopayVal(), dentalBenefit.getNetworkT2CopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, false, planDispRulesMap, benefitsDisplayMappingParamsDTO);
	
	
					oonDispText = planDisplayMapping.getBenefitDisplayText(dentalBenefit.getOutOfNetworkCoinsurVal(), dentalBenefit.getOutOfNetworkCoinsurAttr(),
							dentalBenefit.getOutOfNetworkCopayVal(), dentalBenefit.getOutOfNetworkCopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, false, planDispRulesMap, benefitsDisplayMappingParamsDTO);
				}
			}
			
			dentalBenefit.setNetworkT1display(t1Disptext);
			if(processAll){
				dentalBenefit.setNetworkT1TileDisplay(tileDisplayValue);
				dentalBenefit.setNetworkT2display(t2DispText);
				dentalBenefit.setOutOfNetworkDisplay(oonDispText);
				dentalBenefit.setLimitExcepDisplay(serfUtil.getLimitAndExclusionText(dentalBenefit.getNetworkLimitation(), 
						dentalBenefit.getNetworkLimitationAttribute(), dentalBenefit.getNetworkExceptions()));
			}
			if(dentalBenefit.getCreationTimestamp() == null) {
				dentalBenefit.setCreationTimestamp(dentalBenefit.getPlanDental().getPlan().getCreationTimestamp());
			}
			entityManager.merge(dentalBenefit);
		}
	}

	private Set<String> getDentalPlanIDList(List<PlanDentalBenefit> dentalBenefitList) {
		Set<String> planIdSet = new HashSet<String>();
		for (PlanDentalBenefit plan : dentalBenefitList) {
			planIdSet.add(String.valueOf(plan.getPlanDental().getPlan().getId()));      
		}
		return planIdSet;
	}
	
	private Set<String> getHealthPlanIDList(List<PlanHealthBenefit> healthBenefitList) {
		Set<String> planIdSet = new HashSet<String>();
		for (PlanHealthBenefit plan : healthBenefitList) {
			planIdSet.add(String.valueOf(plan.getPlanHealth().getPlan().getId()));      
		}
		return planIdSet;
	}

	private void refreshPlansInSOLR(String planType, Set<String> planIdList) {
		for (String planId : planIdList) {
			LOGGER.info("Updating Plan to SOLR: PlanID: " + planId + " --- " + GhixEndPoints.PlandisplayEndpoints.UPDATE_PLAN_BENEFITS_AND_COST_IN_SOLR + "/" + planType
	        		+ "/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(planId));
	        //restTemplate.getForObject(GhixEndPoints.PlandisplayEndpoints.UPDATE_PLAN_BENEFITS_AND_COST_IN_SOLR + "/" + planType
	        //		+ "/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(planId), String.class);      
			ghixRestTemplate.exchange(GhixEndPoints.PlandisplayEndpoints.UPDATE_PLAN_BENEFITS_AND_COST_IN_SOLR + "/" + planType
	        		+ "/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(planId), EXCHANGE_USER, HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null);      
		}
	}

	@Override
	public Plan getPlanIdbyIssuerPlanNumberAndIsDeleted(String issuerPlanNumber , String isDeleted) {
		Plan plan  = null;
		if (null != issuerPlanNumber) {
			plan = planRepo.getPlanIdbyIssuerPlanNumberAndIsDeleted(issuerPlanNumber , isDeleted);
			
		}
		return plan;
	}

	@Override
	public List<STMHCCPlansRate> getallratesbyPlanId(Integer planId, String isDeleted) {
		
		return  iStmHccRaterepo.findByPlanIdAndIsDeleted(planId, isDeleted);
	}

	@Override
	public List<STMHCCPlansAreaFactor> getallAreaFactor() {
		return  iStmAreaFactorrepo.findAllAreaFactors();
	}

	@Override
	public List<STMHCCPlansAdminFees> getallAdminFees() {
		return istmAdminFeeRepo.findAllAdminFees();
	}
	
	
	/**
	 * 
	 * @param fileName
	 * @param message
	 * @param planFileName
	 * @return
	 */
	@Override
	public SerffPlanMgmt getTrackingRecord(String serffRequestId, StringBuilder message, String fileName) {
		
		SerffPlanMgmt trackingRecord = null;
		
		if (StringUtils.isNotBlank(serffRequestId) && StringUtils.isNumeric(serffRequestId)) {
			// Get tracking record from SERFF_PLAN_MGMT table. 
			trackingRecord = getSerffPlanMgmtById(Long.parseLong(serffRequestId));
			
			if (null == trackingRecord) {
				message.append("Tracking record is not available for file: ");
				message.append(fileName);
			}
		}
		else {
			message.append("Tracking record Id is not linked to file Name: ");
			message.append(fileName);
		}
		return trackingRecord;
	}
	
	

	/**
	 * Method is used to re-apply edited benefits to existing plans using Plan Management API.
	 */
	@Override
	public String reapplyEditBenefitsToPlan(String applicableYear, String issuerPlanNumber) {

		LOGGER.info("Execution of reapplyEditBenefitsToPlan() Begins.");
		String reapplyEditMessage = null;

		try {
			// Get the new plan from db which is recently reloaded but lost benefit edits which were done when the old-plan was active.
			// This newly loaded plan will qualify to re-apply lost benefit edits.
			com.getinsured.hix.model.Plan activePlan = planRepo.findByIssuerPlanNumberAndIsDeletedAndApplicableYear(issuerPlanNumber, SerffConstants.NO_ABBR, Integer.parseInt(applicableYear));

			if (null != activePlan) {//Redundant safety check.

				PlanRequest planRequest = new PlanRequest();
			    planRequest.setPlanId(Integer.toString(activePlan.getId()));
			    // Invoking Reapply Benefits API from Plan-Management module
			    String apiResponse = ghixRestTemplate.postForObject(PlanMgmtEndPoints.REAPPLY_PLAN_BENEFIT_EDITS, planRequest, String.class);
			    LOGGER.debug("REAPPLY_PLAN_BENEFIT_EDITS API response: " + apiResponse);

			    if (StringUtils.isNotBlank(apiResponse)) {
			    	PlanResponse planResponse = platformGson.fromJson(apiResponse, PlanResponse.class);

			    	if (null != planResponse) {
			    		reapplyEditMessage = planResponse.getErrMsg();
			    	}
			    	else {
			    		LOGGER.error("API PlanResponse object is null.");
			    	}
			    }
			    else {
			    	LOGGER.error("API response is empty.");
			    }
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred in Reapply Edit Benefits To Plan: " + ex.getMessage(), ex);
		}
		finally {

			if (StringUtils.isBlank(reapplyEditMessage)) {
				reapplyEditMessage = "Error: Benefits cannot be updated!";
			}
			LOGGER.info("Execution of reapplyEditBenefitsToPlan() End.");
		}
		return reapplyEditMessage;
	}
	
	@Override
	public List<Issuer> getAllIssuerForState(String stateCode) {
		List <Issuer> issuerList = null;
		issuerList= iIssuerRepository.getByState(stateCode);
		return issuerList;
	}

	@Override
	public Long getPlanCountforIssuer(String issuerId , String isDeleted) {
		Long planCount = null;
		List <Issuer> issuerList = null;
		Integer id = null;
		issuerList = iIssuerRepository.getIssuerFromHiosID(issuerId);
		if(!CollectionUtils.isEmpty(issuerList)){
			id = issuerList.get(0).getId();
			planCount = planRepo.getPlanCountForIssuerId(id , isDeleted);
		}
		return planCount;
	}

	@Override
	public SerffPlanMgmtBatch getTrackingBatchRecord(String fileName, String fileType, StringBuilder message) {
		SerffPlanMgmtBatch trackingRecord = null;
		String serffReqId = fileName.substring(0, fileName.indexOf(SerffConstants.FTP_ANCILLARY_FILEID_SUFFIX+fileType));
		
		if (StringUtils.isNotBlank(serffReqId) && StringUtils.isNumeric(serffReqId)) {
			// Get tracking record from SERFF_PLAN_MGMT_BATCH table. 
			trackingRecord = getSerffPlanMgmtBatchById(Long.parseLong(serffReqId));
			
			if (null == trackingRecord) {
				message.append("Tracking record is not available for file: ");
				message.append(fileName);
			}
		}
		else {
			message.append("Tracking record Id is not linked to file Name: ");
			message.append(fileName);
		}
		return trackingRecord;
	}
	
	/* (non-Javadoc)
	 * @see com.serff.service.SerffService#updateTrackingBatchRecordAsInProgress(com.getinsured.hix.model.serff.SerffPlanMgmtBatch)
	 */
	@Override
	public SerffPlanMgmtBatch updateTrackingBatchRecordAsInProgress(SerffPlanMgmtBatch trackingBatchRecord) {
		if(null != trackingBatchRecord) {
			trackingBatchRecord.setBatchStartTime(new Date());
			trackingBatchRecord.setBatchStatus(SerffPlanMgmtBatch.BATCH_STATUS.IN_PROGRESS);
			return saveSerffPlanMgmtBatch(trackingBatchRecord);
		} 
		return trackingBatchRecord;
	}

	/* (non-Javadoc)
	 * @see com.serff.service.SerffService#updateTrackingBatchRecordAsCompleted(com.getinsured.hix.model.serff.SerffPlanMgmtBatch, com.getinsured.hix.model.serff.SerffPlanMgmt, com.getinsured.hix.model.serff.SerffPlanMgmtBatch.BATCH_STATUS)
	 */
	@Override
	public SerffPlanMgmtBatch updateTrackingBatchRecordAsCompleted(SerffPlanMgmtBatch trackingBatchRecord, SerffPlanMgmt serffTrackingRecord, SerffPlanMgmtBatch.BATCH_STATUS status ) {
		if(null != trackingBatchRecord) {
			trackingBatchRecord.setBatchEndTime(new Date());
			trackingBatchRecord.setSerffReqId(serffTrackingRecord);
			trackingBatchRecord.setBatchStatus(status);
			return saveSerffPlanMgmtBatch(trackingBatchRecord);
		} 
		return trackingBatchRecord;
	}
}