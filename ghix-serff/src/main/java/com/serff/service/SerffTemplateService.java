package com.serff.service;

import com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws.PlanMgmtRequest;
import com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws.PlanMgmtResponse;
import com.serff.util.PlanUpdateStatistics;

public interface SerffTemplateService {

	PlanMgmtResponse tranferPlan(PlanMgmtRequest req, String exchangeTypeFilter, String tenantCode, PlanUpdateStatistics uploadStatistics, byte[] logoBytes);
	PlanMgmtResponse processCSR(PlanMgmtRequest req);
}
