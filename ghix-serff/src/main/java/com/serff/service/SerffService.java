package com.serff.service;

import java.util.List;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.STMHCCPlansAdminFees;
import com.getinsured.hix.model.STMHCCPlansAreaFactor;
import com.getinsured.hix.model.STMHCCPlansRate;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Interface is used to provide services of SERFF.
 * @author Bhavin Parmar
 */
public interface SerffService {

	SerffPlanMgmt getSerffPlanMgmtById(long id);
	SerffPlanMgmt saveSerffPlanMgmt(final SerffPlanMgmt serffPlanMgmt);
	
	SerffDocument getSerffDocumentById(long id);
	SerffDocument saveSerffDocument(final SerffDocument serffDocument);
	
	SerffPlanMgmtBatch getSerffPlanMgmtBatchById(long id);
	SerffPlanMgmtBatch saveSerffPlanMgmtBatch(final SerffPlanMgmtBatch serffPlanMgmtBatch);

	//SerffConstants.PUF_STATUS doMultipartFormPost(boolean isLocalPath, String carrier, String carrierText,
	//		String stateCodes, String exchangeType, String tenant, String sourceFolderPath, String postURL);
	
	//void pushPUFToQA(String batchId ,String hiosId, String srcPath);
	
	//void pushPUFToStage(String batchId ,String hiosId, String srcPath);
	
	void generateTemplatesForPUF(String stateCode, String hiosId) throws GIException;
	
	void getAllHealthBenefits(String[] issuerIdArr);
	
	void getAllDentalBenefits(String[] issuerIdArr);
	
	void getAllHealthBenefitsByPlans(String planOrIssuerId, Integer applicableYear, boolean processAll);

	void getAllDentalBenefitsByPlans(String planOrIssuerId, Integer applicableYear, boolean processAll);
	
	Plan getPlanIdbyIssuerPlanNumberAndIsDeleted(String issuerPlanNumber , String isDeleted);
	
	List<STMHCCPlansRate> getallratesbyPlanId(Integer planId , String isDeleted);
	
	List<STMHCCPlansAreaFactor> getallAreaFactor();
	
	List<STMHCCPlansAdminFees> getallAdminFees();
	
	SerffPlanMgmt getTrackingRecord(String serffRequestId, StringBuilder message, String fileName);
	
	String reapplyEditBenefitsToPlan(String applicableYear, String issuerPlanNumber);
	
	List<Issuer> getAllIssuerForState(String stateCode);
	
	Long getPlanCountforIssuer(String issuerId ,String isDeleted);
	
	SerffPlanMgmtBatch getTrackingBatchRecord(String fileName, String fileType, StringBuilder message);
	
	SerffPlanMgmtBatch updateTrackingBatchRecordAsInProgress(SerffPlanMgmtBatch trackingBatchRecord);
	
	SerffPlanMgmtBatch updateTrackingBatchRecordAsCompleted(SerffPlanMgmtBatch trackingBatchRecord, SerffPlanMgmt serffTrackingRecord, SerffPlanMgmtBatch.BATCH_STATUS status);

}