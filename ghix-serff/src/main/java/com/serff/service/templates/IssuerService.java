package com.serff.service.templates;

import com.getinsured.hix.model.Issuer;

public interface IssuerService {

	Issuer saveIssuer(Issuer issuer);
    
	Issuer getIssuerByHiosID(String hiosIssuerID);
	
}
