package com.serff.service.templates;

import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.medicaid.MedicaidPlanInfoVO;

/**
 * Interface is used to persist Medicaid Plans to database using
 * MedicaidPlanMgmtSerffServiceImpl class.
 * 
 * @author Bhavin Parmar
 * @since January 15, 2015
 */
public interface MedicaidPlanMgmtSerffService {

	boolean populateAndPersistMedicaidData(MedicaidPlanInfoVO planInfoVO, SerffPlanMgmt trackingRecord) throws GIException;
}
