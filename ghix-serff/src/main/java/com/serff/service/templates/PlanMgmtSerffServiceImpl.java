package com.serff.service.templates;

import static com.getinsured.hix.platform.util.GhixConstants.RESPONSE_SUCCESS;
import static com.serff.util.SerffConstants.ALLOWS_ADULT_ONLY;
import static com.serff.util.SerffConstants.ALLOWS_CHILD_ONLY;
import static com.serff.util.SerffConstants.DOLLAR;
import static com.serff.util.SerffConstants.DOLLARSIGN;
import static com.serff.util.SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE;
import static com.serff.util.SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_MANY_DEPENDENT;
import static com.serff.util.SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_ONE_DEPENDENT;
import static com.serff.util.SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_TWO_DEPENDENT;
import static com.serff.util.SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE;
import static com.serff.util.SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_MANY_DEPENDENT;
import static com.serff.util.SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_ONE_DEPENDENT;
import static com.serff.util.SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_TWO_DEPENDENT;
import static com.serff.util.SerffConstants.PERCENTAGE;
import static java.lang.Boolean.FALSE;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.BusinessRule;
import com.getinsured.hix.model.CostShare;
import com.getinsured.hix.model.Drug;
import com.getinsured.hix.model.DrugList;
import com.getinsured.hix.model.DrugTier;
import com.getinsured.hix.model.Formulary;
import com.getinsured.hix.model.FormularyCostShareBenefit;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Issuer.certification_status;
import com.getinsured.hix.model.IssuerAccreditation;
import com.getinsured.hix.model.IssuerServiceArea;
import com.getinsured.hix.model.IssuerServiceAreaExt;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Network;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.IS_PUF;
import com.getinsured.hix.model.Plan.IssuerVerificationStatus;
import com.getinsured.hix.model.Plan.PlanStatus;
import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanDentalCost;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.PlanHealthCost;
import com.getinsured.hix.model.PlanRate;
import com.getinsured.hix.model.PlanRate.RATE_OPTION;
import com.getinsured.hix.model.PlanSbcScenario;
import com.getinsured.hix.model.RatingArea;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.URRTAllowedClaim;
import com.getinsured.hix.model.URRTInsurancePlanRateChange;
import com.getinsured.hix.model.URRTPRCPremiumClaimInfo;
import com.getinsured.hix.model.URRTPRCPremiumComponent;
import com.getinsured.hix.model.URRTProductRateChange;
import com.getinsured.hix.model.UnifiedRate;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration.PlanmgmtConfigurationEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.lookup.repository.ILookupRepository;
import com.getinsured.hix.platform.repository.IZipCodeRepository;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.serff.config.SerffConfiguration.SerffConfigurationEnum;
import com.serff.dto.BenefitsDisplayMappingParamsDTO;
import com.serff.dto.SupportingDocumentDTO;
/*import com.serff.template.ncqa.IssuerType;
import com.serff.template.urac.IssuerType;*/
import com.serff.planmanagementexchangeapi.exchange.model.pm.TransferDataTemplate;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlan;
import com.serff.repository.templates.IBusinessRuleRepository;
import com.serff.repository.templates.ICostShareRepository;
import com.serff.repository.templates.IFormularyCostShareBenefitRepository;
import com.serff.repository.templates.IFormularyRepository;
import com.serff.repository.templates.IIssuerServiceAreaRepository;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.INetworkRepository;
import com.serff.repository.templates.IPlanDentalBenefitRepository;
import com.serff.repository.templates.IPlanHealthBenefitRepository;
import com.serff.repository.templates.IPlanHealthRepository;
import com.serff.repository.templates.IRatingAreaRepository;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.repository.templates.IServiceAreaRepository;
import com.serff.repository.templates.IUnifiedRatePlanRateChangeRepository;
import com.serff.repository.templates.IUnifiedRateProductRateChangeRepository;
import com.serff.repository.templates.IUnifiedRateRepository;
import com.serff.repository.templates.IissuerAccreditation;
import com.serff.service.PlanDisplayMapping;
import com.serff.service.PlanDisplayRulesMap;
import com.serff.service.validation.PlanValidator;
import com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType;
import com.serff.template.csr.extension.InsurancePlanVariantType;
import com.serff.template.csr.hix.pm.InsurancePlanType;
import com.serff.template.csr.niem.core.AmountType;
import com.serff.template.ncqa.NCQAAccreditationType;
import com.serff.template.networkProv.hix.core.OrganizationType;
import com.serff.template.networkProv.hix.pm.HealthcareProviderNetworkType;
import com.serff.template.plan.AvCalculatorVO;
import com.serff.template.plan.BenefitsListVO;
import com.serff.template.plan.CostShareVarianceVO;
import com.serff.template.plan.CostSharingTypeVO;
import com.serff.template.plan.DrugListVO;
import com.serff.template.plan.DrugTierLevelVO;
import com.serff.template.plan.DrugVO;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.FormularyCostSharingTypeVO;
import com.serff.template.plan.FormularyVO;
import com.serff.template.plan.MoopListVO;
import com.serff.template.plan.MoopVO;
import com.serff.template.plan.PlanAndBenefitsPackageVO;
import com.serff.template.plan.PlanAndBenefitsVO;
import com.serff.template.plan.PlanBenefitTemplateVO;
import com.serff.template.plan.PlanDeductibleListVO;
import com.serff.template.plan.PlanDeductibleVO;
import com.serff.template.plan.PlansListVO;
import com.serff.template.plan.PrescriptionDrugTemplateVO;
import com.serff.template.plan.QhpApplicationRateItemVO;
import com.serff.template.plan.ServiceVisitVO;
import com.serff.template.rbrules.hix.pm.FamilyRelationshipCohabitationRuleType;
import com.serff.template.rbrules.hix.pm.RateCategoryDeterminationRulesetType;
import com.serff.template.svcarea.hix.pm.GeographicAreaType;
import com.serff.template.svcarea.hix.pm.ServiceAreaType;
import com.serff.template.svcarea.niem.fips.USCountyCodeType;
import com.serff.template.urac.AccreditationType;
import com.serff.template.urrt.InsuranceRateForecastTempVO;
import com.serff.template.urrt.InsuranceRateForecastTempVO.ChangedInsuranceProductRateChange;
import com.serff.template.urrt.InsuranceRateForecastTempVO.ChangedInsuranceProductRateChange.AssociatingInsurancePlanRateChange;
import com.serff.template.urrt.InsuranceRateForecastTempVO.ChangedInsuranceProductRateChange.AssociatingInsurancePlanRateChange.DefinedInsurancePlanPremiumComponent;
import com.serff.template.urrt.InsuranceRateForecastTempVO.ChangedInsuranceProductRateChange.AssociatingInsurancePlanRateChange.ProjecedInsurancePlanRateChangePeriod;
import com.serff.template.urrt.InsuranceRateForecastTempVO.ChangedInsuranceProductRateChange.ProjectedInsuranceProductLifetimeLoss;
import com.serff.template.urrt.InsuranceRateForecastTempVO.CollectedBenefitCategoryAllowedClaim;
import com.serff.util.FamilyRelationEnum;
import com.serff.util.PlanUpdateStatistics;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * PlanManagement SERFF Service implementation class.
 * Important rules for loading Plans [Update rule if any change]:
 * 1. All templates are at plan level only not at variant level.
 * 2. Certification process is at variable level.
 * 3. If any one variant certified, treat all as certified and continue [linked new variant with old common templates data].
 * 4. If all variants are not certified, then update common templates only once [lined old variant with new common data].
 * 5. Don't delete old plan variant if it is not present in new arrival template.
 * 6. All plans must have same Applicable Year in template.
 * 7. Applicable Year must be apply on Plan Load for templates [Plan & Benefit, Prescription Drug, Service Area, Network & Business Rules].
 *
 * @author chalse_v
 * @version 1.6
 * @since June 19, 2013
 */
@Service("planMgmtSerffService")
public class PlanMgmtSerffServiceImpl implements PlanMgmtSerffService {
	
	private static final Logger LOGGER = Logger.getLogger(PlanMgmtSerffServiceImpl.class);
	private static final String MSG_AND_ISSUER_IDENTIFIED_BY = "' and current Issuer identified by '";
	private static final String MSG_PLAN_IDENTIFIED_BY = "', Plan identified by '";
	private static final String TIME_UNIT_CODE_MONTH = "MON";
	private static final String DRUG_COST_SHARING_PHARMACY_CODE_RETAIL= "Retail";
	private static final String DRUG_COST_SHARING_PHARMACY_CODE_MAIL_ORDER = "MailOrder";
	private static final String NO_CHARGE ="No Charge";
	private static final String NOT_APPLICABLE ="Not Applicable";
	private static final String COINSURANCE_IN_NETWORK_TIER1 = "coInsuranceInNetworkTier1";
	private static final String COINSURANCE_IN_NETWORK_TIER2 = "coInsuranceInNetworkTier2";
	private static final String COINSURANCE_OUT_OF_NETWORK = "coInsuranceOutOfNetwork";
	private static final String COPAY_IN_NETWORK_TIER1 = "copayInNetworkTier1";
	private static final String COPAY_IN_NETWORK_TIER2 = "copayInNetworkTier2";
	private static final String COPAY_OUT_OF_NETWORK = "copayOutOfNetwork";
	private static final String COPAY_NETWORK_TIER1_ATTR ="copayNetworkTier1Attr";
	private static final String COPAY_NETWORK_TIER1_VALUE ="copayNetworkTier1Value";
	private static final String COPAY_NETWORK_TIER2_ATTR ="copayNetworkTier2Attr";
	private static final String COPAY_NETWORK_TIER2_VALUE ="copayNetworkTier2Value";
	private static final String COPAY_NO_NETWORK_ATTR ="copayNoNetworkAttr";
	private static final String COPAY_NO_NETWORK_VALUE ="copayNoNetworkValue";
	private static final String COINS_NETWORK_TIER1_ATTR ="coinsNetworkTier1Attr";
	private static final String COINS_NETWORK_TIER1_VALUE ="coinsNetworkTier1Value";
	private static final String COINS_NETWORK_TIER2_ATTR ="coinsNetworkTier2Attr";
	private static final String COINS_NETWORK_TIER2_VALUE ="coinsNetworkTier2Value";
	private static final String COINS_NO_NETWORK_ATTR ="coinsNoNetworkAttr";
	private static final String COINS_NO_NETWORK_VALUE ="coinsNoNetworkValue";
	private static final String COPAY = "Dollar Amount copay";
	private static final String COINSURANCE = "Percent co-insurance";
	private static final String ZERO = "0";
	private static final String YEAR2 = "Year - 2";
	private static final String YEAR1 = "Year - 1";
	private static final String YEAR0 = "Year - 0";
	private static final String STATUS_MSG = " Executing populateAndPersistAllBeansFromSERFF() is done with status as '.";
	private static final String ID_NOT_PRESENT = " is present in database but not present in template.";
	private static final String DATE_FORMAT = "yyyy-MM-dd";
	private static final String SVCAREA_DELETED = "All records marked for deletion for issuerServiceArea =" ;
	private static final String NO_DEDUCTIBLE_MAP = "Saving supplied Deductible name : No mapping found for Deductible type[";
	private static final String NOT_COVERED = "Not Covered";
	
	protected static final String YES = "yes";
	protected static final String BASIC_DENTAL_CARE_CHILD = "Basic Dental Care - Child";
	protected static final String DENTAL_CHECKUP_CHILD = "Dental Check-Up for Children";
	protected static final String MAX_OOP_DENTAL_ADULT_CONST = "MAX_OOP_DENTAL_ADULT";
	protected static final String COVERED = "Covered";
	private static final Character Y = 'Y';
	private static final Character N = 'N';

	private static final String IS_EXISTING_PLAN = "isExistingPlan";
	private static final String IS_CERTIFIED_PLAN = "isCertifiedPlan";
	private static final String IS_NONPUF_PLAN = "isNonPUFPlan";
	private static final String EXPIRATION_DATE = "12/31/";
	
	private static final int EXCLUSION_LEN = 2000;
	private static final int PARTIAL_COUNTY_JUSTIFICATION_TEXT_LEN = 1000;
	private static final int OUT_OF_AREA_COVERAGE_DESC_LEN = 4000;
	private static final int BENEFITS_EXCLUSION_TEXT_LEN = 4000;
	private static final int BENEFITS_EXPLANATION_TEXT_LEN = 4000;
	private int healthParentId = -1;
	private int dentalParentId = -1;
  
	//XML data constants.	
	private static final String PHONE_EXTENSION_NODE_START = "<phoneExt>";
	private static final String PHONE_EXTENSION_NODE_END = "</phoneExt>\n";
	private static final String PHONE_NUMBER_NODE_START = "<phoneNumber>";
	private static final String PHONE_NUMBER_NODE_END = "</phoneNumber>";
	private static final String EMAILADDRESS_NODE_START = "<emailAddress>";
	private static final String EMAILADDRESS_NODE_END = "</emailAddress>";
	private static final String LAST_NAME_NODE_START = "<lastName>";
	private static final String LAST_NAME_NODE_END = "</lastName>";
	private static final String FIRST_NAME_NODE_START = "<firstName>";
	private static final String FIRST_NAME_NODE_END = "</firstName>";
	private static final String CONTACT_NODE_START = "<Contact>\n";
	private static final String CONTACT_NODE_END = "</Contact>\n";
	private static final String ISSUER_NODE_START = "<Issuer>\n";
	private static final String ISSUER_NODE_END = "</Issuer>\n";
	private static final String INSURANCE_COMPANY_NODE_START = "<InsuranceCompany>\n";
	private static final String INSURANCE_COMPANY_NODE_END = "</InsuranceCompany>\n";
	private static final String ONLINE_NODE_START = "<Online>\n";
	private static final String ONLINE_NODE_END = "</Online>\n";
	private static final String ENROLLMENTCENTER_NODE_START = "<EnrollmentCenter>\n";
	private static final String ENROLLMENTCENTER_NODE_END = "</EnrollmentCenter>\n";
	private static final String USERACCESS_NODE_START = "<UserAccess>\n";
	private static final String USERACCESS_NODE_END = "</UserAccess>\n";
	private static final String FINANCIAL_NODE_START = "<Financial>\n";
	private static final String FINANCIAL_NODE_END = "</Financial>\n";
	private static final String RISK_NODE_START = "<Risk>\n";
	private static final String RISK_NODE_END = "</Risk>\n";
	private static final String NODE_NAME_INDIVIDUAL_MARKET = "IndividualMarket";
	private static final String NODE_NAME_SHOP = "Shop";
	private static final String NODE_NAME_CEO = "CEO";
	private static final String NODE_NAME_CFO = "CFO";
	private static final String NODE_NAME_ENROLLMENT = "Enrollment";
	private static final String NODE_NAME_PRIMARY = "Primary";
	private static final String NODE_NAME_BACKUP = "Backup";
	private static final String NODE_NAME_SYSTEM = "System";
	private static final String NODE_NAME_APPGRIEVANCE = "Appeals_Grievances";
	private static final String NODE_NAME_CUSTOMER_SERVICE_OPERATIONS = "CustomerServiceOperations";
	private static final String NODE_NAME_MARKETING = "Marketing";
	private static final String NODE_NAME_MEDICAL_DIRECTOR = "MedicalDirector";
	private static final String NODE_NAME_CHIEF_DENTAL_DIRECTOR = "ChiefDentalDirector";
	private static final String NODE_NAME_PHARMACY_BENEFIT_MANAGER = "PharmacyBenefitManager";
	private static final String NODE_NAME_GOVERNMENT_RELATIONS  = "GovernmentRelations";
	private static final String NODE_NAME_HIPPA_SECURITY_OFFICER  = "HIPAASecurityOfficer";
	private static final String NODE_NAME_COMPLAINTS_TRACKING  = "ComplaintsTracking";
	private static final String NODE_NAME_QUALITY  = "Quality";
	private static final String NODE_NAME_COMPLIANCE_OFFICER  = "ComplianceOfficer";
	private static final String NODE_NAME_PAYMENT  = "Payment";
	private static final String NODE_NAME_APTC_CSR  = "APTC_CSR";
	private static final String NODE_NAME_REPORTING  = "Reporting";
	private static final String NODE_NAME_TRANSFER  = "Transfer";
	private static final String NODE_NAME_CORRIDORS  = "Corridors";
	private static final String NODE_NAME_ADJUSTMENT  = "Adjustment";
	private static final String NODE_NAME_REINSURANCE  = "Reinsurance";
	// private static final String PLANMGMT_DUPLICATE_KEY = "2";
	private DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static final int ISSUER_RULE_TYPE = 1;
	private static final int PRODUCT_RULE_TYPE = 2;
	private static final int PLAN_RULE_TYPE = 3;
	private static final int SINGLE_COUNT = 1;
	private static final int MAX_CUSTOM_DEDUCTIBLE_COUNT = 5;
	private static final String PER_PERSON_STRING = "per person";
	private static final String PER_GROUP_STRING = "per group";
	private static final String AMOUNT_STRING_SEPERATOR = "|";
	private static final String COPAY_COINSURANCE_TEXT = "CoPay CoInsurance";
	private static final String AMOUNT_NOT_APPLICABLE_TEXT = "not applicable";

	// @Autowired private UserService userService;
	@Autowired private IssuerService serffIssuerService;
	@Autowired private IIssuersRepository iIssuerRepository;
	@Autowired private INetworkRepository iNetworkRepository;
	@Autowired private IRatingAreaRepository iRatingAreaRepository;
	@Autowired private ILookupRepository iLookupRepository;
	// @Autowired private ITenantPlanRepository iTenantPlanRepository;
	@Autowired private IIssuerServiceAreaRepository iIssuerServiceAreaRepository;
	@Autowired private IZipCodeRepository iZipCodeRepository;
	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private IPlanHealthRepository iSerffPlanHealthRepository;
	@Autowired private IPlanHealthBenefitRepository iSerffPlanHealthBenefitRepository;
	@Autowired private IPlanDentalBenefitRepository iSerffPlanDentalBenefitRepository;
	@Autowired private IServiceAreaRepository iServiceAreaRepository;
	@Autowired private IFormularyCostShareBenefitRepository iformualryCostShareBrnefit;
	@Autowired private ICostShareRepository iCostShare;
	@Autowired private UpdatePlanServiceImpl updatePlanService;
	@Autowired private PlanDisplayMapping planDisplayMapping;
	@Autowired private PlanDisplayMapping planDisplayMappingExtended;
	@Autowired private PlanDisplayMapping planDisplayMappingForCA;
	@Autowired private IFormularyRepository iSerffFormularyRepository;
	@Autowired private IBusinessRuleRepository ibusinessRuleRepo;
	@Autowired private IUnifiedRateRepository iUnifiedRateRepository;
	@Autowired private IUnifiedRateProductRateChangeRepository iUnifiedRateProductRateChangeRepository;
	@Autowired private IUnifiedRatePlanRateChangeRepository iUnifiedRatePlanRateChangeRepository;
	@Autowired private IissuerAccreditation iIssuerAccreditation;
	
	@Autowired private SerffUtils serffUtils;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	//private EntityManager entityManager;
	private boolean isPHIXProfile = false;
	

	/**
	 * Constructor.
	 */
	public PlanMgmtSerffServiceImpl() {
	}

	/**
	 * @see com.com.getinsured.hix.planmgmt.serff.service.PlanMgmtSerffService#populateAndPersistAllBeansFromSERFF(java.lang.Object[])
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean populateAndPersistAllBeansFromSERFF(Map<String, Object> reqTemplates, String exchangeTypeFilter, StringBuffer responseMessage, 
			String tenantIds, PlanUpdateStatistics uploadStatistics, byte[] logoBytes) throws GIException {
		LOGGER.info("Beginning executing populateAndPersistAllBeansFromSERFF().");
		isPHIXProfile = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE).equals(SerffConstants.PHIX_PROFILE);
		
		if(!isPHIXProfile) {
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
			if(SerffConstants.CONNECTICUT_STATE_CODE.equalsIgnoreCase(stateCode)) {
				//planDisplayMapping = (PlanDisplayMapping) GHIXApplicationContext.getBean("planDisplayMappingExtended");
				planDisplayMapping = planDisplayMappingExtended;
				LOGGER.debug("Using Extended PlanDisplayText Mappings for CT");
			} else if(SerffConstants.CA_STATE_CODE.equalsIgnoreCase(stateCode)) {
				planDisplayMapping = planDisplayMappingForCA;
				LOGGER.debug("Using Extended PlanDisplayText Mappings for CA");
			}
		}

		boolean isPlanExist = false, isPlanCertified = false, isNonPUFPlanExist = false;
		boolean isTemplateSaved =  Boolean.FALSE;
		boolean isPlansSaved =  Boolean.FALSE;
		Issuer issuer = null;
	    List<IssuerServiceArea> issuerServiceArea = null;
		Map<String, Formulary> savedFormulary=null;
		Map<String, BusinessRule> savedBusinessRule=null;
		Map<String, Network> networks = null;
		Map<String, List<PlanRate>> planRates = new HashMap<String, List<PlanRate>>();
		Map<String, Date> templatesLastModifiedDates = new HashMap<String, Date>();
		Map<String, String> documents = new HashMap<String, String>();
		//Map<String, UnifiedRate> savedUnifiedRate=null;
		Object object = null;
		Map<String, Boolean> planStatus = new HashMap<String, Boolean>();
		Map<String, Object> templateObjectMap = new HashMap<String, Object>();
		String issuerID = null;
		EntityManager entityManager = null;
		boolean status = Boolean.FALSE;
		boolean isBRulePresent = Boolean.FALSE;
		boolean persistOffExchangePlans= false, persistOnExchangePlans = true, persistPUFPlans = false; 
		PlanDisplayRulesMap planDispRulesMap = null;
		int applicableYear = 0;
		String planInsuranceType = null;
		Date planTemplateLastModifiedDate, rateTemplateLastModifiedDate;
		boolean isTemplateMessageCreated = false;

		Map<String, Date> lastUpdatedTemplateMap = new HashMap<String, Date>();
		
		if(isPHIXProfile) {
			if(exchangeTypeFilter.equals(SerffPlanMgmtBatch.EXCHANGE_TYPE.ANY.toString())) {
				persistOffExchangePlans= true; 
				persistOnExchangePlans = true;
			} else if(exchangeTypeFilter.equals(SerffPlanMgmtBatch.EXCHANGE_TYPE.OFF.toString())){
				persistOffExchangePlans= true; 
				persistOnExchangePlans = false;
			} else if(exchangeTypeFilter.equals(SerffPlanMgmtBatch.EXCHANGE_TYPE.ON.toString())){
				persistOffExchangePlans= false; 
				persistOnExchangePlans = true;
			} else if(exchangeTypeFilter.equals(SerffPlanMgmtBatch.EXCHANGE_TYPE.PUF.toString())){
				persistOffExchangePlans= false; 
				persistOnExchangePlans = true;
				persistPUFPlans = true;
			}
		}

		try {

			Validate.notNull(serffIssuerService, "IssuerService instance is not properly configured.");

			PlanBenefitTemplateVO planTemplate = null;

			entityManager = entityManagerFactory.createEntityManager();

			if (null == entityManager) {
				return status;
			}

			Object objPlanTemplate = reqTemplates.get(SerffConstants.DATA_BENEFITS);

			if (objPlanTemplate instanceof com.serff.template.plan.PlanBenefitTemplateVO) {
				planTemplate = (PlanBenefitTemplateVO) objPlanTemplate;
				List<PlanAndBenefitsPackageVO> packages =null;
				List<Plan> savedPlans = null;
				packages = planTemplate.getPackagesList().getPackages();
				entityManager.getTransaction().begin();
				//Check template version.
				boolean isNewVersion = false;
				String templateVersion = serffUtils.getTemplateVersion(objPlanTemplate);
				if (StringUtils.isNotBlank(templateVersion)) {
					if (templateVersion.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_6)
							|| templateVersion.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_7)
							|| templateVersion.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_8)
							|| templateVersion.toLowerCase().startsWith(PlanValidator.TEMPLATE_VERSION_9)) {
						isNewVersion = true;
					}
				}
				
				//Get Tenant.
				List<Tenant> tenantList = serffUtils.getTenantList(tenantIds);				
				object = reqTemplates.get(SerffConstants.TRANSFER_PLAN);
				TransferPlan request = (TransferPlan) object;
				templatesLastModifiedDates = getTemplatesLastModifedDate(request);
				planTemplateLastModifiedDate = templatesLastModifiedDates.get(SerffConstants.DATA_BENEFITS);
				rateTemplateLastModifiedDate = templatesLastModifiedDates.get(SerffConstants.DATA_RATING_TABLE);
				int currentApplicableYear = 0;
				String planEffectiveDate = null;
				
				for (PlanAndBenefitsPackageVO planAndBenefitsPackageVO : packages) {
					
					if (null == planAndBenefitsPackageVO) {
						continue;
					}
					
					issuerID = planAndBenefitsPackageVO.getHeader().getIssuerId().getCellValue();
					if( null == planDispRulesMap) { //Commented for HIX-63472 --- && SerffConstants.YES_ABBR.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.USE_BENEFIT_DISPLAY_RULES_FROM_DB))
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("Fetching Benefit Display Rules for Issuer: " + SecurityUtil.sanitizeForLogging(issuerID));
						}
						planDispRulesMap = planDisplayMapping.loadPlanDisplayRulesInMap(issuerID);
						if(!CollectionUtils.isEmpty(planDispRulesMap)) {
							LOGGER.info("Got " + planDispRulesMap.size() + " Benefit Display Rules for Issuer: " + planDispRulesMap.elements().nextElement().getHiosIssuerId());
						} else {
							LOGGER.warn("No Benefit Display Rules found for Issuer: " + planDispRulesMap.elements().nextElement().getHiosIssuerId());
						}
					}
					if(null != planAndBenefitsPackageVO.getPlansList()
						&& null != planAndBenefitsPackageVO.getPlansList().getPlans()) {
						
						PlansListVO plansListVO = planAndBenefitsPackageVO.getPlansList();
						List<PlanAndBenefitsVO> planAndBenefitsList = plansListVO.getPlans();
						
						currentApplicableYear = 0;
						planEffectiveDate = null;
						
						planInsuranceType = getPlanInsuranceType(planAndBenefitsPackageVO);
						
						for (PlanAndBenefitsVO currentPBVO : planAndBenefitsList) {

							/*//Commented for PHIX story HIX-18012 (point 2)
							 //Now everything is committed in single transaction that begins above
							entityManager.getTransaction().begin();*/
							
							if (null == currentPBVO) {
								continue;
							}

							/**
							 * Assumption: We never have partial year plans. All plans start on Jan 1 and ends on Dec 31.
							 * So, all plans must have same Applicable Year in template.
							 */
							if (null != currentPBVO.getPlanAttributes()
									&& null != currentPBVO.getPlanAttributes().getPlanEffectiveDate()
									&& StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getPlanEffectiveDate().getCellValue())) {
								planEffectiveDate = currentPBVO.getPlanAttributes().getPlanEffectiveDate().getCellValue();
							}
							
							if (0 == applicableYear) {
								applicableYear = getApplicableYear(planEffectiveDate);
								currentApplicableYear = applicableYear;
								LOGGER.info("Applicable Year: " + applicableYear);
							}
							else {
								currentApplicableYear = getApplicableYear(planEffectiveDate);
							}
														
							//isNewVersion = true if plan benefit template version is v6/7/8/9.
							currentPBVO.setNewVersion(isNewVersion);
							
							if (0 == currentApplicableYear || applicableYear != currentApplicableYear) {
								if(LOGGER.isInfoEnabled()) {
									LOGGER.info("Unmatched [ applicableYear("+ applicableYear +") != currentApplicableYear("+ currentApplicableYear +") ] ");
									LOGGER.info("Skipping Standard Component ID because unmatched Applicable Year: " + SecurityUtil.sanitizeForLogging(currentPBVO.getPlanAttributes().getStandardComponentID().getCellValue()));
								}
								continue;
							}
							
							//Resetting parent ids for each plan
							isPlanExist = false;
							isPlanCertified = false;
							savedPlans = null;
							//Check if existing plan/variance is CERTIFIED
							planStatus = validatePlan(planInsuranceType, applicableYear, currentPBVO);
							isPlanCertified = planStatus.get(IS_CERTIFIED_PLAN);
							isPlanExist = planStatus.get(IS_EXISTING_PLAN);
							isNonPUFPlanExist = planStatus.get(IS_NONPUF_PLAN);
							//Reset Last Modified date to one in the request 
							templatesLastModifiedDates.put(SerffConstants.DATA_BENEFITS, planTemplateLastModifiedDate);
							templatesLastModifiedDates.put(SerffConstants.DATA_RATING_TABLE, rateTemplateLastModifiedDate);
							if(null!=reqTemplates.get(SerffConstants.DATA_RATING_RULES)){
								lastUpdatedTemplateMap.put(SerffConstants.DATA_RATING_RULES, null);
							}
							if(null!=reqTemplates.get(SerffConstants.DATA_UNIFIED_RATE)){
								lastUpdatedTemplateMap.put(SerffConstants.DATA_UNIFIED_RATE ,null);
							}
							if (isPlanExist) {
								
								if (!persistPUFPlans || !isNonPUFPlanExist) {
									
									if (isPlanCertified) {
										if(LOGGER.isInfoEnabled()) {
											LOGGER.info("Certified Plans already exist for the given plan id " + SecurityUtil.sanitizeForLogging(currentPBVO.getPlanAttributes().getStandardComponentID().getCellValue()));
										}
										//HIX-14353 Update logic when existing PLANs are CERTIFIED
										savedPlans = updatePlanService.updateCertifiedPlans(applicableYear, planInsuranceType, issuerID,
														planAndBenefitsPackageVO, currentPBVO, reqTemplates, templateObjectMap,
														persistOffExchangePlans, persistOnExchangePlans, persistPUFPlans,
														entityManager, responseMessage, tenantList, lastUpdatedTemplateMap,
														templatesLastModifiedDates, uploadStatistics, planDispRulesMap);
									}
									else {
										if(LOGGER.isInfoEnabled()) {
											LOGGER.info("Non certified Plans already exist for the given plan id " + SecurityUtil.sanitizeForLogging(currentPBVO.getPlanAttributes().getStandardComponentID().getCellValue()));
										}
										//HIX-14351 Update logic when existing PLANs are not CERTIFIED
										savedPlans = updatePlanService.updateNonCertifiedPlans(applicableYear, planInsuranceType, issuerID,
														planAndBenefitsPackageVO, currentPBVO, reqTemplates, templateObjectMap,
														persistOffExchangePlans, persistOnExchangePlans, entityManager,
														responseMessage, tenantList, exchangeTypeFilter, lastUpdatedTemplateMap,
														templatesLastModifiedDates, uploadStatistics, planDispRulesMap, logoBytes);
										//HIX-50974 : Send new error messages to SERFF when Plan is in certified status or non-incomplete status/
										if(!isTemplateMessageCreated && !isPHIXProfile){
											getNonModifiedTemplateList(lastUpdatedTemplateMap, reqTemplates, responseMessage);
											isTemplateMessageCreated = true;
										}
									}
								}
								else {
									if(LOGGER.isInfoEnabled()) {
										LOGGER.info("Skipping plan load as Non-PUF-Plans already exist for the given plan id " + SecurityUtil.sanitizeForLogging(currentPBVO.getPlanAttributes().getStandardComponentID().getCellValue()));
									}
								}
							}
							else {
								LOGGER.info("Plan does not exist for the given plan id");
								if(!isTemplateSaved){
									String stateCode = planAndBenefitsPackageVO.getHeader().getStatePostalCode().getCellValue();

									com.serff.template.admin.extension.PayloadType adminTemplate = null;
									object = reqTemplates.get(SerffConstants.DATA_ADMIN_DATA);
									if (null != object) {
										adminTemplate = (com.serff.template.admin.extension.PayloadType) object;
									}

									if (!templateObjectMap.containsKey(SerffConstants.DATA_ADMIN_DATA)) {

										object = reqTemplates.get(SerffConstants.SUPPORTING_DOC);
										documents =	(HashMap<String, String>) object;
										LOGGER.info("Creating new DATA_ADMIN_DATA");
										issuer = this.saveIssuerBean(entityManager, adminTemplate, issuerID, stateCode, documents, logoBytes);
										templateObjectMap.put(SerffConstants.DATA_ADMIN_DATA, issuer);
										lastUpdatedTemplateMap.put(SerffConstants.DATA_ADMIN_DATA, null);

										com.serff.template.ncqa.IssuerType ncqaInfo = null; 
										object = reqTemplates.get(SerffConstants.DATA_NCQA_DATA);
										if (null != object) {
											ncqaInfo = (com.serff.template.ncqa.IssuerType) object;
											lastUpdatedTemplateMap.put(SerffConstants.DATA_NCQA_DATA, null);
										}
										LOGGER.debug("Persisting NCQA template");
										populateAndPersistNcqaTemplate(ncqaInfo, issuer, entityManager);

										com.serff.template.urac.IssuerType uracInfo = null;
										object = reqTemplates.get(SerffConstants.DATA_URAC_DATA);
										if (null != object) {
											uracInfo = (com.serff.template.urac.IssuerType) object;
											lastUpdatedTemplateMap.put(SerffConstants.DATA_URAC_DATA, null);
										}
										LOGGER.debug("Persisting URAC template");
										populateAndPersistURACTemplate(uracInfo, issuer, entityManager);
									}
									else {
										issuer = (Issuer) templateObjectMap.get(SerffConstants.DATA_ADMIN_DATA);
									}
									stateCode = issuer.getStateOfDomicile();

									if(!templateObjectMap.containsKey(SerffConstants.DATA_SERVICE_AREA)) {
										LOGGER.info("Creating new DATA_SERVICE_AREA");
										object = reqTemplates.get(SerffConstants.DATA_SERVICE_AREA);
										com.serff.template.svcarea.extension.PayloadType serviceAreaTemplate =
												(com.serff.template.svcarea.extension.PayloadType) object;
										templateObjectMap.put(SerffConstants.DATA_SERVICE_AREA, this.saveIssuerServiceAreaBean(applicableYear, entityManager, serviceAreaTemplate,issuer.getHiosIssuerId()));
										lastUpdatedTemplateMap.put(SerffConstants.DATA_SERVICE_AREA, null);
									}
									issuerServiceArea = (List<IssuerServiceArea>) templateObjectMap.get(SerffConstants.DATA_SERVICE_AREA);

									if(!templateObjectMap.containsKey(SerffConstants.DATA_NETWORK_ID)) {
										LOGGER.info("Creating new DATA_NETWORK_ID");
										object = reqTemplates.get(SerffConstants.DATA_NETWORK_ID);
										com.serff.template.networkProv.extension.PayloadType networkProvider =
												(com.serff.template.networkProv.extension.PayloadType) object;
										templateObjectMap.put(SerffConstants.DATA_NETWORK_ID, this.populateAndPersistNetworkBean(applicableYear, entityManager, issuer, networkProvider));
										lastUpdatedTemplateMap.put(SerffConstants.DATA_NETWORK_ID, null);
									}
									networks= (Map<String, Network>) templateObjectMap.get(SerffConstants.DATA_NETWORK_ID);
									
									if(!templateObjectMap.containsKey(SerffConstants.DATA_RATING_TABLE)) {
										object = reqTemplates.get(SerffConstants.DATA_RATING_TABLE);
										com.serff.template.plan.QhpApplicationRateGroupVO ratesTemplate =
												(com.serff.template.plan.QhpApplicationRateGroupVO) object;
										templateObjectMap.put(SerffConstants.DATA_RATING_TABLE, this.savePlanRateBean(entityManager, stateCode, ratesTemplate));
									}
									planRates = (Map<String, List<PlanRate>>) templateObjectMap.get(SerffConstants.DATA_RATING_TABLE);

									object = reqTemplates.get(SerffConstants.DATA_PRESCRIPTION_DRUG);
									if(null != object) {
										if(!templateObjectMap.containsKey(SerffConstants.DATA_PRESCRIPTION_DRUG)) {
											LOGGER.info("Creating new DATA_PRESCRIPTION_DRUG");
											PrescriptionDrugTemplateVO prescriptionDrug = (PrescriptionDrugTemplateVO) object;
											templateObjectMap.put(SerffConstants.DATA_PRESCRIPTION_DRUG, this.populateAndPersistPrescriptionDrugBean(applicableYear, entityManager, issuer, prescriptionDrug));
											lastUpdatedTemplateMap.put(SerffConstants.DATA_PRESCRIPTION_DRUG, null);
										}
										savedFormulary = (Map<String, Formulary>) templateObjectMap.get(SerffConstants.DATA_PRESCRIPTION_DRUG);
									} else {
										savedFormulary = new HashMap<String, Formulary>();
										List<Formulary> formularyList = iSerffFormularyRepository.findByIssuer_IdAndApplicableYear(issuer.getId(), applicableYear);
										if(!CollectionUtils.isEmpty(formularyList)) {
											for (Formulary formulary : formularyList) {
												savedFormulary.put(formulary.getFormularyId(), formulary);
											}
										}
									}

									
									/*
									 //Commented for PHIX story HIX-18012 (point 2)
									 //Now everything is committed in single transaction that end below in the end
									if (null != entityManager && entityManager.isOpen()) {
										entityManager.getTransaction().commit();
										entityManager.getTransaction().begin();
									}*/
									isTemplateSaved = Boolean.TRUE;
								}
								
								if (validateRateExistForPlans(planAndBenefitsPackageVO, planRates, persistOffExchangePlans, persistOnExchangePlans, responseMessage , applicableYear)) {
									lastUpdatedTemplateMap.put(SerffConstants.DATA_BENEFITS, null);
									lastUpdatedTemplateMap.put(SerffConstants.DATA_RATING_TABLE, null);
									savedPlans = this.savePlanBenefitTemplateVO(applicableYear, isPlanCertified, entityManager, planAndBenefitsPackageVO, currentPBVO, issuer, issuerServiceArea, networks, documents, planRates, persistOffExchangePlans, persistOnExchangePlans, persistPUFPlans, savedFormulary,templatesLastModifiedDates, responseMessage, tenantList ,lastUpdatedTemplateMap, uploadStatistics, planDispRulesMap);
								} else {
									status = Boolean.FALSE;
									LOGGER.error("Invalid Data. Either Rates are missing for some plans or applicable year is diffent in Plan and Rates ");
									throw new GIException(Integer.parseInt(SerffConstants.INVALID_PARAMETER_CODE), SerffConstants.INVALID_PARAMETER_MESSAGE_DATA_FORMAT_INVALID + " : Either Rates are missing for some plans or applicable year is diffent in Plan and Rates.", StringUtils.EMPTY);
								}
							}
							object = reqTemplates.get(SerffConstants.DATA_RATING_RULES) ;
							if(!CollectionUtils.isEmpty(savedPlans)){
								if(templateObjectMap.containsKey(SerffConstants.DATA_ADMIN_DATA)) {
									issuer = (Issuer) templateObjectMap.get(SerffConstants.DATA_ADMIN_DATA);
								}
								if(null != issuer) {
									if (null != object) {
										if(!templateObjectMap.containsKey(SerffConstants.DATA_RATING_RULES)) {
											isBRulePresent = Boolean.TRUE;
											LOGGER.info("Creating new DATA_RATING_RULES");
											com.serff.template.rbrules.extension.PayloadType rulePayloadType = (com.serff.template.rbrules.extension.PayloadType) object;
											templateObjectMap.put(SerffConstants.DATA_RATING_RULES, this.populateAndPersistBusinessRuleBean(entityManager, issuer, rulePayloadType , applicableYear));
											savedBusinessRule = (Map<String, BusinessRule>) templateObjectMap.get(SerffConstants.DATA_RATING_RULES);
											
										}
									}
									else if(null==savedBusinessRule) {
										List<BusinessRule> existingBusinessRuleList = ibusinessRuleRepo.findByIssuerAndIsObsoleteAndApplicableYear(issuer, SerffConstants.NO_ABBR, applicableYear);
										savedBusinessRule = new HashMap<String, BusinessRule>();
										
										for(BusinessRule existingBrule : existingBusinessRuleList){
											savedBusinessRule.put(existingBrule.getAppliesTo(), existingBrule);
										}
									}
									
									if (null != savedBusinessRule) {
										updateBussinessRuleForPlans(savedPlans , entityManager , savedBusinessRule);
									}
								} else {
									LOGGER.error("Skipped processing business rules due to missing issuer details.");
								}
							}
							
							//Process Unified Rate template data.						
							object = reqTemplates.get(SerffConstants.DATA_UNIFIED_RATE) ;
							if (null != object){
							if(!CollectionUtils.isEmpty(savedPlans)){
								//Set flag.
								isPlansSaved = true;
								LOGGER.info("Plans saved flag is set as true.");
								if(templateObjectMap.containsKey(SerffConstants.DATA_ADMIN_DATA)) {
									issuer = (Issuer) templateObjectMap.get(SerffConstants.DATA_ADMIN_DATA);
								}
								if(null != issuer) {
										if(!templateObjectMap.containsKey(SerffConstants.DATA_UNIFIED_RATE)) {
											LOGGER.info("Unified Rate template is present. Creating new unified rate from template data.");
											com.serff.template.urrt.InsuranceRateForecastTempVO unifiedRateVO = (com.serff.template.urrt.InsuranceRateForecastTempVO) object;
											UnifiedRate savedUnifiedRateInDb = populateAndPersistUnifiedRate(entityManager, issuer, unifiedRateVO, currentApplicableYear);
											LOGGER.info("New Unified Rate from template is saved in database :" + savedUnifiedRateInDb.getId());
											
										}
								} else {
									LOGGER.error("Skipped processing unified rate due to missing issuer details.");
								}
							  }
							}else{
								LOGGER.info("Unified Rate template is not present.");
							}
							/*
							//Commented for PHIX story HIX-18012 (point 2)
							//Now everything is committed in single transaction that end below in the end
							if (null != entityManager && entityManager.isOpen()) {
								if(savedPlans != null && !savedPlans.isEmpty()) {
									entityManager.getTransaction().commit();
									status = Boolean.TRUE;
								} else {
									entityManager.getTransaction().rollback();
								}
							}*/
						}
					}
				}
				status = Boolean.TRUE;
				
				if (null != savedPlans && !savedPlans.isEmpty()) {
					LOGGER.info("Number of Plans to be save: " + savedPlans.size());
				}
			}
		}
		catch(GIException ex) {
			LOGGER.error("Exception occurred in populateAndPersistAllBeansFromSERFF(). Exception:" + ex.getMessage(), ex);
			status = Boolean.FALSE;
			throw new GIException(Integer.parseInt(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE), SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE + " : Exception occurred :" + ex.getMessage(), StringUtils.EMPTY);
		}
		finally {
			LOGGER.info("responseMessage:" + responseMessage.toString());
			if (null != entityManager && entityManager.isOpen()) {
				if (status) {
					LOGGER.info(STATUS_MSG + status + "'. Transaction Commited");
					entityManager.getTransaction().commit();
					
					networks = (Map<String, Network>) templateObjectMap.get(SerffConstants.DATA_NETWORK_ID);
					if (networks != null && iNetworkRepository != null) {
						
						for (Map.Entry<String, Network> entry : networks.entrySet()) {

							if (null != entry.getValue()) {
								LOGGER.info("Saving NetworkID: " + entry.getValue().getNetworkID());
								iNetworkRepository.save(entry.getValue());
							}
						}
					}

					issuer = (Issuer) templateObjectMap.get(SerffConstants.DATA_ADMIN_DATA);
					if (issuer != null && iIssuerRepository != null) {
						LOGGER.info("Saving HiosIssuerId: " + issuer.getHiosIssuerId());
						iIssuerRepository.save(issuer);
					}
				}
				else {
					LOGGER.error(STATUS_MSG + status + "'. Transaction Rolledback");
					entityManager.getTransaction().rollback();
				}
				if(isBRulePresent) { //Update existing plans with new business rule id
					entityManager.getTransaction().begin();
					refreshBusinessRule(planInsuranceType, issuerID, entityManager ,savedBusinessRule, applicableYear);
					entityManager.getTransaction().commit();
				}
				
				if(isPlansSaved){
					//Update existing plans with new business rule id
					updateUnifiedRateForPlans(issuer, applicableYear, entityManager);
				}
				
				if (!CollectionUtils.isEmpty(planDispRulesMap)) {
					planDispRulesMap.clear();
					planDispRulesMap = null;
				}
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			} else  {
				LOGGER.error(STATUS_MSG + status + "'. Connection already closed");
			}
			
			LOGGER.info("End execution populateAndPersistAllBeansFromSERFF().");
		}
		return status;
	}

	/**
	 * This method updates plan rate change ids to corresponding plans in Plan table. It checks existing plan rate change in the database which saved in
	 * earlier stage and updates column pm_urrt_prc_id in Plan table with plan rate change id.
	 * @param issuer : Issuer for which URRT template is processed.
	 * @param applicableYear : Applicable year for URRT.
	 * @param entityManager :  Entity manage object for transaction.
	 */
	private void updateUnifiedRateForPlans(Issuer issuer, int applicableYear, EntityManager entityManager) {
		
		   //Begin transaction.
			entityManager.getTransaction().begin();
		   //Get all Unified Rates from database.
		    List<URRTInsurancePlanRateChange> existingPlanRateChangeList = iUnifiedRatePlanRateChangeRepository.getInsuranceRateChangeListByApplicableYear(issuer.getHiosIssuerId()+issuer.getStateOfDomicile(), applicableYear);
		    LOGGER.info("Existing Plan rate chnage list in db : " + (null == existingPlanRateChangeList ? null : existingPlanRateChangeList.size()));
		   
		   //Get all the plans for the issuer from database.
		    List<Plan> plansForIssuer = iPlanRepository.findByIssuerPlanNumberStartingWithAndIsDeletedAndApplicableYear(issuer.getHiosIssuerId()+issuer.getStateOfDomicile(), SerffConstants.NO_ABBR, applicableYear);
		   	LOGGER.info("Plan List to apply Unified Rate is  : " + (null == plansForIssuer ? null : plansForIssuer.size()));
		   
		    if((!CollectionUtils.isEmpty(plansForIssuer)) && (!CollectionUtils.isEmpty(existingPlanRateChangeList))){
			   //Create Map of existing Unified Rate Change and Ids to apply to Plans.
			    Map<String, Integer> componentIdAndPlanRateChangeIdMap = new HashMap<String, Integer>();
					Iterator<URRTInsurancePlanRateChange> iterator = existingPlanRateChangeList.iterator();
					while (iterator.hasNext()) {
						URRTInsurancePlanRateChange unifiedPlanRateChange = iterator.next();
						if(SerffConstants.NO_ABBR.equals(unifiedPlanRateChange.getIsObsolete())){
							componentIdAndPlanRateChangeIdMap.put(unifiedPlanRateChange.getBasedInsurancePlanIdentifier(), unifiedPlanRateChange.getId());
							if(LOGGER.isDebugEnabled()) {
								LOGGER.debug("Added entry in ComponentIdToPlanRateChangeIdMap as ComponentId <=> Id : " + SecurityUtil.sanitizeForLogging(unifiedPlanRateChange.getBasedInsurancePlanIdentifier()) + " <=> " + unifiedPlanRateChange.getId());
							}
						}
					}	
			LOGGER.info("Component Id and Plan rate change Id map : " + componentIdAndPlanRateChangeIdMap);
		  		
			//Apply Plan Rate Change id to Recently Saved Plans.
			String componentId = null;
			for(Plan plan : plansForIssuer){				
						if(!PlanStatus.CERTIFIED.toString().equalsIgnoreCase(plan.getStatus())){
							componentId = plan.getIssuerPlanNumber().substring(SerffConstants.PLAN_INITIAL_INDEX, SerffConstants.LEN_HIOS_PLAN_ID);
							LOGGER.info("Plan is not certified : " + plan.getIssuerPlanNumber() + " : Component id : " + componentId);
							Integer planRateChangeId = componentIdAndPlanRateChangeIdMap.get(componentId);
							if(null != planRateChangeId){
								LOGGER.info("Applying Unified Rate Rule. Plan Id : " + plan.getId() + " Plan Rate Change Id : " + componentIdAndPlanRateChangeIdMap.get(componentId));
								plan.setUnifiedPlanRateChangeId(planRateChangeId);
								entityManager.merge(plan);
							}else{
								LOGGER.info("No unified plan rate change found for : " + plan.getIssuerPlanNumber());
							}
						}else{
							LOGGER.info("Plan is certified : " + plan.getIssuerPlanNumber());
						}
					}
				 LOGGER.info("updateUnifiedRateForPlans() : end : ");
		   }
		  
		 //End Transaction.
		 entityManager.getTransaction().commit();
	}

	/**
	 * Method to populate Network bean from SERFF Network PayloadType.
	 *
	 * @param networkProvider The source SERFF PayloadType instance for Network bean.
	 * @throws GIException
	 */
	@Transactional
	public Map <String, Network> populateAndPersistNetworkBean(int applicableYear, EntityManager entityManager, Issuer issuer, com.serff.template.networkProv.extension.PayloadType networkProvider) throws GIException {
//		boolean status = Boolean.FALSE;
		List<HealthcareProviderNetworkType> healthcareProviderNetworks = null;
		List<Network> existingNetworkList = null;
		Map <String, Network> savedNetwork = new HashMap<String, Network>();

		Validate.notNull(issuer, "No issuer associated for current network.");
		Validate.notNull(networkProvider, "Network Payload is missing in current request");

		LOGGER.debug("Current HIOS Issuer ID:" + issuer.getHiosIssuerId());
		// Validate.notNull(networkService, "NetworkService instance is improperly configured.");

		if(null != networkProvider.getIssuer() && null != networkProvider.getIssuer().getHealthcareProviderNetwork()) {
			healthcareProviderNetworks = networkProvider.getIssuer().getHealthcareProviderNetwork();
		}

		if(null == healthcareProviderNetworks || healthcareProviderNetworks.isEmpty()) {
			LOGGER.error("HealthcareProviderNetworks is missing in the request.");			
			throw new GIException(Integer.parseInt(SerffConstants.REQUIRED_FIELD_MISSING_CODE), SerffConstants.REQUIRED_FIELD_MISSING_MESSAGE + " : HealthcareProviderNetworks is missing in the request.", StringUtils.EMPTY);
		}
		else {

			for (int i = 0; i < healthcareProviderNetworks.size(); i++) {
				if(null != healthcareProviderNetworks.get(i)) {
					String networkId = null;
					if(null != healthcareProviderNetworks.get(i).getProviderNetworkIdentification() && null != healthcareProviderNetworks.get(i).getProviderNetworkIdentification().getIdentificationID() && null != healthcareProviderNetworks.get(i).getProviderNetworkIdentification().getIdentificationID().getValue()) {
						networkId = healthcareProviderNetworks.get(i).getProviderNetworkIdentification().getIdentificationID().getValue();
					}
					Network network = null;
					existingNetworkList = iNetworkRepository.getProviderNetworkByIssuerIdNetworkIdApplicableYear(issuer.getId(), networkId, applicableYear);

					if(existingNetworkList == null || existingNetworkList.isEmpty()) {
						network = new Network();

						if(null != networkId ) {
							network.setNetworkID(networkId);
						}
						//set default values
						network.setCertified(Network.CERTIFIED.YES);
						network.setVerified(Network.VERIFIED.YES);
						network.setHasProviderData(Network.PROVIDER_DATA.NO);
					}
					else {
						network = existingNetworkList.get(0);
					}
					network.setIssuer(issuer);
					network.setLastUpdatedBy(issuer.getLastUpdatedBy());
					
					List<OrganizationType> organizationTypes = networkProvider.getOrganization();

					Validate.notNull(organizationTypes, "OrganizationTypes not found.");
					Validate.notEmpty(organizationTypes, "OrganizationTypes is missing.");

					if(healthcareProviderNetworks.size() == organizationTypes.size()) {
						if(null != organizationTypes.get(i)) {
							if(null != organizationTypes.get(i).getOrganizationAugmentation() && null != organizationTypes.get(i).getOrganizationAugmentation().getOrganizationMarketingName() && null != organizationTypes.get(i).getOrganizationAugmentation().getOrganizationMarketingName().getValue()) {
								network.setName(organizationTypes.get(i).getOrganizationAugmentation().getOrganizationMarketingName().getValue());
							}

							if(null != organizationTypes.get(i).getOrganizationPrimaryContactInformation() && null != organizationTypes.get(i).getOrganizationPrimaryContactInformation().getContactWebsiteURI() && null != organizationTypes.get(i).getOrganizationPrimaryContactInformation().getContactWebsiteURI().getValue()) {
								network.setNetworkURL(SerffUtils.checkAndcorrectURL(organizationTypes.get(i).getOrganizationPrimaryContactInformation().getContactWebsiteURI().getValue()));
							}
						}

						network.setApplicableYear(applicableYear);
						//HIX-34137 : Populate network key during plan load process.
						network.setNetworkKey(issuer.getHiosIssuerId() + SerffConstants.HYPHEN + networkId + SerffConstants.HYPHEN + applicableYear);
						
						// savedNetwork.put(network.getNetworkID(), networkService.saveNetwork(network));
						if (null != entityManager && entityManager.isOpen()) {
							savedNetwork.put(network.getNetworkID(), entityManager.merge(network));
						}
					}
					else {
						LOGGER.error("HealthcareProviderNetwork count does not match to OrganizationType count.");
						throw new GIException(Integer.parseInt(SerffConstants.INVALID_PARAMETER_CODE), SerffConstants.INVALID_PARAMETER_MESSAGE_DATA_FORMAT_INVALID + " : HealthcareProviderNetwork count does not match to OrganizationType count.", StringUtils.EMPTY);
						
					}
				}
			}
		}
		return savedNetwork;
	}

	/**
	 * Method to populate Formulary bean from SERFF Prescription drug PayloadType.
	 *
	 * @param prescriptionDrug The source SERFF PayloadType instance for Formulary bean.
	 */
	@Transactional
	@Override
	public Map<String, Formulary> populateAndPersistPrescriptionDrugBean(int applicableYear, EntityManager entityManager, Issuer issuer,
			com.serff.template.plan.PrescriptionDrugTemplateVO prescriptionDrug) {
		
		LOGGER.info("Beginning populateAndPersistPrescriptionDrugBean() : issuer id : " + issuer.getId());
		String costSharingType = null;
		List<FormularyVO> formularyVOs = null;
		Integer drugTierLevel = null;
		//Formulary formulary = null;
		FormularyCostShareBenefit formularyCostShareBenefit = null;
		CostShare costShare = null;
		Map<String, Formulary> savedFormulary = new HashMap<String, Formulary>();
		List <Formulary> existingFormualryList =null;
		Validate.notNull(issuer, "No issuer associated for current PrescriptionDrug payload.");
		Validate.notNull(prescriptionDrug, "Prescription Drug payload is missing in current request");
		// Validate.notNull(formularyService, "FormularyService instance is improperly configured.");
		// Validate.notNull(formularyCostShareBenefitService, "FormularyCostShareBenefitService instance is improperly configured.");
		// Validate.notNull(costShareService, "CostShareService instance is improperly configured.");

		formularyVOs = prescriptionDrug.getFormularyList().getFormularyVO();
		Validate.notNull(formularyVOs, "No FORMULARY data is found for the request.");
		Validate.notEmpty(formularyVOs, "FORMULARY data is missing in the request.");
         
		Map<Integer, DrugList> drugListMap = loadDrugList(entityManager, issuer, prescriptionDrug);
		LOGGER.info("populateAndPersistPrescriptionDrugBean() : drugListMap : " + drugListMap.size());
		//Setting unique formulary Id
		for(FormularyVO formularyVO: formularyVOs) {
			if(null != formularyVO) {
				/** FORMULARY */
				Formulary formulary = null;
				String fromularyId = formularyVO.getFormularyID().getCellValue();
				existingFormualryList = iSerffFormularyRepository.findByIssuer_IdAndFormularyIdAndApplicableYear(issuer.getId(), fromularyId, applicableYear);

				if(CollectionUtils.isEmpty(existingFormualryList)) {
//					existingFormualryList = new ArrayList<Formulary>();
					formulary = new Formulary();
					formulary.setIssuer(issuer);
					if(null != fromularyId ) {
						formulary.setFormularyId(fromularyId);
					}
					Validate.notNull(formularyVO.getFormularyCostSharingTypeList(), "No FormularyCostSharing data is found in the request object.");
				}
				else {
					formulary = existingFormualryList.get(0);
					//Cleanup existing data if exist, as we will be linking new data
					softdeleteExistingFormularyLinkedCostShareAndDrugRecord(formulary, entityManager);
					//savedFormulary.put(formulary.getFormularyId(), formulary);
				}
				
				if(null != formularyVO.getFormularyUrl() && null != formularyVO.getFormularyUrl().getCellValue()) {
					formulary.setFormularyUrl(SerffUtils.checkAndcorrectURL(formularyVO.getFormularyUrl().getCellValue()));
				}
				DrugList dl = drugListMap.get(Integer.valueOf(formularyVO.getDrugListID().getCellValue().trim()));
				formulary.setDrugList(dl);
				formulary.setNoOfTiers(Integer.valueOf(formularyVO.getNumberTiers().getCellValue().trim()));
				formulary.setApplicableYear(applicableYear);

				if (null != entityManager && entityManager.isOpen()) {
					formulary = entityManager.merge(formulary);
					savedFormulary.put(formulary.getFormularyId(), formulary);
					Validate.notNull(formulary, "Formulary instance is missing post persistence.");
				}
				
				List<FormularyCostSharingTypeVO> formularyCostSharingTypeVOList =  formularyVO.getFormularyCostSharingTypeList().getFormularyCostSharingTypeVO();

				Validate.notNull(formularyCostSharingTypeVOList, "No Formulary Cost Sharing data is found in the request object.");
				Validate.notEmpty(formularyCostSharingTypeVOList, "Formulary Cost Sharing data is missing from the request object.");
				
				for(FormularyCostSharingTypeVO formularyCostSharingTypeVO: formularyCostSharingTypeVOList) {

				/**  FORMULARY COST SHARE BENEFIT*/
				if(null != formularyCostSharingTypeVO) {
					formularyCostShareBenefit = new FormularyCostShareBenefit();
					formularyCostShareBenefit.setIsDeleted(FormularyCostShareBenefit.IS_DELETED.N.toString());
					if(null == formularyCostSharingTypeVO.getOneMonthOutNetworkRetailOfferedIndicator() || null == formularyCostSharingTypeVO.getOneMonthOutNetworkRetailOfferedIndicator().getCellValue() || "No".equalsIgnoreCase(formularyCostSharingTypeVO.getOneMonthOutNetworkRetailOfferedIndicator().getCellValue().trim())) {
						formularyCostShareBenefit.setOneMonthOutNetworkRetailOfferedIndicator(N);
					}
					else {
						formularyCostShareBenefit.setOneMonthOutNetworkRetailOfferedIndicator(Y);
					}

					if(null == formularyCostSharingTypeVO.getThreeMonthInNetworkMailOfferedIndicator() || null == formularyCostSharingTypeVO.getThreeMonthInNetworkMailOfferedIndicator().getCellValue() || "No".equalsIgnoreCase(formularyCostSharingTypeVO.getThreeMonthInNetworkMailOfferedIndicator().getCellValue().trim())) {
						formularyCostShareBenefit.setThreeMonthInNetworkMailOfferedIndicator(N);
					}
					else {
						formularyCostShareBenefit.setThreeMonthInNetworkMailOfferedIndicator(Y);
					}

					if(null == formularyCostSharingTypeVO.getThreeMonthOutNetworkMailOfferedIndicator() || null == formularyCostSharingTypeVO.getThreeMonthOutNetworkMailOfferedIndicator().getCellValue() || "No".equalsIgnoreCase(formularyCostSharingTypeVO.getThreeMonthOutNetworkMailOfferedIndicator().getCellValue().trim())) {
						formularyCostShareBenefit.setThreeMonthOutNetworkMailOfferedIndicator(N);
					}
					else {
						formularyCostShareBenefit.setThreeMonthOutNetworkMailOfferedIndicator(Y);
					}

					formularyCostShareBenefit.setFormulary(formulary);

					// formularyCostShareBenefit = formularyCostShareBenefitService.saveFormularyCostShareBenefit(formularyCostShareBenefit);
					if (null != entityManager && entityManager.isOpen()) {
						formularyCostShareBenefit = entityManager.merge(formularyCostShareBenefit);
						Validate.notNull(formularyCostShareBenefit, "FormularyCostShareBenefit instance is missing post persistence.");
					}
					Validate.notNull(formularyCostSharingTypeVO.getCostSharingTypeList(), "No CostSharing data is found in the request object.");

					List<CostSharingTypeVO> costSharingTypeVOList = formularyCostSharingTypeVO.getCostSharingTypeList().getCostSharingTypeVO();

					Validate.notNull(costSharingTypeVOList, "No Cost Sharing data is found in the request object.");
					Validate.notEmpty(costSharingTypeVOList, "Cost Sharing data is missing from the request object.");
					drugTierLevel = Integer.valueOf(formularyCostSharingTypeVO.getDrugTierLevel().getCellValue().trim());

					for(CostSharingTypeVO costSharingTypeVO: costSharingTypeVOList) {
						if(null != costSharingTypeVO) {
							/** Cost Share*/
							costSharingType = null;
							costShare = new CostShare();
							costShare.setCostSharingType(COPAY_COINSURANCE_TEXT);
							
							String copaymentCostSharingType = DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.DV_COPAYMENT_COST_SHARING_TYPE);
							LOGGER.debug("SerffConfigurationEnum.DV_COPAYMENT_COST_SHARING_TYPE: " + copaymentCostSharingType);
							
							String coinsuranceCostSharingType = DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.DV_COINSURANCE_COST_SHARING_TYPE);
							LOGGER.debug("SerffConfigurationEnum.DV_COINSURANCE_COST_SHARING_TYPE: " + coinsuranceCostSharingType);

                            if(null == costSharingTypeVO.getCoInsurance() || null == costSharingTypeVO.getCoInsurance().getCellValue() || costSharingTypeVO.getCoInsurance().getCellValue().trim().isEmpty()) {
                                if(!copaymentCostSharingType.equalsIgnoreCase(costSharingType)) {
                                       LOGGER.error("populateAndPersistPrescriptionDrugBean() : Co Insurance value is missing from current Cost Sharing data.");
                                       throw new IllegalArgumentException("Co Insurance value is missing from current Cost Sharing data.");
                                }
                            }
                            else {
                        		String coInsurance = costSharingTypeVO.getCoInsurance().getCellValue().trim(); 
                        		
                        		if (!coInsurance.startsWith(NO_CHARGE)
                    					&& !coInsurance.startsWith(NOT_APPLICABLE)) {
                    				
                        			String[] coInsuranceValues = coInsurance.split(SerffConstants.SPACE);
                        			
                        			if(coInsuranceValues.length > 0){

                        				costShare.setCoInsurancePercent(BigDecimal.valueOf(Double.valueOf(coInsuranceValues[0]
                        						.replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY)
                        						.replaceAll(SerffConstants.PERCENTAGE, StringUtils.EMPTY)
                            					.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))));
                        				
                        				costShare.setCoinsuranceAttribute(coInsurance.replace(coInsuranceValues[0], StringUtils.EMPTY).trim());
                        			}
                    				
                    			} else {
                    				costShare.setCoinsuranceAttribute(coInsurance);
                    				costShare.setCoInsurancePercent(BigDecimal.valueOf(Double.valueOf(ZERO)));
                    			}
                            }

                            if(null == costSharingTypeVO.getCoPayment() || null == costSharingTypeVO.getCoPayment().getCellValue() || costSharingTypeVO.getCoPayment().getCellValue().trim().isEmpty()) {
                                if(!coinsuranceCostSharingType.equalsIgnoreCase(costSharingType)) {
                                       LOGGER.error("populateAndPersistPrescriptionDrugBean() : Co Payment value is missing from current Cost Sharing data.");
                                       throw new IllegalArgumentException("Co Payment value is missing from current Cost Sharing data.");
                                }
                            }
                            else {
                        		String coPayment = costSharingTypeVO.getCoPayment().getCellValue().trim(); 
                        		
                        		if (!coPayment.startsWith(NO_CHARGE)
                    					&& !coPayment.startsWith(NOT_APPLICABLE)) {
                    				
                        			String[] coPaymentValues = coPayment.split(SerffConstants.SPACE);
                        			
                        			if(coPaymentValues.length > 0){

                        				costShare.setCoPaymentAmount(BigDecimal.valueOf(Double.valueOf(coPaymentValues[0]
                        						.replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY)
                        						.replaceAll(SerffConstants.PERCENTAGE, StringUtils.EMPTY)
                            					.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))));
                        				
                        				costShare.setCopaymentAttribute(coPayment.replace(coPaymentValues[0], StringUtils.EMPTY).trim());
                        			}
                    				
                    			} else {
                    				costShare.setCopaymentAttribute(coPayment);
                    				costShare.setCoPaymentAmount(BigDecimal.valueOf(Double.valueOf(ZERO)));
                    			}
                            }
                            
							if(null == costSharingTypeVO.getDrugPrescriptionPeriodType() || costSharingTypeVO.getDrugPrescriptionPeriodType().isEmpty()) {
								LOGGER.error("populateAndPersistPrescriptionDrugBean() : Drug Prescription Period Type value is missing from current Cost Sharing data.");
							}
							else {
								costShare.setDrugPrescriptionPeriod(Integer.valueOf(costSharingTypeVO.getDrugPrescriptionPeriodType().trim()));
								costShare.setDrugPrescriptionPeriodUnit(TIME_UNIT_CODE_MONTH);
							}

							if(null == costSharingTypeVO.getNetworkCostType() || costSharingTypeVO.getNetworkCostType().trim().isEmpty()) {
								LOGGER.error("populateAndPersistPrescriptionDrugBean() : Network Cost Type value is missing from current Cost Sharing data.");
							}
							else {
								costShare.setNetworkCostType(costSharingTypeVO.getNetworkCostType().trim());
							}
							
							if(Y.equals(formularyCostShareBenefit.getThreeMonthInNetworkMailOfferedIndicator()) || Y.equals(formularyCostShareBenefit.getThreeMonthOutNetworkMailOfferedIndicator())) {
								costShare.setPharmacyCode(DRUG_COST_SHARING_PHARMACY_CODE_MAIL_ORDER);
							}
							else {
								costShare.setPharmacyCode(DRUG_COST_SHARING_PHARMACY_CODE_RETAIL);
							}

							costShare.setFormulary(formulary);
							costShare.setDrugTierLevel(drugTierLevel);
							costShare.setIsDeleted(SerffConstants.NO_ABBR);

							// costShare = costShareService.saveCostShare(costShare);
							if (null != entityManager && entityManager.isOpen()) {
								costShare = entityManager.merge(costShare);
								Validate.notNull(costShare, "CostShare instance is missing post persistence.");
							}
						}
					}
				}
			}
		}
	}
	LOGGER.info("populateAndPersistPrescriptionDrugBean() complete.");
	return savedFormulary;
	}

	private BusinessRule createBasicBusinessRule(Issuer issuer, String identification, int ruleType, int applicableYear) {
		BusinessRule businessRule = new BusinessRule();
		businessRule.setIssuer(issuer);
		businessRule.setAppliesTo(identification);
		LOGGER.info("Rule applies to plan : " +identification);
		businessRule.setRuleType(ruleType);
		businessRule.setIsObsolete(SerffConstants.NO_ABBR);
		businessRule.setApplicableYear(applicableYear);
		businessRule.setLastUpdateBy(issuer.getLastUpdatedBy());
		return businessRule;
	}
	
	/**
	 * This method creates Unified Rate object by parsing provided templates. It retrieves list of products and iterates them to retrieve all the plan rate change.
	 * 
	 * @param issuer : Issuer for which URRT template is processed.
	 * @param applicableYear : Applicable year for URRT.
	 * @param unifiedRateVO : Unmarshalled Unified Rate VO object.
	 * @return Unified Rate object created by parsing templates.
	 * @throws ParseException
	 */
	private  UnifiedRate createUnifiedRate(Issuer issuer, int applicableYear, com.serff.template.urrt.InsuranceRateForecastTempVO unifiedRateVO) throws ParseException {
		
		LOGGER.info("Creating Unified Rate createUnifiedRate():start:");
		
		UnifiedRate unifiedRate = createUnifiedRateFromTemplateVO(issuer, applicableYear, unifiedRateVO);
		
		//Set List<URRTAllowedClaim> allowedClaims list.
		List<CollectedBenefitCategoryAllowedClaim> allowedClaimsInTemplate = unifiedRateVO.getCollectedBenefitCategoryAllowedClaim();
		if(null != allowedClaimsInTemplate && (!allowedClaimsInTemplate.isEmpty())){
			LOGGER.info("Setting allowed claims in UnifiedRate:start:");
			List<URRTAllowedClaim> allowedUnifiedRateClaims = new ArrayList<URRTAllowedClaim>();
			for(CollectedBenefitCategoryAllowedClaim claim : allowedClaimsInTemplate){
				URRTAllowedClaim unifiedRateClaim = createUnifiedRateClaim(claim);
				unifiedRateClaim.setUnifiedRate(unifiedRate);
				//Adding unifiedRateClaim into list.
				allowedUnifiedRateClaims.add(unifiedRateClaim);
			}			
			LOGGER.info("Setting allowed claims in UnifiedRate:end: Claim size is : " + allowedUnifiedRateClaims.size());
			unifiedRate.setAllowedClaims(allowedUnifiedRateClaims);			
		}
		
		//Set List<URRTProductRateChange> productRateChange list.
		List<InsuranceRateForecastTempVO.ChangedInsuranceProductRateChange> productRateChangeListInTemplate = unifiedRateVO.getChangedInsuranceProductRateChange();
		if(null != productRateChangeListInTemplate && (!productRateChangeListInTemplate.isEmpty())){
			LOGGER.info("Setting productRateChange list in UnifiedRate:start:");
			List<URRTProductRateChange> productRateChangeList = new ArrayList<URRTProductRateChange>();
			for(InsuranceRateForecastTempVO.ChangedInsuranceProductRateChange productRateChange : productRateChangeListInTemplate){
				URRTProductRateChange urrtProductRange = createProductRateRange(productRateChange);
				urrtProductRange.setUnifiedRate(unifiedRate);
				
				//Set plan rate change list.
				List<AssociatingInsurancePlanRateChange> insurancePlanRateChangeInTemplate = productRateChange.getAssociatingInsurancePlanRateChange();				
				if(null != insurancePlanRateChangeInTemplate && (!insurancePlanRateChangeInTemplate.isEmpty())){
					LOGGER.info("Setting insurancePlanRateChangeInTemplate list in ChangedInsuranceProductRateChange:start:");
					List<URRTInsurancePlanRateChange> unifiedRateInsuracePlanRateChange= new ArrayList<URRTInsurancePlanRateChange>();
					for(AssociatingInsurancePlanRateChange insurancePlanRateChange :  insurancePlanRateChangeInTemplate){	
						URRTInsurancePlanRateChange unifiedRateInsurancePlanRateChange= createPlanRateChange(insurancePlanRateChange);
						unifiedRateInsurancePlanRateChange.setUnifiedProductRateChange(urrtProductRange);
						
						//Set List of Claim Infos.
						List<ProjecedInsurancePlanRateChangePeriod> claimInfosInTemplate = insurancePlanRateChange.getProjecedInsurancePlanRateChangePeriod();
						if(null != claimInfosInTemplate && (!claimInfosInTemplate.isEmpty())){
							LOGGER.info("Setting Claim Infos list in unifiedRateInsurancePlanRateChange:");
							List<URRTPRCPremiumClaimInfo> unifiedRateClaimInfoList= new ArrayList<URRTPRCPremiumClaimInfo>();
							for(ProjecedInsurancePlanRateChangePeriod claimInfo : claimInfosInTemplate){
								URRTPRCPremiumClaimInfo unifiedRateClaimInfo = createUnifiedRateClaim(claimInfo);
								unifiedRateClaimInfo.setUnifiedRatePlanRateChange(unifiedRateInsurancePlanRateChange);
								//Adding Claim Info into list.
								unifiedRateClaimInfoList.add(unifiedRateClaimInfo);
							}
							LOGGER.info("Setting Claim Infos list in unifiedRateInsurancePlanRateChange: List size is : " + unifiedRateClaimInfoList.size());
							unifiedRateInsurancePlanRateChange.setPremiumClaimsInfoList(unifiedRateClaimInfoList);
						}
						
						//Set List of Premium Components.
						List<DefinedInsurancePlanPremiumComponent> premiumComponentsInTemplate = insurancePlanRateChange.getDefinedInsurancePlanPremiumComponent();	
						if(null != premiumComponentsInTemplate && (!premiumComponentsInTemplate.isEmpty())){
							List<URRTPRCPremiumComponent> premiumComponentsList = new ArrayList<URRTPRCPremiumComponent>();
							LOGGER.info("Setting Premium Components :start:");
							for(DefinedInsurancePlanPremiumComponent component : premiumComponentsInTemplate){
								URRTPRCPremiumComponent unifieRatePremiumComponent = createPremiumComponent(component);
								unifieRatePremiumComponent.setUnifiedRatePlanRateChange(unifiedRateInsurancePlanRateChange);	
								premiumComponentsList.add(unifieRatePremiumComponent);
							}
							LOGGER.info("Setting Premium Component list in unifiedRateInsurancePlanRateChange: List size is : " + premiumComponentsList.size());
							unifiedRateInsurancePlanRateChange.setInsurancePremiumComponents(premiumComponentsList);
						}														
						unifiedRateInsuracePlanRateChange.add(unifiedRateInsurancePlanRateChange);
					}
					
					LOGGER.info("Setting allowed insurancePlanRateChange in UnifiedRate:end: Claim size is : " + unifiedRateInsuracePlanRateChange.size());
					urrtProductRange.setPlaRateChangeList(unifiedRateInsuracePlanRateChange);
				}
				
				LOGGER.info("Adding product rate change in the list : " + productRateChangeList.size());
				productRateChangeList.add(urrtProductRange);
				
			}			
			LOGGER.info("Setting productRateChange list in UnifiedRate:end: Product Rate Change list size is : " + productRateChangeList.size());
			unifiedRate.setProductRateChangeList(productRateChangeList);			
		}
		
		return unifiedRate;
	}
	
	/**
	 * This utility method creates URRTPRCPremiumComponent object from unmarshalled DefinedInsurancePlanPremiumComponent VO.
	 * 
	 * @param component : DefinedInsurancePlanPremiumComponent VO object.
	 * @return URRTPRCPremiumComponent object.
	 */
	private URRTPRCPremiumComponent createPremiumComponent(DefinedInsurancePlanPremiumComponent component){
		
		URRTPRCPremiumComponent unifieRatePremiumComponent = new URRTPRCPremiumComponent();								
		unifieRatePremiumComponent.setChangedRatePerPMPMQuantity(component.getChangedRatePerPMPMQuantity().getCellValue());
		unifieRatePremiumComponent.setDefiningRateChangeComponent(component.getDefiningRateChangeComponentType().getCellValue());
		
		return unifieRatePremiumComponent;
	}
	
	/**
	 * This utility method creates URRTProductRateChange object from unmarshalled ChangedInsuranceProductRateChange VO.
	 * 
	 * @param productRateChange : ChangedInsuranceProductRateChange VO.
	 * @return URRTProductRateChange object.
	 */
	private URRTProductRateChange createProductRateRange(ChangedInsuranceProductRateChange productRateChange){
		LOGGER.info("Creating URRTProductRateChange : createProductRateRange() : start :");
		URRTProductRateChange urrtProductRange = new URRTProductRateChange();
		//Set URRTProductRateChange level attributes.
		urrtProductRange.setCreationTimestamp(new Date());
		urrtProductRange.setHiosInsuranceProductIdentifier(productRateChange.getHiosInsuranceProductIdentifier().getCellValue());
		urrtProductRange.setIsObsolete(SerffConstants.NO_ABBR);
		urrtProductRange.setProductName(productRateChange.getProductName().getCellValue());	
		urrtProductRange.setWeightedAverageProductRateChangePercentQuantity(productRateChange.getWeightedAverageProductRateChangePercentQuantity().getCellValue());
		//Set lifetime loss as per year.
		List<InsuranceRateForecastTempVO.ChangedInsuranceProductRateChange.ProjectedInsuranceProductLifetimeLoss> lifeTimeLoss = productRateChange.getProjectedInsuranceProductLifetimeLoss();
		
		if (null != lifeTimeLoss && (!lifeTimeLoss.isEmpty())) {
			String lossYearValue = null;
			
			for (ProjectedInsuranceProductLifetimeLoss loss : lifeTimeLoss) {
				lossYearValue = loss.getDocumentingPeriodCycleYearType().getCellValue();
				
				if (YEAR2.equalsIgnoreCase(lossYearValue)) {
					urrtProductRange.setRateChangePercentQuantityYear2(loss.getRateChangePercentQuantity().getCellValue());
				}
				else if (YEAR1.equalsIgnoreCase(lossYearValue)) {
					urrtProductRange.setRateChangePercentQuantityYear1(loss.getRateChangePercentQuantity().getCellValue());
				}
				else if (YEAR0.equalsIgnoreCase(lossYearValue)) {
					urrtProductRange.setRateChangePercentQuantityYear0(loss.getRateChangePercentQuantity().getCellValue());
				}
			}					
		}				
		LOGGER.info("Creating URRTProductRateChange : createProductRateRange() : ends :");
		return urrtProductRange;
	}
	
	/**
	 * This utility method creates UnifiedRate object from unmarshalled InsuranceRateForecastTempVO.
	 * 
	 * @param issuer : The issuer of Unified Rates.
	 * @param applicableYear : Year to which Unified Rates are applicable.
	 * @param unifiedRateVO : InsuranceRateForecastTempVO instance.
	 * @return UnifiedRate object.
	 * @throws ParseException
	 */
	private UnifiedRate createUnifiedRateFromTemplateVO(Issuer issuer, int applicableYear, com.serff.template.urrt.InsuranceRateForecastTempVO unifiedRateVO) throws ParseException{
		LOGGER.info("Creating UnifiedRate : createUnifiedRateFromTemplateVO() : start :");
		UnifiedRate unifiedRate = new UnifiedRate();
		//Set UnifiedRate level attributes.
		unifiedRate.setIssuer(issuer);
		unifiedRate.setAdministrativeExpenseLoadPMPMAmount(unifiedRateVO.getAdministrativeExpenseLoadPMPMAmount().getCellValue());
		unifiedRate.setAdministrativeExpenseLoadPMPMTotalAmount(unifiedRateVO.getAdministrativeExpenseLoadPMPMTotalAmount().getCellValue());
		unifiedRate.setAdministrativeExpenseLoadQuantity(unifiedRateVO.getAdministrativeExpenseLoadQuantity().getCellValue());
		unifiedRate.setAllowedClaimsAmount(unifiedRateVO.getAllowedClaimsAmount().getCellValue());
		unifiedRate.setAllowedClaimsPercentQuantity(unifiedRateVO.getAllowedClaimsPercentQuantity().getCellValue());
		unifiedRate.setAllowedClaimsPMPMAmount(unifiedRateVO.getAllowedClaimsPMPMAmount().getCellValue());
		unifiedRate.setApplicableYear(applicableYear);
		unifiedRate.setCreationTimestamp(new Date());
		unifiedRate.setExperiencePeriodBeginingDate(dateFormat.parse(unifiedRateVO.getExperiencePeriodBeginningDate().getCellValue()));
		unifiedRate.setExperiencePeriodEndingDate(dateFormat.parse(unifiedRateVO.getExperiencePeriodEndingDate().getCellValue()));
		unifiedRate.setExperiencePeriodMemberMonthsQuantity(unifiedRateVO.getExperiencePeriodMemberMonthsQuantity().getCellValue());
		unifiedRate.setExpPeriodIndexRatePMPMQuantity(unifiedRateVO.getExperiencePeriodIndexRatePMPMQuantity().getCellValue());
		unifiedRate.setIncurredClaimsAmount(unifiedRateVO.getIncurredClaimsInExperiencePeriodAmount().getCellValue());
		unifiedRate.setIncurredClaimsInExperiencePeriodPMPMAmount(unifiedRateVO.getIncurredClaimsInExperiencePeriodPMPMAmount().getCellValue());
		unifiedRate.setIncurredClaimsInExperiencePeriodQuantity(unifiedRateVO.getIncurredClaimsInExperiencePeriodQuantity().getCellValue());
		unifiedRate.setIsDeleted(SerffConstants.NO_ABBR);
		unifiedRate.setLastUpdateTimestamp(new Date());
		unifiedRate.setMidpointToMidpointMonthsQuantity(unifiedRateVO.getMidpointToMidpointMonthsQuantity().getCellValue());
		unifiedRate.setPaidToAllowedAverageFactorInProjectionPeriodQuantity(unifiedRateVO.getPaidToAllowedAverageFactorInProjectionPeriodQuantity().getCellValue());
		unifiedRate.setPercentIncreaseAnnualizedQuantity(unifiedRateVO.getPercentIncreaseAnnualizedQuantity().getCellValue());
		unifiedRate.setPercentIncreaseOverExperiencePeriodQuantity(unifiedRateVO.getPercentIncreaseOverExperiencePeriodQuantity().getCellValue());
		unifiedRate.setPoolingInsuranceMarketLevelType(unifiedRateVO.getPoolingInsuranceMarketLevelType().getCellValue());
		unifiedRate.setPremiumsNetOfMlrRebateInExperiencePeriodAmount(unifiedRateVO.getPremiumsNetOfMlrRebateInExperiencePeriodAmount().getCellValue());
		unifiedRate.setPremiumsNetOfMlrRebateInExperiencePeriodPMPMAmount(unifiedRateVO.getPremiumsNetOfMlrRebateInExperiencePeriodPMPMAmount().getCellValue());
		unifiedRate.setPremiumsNetOfMlrRebateInExperiencePeriodQuantity(unifiedRateVO.getPremiumsNetOfMlrRebateInExperiencePeriodQuantity().getCellValue());
		unifiedRate.setProfitAndRiskLoadPMPMAmount(unifiedRateVO.getProfitAndRiskLoadPMPMAmount().getCellValue());
		unifiedRate.setProfitAndRiskLoadPMPMTotalAmount(unifiedRateVO.getProfitAndRiskLoadPMPMTotalAmount().getCellValue());
		unifiedRate.setProfitAndRiskLoadQuantity(unifiedRateVO.getProfitAndRiskLoadQuantity().getCellValue());
		unifiedRate.setProjectedACAReinsuranceRecoveriesPMPMAmount(unifiedRateVO.getProjectedACAReinsuranceRecoveriesPMPMAmount().getCellValue());
		unifiedRate.setProjectedACAReinsuranceRecoveriesPMPMTotalAmount(unifiedRateVO.getProjectedACAReinsuranceRecoveriesPMPMTotalAmount().getCellValue());
		unifiedRate.setProjectedAdjustedExperienceClaimsPMPMAmount(unifiedRateVO.getProjectedAdjustedExperienceClaimsPMPMAmount().getCellValue());
		unifiedRate.setProjectedAllowedExperienceClaimsAmount(unifiedRateVO.getProjectedAllowedExperienceClaimsAmount().getCellValue());
		unifiedRate.setProjectedAllowedExperienceClaimsPMPMCredibilityIndexQuantity(unifiedRateVO.getProjectedAllowedExperienceClaimsPMPMCredibilityIndexQuantity().getCellValue());
		unifiedRate.setProjectedAllowedManualClaimsPMPMCredibilityIndexQuantity(unifiedRateVO.getProjectedAllowedManualClaimsPMPMCredibilityIndexQuantity().getCellValue());
		unifiedRate.setProjectedIncurredClaimsBeforeAdjustmentPMPMTotalAmount(unifiedRateVO.getProjectedIncurredClaimsBeforeAdjustmentPMPMTotalAmount().getCellValue());
		unifiedRate.setProjectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMAmount(unifiedRateVO.getProjectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMAmount().getCellValue());
		unifiedRate.setProjectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMTotalAmount(unifiedRateVO.getProjectedIncurredClaimsBeforeReinsuranceRecoveriesPMPMTotalAmount().getCellValue());
		unifiedRate.setProjectedIncurredClaimsPMPMAmount(unifiedRateVO.getProjectedIncurredClaimsPMPMAmount().getCellValue());
		unifiedRate.setProjectedIncurredClaimsPMPMTotalAmount(unifiedRateVO.getProjectedIncurredClaimsPMPMTotalAmount().getCellValue());
		unifiedRate.setProjectedMemberMonthsQuantity(unifiedRateVO.getProjectedMemberMonthsQuantity().getCellValue());
		unifiedRate.setProjectedRiskAdjustmentsPMPMAmount(unifiedRateVO.getProjectedRiskAdjustmentsPMPMAmount().getCellValue());
		unifiedRate.setProjectionPeriodEndDate(dateFormat.parse(unifiedRateVO.getProjectionPeriodEndDate().getCellValue()));
		unifiedRate.setProjectionPeriodStartDate(dateFormat.parse(unifiedRateVO.getProjectionPeriodStartDate().getCellValue()));
		unifiedRate.setRateReviewEffectiveDate(dateFormat.parse(unifiedRateVO.getRateReviewEffectiveDate().getCellValue()));
		unifiedRate.setSingleRiskPoolGrossPremiumAvgRatePMPMAmount(unifiedRateVO.getSingleRiskPoolGrossPremiumAvgRatePMPMAmount().getCellValue());
		unifiedRate.setSingleRiskPoolGrossPremiumAvgRatePMPMTotalAmount(unifiedRateVO.getSingleRiskPoolGrossPremiumAvgRatePMPMTotalAmount().getCellValue());
		unifiedRate.setState(unifiedRateVO.getStateAbbreviationText().getCellValue());
		unifiedRate.setTaxesAndFeesPercentQuantity(unifiedRateVO.getTaxesAndFeesPercentQuantity().getCellValue());
		unifiedRate.setTaxesAndFeesPMPMAmount(unifiedRateVO.getTaxesAndFeesPMPMAmount().getCellValue());
		unifiedRate.setTotalCredibilityManualPMPMAmount(unifiedRateVO.getTotalCredibilityManualPMPMAmount().getCellValue());
		unifiedRate.setTotalExperiencePeriodOnActualExperienceAllowedPMPMAmount(unifiedRateVO.getTotalExperiencePeriodOnActualExperienceAllowedPMPMAmount().getCellValue());
		unifiedRate.setTotalProjectionsBeforeCredibilityAdjustmentPMPMAmount(unifiedRateVO.getTotalProjectionsBeforeCredibilityAdjustmentPMPMAmount().getCellValue());
		//HIX-HIX-46153 : URRT data for some fields is not saved in database.
		unifiedRate.setProjectedIncurredClaimsBeforeAdjustmentAmount(unifiedRateVO.getProjectedIncurredClaimsBeforeAdjustmentAmount().getCellValue());
		unifiedRate.setIndexRateForProjectionPeriodAmount(unifiedRateVO.getIndexRateForProjectionPeriodAmount().getCellValue());
		unifiedRate.setTaxesAndFeesPMPMTotalAmount(unifiedRateVO.getTaxesAndFeesPMPMTotalAmount().getCellValue());
		unifiedRate.setProjectedRiskAdjustmentsPMPMTotalAmount(unifiedRateVO.getProjectedRiskAdjustmentsPMPMTotalAmount().getCellValue());
		LOGGER.info("Creating UnifiedRate : createUnifiedRateFromTemplateVO() : end");
		return unifiedRate;
	}
	
	/**
	 * This utility method creates URRTAllowedClaim object from unmarshalled CollectedBenefitCategoryAllowedClaim.
	 *  
	 * @param claim : CollectedBenefitCategoryAllowedClaim VO from templates.
	 * @return URRTAllowedClaim object.
	 */
	private URRTAllowedClaim createUnifiedRateClaim(CollectedBenefitCategoryAllowedClaim claim){
		LOGGER.info(" Creating URRTAllowedClaim : createUnifiedRateClaim() : start :");
		URRTAllowedClaim unifiedRateClaim = new URRTAllowedClaim();
		unifiedRateClaim.setAnnualizedCostQuantity(claim.getAnnualizedCostQuantity().getCellValue());
		unifiedRateClaim.setAnnualizedUtilizationQuantity(claim.getAnnualizedUtilizationQuantity().getCellValue());
		unifiedRateClaim.setCredibilityUtilizationPer1000Quantity(claim.getCredibilityUtilizationPer1000Quantity().getCellValue());
		unifiedRateClaim.setCreditabilityAverageCostPerServiceAmount(claim.getCreditabilityAverageCostPerServiceAmount().getCellValue());
		unifiedRateClaim.setCreditabilityPMPMAmount(claim.getCreditabilityPMPMAmount().getCellValue());
		unifiedRateClaim.setDefiningBenefitCostType(claim.getDefiningBenefitCostType().getCellValue());
		unifiedRateClaim.setExperienceAverageCostPerServiceAmount(claim.getExperienceAverageCostPerServiceAmount().getCellValue());
		unifiedRateClaim.setExperiencePMPMAmount(claim.getExperiencePMPMAmount().getCellValue());
		unifiedRateClaim.setExperienceUtilizationDescriptionText(claim.getExperienceUtilizationDescriptionText().getCellValue());
		unifiedRateClaim.setExperienceUtilizationPer1000Quantity(claim.getExperienceUtilizationPer1000Quantity().getCellValue());
		unifiedRateClaim.setOtherAnticipatedChangeQuantity(claim.getOtherAnticipatedChangeQuantity().getCellValue());
		unifiedRateClaim.setPopulationRiskMorbidityQuantity(claim.getPopulationRiskMorbidityQuantity().getCellValue());
		unifiedRateClaim.setProjectionAverageCostPerServiceAmount(claim.getProjectionAverageCostPerServiceAmount().getCellValue());
		unifiedRateClaim.setProjectionPMPMAmount(claim.getProjectionPMPMAmount().getCellValue());
		unifiedRateClaim.setProjectionUtilizationPer1000Quantity(claim.getProjectionUtilizationPer1000Quantity().getCellValue());
		LOGGER.info("Creating URRTAllowedClaim : createUnifiedRateClaim() : end :");
		return unifiedRateClaim;
	}
	
	/**
	 * This utility method creates URRTInsurancePlanRateChange object from unmarshalled AssociatingInsurancePlanRateChange.
	 * 
	 * @param insurancePlanRateChange : AssociatingInsurancePlanRateChange VO
	 * @return
	 * @throws ParseException
	 */
	private URRTInsurancePlanRateChange createPlanRateChange(AssociatingInsurancePlanRateChange insurancePlanRateChange) throws ParseException{
		LOGGER.info(" Creating URRTInsurancePlanRateChange : createPlanRateChange() : start :");
		URRTInsurancePlanRateChange unifiedRateInsurancePlanRateChange= new URRTInsurancePlanRateChange();
		unifiedRateInsurancePlanRateChange.setAverageCurrentRatePerPMPMAmount(insurancePlanRateChange.getAverageCurrentRatePerPMPMAmount().getCellValue());
		unifiedRateInsurancePlanRateChange.setAvMetalValueQuantity(insurancePlanRateChange.getAvMetalValueQuantity().getCellValue());
		unifiedRateInsurancePlanRateChange.setAvPricingValueQuantity(insurancePlanRateChange.getAvPricingValueQuantity().getCellValue());
		unifiedRateInsurancePlanRateChange.setBasedInsurancePlanIdentifier(insurancePlanRateChange.getBasedInsurancePlanIdentifier().getCellValue());
		unifiedRateInsurancePlanRateChange.setCreationTimestamp(new Date());
		unifiedRateInsurancePlanRateChange.setDefiningInsurancePlanBenefitMetalTierType(insurancePlanRateChange.getDefiningInsurancePlanBenefitMetalTierType().getCellValue());
		unifiedRateInsurancePlanRateChange.setExchangePlanIndicator(insurancePlanRateChange.getExchangePlanIndicator().getCellValue());
		unifiedRateInsurancePlanRateChange.setIsObsolete(SerffConstants.NO_ABBR);
		unifiedRateInsurancePlanRateChange.setMemberCostShareIncreaseQuantity(insurancePlanRateChange.getMemberCostShareIncreaseQuantity().getCellValue());
		unifiedRateInsurancePlanRateChange.setPlanName(insurancePlanRateChange.getPlanName().getCellValue());
		unifiedRateInsurancePlanRateChange.setPlanType(insurancePlanRateChange.getPlanType().getCellValue());
		unifiedRateInsurancePlanRateChange.setProjectedMemberMonthsQuantity(insurancePlanRateChange.getProjectedMemberMonthsQuantity().getCellValue());
		unifiedRateInsurancePlanRateChange.setProposedRateEffectiveDate(dateFormat.parse(insurancePlanRateChange.getProposedRateEffectiveDate().getCellValue()));
		unifiedRateInsurancePlanRateChange.setRateChangeQuantity(insurancePlanRateChange.getRateChangeQuantity().getCellValue());
		unifiedRateInsurancePlanRateChange.setRateCumulativeChangeQuantity(insurancePlanRateChange.getRateCumulativeChangeQuantity().getCellValue());
		unifiedRateInsurancePlanRateChange.setRateProjectedChangeQuantity(insurancePlanRateChange.getRateProjectedChangeQuantity().getCellValue());
		unifiedRateInsurancePlanRateChange.setTotalRateChangeQuantity(insurancePlanRateChange.getTotalRateChangeQuantity().getCellValue());
		LOGGER.info(" Creating URRTInsurancePlanRateChange : createPlanRateChange() : end :");
		return unifiedRateInsurancePlanRateChange;
	}
	
	/**
	 * This utility method creates URRTPRCPremiumClaimInfo object from unmarshalled ProjecedInsurancePlanRateChangePeriod VO.
	 * 
	 * @param claimInfo : ProjecedInsurancePlanRateChangePeriod
	 * @return URRTPRCPremiumClaimInfo object.
	 */
	private URRTPRCPremiumClaimInfo createUnifiedRateClaim(ProjecedInsurancePlanRateChangePeriod claimInfo){
		LOGGER.info("Creating URRTPRCPremiumClaimInfo : createUnifiedRateClaim() : start :");
		URRTPRCPremiumClaimInfo unifiedRateClaimInfo = new URRTPRCPremiumClaimInfo();
		unifiedRateClaimInfo.setAllowedClaimsNotIssuersObligationAmount(claimInfo.getAllowedClaimsNotIssuersObligationAmount().getCellValue());
		unifiedRateClaimInfo.setAllowedClaimsPMPMQuantity(claimInfo.getAllowedClaimsPMPMQuantity().getCellValue());
		unifiedRateClaimInfo.setAverageRatePMPMQuantity(claimInfo.getAverageRatePMPMQuantity().getCellValue());
		unifiedRateClaimInfo.setDefiningInsuranceRateCostPeriodType(claimInfo.getDefiningInsuranceRateCostPeriodType().getCellValue());
		unifiedRateClaimInfo.setEhbpoacPMPMQuantity(claimInfo.getEhbPortionOfAllowedClaimsPMPMQuantity().getCellValue());
		unifiedRateClaimInfo.setFullPortionOrEhbBasisOfTACQuantity(claimInfo.getFullPortionOrEhbBasisOfTACQuantity().getCellValue());
		unifiedRateClaimInfo.setFullPortionOrEhbBasisOfTPQuantity(claimInfo.getFullPortionOrEhbBasisOfTPQuantity().getCellValue());
		unifiedRateClaimInfo.setIncurredClaimsPMPMQuantity(claimInfo.getIncurredClaimsPMPMQuantity().getCellValue());
		unifiedRateClaimInfo.setMemberMonthsQuantity(claimInfo.getMemberMonthsQuantity().getCellValue());
		unifiedRateClaimInfo.setNetReinAmount(claimInfo.getNetReinAmount().getCellValue()); 
		unifiedRateClaimInfo.setNetRiskAdjustmentAmount(claimInfo.getNetRiskAdjustmentAmount().getCellValue());
		unifiedRateClaimInfo.setOtherBenefitsPortionOfTACQuantity(claimInfo.getOtherBenefitsPortionOfTACQuantity().getCellValue());
		unifiedRateClaimInfo.setOtherBenefitsPortionOfTPQuantity(claimInfo.getOtherBenefitsPortionOfTPQuantity().getCellValue());
		String amount = claimInfo.getPortionOfAllowedClaimsPayableByHHSFundsOnBehalfOfInsuredPersonAmount().getCellValue();
		String quantity = claimInfo.getPortionOfAllowedClaimsPayableByHHSOnBehalfOfInsuredPersonQuantity().getCellValue();
		if(StringUtils.isNotBlank(amount)) {
			unifiedRateClaimInfo.setPoacPayableByHHSFundsOnBehalfOfInsuredPersonAmount(Double.valueOf(amount));
		}
		if(StringUtils.isNotBlank(quantity)) {
			unifiedRateClaimInfo.setPoacPayableByHHSOnBehalfOfInsuredPersonQuantity(Double.valueOf(quantity));
		}
		unifiedRateClaimInfo.setSmbpOfTACThatAreOtherThanEHBQuantity(claimInfo.getStateMandatedBenefitsPortionOfTACThatAreOtherThanEHBQuantity().getCellValue());
		unifiedRateClaimInfo.setSmbpThatAreOtherThanEHBQuantity(claimInfo.getStateMandatedBenefitsPortionThatAreOtherThanEHBQuantity().getCellValue());
		unifiedRateClaimInfo.setTicAmount(claimInfo.getTotalIncurredClaimsPayableWithIssuerFundsAmount().getCellValue());
		unifiedRateClaimInfo.setTotalAllowedClaimsAmount(claimInfo.getTotalAllowedClaimsAmount().getCellValue());
		unifiedRateClaimInfo.setTotalPremiumAmount(claimInfo.getTotalPremiumAmount().getCellValue());
		LOGGER.info("Creating URRTPRCPremiumClaimInfo : createUnifiedRateClaim() : end :");
		return unifiedRateClaimInfo;		
	}
	
	/**
	 * This method retrieves available plan rate change ids i.e. BI plan ids from Unified Rate object.
	 * 
	 * @param unifiedRate : Unified Rate object.
	 * @return set of ids of Plan Rate Change ids.
	 */
	private Set<String> getListOfBIPlanIdsFromUnifiedRate(UnifiedRate unifiedRate){

		LOGGER.info("getListOfBIPlanIdsFromUnifiedRate() : Getting BI Plan Ids from Rate Change List.");
		Set<String> planRateChangIds = new HashSet<String>();
		//Retrieve list of Plan Rate Change from Unified Rate.
		List<URRTProductRateChange> productRateChangeList = unifiedRate.getProductRateChangeList();
		if(!CollectionUtils.isEmpty(productRateChangeList)){
			for(URRTProductRateChange productRateChange : productRateChangeList){
				List<URRTInsurancePlanRateChange> plaRateChangeList = productRateChange.getPlaRateChangeList();
				if(!CollectionUtils.isEmpty(plaRateChangeList)){
					for(URRTInsurancePlanRateChange planRateChange : plaRateChangeList){
						planRateChangIds.add(planRateChange.getBasedInsurancePlanIdentifier());
						LOGGER.debug("BI Plan is added in set : " + planRateChange.getBasedInsurancePlanIdentifier());
					}
				}
			}
		}else{
			LOGGER.error("getListOfBIPlanIdsFromUnifiedRate() : unifiedRate.getProductRateChangeList() returned null or empty.");
		}
		LOGGER.info("Number of BI Plan Ids i.e. Plan Rate Change in Unified Rate : " + planRateChangIds.size());
		LOGGER.debug("Set of plan rate change : " + planRateChangIds);
		return planRateChangIds;
	}
	
	
	/**
	 * This method retrieves available product rate change ids from Unified Rate object.
	 * 
	 * @param unifiedRate : Unified Rate object.
	 * @return set of product rate change ids those are present in Unified Rate.
	 */
	private Set<String> getListOfProductRateChangeIdsFromUnifiedRate(UnifiedRate unifiedRate){

		LOGGER.info(" getListOfProductRateChangeIdsFromUnifiedRate() : Getting List of Product Rate Change list from Unified Rate.");
		Set<String> productRateChangIds = new HashSet<String>();
		//Retrieve list of Product Rate Change from Unified Rate.
		List<URRTProductRateChange> productRateChangeList = unifiedRate.getProductRateChangeList();
		if(!CollectionUtils.isEmpty(productRateChangeList)){
			for(URRTProductRateChange productRateChange : productRateChangeList){
				productRateChangIds.add(productRateChange.getHiosInsuranceProductIdentifier());
				LOGGER.debug("Product rate change id added in set : " + productRateChange.getHiosInsuranceProductIdentifier());
			}
		}else{
			LOGGER.error("getListOfProductRateChangeIdsFromUnifiedRate() : unifiedRate.getProductRateChangeList() returned null or empty.");
		}
		LOGGER.info("Number of Product Rate Change in Unified Rate : " + productRateChangIds.size());
		LOGGER.info("Set of product rate change : " + productRateChangIds);
		return productRateChangIds;
	}
	
	/**
	 * This method populates and save Unified Rate object in database with the help of other methods which are responsible for Unified Rate object construction.
	 * 
	 * @param entityManager  : The entity manager object for db operations.
	 * @param issuer : The issuer
	 * @param unifiedRateVO  : Unmarshalled InsuranceRateForecastTempVO instance.
	 * @param applicableYear : The year for which URRT applied.
	 * @return UnifiedRate : persisted UnifiedRate instance.
	 * @throws GIException
	 */
	@Transactional
	public UnifiedRate populateAndPersistUnifiedRate(EntityManager entityManager, Issuer issuer,
			com.serff.template.urrt.InsuranceRateForecastTempVO unifiedRateVO, int applicableYear) throws GIException {
		//Get list of existing list of Plan Rate Change that present in the database.
		String planNumberPrefix = issuer.getHiosIssuerId()+issuer.getStateOfDomicile();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("populateAndPersistUnifiedRate : start : applicableYear " + applicableYear + " planNumberPrefix:" + SecurityUtil.sanitizeForLogging(planNumberPrefix));
		}
		UnifiedRate unifiedRateSavedInDb = null;
		try{	
			//Create Unified rates from uploaded template.				
			UnifiedRate unifiedRateInTemplate = createUnifiedRate(issuer, applicableYear, unifiedRateVO);
			
			Set<String> planRateChangeInTemplate = getListOfBIPlanIdsFromUnifiedRate(unifiedRateInTemplate);
			LOGGER.info("Plan Rate Change Ids i.e. BI Plan Ids present in Template : " + planRateChangeInTemplate);
			//Get list of existing plan rate change from db.
			List<URRTInsurancePlanRateChange> existingPlanRateChangeList = iUnifiedRatePlanRateChangeRepository.getInsuranceRateChangeListByApplicableYear(planNumberPrefix, applicableYear);
			LOGGER.info("Existing Plan Rate Change list in database : " + existingPlanRateChangeList);
			//Check Plan rate change and set obsolete.
			for(URRTInsurancePlanRateChange planRateChange : existingPlanRateChangeList){
				if(planRateChangeInTemplate.contains(planRateChange.getBasedInsurancePlanIdentifier())){
					planRateChange.setIsObsolete(SerffConstants.YES_ABBR);
					entityManager.merge(planRateChange);
					LOGGER.info("Plan Rate Change is set as obsolete : BasedInsurancePlanIdentifier : " + planRateChange.getBasedInsurancePlanIdentifier());
				}else{
					LOGGER.info("BI Plan Id - "+planRateChange.getBasedInsurancePlanIdentifier()+ID_NOT_PRESENT);
					LOGGER.info("Skipped Plan Rate Change while marking as obsolete:");
				}
			}
			
			Set<String> productsRateChangeInTemplate = getListOfProductRateChangeIdsFromUnifiedRate(unifiedRateInTemplate);	
			LOGGER.info("Product Rate Change Ids i.e. HiosInsuranceProductIdentifier present in Template : " + productsRateChangeInTemplate);
			//Get list of existing product rate change from db.
			List<URRTProductRateChange> existingProductRateChangeList = iUnifiedRateProductRateChangeRepository.getProductRateChangeListByApplicableYear(planNumberPrefix, applicableYear);
			LOGGER.info("existingProductRateChangeList in database : " + existingProductRateChangeList);
			//Check Product rate change and set obsolete.
			for(URRTProductRateChange productRateChange : existingProductRateChangeList){
				if(productsRateChangeInTemplate.contains(productRateChange.getHiosInsuranceProductIdentifier())){
					productRateChange.setIsObsolete(SerffConstants.YES_ABBR);
					entityManager.merge(productRateChange);
					LOGGER.info("Product Rate Change is set as obsolete : HiosInsuranceProductIdentifier : " + productRateChange.getHiosInsuranceProductIdentifier());
				}else{
					LOGGER.info("Product Rate Change - " + productRateChange.getHiosInsuranceProductIdentifier() + ID_NOT_PRESENT);
					LOGGER.info("Skipped Product Rate Change while marking as obsolete:");
				}
			}
			
			//Set all existing Unified Rates as deleted. 
			LOGGER.info("Retrieving existing Unified Rates. These Unified Rates will be marked as obsolete.");
			List<UnifiedRate> existingUnifiedRateList = iUnifiedRateRepository.findByIssuerAndApplicableYearAndIsDeleted(issuer, applicableYear, SerffConstants.NO_ABBR);
			LOGGER.info("ExistingUnifiedRateList is :" + (existingUnifiedRateList == null ? null : existingUnifiedRateList.size()));
			if( null != existingUnifiedRateList && !existingUnifiedRateList.isEmpty()){
				for(UnifiedRate rate : existingUnifiedRateList){
					rate.setIsDeleted(SerffConstants.YES_ABBR);
					rate = entityManager.merge(rate);
					LOGGER.info("Unified Rate is set as deleted : Id : " + rate.getId());
				}
			}
			
			//Persist Newly Created Unified Rate.
			unifiedRateSavedInDb = persistUnifiedRate(unifiedRateInTemplate, entityManager);
		}catch(Exception e){
			LOGGER.error("Error occurred while creating unified rate : " + e);
			throw new GIException(Integer.parseInt(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE), SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE + " : "+ e.getMessage(), StringUtils.EMPTY);
		}
		
		return unifiedRateSavedInDb;
	}
	
	@Transactional
	public Map <String, BusinessRule> populateAndPersistBusinessRuleBean(EntityManager entityManager, Issuer issuer,
			com.serff.template.rbrules.extension.PayloadType rulePayloadType, int applicableYear) throws GIException {
		
		LOGGER.info("populateAndPersistBusinessRuleBean start : ");
		
		Map<String, BusinessRule> savedBusinessRulesMap = new HashMap<String, BusinessRule>();
		
		try {
			
			if (null != issuer) {
				
				List<BusinessRule> existingBusinessRuleList  = ibusinessRuleRepo.findByIssuerAndIsObsoleteAndApplicableYear(issuer, SerffConstants.NO_ABBR, applicableYear);
				//Mark current rules as obsolete
				if(!CollectionUtils.isEmpty(existingBusinessRuleList)){
					for(BusinessRule existingbr : existingBusinessRuleList){
						existingbr.setIsObsolete(SerffConstants.YES_ABBR);
						existingbr.setIssuer(issuer);
						entityManager.merge(existingbr);
					}
				}
				
				RateCategoryDeterminationRulesetType rateCategoryDeterminationRulesetType = null;
				List<com.serff.template.rbrules.hix.pm.InsuranceProductType > insuranceProductTypeList = null;
			
				String issuerIdentification = null;
				com.serff.template.rbrules.hix.pm.IssuerType issuerType = rulePayloadType.getIssuer();
				
				if(null!= issuerType && null!= issuerType.getIssuerIdentification() && 
						null!=issuerType.getIssuerIdentification().getIdentificationID()){
					issuerIdentification = issuerType.getIssuerIdentification().getIdentificationID().getValue();
				}
				
				BusinessRule businessRule = createBasicBusinessRule(issuer, issuerIdentification, ISSUER_RULE_TYPE, applicableYear);
				
				if (null != issuerType && null != issuerType.getIssuerRateCategoryDeterminationRuleset()) {
					rateCategoryDeterminationRulesetType = issuerType.getIssuerRateCategoryDeterminationRuleset();
					setBusinesRulesParam(rateCategoryDeterminationRulesetType, businessRule);
				}
				
				persistBusinessRule(businessRule, entityManager, savedBusinessRulesMap);

				insuranceProductTypeList = rulePayloadType.getIssuer().getInsuranceProduct();
				
				if (!CollectionUtils.isEmpty(insuranceProductTypeList)) {
					for (com.serff.template.rbrules.hix.pm.InsuranceProductType insuranceProductType : insuranceProductTypeList) {
							if (null!= insuranceProductType && null != insuranceProductType.getInsuranceProductIdentification()
									&& null != insuranceProductType.getInsuranceProductIdentification().getIdentificationID()
									&& StringUtils.isNotEmpty(insuranceProductType.getInsuranceProductIdentification().getIdentificationID().getValue())) {
								createProductBusinessRule(insuranceProductType, issuer, entityManager, savedBusinessRulesMap, applicableYear);
							}
							else if (null!= insuranceProductType && null != insuranceProductType.getInsurancePlan()) {
								createPlanBusinessRule(insuranceProductType.getInsurancePlan(), issuer, entityManager, savedBusinessRulesMap, applicableYear);
							}
						}
					}
				}
			} finally {
			LOGGER.info("populateAndPersistBusinessRuleBean Ends : ");
		}
		return savedBusinessRulesMap;
	}
	
	/**
	 * 
	 * @param businessRule
	 * @param entityManager
	 * @param savedBusinessRule
	 */
	private void persistBusinessRule(BusinessRule businessRule, EntityManager entityManager, Map<String, BusinessRule> savedBusinessRule) {
		if (null != entityManager && entityManager.isOpen()) {
			BusinessRule mergedBusinessRule = entityManager.merge(businessRule);
			String appliesTo= mergedBusinessRule.getAppliesTo();
			savedBusinessRule.put(appliesTo, mergedBusinessRule);
		}
	}
	
	
	/**
	 * This method uses entity manager and persist Unified Rate instance in the database.
	 * 
	 * @param unifiedRate : Unified Rate object which is to be persisted in db.
	 * @param entityManager : The entity manager instance.
	 * @return UnifiedRate object persisted in the database.
	 * @throws GIException 
	 */
	private UnifiedRate persistUnifiedRate(UnifiedRate unifiedRate, EntityManager entityManager) throws GIException {
		
		UnifiedRate mergedUnifiedRate = null;
		
		if (null != entityManager && entityManager.isOpen()) {
			LOGGER.info("persistUnifiedRate() : start : ");
			mergedUnifiedRate = entityManager.merge(unifiedRate);
			LOGGER.info("persistUnifiedRate() : Unified Rate is saved in db : " + mergedUnifiedRate.getId());
			LOGGER.info("persistUnifiedRate() : end : ");
		}else{
			LOGGER.error("Entity manager is not open.");
			throw new GIException("Error occurred while saving Unified Rate in database. Entity manager is not open.");
		}
		
		return mergedUnifiedRate;
	}
	
	/**
	 * 
	 * @param insurancPlanTypeList
	 * @param issuer
	 * @param entityManager
	 * @param savedBusinessRule
	 * @param applicableYear
	 */
	private void createPlanBusinessRule(List<com.serff.template.rbrules.hix.pm.InsurancePlanType> insurancPlanTypeList, Issuer issuer, 
			EntityManager entityManager, Map<String, BusinessRule> savedBusinessRule, int applicableYear) {
		
		String planIdentification = null;
		BusinessRule businessRule = null;
		RateCategoryDeterminationRulesetType rateCategoryDeterminationRulesetType = null;
		for(com.serff.template.rbrules.hix.pm.InsurancePlanType insurancPlanType : insurancPlanTypeList) {
			
			if (null != insurancPlanType) {
				planIdentification = insurancPlanType.getInsurancePlanIdentification().getIdentificationID().getValue();
				businessRule = createBasicBusinessRule(issuer, planIdentification, PLAN_RULE_TYPE, applicableYear);
				rateCategoryDeterminationRulesetType = insurancPlanType.getInsurancePlanRateCategoryDeterminationRuleset();
				
				if (null != rateCategoryDeterminationRulesetType) {
					setBusinesRulesParam(rateCategoryDeterminationRulesetType, businessRule);
					persistBusinessRule(businessRule, entityManager, savedBusinessRule);
				}
			}
		}
	}

	private void createProductBusinessRule(com.serff.template.rbrules.hix.pm.InsuranceProductType insuranceProductType, Issuer issuer,
			EntityManager entityManager, Map<String, BusinessRule> savedBusinessRule, int applicableYear){
	
		String productIdentification = insuranceProductType.getInsuranceProductIdentification().getIdentificationID().getValue();
		RateCategoryDeterminationRulesetType rateCategoryDeterminationRulesetType =null;
		if(null!=productIdentification && StringUtils.isNotEmpty(productIdentification)){
			BusinessRule businessRule = createBasicBusinessRule(issuer, productIdentification,PRODUCT_RULE_TYPE, applicableYear);
			rateCategoryDeterminationRulesetType =  insuranceProductType.getInsuranceProductRateCategoryDeterminationRuleset();
			if(null!=rateCategoryDeterminationRulesetType){
				setBusinesRulesParam(rateCategoryDeterminationRulesetType, businessRule);
				persistBusinessRule(businessRule, entityManager, savedBusinessRule);
			}
		}	
	}
	
	private String isCohabitationRequiredIndicator(FamilyRelationshipCohabitationRuleType familyRelationshipCohabitationRuleType) {
		
		if(familyRelationshipCohabitationRuleType.getCohabitationRequiredIndicator().isValue()){
			return SerffConstants.YES;
		}
		return SerffConstants.NO;
	}
	
	private void setFamilyRelations(List<FamilyRelationshipCohabitationRuleType> familyRelationshipCohabitationRuleTypelist,
			BusinessRule businessRule) {
		
		if (null == familyRelationshipCohabitationRuleTypelist
				|| familyRelationshipCohabitationRuleTypelist.isEmpty()) {
			return;
		}
		
		String ruleCode = null;
		String isRequiredIndicator = null;
		
		for(FamilyRelationshipCohabitationRuleType familyRelationshipCohabitationRuleType : familyRelationshipCohabitationRuleTypelist){
			
			if (null == familyRelationshipCohabitationRuleType) {
				continue;
			}
			ruleCode = familyRelationshipCohabitationRuleType.getFamilyRelationshipCohabitationRuleCode().value();
			isRequiredIndicator = isCohabitationRequiredIndicator(familyRelationshipCohabitationRuleType);
			
			if (FamilyRelationEnum.ADOPTED_CHILD.value().equals(ruleCode)) {
				businessRule.setAdoptedChild(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.ANNULTANT.value().equals(ruleCode)) {
				businessRule.setAnnuitant(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.CHILD.value().equals(ruleCode)) {
				businessRule.setChild(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.COLLATERAL_DEPENDENT.value().equals(ruleCode)) {
				businessRule.setColateralDpdnt(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.COURT_APPOINTED_GUARDIAN.value().equals(ruleCode)) {
				businessRule.setCourtAppGuardian(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.COUSIN.value().equals(ruleCode)) {
				businessRule.setCousin(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.DEPENDENT_OF_MINOR_DEPENDENT.value().equals(ruleCode)) {
				businessRule.setDpndntOnMinorDpdnt(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.EX_SPOUSE.value().equals(ruleCode)) {
				businessRule.setExSpouse(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.FOSTER_CHILD.value().equals(ruleCode)) {
				businessRule.setFosterChild(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.GRANDCHILD.value().equals(ruleCode)) {
				businessRule.setgSonOrGdaughter(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.GRANDPARENT.value().equals(ruleCode)) {
				businessRule.setGfatherOrGmother(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.GUARDIAN.value().equals(ruleCode)) {
				businessRule.setGuardian(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.LIFE_PARTNER.value().equals(ruleCode)) {
				businessRule.setLifePartner(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.OTHER_RELATIONSHIP.value().equals(ruleCode)) {
				businessRule.setOtherRelationship(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.OTHER_RELATIVE.value().equals(ruleCode)) {
				businessRule.setOtherRelative(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.PARENT.value().equals(ruleCode)) {
				businessRule.setFatherOrMother(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.PARENT_IN_LAW.value().equals(ruleCode)) {
				businessRule.setFatherInLawOrMotherInLaw(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.SELF.value().equals(ruleCode)) {
				businessRule.setSelf(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.SIBLING.value().equals(ruleCode)) {
				businessRule.setBrotherOrSister(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.SIBLING_CHILD.value().equals(ruleCode)) {
				businessRule.setNephewOrNiece(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.SIBLING_IN_LAW.value().equals(ruleCode)) {
				businessRule.setBroInLawOrSisInLaw(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.SPONSORED_DEPENDENT.value().equals(ruleCode)) {
				businessRule.setSponsoredDpdnt(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.SPOUSE.value().equals(ruleCode)) {
				businessRule.setSpouse(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.STEP_CHILD.value().equals(ruleCode)) {
				businessRule.setStepsonOrStepDaughter(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.STEP_PARENT.value().equals(ruleCode)) {
				businessRule.setStepParent(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.TRUSTEE.value().equals(ruleCode)) {
				businessRule.setTrustee(isRequiredIndicator);
			}
			else if (FamilyRelationEnum.WARD.value().equals(ruleCode)) {
				businessRule.setWard(isRequiredIndicator);
			}
		}
	}

	private void setBusinesRulesParam(RateCategoryDeterminationRulesetType rateCategoryDeterminationRulesetType, BusinessRule busRule) {
	
		List<FamilyRelationshipCohabitationRuleType> familyRelationshipCohabitationRuleTypelist = null;
		familyRelationshipCohabitationRuleTypelist = rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetFamilyRelationshipCohabitationRule();
		setFamilyRelations(familyRelationshipCohabitationRuleTypelist, busRule);
		setIdicatorValues(rateCategoryDeterminationRulesetType , busRule);
		if(null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetChildrenOnlyContractMaxChildrenRuleCode()&& null!=
				rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetChildrenOnlyContractMaxChildrenRuleCode().value()){
			busRule.setMaxChildren(rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetChildrenOnlyContractMaxChildrenRuleCode().value().toString());
		}
		if(null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetAgeDeterminationRuleCode()&&  
				null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetAgeDeterminationRuleCode().value()){
			busRule.setAgeRule(rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetAgeDeterminationRuleCode().value().toString());
		}
		if(null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetEnrolleeContractRateDeterminationRuleCode() &&
				null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetEnrolleeContractRateDeterminationRuleCode().value()){
			busRule.setEnrolleeRateRule(rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetEnrolleeContractRateDeterminationRuleCode().value().toString());
		}
		if(null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetSingleParentFamilyMaxDependentsRuleCode() &&
				null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetSingleParentFamilyMaxDependentsRuleCode().value()){
			busRule.setSingleParentFmlyDpndt(rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetSingleParentFamilyMaxDependentsRuleCode().value().toString());
		}
		if(null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetTwoParentFamilyMaxDependentsRuleCode() &&
				null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetTwoParentFamilyMaxDependentsRuleCode().value()){
			
			busRule.setTwoParentFmlyDpndt(rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetTwoParentFamilyMaxDependentsRuleCode().value().toString());
		}
	}
	
	private void setIdicatorValues(RateCategoryDeterminationRulesetType rateCategoryDeterminationRulesetType,
			BusinessRule busRule) {

		if (null == rateCategoryDeterminationRulesetType || null == busRule) {
			LOGGER.debug("Input parameters are empty in setIdicatorValues().");
			return;
		}
		
		if(null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetDependentMaximumAgeMeasure() &&  
				null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetDependentMaximumAgeMeasure().getMeasurePointValue() &&
				null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetDependentMaximumAgeMeasure().getMeasurePointValue().getValue() ){
			busRule.setIsDpdntMaxAge(rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetDependentMaximumAgeMeasure().getMeasurePointValue().getValue().toString());
		}
		
		if(null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetMinimumTobaccoFreeMonthsMeasure() && 
				null!=rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetMinimumTobaccoFreeMonthsMeasure().getMeasurePointValue() && 
				null!= rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetMinimumTobaccoFreeMonthsMeasure().getMeasurePointValue().getValue()){
			busRule.setMinTobacoMonths(rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetMinimumTobaccoFreeMonthsMeasure().getMeasurePointValue().getValue().toString());
		}
		
		if(null!= rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetDomesticPartnerAsSpouseIndicator()){
			if(rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetDomesticPartnerAsSpouseIndicator().isValue()){
				busRule.setDomesticPartnerSubsc(SerffConstants.YES);
			}
			else {
				busRule.setDomesticPartnerSubsc(SerffConstants.NO);
			}
		}
		
		if(null!= rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetSameSexPartnerAsSpouseIndicator()){
			if(rateCategoryDeterminationRulesetType.getRateCategoryDeterminationRulesetSameSexPartnerAsSpouseIndicator().isValue()){
				busRule.setSameSexPartnerSubsc(SerffConstants.YES);
			}
			else {
				busRule.setSameSexPartnerSubsc(SerffConstants.NO);
			}
		}
	}

	private BusinessRule getBusinessRule(String standarCompId,  Map<String , BusinessRule> businesRuleMap){
	
		BusinessRule bRule = null;
		String planNumer = standarCompId ;
		String productId = standarCompId.substring(SerffConstants.PLAN_INITIAL_INDEX, SerffConstants.LEN_HIOS_PRODUCT_ID);
		String issuerId =  standarCompId.substring(SerffConstants.PLAN_INITIAL_INDEX, SerffConstants.LEN_HIOS_ISSUER_ID);
		 LOGGER.info("PlanNumber : ="+planNumer);
		if(!CollectionUtils.isEmpty(businesRuleMap)){
			
			if(null!=businesRuleMap.get(planNumer)){
				LOGGER.info("business rule is applied to plan :  " + planNumer);
				bRule = businesRuleMap.get(planNumer);
			
				
			}else if (null!=businesRuleMap.get(productId)){
				LOGGER.info("business rule is applied to product :  " + productId);
				bRule = businesRuleMap.get(productId);
				
			}else if(null!=businesRuleMap.get(issuerId)){
				LOGGER.info("business rule is applied to Issuer :  " + issuerId);
				bRule = businesRuleMap.get(issuerId);
				
			}
		}
		return bRule;
	}
	
	/*	
	 *	If(flagIsBRuleTempPresent)
	 * {
	 * 		//get all plans for that issuer
	 * 		Make maps for plan array & cert status
	 * 		for all non-cert plans, find brule and update all variants with that brule id
	 * 		skip certified plans (set the existing brule id)
	 * 	} else {
	 * 		for all savedPlans, find brule and set it & merge
	 * }
	 */
	public void refreshBusinessRule(String planInsuranceType, String issuerid, EntityManager entityManager,
			Map<String, BusinessRule> brMap, Integer applicableYear) {
		
		LOGGER.info("refreshBusinessRule start : ");
		
		try {
			List <Plan> issuerPlanList = iPlanRepository.findByIssuerPlanNumberStartingWithAndIsDeletedAndApplicableYearAndInsuranceType(issuerid, SerffConstants.NO_ABBR, applicableYear, planInsuranceType); 
			updateBussinessRuleForPlans(issuerPlanList, entityManager, brMap);
		}
		finally {
			LOGGER.info("refreshBusinessRule ends : ");
		}
	}
	
	private void updateBussinessRuleForPlans(List<Plan> issuerPlanList,
			EntityManager entityManager, Map<String, BusinessRule> brMap) {
		
		if(!CollectionUtils.isEmpty(issuerPlanList)) {
			
			Map <String , List<Plan>> planMaps  = new HashMap<String, List<Plan>>();
			List<String> cretifiedPlanList = new ArrayList<String>();
			List <Plan> planList = null;
			String componentId = null;
			String status = null;
			
			for (Plan issuerPlan : issuerPlanList) {
				
				componentId = issuerPlan.getIssuerPlanNumber().substring(SerffConstants.PLAN_INITIAL_INDEX, SerffConstants.LEN_HIOS_PLAN_ID);
				
				status = issuerPlan.getStatus();
				
				if(PlanStatus.CERTIFIED.toString().equalsIgnoreCase(status)){
					LOGGER.info(" certified plan component id : "+componentId);
					cretifiedPlanList.add(componentId);
				}
				else {
					if (planMaps.containsKey(componentId)) {
						planList = planMaps.get(componentId);
						planList.add(issuerPlan);
					}
					else {
						List <Plan> newPlanList  = new ArrayList<Plan>();
						newPlanList.add(issuerPlan);
						planMaps.put(componentId , newPlanList);
					}
				}
			}
			
			linkBusinessRuletoPlan(cretifiedPlanList, planMaps , entityManager ,brMap);
		}
	}
	
	private void linkBusinessRuletoPlan(List<String> cretifiedPlanList, Map<String, List<Plan>> planMaps,
			EntityManager entityManager, Map<String, BusinessRule> brMap) {
		BusinessRule bruletoApply = null;
		if(!CollectionUtils.isEmpty(planMaps)){
			for (Map.Entry<String, List<Plan>> entry : planMaps.entrySet()) {
				
				String nonCertifiedPlancomponentId = entry.getKey();
				
				if (!cretifiedPlanList.contains(nonCertifiedPlancomponentId)) {

					List<Plan> noncertifiedPlanList = planMaps.get(nonCertifiedPlancomponentId);
				
					if(!CollectionUtils.isEmpty(noncertifiedPlanList)){
						for (Plan eachNonCertifiedPlan : noncertifiedPlanList) {
							
							bruletoApply = getBusinessRule(nonCertifiedPlancomponentId, brMap);
							
							if (null != bruletoApply) {
								eachNonCertifiedPlan.setBusinessRuleId(bruletoApply.getId());
								entityManager.merge(eachNonCertifiedPlan);
							}
						}
					}
				}
			}
		}
	}

	public Map<Integer, DrugList> loadDrugList(EntityManager entityManager, Issuer issuer, 
			com.serff.template.plan.PrescriptionDrugTemplateVO prescriptionDrug) {
		
		LOGGER.info("Beginning loadDrugList() : issuer id : " + issuer.getId());
		List<DrugListVO> drugListVOs = null;
		drugListVOs = prescriptionDrug.getDrugList().getDrugListVO();
		Validate.notNull(drugListVOs, "No Drug List data is found for the request.");
		Validate.notEmpty(drugListVOs, "Drug List data is missing in the request.");
		//setting Drug list
		Map<Integer, DrugList> drugListMap = new HashMap<Integer, DrugList>();
		String[] drugTierType = null;
		Integer drugTierLevel = null;
		
		if (null != drugListVOs) {
			LOGGER.info("loadDrugList() : drugListVOs size : " + drugListVOs.size());
			
			for (DrugListVO drugListVO : drugListVOs) {
				
				if (null == drugListVO) {
					continue;
				}
				Map<Integer, DrugTier> drugTierLevelMap = new HashMap<Integer, DrugTier>();
				DrugList drugList = new DrugList();
				drugList.setIssuer(issuer);
				Integer formularyDrugListId = Integer.valueOf(drugListVO.getDrugListID().getCellValue().trim());
				drugList.setFormularyDrugListId(formularyDrugListId);
				drugList.setDrugsTiers(new ArrayList<DrugTier>());
				drugList.setIsDeleted(SerffConstants.NO_ABBR);
				
				List<DrugTierLevelVO> drugTierLevelVOList = drugListVO.getDrugTierLevelList().getDrugTierLevelVO();
				List<DrugVO> drugVOList = drugListVO.getDrugVOList().getDrugVO();
				Validate.notNull(drugTierLevelVOList, "No Drug Tier Level data is found in the request object.");
				Validate.notEmpty(drugTierLevelVOList, "Drug Tier Level data is missing from the request object.");
				Validate.notNull(drugVOList, "No Drug data is found in the request object.");
				Validate.notEmpty(drugVOList, "Drug data is missing from the request object.");
				
				for(DrugTierLevelVO drugTierLevelVO: drugTierLevelVOList) {
					if(null != drugTierLevelVO) {
						DrugTier drugTier = new DrugTier(); 
						drugTierLevel = Integer.valueOf(drugTierLevelVO.getDrugTierLevel().getCellValue().trim());
						drugTierType = drugTierLevelVO.getDrugTierType().getCellValue().split(",");
						drugTier.setDrugs(new ArrayList<Drug>());
						drugTier.setDrugTierLevel(drugTierLevel);
						drugTier.setDrugTierType1(drugTierType[0]);
						if(drugTierType.length > 1) {
							drugTier.setDrugTierType2(drugTierType[1]);
						}
						drugTierLevelMap.put(drugTierLevel, drugTier);
						drugTier.setDrugList(drugList);
						drugList.getDrugsTiers().add(drugTier);
					}
				}
				for(DrugVO drugVO: drugVOList) {
					if(null != drugVO) {
						//If tier is NA, skip it
						if(!StringUtils.isNumeric(drugVO.getTier().getCellValue())) {
							continue;
						}
						Drug drug = new Drug(); 
						drugTierLevel = Integer.valueOf(drugVO.getTier().getCellValue().trim());
						DrugTier dt = drugTierLevelMap.get(drugTierLevel);
						if(null == dt) {
							LOGGER.error("loadDrugList() : Drug tier object not found for Drug List ID:" + formularyDrugListId + " Tier :" + drugTierLevel);
							throw new IllegalArgumentException("Drug tier object not found for Drug List ID:" + formularyDrugListId + " Tier:" + drugTierLevel);
						}
						drug.setRxcui(drugVO.getRXCUI().getCellValue());
						drug.setAuthRequired(SerffConstants.YES.equalsIgnoreCase(drugVO.getAuthorizationRequired().getCellValue()) ? SerffConstants.YES_ABBR : SerffConstants.NO_ABBR);
						drug.setStepTherapyRequired(SerffConstants.YES.equalsIgnoreCase(drugVO.getStepTherapyRequired().getCellValue()) ? SerffConstants.YES_ABBR : SerffConstants.NO_ABBR);
						drug.setDrugTier(dt);
						dt.getDrugs().add(drug);
					}
				}
	            LOGGER.info("loadDrugList() : Druglist saved for ID: " + formularyDrugListId);

				if (null != entityManager && entityManager.isOpen()) {
					drugList = entityManager.merge(drugList);
					Validate.notNull(drugList, "drugList instance is missing post persistence.");
				}
				drugListMap.put(formularyDrugListId, drugList);
			}
		}		
		 
		LOGGER.info("loadDrugList() : returning drugListMap : size :  " + drugListMap.size());
		 
		return drugListMap;
	}
	@Override
	public boolean populateAndPersistCSRAmountFromSERFF(CSRAdvancePaymentDeterminationType advancePaymentDeterminationType) throws GIException  {
		/* Issuer Type List */
		List<com.serff.template.csr.hix.pm.IssuerType> issuerList = null;
		/* InsurancePlanStandardComponentIdentification */
		List<InsurancePlanType> issuerPlanNameList = null;
		/* InsurancePlanVariantIdentification */
		List<InsurancePlanVariantType> costSharingList = null;

		com.serff.template.csr.hix.pm.IssuerType issuerType = null;
		InsurancePlanType insurancePlanType = null;
		InsurancePlanVariantType insurancePlanVariantType = null;
		AmountType amountType = null;

		Issuer issuer=null;
		Plan plan=null;
		PlanHealth planHealth=null;

		String hiosIssuerID = null;
		String planNumberForIssuer = null;
		String costSharingTypeID = null;
		String updateResult = null;
		Float payAmout = null;
		String csType=null;

		try {

			Validate.notNull(advancePaymentDeterminationType, "Invalid CSRAdvancePaymentDeterminationType instance encountered in request.");

			issuerList = advancePaymentDeterminationType.getIssuer();/* IssuerTypeList */

			Validate.notNull(issuerList, "Issuer(s) is missing for current CSR process request.");
			Validate.notEmpty(issuerList, "Issuer(s) is missing/invalid for current CSR process request.");

			Validate.notNull(serffIssuerService, "Issuer Service is improperly configured.");
//			Validate.notNull(planManagementService, "PlanManagement Service is improperly configured.");

			Iterator<com.serff.template.csr.hix.pm.IssuerType> issuerListItr = issuerList.iterator();

			if(null == issuerListItr) {
				LOGGER.info("No issuers found for current CSR process request.");
			}
			else {

				while (issuerListItr.hasNext()) {
					issuerType = issuerListItr.next();

					Validate.notNull(issuerType, "No IssuerType found in current iteration of CSR process request.");

					Validate.notNull(issuerType.getIssuerIdentification(), "Missing IssuerIdentification for current IssuerType.");
					Validate.notNull(issuerType.getIssuerIdentification().getIdentificationID(), "Missing IssuerIdentification's IdentificationID for current IssuerType.");

					hiosIssuerID = issuerType.getIssuerIdentification().getIdentificationID().getValue();

					Validate.notNull(hiosIssuerID, "Missing Issuer HIOS ID for current Issuer identified by '" + hiosIssuerID + "'.");
					Validate.notEmpty(hiosIssuerID, "Missing/Empty Issuer HIOS ID for current Issuer identified by '" + hiosIssuerID + "'.");

					issuer = serffIssuerService.getIssuerByHiosID(hiosIssuerID);/* Issuer Plan List */

					Validate.notNull(issuer, "No Issuer record for current Issuer identified by '" + hiosIssuerID + "' found in database.");

					issuerPlanNameList = issuerType.getInsurancePlan();

					Validate.notNull(issuerPlanNameList, "Issuer Plan Names is missing for current Issuer identified by '" + hiosIssuerID + "'.");
					Validate.notEmpty(issuerPlanNameList, "Issuer Plan Names is missing/invalid for current Issuer identified by '" + hiosIssuerID + "'.");

					Iterator<InsurancePlanType> issuerPlanNameListItr = issuerPlanNameList.iterator();

					while (issuerPlanNameListItr.hasNext()) {
						insurancePlanType = issuerPlanNameListItr.next();

						Validate.notNull(insurancePlanType, "InsurancePlanType is missing for current Issuer identified by '" + hiosIssuerID + "'.");

						Validate.notNull(insurancePlanType.getInsurancePlanStandardComponentIdentification(), "Missing InsurancePlanStandardComponentIdentification for current Issuer identified by '" + hiosIssuerID + "'.");
						Validate.notNull(insurancePlanType.getInsurancePlanStandardComponentIdentification().getIdentificationID(), "Missing InsurancePlanStandardComponentIdentification's IdentificationID for current Issuer identified by '" + hiosIssuerID + "'.");

						planNumberForIssuer = insurancePlanType.getInsurancePlanStandardComponentIdentification().getIdentificationID().getValue();

						Validate.notNull(planNumberForIssuer, "Missing InsurancePlanStandardComponentIdentification's IdentificationID's Value for current Issuer identified by '" + hiosIssuerID + "'.");
						Validate.notEmpty(planNumberForIssuer, "Missing/Empty InsurancePlanStandardComponentIdentification's IdentificationID's Value for current Issuer identified by '" + hiosIssuerID + "'.");

						plan = getRequiredPlan(issuer.getId(), planNumberForIssuer);

						Validate.notNull(plan, "Plan record for current Plan identified by '" + planNumberForIssuer + MSG_AND_ISSUER_IDENTIFIED_BY + hiosIssuerID + "' was not found in database.");

						costSharingList = insurancePlanType.getInsurancePlanVariant();/* Cost Sharing List */

						Validate.notNull(costSharingList, "Missing InsurancePlanVariant for current Issuer identified by '" + hiosIssuerID + "'.");
						Validate.notEmpty(costSharingList, "Missing/empty InsurancePlanVariant for current Issuer identified by '" + hiosIssuerID + "'.");

						Iterator<InsurancePlanVariantType> costSharingListItr = costSharingList.iterator();

						while (costSharingListItr.hasNext()) {
							insurancePlanVariantType = costSharingListItr.next();

							Validate.notNull(insurancePlanVariantType, "Missing InsurancePlanVariantType for Plan identified by '" + planNumberForIssuer + MSG_AND_ISSUER_IDENTIFIED_BY + hiosIssuerID + "'.");

							Validate.notNull(insurancePlanVariantType.getInsurancePlanVariantIdentification(), "Missing InsurancePlanVariantIdentification in payload bean.");
							Validate.notNull(insurancePlanVariantType.getInsurancePlanVariantIdentification().getIdentificationID(), "Missing InsurancePlanVariantIdentification's IdentificationID in payload bean.");
							Validate.notNull(insurancePlanVariantType.getInsurancePlanVariantIdentification().getIdentificationID().getValue(), "Missing InsurancePlanVariantIdentification's IdentificationID's Value in payload bean.");

							costSharingTypeID = insurancePlanVariantType.getInsurancePlanVariantIdentification().getIdentificationID().getValue();

							Validate.notNull(costSharingTypeID, "Missing Cost sharing Type encountered for current Insurance Plan Variant Type.");
							Validate.notEmpty(costSharingTypeID, "Missing/Empty Cost sharing Type encountered for current Insurance Plan Variant Type.");

							csType = getCSType(costSharingTypeID);

							Validate.notNull(csType, "Missing Cost Sharing Type for current Plan identified by '" + planNumberForIssuer + MSG_AND_ISSUER_IDENTIFIED_BY + hiosIssuerID + "'.");

							planHealth = getRequiredPlanHealthObject(issuer.getId(), plan.getId(), csType);

							Validate.notNull(planHealth, "No Plan Health found for Cost Sharing Type identified by '" + csType + MSG_PLAN_IDENTIFIED_BY + planNumberForIssuer + MSG_AND_ISSUER_IDENTIFIED_BY + hiosIssuerID + "'.");

							amountType = insurancePlanVariantType.getCostSharingReductionAdvancePaymentAmount();/* CSR Advance Pay Amout */

							Validate.notNull(amountType, "Missing CostSharingReductionAdvancePaymentAmount for Cost Sharing Type identified by '" + csType + MSG_PLAN_IDENTIFIED_BY + planNumberForIssuer + MSG_AND_ISSUER_IDENTIFIED_BY + hiosIssuerID + "'.");

							Validate.notNull(amountType.getValue(), "Missing CostSharingReductionAdvancePaymentAmount value for current Cost Sharing Type identified by '" + csType + MSG_PLAN_IDENTIFIED_BY + planNumberForIssuer + MSG_AND_ISSUER_IDENTIFIED_BY + hiosIssuerID + "'.");

							payAmout = Float.parseFloat(amountType.getValue().toString().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));

							updateResult = updateCSRAmoutFromSerff(issuer, plan, csType, payAmout);

							LOGGER.debug("The outcome of current CSRAMount update for Cost Sharing Type identified by '" + csType + MSG_PLAN_IDENTIFIED_BY + planNumberForIssuer + MSG_AND_ISSUER_IDENTIFIED_BY + hiosIssuerID + "'  is '" + updateResult + "'.");
						}
					}
				}
			}

			if(StringUtils.isBlank(updateResult)){
				return FALSE;
			}

		}
		catch (Exception e) {
			LOGGER.error("Exception occured while persisting CSR information. Exception: " + e.getMessage(), e);
			return FALSE;
		}

		return (RESPONSE_SUCCESS.equalsIgnoreCase(updateResult));
	}

	private String updateCSRAmoutFromSerff(Issuer issuer, Plan plan,String csType, Float payAmout) {
		return (updateCsrAmount(payAmout,issuer.getId(),plan.getId(),csType));
	}

	private String getCSType(String costSharingTypeID) {
		String csType = null;
		/*Sandip-Below String needs to be moved in constants need a cleanup JIRA for entire PlanMgmt module*/
		if (SerffConstants.OFF_EXCHANGE_VARIANT_SUFFIX.equals(costSharingTypeID)) {
			csType = "CS0";
		}
		else if ("01".equals(costSharingTypeID)) {
			csType = "CS1";
		}
		else if ("02".equals(costSharingTypeID)) {
			csType = "CS2";
		}
		else if ("03".equals(costSharingTypeID)) {
			csType = "CS3";
		}
		else if ("04".equals(costSharingTypeID)) {
			csType = "CS4";
		}
		else if ("05".equals(costSharingTypeID)) {
			csType = "CS5";
		}
		else if ("06".equals(costSharingTypeID)) {
			csType = "CS6";
		}
		return csType;
	}

	/**
	 * Method to  populate and save Issuer bean from SERFF Admin PayloadType.
	 *
	 * @param adminTemplate The source SERFF PayloadType instance for Issuer bean.
	 * @return populated issuer Issuer Bean.
	 * @throws GIException
	 */
	public Issuer saveIssuerBean(EntityManager entityManager, com.serff.template.admin.extension.PayloadType adminTemplate,
			String issuerID, String stateCode, Map<String, String> documents, byte[] logoBytes) throws GIException {

		LOGGER.info("saveIssuerBean() Started");

		Issuer savedIssuer = null;
		Issuer issuer = null;
		List <Issuer> list = null;
		boolean issuerNotExistInDB = false;
		String adminID = null;

		if (null != adminTemplate) {

			if (null != adminTemplate.getIssuer().getIssuerIdentification()
					&& null != adminTemplate.getIssuer().getIssuerIdentification().getIdentificationID()) {
				adminID = adminTemplate.getIssuer().getIssuerIdentification().getIdentificationID().getValue();
				// Integer intAdminId =Integer.parseInt(adminID);
				//checking id HIOS ID allready exist or not.
				list = iIssuerRepository.getIssuerFromHiosID(adminID);

				if (null != list && !list.isEmpty()) {
					issuer = list.get(0);
					issuer.setHiosIssuerId(adminID);

					if (null == issuer.getCertificationStatus()) {
						issuer.setCertificationStatus(certification_status.PENDING.toString());
					}
				}
				else{
					issuerNotExistInDB = true;
				}
			}
		}
		else {
			LOGGER.info("Retrieving Existing Issuer["+ issuerID +"] from Database.");
			issuer = iIssuerRepository.getIssuerByHiosID(issuerID);
			adminID = issuerID;

			if (null == issuer) {
				issuerNotExistInDB = true;
			}
		}

		if (issuerNotExistInDB) {
			LOGGER.error("Issuer account does not exist. HIOS ID: " + adminID);				
			throw new GIException(Integer.parseInt(SerffConstants.INVALID_PARAMETER_CODE), SerffConstants.INVALID_PARAMETER_MESSAGE_ISSUER_NOT_SETUP + ". HIOS ID: " + adminID, StringUtils.EMPTY);
		}

		if (!CollectionUtils.isEmpty(documents)) {

			for(Entry<String, String> documentEntry : documents.entrySet()){
				if(documentEntry.getKey().toLowerCase().contains("logo")){
					issuer.setLogoURL(documentEntry.getValue());
				}
			}
		}
		
		if(null == issuer.getStateOfDomicile() && null != stateCode) {
			issuer.setStateOfDomicile(stateCode);
		}
		
		if (null != logoBytes) {
			issuer.setLogo(logoBytes);
		}

		if(null != adminTemplate) {

			String indvlMktFName = null, indvlMktSName = null, shopFName = null, shopSName = null;
			String ceoFName = null, ceoSName = null, cfoFName = null, cfoSName = null;

			if(null != adminTemplate.getIssuer()) {

				/*if(null != adminTemplate.getIssuer().getIssuerIdentification()
						&& null != adminTemplate.getIssuer().getIssuerIdentification().getIdentificationID()) {
					issuer.setHiosIssuerId(adminTemplate.getIssuer().getIssuerIdentification().getIdentificationID().getValue() !=null? Integer.valueOf(adminTemplate.getIssuer().getIssuerIdentification().getIdentificationID().getValue()):null);
				}
*/
				if(null != adminTemplate.getIssuer().getIssuerStateCode()) {
					issuer.setStateOfDomicile(adminTemplate.getIssuer().getIssuerStateCode().getValue()!=null?adminTemplate.getIssuer().getIssuerStateCode().getValue().toString() : null);
				}

				//9. Customer Service - Individual Market
				if(null != adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService()) {

					if(null != adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactMainTelephoneNumber()
							&& null != adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactMainTelephoneNumber().getFullTelephoneNumber()) {
						issuer.setIndvCustServicePhone(adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID() != null
								? adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().getValue() : null);
						issuer.setIndvCustServicePhoneExt(adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID() != null
								? adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID().getValue() : null);
					}

					if(null != adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactTTYTelephoneNumber()
							&& null != adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactTTYTelephoneNumber().getFullTelephoneNumber()) {
						issuer.setIndvCustServiceTTY(adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactTTYTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID() != null
								? adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactTTYTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().getValue() : null);
					}

					if(null != adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactTollFreeTelephoneNumber()
							&& null != adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactTollFreeTelephoneNumber().getFullTelephoneNumber()) {
						issuer.setIndvCustServiceTollFree(adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactTollFreeTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID() != null
								? adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactTollFreeTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().getValue() : null);
					}

					if(null != adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactWebsiteURI()) {
						issuer.setIndvSiteUrl(SerffUtils.checkAndcorrectURL(adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactWebsiteURI() != null
								? adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService().getContactWebsiteURI().getValue() : null));
					}
				}

				//10. Customer Service - Shop
				if(null != adminTemplate.getIssuer().getIssuerSHOPCustomerService()) {

					if(null != adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactMainTelephoneNumber()
							&& null != adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactMainTelephoneNumber().getFullTelephoneNumber()) {
						issuer.setShopCustServicePhone(adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID() != null
								? adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().getValue() : null);
						issuer.setShopCustServicePhoneExt(adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID() != null
								? adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID().getValue() : null);
					}

					if(null != adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactTTYTelephoneNumber()
							&& null != adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactTTYTelephoneNumber().getFullTelephoneNumber()) {
						issuer.setShopCustServiceTTY(adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactTTYTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID() != null
								? adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactTTYTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().getValue() : null);
					}

					if(null != adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactTollFreeTelephoneNumber()
							&& null != adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactTollFreeTelephoneNumber().getFullTelephoneNumber()) {
						issuer.setShopCustServiceTollFree(adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactTollFreeTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID() != null
								? adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactTollFreeTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().getValue() : null);
					}

					if(null != adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactWebsiteURI()) {
						issuer.setShopSiteUrl(SerffUtils.checkAndcorrectURL(adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactWebsiteURI() != null
								? adminTemplate.getIssuer().getIssuerSHOPCustomerService().getContactWebsiteURI().getValue() : null));
					}
				}

				//5. Issuer Individual Market Contact--OrgChart
				if(null != adminTemplate.getIssuer().getIssuerIndividualMarketContact()
						&& null != adminTemplate.getIssuer().getIssuerIndividualMarketContact().getPersonName()
						&& null != adminTemplate.getIssuer().getIssuerIndividualMarketContact().getPersonName()) {
		            indvlMktFName = adminTemplate.getIssuer().getIssuerIndividualMarketContact().getPersonName().getPersonGivenName() != null ?
		            		adminTemplate.getIssuer().getIssuerIndividualMarketContact().getPersonName().getPersonGivenName().getValue() : null;
		            indvlMktSName = adminTemplate.getIssuer().getIssuerIndividualMarketContact().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getIssuer().getIssuerIndividualMarketContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //6. Issuer SHOP (Small Group) Contact--ORG Chart
				if(null != adminTemplate.getIssuer().getIssuerSmallGroupMarketContact()
						&& null != adminTemplate.getIssuer().getIssuerSmallGroupMarketContact().getPersonName()
						&& null != adminTemplate.getIssuer().getIssuerSmallGroupMarketContact().getPersonName()) {
					shopFName = adminTemplate.getIssuer().getIssuerSmallGroupMarketContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getIssuer().getIssuerSmallGroupMarketContact().getPersonName().getPersonGivenName().getValue() : null;
	            	shopSName =  adminTemplate.getIssuer().getIssuerSmallGroupMarketContact().getPersonName().getPersonSurName() != null ?
	            			adminTemplate.getIssuer().getIssuerSmallGroupMarketContact().getPersonName().getPersonSurName().getValue() : null;
				}
			}

			if(null != adminTemplate.getInsuranceCompanyOrganization()) {

				if(null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation()) {
					LOGGER.debug("checking OrganizationLegalName");

					if(null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationLegalName()) {
						issuer.setCompanyLegalName(adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationLegalName().getValue()!=null?adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationLegalName().getValue():null);
					}

					/*if(null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationMarketingName()) {
						issuer.setMarketingName(adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationMarketingName().getValue()!=null?adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationMarketingName().getValue():null);
					}*/
					}

				// Company Address
				if(null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation()
						&& null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress()
						&& null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress()) {

					if(null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet()) {
						issuer.setCompanyAddressLine1(adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetFullText()!=null?
								adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetFullText().getValue():null);
						issuer.setCompanyAddressLine2(adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetExtensionText()!=null?
								adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetExtensionText().getValue():null);
					}

					issuer.setCompanyCity(adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationCityName()!=null?
							adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationCityName().getValue():null);
					issuer.setCompanyState(adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStateUSPostalServiceCode()!=null?
							adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStateUSPostalServiceCode().getValue().toString():null);
					/*issuer.setCompanyZip(adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationPostalCode()!=null?
							adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationPostalCode().getValue().toString():null);*/

					if (adminTemplate.getInsuranceCompanyOrganization()
							.getOrganizationLocation().getLocationAddress()
							.getStructuredAddress().getLocationPostalCode() != null) {
						String zip = adminTemplate
								.getInsuranceCompanyOrganization()
								.getOrganizationLocation().getLocationAddress()
								.getStructuredAddress().getLocationPostalCode()
								.getValue();
						if (StringUtils.isNotBlank(zip) && zip.contains(SerffConstants.HYPHEN)) {
							zip = zip.replaceAll(SerffConstants.HYPHEN, StringUtils.EMPTY);
						}
						issuer.setCompanyZip(zip);

					} else {
						issuer.setCompanyZip(null);
					}

				}
	            //7. CEO
				if(null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationCEO()
						&& null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationCEO().getPersonName()) {
					ceoFName = adminTemplate.getInsuranceCompanyOrganization().getOrganizationCEO().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompanyOrganization().getOrganizationCEO().getPersonName().getPersonGivenName().getValue() : null;
					ceoSName = adminTemplate.getInsuranceCompanyOrganization().getOrganizationCEO().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompanyOrganization().getOrganizationCEO().getPersonName().getPersonSurName().getValue() : null;
				}

				//8. CFO
				if(null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationCFO()
						&& null != adminTemplate.getInsuranceCompanyOrganization().getOrganizationCFO().getPersonName()) {
		            cfoFName = adminTemplate.getInsuranceCompanyOrganization().getOrganizationCFO().getPersonName().getPersonGivenName() != null ?
		            		adminTemplate.getInsuranceCompanyOrganization().getOrganizationCFO().getPersonName().getPersonGivenName().getValue() : null;
		            cfoSName = adminTemplate.getInsuranceCompanyOrganization().getOrganizationCFO().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getInsuranceCompanyOrganization().getOrganizationCFO().getPersonName().getPersonSurName().getValue() : null;
				}
			}

			String enrollmentFName = null, enrollmentSName = null, onlineEnrollmentFName = null, onlineEnrollmentSName = null, onlineSecEnrollmentFName = null, onlineSecEnrollmentSName = null;
			String insCompSystemFName = null, insCompSystemSName = null, appealOrGrievanceFName = null, appealOrGrievanceSName = null;
			String custSerOpFname = null, custSerOpSname = null;
			String usrAccessFname = null, usrAccessSname = null, usrSecAccessFname = null, usrSecAccessSname = null;
			String chfDentalFname = null, chfDentalSname = null;
			String mktFname = null, mktSname = null;
			String medDirectorFname = null, medDirectorSname = null;
			String benefitMgrFname = null, benefitMgrSname = null;
			String govFname = null, govSname = null;
			String hipaaSecurityFname = null, hipaaSecuritySname = null;
			String compTracFname = null, compTracSname = null;
			String qualityFname = null, qualitySname = null;
			String complOffcrFName = null, complOffcrSName = null;
			String paymentFName = null, paymentSName = null;
			String aptccsrFName = null, aptccsrSName = null;
			String reportFName = null, reportSName = null, transferFName = null, transferSName = null;
			String corridorsFName = null, corridorsSName = null, adjustmentFName = null, adjustmentSName = null;
			String reinsuranceFName = null, reinsuranceSName = null;

			if(null != adminTemplate.getInsuranceCompany()) {

				issuer.setFederalEin(adminTemplate.getInsuranceCompany().getInsuranceCompanyTINID()!=null?adminTemplate.getInsuranceCompany().getInsuranceCompanyTINID().getValue().toString():null);
				
				if (StringUtils.isBlank(issuer.getNaicCompanyCode())
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyNAICID()) {
					issuer.setNaicCompanyCode(StringUtils.isNotBlank(adminTemplate.getInsuranceCompany().getInsuranceCompanyNAICID().getValue())?adminTemplate.getInsuranceCompany().getInsuranceCompanyNAICID().getValue():null);
				}
				
				if (StringUtils.isBlank(issuer.getNaicGroupCode())
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyNAICGroupID()) {
					issuer.setNaicGroupCode(StringUtils.isNotBlank(adminTemplate.getInsuranceCompany().getInsuranceCompanyNAICGroupID().getValue())?adminTemplate.getInsuranceCompany().getInsuranceCompanyNAICGroupID().getValue():null);
				}
	            //Enrollment Online
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyEnrollmentContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyEnrollmentContact().getPersonName()) {
					enrollmentFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyEnrollmentContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyEnrollmentContact().getPersonName().getPersonGivenName().getValue() : null;
					enrollmentSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyEnrollmentContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyEnrollmentContact().getPersonName().getPersonSurName().getValue() : null;
				}

				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyPrimaryOnlineEnrollmentCenterContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyPrimaryOnlineEnrollmentCenterContact().getPersonName()) {
					onlineEnrollmentFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyPrimaryOnlineEnrollmentCenterContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyPrimaryOnlineEnrollmentCenterContact().getPersonName().getPersonGivenName().getValue() : null;
					onlineEnrollmentSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyPrimaryOnlineEnrollmentCenterContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyPrimaryOnlineEnrollmentCenterContact().getPersonName().getPersonSurName().getValue() : null;
				}

				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanySecondaryOnlineEnrollmentCenterContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanySecondaryOnlineEnrollmentCenterContact().getPersonName()) {
					onlineSecEnrollmentFName = adminTemplate.getInsuranceCompany().getInsuranceCompanySecondaryOnlineEnrollmentCenterContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanySecondaryOnlineEnrollmentCenterContact().getPersonName().getPersonGivenName().getValue() : null;
					onlineSecEnrollmentSName = adminTemplate.getInsuranceCompany().getInsuranceCompanySecondaryOnlineEnrollmentCenterContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanySecondaryOnlineEnrollmentCenterContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //System Contact
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanySystemContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanySystemContact().getPersonName()) {
					insCompSystemFName = adminTemplate.getInsuranceCompany().getInsuranceCompanySystemContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanySystemContact().getPersonName().getPersonGivenName().getValue() : null;
					insCompSystemSName = adminTemplate.getInsuranceCompany().getInsuranceCompanySystemContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanySystemContact().getPersonName().getPersonSurName().getValue() : null;
				}

				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyAppealOrGrievanceContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyAppealOrGrievanceContact().getPersonName()) {
					appealOrGrievanceFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyAppealOrGrievanceContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyAppealOrGrievanceContact().getPersonName().getPersonGivenName().getValue() : null;
					appealOrGrievanceSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyAppealOrGrievanceContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyAppealOrGrievanceContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //CustomerServiceOperations
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyCustomerServiceOperationsContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyCustomerServiceOperationsContact().getPersonName()) {
					custSerOpFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyCustomerServiceOperationsContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyCustomerServiceOperationsContact().getPersonName().getPersonGivenName().getValue() : null;
					custSerOpSname = adminTemplate.getInsuranceCompany().getInsuranceCompanyCustomerServiceOperationsContact().getPersonName().getPersonSurName().getValue();
				}

	            //UserAccess
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessPrimaryContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessPrimaryContact().getPersonName()) {
		            usrAccessFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessPrimaryContact().getPersonName().getPersonGivenName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessPrimaryContact().getPersonName().getPersonGivenName().getValue() : null;
		            usrAccessSname = adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessPrimaryContact().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessPrimaryContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //UserAccess
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessSecondaryContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessSecondaryContact().getPersonName()) {
		            usrSecAccessFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessSecondaryContact().getPersonName().getPersonGivenName() !=  null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessSecondaryContact().getPersonName().getPersonGivenName().getValue() : null;
		            usrSecAccessSname = adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessSecondaryContact().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyUserAccessSecondaryContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //ChiefDentalDirectorContact
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyChiefDentalDirectorContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyChiefDentalDirectorContact().getPersonName()) {
					chfDentalFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyChiefDentalDirectorContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyChiefDentalDirectorContact().getPersonName().getPersonGivenName().getValue() : null;
					chfDentalSname = adminTemplate.getInsuranceCompany().getInsuranceCompanyChiefDentalDirectorContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyChiefDentalDirectorContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //MarketingContact
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyMarketingContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyMarketingContact().getPersonName()) {
					mktFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyMarketingContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyMarketingContact().getPersonName().getPersonGivenName().getValue() : null;
					mktSname = adminTemplate.getInsuranceCompany().getInsuranceCompanyMarketingContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyMarketingContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //MedicalDirector
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyMedicalDirectorContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyMedicalDirectorContact().getPersonName()) {
		            medDirectorFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyMedicalDirectorContact().getPersonName().getPersonGivenName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyMedicalDirectorContact().getPersonName().getPersonGivenName().getValue() : null;
		            medDirectorSname = adminTemplate.getInsuranceCompany().getInsuranceCompanyMedicalDirectorContact().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyMedicalDirectorContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //PharmacyBenefitManagerContact
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyPharmacyBenefitManagerContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyPharmacyBenefitManagerContact().getPersonName()) {
		            benefitMgrFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyPharmacyBenefitManagerContact().getPersonName().getPersonGivenName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyPharmacyBenefitManagerContact().getPersonName().getPersonGivenName().getValue() : null;
		            benefitMgrSname = adminTemplate.getInsuranceCompany().getInsuranceCompanyPharmacyBenefitManagerContact().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyPharmacyBenefitManagerContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //GovernmentRelations
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyGovernmentRelationsContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyGovernmentRelationsContact().getPersonName()) {
		            govFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyGovernmentRelationsContact().getPersonName().getPersonGivenName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyGovernmentRelationsContact().getPersonName().getPersonGivenName().getValue() : null;
		            govSname = adminTemplate.getInsuranceCompany().getInsuranceCompanyGovernmentRelationsContact().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyGovernmentRelationsContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //HIPAASecurityOfficer
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyHIPAASecurityOfficerContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyHIPAASecurityOfficerContact().getPersonName()) {
					hipaaSecurityFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyHIPAASecurityOfficerContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyHIPAASecurityOfficerContact().getPersonName().getPersonGivenName().getValue() : null;
					hipaaSecuritySname = adminTemplate.getInsuranceCompany().getInsuranceCompanyHIPAASecurityOfficerContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyHIPAASecurityOfficerContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //ComplaintsTracking
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyComplaintTrackingContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyComplaintTrackingContact().getPersonName()) {
					compTracFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyComplaintTrackingContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyComplaintTrackingContact().getPersonName().getPersonGivenName().getValue() : null;
					compTracSname = adminTemplate.getInsuranceCompany().getInsuranceCompanyComplaintTrackingContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyComplaintTrackingContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //Quality
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyQualityContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyQualityContact().getPersonName()) {
					qualityFname = adminTemplate.getInsuranceCompany().getInsuranceCompanyQualityContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyQualityContact().getPersonName().getPersonGivenName().getValue() : null;
					qualitySname = adminTemplate.getInsuranceCompany().getInsuranceCompanyQualityContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyQualityContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //ComplianceOfficer
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyComplianceOfficerContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyComplianceOfficerContact().getPersonName()) {
					complOffcrFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyComplianceOfficerContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyComplianceOfficerContact().getPersonName().getPersonGivenName().getValue() : null;
					complOffcrSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyComplianceOfficerContact().getPersonName().getPersonSurName().getValue();
				}

	            //Payment
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyPaymentContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyPaymentContact().getPersonName()) {
					paymentFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyPaymentContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyPaymentContact().getPersonName().getPersonGivenName().getValue() : null;
					paymentSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyPaymentContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyPaymentContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //APTC_CSR
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyAptcCsrContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyAptcCsrContact().getPersonName()) {
					aptccsrFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyAptcCsrContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyAptcCsrContact().getPersonName().getPersonGivenName().getValue() : null;
					aptccsrSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyAptcCsrContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyAptcCsrContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //Financial-reporting & transfer
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialReportingContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialReportingContact().getPersonName()) {
		            reportFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialReportingContact().getPersonName().getPersonGivenName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialReportingContact().getPersonName().getPersonGivenName().getValue() : null;
		            reportSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialReportingContact().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialReportingContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //Financial-reporting & transfer
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialTransfersContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialTransfersContact().getPersonName()) {
		            transferFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialTransfersContact().getPersonName().getPersonGivenName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialTransfersContact().getPersonName().getPersonGivenName().getValue() : null;
		            transferSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialTransfersContact().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyFinancialTransfersContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //Risk-Corridors & Adjustment
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskCorridorsContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskCorridorsContact().getPersonName()) {
		            corridorsFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskCorridorsContact().getPersonName().getPersonGivenName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskCorridorsContact().getPersonName().getPersonGivenName().getValue() : null;
		            corridorsSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskCorridorsContact().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskCorridorsContact().getPersonName().getPersonSurName().getValue() : null;
				}

	            //Risk-Corridors & Adjustment
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskAdjustmentContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskAdjustmentContact().getPersonName()) {
					adjustmentFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskAdjustmentContact().getPersonName().getPersonGivenName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskAdjustmentContact().getPersonName().getPersonGivenName().getValue() : null;
		            adjustmentSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskAdjustmentContact().getPersonName().getPersonSurName() != null ?
		            		adminTemplate.getInsuranceCompany().getInsuranceCompanyRiskAdjustmentContact().getPersonName().getPersonSurName().getValue() : null;

				}

	            //Reinsurance
				if(null != adminTemplate.getInsuranceCompany().getInsuranceCompanyReinsuranceContact()
						&& null != adminTemplate.getInsuranceCompany().getInsuranceCompanyReinsuranceContact().getPersonName()) {
					reinsuranceFName = adminTemplate.getInsuranceCompany().getInsuranceCompanyReinsuranceContact().getPersonName().getPersonGivenName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyReinsuranceContact().getPersonName().getPersonGivenName().getValue() : null;
					reinsuranceSName = adminTemplate.getInsuranceCompany().getInsuranceCompanyReinsuranceContact().getPersonName().getPersonSurName() != null ?
							adminTemplate.getInsuranceCompany().getInsuranceCompanyReinsuranceContact().getPersonName().getPersonSurName().getValue() : null;
				}
			}

			// IssuerAddress
			if(null != adminTemplate.getIssuerOrganization()
					&& null != adminTemplate.getIssuerOrganization().getOrganizationLocation()
					&& null != adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress()
					&& null != adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress()) {

				if(null != adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet()) {
					issuer.setAddressLine1(adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetFullText()!=null?
							adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetFullText().getValue():null);
					issuer.setAddressLine2(adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetExtensionText()!=null?
							adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetExtensionText().getValue():null);
				}
				issuer.setCity(adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationCityName()!=null?
						adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationCityName().getValue():null);
				issuer.setState(adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStateUSPostalServiceCode()!=null?
						adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStateUSPostalServiceCode().getValue().toString():null);
				/*issuer.setZip(adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationPostalCode()!=null?
						adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationPostalCode().getValue().toString():null);*/
				if (adminTemplate.getIssuerOrganization()
						.getOrganizationLocation().getLocationAddress()
						.getStructuredAddress().getLocationPostalCode() != null) {

					String zip = adminTemplate.getIssuerOrganization()
							.getOrganizationLocation().getLocationAddress()
							.getStructuredAddress().getLocationPostalCode()
							.getValue();
					if (StringUtils.isNotBlank(zip) && zip.contains(SerffConstants.HYPHEN)) {
						zip = zip.replaceAll(SerffConstants.HYPHEN, StringUtils.EMPTY);
					}
					issuer.setZip(zip);

				} else {
					issuer.setZip(null);
				}

			}
			if(null != adminTemplate.getIssuerOrganization()&& null!= adminTemplate.getIssuerOrganization().getOrganizationAugmentation()&&
					null != adminTemplate.getIssuerOrganization().getOrganizationAugmentation().getOrganizationMarketingName()){
				issuer.setMarketingName(adminTemplate.getIssuerOrganization().getOrganizationAugmentation().getOrganizationMarketingName().getValue());
			}
			
			StringBuffer contactsTemplate = new StringBuffer();			
			contactsTemplate.append(CONTACT_NODE_START)
							.append(ISSUER_NODE_START)
							.append(createXMLNode(NODE_NAME_INDIVIDUAL_MARKET, indvlMktFName, indvlMktSName))
							.append(createXMLNode(NODE_NAME_SHOP, shopFName, shopSName))
							.append(ISSUER_NODE_END)
							.append(createXMLNode(NODE_NAME_CEO, ceoFName, ceoSName))
							.append(createXMLNode(NODE_NAME_CFO, cfoFName, cfoSName))
							.append(INSURANCE_COMPANY_NODE_START)
							.append(createXMLNode(NODE_NAME_ENROLLMENT, enrollmentFName, enrollmentSName))
							.append(ONLINE_NODE_START)
							.append(ENROLLMENTCENTER_NODE_START)
							.append(createXMLNode(NODE_NAME_PRIMARY, onlineEnrollmentFName, onlineEnrollmentSName))
							.append(createXMLNode(NODE_NAME_BACKUP, onlineSecEnrollmentFName, onlineSecEnrollmentSName))
							.append(ENROLLMENTCENTER_NODE_END)
							.append(ONLINE_NODE_END)
							.append(createXMLNode(NODE_NAME_SYSTEM, insCompSystemFName, insCompSystemSName))
							.append(createXMLNode(NODE_NAME_APPGRIEVANCE, appealOrGrievanceFName, appealOrGrievanceSName))
							.append(createXMLNode(NODE_NAME_CUSTOMER_SERVICE_OPERATIONS, custSerOpFname, custSerOpSname))
							.append(USERACCESS_NODE_START)
							.append(createXMLNode(NODE_NAME_PRIMARY, usrAccessFname, usrAccessSname))
							.append(createXMLNode(NODE_NAME_BACKUP, usrSecAccessFname, usrSecAccessSname))
							.append(USERACCESS_NODE_END)
							.append(createXMLNode(NODE_NAME_MARKETING, mktFname, mktSname))
							.append(createXMLNode(NODE_NAME_MEDICAL_DIRECTOR, medDirectorFname, medDirectorSname))
							.append(createXMLNode(NODE_NAME_CHIEF_DENTAL_DIRECTOR, chfDentalFname, chfDentalSname))
							.append(createXMLNode(NODE_NAME_PHARMACY_BENEFIT_MANAGER, benefitMgrFname, benefitMgrSname))
							.append(createXMLNode(NODE_NAME_GOVERNMENT_RELATIONS, govFname, govSname))
							.append(createXMLNode(NODE_NAME_HIPPA_SECURITY_OFFICER, hipaaSecurityFname, hipaaSecuritySname))
							.append(createXMLNode(NODE_NAME_COMPLAINTS_TRACKING, compTracFname, compTracSname))
							.append(createXMLNode(NODE_NAME_QUALITY, qualityFname, qualitySname))
							.append(createXMLNode(NODE_NAME_COMPLIANCE_OFFICER, complOffcrFName, complOffcrSName))
							.append(createXMLNode(NODE_NAME_PAYMENT, paymentFName, paymentSName))
							.append(createXMLNode(NODE_NAME_APTC_CSR, aptccsrFName, aptccsrSName))
							.append(FINANCIAL_NODE_START)
							.append(createXMLNode(NODE_NAME_REPORTING, reportFName, reportSName))
							.append(createXMLNode(NODE_NAME_TRANSFER, transferFName, transferSName))
							.append(FINANCIAL_NODE_END)
							.append(RISK_NODE_START)
							.append(createXMLNode(NODE_NAME_CORRIDORS, corridorsFName, corridorsSName))
							.append(createXMLNode(NODE_NAME_ADJUSTMENT, adjustmentFName, adjustmentSName))
							.append(RISK_NODE_END)
							.append(createXMLNode(NODE_NAME_REINSURANCE, reinsuranceFName, reinsuranceSName))
							.append(INSURANCE_COMPANY_NODE_END)
							.append(CONTACT_NODE_END);

		byte[] contactXMLStringBLOB = contactsTemplate.toString().getBytes();
		issuer.setOrgChart(contactXMLStringBLOB);
		issuer.setTxn834Version("X220A1");
		issuer.setTxn820Version("X220A33");
//		boolean  issuerProcessed = Boolean.FALSE;

//		if(!issuerProcessed) {
			/*LOGGER.info("Account User -> GhixConstants.PLAN_MGMT_USER: " + GhixConstants.PLAN_MGMT_USER);
			AccountUser accountUser = userService.findByUserName(GhixConstants.PLAN_MGMT_USER);
			
			if (null == accountUser) {
				throw new GIException("Account User ("+ GhixConstants.PLAN_MGMT_USER +") does not exist.");
			}
			
			UsernamePasswordAuthenticationToken userNamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(accountUser.getUserName(), null);
			LOGGER.info("UserName Password Authentication Done");

			userNamePasswordAuthenticationToken.setDetails(accountUser);
			LOGGER.info("UserName Password Authentication Set Details Completed");

			SecurityContextHolder.getContext().setAuthentication(userNamePasswordAuthenticationToken);

			LOGGER.info("UserName Password Authentication Token Authenticated");
			issuer.setLastUpdatedBy(accountUser.getId());*/
//			}
		}
//		issuer.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);
		if (null != entityManager && entityManager.isOpen()) {
			savedIssuer = entityManager.merge(issuer);
			LOGGER.info("saveIssuerBean() Completed");
			LOGGER.info("Issuer bean has been Persisted Successfully .");
		}
		return savedIssuer;
	}

	/**
	 * This method is used to persist URAC template
	 * @param uracInfo
	 * @param fetchedIssuer
	 * @param entityManager
	 */
	public void populateAndPersistURACTemplate( com.serff.template.urac.IssuerType uracInfo, Issuer fetchedIssuer,
			EntityManager entityManager) {
		
		List<JAXBElement<? extends AccreditationType>> uracAccreditationList = null;
		if(null!=uracInfo && null!=fetchedIssuer ){
			
			IssuerAccreditation issuerAccreditation = null;
			List<IssuerAccreditation> issuerAccreditationList = null;
			uracAccreditationList = uracInfo.getAccreditation();
			int issuerId = fetchedIssuer.getId();
			
			for(JAXBElement<? extends AccreditationType> uracAccretitation1 : uracAccreditationList){
				AccreditationType uracAccretitation  = uracAccretitation1.getValue();
				
				String uracAppNo = null;
				String marketType = null;
				String accStatus = null;
				Date expirationDate = null;
				
				uracAppNo =  getValidURACAppNumber(uracAccretitation); 
				if(null!= uracAccretitation.getProductLineCode()&& StringUtils.isNotBlank(uracAccretitation.getProductLineCode().value())){
					marketType = uracAccretitation.getProductLineCode().value();
				}
				
				if(null!=uracAccretitation.getAccreditationStatusCode() ){
					accStatus = uracAccretitation.getAccreditationStatusCode().value();
				}
				expirationDate = getValidURACExpirationDate(uracAccretitation);
				LOGGER.debug("fetching exixting URAC data");
				issuerAccreditationList = iIssuerAccreditation.findByUracAppNumAndMarketTypeAndIssuer_Id(uracAppNo , marketType, issuerId);

				if(CollectionUtils.isEmpty(issuerAccreditationList)) {
					issuerAccreditation = new IssuerAccreditation();
					issuerAccreditation.setCreatedBy(fetchedIssuer.getLastUpdatedBy());
				
				}else{
					issuerAccreditation = issuerAccreditationList.get(0);
				}
				issuerAccreditation.setUracAppNum(uracAppNo);
				issuerAccreditation.setAccreditationStatus(accStatus);
				issuerAccreditation.setExpirationDate(expirationDate);
				issuerAccreditation.setMarketType(marketType);
				issuerAccreditation.setLastUpdatedBy(fetchedIssuer.getLastUpdatedBy());
				issuerAccreditation.setIssuer(fetchedIssuer);

				if (null != entityManager && entityManager.isOpen()) {	
					entityManager.merge(issuerAccreditation);
					LOGGER.info("URAC bean has been Persisted Successfully.");
					}
				}
			} 
		}

	

	private Date getValidURACExpirationDate(AccreditationType uracAccretitation) {
		boolean isValid = false ;
		Date expirationDate = null;
		if(null!=uracAccretitation.getAccreditationIdentification() && null!=uracAccretitation.getAccreditationIdentification().getIdentificationExpirationDate()){
			isValid = true;
		}
		if(isValid && null!=uracAccretitation.getAccreditationIdentification().getIdentificationExpirationDate().getDate() && 
				null!=uracAccretitation.getAccreditationIdentification().getIdentificationExpirationDate().getDate().getValue() ){
			expirationDate = uracAccretitation.getAccreditationIdentification().getIdentificationExpirationDate().getDate().getValue().toGregorianCalendar().getTime();
		}
		return expirationDate;
	}

	private String getValidURACAppNumber(AccreditationType uracAccretitation) {
		boolean isValid = false ;
		String uracAppNo = null;
		if(null!= uracAccretitation.getAccreditationIdentification() && null!= uracAccretitation.getAccreditationIdentification().getIdentificationID()){
			isValid = true;
		}
		if(isValid && StringUtils.isNotBlank(uracAccretitation.getAccreditationIdentification().getIdentificationID().getValue())){
			uracAppNo = uracAccretitation.getAccreditationIdentification().getIdentificationID().getValue();
		}
		return uracAppNo;
	}

	
	/**
	 * 
	 * @param nCQAAccreditationType
	 * @return
	 */
	private String getValidNcqaOrgId(NCQAAccreditationType nCQAAccreditationType){
		
		boolean isValid = false;
		String ncqaOrgId = null;
		
		if(null!=nCQAAccreditationType.getAccreditationIdentification() && null!= nCQAAccreditationType.getAccreditationIdentification().getIdentificationID()){
			isValid = true;
		}
		if(isValid && StringUtils.isNotBlank(nCQAAccreditationType.getAccreditationIdentification().getIdentificationID().getValue())){
			ncqaOrgId = nCQAAccreditationType.getAccreditationIdentification().getIdentificationID().getValue();
		}
		
		return ncqaOrgId;
	}
	

	/**
	 * This method checks for ncqaSubId validity 
	 * @param ncqaAccreditation
	 * @return
	 */
	private String getValidNcqaSubId(NCQAAccreditationType ncqaAccreditation){
		
		boolean isValid = false;
		String ncqaSubId = null;
		
		if(null!=ncqaAccreditation.getAccreditationSubmission() && null!= ncqaAccreditation.getAccreditationSubmission().getSubmissionIdentification()){
			isValid = true;
		}
		if(isValid && null!= ncqaAccreditation.getAccreditationSubmission().getSubmissionIdentification().getIdentificationID()&& 
				StringUtils.isNotBlank(ncqaAccreditation.getAccreditationSubmission().getSubmissionIdentification().getIdentificationID().getValue()
				)){
			ncqaSubId = ncqaAccreditation.getAccreditationSubmission().getSubmissionIdentification().getIdentificationID().getValue();
		}
		
		return ncqaSubId;
	}
	
	
	
	/**This method is used to persist NCQA template
	 * 
	 * @param ncqaInfo
	 * @param fetchedIssuer
	 * @param entityManager
	 */

	public void populateAndPersistNcqaTemplate(com.serff.template.ncqa.IssuerType  ncqaInfo,Issuer fetchedIssuer ,EntityManager entityManager) {
		
		IssuerAccreditation issuerAccreditation = null;
		List<IssuerAccreditation> issuerAccreditationList = null;
	
		List<NCQAAccreditationType> ncqaAccreditationList = null;
		
		if(null!=ncqaInfo &&  null!= fetchedIssuer ){
			ncqaAccreditationList = ncqaInfo.getNCQAAccreditation();
			int issuerId = fetchedIssuer.getId();
			
			for(NCQAAccreditationType ncqaAccreditation : ncqaAccreditationList ){
				String ncqaOrgId =null;
				String ncqaSubId =null;
				String marketCategory =null;
				String productId = null;
				String accStatus = null;
				String productCatogery = null;
				Date expirationDate = null;
				
				if(null!=ncqaAccreditation ){
					
					ncqaOrgId = getValidNcqaOrgId(ncqaAccreditation);
					ncqaSubId= getValidNcqaSubId(ncqaAccreditation);
					marketCategory = getValidMarketCategory(ncqaAccreditation);
					productId = getValidProductId(ncqaAccreditation);
					accStatus = getValidAccStatus(ncqaAccreditation);
					productCatogery = getValidProductCategory(ncqaAccreditation);
					expirationDate = getValidNcqaExpirationDate(ncqaAccreditation);
					LOGGER.debug("fetching exixting NCQA data");
					issuerAccreditationList = iIssuerAccreditation.findByNcqaOrgIdAndNcqaSubIdAndMarketTypeAndProductIdAndIssuer_Id(ncqaOrgId , ncqaSubId ,marketCategory,productId,issuerId  );
				
					if(CollectionUtils .isEmpty(issuerAccreditationList)) {
						issuerAccreditation = new IssuerAccreditation();
						issuerAccreditation.setCreatedBy(fetchedIssuer.getLastUpdatedBy());
					}else{
						issuerAccreditation = issuerAccreditationList.get(0);
					}
					issuerAccreditation.setExpirationDate(expirationDate);
					issuerAccreditation.setNcqaSubId(ncqaSubId);
					issuerAccreditation.setLastUpdatedBy(fetchedIssuer.getLastUpdatedBy());
					issuerAccreditation.setProductCategory(productCatogery);
					issuerAccreditation.setProductId(productId);
					issuerAccreditation.setMarketType(marketCategory);
					issuerAccreditation.setNcqaOrgId(ncqaOrgId);
					issuerAccreditation.setIssuer(fetchedIssuer);
					issuerAccreditation.setAccreditationStatus(accStatus);
				}

				if (null != entityManager && entityManager.isOpen()) {
					entityManager.merge(issuerAccreditation);
					LOGGER.info("NCQA bean has been Persisted Successfully.");
				}
			}
	
		}
		
	}
	
	/**
	 * This method checks for ncqaExpiration validity 
	 * @param ncqaAccreditation
	 * @return
	 */
	private Date getValidNcqaExpirationDate(NCQAAccreditationType ncqaAccreditation) {
		boolean isValid = false;
		Date ncqaExpirationDate = null ;
		
		if(null!=ncqaAccreditation.getAccreditationIdentification() && null!= ncqaAccreditation.getAccreditationIdentification().getIdentificationExpirationDate()){
			isValid = true;
		}
		
		if(isValid && null!=ncqaAccreditation.getAccreditationIdentification().getIdentificationExpirationDate().getDate() && 
				null!= ncqaAccreditation.getAccreditationIdentification().getIdentificationExpirationDate().getDate().getValue() ){
			ncqaExpirationDate = ncqaAccreditation.getAccreditationIdentification().getIdentificationExpirationDate().getDate().getValue().toGregorianCalendar().getTime();
		}
		return ncqaExpirationDate;
	}
	
	/**
	 * This method checks for productCategory validity 
	 * @param ncqaAccreditation
	 * @return
	 */
	private String getValidProductCategory(NCQAAccreditationType ncqaAccreditation) {
		
		String productCategory = null;
		boolean isValid = false;
		if(null!=ncqaAccreditation.getInsuranceProductCategoryCode() ){
			isValid  = true;
		}
		if(isValid && null!= ncqaAccreditation.getInsuranceProductCategoryCode().value() && StringUtils.isNotBlank(ncqaAccreditation.getInsuranceProductCategoryCode().value())){
			productCategory = ncqaAccreditation.getInsuranceProductCategoryCode().value();
		}
		return productCategory;
	}
	
	/**
	 * This method checks for accreditationStatus validity 
	 * @param ncqaAccreditation
	 * @return
	 */
	private String getValidAccStatus(NCQAAccreditationType ncqaAccreditation) {
		boolean isValid = false;
		String accStatus = null;
		
		if(null!= ncqaAccreditation.getAccreditationStatusCode()&&null!= ncqaAccreditation.getAccreditationStatusCode().getValue() ){
			isValid = true;
		}
		
		if(isValid && null!=ncqaAccreditation.getAccreditationStatusCode().getValue().value()){
			accStatus = ncqaAccreditation.getAccreditationStatusCode().getValue().value();
		}
		return accStatus;
	}
	
	/**
	 * This method checks for productID validity 
	 * @param ncqaAccreditation
	 * @return
	 */
	private String getValidProductId(NCQAAccreditationType ncqaAccreditation) {
		boolean isValid = false;
		String productId = null;
		
		if(null!=  ncqaAccreditation.getInsuranceProductSpecification() && null!= ncqaAccreditation.getInsuranceProductSpecification().getInsuranceProductIdentification()){
			isValid = true;
		}else{
			isValid = false;
		}
		
		if(isValid && null!= ncqaAccreditation.getInsuranceProductSpecification().getInsuranceProductIdentification().getIdentificationID()
				&& StringUtils.isNotBlank(ncqaAccreditation.getInsuranceProductSpecification().getInsuranceProductIdentification().getIdentificationID().getValue())){
			productId = ncqaAccreditation.getInsuranceProductSpecification().getInsuranceProductIdentification().getIdentificationID().getValue();
		}
		return productId;
	}
	
	/**
	 * This method checks for marketCatogory validity 
	 * @param ncqaAccreditation
	 * @return
	 */
	private String getValidMarketCategory(NCQAAccreditationType ncqaAccreditation) {
		boolean isValid = false;
		String marketType = null;
		
		if(null!= ncqaAccreditation.getInsuranceProductSpecification() && null!= ncqaAccreditation.getInsuranceProductSpecification().getMarketCategoryCode()){
			isValid = true;
		}
		
		if(isValid && null!=ncqaAccreditation.getInsuranceProductSpecification().getMarketCategoryCode().getValue()
				&& StringUtils.isNotBlank(ncqaAccreditation.getInsuranceProductSpecification().getMarketCategoryCode().getValue().value())){
			marketType = ncqaAccreditation.getInsuranceProductSpecification().getMarketCategoryCode().getValue().value();
		}
		
		return marketType;
	}

	/**
	 * Method to populate and save PlanHealthRate from SERFF rates.
	 *
	 * @param rates Template The source SERFF PayloadType instance for Issuer bean.
	 * @return populated issuer Issuer Bean.
	 */

	public Map<String, List<PlanRate>> savePlanRateBean(EntityManager entityManager, String stateCode,
			com.serff.template.plan.QhpApplicationRateGroupVO rates)
			throws GIException {
		LOGGER.info("processing of PlanRateBean() starts ");

		Calendar calendar = Calendar.getInstance();
		Map<String, List<PlanRate>> planRates = new HashMap<String, List<PlanRate>>();
		List<PlanRate> ratesList = new ArrayList<PlanRate>();
		List<QhpApplicationRateItemVO> items = rates.getItems();
		final String rateFamilyOption = DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.RV_RATE_FAMILY_OPTION);

		//Collecting rate area ids for all rating area
		Map<String, RatingArea> ratingAreaIds = new HashMap<String, RatingArea>();
		if(StringUtils.isNotEmpty(stateCode)){
			List<RatingArea> ratingAreas = iRatingAreaRepository.findByState(stateCode);
			LOGGER.info("RatingArea List: " + ratingAreas);

			if(null != ratingAreas
					&& !ratingAreas.isEmpty()){
				for(RatingArea ratingArea : ratingAreas){
					ratingAreaIds.put(ratingArea.getRatingArea(), ratingArea);
				}
			}
			else {
				LOGGER.error("savePlanRateBean() : Rating Area is empty for state code.");
				throw new GIException(Integer.parseInt(SerffConstants.INVALID_PARAMETER_CODE), SerffConstants.INVALID_PARAMETER_MESSAGE_INVALID_CHARACTERS + " : Rating Area is empty for state code : State code : " + stateCode , StringUtils.EMPTY);
			}
		}

		for (QhpApplicationRateItemVO item : items) {

			PlanRate planRate = new PlanRate();
			planRate.setIsDeleted(PlanRate.IS_DELETED.N.toString());

			if (null != item && null != item.getEffectiveDate()
					&& null != item.getEffectiveDate().getCellValue()) {
				String date = item.getEffectiveDate().getCellValue();
				DateFormat df = new SimpleDateFormat(DATE_FORMAT);
				Date startDate = null;
				try {
					startDate = df.parse(date);
				} catch (ParseException e) {
					LOGGER.error(" : savePlanRateBean() : Exception occured while parsing date : " + date);
					throw new GIException(Integer.parseInt(SerffConstants.INVALID_PARAMETER_CODE), SerffConstants.INVALID_PARAMETER_MESSAGE_PARSING_REQUEST + " :  Exception while parsing date : " + date, StringUtils.EMPTY);
				}
				planRate.setEffectiveStartDate(startDate);
			}
			if (null != item && null != item.getExpirationDate()
					&& null != item.getExpirationDate().getCellValue()) {
				String date = item.getExpirationDate().getCellValue();
				DateFormat df = new SimpleDateFormat(DATE_FORMAT);
				Date endDate;
				try {
					endDate = df.parse(date);
				} catch (ParseException e) {
					LOGGER.error(" : savePlanRateBean() : Exception while parsing date: " + date);
					throw new GIException(Integer.parseInt(SerffConstants.INVALID_PARAMETER_CODE), SerffConstants.INVALID_PARAMETER_MESSAGE_PARSING_REQUEST, StringUtils.EMPTY);
				}
				planRate.setEffectiveEndDate(endDate);
			}

			boolean isFamilyOption = false;

			if (null != item && null != item.getAgeNumber()
					&& null != item.getAgeNumber().getCellValue()) {

				String ageRange = item.getAgeNumber().getCellValue();
				if (StringUtils.isNotBlank(ageRange)) {

					if (ageRange.equalsIgnoreCase(rateFamilyOption)) {
						isFamilyOption = true;
					} else {
						String minage = null;
						String maxage = null;

						planRate.setRateOption(RATE_OPTION.A);
						if (ageRange.startsWith("65 and")) {
							minage = "65";
							maxage = "150";
						} else if (ageRange.startsWith("64 and")) {
							minage = "64";
							maxage = "150";
						} else if (ageRange.contains(SerffConstants.HYPHEN)) {
							String[] token = ageRange.split(SerffConstants.HYPHEN);
							minage = token[0];
							maxage = token[1];
						} else {
							minage = ageRange;
							maxage = ageRange;
						}
						if(StringUtils.isNumeric(minage)) {
							Integer minAge = Integer.parseInt(minage);
							planRate.setMinAge(minAge);
						}
						if(StringUtils.isNumeric(maxage)) {
							Integer maxAge = Integer.parseInt(maxage);
							planRate.setMaxAge(maxAge);
						}
					}
				}
			}
			// planHealthRate.setPlanHealthServiceRegion(planHealthServiceRegion);
			planRate.setCreationTimestamp(calendar.getTime());
			planRate.setLastUpdateTimestamp(calendar.getTime());

			if (null != item && null != item.getRateAreaId()
					&& null != item.getRateAreaId().getCellValue() && null != ratingAreaIds) {
				planRate.setRatingArea(ratingAreaIds.get(item.getRateAreaId().getCellValue()));
			}

			boolean isTobaccoOrNonUser = false;

			if (!isFamilyOption && null != item && null != item.getTobacco()) {
				String userType = item.getTobacco().getCellValue();

				// planRate.setTobacco(userType);
				if (SerffConstants.TOBACCO_AND_NON_TOBACCO.equalsIgnoreCase(userType)) {
					planRate.setTobacco("Y");
					isTobaccoOrNonUser = true;

					if (null != item.getPrimaryEnrolleeTobacco()
							&& validateAmount(item.getPrimaryEnrolleeTobacco().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
						planRate.setRate(Float.parseFloat(item.getPrimaryEnrolleeTobacco().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
					}
				}
				else {
					planRate.setTobacco(null);

					if (null != item.getPrimaryEnrollee()
							&& validateAmount(item.getPrimaryEnrollee().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
						planRate.setRate(Float.parseFloat(item.getPrimaryEnrollee().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
					}
				}
			}

			if(null != item && null != item.getPlanId() && StringUtils.isNotEmpty(item.getPlanId().getCellValue())){
				if(!CollectionUtils.isEmpty(planRates.get(item.getPlanId().getCellValue()))){
					ratesList = planRates.get(item.getPlanId().getCellValue());
					LOGGER.debug("Got ratelist for planid: " + item.getPlanId().getCellValue() + " Size: " + ratesList.size());
				}
				else{
					ratesList = new ArrayList<PlanRate>();
					LOGGER.debug("Created new ratesList");
				}

				if (isTobaccoOrNonUser) {
					PlanRate planRateTobaccoUser = planRate.clone();
					planRateTobaccoUser.setTobacco("N");

					if (null != item.getPrimaryEnrollee()
							&& validateAmount(item.getPrimaryEnrollee().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
						planRateTobaccoUser.setRate(Float.parseFloat(item.getPrimaryEnrollee().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
					}
					else {
						planRateTobaccoUser.setRate(null);
					}
					ratesList.add(planRateTobaccoUser);
				}

				if (isFamilyOption) {
					saveFamilyOptionList(item, planRate, ratesList);
				}
				else {
					ratesList.add(planRate);
				}
				planRates.put(item.getPlanId().getCellValue(), ratesList);
			}
		}
		LOGGER.debug("Adding rate to ratesList - new size:" + ratesList.size());
		LOGGER.info("processing of PlanRateBean() Ends");
		return planRates;
	}

	private void saveFamilyOptionList(final QhpApplicationRateItemVO item, PlanRate planRate, List<PlanRate> ratesList) {

		List<LookupValue> lookupValueCodeList = iLookupRepository.getLookupValueList(SerffConstants.SERFF_FAMILY_OPTION_VALUE);

		if (null != item && null != planRate && null != lookupValueCodeList
				&& !lookupValueCodeList.isEmpty()) {

			PlanRate clonePlanRate = null;
			Float rate;
			String lookupValueStr = null;
			
			for (LookupValue lookupValue : lookupValueCodeList) {

				if(null == lookupValue) {
					continue;
				}
				//initialize to invalid rate
				rate = -1F;
				lookupValueStr = lookupValue.getLookupValueLabel();

				if (FAMILY_OPTION_PRIMARY_ENROLLEE.equalsIgnoreCase(lookupValueStr)
						&& null != item.getPrimaryEnrollee()
						&& validateAmount(item.getPrimaryEnrollee().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
					rate = Float.parseFloat(item.getPrimaryEnrollee().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));
				}
				else if (FAMILY_OPTION_COUPLE_ENROLLEE.equalsIgnoreCase(lookupValueStr)
						&& null != item.getCoupleEnrollee()
						&& validateAmount(item.getCoupleEnrollee().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
					rate = Float.parseFloat(item.getCoupleEnrollee().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));
				}
				else if (FAMILY_OPTION_PRIMARY_ENROLLEE_ONE_DEPENDENT.equalsIgnoreCase(lookupValueStr)
						&& null != item.getPrimaryEnrolleeOneDependent()
						&& validateAmount(item.getPrimaryEnrolleeOneDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
					rate = Float.parseFloat(item.getPrimaryEnrolleeOneDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));
				}
				else if (FAMILY_OPTION_PRIMARY_ENROLLEE_TWO_DEPENDENT.equalsIgnoreCase(lookupValueStr)
						&& null != item.getPrimaryEnrolleeTwoDependent()
						&& validateAmount(item.getPrimaryEnrolleeTwoDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
					rate = Float.parseFloat(item.getPrimaryEnrolleeTwoDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));
				}
				else if (FAMILY_OPTION_PRIMARY_ENROLLEE_MANY_DEPENDENT.equalsIgnoreCase(lookupValueStr)
						&& null != item.getPrimaryEnrolleeManyDependent()
						&& validateAmount(item.getPrimaryEnrolleeManyDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
					rate = Float.parseFloat(item.getPrimaryEnrolleeManyDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));
				}
				else if (FAMILY_OPTION_COUPLE_ENROLLEE_ONE_DEPENDENT.equalsIgnoreCase(lookupValueStr)
						&& null != item.getCoupleEnrolleeOneDependent()
						&& validateAmount(item.getCoupleEnrolleeOneDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
					rate = Float.parseFloat(item.getCoupleEnrolleeOneDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));
				}
				else if (FAMILY_OPTION_COUPLE_ENROLLEE_TWO_DEPENDENT.equalsIgnoreCase(lookupValueStr)
						&& null != item.getCoupleEnrolleeTwoDependent()
						&& validateAmount(item.getCoupleEnrolleeTwoDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
					rate = Float.parseFloat(item.getCoupleEnrolleeTwoDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));
				}
				else if (FAMILY_OPTION_COUPLE_ENROLLEE_MANY_DEPENDENT.equalsIgnoreCase(lookupValueStr)
						&& null != item.getCoupleEnrolleeManyDependent()
						&& validateAmount(item.getCoupleEnrolleeManyDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
					rate = Float.parseFloat(item.getCoupleEnrolleeManyDependent().getCellValue().trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY));
				}

				if(StringUtils.isNotBlank(lookupValue.getLookupValueCode())
						&& StringUtils.isNumeric(lookupValue.getLookupValueCode().trim())) {

					int lookupValueCode = Integer.valueOf(lookupValue.getLookupValueCode().trim());
					clonePlanRate = planRate.clone();
					clonePlanRate.setMinAge(lookupValueCode);
					clonePlanRate.setMaxAge(lookupValueCode);
					clonePlanRate.setTobacco(null);
					clonePlanRate.setRateOption(RATE_OPTION.F);
					if (rate >= 0) {
						clonePlanRate.setRate(rate);
					} else {
						clonePlanRate.setRate(null);
					}
					ratesList.add(clonePlanRate);
				}
			}
		}
	}

	/**
	 * Method to populate Region bean from SERFF ServiceArea PayloadType.
	 *
	 * @param networkProvider The source SERFF PayloadType instance for Network bean.
	 * @return populated region Region Bean.
	 * @throws GIException
	 */
	public List<IssuerServiceArea> saveIssuerServiceAreaBean(int applicableYear, EntityManager entityManager, com.serff.template.svcarea.extension.PayloadType serviceAreaTemplate, String issuerHiosID) throws GIException {

		LOGGER.info("saveIssuerServiceAreaBean() Starts " );
		//boolean isSaved= Boolean.FALSE, entitySaved=Boolean.FALSE;
		String stateCode = null;

		List<IssuerServiceArea> savedServiceArea = new ArrayList<IssuerServiceArea>();
		IssuerServiceArea issuerServiceArea =null;
		IssuerServiceAreaExt issuerServiceAreaExt = null;
		ServiceArea issuerPmServiceArea = null;

		List<ServiceAreaType> issuerServiceAreaList = null;
		Map<String, IssuerServiceArea> pmIssuerServiceAreaMap = new HashMap<String, IssuerServiceArea>();

		if(null != serviceAreaTemplate && null != serviceAreaTemplate.getIssuer()){
			issuerServiceAreaList = serviceAreaTemplate.getIssuer().getServiceArea();
			if(null != serviceAreaTemplate.getIssuer().getIssuerStateCode() && null != serviceAreaTemplate.getIssuer().getIssuerStateCode().getValue()) {
				stateCode = serviceAreaTemplate.getIssuer().getIssuerStateCode().getValue().value();
			}
		}

		if( null != issuerHiosID && null != stateCode) {
			String svcAreaId = null, svcAreaName = null;
			boolean isState = false, isPartialCounty = false;
			Map<String, Map<String, List<ServiceArea>>> partialCountyMap = new HashMap<String, Map<String,List<ServiceArea>>>();
			Map<String, List<ServiceArea>> countyZipMap = null;
			List<ServiceArea> pmServiceAreasList = null;
			List<IssuerServiceAreaExt> pmIssuerServiceAreaExtList  = null;
			Map<String, String> addedFullCountyMap = new HashMap<String, String>();
			for(ServiceAreaType serviceAreaType : issuerServiceAreaList) {
				
				if (null != serviceAreaType.getServiceAreaIdentification().getIdentificationID()
						&& StringUtils.isNotBlank(serviceAreaType.getServiceAreaIdentification().getIdentificationID().getValue())) {
					svcAreaId = serviceAreaType.getServiceAreaIdentification().getIdentificationID().getValue();
				}
				else {
					svcAreaId = null;
				}
				
				if (null != serviceAreaType.getServiceAreaName()
						&& StringUtils.isNotBlank(serviceAreaType.getServiceAreaName().getValue())) {
					svcAreaName = serviceAreaType.getServiceAreaName().getValue();
				}
				else {
					svcAreaName = null;
				}
				
				List<GeographicAreaType> geographicAreaList = serviceAreaType.getGeographicArea();
				if(StringUtils.isBlank(svcAreaId)){
					LOGGER.error("Service Area Id not provided HiosID: " + issuerHiosID);
					throw new GIException(Integer.parseInt(SerffConstants.NO_OBJECT_FOUND_CODE), SerffConstants.NO_OBJECT_FOUND_MESSAGE_SERVICE_AREA_MISSING + " : Service Area Id not provided HiosID: " + issuerHiosID, StringUtils.EMPTY);
				}
				if(null == pmIssuerServiceAreaMap.get(svcAreaId)){
					//Checking if there is an existing record in PM_ISSUER_SERVICE_AREA
					LOGGER.info("Checking if PM_ISSUER_SERVICE_AREA already have data for provided state,issuerHiosID and ServiceAreadId ");
					List<IssuerServiceArea> servserviceAreasListFromDB = iIssuerServiceAreaRepository.findbyStateIssuerAndServAreaAndApplicableYear(stateCode, issuerHiosID, svcAreaId, applicableYear);
					
					if (!CollectionUtils.isEmpty(servserviceAreasListFromDB)) {
						//Ideally there should be only one record.Still using list to avoid any junk data
						LOGGER.info("Record already exist in PM_ISSUER_SERVICE_AREA for provided state,issuerHiosID and ServiceAreadId ");
						issuerServiceArea = servserviceAreasListFromDB.get(0);
						pmIssuerServiceAreaExtList = issuerServiceArea.getIssuerServiceAreaExt();
						//Need to soft delete existing records in child tables
						boolean isOrphansRemoved = cleanUpExistingChildren(issuerServiceArea, entityManager);
						
						if (isOrphansRemoved) {
							LOGGER.info("Existing child records cleaned up for issuerServiceArea with id " + issuerServiceArea.getId());
						}
					}
					else {
						LOGGER.info("No existing record found. New entry will be done for PM_ISSUER_SERVICE_AREA");
						issuerServiceArea = new  IssuerServiceArea();
						pmIssuerServiceAreaExtList = new ArrayList<IssuerServiceAreaExt>();
						issuerServiceArea.setIssuerServiceAreaExt(pmIssuerServiceAreaExtList);
						issuerServiceArea.setSerffServiceAreaId(svcAreaId);
						issuerServiceArea.setServiceAreaName(svcAreaName);
						issuerServiceArea.setHiosIssuerId(issuerHiosID);
						issuerServiceArea.setIssuerState(stateCode);
					}

					// Fixed HP-FOD issue
					if (null != issuerServiceArea) {
						pmIssuerServiceAreaMap.put(svcAreaId, issuerServiceArea);
					}
				}
				else {
					issuerServiceArea = pmIssuerServiceAreaMap.get(svcAreaId);
					pmIssuerServiceAreaExtList = issuerServiceArea.getIssuerServiceAreaExt();
				}

				for(GeographicAreaType geographicAreaType: geographicAreaList) {

					if(null != geographicAreaType.getGeographicAreaEntireStateIndicator()){
						isState = geographicAreaType.getGeographicAreaEntireStateIndicator().isValue();
					}
						
					if(null != geographicAreaType.getGeographicAreaPartialCountyIndicator()){
						isPartialCounty = geographicAreaType.getGeographicAreaPartialCountyIndicator().isValue();
					}
						
					if(isState || !isPartialCounty){

						List<String> counties = new ArrayList<String>();
						if(isState){
							//Scenario 1: Cover full state
							issuerServiceArea.setCompleteState(SerffConstants.YES);
							counties = iZipCodeRepository.findCountiesByState(stateCode);
						}
						else{
							//Scenario 2: Partial State full county
							issuerServiceArea.setCompleteState(SerffConstants.NO);
							if(null != geographicAreaType.getLocationCounty() && null != geographicAreaType.getLocationCounty().getValue()){
								USCountyCodeType usCountyCode = (USCountyCodeType) geographicAreaType.getLocationCounty().getValue();
								counties.add(usCountyCode.getValue());
							}
						}

						for(String county : counties){
							StringBuilder key = new StringBuilder();
							key.append(svcAreaId);
							key.append(county);
							
							if(addedFullCountyMap.get(key.toString()) != null && addedFullCountyMap.get(key.toString()).equals(stateCode)) {
								//This county is already processed
								if(LOGGER.isInfoEnabled()) {
									LOGGER.info("Skipping County " + SecurityUtil.sanitizeForLogging(county) + " and Service area =" + SecurityUtil.sanitizeForLogging(svcAreaId));
								}
								continue;
							}
							if(LOGGER.isInfoEnabled()) {
								LOGGER.info("Scenario 2 : Loading zipcode for county " + SecurityUtil.sanitizeForLogging(county) + " and Service Area =" + SecurityUtil.sanitizeForLogging(svcAreaId));
							}
							List<ZipCode> zipCodes = iZipCodeRepository.findByCountyAndState(stateCode, county);
							ZipCode zipCode = null;
							if(!CollectionUtils.isEmpty(zipCodes)){
								zipCode = zipCodes.get(0);
							}else{
								if(LOGGER.isInfoEnabled()) {
									LOGGER.info("No Zip codes found for County " + SecurityUtil.sanitizeForLogging(county) + " and service area =" + SecurityUtil.sanitizeForLogging(svcAreaId));
								}
								continue;
							}

							issuerServiceAreaExt = new IssuerServiceAreaExt();
							issuerServiceAreaExt.setIsDeleted(IssuerServiceAreaExt.IS_DELETED.N.toString());
							issuerServiceAreaExt.setServiceAreaId(issuerServiceArea);
							issuerServiceAreaExt.setCounty(zipCode.getCounty());
							issuerServiceAreaExt.setPartialCounty(SerffConstants.NO);
							pmServiceAreasList = new ArrayList<ServiceArea>();

							List<String> allZipCodes = iZipCodeRepository.findZipByCountyAndState(county, stateCode);
							String stateFips;
							for(String zip : allZipCodes){
								issuerPmServiceArea = new ServiceArea();
								issuerPmServiceArea.setIsDeleted(ServiceArea.IS_DELETED.N.toString());
								issuerPmServiceArea.setServiceAreaId(issuerServiceArea);
								issuerPmServiceArea.setServiceAreaExtId(issuerServiceAreaExt);
								issuerPmServiceArea.setCounty(zipCode.getCounty());
								issuerPmServiceArea.setState(zipCode.getState());
								issuerPmServiceArea.setZip(zip);
								stateFips = zipCode.getStateFips();
								/*if(null == stateFips || stateFips.equalsIgnoreCase("null")) {
									stateFips = "";
								}*/
								issuerPmServiceArea.setFips(stateFips+zipCode.getCountyFips());
								pmServiceAreasList.add(issuerPmServiceArea);
							}

							issuerServiceAreaExt.setServiceArea(pmServiceAreasList);
							pmIssuerServiceAreaExtList.add(issuerServiceAreaExt);
							addedFullCountyMap.put(key.toString(), stateCode);
						}

					}
					else {
						//Scenario 3: Partial State partial county
						String countyCode = null;

						if (null != issuerServiceArea) {
							issuerServiceArea.setCompleteState(SerffConstants.NO);
						}

						if(null != geographicAreaType.getLocationCounty() && null != geographicAreaType.getLocationCounty().getValue()){
							USCountyCodeType usCountyCode = (USCountyCodeType) geographicAreaType.getLocationCounty().getValue();
							countyCode = usCountyCode.getValue();
							if(StringUtils.isBlank(countyCode)){
								LOGGER.error("County code not provided for PartialCounty. Service AreaId: " + SecurityUtil.sanitizeForLogging(svcAreaId));								
								throw new GIException(Integer.parseInt(SerffConstants.INVALID_PARAMETER_CODE), SerffConstants.INVALID_PARAMETER_MESSAGE_DATA_FORMAT_INVALID + " : County code not provided for PartialCounty. Service AreaId : " + svcAreaId, StringUtils.EMPTY);								
							}
						}
						else{
							LOGGER.error("County code not provided for PartialCounty");
							throw new GIException(Integer.parseInt(SerffConstants.INVALID_PARAMETER_CODE), SerffConstants.INVALID_PARAMETER_MESSAGE_DATA_FORMAT_INVALID + " : County code not provided for PartialCounty. Service AreaId : " + svcAreaId, StringUtils.EMPTY);								
						}

						List<ZipCode> counties = iZipCodeRepository.findByCountyAndState(stateCode, countyCode);
						ZipCode county = null;
						
						if (!CollectionUtils.isEmpty(counties)) {
							county = counties.get(0);
						}
						else {
							throw new GIException(Integer.parseInt(SerffConstants.INVALID_PARAMETER_CODE), SerffConstants.INVALID_PARAMETER_MESSAGE_DATA_FORMAT_INVALID + " : Provided county code " + countyCode + " not for the given State " + stateCode, StringUtils.EMPTY);
						}
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("Scenario 3 : Loading zipcode for county " + SecurityUtil.sanitizeForLogging(countyCode) + " and service-area =" + SecurityUtil.sanitizeForLogging(svcAreaId));
						}
						Map<String,List<ServiceArea>> countyandZip = partialCountyMap.get(svcAreaId);
						StringBuffer zipList = null;
						if (null != countyandZip && null != countyandZip.get(countyCode)) {
							pmServiceAreasList = countyandZip.get(countyCode);
							countyZipMap = countyandZip;
							issuerServiceAreaExt = pmServiceAreasList.get(0).getServiceAreaExtId();
							String ziplist = issuerServiceAreaExt.getZipList();
							zipList = new StringBuffer(ziplist);
							zipList.append(SerffConstants.COMMA);
							
						}
						else {
							pmServiceAreasList = new ArrayList<ServiceArea>();
							issuerServiceAreaExt = new IssuerServiceAreaExt();
							issuerServiceAreaExt.setIsDeleted(IssuerServiceAreaExt.IS_DELETED.N.toString());
							issuerServiceAreaExt.setServiceAreaId(issuerServiceArea);
							issuerServiceAreaExt.setCounty(county.getCounty());
							issuerServiceAreaExt.setPartialCounty(SerffConstants.YES);
							pmIssuerServiceAreaExtList.add(issuerServiceAreaExt);
							issuerServiceAreaExt.setServiceArea(pmServiceAreasList);
		
							if (null != geographicAreaType.getGeographicAreaPartialCountyJustificationText()) {
								String justificationText = org.apache.commons.lang.StringUtils.trimToEmpty(geographicAreaType.getGeographicAreaPartialCountyJustificationText().getValue());
								if(justificationText.length() > PARTIAL_COUNTY_JUSTIFICATION_TEXT_LEN) {
									LOGGER.warn("Truncating partial county justification text to " + PARTIAL_COUNTY_JUSTIFICATION_TEXT_LEN + " characters: " + justificationText);
									justificationText = justificationText.substring(0, PARTIAL_COUNTY_JUSTIFICATION_TEXT_LEN);
								}
								issuerServiceAreaExt.setPartialCountyJustification(justificationText);
							}
							zipList = new StringBuffer();
							countyZipMap = new HashMap<String,List<ServiceArea>>();
						}

						List<com.serff.template.svcarea.niem.proxy.xsd.String> partialZipCodes = geographicAreaType.getLocationPostalCode();
						String stateFips = null;
						
						for (com.serff.template.svcarea.niem.proxy.xsd.String locationPostalCode : partialZipCodes) {
							
							if (null != locationPostalCode) {
								
								zipList.append(locationPostalCode.getValue());
								zipList.append(SerffConstants.COMMA);
								
								issuerPmServiceArea = new ServiceArea();
								issuerPmServiceArea.setIsDeleted(ServiceArea.IS_DELETED.N.toString());
								issuerPmServiceArea.setServiceAreaId(issuerServiceArea);
								issuerPmServiceArea.setServiceAreaExtId(issuerServiceAreaExt);
								issuerPmServiceArea.setZip(locationPostalCode.getValue());
								issuerPmServiceArea.setCounty(county.getCounty());
								issuerPmServiceArea.setState(county.getState());
								stateFips = county.getStateFips();
								/*if(null == stateFips || stateFips.equalsIgnoreCase("null")) {
									stateFips = "";
								}*/
								issuerPmServiceArea.setFips(stateFips+countyCode);
								pmServiceAreasList.add(issuerPmServiceArea);
							}
						}
						// Fixed HP-FOD issue
						if (null != issuerServiceArea && null != issuerServiceAreaExt) {
							
							if (null != zipList) {
								LOGGER.debug("IssuerServiceArea Id: " + issuerServiceArea.getId() + ", Zip List: " + zipList);
								issuerServiceAreaExt.setZipList(zipList.substring(0, zipList.lastIndexOf(SerffConstants.COMMA)));
							}
							issuerServiceAreaExt.setServiceArea(pmServiceAreasList);
							//issuerServiceArea.setIssuerServiceAreaExt(pmIssuerServiceAreaExtList);
						}
						countyZipMap.put(countyCode, pmServiceAreasList);
						partialCountyMap.put(svcAreaId, countyZipMap);
					}
				}
			}

			if (null != entityManager && entityManager.isOpen()) {

				for (Map.Entry<String, IssuerServiceArea> entry : pmIssuerServiceAreaMap.entrySet()) {
					// savedServiceArea.add(iIssuerServiceAreaRepository.save(entry.getValue()));
					entry.getValue().setApplicableYear(applicableYear);
					savedServiceArea.add(entityManager.merge(entry.getValue()));
				}
			}
		}
		else {
			LOGGER.error("Invalid ISSUER HIOS ID associated with request.");
		}
		LOGGER.info("saveIssuerServiceAreaBean() Ends" );
		return savedServiceArea;
	}

	/**
	 * This method deletes all the existing child data for the Issuer service area provided.
	 * @author Geetha Chandran
	 * @since 21 July, 2013
	 * @return boolean
	 */
	private boolean cleanUpExistingChildren(
			IssuerServiceArea issuerServiceArea, EntityManager entityManager) {
		LOGGER.info("Cleaning up of existing service area data begins" );
		boolean isChildDeleted = false;
		if(null != issuerServiceArea){
			List<IssuerServiceAreaExt> issuerServiceAreaExts = issuerServiceArea.getIssuerServiceAreaExt();
			if(!CollectionUtils.isEmpty(issuerServiceAreaExts)){
				for(IssuerServiceAreaExt issuerServiceAreaExt : issuerServiceAreaExts){
					LOGGER.info("Fetching service area records for issuerServiceAreaExt with id " + issuerServiceAreaExt.getId());
					List<ServiceArea> serviceAreas = iServiceAreaRepository.findByServiceAreaIdAndServiceAreaExtIdAndIsDeleted(issuerServiceArea, issuerServiceAreaExt ,ServiceArea.IS_DELETED.N.toString() );
					if(!CollectionUtils.isEmpty(serviceAreas)){
						for(ServiceArea serviceArea : serviceAreas){
							serviceArea.setIsDeleted(ServiceArea.IS_DELETED.Y.toString());
							entityManager.merge(serviceArea);
						}
						LOGGER.info(SVCAREA_DELETED + issuerServiceArea.getId() + " and issuerServiceAreaExt =" + issuerServiceAreaExt.getId());
					}
					issuerServiceAreaExt.setIsDeleted(IssuerServiceAreaExt.IS_DELETED.Y.toString());
					entityManager.merge(issuerServiceAreaExt);
				}
				isChildDeleted = true;
				LOGGER.info(SVCAREA_DELETED+ issuerServiceArea.getId());
			}

		}
		LOGGER.info("Cleaning up of existing service area data ends" );
		return isChildDeleted;
	}

	public List<Plan> savePlanBenefitTemplateVO(int applicableYear, boolean isCertified, EntityManager entityManager, PlanAndBenefitsPackageVO planAndBenefitsPackageVO, 
			PlanAndBenefitsVO currentPBVO, Issuer issuer, List<IssuerServiceArea> issuerServiceArea, Map<String, Network> networks, Map<String, String> documents,
			Map<String, List<PlanRate>> planRates, boolean persistOffExchangePlans, boolean persistOnExchangePlans, boolean isPUFPlan,
			Map<String, Formulary> formulary, Map<String, Date> templatesLastModifiedDates, StringBuffer responseMessage, List<Tenant> tenantList, 
			Map<String,Date>lastUpdatedTemplateMap, PlanUpdateStatistics uploadStatistics, PlanDisplayRulesMap planDispRulesMap)
			throws GIException{
		Map<String, Formulary> formularyMap = formulary;
		
		LOGGER.info("savePlanBenefitTemplateVO() Started " );
		boolean isOffExchangePlan = false;
		// List<PlanAndBenefitsPackageVO> packages =null;
		List<com.serff.template.plan.CostShareVarianceVO> costShareVariancesList =null;
		Plan savedPlan = null;
		List<Plan> savedPlans = new ArrayList<Plan>();
		healthParentId = -1;
		dentalParentId = -1;

		Validate.notNull(issuer, "No issuer associated for current PlanBenefit template.");
		Validate.notNull(planAndBenefitsPackageVO, "PlanBenefit template Payload is missing in current request");

		LOGGER.debug("Current Issuer HIOS ID:" + issuer.getHiosIssuerId());

		Validate.notNull(iPlanRepository, "PlanRepository instance is improperly configured.");
		//Validate.notNull(planHealthServiceRegionRepository, "PlanHealthServiceRegionRepository instance is improperly configured.");

		boolean isDentalPlanOnly = SerffConstants.YES.equalsIgnoreCase(planAndBenefitsPackageVO.getHeader().getDentalPlanOnlyInd().getCellValue());
		String planInsuranceType = getPlanInsuranceType(planAndBenefitsPackageVO);
		
		if(null != planAndBenefitsPackageVO.getPlansList()
			&& null != planAndBenefitsPackageVO.getPlansList().getPlans()) {

			String standarCompId = null;
			boolean isDecertified = false;
			if(null != currentPBVO) {

				costShareVariancesList = currentPBVO.getCostShareVariancesList().getCostShareVariance();
				//Header
				if(null != currentPBVO.getPlanAttributes() && null != currentPBVO.getPlanAttributes().getStandardComponentID()
						&& StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getStandardComponentID().getCellValue())){
					standarCompId = currentPBVO.getPlanAttributes().getStandardComponentID().getCellValue();
					if(null != currentPBVO.getPlanAttributes().getFormularyID() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getFormularyID().getCellValue()) 
							&& null != currentPBVO.getPlanAttributes().getFormularyURL() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getFormularyURL().getCellValue())) {
						addBasicFormularyEntry(currentPBVO.getPlanAttributes().getFormularyID().getCellValue(), 
								currentPBVO.getPlanAttributes().getFormularyURL().getCellValue(), 
								formularyMap, issuer, applicableYear, entityManager);
					}
				}else{
					LOGGER.error("StandardComponentID is null");
					throw new GIException(Integer.parseInt(SerffConstants.NO_DATA_CODE), SerffConstants.NO_DATA_MESSAGE_EMPTY_DATA + " : StandardComponentID is null.", StringUtils.EMPTY);
				}

				if(!CollectionUtils.isEmpty(costShareVariancesList)){
					int counter = 0, onExchangeVarianceCount = 0;
					
					for (com.serff.template.plan.CostShareVarianceVO costShareVarianceVO : costShareVariancesList) {
						if(null != costShareVarianceVO.getPlanId()
								&& StringUtils.isNotEmpty(costShareVarianceVO.getPlanId().getCellValue())) {
							if(costShareVarianceVO.getPlanId().getCellValue().endsWith(SerffConstants.OFF_EXCHANGE_SUFFIX)) {
								isOffExchangePlan = true;
							} else {
								isOffExchangePlan = false;
								onExchangeVarianceCount++;
							}
							
							// Scenario: Avoid existing IssuerPlanNumber from certified plans and insert new IssuerPlanNumber.
							String issuerPlanNumber = costShareVarianceVO.getPlanId().getCellValue().replaceAll(SerffConstants.HYPHEN, StringUtils.EMPTY);
							List<Plan> existingPlans = iPlanRepository.findByIssuerPlanNumberAndIsDeletedAndApplicableYearAndInsuranceType(issuerPlanNumber, Plan.IS_DELETED.N.toString(), applicableYear, planInsuranceType);
						
							if (null != existingPlans
									&& !existingPlans.isEmpty()){
								
								if(SerffConstants.DECERTIFIED.equals(existingPlans.get(0).getStatus())){
									LOGGER.info("Variant is marked as decertified ,so skipping this record: " + issuerPlanNumber);
									isDecertified = true;
									uploadStatistics.planSkipped();
									continue;
								}else if(isCertified){
									LOGGER.info("Variant is certified ,so skipping this record: " + issuerPlanNumber);
									//uploadStatistics.planSkippedCertified();
									continue;
								}
								
							}
							counter ++;
						
							//Skip Off-Exchange / On-Exchange plans as required looking at the flags
							if(isOffExchangePlan) {
								if(!persistOffExchangePlans) {
									LOGGER.info("Skipping saving of Off Exchange Plans " + costShareVarianceVO.getPlanId());
									uploadStatistics.planSkipped();
									counter --;
									continue;
								}
							} else {
								if(!persistOnExchangePlans) {
									LOGGER.info("Skipping saving of On Exchange Plans " + costShareVarianceVO.getPlanId());
									uploadStatistics.planSkipped();
									counter --;
									continue;
								}
							}
							if(!isDentalPlanOnly){
								savedPlan = savePlanHealth(applicableYear, entityManager, planAndBenefitsPackageVO, issuer, currentPBVO, costShareVarianceVO, issuerServiceArea, networks, documents, planRates, formularyMap, isOffExchangePlan, isPUFPlan, tenantList, planDispRulesMap);
							}else{
								savedPlan = savePlanDental(applicableYear, entityManager, planAndBenefitsPackageVO, issuer, currentPBVO, costShareVarianceVO, issuerServiceArea, networks, documents, planRates, formularyMap, isOffExchangePlan, isPUFPlan, tenantList, planDispRulesMap);
							}
							if(null != savedPlan) {
								uploadStatistics.planAdded();
								savedPlans.add(savedPlan);
							}
						}
					}

					if(!isPHIXProfile && onExchangeVarianceCount == 0){
						//HIX-106281 - treat as warn and don't throw exception
						LOGGER.warn("Invalid request. Plan does not contain any valid on-exchange cost share variances for plan:" + SecurityUtil.sanitizeForLogging(standarCompId));
						//throw new GIException(Integer.parseInt(SerffConstants.NO_OBJECT_FOUND_CODE), SerffConstants.NO_OBJECT_FOUND_MESSAGE_INVALID_PLAN + " : Plan does not contain any valid on-exchange cost share variances for plan:" + standarCompId, StringUtils.EMPTY);
					}
					
					if(counter > 0){
						updatePlanService.updateSerffTemplate(applicableYear, standarCompId, templatesLastModifiedDates, false, entityManager , lastUpdatedTemplateMap);
					}
				}else{
					LOGGER.error("Invalid request. Plan does not contain any valid cost share variances for plan:" + SecurityUtil.sanitizeForLogging(standarCompId));
					throw new GIException(Integer.parseInt(SerffConstants.NO_OBJECT_FOUND_CODE), SerffConstants.NO_OBJECT_FOUND_MESSAGE_INVALID_PLAN + " : Plan does not contain any valid cost share variances for plan:" + standarCompId, StringUtils.EMPTY);
				}
			}

			LOGGER.info("Plan and Benefit Template Bean Saved ");
			if(!isCertified && savedPlans.isEmpty()) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Plan and Benefit Template Bean Save Operation found that no plans saved for " + SecurityUtil.sanitizeForLogging(standarCompId));
				}
				//Throw exception if it is non-PHIX
				if(!isPHIXProfile) { 
					if(isDecertified){
						LOGGER.error(" No Plans were saved for the given request ,As plans are decertified.");
						throw new GIException(Integer.parseInt(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE), SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE + " : No Plans were saved for the given request. As plans are decertified", StringUtils.EMPTY);

					}else{
						//HIX-106281 - Ignore OFF-exchange plans on state exchange plan load pages, don't throw exception
						LOGGER.warn("No Plans were saved for the given request for plan ID : " + standarCompId);
						StringBuffer responseMsg = new StringBuffer(" No Plans were saved for ID : " );
						responseMsg.append(standarCompId);
						responseMessage.append(responseMsg.toString());
						//throw new GIException(Integer.parseInt(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE), SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE + " : No Plans were saved for the given request.", StringUtils.EMPTY);
					}
					
				} else {
					StringBuffer responseMsg = new StringBuffer(" No Plans were saved for ID : " );
					responseMsg.append(standarCompId);
					responseMessage.append(responseMsg.toString());
				}
			}
			else {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Plan and Benefit Template Bean Saved for " + SecurityUtil.sanitizeForLogging(standarCompId));
				}
			}
		}
		LOGGER.info("savePlanBenefitTemplateVO() Ends " );
		return savedPlans;
	}

	private void addBasicFormularyEntry(String formularyId, String formularyURL, Map<String, Formulary> formularyMap, Issuer issuer, int applicableYear, EntityManager entityManager) {
		if(StringUtils.isBlank(formularyId) || StringUtils.isBlank(formularyURL) || null == entityManager) {
			return;
		}
		
		if(null == formularyMap.get(formularyId)) {
			LOGGER.info("Adding formulary record with basic data: Issuer: " + issuer.getHiosIssuerId() + " FormularyId: " + formularyId);
			DrugList drugList = new DrugList();
			drugList.setIssuer(issuer);
			drugList.setFormularyDrugListId(0);
			drugList.setIsDeleted(SerffConstants.YES_ABBR);
			Formulary formulary = new Formulary();
			formulary.setIssuer(issuer);
			formulary.setFormularyId(formularyId);
			formulary.setFormularyUrl(formularyURL);
			formulary.setDrugList(drugList);
			formulary.setNoOfTiers(0);
			formulary.setApplicableYear(applicableYear);
			if (entityManager.isOpen()) {
				formulary = entityManager.merge(formulary);
				formularyMap.put(formulary.getFormularyId(), formulary);
			}
		}
	}

	/*
	 * Method is used validate Plan Formulary Id.
	 * @throws IllegalArgumentException
	 * @return Existing Formulary ID in String formate.
	 */
	public String validatePlanFormularyId(int applicableYear, String formularyID, Map<String, Formulary> formulary, int issuerId) {
		/*throws IllegalArgumentException {*/

		LOGGER.info("validatePlanFormularyId() Start");
		String planFormularyID = null;

		try {
			Formulary existingFormulary = null;
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Plan Formulary ID: " + SecurityUtil.sanitizeForLogging(formularyID));
			}

			if (StringUtils.isNotBlank(formularyID)) {

				if (null != formulary && !formulary.isEmpty()) {
					existingFormulary = formulary.get(formularyID);

					if (null != existingFormulary) {
						LOGGER.info("Found Formulary in template: " + formularyID);
						planFormularyID = String.valueOf(existingFormulary.getId());
					}
				}

				if (StringUtils.isBlank(planFormularyID)) {
					List<Formulary> existingFormualryList = iSerffFormularyRepository.findByIssuer_IdAndFormularyIdAndApplicableYear(issuerId, formularyID, applicableYear);

					if (null != existingFormualryList
							&& !existingFormualryList.isEmpty()) {
						existingFormulary = existingFormualryList.get(0);

						if (null != existingFormulary) {
							LOGGER.info("Found Formulary in DB: " + formularyID);
							planFormularyID = String.valueOf(existingFormulary.getId());
						}
					}
				}
				LOGGER.info("Existing Formulary ID: " + planFormularyID);

				/*This check is not required, and set NULL  if Formulary is not found
				 *
				 * if(StringUtils.isBlank(planFormularyID)) {
					throw new IllegalArgumentException("Plan Formularly Id("+ formularyID +") does not exist.");
				}*/
			}
		}
		finally {
			LOGGER.info("validatePlanFormularyId() End");
		}
		return planFormularyID;
	}

	private Plan savePlanHealth(int applicableYear, EntityManager entityManager, PlanAndBenefitsPackageVO planAndBenefitsPackageVO, Issuer issuer,
			PlanAndBenefitsVO currentPBVO, com.serff.template.plan.CostShareVarianceVO costShareVarianceVO, List<IssuerServiceArea> issuerServiceAreas,
			Map<String, Network> networks, Map<String, String> documents, Map<String, List<PlanRate>> planRates, Map<String, Formulary> formulary,
			boolean isOffExchangePlan, boolean isPUFPlan, List<Tenant> tenantList, PlanDisplayRulesMap planDispRulesMap) throws GIException {
		
		Plan plan = null;
		PlanHealth planHealth = null;
		PlanHealthCost planHealthCost= null;
		PlanSbcScenario plansbcScenario = null;
		List<PlanHealthCost>  planHealthCostList = null;
		List<PlanHealthBenefit> planHealthBenefits =null;
		List<com.serff.template.plan.MoopVO> moopVOList=null;
		List<com.serff.template.plan.PlanDeductibleVO> planDeductibleVOListObj =null;
		List<com.serff.template.plan.BenefitAttributeVO> currentPlanBAs = null;
		Map<String, List<Map<String,String>>> benefitCopayCoinsData = null;

		String moopName =null;
		String inNetTier1IndAmnt =null;
		String inNetTier1FamilyAmnt=null;
		String outNetIndAmnt=null;
		String outNetFamilyAmnt=null;
		String combinedInOutNetIndAmnt=null;
		String combinedInOutNetFamAmnt=null;
		String deductibleType=null;
		String inNetTier1Ind =null;
		String inNetTier1Family=null;
		String coinsuranceInNetTier1=null;
		String coinsuranceInNetTier2=null;
		String outOfNetInd=null;
		String outOfNetFamily=null;
		String combinedInOrOutNetInd=null;
		String combinedInOrOutNetFamily=null;
		String url =null;
		String inNetTier2IndAmnt =null;
		String inNetTier2FamilyAmnt =null;
		String multipleTierNtwrk=null;

		planHealth = new PlanHealth();
		plan = new Plan();
		if(isOffExchangePlan) {
			plan.setExchangeType(Plan.EXCHANGE_TYPE.OFF.toString());
		} else {
			plan.setExchangeType(Plan.EXCHANGE_TYPE.ON.toString());
		}
		if(isPUFPlan) {
			plan.setIsPUF(IS_PUF.Y);
		} else {
			plan.setIsPUF(IS_PUF.N);
		}
		plan.setIsDeleted(Plan.IS_DELETED.N.toString());
		plan.setApplicableYear(applicableYear);
		plan.setLastUpdatedBy(issuer.getLastUpdatedBy());
		planHealth.setLastUpdatedBy(issuer.getLastUpdatedBy());
		
		planHealthCostList = new ArrayList<PlanHealthCost>();
		planHealthBenefits = new ArrayList<PlanHealthBenefit>();

		//Set 2016 fields.
		if(StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getEhbPercentPremium().getCellValue())){
			plan.setEhbPremiumFraction(Float.parseFloat(currentPBVO.getPlanAttributes().getEhbPercentPremium().getCellValue()));
		}
		if(StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getInsurancePlanCompositePremiumAvailableIndicator().getCellValue())){
			plan.setCompositPremiumAvailableIndicator(SerffConstants.YES.equalsIgnoreCase(currentPBVO.getPlanAttributes().getInsurancePlanCompositePremiumAvailableIndicator().getCellValue()) ? SerffConstants.YES_ABBR : SerffConstants.NO_ABBR);
		}
		if(StringUtils.isNotBlank(costShareVarianceVO.getHsa().getEmployerHSAHRAContributionIndicator().getCellValue())){
			plan.setEmpHSAHRAContributionIndicator(SerffConstants.YES.equalsIgnoreCase(costShareVarianceVO.getHsa().getEmployerHSAHRAContributionIndicator().getCellValue()) ? SerffConstants.YES_ABBR : SerffConstants.NO_ABBR);
		}
		if(StringUtils.isNotBlank(costShareVarianceVO.getHsa().getEmpContributionAmountForHSAOrHRA().getCellValue())){
			plan.setEmpHSAHRAContributionAmount(Float.parseFloat(costShareVarianceVO.getHsa().getEmpContributionAmountForHSAOrHRA().getCellValue().trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
		}
		//end 2016 fields
		
		if(null != planAndBenefitsPackageVO.getHeader()) {
			if(null != planAndBenefitsPackageVO.getHeader().getMarketCoverage() && null != planAndBenefitsPackageVO.getHeader().getMarketCoverage().getCellValue()) {
				// plan.setMarket(planAndBenefitsPackageVO.getHeader().getMarketCoverage().getCellValue().toString());
				String market = planAndBenefitsPackageVO.getHeader().getMarketCoverage().getCellValue();

				if(Plan.PlanMarket.INDIVIDUAL.toString().equalsIgnoreCase(market)) {
					plan.setMarket(Plan.PlanMarket.INDIVIDUAL.toString());
				}
				else {
					plan.setMarket(Plan.PlanMarket.SHOP.toString());
				} 
			}
			plan.setInsuranceType(SerffConstants.HEALTH);
			//plan.setInsuranceType(SerffConstants.DENTAL);
		}

		if(null != currentPBVO.getPlanAttributes()) {
			String issuerPlanNumberWithVariant = null;
			String issuerPlanNumberWithoutVariant = null;
		 	//Plan Identifiers
			if(null != costShareVarianceVO.getPlanId() && StringUtils.isNotBlank(costShareVarianceVO.getPlanId().getCellValue())) {
				issuerPlanNumberWithVariant = costShareVarianceVO.getPlanId().getCellValue();
				issuerPlanNumberWithoutVariant = issuerPlanNumberWithVariant.substring(SerffConstants.PLAN_INITIAL_INDEX, SerffConstants.LEN_HIOS_PLAN_ID);
				plan.setIssuerPlanNumber(issuerPlanNumberWithVariant.replaceAll(SerffConstants.HYPHEN, StringUtils.EMPTY));
				planHealth.setCostSharing(getCSType(issuerPlanNumberWithVariant.substring(SerffConstants.LEN_HIOS_PLAN_ID + 1)));
			}

			/*if(null != currentPBVO.getPlanAttributes().getPlanMarketingName() && null != currentPBVO.getPlanAttributes().getPlanMarketingName().getCellValue()) {
				plan.setName(serffUtils.removeNewLines(currentPBVO.getPlanAttributes().getPlanMarketingName().getCellValue()));
			}*/
			
			if(null != costShareVarianceVO.getPlanVariantMarketingName() && null != costShareVarianceVO.getPlanVariantMarketingName().getCellValue()) {
				plan.setName(serffUtils.removeNewLines(costShareVarianceVO.getPlanVariantMarketingName().getCellValue()));
			} else if(null != currentPBVO.getPlanAttributes().getPlanMarketingName() && null != currentPBVO.getPlanAttributes().getPlanMarketingName().getCellValue()) {
				plan.setName(serffUtils.removeNewLines(currentPBVO.getPlanAttributes().getPlanMarketingName().getCellValue()));
			}
			
			if(null != currentPBVO.getPlanAttributes().getHiosProductID() && null != currentPBVO.getPlanAttributes().getHiosProductID().getCellValue()) {
				plan.setHiosProductId(currentPBVO.getPlanAttributes().getHiosProductID().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getHpid() && null != currentPBVO.getPlanAttributes().getHpid().getCellValue()) {
				plan.setHpid(currentPBVO.getPlanAttributes().getHpid().getCellValue());
			}

			/*if(null != currentPBVO.getPlanAttributes().getNetworkID() && null != currentPBVO.getPlanAttributes().getNetworkID().getCellValue()) {
				plan.setNetworkId(currentPBVO.getPlanAttributes().getNetworkID().getCellValue());
			}*/

			
			//2016 plans
			if(null != costShareVarianceVO.getUrl() && null != costShareVarianceVO.getUrl().getPlanBrochure()) {
				url =checkAndcorrectURL(costShareVarianceVO.getUrl().getPlanBrochure());
				plan.setBrochure(url);
			}

			boolean found =false ;
			String oldServiceAreaID =null;
			if(!CollectionUtils.isEmpty(issuerServiceAreas) && null != currentPBVO.getPlanAttributes().getServiceAreaID()
					&&  StringUtils.isNotEmpty(currentPBVO.getPlanAttributes().getServiceAreaID().getCellValue())) {
				for(IssuerServiceArea issuerServiceArea : issuerServiceAreas){
					oldServiceAreaID = issuerServiceArea.getSerffServiceAreaId();
					if(currentPBVO.getPlanAttributes().getServiceAreaID().getCellValue().equalsIgnoreCase(oldServiceAreaID)){
						found =true;
						plan.setServiceAreaId(issuerServiceArea.getId());
						break;
					}
				}
			}
			if(!found){
				LOGGER.error("Service Area ID is modified .Old Id: "+oldServiceAreaID + " New Id: " + SecurityUtil.sanitizeForLogging(currentPBVO.getPlanAttributes().getServiceAreaID().getCellValue()));
				return null;
			}
			if(!CollectionUtils.isEmpty(networks) && null != currentPBVO.getPlanAttributes().getNetworkID()){
				Network network = networks.get(currentPBVO.getPlanAttributes().getNetworkID().getCellValue());
				if (network != null) {
					plan.setProviderNetworkId(network.getId());
					plan.setNetwork(network);
				} else {
					LOGGER.error("No network found for network id: " +  SecurityUtil.sanitizeForLogging(currentPBVO.getPlanAttributes().getNetworkID().getCellValue()));
				}
			}

			if(StringUtils.isEmpty(plan.getIssuerVerificationStatus())){
				plan.setIssuerVerificationStatus(IssuerVerificationStatus.PENDING.toString());
			}

			plan.setIssuer(issuer);
			plan.setCreatedBy(issuer.getLastUpdatedBy());
			plan.setCreationTimestamp(issuer.getCreationTimestamp());
			LOGGER.info("Inserting state code in plan table ");
			plan.setState(issuer.getStateOfDomicile());

			// plan = iPlanRepository.save(plan);
			/*if (null != entityManager && entityManager.isOpen()) {
				plan = entityManager.merge(plan);
				LOGGER.info("Plan saved with basic information. Plan id is = " + plan.getId());
			}*/
			
			if(isPUFPlan) {
				//HIX-26894
				plan.setStatus(PlanStatus.CERTIFIED.toString());
			} else {
				plan.setStatus(PlanStatus.LOADED.toString());
			}

			if(null != currentPBVO.getPlanAttributes().getFormularyID()) {
				plan.setFormularlyId(validatePlanFormularyId(applicableYear, currentPBVO.getPlanAttributes().getFormularyID().getCellValue(), formulary, issuer.getId()));
			}

			//Plan Attributes
			if(null != currentPBVO.getPlanAttributes().getPlanType() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getPlanType().getCellValue())) {
				plan.setNetworkType(currentPBVO.getPlanAttributes().getPlanType().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getMetalLevel() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getMetalLevel().getCellValue())) {
				planHealth.setPlanLevel(currentPBVO.getPlanAttributes().getMetalLevel().getCellValue().replaceAll(SerffConstants.SPACE, StringUtils.EMPTY).toUpperCase());
			}
			//New attribute from 2017 template V6
			if(currentPBVO.isNewVersion() && null != currentPBVO.getPlanAttributes().getPlanDesignType() && 
					null != currentPBVO.getPlanAttributes().getPlanDesignType().getCellValue()) {
				planHealth.setPlanDesignType(StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getPlanDesignType().getCellValue()) ? currentPBVO.getPlanAttributes().getPlanDesignType().getCellValue() : "No");
			}

			if(null != currentPBVO.getPlanAttributes().getUniquePlanDesign() && null != currentPBVO.getPlanAttributes().getUniquePlanDesign().getCellValue()) {
				planHealth.setUniquePlanDesign(StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getUniquePlanDesign().getCellValue()) ? currentPBVO.getPlanAttributes().getUniquePlanDesign().getCellValue() : "No");
			}

			if(null != currentPBVO.getPlanAttributes().getQhpOrNonQhp() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getQhpOrNonQhp().getCellValue())) {
				planHealth.setIsQHP(currentPBVO.getPlanAttributes().getQhpOrNonQhp().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getInsurancePlanPregnancyNoticeReqInd() && null != currentPBVO.getPlanAttributes().getInsurancePlanPregnancyNoticeReqInd().getCellValue()) {
				planHealth.setPregnancyNotice(StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getInsurancePlanPregnancyNoticeReqInd().getCellValue()) ? currentPBVO.getPlanAttributes().getInsurancePlanPregnancyNoticeReqInd().getCellValue() : "No");
			}

			if(null != currentPBVO.getPlanAttributes().getIsSpecialistReferralRequired() && null != currentPBVO.getPlanAttributes().getIsSpecialistReferralRequired().getCellValue()) {
				planHealth.setSpecialistReferal(StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getIsSpecialistReferralRequired().getCellValue()) ? currentPBVO.getPlanAttributes().getIsSpecialistReferralRequired().getCellValue() : "No");
			}

			if(null != currentPBVO.getPlanAttributes().getInsurancePlanBenefitExclusionText() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getInsurancePlanBenefitExclusionText().getCellValue())) {
				String exclusions = StringUtils.trimToEmpty(currentPBVO.getPlanAttributes().getInsurancePlanBenefitExclusionText().getCellValue());
				if(exclusions.length() > BENEFITS_EXCLUSION_TEXT_LEN) {
					LOGGER.warn("Truncating Plan Benefit Exclusion Text to " + BENEFITS_EXCLUSION_TEXT_LEN + " characters: " + exclusions);
					exclusions = exclusions.substring(0, BENEFITS_EXCLUSION_TEXT_LEN);
				}
				planHealth.setPlanLevelExclusions(exclusions);
			}
			
			//2016 plans
			if(null != costShareVarianceVO.getHsa()) {
				plan.setHsa(StringUtils.isNotBlank(costShareVarianceVO.getHsa().getHsaEligibility().getCellValue()) ? costShareVarianceVO.getHsa().getHsaEligibility().getCellValue() : "No");
			}

			if(null != currentPBVO.getPlanAttributes().getOutOfCountryCoverage() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getOutOfCountryCoverage().getCellValue())){
				planHealth.setOutOfCountryCoverage(currentPBVO.getPlanAttributes().getOutOfCountryCoverage().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getOutOfCountryCoverageDescription() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getOutOfCountryCoverageDescription().getCellValue())){
				String outOfCountryCoverageDesc = StringUtils.trimToEmpty(currentPBVO.getPlanAttributes().getOutOfCountryCoverageDescription().getCellValue());
				if(outOfCountryCoverageDesc.length() > OUT_OF_AREA_COVERAGE_DESC_LEN) {
					LOGGER.warn("Truncating Out Of Country Coverage Description Text to " + OUT_OF_AREA_COVERAGE_DESC_LEN + " characters: " + outOfCountryCoverageDesc);
					outOfCountryCoverageDesc = outOfCountryCoverageDesc.substring(0, OUT_OF_AREA_COVERAGE_DESC_LEN);
				}
				planHealth.setOutOfCountryCoverageDesc(outOfCountryCoverageDesc);
			}

			if(null != currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverage() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverage().getCellValue())){
				planHealth.setOutOfServiceAreaCoverage(currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverage().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverageDescription() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverageDescription().getCellValue())){
				String outOfSvcAreaCoverageDesc = StringUtils.trimToEmpty(currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverageDescription().getCellValue());
				if(outOfSvcAreaCoverageDesc.length() > OUT_OF_AREA_COVERAGE_DESC_LEN) {
					LOGGER.warn("Truncating Out Of Service Area Coverage Description Text to " + OUT_OF_AREA_COVERAGE_DESC_LEN + " characters: " + outOfSvcAreaCoverageDesc);
					outOfSvcAreaCoverageDesc = outOfSvcAreaCoverageDesc.substring(0, OUT_OF_AREA_COVERAGE_DESC_LEN);
				}
				planHealth.setOutOfServiceAreaCoverageDesc(outOfSvcAreaCoverageDesc);
			}

			if(null != currentPBVO.getPlanAttributes().getNationalNetwork() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getNationalNetwork().getCellValue())){
				planHealth.setNationalNetwork(currentPBVO.getPlanAttributes().getNationalNetwork().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getChildOnlyOffering() && null != currentPBVO.getPlanAttributes().getChildOnlyOffering().getCellValue()) {
				
				String childOffering = currentPBVO.getPlanAttributes().getChildOnlyOffering().getCellValue();
				plan.setAvailableFor(getAvailableFor(childOffering));
				planHealth.setChildOnlyOffering(childOffering);
			}

			if(null != currentPBVO.getPlanAttributes().getIsWellnessProgramOffered() && null != currentPBVO.getPlanAttributes().getIsWellnessProgramOffered().getCellValue()) {
				planHealth.setWellnessProgramOffered(StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getIsWellnessProgramOffered().getCellValue()) ? currentPBVO.getPlanAttributes().getIsWellnessProgramOffered().getCellValue() : "No");
			}

			if(null != currentPBVO.getPlanAttributes().getIsDiseaseMgmtProgramsOffered() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getIsDiseaseMgmtProgramsOffered().getCellValue())) {
				planHealth.setDiseaseMgmtProgramOffered(currentPBVO.getPlanAttributes().getIsDiseaseMgmtProgramsOffered().getCellValue());
			}

			Long lValue = null;
			String maxCoinsuranceForSPDrug = null;
			String maxNumDaysForChargingInpatientCopay = null;
			String prmryCareCostSharingAfterSetNumberVisits = null;
			String prmryCareDedtAftrCopay = null;
			
			if(currentPBVO.isNewVersion()) {
				if(null != costShareVarianceVO && null != costShareVarianceVO.getAvCalculator()) {
					if(null != costShareVarianceVO.getAvCalculator().getMaximumCoinsuranceForSpecialtyDrugs()
							&& StringUtils.isNotBlank(costShareVarianceVO.getAvCalculator().getMaximumCoinsuranceForSpecialtyDrugs().getCellValue())){
						maxCoinsuranceForSPDrug = costShareVarianceVO.getAvCalculator().getMaximumCoinsuranceForSpecialtyDrugs().getCellValue().trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
					}				
					if(null != costShareVarianceVO.getAvCalculator().getMaxNumDaysForChargingInpatientCopay()
							&& StringUtils.isNotBlank(costShareVarianceVO.getAvCalculator().getMaxNumDaysForChargingInpatientCopay().getCellValue())){
						maxNumDaysForChargingInpatientCopay = costShareVarianceVO.getAvCalculator().getMaxNumDaysForChargingInpatientCopay().getCellValue().trim();
					}
					if(null != costShareVarianceVO.getAvCalculator().getBeginPrimaryCareCostSharingAfterSetNumberVisits()
							&& StringUtils.isNotBlank(costShareVarianceVO.getAvCalculator().getBeginPrimaryCareCostSharingAfterSetNumberVisits().getCellValue())){
						prmryCareCostSharingAfterSetNumberVisits = costShareVarianceVO.getAvCalculator().getBeginPrimaryCareCostSharingAfterSetNumberVisits().getCellValue().trim();
					}
					if(null != costShareVarianceVO.getAvCalculator().getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays()
							&& StringUtils.isNotBlank(costShareVarianceVO.getAvCalculator().getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays().getCellValue())){
						prmryCareDedtAftrCopay = costShareVarianceVO.getAvCalculator().getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays().getCellValue().trim();
					}
				}
			} else {
				if(null != currentPBVO.getPlanAttributes().getMaximumCoinsuranceForSpecialtyDrugs()
						&& StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getMaximumCoinsuranceForSpecialtyDrugs().getCellValue())){
					maxCoinsuranceForSPDrug = currentPBVO.getPlanAttributes().getMaximumCoinsuranceForSpecialtyDrugs().getCellValue().trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
				}				
				if(null != currentPBVO.getPlanAttributes().getMaxNumDaysForChargingInpatientCopay()
						&& StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getMaxNumDaysForChargingInpatientCopay().getCellValue())){
					maxNumDaysForChargingInpatientCopay = currentPBVO.getPlanAttributes().getMaxNumDaysForChargingInpatientCopay().getCellValue().trim();
				}
				if(null != currentPBVO.getPlanAttributes().getBeginPrimaryCareCostSharingAfterSetNumberVisits()
						&& StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getBeginPrimaryCareCostSharingAfterSetNumberVisits().getCellValue())){
					prmryCareCostSharingAfterSetNumberVisits = currentPBVO.getPlanAttributes().getBeginPrimaryCareCostSharingAfterSetNumberVisits().getCellValue().trim();
				}
				if(null != currentPBVO.getPlanAttributes().getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays()
						&& StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays().getCellValue())){
					prmryCareDedtAftrCopay = currentPBVO.getPlanAttributes().getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays().getCellValue().trim();
				}
			}
			
			if(StringUtils.isNumeric(maxCoinsuranceForSPDrug)) {
				lValue = Long.parseLong(maxCoinsuranceForSPDrug);
			}
			planHealth.setMaxCoinsuranceForSPDrug(lValue);
			lValue = null;
			
			if(StringUtils.isNumeric(maxNumDaysForChargingInpatientCopay)) {
				lValue = Long.parseLong(maxNumDaysForChargingInpatientCopay);
			}
			planHealth.setMaxDaysForChargingInpatientCopay(lValue);
			lValue = null;
			
			if(StringUtils.isNumeric(prmryCareCostSharingAfterSetNumberVisits)){
				lValue = Long.parseLong(prmryCareCostSharingAfterSetNumberVisits);
			}
			planHealth.setBeginPmryCareCsAfterNumOfVisits(lValue);
			lValue = null;
			
			if(StringUtils.isNumeric(prmryCareDedtAftrCopay)){
				lValue = Long.parseLong(prmryCareDedtAftrCopay);
			}
			planHealth.setBeginPmrycareDedtOrCoinsAfterNumOfCopays(lValue);

			planHealth.setPlan(plan);
			// planHealth = iSerffPlanHealthRepository.save(planHealth);
			/*if (null != entityManager && entityManager.isOpen()) {
				planHealth = entityManager.merge(planHealth);
				LOGGER.info("PlanHealth saved with basic information. Plan id is = " + plan.getId());
			}*/

			// Saving documents details 
			if (!CollectionUtils.isEmpty(documents)) {

				SupportingDocumentDTO documentDTO = getSupportingDocument(documents, issuerPlanNumberWithVariant, issuerPlanNumberWithoutVariant);
				plan.setEocDocName(documentDTO.getEocDocumentName());
				plan.setEocDocId(documentDTO.getEocDocumentId());

				plan.setBrochureDocName(documentDTO.getBrochureDocumentName());
				plan.setBrochureUCmId(documentDTO.getBrochureDocumentId());

				planHealth.setSbcDocName(documentDTO.getSbcDocumentName());
				planHealth.setSbcUcmId(documentDTO.getSbcDocumentId());

				planHealth.setBenefitFile(documentDTO.getBenefitFile());
				planHealth.setRateFile(documentDTO.getRateFile());
			}

			//Plan Dates
			try {
				if(null != currentPBVO.getPlanAttributes().getPlanEffectiveDate() &&  null != currentPBVO.getPlanAttributes().getPlanEffectiveDate().getCellValue()) {
					plan.setStartDate(dateFormat.parse(currentPBVO.getPlanAttributes().getPlanEffectiveDate().getCellValue()));
					// planHealth.setEffectiveStartDate(formatter.parse(currentPBVO.getPlanAttributes().getPlanEffectiveDate().getCellValue()));
					planHealth.setBenefitEffectiveDate(dateFormat.parse(currentPBVO.getPlanAttributes().getPlanEffectiveDate().getCellValue()));
					
					String expirationDateString = EXPIRATION_DATE + applicableYear;
					Date expirationDate = dateFormat.parse(expirationDateString);
					LOGGER.info("Expiration Date of Plan : " + expirationDate);
					
					plan.setEndDate(expirationDate);
					//planHealth.setEffectiveEndDate(expirationDate);
					plan.setEnrollmentAvailEffDate(plan.getStartDate());

				} else {
					plan.setEnrollmentAvailEffDate(new Date());
				}
				if(isPUFPlan) {
					//HIX-26894
					plan.setEnrollmentAvail(Plan.EnrollmentAvail.AVAILABLE.toString());
				} else {
					plan.setEnrollmentAvail(Plan.EnrollmentAvail.NOTAVAILABLE.toString());
				}
			}
			catch (Exception e) {
				LOGGER.error("Exception occurred while parsing date: Exception:" + e.getMessage(), e);
				throw new GIException(Integer.parseInt(SerffConstants.UNEXPECTED_GENERAL_ERROR_CODE), SerffConstants.UNEXPECTED_GENERAL_ERROR_MESSAGE + " : Exception occurred while parsing date. " , StringUtils.EMPTY);
			}

			//planBenefitTemplate.getPlans().get(0).getPlanAttributes().getPlanEffectiveDate().getCellValue());

			//URLs
			//2016 plans
			if(null != costShareVarianceVO.getUrl()) {
				 url =checkAndcorrectURL(costShareVarianceVO.getUrl().getSummaryBenefitAndCoverageURL());
				 planHealth.setBenefitsUrl(url);
			}
			
			if(null != currentPBVO.getPlanAttributes().getEnrollmentPaymentURL() && null != currentPBVO.getPlanAttributes().getEnrollmentPaymentURL().getCellValue()) {
				 url =checkAndcorrectURL(currentPBVO.getPlanAttributes().getEnrollmentPaymentURL());
				planHealth.setEnrollmentUrl(url);
			}

		}

		boolean isCompletePlan = checkIfBenefitsExistForCompletePlan(planAndBenefitsPackageVO.getBenefitsList()) ;
		//	currentPlanCSVs = currentPBVO.getCostShareVariancesList().getCostShareVariance();


				if(null != costShareVarianceVO) {
					//Cost Sharing.
					/*if(null != costShareVarianceVO.getCsrVariationType() && null != costShareVarianceVO.getCsrVariationType().getCellValue() && null != costShareVarianceVO.getCsrVariationType().getCellValue()) {
						//planHealth.setCostSharing(costShareVarianceVO.getCsrVariationType().getCellValue());
					}*/
					if(currentPBVO.isNewVersion()) {
						AvCalculatorVO avCalc = costShareVarianceVO.getAvCalculator();
						if(null != avCalc) {
							if(null != avCalc.getIssuerActuarialValue() && StringUtils.isNotBlank(avCalc.getIssuerActuarialValue().getCellValue())) {
								planHealth.setActurialValueCertificate(avCalc.getIssuerActuarialValue().getCellValue());
							}
		
							if(null != avCalc.getAvCalculatorOutputNumber() && StringUtils.isNotBlank(avCalc.getAvCalculatorOutputNumber().getCellValue())) {
								planHealth.setAvCalcOutputNumber(avCalc.getAvCalculatorOutputNumber().getCellValue());
							}
						} else {
							LOGGER.info("Missing AvCalculator block in new template for CSV " + costShareVarianceVO.getPlanId().getCellValue());
						}
					} else {
						if(null != costShareVarianceVO.getIssuerActuarialValue() && StringUtils.isNotBlank(costShareVarianceVO.getIssuerActuarialValue().getCellValue())) {
							planHealth.setActurialValueCertificate(costShareVarianceVO.getIssuerActuarialValue().getCellValue());
						}
	
						if(null != costShareVarianceVO.getAvCalculatorOutputNumber() && StringUtils.isNotBlank(costShareVarianceVO.getAvCalculatorOutputNumber().getCellValue())) {
							planHealth.setAvCalcOutputNumber(costShareVarianceVO.getAvCalculatorOutputNumber().getCellValue());
						}
					}

					if(null != costShareVarianceVO.getMultipleProviderTiers() && StringUtils.isNotBlank(costShareVarianceVO.getMultipleProviderTiers().getCellValue())) {
						multipleTierNtwrk= costShareVarianceVO.getMultipleProviderTiers().getCellValue();
						planHealth.setMultipleNetworkTiers(multipleTierNtwrk);
						
					}

					if(null != costShareVarianceVO.getFirstTierUtilization() && StringUtils.isNotBlank(costShareVarianceVO.getFirstTierUtilization().getCellValue())) {
						planHealth.setTier1Util(costShareVarianceVO.getFirstTierUtilization().getCellValue());
					}

					if(null != costShareVarianceVO.getSecondTierUtilization() && StringUtils.isNotBlank(costShareVarianceVO.getSecondTierUtilization().getCellValue())) {
						planHealth.setTier2Util(costShareVarianceVO.getSecondTierUtilization().getCellValue());
					}

					if(null != costShareVarianceVO.getMedicalAndDrugDeductiblesIntegrated() && StringUtils.isNotBlank(costShareVarianceVO.getMedicalAndDrugDeductiblesIntegrated().getCellValue())) {
						planHealth.setIsMedDrugDeductibleIntegrated(costShareVarianceVO.getMedicalAndDrugDeductiblesIntegrated().getCellValue());
					}

					if(null != costShareVarianceVO.getMedicalAndDrugMaxOutOfPocketIntegrated() && StringUtils.isNotBlank(costShareVarianceVO.getMedicalAndDrugMaxOutOfPocketIntegrated().getCellValue())) {
						planHealth.setIsMedDrugMaxOutPocketIntegrated(costShareVarianceVO.getMedicalAndDrugMaxOutOfPocketIntegrated().getCellValue());
					}
					
					if(isCompletePlan){
						planHealth.setEhbCovered(SerffConstants.COMPLETED);
					}else{
						planHealth.setEhbCovered(SerffConstants.PARTIAL);
					}
					//saving plan SBC details

					plansbcScenario = new PlanSbcScenario();
					if(null!=costShareVarianceVO.getSbc()){

						if( null!=costShareVarianceVO.getSbc().getHavingBabyCoInsurance()){
							String babyCoinsurance =costShareVarianceVO.getSbc().getHavingBabyCoInsurance().getCellValue();
							if(StringUtils.isNotBlank(babyCoinsurance)){
								plansbcScenario.setBabyCoinsurance(Integer.parseInt(babyCoinsurance.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
							}
						}
						if(null!=costShareVarianceVO.getSbc().getHavingBabyCoPayment()){
							String babyCopayment = costShareVarianceVO.getSbc().getHavingBabyCoPayment().getCellValue();
							if(StringUtils.isNotBlank(babyCopayment)){
								plansbcScenario.setBabyCopayment(Integer.parseInt(babyCopayment.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
							}
						}
						if(null!=costShareVarianceVO.getSbc().getHavingBabyDeductible()){
							String babyDeductible = costShareVarianceVO.getSbc().getHavingBabyDeductible().getCellValue();
							if(StringUtils.isNotBlank(babyDeductible)){
								plansbcScenario.setBabyDeductible(Integer.parseInt(babyDeductible.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
							}
						}
						if(null!=costShareVarianceVO.getSbc().getHavingBabyLimit()){
							String babyLimit = costShareVarianceVO.getSbc().getHavingBabyLimit().getCellValue();
							if(StringUtils.isNotBlank(babyLimit)){
								plansbcScenario.setBabyLimit(Integer.parseInt(babyLimit.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
							}
						}
						if(null!=costShareVarianceVO.getSbc().getHavingDiabetesCoInsurance()){
							String diabetesCoinsurance = costShareVarianceVO.getSbc().getHavingDiabetesCoInsurance().getCellValue();
							if(StringUtils.isNotBlank(diabetesCoinsurance)){
								plansbcScenario.setDiabetesCoinsurance(Integer.parseInt(diabetesCoinsurance.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
							}
						}
						if(null!=costShareVarianceVO.getSbc() && null!=costShareVarianceVO.getSbc().getHavingDiabetesCopay()){
							String diabetesCopay = costShareVarianceVO.getSbc().getHavingDiabetesCopay().getCellValue();
							if(StringUtils.isNotBlank(diabetesCopay)){
								plansbcScenario.setDiabetesCopay(Integer.parseInt(diabetesCopay.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
							}
						}
						if(null!=costShareVarianceVO.getSbc().getHavingDiabetesDeductible()){
							String diabetesDeductible = costShareVarianceVO.getSbc().getHavingDiabetesDeductible().getCellValue();
							if(StringUtils.isNotBlank(diabetesDeductible)){
								plansbcScenario.setDiabetesDeductible(Integer.parseInt(diabetesDeductible.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
							}
						}
						if(null!=costShareVarianceVO.getSbc().getHavingDiabetesLimit()){
							String diabetesLimit = costShareVarianceVO.getSbc().getHavingDiabetesLimit().getCellValue();
							if(StringUtils.isNotBlank(diabetesLimit)){
								plansbcScenario.setDiabetesLimit(Integer.parseInt(diabetesLimit.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
							}
						}
						//New attribute from 2017 template V6
						if(currentPBVO.isNewVersion()) {
							String simpleFractureValue;
							if(null!=costShareVarianceVO.getSbc().getHavingSimplefractureCoInsurance()){
								simpleFractureValue = costShareVarianceVO.getSbc().getHavingSimplefractureCoInsurance().getCellValue();
								if(StringUtils.isNotBlank(simpleFractureValue)){
									plansbcScenario.setFractureCoinsurance(Integer.parseInt(simpleFractureValue.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
								}
							}
							if(null!=costShareVarianceVO.getSbc().getHavingSimplefractureCopayment()){
								simpleFractureValue = costShareVarianceVO.getSbc().getHavingSimplefractureCopayment().getCellValue();
								if(StringUtils.isNotBlank(simpleFractureValue)){
									plansbcScenario.setFractureCopay(Integer.parseInt(simpleFractureValue.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
								}
							}
							if(null!=costShareVarianceVO.getSbc().getHavingSimplefractureDeductible()){
								simpleFractureValue = costShareVarianceVO.getSbc().getHavingSimplefractureDeductible().getCellValue();
								if(StringUtils.isNotBlank(simpleFractureValue)){
									plansbcScenario.setFractureDeductible(Integer.parseInt(simpleFractureValue.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
								}
							}
							if(null!=costShareVarianceVO.getSbc().getHavingSimplefractureLimit()){
								simpleFractureValue = costShareVarianceVO.getSbc().getHavingSimplefractureLimit().getCellValue();
								if(StringUtils.isNotBlank(simpleFractureValue)){
									plansbcScenario.setFractureLimit(Integer.parseInt(simpleFractureValue.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA,  StringUtils.EMPTY).replaceAll(SerffConstants.DECIMAL_POINT_AND_ANYTHING_ELSE,  StringUtils.EMPTY)));
								}
							}
						}
 					}

					if (null != entityManager && entityManager.isOpen()) {
						PlanSbcScenario plansbcScenarioObj = entityManager.merge(plansbcScenario);
						planHealth.setPlanSbcScenarioId(plansbcScenarioObj);
					}
					//Maximum Out of Pocket for Medical and Drug EHB Benefits (Total)
					MoopListVO  moopListObj = costShareVarianceVO.getMoopList();
					Map<String,String> healthCosts = serffUtils.getPropertyList(PlanmgmtConfigurationEnum.HEALTH_COSTS_LIST);
					if(null!=moopListObj && null!= moopListObj.getMoop()){
					moopVOList = moopListObj.getMoop();

						for(MoopVO moopVO: moopVOList){

							if(null!=moopVO){
							if(null != moopVO.getName() && StringUtils.isNotBlank(moopVO.getName().getCellValue())) {
								moopName =moopVO.getName().getCellValue();
								if(LOGGER.isDebugEnabled()) {
									LOGGER.debug("Health Cost Name " + SecurityUtil.sanitizeForLogging(moopName));
								}
							}
							if(null == moopName || null == healthCosts.get(moopName.toLowerCase())) {
								if(LOGGER.isInfoEnabled()) {
									LOGGER.info("Skipping Plan Health Cost : No mapping found for moop " + SecurityUtil.sanitizeForLogging(moopName));
								}
								continue;
							}
							planHealthCost = new PlanHealthCost();
							planHealthCost.setName(healthCosts.get(moopName.toLowerCase()));

							if(null != moopVO.getInNetworkTier1IndividualAmount() && StringUtils.isNotBlank(moopVO.getInNetworkTier1IndividualAmount().getCellValue())) {
								inNetTier1IndAmnt = moopVO.getInNetworkTier1IndividualAmount().getCellValue();
								inNetTier1IndAmnt = inNetTier1IndAmnt.replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).trim();

								if (validateAmount(inNetTier1IndAmnt.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
									planHealthCost.setInNetWorkInd(Double.parseDouble(inNetTier1IndAmnt.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
								}
							}
							
							if(null != moopVO.getInNetworkTier1FamilyAmount() && null !=moopVO.getInNetworkTier1FamilyAmount().getCellValue()) {
								
								String[] inNetTier1Values = getAmountValues(moopVO.getInNetworkTier1FamilyAmount().getCellValue());
								
								if(null != inNetTier1Values){
									if (validateAmount(inNetTier1Values[0])) {
										planHealthCost.setInNetWorkFly(Double.parseDouble(inNetTier1Values[0]));
									}
									
									if (validateAmount(inNetTier1Values[1])) {
										planHealthCost.setInNetworkFlyPerPerson(Double.parseDouble(inNetTier1Values[1]));
									}
								}
							
							}
							
						/*	if multiple tier network = yes*/
							if(YES.equalsIgnoreCase(multipleTierNtwrk)){
								if(null != moopVO.getInNetworkTier2IndividualAmount() && StringUtils.isNotBlank(moopVO.getInNetworkTier2IndividualAmount().getCellValue())) {
									inNetTier2IndAmnt= moopVO.getInNetworkTier2IndividualAmount().getCellValue();
									inNetTier2IndAmnt=inNetTier2IndAmnt.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
									
									if (validateAmount(inNetTier2IndAmnt)) {
										planHealthCost.setInNetworkTier2Ind(Double.parseDouble(inNetTier2IndAmnt));
									}	
								}
								
								if(null != moopVO.getInNetworkTier2FamilyAmount() && null !=moopVO.getInNetworkTier2FamilyAmount().getCellValue()) {
									
									String[] inNetTier2AmntValues = getAmountValues(moopVO.getInNetworkTier2FamilyAmount().getCellValue());
									
									if(null != inNetTier2AmntValues){
										
										if (validateAmount(inNetTier2AmntValues[0])) {
											planHealthCost.setInNetworkTier2Fly(Double.parseDouble(inNetTier2AmntValues[0]));
										}
										
										if (validateAmount(inNetTier2AmntValues[1])) {
											planHealthCost.setInNetworkTier2FlyPerPerson(Double.parseDouble(inNetTier2AmntValues[1]));
										}
									}
									
								}
							}
								
							if(null != moopVO.getOutOfNetworkIndividualAmount() && StringUtils.isNotBlank(moopVO.getOutOfNetworkIndividualAmount().getCellValue())) {
								outNetIndAmnt = moopVO.getOutOfNetworkIndividualAmount().getCellValue();
								outNetIndAmnt = outNetIndAmnt.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);

								if (validateAmount(outNetIndAmnt)) {
									planHealthCost.setOutNetworkInd(Double.parseDouble(outNetIndAmnt));
								}
							}
							
							if(null != moopVO.getOutOfNetworkFamilyAmount() && null !=moopVO.getOutOfNetworkFamilyAmount().getCellValue()) {
								
								String[] outOfNetworkAmountValues = getAmountValues(moopVO.getOutOfNetworkFamilyAmount().getCellValue());
								
								if(null != outOfNetworkAmountValues){
									
									if (validateAmount(outOfNetworkAmountValues[0])) {
										planHealthCost.setOutNetworkFly(Double.parseDouble(outOfNetworkAmountValues[0]));
									}
									
									if (validateAmount(outOfNetworkAmountValues[1])) {
										planHealthCost.setOutNetworkFlyPerPerson(Double.parseDouble(outOfNetworkAmountValues[1]));
									}
								}
								
							}
								
							if(null != moopVO.getCombinedInOutNetworkIndividualAmount() && StringUtils.isNotBlank(moopVO.getCombinedInOutNetworkIndividualAmount().getCellValue())) {
								combinedInOutNetIndAmnt = moopVO.getCombinedInOutNetworkIndividualAmount().getCellValue();
								combinedInOutNetIndAmnt = combinedInOutNetIndAmnt.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);

								if (validateAmount(combinedInOutNetIndAmnt)) {
									planHealthCost.setCombinedInOutNetworkInd(Double.parseDouble(combinedInOutNetIndAmnt));
								}
							}
							
							if(null != moopVO.getCombinedInOutNetworkFamilyAmount() && StringUtils.isNotBlank(moopVO.getCombinedInOutNetworkFamilyAmount().getCellValue())) {
								
								String[] combinedInOutNetworkAmountValues = getAmountValues(moopVO.getCombinedInOutNetworkFamilyAmount().getCellValue());
								
								if(null != combinedInOutNetworkAmountValues){
									
									if (validateAmount(combinedInOutNetworkAmountValues[0])) {
										planHealthCost.setCombinedInOutNetworkFly(Double.parseDouble(combinedInOutNetworkAmountValues[0]));
									}
									
									if (validateAmount(combinedInOutNetworkAmountValues[1])) {
										planHealthCost.setCombinedInOutNetworkFlyPerPerson(Double.parseDouble(combinedInOutNetworkAmountValues[1]));
									}
								}
								
							}
								
							planHealthCost.setPlanHealth(planHealth);
							planHealthCostList.add(planHealthCost);
						}
					}
				}
					//Combined Medical and Drug EHB Deductible
					PlanDeductibleListVO  planDeductibleListObj = costShareVarianceVO.getPlanDeductibleList();
					if(null!=planDeductibleListObj && null!=planDeductibleListObj.getPlanDeductible()){
					planDeductibleVOListObj = planDeductibleListObj.getPlanDeductible();
					String deductibleName = null;
					int customDeductibleCount = 0;
						for(PlanDeductibleVO planDeductibleVO: planDeductibleVOListObj){
							deductibleName = null;

							if(null!=planDeductibleVO && null !=planDeductibleVO.getDeductibleType() && StringUtils.isNotBlank(planDeductibleVO.getDeductibleType().getCellValue())) {
								deductibleType = planDeductibleVO.getDeductibleType().getCellValue();
								if(LOGGER.isDebugEnabled()) {
									LOGGER.debug("Deductible type " + SecurityUtil.sanitizeForLogging(deductibleType));
								}
							}
							/*if(null == deductibleType || null == healthCosts.get(deductibleType.toLowerCase())) {
								LOGGER.info("Skipping Plan Health Cost : No mapping found for Deductible type " + deductibleType);
								continue;
							}*/
							if(null == deductibleType) {
								LOGGER.info("Skipping Plan Health Cost : deductibleType is null");
								continue;
							}
							
							deductibleName = healthCosts.get(deductibleType.toLowerCase());
							if(null == deductibleName) {
								customDeductibleCount++;
								if(customDeductibleCount <= MAX_CUSTOM_DEDUCTIBLE_COUNT) {
									deductibleName = deductibleType;
									if(LOGGER.isInfoEnabled()) {
										LOGGER.info(NO_DEDUCTIBLE_MAP + customDeductibleCount + "] "+ SecurityUtil.sanitizeForLogging(deductibleType));
									}
								} else {
									if(LOGGER.isInfoEnabled()) {
										LOGGER.info("Skipping supplied Deductible name as count exceeds limit: No mapping found for Deductible type[" + customDeductibleCount + "] "+ SecurityUtil.sanitizeForLogging(deductibleType));
									}
									continue;
								}
							}
							planHealthCost = new PlanHealthCost();
							planHealthCost.setName(deductibleName);
							
							if(null !=planDeductibleVO.getInNetworkTier1Individual() && StringUtils.isNotBlank(planDeductibleVO.getInNetworkTier1Individual().getCellValue())) {
								inNetTier1Ind = planDeductibleVO.getInNetworkTier1Individual().getCellValue();
								inNetTier1Ind = inNetTier1Ind.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);

								if (validateAmount(inNetTier1Ind)) {
									planHealthCost.setInNetWorkInd(Double.parseDouble(inNetTier1Ind));
								}
							}
							
							
							if(null !=planDeductibleVO.getInNetworkTier1Family() && null != planDeductibleVO.getInNetworkTier1Family().getCellValue()) {
								
								String[] inNetworkTier1FamilyValues = getAmountValues(planDeductibleVO.getInNetworkTier1Family().getCellValue());
								
								if(null != inNetworkTier1FamilyValues){
									
									if (validateAmount(inNetworkTier1FamilyValues[0])) {
										planHealthCost.setInNetWorkFly(Double.parseDouble(inNetworkTier1FamilyValues[0]));
									}
									
									if (validateAmount(inNetworkTier1FamilyValues[1])) {
										planHealthCost.setInNetworkFlyPerPerson(Double.parseDouble(inNetworkTier1FamilyValues[1]));
									}
								}
								
							}

							if(YES.equalsIgnoreCase(multipleTierNtwrk)){
								if(null != planDeductibleVO.getInNetworkTierTwoIndividual() && StringUtils.isNotBlank(planDeductibleVO.getInNetworkTierTwoIndividual().getCellValue())) {
									inNetTier2IndAmnt= planDeductibleVO.getInNetworkTierTwoIndividual().getCellValue();
									inNetTier2IndAmnt=inNetTier2IndAmnt.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
									
									if (validateAmount(inNetTier2IndAmnt)) {
										planHealthCost.setInNetworkTier2Ind(Double.parseDouble(inNetTier2IndAmnt));
									}	
								}
								
								if(null != planDeductibleVO.getInNetworkTierTwoFamily()&& null != planDeductibleVO.getInNetworkTierTwoFamily().getCellValue()) {
									
									String[] inNetworkTierTwoFamilyValues = getAmountValues(planDeductibleVO.getInNetworkTierTwoFamily().getCellValue());
									
									if(null != inNetworkTierTwoFamilyValues){
										
										if (validateAmount(inNetworkTierTwoFamilyValues[0])) {
											planHealthCost.setInNetworkTier2Fly(Double.parseDouble(inNetworkTierTwoFamilyValues[0]));
										}
										
										if (validateAmount(inNetworkTierTwoFamilyValues[1])) {
											planHealthCost.setInNetworkTier2FlyPerPerson(Double.parseDouble(inNetworkTierTwoFamilyValues[1]));
										}
									}
									
								}
							}
							
							
							if(null !=planDeductibleVO.getCoinsuranceInNetworkTier1() && StringUtils.isNotBlank(planDeductibleVO.getCoinsuranceInNetworkTier1().getCellValue())) {
								
								coinsuranceInNetTier1 = planDeductibleVO.getCoinsuranceInNetworkTier1().getCellValue();
								coinsuranceInNetTier1 = coinsuranceInNetTier1.replaceAll(SerffConstants.PERCENTAGE, StringUtils.EMPTY).trim();

								if (validateAmount(coinsuranceInNetTier1.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
									planHealthCost.setCombDefCoinsNetworkTier1(Double.parseDouble(coinsuranceInNetTier1.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
								}
							}
							if(null !=planDeductibleVO.getCoinsuranceInNetworkTier2() && StringUtils.isNotBlank(planDeductibleVO.getCoinsuranceInNetworkTier2().getCellValue())) {
								
								coinsuranceInNetTier2 = planDeductibleVO.getCoinsuranceInNetworkTier2().getCellValue();
								coinsuranceInNetTier2 = coinsuranceInNetTier2.replaceAll(SerffConstants.PERCENTAGE, StringUtils.EMPTY).trim();

								if (validateAmount(coinsuranceInNetTier2.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
									planHealthCost.setCombDefCoinsNetworkTier2(Double.parseDouble(coinsuranceInNetTier2.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
								}
							}
							if(null !=planDeductibleVO.getOutOfNetworkIndividual() && StringUtils.isNotBlank(planDeductibleVO.getOutOfNetworkIndividual().getCellValue())) {

								outOfNetInd = planDeductibleVO.getOutOfNetworkIndividual().getCellValue();
								outOfNetInd = outOfNetInd.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);

								if (validateAmount(outOfNetInd)) {
									planHealthCost.setOutNetworkInd(Double.parseDouble(outOfNetInd));
								}
							}
							
							if(null !=planDeductibleVO.getOutOfNetworkFamily() && StringUtils.isNotBlank(planDeductibleVO.getOutOfNetworkFamily().getCellValue())) {
								
								String[] outOfNetFamilyValues = getAmountValues(planDeductibleVO.getOutOfNetworkFamily().getCellValue());
								
								if(null != outOfNetFamilyValues){
									
									if (validateAmount(outOfNetFamilyValues[0])) {
										planHealthCost.setOutNetworkFly(Double.parseDouble(outOfNetFamilyValues[0]));
									}
									
									if (validateAmount(outOfNetFamilyValues[1])) {
										planHealthCost.setOutNetworkFlyPerPerson(Double.parseDouble(outOfNetFamilyValues[1]));
									}
								}
							
							}
								
							if(null !=planDeductibleVO.getCombinedInOrOutNetworkIndividual() && StringUtils.isNotBlank(planDeductibleVO.getCombinedInOrOutNetworkIndividual().getCellValue())) {

								combinedInOrOutNetInd = planDeductibleVO.getCombinedInOrOutNetworkIndividual().getCellValue();
								combinedInOrOutNetInd = combinedInOrOutNetInd.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);

								if (validateAmount(combinedInOrOutNetInd)) {
									planHealthCost.setCombinedInOutNetworkInd(Double.parseDouble(combinedInOrOutNetInd));
								}
							}
							
							if(null !=planDeductibleVO.getCombinedInOrOutNetworkFamily() && null != planDeductibleVO.getCombinedInOrOutNetworkFamily().getCellValue()) {
								
								String[] combinedInOrOutNetFamilyValues = getAmountValues(planDeductibleVO.getCombinedInOrOutNetworkFamily().getCellValue());
								
								if(null != combinedInOrOutNetFamilyValues){
									
									if (validateAmount(combinedInOrOutNetFamilyValues[0])) {
										planHealthCost.setCombinedInOutNetworkFly(Double.parseDouble(combinedInOrOutNetFamilyValues[0]));
									}
									
									if (validateAmount(combinedInOrOutNetFamilyValues[1])) {
										planHealthCost.setCombinedInOutNetworkFlyPerPerson(Double.parseDouble(combinedInOrOutNetFamilyValues[1]));
									}
								}
							
							}
							planHealthCost.setPlanHealth(planHealth);
							planHealthCostList.add(planHealthCost);
						}
					}
				}

				benefitCopayCoinsData = getBenefitCopayCoinsData(costShareVarianceVO);

					if(null != planAndBenefitsPackageVO.getBenefitsList() &&  null!=planAndBenefitsPackageVO.getBenefitsList().getBenefits()) {
						Map<String,String> healthBenefitNames = serffUtils.getPropertyList(PlanmgmtConfigurationEnum.HEALTH_BENEFITS_LIST);
						//String benefitValue;

						currentPlanBAs = planAndBenefitsPackageVO.getBenefitsList().getBenefits();
						planHealthBenefits = new ArrayList<PlanHealthBenefit>();
						PlanHealthBenefit planHealthBenefit = null;
						
						for(com.serff.template.plan.BenefitAttributeVO currentPlanBA: currentPlanBAs) {

							if(null != currentPlanBA) {
								//Benefit Information
								planHealthBenefit = createPlanHealthBenefit(currentPlanBA);
								String benefitName = healthBenefitNames.get(planHealthBenefit.getName().replaceAll(SerffConstants.COMMA, SerffConstants.DOT).toLowerCase());
								if(benefitName == null) {
									benefitName= getDerivedBenefitName(planHealthBenefit.getName());
									if(LOGGER.isInfoEnabled()) {
										LOGGER.info("No mapping found for Health Benefit name : " + SecurityUtil.sanitizeForLogging(currentPlanBA.getBenefitTypeCode().getCellValue()) + ". Using derived value " + benefitName);
									}
								}
								populateBenefitData(planHealthBenefit, benefitName, benefitCopayCoinsData, planHealth, planDispRulesMap);
								planHealthBenefit.setPlanHealth(planHealth);
								planHealthBenefit.setEffStartDate(plan.getStartDate());
								planHealthBenefit.setEffEndDate(plan.getEndDate());
								planHealthBenefit.setCreatedBy(issuer.getLastUpdatedBy());
								planHealthBenefit.setCreationTimestamp(issuer.getCreationTimestamp());
								planHealthBenefits.add(planHealthBenefit);
							}
						}
					}

					planHealth.setPlanHealthBenefitVO(planHealthBenefits);
					planHealth.setPlanHealthCostsList(planHealthCostList);


					if(healthParentId != -1){
						planHealth.setParentPlanId(healthParentId);
					}

					LOGGER.info("Processing of Rates begin PlanID: " + plan.getIssuerPlanNumber());
					String planId = StringUtils.EMPTY;
					if(null != plan.getIssuerPlanNumber()){
						planId = plan.getIssuerPlanNumber().substring(0, SerffConstants.LEN_HIOS_PLAN_ID);
						if(!CollectionUtils.isEmpty(planRates.get(planId))){
							List<PlanRate> rates = planRates.get(planId);
							List<PlanRate> ratesForPlan = new ArrayList<PlanRate>();
							for(PlanRate rate : rates){
								PlanRate tmpRate = rate.clone();
								tmpRate.setLastUpdatedBy(issuer.getLastUpdatedBy());
								tmpRate.setPlan(plan);
								ratesForPlan.add(tmpRate);
							}
							plan.setPlanRate(ratesForPlan);
							planHealth.setRateEffectiveDate(rates.get(0).getEffectiveStartDate());
						}
					}
					LOGGER.info("Processing of Rates ends PlanID: " + plan.getIssuerPlanNumber());

					planHealth.setPlan(plan);
					plan.setPlanHealth(planHealth);
					// plan = iPlanRepository.save(plan);
					if (null != entityManager && entityManager.isOpen()) {
						plan = entityManager.merge(plan);
						LOGGER.info("Health Plans saved successfully");	
						if(!CollectionUtils.isEmpty(tenantList)){
							serffUtils.saveTenantPlan(plan, tenantList, entityManager);	
							LOGGER.info("Saved tenant plan successfully.");	
						}						
						if(null != costShareVarianceVO  && healthParentId == -1 && costShareVarianceVO.getPlanId().getCellValue().endsWith("-01")){
							healthParentId = plan.getId();
						}
					}
					return plan;
				}

	private Plan savePlanDental(int applicableYear, EntityManager entityManager, PlanAndBenefitsPackageVO planAndBenefitsPackageVO, Issuer issuer,
			PlanAndBenefitsVO currentPBVO, com.serff.template.plan.CostShareVarianceVO costShareVarianceVO, List<IssuerServiceArea> issuerServiceAreas, 
			Map<String, Network> networks, Map<String, String> documents, Map<String, List<PlanRate>> planRates, Map<String, Formulary> formulary, 
			boolean isOffExchangePlan, boolean isPUFPlan, List<Tenant> tenantList, PlanDisplayRulesMap planDispRulesMap) throws GIException {
		
		Plan plan = null;
		PlanDental planDental = null;
		PlanDentalCost planDentalCost= null;

		List<PlanDentalCost>  planDentalCostList = null;
		List<PlanDentalBenefit> planDentalBenefits =null;
		List<com.serff.template.plan.MoopVO> moopVOList=null;
		List<com.serff.template.plan.PlanDeductibleVO> planDeductibleVOListObj =null;
		List<com.serff.template.plan.BenefitAttributeVO> currentPlanBAs = null;
		Map<String, List<Map<String,String>>> benefitCopayCoinsData = null;

		String moopName =null;
		String inNetTier1IndAmnt =null;
		String inNetTier1FamilyAmnt=null;
		String outNetIndAmnt=null;
		String outNetFamilyAmnt=null;
		String combinedInOutNetIndAmnt=null;
		String combinedInOutNetFamAmnt=null;
		String deductibleType=null;
		String inNetTier1Ind =null;
		String inNetTier1Family=null;
		String coinsuranceInNetTier1=null;
		String coinsuranceInNetTier2=null;
		String outOfNetInd=null;
		String outOfNetFamily=null;
		String combinedInOrOutNetInd=null;
		String combinedInOrOutNetFamily=null;
		String inNetTier2IndAmnt =null;
		String inNetTier2FamilyAmnt =null;
		String multipleTierNtwrk=null;
		String url =null;
		planDental = new PlanDental();
		plan = new Plan();
		plan.setIsDeleted(Plan.IS_DELETED.N.toString());
		if(isOffExchangePlan) {
			plan.setExchangeType(Plan.EXCHANGE_TYPE.OFF.toString());
		} else {
			plan.setExchangeType(Plan.EXCHANGE_TYPE.ON.toString());
		}
		if(isPUFPlan) {
			plan.setIsPUF(IS_PUF.Y);
		} else {
			plan.setIsPUF(IS_PUF.N);
		}
		plan.setApplicableYear(applicableYear);
		plan.setLastUpdatedBy(issuer.getLastUpdatedBy());
		planDental.setLastUpdatedBy(issuer.getLastUpdatedBy());
		
		planDentalCostList = new ArrayList<PlanDentalCost>();
		planDentalBenefits = new ArrayList<PlanDentalBenefit>();

		if(null != planAndBenefitsPackageVO.getHeader()) {
			if(null != planAndBenefitsPackageVO.getHeader().getMarketCoverage() && null != planAndBenefitsPackageVO.getHeader().getMarketCoverage().getCellValue()) {
				// plan.setMarket(planAndBenefitsPackageVO.getHeader().getMarketCoverage().getCellValue());
				String market = planAndBenefitsPackageVO.getHeader().getMarketCoverage().getCellValue();

				if(Plan.PlanMarket.INDIVIDUAL.toString().equalsIgnoreCase(market)) {
					plan.setMarket(Plan.PlanMarket.INDIVIDUAL.toString());
				}
				else {
					plan.setMarket(Plan.PlanMarket.SHOP.toString());
				}
			}
			plan.setInsuranceType(SerffConstants.DENTAL);
		}

		if(null != currentPBVO.getPlanAttributes()) {
			String issuerPlanNumberWithVariant = null;
			String issuerPlanNumberWithoutVariant = null;
		 	//Plan Identifiers
			if (null != costShareVarianceVO.getPlanId() && StringUtils.isNotBlank(costShareVarianceVO.getPlanId().getCellValue())) {
				issuerPlanNumberWithVariant = costShareVarianceVO.getPlanId().getCellValue();
				issuerPlanNumberWithoutVariant = issuerPlanNumberWithVariant.substring(SerffConstants.PLAN_INITIAL_INDEX, SerffConstants.LEN_HIOS_PLAN_ID);
				plan.setIssuerPlanNumber(issuerPlanNumberWithVariant.replaceAll(SerffConstants.HYPHEN, StringUtils.EMPTY));
				planDental.setCostSharing(getCSType(issuerPlanNumberWithVariant.substring(SerffConstants.LEN_HIOS_PLAN_ID + 1)));
			}

			if(null != costShareVarianceVO.getPlanVariantMarketingName() && null != costShareVarianceVO.getPlanVariantMarketingName().getCellValue()) {
				plan.setName(serffUtils.removeNewLines(costShareVarianceVO.getPlanVariantMarketingName().getCellValue()));
			} else if(null != currentPBVO.getPlanAttributes().getPlanMarketingName() && null != currentPBVO.getPlanAttributes().getPlanMarketingName().getCellValue()) {
				plan.setName(serffUtils.removeNewLines(currentPBVO.getPlanAttributes().getPlanMarketingName().getCellValue()));
			}

			if(null != currentPBVO.getPlanAttributes().getHiosProductID() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getHiosProductID().getCellValue())) {
				plan.setHiosProductId(currentPBVO.getPlanAttributes().getHiosProductID().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getHpid() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getHpid().getCellValue())) {
				plan.setHpid(currentPBVO.getPlanAttributes().getHpid().getCellValue());
			}

			/*if(null != currentPBVO.getPlanAttributes().getNetworkID() && null != currentPBVO.getPlanAttributes().getNetworkID().getCellValue()) {
				plan.setNetworkId(currentPBVO.getPlanAttributes().getNetworkID().getCellValue());
			}*/

			
			//2016 plans
			if(StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getEhbPercentPremium().getCellValue())){
				plan.setEhbPremiumFraction(Float.parseFloat(currentPBVO.getPlanAttributes().getEhbPercentPremium().getCellValue()));
			}
			if(StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getInsurancePlanCompositePremiumAvailableIndicator().getCellValue())){
				plan.setCompositPremiumAvailableIndicator(SerffConstants.YES.equalsIgnoreCase(currentPBVO.getPlanAttributes().getInsurancePlanCompositePremiumAvailableIndicator().getCellValue()) ? SerffConstants.YES_ABBR : SerffConstants.NO_ABBR);
			}
			if(StringUtils.isNotBlank(costShareVarianceVO.getHsa().getEmployerHSAHRAContributionIndicator().getCellValue())){
				plan.setEmpHSAHRAContributionIndicator(SerffConstants.YES.equalsIgnoreCase(costShareVarianceVO.getHsa().getEmployerHSAHRAContributionIndicator().getCellValue()) ? SerffConstants.YES_ABBR : SerffConstants.NO_ABBR);
			}
			if(StringUtils.isNotBlank(costShareVarianceVO.getHsa().getEmpContributionAmountForHSAOrHRA().getCellValue())){
				plan.setEmpHSAHRAContributionAmount(Float.parseFloat(costShareVarianceVO.getHsa().getEmpContributionAmountForHSAOrHRA().getCellValue().trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
			}
			if(null != costShareVarianceVO.getUrl()) {
				url =checkAndcorrectURL(costShareVarianceVO.getUrl().getPlanBrochure());
				plan.setBrochure(url);
			}

			boolean found =false ;
			String oldServiceAreaID =null;
			if(!CollectionUtils.isEmpty(issuerServiceAreas) && null != currentPBVO.getPlanAttributes().getServiceAreaID()
					&&  StringUtils.isNotEmpty(currentPBVO.getPlanAttributes().getServiceAreaID().getCellValue())) {

				for(IssuerServiceArea issuerServiceArea : issuerServiceAreas){
					oldServiceAreaID = issuerServiceArea.getSerffServiceAreaId();
					if(currentPBVO.getPlanAttributes().getServiceAreaID().getCellValue().equalsIgnoreCase(oldServiceAreaID)){
						found =true;
						plan.setServiceAreaId(issuerServiceArea.getId());
						break;
					}
				}
			}
			if(!found){
				LOGGER.error("Service Area ID is modified .Old Id : "+oldServiceAreaID + " New Id : " + SecurityUtil.sanitizeForLogging(currentPBVO.getPlanAttributes().getServiceAreaID().getCellValue()));
				return null;
			}
			if(!CollectionUtils.isEmpty(networks) && null != currentPBVO.getPlanAttributes().getNetworkID()){
				Network network = networks.get(currentPBVO.getPlanAttributes().getNetworkID().getCellValue());
				if (network != null) {
					plan.setProviderNetworkId(network.getId());
					plan.setNetwork(network);
				} else {
					LOGGER.error("No network found for networkid: " +  SecurityUtil.sanitizeForLogging(currentPBVO.getPlanAttributes().getNetworkID().getCellValue()));
				}
			}

			if(StringUtils.isEmpty(plan.getIssuerVerificationStatus())){
				plan.setIssuerVerificationStatus(IssuerVerificationStatus.PENDING.toString());
			}

			plan.setIssuer(issuer);
			plan.setCreatedBy(issuer.getLastUpdatedBy());
			plan.setCreationTimestamp(issuer.getCreationTimestamp());
			LOGGER.info("Inserting state code in plan table for dental plans");
			plan.setState(issuer.getStateOfDomicile());

			// plan = iPlanRepository.save(plan);
			/*if (null != entityManager && entityManager.isOpen()) {
				plan = entityManager.merge(plan);
				LOGGER.info("Plan saved with basic information. Plan id is = " + plan.getId());
			}*/
			if(isPUFPlan) {
				//HIX-26894
				plan.setStatus(PlanStatus.CERTIFIED.toString());
			} else {
				plan.setStatus(PlanStatus.LOADED.toString());
			}

			if (null != currentPBVO.getPlanAttributes().getFormularyID()) {
				plan.setFormularlyId(validatePlanFormularyId(applicableYear, currentPBVO.getPlanAttributes().getFormularyID().getCellValue(), formulary, issuer.getId()));
			}

			//Plan Attributes
			if(null != currentPBVO.getPlanAttributes().getPlanType() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getPlanType().getCellValue())) {
				plan.setNetworkType(currentPBVO.getPlanAttributes().getPlanType().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getMetalLevel() && null != currentPBVO.getPlanAttributes().getMetalLevel().getCellValue()) {
				planDental.setPlanLevel(currentPBVO.getPlanAttributes().getMetalLevel().getCellValue().toUpperCase());
			}

			//New attribute from 2017 template V6
			if(currentPBVO.isNewVersion() && null != currentPBVO.getPlanAttributes().getPlanDesignType() && 
					null != currentPBVO.getPlanAttributes().getPlanDesignType().getCellValue()) {
				planDental.setPlanDesignType(StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getPlanDesignType().getCellValue()) ? currentPBVO.getPlanAttributes().getPlanDesignType().getCellValue() : "No");
			}

			if(null != currentPBVO.getPlanAttributes().getQhpOrNonQhp() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getQhpOrNonQhp().getCellValue())) {
				planDental.setIsQHP(currentPBVO.getPlanAttributes().getQhpOrNonQhp().getCellValue());
			}

			/*if(null != currentPBVO.getPlanAttributes().getInsurancePlanPregnancyNoticeReqInd() && null != currentPBVO.getPlanAttributes().getInsurancePlanPregnancyNoticeReqInd().getCellValue()) {
				planDental.setPregnancyNotice(currentPBVO.getPlanAttributes().getInsurancePlanPregnancyNoticeReqInd().getCellValue().toString()!=null?currentPBVO.getPlanAttributes().getInsurancePlanPregnancyNoticeReqInd().getCellValue().toString():"No");
			}

			if(null != currentPBVO.getPlanAttributes().getIsSpecialistReferralRequired() && null != currentPBVO.getPlanAttributes().getIsSpecialistReferralRequired().getCellValue()) {
				planDental.setSpecialistReferal(currentPBVO.getPlanAttributes().getIsSpecialistReferralRequired().getCellValue().toString()!=null?currentPBVO.getPlanAttributes().getIsSpecialistReferralRequired().getCellValue().toString():"No");
			}*/

			if(null != currentPBVO.getPlanAttributes().getInsurancePlanBenefitExclusionText() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getInsurancePlanBenefitExclusionText().getCellValue())) {
				String exclusions = StringUtils.trimToEmpty(currentPBVO.getPlanAttributes().getInsurancePlanBenefitExclusionText().getCellValue());
				if(exclusions.length() > BENEFITS_EXCLUSION_TEXT_LEN) {
					LOGGER.warn("Truncating Plan Benefit Exclusion Text to " + BENEFITS_EXCLUSION_TEXT_LEN + " characters: " + exclusions);
					exclusions = exclusions.substring(0, BENEFITS_EXCLUSION_TEXT_LEN);
				}
				planDental.setPlanLevelExclusions(exclusions);
			}
			
			//2016 plans
			if(null != costShareVarianceVO.getHsa()) {
				plan.setHsa(costShareVarianceVO.getHsa().getHsaEligibility().getCellValue() != null ? costShareVarianceVO.getHsa().getHsaEligibility().getCellValue() : "No");
			}

			if(null != currentPBVO.getPlanAttributes().getOutOfCountryCoverage() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getOutOfCountryCoverage().getCellValue())){
				planDental.setOutOfCountryCoverage(currentPBVO.getPlanAttributes().getOutOfCountryCoverage().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getOutOfCountryCoverageDescription() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getOutOfCountryCoverageDescription().getCellValue())){
				String outOfCountryCoverageDesc = StringUtils.trimToEmpty(currentPBVO.getPlanAttributes().getOutOfCountryCoverageDescription().getCellValue());
				if(outOfCountryCoverageDesc.length() > OUT_OF_AREA_COVERAGE_DESC_LEN) {
					LOGGER.warn("Truncating Out Of Country Coverage Description Text to " + OUT_OF_AREA_COVERAGE_DESC_LEN + " characters: " + outOfCountryCoverageDesc);
					outOfCountryCoverageDesc = outOfCountryCoverageDesc.substring(0, OUT_OF_AREA_COVERAGE_DESC_LEN);
				}
				planDental.setOutOfCountryCoverageDesc(outOfCountryCoverageDesc);
			}

			if(null != currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverage() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverage().getCellValue())){
				planDental.setOutOfServiceAreaCoverage(currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverage().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverageDescription() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverageDescription().getCellValue())){
				String outOfSvcAreaCoverageDesc = StringUtils.trimToEmpty(currentPBVO.getPlanAttributes().getOutOfServiceAreaCoverageDescription().getCellValue());
				if(outOfSvcAreaCoverageDesc.length() > OUT_OF_AREA_COVERAGE_DESC_LEN) {
					LOGGER.warn("Truncating Out Of Service Area Coverage Description Text to " + OUT_OF_AREA_COVERAGE_DESC_LEN + " characters: " + outOfSvcAreaCoverageDesc);
					outOfSvcAreaCoverageDesc = outOfSvcAreaCoverageDesc.substring(0, OUT_OF_AREA_COVERAGE_DESC_LEN);
				}
				planDental.setOutOfServiceAreaCoverageDesc(outOfSvcAreaCoverageDesc);
			}

			if(null != currentPBVO.getPlanAttributes().getNationalNetwork() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getNationalNetwork().getCellValue())){
				planDental.setNationalNetwork(currentPBVO.getPlanAttributes().getNationalNetwork().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getEhbApportionmentForPediatricDental() && null != currentPBVO.getPlanAttributes().getEhbApportionmentForPediatricDental().getCellValue()) {
				planDental.setEhbApptForPediatricDental(currentPBVO.getPlanAttributes().getEhbApportionmentForPediatricDental().getCellValue());
			}

			if(null != currentPBVO.getPlanAttributes().getGuaranteedVsEstimatedRate() && null != currentPBVO.getPlanAttributes().getGuaranteedVsEstimatedRate().getCellValue()) {
				planDental.setGuaranteedVsEstimatedRate(currentPBVO.getPlanAttributes().getGuaranteedVsEstimatedRate().getCellValue());
			}

			planDental.setPlan(plan);
			// planDental = iPlanDentalRepository.save(planDental);
			/*if (null != entityManager && entityManager.isOpen()) {
				planDental = entityManager.merge(planDental);
				LOGGER.info("PlanDental saved with basic information. Plan id is = " + plan.getId());
			}*/

			// Saving documents details
			if (!CollectionUtils.isEmpty(documents)) {

				SupportingDocumentDTO documentDTO = getSupportingDocument(documents, issuerPlanNumberWithVariant, issuerPlanNumberWithoutVariant);
				plan.setEocDocName(documentDTO.getEocDocumentName());
				plan.setEocDocId(documentDTO.getEocDocumentId());

				plan.setBrochureDocName(documentDTO.getBrochureDocumentName());
				plan.setBrochureUCmId(documentDTO.getBrochureDocumentId());

				planDental.setSbcDocName(documentDTO.getSbcDocumentName());
				planDental.setSbcUcmId(documentDTO.getSbcDocumentId());

				planDental.setBenefitFile(documentDTO.getBenefitFile());
				planDental.setRateFile(documentDTO.getRateFile());
			}

			if(null != currentPBVO.getPlanAttributes().getChildOnlyOffering() && StringUtils.isNotBlank(currentPBVO.getPlanAttributes().getChildOnlyOffering().getCellValue())) {
				String childOffering = currentPBVO.getPlanAttributes().getChildOnlyOffering().getCellValue();
				plan.setAvailableFor(getAvailableFor(childOffering));
				planDental.setChildOnlyOffering(childOffering);
			}

			//Plan Dates
			try {
				if(null != currentPBVO.getPlanAttributes().getPlanEffectiveDate() &&  null != currentPBVO.getPlanAttributes().getPlanEffectiveDate().getCellValue()) {
					plan.setStartDate(dateFormat.parse(currentPBVO.getPlanAttributes().getPlanEffectiveDate().getCellValue()));
					planDental.setEffectiveStartDate(dateFormat.parse(currentPBVO.getPlanAttributes().getPlanEffectiveDate().getCellValue()));
					planDental.setBenefitEffectiveDate(dateFormat.parse(currentPBVO.getPlanAttributes().getPlanEffectiveDate().getCellValue()));
					
					String expirationDateString = EXPIRATION_DATE + applicableYear;
					Date expirationDate = dateFormat.parse(expirationDateString);
					LOGGER.info("Plan Expiration Date : " + expirationDate);
					
					plan.setEndDate(expirationDate);
					planDental.setEffectiveEndDate(expirationDate);
					plan.setEnrollmentAvailEffDate(plan.getStartDate());

				} else {
					plan.setEnrollmentAvailEffDate(new Date());
				}
				if(isPUFPlan) {
					//HIX-26894
					plan.setEnrollmentAvail(Plan.EnrollmentAvail.AVAILABLE.toString());
				} else {
					plan.setEnrollmentAvail(Plan.EnrollmentAvail.NOTAVAILABLE.toString());
				}
			}
			catch (Exception e) {
				LOGGER.error("Exception occurred while parsing date. Exception:" + e.getMessage(), e);
			}

			//planBenefitTemplate.getPlans().get(0).getPlanAttributes().getPlanEffectiveDate().getCellValue());

			//URLs
			//2016 plans
			if(null != costShareVarianceVO.getUrl()) {
				 url =checkAndcorrectURL(costShareVarianceVO.getUrl().getSummaryBenefitAndCoverageURL());
				 planDental.setBenefitsUrl(url);
			}
			
			if(null != currentPBVO.getPlanAttributes().getEnrollmentPaymentURL() && null != currentPBVO.getPlanAttributes().getEnrollmentPaymentURL().getCellValue()) {
				url = checkAndcorrectURL(currentPBVO.getPlanAttributes().getEnrollmentPaymentURL());
				planDental.setEnrollmentUrl(url);
			}

		}

		/*int ehbCount = 0 ;
		if(null != planAndBenefitsPackageVO.getBenefitsList() &&  null!=planAndBenefitsPackageVO.getBenefitsList().getBenefits()) {
			//currentPlanBAs = currentPBVO.getBenefitAttributesList().getBenefitAttributes();
			currentPlanBAs = planAndBenefitsPackageVO.getBenefitsList().getBenefits();
			for(com.serff.template.plan.BenefitAttributeVO currentPlanBA: currentPlanBAs) {
				if(null != currentPlanBA && null!=currentPlanBA.getIsEHB()) {
					String isEHB = currentPlanBA.getIsEHB().getCellValue();
					if (StringUtils.isNotEmpty(isEHB) && isEHB.equalsIgnoreCase(YES)) {
						ehbCount++;
					}
				}
			}
		}*/

		//	currentPlanCSVs = currentPBVO.getCostShareVariancesList().getCostShareVariance();


				if(null != costShareVarianceVO) {
					//Cost Sharing.
					/*if(null != costShareVarianceVO.getCsrVariationType() && null != costShareVarianceVO.getCsrVariationType().getCellValue() && null != costShareVarianceVO.getCsrVariationType().getCellValue().toString()) {
						//planHealth.setCostSharing(costShareVarianceVO.getCsrVariationType().getCellValue().toString());
					}*/

					if(currentPBVO.isNewVersion()) {
						AvCalculatorVO avCalc = costShareVarianceVO.getAvCalculator();
						if(null != avCalc) {
							if(null != avCalc.getIssuerActuarialValue() && StringUtils.isNotBlank(avCalc.getIssuerActuarialValue().getCellValue())) {
								planDental.setActurialValueCertificate(avCalc.getIssuerActuarialValue().getCellValue());
							}
		
							if(null != avCalc.getAvCalculatorOutputNumber() && StringUtils.isNotBlank(avCalc.getAvCalculatorOutputNumber().getCellValue())) {
								planDental.setAvCalcOutputNumber(avCalc.getAvCalculatorOutputNumber().getCellValue());
							}
						} else {
							LOGGER.info("Missing AvCalculator block in new template for CSV " + costShareVarianceVO.getPlanId().getCellValue());
						}
					} else {
						if(null != costShareVarianceVO.getIssuerActuarialValue() && StringUtils.isNotBlank(costShareVarianceVO.getIssuerActuarialValue().getCellValue())) {
							planDental.setActurialValueCertificate(costShareVarianceVO.getIssuerActuarialValue().getCellValue());
						}
	
						if(null != costShareVarianceVO.getAvCalculatorOutputNumber() && StringUtils.isNotBlank(costShareVarianceVO.getAvCalculatorOutputNumber().getCellValue())) {
							planDental.setAvCalcOutputNumber(costShareVarianceVO.getAvCalculatorOutputNumber().getCellValue());
						}
					}

					if(null != costShareVarianceVO.getMultipleProviderTiers() && StringUtils.isNotBlank(costShareVarianceVO.getMultipleProviderTiers().getCellValue())) {
						multipleTierNtwrk = costShareVarianceVO.getMultipleProviderTiers().getCellValue();
						planDental.setMultipleNetworkTiers(multipleTierNtwrk);
						
					}

					if(null != costShareVarianceVO.getFirstTierUtilization() && StringUtils.isNotBlank(costShareVarianceVO.getFirstTierUtilization().getCellValue())) {
						planDental.setTier1Util(costShareVarianceVO.getFirstTierUtilization().getCellValue());
					}

					if(null != costShareVarianceVO.getSecondTierUtilization() && StringUtils.isNotBlank(costShareVarianceVO.getSecondTierUtilization().getCellValue())) {
						planDental.setTier2Util(costShareVarianceVO.getSecondTierUtilization().getCellValue());
					}

					/*if(null != costShareVarianceVO.getMedicalAndDrugDeductiblesIntegrated() && null != costShareVarianceVO.getMedicalAndDrugDeductiblesIntegrated().getCellValue()) {
						planDental.setIsMedDrugDeductibleIntegrated(costShareVarianceVO.getMedicalAndDrugDeductiblesIntegrated().getCellValue().toString());
					}

					if(null != costShareVarianceVO.getMedicalAndDrugMaxOutOfPocketIntegrated() && null != costShareVarianceVO.getMedicalAndDrugMaxOutOfPocketIntegrated().getCellValue()) {
						planDental.setIsMedDrugMaxOutPocketIntegrated(costShareVarianceVO.getMedicalAndDrugMaxOutOfPocketIntegrated().getCellValue().toString());

					}
					if(ehbCount == 10){
						planDental.setEhbCovered(SerffConstants.COMPLETED);
					}else{
						planDental.setEhbCovered(SerffConstants.PARTIAL);
					}

					*/

					//plansbcScenario





					//Maximum Out of Pocket for Medical and Drug EHB Benefits (Total)
					MoopListVO  moopListObj = costShareVarianceVO.getMoopList();
					Map<String,String> healthCosts = serffUtils.getPropertyList(PlanmgmtConfigurationEnum.DENTAL_COSTS_LIST);
					boolean isAdultDentalMOOPAdded = false;
					
					if (null != moopListObj && null != moopListObj.getMoop()) {
						moopVOList = moopListObj.getMoop();

						for(MoopVO moopVO: moopVOList){

							if(null!=moopVO){
							if(null != moopVO.getName() && StringUtils.isNotBlank(moopVO.getName().getCellValue())) {
								moopName =moopVO.getName().getCellValue();
								if(LOGGER.isDebugEnabled()) {
									LOGGER.debug("Dental Cost Name " + SecurityUtil.sanitizeForLogging(moopName));
								}

							}
							if(null == moopName || null == healthCosts.get(moopName.toLowerCase())) {
								if(LOGGER.isInfoEnabled()) {
									LOGGER.info("Skipping Plan Dental Cost : No mapping found for moop " + SecurityUtil.sanitizeForLogging(moopName));
								}
								continue;
							}
							planDentalCost = new PlanDentalCost();
							planDentalCost.setName(healthCosts.get(moopName.toLowerCase()));

							if(planDentalCost.getName() != null) {
								
								if(null != moopVO.getInNetworkTier1IndividualAmount() && StringUtils.isNotBlank(moopVO.getInNetworkTier1IndividualAmount().getCellValue())) {
									inNetTier1IndAmnt = moopVO.getInNetworkTier1IndividualAmount().getCellValue();
									inNetTier1IndAmnt = inNetTier1IndAmnt.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
	
									if (validateAmount(inNetTier1IndAmnt)) {
										planDentalCost.setInNetWorkInd(Double.parseDouble(inNetTier1IndAmnt));
									}
								}
								
								if(null != moopVO.getInNetworkTier1FamilyAmount() && null !=moopVO.getInNetworkTier1FamilyAmount().getCellValue()) {
									
									String[] inNetworkTier1FamilyAmountValues = getAmountValues(moopVO.getInNetworkTier1FamilyAmount().getCellValue());
									
									if(null != inNetworkTier1FamilyAmountValues){
										
										if (validateAmount(inNetworkTier1FamilyAmountValues[0])) {
											planDentalCost.setInNetWorkFly(Double.parseDouble(inNetworkTier1FamilyAmountValues[0]));
										}
										
										if (validateAmount(inNetworkTier1FamilyAmountValues[1])) {
											planDentalCost.setInNetworkFlyPerPerson(Double.parseDouble(inNetworkTier1FamilyAmountValues[1]));
										}
									}
									
								}							
								
								if(YES.equalsIgnoreCase(multipleTierNtwrk)){
									if(null != moopVO.getInNetworkTier2IndividualAmount()&& StringUtils.isNotBlank(moopVO.getInNetworkTier2IndividualAmount().getCellValue())) {
										inNetTier2IndAmnt= moopVO.getInNetworkTier2IndividualAmount().getCellValue();
										inNetTier2IndAmnt=inNetTier2IndAmnt.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
										
										if (validateAmount(inNetTier2IndAmnt)) {
											planDentalCost.setInNetworkTier2Ind(Double.parseDouble(inNetTier2IndAmnt));
										}	
									}
									
									if(null != moopVO.getInNetworkTier2FamilyAmount()&& null != moopVO.getInNetworkTier2FamilyAmount().getCellValue()) {
										
										String[] inNetTier2FamilyAmntValues = getAmountValues(moopVO.getInNetworkTier2FamilyAmount().getCellValue());
										
										if(null != inNetTier2FamilyAmntValues){
											
											if (validateAmount(inNetTier2FamilyAmntValues[0])) {
												planDentalCost.setInNetworkTier2Fly(Double.parseDouble(inNetTier2FamilyAmntValues[0]));
											}
											
											if (validateAmount(inNetTier2FamilyAmntValues[1])) {
												planDentalCost.setInNetworkTier2FlyPerPerson(Double.parseDouble(inNetTier2FamilyAmntValues[1]));
											}
										}
									
									}
								}
								
								
								if(null != moopVO.getOutOfNetworkIndividualAmount() && StringUtils.isNotBlank(moopVO.getOutOfNetworkIndividualAmount().getCellValue())) {
									outNetIndAmnt = moopVO.getOutOfNetworkIndividualAmount().getCellValue();
									outNetIndAmnt = outNetIndAmnt.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
	
									if (validateAmount(outNetIndAmnt)) {
										planDentalCost.setOutNetworkInd(Double.parseDouble(outNetIndAmnt));
									}
								}
								
								if(null != moopVO.getOutOfNetworkFamilyAmount() && null != moopVO.getOutOfNetworkFamilyAmount().getCellValue()) {
									
									String[] outOfNetworkFamilyAmountValues = getAmountValues(moopVO.getOutOfNetworkFamilyAmount().getCellValue());
									
									if(null != outOfNetworkFamilyAmountValues){
										
										if (validateAmount(outOfNetworkFamilyAmountValues[0])) {
											planDentalCost.setOutNetworkFly(Double.parseDouble(outOfNetworkFamilyAmountValues[0]));
										}
										
										if (validateAmount(outOfNetworkFamilyAmountValues[1])) {
											planDentalCost.setOutNetworkFlyPerPerson(Double.parseDouble(outOfNetworkFamilyAmountValues[1]));
										}
									}
									
								}
								
								if(null != moopVO.getCombinedInOutNetworkIndividualAmount() && StringUtils.isNotBlank(moopVO.getCombinedInOutNetworkIndividualAmount().getCellValue())) {
									combinedInOutNetIndAmnt = moopVO.getCombinedInOutNetworkIndividualAmount().getCellValue();
									combinedInOutNetIndAmnt = combinedInOutNetIndAmnt.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
	
									if (validateAmount(combinedInOutNetIndAmnt)) {
										planDentalCost.setCombinedInOutNetworkInd(Double.parseDouble(combinedInOutNetIndAmnt));
									}
								}
								
								if(null != moopVO.getCombinedInOutNetworkFamilyAmount() && null != moopVO.getCombinedInOutNetworkFamilyAmount().getCellValue()) {
									
									String[] combinedInOutNetworkFlyValues = getAmountValues(moopVO.getCombinedInOutNetworkFamilyAmount().getCellValue());
									
									if(null != combinedInOutNetworkFlyValues){
										
										if (validateAmount(combinedInOutNetworkFlyValues[0])) {
											planDentalCost.setCombinedInOutNetworkFly(Double.parseDouble(combinedInOutNetworkFlyValues[0]));
										}
										
										if (validateAmount(combinedInOutNetworkFlyValues[1])) {
											planDentalCost.setCombinedInOutNetworkFlyPerPerson(Double.parseDouble(combinedInOutNetworkFlyValues[1]));
										}
									}
								}
								planDentalCost.setPlanDental(planDental);
								planDentalCostList.add(planDentalCost);
								
								if (MAX_OOP_DENTAL_ADULT_CONST.equals(planDentalCost.getName())) {
									isAdultDentalMOOPAdded = true;
								}
							}
							else {
								LOGGER.info("Skipping Invalid MOOP Name " + moopVO.getName());
							}
						}
					}
				}

				//Implemented for HIX-84072
				boolean isCAProfile = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE).equals(SerffConstants.CA_STATE_CODE);
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("CA Profile: " + isCAProfile + ", Adult Dental MOOP added: " + isAdultDentalMOOPAdded);
				}

				if (isCAProfile && !isAdultDentalMOOPAdded) {
					planDentalCost = new PlanDentalCost();
					planDentalCost.setName(MAX_OOP_DENTAL_ADULT_CONST);
					planDentalCost.setPlanDental(planDental);
					planDentalCostList.add(planDentalCost);
				}

				//Combined Medical and Drug EHB Deductible
				PlanDeductibleListVO  planDeductibleListObj = costShareVarianceVO.getPlanDeductibleList();
				if(null!=planDeductibleListObj && null!=planDeductibleListObj.getPlanDeductible()){
					planDeductibleVOListObj = planDeductibleListObj.getPlanDeductible();
						String deductibleName = null;
						int customDeductibleCount = 0;
						for(PlanDeductibleVO planDeductibleVO: planDeductibleVOListObj){
							deductibleName = null;
							if(null!=planDeductibleVO && null !=planDeductibleVO.getDeductibleType() && StringUtils.isNotBlank(planDeductibleVO.getDeductibleType().getCellValue())) {
								deductibleType = planDeductibleVO.getDeductibleType().getCellValue();
								if(LOGGER.isDebugEnabled()) {
									LOGGER.debug("Plan Deductible type " + SecurityUtil.sanitizeForLogging(deductibleType));
								}
							}
							if(null == deductibleType) {
								LOGGER.info("Skipping Plan Dental Cost : deductibleType is null");
								continue;
							}
							planDentalCost = new PlanDentalCost();
							deductibleName = healthCosts.get(deductibleType.toLowerCase());
							if(null == deductibleName) {
								customDeductibleCount++;
								if(customDeductibleCount <= MAX_CUSTOM_DEDUCTIBLE_COUNT) {
									deductibleName = deductibleType;
									if(LOGGER.isInfoEnabled()) {
										LOGGER.info(NO_DEDUCTIBLE_MAP + customDeductibleCount + "] "+ SecurityUtil.sanitizeForLogging(deductibleType));
									}
								} else {
									if(LOGGER.isInfoEnabled()) {
										LOGGER.info(NO_DEDUCTIBLE_MAP+ customDeductibleCount + "] "+ SecurityUtil.sanitizeForLogging(deductibleType));
									}
									continue;
								}
							}
							planDentalCost.setName(deductibleName);
							if(null !=planDeductibleVO.getInNetworkTier1Individual() && StringUtils.isNotBlank(planDeductibleVO.getInNetworkTier1Individual().getCellValue())) {

								inNetTier1Ind = planDeductibleVO.getInNetworkTier1Individual().getCellValue();
								inNetTier1Ind = inNetTier1Ind.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);

								if (validateAmount(inNetTier1Ind)) {
									planDentalCost.setInNetWorkInd(Double.parseDouble(inNetTier1Ind));
								}
							}
							
							
							if(null !=planDeductibleVO.getInNetworkTier1Family() && StringUtils.isNotBlank(planDeductibleVO.getInNetworkTier1Family().getCellValue())) {
								
								String[] inNetWorkFlyValues = getAmountValues(planDeductibleVO.getInNetworkTier1Family().getCellValue());
								
								if(null != inNetWorkFlyValues){
									
									if (validateAmount(inNetWorkFlyValues[0])) {
										planDentalCost.setInNetWorkFly(Double.parseDouble(inNetWorkFlyValues[0]));
									}
									
									if (validateAmount(inNetWorkFlyValues[1])) {
										planDentalCost.setInNetworkFlyPerPerson(Double.parseDouble(inNetWorkFlyValues[1]));
									}
								}
							
							}
								
							if(YES.equalsIgnoreCase(multipleTierNtwrk)){
								if(null != planDeductibleVO.getInNetworkTierTwoIndividual() && StringUtils.isNotBlank(planDeductibleVO.getInNetworkTierTwoIndividual().getCellValue())) {
									inNetTier2IndAmnt= planDeductibleVO.getInNetworkTierTwoIndividual().getCellValue();
									inNetTier2IndAmnt=inNetTier2IndAmnt.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
									
									if (validateAmount(inNetTier2IndAmnt)) {
										planDentalCost.setInNetworkTier2Ind(Double.parseDouble(inNetTier2IndAmnt));
									}	
								}
								
								
								if(null != planDeductibleVO.getInNetworkTierTwoFamily()&& null != planDeductibleVO.getInNetworkTierTwoFamily().getCellValue()) {
									
									String[] inNetworkTier2FlyValues = getAmountValues(planDeductibleVO.getInNetworkTierTwoFamily().getCellValue());
									
									if(null != inNetworkTier2FlyValues){
										
										if (validateAmount(inNetworkTier2FlyValues[0])) {
											planDentalCost.setInNetworkTier2Fly(Double.parseDouble(inNetworkTier2FlyValues[0]));
										}
										
										if (validateAmount(inNetworkTier2FlyValues[1])) {
											planDentalCost.setInNetworkTier2FlyPerPerson(Double.parseDouble(inNetworkTier2FlyValues[1]));
										}
									}
									
								}
							}
							
							if(null !=planDeductibleVO.getCoinsuranceInNetworkTier1() && StringUtils.isNotBlank(planDeductibleVO.getCoinsuranceInNetworkTier1().getCellValue())) {
								
								coinsuranceInNetTier1 = planDeductibleVO.getCoinsuranceInNetworkTier1().getCellValue();
								coinsuranceInNetTier1 = coinsuranceInNetTier1.replaceAll(SerffConstants.PERCENTAGE, StringUtils.EMPTY).trim();

								if (validateAmount(coinsuranceInNetTier1.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
									planDentalCost.setCombDefCoinsNetworkTier1(Double.parseDouble(coinsuranceInNetTier1.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
								}
							}
							if(null !=planDeductibleVO.getCoinsuranceInNetworkTier2() && StringUtils.isNotBlank(planDeductibleVO.getCoinsuranceInNetworkTier2().getCellValue())) {
								
								coinsuranceInNetTier2 = planDeductibleVO.getCoinsuranceInNetworkTier2().getCellValue();
								coinsuranceInNetTier2 = coinsuranceInNetTier2.replaceAll(SerffConstants.PERCENTAGE, StringUtils.EMPTY).trim();

								if (validateAmount(coinsuranceInNetTier2.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY))) {
									planDentalCost.setCombDefCoinsNetworkTier2(Double.parseDouble(coinsuranceInNetTier2.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
								}
							}
							if(null !=planDeductibleVO.getOutOfNetworkIndividual() && StringUtils.isNotBlank(planDeductibleVO.getOutOfNetworkIndividual().getCellValue())) {

								outOfNetInd = planDeductibleVO.getOutOfNetworkIndividual().getCellValue();
								outOfNetInd = outOfNetInd.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);

								if (validateAmount(outOfNetInd)) {
									planDentalCost.setOutNetworkInd(Double.parseDouble(outOfNetInd));
								}
							}
							
							if(null !=planDeductibleVO.getOutOfNetworkFamily() && null != planDeductibleVO.getOutOfNetworkFamily().getCellValue()) {
								
								String[] outOfNetFamilyValues = getAmountValues(planDeductibleVO.getOutOfNetworkFamily().getCellValue());
								
								if(null != outOfNetFamilyValues){
									
									if (validateAmount(outOfNetFamilyValues[0])) {
										planDentalCost.setOutNetworkFly(Double.parseDouble(outOfNetFamilyValues[0]));
									}
									
									if (validateAmount(outOfNetFamilyValues[1])) {
										planDentalCost.setOutNetworkFlyPerPerson(Double.parseDouble(outOfNetFamilyValues[1]));
									}
								}
							}
								
							if(null !=planDeductibleVO.getCombinedInOrOutNetworkIndividual() && StringUtils.isNotBlank(planDeductibleVO.getCombinedInOrOutNetworkIndividual().getCellValue())) {

								combinedInOrOutNetInd = planDeductibleVO.getCombinedInOrOutNetworkIndividual().getCellValue();
								combinedInOrOutNetInd = combinedInOrOutNetInd.trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);

								if (validateAmount(combinedInOrOutNetInd)) {
									planDentalCost.setCombinedInOutNetworkInd(Double.parseDouble(combinedInOrOutNetInd));
								}
							}
							
							if(null !=planDeductibleVO.getCombinedInOrOutNetworkFamily() && null != planDeductibleVO.getCombinedInOrOutNetworkFamily().getCellValue()) {
								
								String[] combinedInOrOutNetFamilyValues = getAmountValues(planDeductibleVO.getCombinedInOrOutNetworkFamily().getCellValue());
								
								if(null != combinedInOrOutNetFamilyValues){
									
									if (validateAmount(combinedInOrOutNetFamilyValues[0])) {
										planDentalCost.setCombinedInOutNetworkFly(Double.parseDouble(combinedInOrOutNetFamilyValues[0]));
									}
									
									if (validateAmount(combinedInOrOutNetFamilyValues[1])) {
										planDentalCost.setCombinedInOutNetworkFlyPerPerson(Double.parseDouble(combinedInOrOutNetFamilyValues[1]));
									}
								}
								
							}
							
							planDentalCost.setPlanDental(planDental);
							planDentalCostList.add(planDentalCost);
						}
					}
				}

				benefitCopayCoinsData = getBenefitCopayCoinsData(costShareVarianceVO);
				if(null != planAndBenefitsPackageVO.getBenefitsList() &&  null!=planAndBenefitsPackageVO.getBenefitsList().getBenefits()) {
						Map<String,String> dentalBenefitNames = serffUtils.getPropertyList(PlanmgmtConfigurationEnum.DENTAL_BENEFITS_LIST);
						//String benefitValue;

						currentPlanBAs = planAndBenefitsPackageVO.getBenefitsList().getBenefits();
						planDentalBenefits = new ArrayList<PlanDentalBenefit>();
						PlanDentalBenefit planDentalBenefit = null;
						
						for(com.serff.template.plan.BenefitAttributeVO currentPlanBA: currentPlanBAs) {

							/*PlanHealthBenefit planHealthBenefit = createPlanHealthBenefit(currentPlanBA);
							String benefitName = healthBenefitNames.get(planHealthBenefit.getName().replaceAll(SerffConstants.COMMA, SerffConstants.DOT).toLowerCase());
							if(benefitName == null) {
								benefitName= getDerivedBenefitName(planHealthBenefit.getName());
								LOGGER.info("No mapping found for Health Benefit name : " + currentPlanBA.getBenefitTypeCode().getCellValue() + ". Using derived value " + benefitName);
							}
							populateBenefitData(planHealthBenefit, benefitName, benefitCopayCoinsData);*/

							if(null != currentPlanBA) {
								//Benefit Information
								planDentalBenefit = createPlanDentalBenefit(currentPlanBA);
								String benefitName = dentalBenefitNames.get(currentPlanBA.getBenefitTypeCode().getCellValue().replaceAll(SerffConstants.COMMA, SerffConstants.DOT).toLowerCase());
								if(benefitName == null) {
									benefitName = getDerivedBenefitName(currentPlanBA.getBenefitTypeCode().getCellValue());
									LOGGER.info("No mapping found for Dental Benefit name : " + currentPlanBA.getBenefitTypeCode().getCellValue() + ". Using derived value " + benefitName);
								}
								populateBenefitData(planDentalBenefit, benefitName, benefitCopayCoinsData, planDispRulesMap);
								planDentalBenefit.setPlanDental(planDental);
								planDentalBenefit.setEffStartDate(plan.getStartDate());
								planDentalBenefit.setEffEndDate(plan.getEndDate());
								planDentalBenefit.setCreatedBy(issuer.getLastUpdatedBy());
								planDentalBenefit.setCreationTimestamp(issuer.getCreationTimestamp());
								planDentalBenefits.add(planDentalBenefit);
							}
						}
					}

					planDental.setPlanDentalBenefitVO(planDentalBenefits);
					planDental.setPlanDentalCostsList(planDentalCostList);

					if(dentalParentId != -1){
						planDental.setParentPlanId(dentalParentId);
					}

					LOGGER.info("Processing of Rates begin");
					if(null != plan.getIssuerPlanNumber()){
						String planId = plan.getIssuerPlanNumber().substring(0, SerffConstants.LEN_HIOS_PLAN_ID);
						boolean isFamilyRate = false;
						boolean isIndividualRate = false;
						if(!CollectionUtils.isEmpty(planRates.get(planId))){
							List<PlanRate> rates = planRates.get(planId);

							//HIX-63650 : Change rates persistence code to override child max age for rates in the template.
							rates = serffUtils.preProcessDentalPlanRates(rates);
							
							for(PlanRate rate : rates){
								
								rate.setLastUpdatedBy(issuer.getLastUpdatedBy());
								rate.setPlan(plan);
								
								if(!isFamilyRate && (PlanRate.RATE_OPTION.F).equals(rate.getRateOption())){
									isFamilyRate = true;
								}
								else if(!isIndividualRate && (PlanRate.RATE_OPTION.A).equals(rate.getRateOption())){
									isIndividualRate = true;
								}
							}
							plan.setPlanRate(rates);
							
							if(isFamilyRate && isIndividualRate ){
								LOGGER.info("Community rates are present");
								planDental.setIsCommunityRate(SerffConstants.YES_ABBR);
							}
							else{
								planDental.setIsCommunityRate(SerffConstants.NO_ABBR);
							}
							planDental.setRateEffectiveDate(rates.get(0).getEffectiveStartDate());
						}

					}
					LOGGER.info("Processing of Rates ends");

					planDental.setPlan(plan);
					plan.setPlanDental(planDental);

					// plan = iPlanRepository.save(plan);
					if (null != entityManager && entityManager.isOpen()) {
						plan = entityManager.merge(plan);
						LOGGER.info("Dental Plans saved successfully");	
						if(null != tenantList){
							serffUtils.saveTenantPlan(plan, tenantList, entityManager);	
							LOGGER.info("tenant plan saved successfully.");	
						}						
						if(null != costShareVarianceVO  && dentalParentId == -1 && costShareVarianceVO.getPlanId().getCellValue().endsWith("-01")){
							dentalParentId = plan.getId();
						}
					}
					return plan;
				}

	/**
	 * Method is used to get Supporting Document [SBC, EOC, BROCHURE, etc.] name and it's ECM-ID.
	 */
	private SupportingDocumentDTO getSupportingDocument(final Map<String, String> documents,
			final String issuerPlanNumberWithVariant, final String issuerPlanNumberWithoutVariant) {

		LOGGER.debug("getSupportingDocument() Start");
		SupportingDocumentDTO documentDTO = new SupportingDocumentDTO();
		String documentKey, documentName;

		try {
			// Set value for SBC Document
			documentName = issuerPlanNumberWithVariant + SerffConstants.POSTFIX_SBC;
			documentKey = documents.get(documentName);

			if (StringUtils.isBlank(documentKey)) {
				documentName = issuerPlanNumberWithoutVariant + SerffConstants.POSTFIX_SBC;
				documentKey = documents.get(documentName);
			}

			if (StringUtils.isNotBlank(documentKey)) {
				documentDTO.setSbcDocumentName(documentName);
				documentDTO.setSbcDocumentId(documentKey);
			}

			// Set value for EOC Document
			documentName = issuerPlanNumberWithVariant + SerffConstants.POSTFIX_EOC;
			documentKey = documents.get(documentName);

			if (StringUtils.isBlank(documentKey)) {
				documentName = issuerPlanNumberWithoutVariant + SerffConstants.POSTFIX_EOC;
				documentKey = documents.get(documentName);
			}

			if (StringUtils.isNotBlank(documentKey)) {
				documentDTO.setEocDocumentName(documentName);
				documentDTO.setEocDocumentId(documentKey);
			}

			// Set value for BROCHURE Document
			documentName = issuerPlanNumberWithVariant + SerffConstants.POSTFIX_BROCHURE;
			documentKey = documents.get(documentName);

			if (StringUtils.isBlank(documentKey)) {
				documentName = issuerPlanNumberWithoutVariant + SerffConstants.POSTFIX_BROCHURE;
				documentKey = documents.get(documentName);
			}

			if (StringUtils.isNotBlank(documentKey)) {
				documentDTO.setBrochureDocumentName(documentName);
				documentDTO.setBrochureDocumentId(documentKey);
			}

			// Set value for BENEFITS File
			documentName = SerffConstants.DATA_BENEFITS;
			documentKey = documents.get(documentName);

			if (StringUtils.isBlank(documentKey)) {
				documentName = SerffConstants.DATA_DENTAL_BENEFITS;
				documentKey = documents.get(documentName);
			}

			if (StringUtils.isNotBlank(documentKey)) {
				documentDTO.setBenefitFile(documentKey);
			}

			// Set value for RATE File
			documentName = SerffConstants.DATA_RATING_TABLE;
			documentKey = documents.get(documentName);

			if (StringUtils.isNotBlank(documentKey)) {
				documentDTO.setRateFile(documentKey);
			}
		}
		finally {
			LOGGER.debug("getSupportingDocument() End");
		}
		return documentDTO;
	}

	public boolean refreshHealthPlanBenefits(int applicableYear, PlansListVO plansListVO, BenefitsListVO benefitsListVO,
			EntityManager entityManager, boolean isDentalPlan, String hiosId) {
		PlanDisplayRulesMap planDispRulesMap = null;
		//Commented for HIX-63472 - use DB based display text rules 
		//if(SerffConstants.YES_ABBR.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SerffConfigurationEnum.USE_BENEFIT_DISPLAY_RULES_FROM_DB))) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Fetching Benefit Display Rules for Issuer: " + SecurityUtil.sanitizeForLogging(hiosId));
		}
			planDispRulesMap = planDisplayMapping.loadPlanDisplayRulesInMap(hiosId);
			if(!CollectionUtils.isEmpty(planDispRulesMap)) {
				LOGGER.info("Got " + planDispRulesMap.size() + " Benefit Display Rules for Issuer: " + planDispRulesMap.elements().nextElement().getHiosIssuerId());
			} else {
				LOGGER.warn("Got No Benefit Display Rules for Issuer: " + planDispRulesMap.elements().nextElement().getHiosIssuerId());
			}
		//}
		List<PlanAndBenefitsVO> plansVOList = plansListVO.getPlans();
		List<com.serff.template.plan.BenefitAttributeVO> currentPlanBAs = null;
		boolean isCompletePlan = checkIfBenefitsExistForCompletePlan(benefitsListVO) ;
		Map<String,String> benefitNames = null;
		String planInsuranceType = null;
		
		if(isDentalPlan) {
			benefitNames = serffUtils.getPropertyList(PlanmgmtConfigurationEnum.DENTAL_BENEFITS_LIST);
			planInsuranceType = Plan.PlanInsuranceType.DENTAL.toString();
		}
		else {
			benefitNames = serffUtils.getPropertyList(PlanmgmtConfigurationEnum.HEALTH_BENEFITS_LIST);
			planInsuranceType = Plan.PlanInsuranceType.HEALTH.toString();
		}
		LOGGER.debug("Refresh Benefits for Plans Called type " + (isDentalPlan ? "Dental" : "Health") );
		
		for (PlanAndBenefitsVO currentPlanVO : plansVOList) {
			List<com.serff.template.plan.CostShareVarianceVO> costShareVariancesList = currentPlanVO.getCostShareVariancesList().getCostShareVariance();
			
			if (!CollectionUtils.isEmpty(costShareVariancesList)) {
				
				for (com.serff.template.plan.CostShareVarianceVO costShareVarianceVO : costShareVariancesList) {
					String issuerPlanNumber = costShareVarianceVO.getPlanId().getCellValue().replaceAll(SerffConstants.HYPHEN, StringUtils.EMPTY);
					List<Plan> existingPlans = iPlanRepository.findByIssuerPlanNumberAndIsDeletedAndApplicableYearAndInsuranceType(issuerPlanNumber, Plan.IS_DELETED.N.toString(), applicableYear, planInsuranceType);
					
					if (null != existingPlans && existingPlans.size() == 1) {
						Plan plan = existingPlans.get(0);
						LOGGER.info("Refresh Benefits Called for planID " + issuerPlanNumber);
						Map<String, List<Map<String,String>>> benefitCopayCoinsData = getBenefitCopayCoinsData(costShareVarianceVO);
						List<String> existingBenefitsList = new ArrayList<String>();
						PlanDental planDental = null;
						PlanHealth planHealth = null;
						
						if(isDentalPlan) {
							planDental = plan.getPlanDental();
							List<PlanDentalBenefit> sourcePlanDentalBenefits; 
							sourcePlanDentalBenefits = iSerffPlanDentalBenefitRepository.findByPlanDentalId(planDental.getId());
							
							for (PlanDentalBenefit planDentalBenefit : sourcePlanDentalBenefits) {
								existingBenefitsList.add(planDentalBenefit.getName());
							}
						}
						else {
							planHealth = plan.getPlanHealth();
							
							if (isCompletePlan) {
								planHealth.setEhbCovered(SerffConstants.COMPLETED);
							}
							else {
								planHealth.setEhbCovered(SerffConstants.PARTIAL);
							}
							List<PlanHealthBenefit> sourcePlanHealthBenefits; 
							sourcePlanHealthBenefits = iSerffPlanHealthBenefitRepository.findByPlanHealthId(planHealth.getId());
							for (PlanHealthBenefit planHealthBenefit : sourcePlanHealthBenefits) {
								existingBenefitsList.add(planHealthBenefit.getName());
							}
						}

						if(null != benefitsListVO &&  null!=benefitsListVO.getBenefits()) {
							currentPlanBAs = benefitsListVO.getBenefits();
							for(com.serff.template.plan.BenefitAttributeVO currentPlanBA: currentPlanBAs) {
								if(null != currentPlanBA) {
									String benefitName = null;
									if(null != currentPlanBA.getBenefitTypeCode() && StringUtils.isNotEmpty(currentPlanBA.getBenefitTypeCode().getCellValue())) {
										benefitName = benefitNames.get(currentPlanBA.getBenefitTypeCode().getCellValue().replaceAll(SerffConstants.COMMA, SerffConstants.DOT).toLowerCase());
										if(null == benefitName) {
											benefitName = getDerivedBenefitName(currentPlanBA.getBenefitTypeCode().getCellValue());
											if(LOGGER.isInfoEnabled()) {
												LOGGER.info("No mapping found for Benefit name : " + SecurityUtil.sanitizeForLogging(currentPlanBA.getBenefitTypeCode().getCellValue()) + ". Using derived value " + benefitName);
											}
										}
									}
									
									//IF the BENEFITS ALREADY EXIST IN PLAN, CONTINUE
									if(null == benefitName || existingBenefitsList.contains(benefitName))
									{
										LOGGER.debug("Skipping Refresh Benefits for plan ID " + issuerPlanNumber + " benefitName: " + benefitName + ". Benefit already exist.");
										continue;
									}

									if(isDentalPlan) {
										PlanDentalBenefit planDentalBenefit = createPlanDentalBenefit(currentPlanBA);
										populateBenefitData(planDentalBenefit, benefitName, benefitCopayCoinsData, planDispRulesMap);
										planDentalBenefit.setPlanDental(planDental);
										planDentalBenefit.setEffStartDate(plan.getStartDate());
										planDentalBenefit.setEffEndDate(plan.getEndDate());
										//PlanDentalBenefits.add(PlanDentalBenefit);
										if (null != entityManager && entityManager.isOpen()) {
											planDentalBenefit = entityManager.merge(planDentalBenefit);
										}
										
									} else {
										PlanHealthBenefit planHealthBenefit = createPlanHealthBenefit(currentPlanBA);
										populateBenefitData(planHealthBenefit, benefitName, benefitCopayCoinsData, planHealth, planDispRulesMap);
										planHealthBenefit.setPlanHealth(planHealth);
										planHealthBenefit.setEffStartDate(plan.getStartDate());
										planHealthBenefit.setEffEndDate(plan.getEndDate());
										//planHealthBenefits.add(planHealthBenefit);
										if (null != entityManager && entityManager.isOpen()) {
											planHealthBenefit = entityManager.merge(planHealthBenefit);
										}
									}
								}
							}
						}
					} else {
						LOGGER.info("Skipping Refresh Benefits for planID " + issuerPlanNumber + " as either no or multiple plan exist");
					}
				}
			}
		}
		return true;
	}
	
	
	private PlanHealthBenefit createPlanHealthBenefit(com.serff.template.plan.BenefitAttributeVO currentPlanBA) {
		//Benefit Information
		PlanHealthBenefit planHealthBenefit = new PlanHealthBenefit();
		LOGGER.debug("Creating PlanHealthBenefit...");
		//set the incoming name value, it would be replaced with mapping later in the flow.
		if(null != currentPlanBA.getBenefitTypeCode() && StringUtils.isNotEmpty(currentPlanBA.getBenefitTypeCode().getCellValue())) {
			/*benefitValue = currentPlanBA.getBenefitTypeCode().getCellValue().replaceAll(",", ".");
			if(healthBenefitNames.get(benefitValue.toLowerCase()) != null) {*/
				//set the incoming name value, it would be replaced with mapping later in the flow.
				planHealthBenefit.setName(currentPlanBA.getBenefitTypeCode().getCellValue());
			/*} else {
				LOGGER.warn("Ignoring Benefit as no mapping found for Benefit name : " + currentPlanBA.getBenefitTypeCode().getCellValue());
				continue;
			}*/
		}

		if(null != currentPlanBA.getIsEHB() && StringUtils.isNotBlank(currentPlanBA.getIsEHB().getCellValue())) {
			planHealthBenefit.setIsEHB(currentPlanBA.getIsEHB().getCellValue());
		}

		if(null != currentPlanBA.getIsBenefitCovered() && StringUtils.isNotBlank(currentPlanBA.getIsBenefitCovered().getCellValue())) {
//			planHealthBenefit.setIsCovered(StringUtils.isNotBlank(currentPlanBA.getIsBenefitCovered().getCellValue()) ? currentPlanBA.getIsBenefitCovered().getCellValue() : NOT_COVERED);
			planHealthBenefit.setIsCovered(currentPlanBA.getIsBenefitCovered().getCellValue());
		}

		if(null != currentPlanBA.getIsStateMandate() && StringUtils.isNotBlank(currentPlanBA.getIsStateMandate().getCellValue())) {
			planHealthBenefit.setIsStateMandate(currentPlanBA.getIsStateMandate().getCellValue());
		}

		if(null != currentPlanBA.getServiceLimit() && StringUtils.isNotBlank(currentPlanBA.getServiceLimit().getCellValue())) {
			planHealthBenefit.setServiceLimit(currentPlanBA.getServiceLimit().getCellValue());
		}
		
		if(null != currentPlanBA.getQuantityLimit() && StringUtils.isNotBlank(currentPlanBA.getQuantityLimit().getCellValue())) {
//			planHealthBenefit.setNetworkLimitation(StringUtils.isNotBlank(currentPlanBA.getQuantityLimit().getCellValue().toString()) ? currentPlanBA.getQuantityLimit().getCellValue().toString():"No");
			planHealthBenefit.setNetworkLimitation(currentPlanBA.getQuantityLimit().getCellValue());
		}

		if(null != currentPlanBA.getUnitLimit() && StringUtils.isNotBlank(currentPlanBA.getUnitLimit().getCellValue())) {
			planHealthBenefit.setNetworkLimitationAttribute(currentPlanBA.getUnitLimit().getCellValue());
		}

		if(null != currentPlanBA.getMinimumStay() && StringUtils.isNotBlank(currentPlanBA.getMinimumStay().getCellValue())) {
			planHealthBenefit.setMinStay(currentPlanBA.getMinimumStay().getCellValue());
		}

		if(null != currentPlanBA.getExclusion() && StringUtils.isNotBlank(currentPlanBA.getExclusion().getCellValue())) {
			
			String exclusion = currentPlanBA.getExclusion().getCellValue();
			int exclusionLength = exclusion.length();
			if(exclusionLength > EXCLUSION_LEN) {
				LOGGER.warn("Truncating plan Health Benefit Exclusion text to " + EXCLUSION_LEN  + " characters: " + exclusion);
				exclusionLength = EXCLUSION_LEN ;
			}
			planHealthBenefit.setNetworkExceptions(exclusion.substring(0,  exclusionLength));
		}
	
		if(null != currentPlanBA.getEhbVarianceReason() && StringUtils.isNotBlank(currentPlanBA.getEhbVarianceReason().getCellValue())) {
			planHealthBenefit.setEhbVarianceReason(currentPlanBA.getEhbVarianceReason().getCellValue());
		}

		if(null != currentPlanBA.getExplanation() && StringUtils.isNotBlank(currentPlanBA.getExplanation().getCellValue())) {
			String explanation = StringUtils.trimToEmpty(currentPlanBA.getExplanation().getCellValue());
			if(explanation.length() > BENEFITS_EXPLANATION_TEXT_LEN) {
				LOGGER.warn("Truncating plan health benefit explanation text to " + BENEFITS_EXPLANATION_TEXT_LEN + " characters: " + explanation);
				explanation = explanation.substring(0, BENEFITS_EXPLANATION_TEXT_LEN);
			}
			planHealthBenefit.setExplanation(explanation);
		}

		//Deductible and Out of Pocket Inclusions and Exceptions
		if(null != currentPlanBA.getSubjectToDeductibleTier1() && null != currentPlanBA.getSubjectToDeductibleTier1().getCellValue()) {
			planHealthBenefit.setSubjectToInNetworkDuductible(StringUtils.isNotBlank(currentPlanBA.getSubjectToDeductibleTier1().getCellValue()) ? currentPlanBA.getSubjectToDeductibleTier1().getCellValue() : "No");
		}

		if(null != currentPlanBA.getSubjectToDeductibleTier2() && null != currentPlanBA.getSubjectToDeductibleTier2().getCellValue()) {
			planHealthBenefit.setSubjectToOutNetworkDeductible(StringUtils.isNotBlank(currentPlanBA.getSubjectToDeductibleTier2().getCellValue()) ? currentPlanBA.getSubjectToDeductibleTier2().getCellValue() : "No");
		}

		if(null != currentPlanBA.getExcludedOutOfNetworkMOOP() && null != currentPlanBA.getExcludedOutOfNetworkMOOP().getCellValue()) {
			planHealthBenefit.setExcludedFromOutOfNetworkMoop(currentPlanBA.getExcludedOutOfNetworkMOOP().getCellValue());
		}

		if(null != currentPlanBA.getExcludedInNetworkMOOP() && StringUtils.isNotBlank(currentPlanBA.getExcludedInNetworkMOOP().getCellValue())) {
			planHealthBenefit.setExcludedFromInNetworkMoop(currentPlanBA.getExcludedInNetworkMOOP().getCellValue());
		}
		return planHealthBenefit;
	}
	
	private void populateBenefitData(PlanHealthBenefit planHealthBenefit, String benefitName,
			Map<String, List<Map<String, String>>> benefitCopayCoinsData, PlanHealth planHealth, PlanDisplayRulesMap planDispRulesMap) {
		
		if (StringUtils.isNotEmpty(planHealthBenefit.getName())) {
			if(null !=benefitCopayCoinsData) {
				
				List<Map<String, String>> copayConinsDataList = benefitCopayCoinsData.get(planHealthBenefit.getName());
				
				if (!CollectionUtils.isEmpty(copayConinsDataList)) {
					
					LOGGER.debug("Populating PlanHealth BenefitData...");
					
					for (Map<String, String> copayConinsData : copayConinsDataList) {
	
						// Storing Actual Value Start:
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_IN_NETWORK_TIER1))) {
							planHealthBenefit.setCopayInNetworkTier1(copayConinsData.get(COPAY_IN_NETWORK_TIER1));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_IN_NETWORK_TIER2))) {
							planHealthBenefit.setCopayInNetworkTier2(copayConinsData.get(COPAY_IN_NETWORK_TIER2));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_OUT_OF_NETWORK))) {
							planHealthBenefit.setCopayOutOfNetwork(copayConinsData.get(COPAY_OUT_OF_NETWORK));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINSURANCE_IN_NETWORK_TIER1))) {
							planHealthBenefit.setCoinsuranceInNetworkTier1(copayConinsData.get(COINSURANCE_IN_NETWORK_TIER1));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINSURANCE_IN_NETWORK_TIER2))) {
							planHealthBenefit.setCoinsuranceInNetworkTier2(copayConinsData.get(COINSURANCE_IN_NETWORK_TIER2));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINSURANCE_OUT_OF_NETWORK))) {
							planHealthBenefit.setCoinsuranceOutOfNetwork(copayConinsData.get(COINSURANCE_OUT_OF_NETWORK));
						}
						// Storing Actual Value End.
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_NETWORK_TIER1_VALUE))) {
							planHealthBenefit.setNetworkT1CopayVal(copayConinsData.get(COPAY_NETWORK_TIER1_VALUE));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_NETWORK_TIER1_ATTR))) {
							planHealthBenefit.setNetworkT1CopayAttr(copayConinsData.get(COPAY_NETWORK_TIER1_ATTR));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_NETWORK_TIER2_VALUE))) {
							planHealthBenefit.setNetworkT2CopayVal(copayConinsData.get(COPAY_NETWORK_TIER2_VALUE));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_NETWORK_TIER2_ATTR))) {
							planHealthBenefit.setNetworkT2CopayAttr(copayConinsData.get(COPAY_NETWORK_TIER2_ATTR));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_NO_NETWORK_VALUE))) {
							planHealthBenefit.setOutOfNetworkCopayVal(copayConinsData.get(COPAY_NO_NETWORK_VALUE));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_NO_NETWORK_ATTR))) {
							planHealthBenefit.setOutOfNetworkCopayAttr(copayConinsData.get(COPAY_NO_NETWORK_ATTR));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINS_NETWORK_TIER1_VALUE))) {
							planHealthBenefit.setNetworkT1CoinsurVal(copayConinsData.get(COINS_NETWORK_TIER1_VALUE));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINS_NETWORK_TIER1_ATTR))) {
							planHealthBenefit.setNetworkT1CoinsurAttr(copayConinsData.get(COINS_NETWORK_TIER1_ATTR));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINS_NETWORK_TIER2_VALUE))) {
							planHealthBenefit.setNetworkT2CoinsurVal(copayConinsData.get(COINS_NETWORK_TIER2_VALUE));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINS_NETWORK_TIER2_ATTR))) {
							planHealthBenefit.setNetworkT2CoinsurAttr(copayConinsData.get(COINS_NETWORK_TIER2_ATTR));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINS_NO_NETWORK_VALUE))) {
							planHealthBenefit.setOutOfNetworkCoinsurVal(copayConinsData.get(COINS_NO_NETWORK_VALUE));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINS_NO_NETWORK_ATTR))) {
							planHealthBenefit.setOutOfNetworkCoinsurAttr(copayConinsData.get(COINS_NO_NETWORK_ATTR));
						}
					}
				}
			}
			String tileDisplayValue = null, t1DisplayValue = null, t2DisplayValue = null, oonDisplayValue = null;
			Long coinsAfterNumOfCopay = planHealth.getBeginPmrycareDedtOrCoinsAfterNumOfCopays();
			Long afterNumOfVisit = planHealth.getBeginPmryCareCsAfterNumOfVisits();
			if(coinsAfterNumOfCopay == null) {
				coinsAfterNumOfCopay = 0L;
			}
			if(afterNumOfVisit == null) {
				afterNumOfVisit = 0L;
			}
			
			if(CollectionUtils.isEmpty(planDispRulesMap) ) {
				LOGGER.warn("Getting Benefit Display text for Health Plan using default hardwired logic");
				t1DisplayValue = serffUtils.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit, planHealthBenefit.getNetworkT1CoinsurVal(), 
					planHealthBenefit.getNetworkT1CoinsurAttr(), planHealthBenefit.getNetworkT1CopayVal(), planHealthBenefit.getNetworkT1CopayAttr(), benefitName, true, isPHIXProfile);
	
				tileDisplayValue = serffUtils.getBenefitDisplayTileText(planHealthBenefit.getNetworkT1CoinsurVal(), 
						planHealthBenefit.getNetworkT1CoinsurAttr(), planHealthBenefit.getNetworkT1CopayVal(), planHealthBenefit.getNetworkT1CopayAttr(), isPHIXProfile);
	
				t2DisplayValue = serffUtils.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit, planHealthBenefit.getNetworkT2CoinsurVal(), 
						planHealthBenefit.getNetworkT2CoinsurAttr(), planHealthBenefit.getNetworkT2CopayVal(), planHealthBenefit.getNetworkT2CopayAttr(), benefitName, false, isPHIXProfile);
	
				oonDisplayValue = serffUtils.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit, planHealthBenefit.getOutOfNetworkCoinsurVal(), 
						planHealthBenefit.getOutOfNetworkCoinsurAttr(), planHealthBenefit.getOutOfNetworkCopayVal(), planHealthBenefit.getOutOfNetworkCopayAttr(), benefitName, false, isPHIXProfile);
			} else {
				LOGGER.debug("Getting Benefit Display text for Health Plan using benefitText rules for HIOS ID: " + planDispRulesMap.elements().nextElement().getHiosIssuerId());
				BenefitsDisplayMappingParamsDTO benefitsDisplayMappingParamsDTO = null;
				if(planDisplayMapping.isExtendedParamSupported()) {
					benefitsDisplayMappingParamsDTO = new BenefitsDisplayMappingParamsDTO();
					benefitsDisplayMappingParamsDTO.setBenefitName(benefitName);
					if(null != planHealth.getMaxCoinsuranceForSPDrug()) {
						benefitsDisplayMappingParamsDTO.setMaximumCoinsuranceForSpecialtyDrugs(planHealth.getMaxCoinsuranceForSPDrug());
					}
					if(null != planHealth.getMaxDaysForChargingInpatientCopay()) {
						benefitsDisplayMappingParamsDTO.setMaxNumDaysForChargingInpatientCopay(planHealth.getMaxDaysForChargingInpatientCopay());
					}
					benefitsDisplayMappingParamsDTO.setMedicalAndDrugIntegrated(YES.equalsIgnoreCase(planHealth.getIsMedDrugDeductibleIntegrated()));
					benefitsDisplayMappingParamsDTO.setPlanType(planHealth.getPlan().getNetworkType());
				}
				tileDisplayValue = planDisplayMapping.getBenefitDisplayTileText(planHealthBenefit.getNetworkT1CoinsurVal(), 
						planHealthBenefit.getNetworkT1CoinsurAttr(), planHealthBenefit.getNetworkT1CopayVal(), planHealthBenefit.getNetworkT1CopayAttr(), planDispRulesMap);
				if(null != benefitsDisplayMappingParamsDTO) {
					benefitsDisplayMappingParamsDTO.setNetworkType(BenefitsDisplayMappingParamsDTO.NetworkType.IN_NETWORK_TIER1);
				}
				t1DisplayValue = planDisplayMapping.getBenefitDisplayText(planHealthBenefit.getNetworkT1CoinsurVal(), planHealthBenefit.getNetworkT1CoinsurAttr(), 
						planHealthBenefit.getNetworkT1CopayVal(), planHealthBenefit.getNetworkT1CopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, 
						SerffUtils.PRIMARY_CARE_BENEFIT.equals(benefitName), planDispRulesMap, benefitsDisplayMappingParamsDTO);
				if(null != benefitsDisplayMappingParamsDTO) {
					benefitsDisplayMappingParamsDTO.setNetworkType(BenefitsDisplayMappingParamsDTO.NetworkType.IN_NETWORK_TIER2);
				}
				t2DisplayValue = planDisplayMapping.getBenefitDisplayText(planHealthBenefit.getNetworkT2CoinsurVal(), planHealthBenefit.getNetworkT2CoinsurAttr(), 
						planHealthBenefit.getNetworkT2CopayVal(), planHealthBenefit.getNetworkT2CopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, 
						false, planDispRulesMap, benefitsDisplayMappingParamsDTO);
				if(null != benefitsDisplayMappingParamsDTO) {
					benefitsDisplayMappingParamsDTO.setNetworkType(BenefitsDisplayMappingParamsDTO.NetworkType.OUT_OF_NETWORK);
				}
				oonDisplayValue = planDisplayMapping.getBenefitDisplayText(planHealthBenefit.getOutOfNetworkCoinsurVal(), planHealthBenefit.getOutOfNetworkCoinsurAttr(), 
						planHealthBenefit.getOutOfNetworkCopayVal(), planHealthBenefit.getOutOfNetworkCopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, 
						false, planDispRulesMap, benefitsDisplayMappingParamsDTO);
			}
			planHealthBenefit.setNetworkT1TileDisplay(tileDisplayValue);
			planHealthBenefit.setNetworkT1display(t1DisplayValue);
			planHealthBenefit.setNetworkT2display(t2DisplayValue);
			planHealthBenefit.setOutOfNetworkDisplay(oonDisplayValue);
			planHealthBenefit.setLimitExcepDisplay(serffUtils.getLimitAndExclusionText(planHealthBenefit.getNetworkLimitation(), 
									planHealthBenefit.getNetworkLimitationAttribute(), planHealthBenefit.getNetworkExceptions()));
			planHealthBenefit.setName(benefitName);
		}		
	}
	
	
	
	private Map<String, List<Map<String,String>>> getBenefitCopayCoinsData(com.serff.template.plan.CostShareVarianceVO costShareVarianceVO) {
		Map<String, List<Map<String,String>>> benefitCopayCoinsData = new HashMap<String, List<Map<String, String>>>();
		if (null != costShareVarianceVO && null != costShareVarianceVO.getServiceVisitList()
				&& null != costShareVarianceVO.getServiceVisitList().getServiceVisit()
				&& !CollectionUtils.isEmpty(costShareVarianceVO.getServiceVisitList().getServiceVisit())) {
			List<ServiceVisitVO> serviceVisits = costShareVarianceVO.getServiceVisitList().getServiceVisit();
			String defaultCopayNetworkAttr = COPAY;
			String defaultCoinsNetworkAttr = COINSURANCE;

			for (ServiceVisitVO serviceVisit : serviceVisits) {
				Map<String, String> copayCoinsData = new HashMap<String, String>();
				List<Map<String, String>> copayCoinsList = new ArrayList<Map<String, String>>();

				if (null != serviceVisit.getVisitType() && StringUtils.isNotEmpty(serviceVisit.getVisitType().getCellValue())) {
					retrieveCopayCoinsData(serviceVisit.getCopayInNetworkTier1(), copayCoinsData, COPAY_IN_NETWORK_TIER1,
							COPAY_NETWORK_TIER1_ATTR, COPAY_NETWORK_TIER1_VALUE, defaultCopayNetworkAttr);

					retrieveCopayCoinsData(serviceVisit.getCopayInNetworkTier2(), copayCoinsData, COPAY_IN_NETWORK_TIER2,
							COPAY_NETWORK_TIER2_ATTR, COPAY_NETWORK_TIER2_VALUE, defaultCopayNetworkAttr);
					
					retrieveCopayCoinsData(serviceVisit.getCopayOutOfNetwork(), copayCoinsData, COPAY_OUT_OF_NETWORK,
							COPAY_NO_NETWORK_ATTR, COPAY_NO_NETWORK_VALUE, defaultCopayNetworkAttr);

					retrieveCopayCoinsData(serviceVisit.getCoInsuranceInNetworkTier1(), copayCoinsData, COINSURANCE_IN_NETWORK_TIER1,
							COINS_NETWORK_TIER1_ATTR, COINS_NETWORK_TIER1_VALUE, defaultCoinsNetworkAttr);

					retrieveCopayCoinsData(serviceVisit.getCoInsuranceInNetworkTier2(), copayCoinsData, COINSURANCE_IN_NETWORK_TIER2,
							COINS_NETWORK_TIER2_ATTR, COINS_NETWORK_TIER2_VALUE, defaultCoinsNetworkAttr);

					retrieveCopayCoinsData(serviceVisit.getCoInsuranceOutOfNetwork(), copayCoinsData, COINSURANCE_OUT_OF_NETWORK,
							COINS_NO_NETWORK_ATTR, COINS_NO_NETWORK_VALUE, defaultCoinsNetworkAttr);

					copayCoinsList.add(copayCoinsData);
					benefitCopayCoinsData.put(serviceVisit.getVisitType().getCellValue(), copayCoinsList);
				}
			}
		}		
		return benefitCopayCoinsData;
	}
	
	private void retrieveCopayCoinsData(com.serff.template.plan.ExcelCellVO excelCellVO,
			Map<String, String> copayCoinsData, String actualValueName, String attrName, String valueName, String defaultAttrValue) {
		
		if (null != excelCellVO && null != excelCellVO.getCellValue()) {
			
			String value = excelCellVO.getCellValue();
			copayCoinsData.put(actualValueName, value);
			
			if (!value.startsWith(NO_CHARGE)
					&& !value.startsWith(NOT_APPLICABLE)) {
				
				value = value.trim();

				int spaceIndex = value.indexOf(SerffConstants.SPACE);

				if (spaceIndex > 0) {
					String attr = value.substring(spaceIndex + 1, value.length());
					value = value.substring(0, spaceIndex);
					copayCoinsData.put(attrName, attr);
				} else {
					copayCoinsData.put(attrName, defaultAttrValue);
				}
				
				if (value.indexOf(DOLLARSIGN) > -1) {
					value = value.replaceAll(DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
				}
				else if (value.indexOf(PERCENTAGE) > -1) {
					value = value.replaceAll(PERCENTAGE, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY);
				}

				copayCoinsData.put(valueName, value);
			} else {
				copayCoinsData.put(valueName, ZERO);
				copayCoinsData.put(attrName, value);
			}
		}
	}
	
	private PlanDentalBenefit createPlanDentalBenefit(com.serff.template.plan.BenefitAttributeVO currentPlanBA) {
		PlanDentalBenefit planDentalBenefit = new PlanDentalBenefit();
		
		if(null != currentPlanBA.getBenefitTypeCode() && StringUtils.isNotEmpty(currentPlanBA.getBenefitTypeCode().getCellValue())) {
			/*benefitValue = currentPlanBA.getBenefitTypeCode().getCellValue().replaceAll(",", ".");
			if(dentalBenefitNames.get(benefitValue.toLowerCase()) != null) {*/
				//set the incoming name value, it would be replaced with mapping later in the flow.
				planDentalBenefit.setName(currentPlanBA.getBenefitTypeCode().getCellValue());
			/*} else {
				LOGGER.warn("Ignoring Benefit as no mapping found for Benefit name : " + currentPlanBA.getBenefitTypeCode().getCellValue());
				continue;
			}*/
		}

		if(null != currentPlanBA.getIsEHB() && null != currentPlanBA.getIsEHB().getCellValue()) {
			planDentalBenefit.setIsEHB(currentPlanBA.getIsEHB().getCellValue());
		}

		if(null != currentPlanBA.getIsBenefitCovered() && null != currentPlanBA.getIsBenefitCovered().getCellValue()) {
			planDentalBenefit.setIsCovered(StringUtils.isNotBlank(currentPlanBA.getIsBenefitCovered().getCellValue()) ? currentPlanBA.getIsBenefitCovered().getCellValue() : NOT_COVERED);
		}

		//Ask Archana plan.getPlanHealth().getPlanHealthBenefits().get("").set planBenefitTemplate.getPackages().get(0).getPlans().get(0).getBenefitAttributes().get(0).getServiceLimit();

		if(null != currentPlanBA.getQuantityLimit() && StringUtils.isNotBlank(currentPlanBA.getQuantityLimit().getCellValue())) {
//			planDentalBenefit.setNetworkLimitation(StringUtils.isNotBlank(currentPlanBA.getQuantityLimit().getCellValue().toString()) ? currentPlanBA.getQuantityLimit().getCellValue().toString():"No");
			planDentalBenefit.setNetworkLimitation(currentPlanBA.getQuantityLimit().getCellValue());
		}

		if(null != currentPlanBA.getUnitLimit() && StringUtils.isNotBlank(currentPlanBA.getUnitLimit().getCellValue())) {
			planDentalBenefit.setNetworkLimitationAttribute(currentPlanBA.getUnitLimit().getCellValue());
		}

		if(null != currentPlanBA.getMinimumStay() && StringUtils.isNotBlank(currentPlanBA.getMinimumStay().getCellValue())) {
			planDentalBenefit.setMinStay(currentPlanBA.getMinimumStay().getCellValue());
		}

			if(null != currentPlanBA.getExclusion() && StringUtils.isNotBlank(currentPlanBA.getExclusion().getCellValue())) {
				
				String exclusion = currentPlanBA.getExclusion().getCellValue();
				int exclusionLength = exclusion.length();
				if(exclusionLength >EXCLUSION_LEN ) {
					LOGGER.warn("Truncating plan Dental Benefit Exclusion text to " + EXCLUSION_LEN  + " characters : " + currentPlanBA.getExclusion().getCellValue());
					exclusionLength = EXCLUSION_LEN ;
				}
				planDentalBenefit.setNetworkExceptions(exclusion.substring(0,  exclusionLength));
			}
		
			if(null != currentPlanBA.getExplanation() && StringUtils.isNotBlank(currentPlanBA.getExplanation().getCellValue())) {
				String explanation = StringUtils.trimToEmpty(currentPlanBA.getExplanation().getCellValue());
				if(explanation.length() > BENEFITS_EXPLANATION_TEXT_LEN) {
					LOGGER.warn("Truncating plan dental benefit explanation text to " + BENEFITS_EXPLANATION_TEXT_LEN + " characters: " + explanation);
					explanation = explanation.substring(0, BENEFITS_EXPLANATION_TEXT_LEN);
				}
				planDentalBenefit.setExplanation(explanation);
			}

			//Deductible and Out of Pocket Inclusions and Exceptions

			if(null != currentPlanBA.getSubjectToDeductibleTier1() && null != currentPlanBA.getSubjectToDeductibleTier1().getCellValue()) {
				planDentalBenefit.setSubjectToInNetworkDuductible(StringUtils.isNotBlank(currentPlanBA.getSubjectToDeductibleTier1().getCellValue()) ? currentPlanBA.getSubjectToDeductibleTier1().getCellValue() : "No");
			}

			if(null != currentPlanBA.getSubjectToDeductibleTier2() && null != currentPlanBA.getSubjectToDeductibleTier2().getCellValue()) {
				planDentalBenefit.setSubjectToOutNetworkDeductible(StringUtils.isNotBlank(currentPlanBA.getSubjectToDeductibleTier2().getCellValue()) ? currentPlanBA.getSubjectToDeductibleTier2().getCellValue() : "No");
			}

			if(null != currentPlanBA.getExcludedOutOfNetworkMOOP() && null != currentPlanBA.getExcludedOutOfNetworkMOOP().getCellValue()) {
				planDentalBenefit.setExcludedFromOutOfNetworkMoop(currentPlanBA.getExcludedOutOfNetworkMOOP().getCellValue());
			}

			if(null != currentPlanBA.getExcludedInNetworkMOOP() && StringUtils.isNotBlank(currentPlanBA.getExcludedInNetworkMOOP().getCellValue())) {
				planDentalBenefit.setExcludedFromInNetworkMoop(currentPlanBA.getExcludedInNetworkMOOP().getCellValue());
			}
			return planDentalBenefit;
	}

	private void populateBenefitData(PlanDentalBenefit planDentalBenefit, String benefitName,
			Map<String, List<Map<String, String>>> benefitCopayCoinsData, PlanDisplayRulesMap planDispRulesMap) {
		
		if (StringUtils.isNotEmpty(planDentalBenefit.getName())) {

			if(null != benefitCopayCoinsData) {
			
				List<Map<String, String>> copayConinsDataList = benefitCopayCoinsData.get(planDentalBenefit.getName());
				
				if (!CollectionUtils.isEmpty(copayConinsDataList)) {
					LOGGER.debug("Populating PlanDental BenefitData...");
					
					for (Map<String, String> copayConinsData : copayConinsDataList) {
	
						// Storing Actual Value Start:
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_IN_NETWORK_TIER1))) {
							planDentalBenefit.setCopayInNetworkTier1(copayConinsData.get(COPAY_IN_NETWORK_TIER1));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_IN_NETWORK_TIER2))) {
							planDentalBenefit.setCopayInNetworkTier2(copayConinsData.get(COPAY_IN_NETWORK_TIER2));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COPAY_OUT_OF_NETWORK))) {
							planDentalBenefit.setCopayOutOfNetwork(copayConinsData.get(COPAY_OUT_OF_NETWORK));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINSURANCE_IN_NETWORK_TIER1))) {
							planDentalBenefit.setCoinsuranceInNetworkTier1(copayConinsData.get(COINSURANCE_IN_NETWORK_TIER1));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINSURANCE_IN_NETWORK_TIER2))) {
							planDentalBenefit.setCoinsuranceInNetworkTier2(copayConinsData.get(COINSURANCE_IN_NETWORK_TIER2));
						}
	
						if (StringUtils.isNotEmpty(copayConinsData.get(COINSURANCE_OUT_OF_NETWORK))) {
							planDentalBenefit.setCoinsuranceOutOfNetwork(copayConinsData.get(COINSURANCE_OUT_OF_NETWORK));
						}
						// Storing Actual Value End.
	
						if(StringUtils.isNotEmpty(copayConinsData.get(COPAY_NETWORK_TIER1_VALUE))){
							planDentalBenefit.setNetworkT1CopayVal(copayConinsData.get(COPAY_NETWORK_TIER1_VALUE));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COPAY_NETWORK_TIER1_ATTR))){
							planDentalBenefit.setNetworkT1CopayAttr(copayConinsData.get(COPAY_NETWORK_TIER1_ATTR));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COPAY_NETWORK_TIER2_VALUE))){
							planDentalBenefit.setNetworkT2CopayVal(copayConinsData.get(COPAY_NETWORK_TIER2_VALUE));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COPAY_NETWORK_TIER2_ATTR))){
							planDentalBenefit.setNetworkT2CopayAttr(copayConinsData.get(COPAY_NETWORK_TIER2_ATTR));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COPAY_NO_NETWORK_VALUE))){
							planDentalBenefit.setOutOfNetworkCopayVal(copayConinsData.get(COPAY_NO_NETWORK_VALUE));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COPAY_NO_NETWORK_ATTR))){
							planDentalBenefit.setOutOfNetworkCopayAttr(copayConinsData.get(COPAY_NO_NETWORK_ATTR));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COINS_NETWORK_TIER1_VALUE))){
							planDentalBenefit.setNetworkT1CoinsurVal(copayConinsData.get(COINS_NETWORK_TIER1_VALUE));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COINS_NETWORK_TIER1_ATTR))){
							planDentalBenefit.setNetworkT1CoinsurAttr(copayConinsData.get(COINS_NETWORK_TIER1_ATTR));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COINS_NETWORK_TIER2_VALUE))){
							planDentalBenefit.setNetworkT2CoinsurVal(copayConinsData.get(COINS_NETWORK_TIER2_VALUE));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COINS_NETWORK_TIER2_ATTR))){
							planDentalBenefit.setNetworkT2CoinsurAttr(copayConinsData.get(COINS_NETWORK_TIER2_ATTR));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COINS_NO_NETWORK_VALUE))){
							planDentalBenefit.setOutOfNetworkCoinsurVal(copayConinsData.get(COINS_NO_NETWORK_VALUE));
						}
		
						if(StringUtils.isNotEmpty(copayConinsData.get(COINS_NO_NETWORK_ATTR))){
							planDentalBenefit.setOutOfNetworkCoinsurAttr(copayConinsData.get(COINS_NO_NETWORK_ATTR));
						}
					}
				}
			}
			String tileDisplayValue = null, t1DisplayValue = null, t2DisplayValue = null, oonDisplayValue = null;
			Long coinsAfterNumOfCopay = 0L;//planDental.getBeginPmrycareDedtOrCoinsAfterNumOfCopays();
			Long afterNumOfVisit = 0L;//planDental.getBeginPmryCareCsAfterNumOfVisits();
			if(CollectionUtils.isEmpty(planDispRulesMap) ) {
				LOGGER.warn("Getting Benefit Display text for Dental Plan using default hardwired logic");
				t1DisplayValue = serffUtils.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit, planDentalBenefit.getNetworkT1CoinsurVal(), 
						planDentalBenefit.getNetworkT1CoinsurAttr(), planDentalBenefit.getNetworkT1CopayVal(), planDentalBenefit.getNetworkT1CopayAttr(), benefitName, true, isPHIXProfile);
	
				tileDisplayValue = serffUtils.getBenefitDisplayTileText(planDentalBenefit.getNetworkT1CoinsurVal(), 
						planDentalBenefit.getNetworkT1CoinsurAttr(), planDentalBenefit.getNetworkT1CopayVal(), planDentalBenefit.getNetworkT1CopayAttr(), isPHIXProfile);
	
				t2DisplayValue = serffUtils.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit, planDentalBenefit.getNetworkT2CoinsurVal(), 
						planDentalBenefit.getNetworkT2CoinsurAttr(), planDentalBenefit.getNetworkT2CopayVal(), planDentalBenefit.getNetworkT2CopayAttr(), benefitName, false, isPHIXProfile);
	
				oonDisplayValue = serffUtils.getBenefitDisplayText(coinsAfterNumOfCopay, afterNumOfVisit, planDentalBenefit.getOutOfNetworkCoinsurVal(), 
						planDentalBenefit.getOutOfNetworkCoinsurAttr(), planDentalBenefit.getOutOfNetworkCopayVal(), planDentalBenefit.getOutOfNetworkCopayAttr(), benefitName, false, isPHIXProfile);
			} else {
				LOGGER.debug("Getting Benefit Display text for Dental Plan using benefitText rules for HIOS ID: " + planDispRulesMap.elements().nextElement().getHiosIssuerId());
				BenefitsDisplayMappingParamsDTO benefitsDisplayMappingParamsDTO = null;
				/*if(planDisplayMapping.isExtendedParamSupported()) {
					benefitsDisplayMappingParamsDTO = new BenefitsDisplayMappingParamsDTO();
					benefitsDisplayMappingParamsDTO.setBenefitName(benefitName);
				}*/
				tileDisplayValue = planDisplayMapping.getBenefitDisplayTileText(planDentalBenefit.getNetworkT1CoinsurVal(), 
						planDentalBenefit.getNetworkT1CoinsurAttr(), planDentalBenefit.getNetworkT1CopayVal(), planDentalBenefit.getNetworkT1CopayAttr(), planDispRulesMap);
				t1DisplayValue = planDisplayMapping.getBenefitDisplayText(planDentalBenefit.getNetworkT1CoinsurVal(), planDentalBenefit.getNetworkT1CoinsurAttr(), 
						planDentalBenefit.getNetworkT1CopayVal(), planDentalBenefit.getNetworkT1CopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, 
						false, planDispRulesMap, benefitsDisplayMappingParamsDTO);
				t2DisplayValue = planDisplayMapping.getBenefitDisplayText(planDentalBenefit.getNetworkT2CoinsurVal(), planDentalBenefit.getNetworkT2CoinsurAttr(), 
						planDentalBenefit.getNetworkT2CopayVal(), planDentalBenefit.getNetworkT2CopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, 
						false, planDispRulesMap, benefitsDisplayMappingParamsDTO);
				oonDisplayValue = planDisplayMapping.getBenefitDisplayText(planDentalBenefit.getOutOfNetworkCoinsurVal(), planDentalBenefit.getOutOfNetworkCoinsurAttr(), 
						planDentalBenefit.getOutOfNetworkCopayVal(), planDentalBenefit.getOutOfNetworkCopayAttr(), coinsAfterNumOfCopay, afterNumOfVisit, 
						false, planDispRulesMap, benefitsDisplayMappingParamsDTO);
			}
			planDentalBenefit.setNetworkT1display(t1DisplayValue);
			planDentalBenefit.setNetworkT1TileDisplay(tileDisplayValue);
			planDentalBenefit.setNetworkT2display(t2DisplayValue);
			planDentalBenefit.setOutOfNetworkDisplay(oonDisplayValue);
			planDentalBenefit.setLimitExcepDisplay(serffUtils.getLimitAndExclusionText(planDentalBenefit.getNetworkLimitation(), 
					planDentalBenefit.getNetworkLimitationAttribute(), planDentalBenefit.getNetworkExceptions()));
			planDentalBenefit.setName(benefitName);
		}
	}
	
	private boolean checkIfBenefitsExistForCompletePlan(BenefitsListVO benefitsListVO) {
		boolean isCompletePlan = false ;
		//HIX-15521 - Check for EHB covered to set Partial Or Complete Plan
		//Change in logic to check if plan is a complete plan or partial plan -
		if(null != benefitsListVO && null!= benefitsListVO.getBenefits()) {
			//currentPlanBAs = currentPBVO.getBenefitAttributesList().getBenefitAttributes();
			List<com.serff.template.plan.BenefitAttributeVO> currentPlanBAs = benefitsListVO.getBenefits();
			String benefitType = null;
			String benefitCovered = null;
			
			for (com.serff.template.plan.BenefitAttributeVO currentPlanBA : currentPlanBAs) {
				
				if (null != currentPlanBA && null != currentPlanBA.getBenefitTypeCode()) {
					benefitType = currentPlanBA.getBenefitTypeCode().getCellValue();
					//HIX-108863 - Basic Dental Care or Dental Checkup for child, any one if covered makes it a complete plan
					if ((BASIC_DENTAL_CARE_CHILD.equalsIgnoreCase(benefitType) || DENTAL_CHECKUP_CHILD.equalsIgnoreCase(benefitType))
							&& null != currentPlanBA.getIsEHB()) {
						String isEHB = currentPlanBA.getIsEHB().getCellValue();
						
						if (YES.equalsIgnoreCase(isEHB)) {
							
							benefitCovered = currentPlanBA.getIsBenefitCovered().getCellValue();
							if (COVERED.equalsIgnoreCase(benefitCovered)) {
								isCompletePlan = true;
								break;
							}
						}
					}
				}
			}
		}
		return isCompletePlan;
	}
	
	private String getDerivedBenefitName(String benefitName) {
		return benefitName.replaceAll(SerffConstants.SPECIAL_CHARS_REGEX, SerffConstants.UNDERSCORE).toUpperCase();
	}

	private boolean validateAmount(String value) {

		boolean flag = false;

		try {
			if(!StringUtils.isEmpty(value)){
				LOGGER.debug("Amount is " + Double.valueOf(value));
				flag = true;
			}
		}
		catch (Exception ex) {
			LOGGER.debug("Invalid Amount value: " + ex.getMessage(), ex);
		}
		return flag;
	}

	@Override
	public Plan getRequiredPlan(int issuerID, String issuerPlanNumber) {
		Plan plan=iPlanRepository.getRequiredPlan(issuerID, issuerPlanNumber);
		if(plan!=null){
			return plan;
		}else{
			return null;
		}
	}

	@Override
	public PlanHealth getRequiredPlanHealthObject(int issuerID, int planID,
			String csType) {
		PlanHealth planHealth=iSerffPlanHealthRepository.getRequiredPlanHealthObject(issuerID, planID, csType);
		if(planHealth!=null){
			return planHealth;
		}else{
			return null;
		}
	}

	@Override
	public String updateCsrAmount(Float csrAdvPayAmt, Integer issuerID,
			Integer planID, String csType) {

		/*if(null == entityManager || !entityManager.isOpen()) {
			return null;
		}*/
		PlanHealth planHealth=getRequiredPlanHealthObject(issuerID,planID,csType);
		planHealth.setCsrAdvPayAmt(csrAdvPayAmt);
		iSerffPlanHealthRepository.save(planHealth);
		//entityManager.merge(planHealth);
		return GhixConstants.RESPONSE_SUCCESS;
	}

	@Override
	public Plan getPlanByHIOSID(String hiosId) {
		return iPlanRepository.getPlanByHIOSID(hiosId);
	}

	/**
	 * This method verifyies whether the provided plan or any of its variance is CERTIFIED
	 * @author Geetha Chandran
	 * @since 16 July, 2013
	 * @return boolean
	 */
	private Map<String, Boolean> validatePlan(String planInsuranceType, int applicableYear, PlanAndBenefitsVO planAndBenefitsVO) {

		Map<String, Boolean> planStatus = new HashMap<String, Boolean>();
		planStatus.put(IS_EXISTING_PLAN, false);
		planStatus.put(IS_CERTIFIED_PLAN, false);
		planStatus.put(IS_NONPUF_PLAN, false);
		
		boolean isNonPUFPlanExist = false;
		boolean isCertifiedPlanExist = false;
		
		if (null != planAndBenefitsVO.getPlanAttributes()
				&& null != planAndBenefitsVO.getPlanAttributes().getStandardComponentID()
				&& StringUtils.isNotEmpty(planAndBenefitsVO.getPlanAttributes().getStandardComponentID().getCellValue())) {
			
			String planId = planAndBenefitsVO.getPlanAttributes().getStandardComponentID().getCellValue();
			List<Plan> existingPlans = iPlanRepository.findByIssuerPlanNumberStartingWithAndIsDeletedAndApplicableYearAndInsuranceType(planId, Plan.IS_DELETED.N.toString(), applicableYear, planInsuranceType);

			if (!CollectionUtils.isEmpty(existingPlans)) {
				
				LOGGER.info("Plan exists for given plan number " + planId);
				planStatus.put(IS_EXISTING_PLAN, true);
				String status = null;
				
				for (Plan existingPlan : existingPlans) {
					
					status = existingPlan.getStatus();
					
					if (PlanStatus.CERTIFIED.toString().equalsIgnoreCase(status)) {
						
						LOGGER.debug("Plan Status is CERTIFIED.");
						if (!isCertifiedPlanExist) {
							LOGGER.info("Plan id " + planId + " is already CERTIFIED.");
							isCertifiedPlanExist = true;
						}
					}
					
					if (!IS_PUF.Y.equals(existingPlan.getIsPUF())) {
						LOGGER.debug("Plan is not a PUF plan.");
						
						if (!isNonPUFPlanExist) {
							LOGGER.info("Non-Puf Plan id " + planId + " already exist.");
							isNonPUFPlanExist = true;
						}
					}
				}
			}
			planStatus.put(IS_CERTIFIED_PLAN, isCertifiedPlanExist);
			planStatus.put(IS_NONPUF_PLAN, isNonPUFPlanExist);
		}
		return planStatus;
	}
	
	/**
	 * Method is used to get Applicable Year from Plan Effective Date.
	 * @param planEffectiveDate, Plan Effective Date in String format.
	 * @return Applicable Year in Integer format.
	 */
	private Integer getApplicableYear(String planEffectiveDate) {
		
		Integer applicableYear = null;
		LOGGER.info("getApplicableYear() Start");
		
		try {
			
			LOGGER.debug("planEffectiveDate: " + planEffectiveDate);
			
			if (StringUtils.isNotBlank(planEffectiveDate)) {
				Calendar expirationCal = Calendar.getInstance();
				expirationCal.setTime(dateFormat.parse(planEffectiveDate));
				applicableYear = expirationCal.get(Calendar.YEAR);
			}
		}
		catch (ParseException parseException) {
			LOGGER.error("ParseException for PlanEffectiveDate: ", parseException);
		}
		finally {
			LOGGER.info("getApplicableYear() End");
		}
		return applicableYear;
	}

	/**
	 * This method checks if rates are available for all incoming Plans.
	 * @author Geetha Chandran
	 * @param persistOnExchangePlans 
	 * @param persistOffExchangePlans 
	 * @since 16 July, 2013
	 * @return boolean
	 */
	public boolean validateRateExistForPlans(PlanAndBenefitsPackageVO planAndBenefitsPackageVO,  Map<String, List<PlanRate>> planRates, boolean persistOffExchangePlans, boolean persistOnExchangePlans, StringBuffer response, int applicableYear) {
		LOGGER.info("validateRateExistForPlans begins");
		StringBuilder missingRates = new StringBuilder();
		StringBuilder ignoredPlans = new StringBuilder();
		boolean isValid = true;
	    Calendar cal = Calendar.getInstance();

		if(!CollectionUtils.isEmpty(planRates)){
			if(null != planAndBenefitsPackageVO.getPlansList()
					&& null != planAndBenefitsPackageVO.getPlansList().getPlans()) {
				List<PlanAndBenefitsVO> planAndBenefitsVO = planAndBenefitsPackageVO.getPlansList().getPlans();
				for(PlanAndBenefitsVO planBenefitVO : planAndBenefitsVO){
					if(null != planBenefitVO.getPlanAttributes() && null != planBenefitVO.getPlanAttributes().getStandardComponentID()){
						String planId = planBenefitVO.getPlanAttributes().getStandardComponentID().getCellValue();
						boolean validateRate = isPlanEligibleForLoad(planBenefitVO, persistOffExchangePlans, persistOnExchangePlans);
						
						if(!validateRate) { 
							ignoredPlans.append(planId);
							ignoredPlans.append(",");
						} else if(null == planRates.get(planId)){
							missingRates.append(planId);
							missingRates.append(",");
						} else {
							//validating applicable year is same in plan and rate template
							int rateYear =0;
							List <PlanRate> planRateList =  planRates.get(planId);
							if(!CollectionUtils.isEmpty(planRateList)){
								
								for(PlanRate planRate : planRateList){
									Date date = planRate.getEffectiveStartDate();
								    cal.setTime(date);
								    rateYear = cal.get(Calendar.YEAR);
									
								    if(applicableYear!=rateYear){
								    	 LOGGER.error("Applicable year for Plan and Plan Rates are different.");
								    	 response.append("Applicable year for Plan and Plan Rates are different . ");
								    	 isValid = false;
								    }
								}
							}
						}
					}
				}
			}
			if(StringUtils.isNotEmpty(ignoredPlans)){ 
				response.append("Following plans are not validated -" + ignoredPlans + ". ");
				LOGGER.warn("Following plans are not valid -" + ignoredPlans);
			}
			if(StringUtils.isNotEmpty(missingRates)){
				LOGGER.error("Rates are not available for some Plans");
				isValid = false;
				//throw new GIException("No Rates found for Plan ID(s) "+ missingRates);
				response.append("No Rates found for Plan ID(s) "+ StringUtils.removeEnd(missingRates.toString(), ",") + ". ");
			}
		}else{
			LOGGER.debug("Plan rates are empty thus returning false");
			isValid = false;
		}
		LOGGER.info("validateRateExistForPlans ends");
		return isValid;
	}

	private boolean isPlanEligibleForLoad(PlanAndBenefitsVO planBenefitVO, boolean persistOffExchangePlans, boolean persistOnExchangePlans) {
		int costVarianceCount = planBenefitVO.getCostShareVariancesList().getCostShareVariance().size();
		boolean eligibleForLoad = false;
		if(persistOffExchangePlans && persistOnExchangePlans) {
			eligibleForLoad = true;
		} else {
			if(costVarianceCount == SINGLE_COUNT) {
				//There is only one cost variance
				String costVariancePlanId = planBenefitVO.getCostShareVariancesList().getCostShareVariance().get(0).getPlanId().getCellValue();
				if(costVariancePlanId.endsWith(SerffConstants.OFF_EXCHANGE_SUFFIX)) {
					//costVariance is off-exchange, plan eligible for load if persistOffExchangePlans is true
					eligibleForLoad = persistOffExchangePlans;
				} else {
					//costVariance is on-exchange, plan eligible for load if persistOnExchangePlans is true
					eligibleForLoad = persistOnExchangePlans;
				}
			} else {
				//There are more than one cost variance, which must have ON exchange and may have OFF exchange
				if(persistOnExchangePlans) {
					eligibleForLoad = true;
				} else {
					String costVariancePlanId; 
					for(CostShareVarianceVO costShareVariance : planBenefitVO.getCostShareVariancesList().getCostShareVariance()) {
						costVariancePlanId = costShareVariance.getPlanId().getCellValue();
						if(costVariancePlanId.endsWith(SerffConstants.OFF_EXCHANGE_SUFFIX)) {
							//costVariance is off-exchange, plan eligible for load if persistOffExchangePlans is true
							eligibleForLoad = true;
							break;
						} 
					}
				}
			}
		}
		return eligibleForLoad;
	}
	
	private String createXMLNode(String nodeName, String firstName, String lastName){
		
		StringBuffer nodeData = new StringBuffer();
		nodeData.append("<"+nodeName+">\n")
		.append(FIRST_NAME_NODE_START+firstName+FIRST_NAME_NODE_END)
		.append(LAST_NAME_NODE_START+lastName+LAST_NAME_NODE_END)
		.append(EMAILADDRESS_NODE_START+ EMAILADDRESS_NODE_END)
		.append(PHONE_NUMBER_NODE_START+ PHONE_NUMBER_NODE_END)
		.append(PHONE_EXTENSION_NODE_START+ PHONE_EXTENSION_NODE_END)
		.append("</"+nodeName+">\n");
		
		return nodeData.toString();
		
	}
	
	/**
	 * This method checks if url starts with http or https id not then append http://.
	 * @author Vani Sharma
	 * @param excelCellVO
	 * @return String
	 */
	private String checkAndcorrectURL(ExcelCellVO excelCellVO) {
			String rval= null;
			if (null != excelCellVO
					&& StringUtils.isNotEmpty(excelCellVO.getCellValue())) {
				rval = SerffUtils.checkAndcorrectURL(excelCellVO.getCellValue());
			}
			return rval;
	}
	
	private Plan.AVAILABLE_FOR getAvailableFor(String childOnlyOffering) {

		Plan.AVAILABLE_FOR availableFor = Plan.AVAILABLE_FOR.ADULTANDCHILD;

		if (ALLOWS_ADULT_ONLY.equalsIgnoreCase(childOnlyOffering)) {
			availableFor = Plan.AVAILABLE_FOR.ADULTONLY;
		}
		else if (ALLOWS_CHILD_ONLY.equalsIgnoreCase(childOnlyOffering)) {
			availableFor = Plan.AVAILABLE_FOR.CHILDONLY;
		}
		return availableFor;
	}
	
	private String getPlanInsuranceType(PlanAndBenefitsPackageVO planAndBenefitsPackageVO) {
		
		String planType = null;
		
		if (SerffConstants.YES.equalsIgnoreCase(planAndBenefitsPackageVO.getHeader().getDentalPlanOnlyInd().getCellValue())) {
			planType = Plan.PlanInsuranceType.DENTAL.toString();
		}
		else {
			planType = Plan.PlanInsuranceType.HEALTH.toString();
		}
		return planType;
	}
	
	/**
	 * This method gets the last modified date for all templates.
	 * @author Geetha Chandran
	 * @since 12 Aug, 2013
	 * @return Map containing last modified date for all templates
	 */
	private Map<String, Date> getTemplatesLastModifedDate(TransferPlan req){
		 Map<String, Date> templatesLastModifiedDates = new HashMap<String, Date>();
		if(null != req && null != req.getPlan() && null != req.getPlan().getDataTemplates()){
			List<TransferDataTemplate> dataTemplates = req.getPlan().getDataTemplates().getDataTemplate();
			if(!CollectionUtils.isEmpty(dataTemplates)){
				for(TransferDataTemplate dataTemplate : dataTemplates){
					if(null != dataTemplate.getInsurerAttachment() && null != dataTemplate.getInsurerAttachment().getDateLastModified()){
						Date lmdDate = dataTemplate.getInsurerAttachment().getDateLastModified().toGregorianCalendar(TimeZone.getTimeZone("UTC"), null, null).getTime();
						templatesLastModifiedDates.put(dataTemplate.getDataTemplateType(), lmdDate);
					}
				}
			}
		}
		return templatesLastModifiedDates;
	}

	
    /**
     * Returns list of templates that are not modified.
     * 
     * @param lastUpdatedTemplateMap
     * @return
     */
	private void getNonModifiedTemplateList(Map<String ,Date> lastUpdatedTemplateMap, Map<String, Object> reqTemplates, StringBuffer responseMessage) {
		
		boolean isFirstTemplate = false;
		
		if(null != lastUpdatedTemplateMap && !lastUpdatedTemplateMap.isEmpty()) {
			String[] templateNames = {SerffConstants.DATA_BENEFITS, SerffConstants.DATA_RATING_TABLE, SerffConstants.DATA_ADMIN_DATA, SerffConstants.DATA_SERVICE_AREA , SerffConstants.DATA_NETWORK_ID , SerffConstants.DATA_PRESCRIPTION_DRUG };
			for (String template : templateNames) {
				if(reqTemplates.get(template) != null && (!lastUpdatedTemplateMap.containsKey(template) || lastUpdatedTemplateMap.get(template) != null)) {
					//Template is not modified. Add to list.
					if(!isFirstTemplate){
						responseMessage.append(SerffConstants.TEMPLATES_NOT_MODIFIED_MESSAGE);
						isFirstTemplate = true;
					}else{
						responseMessage.append(", ");
					}
					
					responseMessage.append(template);
				}
			}
		}
		
	}
	
	
	
	private String[] getAmountValues(String amountValueString){
		
		String[] amountValueArr = null;
		
		if(amountValueString.contains(AMOUNT_STRING_SEPERATOR)){
		
			amountValueArr = amountValueString.trim().split("["+AMOUNT_STRING_SEPERATOR+"]");
			
			if(amountValueArr.length == 2){
				
				String perPersonValue = StringUtils.EMPTY;
				String perGroupValue = StringUtils.EMPTY;
				
				for(int index = 0 ; index < 2 ; index ++){
					//Check string at index position in array.
					String value = amountValueArr[index];
					
					if(StringUtils.isNotEmpty(value) && !value.contains(AMOUNT_NOT_APPLICABLE_TEXT)){
						
						if(value.contains(PER_PERSON_STRING)){
							
							perPersonValue = value.replaceAll(PER_PERSON_STRING, StringUtils.EMPTY)
									.replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY)
									.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)
									.trim();
							
						}else if(value.contains(PER_GROUP_STRING)){
							
							perGroupValue = value.replaceAll(PER_GROUP_STRING, StringUtils.EMPTY)
									.replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY)
									.replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)
									.trim();
							
						}else{
							LOGGER.error("getAmountValues() : Invalid amount value : " + SecurityUtil.sanitizeForLogging(value));
						}
					}
					
				}
				
				LOGGER.debug("getAmountValues() : perPersonValue : " + perPersonValue);
				LOGGER.debug("getAmountValues() : perGroupValue : " + perGroupValue);
				
				amountValueArr[0] = perGroupValue;//family
				amountValueArr[1] = perPersonValue;//per person
				
			}else{
				LOGGER.error("Invalid amount array created for string " + amountValueString);
				amountValueArr = null;
			}
			
		}else{
			LOGGER.error("No seperator \"|\" found in amountString : " + amountValueString);
		}
		
		return amountValueArr;
	}
	
	
	private void softdeleteExistingFormularyLinkedCostShareAndDrugRecord(Formulary formulary, EntityManager entityManager){
		
		LOGGER.info("softdeleteExistingFormularyLinkedCostShareAndDrugRecord() : Start");
		
		List<FormularyCostShareBenefit> formularyCostBenefitList = null;
		List<CostShare> costShareList = null;
		
		if (null != formulary) {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("softdeleteExistingFormularyLinkedCostShareAndDrugRecord() : formulary ID : " + formulary.getFormularyId() + " : " +  formulary.getId() );
			}
			//Soft delete all FormualryCostShareBenefit for the formularyID
			formularyCostBenefitList = iformualryCostShareBrnefit.findByFormularyAndIsDeleted(formulary, FormularyCostShareBenefit.IS_DELETED.N.toString());
			if (!CollectionUtils.isEmpty(formularyCostBenefitList)) {
				for (FormularyCostShareBenefit formularyCostBenefit : formularyCostBenefitList) {
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("softdeleteExistingFormularyLinkedCostShareAndDrugRecord() : Marking soft deleted formularycostsharebenefit with id " + formularyCostBenefit.getId());
					}
					formularyCostBenefit.setIsDeleted(FormularyCostShareBenefit.IS_DELETED.Y.toString());
					entityManager.merge(formularyCostBenefit);
				}
			}
			//soft delete all cost_share for the formularyID
			costShareList = iCostShare.findByFormularyAndIsDeleted(formulary, SerffConstants.NO_ABBR);
			if (!CollectionUtils.isEmpty(costShareList)) {
				LOGGER.info("softdeleteExistingFormularyLinkedCostShareAndDrugRecord() : costShareList size : " + costShareList.size() );
				for (CostShare costShare : costShareList) {
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("softdeleteExistingFormularyLinkedCostShareAndDrugRecord() : Marking soft deleted CostShare with id " + costShare.getId());
					}
					costShare.setIsDeleted(SerffConstants.YES_ABBR);
					entityManager.merge(costShare);
				}
			}
			//soft delete drug list for the issuer
			DrugList drugList = formulary.getDrugList();
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("softdeleteExistingFormularyLinkedCostShareAndDrugRecord() : Marking soft deleted drugList with id " + drugList.getId());
			}
			drugList.setIsDeleted(SerffConstants.YES_ABBR);
			entityManager.merge(drugList);
		}else{
			LOGGER.debug("softdeleteExistingFormularyLinkedCostShareAndDrugRecord() : formulary is null");
		}
		LOGGER.info("softdeleteExistingFormularyLinkedCostShareAndDrugRecord() : End");
	}
	
}
