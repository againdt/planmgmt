package com.serff.service.templates;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerServiceArea;
import com.getinsured.hix.model.IssuerServiceAreaExt;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.IssuerVerificationStatus;
import com.getinsured.hix.model.PlanMedicaid;
import com.getinsured.hix.model.PlanMedicaidBenefits;
import com.getinsured.hix.model.PlanMedicaidServices;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration.PlanmgmtConfigurationEnum;
import com.getinsured.hix.platform.repository.IZipCodeRepository;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.medicaid.MedicaidOtherServicesVO;
import com.getinsured.serffadapter.medicaid.MedicaidPlanBenefitsVO;
import com.getinsured.serffadapter.medicaid.MedicaidPlanInfoVO;
import com.getinsured.serffadapter.medicaid.MedicaidServiceAreaExtVO;
import com.getinsured.serffadapter.medicaid.MedicaidServiceAreaVO;
import com.serff.repository.templates.IIssuerServiceAreaRepository;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.repository.templates.IServiceAreaRepository;
import com.serff.service.validation.MedicaidPlanDataValidator;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * Class is used to persist Medicaid Plans to the database.
 * 
 * @author Bhavin Parmar
 * @since January 15, 2015
 */
@Service("medicaidPlanMgmtSerffService")
public class MedicaidPlanMgmtSerffServiceImpl implements MedicaidPlanMgmtSerffService {

	private static final Logger LOGGER = Logger.getLogger(MedicaidPlanMgmtSerffServiceImpl.class);
	private static final String EMSG_SKIPPING_MEDICAID = "Skipping Medicaid Plan[";
	private static final String EMSG_UNMATCHED_BENEFIT = "Skipping Medicaid Plan Benefit: No mapping found with config data: ";
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
	private static final int MIN_AGE_0 = 0;
	private static final int MIN_AGE_19 = 19;
	private static final int MIN_AGE_21 = 21;
	
	private static final int MAX_AGE_18 = 18;
	private static final int MAX_AGE_20 = 20;
	private static final int MAX_AGE_150 = 150;

	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private IIssuersRepository iIssuerRepository;
	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private IZipCodeRepository iZipCodeRepository;
	@Autowired private MedicaidPlanDataValidator medicaidValidator;
	@Autowired private IIssuerServiceAreaRepository iIssuerServiceAreaRepository;
	@Autowired private IServiceAreaRepository iServiceAreaRepository;
	@Autowired private SerffUtils serffUtils;

	@Override
	public boolean populateAndPersistMedicaidData(MedicaidPlanInfoVO planInfoVO, SerffPlanMgmt trackingRecord) throws GIException {

		LOGGER.debug("populateAndPersistMedicaidData() Start");
		boolean savedStatus = false;
		EntityManager entityManager = null;

		try {
			if (null == planInfoVO || null == trackingRecord) {
				return savedStatus;
			}

			if (medicaidValidator.validateMedicaidPlanData(planInfoVO)) {
				entityManager = entityManagerFactory.createEntityManager(); // Get DB connection using Entity Manager Factory
				entityManager.getTransaction().begin(); // Begin Transaction
				savedStatus = persistMedicaidData(planInfoVO, entityManager); // Persist Medicaid Plan
			}
		}
		catch(Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
			throw new GIException(ex);
		}
		finally {
			savedStatus = getTransactionCommit(savedStatus, entityManager);
			updateTrackingRecord(savedStatus, planInfoVO, trackingRecord);

			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			LOGGER.info("populateAndPersistMedicaidData() End");
		}
		return savedStatus;
	}

	/**
	 *  Method is used to persist data in Database.
	 */
	private boolean persistMedicaidData(MedicaidPlanInfoVO planInfoVO, EntityManager entityManager) {

		LOGGER.info("persistMedicaidData() Start");
		boolean savedStatus = false;

		try {

			Issuer existingIssuer = getExistingIssuer(planInfoVO);
			/*String issuerPlanNumber = planInfoVO.getIssuerHiosId() + planInfoVO.getPlanState() + planInfoVO.getPlanNumber()
					+ SerffConstants.OFF_EXCHANGE_VARIANT_SUFFIX;*/
			String issuerPlanNumber = planInfoVO.getPlanNumber();
			int applicableYear = 0; // Set Default Applicable Year

			if (null == existingIssuer
					|| !processExistingMedicaidPlan(planInfoVO, issuerPlanNumber, applicableYear, entityManager)) {
				return savedStatus;
			}

			Plan savedPlan = savePlan(applicableYear, issuerPlanNumber, existingIssuer, planInfoVO, entityManager);
			if (null != savedPlan) {
				savedStatus = savePlanMedicaidData(savedPlan, planInfoVO, applicableYear, entityManager);
			}
		}
		finally {
			LOGGER.info("persistMedicaidData() End");
		}
		return savedStatus;
	}

	/**
	 * Method is used to save save Plan Medicaid, Medicaid Services and Service Area objects.
	 */
	private boolean savePlanMedicaidData(Plan savedPlan, MedicaidPlanInfoVO planInfoVO, int applicableYear, EntityManager entityManager) {

		LOGGER.info("savePlanMedicaidData() Start");
		boolean savedStatus = true;

		try {
			PlanMedicaid savedPlanMedicaid = savePlanMedicaid(savedPlan, planInfoVO, entityManager);
			if (null == savedPlanMedicaid) {
				savedStatus = false;
				planInfoVO.setNotValid(Boolean.TRUE);
				planInfoVO.setErrorMessages(EMSG_SKIPPING_MEDICAID + savedPlan.getIssuerPlanNumber() +"] because PlanMedicaid table is not saved.");
				return savedStatus;
			}

			savedStatus = saveMedicaidBenefitsList(savedPlan, savedPlanMedicaid, planInfoVO, entityManager);
			savedStatus = savedStatus && saveMedicaidServices(planInfoVO, savedPlanMedicaid, entityManager);

			if (savedStatus) {
				IssuerServiceArea issuerServiceArea = saveServiceArea(savedPlan, planInfoVO.getServiceAreaVO(), applicableYear, entityManager);
				LOGGER.info("Saved IssuerServiceArea is null: " + (null == issuerServiceArea));
				if (null == issuerServiceArea) {
					savedStatus = false;
					planInfoVO.setNotValid(Boolean.TRUE);
					planInfoVO.setErrorMessages(EMSG_SKIPPING_MEDICAID + savedPlan.getIssuerPlanNumber() +"] because Service Area tables are not saved.");
				}
			}
		}
		finally {
			LOGGER.info("savePlanMedicaidData() End - savedStatus: " + savedStatus);
		}
		return savedStatus;
	}

	/**
	 * Method is used to save List of Medicaid Benefits data.
	 */
	private boolean saveMedicaidBenefitsList(Plan savedPlan, PlanMedicaid savedPlanMedicaid, MedicaidPlanInfoVO planInfoVO,
			EntityManager entityManager) {

		LOGGER.info("saveMedicaidBenefitsList() Start");
		boolean savedStatus = true;

		try {
			Map<String,String> medicaidBenefitMap = serffUtils.getPropertyList(PlanmgmtConfigurationEnum.MEDICAID_LIST);
			if (CollectionUtils.isEmpty(medicaidBenefitMap)) {
				savedStatus = false;
				planInfoVO.setNotValid(Boolean.TRUE);
				planInfoVO.setErrorMessages("Medicaid properties of Plan Managment configuration list Map is empty or null.");
				return savedStatus;
			}

			if (!CollectionUtils.isEmpty(planInfoVO.getPlanBenefitsList())) {
				List<PlanMedicaidBenefits> savedBenefitsList = new ArrayList<PlanMedicaidBenefits>();

				for (MedicaidPlanBenefitsVO planBenefitsVO : planInfoVO.getPlanBenefitsList()) {
					saveMedicaidBenefits(medicaidBenefitMap, planInfoVO, savedBenefitsList, savedPlanMedicaid, planBenefitsVO, entityManager);
				}

				if (!CollectionUtils.isEmpty(savedBenefitsList)) {
					LOGGER.info("Number of saved Benefits: " + savedBenefitsList.size());
				}
				else {
					savedStatus = false;
					planInfoVO.setNotValid(Boolean.TRUE);
					planInfoVO.setErrorMessages(EMSG_SKIPPING_MEDICAID + savedPlan.getIssuerPlanNumber() +"] because Medicaid Benefit table is not saved.");
				}
			}
			else {
				savedStatus = false;
			}
		}
		finally {
			LOGGER.info("saveMedicaidBenefitsList() End - savedStatus: " + savedStatus);
		}
		return savedStatus;
	}

	/**
	 * Method is used to get Existing Issuer.
	 */
	private Issuer getExistingIssuer(MedicaidPlanInfoVO planInfoVO) {

		Issuer existingIssuer = iIssuerRepository.getIssuerByHiosID(planInfoVO.getIssuerHiosId());
		if(null == existingIssuer) {
			planInfoVO.setNotValid(Boolean.TRUE);
			planInfoVO.setErrorMessages("Skipping Issuer["+ planInfoVO.getIssuerHiosId() +"] account because it does not exist.");
		}
		return existingIssuer;
	}

	/**
	 * Method is used to process existing Medicaid Plans before persistence.
	 * Rule: Certified Medicaid plan will not update.
	 */
	private boolean processExistingMedicaidPlan(MedicaidPlanInfoVO planInfoVO, String issuerPlanNumber, int applicableYear, EntityManager entityManager) {

		LOGGER.debug("processExistingMedicaidPlan() Start");
		boolean processMedicaidPlan = Boolean.TRUE;

		try {
			List<Plan> existingPlanList = iPlanRepository.findByIssuerPlanNumberAndIsDeletedAndApplicableYearAndInsuranceType(
					issuerPlanNumber, Plan.IS_DELETED.N.toString(), applicableYear, Plan.PlanInsuranceType.MEDICAID.name());

			if (CollectionUtils.isEmpty(existingPlanList)) {
				return processMedicaidPlan;
			}

			Plan existingPlan = existingPlanList.get(0);
			LOGGER.debug("Medicaid Plan exists for given plan number " + issuerPlanNumber);

			if (null != existingPlan) {
				// Skipping CERTIFIED plans logic.
				if (Plan.PlanStatus.CERTIFIED.toString().equalsIgnoreCase(existingPlan.getStatus())) {
					planInfoVO.setNotValid(Boolean.TRUE);
					planInfoVO.setErrorMessages("Skipping Certified Medicaid Plan["+ issuerPlanNumber +"].");
					processMedicaidPlan = Boolean.FALSE;
				}
				else {
					deleteExistingPlan(existingPlan, entityManager);
				}
			}
		}
		finally {
			LOGGER.debug("processExistingMedicaidPlan() End");
		}
		return processMedicaidPlan;
	}

	/**
	 * Method is used to soft delete existing Medicaid Plans [Non-Certified].
	 */
	private void deleteExistingPlan(Plan existingPlan, EntityManager entityManager) {

		LOGGER.debug("deleteExistingPlan() Start");

		try {
			// If any plan does not CERTIFIED then mark plans as soft delete.
			existingPlan.setIsDeleted(Plan.IS_DELETED.Y.toString());
			mergeEntityManager(entityManager, existingPlan);
			LOGGER.info("Plan["+ existingPlan.getIssuerPlanNumber() +"] bean has been soft deleted Successfully for State["+ existingPlan.getState() +"].");
		}
		finally {
			LOGGER.debug("deleteExistingPlan() End");
		}
	}

	/**
	 * Method is used to save Plan table.
	 */
	private Plan savePlan(int applicableYear, String issuerPlanNumber, Issuer existingIssuer, MedicaidPlanInfoVO planInfoVO,
			EntityManager entityManager) {

		LOGGER.debug("Persisting Plan Object Start : " + issuerPlanNumber);
		Plan plan = null;

		try {
			if (null == existingIssuer) {
				return plan;
			}
			plan = new Plan();
			plan.setIssuerVerificationStatus(IssuerVerificationStatus.PENDING.toString());
			plan.setIssuer(existingIssuer);
			plan.setInsuranceType(Plan.PlanInsuranceType.MEDICAID.name());
			plan.setStatus(Plan.PlanStatus.LOADED.toString());
			// Appending Off Exchange Variant in Issuer Plan Number
			plan.setIssuerPlanNumber(issuerPlanNumber);
			plan.setName(planInfoVO.getPlanName());
			plan.setStartDate(dateFormat.parse(planInfoVO.getPlanStartDate()));
			plan.setEndDate(dateFormat.parse(planInfoVO.getPlanEndDate()));
			plan.setHsa(SerffConstants.NO);
			plan.setState(planInfoVO.getPlanState());
			plan.setMarket(Plan.PlanMarket.INDIVIDUAL.toString());

			if (SerffConstants.LEN_HIOS_PRODUCT_ID < issuerPlanNumber.length()) {
				plan.setHiosProductId(issuerPlanNumber.substring(0, SerffConstants.LEN_HIOS_PRODUCT_ID));
			}
			else {
				plan.setHiosProductId(issuerPlanNumber);
			}
			plan.setExchangeType(Plan.EXCHANGE_TYPE.OFF.toString());
			plan.setIsPUF(Plan.IS_PUF.N);
			plan.setIsDeleted(Plan.IS_DELETED.N.toString());
			plan.setApplicableYear(applicableYear);
			plan = (Plan) mergeEntityManager(entityManager, plan);
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting Plan object", ex);
		}
		finally {
			LOGGER.debug("Persisting Plan Object Done");
		}
		return plan;
	}

	/**
	 * Method is used to save PlanMedicaid table.
	 */
	private PlanMedicaid savePlanMedicaid(Plan savedPlan, MedicaidPlanInfoVO planInfoVO,
			EntityManager entityManager) {

		LOGGER.debug("Persisting PlanMedicaid Object Start");
		PlanMedicaid planMedicaid = null;
		
		try {
			if (null == savedPlan) {
				return planMedicaid;
			}
			planMedicaid = new PlanMedicaid();
			planMedicaid.setPlan(savedPlan);
			planMedicaid.setCreatedBy(SerffConstants.DEFAULT_USER_ID);
			planMedicaid.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);
			planMedicaid.setMemberNumber(planInfoVO.getMemberNumber());
			planMedicaid.setQualityRating(planInfoVO.getQualityRating());
			planMedicaid.setWebsiteURL(SerffUtils.checkAndcorrectURL(planInfoVO.getWebsiteUrl()));
			planMedicaid.setCoverageArea(planInfoVO.getCountiesServed());
			planMedicaid = (PlanMedicaid) mergeEntityManager(entityManager, planMedicaid);
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting Plan Medicaid object", ex);
		}
		finally {
			LOGGER.debug("Persisting PlanMedicaid Object Done");
		}
		return planMedicaid;
	}

	/**
	 * Method is used to save PlanMedicaidBenefits table.
	 */
	private void saveMedicaidBenefits(Map<String,String> medicaidBenefitMap, MedicaidPlanInfoVO planInfoVO,
			List<PlanMedicaidBenefits> savedBenefitsList, PlanMedicaid savedPlanMedicaid,
			MedicaidPlanBenefitsVO planBenefitsVO, EntityManager entityManager) {

		LOGGER.debug("Persisting PlanMedicaidBenefits Object Start");

		try {
			if (null == savedPlanMedicaid) {
				return;
			}

			String benefitKey = getMedicaidKey(medicaidBenefitMap, planInfoVO, planBenefitsVO.getBenefits());
			if (StringUtils.isBlank(benefitKey)) {
				return;
			}

			int minAge = 0;
			int maxAge = 0;
			PlanMedicaidBenefits medicaidBenefits = null;

			if (StringUtils.isNotBlank(planBenefitsVO.getChildren())) {
				minAge = MIN_AGE_0;
				maxAge = MAX_AGE_18;
				medicaidBenefits = saveMedicaidBenefitsObject(planBenefitsVO.getChildren(), benefitKey, minAge, maxAge, savedPlanMedicaid, entityManager);
				savedBenefitsList.add(medicaidBenefits);
			}
			if (StringUtils.isNotBlank(planBenefitsVO.getChildrenAgeNinteenToTwenty())) {
				minAge = MIN_AGE_19;
				maxAge = MAX_AGE_20;
				medicaidBenefits = saveMedicaidBenefitsObject(planBenefitsVO.getChildrenAgeNinteenToTwenty(), benefitKey, minAge, maxAge, savedPlanMedicaid, entityManager);
				savedBenefitsList.add(medicaidBenefits);
			}
			if (StringUtils.isNotBlank(planBenefitsVO.getAdults())) {
				minAge = MIN_AGE_21;
				maxAge = MAX_AGE_150;
				medicaidBenefits = saveMedicaidBenefitsObject(planBenefitsVO.getAdults(), benefitKey, minAge, maxAge, savedPlanMedicaid, entityManager);
				savedBenefitsList.add(medicaidBenefits);
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting Medicaid Benefits object", ex);
		}
		finally {
			LOGGER.debug("Persisting PlanMedicaidBenefits Object Done");
		}
	}

	/**
	 * Method is used to get Medicaid Benefit Key from GI-CONFIG Table.
	 */
	private String getMedicaidKey(Map<String,String> medicaidBenefitMap, MedicaidPlanInfoVO planInfoVO, String benefitName) {

		String benefitKey = null;
		if (CollectionUtils.isEmpty(medicaidBenefitMap)) {
			return benefitKey;
		}
		benefitKey = medicaidBenefitMap.get(benefitName.toLowerCase());

		if (StringUtils.isBlank(benefitKey)) {
			planInfoVO.setNotValid(Boolean.TRUE);

			if (StringUtils.isBlank(planInfoVO.getErrorMessages())) {
				planInfoVO.setErrorMessages(EMSG_UNMATCHED_BENEFIT + benefitName);
			}
			else {
				planInfoVO.setErrorMessages(planInfoVO.getErrorMessages() + SerffConstants.NEW_LINE + EMSG_UNMATCHED_BENEFIT + benefitName);
			}
			LOGGER.warn(planInfoVO.getErrorMessages());
		}
		return benefitKey;
	}

	/**
	 * Method is used to save PlanMedicaidBenefits table.
	 */
	private PlanMedicaidBenefits saveMedicaidBenefitsObject(String benefitAttribute, String benefitName,
			int minAge, int maxAge, PlanMedicaid savedPlanMedicaid, EntityManager entityManager) {
		
		PlanMedicaidBenefits medicaidBenefits = null;

		try {
			if (null == savedPlanMedicaid) {
				return medicaidBenefits;
			}
			medicaidBenefits = new PlanMedicaidBenefits();
			medicaidBenefits.setBenefitAttribute(benefitAttribute);
			medicaidBenefits.setBenefitName(benefitName);
			medicaidBenefits.setCreatedBy(SerffConstants.DEFAULT_USER_ID);
			medicaidBenefits.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);
			medicaidBenefits.setMinAge(minAge);
			medicaidBenefits.setMaxAge(maxAge);
			medicaidBenefits.setPlanMedicaid(savedPlanMedicaid);
			medicaidBenefits = (PlanMedicaidBenefits) mergeEntityManager(entityManager, medicaidBenefits);
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting service Area object", ex);
		}
		return medicaidBenefits;
	}

	/**
	 * Method is used to save PlanMedicaidServices table.
	 */
	private boolean saveMedicaidServices(MedicaidPlanInfoVO planInfoVO, PlanMedicaid savedPlanMedicaid, EntityManager entityManager) {

		LOGGER.debug("Persisting PlanMedicaidServices Object Start");
		boolean savedStatus = false;
		PlanMedicaidServices medicaidServices = null;

		try {

			if (CollectionUtils.isEmpty(planInfoVO.getOtherServicesList())) {
				return savedStatus;
			}

			for (MedicaidOtherServicesVO otherServicesVO : planInfoVO.getOtherServicesList()) {
				if (null == otherServicesVO) {
					continue;
				}
				medicaidServices = new PlanMedicaidServices();
				medicaidServices.setServiceType(otherServicesVO.getServiceType());
				medicaidServices.setServiceName(otherServicesVO.getServiceName());
				medicaidServices.setServiceAttribut(otherServicesVO.getServiceAttributes());
				medicaidServices.setPlanMedicaid(savedPlanMedicaid);
				medicaidServices.setCreatedBy(SerffConstants.DEFAULT_USER_ID);
				medicaidServices.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);
				medicaidServices = (PlanMedicaidServices) mergeEntityManager(entityManager, medicaidServices);
			}
			savedStatus = true;
		}
		catch (Exception ex) {

			if (!savedStatus) {
				planInfoVO.setNotValid(Boolean.TRUE);
				planInfoVO.setErrorMessages(EMSG_SKIPPING_MEDICAID + planInfoVO.getPlanNumber() +"] because Medicaid Service table is not saved.");
			}
			LOGGER.error("Error occured while persisting Plan Medicaid Services object", ex);
		}
		finally {
			LOGGER.debug("Persisting PlanMedicaidServices Object Done");
		}
		return savedStatus;
	}

	/**
	 * Method is used to save ServiceArea table.
	 */
	private IssuerServiceArea saveServiceArea(Plan savedPlan, MedicaidServiceAreaVO serviceAreaVO, int applicableYear, EntityManager entityManager) {

		LOGGER.info("Persisting ServiceArea Object Start");
		IssuerServiceArea issuerServiceArea = null;
		List<IssuerServiceArea> issuerServiceAreas = null;

		try {
			if (null == savedPlan || null == serviceAreaVO) {
				return issuerServiceArea;
			}
			LOGGER.debug("Checking if PM_ISSUER_SERVICE_AREA already have data for provided state,issuerHiosID and ServiceAreadId Issuer: " );
			issuerServiceAreas = iIssuerServiceAreaRepository.findbyStateIssuerAndServAreaAndApplicableYear(savedPlan.getState(), savedPlan.getIssuer().getHiosIssuerId(), serviceAreaVO.getServiceAreaId(), 0);

			if (!CollectionUtils.isEmpty(issuerServiceAreas) && null != issuerServiceAreas.get(0)) {
				issuerServiceArea = issuerServiceAreas.get(0);
				LOGGER.info("Retrieved Existing IssuerServiceArea object.");
				softDeleteExistingPmServiceare(issuerServiceArea, entityManager);
			}
			else {
				issuerServiceArea = createNewIssuerServiceArea(serviceAreaVO, savedPlan, applicableYear);
				LOGGER.info("IssuerServiceArea object is created.");
			}

			List<IssuerServiceAreaExt> issuerServiceAreaExtList = null;
			// Process if entire state is yes
			if (SerffConstants.YES.equalsIgnoreCase(serviceAreaVO.getEntireState())) {
				LOGGER.info("Process entire state to get all counties from state: " + savedPlan.getState());
				issuerServiceAreaExtList = processEntireState(savedPlan, issuerServiceArea);
			}
			// Process if partial state is No
			else {
				LOGGER.info("Process partial state.");
				issuerServiceAreaExtList = processPartialState(serviceAreaVO.getServiceAreaExtList(), issuerServiceArea, savedPlan);
			}

			if (!CollectionUtils.isEmpty(issuerServiceAreaExtList)) {
				issuerServiceArea.setIssuerServiceAreaExt(issuerServiceAreaExtList);
				issuerServiceArea = (IssuerServiceArea) mergeEntityManager(entityManager, issuerServiceArea);
			}
			else {
				issuerServiceArea = null;
				LOGGER.error("IssuerServiceAreaExtList List is empty.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting service Area", ex);
		}
		finally {
			LOGGER.info("Persisting ServiceArea Object Done");
		}
		return issuerServiceArea;
	}

	/**
	 * This method process partial state condition.
	 */
	private List<IssuerServiceAreaExt> processPartialState(List<MedicaidServiceAreaExtVO> serviceAreaExtList,
			IssuerServiceArea issuerServiceArea, Plan savedPlan) {

		LOGGER.debug("processPartialState() Start");
		List<IssuerServiceAreaExt> issuerServiceAreaExtList = null;

		try {
			if (CollectionUtils.isEmpty(serviceAreaExtList)) {
				return issuerServiceAreaExtList;
			}
			issuerServiceAreaExtList = new ArrayList<IssuerServiceAreaExt>();
			List<ZipCode> zipCodes = null;
			IssuerServiceAreaExt issuerServiceAreaExt = null;
			List<String> zipCodelist = null;
			String zipCodesArray[] = null;
			String ziplist = null;

			for (MedicaidServiceAreaExtVO eachSvcAreaExt : serviceAreaExtList) {

				issuerServiceAreaExt = createServiseAreaExtObject(eachSvcAreaExt.getCountyName(), issuerServiceArea,
						eachSvcAreaExt.getPartialcounty(), eachSvcAreaExt.getZiplist());
				issuerServiceAreaExt.setServiceAreaId(issuerServiceArea);
				issuerServiceAreaExtList.add(issuerServiceAreaExt);

				if (SerffConstants.YES.equalsIgnoreCase(eachSvcAreaExt.getPartialcounty())
						&& StringUtils.isNotBlank(eachSvcAreaExt.getZiplist())) {
					ziplist = StringUtils.isNotBlank((eachSvcAreaExt.getZiplist())) ? eachSvcAreaExt.getZiplist().replaceAll(SerffConstants.SPACE, StringUtils.EMPTY) : StringUtils.EMPTY;
					zipCodesArray = ziplist.split(SerffConstants.COMMA);
					zipCodelist = Arrays.asList(zipCodesArray);
					LOGGER.debug("Partical county is Yes get all zipcodes for zip and state, ziplist: " + ziplist);
					zipCodes = iZipCodeRepository.getZipListByZipandState(zipCodelist, savedPlan.getState());
				}
				else {
					// fetch records from zipcodes on the basis of state and county.
					LOGGER.debug("Partical county is NO get all zipcodes for county and state");
					zipCodes = iZipCodeRepository.getZipListByCountyNameandState(eachSvcAreaExt.getCountyName(), savedPlan.getState());
				}
				List<ServiceArea> addedServiceAreas = addServiceArea(zipCodes, eachSvcAreaExt.getCountyName(), savedPlan, issuerServiceArea, issuerServiceAreaExt);

				if (!CollectionUtils.isEmpty(addedServiceAreas)) {
					issuerServiceAreaExt.setServiceArea(addedServiceAreas);
				}
				else {
					issuerServiceAreaExtList = null;
					break;
				}
				issuerServiceAreaExt.setServiceArea(addedServiceAreas);
			}
		}
		finally {
			LOGGER.debug("processPartialState() End - IssuerServiceAreaExtList List is empty: " + CollectionUtils.isEmpty(issuerServiceAreaExtList));
		}
		return issuerServiceAreaExtList;
	}

	/**
	 * This method process entire state condition.
	 */
	private List<IssuerServiceAreaExt> processEntireState(Plan savedPlan, IssuerServiceArea issuerServiceArea) {

		LOGGER.debug("processEntireState() Start");
		List<IssuerServiceAreaExt> issuerServiceAreaExtList = null;

		try {
			List<String> countiesList = iZipCodeRepository.findCountyNameByState(savedPlan.getState());
			if (CollectionUtils.isEmpty(countiesList)) {
				LOGGER.error("County List is empty.");
				return issuerServiceAreaExtList;
			}

			List<ZipCode> zipCodes = null;
			IssuerServiceAreaExt issuerServiceAreaExt = null;
			issuerServiceAreaExtList = new ArrayList<IssuerServiceAreaExt>();
			LOGGER.debug("Iterate over each counties ");

			for (String eachCounty : countiesList) {
				issuerServiceAreaExt = createServiseAreaExtObject(eachCounty, issuerServiceArea, SerffConstants.NO, null);
				issuerServiceAreaExtList.add(issuerServiceAreaExt);
				// Get all Zipcode for county
				zipCodes = iZipCodeRepository.getZipListByCountyNameandState(eachCounty, savedPlan.getState());
				List<ServiceArea> addedServiceAreas = addServiceArea(zipCodes, eachCounty, savedPlan, issuerServiceArea, issuerServiceAreaExt);

				if (!CollectionUtils.isEmpty(addedServiceAreas)) {
					issuerServiceAreaExt.setServiceArea(addedServiceAreas);
				}
				else {
					issuerServiceAreaExtList = null;
					break;
				}
			}
		}
		finally {
			LOGGER.debug("processEntireState() End - IssuerServiceAreaExtList List is empty: " + CollectionUtils.isEmpty(issuerServiceAreaExtList));
		}
		return issuerServiceAreaExtList;
	}

	/**
	 * Method is used to add new record in IssuerServiceArea POJO.
	 */
	private IssuerServiceArea createNewIssuerServiceArea(MedicaidServiceAreaVO serviceAreaVO, Plan savedPlan, int applicableYear) {

		LOGGER.debug("createNewIssuerServiceArea() Start");
		IssuerServiceArea issuerServiceArea = new IssuerServiceArea();

		if (null != serviceAreaVO) {
			issuerServiceArea.setSerffServiceAreaId(serviceAreaVO.getServiceAreaId());
			issuerServiceArea.setServiceAreaName(serviceAreaVO.getServiceAreaName());
			issuerServiceArea.setCompleteState(serviceAreaVO.getEntireState());
			issuerServiceArea.setIssuerState(savedPlan.getState());
			issuerServiceArea.setHiosIssuerId(savedPlan.getIssuer().getHiosIssuerId());
			issuerServiceArea.setApplicableYear(applicableYear);
		}
		LOGGER.debug("createNewIssuerServiceArea() End");
		return issuerServiceArea;
	}

	/**
	 * Method is used to add new records in ServiceArea POJO List.
	 */
	private List<ServiceArea> addServiceArea(List<ZipCode> zipCodes, String eachCounty, Plan savedPlan,
			IssuerServiceArea issuerServiceArea, IssuerServiceAreaExt issuerServiceAreaExt) {

		LOGGER.debug("addServiceArea() Start");

		List<ServiceArea> pmServiceAreas = null;
		if (CollectionUtils.isEmpty(zipCodes)) {
			LOGGER.error("ZipCode List is empty.");
			return pmServiceAreas;
		}

		ServiceArea pmServiceArea = null;
		pmServiceAreas = new ArrayList<ServiceArea>();

		for (ZipCode zip : zipCodes) {
			pmServiceArea = new ServiceArea();
			pmServiceArea.setCounty(eachCounty);
			pmServiceArea.setFips(zip.getCountyFips());
			pmServiceArea.setState(savedPlan.getState());
			pmServiceArea.setZip(zip.getZip());
			pmServiceArea.setServiceAreaId(issuerServiceArea);
			pmServiceArea.setIsDeleted(ServiceArea.IS_DELETED.N.name());
			pmServiceArea.setServiceAreaExtId(issuerServiceAreaExt);
			pmServiceAreas.add(pmServiceArea);
		}
		LOGGER.debug("addServiceArea() End - PMServiceAreas List is null: " + CollectionUtils.isEmpty(pmServiceAreas));
		return pmServiceAreas;
	}

	/**
	 * Method is used to add new record in IssuerServiceAreaExt POJO.
	 */
	private IssuerServiceAreaExt createServiseAreaExtObject(String county, IssuerServiceArea issuerServiceArea, String partialCounty, String ziplist) {

		LOGGER.debug("createServiseAreaExtObject() Start");
		IssuerServiceAreaExt issuerServiceAreaExt = new IssuerServiceAreaExt();
		issuerServiceAreaExt.setCounty(county);
		issuerServiceAreaExt.setPartialCounty(partialCounty);
		issuerServiceAreaExt.setIsDeleted(IssuerServiceAreaExt.IS_DELETED.N.name());
		issuerServiceAreaExt.setServiceAreaId(issuerServiceArea);
		issuerServiceAreaExt.setZipList(ziplist);
		LOGGER.debug("createServiseAreaExtObject() End");
		return issuerServiceAreaExt;
	}

	/**
	 * Method is used to soft delete existing records of IssuerServiceAreaExt and ServiceArea tables
	 */
	private void softDeleteExistingPmServiceare(IssuerServiceArea issuerServiceAreas, EntityManager entityManager) {

		LOGGER.debug("Marking soft deleting IssuerServiceAreaExt and ServiceArea tables Start.");
		List<IssuerServiceAreaExt> issuerServiceAreaExts = null;
		issuerServiceAreaExts = issuerServiceAreas.getIssuerServiceAreaExt();

		for (IssuerServiceAreaExt eachServiceAreaExts : issuerServiceAreaExts) {
			eachServiceAreaExts.setIsDeleted(IssuerServiceAreaExt.IS_DELETED.Y.name());
			eachServiceAreaExts = (IssuerServiceAreaExt) mergeEntityManager(entityManager, eachServiceAreaExts);

			List<ServiceArea> serviceAreas = iServiceAreaRepository.findByServiceAreaIdAndServiceAreaExtIdAndIsDeleted(
					issuerServiceAreas, eachServiceAreaExts, ServiceArea.IS_DELETED.N.toString());

			if (!CollectionUtils.isEmpty(serviceAreas)) {

				for (ServiceArea serviceArea : serviceAreas) {
					LOGGER.debug("Marking soft deleted ServiceArea with id " + serviceArea.getId());
					serviceArea.setIsDeleted(ServiceArea.IS_DELETED.Y.name());
					mergeEntityManager(entityManager, serviceArea);
				}
			}
		}
		LOGGER.debug("Marking soft deleted IssuerServiceAreaExt and ServiceArea tables End.");
	}

	/**
	 * Method is used to merge Table to Entity Manager.
	 */
	private Object mergeEntityManager(EntityManager entityManager, Object saveObject) {

		Object savedObject = null;

		if (null != saveObject && null != entityManager && entityManager.isOpen()) {
			savedObject = entityManager.merge(saveObject);
			LOGGER.debug(saveObject.getClass() + " bean has been Persisted Successfully.");
		}
		return savedObject;
	}

	/**
	 * Method is used to Commit/Rollback transaction depends on Status argument.
	 */
	private boolean getTransactionCommit(boolean status, EntityManager entityManager) throws GIException {

		LOGGER.debug("getTransactionCommit() Start");
		boolean commitStatus = status;

		try {

			if (null != entityManager && entityManager.isOpen()) {

				if (status) {
					entityManager.getTransaction().commit();
					LOGGER.info("Transaction Commit has been Commited");
				}
				else {
					entityManager.getTransaction().rollback();
					LOGGER.info("Transaction Commit has been Rollback");
				}
			}
		}
		catch (Exception ex) {
			commitStatus = Boolean.FALSE;
			LOGGER.error("Error occured while commiting Transaction " + commitStatus, ex);
			throw new GIException(ex);
		}
		finally {
			LOGGER.debug("getTransactionCommit() End");
		}
		return commitStatus;
	}

	/**
	 * Method is used to update tracking record of SERFF Plan Management table.
	 */
	private void updateTrackingRecord(boolean savedStatus, MedicaidPlanInfoVO planInfoVO, SerffPlanMgmt trackingRecord) {

		LOGGER.debug("updateTrackingRecord() Start");

		try {
			if (null == planInfoVO || null == trackingRecord) {
				return;
			}

			if (savedStatus && planInfoVO.isNotValid()) {
				trackingRecord.setRequestStateDesc("Medicaid Plan has been Loaded with warnings. Please see status message.");
				trackingRecord.setPmResponseXml(planInfoVO.getErrorMessages());
				LOGGER.warn(trackingRecord.getRequestStateDesc());
			}
			else if (savedStatus) {
				trackingRecord.setRequestStateDesc("Medicaid Plan has been Loaded Successfully.");
				trackingRecord.setPmResponseXml(trackingRecord.getRequestStateDesc());
				LOGGER.info(trackingRecord.getRequestStateDesc());
			}
			else {
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
				trackingRecord.setPmResponseXml(planInfoVO.getErrorMessages());
				trackingRecord.setRequestStateDesc("Failed to load Medicaid Plan due to validation errors.");
				LOGGER.error(trackingRecord.getRequestStateDesc());
			}
		}
		finally {
			LOGGER.debug("updateTrackingRecord() End");
		}
	}
}
