package com.serff.service.templates;

import static com.serff.util.SerffConstants.POS_APPLICABLE_YEAR;
import static com.serff.util.SerffConstants.POS_BROCHURE_ECM;
import static com.serff.util.SerffConstants.POS_BROCHURE_FILE;
import static com.serff.util.SerffConstants.POS_EOC_ECM;
import static com.serff.util.SerffConstants.POS_EOC_FILE;
import static com.serff.util.SerffConstants.POS_PLAN_ISSUER_NUM;
import static com.serff.util.SerffConstants.POS_SBC_ECM;
import static com.serff.util.SerffConstants.POS_SBC_FILE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanDocumentsJob;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.plandoc.PlanDocumnetExcelVO;
import com.serff.repository.templates.IPlanDocumentsJobRepository;
import com.serff.repository.templates.IPlanHealthRepository;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.service.validation.PlanDocumentsValidator;
import com.serff.util.SerffConstants;

@Service("PlanDocumentsJobService")
@Repository
@Transactional
public class PlanDocumentsJobServiceImpl implements PlanDocumentsJobService {

	private static final Logger LOGGER = Logger.getLogger(PlanDocumentsJobServiceImpl.class);

	private static final int MAX_ISSUER_COUNT = 5;
	private static final String DOCS_TYPE_ECM = "ECM";
	private static final String DOCS_TYPE_URL = "URL";

	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private IPlanHealthRepository iSerffPlanHealthRepository;
	@Autowired private IPlanDocumentsJobRepository iPlanDocumentsJobRepository;
	@Autowired private PlanDocumentsValidator planDocumentsValidator;
	@Autowired private ISerffPlanRepository iSerffPlanRepository;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;

	@Override
	public int persistPlanDocumentRecord(PlanDocumentsJob documentJob) {
		LOGGER.debug("Saving Plan Document Job. : "
				+ documentJob.getIssuerPlanNumber());
		PlanDocumentsJob documentJobInserted = iPlanDocumentsJobRepository
				.save(documentJob);
		LOGGER.debug("Plan Document Job id : "
				+ documentJobInserted.getId());
		return documentJobInserted.getId();
	}

	@Override
	public Long getPlanCount(@Param("planId") String planId , @Param("applicableYear")Integer applicableYear) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Getting Plan Count by planissuerNumber." + SecurityUtil.sanitizeForLogging(planId));
		}
		return iPlanRepository.getPlanCountWithApplicableYear(planId , applicableYear);
	}

	@Override
	public Long getPlanHealthCount(@Param("planId") String planId ,  @Param("applicableYear") Integer applicableYear) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Getting Plan Helath Count by planissuerNumber." + SecurityUtil.sanitizeForLogging(planId));
		}
		return iSerffPlanHealthRepository.getPlanHealthCount(planId , applicableYear);
	}
	
	@Override
	public Long getPlanDentalCount(@Param("planId") String planId ,  @Param("applicableYear") Integer applicableYear) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Getting Plan Dental Count by planissuerNumber." + SecurityUtil.sanitizeForLogging(planId));
		}
		return iSerffPlanHealthRepository.getPlanDentalCount(planId , applicableYear);
	}

	@Override
	public Map<String, String> updatePlanDocuments(
			Map<String, String[]> planIdDocumentsMap) {

		// Create Map to save values of failed plan ids and error message.
		Map<String, String> failedPlanIdsAndError = null;
		final String loggerData = " for plan with id : ";

		if (null != planIdDocumentsMap) {

			failedPlanIdsAndError = new HashMap<String, String>();

			for (Entry<String, String[]> planDocumentEntry : planIdDocumentsMap.entrySet()) {				
				
				String[] planDocumentValues = planDocumentEntry.getValue();
				
				if (null == planDocumentValues || planDocumentValues.length == 0) {
					continue;
				}
				
				LOGGER.debug("Issuer planNumber to Update in database : " + planDocumentEntry.getKey());
				
				try {					
					List<Plan> planList = iPlanRepository.findByIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYear(Plan.IS_DELETED.N.toString(), planDocumentValues[POS_PLAN_ISSUER_NUM] , Integer.parseInt(planDocumentValues[POS_APPLICABLE_YEAR]));
					
					LOGGER.info("Fetched " +  ((null != planList) ? planList.size() : null) + " plans for id " + planDocumentEntry.getKey());
					
					if (!CollectionUtils.isEmpty(planList)) {
						
						for (Plan plan : planList) {
							
							if (null != planDocumentValues[POS_BROCHURE_ECM] && null != planDocumentValues[POS_BROCHURE_FILE]) {
								plan.setBrochureUCmId(planDocumentValues[POS_BROCHURE_ECM]);
								LOGGER.debug("Setting brochure id " + planDocumentValues[POS_BROCHURE_ECM] + loggerData + plan.getId());
								plan.setBrochureDocName(planDocumentValues[POS_BROCHURE_FILE]);
								LOGGER.debug("Setting brochure name " + planDocumentValues[POS_BROCHURE_FILE] + loggerData + plan.getId());
							}

							// Null check for plan health object.
							if (null != planDocumentValues[POS_SBC_ECM]
									&& null != planDocumentValues[POS_SBC_FILE]) {
								if(null != plan.getPlanHealth()) {
									plan.getPlanHealth().setSbcUcmId(planDocumentValues[POS_SBC_ECM]);
									LOGGER.debug("Setting sbc id for plan health " + planDocumentValues[POS_SBC_ECM] + loggerData + plan.getId());
									plan.getPlanHealth().setSbcDocName(planDocumentValues[POS_SBC_FILE]);
									LOGGER.debug("Setting sbc name for plan health " + planDocumentValues[POS_SBC_FILE] + loggerData + plan.getId());
								} else if(null != plan.getPlanDental()){
									plan.getPlanDental().setSbcUcmId(planDocumentValues[POS_SBC_ECM]);
									LOGGER.debug("Setting sbc id for plan dental " + planDocumentValues[POS_SBC_ECM] + loggerData + plan.getId());
									plan.getPlanDental().setSbcDocName(planDocumentValues[POS_SBC_FILE]);
									LOGGER.debug("Setting sbc name for plan dental " + planDocumentValues[POS_SBC_FILE] + loggerData + plan.getId());
								}else{
									LOGGER.info("No matching health or dental plan found for " + planDocumentEntry.getKey());
								}
							}
							
							if(null!=planDocumentValues[POS_EOC_ECM] && null != planDocumentValues[POS_EOC_FILE]){
								plan.setEocDocId(planDocumentValues[POS_EOC_ECM]);
								LOGGER.debug("Setting Eoc id " + planDocumentValues[POS_EOC_ECM] + loggerData + plan.getId());
								plan.setEocDocName(planDocumentValues[POS_EOC_FILE]);
								LOGGER.debug("Setting Eoc name " + planDocumentValues[POS_EOC_FILE] + loggerData + plan.getId());
							}
							
						}
						// Save whole updated list.
						iPlanRepository.save(planList);
					}
				} catch (Exception e) {
					LOGGER.debug("Database error occurred while saving plan. Adding to failed plan map : " + planDocumentEntry.getKey() + " : " + e.getMessage(), e);
					failedPlanIdsAndError.put(planDocumentEntry.getKey(), e.getMessage());
				}

			}
		}

		return failedPlanIdsAndError;
	}

	@Override
	public Object getEnvironmentTestResult(String env) {
	
		try{
			LOGGER.info("getTestIssuer() called. Environmnt : " + env );
			if( null != entityManagerFactory ){			
				
				EntityManager entityManager = entityManagerFactory.createEntityManager();
				if(env.equalsIgnoreCase("db")){
					
					List<Object> dbTest = null;
					try{
						dbTest = entityManager.createNativeQuery("SELECT 'System Time : ' ||SYSTIMESTAMP || ' IP Adress : '|| SYS_CONTEXT('USERENV','IP_ADDRESS') || ' Host Name : '|| SYS_CONTEXT('USERENV','SERVER_HOST') as result  FROM  DUAL").getResultList();
					}catch(Exception e){
						LOGGER.error("getEnvironmentTestResult() : Error occurred : " + e.getMessage(), e);
					}finally{
						if(null != entityManager){
							entityManager.clear();
							entityManager.close();
							entityManager = null;
						}
					}
					if( null != dbTest ){						
						LOGGER.info("getTestIssuer(). Returning succes db test.");
						return dbTest;
						
					}else{
						LOGGER.info("getTestIssuer(). No issuer found.");
						return "Database is not connected.";
					}							
					
				}else if(env.equalsIgnoreCase("ecm")){
					List results = null;
					try{
						Query query = entityManager.createQuery("SELECT i FROM Issuer i WHERE companyLogo IS NOT NULL");
						query.setMaxResults(MAX_ISSUER_COUNT);
						results = query.getResultList();
					}catch(Exception e){
						LOGGER.error("getEnvironmentTestResult() : Error occurred : " + e.getMessage(), e);
					}finally{
						if(null != entityManager){
							entityManager.clear();
							entityManager.close();
							entityManager = null;
						}
					}
					
					if( null != results){						
						LOGGER.info("getTestIssuer(). Issuer record found with logo.");
						return results.get(0);						
					}else{
						LOGGER.info("getTestIssuer(). No issuer found.");
						return "No issuer found having LOGO uploaded to check ECM connectivity.";
					}
					
				}				
				
			}else{
				LOGGER.info("getTestIssuer(). Database is not connected to server.");
				return "Database is not connected. Contact administrator.";						
			}
			
		}catch(Exception e){		
			LOGGER.info("getTestIssuer(). Exception occurred : " + e.getMessage(), e);
			return e.getMessage();
		}
		LOGGER.info("getTestIssuer(). Returning null.");
		return null;		
	}

	/**
	 * Method is used to process Plan Document Excel data.
	 */
	@Override
	public boolean populateAndPersistPlanDocumentData(List<PlanDocumnetExcelVO> planDocumnetExcelList, SerffPlanMgmt trackingRecord) throws GIException {

		LOGGER.info("populateAndPersistPlanDocumentData() Strat");
		boolean persistStatus = false;
		boolean failedStatus = false;
		EntityManager entityManager = null;

		try {

			if (CollectionUtils.isEmpty(planDocumnetExcelList) || null == trackingRecord) {
				LOGGER.error("Plan Documnet Excel Data List is empty !!");
				return persistStatus;
			}

			entityManager = entityManagerFactory.createEntityManager(); // Get DB connection using Entity Manager Factory
			entityManager.getTransaction().begin(); // Begin Transaction
			Plan existingPlan = null;
			List<Plan> savedPlanList = new ArrayList<Plan>();
			StringBuilder messages = new StringBuilder();

			for (PlanDocumnetExcelVO planDocumnetExcelVO : planDocumnetExcelList) {

				if (!planDocumentsValidator.validatePlanDocumentsList(planDocumnetExcelVO, messages)) {
					failedStatus = true;
					continue;
				}
				existingPlan = fetchExistingPlan(planDocumnetExcelVO);

				if (null != existingPlan && persistPlanDocumentExcelData(planDocumnetExcelVO, existingPlan)) {
					savedPlanList.add((Plan) mergeEntityManager(entityManager, existingPlan));
				}
			}

			if (StringUtils.isNotBlank(messages.toString())) {
				trackingRecord.setPmResponseXml(messages.toString());
			}

			if (!CollectionUtils.isEmpty(savedPlanList)) {
				persistStatus = true;
				LOGGER.info("Number of Plans to be update from document: " + savedPlanList.size());
				LOGGER.info("Validation Messages: " + messages);
			}
		}
		catch (Exception ex) {
			persistStatus = false;
			LOGGER.error("populateAndPersistPlanDocumentData() Exception occurred: " + ex.getMessage(), ex);
			throw new GIException(ex);
		}
		finally {
			persistStatus = getTransactionCommit(persistStatus, entityManager);
			
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}

			if (failedStatus && persistStatus) {
				trackingRecord.setRequestStateDesc("Plan Document has been Loaded with warnings. Please see status message.");
			}
			else if (persistStatus) {
				trackingRecord.setPmResponseXml("Plan Document has been Loaded Successfully.");
				trackingRecord.setRequestStateDesc(trackingRecord.getPmResponseXml());
			}
			else {
				trackingRecord.setRequestStateDesc("Failed to load Plan Document Excel.");
			}
			LOGGER.info("populateAndPersistPlanDocumentData() End");
		}
		return persistStatus;
	}

	/**
	 * Method is used to fetch Existing Plan from Issuer Plan Number.
	 */
	private Plan fetchExistingPlan(PlanDocumnetExcelVO planDocumnetExcelVO) {

		LOGGER.info("fetchExistingPlan() Strat");
		Plan existingPlan = null;

		try {

			if (null == planDocumnetExcelVO
					|| StringUtils.isBlank(planDocumnetExcelVO.getPlanId())
					|| !StringUtils.isNumeric(planDocumnetExcelVO.getApplicableYear())) {
				LOGGER.error("Required inputs are empty for fectching Existing-Plan.");
				return existingPlan;
			}
			existingPlan = iSerffPlanRepository.findByIssuerPlanNumberAndIsDeletedAndApplicableYear(planDocumnetExcelVO.getPlanId().replaceFirst(SerffConstants.HYPHEN, StringUtils.EMPTY), Plan.IS_DELETED.N.name(), Integer.valueOf(planDocumnetExcelVO.getApplicableYear()));

			if (null == existingPlan) {
				planDocumnetExcelVO.setErrorMessages("Issuer Plan Number does not exist in Plan table.");
				LOGGER.error(planDocumnetExcelVO.getErrorMessages());
			}
		}
		catch (Exception ex) {
			LOGGER.error("fetchExistingPlan() Exception occurred: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("fetchExistingPlan() End");
		}
		return existingPlan;
	}

	/**
	 * Method is used to persisting Plan Document Excel data in Plan & Plan-Health/Dental tables.
	 */
	private boolean persistPlanDocumentExcelData(PlanDocumnetExcelVO planDocumnetExcelVO, Plan existingPlan) {

		LOGGER.info("persistPlanDocumentExcelData() Strat");
		boolean status = false;
		boolean broucherStatus = false;
		boolean sbcStatus = false;
		boolean eocStatus = false;

		try {

			if (null == existingPlan) {
				return status;
			}

			broucherStatus = setBrochureParams(planDocumnetExcelVO, existingPlan);
			sbcStatus = setSBCParams(planDocumnetExcelVO, existingPlan);
			eocStatus = setEOCParams(planDocumnetExcelVO, existingPlan);
			
			if(broucherStatus||sbcStatus||eocStatus){
				status = true;
			}
		}
		catch (Exception ex) {
			LOGGER.error("persistPlanDocumentExcelData() Exception occurred: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("persistPlanDocumentExcelData() End with status: " + status);
		}
		return status;
	}
	/**
	 * This method saves the Brochure Params
	 * @param planDocumnetExcelVO
	 * @param existingPlan
	 * @return
	 */
	private boolean setBrochureParams(PlanDocumnetExcelVO planDocumnetExcelVO,
			Plan existingPlan) {
		boolean status = false;

		if (StringUtils.isNotBlank(planDocumnetExcelVO.getBrochureDocumentName())
				&& StringUtils.isNotBlank(planDocumnetExcelVO.getBrochureType())) {

			if (DOCS_TYPE_ECM.equalsIgnoreCase(planDocumnetExcelVO.getBrochureType())
					&& StringUtils.isNotBlank(planDocumnetExcelVO.getBrochureDocumentFileName())) {
				status = true;
				existingPlan.setBrochureUCmId(planDocumnetExcelVO.getBrochureDocumentName());
				existingPlan.setBrochureDocName(planDocumnetExcelVO.getBrochureDocumentFileName());
			}
			else if (DOCS_TYPE_URL.equalsIgnoreCase(planDocumnetExcelVO.getBrochureType())) {
				status = true;
				existingPlan.setBrochure(planDocumnetExcelVO.getBrochureDocumentName());
			}
		}
		return status;
	}

	/**
	 * This method saves SBC params
	 * @param planDocumnetExcelVO
	 * @param existingPlan
	 * @return
	 */
	private boolean setSBCParams(PlanDocumnetExcelVO planDocumnetExcelVO,
			Plan existingPlan) {

		boolean status = false;

		if (StringUtils.isBlank(planDocumnetExcelVO.getSbcName())
				|| StringUtils.isBlank(planDocumnetExcelVO.getSbcType())) {
			return status;
		}

		if (DOCS_TYPE_ECM.equalsIgnoreCase(planDocumnetExcelVO.getSbcType())
				&& StringUtils.isNotBlank(planDocumnetExcelVO.getSbcFileName())) {
			status = setSBCForECMParams(planDocumnetExcelVO, existingPlan);
		}
		else if (DOCS_TYPE_URL.equalsIgnoreCase(planDocumnetExcelVO.getSbcType())) {
			status = setSBCForURLParams(planDocumnetExcelVO, existingPlan);
		}
		return status;
	}

	/**
	 * This method saves SBC params for Doc type ECM
	 * @param planDocumnetExcelVO
	 * @param existingPlan
	 * @return
	 */
	private boolean setSBCForECMParams(PlanDocumnetExcelVO planDocumnetExcelVO,
			Plan existingPlan) {

		boolean status = false;

		if (Plan.PlanInsuranceType.HEALTH.name().equalsIgnoreCase(existingPlan.getInsuranceType())
				&& null != existingPlan.getPlanHealth()) {
			status = true;
			existingPlan.getPlanHealth().setSbcUcmId(planDocumnetExcelVO.getSbcName());
			existingPlan.getPlanHealth().setSbcDocName(planDocumnetExcelVO.getSbcFileName());
		}
		else if (Plan.PlanInsuranceType.DENTAL.name().equalsIgnoreCase(existingPlan.getInsuranceType())
				&& null != existingPlan.getPlanDental()) {
			status = true;
			existingPlan.getPlanDental().setSbcUcmId(planDocumnetExcelVO.getSbcName());
			existingPlan.getPlanDental().setSbcDocName(planDocumnetExcelVO.getSbcFileName());
		}
		return status;
	}

	/**
	 * This method saves SBC params for doc type URL
	 * @param planDocumnetExcelVO
	 * @param existingPlan
	 * @return
	 */
	private boolean setSBCForURLParams(PlanDocumnetExcelVO planDocumnetExcelVO,
			Plan existingPlan) {

		boolean status = false;

		if (Plan.PlanInsuranceType.HEALTH.name().equalsIgnoreCase(existingPlan.getInsuranceType())
				&& null != existingPlan.getPlanHealth()) {
			status = true;
			existingPlan.getPlanHealth().setBenefitsUrl(planDocumnetExcelVO.getSbcName());
		}
		else if (Plan.PlanInsuranceType.DENTAL.name().equalsIgnoreCase(existingPlan.getInsuranceType())
				&& null != existingPlan.getPlanDental()) {
			status = true;
			 existingPlan.getPlanDental().setBenefitsUrl(planDocumnetExcelVO.getSbcName());
		}
		return status;
	}
	
	/**
	 * This method saves EOC params
	 * @param planDocumnetExcelVO
	 * @param existingPlan
	 * @return
	 */
	private boolean setEOCParams(PlanDocumnetExcelVO planDocumnetExcelVO,
			Plan existingPlan) {

		boolean status = false;

		if (StringUtils.isNotBlank(planDocumnetExcelVO.getEocDocumentName())
				&& DOCS_TYPE_ECM.equalsIgnoreCase(planDocumnetExcelVO.getEocType())
				&& StringUtils.isNotBlank(planDocumnetExcelVO.getEocDocumentFileName())) {
			status = true;
			existingPlan.setEocDocId(planDocumnetExcelVO.getEocDocumentName());
			existingPlan.setEocDocName(planDocumnetExcelVO.getEocDocumentFileName());
		}
		return status;
	}

	/**
	 * Method is used to merge Table to Entity Manager.
	 */
	private Object mergeEntityManager(EntityManager entityManager, Object saveObject) {

		Object savedObject = null;

		if (null != entityManager && entityManager.isOpen()) {
			savedObject = entityManager.merge(saveObject);
			LOGGER.debug(saveObject.getClass() + " bean has been Persisted Successfully.");
		}
		return savedObject;
	}

	/**
	 * Method is used to Commit/Rollback transaction depends on Status argument.
	 */
	private boolean getTransactionCommit(boolean status, EntityManager entityManager) throws GIException {

		LOGGER.debug("getTransactionCommit() Start");
		boolean commitStatus = status;

		try {

			if (null != entityManager && entityManager.isOpen()) {

				if (status) {
					entityManager.getTransaction().commit();
					LOGGER.info("Transaction Commit has been Commited");
				}
				else {
					entityManager.getTransaction().rollback();
					LOGGER.info("Transaction Commit has been Rollback");
				}
			}
		}
		catch (Exception ex) {
			commitStatus = Boolean.FALSE;
			LOGGER.error("Error occured while commiting Transaction " + commitStatus, ex);
			throw new GIException(ex);
		}
		finally {
			LOGGER.debug("getTransactionCommit() End");
		}
		return commitStatus;
	}
}
