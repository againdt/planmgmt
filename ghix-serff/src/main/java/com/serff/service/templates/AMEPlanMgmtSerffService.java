package com.serff.service.templates;

import java.util.Map;

import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.ame.AMEExcelVO;
import com.getinsured.serffadapter.ame.AMEPremiumFactorExcelVO;

/**
 * Interface is used to persist AME Plans to database using AMEPlanMgmtSerffServiceImpl class.
 * 
 * @author Bhavin Parmar
 * @since August 14, 2014
 */
public interface AMEPlanMgmtSerffService {

	boolean populateAndPersistAMEData(Map<String, AMEExcelVO> amePlansMapData, AMEExcelVO ameExcelHeader,
			Map<String, AMEPremiumFactorExcelVO> amePremiumFactorMapData, SerffPlanMgmt trackingRecord, String planType, String defaultTenantIds)
			throws GIException;
}
