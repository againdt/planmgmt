package com.serff.service.templates;

import java.util.Map;

import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.puf.medicare.MedicareVO;

/**
 * Interface is used to persist Medicare Plans to database using MedicarePlanMgmtSerffServiceImpl class.
 * 
 * @author Bhavin Parmar
 * @since Apr 14, 2015
 */
public interface MedicarePlanMgmtSerffService {

	boolean populateAndPersistMedicareData(Map<String, Map<String, MedicareVO>> medicareMapList, SerffPlanMgmt trackingRecord) throws GIException;
}
