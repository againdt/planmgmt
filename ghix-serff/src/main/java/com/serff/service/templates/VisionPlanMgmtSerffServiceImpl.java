package com.serff.service.templates;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.Plan.IssuerVerificationStatus;
import com.getinsured.hix.model.PlanRate;
import com.getinsured.hix.model.PlanVision;
import com.getinsured.hix.model.PlanVisionBenefits;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.lookup.repository.ILookupRepository;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.vision.VisionPlanDataVO;
import com.getinsured.serffadapter.vision.VisionPlanRatesVO;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.IPlanRate;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.service.validation.VisionPlanDataValidator;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 *-----------------------------------------------------------------------------
 * HIX-59162 PHIX - Load and persist vision plans.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to persist Vision Plans to the database.
 * 
 * @author Bhavin Parmar
 * @since January 27, 2015
 */
@Service("visionPlanMgmtSerffService")
public class VisionPlanMgmtSerffServiceImpl implements VisionPlanMgmtSerffService {

	private static final Logger LOGGER = Logger.getLogger(VisionPlanMgmtSerffServiceImpl.class);

	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private IIssuersRepository iIssuerRepository;
	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private ILookupRepository iLookupRepository;
	@Autowired private VisionPlanDataValidator visionPlanDataValidator;
	@Autowired private IPlanRate iPlanRateRepository;
	@Autowired private SerffUtils serffUtils;

	private DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static final String PLAN_END_DATE = "12/31/2099";
	private static final String VALUE_ZERO = "0";
	private static final String FUTURE_RATES_PROCESSING_FAILED_MSG = "Invalid rates present for Certified Vision Plan : planid : ";

	/**
	 * Method is used to populate and persist Vision plans from AMEExcelVO to database.
	 */
	@Override
	public boolean populateAndPersistVisionData(Map<String, VisionPlanDataVO> visionPlansMap,
			VisionPlanDataVO visionExcelHeader, String defaultTenantIds, SerffPlanMgmt trackingRecord) throws GIException {

		LOGGER.info("populateAndPersistVisionData() Start");
		boolean isDataProcessingComplete = true;
		EntityManager entityManager = null;
		Map<String, Issuer> issuerMap = new HashMap<String, Issuer>();
		int applicableYear = 0;
		List<Plan> savedPlanList = new ArrayList<Plan>();
		List<VisionPlanDataVO> failedVisionPlanVOList = new ArrayList<VisionPlanDataVO>();
		Map<String, Boolean> duplicatePlansMap = new HashMap<String, Boolean>(); // Map<String: IssuerPlanNumber, Boolean: Duplicate Or Not>
		
		try {
			entityManager = entityManagerFactory.createEntityManager(); // Get DB connection using Entity Manager Factory
			entityManager.getTransaction().begin(); // Begin Transaction
			List<Tenant> tenantList = serffUtils.getTenantList(defaultTenantIds);
			for (Map.Entry<String, VisionPlanDataVO> visionEntry : visionPlansMap.entrySet()) {
				
				// Appending Off Exchange Variant in Issuer Plan Number
				String issuerPlanNumber = visionEntry.getKey() + SerffConstants.OFF_EXCHANGE_VARIANT_SUFFIX;
				VisionPlanDataVO visionPlanDataVO = visionEntry.getValue();
				LOGGER.info("Processing Vision plan : " + issuerPlanNumber);
				
				//Validate Plan. If plan is invalid add in Failed Plan List and continue.
				if(!visionPlanDataValidator.validateVisionExcelVO(issuerPlanNumber, visionPlanDataVO)){
					LOGGER.error("Excel data is not valid for plan : issuerPlanMumber : " + issuerPlanNumber);
					failedVisionPlanVOList.add(visionPlanDataVO);
					continue;
				}
				
				//Check if Issuer exist in DB for given HIOSID.
				Issuer existingIssuer = getIssuerFromDb(visionPlanDataVO.getCarrierHIOSId());
				if(null != existingIssuer){

					if (!CollectionUtils.isEmpty(duplicatePlansMap) && duplicatePlansMap.containsKey(issuerPlanNumber)) {
						duplicatePlansMap.put(issuerPlanNumber, true); // Override Key with flag value 
						continue;
					}
					duplicatePlansMap.put(issuerPlanNumber, false);
					
					//Issuer present in DB.
					issuerMap.put(visionPlanDataVO.getCarrierHIOSId(), existingIssuer);
					LOGGER.info("Issuer added in map : hiosIssuerId : " + visionPlanDataVO.getCarrierHIOSId());
					//Start processing and persisting Vision Plan data.
					if (isNewPlanToPersist(visionEntry.getKey(), applicableYear, visionPlanDataVO, savedPlanList, failedVisionPlanVOList, entityManager)) {
						// Save Plan object.
						Plan plan = savePlan(applicableYear, issuerPlanNumber, existingIssuer, visionPlanDataVO, entityManager);
						// Save Plan Vision Object.
						savePlanVision(plan, visionPlanDataVO, visionExcelHeader, entityManager);
						// Save Rates for Vision plan.
						savePlanVisionRates(plan, visionPlanDataVO, entityManager);
						// Add Plan in saved plans list.
						LOGGER.info("Processing of Vision plan is successful. Saving plan in DB : " + issuerPlanNumber);
						savedPlanList.add(plan);
						//Set tenant
						if(null != tenantList) {
							serffUtils.saveTenantPlan(plan, tenantList, entityManager);
						}
					}
					else {
						// Certified plan will be added in either saved or failed list in isNewPlanToPersist() method.
						LOGGER.info("Skipping processing of Vision plan : " + issuerPlanNumber + " because plan was already Certified in database.");
					}
				}
				else {
					//Issuer is not present in DB.
					LOGGER.error("Issuer does not exist for hiosIssuerId : " + visionPlanDataVO.getCarrierHIOSId());
					visionPlanDataVO.setErrorMessages("Issuer does not exist for Vision plan : " + issuerPlanNumber + " : hiosIssuerId : " + visionPlanDataVO.getCarrierHIOSId());
					visionPlanDataVO.setNotValid(true);
					failedVisionPlanVOList.add(visionPlanDataVO);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred in populateAndPersistVisionData()"  + ex.getMessage(), ex);
			isDataProcessingComplete = false;
			throw new GIException(ex);
		}
		finally {
			isDataProcessingComplete = completeTransaction(isDataProcessingComplete, entityManager);
			updateTrackingRecord(isDataProcessingComplete, savedPlanList, failedVisionPlanVOList, duplicatePlansMap, trackingRecord);

			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			savedPlanList.clear();
			savedPlanList = null;
			failedVisionPlanVOList.clear();
			failedVisionPlanVOList = null;
			duplicatePlansMap.clear();
			duplicatePlansMap = null;
			LOGGER.info("populateAndPersistVisionData() End");
		}
		return isDataProcessingComplete;
	}
	
	/**
	 * Returns existing issuer from database fro provided HiosId.
	 * 
	 * @param issuerHiosId - Issuer hios id
	 * @return - The Issuer instance
	 */
	private Issuer getIssuerFromDb(String issuerHiosId){
		
		LOGGER.debug("getIssuerFromDb() satrt");
		Issuer existingIssuer = null;
		try{
			existingIssuer = iIssuerRepository.getIssuerByHiosID(issuerHiosId);
		}catch(Exception e){
			LOGGER.error("Exception while retrieving issuer from database.", e);
		}finally{
			LOGGER.debug("getIssuerFromDb() existingIssuer : " + existingIssuer);
			LOGGER.debug("getIssuerFromDb() end");
		}
		
		return existingIssuer;
	}
	
	/**
	 * Creates error message by reading error messages set in failed list of Vision Plan VO objects.
	 * 
	 * @param failedVisionPlanList - Vision plans those are failed to persist.
	 * @return - String representation of error messages.
	 */
	private String createErrorMessage(List<VisionPlanDataVO> failedVisionPlanList, Map<String, Boolean> duplicatePlansMap) {
		LOGGER.debug("createErrorMessage() Start");
		StringBuffer errorMessage = new StringBuffer();
		
		if (!CollectionUtils.isEmpty(failedVisionPlanList)) {
			
			for (VisionPlanDataVO visionPlanDataVO : failedVisionPlanList) {

				if (StringUtils.isBlank(visionPlanDataVO.getErrorMessages())) {
					continue;
				}
				errorMessage.append(visionPlanDataVO.getErrorMessages());
				errorMessage.append("\n");
			}
		}

		if (!CollectionUtils.isEmpty(duplicatePlansMap)) {
			List<String> duplicatePlansList = new ArrayList<String>();

			for (Map.Entry<String, Boolean> entryData : duplicatePlansMap.entrySet()) {

				if (null == entryData.getValue() || !entryData.getValue()) {
					continue;
				}
				duplicatePlansList.add(entryData.getKey());
			}

			if (!CollectionUtils.isEmpty(duplicatePlansList)) {
				errorMessage.append("Skipped duplicate IssuerPlanNumber(s) from Template: ");
				errorMessage.append(duplicatePlansList.toString());
			}
		}
		LOGGER.error(errorMessage.toString());
		LOGGER.debug("createErrorMessage() End");
		return errorMessage.toString();
	}
	
	/**
	 *  Method is used to update tracking record of SERFF Plan Management table.
	 * 
	 * @param isDataProcessingComplete - Whether data processing is completed
	 * @param savedVisionPlanList - Plans those are saved successfully
	 * @param failedVisionPlanList - Plans those are failed to persist
	 * @param trackingRecord - Serff Plan Mgmt record.
	 */
	private void updateTrackingRecord(boolean isDataProcessingComplete, List<Plan> savedVisionPlanList,
			List<VisionPlanDataVO> failedVisionPlanList, Map<String, Boolean> duplicatePlansMap, SerffPlanMgmt trackingRecord) {

		LOGGER.debug("updateTrackingRecord() Start");

		try {
			if (null == trackingRecord) {
				return;
			}
			String errorMessages = createErrorMessage(failedVisionPlanList, duplicatePlansMap);

			if (isDataProcessingComplete) {
				//Process completed. No plans saved.All Certified plans or All plan's validation failed.
				if (CollectionUtils.isEmpty(savedVisionPlanList)) {
					trackingRecord.setRequestStateDesc("Process completed successfully. No Vision Plans Loaded. Please see status message.");
				}
				//Process completed. All plans saved.
				else if (StringUtils.isBlank(errorMessages)) {
					trackingRecord.setRequestStateDesc("Vision Plan has been Loaded Successfully.");
				}
				//Process completed. Few Plans saved and few plans are Not Saved in db.
				else {
					//Either more than one plans are certified or more than one plans with Validation errors present.
					trackingRecord.setRequestStateDesc("Vision Plan has been Loaded with warnings. Please see status message.");
					trackingRecord.setPmResponseXml(errorMessages);
				}
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
				LOGGER.info(trackingRecord.getRequestStateDesc());
			}
			else {
				//Exception occurred while committing or merging entity.
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
				trackingRecord.setPmResponseXml(errorMessages);
				trackingRecord.setRequestStateDesc("Error occurred while saving Vision plans. Check response for details.");
				LOGGER.error(trackingRecord.getRequestStateDesc());
			}
		}
		finally {

			if (null != trackingRecord && StringUtils.isBlank(trackingRecord.getPmResponseXml())
					&& StringUtils.isNotBlank(trackingRecord.getRequestStateDesc())) {
				trackingRecord.setPmResponseXml(trackingRecord.getRequestStateDesc());
			}
			LOGGER.debug("updateTrackingRecord() End");
		}
	}
	
	/**
	 * Merges entities by using provided entity manager.
	 * 
	 * @param entityManager  -The entity manager instance.
	 * @param saveObject - Entity to be saved in database.
	 * @return - Entity saved in database.
	 */
	private Object mergeEntityManager(EntityManager entityManager, Object saveObject) {

		LOGGER.debug("mergeEntityManager() Start");
		Object savedObject = null;

		if (null != saveObject && null != entityManager && entityManager.isOpen()) {
			savedObject = entityManager.merge(saveObject);
			LOGGER.debug(saveObject.getClass() + " bean has been Persisted Successfully.");
		}
		LOGGER.debug("mergeEntityManager() End");
		return savedObject;
	}
	
	/**
	 * Method is used to Commit/Rollback transaction depends on Status argument.
	 * 
	 * @param commitRequestStatus - Request to whether commit the transaction
	 * @param entityManager - The entity manager instance
	 * @return - Whether transaction is commited
	 * @throws GIException
	 */
	private boolean completeTransaction(boolean commitRequestStatus, EntityManager entityManager) throws GIException {

		LOGGER.info("completeTransaction() Start");
		boolean commitResponseStatus = false;
		
		try {

			if (null != entityManager && entityManager.isOpen()) {

				if (commitRequestStatus) {
					entityManager.getTransaction().commit();
					commitResponseStatus = true;
					LOGGER.info("Transaction has been Commited");
				}
				else {
					entityManager.getTransaction().rollback();
					commitResponseStatus = false;
					LOGGER.info("Transaction has been Rollback");
				}
			}
		}
		catch (Exception ex) {
			commitResponseStatus = false;
			LOGGER.error("Error occured while commiting Transaction " + commitResponseStatus, ex);
			throw new GIException(ex);
		}
		finally {
			LOGGER.info("commitStatus : "  + commitResponseStatus);
			LOGGER.info("completeTransaction() End");
		}
		
		return commitResponseStatus;
	}
	
	
	/**
	 * Saves Parent Plan instance in PLAN table
	 * 
	 * @param applicableYear - Applicable year
	 * @param issuerPlanNumber - The issuer plan number
	 * @param existingIssuer - Existing issuer for this Plan
	 * @param visionPlanDataVO - Plan Vision VO
	 * @param entityManager - Entity Manager instance.
	 * @return - Saved plan in DB
	 */
	private Plan savePlan(int applicableYear, String issuerPlanNumber, Issuer existingIssuer, VisionPlanDataVO visionPlanDataVO, EntityManager entityManager) {
		
		LOGGER.debug("savePlan() Start : " + visionPlanDataVO.getPlanName());
		Plan plan = null;
		
		try {
			if (StringUtils.isNotBlank(issuerPlanNumber)) {
				plan = new Plan();
				plan.setIssuerVerificationStatus(IssuerVerificationStatus.PENDING.toString());
				plan.setIssuer(existingIssuer);
				plan.setInsuranceType(Plan.PlanInsuranceType.VISION.name());
				plan.setStatus(Plan.PlanStatus.LOADED.toString());
				plan.setIssuerPlanNumber(issuerPlanNumber);
				plan.setName(visionPlanDataVO.getPlanName());
				plan.setBrochure(SerffUtils.checkAndcorrectURL(visionPlanDataVO.getPlanBrochureURL()));
				plan.setHsa(SerffConstants.NO);
				plan.setState(visionPlanDataVO.getState());
				plan.setMarket(Plan.PlanMarket.INDIVIDUAL.toString());
				plan.setHiosProductId(issuerPlanNumber.substring(0, SerffConstants.LEN_HIOS_PRODUCT_ID));
				plan.setExchangeType(Plan.EXCHANGE_TYPE.OFF.toString());
				plan.setIsPUF(Plan.IS_PUF.N);
				plan.setIsDeleted(Plan.IS_DELETED.N.toString());
				plan.setApplicableYear(applicableYear);
				plan.setCreatedBy(SerffConstants.DEFAULT_USER_ID);
				plan = (Plan) mergeEntityManager(entityManager, plan);
			}
		}
		finally {
			LOGGER.debug("savePlan() End");
		}
		return plan;
	}
	
	
	/**
	 * Saves Vision Plan and Benefits instance in database.
	 * 
	 * @param savedPlan - Parent plan saved.
	 * @param visionPlanDataVO - Vision plan VO
	 * @param visionExcelHeader - Excel data header
	 * @param entityManager - Entity manager instance
	 * @return - Saved Plan Vision instance
	 */
	private PlanVision savePlanVision(Plan savedPlan, VisionPlanDataVO visionPlanDataVO, VisionPlanDataVO visionExcelHeader, EntityManager entityManager) {
		
		LOGGER.debug("savePlanVision() Start");
		LOGGER.debug("savePlanVision() Plan Name : " + visionPlanDataVO.getPlanName());
		PlanVision planVision = new PlanVision();
		
		try {
			
			planVision.setOutOfNetAvailability(visionPlanDataVO.getOutOfNetworkAvailability());
			planVision.setOutOfNetCoverage(visionPlanDataVO.getOutOfNetworkCoverage());
			planVision.setPlan(savedPlan);
			planVision.setPremiumPayment(visionPlanDataVO.getPremiumPayment());
			planVision.setProviderNetURL(SerffUtils.checkAndcorrectURL(visionPlanDataVO.getProviderNetworkURL()));
			planVision.setCreatedBy(SerffConstants.DEFAULT_USER_ID);
			
			planVision = (PlanVision) mergeEntityManager(entityManager, planVision);
			
			//Benefit "EYE_EXAM"
			savePlanVisionBenefit(visionExcelHeader.getEyeExamDetails(), visionPlanDataVO.getEyeExamDetails(), visionPlanDataVO.getEyeExamCopayAttr(), 
					visionPlanDataVO.getEyeExamCopayVal(), planVision, entityManager);
			//Benefit "GLASSES"
			savePlanVisionBenefit(visionExcelHeader.getGlassesDetails(), visionPlanDataVO.getGlassesDetails(), visionPlanDataVO.getGlassesCopayAttr(), 
					visionPlanDataVO.getGlassesCopayVal(), planVision, entityManager);
			//Benefit "FRAMES"
			savePlanVisionBenefit(visionExcelHeader.getFramesDetails(), visionPlanDataVO.getFramesDetails(), visionPlanDataVO.getFramesCopayAttr(), 
					visionPlanDataVO.getFramesCopayVal(), planVision, entityManager);
			//Benefit "CONTACT_LENS"
			savePlanVisionBenefit(visionExcelHeader.getContactsDetails(), visionPlanDataVO.getContactsDetails(), visionPlanDataVO.getContactsCopayAttr(), 
					visionPlanDataVO.getContactsCopayVal(), planVision, entityManager);
			//Benefit "LASER_VISION_CORRECTION"
			savePlanVisionBenefit(visionExcelHeader.getLaserVisionCorrectionDetails(), visionPlanDataVO.getLaserVisionCorrectionDetails(), null, 
					VALUE_ZERO, planVision, entityManager);
			//Benefit "SUNGLASSES_AND_EXTRA_GLASSES"
			savePlanVisionBenefit(visionExcelHeader.getSunglassesandExtraGlassesDetails(), visionPlanDataVO.getSunglassesandExtraGlassesDetails(), null, 
					VALUE_ZERO, planVision, entityManager);

		}
		finally {
			LOGGER.debug("savePlanVision() End");
		}
		return planVision;
	}
	
	
	/**
	 * Saves Benefits of Vision plan.
	 * 
	 * @param benefitName - Benefit name
	 * @param benefitDetails - Benefit details
	 * @param benefitCopayAttribute - Benefit Copay attribute
	 * @param benefitCopayValue - Benefit copay value
	 * @param planVision - Plan vision object
	 * @param entityManager - Entity manager instance.
	 */
	private void savePlanVisionBenefit(String benefitName, String benefitDetails, String benefitCopayAttribute, String benefitCopayValue, 
			PlanVision planVision, EntityManager entityManager){

		LOGGER.debug("savePlanVisionBenefit() Start");
		PlanVisionBenefits planVisionBenefit = new PlanVisionBenefits();
		planVisionBenefit.setBenefitName(benefitName);
		planVisionBenefit.setBenefitDetails(benefitDetails);
		planVisionBenefit.setCopayAttribute(benefitCopayAttribute);
		planVisionBenefit.setCopayVal(Integer.parseInt(benefitCopayValue));
		planVisionBenefit.setPlanVision(planVision);
		planVisionBenefit.setCreatedBy(SerffConstants.DEFAULT_USER_ID);
		mergeEntityManager(entityManager, planVisionBenefit);
		LOGGER.debug("savePlanVisionBenefit() End");
		
	}
		
	
	/**
	 * Vision plan rates are Family Rates so this method will lookup Family Rates only. 
	 * 
	 * @param savedPlan - Parent plan saved
	 * @param visionPlanDataVO - Vision plan VO
	 * @param entityManager - Entity manager instance
	 */
    private void savePlanVisionRates(Plan savedPlan, VisionPlanDataVO visionPlanDataVO, EntityManager entityManager){
    	
		LOGGER.debug("savePlanVisionRates() Start");
		try {
			List<LookupValue> lookupValueCodeList = iLookupRepository.getLookupValueList(SerffConstants.SERFF_FAMILY_OPTION_VALUE);
			
			if (CollectionUtils.isEmpty(lookupValueCodeList)) {
				return;
			}
			
			int lookupValueCode;
			String rate = null;
			
			for (LookupValue lookupValue : lookupValueCodeList) {

				if(null == lookupValue) {
					continue;
				}
				lookupValueCode = Integer.valueOf(lookupValue.getLookupValueCode().trim());
				LOGGER.debug("Persisting Rate for " + lookupValueCode);
				
				if(lookupValueCode > 0) {
					rate = getRateForVisionPlan(lookupValueCode, visionPlanDataVO.getVisionPlanRatesVO());
					// For Family base rates always send relation as NULL.
					saveVisionPlanRate(savedPlan, lookupValueCode, lookupValueCode, rate, getRateStartDate(visionPlanDataVO), null, entityManager);
				}
			}
		}
		finally {
			LOGGER.debug("savePlanVisionRates() End");
		}
		
    }
	    
    /**
     * 
     *  Method is used to save PlanVisionRate data.
     *  
     * @param savedPlan - Parent plan saved.
     * @param minAge - Minimum age
     * @param maxAge -  Maximum age
     * @param rate - Plan rate
     * @param startDate  - Plan statr date. 
     * @param relation - Relation
     * @param entityManager _ Entity mager instance.
     */
	private void saveVisionPlanRate(Plan savedPlan, int minAge, int maxAge, String rate, Date startDate, PlanRate.Relation relation, EntityManager entityManager) {
		
		LOGGER.debug("saveVisionPlanRate() Start");
		LOGGER.debug("saveVisionPlanRate() Rate: " + rate);
		try {
			if(null != rate) {
				PlanRate planRate = new PlanRate();
				planRate.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);
				planRate.setMaxAge(maxAge);
				planRate.setMinAge(minAge);
				planRate.setRate(Float.parseFloat(rate.trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
				
				if (null != relation) {
					planRate.setRelation(relation.name());
					planRate.setRateOption(PlanRate.RATE_OPTION.A);
				}
				else {
					planRate.setRateOption(PlanRate.RATE_OPTION.F);
				}
				planRate.setIsDeleted(PlanRate.IS_DELETED.N.toString());
				planRate.setEffectiveStartDate(startDate);
				planRate.setEffectiveEndDate(dateFormat.parse(PLAN_END_DATE));				
				planRate.setPlan(savedPlan);
				mergeEntityManager(entityManager, planRate);
			}
		} catch (ParseException e) {
			LOGGER.error("Exception occurred while saving Vision Plan Rate.", e);
		}
		finally {
			LOGGER.debug("saveVisionPlanRate() End");
		}
	}
	
	/**
	 * Returns rate for Vision plan.
	 * 
	 * @param lookupValueCode - lookupValueCode for family rates.
	 * @param visionPlanRatesVO - Vision plan VO
	 * @return - Rate for lookup value code
	 */
	private String getRateForVisionPlan(int lookupValueCode, VisionPlanRatesVO visionPlanRatesVO) {
		String rate = null;
		LOGGER.debug("getRateForVisionPlan() Start");
		LOGGER.debug("getRateForVisionPlan() lookupValueCode : "  + lookupValueCode);
		switch(lookupValueCode) {
			case SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_CODE: 
				rate = visionPlanRatesVO.getIndividual();
				break;
			case SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_CODE: 
				rate = visionPlanRatesVO.getCouple();
				break;
			case SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_ONE_DEPENDENT_CODE: 
				rate = visionPlanRatesVO.getOneAdultOneKid();
				break;
			case SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_TWO_DEPENDENT_CODE: 
				rate = visionPlanRatesVO.getOneAdultTwoKid();
				break;
			case SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_MANY_DEPENDENT_CODE: 
				rate = visionPlanRatesVO.getOneAdultThreePlusKid();
				break;
			case SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_ONE_DEPENDENT_CODE: 
				rate = visionPlanRatesVO.getTwoAdultOneKid();
				break;
			case SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_TWO_DEPENDENT_CODE: 
				rate = visionPlanRatesVO.getTwoAdultTwoKid();
				break;
			case SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_MANY_DEPENDENT_CODE: 
				rate = visionPlanRatesVO.getTwoAdultThreePlusKid();
				break;
			default: 
				break;
		}
		LOGGER.debug("getRateForVisionPlan() End");
		
		return rate;
	}
	
	/**
	 * Returns Plan Start Date from VO.
	 * 	
	 * @param visionPlanDataVO  - Vision plan VO
	 * @return - Start date
	 */
	private Date getRateStartDate(VisionPlanDataVO visionPlanDataVO){
		
		LOGGER.debug("getRateStartDate() Start");
		Date rateStartDate = null;
		
			try {
				rateStartDate = dateFormat.parse(visionPlanDataVO.getStartDate());
			} catch (ParseException e) {
				LOGGER.debug("Error occurred while parsing Vision plan rate start date : " + visionPlanDataVO.getStartDate(), e);
			}
		LOGGER.debug("getRateStartDate() End");
		
		return rateStartDate;
	}
	
	
	/**
	 * Checks in db whether plan exist for given issuerPlanNumber. 
	 * 
	 * If plan exist and is in CERTIFIED status no action will be taken and new plan will be skipped. 
	 * If plan exist and is in NON-CERTIFIED status then new plan will be saved in DB. 
	 * 
	 * @param issuerPlanNumber - The plan number
	 * @param applicableYear - Plan applicanle year
	 * @param entityManager - Entity manager instance
	 * @return - Whether provided plan number is a new Plan required to be saved in DB
	 */
	private boolean isNewPlanToPersist(String issuerPlanNumber, int applicableYear, VisionPlanDataVO visionPlanDataVO, List<Plan> savedPlanList, 
			List<VisionPlanDataVO> failedPlanList, EntityManager entityManager) {
		
		LOGGER.info("isNewPlanToPersist() Start");
		boolean isNewPlan = true;//This flag will be false only plan exist in DB and is CERTIFIED. In all other cases this flag will be true.
		
		try {
			List<Plan> existingVisionPlans = iPlanRepository.findByIssuerPlanNumberStartingWithAndIsDeletedAndApplicableYearAndInsuranceType(issuerPlanNumber, Plan.IS_DELETED.N.toString(), applicableYear, Plan.PlanInsuranceType.VISION.name());
			
			if (CollectionUtils.isEmpty(existingVisionPlans)) {
				LOGGER.debug(issuerPlanNumber + " is a new Vision plan. No existing Vision plan found for " + issuerPlanNumber + " in database.");
				return isNewPlan;
			}
			LOGGER.debug("Existing Vision plan found for IssuerPlanNumber : " + issuerPlanNumber + " : No of Plans : " + existingVisionPlans.size());
			boolean certifiedPlanExist = false;
			
			for (Plan existingVisionPlan : existingVisionPlans) {
				//Soft Delete all existing plans. In case any of them is CERTIFIED, skip first such plan and soft delete others (normally there should be single such plan) 
				if (null == existingVisionPlan) {
					continue;
				}
				// Skipping CERTIFIED plans logic.
				if (Plan.PlanStatus.CERTIFIED.toString().equalsIgnoreCase(existingVisionPlan.getStatus()) && !certifiedPlanExist) {
	     			LOGGER.info("Vision Plan exist for [" + issuerPlanNumber + "] in Certified state. Processing future rates");
	     			isNewPlan = false;
	     			//Process future rates for certified plan.
	     			processFutureRates(existingVisionPlan, visionPlanDataVO, savedPlanList, failedPlanList, entityManager);
	     			certifiedPlanExist = true;
				}
				else {
		     		existingVisionPlan.setIsDeleted(Plan.IS_DELETED.Y.toString());
		     		
					if (null != entityManager && entityManager.isOpen()) {
						entityManager.merge(existingVisionPlan);
						LOGGER.info("Existing NON-CERTIFIED Vision Plan["+ existingVisionPlan.getIssuerPlanNumber() +"] has been soft deleted successfully.");
					}
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while searching existing Vision plan in database for issuerPlanNumber : " + issuerPlanNumber, ex);
		}
		finally {
			LOGGER.info("isNewPlanToPersist() End with isNewPlan: " + isNewPlan);
		}
		return isNewPlan;
	}

	/**
	 * This method will process future rates if present in excel template.
	 * 
	 * @param certifiedVisionPlan
	 * @param visionPlanDataVO
	 * @param failedVisionPlanVOList
	 * @param entityManager
	 */
	private void processFutureRates(Plan certifiedVisionPlan, VisionPlanDataVO visionPlanDataVO,  List<Plan> savedPlanList, 
			List<VisionPlanDataVO> failedVisionPlanVOList, EntityManager entityManager){
		LOGGER.info("processFutureRates() Start");
		boolean isPlanProcessedForFutureRates = false;
		
		try {
			Date newRateStartDate = dateFormat.parse(visionPlanDataVO.getStartDate());
			Date currentTimeStamp = new Date();
			LOGGER.info("newRateStartDate : " + newRateStartDate);
			//Set default message for Certified Plan.
			visionPlanDataVO.setErrorMessages("Certified Plan present in DB for : " + visionPlanDataVO.getPlanId());
			//Check if new rates are valid rates.
			if(!newRateStartDate.after(currentTimeStamp)){
				LOGGER.error("Invalid rates for plan : " + visionPlanDataVO.getPlanId());
				//Change message to 'Invalid future rates'.
				visionPlanDataVO.setErrorMessages(FUTURE_RATES_PROCESSING_FAILED_MSG + visionPlanDataVO.getPlanId());
				visionPlanDataVO.setNotValid(true);
			}else{
				//Find plans in db which having effective start date after newRateStartDate.
				List<PlanRate> existingPlanRates = iPlanRateRepository.findRatesInFutureForEffectiveStartDate(certifiedVisionPlan.getId(), newRateStartDate);
				LOGGER.debug("Existing plans having effective start date after newRateStartDate : " + (existingPlanRates == null ? "NULL" : existingPlanRates.size()));
				//Find plans in db which having effective start date before newRateStartDate and end date after new start date.
				List<PlanRate> existingPastDatePlanRates = iPlanRateRepository.findRatesBeforeEffectiveStartDate(certifiedVisionPlan.getId(), newRateStartDate);
				LOGGER.debug("Existing plans having effective start date before newRateStartDate and end date after newRateStartDate : " + (existingPastDatePlanRates == null ? "NULL" : existingPastDatePlanRates.size()));
				
				if(!CollectionUtils.isEmpty(existingPlanRates)){
					for(PlanRate planRate : existingPlanRates){
						planRate.setIsDeleted(SerffConstants.YES_ABBR);
						LOGGER.debug("Plan rate set as deleted : Rate Id : " + planRate.getId() + " : Plan : " + planRate.getPlan().getIssuerPlanNumber());
						planRate.setLastUpdateTimestamp(currentTimeStamp);
						planRate.setPlan(certifiedVisionPlan);
						planRate = (PlanRate) mergeEntityManager(entityManager, planRate);
					}
				}
				
				if(!CollectionUtils.isEmpty(existingPastDatePlanRates)){
					for(PlanRate planRate : existingPastDatePlanRates){
						planRate.setEffectiveEndDate(DateUtil.addToDate(newRateStartDate, 0, 0, 0, -1));
						LOGGER.debug("Changed end date of plan rate to " + planRate.getEffectiveEndDate() + 
								": Rate Id : " + planRate.getId() + " : Plan : " + planRate.getPlan().getIssuerPlanNumber());
						planRate.setLastUpdateTimestamp(currentTimeStamp);
						planRate.setPlan(certifiedVisionPlan);
						planRate = (PlanRate) mergeEntityManager(entityManager, planRate);
					}
				}
				//Insert new rates.
				savePlanVisionRates(certifiedVisionPlan, visionPlanDataVO, entityManager);
				//Future rates processing is completed. Add in saved Plan list.
				isPlanProcessedForFutureRates = true;
				savedPlanList.add(certifiedVisionPlan);
			}
		}
		catch(Exception e) {
			LOGGER.error("Error occurred while processing future rates for Vision plan : " + e.getMessage(), e);
		}
		finally {
			//Plan is not processed for future rates. Plan is certified and no change/invalid change in start date. Consider as failed Plan.
			if(!isPlanProcessedForFutureRates){
				failedVisionPlanVOList.add(visionPlanDataVO);
			}
		}
		LOGGER.info("processFutureRates() End");
	}
	
}
