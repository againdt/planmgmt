package com.serff.service.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Issuer;
import com.serff.repository.templates.IIssuersRepository;

@Service("serffIssuerService")
@Repository
@Transactional
public class IssuerServiceImpl implements IssuerService{

	// private static final Logger LOGGER = Logger.getLogger(IssuerServiceImpl.class);
	
	@Autowired private IIssuersRepository iIssuerRepository;
	
	@Override
	public Issuer saveIssuer(Issuer issuer) {
		return iIssuerRepository.save(issuer);
	}
	
	@Override
	public Issuer getIssuerByHiosID(String hiosIssuerID) {
		Issuer issuer=iIssuerRepository.getIssuerByHiosID(hiosIssuerID);
		if(issuer!=null){
			return issuer;
		}else{
			return null;			
		}
	}

}
