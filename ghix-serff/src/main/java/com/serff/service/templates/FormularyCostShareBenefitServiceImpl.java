/**
 * File 'FormularyCostShareServiceImpl.java' for defining persistence contract for 'FORMULARY_COST_SHARE' records.
 *
 * @author chalse_v
 * @since Jun 14, 2013
 *
 */
package com.serff.service.templates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.FormularyCostShareBenefit;
import com.serff.repository.templates.IFormularyCostShareBenefitRepository;

/**
 * Service implementor class 'FormularyCostShareBenefitServiceImpl' for defining persistence contract for 'FormularyCostShareBenefit' data.
 *
 * @author chalse_v
 * @version 1.0
 * @since Jun 14, 2013
 *
 */
@Repository
@Transactional
public class FormularyCostShareBenefitServiceImpl implements FormularyCostShareBenefitService {
	private static final Logger LOGGER = LoggerFactory.getLogger(FormularyCostShareBenefitServiceImpl.class);

	@Autowired(required = true)
	private IFormularyCostShareBenefitRepository formularyCostShareBenefitRepository;
	
	/**
	 * Default constructor.
	 */
	public FormularyCostShareBenefitServiceImpl() {
	}

	/**
	 * @see com.serff.service.templates.FormularyCostShareBenefitService#saveFormularyCostShareBenefit(com.getinsured.hix.model.FormularyCostShareBenefit)
	 */
	@Override
	public FormularyCostShareBenefit saveFormularyCostShareBenefit(FormularyCostShareBenefit formularyCostShareBenefit) {
		LOGGER.info("About to persist FormularyCostShareBenefit data.");
		return formularyCostShareBenefitRepository.save(formularyCostShareBenefit);
	}

}
