package com.serff.service.templates;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.IssuerVerificationStatus;
import com.getinsured.hix.model.PlanAME;
import com.getinsured.hix.model.PlanAMEBenefits;
import com.getinsured.hix.model.PlanAMEPremiumFactor;
import com.getinsured.hix.model.PlanRate;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.lookup.repository.ILookupRepository;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.ame.AMEAgeGroupExcelVO;
import com.getinsured.serffadapter.ame.AMEExcelVO;
import com.getinsured.serffadapter.ame.AMEPremiumFactorExcelVO;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.IPlanAMEPremiumFactorRepository;
import com.serff.repository.templates.IPlanRate;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.service.validation.AMEPlanDataValidator;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * Class is used to persist AME Plans to database.
 * 
 * @author Bhavin Parmar
 * @since August 14, 2014
 */
@Service("amePlanMgmtSerffService")
public class AMEPlanMgmtSerffServiceImpl implements AMEPlanMgmtSerffService {

	private static final Logger LOGGER = Logger.getLogger(AMEPlanMgmtSerffServiceImpl.class);
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private static final String PLAN_END_DATE = "31/12/2099";

	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private AMEPlanDataValidator amePlanDataValidator;
	@Autowired private IIssuersRepository iIssuerRepository;
	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private ILookupRepository iLookupRepository;
	@Autowired private IPlanAMEPremiumFactorRepository iPlanAMEPremiumFactorRepository;
	@Autowired private IPlanRate iPlanRateRepository;
	@Autowired private SerffUtils serffUtils;

	/**
	 * Method is used to populate and persist AME plans from AMEExcelVO to database.
	 */
	@Override
	public boolean populateAndPersistAMEData(Map<String, AMEExcelVO> amePlansMapData, AMEExcelVO ameExcelHeader,
			Map<String, AMEPremiumFactorExcelVO> amePremiumFactorMapData, SerffPlanMgmt trackingRecord, String planType, String defaultTenantIds)
			throws GIException {
		
		LOGGER.info("populateAndPersistAMEData() Start");
		boolean status = Boolean.FALSE;
		EntityManager entityManager = null;
		
		try {
			boolean isAMEFamilyRate = SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType);
			
			if (!validateParamsForNotNull(amePlansMapData, ameExcelHeader, amePremiumFactorMapData, trackingRecord, isAMEFamilyRate)) {
				return status;
			}
			
			entityManager = entityManagerFactory.createEntityManager(); // Get DB connection using Entity Manager Factory
			entityManager.getTransaction().begin(); // Begin Transaction
			
			status = populateAndPersistAMEPlans(amePlansMapData, ameExcelHeader, trackingRecord, planType, defaultTenantIds, entityManager);
			
			if (status && !isAMEFamilyRate) {
				status = populateAndPersistAMEPremimumFactor(amePremiumFactorMapData, trackingRecord, entityManager);
			}
		}
		catch(Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
			status = Boolean.FALSE;
			throw new GIException(ex);
		}
		finally {
			
			status = getTransactionCommit(status, entityManager);
			
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			LOGGER.info("populateAndPersistAMEData() End");
		}
		return status;
	}

	private boolean populateAndPersistAMEPlans(Map<String, AMEExcelVO> amePlansMapData, AMEExcelVO ameExcelHeader,
			SerffPlanMgmt trackingRecord, String planType, String defaultTenantIds, EntityManager entityManager)
			throws GIException {
		
		LOGGER.info("populateAndPersistAMEPlans() Start");
		boolean status = Boolean.FALSE;
		List<Plan> savedPlanList = new ArrayList<Plan>();
		List<AMEExcelVO> failedPlanList = new ArrayList<AMEExcelVO>();
		Map<String, Issuer> issuerMap = new HashMap<>();
		Map<String, Boolean> duplicatePlansMap = new HashMap<String, Boolean>(); // Map<String: IssuerPlanNumber, Boolean: Duplicate Or Not>
		
		try {
			String hiosPlanNumber = null;
			AMEExcelVO ameExcelVO = null;
			List<Tenant> tenantList = serffUtils.getTenantList(defaultTenantIds);

			for (Map.Entry<String, AMEExcelVO> ameEntry : amePlansMapData.entrySet()) {
				hiosPlanNumber = ameEntry.getKey();
				ameExcelVO = ameEntry.getValue();
				
				// Validating AME Plans & Rates data
				amePlanDataValidator.validateAMEExcelVO(hiosPlanNumber, ameExcelVO, planType);
				
				if (null != ameExcelVO && !ameExcelVO.isNotValid()) {
					// Persisting AME Plans & Rates data
					persistAMEPlans(hiosPlanNumber, ameExcelVO, ameExcelHeader, issuerMap, savedPlanList,
							failedPlanList, duplicatePlansMap, entityManager, planType, tenantList);
				}
				else if (null != ameExcelVO) {
					failedPlanList.add(ameExcelVO);
				}
			}
			generateErrorMessages(trackingRecord, failedPlanList, duplicatePlansMap);
			
			if (!CollectionUtils.isEmpty(savedPlanList)) {
				status = Boolean.TRUE;
				LOGGER.info("Number of AME Plans to be save: " + savedPlanList.size());
			}
		}
		catch(Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
//			status = Boolean.FALSE;
			throw new GIException(ex);
		}
		finally {
			savedPlanList.clear();
			savedPlanList = null;
			failedPlanList.clear();
			failedPlanList = null;
			issuerMap.clear();
			issuerMap = null;
			duplicatePlansMap.clear();
			duplicatePlansMap = null;
			LOGGER.info("populateAndPersistAMEPlans() End");
		}
		return status;
	}

	/**
	 * Method is used to generate Error messages from failed plan list and duplicate plan list.
	 */
	private void generateErrorMessages(SerffPlanMgmt trackingRecord, List<AMEExcelVO> failedPlanList, Map<String, Boolean> duplicatePlansMap) {

		LOGGER.info("generateErrorMessages() Start");

		try {

			if (CollectionUtils.isEmpty(failedPlanList) && CollectionUtils.isEmpty(duplicatePlansMap)) {
				return;
			}

			StringBuilder errorMessage = new StringBuilder();

			if (!CollectionUtils.isEmpty(failedPlanList)) {
				LOGGER.error("Number of Failed AME Plans: " + failedPlanList.size());

				for (AMEExcelVO AMEExcelVO : failedPlanList) {
					errorMessage.append(AMEExcelVO.getErrorMessages());
					errorMessage.append("\n");
				}
			}

			if (!CollectionUtils.isEmpty(duplicatePlansMap)) {
				List<String> duplicatePlansList = new ArrayList<String>();

				for (Map.Entry<String, Boolean> entryData : duplicatePlansMap.entrySet()) {

					if (null == entryData.getValue() || !entryData.getValue()) {
						continue;
					}
					duplicatePlansList.add(entryData.getKey());
				}

				if (!CollectionUtils.isEmpty(duplicatePlansList)) {
					errorMessage.append("Skipped duplicate IssuerPlanNumber(s) from Template: ");
					errorMessage.append(duplicatePlansList.toString());
				}
			}
			LOGGER.error(errorMessage.toString());
			trackingRecord.setPmResponseXml(errorMessage.toString());
		}
		finally {
			LOGGER.info("generateErrorMessages() End");
		}
	}

	/**
	 * Method is used to validate Null values for parameters.
	 */
	private boolean validateParamsForNotNull(Map<String, AMEExcelVO> amePlansMapData, AMEExcelVO ameExcelHeader,
			Map<String, AMEPremiumFactorExcelVO> amePremiumFactorMapData, SerffPlanMgmt trackingRecord, boolean isAMEFamilyRate) {

		boolean status = true;

		if (CollectionUtils.isEmpty(amePlansMapData)) {
			LOGGER.error("PlanList for Adding AME is empty !!");
			status = false;
		}

		if (null == ameExcelHeader) {
			LOGGER.error("Failed to get AME Benefit names.");
			status = false;
		}

		if (!isAMEFamilyRate && CollectionUtils.isEmpty(amePremiumFactorMapData)) {
			LOGGER.error("AME Premium Factor List is empty !!");
			status = false;
		}

		if (null == trackingRecord) {
			LOGGER.error("Tracking Record(SerffPlanMgmt) is null.");
			status = false;
		}
		return status;
	}

	/**
	 * Method is used to persist AME plans to database after validations.
	 */
	private void persistAMEPlans(String hiosPlanNumber, AMEExcelVO ameExcelVO, AMEExcelVO ameExcelHeader, Map<String, Issuer> issuerMap,
			List<Plan> savedPlanList, List<AMEExcelVO> failedPlanList, Map<String, Boolean> duplicatePlansMap,
			EntityManager entityManager, String planType, List<Tenant> tenantList) {
		
		LOGGER.info("persistAMEPlans() Start for " + hiosPlanNumber);
		
		try {
			
			Issuer existingIssuer = null;
			String hiosIssuerId = ameExcelVO.getCarrierHIOS();
			if(null != hiosIssuerId) {
				existingIssuer = issuerMap.get(hiosPlanNumber);
			}
			
			if(null == existingIssuer) {
				existingIssuer = iIssuerRepository.getIssuerByHiosID(hiosIssuerId);
				if(null != existingIssuer) {
					issuerMap.put(hiosIssuerId, existingIssuer);
				}
			}
			
			// Skipping Issuer if it does not exist.
			if (null != existingIssuer) {
				saveAMEPlanList(hiosPlanNumber, savedPlanList, failedPlanList, duplicatePlansMap, existingIssuer, ameExcelVO, ameExcelHeader, entityManager, planType, tenantList);
			}
			else {
				addToFailedList(ameExcelVO, failedPlanList, "Skipping Issuer["+ hiosIssuerId +"] account because it does not exist.");
			}
		}
		finally {
			LOGGER.info("persistAMEPlans() End");
		}
	}

	/**
	 * Method is used to add failed AME plan to Array List.
	 */
	private void addToFailedList(AMEExcelVO ameExcelVO, List<AMEExcelVO> failedPlanList, String message) {
		ameExcelVO.setNotValid(Boolean.TRUE);
		ameExcelVO.setErrorMessages(message);
		failedPlanList.add(ameExcelVO);
	}

	/**
	 * Method is used to save validated AME Plan List to DB.
	 */
	private void saveAMEPlanList(String hiosPlanNumber, List<Plan> savedPlanList, List<AMEExcelVO> failedPlanList,
			Map<String, Boolean> duplicatePlansMap, Issuer existingIssuer, AMEExcelVO ameExcelVO,
			AMEExcelVO ameExcelHeader, EntityManager entityManager, String planType, List<Tenant> tenantList) {
		
		LOGGER.info("saveAMEPlanList() Start for " + hiosPlanNumber);
		
		try {
			Plan savedPlan = null;
			PlanAME savedPlanAME = null;
			int applicableYear = 0;

			// Appending Off Exchange Variant in Issuer Plan Number
			String issuerPlanNumber = hiosPlanNumber + SerffConstants.OFF_EXCHANGE_VARIANT_SUFFIX;

			if (!CollectionUtils.isEmpty(duplicatePlansMap) && duplicatePlansMap.containsKey(issuerPlanNumber)) {
				duplicatePlansMap.put(issuerPlanNumber, true); // Override Key with flag value 
				return;
			}
			duplicatePlansMap.put(issuerPlanNumber, false);
			Plan certifiedAMEPlan = processExistingAMEPlan(failedPlanList, ameExcelVO, issuerPlanNumber, applicableYear, entityManager);

			if (null == certifiedAMEPlan) {

				savedPlan = savePlan(applicableYear, issuerPlanNumber, existingIssuer, ameExcelVO, entityManager);
				if (null == savedPlan) {
					LOGGER.error("Saved Plan object is empty.");
					return;
				}
				savedPlanAME = savePlanAME(savedPlan, ameExcelVO, entityManager);
				savePlanAMEBenefitsData(savedPlanAME, ameExcelHeader, ameExcelVO, entityManager);
				LOGGER.info("Saved AME Plan Number: " + issuerPlanNumber);
				
				if (SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType)) {
					// Persisting AME plan age group rates.
					savePlanAMEFamilyRates(savedPlan, ameExcelVO, entityManager);
				}
				else {
					// Persisting AME plan family rates.
					savePlanAMEAgeGroupRates(savedPlan, ameExcelVO, planType, entityManager);
				}
				savedPlanList.add(savedPlan);
				//Set tenant
				if(null != tenantList) {
					serffUtils.saveTenantPlan(savedPlan, tenantList, entityManager);
				}
			}
			else {
				LOGGER.info("Plan status: " + certifiedAMEPlan.getStatus());
				processFutureRates(certifiedAMEPlan, ameExcelVO, planType, failedPlanList, entityManager);
				savedPlanList.add(certifiedAMEPlan);
			}
			
			//Commented as not needed.................
			/*if(!planSaved) {
				addToFailedList(ameExcelVO, failedPlanList, "Failed to persist AME Plan to DB - " + hiosPlanNumber);
			}*/
		}
		finally {
			LOGGER.info("saveAMEPlanList() End");
		}
	}

	/**
	 * Method is used to process existing AME Plans before persistence.
	 * Rule: Certified AME plan will not update.
	 */
	private Plan processExistingAMEPlan(List<AMEExcelVO> failedPlanList, AMEExcelVO ameExcelVO, String issuerPlanNumber, int applicableYear, EntityManager entityManager) {

		LOGGER.debug("processExistingAMEPlan() Start");
		Plan certifiedAMEPlan = null;

		try {
			List<Plan> existingAMEPlans = iPlanRepository.findByIssuerPlanNumberStartingWithAndIsDeletedAndApplicableYearAndInsuranceType(issuerPlanNumber, Plan.IS_DELETED.N.toString(), applicableYear, Plan.PlanInsuranceType.AME.name());
			
			if (CollectionUtils.isEmpty(existingAMEPlans)) {
				return certifiedAMEPlan;
			}
			
			boolean certifiedPlanExist = false;
			for (Plan existingAMEPlan : existingAMEPlans) {
				LOGGER.debug("AME Plan exists for given plan number " + issuerPlanNumber);
				//Soft Delete all existing plans. In case any of them is CERTIFIED, skip first such plan and soft delete others (normally there should be single such plan) 
				if (null == existingAMEPlan) {
					continue;
				}
				// Skipping CERTIFIED plans logic.
				if (Plan.PlanStatus.CERTIFIED.toString().equalsIgnoreCase(existingAMEPlan.getStatus()) && !certifiedPlanExist) {
					certifiedAMEPlan = existingAMEPlan;
					ameExcelVO.setNotValid(Boolean.TRUE);
					ameExcelVO.setErrorMessages("Skipping Certified AME Plan["+ issuerPlanNumber +"].");
					failedPlanList.add(ameExcelVO);
					certifiedPlanExist = Boolean.TRUE;
				}
				else {
					deleteExistingPlan(existingAMEPlan, entityManager);
				}
			}
		}
		finally {
			LOGGER.debug("processExistingAMEPlan() End");
		}
		return certifiedAMEPlan;
	}

	/**
	 * Method is used to soft delete existing AME Plans [Non-Certified].
	 */
	private void deleteExistingPlan(Plan existingAMEPlan, EntityManager entityManager) {
		
		LOGGER.debug("deleteExistingPlan() Start");
		
		try {
			// If any plan does not CERTIFIED then mark plans as soft delete.
			existingAMEPlan.setIsDeleted(Plan.IS_DELETED.Y.toString());
			
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.merge(existingAMEPlan);
				LOGGER.info("Plan["+ existingAMEPlan.getIssuerPlanNumber() +"] bean has been soft deleted Successfully for State["+ existingAMEPlan.getState() +"].");
			}
		}
		finally {
			LOGGER.debug("deleteExistingPlan() End");
		}
	}

	/**
	 * Method is used to save Plan table.
	 */
	private Plan savePlan(int applicableYear, String issuerPlanNumber, Issuer existingIssuer, AMEExcelVO ameExcelVO, EntityManager entityManager) {
		
		LOGGER.debug("Persisting Plan Object Start : " + ameExcelVO.getPlanName() + " Type:" + ameExcelVO.getPlanType());
		Plan plan = null;
		
		try {
			if (StringUtils.isNotBlank(issuerPlanNumber)) {
				plan = new Plan();
				plan.setIssuerVerificationStatus(IssuerVerificationStatus.PENDING.toString());
				plan.setIssuer(existingIssuer);
				plan.setInsuranceType(Plan.PlanInsuranceType.AME.name());
				plan.setStatus(Plan.PlanStatus.LOADED.toString());
				plan.setIssuerPlanNumber(issuerPlanNumber);
				plan.setName(ameExcelVO.getPlanName());
				plan.setNetworkType(ameExcelVO.getPlanType());
				plan.setBrochure(SerffUtils.checkAndcorrectURL(ameExcelVO.getPlanBrochure()));
				plan.setHsa(SerffConstants.NO);
				plan.setState(ameExcelVO.getState());
				plan.setMarket(Plan.PlanMarket.INDIVIDUAL.toString());
				plan.setHiosProductId(issuerPlanNumber.substring(0, SerffConstants.LEN_HIOS_PRODUCT_ID));
				plan.setExchangeType(Plan.EXCHANGE_TYPE.OFF.toString());
				plan.setIsPUF(Plan.IS_PUF.N);
				plan.setIsDeleted(Plan.IS_DELETED.N.toString());
				plan.setApplicableYear(applicableYear);
				plan = (Plan) mergeEntityManager(entityManager, plan);
			}
		}catch(Exception e){
			LOGGER.error("Error occured while parsing dates" , e);
			return plan;
		}
		finally {
			LOGGER.debug("Persisting Plan Object Done");
		}
		return plan;
	}

	/**
	 * Method is used to save PlanAME table.
	 */
	private PlanAME savePlanAME(Plan savedPlan, AMEExcelVO ameExcelVO, EntityManager entityManager) {
		
		LOGGER.debug("Persisting PlanAME Object Start : Benefit: " + ameExcelVO.getMaxBenefitValue() + " DDuct: " + ameExcelVO.getDeductibleValue());
		PlanAME planAME = new PlanAME();
		
		try {
			planAME.setBenefitMaxAttribute(ameExcelVO.getMaxBenefitDesc());
			
			if (!SerffConstants.NOT_APPLICABLE.equalsIgnoreCase(ameExcelVO.getMaxBenefitValue())) {
				planAME.setBenefitMaxValue(Integer.parseInt(ameExcelVO.getMaxBenefitValue()));
			}
			planAME.setDeductibleAttribute(ameExcelVO.getDeductibleDesc());
			
			if (!SerffConstants.NOT_APPLICABLE.equalsIgnoreCase(ameExcelVO.getDeductibleValue())) {
				planAME.setDeductibleValue(Integer.parseInt(ameExcelVO.getDeductibleValue()));
			}
			planAME.setExclusionURL(SerffUtils.checkAndcorrectURL(ameExcelVO.getExclusionsAndLimitations()));
			planAME.setCreatedBy(SerffConstants.DEFAULT_USER_ID);
			planAME.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);
			planAME.setPlan(savedPlan);
			planAME = (PlanAME) mergeEntityManager(entityManager, planAME);
		}
		finally {
			LOGGER.debug("Persisting PlanAME Object Done");
		}
		return planAME;
	}

	/**
	 * Method is used to save List PlanAMEBenefit data using AME Excel Header.
	 */
	private void savePlanAMEBenefitsData(PlanAME savedPlanAME, AMEExcelVO ameExcelHeader, AMEExcelVO ameExcelVO, EntityManager entityManager) {
		
		LOGGER.debug("Persisting All PlanAMEBenefits Start");
		try {
			savePlanAMEBenefit(savedPlanAME, ameExcelHeader.getSupplementalAccident(), ameExcelVO.getSupplementalAccident(), entityManager);
			savePlanAMEBenefit(savedPlanAME, ameExcelHeader.getEmergencyTreatment(), ameExcelVO.getEmergencyTreatment(), entityManager);
			savePlanAMEBenefit(savedPlanAME, ameExcelHeader.getFollowUpTreatment(), ameExcelVO.getFollowUpTreatment(), entityManager);
			savePlanAMEBenefit(savedPlanAME, ameExcelHeader.getAmbulance(), ameExcelVO.getAmbulance(), entityManager);
			savePlanAMEBenefit(savedPlanAME, ameExcelHeader.getInitialHospitalization(), ameExcelVO.getInitialHospitalization(), entityManager);
			savePlanAMEBenefit(savedPlanAME, ameExcelHeader.getHospitalStay(), ameExcelVO.getHospitalStay(), entityManager);
			savePlanAMEBenefit(savedPlanAME, ameExcelHeader.getSurgery(), ameExcelVO.getSurgery(), entityManager);
			savePlanAMEBenefit(savedPlanAME, ameExcelHeader.getAccidentalDeath(), ameExcelVO.getAccidentalDeath(), entityManager);
			savePlanAMEBenefit(savedPlanAME, ameExcelHeader.getAccidentalDismemberment(), ameExcelVO.getAccidentalDismemberment(), entityManager);
			savePlanAMEBenefit(savedPlanAME, ameExcelHeader.getOtherBenefits(), ameExcelVO.getOtherBenefits(), entityManager);
		}
		finally {
			LOGGER.debug("Persisting All PlanAMEBenefits Done");
		}
	}

	/**
	 * Method is used to save PlanAMEBenefits table.
	 */
	private void savePlanAMEBenefit(PlanAME savedPlanAME, String benefitName, String benefitValue, EntityManager entityManager) {

		LOGGER.debug("Persisting PlanAMEBenefits Object Name: " +  benefitName + " Value:" + benefitValue);
		PlanAMEBenefits planAMEBenefits = new PlanAMEBenefits();
		
		try {
			planAMEBenefits.setBenefitName(benefitName);
			
			if (!SerffConstants.NOT_APPLICABLE.equalsIgnoreCase(benefitValue)) {
				planAMEBenefits.setBenefitValue(benefitValue);
			}
			planAMEBenefits.setCreatedBy(savedPlanAME.getCreatedBy());
			planAMEBenefits.setLastUpdatedBy(savedPlanAME.getLastUpdatedBy());
			planAMEBenefits.setPlanAME(savedPlanAME);
			mergeEntityManager(entityManager, planAMEBenefits);
		}
		finally {
			LOGGER.debug("Persisting PlanAMEBenefits Object Done");
		}
	}

	/**
	 * Method is used to save List PlanAMERate data using AME plan age group rates.
	 */
	private void savePlanAMEAgeGroupRates(Plan savedPlan, AMEExcelVO ameExcelVO, String planType, EntityManager entityManager) {
		
		LOGGER.debug("Persisting savePlanAMEAgeGroupRates Start");
		boolean isGenderBasedRate = SerffConstants.PLAN_TYPE.AME_GENDER.name().equalsIgnoreCase(planType);
		try {
			Integer minAge, maxAge;
			
			for (AMEAgeGroupExcelVO ameAgeGroupVO : ameExcelVO.getAmeAgeGroupExcelVO()) {

				if(null == ameAgeGroupVO) {
					continue;
				}
				minAge = Integer.valueOf(ameAgeGroupVO.getMinAge());
				maxAge = Integer.valueOf(ameAgeGroupVO.getMaxAge());
				LOGGER.info("Persisting Rate for " + minAge + " to " + ameAgeGroupVO.getMaxAge());
				if(isGenderBasedRate) {
					savePlanAMERate(savedPlan, minAge,
							maxAge, ameAgeGroupVO.getApplicant(), getRateStartDate(ameExcelVO), PlanRate.Relation.APPLICANT, SerffConstants.MALE, entityManager);
					savePlanAMERate(savedPlan, minAge,
							maxAge, ameAgeGroupVO.getSpouse(), getRateStartDate(ameExcelVO), PlanRate.Relation.SPOUSE, SerffConstants.MALE, entityManager);
					savePlanAMERate(savedPlan, minAge,
							maxAge, ameAgeGroupVO.getApplicantFemale(), getRateStartDate(ameExcelVO), PlanRate.Relation.APPLICANT, SerffConstants.FEMALE, entityManager);
					savePlanAMERate(savedPlan, minAge,
							maxAge, ameAgeGroupVO.getSpouseFemale(), getRateStartDate(ameExcelVO), PlanRate.Relation.SPOUSE, SerffConstants.FEMALE, entityManager);
					
				} else {
					savePlanAMERate(savedPlan, minAge,
							maxAge, ameAgeGroupVO.getApplicant(), getRateStartDate(ameExcelVO), PlanRate.Relation.APPLICANT, null, entityManager);
					savePlanAMERate(savedPlan, minAge,
							maxAge, ameAgeGroupVO.getSpouse(), getRateStartDate(ameExcelVO), PlanRate.Relation.SPOUSE, null, entityManager);
				}
				
				if (StringUtils.isNotBlank(ameAgeGroupVO.getChild())) {
					savePlanAMERate(savedPlan, minAge,
							maxAge, ameAgeGroupVO.getChild(), getRateStartDate(ameExcelVO), PlanRate.Relation.CHILD, null, entityManager);
				}
			}
		}
		finally {
			LOGGER.debug("Persisting savePlanAMEAgeGroupRates Done");
		}
	}

	/**
	 * Method is used to save List PlanAMERate data using AME plan family rates.
	 */
	private void savePlanAMEFamilyRates(Plan savedPlan, AMEExcelVO ameExcelVO, EntityManager entityManager) {
		
		LOGGER.debug("Persisting PlanAMEFamilyRate Start");
		
		try {
			List<LookupValue> lookupValueCodeList = iLookupRepository.getLookupValueList(SerffConstants.SERFF_FAMILY_OPTION_VALUE);
			
			if (CollectionUtils.isEmpty(lookupValueCodeList)) {
				return;
			}
			
			int lookupValueCode;
			String rate = null;
			
			for (LookupValue lookupValue : lookupValueCodeList) {

				if(null == lookupValue) {
					continue;
				}
				lookupValueCode = Integer.valueOf(lookupValue.getLookupValueCode().trim());
				LOGGER.debug("Persisting Rate for " + lookupValueCode);
				
				if(lookupValueCode > 0) {
					rate = getRateFor(lookupValueCode, ameExcelVO);
					// For Family base rates always send relation as NULL.
					savePlanAMERate(savedPlan, lookupValueCode, lookupValueCode, rate, getRateStartDate(ameExcelVO), null, null, entityManager);
				}
			}
		}
		finally {
			LOGGER.debug("Persisting PlanAMEFamilyRate Done");
		}
	}

	/**
	 * Method is used to identified Rate for lookup value.
	 */
	private String getRateFor(int lookupValueCode, AMEExcelVO ameExcelVO) {
		String rate = null;
		switch(lookupValueCode) {
			case SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_CODE: 
				rate = ameExcelVO.getIndividualRate();
				break;
			case SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_CODE: 
				rate = ameExcelVO.getCoupleRate();
				break;
			case SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_ONE_DEPENDENT_CODE: 
				rate = ameExcelVO.getOneAdultOneDependentRate();
				break;
			case SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_TWO_DEPENDENT_CODE: 
				rate = ameExcelVO.getOneAdultTwoDependentRate();
				break;
			case SerffConstants.FAMILY_OPTION_PRIMARY_ENROLLEE_MANY_DEPENDENT_CODE: 
				rate = ameExcelVO.getOneAdultThreePlusDependentRate();
				break;
			case SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_ONE_DEPENDENT_CODE: 
				rate = ameExcelVO.getTwoAdultOneDependentRate();
				break;
			case SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_TWO_DEPENDENT_CODE: 
				rate = ameExcelVO.getTwoAdultTwoDependentRate();
				break;
			case SerffConstants.FAMILY_OPTION_COUPLE_ENROLLEE_MANY_DEPENDENT_CODE: 
				rate = ameExcelVO.getTwoAdultThreePlusDependentRate();
				break;
			default: 
				break;
		}
		return rate;
	}

	/**
	 * Method is used to save List PlanAMERate data using AME Excel Header.
	 */
	private void savePlanAMERate(Plan savedPlan, int minAge, int maxAge, String rate, Date startDate, PlanRate.Relation relation, String gender, EntityManager entityManager) {
		
		LOGGER.debug("Persisting PlanAMERate Object for rate: " + rate);
		try {
			if(null != rate) {
				PlanRate planRate = new PlanRate();
				planRate.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);
				planRate.setMaxAge(maxAge);
				planRate.setMinAge(minAge);
				planRate.setRate(Float.parseFloat(rate.trim().replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
				
				if (null != relation) {
					planRate.setRelation(relation.name());
					planRate.setRateOption(PlanRate.RATE_OPTION.A);
				}
				else {
					planRate.setRateOption(PlanRate.RATE_OPTION.F);
				}
				
				if(null != gender) {
					planRate.setGender(gender);
				}
				
				planRate.setIsDeleted(PlanRate.IS_DELETED.N.toString());
				planRate.setEffectiveStartDate(startDate);
				planRate.setEffectiveEndDate(dateFormat.parse(PLAN_END_DATE));				
				//planRate.setTobacco(tobacco);
				planRate.setPlan(savedPlan);
				mergeEntityManager(entityManager, planRate);
			}
		} catch (ParseException e) {
			LOGGER.debug("Exception occurred while parsing end date.");
		}
		finally {
			LOGGER.debug("Persisting PlanAMERate Object Done");
		}
	}

	/**
	 * Method is used to merge AME Table to Entity Manager.
	 */
	private Object mergeEntityManager(EntityManager entityManager, Object saveObject) {
		
		Object savedObject = null;
		
		if (null != entityManager && entityManager.isOpen()) {
			savedObject = entityManager.merge(saveObject);
			LOGGER.debug(saveObject.getClass() + " bean has been Persisted Successfully.");
		}
		return savedObject;
	}

	/**
	 * Method is used to Commit/Rollback transaction depends on Status argument.
	 */
	private boolean getTransactionCommit(boolean status, EntityManager entityManager) throws GIException {
		
		LOGGER.debug("getTransactionCommit() Start");
		boolean commitStatus = status;
		
		try {
			
			if (null != entityManager && entityManager.isOpen()) {
				
				if (status) {
					entityManager.getTransaction().commit();
					LOGGER.info("Transaction Commit has been Commited");
				}
				else {
					entityManager.getTransaction().rollback();
					LOGGER.info("Transaction Commit has been Rollback");
				}
			}
		}
		catch (Exception ex) {
			commitStatus = Boolean.FALSE;
			LOGGER.error("Error occured while commiting Transaction " + commitStatus, ex);
			throw new GIException(ex);
		}
		finally {
			LOGGER.debug("getTransactionCommit() End");
		}
		return commitStatus;
	}

	/**
	 * Method is used to populate and persist AME Premimum Factor from AMEPremiumFactorExcelVO to database.
	 */
	private boolean populateAndPersistAMEPremimumFactor(Map<String, AMEPremiumFactorExcelVO> amePremiumFactorMapData, 
			SerffPlanMgmt trackingRecord, EntityManager entityManager) throws GIException {
		
		LOGGER.info("populateAndPersistAMEPremimumFactor() Start");
		boolean status = Boolean.FALSE;
		List<PlanAMEPremiumFactor> savedPremiumFactorList = new ArrayList<PlanAMEPremiumFactor>();
		List<AMEPremiumFactorExcelVO> failedPremiumFactorList = new ArrayList<AMEPremiumFactorExcelVO>();
		
		try {
			AMEPremiumFactorExcelVO amePremiumFactorVO = null;
			
			for (Map.Entry<String, AMEPremiumFactorExcelVO> amePremiumEntry : amePremiumFactorMapData.entrySet()) {
				
				amePremiumFactorVO = amePremiumEntry.getValue();
				
				if (null == amePremiumFactorVO) {
					continue;
				}
				
				// Validates AME Excel data.
				if (amePlanDataValidator.validateAMEPremiumFactorExcelVO(amePremiumFactorVO)) {
					persistAMEPremiumFactor(savedPremiumFactorList, amePremiumFactorVO, entityManager);
				}
				else {
					failedPremiumFactorList.add(amePremiumFactorVO);
				}
			}
			
			if (!CollectionUtils.isEmpty(failedPremiumFactorList)) {
				LOGGER.error("Number of Failed AME Premium Factors: " + failedPremiumFactorList.size());
				StringBuilder errorMessage = new StringBuilder();
				
				for (AMEPremiumFactorExcelVO premiumFactorVO : failedPremiumFactorList) {
					errorMessage.append(premiumFactorVO.getErrorMessages());
					errorMessage.append("\n");
				}
				LOGGER.error(errorMessage.toString());
				
				if (StringUtils.isNotBlank(trackingRecord.getPmResponseXml())) {
					trackingRecord.setPmResponseXml(trackingRecord.getPmResponseXml() + errorMessage.toString());
				}
				else {
					trackingRecord.setPmResponseXml(errorMessage.toString());
				}
			}
			
			if (!CollectionUtils.isEmpty(savedPremiumFactorList)) {
				status = Boolean.TRUE;
				LOGGER.info("Number of AME Premium Factors to be save: " + savedPremiumFactorList.size());
			}
		}
		catch(Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
//			status = Boolean.FALSE;
			throw new GIException(ex);
		}
		finally {
			savedPremiumFactorList.clear();
			savedPremiumFactorList = null;
			failedPremiumFactorList.clear();
			failedPremiumFactorList = null;
			LOGGER.info("populateAndPersistAMEPremimumFactor() End");
		}
		return status;
	}

	/**
	 * Method is used to persist AME Premium Factor to database after validations.
	 */
	private void persistAMEPremiumFactor(List<PlanAMEPremiumFactor> savedPremiumFactorList, AMEPremiumFactorExcelVO amePremiumFactorVO, EntityManager entityManager) {
		
		LOGGER.debug("Persisting AMEPremiumFactor Object Start for CarrierHIOS: " + amePremiumFactorVO.getCarrierHIOS() + " State:" + amePremiumFactorVO.getState());
		PlanAMEPremiumFactor amePremiumFactor = null;
		
		try {
			
			PlanAMEPremiumFactor existingPremiumFactor = iPlanAMEPremiumFactorRepository.getIssuerHiosIdAndState(amePremiumFactorVO.getCarrierHIOS(), amePremiumFactorVO.getState());
			if (null != existingPremiumFactor) {
				existingPremiumFactor.setIsDeleted(PlanAMEPremiumFactor.IS_DELETED.Y.toString());
				
				amePremiumFactor = (PlanAMEPremiumFactor) mergeEntityManager(entityManager, existingPremiumFactor);
				LOGGER.info("AMEPremiumFactor - HIOS Issuer Id ["+ existingPremiumFactor.getIssuerHiosId() +"] bean has been soft deleted Successfully for State["+ existingPremiumFactor.getState() +"].");
			}
			amePremiumFactor = new PlanAMEPremiumFactor();
			amePremiumFactor.setIssuerHiosId(amePremiumFactorVO.getCarrierHIOS());
			amePremiumFactor.setState(amePremiumFactorVO.getState());
			amePremiumFactor.setAnnualFactor(Double.valueOf(amePremiumFactorVO.getAnnual()));
			amePremiumFactor.setSemiAnnualFactor(Double.valueOf(amePremiumFactorVO.getSemiAnnual()));
			amePremiumFactor.setQuarterlyFactor(Double.valueOf(amePremiumFactorVO.getQuarterly()));
			amePremiumFactor.setMonthlyFactor(Double.valueOf(amePremiumFactorVO.getMonthly()));
			amePremiumFactor.setIsDeleted(PlanAMEPremiumFactor.IS_DELETED.N.toString());
			// TO-DO modify logic of Created By & Last Updated By.
			amePremiumFactor.setCreatedBy(SerffConstants.DEFAULT_USER_ID);
			amePremiumFactor.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);
			amePremiumFactor = (PlanAMEPremiumFactor) mergeEntityManager(entityManager, amePremiumFactor);
			savedPremiumFactorList.add(amePremiumFactor);
		}
		finally {
			LOGGER.debug("Persisting AMEPremiumFactor Object Done");
		}
	}
	
	/**
	 * This method will process future rates if present in excel template.
	 * 
	 * @param existingAMEPlan
	 * @param ameExcelVO
	 * @param isAMEFamilyRate
	 * @param entityManager
	 */
	private void processFutureRates(Plan existingAMEPlan, AMEExcelVO ameExcelVO, String planType, List<AMEExcelVO> failedPlanList, EntityManager entityManager){
		LOGGER.info("processFutureRates() started");
		
		try {
			Date newRateStartDate = dateFormat.parse(ameExcelVO.getStartDate());
			Date currentTimeStamp = new Date();
			LOGGER.info("processFutureRates() : newRateStartDate : " + newRateStartDate);
			//Check if new rates are valid rates.
			if(!newRateStartDate.after(currentTimeStamp)){
				LOGGER.error("Invalid rates for plan : " + existingAMEPlan.getIssuerPlanNumber());
				addToFailedList(ameExcelVO, failedPlanList, "Invalid rates present for plnaid : " + existingAMEPlan.getIssuerPlanNumber());
			}else{
				//Find plans in db which having effective start date after newRateStartDate.
				List<PlanRate> existingPlanRates = iPlanRateRepository.findRatesInFutureForEffectiveStartDate(existingAMEPlan.getId(), newRateStartDate);
				LOGGER.debug("Existing plans having effective start date after newRateStartDate is Empty: " + CollectionUtils.isEmpty(existingPlanRates));
				//Find plans in db which having effective start date before newRateStartDate and end date after new start date.
				List<PlanRate> existingPastDatePlanRates = iPlanRateRepository.findRatesBeforeEffectiveStartDate(existingAMEPlan.getId(), newRateStartDate);
				LOGGER.debug("Existing plans having effective start date before newRateStartDate and end date after newRateStartDate : " + CollectionUtils.isEmpty(existingPastDatePlanRates));
				
				if(!CollectionUtils.isEmpty(existingPlanRates)){
					for(PlanRate planRate : existingPlanRates){
						planRate.setIsDeleted(SerffConstants.YES_ABBR);
						LOGGER.debug("Plan rate set as deleted : Rate Id : " + planRate.getId() + " : Plan : " + planRate.getPlan().getIssuerPlanNumber());
						planRate.setLastUpdateTimestamp(currentTimeStamp);
						planRate.setPlan(existingAMEPlan);
						planRate = (PlanRate) mergeEntityManager(entityManager, planRate);
					}
				}
				
				if(!CollectionUtils.isEmpty(existingPastDatePlanRates)){
					for(PlanRate planRate : existingPastDatePlanRates){
						planRate.setEffectiveEndDate(DateUtil.addToDate(newRateStartDate, 0, 0, 0, -1));
						LOGGER.debug("Changed end date of plan rate to " + planRate.getEffectiveEndDate() + 
								": Rate Id : " + planRate.getId() + " : Plan : " + planRate.getPlan().getIssuerPlanNumber());
						planRate.setLastUpdateTimestamp(currentTimeStamp);
						planRate.setPlan(existingAMEPlan);
						planRate = (PlanRate) mergeEntityManager(entityManager, planRate);
					}
				}
				
				//Insert new rates.
				if(SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType)){
					savePlanAMEFamilyRates(existingAMEPlan, ameExcelVO, entityManager);
				}else{
					savePlanAMEAgeGroupRates(existingAMEPlan, ameExcelVO, planType, entityManager);
				}
				
			}
			
		} catch (ParseException e) {
			LOGGER.debug("Error occurred while processing future rates : " + e.getMessage(), e);
		}
		LOGGER.info("processFutureRates() end");
	}
	
	/**
	 * Returns effective start date for rate.
	 * 
	 * @param ameExcelVO
	 * @return
	 */
	private Date getRateStartDate(AMEExcelVO ameExcelVO){
		
		Date rateStartDate = null;
		
			try {
				rateStartDate = dateFormat.parse(ameExcelVO.getStartDate());
			} catch (ParseException e) {
				LOGGER.debug("Error occurred while parsing rate start date : " + ameExcelVO.getStartDate(), e);
			}
		
		return rateStartDate;
	}
	
	
}
