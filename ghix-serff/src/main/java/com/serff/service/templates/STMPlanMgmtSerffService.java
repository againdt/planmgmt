package com.serff.service.templates;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.stm.STMExcelVO;

/**
 * Interface is used to persist STM Plans to database using STMPlanMgmtSerffServiceImpl class.
 * 
 * @author Bhavin Parmar
 * @since May 3, 2014
 */
public interface STMPlanMgmtSerffService {

	boolean populateAndPersistSTMPlans(List<STMExcelVO> stmList, SerffPlanMgmt trackingRecord, EntityManager hccEntityManager, Map<String, Plan> hccPlanMap, String defaultTenantIds) throws GIException;
}
