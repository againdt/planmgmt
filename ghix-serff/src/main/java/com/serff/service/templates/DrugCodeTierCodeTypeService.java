/**
 * File 'DrugCodeTierCodeTypeService' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:34:25 AM
 */
package com.serff.service.templates;

import com.getinsured.hix.model.DrugCodeTierCodeType;

/**
 * Class 'DrugCodeTierCodeTypeService' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:34:25 AM
 *
 */
public interface DrugCodeTierCodeTypeService {
	DrugCodeTierCodeType saveDrugCodeTierCodeType(DrugCodeTierCodeType drugCodeTierCodeType);
}
