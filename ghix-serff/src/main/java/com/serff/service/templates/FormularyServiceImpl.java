/**
 * File 'FormularyServiceImpl' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 18, 2013 6:30:28 PM
 */
package com.serff.service.templates;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.dto.plandisplay.DrugPropertiesRequestDTO;
import com.getinsured.hix.dto.plandisplay.DrugPropertiesResponseDTO;
import com.getinsured.hix.dto.plandisplay.DrugSearchDTO;
import com.getinsured.hix.model.Formulary;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.google.gson.Gson;
import com.serff.repository.templates.IFormularyRepository;
import com.serff.util.ExcelReportGenerator;
import com.serff.util.SerffConstants;

/**
 * Service implementor 'FormularyServiceImpl' to implement service methods of 'FormularyService'.
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 18, 2013 6:30:28 PM
 *
 */
@Service("formularyService")
@Repository
@Transactional
public class FormularyServiceImpl implements FormularyService {
	private static final Logger LOGGER = LoggerFactory.getLogger(FormularyServiceImpl.class);

	@Autowired
	private IFormularyRepository iSerffFormularyRepository;
	@PersistenceUnit private EntityManagerFactory emf;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private Gson platformGson;
	ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * Constructor.
	 */
	public FormularyServiceImpl() {
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.FormularyService#saveFormulary(com.getinsured.hix.model.Formulary)
	 */
	@Override
	public Formulary saveFormulary(Formulary formulary) {
		return iSerffFormularyRepository.save(formulary);
	}
	
	@Override
	public HSSFWorkbook getFormularyDrugListDiffReport(List<Long> oldDrugListIds, List<Long> newDrugListIds) {
		HSSFWorkbook workbook = null;
		if(!CollectionUtils.isEmpty(oldDrugListIds) && !CollectionUtils.isEmpty(newDrugListIds) && newDrugListIds.size() == oldDrugListIds.size()) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Getting Formulary Drug List difference report for Old: " + oldDrugListIds + " New : " + newDrugListIds);
			}
			EntityManager entityManager = null;
			try {
				entityManager = emf.createEntityManager();
				String buildCountQuery = buildFormularyDrugListCountQuery();
				Query countQuery = entityManager.createNativeQuery(buildCountQuery);
				countQuery.setParameter("drug_list_ids", ListUtils.union(oldDrugListIds, newDrugListIds));
				Map<Long, Long> drugsCountMap = new HashMap<Long, Long>();
				List<?> rsCountList = countQuery.getResultList();
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Got result for Drug count query : " + rsCountList.size());
				}
				
				Iterator<?> itr = rsCountList.iterator();
				while (itr.hasNext()) {
					Object[] objArray = (Object[]) itr.next();
					if(null != objArray && objArray.length > 1) {
						drugsCountMap.put(Long.parseLong(objArray[0].toString()), Long.parseLong(objArray[1].toString()));
					}
				}
					
				workbook = new HSSFWorkbook();
				String buildquery = buildFormularyDrugListDifferenceQuery();
				for (int i = 0; i < newDrugListIds.size(); i++) {
					Query query = entityManager.createNativeQuery(buildquery);
					query.setParameter("old_drug_list_id", oldDrugListIds.get(i));
					query.setParameter("new_drug_list_id", newDrugListIds.get(i));
					List<?> rsList = query.getResultList();
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("Formulary Drug DataList Difference is fetched from db result size is " + rsList.size());
					}
					Map<String, List<String[]>> diffRecordsMap = new HashMap<String, List<String[]>>();
					List<String> rxcuiList = extractRxcuiForDifferenceReport(rsList); 
					Map<String, DrugSearchDTO> drugProperties = getDrugProperties(rxcuiList);
					processDrugListRecordsForDifferenceReport(rsList, diffRecordsMap, drugProperties);
					LOGGER.debug("Generating Drug Difference report");
					ExcelReportGenerator.createFormularyDrugListDiffReport(workbook, diffRecordsMap, 
							drugsCountMap.get(oldDrugListIds.get(i)), 
							drugsCountMap.get(newDrugListIds.get(i)), i+1);
				}
			} catch (Exception e) {
				LOGGER.error("Failed to create Formulary DrugList Difference data. Exception: ", e);
			}
			finally
			{
				if(entityManager != null && entityManager.isOpen())
				{
					try {
						entityManager.close();
					}
					catch(IllegalStateException ex)
					{
						LOGGER.error("EntityManager is managed by application server", ex);
					}
				}
			}
		} else {
			LOGGER.error("Failed to get Formulary DrugList difference data as params are invalid. oldDrugListIds : " + oldDrugListIds + " newDrugListIds : "
					+ newDrugListIds );
		}
		return workbook;	
	}	
	
	private void processDrugListRecordsForDifferenceReport(List<?> rsList, Map<String, List<String[]>> diffRecordsMap, Map<String, DrugSearchDTO> drugProperties) {
		if(null != rsList && null != diffRecordsMap) {
			Iterator<?> rsIterator = rsList.iterator();
			List<String[]> addList = new ArrayList<String[]>(); 
			List<String[]> removeList = new ArrayList<String[]>(); 
			List<String[]> modifiedList = new ArrayList<String[]>();
			diffRecordsMap.put(SerffConstants.DRUG_LIST_REC_ADDED, addList);
			diffRecordsMap.put(SerffConstants.DRUG_LIST_REC_REMOVED, removeList);
			diffRecordsMap.put(SerffConstants.DRUG_LIST_REC_MODIFIED, modifiedList);
			LOGGER.debug("Processing drug list records for difference report");
			DrugSearchDTO drugDetails = null;
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				if(objArray != null && objArray.length >= SerffConstants.DRUG_QUERY_COL_COUNT ) {
					drugDetails = null;
					String[] drugDataArray = new String[SerffConstants.DRUG_LIST_NEW_DRUG_TIER_TYPE2+1];
					int index = SerffConstants.DRUG_LIST_OLD_RXCUI;
					for (Object objVal : objArray) {
						if(null != objVal) {
							drugDataArray[index] = (String) objVal;
						}
						index++;
					}
					if(drugDataArray[SerffConstants.DRUG_LIST_OLD_RXCUI] != null && drugDataArray[SerffConstants.DRUG_LIST_NEW_RXCUI] == null) { //Drug Removed in new
						drugDetails = drugProperties.get(drugDataArray[SerffConstants.DRUG_LIST_OLD_RXCUI]);
						removeList.add(drugDataArray);
					} else if(drugDataArray[SerffConstants.DRUG_LIST_OLD_RXCUI] == null && drugDataArray[SerffConstants.DRUG_LIST_NEW_RXCUI] != null) { //Drug added to new
						drugDetails = drugProperties.get(drugDataArray[SerffConstants.DRUG_LIST_NEW_RXCUI]);
						addList.add(drugDataArray);
					} else { //Drug modified
						drugDetails = drugProperties.get(drugDataArray[SerffConstants.DRUG_LIST_OLD_RXCUI]);
						modifiedList.add(drugDataArray);
					}
					if(drugDetails != null) {
						drugDataArray[SerffConstants.DRUG_LIST_OLD_RX_NAME] = drugDetails.getDrugName();
						drugDataArray[SerffConstants.DRUG_LIST_OLD_RX_FULL_NAME] = drugDetails.getDrugFullName();
						drugDataArray[SerffConstants.DRUG_LIST_OLD_RX_STRENGTH] = drugDetails.getStrength();
					} else {
						drugDataArray[SerffConstants.DRUG_LIST_OLD_RX_NAME] = StringUtils.EMPTY;
						drugDataArray[SerffConstants.DRUG_LIST_OLD_RX_FULL_NAME] = StringUtils.EMPTY;
						drugDataArray[SerffConstants.DRUG_LIST_OLD_RX_STRENGTH] = StringUtils.EMPTY;
					}
				}
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Done Processing drug list records for difference report. Added:" + addList.size() + " Removed:" + removeList.size() 
				+ " Modified:" + modifiedList.size());
			}
		}
		return;
	}
	
	private List<String> extractRxcuiForDifferenceReport(List<?> rsList) {
		List<String> rxcuiList = null;
		if(null != rsList ) {
			Iterator<?> rsIterator = rsList.iterator();
			rxcuiList = new ArrayList<String>();
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				if(objArray != null && objArray.length >= SerffConstants.DRUG_QUERY_COL_COUNT ) {
					if(objArray[SerffConstants.DRUG_LIST_OLD_RXCUI-(SerffConstants.DRUG_LIST_OLD_RX_STRENGTH+1)] != null ) { 
						rxcuiList.add((String) objArray[SerffConstants.DRUG_LIST_OLD_RXCUI-(SerffConstants.DRUG_LIST_OLD_RX_STRENGTH+1)]);
					} else if(objArray[SerffConstants.DRUG_LIST_NEW_RXCUI-(SerffConstants.DRUG_LIST_OLD_RX_STRENGTH+1)] != null) { 
						rxcuiList.add((String) objArray[SerffConstants.DRUG_LIST_NEW_RXCUI-(SerffConstants.DRUG_LIST_OLD_RX_STRENGTH+1)]);
					} 
				}
			}
		}
		return rxcuiList;
	}
	
	private String buildFormularyDrugListDifferenceQuery() {
		StringBuilder buildquery = new StringBuilder();
		buildquery.append("Select D1.rxcui as old_rxcui, D1.auth_Required as old_auth_Required, D1.step_Therapy_Required as old_step_Therapy_Required, "
				+ " D1.drug_Tier_Type1 as old_drug_Tier_Type1, D1.drug_Tier_Type2 as old_drug_Tier_Type2, " 
		+ " D2.rxcui as new_rxcui, D2.auth_Required as new_auth_Required, D2.step_Therapy_Required as new_step_Therapy_Required, "
		+ " D2.drug_Tier_Type1 as new_drug_Tier_Type1, D2.drug_Tier_Type2 as new_drug_Tier_Type2 FROM "
		+ " (select pd1.rxcui, pd1.auth_Required, pd1.step_Therapy_Required, pdt1.drug_Tier_Type1, pdt1.drug_Tier_Type2, "
		+ " pdt1.id from pm_drug pd1, pm_drug_tier pdt1 where pd1.drug_Tier_Id = pdt1.id and pdt1.drug_List_Id in (:old_drug_list_id) ) D1 "
		+ " FULL OUTER JOIN "
		+ " ( "
		+ " select pd2.rxcui, pd2.auth_Required, pd2.step_Therapy_Required, pdt2.drug_Tier_Type1, pdt2.drug_Tier_Type2, pdt2.id "
		+ " from pm_drug pd2, pm_drug_tier pdt2 where pd2.drug_Tier_Id = pdt2.id and pdt2.drug_List_Id in (:new_drug_list_id) "
		+ " ) D2"
		+ " ON D2.rxcui=D1.rxcui"
		+ " where"
		+ " D1.auth_Required <> D2.auth_Required"
		+ " OR D1.step_Therapy_Required <> D2.step_Therapy_Required"
		+ " OR D1.drug_Tier_Type1 <> D2.drug_Tier_Type1"
		+ " OR D1.drug_Tier_Type2 <> D2.drug_Tier_Type2"
		+ " OR D1.auth_Required is null or D2.auth_Required is null");

		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("build query : " + buildquery.toString());
		}
		return buildquery.toString();
	}

	private String buildFormularyDrugListCountQuery() {
		StringBuilder buildquery = new StringBuilder();
		buildquery.append("select pdt.drug_list_id, count(*) from pm_drug pd, pm_drug_tier pdt "
				+ "where pd.drug_tier_id = pdt.id and "
				+ "pdt.drug_list_id in (:drug_list_ids) group by pdt.drug_list_id");
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("build query for drug count : " + buildquery.toString());
		}
		return buildquery.toString();
	}
	
	
	public Map<String, DrugSearchDTO> getDrugProperties(List<String> rxcuiList) {
		DrugPropertiesResponseDTO drugPropertiesResponseDTO = null;
		Map<String, DrugSearchDTO> rxcuiDrugPropertiesMap = new HashMap<String, DrugSearchDTO>();
		if(!CollectionUtils.isEmpty(rxcuiList)) {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Making call to get drug properties for list of RXCUI of size " + rxcuiList.size());
			}
			DrugPropertiesRequestDTO request = new DrugPropertiesRequestDTO();
			request.setDrugRxCodeList(rxcuiList);
			try {
				String response = ghixRestTemplate.postForObject(GhixEndPoints.PlandisplayEndpoints.DRUG_PROPERTIES_BY_RXCUI, request, String.class);
				if (StringUtils.isNotBlank(response)){
					drugPropertiesResponseDTO = objectMapper.readValue(response, DrugPropertiesResponseDTO.class);
					List<DrugSearchDTO> drugList = drugPropertiesResponseDTO.getDrugList();
					for (DrugSearchDTO drugSearchDTO : drugList) {
						rxcuiDrugPropertiesMap.put(drugSearchDTO.getDrugID(), drugSearchDTO);
					}
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Got drug properties for list of RXCUI, properties size: " + rxcuiDrugPropertiesMap.size());
					}
				 }
			} catch (Exception e) {
				LOGGER.error("Exceptin Occured in getDrugProperties ", e);
			}
		}
		return rxcuiDrugPropertiesMap;
	}
}
