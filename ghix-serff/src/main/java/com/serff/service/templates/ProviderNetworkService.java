package com.serff.service.templates;

import com.getinsured.hix.model.Network;

public interface ProviderNetworkService {
	Network saveNetwork(Network network);
}
