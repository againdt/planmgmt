/**
 * File 'DCSTypeServiceImpl' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:47:32 AM
 */
package com.serff.service.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.DCSType;
import com.serff.repository.templates.IDCSTypeRepository;

/**
 * Class 'DCSTypeServiceImpl' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:47:32 AM
 *
 */
@Service("dcsTypeService")
@Repository
@Transactional
public class DCSTypeServiceImpl implements DCSTypeService {
	
	//private static final Logger LOGGER = LoggerFactory.getLogger(DCSTypeServiceImpl.class);

	
	@Autowired
	private IDCSTypeRepository dcsTypeRepository;

	/**
	 * Constructor.
	 */
	public DCSTypeServiceImpl() {
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.DCSTypeService#saveDCSType(com.getinsured.hix.model.DCSType)
	 */
	@Override
	public DCSType saveDCSType(DCSType dcsType) {
		return dcsTypeRepository.save(dcsType);
	}

}
