package com.serff.service.templates;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Formulary;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerServiceArea;
import com.getinsured.hix.model.IssuerServiceAreaExt;
import com.getinsured.hix.model.Network;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.PlanRate;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.model.serff.SerffTemplates;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.util.exception.GIException;
import com.serff.repository.ISerffTemplates;
import com.serff.repository.templates.IFormularyRepository;
import com.serff.repository.templates.IIssuerServiceAreaRepository;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.INetworkRepository;
import com.serff.repository.templates.IPlanDentalBenefitRepository;
import com.serff.repository.templates.IPlanHealthBenefitRepository;
import com.serff.repository.templates.IPlanRate;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.repository.templates.IServiceAreaRepository;
import com.serff.service.PlanDisplayRulesMap;
import com.serff.template.plan.PlanAndBenefitsPackageVO;
import com.serff.template.plan.PlanAndBenefitsVO;
import com.serff.template.plan.QhpApplicationRateGroupVO;
import com.serff.template.svcarea.hix.pm.ServiceAreaType;
import com.serff.util.PlanUpdateStatistics;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

@Component
/**
 * Class is used to update certified and non certified plans data
 * 
 * Common template [Network, Service Area, Formulary, Rates]
 * Common data from Plan Benefits
 */
public class UpdatePlanServiceImpl {

	private static final Logger LOGGER = Logger.getLogger(UpdatePlanServiceImpl.class);
	private static final String MSG_PLAN_ID = " PlanId: ";
	private static final String MSG_STATE_CODE = " stateCode ";
	private static final int DEFAULT_LMD_YEAR = 2010;
	// private final String dateFormat = "yyyy-MM-dd hh:mm:ss";
	@Autowired private ISerffTemplates iSerffTemplatesRepository;
	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private IPlanRate iPlanRateRepository;
	@Autowired private IIssuersRepository iIssuerRepository;
	@Autowired private IIssuerServiceAreaRepository iIssuerServiceAreaRepository;
	@Autowired private IServiceAreaRepository iServiceAreaRepository;
	@Autowired private PlanMgmtSerffServiceImpl planMgmtServiceImpl;
	@Autowired private IFormularyRepository iSerffFormularyRepository;
	@Autowired private INetworkRepository iNetworkRepository;
	@Autowired private IPlanHealthBenefitRepository iSerffPlanHealthBenefitRepository;
	@Autowired private IPlanDentalBenefitRepository iSerffPlanDentalBenefitRepository;
	@Autowired private SerffUtils serffUtils;

	/**
	 * This method updates the Serff_Templates table with Last modified dates of templates
	 * @author Geetha Chandran
	 * @since 12 Aug, 2013
	 * @return void
	 */
	public void updateSerffTemplate(int applicableYear, String planId, Map<String, Date> templatesLastModifiedDates,
			boolean isPartialUpdate, EntityManager entityManager ,Map<String , Date>lastUpdatedTemplateMap ) throws GIException {

		if (StringUtils.isNotEmpty(planId) && null != entityManager
				&& entityManager.isOpen()) {

			SerffTemplates serffTemplate = iSerffTemplatesRepository.findByPlanIdAndApplicableYear(planId, applicableYear);

			if(null == serffTemplate){
				LOGGER.info("Updating Serff Templates table with Last modified date of tempaltes for given PLAN " + planId);
				serffTemplate = new SerffTemplates();
			}
			serffTemplate.setPlanId(planId);
			Calendar cal = Calendar.getInstance();
			cal.set(DEFAULT_LMD_YEAR, 1, 1, 0, 0);

			if(!isPartialUpdate){
				if(null == templatesLastModifiedDates.get(SerffConstants.DATA_ADMIN_DATA)) {
					templatesLastModifiedDates.put(SerffConstants.DATA_ADMIN_DATA, cal.getTime());
				}
				if(null == templatesLastModifiedDates.get(SerffConstants.DATA_BENEFITS)) {
					templatesLastModifiedDates.put(SerffConstants.DATA_BENEFITS, cal.getTime());
				}
				if(null == templatesLastModifiedDates.get(SerffConstants.DATA_NETWORK_ID)) {
					templatesLastModifiedDates.put(SerffConstants.DATA_NETWORK_ID, cal.getTime());
				}
				if(null == templatesLastModifiedDates.get(SerffConstants.DATA_SERVICE_AREA)) {
					templatesLastModifiedDates.put(SerffConstants.DATA_SERVICE_AREA, cal.getTime());
				}
				if(null == templatesLastModifiedDates.get(SerffConstants.DATA_PRESCRIPTION_DRUG)) {
					templatesLastModifiedDates.put(SerffConstants.DATA_PRESCRIPTION_DRUG, cal.getTime());
				}
				serffTemplate.setAdminLastModifiedDate(templatesLastModifiedDates.get(SerffConstants.DATA_ADMIN_DATA));
				serffTemplate.setPlanLastModifiedDate(templatesLastModifiedDates.get(SerffConstants.DATA_BENEFITS));
				serffTemplate.setNetworkLastModifiedDate(templatesLastModifiedDates.get(SerffConstants.DATA_NETWORK_ID));
				serffTemplate.setSvcAreaLastModifiedDate(templatesLastModifiedDates.get(SerffConstants.DATA_SERVICE_AREA));
				serffTemplate.setPresDrugLastModifiedDate(templatesLastModifiedDates.get(SerffConstants.DATA_PRESCRIPTION_DRUG));
			}
			if(null == templatesLastModifiedDates.get(SerffConstants.DATA_RATING_TABLE)) {
				templatesLastModifiedDates.put(SerffConstants.DATA_RATING_TABLE, cal.getTime());
			}
			//updating last updated dates
			updateLastUpdatedDates(serffTemplate , lastUpdatedTemplateMap);
			
			serffTemplate.setApplicableYear(applicableYear);
			serffTemplate.setRatesLastModifiedDate(templatesLastModifiedDates.get(SerffConstants.DATA_RATING_TABLE));
			entityManager.merge(serffTemplate);
			LOGGER.info("Updating Serff Templates table ends");
		}
	}
	
	/**
	 * This method update the last updated dates of specified templates.
	 * @param serffTemplate
	 * @param lastUpdatedTemplateMap
	 */
	private void updateLastUpdatedDates(SerffTemplates serffTemplate, Map<String , Date> lastUpdatedTemplateMap) {
		LOGGER.info("updateLastUpdatedDates starts");
		if(!CollectionUtils.isEmpty(lastUpdatedTemplateMap)){
			Date newDate = new Date();
			for (Map.Entry<String,Date> entry : lastUpdatedTemplateMap.entrySet()) {
			    String templateName = entry.getKey();
			    Date lastUpdateDate = entry.getValue();
			    
			    switch(templateName){
				    case SerffConstants.DATA_ADMIN_DATA :
				    	if(lastUpdateDate == null){
			    			serffTemplate.setAdminLastUpdated(newDate);
			    		}else{
			    			serffTemplate.setAdminLastUpdated(lastUpdateDate);
			    		}
				    	break;
				    case SerffConstants.DATA_BENEFITS :
				    	if(lastUpdateDate == null){
			    			serffTemplate.setPlanLastUpdated(newDate);
			    		}else{
			    			serffTemplate.setPlanLastUpdated(lastUpdateDate);
			    		}
				    	break;
				    case SerffConstants.DATA_RATING_TABLE:
				    	if(lastUpdateDate == null){
			    			serffTemplate.setRatesLastUpdated(newDate);
			    		}else{
			    			serffTemplate.setRatesLastUpdated(lastUpdateDate);
			    		}
				    break;
				    case SerffConstants.DATA_NETWORK_ID :
				    	if(lastUpdateDate == null){
			    			serffTemplate.setNetworkLastUpdated(newDate);
			    		}else{
			    			serffTemplate.setNetworkLastUpdated(lastUpdateDate);
			    		}
				    break;
				    case SerffConstants.DATA_SERVICE_AREA:
				    	if(lastUpdateDate == null){
			    			serffTemplate.setServiceAreaLastUpdated(newDate);
			    		}else{
			    			serffTemplate.setServiceAreaLastUpdated(lastUpdateDate);
			    		}
				    break;
				    case SerffConstants.DATA_PRESCRIPTION_DRUG :
				    	if(lastUpdateDate == null){
			    			serffTemplate.setPresDrugLastUpdated(newDate);
			    		}else{
			    			serffTemplate.setPresDrugLastUpdated(lastUpdateDate);
			    		}
				    break;
				    case SerffConstants.DATA_RATING_RULES :
				    	if(lastUpdateDate == null){
			    			serffTemplate.setBusinessRuleLastUpdated(newDate);
			    		}else{
			    			serffTemplate.setBusinessRuleLastUpdated(lastUpdateDate);
			    		}
				    break;
				    case SerffConstants.DATA_NCQA_DATA :
				    	if(lastUpdateDate == null){
			    			serffTemplate.setUracLastUpdated(newDate);
			    		}else{
			    			serffTemplate.setUracLastUpdated(lastUpdateDate);
			    		}
				    break;
				    case SerffConstants.DATA_URAC_DATA :
				    	if(lastUpdateDate == null){
			    			serffTemplate.setNcqaLastUpdated(newDate);
			    		}else{
			    			serffTemplate.setNcqaLastUpdated(lastUpdateDate);
			    		}
				    break;
				    case SerffConstants.DATA_UNIFIED_RATE :
				    	if(lastUpdateDate == null){
			    			serffTemplate.setUrrtLastUpdated(newDate);
			    		}else{
			    			serffTemplate.setUrrtLastUpdated(lastUpdateDate);
			    		}
				    break;
				   default:
					LOGGER.info("Invalid Template");  
			    }
			}
		}
		LOGGER.info("updateLastUpdatedDates starts");
	}

	/**
	 * Method is used to create SerffTemplates object with Last Modified date of 2010-01-01.
	 * @param planId
	 */
	private SerffTemplates createSerffTemplates(int applicableYear, String planId) {
		
		SerffTemplates serffTemplate = null;
		
		if (StringUtils.isNotBlank(planId)) {
			
			Calendar cal = Calendar.getInstance();
			cal.set(DEFAULT_LMD_YEAR, 1, 1, 0, 0);
			LOGGER.info("createSerffTemplates() -> Last Modified date is " + cal.getTime().toString());
			
			serffTemplate = new SerffTemplates();
			serffTemplate.setApplicableYear(applicableYear);
			serffTemplate.setPlanId(planId);
			serffTemplate.setAdminLastModifiedDate(cal.getTime());
			serffTemplate.setPlanLastModifiedDate(cal.getTime());
			serffTemplate.setNetworkLastModifiedDate(cal.getTime());
			serffTemplate.setSvcAreaLastModifiedDate(cal.getTime());
			serffTemplate.setPresDrugLastModifiedDate(cal.getTime());
			serffTemplate.setRatesLastModifiedDate(cal.getTime());
		}
		return serffTemplate;
	}

	/**
	 * This method updates the RATES data for the CERTIFIED plans.
	 * @author Geetha Chandran
	 * @param persistOnExchangePlans 
	 * @param persistOffExchangePlans 
	 * @param entityManager
	 * @since 12 Aug, 2013
	 * @return List of plans for which RATES where updated.
	 * @throws GIException 
	 */
	public List<Plan> updateCertifiedPlans(int applicableYear, String planInsuranceType, String issuerID,
			PlanAndBenefitsPackageVO planAndBenefitsPackageVO, PlanAndBenefitsVO planBenefitTemplate,
			Map<String, Object> reqTemplates, Map<String, Object> templateObjectMap,
			boolean persistOffExchangePlans, boolean persistOnExchangePlans, boolean persistPUFPlans,
			EntityManager entityManager, StringBuffer responseMessage, List<Tenant> tenantList,
			Map<String, Date> lastUpdatedTemplateMap, Map<String, Date> templatesLastModifiedDates,
			PlanUpdateStatistics uploadStatistics, PlanDisplayRulesMap planDispRulesMap) throws GIException {

		LOGGER.info("update Certified Plans begins for issuerID " + issuerID);

		boolean isPHIXProfile = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE).equals(SerffConstants.PHIX_PROFILE);
		Issuer issuer = null;
		Map<String, List<PlanRate>> planRates;
		Object object = reqTemplates.get(SerffConstants.TRANSFER_PLAN);
		
		object = reqTemplates.get(SerffConstants.DATA_RATING_TABLE);
		QhpApplicationRateGroupVO rates = (QhpApplicationRateGroupVO) object;
		List<Plan> existingPlan = null;
		List<String> newPlanIdList;
		
		if(null != planBenefitTemplate
				&& null != planBenefitTemplate.getPlanAttributes()
				&& null != planBenefitTemplate.getPlanAttributes().getStandardComponentID()) {
			
			String planId = planBenefitTemplate.getPlanAttributes().getStandardComponentID().getCellValue();
			SerffTemplates serffTemplate = iSerffTemplatesRepository.findByPlanIdAndApplicableYear(planId, applicableYear);
			
			if (null == serffTemplate) {
				serffTemplate = createSerffTemplates(applicableYear, planId);
			}
			
			if (null != serffTemplate) {
				//For Certified plans PlanBenefits last modified date does not change
				templatesLastModifiedDates.put(SerffConstants.DATA_BENEFITS, serffTemplate.getPlanLastModifiedDate());
				
				Date savedRatesLMD = serffTemplate.getRatesLastModifiedDate();
				Date currentRatesLMD = templatesLastModifiedDates.get(SerffConstants.DATA_RATING_TABLE);
				LOGGER.info("Comparing LastModifiedDates for Certified Plans for issuerID " + issuerID + " and planId: " + planId);

				lastUpdatedTemplateMap.put(SerffConstants.DATA_BENEFITS, serffTemplate.getPlanLastUpdated());

				//If only rates are updated then existing plans only updated with rates data.
				if(lmdDateCompare(savedRatesLMD, currentRatesLMD)){
					Date currentTime = Calendar.getInstance().getTime();
					issuer = iIssuerRepository.getIssuerByHiosID(issuerID);
					newPlanIdList = new ArrayList<String>();
					
					LOGGER.debug("Rates are modified to existing plans " + planId);
					planRates = planMgmtServiceImpl.savePlanRateBean(entityManager, issuer.getStateOfDomicile(), rates);
					existingPlan = iPlanRepository.findByIssuerAndIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYearAndInsuranceType(issuer,Plan.IS_DELETED.N.toString(), planId, applicableYear, planInsuranceType);
					//Rates are modified so updating last updated date
					lastUpdatedTemplateMap.put(SerffConstants.DATA_RATING_TABLE, null);
					LOGGER.debug("Last updated date is modified for plan :"+ planId);
					boolean planBelongsToSelectedExchangeType = false;
							
					for(com.serff.template.plan.CostShareVarianceVO csvVO : planBenefitTemplate.getCostShareVariancesList().getCostShareVariance()){
						if (null != csvVO.getPlanId()
								&& null != csvVO.getPlanId().getCellValue()) {
							LOGGER.debug("newPlanIdLoadStatusMap entry : "  + csvVO.getPlanId().getCellValue().replace("-", "") );
							newPlanIdList.add(csvVO.getPlanId().getCellValue().replace("-", ""));
							if(!planBelongsToSelectedExchangeType) {
								if(csvVO.getPlanId().getCellValue().endsWith(SerffConstants.OFF_EXCHANGE_SUFFIX)) {
									if(persistOffExchangePlans) {
										planBelongsToSelectedExchangeType = true;
									}
								} else {
									if(persistOnExchangePlans) {
										planBelongsToSelectedExchangeType = true;
									}
								}
							}
						}
					}
					
					//Change rates only if at-least one variant matches the selected exchange-type
					if(planBelongsToSelectedExchangeType) {
						//update rates for existing plans in DB
						for (Plan plan : existingPlan) {
							/**
							 * SCENARIO:
							 * 1) Soft-delete existing future rates
							 * 2) Load new Future Rates
							 */
							List<PlanRate> newRatesList = planRates.get(planId);
							//HIX-63650 : Change rates persistence code to override child max age for rates in the template.
							if(Plan.PlanInsuranceType.DENTAL.toString().equals(plan.getInsuranceType())){
								newRatesList = serffUtils.preProcessDentalPlanRates(newRatesList);
							}
							
							List<PlanRate> updateRateList = new ArrayList<PlanRate>();
							
							//Existing rates logic. 
							for (PlanRate rate : newRatesList) {
								//update only future rates
								if(rate.getEffectiveStartDate().after(currentTime)) {
									if(rate.getEffectiveStartDate().before(plan.getEndDate())) {
										PlanRate availRate = iPlanRateRepository.findByRatingAreaAndPlanAndMinAgeAndMaxAgeAndTobaccoAndEffectiveStartDateAndEffectiveEndDateAndIsDeleted(rate.getRatingArea(), plan, rate.getMinAge(), rate.getMaxAge(), rate.getTobacco(), rate.getEffectiveStartDate(), rate.getEffectiveEndDate(), PlanRate.IS_DELETED.N.toString());
			
										if(null != availRate){
											LOGGER.debug("Marking rate as deleted for rating area= " +  rate.getRatingArea().getId() + " planid=" +  plan.getIssuerPlanNumber() + " minAge= " + rate.getMinAge() + " maxAge " + rate.getMaxAge() + " tobacco= " + rate.getTobacco() + " effecStartDate= " + rate.getEffectiveStartDate()+ " effecEndDate= " + rate.getEffectiveEndDate());
											availRate.setIsDeleted(PlanRate.IS_DELETED.Y.toString());
											updateRateList.add(availRate);
										}
										LOGGER.debug("Inserting new rates for " + plan.getIssuerPlanNumber());
										PlanRate tmpRate = rate.clone();
										tmpRate.setPlan(plan);
										tmpRate.setIsDeleted(PlanRate.IS_DELETED.N.toString());
										updateRateList.add(tmpRate);
									} else {
										LOGGER.info("Skipping Rates beyond plan end date for plan " + plan.getIssuerPlanNumber());
									}
								} 
							}
														
							//HIX-50974 : Send new error messages to SERFF when Plan is in certified status or non-incomplete status/
							if((!isPHIXProfile) && CollectionUtils.isEmpty(updateRateList) && (!responseMessage.toString().contains(SerffConstants.PLAN_CERTIFIED_MESSAGE_NO_FUTURE_RATES))){
									responseMessage.append(SerffConstants.PLAN_CERTIFIED_MESSAGE_NO_FUTURE_RATES);
							}
							
							plan.setPlanRate(updateRateList);
							plan = entityManager.merge(plan);										
							//Rates updated for CERTIFIED plans CSV that exist in DB 
							uploadStatistics.planUpdated(false);
							LOGGER.info("Plans updated with new rates list." + plan.getIssuerPlanNumber());
							//update status in map as plan updated = true
							if(newPlanIdList.contains(plan.getIssuerPlanNumber())) {
								newPlanIdList.remove(plan.getIssuerPlanNumber());
							}
						}
					}
					//update serff_tempalte table
					updateSerffTemplate(applicableYear, planId, templatesLastModifiedDates, true, entityManager ,lastUpdatedTemplateMap );
					//Now insert missing CSV for the plan in the DB
					if(!CollectionUtils.isEmpty(newPlanIdList)) {
						//To store common templates data from existing plans.
						List<IssuerServiceArea> certifiedServiceAreas = new ArrayList<IssuerServiceArea>();
						Map<String, Network> certifiedNetworks = new HashMap<String, Network>();
						Map<String, Formulary> certifiedFormularies = new HashMap<String, Formulary>();
						Plan plan = existingPlan.get(0);						
						LOGGER.debug("Adding old Network to existing plan variant.");
						List<Network> networks = iNetworkRepository.getProviderNetworkByIssuerIdAndApplicableYear(issuer.getId(), applicableYear);
						for (Network network : networks) {
							certifiedNetworks.put(network.getNetworkID(), network);
						}
						LOGGER.debug("Adding old IssuerServiceArea to existing plan variant.");
						certifiedServiceAreas.add(iIssuerServiceAreaRepository.findOne(plan.getServiceAreaId()));
						List<Formulary> existingPlanFormaularyList = iSerffFormularyRepository.findByIssuer_IdAndFormularyIdAndApplicableYear(issuer.getId(), plan.getFormularlyId(), applicableYear);
						if (null != existingPlanFormaularyList
								&& !existingPlanFormaularyList.isEmpty()) {
							LOGGER.debug("Adding old Prescription Drug(Formulary) to existing plan variant.");
							certifiedFormularies.put(existingPlanFormaularyList.get(0).getFormularyId(), existingPlanFormaularyList.get(0));
						}
						Map<String, String> documents =	(HashMap<String, String>) reqTemplates.get(SerffConstants.SUPPORTING_DOC);
						Object objPlanTemplate = reqTemplates.get(SerffConstants.DATA_BENEFITS);
	
						LOGGER.debug("Adding new Certified Plan list begins");
						if (objPlanTemplate instanceof com.serff.template.plan.PlanBenefitTemplateVO) {
							/**
							 * Update Plan template
							 * SCENARIO:
							 * 1) Load CSV using common data from existing plan in DB
							 *    Common templates [Network, Service Area, Formulary, Rates]
							 *    Common data from Plan Benefits
							 * 3) Skip this CSV
							 */
							List<Plan> savedCertifiedPlans = planMgmtServiceImpl.savePlanBenefitTemplateVO(applicableYear, true, entityManager,
											planAndBenefitsPackageVO, planBenefitTemplate, issuer, certifiedServiceAreas, certifiedNetworks,
											documents, planRates, persistOffExchangePlans, persistOnExchangePlans, persistPUFPlans,
											certifiedFormularies, templatesLastModifiedDates, responseMessage, tenantList,
											lastUpdatedTemplateMap, uploadStatistics, planDispRulesMap);
							
							if(savedCertifiedPlans != null && !savedCertifiedPlans.isEmpty()) {
								
								boolean isDentalPlanOnly = SerffConstants.YES.equalsIgnoreCase(
										planAndBenefitsPackageVO.getHeader().getDentalPlanOnlyInd().getCellValue());
								
								for (Plan plan2 : savedCertifiedPlans) {
									copyCommonPlanData(isDentalPlanOnly, plan, true, plan2, false, entityManager);
									plan2 = entityManager.merge(plan2);	
								}
							}
							LOGGER.info("Adding new Certified Plan list ends. Saved Plans Count: " + ((savedCertifiedPlans != null) ? savedCertifiedPlans.size() : "0"));
						}
					}
				}
				else {
					//HIX-50974 : Send new error messages to SERFF when Plan is in certified status or non-incomplete status/
					if((!isPHIXProfile) && (!responseMessage.toString().contains(SerffConstants.PLAN_CERTIFIED_MESSAGE_RATES_NOT_MODIFIED))){
							responseMessage.append(SerffConstants.PLAN_CERTIFIED_MESSAGE_RATES_NOT_MODIFIED);
					}
					
					if(!CollectionUtils.isEmpty(planBenefitTemplate.getCostShareVariancesList().getCostShareVariance())){
						for(int i = 0; i < planBenefitTemplate.getCostShareVariancesList().getCostShareVariance().size(); i++){
							uploadStatistics.planSkippedCertified();
						}
					}
					
					LOGGER.info("Rates are not modified to existing plans " + planId);
					templatesLastModifiedDates.put(SerffConstants.DATA_RATING_TABLE, savedRatesLMD);
					lastUpdatedTemplateMap.put(SerffConstants.DATA_RATING_TABLE, serffTemplate.getRatesLastUpdated());
				}
			}
		}
		LOGGER.info("updateCertifiedPlans ends");
		return existingPlan;
	}

	/**
	 * This method updates the Plan data for the NON-CERTIFIED plans.
	 * @author Bhavin Parmar
	 * @param persistOnExchangePlans 
	 * @param persistOffExchangePlans 
	 * @since 14 Aug, 2013
	 * @return List of new added plans.
	 */
	@SuppressWarnings("unchecked")
	private List<Plan> updateNonCertifiedPlanAndBenefit(int applicableYear, String planInsuranceType, Map<String, Object> reqTemplates,
			PlanAndBenefitsPackageVO planAndBenefitsPackageVO, PlanAndBenefitsVO planBenefitTemplate,
			List<IssuerServiceArea> issuerServiceAreas, Map<String, Network> networks, Map<String, List<PlanRate>> planRates,
			boolean persistOffExchangePlans, boolean persistOnExchangePlans, Map<String, Formulary> savedFormulary, String planId,
			SerffTemplates serffTemplate, Issuer issuer, Map<String, Date> templatesLastModifiedDates,
			EntityManager entityManager, StringBuffer responseMessage, List<Tenant> tenantList, String exchangeTypeFilter,
			Map<String ,Date> lastUpdatedTemplateMap, PlanUpdateStatistics uploadStatistics,
			PlanDisplayRulesMap planDispRulesMap) throws GIException {

		LOGGER.info("updateNonCertifiedPlanAndBenefit begins for planId " + planId);
		List<Plan> savedPlans = null;
		boolean isPlanDeleted = false;
		String exchangeType = null;
		boolean isPUFPlan = SerffPlanMgmtBatch.EXCHANGE_TYPE.PUF.toString().equalsIgnoreCase(exchangeTypeFilter);
		
		if(isPUFPlan) {
			exchangeType = SerffPlanMgmtBatch.EXCHANGE_TYPE.ON.toString();
		} else {
			exchangeType = exchangeTypeFilter;
		}
		
		try {
			
			boolean isNullData = (null == serffTemplate || null == reqTemplates || null == issuer || null == templatesLastModifiedDates);

			if (isNullData || StringUtils.isBlank(planId) || null == entityManager || !entityManager.isOpen()) {
				return savedPlans;
			}

			Date savedLMD = serffTemplate.getPlanLastModifiedDate();
			Date currentLMD = templatesLastModifiedDates.get(SerffConstants.DATA_BENEFITS);
			if (lmdDateCompare(savedLMD, currentLMD)) {
				
				List<String> planIdListForCommonDataUpdate = new ArrayList<String>() ;

				if (planMgmtServiceImpl.validateRateExistForPlans(planAndBenefitsPackageVO, planRates, persistOffExchangePlans, persistOnExchangePlans, responseMessage , applicableYear)) {
					
					List<Plan> existingPlan = iPlanRepository.findByIssuerAndIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYearAndInsuranceType(issuer,Plan.IS_DELETED.N.toString(), planId, applicableYear, planInsuranceType);
					//This map holds new issuer plan number and exchange type selected.
					Map<String, String> newPlanIdExchageTypeMap = new HashMap<String, String>();
						
					List<com.serff.template.plan.CostShareVarianceVO> csvList = planBenefitTemplate.getCostShareVariancesList().getCostShareVariance();
					lastUpdatedTemplateMap.put(SerffConstants.DATA_BENEFITS, null);
					for(com.serff.template.plan.CostShareVarianceVO csvVO : csvList) {
						if (null != csvVO.getPlanId()
								&& null != csvVO.getPlanId().getCellValue()) {
							LOGGER.info("planIdExchanegeMap entry : "  + csvVO.getPlanId().getCellValue().replace("-", "") + " - " + exchangeType);
							newPlanIdExchageTypeMap.put(csvVO.getPlanId().getCellValue().replace("-", ""), exchangeType);
						}
					}
//					LOGGER.debug("newPlanIdExchageTypeMap is null : " + (null == newPlanIdExchageTypeMap));
					LOGGER.debug("exchangeTypeFilter : " + exchangeType);
					
					if (null != existingPlan && !existingPlan.isEmpty()
							&& null != newPlanIdExchageTypeMap && !newPlanIdExchageTypeMap.isEmpty()) {
						
						LOGGER.info("Checking to Delete existing Plans record begins. PlanID "  + planId + " Plans Count: " + existingPlan.size());
						for (Plan plan : existingPlan) {
							
							if (null != plan) {
							
								//Checking if plan status is not de-certified
								if(!plan.getStatus().equalsIgnoreCase(SerffConstants.DECERTIFIED)){
						
									boolean planBelongsToSelectedExchangeType = false;
									//If exchange type is ANY then delete Plan by issuer plan number irrespective of exchange type.
									if (SerffPlanMgmtBatch.EXCHANGE_TYPE.ANY.toString().equals(exchangeType)){
										planBelongsToSelectedExchangeType = true;										
									}	//If exchange Type is not ANY check for ON and OFF exchange type too.
									else if (plan.getExchangeType().equals(exchangeType)){
										planBelongsToSelectedExchangeType = true;
									}
										
									//If the plan varient is not present in template, update only common data 
									if (!newPlanIdExchageTypeMap.containsKey(plan.getIssuerPlanNumber())) {
										/**
										 * SCENARIO:
										 * 1) Update common template [Network, Service Area, Formulary, Rates]
										 * 2) Update common data from Plan Benefits
										 */
										planIdListForCommonDataUpdate.add(plan.getIssuerPlanNumber());
										//Common data would be updated later in the method 
										uploadStatistics.planSkipped();
									}
									else {
										/**
										 * SCENARIO:
										 * 1) Soft delete CSV
										 * 2) Load CSV from template (along with common data once)
										 */
										//If the plan varient is present in template, soft-delete and reload if exchange type matches 
										if (planBelongsToSelectedExchangeType) {
											LOGGER.info("Deleting existing Plan with id "+plan.getId()+" : " + plan.getIssuerPlanNumber() + " - " + plan.getExchangeType());										
											plan.setIsDeleted(Plan.IS_DELETED.Y.toString());
											plan = entityManager.merge(plan);
											LOGGER.debug("Plan("+ plan.getId() +") Marked as deleted.");																		
											uploadStatistics.planDeleted();
											
											if(!isPlanDeleted) {
												isPlanDeleted = true;
											}
										} else {
											/**
											 * SCENARIO:
											 * 1) Update common template [Network, Service Area, Formulary, Rates]
											 * 2) Update common data from Plan Benefits
											 */
											//If the plan varient is present in template, but exchange type doesn't matches update only common data
											planIdListForCommonDataUpdate.add(plan.getIssuerPlanNumber());
										}
									}
								}else{
									LOGGER.info("Plan marked as decertified ,so skipping this record: " + plan.getIssuerPlanNumber());
								}
							}
						}
						LOGGER.info("Deleting existing Plans record ends for planId "  + planId);
	
						Map<String, String> documents =	(HashMap<String, String>) reqTemplates.get(SerffConstants.SUPPORTING_DOC);
						Object objPlanTemplate = reqTemplates.get(SerffConstants.DATA_BENEFITS);
	
						LOGGER.debug("Fetching new Plan list begins");
						//re-Load all plan-csv from the template 
						if (objPlanTemplate instanceof com.serff.template.plan.PlanBenefitTemplateVO) {	
							//Update Plan template
							/**
							 * SCENARIO:
							 * 1) Soft delete CSV
							 * 2) Load CSV from template (along with common data once)
							 * 3) Skip this CSV
							 */
							savedPlans = planMgmtServiceImpl.savePlanBenefitTemplateVO(applicableYear, false, entityManager, planAndBenefitsPackageVO,
											planBenefitTemplate, issuer, issuerServiceAreas, networks, documents, planRates,
											persistOffExchangePlans, persistOnExchangePlans, isPUFPlan, savedFormulary,
											templatesLastModifiedDates, responseMessage, tenantList, lastUpdatedTemplateMap,
											uploadStatistics, planDispRulesMap);
						}
						LOGGER.debug("Fetching new Plan list ends. Saved Plans Count: " + ((savedPlans != null) ? savedPlans.size() : "0"));
						
						if(savedPlans != null && !savedPlans.isEmpty()) {
							LOGGER.debug("Saved Plans is not empty, Update common data for remaining plans if exist for plan ID " +  planId);

							//Some variant got saved so update common data for remaining variants
							if(!planIdListForCommonDataUpdate.isEmpty()) {
								LOGGER.debug("Updating common data for remaining plans. Plan Count: " +  planIdListForCommonDataUpdate.size());
								
								for (String planIdForUpdate : planIdListForCommonDataUpdate) {
									List<Plan> plans = iPlanRepository.findByIssuerAndIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYearAndInsuranceType(issuer,Plan.IS_DELETED.N.toString(), planIdForUpdate, applicableYear, planInsuranceType);
									LOGGER.info("Updating new common templates of existing plan which is not present the template with id " + planIdForUpdate);
									
									if(plans != null && plans.size() == 1) {
										Plan plan = updateNonCertifiedCommonTemplatesData(applicableYear, savedPlans.get(0), plans.get(0), planAndBenefitsPackageVO,
												planBenefitTemplate, issuerServiceAreas, networks, planRates, savedFormulary, issuer, entityManager);
										plan = entityManager.merge(plan);
										uploadStatistics.planUpdated(true);
									}
								}
							}
						}
					}
				}
				else {
					LOGGER.error(" updateNonCertifiedPlanAndBenefit() : Invalid Data. Either Rates are missing for some plans Or applicable year is diffent in Plan and Rates ");					
					throw new GIException(Integer.parseInt(SerffConstants.INVALID_PARAMETER_CODE), SerffConstants.INVALID_PARAMETER_MESSAGE_DATA_FORMAT_INVALID  + " : Invalid Data. Either Rates are missing for some plans Or applicable year is diffent in Plan and Rates.", "");
				}
			}
			else {
				
				//Update Skip count because LMD is older
				for(com.serff.template.plan.CostShareVarianceVO csvVO : planBenefitTemplate.getCostShareVariancesList().getCostShareVariance()) {
					if (null != csvVO.getPlanId() && null != csvVO.getPlanId().getCellValue()) {
						uploadStatistics.planSkipped();
					}
				}
				templatesLastModifiedDates.put(SerffConstants.DATA_BENEFITS, savedLMD);
				//Check and update plan status if required
				if(anyPlanDataUpdated(lastUpdatedTemplateMap)) {
					updateIncompletePlanStatus(issuer, planId, applicableYear, entityManager);
				}
				lastUpdatedTemplateMap.put(SerffConstants.DATA_BENEFITS, serffTemplate.getPlanLastUpdated());
			}
		}
		finally {

			if(!isPlanDeleted) {
				LOGGER.info("No Plan was marked as deleted for issuer " + issuer.getHiosIssuerId() + " and PlanID Starting With:" + planId);
			}
			LOGGER.info("updateNonCertifiedPlanAndBenefit ends");
		}
		return savedPlans;
	}

	private void updateIncompletePlanStatus(Issuer issuer, String planId, int applicableYear, EntityManager entityManager ) {
		List<Plan> plans = iPlanRepository.findByIssuerAndIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYearAndStatus(issuer,Plan.IS_DELETED.N.toString(), planId, applicableYear, Plan.PlanStatus.INCOMPLETE.toString());
		LOGGER.debug("Updating status for incomplete plan with id " + planId);
		if(plans != null && !plans.isEmpty()) {
			LOGGER.info("There are " + plans.size() + " to update status to LOADED");
			if(null != entityManager && entityManager.isOpen()) {
				for (Plan plan : plans) {
					plan.setStatus(Plan.PlanStatus.LOADED.toString());
					entityManager.merge(plan);
				}
			}
			LOGGER.info("Done with updating status for incomplete plan to loaded for id " + planId);
		}
	}
	
	/**
	 * Update common data of Plan and Benefit templates.
	 * 
	 * @param sourcePlan
	 * @param targetPlan
	 */
	private void copyCommonPlanData(boolean isDentalPlanOnly, Plan sourcePlan, boolean sourceFromDB, Plan targetPlan, boolean targetFromDB, EntityManager entityManager) {
		LOGGER.info("copyCommonPlanData start for plan " + sourcePlan.getIssuerPlanNumber());
		targetPlan.setLastUpdateTimestamp(Calendar.getInstance().getTime());
		targetPlan.setName(sourcePlan.getName());
		targetPlan.setNetworkType(sourcePlan.getNetworkType());
		targetPlan.setStartDate(sourcePlan.getStartDate());
		targetPlan.setEndDate(sourcePlan.getEndDate());
		targetPlan.setBrochure(sourcePlan.getBrochure());
		targetPlan.setHsa(sourcePlan.getHsa());
		targetPlan.setHiosProductId(sourcePlan.getHiosProductId());
		targetPlan.setHpid(sourcePlan.getHpid());
		targetPlan.setMarket(sourcePlan.getMarket());
		targetPlan.setInsuranceType(sourcePlan.getInsuranceType());
		targetPlan.setAvailableFor(sourcePlan.getAvailableFor());
		targetPlan.setBusinessRuleId(sourcePlan.getBusinessRuleId());
		
		if (isDentalPlanOnly) {
			copyCommonPlanDentalData(sourcePlan.getPlanDental(), sourceFromDB, targetPlan.getPlanDental(), targetFromDB, entityManager);
		}
		else {
			copyCommonPlanHealthData(sourcePlan.getPlanHealth(), sourceFromDB, targetPlan.getPlanHealth(), targetFromDB, entityManager);
		}
		LOGGER.info("copyCommonPlanData End for Plan " + sourcePlan.getIssuerPlanNumber());
	}
	
	private void copyCommonPlanHealthData(PlanHealth sourcePlanHealth, boolean sourceFromDB, PlanHealth targetPlanHealth, boolean targetFromDB, EntityManager entityManager) {
		LOGGER.info("copyCommonPlanHealthData Start");
		targetPlanHealth.setPlanLevel(sourcePlanHealth.getPlanLevel());
		targetPlanHealth.setUniquePlanDesign(sourcePlanHealth.getUniquePlanDesign());
		targetPlanHealth.setIsQHP(sourcePlanHealth.getIsQHP());
		targetPlanHealth.setPregnancyNotice(sourcePlanHealth.getPregnancyNotice());
		targetPlanHealth.setSpecialistReferal(sourcePlanHealth.getSpecialistReferal());
		targetPlanHealth.setPlanLevelExclusions(sourcePlanHealth.getPlanLevelExclusions());
		targetPlanHealth.setChildOnlyOffering(sourcePlanHealth.getChildOnlyOffering());
		targetPlanHealth.setWellnessProgramOffered(sourcePlanHealth.getWellnessProgramOffered());
		targetPlanHealth.setDiseaseMgmtProgramOffered(sourcePlanHealth.getDiseaseMgmtProgramOffered());
		targetPlanHealth.setBenefitsUrl(SerffUtils.checkAndcorrectURL(sourcePlanHealth.getBenefitsUrl()));
		targetPlanHealth.setEnrollmentUrl(SerffUtils.checkAndcorrectURL(sourcePlanHealth.getEnrollmentUrl()));
		targetPlanHealth.setOutOfCountryCoverage(sourcePlanHealth.getOutOfCountryCoverage());
		targetPlanHealth.setOutOfServiceAreaCoverage(sourcePlanHealth.getOutOfServiceAreaCoverage());
		targetPlanHealth.setNationalNetwork(sourcePlanHealth.getNationalNetwork());
		targetPlanHealth.setOutOfCountryCoverageDesc(sourcePlanHealth.getOutOfCountryCoverageDesc());
		targetPlanHealth.setOutOfServiceAreaCoverageDesc(sourcePlanHealth.getOutOfServiceAreaCoverageDesc());
		targetPlanHealth.setMaxCoinsuranceForSPDrug(sourcePlanHealth.getMaxCoinsuranceForSPDrug());
		targetPlanHealth.setMaxDaysForChargingInpatientCopay(sourcePlanHealth.getMaxDaysForChargingInpatientCopay());
		targetPlanHealth.setBeginPmryCareCsAfterNumOfVisits(sourcePlanHealth.getBeginPmryCareCsAfterNumOfVisits());
		targetPlanHealth.setBeginPmrycareDedtOrCoinsAfterNumOfCopays(sourcePlanHealth.getBeginPmrycareDedtOrCoinsAfterNumOfCopays());
		//targetPlanHealth.setBenefitFile(sourcePlanHealth.getBenefitFile());
		//targetPlanHealth.setRateFile(sourcePlanHealth.getRateFile());
		targetPlanHealth.setEhbCovered(sourcePlanHealth.getEhbCovered());
		targetPlanHealth.setRateEffectiveDate(sourcePlanHealth.getRateEffectiveDate());
		targetPlanHealth.setBenefitEffectiveDate(sourcePlanHealth.getBenefitEffectiveDate());
		
		List<PlanHealthBenefit> sourcePlanHealthBenefits; 
		List<PlanHealthBenefit> targetPlanHealthBenefits; 
		if(sourceFromDB) {
			sourcePlanHealthBenefits = iSerffPlanHealthBenefitRepository.findByPlanHealthId(sourcePlanHealth.getId());
		} else {
			sourcePlanHealthBenefits = sourcePlanHealth.getPlanHealthBenefitVO();
		}
		if(targetFromDB) {
			targetPlanHealthBenefits = iSerffPlanHealthBenefitRepository.findByPlanHealthId(targetPlanHealth.getId());
		} else {
			targetPlanHealthBenefits = targetPlanHealth.getPlanHealthBenefitVO();
		}

		// Add plan benefits here
		if (null != sourcePlanHealthBenefits
				&& !sourcePlanHealthBenefits.isEmpty() && null != targetPlanHealthBenefits) {

			LOGGER.info("Updating plan benefits for Plan " + sourcePlanHealth.getPlan().getIssuerPlanNumber() + " Benefit count: " + sourcePlanHealthBenefits.size());
			List<String> processedBenefitName = new ArrayList<String>();			
						
			//plan health id.
			for (PlanHealthBenefit sourceBenefit : sourcePlanHealthBenefits) {
				
				// No need to update common data which has been already updated to each target benefit from source benefit.
				if (!processedBenefitName.isEmpty()
						&& processedBenefitName.contains(sourceBenefit.getName())) {
					continue;
				}

				for (PlanHealthBenefit targetBenefit : targetPlanHealthBenefits) {
					
					if (sourceBenefit.getName().equals(targetBenefit.getName())) {
						processedBenefitName.add(sourceBenefit.getName());						
						targetBenefit.setNetworkLimitation(sourceBenefit.getNetworkLimitation());
						targetBenefit.setNetworkLimitationAttribute(sourceBenefit.getNetworkLimitationAttribute());
						targetBenefit.setNetworkExceptions(sourceBenefit.getNetworkExceptions());
						targetBenefit.setSubjectToInNetworkDuductible(sourceBenefit.getSubjectToInNetworkDuductible());
						targetBenefit.setSubjectToOutNetworkDeductible(sourceBenefit.getSubjectToOutNetworkDeductible());
						targetBenefit.setExcludedFromInNetworkMoop(sourceBenefit.getExcludedFromInNetworkMoop());
						targetBenefit.setExcludedFromOutOfNetworkMoop(sourceBenefit.getExcludedFromOutOfNetworkMoop());
						targetBenefit.setIsEHB(sourceBenefit.getIsEHB());
						targetBenefit.setIsCovered(sourceBenefit.getIsCovered());
						targetBenefit.setMinStay(sourceBenefit.getMinStay());
						targetBenefit.setExplanation(sourceBenefit.getExplanation());
						targetBenefit.setIsStateMandate(sourceBenefit.getIsStateMandate());
						targetBenefit.setServiceLimit(sourceBenefit.getServiceLimit());
						targetBenefit.setEhbVarianceReason(sourceBenefit.getEhbVarianceReason());
						targetBenefit.setEffStartDate(sourceBenefit.getEffStartDate());
						targetBenefit.setEffEndDate(sourceBenefit.getEffEndDate());
						if(targetFromDB && null != entityManager && entityManager.isOpen()) {
							targetBenefit = entityManager.merge(targetBenefit);
						}
					}
				}
			}
			processedBenefitName.clear();
			processedBenefitName = null;
		}
		LOGGER.info("copyCommonPlanHealthData End");
	}
	
	private void copyCommonPlanDentalData(PlanDental sourcePlanDental, boolean sourceFromDB, PlanDental targetPlanDental, boolean targetFromDB, EntityManager entityManager) {
		LOGGER.info("copyCommonPlanDentalData Start");
		targetPlanDental.setPlanLevel(sourcePlanDental.getPlanLevel());
		targetPlanDental.setIsQHP(sourcePlanDental.getIsQHP());
		targetPlanDental.setPlanLevelExclusions(sourcePlanDental.getPlanLevelExclusions());
		targetPlanDental.setEhbApptForPediatricDental(sourcePlanDental.getEhbApptForPediatricDental());
		targetPlanDental.setGuaranteedVsEstimatedRate(sourcePlanDental.getGuaranteedVsEstimatedRate());
		targetPlanDental.setBenefitsUrl(SerffUtils.checkAndcorrectURL(sourcePlanDental.getBenefitsUrl()));
		targetPlanDental.setEnrollmentUrl(SerffUtils.checkAndcorrectURL(sourcePlanDental.getEnrollmentUrl()));
		targetPlanDental.setOutOfCountryCoverage(sourcePlanDental.getOutOfCountryCoverage());
		targetPlanDental.setOutOfServiceAreaCoverage(sourcePlanDental.getOutOfServiceAreaCoverage());
		targetPlanDental.setNationalNetwork(sourcePlanDental.getNationalNetwork());
		targetPlanDental.setOutOfCountryCoverageDesc(sourcePlanDental.getOutOfCountryCoverageDesc());
		targetPlanDental.setOutOfServiceAreaCoverageDesc(sourcePlanDental.getOutOfServiceAreaCoverageDesc());
		targetPlanDental.setChildOnlyOffering(sourcePlanDental.getChildOnlyOffering());
		
		targetPlanDental.setBenefitFile(sourcePlanDental.getBenefitFile());
		targetPlanDental.setRateFile(sourcePlanDental.getRateFile());
		targetPlanDental.setRateEffectiveDate(sourcePlanDental.getRateEffectiveDate());
		targetPlanDental.setBenefitEffectiveDate(sourcePlanDental.getBenefitEffectiveDate());
		
		List<PlanDentalBenefit> sourcePlanDentalBenefits; 
		List<PlanDentalBenefit> targetPlanDentalBenefits; 
		if(sourceFromDB) {
			sourcePlanDentalBenefits = iSerffPlanDentalBenefitRepository.findByPlanDentalId(sourcePlanDental.getId());
		} else {
			sourcePlanDentalBenefits = sourcePlanDental.getPlanDentalBenefitVO();
		}
		if(targetFromDB) {
			targetPlanDentalBenefits = iSerffPlanDentalBenefitRepository.findByPlanDentalId(targetPlanDental.getId());
		} else {
			targetPlanDentalBenefits = targetPlanDental.getPlanDentalBenefitVO();
		}
		// Add plan benefits here
		if (null != sourcePlanDentalBenefits
				&& null != targetPlanDentalBenefits) {

			LOGGER.info("Updating plan benefits for Plan " + sourcePlanDental.getPlan().getIssuerPlanNumber() + " Benefit count: " + sourcePlanDentalBenefits.size());
			List<String> processedBenefitName = new ArrayList<String>();
			
			for (PlanDentalBenefit sourceBenefit : sourcePlanDentalBenefits) {
				
				// No need to update common data which has been already updated to each target benefit from source benefit.
				if (!processedBenefitName.isEmpty()
						&& processedBenefitName.contains(sourceBenefit.getName())) {
					continue;
				}

				for (PlanDentalBenefit targetBenefit : targetPlanDentalBenefits) {
					
					if (sourceBenefit.getName().equals(targetBenefit.getName())) {
						processedBenefitName.add(sourceBenefit.getName());
						
						targetBenefit.setNetworkExceptions(sourceBenefit.getNetworkExceptions());
						targetBenefit.setNetworkLimitationAttribute(sourceBenefit.getNetworkLimitationAttribute());
						targetBenefit.setNetworkLimitation(sourceBenefit.getNetworkLimitation());
						targetBenefit.setIsEHB(sourceBenefit.getIsEHB());
						targetBenefit.setIsCovered(sourceBenefit.getIsCovered());
						targetBenefit.setMinStay(sourceBenefit.getMinStay());
						targetBenefit.setExplanation(sourceBenefit.getExplanation());
						targetBenefit.setSubjectToInNetworkDuductible(sourceBenefit.getSubjectToInNetworkDuductible());
						targetBenefit.setSubjectToOutNetworkDeductible(sourceBenefit.getSubjectToOutNetworkDeductible());
						targetBenefit.setExcludedFromInNetworkMoop(sourceBenefit.getExcludedFromInNetworkMoop());
						targetBenefit.setExcludedFromOutOfNetworkMoop(sourceBenefit.getExcludedFromOutOfNetworkMoop());
						if(targetFromDB && null != entityManager && entityManager.isOpen()) {
							targetBenefit = entityManager.merge(targetBenefit);
						}					
					}
				}
			}
			processedBenefitName.clear();
			processedBenefitName = null;
		}
		LOGGER.info("copyCommonPlanDentalData End");
	}

	private Plan updateNonCertifiedCommonTemplatesData(int applicableYear, Plan templatePlan, Plan targetPlan,
			PlanAndBenefitsPackageVO planAndBenefitsPackageVO, PlanAndBenefitsVO planBenefitTemplate,
			List<IssuerServiceArea> issuerServiceAreas, Map<String, Network> networks,
			Map<String, List<PlanRate>> planRates, Map<String, Formulary> savedFormulary, Issuer issuer,
			EntityManager entityManager) throws GIException {
		
		LOGGER.info("updateNonCertifiedCommonTemplatesData Start");
		
		try {
			boolean isDentalPlanOnly = SerffConstants.YES.equalsIgnoreCase(
					planAndBenefitsPackageVO.getHeader().getDentalPlanOnlyInd().getCellValue());
			copyCommonPlanData(isDentalPlanOnly, templatePlan, false, targetPlan, true, entityManager);
			
			LOGGER.debug("Adding new IssuerServiceArea to existing plan variant.");
			if (!CollectionUtils.isEmpty(issuerServiceAreas)
					&& null != planBenefitTemplate.getPlanAttributes().getServiceAreaID()
					&& StringUtils.isNotEmpty(planBenefitTemplate.getPlanAttributes().getServiceAreaID().getCellValue())) {
				
				String serviceAreaID = planBenefitTemplate.getPlanAttributes().getServiceAreaID().getCellValue();
	
				for (IssuerServiceArea issuerServiceArea : issuerServiceAreas) {
					
					if (serviceAreaID.equalsIgnoreCase(issuerServiceArea.getSerffServiceAreaId())) {
						targetPlan.setServiceAreaId(issuerServiceArea.getId());
						break;
					}
				}
			}
			
			LOGGER.debug("Adding new Network to existing plan variant.");
			if(!CollectionUtils.isEmpty(networks)
					&& null != planBenefitTemplate.getPlanAttributes().getNetworkID()) {
				
				Network network = networks.get(planBenefitTemplate.getPlanAttributes().getNetworkID().getCellValue());
				
				if (network != null) {
					targetPlan.setProviderNetworkId(network.getId());
					targetPlan.setNetwork(network);
				}
				else {
					LOGGER.error("No network found for networkid: " +  planBenefitTemplate.getPlanAttributes().getNetworkID());
				}
			}
			
			LOGGER.debug("Adding new Prescription Drug(Formulary) to existing plan variant.");
			if(null != planBenefitTemplate.getPlanAttributes().getFormularyID()) {
				targetPlan.setFormularlyId(planMgmtServiceImpl.validatePlanFormularyId(applicableYear, planBenefitTemplate.getPlanAttributes().getFormularyID().getCellValue(),
						savedFormulary, issuer.getId()));
			}
			
			LOGGER.info("Adding new Recieved Rates for PlanID: " + targetPlan.getIssuerPlanNumber() + " Rate count: " + planRates.size());

			LOGGER.info("Processing of Rates begin PlanID: " + targetPlan.getIssuerPlanNumber());
			if (null != targetPlan.getIssuerPlanNumber()) {
				
				String newPlanId = targetPlan.getIssuerPlanNumber().substring(0, SerffConstants.LEN_HIOS_PLAN_ID);
				
				if (!CollectionUtils.isEmpty(planRates.get(newPlanId))) {
					
					List<PlanRate> rates = planRates.get(newPlanId);
					List<PlanRate> ratesForPlan = new ArrayList<PlanRate>();
					List<PlanRate> existingRates = iPlanRateRepository.findByPlan_IdAndIsDeleted(targetPlan.getId() ,PlanRate.IS_DELETED.N.toString());
					
					if (null != existingRates
							&& !existingRates.isEmpty()) {
						
						for (PlanRate rate : existingRates) {
								rate.setIsDeleted(PlanRate.IS_DELETED.Y.toString());
								ratesForPlan.add(rate);
						}
					}				
					for(PlanRate rate : rates){
						PlanRate tmpRate = rate.clone();
						tmpRate.setPlan(targetPlan);
						ratesForPlan.add(tmpRate);
					}
					targetPlan.setPlanRate(ratesForPlan);
					
					if (isDentalPlanOnly) {
						//HIX-63650 : Change rates persistence code to override child max age for rates in the template.
						ratesForPlan = serffUtils.preProcessDentalPlanRates(ratesForPlan);
						targetPlan.setPlanRate(ratesForPlan);
						
						targetPlan.getPlanDental().setRateEffectiveDate(rates.get(0).getEffectiveStartDate());
					}
					else {
						targetPlan.getPlanHealth().setRateEffectiveDate(rates.get(0).getEffectiveStartDate());
					}
				}
			}
			LOGGER.info("Processing of Rates ends PlanID: " + targetPlan.getIssuerPlanNumber());
		}
		finally {
			LOGGER.info("updateNonCertifiedCommonTemplatesData ends");
		}
		return targetPlan;
	}

	/**
	 * This method updates all templates data for the NON-CERTIFIED plans.
	 * @author Geetha Chandran
	 * @param persistOnExchangePlans 
	 * @param persistOffExchangePlans 
	 * @throws GIException
	 * @throws ParseException
	 * @since 16 Aug, 2013
	 */
	public List<Plan> updateNonCertifiedPlans(int applicableYear, String planInsuranceType, String issuerID, PlanAndBenefitsPackageVO planAndBenefitsPackageVO,
			PlanAndBenefitsVO planBenefitTemplate, Map<String, Object> reqTemplates, Map<String, Object> templateObjectMap,
			boolean persistOffExchangePlans, boolean persistOnExchangePlans, EntityManager entityManager, StringBuffer responseMessage,
			List<Tenant> tenantList, String exchangeTypeFilter ,Map<String ,Date> lastUpdatedTemplateMap, Map<String, Date> templatesLastModifiedDates, 
			PlanUpdateStatistics uploadStatistics, PlanDisplayRulesMap planDispRulesMap, byte[] logoBytes) throws GIException{

		List<Plan> savedPlans = null;
		String planId= null;
		Issuer issuer = null;
		Map<String, List<PlanRate>> planRates = null;
		List<IssuerServiceArea> issuerServiceArea = null;
		Map<String, Network> networks = null;
		Map<String, Formulary> savedFormulary = null;

		if (null == entityManager || !entityManager.isOpen()) {
			return savedPlans;
		}
		
		if(null != planBenefitTemplate && null != planBenefitTemplate.getPlanAttributes() && null != planBenefitTemplate.getPlanAttributes().getStandardComponentID()){
			planId= planBenefitTemplate.getPlanAttributes().getStandardComponentID().getCellValue();
		}
		else{
			LOGGER.error("Error while updating NonCertified Plans. Plan ID cannot be null.");
			throw new GIException(Integer.parseInt(SerffConstants.NO_DATA_CODE), SerffConstants.NO_DATA_MESSAGE_EMPTY_DATA+" : Plan ID cannot is null.", "");
		}

		SerffTemplates serffTemplate = iSerffTemplatesRepository.findByPlanIdAndApplicableYear(planId, applicableYear);
		if (null == serffTemplate) {
			serffTemplate = createSerffTemplates(applicableYear, planId);
		}
		
		//Update Admin template
		if(!templateObjectMap.containsKey(SerffConstants.DATA_ADMIN_DATA)) {
			LOGGER.info("Fetching DATA_ADMIN_DATA for Issuer: " + issuerID + MSG_PLAN_ID + planId);
			issuer = updateIssuerForNonCertifiedPlans(entityManager, planId, issuerID, reqTemplates, serffTemplate,
					templatesLastModifiedDates, lastUpdatedTemplateMap, logoBytes);
			templateObjectMap.put(SerffConstants.DATA_ADMIN_DATA, issuer);

			updateURACForNonCertifiedPlans(entityManager, issuer, reqTemplates, serffTemplate, lastUpdatedTemplateMap);
			updateNCQAForNonCertifiedPlans(entityManager, issuer, reqTemplates, serffTemplate, lastUpdatedTemplateMap);
		}
		else {
			issuer = (Issuer) templateObjectMap.get(SerffConstants.DATA_ADMIN_DATA);
		}

		if(!templateObjectMap.containsKey(SerffConstants.DATA_NETWORK_ID)) {
			//Update Network template
			LOGGER.info("Fetching DATA_NETWORK_ID - Issuer: " + issuerID + MSG_PLAN_ID + planId);
			templateObjectMap.put(SerffConstants.DATA_NETWORK_ID, updateNetworkForNonCertifiedPlan(applicableYear, issuer ,reqTemplates ,serffTemplate ,entityManager ,templatesLastModifiedDates ,planId , lastUpdatedTemplateMap));
		}
		
		if (null != templateObjectMap.get(SerffConstants.DATA_NETWORK_ID)) {
			networks = (Map<String, Network>) templateObjectMap.get(SerffConstants.DATA_NETWORK_ID);
		}

		if(!templateObjectMap.containsKey(SerffConstants.DATA_SERVICE_AREA)) {
			//Update ServiceArea template
			LOGGER.info("Fetching DATA_SERVICE_AREA - Issuer: " + issuerID + MSG_PLAN_ID + planId);
			templateObjectMap.put(SerffConstants.DATA_SERVICE_AREA, updateSvcAreaNonCertifiedPlans(applicableYear, planId, issuerID,reqTemplates, serffTemplate, templatesLastModifiedDates, entityManager , lastUpdatedTemplateMap));
		}
		
		if (null != templateObjectMap.get(SerffConstants.DATA_SERVICE_AREA)) {
			issuerServiceArea = (List<IssuerServiceArea>) templateObjectMap.get(SerffConstants.DATA_SERVICE_AREA);
		}

		if(!templateObjectMap.containsKey(SerffConstants.DATA_PRESCRIPTION_DRUG)) {
			//Update PrescriptionDrug template
			LOGGER.info("Fetching DATA_PRESCRIPTION_DRUG - Issuer: " + issuerID + MSG_PLAN_ID + planId);
			savedFormulary = updatePrescriptionDrugForNonCertifiedPlan(applicableYear, planId ,issuer ,reqTemplates, serffTemplate, templatesLastModifiedDates, entityManager ,lastUpdatedTemplateMap );
			
			if (savedFormulary != null) {
				templateObjectMap.put(SerffConstants.DATA_PRESCRIPTION_DRUG, savedFormulary);
			}
		}
		else if (null != templateObjectMap.get(SerffConstants.DATA_PRESCRIPTION_DRUG)) {
			savedFormulary = (Map<String, Formulary>) templateObjectMap.get(SerffConstants.DATA_PRESCRIPTION_DRUG);
		}
		
		if(!templateObjectMap.containsKey(SerffConstants.DATA_RATING_TABLE)) {
			//Update Rates template
			LOGGER.info("Fetching DATA_RATING_TABLE - Issuer: " + issuerID + MSG_PLAN_ID + planId);
			templateObjectMap.put(SerffConstants.DATA_RATING_TABLE, updateRatesForNonCertifiedPlans(applicableYear, planInsuranceType, planId, issuer,reqTemplates, serffTemplate, templatesLastModifiedDates, entityManager ,lastUpdatedTemplateMap));
		}
		
		if (null != templateObjectMap.get(SerffConstants.DATA_RATING_TABLE)) {
			planRates = (Map<String, List<PlanRate>>) templateObjectMap.get(SerffConstants.DATA_RATING_TABLE);
		}
		//Update Plan template
		savedPlans = updateNonCertifiedPlanAndBenefit(applicableYear, planInsuranceType, reqTemplates, planAndBenefitsPackageVO, planBenefitTemplate,
				issuerServiceArea, networks, planRates, persistOffExchangePlans, persistOnExchangePlans, savedFormulary, planId,
				serffTemplate, issuer, templatesLastModifiedDates, entityManager, responseMessage, tenantList, exchangeTypeFilter,
				lastUpdatedTemplateMap, uploadStatistics, planDispRulesMap);

		if (null == savedPlans || savedPlans.isEmpty()) {
			//update serff_tempalte table
			updateSerffTemplate(applicableYear, planId, templatesLastModifiedDates, false, entityManager ,lastUpdatedTemplateMap );
		}
		return savedPlans;
	}

	/**
	 * This method updates Admin templates data for the NON-CERTIFIED plans.
	 * @author Geetha Chandran
	 * @param entityManager
	 * @param serffTemplate
	 * @throws GIException,ParseException
	 * @return Updated Issuer object
	 * @since 16 Aug, 2013
	 */
	@SuppressWarnings("unchecked")
	private Issuer updateIssuerForNonCertifiedPlans(EntityManager entityManager, String planId, String issuerID, Map<String, Object> reqTemplates, SerffTemplates serffTemplate, Map<String, Date> templatesLastModifiedDates , Map<String ,Date> lastUpdatedTemplateMap, byte[] logoBytes) throws GIException{

		Map<String, String> documents = null;
		Issuer issuer = null;
		Object object = null;

		if (null != serffTemplate) {
			Date savedAdminLMD = serffTemplate.getAdminLastModifiedDate();
			Date currentAdminLMD = templatesLastModifiedDates.get(SerffConstants.DATA_ADMIN_DATA);

			if (lmdDateCompare(savedAdminLMD, currentAdminLMD)) {
				LOGGER.info("Admin data is modified for existing plans Issuer: " + issuerID + MSG_PLAN_ID + planId);
				object = reqTemplates.get(SerffConstants.SUPPORTING_DOC);
				documents =	(HashMap<String, String>) object;

				com.serff.template.admin.extension.PayloadType adminTemplate = null;
				object = reqTemplates.get(SerffConstants.DATA_ADMIN_DATA);
				if (null != object) {
					adminTemplate = (com.serff.template.admin.extension.PayloadType) object;
				}
				issuer = planMgmtServiceImpl.saveIssuerBean(entityManager, adminTemplate, issuerID, null, documents, logoBytes);
				lastUpdatedTemplateMap.put(SerffConstants.DATA_ADMIN_DATA , null);
			}
			else{
				LOGGER.info("Admin data is not modified for existing plans. Loading existing issuer. Issuer: " + issuerID + MSG_PLAN_ID + planId);
				templatesLastModifiedDates.put(SerffConstants.DATA_ADMIN_DATA, savedAdminLMD);
				lastUpdatedTemplateMap.put(SerffConstants.DATA_ADMIN_DATA, serffTemplate.getAdminLastUpdated());
				issuer = planMgmtServiceImpl.saveIssuerBean(entityManager, null, issuerID, null, documents, logoBytes);
			}
		}
		return issuer;
	}

	/**
	 * This method updates URAC template data for the NON-CERTIFIED plans.
	 * 
	 * @throws GIException,ParseException
	 */
	private void updateURACForNonCertifiedPlans(EntityManager entityManager, Issuer issuer, Map<String, Object> reqTemplates,
			SerffTemplates serffTemplate, Map<String, Date> lastUpdatedTemplateMap) throws GIException {

		if (null == serffTemplate) {
			return;
		}

		com.serff.template.urac.IssuerType uracInfo = null;
		Object object = reqTemplates.get(SerffConstants.DATA_URAC_DATA);
		if (null != object) {
			lastUpdatedTemplateMap.put(SerffConstants.DATA_URAC_DATA , null);
			uracInfo = (com.serff.template.urac.IssuerType) object;
			LOGGER.debug("Persisting URAC template");
			planMgmtServiceImpl.populateAndPersistURACTemplate(uracInfo, issuer, entityManager);
		}
	}

	/**
	 * This method updates NCQA template data for the NON-CERTIFIED plans.
	 * 
	 * @throws GIException,ParseException
	 */
	private void updateNCQAForNonCertifiedPlans(EntityManager entityManager, Issuer issuer, Map<String, Object> reqTemplates,
			SerffTemplates serffTemplate, Map<String, Date> lastUpdatedTemplateMap) throws GIException {

		if (null == serffTemplate) {
			return;
		}

		com.serff.template.ncqa.IssuerType ncqaInfo = null; 
		Object object = reqTemplates.get(SerffConstants.DATA_NCQA_DATA);
		if (null != object) {
			lastUpdatedTemplateMap.put(SerffConstants.DATA_NCQA_DATA , null);
			ncqaInfo = (com.serff.template.ncqa.IssuerType) object;
			LOGGER.debug("Persisting NCQA template");
			planMgmtServiceImpl.populateAndPersistNcqaTemplate(ncqaInfo, issuer, entityManager);
		}
	}

	/**
	 * This method set all Rate data for the NON-CERTIFIED plans to 'Y'
	 * @author Geetha Chandran
	 * @throws GIException,ParseException
	 * @return Map containing Updated rates
	 * @since 16 Aug, 2013
	 */
	private Map<String, List<PlanRate>> updateRatesForNonCertifiedPlans(int applicableYear, String planInsuranceType, String planId, Issuer issuer,
			Map<String, Object> reqTemplates, SerffTemplates serffTemplate, Map<String, Date> templatesLastModifiedDates,
			EntityManager entityManager ,Map<String ,Date> lastUpdatedTemplateMap) throws GIException {

		Map<String, List<PlanRate>> planRates = null;
		Object object = null;

		if (null == entityManager || !entityManager.isOpen()
				|| StringUtils.isBlank(planId)) {
			return planRates;
		}

		if (null != serffTemplate) {
			
			if (null == issuer) {
				return planRates;
			}
			
			//get all issuer plans
			String issuerID = issuer.getHiosIssuerId();
			List<Plan> existingPlan = iPlanRepository.findByIssuerAndIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYearAndInsuranceType(issuer,Plan.IS_DELETED.N.toString(), planId.substring(0, (SerffConstants.LEN_HIOS_ISSUER_ID + SerffConstants.LEN_ISSUER_STATE)), applicableYear, planInsuranceType);
			Date savedRateLMD = serffTemplate.getRatesLastModifiedDate();
			Date currentRatesLMD = templatesLastModifiedDates.get(SerffConstants.DATA_RATING_TABLE);
			planRates = new HashMap<String, List<PlanRate>>();

			if (lmdDateCompare(savedRateLMD, currentRatesLMD)) {
				
				LOGGER.info("Rates data is modified for existing plans - Issuer: " + issuerID + MSG_PLAN_ID + planId);
				object = reqTemplates.get(SerffConstants.DATA_RATING_TABLE);
				QhpApplicationRateGroupVO ratesTemplate = (QhpApplicationRateGroupVO) object;

				LOGGER.info("Fetching new Rates list for plans begins Issuer: " + issuerID + MSG_PLAN_ID + planId);
				planRates = planMgmtServiceImpl.savePlanRateBean(entityManager, issuer.getStateOfDomicile(), ratesTemplate);
				LOGGER.info("Fetching new Rates list for plans ends Issuer: " + issuerID + MSG_PLAN_ID + planId);
				Date savedLMD = serffTemplate.getPlanLastModifiedDate();
				Date currentLMD = templatesLastModifiedDates.get(SerffConstants.DATA_BENEFITS);
				lastUpdatedTemplateMap.put(SerffConstants.DATA_RATING_TABLE , null);
				if (!lmdDateCompare(savedLMD, currentLMD)) {

					LOGGER.info("Deleting existing Rates record begins");
					
					if (null != existingPlan && !existingPlan.isEmpty()) {
						
						for (Plan plan : existingPlan){
							
							String newPlanId = plan.getIssuerPlanNumber().substring(0, SerffConstants.LEN_HIOS_PLAN_ID);
							
							if (!CollectionUtils.isEmpty(planRates.get(newPlanId))) {
								LOGGER.debug("Deleting existing Rates record with plan id = " + plan.getId());
								
								List<PlanRate> newRates = planRates.get(newPlanId);
								//HIX-63650 : Change rates persistence code to override child max age for rates in the template.
								if(Plan.PlanInsuranceType.DENTAL.toString().equals(plan.getInsuranceType())){
									newRates = serffUtils.preProcessDentalPlanRates(newRates);
								}
								
								List<PlanRate> existingRates = iPlanRateRepository.findByPlan_IdAndIsDeleted(plan.getId() ,PlanRate.IS_DELETED.N.toString());
								
								if (null != existingRates
										&& !existingRates.isEmpty()) {
									
									for (PlanRate rate : existingRates) {
											rate.setIsDeleted(PlanRate.IS_DELETED.Y.toString());
											entityManager.merge(rate);
										
									}
									LOGGER.debug("Marked all existing Rates for soft deletion with plan id = " + plan.getId());
								}				
								for(PlanRate rate : newRates){
									PlanRate tmpRate = rate.clone();
									tmpRate.setPlan(plan);
									entityManager.merge(tmpRate);
								}
								LOGGER.info("Updated rated record as deleted for plan = " + plan.getId());
							}
						}
					}
					LOGGER.info("Deleting existing Rates record ends Issuer: " + issuerID + MSG_PLAN_ID + planId);
				}
			}
			else {
				
				LOGGER.info("Rates data is not modified for existing plans Issuer: " + issuerID + MSG_PLAN_ID + planId);
				templatesLastModifiedDates.put(SerffConstants.DATA_RATING_TABLE, savedRateLMD);
				lastUpdatedTemplateMap.remove(SerffConstants.DATA_RATING_TABLE);
				
				if (null != existingPlan && !existingPlan.isEmpty()) {
					
					String issuerPlanNumber = null;
					
					for (Plan plan : existingPlan) {
						
						if (null != plan) {
							plan.setIssuer(issuer);
							issuerPlanNumber = plan.getIssuerPlanNumber().substring(0, SerffConstants.LEN_HIOS_PLAN_ID);
							if(CollectionUtils.isEmpty(planRates.get(issuerPlanNumber))) {
								List<PlanRate> existingRates = iPlanRateRepository.findByPlan_IdAndIsDeleted(plan.getId(),PlanRate.IS_DELETED.N.toString());
								//plan.setIssuer(issuer);
								
								if (null != existingRates
										&& !existingRates.isEmpty()) {
									planRates.put(issuerPlanNumber, existingRates);
								}
							}
						}
					}
				}
			}
		}
		return planRates;
	}


	/**
	 * This method updates the Service Area data for the NON-CERTIFIED plans
	 * @author Geetha Chandran
	 * @throws GIException,ParseException
	 * @return List of updated service area data
	 * @since 16 Aug, 2013
	 */
	private List<IssuerServiceArea> updateSvcAreaNonCertifiedPlans (int applicableYear, String planId, String issuerID, Map<String, Object> reqTemplates, SerffTemplates serffTemplate, Map<String, Date> templatesLastModifiedDates, EntityManager entityManager ,Map<String ,Date> lastUpdatedTemplateMap ) throws GIException{

		Object object = null;
		List<IssuerServiceArea> issuerServiceAreas = new ArrayList<IssuerServiceArea>();
		IssuerServiceArea issuerServiceArea = null;
		List<ServiceAreaType> issuerServiceAreaList = null;
		String stateCode = null, servicAreaId = null;

		if(null != serffTemplate){
			Date savedSvcAreaLMD = serffTemplate.getSvcAreaLastModifiedDate();
			Date currentSvcAreaLMD = templatesLastModifiedDates.get(SerffConstants.DATA_SERVICE_AREA);

			object = reqTemplates.get(SerffConstants.DATA_SERVICE_AREA);
			com.serff.template.svcarea.extension.PayloadType serviceAreaTemplate =
					(com.serff.template.svcarea.extension.PayloadType) object;

			if(null != serviceAreaTemplate && null != serviceAreaTemplate.getIssuer() && null != serviceAreaTemplate.getIssuer().getIssuerStateCode() && null != serviceAreaTemplate.getIssuer().getIssuerStateCode().getValue()) {
				stateCode = serviceAreaTemplate.getIssuer().getIssuerStateCode().getValue().value();
			}

			if(lmdDateCompare(savedSvcAreaLMD, currentSvcAreaLMD)){
				LOGGER.info("Service Area data is modified for existing plans  Issuer: " + issuerID + MSG_PLAN_ID + planId + MSG_STATE_CODE + stateCode);

				if(null != serviceAreaTemplate && null != serviceAreaTemplate.getIssuer()){
					Set<String> svcAreaIdSet = new HashSet<String>();
					issuerServiceAreaList = serviceAreaTemplate.getIssuer().getServiceArea();

					for(ServiceAreaType serviceAreaType : issuerServiceAreaList) {
						servicAreaId = serviceAreaType.getServiceAreaIdentification().getIdentificationID().getValue();
						
						if (StringUtils.isNotBlank(servicAreaId)) {
							svcAreaIdSet.add(servicAreaId);
						}
					}
					for (String svcAreaId : svcAreaIdSet) {
						LOGGER.debug("Checking if PM_ISSUER_SERVICE_AREA already have data for provided state,issuerHiosID and ServiceAreadId Issuer: " + issuerID + " svcAreaId: " + svcAreaId + MSG_STATE_CODE + stateCode);
						issuerServiceAreas = iIssuerServiceAreaRepository.findbyStateIssuerAndServAreaAndApplicableYear(stateCode, issuerID, svcAreaId, applicableYear);

						if(!CollectionUtils.isEmpty(issuerServiceAreas)){
							//Ideally there should be only one record.Still using list to avoid any junk data
							LOGGER.debug("Record already exist in PM_ISSUER_SERVICE_AREA for provided state,issuerHiosID and ServiceAreadId Issuer: " + issuerID + " svcAreaId: " + svcAreaId + MSG_STATE_CODE + stateCode);
							issuerServiceArea = issuerServiceAreas.get(0);

							List<IssuerServiceAreaExt> issuerServiceAreaExts = issuerServiceArea.getIssuerServiceAreaExt();

							if(!CollectionUtils.isEmpty(issuerServiceAreaExts)){
								for (IssuerServiceAreaExt issuerServiceAreaExt : issuerServiceAreaExts){
									LOGGER.debug("Marking soft deleted IssuerServiceAreaExt with id " + issuerServiceAreaExt.getId());
									issuerServiceAreaExt.setIsDeleted(IssuerServiceAreaExt.IS_DELETED.Y.toString());
									entityManager.merge(issuerServiceAreaExt);

									
									List<ServiceArea> serviceAreas = iServiceAreaRepository.findByServiceAreaIdAndServiceAreaExtIdAndIsDeleted(issuerServiceArea, issuerServiceAreaExt ,ServiceArea.IS_DELETED.N.toString());
								
									if(!CollectionUtils.isEmpty(serviceAreas)){
										for (ServiceArea serviceArea : serviceAreas){
											LOGGER.debug("Marking soft deleted ServiceArea with id " + serviceArea.getId());
											serviceArea.setIsDeleted(ServiceArea.IS_DELETED.Y.toString());
											entityManager.merge(serviceArea);
										}
									}
								}
							}
						}
					}
				}
				issuerServiceAreas = planMgmtServiceImpl.saveIssuerServiceAreaBean(applicableYear, entityManager, serviceAreaTemplate,issuerID);
				lastUpdatedTemplateMap.put(SerffConstants.DATA_SERVICE_AREA , null);
			}
			else{
				LOGGER.info("Service Area data is not modified for existing plans Issuer: " + issuerID + MSG_STATE_CODE + stateCode);
				templatesLastModifiedDates.put(SerffConstants.DATA_SERVICE_AREA, savedSvcAreaLMD);
				lastUpdatedTemplateMap.put(SerffConstants.DATA_SERVICE_AREA, serffTemplate.getServiceAreaLastUpdated());
				issuerServiceAreas = iIssuerServiceAreaRepository.findbyStateAndIssuerAndApplicableYear(stateCode, issuerID, applicableYear);
			}
		}
		return issuerServiceAreas;
	}

	private boolean lmdDateCompare(Date savedLMD, Date currentLMD) {

		boolean flag = false;
		LOGGER.debug("Saved LMD: " + savedLMD  + " Current LMD: " + currentLMD);
		
		if ((null == savedLMD) || (null != currentLMD && !savedLMD.after(currentLMD))) {
			flag = true;
		}
		return flag;
	}


	/**
	 * This method updates the Service Area data for the NON-CERTIFIED plans
	 * @author Vani Sharma
	 * @throws GIException,ParseException
	 * @return Map of updated Network
	 * @since 17 Aug, 2013
	 */
	private Map <String, Network> updateNetworkForNonCertifiedPlan(int applicableYear, Issuer issuer ,Map<String, Object> reqTemplates ,SerffTemplates serffTemplate,
			  EntityManager entityManager ,Map<String, Date> templatesLastModifiedDates , String planId ,Map<String ,Date> lastUpdatedTemplateMap) throws GIException{

		//PayloadType networkProvider =(PayloadType)reqTemplates.get("NETWORK") ;
		String issuerId = issuer.getHiosIssuerId();
		LOGGER.info("updateNonCertifiedPlansforNetworkTemplate begins Issuer: " + issuerId + MSG_PLAN_ID + planId );

		Object object = null;
		Map <String, Network> savedNetwork = new HashMap<String, Network>();
		
		if (null != serffTemplate) {
			
			Date savedNetworkLMD = serffTemplate.getNetworkLastModifiedDate();
			Date currentNetworkLMD = templatesLastModifiedDates.get(SerffConstants.DATA_NETWORK_ID);
			if (lmdDateCompare(savedNetworkLMD, currentNetworkLMD)) {
				LOGGER.info("Network are modified to existing plans Issuer: " + issuerId + MSG_PLAN_ID + planId );
				object = reqTemplates.get(SerffConstants.DATA_NETWORK_ID);
				com.serff.template.networkProv.extension.PayloadType networkProvider =(com.serff.template.networkProv.extension.PayloadType) object;
				savedNetwork= planMgmtServiceImpl.populateAndPersistNetworkBean(applicableYear, entityManager,issuer, networkProvider);
				lastUpdatedTemplateMap.put(SerffConstants.DATA_NETWORK_ID , null);
			}
			else {
				LOGGER.info("Network is not modified for existing plan Issuer: " + issuerId + MSG_PLAN_ID + planId );
				templatesLastModifiedDates.put(SerffConstants.DATA_NETWORK_ID, savedNetworkLMD);
				lastUpdatedTemplateMap.put(SerffConstants.DATA_NETWORK_ID, serffTemplate.getNetworkLastUpdated());
				List<Network> networks = iNetworkRepository.getProviderNetworkByIssuerIdAndApplicableYear(issuer.getId(), applicableYear);
				for (Network network : networks) {
					network.setIssuer(issuer);
					savedNetwork.put(network.getNetworkID(), network);
				}
			}
		}
		return savedNetwork;
	}

	
	/**
	 * This method updates the Service Area data for the NON-CERTIFIED plans
	 * @author Vani Sharma
	 * @return Map of updated Formualry
	 * @since 17 Aug, 2013
	 */
	private Map<String, Formulary> updatePrescriptionDrugForNonCertifiedPlan(int applicableYear, String planId, Issuer issuer, Map<String, Object> reqTemplates,
			SerffTemplates serffTemplate, Map<String, Date> templatesLastModifiedDates, EntityManager entityManager ,Map<String ,Date> lastUpdatedTemplateMap) {
		
		LOGGER.info("Executing updatePrescriptionDrugForNonCertifiedPlan() : planId : " + planId);
		Map<String, Formulary> savedFormulary = new HashMap<String, Formulary>();
		List<Formulary> formularyList = null;

		if (null != serffTemplate) {
			Date savedDrugLMD = serffTemplate.getPresDrugLastModifiedDate();
			Date currentDrugLMD = templatesLastModifiedDates.get(SerffConstants.DATA_PRESCRIPTION_DRUG);
			
			com.serff.template.plan.PrescriptionDrugTemplateVO prescriptionDrug = (com.serff.template.plan.PrescriptionDrugTemplateVO)reqTemplates.get(SerffConstants.DATA_PRESCRIPTION_DRUG);
			
			if (null != prescriptionDrug) {
				if(null != issuer){
					String issuerHiosID = issuer.getHiosIssuerId();
					LOGGER.info("updatePrescriptionDrugForNonCertifiedPlan() : fetching formulary list for issuer.getId() : " + issuer.getId());
					formularyList = iSerffFormularyRepository.findByIssuer_IdAndApplicableYear(issuer.getId(), applicableYear);
					
					if (lmdDateCompare(savedDrugLMD, currentDrugLMD)) {
						//Save new formulary in db.
						savedFormulary = planMgmtServiceImpl.populateAndPersistPrescriptionDrugBean(applicableYear, entityManager, issuer, prescriptionDrug);
						lastUpdatedTemplateMap.put(SerffConstants.DATA_PRESCRIPTION_DRUG , null);
						
					}else{
						templatesLastModifiedDates.put(SerffConstants.DATA_PRESCRIPTION_DRUG, savedDrugLMD);
						lastUpdatedTemplateMap.put(SerffConstants.DATA_PRESCRIPTION_DRUG, serffTemplate.getPresDrugLastUpdated());
					}
					
					//If the formulary template is not modified, or nothing new saved, return the existing list of formulary.
					if(null != formularyList && !formularyList.isEmpty() && null != savedFormulary && savedFormulary.isEmpty()) {
						LOGGER.info("updatePrescriptionDrugForNonCertifiedPlan() : New Recored not found for Formulary... Issuer: " + issuerHiosID + MSG_PLAN_ID + planId );
						for (Formulary formulary : formularyList) {
							formulary.setIssuer(issuer);
							savedFormulary.put(formulary.getFormularyId(), formulary);
						}		
					}
					
				}else{
					LOGGER.error("updatePrescriptionDrugForNonCertifiedPlan() : issuer is null for planId : " + planId);
				}
				
			} else {
				formularyList = iSerffFormularyRepository.findByIssuer_IdAndApplicableYear(issuer.getId(), applicableYear);
				if(!CollectionUtils.isEmpty(formularyList)) {
					for (Formulary formulary : formularyList) {
						savedFormulary.put(formulary.getFormularyId(), formulary);
					}
				}
			}
			
		}
		
		return savedFormulary;
	}
	
	
	private boolean anyPlanDataUpdated(Map<String ,Date> lastUpdatedTemplateMap) {
		if(null != lastUpdatedTemplateMap && !lastUpdatedTemplateMap.isEmpty()) {
			String[] templateNames = {SerffConstants.DATA_RATING_TABLE, SerffConstants.DATA_ADMIN_DATA, SerffConstants.DATA_SERVICE_AREA , SerffConstants.DATA_NETWORK_ID , SerffConstants.DATA_PRESCRIPTION_DRUG };
			for (String template : templateNames) {
				if(lastUpdatedTemplateMap.containsKey(template) && lastUpdatedTemplateMap.get(template) == null) {
					return true;
				}
			}
		}
		return false;
	}
}

