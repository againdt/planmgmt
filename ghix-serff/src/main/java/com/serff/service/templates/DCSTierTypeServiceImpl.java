/**
 * File 'DCSTierTypeServiceImpl' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:20:53 AM
 */
package com.serff.service.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.DCSTierType;
import com.serff.repository.templates.IDCSTierTypeRepository;

/**
 * Service implementor 'DCSTierTypeServiceImpl' to implement service methods of 'DCSTierTypeService'.
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:20:53 AM
 *
 */
@Service("dcsTierTypeService")
@Repository
@Transactional
public class DCSTierTypeServiceImpl implements DCSTierTypeService {
	
	//private static final Logger LOGGER = LoggerFactory.getLogger(DCSTierTypeServiceImpl.class);

	@Autowired
	private IDCSTierTypeRepository dcsTierTypeRepository;

	/**
	 * Constructor.
	 */
	public DCSTierTypeServiceImpl() {
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.DCSTierTypeService#saveDCSTierType(com.getinsured.hix.model.DCSTierType)
	 */
	@Override
	public DCSTierType saveDCSTierType(DCSTierType dcsTierType) {
		return dcsTierTypeRepository.save(dcsTierType);
	}

}
