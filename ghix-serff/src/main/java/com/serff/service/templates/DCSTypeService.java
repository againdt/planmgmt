/**
 * File 'DCSTypeService' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:46:06 AM
 */
package com.serff.service.templates;

import com.getinsured.hix.model.DCSType;

/**
 * Service interface 'DCSTypeService' to handle persistence of 'DCSType' records.
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:46:06 AM
 *
 */
public interface DCSTypeService {
	DCSType saveDCSType(DCSType dcsType);
}
