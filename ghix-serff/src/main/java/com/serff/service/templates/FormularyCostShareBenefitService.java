/**
 * File 'FormularyCostShareBenefitService.java' for publishing persistence contract for 'FormularyCostShareBenefit' records.
 *
 * @author chalse_v
 * @since Jun 14, 2013
 *
 */
package com.serff.service.templates;

import com.getinsured.hix.model.FormularyCostShareBenefit;

/**
 * Service 'FormularyCostShareBenefitService' for publishing persistence contract for 'FormularyCostShareBenefit' records.
 *
 * @author chalse_v
 * @version 1.0
 * @since Jun 14, 2013
 *
 */
public interface FormularyCostShareBenefitService {
	
	/**
	 * Method 'saveFormularyCostShareBenefit' to save FormularyCostShareBenefit data.
	 *
	 * @param formularyCostShareBenefit The FormularyCostShareBenefit instance to persist.
	 * @return FormularyCostShareBenefit The persisted FormularyCostShareBenefit instance.
	 */
	FormularyCostShareBenefit saveFormularyCostShareBenefit(FormularyCostShareBenefit formularyCostShareBenefit);
}
