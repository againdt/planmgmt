/**
 * File 'DrugCodeTierCodeTypeServiceImpl' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:35:10 AM
 */
package com.serff.service.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.DrugCodeTierCodeType;
import com.serff.repository.templates.IDrugCodeTierCodeTypeRepository;

/**
 * Class 'DrugCodeTierCodeTypeServiceImpl' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:35:10 AM
 *
 */
@Service("drugCodeTierCodeTypeService")
@Repository
@Transactional
public class DrugCodeTierCodeTypeServiceImpl implements DrugCodeTierCodeTypeService {
	
	//private static final Logger LOGGER = LoggerFactory.getLogger(DrugCodeTierCodeTypeServiceImpl.class);

	@Autowired
	private IDrugCodeTierCodeTypeRepository drugCodeTierCodeTypeRepository;
	
	/**
	 * Constructor.
	 */
	public DrugCodeTierCodeTypeServiceImpl() {
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.DrugCodeTierCodeTypeService#saveDrugCodeTierCodeType(com.getinsured.hix.model.DrugCodeTierCodeType)
	 */
	@Override
	public DrugCodeTierCodeType saveDrugCodeTierCodeType(DrugCodeTierCodeType drugCodeTierCodeType) {
		return drugCodeTierCodeTypeRepository.save(drugCodeTierCodeType);
	}

}
