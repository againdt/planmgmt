package com.serff.service.templates;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.IssuerVerificationStatus;
import com.getinsured.hix.model.PlanSTM;
import com.getinsured.hix.model.PlanSTMBenefit;
import com.getinsured.hix.model.PlanSTMCost;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.stm.STMExcelVO;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.service.validation.STMPlanValidator;
import com.serff.service.validation.SerffValidator;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * Class is used to persist STM Plans to database.
 * 
 * @author Bhavin Parmar
 * @since May 3, 2014
 */
@Service("stmPlanMgmtSerffService")
public class STMPlanMgmtSerffServiceImpl extends SerffValidator implements STMPlanMgmtSerffService {

	private static final Logger LOGGER = Logger.getLogger(STMPlanMgmtSerffServiceImpl.class);
	
	private static final String DEDUCTIBLE_NAME = "DEDUCTIBLE";
	
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private STMPlanValidator stmPlanValidator;
	@Autowired private IIssuersRepository iIssuerRepository;
	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private SerffUtils serffUtils;

	/**
	 * Method is used to populate and persist STM plans from STMExcelVO to database.
	 * @param stmVOList
	 * @param trackingRecord
	 * @param hccEntityManager
	 * @return
	 * @throws GIException
	 */
	@Override
	public boolean populateAndPersistSTMPlans(List<STMExcelVO> stmVOList, SerffPlanMgmt trackingRecord, EntityManager hccEntityManager, Map<String, Plan> hccPlanMap, String defaultTenantIds) throws GIException {
		
		LOGGER.info("populateAndPersistSTMPlans() Start");
		boolean doCommit = true;
		boolean status = Boolean.FALSE;
		EntityManager entityManager = null;
		List<Plan> savedPlanList = null;
		List<STMExcelVO> failedPlanList = null;
		Map<String, Boolean> duplicatePlansMap = null; // Map<String: IssuerPlanNumber, Boolean: Duplicate Or Not>
		
		try {
			
			if (CollectionUtils.isEmpty(stmVOList)) {
				LOGGER.error("STMExcelVO List is empty.");
				return status;
			}
			
			if (null == trackingRecord) {
				LOGGER.error("Tracking Record(SerffPlanMgmt) is null.");
				return status;
			}
			LOGGER.debug("STM Plan List" + stmVOList.size());
			
			STMExcelVO stmExcelHeader = null;
			boolean isFirstRow = true;
			List<Tenant> tenantList = null;
			
			if (null != hccEntityManager) {
				entityManager = hccEntityManager;
				doCommit = false;
			}
			else {
				entityManager = entityManagerFactory.createEntityManager(); // Get DB connection using Entity Manager Factory
				entityManager.getTransaction().begin(); // Begin Transaction
			}
			
			for (STMExcelVO stmExcelVO : stmVOList) {
				
				if (isFirstRow) {
					savedPlanList = new ArrayList<Plan>();
					failedPlanList = new ArrayList<STMExcelVO>();
					duplicatePlansMap = new HashMap<String, Boolean>();
					isFirstRow = false;
					stmExcelHeader = stmExcelVO;
					tenantList = serffUtils.getTenantList(defaultTenantIds);
				}
				else {
					LOGGER.debug("Persisting STM Plan: " + stmExcelVO.getPlanNumber());
					// Set status as TRUE even if one plan is processed successfully
					status = persistSTMPlans(savedPlanList, failedPlanList, duplicatePlansMap, stmExcelHeader, stmExcelVO, entityManager, hccPlanMap);
				}
			}
			generateErrorMessages(trackingRecord, failedPlanList, duplicatePlansMap);
			
			if (!CollectionUtils.isEmpty(savedPlanList)) {
				LOGGER.info("Number of STM Plans to be save: " + savedPlanList.size());
				for (Plan plan : savedPlanList) {
					serffUtils.saveTenantPlan(plan, tenantList, entityManager);
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
			status = Boolean.FALSE;
			throw new GIException(ex);
		}
		finally {
			
			if (doCommit) {
				status = getTransactionCommit(status, entityManager);
				releaseUnusedObjects(savedPlanList, failedPlanList, duplicatePlansMap, entityManager);
			}
			LOGGER.info("populateAndPersistSTMPlans() End");
		}
		return status;
	}

	
	
	/**
	 * Method is used to release Unused Objects(List, Map & EntityManager) for GC.
	 */
	private void releaseUnusedObjects(List<Plan> savedPlanList, List<STMExcelVO> failedPlanList,
			Map<String, Boolean> duplicatePlansMap, EntityManager entityManager) {

		if (!CollectionUtils.isEmpty(savedPlanList)){
		    savedPlanList.clear();
//		    savedPlanList = null;
		}

		if (!CollectionUtils.isEmpty(failedPlanList)) {
			failedPlanList.clear();
//			failedPlanList = null;
		}

		if (!CollectionUtils.isEmpty(duplicatePlansMap)) {
			duplicatePlansMap.clear();
//			duplicatePlansMap = null;
		}

		if (null != entityManager && entityManager.isOpen()) {
			entityManager.clear();
			entityManager.close();
//			entityManager = null;
		}
	}
	/**
	 * Method is used to generate Error messages from failed plan list and duplicate plan list.
	 */
	private void generateErrorMessages(SerffPlanMgmt trackingRecord, List<STMExcelVO> failedPlanList, Map<String, Boolean> duplicatePlansMap) {

		LOGGER.info("generateErrorMessages() Start");

		try {

			if (CollectionUtils.isEmpty(failedPlanList) && CollectionUtils.isEmpty(duplicatePlansMap)) {
				return;
			}

			StringBuilder errorMessage = new StringBuilder();

			if (!CollectionUtils.isEmpty(failedPlanList)) {
				LOGGER.error("Number of Failed STM Plans: " + failedPlanList.size());

				for (STMExcelVO stmExcelVO : failedPlanList) {
					errorMessage.append(stmExcelVO.getErrorMessages());
					errorMessage.append("\n");
				}
			}

			if (!CollectionUtils.isEmpty(duplicatePlansMap)) {
				List<String> duplicatePlansList = new ArrayList<String>();

				for (Map.Entry<String, Boolean> entryData : duplicatePlansMap.entrySet()) {

					if (null == entryData.getValue() || !entryData.getValue()) {
						continue;
					}
					duplicatePlansList.add(entryData.getKey());
				}

				if (!CollectionUtils.isEmpty(duplicatePlansList)) {
					errorMessage.append("Skipped duplicate IssuerPlanNumber(s) from Template: ");
					errorMessage.append(duplicatePlansList.toString());
				}
			}
			LOGGER.error(errorMessage.toString());
			trackingRecord.setPmResponseXml(errorMessage.toString());
		}
		finally {
			LOGGER.info("generateErrorMessages() End");
		}
	}

	/**
	 * Method is used to persist STM plans to database after validations.
	 */
	private boolean persistSTMPlans(List<Plan> savedPlanList, List<STMExcelVO> failedPlanList, Map<String, Boolean> duplicatePlansMap,
			STMExcelVO stmExcelHeader, STMExcelVO stmExcelVO, EntityManager entityManager, Map<String , Plan> hccPlanMap) {

		LOGGER.debug("persistSTMPlans() Start");
		boolean status = false;
		try {
			
			if (null == stmExcelHeader) {
				stmExcelVO.setNotValid(Boolean.TRUE);
				stmExcelVO.setErrorMessages("Failed to get STM Plan names.");
				failedPlanList.add(stmExcelVO);
				return status;
			}
			// Validates STM Excel data.
			if (stmPlanValidator.validateSTMExcelVO(stmExcelVO, failedPlanList)) {
				
				Issuer existingIssuer = null;
				String hiosIssuerId = stmExcelVO.getHiosId();
				
				if (StringUtils.isNotBlank(hiosIssuerId)) {
					existingIssuer = iIssuerRepository.getIssuerByHiosID(hiosIssuerId);
				}
				// Skipping Issuer if it does not exist.
				if (null != existingIssuer
						&& StringUtils.isNotBlank(stmExcelVO.getPlanNumber())
						&& StringUtils.isNotBlank(stmExcelVO.getPlanState())) {
					status = saveSTMPlanList(savedPlanList, failedPlanList, duplicatePlansMap, existingIssuer, stmExcelVO, stmExcelHeader, entityManager, hccPlanMap);
				}
				else {
					stmExcelVO.setNotValid(Boolean.TRUE);
					stmExcelVO.setErrorMessages("Skipping Issuer["+ hiosIssuerId +"] account because it does not exist.");
					failedPlanList.add(stmExcelVO);
				}
			} 
		}
		finally {
			LOGGER.debug("persistSTMPlans() End");
		}
		return status;
	}
	
	/**
	 * Method is used to save validated STM Plan List to DB.
	 */
	private boolean saveSTMPlanList(List<Plan> savedPlanList, List<STMExcelVO> failedPlanList, Map<String, Boolean> duplicatePlansMap,
			Issuer existingIssuer, STMExcelVO stmExcelVO, STMExcelVO stmExcelHeader, EntityManager entityManager, Map<String, Plan> hccPlanMap) {

		boolean status = Boolean.FALSE;
		LOGGER.info("saveSTMPlanList() Start");
		
		try {
			Plan savedPlan = null;
			PlanSTM savedPlanSTM = null;
			String[] planStateArray = null;
			int applicableYear = 0;//Calendar.getInstance().get(Calendar.YEAR); // Get current year for Applicable Year
			StringBuilder issuerPlanNumber = null;
			
			planStateArray = stmPlanValidator.removeDuplicateStates(stmExcelVO.getPlanState().split(SerffConstants.COMMA));
			issuerPlanNumber = new StringBuilder();
			
			if (null == planStateArray || 0 == planStateArray.length) {
				return status;
			}

			for (String planState : planStateArray) {
				
				if (StringUtils.isBlank(planState)) {
					continue;
				}
				
				planState = planState.trim();
				// Clear ISSUER_PLAN_NUMBER value
				issuerPlanNumber.setLength(0);
				// ISSUER_PLAN_NUMBER = ISSUER_HIOS_ID + STATE_ABV + PLAN_ID
				issuerPlanNumber.append(existingIssuer.getHiosIssuerId()).append(planState).append(stmExcelVO.getPlanNumber());
				// Appending Off Exchange Variant in Issuer Plan Number
				issuerPlanNumber.append(SerffConstants.OFF_EXCHANGE_VARIANT_SUFFIX);

				if (!CollectionUtils.isEmpty(duplicatePlansMap) && duplicatePlansMap.containsKey(issuerPlanNumber.toString())) {
					duplicatePlansMap.put(issuerPlanNumber.toString(), true); // Override Key with flag value 
					continue;
				}
				duplicatePlansMap.put(issuerPlanNumber.toString(), false);

				if (processExistingSTMPlan(failedPlanList, stmExcelVO, issuerPlanNumber.toString(), applicableYear, entityManager)) {
					savedPlan = savePlan(applicableYear, issuerPlanNumber.toString(), planState, existingIssuer, stmExcelVO, entityManager, hccPlanMap);

					if (null != savedPlan) {
						savedPlanSTM = savePlanSTM(savedPlan, stmExcelVO, entityManager);
						savePlanSTMCost(savedPlanSTM, stmExcelVO, entityManager);
						savePlanSTMBenefitData(savedPlanSTM, stmExcelHeader, stmExcelVO, entityManager);
						LOGGER.info("Saved STM Plan Number: " + issuerPlanNumber.toString());
						savedPlanList.add(savedPlan);
					}
				}
				//Plan is Either Saved or Ignored as it was CERTIFIED, so return TRUE
				status = Boolean.TRUE;
			}
		}
		finally {
			LOGGER.info("saveSTMPlanList() End");
		}
		return status;
	}
	
	/**
	 * Method is used to process existing STM Plans before persistence.
	 * Rule: Certified STM plan will not update.
	 */
	private boolean processExistingSTMPlan(List<STMExcelVO> failedPlanList, STMExcelVO stmExcelVO, String issuerPlanNumber, int applicableYear, EntityManager entityManager) {
		
		LOGGER.debug("processExistingSTMPlan() Start");
		boolean processSTMPlan = Boolean.TRUE;
		
		try {
			List<Plan> existingSTMPlans = iPlanRepository.findByIssuerPlanNumberStartingWithAndIsDeletedAndApplicableYearAndInsuranceType(issuerPlanNumber, Plan.IS_DELETED.N.toString(), applicableYear, Plan.PlanInsuranceType.STM.name());
			
			if (CollectionUtils.isEmpty(existingSTMPlans)) {
				return processSTMPlan;
			}
			
			boolean certifiedPlanExist = false;
			for (Plan existingSTMPlan : existingSTMPlans) {
				LOGGER.debug("STM Plan exists for given plan number " + issuerPlanNumber);
				//Soft Delete all existing plans. In case any of them is CERTIFIED, skip first such plan and soft delete others (normally there should be single such plan) 
				if (null == existingSTMPlan) {
					continue;
				}
				// Skipping CERTIFIED plans logic.
				if (Plan.PlanStatus.CERTIFIED.toString().equalsIgnoreCase(existingSTMPlan.getStatus()) && !certifiedPlanExist) {
					stmExcelVO.setNotValid(Boolean.TRUE);
					stmExcelVO.setErrorMessages("Skipping Certified STM Plan["+ issuerPlanNumber +"].");
					failedPlanList.add(stmExcelVO);
					processSTMPlan = Boolean.FALSE;
					certifiedPlanExist = Boolean.TRUE;
				}
				else {
					deleteExistingPlan(existingSTMPlan, entityManager);
				}
			}
			//Plan existingSTMPlan = existingSTMPlans.get(0);
		}
		finally {
			LOGGER.debug("processExistingSTMPlan() End");
		}
		return processSTMPlan;
	}
	
	/**
	 * Method is used to soft delete existing STM Plans [Non-Certified].
	 */
	private void deleteExistingPlan(Plan existingSTMPlan, EntityManager entityManager) {
		
		LOGGER.debug("deleteExistingPlan() Start");
		
		try {
			// If any plan does not CERTIFIED then mark plans as soft delete.
			existingSTMPlan.setIsDeleted(Plan.IS_DELETED.Y.toString());
			
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.merge(existingSTMPlan);
				LOGGER.info("Plan["+ existingSTMPlan.getIssuerPlanNumber() +"] bean has been soft deleted Successfully for State["+ existingSTMPlan.getState() +"].");
			}
		}
		finally {
			LOGGER.debug("deleteExistingPlan() End");
		}
	}
	
	/**
	 * Method is used to save Plan table.
	 */
	private Plan savePlan(int applicableYear, String issuerPlanNumber, String planState, Issuer existingIssuer, STMExcelVO stmExcelVO, 
			EntityManager entityManager, Map<String, Plan> hccPlanMap) {
		
		LOGGER.debug("savePlan() Start");
		Plan plan = null;
		
		try {
			
			if (StringUtils.isNotBlank(issuerPlanNumber)) {
				plan = new Plan();
				plan.setIssuerVerificationStatus(IssuerVerificationStatus.PENDING.toString());
				plan.setIssuer(existingIssuer);
				plan.setExchangeType(Plan.EXCHANGE_TYPE.OFF.toString());
				plan.setIsPUF(Plan.IS_PUF.N);
				plan.setIsDeleted(Plan.IS_DELETED.N.toString());
				plan.setApplicableYear(applicableYear);
				plan.setInsuranceType(Plan.PlanInsuranceType.STM.name());
				plan.setStatus(Plan.PlanStatus.LOADED.toString());
				plan.setIssuerPlanNumber(issuerPlanNumber);
				plan.setName(stmExcelVO.getPlanName());
				plan.setNetworkType(stmExcelVO.getNetworkType());
				plan.setBrochure(stmExcelVO.getPlanBrochure());
				plan.setHsa(stmExcelVO.getIsHsa());
				plan.setState(planState);
				//plan.setStartDate(startDate);
				//plan.setEndDate(endDate);
				// Plan bean has been Persisted Successfully.
				plan = (Plan) mergeEntityManager(entityManager, plan);
				if(null != hccPlanMap) {
					hccPlanMap.put(plan.getIssuerPlanNumber(), plan);
				}
				
			}
		}catch(Exception e){
			LOGGER.error("Error occured while parsing dates",e);
			
			return plan;
		}
		finally {
			LOGGER.debug("savePlan() End");
		}
		return plan;
	}
	
	/**
	 * Method is used to save PlanSTM table.
	 */
	private PlanSTM savePlanSTM(Plan savedPlan, STMExcelVO stmExcelVO, EntityManager entityManager) {
		
		LOGGER.debug("savePlanSTM() Start");
		PlanSTM planSTM = new PlanSTM();
		
		try {
			planSTM.setPlan(savedPlan);
			planSTM.setPlanResponseName(stmExcelVO.getPlanResponseName());
			planSTM.setMaxDuration(getIntegerValue(stmExcelVO.getDurationNumber()));
			planSTM.setDurationUnit(stmExcelVO.getDurationUnit());
			planSTM.setCoinsurance(getPercentageValue(stmExcelVO.getCoinsurance()));
			planSTM.setPolicyMax(getIntegerValue(stmExcelVO.getPolicyMaximum()));
			planSTM.setOutOfNetworkCoverage(stmExcelVO.getOutNetwkCoverage());
			planSTM.setOutOfCountryCoverage(stmExcelVO.getOutCountyCoverage());
			planSTM.setAdditionalInfo(stmExcelVO.getAdditionalInfo());
			planSTM.setRating(stmExcelVO.getAmBestRating());
			planSTM.setElectSignAppAvbl(stmExcelVO.getElectSignAppAvbl());
			planSTM.setApplicationFee(getFloatValue(stmExcelVO.getApplicationFee()));
			planSTM.setExclusions(stmExcelVO.getExclusionsAndLimitations());
			planSTM.setOopMaxValue(getIntegerValue(stmExcelVO.getOutOfPocketMaxIndividual()));
			planSTM.setOopMaxAttr(stmExcelVO.getOutOfPocketMaxIndividualDesc());
			planSTM.setOopMaxFmly(stmExcelVO.getOutOfPocketLimitMaxFamily());
			// PlanSTM bean has been Persisted Successfully.
			planSTM = (PlanSTM) mergeEntityManager(entityManager, planSTM);
		}
		finally {
			LOGGER.debug("savePlanSTM() End");
		}
		return planSTM;
	}
	
	/**
	 * Method is used to save List PlanSTMBenefit data using STM Excel Header.
	 */
	private void savePlanSTMBenefitData(PlanSTM savedPlanSTM, STMExcelVO stmExcelHeader, STMExcelVO stmExcelVO, EntityManager entityManager) {
		
		LOGGER.debug("savePlanSTMBenefitData() Start");
		
		try {
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getOfficeVisits(), stmExcelVO.getOfficeVisits(), stmExcelVO.getOfficeVisitsNumber(), entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getOfficeVisitsForPrimDoct(), stmExcelVO.getOfficeVisitsForPrimDoct(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getOfficeVisitsForSepcialist(), stmExcelVO.getOfficeVisitsForSepcialist(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getPreventiveCareCoverage(), stmExcelVO.getPreventiveCareCoverage(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getPeriodicHealthExam(), stmExcelVO.getPeriodicHealthExam(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getPeriodicObGynExam(), stmExcelVO.getPeriodicObGynExam(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getWellBabyCare(), stmExcelVO.getWellBabyCare(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getDrugCoverage(), stmExcelVO.getDrugCoverage(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getGenericDrugs(), stmExcelVO.getGenericDrugs(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getBrandDrugs(), stmExcelVO.getBrandDrugs(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getNonFormularyDrugsCoverage(), stmExcelVO.getNonFormularyDrugsCoverage(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getMailOrderForDrug(), stmExcelVO.getMailOrderForDrug(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getHospitalServicesCoverage(), stmExcelVO.getHospitalServicesCoverage(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getUrgentCare(), stmExcelVO.getUrgentCare(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getEmergencyRoom(), stmExcelVO.getEmergencyRoom(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getOutpatientLabXray(), stmExcelVO.getOutpatientLabXray(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getOutpatientSurgery(), stmExcelVO.getOutpatientSurgery(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getHospitalization(), stmExcelVO.getHospitalization(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getAdditionalCoverage(), stmExcelVO.getAdditionalCoverage(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getChiropracticCoverage(), stmExcelVO.getChiropracticCoverage(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getMentalHealthCoverage(), stmExcelVO.getMentalHealthCoverage(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getMaternityBenefitCoverage(), stmExcelVO.getMaternityBenefitCoverage(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getPrePostnatalOfficeVisit(), stmExcelVO.getPrePostnatalOfficeVisit(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getLaborDeliveryHospitalStay(), stmExcelVO.getLaborDeliveryHospitalStay(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getPrimCarePhysnReq(), stmExcelVO.getPrimCarePhysnReq(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getSpeciReferralsReq(), stmExcelVO.getSpeciReferralsReq(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getMailOrderGeneric(), stmExcelVO.getMailOrderGeneric(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getMailOrderBrand(), stmExcelVO.getMailOrderBrand(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getMailOrderNonFormulary(), stmExcelVO.getMailOrderNonFormulary(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getMailOrderAnnualDeductible(), stmExcelVO.getMailOrderAnnualDeductible(), null, entityManager);
			savePlanSTMBenefit(savedPlanSTM, stmExcelHeader.getMailOrderOtherCoverage(), stmExcelVO.getMailOrderOtherCoverage(), null, entityManager);
		}
		finally {
			LOGGER.debug("savePlanSTMBenefitData() End");
		}
	}
	
	/**
	 * Method is used to save PlanSTMBenefit table.
	 */
	private void savePlanSTMBenefit(PlanSTM savedPlanSTM, String benefitName, String networkDisplayValue, String networkValue, EntityManager entityManager) {
		
		LOGGER.debug("savePlanSTMBenefit() Start");
		
		try {
			
			if (StringUtils.isBlank(benefitName) || StringUtils.isBlank(networkDisplayValue)) {
				return;
			}
			
			PlanSTMBenefit planSTMBenefit = new PlanSTMBenefit();
			planSTMBenefit.setPlanSTM(savedPlanSTM);
			planSTMBenefit.setName(benefitName);
			planSTMBenefit.setNetworkT1Display(networkDisplayValue);
			
			if (StringUtils.isNotBlank(networkValue)) {
				
				String value = null;
				
				if (networkValue.startsWith(SerffConstants.DOLLARSIGN)) {
					
					value = networkValue.replace(SerffConstants.DOLLARSIGN, StringUtils.EMPTY);
					planSTMBenefit.setNetworkT1CopayValue(String.valueOf(Double.valueOf(value).intValue()));
				}
				else if (networkValue.endsWith(SerffConstants.PERCENTAGE)) {
					
					value = networkValue.replace(SerffConstants.PERCENTAGE, StringUtils.EMPTY);
					planSTMBenefit.setNetworkT1CoinsuranceValue(String.valueOf(Double.valueOf(value).intValue()));
				}
			}
			// PlanSTMBenefit bean has been Persisted Successfully.
			mergeEntityManager(entityManager, planSTMBenefit);
		}
		finally {
			LOGGER.debug("savePlanSTMBenefit() End");
		}
	}
	
	/**
	 * Method is used to save PlanSTMCost table.
	 */
	private void savePlanSTMCost(PlanSTM savedPlanSTM, STMExcelVO stmExcelVO, EntityManager entityManager) {
		
		LOGGER.debug("savePlanSTMCost() Start");
		PlanSTMCost planSTMCost = new PlanSTMCost();
		
		try {
			planSTMCost.setPlanSTM(savedPlanSTM);
			planSTMCost.setName(DEDUCTIBLE_NAME);
			planSTMCost.setInNetworkInd(getDollarValue(stmExcelVO.getDeductibleIndividual()));
			planSTMCost.setIndividualDescription(stmExcelVO.getDeductibleIndividualDesc());
			planSTMCost.setFamilyDescription(stmExcelVO.getDeductibleFamilyDesc());
			planSTMCost.setSeparateDedtPrescptDrug(stmExcelVO.getSepDrugDeductible());
			planSTMCost.setLastUpdateBy(SerffConstants.DEFAULT_USER_ID);
			// PlanSTMCost bean has been Persisted Successfully.
			mergeEntityManager(entityManager, planSTMCost);
		}
		finally {
			LOGGER.debug("savePlanSTMCost() End");
		}
	}
	
	/**
	 * Method is used to merge STM Table to Entity Manager.
	 */
	private Object mergeEntityManager(EntityManager entityManager, Object saveObject) {
		
		Object savedObject = null;
		
		if (null != entityManager && entityManager.isOpen()) {
			savedObject = entityManager.merge(saveObject);
			LOGGER.debug(saveObject.getClass() + " bean has been Persisted Successfully.");
		}
		return savedObject;
	}
	
	/**
	 * Method is used to Commit/Rollback transaction depends on Status argument.
	 */
	private boolean getTransactionCommit(boolean status, EntityManager entityManager) throws GIException {
		
		LOGGER.debug("getTransactionCommit() Start");
		boolean commitStatus = status;
		
		try {
			
			if (null != entityManager && entityManager.isOpen()) {
				
				if (status) {
					entityManager.getTransaction().commit();
					LOGGER.info("Transaction Commit has been Commited");
				}
				else {
					entityManager.getTransaction().rollback();
					LOGGER.info("Transaction Commit has been Rollback");
				}
			}
		}
		catch (Exception ex) {
			commitStatus = Boolean.FALSE;
			LOGGER.error("Error occured while commiting Transaction " + commitStatus, ex);
			throw new GIException(ex);
		}
		finally {
			LOGGER.debug("getTransactionCommit() End");
		}
		return commitStatus;
	}
}
