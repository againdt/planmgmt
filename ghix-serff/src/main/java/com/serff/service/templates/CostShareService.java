/**
 * File 'CostShareService.java' for publishing persistence contract for 'COST_SHARE' records.
 *
 * @author chalse_v
 * @since Jun 14, 2013
 *
 */
package com.serff.service.templates;

import com.getinsured.hix.model.CostShare;

/**
 * Class 'CostShareService' for publishing persistence contract for 'CostShare' records.
 *
 * @author chalse_v
 * @version 1.0
 * @since Jun 14, 2013
 *
 */
public interface CostShareService {
	/**
	 * Method 'saveCostShare' to save CostShare data.
	 *
	 * @param CostShare The CostShare instance to persist.
	 * @return CostShare The persisted CostShare instance.
	 */
	CostShare saveCostShare(CostShare costShare);
}
