package com.serff.service.templates;

import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.ADDITIONAL_COVERAGE_OFFERED_IN_GAP;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.ANNUAL_DRUG_DEDUCTIBLE;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.IN_NETWORK_MOOP_AMT;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.MONTHLY_CONSOLIDATED_PREMIUM;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.OVERALL_STAR_RATING;
import static com.serff.util.SerffConstants.DEFAULT_USER_ID;
import static com.serff.util.SerffConstants.DOT;
import static com.serff.util.SerffConstants.INITIAL_INDEX;
import static com.serff.util.SerffConstants.NEGATIVE_INDEX_OF;
import static com.serff.util.SerffConstants.NO;
import static com.serff.util.SerffConstants.NO_ABBR;
import static com.serff.util.SerffConstants.PUF_DATASOURCE;
import static com.serff.util.SerffConstants.SPACE;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_AD;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_RX;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_SP;
import static com.serff.util.SerffConstants.YES;
import static com.serff.util.SerffConstants.YES_ABBR;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerServiceArea;
import com.getinsured.hix.model.IssuerServiceAreaExt;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanMedicare;
import com.getinsured.hix.model.PlanMedicareBenefit;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration.PlanmgmtConfigurationEnum;
import com.getinsured.hix.platform.repository.IZipCodeRepository;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.puf.medicare.MedicareVO;
import com.serff.repository.templates.IIssuerServiceAreaRepository;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.repository.templates.IServiceAreaRepository;
import com.serff.repository.templates.ITenantRepository;
import com.serff.service.validation.MedicarePUFPlansValidator;
import com.serff.util.PlanUpdateStatistics;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * Class is used to persist Medicare-PUF Plans to the database.
 * 
 * @author Bhavin Parmar
 * @since Apr 14, 2015
 */
@Service("medicarePlanMgmtSerffService")
public class MedicarePlanMgmtSerffServiceImpl implements MedicarePlanMgmtSerffService {

	private static final Logger LOGGER = Logger.getLogger(MedicarePlanMgmtSerffServiceImpl.class);

	private DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	private static final String FRIST_DAY_OF_YEAR = "01/01/";
	private static final String LAST_DAY_OF_YEAR = "12/31/";
	//private static final String BENEFIT_NOT_ENOUGH_DATA = "Not enough data available";
	//private static final String BENEFIT_PLAN_TOO_NEW_TO_MEASURED = "Plan too new to be measured";
	private static final String NOT_APPLICABLE = "NA";
	private static final String BENEFIT_PRESCRIPTION_DRUGS_COVERED = "Prescription Drugs Covered";
	private static final String BENEFIT_VALUE_YES = "Yes";
	private static final String BENEFIT_VALUE_NO = "No";
	private static final String SVCAREA_DELETED = "All records marked for deletion for issuerServiceArea =" ;
	private static final int DECREMENT_CERTIFIED_PLAN = -1;

	@Autowired private MedicarePUFPlansValidator medicarePUFPlansValidator;
	@Autowired private IIssuersRepository iIssuerRepository;
	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private SerffUtils serffUtils;
	@Autowired private IIssuerServiceAreaRepository iIssuerServiceAreaRepository;
	@Autowired private IServiceAreaRepository iServiceAreaRepository;
	@Autowired private IZipCodeRepository iZipCodeRepository;
	@Autowired private ITenantRepository iTenantRepository;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;

	/**
	 * Method is used to populate and persist Medicare-PUF plans from
	 * MedicareIssuerPlansMap<String[IssuerID], Map<IssuerPlanNumber, MedicareVO>> to database.
	 */
	@Override
	public boolean populateAndPersistMedicareData(Map<String, Map<String, MedicareVO>> medicareIssuerPlansMap,
			SerffPlanMgmt trackingRecord) throws GIException {

		LOGGER.info("populateAndPersistMedicareData() Start for " + trackingRecord.getSerffReqId());
		boolean savedStatus = false;
		boolean warnStatus = false;
		EntityManager entityManager = null;
		PlanUpdateStatistics uploadStatistics = new PlanUpdateStatistics();

		try {

			if (CollectionUtils.isEmpty(medicareIssuerPlansMap)) {
				trackingRecord.setRequestStateDesc("No record to persist for request ");
				return savedStatus;
			}
			// Validate Medicare-PUF Plan data.
			List<MedicareVO> failedMedicareList = medicarePUFPlansValidator.validateMedicarePlanList(medicareIssuerPlansMap, trackingRecord.getOperation());

			List<Integer> existingIssuerIdList = null;
			String medicarePlanType = trackingRecord.getOperation();
			int applicableYear = Calendar.getInstance().get(Calendar.YEAR);

			existingIssuerIdList = getExistingIssuerIdList(medicarePlanType, applicableYear);
			savedStatus = saveCarriersAndPlans(medicareIssuerPlansMap, failedMedicareList, existingIssuerIdList, medicarePlanType,
					uploadStatistics);

			LOGGER.debug("Saved Status: " + savedStatus);
			if (savedStatus && !CollectionUtils.isEmpty(existingIssuerIdList)) {
				// Get DB connection using Entity Manager Factory
				entityManager = entityManagerFactory.createEntityManager();
				// Begin Transaction
				entityManager.getTransaction().begin();
				LOGGER.info("Decertifing all plans("+ existingIssuerIdList.size() +") which has been not mentioned in PUF-Template.");
				boolean anyPlanDecertifed = decertifyPlansForIssuer(uploadStatistics, existingIssuerIdList, medicarePlanType, applicableYear, entityManager);
				if (anyPlanDecertifed) {
					savedStatus = getTransactionCommit(true, entityManager) || savedStatus;
				}
				closeEntityManager(entityManager);
			}

			if (!CollectionUtils.isEmpty(failedMedicareList)) {
				warnStatus = true;
				LOGGER.error("Number of Failed Medicare Plans: " + failedMedicareList.size());
				StringBuilder errorMessage = new StringBuilder();

				for (MedicareVO medicareVO : failedMedicareList) {
					errorMessage.append(medicareVO.getErrorMessages());
					errorMessage.append("\n");
				}
				LOGGER.error(errorMessage.toString());
				trackingRecord.setPmResponseXml(errorMessage.toString());
			}
		}
		catch(Exception ex) {
			savedStatus = false;
			LOGGER.error(ex.getMessage(), ex);
			throw new GIException(ex);
		}
		finally {
			updateTrackingRecord(savedStatus, warnStatus, uploadStatistics, trackingRecord);
			closeEntityManager(entityManager);
			LOGGER.info("populateAndPersistMedicareData() End");
		}
		return savedStatus;
	}

	/**
	 * Method is used to populate and persist Medicare-PUF plans from
	 * MedicareIssuerPlansMap<String[IssuerID], Map<IssuerPlanNumber, MedicareVO>> to database.
	 */
	@SuppressWarnings("unchecked")
	private boolean saveCarriersAndPlans(Map<String, Map<String, MedicareVO>> medicareIssuerPlansMap,
			List<MedicareVO> failedMedicareList, List<Integer> existingIssuerIdList, String medicarePlanType,
			PlanUpdateStatistics uploadStatistics) throws GIException {
		EntityManager entityManager = null;
		LOGGER.debug("saveCarriersAndPlans() Start");
		boolean savedStatus = false;

		try {
			Entry<String, MedicareVO> mapEntry;
			Map<String, MedicareVO> medicarePlansMap;
			List<Plan> savedPlanList = null;
			Iterator<Entry<String, Map<String, MedicareVO>>> mapEntries = medicareIssuerPlansMap.entrySet().iterator();

			while (mapEntries.hasNext()) {
				mapEntry = (Entry) mapEntries.next();
				LOGGER.debug("MainMap[HIOS Issuer ID]: " + mapEntry.getKey());
				medicarePlansMap = (Map<String, MedicareVO>) mapEntry.getValue();

				if (CollectionUtils.isEmpty(medicarePlansMap)) {
					LOGGER.warn("Medicare plans are empty for HIOS Issuer ID: " + mapEntry.getKey());
					continue;
				}
				// Get DB connection using Entity Manager Factory
				entityManager = entityManagerFactory.createEntityManager();
				// Begin Transaction
				entityManager.getTransaction().begin();
				// Persisting Medicare-PUF plans
				savedPlanList = saveCarriersPlansBenefitsData(medicarePlansMap, existingIssuerIdList, medicarePlanType, uploadStatistics,
						entityManager, failedMedicareList);

				if (!CollectionUtils.isEmpty(savedPlanList)) {
					savedStatus = getTransactionCommit(true, entityManager) || savedStatus;
				}
				closeEntityManager(entityManager);
			}
		}
		catch(Exception ex) {
			savedStatus = false;
			LOGGER.error(ex.getMessage(), ex);
			throw new GIException(ex);
		}
		finally {
			LOGGER.debug("saveCarriersAndPlans() End");
		}
		return savedStatus;
	}

	/**
	 * Method is used to populate and persist Medicare-PUF plans from
	 * Map<IssuerPlanNumber, MedicareVO> to database.
	 */
	private List<Plan> saveCarriersPlansBenefitsData(Map<String, MedicareVO> medicarePlansMap, List<Integer> existingIssuerIdList,
			String medicarePlanType, PlanUpdateStatistics uploadStatistics, EntityManager entityManager,
			List<MedicareVO> failedMedicareList) {

		LOGGER.debug("saveCarriersPlansBenefitsData() Start");
		List<Plan> savedPlanList = null;

		try {
			boolean firstRow = true;
			MedicareVO medicareVO;
			Issuer savedIssuer = null;
			Plan savedPlan = null;
			List<Plan> existingPlanList = null;
			Map<String, ZipCode> stateZipCodeMap = null;
			int applicableYear = Calendar.getInstance().get(Calendar.YEAR);
			IssuerServiceArea issuerServiceArea = null;
			List<Tenant> defaultTenantList = new ArrayList<Tenant>();
			defaultTenantList.add(iTenantRepository.findByCode(SerffConstants.TENANT_GINS));

			for (Map.Entry<String, MedicareVO> entryData : medicarePlansMap.entrySet()) {
				LOGGER.info("ChildMap[IssuerPlanNumber]: " + entryData.getKey());
				medicareVO = entryData.getValue();

				if (null == medicareVO) {
					continue;
				}

				if (firstRow) {
					firstRow = false;

					// Get existing Issuer from Database.
					Issuer issuer = iIssuerRepository.getIssuerByHiosID(medicareVO.getHiosIssuerID());

					if (medicareVO.isNotValid()) {
						setErrorMsgInMedicareVO(medicareVO, "Skipping Issuer["+ medicareVO.getHiosIssuerID() +"] due to validation errors.");
						LOGGER.warn("Skipping Issuer["+ medicareVO.getHiosIssuerID() +"] due to validation errors.");
						uploadStatistics.planFailed(medicarePlansMap.size());

						if (!CollectionUtils.isEmpty(existingIssuerIdList) && null != issuer) {
							boolean removedIssuerIdFlag = existingIssuerIdList.remove(Integer.valueOf(issuer.getId()));
							LOGGER.info("Issuer Id is removed for failed plan validation: " + removedIssuerIdFlag);
						}
						break;
					}

					if (null == issuer) {
						issuer = new Issuer();
					}
					else {
						existingPlanList = decertifyExistingPlans(medicarePlanType, issuer, applicableYear, uploadStatistics, entityManager);
					}
					savedIssuer = saveIssuer(medicareVO, issuer, entityManager);

					if (null != savedIssuer) {
						stateZipCodeMap = getZipCodeDetails(medicareVO);
						savedPlanList = new ArrayList<Plan>();
					}
				}

				if (null != savedIssuer) {
					Plan existingPlan = getPlanFromList(existingPlanList, medicareVO.getIssuerPlanNumber());
					savedPlan = savePlansBenefitsData(medicareVO, applicableYear, savedIssuer, existingPlan, medicarePlanType, entityManager);

					if (null != savedPlan && null != savedPlanList) {
						//If there is already tenant attached (in case of reload), don't attach default tenant again
						//HIX-78519
						if(CollectionUtils.isEmpty(savedPlan.getTenantPlan() )) {
							serffUtils.saveTenantPlan(savedPlan, defaultTenantList, entityManager);
						} 
						// Create Service Area only if it is not a RX Plan or if the issuerServiceArea is not already created
						if (!MedicareVO.InsuranceType.RX.equals(medicareVO.getInsuranceType())
								|| issuerServiceArea == null) {
							issuerServiceArea = saveServiceAreaCoveringFullCounties(medicareVO, stateZipCodeMap, applicableYear, entityManager);
						}

						if (null != issuerServiceArea) {

							if (null != existingPlan) {
								uploadStatistics.planUpdated(false);
								uploadStatistics.planDeleted(DECREMENT_CERTIFIED_PLAN);
							}
							else {
								uploadStatistics.planAdded();
							}
							savedPlan.setServiceAreaId(issuerServiceArea.getId());
							savedPlanList.add(savedPlan);

							if (!CollectionUtils.isEmpty(existingIssuerIdList)) {
								boolean removedIssuerIdFlag = existingIssuerIdList.remove(Integer.valueOf(savedIssuer.getId()));
								LOGGER.debug("Issuer Id is removed: " + removedIssuerIdFlag);
							}
						}
						else {
							failedMedicareList.add(medicareVO);
							uploadStatistics.planFailed();
						}
					}
					else {
						failedMedicareList.add(medicareVO);
						uploadStatistics.planFailed();
					}
				}
				else {
					failedMedicareList.add(medicareVO);
					uploadStatistics.planFailed();
				}
			}
		}
		finally {
			LOGGER.debug("saveCarriersPlansBenefitsData() End");
		}
		return savedPlanList;
	}

	/**
	 * Method is used to get Existing IssuerId List.
	 */
	private List<Integer> getExistingIssuerIdList(String medicarePlanType, Integer applicableYear) {

		LOGGER.debug("getExistingIssuerIdList() Start");
		List<Integer> existingIssuerIdList = null;

		try {
			if (StringUtils.isBlank(medicarePlanType) || 0 >= applicableYear) {
				LOGGER.error("Plan Insurance Type Or Applicable Year is invalid.");
				return existingIssuerIdList;
			}
			Plan.PlanInsuranceType planInsuranceType = null;
			String cmsIsSnp = null;

			if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(medicarePlanType)) {
				planInsuranceType = Plan.PlanInsuranceType.MC_ADV;
				cmsIsSnp = NO_ABBR;
			}
			else if (TRANSFER_MEDICARE_SP.equalsIgnoreCase(medicarePlanType)) {
				planInsuranceType = Plan.PlanInsuranceType.MC_ADV;
				cmsIsSnp = YES_ABBR;
			}
			else if (TRANSFER_MEDICARE_RX.equalsIgnoreCase(medicarePlanType)) {
				planInsuranceType = Plan.PlanInsuranceType.MC_RX;
				cmsIsSnp = NO_ABBR;
			}

			if (null != planInsuranceType) {
				existingIssuerIdList = iPlanRepository.findByInsuranceTypeAndCmsIsSnpAndIsDeletedAndApplicableYear(planInsuranceType.name(),
						cmsIsSnp, Plan.IS_DELETED.N.name(), applicableYear);
				LOGGER.info("loadedIssuerIdList is null: " + CollectionUtils.isEmpty(existingIssuerIdList));
			}
			else {
				LOGGER.error("Plan Insurance Type is invalid.");
			}
		}
		finally {
			LOGGER.debug("getExistingIssuerIdList() End");
		}
		return existingIssuerIdList;
	}

	/**
	 * Method is used to get and decertify existing plans data.
	 */
	private List<Plan> decertifyExistingPlans(String medicarePlanType, Issuer issuer, Integer applicableYear, PlanUpdateStatistics uploadStatistics,
			EntityManager entityManager) {

		LOGGER.debug("decertifyExistingPlans() Start");
		List<Plan> existingPlanList = null;

		try {
			// Fixed FOD issue
			Plan.PlanInsuranceType planInsuranceType = Plan.PlanInsuranceType.MC_ADV;
			String cmsIsSnp = NO_ABBR;

			if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(medicarePlanType)) {
				planInsuranceType = Plan.PlanInsuranceType.MC_ADV;
				cmsIsSnp = NO_ABBR;
			}
			else if (TRANSFER_MEDICARE_SP.equalsIgnoreCase(medicarePlanType)) {
				planInsuranceType = Plan.PlanInsuranceType.MC_ADV;
				cmsIsSnp = YES_ABBR;
			}
			else if (TRANSFER_MEDICARE_RX.equalsIgnoreCase(medicarePlanType)) {
				planInsuranceType = Plan.PlanInsuranceType.MC_RX;
				cmsIsSnp = NO_ABBR;
			}
			// Get existing Plan from Database.
			existingPlanList = iPlanRepository.findByIssuerIdAndInsuranceTypeAndCmsIsSnpAndIsDeletedAndApplicableYear(
							issuer.getId(), planInsuranceType.name(), cmsIsSnp, Plan.IS_DELETED.N.name(), applicableYear);
			int decertifiedCount = decertifyPlans(existingPlanList, uploadStatistics, entityManager);

			if (0 < decertifiedCount) {
				LOGGER.info("Decertified Plans count is "+ decertifiedCount + " for issuer " + issuer.getHiosIssuerId());
			}
		}
		finally {
			LOGGER.debug("decertifyExistingPlans() End");
		}
		return existingPlanList;
	}

	/**
	 * Method is used to decertify existing plans.
	 */
	private int decertifyPlans(List<Plan> existingPlanList, PlanUpdateStatistics uploadStatistics, EntityManager entityManager) {

		LOGGER.debug("decertifyPlans() Start");
		int decertifiedCount = 0;

		try {
			if (CollectionUtils.isEmpty(existingPlanList)) {
				return decertifiedCount;
			}
			LOGGER.debug("Existing Plan List size: " + existingPlanList.size());

			Plan existingPlan = null;
			for (int index = 0; index < existingPlanList.size(); index++) {

				existingPlan = existingPlanList.get(index);
				if (null == existingPlan) {
					continue;
				}
				decertifiedCount++;
				uploadStatistics.planDeleted();

				if (Plan.PlanStatus.DECERTIFIED.name().equals(existingPlan.getStatus())) {
					LOGGER.debug("Plan is already Decertified.");
					continue;
				}
				existingPlan.setStatus(Plan.PlanStatus.DECERTIFIED.name());
				existingPlan = (Plan) mergeEntityManager(entityManager, existingPlan);
				existingPlanList.set(index, existingPlan);
			}
		}
		finally {
			LOGGER.debug("decertifyPlans() End with Count of Decertified plans: " + decertifiedCount);
		}
		return decertifiedCount;
	}

	/**
	 * Method is used to decertify existing Plans for Issuer in Database.
	 */
	private boolean decertifyPlansForIssuer(PlanUpdateStatistics uploadStatistics, List<Integer> issuerIdList, String medicarePlanType,
			Integer applicableYear, EntityManager entityManager) {

		LOGGER.debug("decertifyPlansForIssuer() Start");
		boolean anyPlanDecertifed = false;
		int decertifiedCount = 0;

		try {
			if (0 >= applicableYear || StringUtils.isBlank(medicarePlanType)
					|| CollectionUtils.isEmpty(issuerIdList) || null == entityManager) {
				LOGGER.error("Required data are missing to decertify existing plans.");
				return anyPlanDecertifed;
			}

			Plan.PlanInsuranceType planInsuranceType = null;
			String cmsIsSnp = null;

			if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(medicarePlanType)) {
				planInsuranceType = Plan.PlanInsuranceType.MC_ADV;
				cmsIsSnp = NO_ABBR;
			}
			else if (TRANSFER_MEDICARE_SP.equalsIgnoreCase(medicarePlanType)) {
				planInsuranceType = Plan.PlanInsuranceType.MC_ADV;
				cmsIsSnp = YES_ABBR;
			}
			else if (TRANSFER_MEDICARE_RX.equalsIgnoreCase(medicarePlanType)) {
				planInsuranceType = Plan.PlanInsuranceType.MC_RX;
				cmsIsSnp = NO_ABBR;
			}

			if (null == planInsuranceType || StringUtils.isBlank(cmsIsSnp)) {
				LOGGER.error("Insurance Type is invalid to decertify existing plans.");
				return anyPlanDecertifed;
			}
			List<Plan> existingPlanList = null;

			for (Integer issuerId : issuerIdList) {

				// Get existing Plan from Database.
				existingPlanList = iPlanRepository.findByIssuerIdAndInsuranceTypeAndCmsIsSnpAndIsDeletedAndApplicableYear(
						issuerId, planInsuranceType.name(), cmsIsSnp, Plan.IS_DELETED.N.name(), applicableYear);
				decertifiedCount += decertifyPlans(existingPlanList, uploadStatistics, entityManager);
			}
		}
		finally {
			if (0 < decertifiedCount) {
				anyPlanDecertifed = true;
				LOGGER.info("Total Decertified Plans count is "+ decertifiedCount);
			}
			LOGGER.debug("decertifyPlansForIssuer() End");
		}
		return anyPlanDecertifed;
	}

	/**
	 * Method is used to set error message in MedicareVO object.
	 */
	private void setErrorMsgInMedicareVO(MedicareVO medicareVO, String errorMessages) {

		LOGGER.debug("setErrorMsgInMedicareVO() Start");

		if (null != medicareVO) {

			if (StringUtils.isNotBlank(medicareVO.getErrorMessages())) {
				medicareVO.setErrorMessages(medicareVO.getErrorMessages() + SerffConstants.NEW_LINE + errorMessages);
			}
			else {
				medicareVO.setErrorMessages(errorMessages);
			}
			medicareVO.setNotValid(true);
		}
//		LOGGER.error(errorMessages);
		LOGGER.debug("setErrorMsgInMedicareVO() Start");
	}

	/**
	 * Method is used to get ZipCodes using State Code & County.
	 * Map<String [County.toUpperCase() + stateCode], ZipCode>
	 */
	private Map<String, ZipCode> getZipCodeDetails(MedicareVO medicareVO) {

		Map<String, ZipCode> stateZipCodeMap = null;
		LOGGER.debug("getZipCodeDetails() Start");
		List<String> countiesList = medicareVO.getCountyList();

		try {
			if (CollectionUtils.isEmpty(countiesList)) {
				LOGGER.info("countiesList is empty, indicates entire state. Getting list of all county for the State " + medicareVO.getStateCode());
				countiesList = iZipCodeRepository.findCountyNameByState(medicareVO.getStateCode());
			}

			if (CollectionUtils.isEmpty(countiesList)) {
				LOGGER.error("Counties List has been empty for HIOS Issuer ID:" + medicareVO.getHiosIssuerID());
				return stateZipCodeMap;
			}

			List<String> addedCounty = new ArrayList<String>();
			stateZipCodeMap = new HashMap<>();

			for (String county : countiesList) {

				if (addedCounty.contains(county.toUpperCase())) {
					//This county is already processed
					LOGGER.warn("Skipping County " + county + " and HIOS Issuer ID:" + medicareVO.getHiosIssuerID());
					continue;
				}
				LOGGER.debug("Loading zipcode for county: " + county + " and HIOS Issuer ID: " + medicareVO.getHiosIssuerID());
				List<ZipCode> zipCodes = iZipCodeRepository.getZipListByCountyNameandState(county, medicareVO.getStateCode());
				ZipCode zipCode = null;

				if (!CollectionUtils.isEmpty(zipCodes)) {
					zipCode = zipCodes.get(0);
				}
				else {
					setErrorMsgInMedicareVO(medicareVO, "No Zip codes found for County: " + county + " and HIOS Issuer ID: " + medicareVO.getHiosIssuerID());
					LOGGER.warn("No Zip codes found for County: " + county + " and HIOS Issuer ID: " + medicareVO.getHiosIssuerID());
					continue;
				}
				addedCounty.add(county.toUpperCase());
				stateZipCodeMap.put(county.toUpperCase() + medicareVO.getStateCode(), zipCode);
			}
		}
		finally {

			if (CollectionUtils.isEmpty(stateZipCodeMap)) {
				setErrorMsgInMedicareVO(medicareVO, "ZipCodes Map has been empty for HIOS Issuer ID: " + medicareVO.getHiosIssuerID());
			}
			LOGGER.debug("getZipCodeDetails() Start");
		}
		return stateZipCodeMap;
	}

	private Plan savePlansBenefitsData(MedicareVO medicareVO, int applicableYear, Issuer savedIssuer, Plan existingPlan,
			String medicarePlanType, EntityManager entityManager) {

		LOGGER.debug("savePlansBenefitsData() Start");
		boolean isSavedPlansBenefits = false;
		boolean deleteExistingBenefits = false;
		Plan savedPlan = null;

		try {

			Plan plan = null;
			if (null != existingPlan) {
				plan = existingPlan;
				deleteExistingBenefits = true;
				LOGGER.debug("Existing Plan IssuerPlanNumber: " + plan.getIssuerPlanNumber());
			}
			else {
				plan = new Plan();
			}
			PlanMedicare planMedicare = null == plan.getPlanMedicare() ? new PlanMedicare() : plan.getPlanMedicare();
			plan = savePlan(medicareVO, applicableYear, savedIssuer, plan, entityManager);

			if (null != plan) {
				planMedicare.setPlan(plan);
				planMedicare = savePlanMedicare(medicareVO, planMedicare, entityManager);
				isSavedPlansBenefits = processMedicareBenefitData(deleteExistingBenefits, medicareVO, planMedicare, medicarePlanType, entityManager);

				if (isSavedPlansBenefits) {
					savedPlan = plan;
				}
			}
		}
		finally {
			LOGGER.debug("savePlansBenefitsData() End");
		}
		return savedPlan;
	}

	/**
	 * Method is used to save Carrier data in Issuer tables.
	 */
	private Issuer saveIssuer(MedicareVO medicareVO, Issuer issuer, EntityManager entityManager) {

		LOGGER.debug("Persisting Issuer Object Start with HiosIssuerID: " + medicareVO.getHiosIssuerID());
		Issuer savedIssuer = null;

		try {
			issuer.setHiosIssuerId(medicareVO.getHiosIssuerID());
			issuer.setName(medicareVO.getOrganizationName());
			issuer.setCompanyLegalName(medicareVO.getOrganizationName());
			issuer.setShortName(medicareVO.getOrganizationName());
			issuer.setMarketingName(medicareVO.getOrganizationName());
			issuer.setState(medicareVO.getStateCode());
			issuer.setStateOfDomicile(medicareVO.getStateCode());
			issuer.setCompanyState(medicareVO.getStateCode());
			issuer.setD2C(NO_ABBR);
			issuer.setLastUpdatedBy(DEFAULT_USER_ID);
			issuer.setSendEnrollmentEndDateFlag("true"); // Default Value of send enrollment end date flag
			savedIssuer = (Issuer) mergeEntityManager(entityManager, issuer);
		}
		finally {

			if (null == savedIssuer) {
				setErrorMsgInMedicareVO(medicareVO, "Failed to persist Issuer for HiosIssuerID: " + medicareVO.getHiosIssuerID());
			}
			LOGGER.debug("Persisting Issuer Object Done");
		}
		return savedIssuer;
	}

	/**
	 * Method is used to save Plan table.
	 */
	private Plan savePlan(MedicareVO medicareVO, int applicableYear, Issuer savedIssuer, Plan plan, EntityManager entityManager) {

		LOGGER.debug("Persisting Plan Object Start with IssuerPlanNumber: " + medicareVO.getIssuerPlanNumber());
		Plan savedPlan = null;

		try {
			plan.setStartDate(dateFormat.parse(FRIST_DAY_OF_YEAR + applicableYear)); // First date of Current Year "MM/dd/yyyy"
			plan.setEndDate(dateFormat.parse(LAST_DAY_OF_YEAR + applicableYear)); // Last date of Current Year "MM/dd/yyyy"
			plan.setApplicableYear(applicableYear); // Derive from EffectiveDate

			if (MedicareVO.InsuranceType.MA.equals(medicareVO.getInsuranceType())) {
				plan.setInsuranceType(Plan.PlanInsuranceType.MC_ADV.name());
				plan.setNetworkType(medicareVO.getMedicareType());
			}
			else if (MedicareVO.InsuranceType.SNP.equals(medicareVO.getInsuranceType())) {
				plan.setInsuranceType(Plan.PlanInsuranceType.MC_ADV.name());
				plan.setNetworkType(medicareVO.getMedicareType());
			}
			else if (MedicareVO.InsuranceType.RX.equals(medicareVO.getInsuranceType())) {
				plan.setInsuranceType(Plan.PlanInsuranceType.MC_RX.name());
			}
			plan.setIssuerPlanNumber(medicareVO.getIssuerPlanNumber());
			plan.setMarket(Plan.PlanMarket.INDIVIDUAL.name());
			plan.setName(medicareVO.getPlanName());
			plan.setStatus(Plan.PlanStatus.CERTIFIED.name());
			plan.setIssuer(savedIssuer);
			plan.setHsa(Plan.HSA.NO.name());
			plan.setIssuerVerificationStatus(Plan.IssuerVerificationStatus.VERIFIED.name());
			plan.setHiosProductId(medicareVO.getIssuerPlanNumber());
			plan.setState(medicareVO.getStateCode());
			plan.setIsDeleted(Plan.IS_DELETED.N.name());
			plan.setExchangeType(Plan.EXCHANGE_TYPE.OFF.name());
			plan.setIsPUF(Plan.IS_PUF.Y);
			plan.setLastUpdatedBy(DEFAULT_USER_ID);
			savedPlan = (Plan) mergeEntityManager(entityManager, plan);
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting Plan object", ex);
		}
		finally {

			if (null == savedPlan) {
				setErrorMsgInMedicareVO(medicareVO, "Failed to persist Plan object for IssuerPlanNumber: "+ medicareVO.getIssuerPlanNumber());
			}
			LOGGER.debug("Persisting Plan Object Done");
		}
		return savedPlan;
	}

	/**
	 * Method is used to save PlanMedicare table.
	 */
	private PlanMedicare savePlanMedicare(MedicareVO medicareVO, PlanMedicare planMedicare, EntityManager entityManager) {

		LOGGER.debug("Persisting PlanMedicare Object Start with IssuerPlanNumber: " + medicareVO.getIssuerPlanNumber());
		PlanMedicare savedMedicare = null;

		try {
			planMedicare.setCmsContractId(medicareVO.getContractID());
			planMedicare.setCmsPlanId(medicareVO.getPlanID());
			//planMedicare.setIsMedicaid(NO_ABBR); //NO need to set this as we don't have data
			planMedicare.setDataSource(PUF_DATASOURCE);

			if (MedicareVO.InsuranceType.MA.equals(medicareVO.getInsuranceType())
					|| MedicareVO.InsuranceType.RX.equals(medicareVO.getInsuranceType())) {
				planMedicare.setCmsIsSnp(NO_ABBR);
			}
			else if (MedicareVO.InsuranceType.SNP.equals(medicareVO.getInsuranceType())) {
				planMedicare.setCmsIsSnp(YES_ABBR);
				planMedicare.setSnpType(medicareVO.getSpecialNeedsPlanType());
			}
			planMedicare.setCmsSegmentId(medicareVO.getSegmentID());
			planMedicare.setCreatedBy(DEFAULT_USER_ID);
			planMedicare.setLastUpdatedBy(DEFAULT_USER_ID);
			savedMedicare = (PlanMedicare) mergeEntityManager(entityManager, planMedicare);
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting PlanMedicare object", ex);
		}
		finally {

			if (null == savedMedicare) {
				setErrorMsgInMedicareVO(medicareVO, "Failed to persist PlanMedicare object for IssuerPlanNumber: "+ medicareVO.getIssuerPlanNumber());
			}
			LOGGER.debug("Persisting PlanMedicare Object Done");
		}
		return savedMedicare;
	}

	/**
	 * Method is used to delete existing Medicare Benefits from Database.
	 */
	private void deleteMedicareBenefits(boolean deleteExistingBenefits, PlanMedicare savedPlanMedicare, EntityManager entityManager) {

		if (deleteExistingBenefits) {
			Query deleteQuery = entityManager.createQuery("delete from PlanMedicareBenefit benefit where benefit.planMedicare.id = :planMedicareId");
			deleteQuery.setParameter("planMedicareId", savedPlanMedicare.getId());
			int deleteCount = deleteQuery.executeUpdate();
			LOGGER.info("Deleted existing Benefits["+ deleteCount +"] for PlanID: " + savedPlanMedicare.getPlan().getIssuerPlanNumber());
		}
	}

	/**
	 * Method is used to save PlanMedicareBenefit table.
	 */
	private boolean processMedicareBenefitData(boolean deleteExistingBenefits, MedicareVO medicareVO, PlanMedicare savedPlanMedicare,
			String medicarePlanType, EntityManager entityManager) {

		LOGGER.debug("processMedicareBenefitData() Start with isBenefitsDeleted: " + deleteExistingBenefits + " & IssuerPlanNumber: " + medicareVO.getIssuerPlanNumber());
		boolean isSavedBenefit = false;
		String benefitValue, benefitTinyValue;
		//PlanMedicareBenefit savedMedicareBenefit = null;

		try {
			// Delete existing medicare benefits.
			deleteMedicareBenefits(deleteExistingBenefits, savedPlanMedicare, entityManager);
			// Persisting Monthly Consolidated Premium Benefit
			isSavedBenefit = savePlanMedicareBenefit(medicareVO.getMonthlyPremium(), medicareVO.getMonthlyPremium(), MONTHLY_CONSOLIDATED_PREMIUM.getColumnName(medicarePlanType), savedPlanMedicare, PlanmgmtConfigurationEnum.MONTHLY_PREMIUM, entityManager, false);
			// Persisting Annual Drug Deductible Benefit
			isSavedBenefit = savePlanMedicareBenefit(medicareVO.getAnnualDrugDeductible(), medicareVO.getAnnualDrugDeductible(), ANNUAL_DRUG_DEDUCTIBLE.getColumnName(medicarePlanType), savedPlanMedicare, PlanmgmtConfigurationEnum.DRUG_DEDUCTIBLE, entityManager, true) && isSavedBenefit;
			// Persisting Additional Coverage Offered in the Gap Benefit
			benefitValue = medicareVO.getAdditionalCoverageInGap();

			if (null != benefitValue) {

				if (benefitValue.toUpperCase().startsWith(YES)) {
					benefitValue = BENEFIT_VALUE_YES;
				}
				else if (benefitValue.toUpperCase().startsWith(NO)) {
					benefitValue = BENEFIT_VALUE_NO;
				}
			}

			isSavedBenefit = savePlanMedicareBenefit(benefitValue, benefitValue, ADDITIONAL_COVERAGE_OFFERED_IN_GAP.getColumnName(medicarePlanType), savedPlanMedicare, PlanmgmtConfigurationEnum.DRUG_COVERAGE_GAP, entityManager, true) && isSavedBenefit;
			if (StringUtils.isBlank(medicareVO.getAnnualDrugDeductible())) {
				isSavedBenefit = savePlanMedicareBenefit(BENEFIT_VALUE_NO, BENEFIT_VALUE_NO, BENEFIT_PRESCRIPTION_DRUGS_COVERED, savedPlanMedicare, PlanmgmtConfigurationEnum.RX_COVERED, entityManager, false) && isSavedBenefit;
			}
			else {
				isSavedBenefit = savePlanMedicareBenefit(BENEFIT_VALUE_YES, BENEFIT_VALUE_YES, BENEFIT_PRESCRIPTION_DRUGS_COVERED, savedPlanMedicare, PlanmgmtConfigurationEnum.RX_COVERED, entityManager, false) && isSavedBenefit;
			}

			if (MedicareVO.InsuranceType.MA.equals(medicareVO.getInsuranceType())) {
				// Persisting In-network MOOP Amount Benefit
				isSavedBenefit = savePlanMedicareBenefit(medicareVO.getInNetworkMOOPAmount(), medicareVO.getInNetworkMOOPAmount(), IN_NETWORK_MOOP_AMT.getColumnName(medicarePlanType), savedPlanMedicare, PlanmgmtConfigurationEnum.MAX_ANNUAL_COPAY, entityManager, true) && isSavedBenefit;
			}
			// Persisting Overall Star Rating / Summary Star Rating Benefit
			benefitValue = medicareVO.getOverallStarRating();
			benefitTinyValue = NOT_APPLICABLE;

			if (NEGATIVE_INDEX_OF < benefitValue.indexOf(SPACE)
					&& validateNumeric(benefitValue.trim().split(SPACE)[INITIAL_INDEX])) {
				benefitTinyValue = benefitValue.trim().split(SPACE)[INITIAL_INDEX];
			}
			isSavedBenefit = savePlanMedicareBenefit(benefitValue, benefitTinyValue, OVERALL_STAR_RATING.getColumnName(medicarePlanType), savedPlanMedicare, PlanmgmtConfigurationEnum.OVERALL_PLAN_RATING, entityManager, true) && isSavedBenefit;
		}
		catch (Exception ex) {
			isSavedBenefit = false;
			LOGGER.error("Error occured while persisting PlanMedicareBenefit object", ex);
		}
		finally {

			if (!isSavedBenefit) {
				setErrorMsgInMedicareVO(medicareVO, "There is no benefit to persist for IssuerPlanNumber: "+ medicareVO.getIssuerPlanNumber());
			}
		}
		return isSavedBenefit;
	}

	/**
	 * Method is used to save PlanMedicareBenefit table.
	 */
	private boolean savePlanMedicareBenefit(String benefitValue, String benefitTinyValue, String benefitName, PlanMedicare savedPlanMedicare,
			PlanmgmtConfigurationEnum configData, EntityManager entityManager, boolean isOptional) {

		//LOGGER.debug("Persisting PlanMedicareBenefit Object Start");
		boolean isBenefitSaved = true;
		//PlanMedicareBenefit savedMedicareBenefit = null;

		try {
			if (StringUtils.isBlank(benefitValue) || null == configData) {
				LOGGER.debug("Skipping benefit as value or config is null");
				//If optional flag is true, return true else false
				return isOptional;
			}

			String entryKey = configData.getValue();
			PlanMedicareBenefit planMedicareBenefit = new PlanMedicareBenefit();
			planMedicareBenefit.setPlanMedicare(savedPlanMedicare);

			if (StringUtils.isNotBlank(entryKey)) {
				planMedicareBenefit.setCategory(entryKey.substring(entryKey.lastIndexOf(DOT) + 1));
				planMedicareBenefit.setCategoryEnum(planMedicareBenefit.getCategory()); // It should not be null
				planMedicareBenefit.setCategoryName(planMedicareBenefit.getCategory()); // It should not be null
				planMedicareBenefit.setName(benefitName); //DynamicPropertiesUtil.getPropertyValue(configData));
				planMedicareBenefit.setFullName(benefitValue); //actually FullName contains Full Value
				planMedicareBenefit.setTinyName(benefitName);
				planMedicareBenefit.setTinyValue(benefitTinyValue);
			}
			planMedicareBenefit.setCreatedBy(DEFAULT_USER_ID);
			planMedicareBenefit.setLastUpdatedBy(DEFAULT_USER_ID);
			mergeEntityManager(entityManager, planMedicareBenefit);
		}
		catch (Exception ex) {
			isBenefitSaved = false;
			LOGGER.error("Error occured while persisting PlanMedicareBenefit object", ex);
		}
		return isBenefitSaved;
	}

	/**
	 * Method is used to save IssuerServiceArea, IssuerServiceAreaExt & ServiceArea tables.
	 */
	private IssuerServiceArea saveServiceAreaCoveringFullCounties(MedicareVO medicareVO, Map<String, ZipCode> stateZipCodeMap,
			int applicableYear, EntityManager entityManager) {

		LOGGER.debug("Persisting Issuer Service Area Objects Start");
		IssuerServiceArea issuerServiceArea = null;
		String serviceAreaName = medicareVO.getIssuerPlanNumber();
		boolean entireState = CollectionUtils.isEmpty(medicareVO.getCountyList());

		try {

			if (CollectionUtils.isEmpty(stateZipCodeMap)) {
				return issuerServiceArea;
			}
			/*
			 * 1. Check if (Service Area + Year + HIOS) already exist
			 * 2a: If exist - delete child records 
			 * 2b: If not exit create new Service Area parent record 
			 * 3: add new child issuerServiceAreaExt and serviceArea records tied to parent issuerServiceArea
			 */
			IssuerServiceAreaExt issuerServiceAreaExt;
			ServiceArea issuerPmServiceArea;
			List<ServiceArea> pmServiceAreas = new ArrayList<ServiceArea>();
			List<IssuerServiceAreaExt> pmIssuerServiceAreaExt  = new ArrayList<IssuerServiceAreaExt>();
			LOGGER.debug("Checking if PM_ISSUER_SERVICE_AREA already have data for provided state,issuerHiosID and ServiceAreadId ");
			List<IssuerServiceArea> serviceAreas = iIssuerServiceAreaRepository.findbyStateIssuerAndServAreaAndApplicableYear(
							medicareVO.getStateCode(), medicareVO.getHiosIssuerID(), serviceAreaName, applicableYear);

			if (!CollectionUtils.isEmpty(serviceAreas)) {
				//Ideally there should be only one record.Still using list to avoid any junk data
				LOGGER.debug("Record already exist in PM_ISSUER_SERVICE_AREA for provided state,issuerHiosID and ServiceAreadId ");
				issuerServiceArea = serviceAreas.get(0);
				//Need to soft delete existing records in child tables
				boolean isOrphansRemoved = cleanUpExistingChildren(issuerServiceArea, entityManager);

				if (isOrphansRemoved) {
					LOGGER.debug("Existing child records cleaned up for issuerServiceArea with id " + issuerServiceArea.getId());
				}
			}
			else {
				LOGGER.debug("No existing record found. New entry will be done for PM_ISSUER_SERVICE_AREA");
				issuerServiceArea = new  IssuerServiceArea();
				issuerServiceArea.setSerffServiceAreaId(serviceAreaName);
				issuerServiceArea.setServiceAreaName(serviceAreaName);
				issuerServiceArea.setHiosIssuerId(medicareVO.getHiosIssuerID());
				issuerServiceArea.setIssuerState(medicareVO.getStateCode());
				issuerServiceArea.setApplicableYear(applicableYear);
			}

			if (entireState) {
				issuerServiceArea.setCompleteState(YES);
			}
			else {
				issuerServiceArea.setCompleteState(NO);
			}

			for (Map.Entry<String, ZipCode> entryData : stateZipCodeMap.entrySet()) {

				ZipCode zipCode = entryData.getValue();
				if (null == zipCode) {
					continue;
				}
				issuerServiceAreaExt = new IssuerServiceAreaExt();
				issuerServiceAreaExt.setIsDeleted(IssuerServiceAreaExt.IS_DELETED.N.toString());
				issuerServiceAreaExt.setServiceAreaId(issuerServiceArea);
				issuerServiceAreaExt.setCounty(zipCode.getCounty());
				issuerServiceAreaExt.setPartialCounty(NO);

				List<String> allZipCodes = iZipCodeRepository.findZipByCountyAndState(zipCode.getCountyFips(), medicareVO.getStateCode());
				String stateFips;
				for (String zip : allZipCodes) {
					issuerPmServiceArea = new ServiceArea();
					issuerPmServiceArea.setIsDeleted(ServiceArea.IS_DELETED.N.toString());
					issuerPmServiceArea.setServiceAreaId(issuerServiceArea);
					issuerPmServiceArea.setServiceAreaExtId(issuerServiceAreaExt);
					issuerPmServiceArea.setCounty(zipCode.getCounty());
					issuerPmServiceArea.setState(zipCode.getState());
					issuerPmServiceArea.setZip(zip);
					stateFips = zipCode.getStateFips();
					issuerPmServiceArea.setFips(stateFips+zipCode.getCountyFips());
					pmServiceAreas.add(issuerPmServiceArea);
				}
				issuerServiceAreaExt.setServiceArea(pmServiceAreas);
				pmIssuerServiceAreaExt.add(issuerServiceAreaExt);
			}
			issuerServiceArea.setIssuerServiceAreaExt(pmIssuerServiceAreaExt);
			issuerServiceArea = (IssuerServiceArea) mergeEntityManager(entityManager, issuerServiceArea);
		}
		finally {

			if (null == issuerServiceArea) {
				setErrorMsgInMedicareVO(medicareVO, "Failed to persist IssuerServiceArea object.");
				LOGGER.error("Failed to persist IssuerServiceArea object.");
			}
			LOGGER.debug("Persisting PlanMedicareBenefit Object Done");
		}
		return issuerServiceArea;
	}

	private boolean cleanUpExistingChildren(IssuerServiceArea issuerServiceArea, EntityManager entityManager) {

		LOGGER.debug("Cleaning up of existing service area data begins");
		boolean isChildDeleted = false;

		try {

			if (null == issuerServiceArea) {
				return isChildDeleted;
			}

			List<IssuerServiceAreaExt> issuerServiceAreaExts = issuerServiceArea.getIssuerServiceAreaExt();
			if (CollectionUtils.isEmpty(issuerServiceAreaExts)) {
				return isChildDeleted;
			}

			for (IssuerServiceAreaExt issuerServiceAreaExt : issuerServiceAreaExts) {
				LOGGER.debug("Fetching service area records for issuerServiceAreaExt with id " + issuerServiceAreaExt.getId());
				List<ServiceArea> serviceAreas = iServiceAreaRepository.findByServiceAreaIdAndServiceAreaExtIdAndIsDeleted(issuerServiceArea, issuerServiceAreaExt, ServiceArea.IS_DELETED.N.toString());

				if (!CollectionUtils.isEmpty(serviceAreas)) {

					for (ServiceArea serviceArea : serviceAreas) {
						serviceArea.setIsDeleted(ServiceArea.IS_DELETED.Y.toString());
						mergeEntityManager(entityManager, serviceArea);
					}
					LOGGER.debug(SVCAREA_DELETED + issuerServiceArea.getId() + " and issuerServiceAreaExt =" + issuerServiceAreaExt.getId());
				}
				issuerServiceAreaExt.setIsDeleted(IssuerServiceAreaExt.IS_DELETED.Y.toString());
				mergeEntityManager(entityManager, issuerServiceAreaExt);
			}
			isChildDeleted = true;
			LOGGER.info(SVCAREA_DELETED + issuerServiceArea.getId());
		}
		finally {
			LOGGER.debug("Cleaning up of existing service area data ends" );
		}
		return isChildDeleted;
	}

	/**
	 * Method is used to return Plan from Existing Plan List which is match with PUF Plan-ID.
	 */
	private Plan getPlanFromList(List<Plan> existingPlanList, String issuerPlanNumber) {

		Plan existingPlan = null;

		if (CollectionUtils.isEmpty(existingPlanList)) {
			return existingPlan;
		}

		for (Plan plan : existingPlanList) {

			if (plan != null &&
					plan.getIssuerPlanNumber().equalsIgnoreCase(issuerPlanNumber)) {
				existingPlan = plan;
				break;
			}
		}
		return existingPlan;
	}

	/**
	 * Method is used to merge Table to Entity Manager.
	 */
	private Object mergeEntityManager(EntityManager entityManager, Object saveObject) {

		Object savedObject = null;

		if (null != saveObject && null != entityManager && entityManager.isOpen()) {
			savedObject = entityManager.merge(saveObject);
			LOGGER.debug(saveObject.getClass() + " bean has been Persisted Successfully.");
		}
		return savedObject;
	}

	/**
	 * Method is used to Commit/Rollback transaction depends on Status argument.
	 */
	private boolean getTransactionCommit(boolean status, EntityManager entityManager) throws GIException {

		LOGGER.debug("getTransactionCommit() Start");
		boolean commitStatus = status;

		try {

			if (null != entityManager && entityManager.isOpen()) {

				if (status) {
					entityManager.getTransaction().commit();
					LOGGER.info("Transaction Commit has been Commited");
				}
				else {
					entityManager.getTransaction().rollback();
					LOGGER.info("Transaction Commit has been Rollback");
				}
			}
		}
		catch (Exception ex) {
			commitStatus = Boolean.FALSE;
			LOGGER.error("Error occured while commiting Transaction " + commitStatus, ex);
			throw new GIException(ex);
		}
		finally {
			LOGGER.debug("getTransactionCommit() End");
		}
		return commitStatus;
	}

	/**
	 * Method is used to update tracking record of SERFF Plan Management table.
	 */
	private void updateTrackingRecord(boolean savedStatus, boolean warnStatus, PlanUpdateStatistics uploadStatistics,
			SerffPlanMgmt trackingRecord) {

		LOGGER.debug("updateTrackingRecord() Start");

		try {
			if (null == trackingRecord) {
				return;
			}

			if (savedStatus && warnStatus) {
				trackingRecord.setRequestStateDesc("Medicare-PUF Plan has been Loaded with warnings. Please see status message.");
				LOGGER.warn(trackingRecord.getRequestStateDesc());
			}
			else if (savedStatus && !warnStatus) {
				trackingRecord.setRequestStateDesc("Medicare-PUF Plan has been Loaded Successfully.");
				trackingRecord.setPmResponseXml(trackingRecord.getRequestStateDesc());
				LOGGER.debug(trackingRecord.getRequestStateDesc());
			}
			else {
				trackingRecord.setRequestStateDesc("Failed to load Medicare-PUF Plan due to validation errors.");
				LOGGER.error(trackingRecord.getRequestStateDesc());
			}
			trackingRecord.setPlanStats(uploadStatistics.toString());
		}
		finally {
			LOGGER.debug("updateTrackingRecord() End");
		}
	}

	/**
	 * Method is used to verified that Value is numeric or not.
	 */
	private boolean validateNumeric(String value) {
		
		try {
			LOGGER.debug("Numeric value is: " + Double.valueOf(value));
			return true;
		}
		catch (Exception ex) {
			LOGGER.debug("Invalid Decimal value: " + ex.getMessage(), ex);
		}
		return false;
	}

	/**
	 * Method is used to clear Entity Manager Object.
	 */
	private void closeEntityManager(EntityManager entityManager) {

		if (null != entityManager && entityManager.isOpen()) {
			entityManager.clear();
			entityManager.close();
			entityManager = null;
		}
	}
}
