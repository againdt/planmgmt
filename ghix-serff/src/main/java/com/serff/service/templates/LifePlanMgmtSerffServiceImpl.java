package com.serff.service.templates;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.IssuerVerificationStatus;
import com.getinsured.hix.model.PlanLife;
import com.getinsured.hix.model.PlanRate;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.life.LifePlanDataVO;
import com.getinsured.serffadapter.life.LifePlanRatesVO;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.IPlanRate;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.service.validation.LifePlanDataValidator;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 *-----------------------------------------------------------------------------
 * HIX-84392 PHIX - Load and persist Life plans.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to persist Life Insurance Plans in database.
 * 
 * @author Bhavin Parmar
 * @since May 2, 2016
 */
@Service("lifePlanMgmtSerffService")
public class LifePlanMgmtSerffServiceImpl implements LifePlanMgmtSerffService {

	private static final Logger LOGGER = Logger.getLogger(LifePlanMgmtSerffServiceImpl.class);

	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private IIssuersRepository iIssuerRepository;
	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private IPlanRate iPlanRateRepository;
	@Autowired private SerffUtils serffUtils;
	@Autowired private LifePlanDataValidator lifePlanDataValidator;

	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	private static final String PLAN_END_DATE = "31/12/2099";
	private static final int MINUS_ONE_DAY = -1;
	int CODE_NON_TOBACCO_MALE = 00;
	int CODE_TOBACCO_MALE = 01;
	int CODE_NON_TOBACCO_FEMALE = 10;
	int CODE_TOBACCO_FEMALE = 11;

	/**
	 * Method is used to populate and persist Life Insurance plans from Map<String, LifePlanDataVO> to database.
	 */
	@Override
	public boolean populateAndPersistLifeData(Map<String, LifePlanDataVO> lifePlansMapData,
			SerffPlanMgmt trackingRecord, String defaultTenantIds) throws GIException {

		LOGGER.info("populateAndPersistLifeData() Start");
		boolean isDataProcessingComplete = false;
		EntityManager entityManager = null;
		List<Plan> savedPlanList = new ArrayList<Plan>();
		List<LifePlanDataVO> failedLifePlanVOList = new ArrayList<LifePlanDataVO>();
//		Map<String, Boolean> duplicatePlansMap = new HashMap<String, Boolean>(); // Map<String: IssuerPlanNumber, Boolean: Duplicate Or Not>

		try {

			if (!validateParamsForNotNull(lifePlansMapData, trackingRecord, defaultTenantIds)) {
				LOGGER.error("PlanList for Adding Life Insurance Plan is empty !!");
				return isDataProcessingComplete;
			}

			List<Tenant> tenantList = serffUtils.getTenantList(defaultTenantIds);
			if (CollectionUtils.isEmpty(tenantList)) {
				LOGGER.error("Invalid Default Tenant: " + defaultTenantIds);
				return isDataProcessingComplete;
			}
			entityManager = entityManagerFactory.createEntityManager(); // Get DB connection using Entity Manager Factory
			entityManager.getTransaction().begin(); // Begin Transaction
			// Populate and Persist Life Plans from Map<String, LifePlanDataVO>
			populateAndPersistLifePlanMapList(lifePlansMapData, savedPlanList, failedLifePlanVOList, tenantList, entityManager);
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred in populateAndPersistLifeData()"  + ex.getMessage(), ex);
			isDataProcessingComplete = false;
			throw new GIException(ex);
		}
		finally {
			LOGGER.info("savedPlanList: " + CollectionUtils.isEmpty(savedPlanList));

			if (!CollectionUtils.isEmpty(savedPlanList)) {
				isDataProcessingComplete = true;
			}
			isDataProcessingComplete = completeTransaction(isDataProcessingComplete, entityManager);
			updateTrackingRecord(isDataProcessingComplete, savedPlanList, failedLifePlanVOList, trackingRecord);

			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			savedPlanList.clear();
			savedPlanList = null;
			failedLifePlanVOList.clear();
			failedLifePlanVOList = null;
//			duplicatePlansMap.clear();
//			duplicatePlansMap = null;
			LOGGER.info("populateAndPersistLifeData() End with value isDataProcessingComplete: " + isDataProcessingComplete);
		}
		return isDataProcessingComplete;
	}

	/**
	 * Populate and Persist Life Plans from Map<String, LifePlanDataVO>.
	 */
	public void populateAndPersistLifePlanMapList(Map<String, LifePlanDataVO> lifePlansMapData, List<Plan> savedPlanList,
			List<LifePlanDataVO> failedLifePlanVOList, List<Tenant> tenantList, EntityManager entityManager) throws GIException {

		LOGGER.debug("populateAndPersistLifePlanMapList() Start");

		try {

			LifePlanDataVO lifePlanDataVO = null;
			Issuer existingIssuer = null;
			Date startDate = null;

			for (Map.Entry<String, LifePlanDataVO> lifePlanEntry : lifePlansMapData.entrySet()) {

				lifePlanDataVO = lifePlanEntry.getValue();

				//Validate Life Plan. If plan is invalid add in Failed Plan List and continue.
				if (!lifePlanDataValidator.validateLifeExcelVO(lifePlanDataVO)) {
					LOGGER.error("Excel data is not valid for plan");
					failedLifePlanVOList.add(lifePlanDataVO);
					continue;
				}

				if (null == existingIssuer) {
					// Check if Issuer exist in DB for given HIOSID.
					existingIssuer = getIssuerFromDb(lifePlanDataVO.getCarrierHIOSId());

					if (null == existingIssuer) {
						//Issuer is not present in DB.
						lifePlanDataVO.setErrorMessages("Issuer does not exist for hiosIssuerId : " + lifePlanDataVO.getCarrierHIOSId());
						LOGGER.error(lifePlanDataVO.getErrorMessages());
						lifePlanDataVO.setNotValid(true);
						failedLifePlanVOList.add(lifePlanDataVO);
						break;
					}
				}
				startDate = dateFormat.parse(lifePlanDataVO.getStartDate());

				for (String state : lifePlanDataVO.getStateList()) {
					populateAndPersistLifePlan(lifePlanDataVO, existingIssuer, state, startDate, savedPlanList,
							failedLifePlanVOList, tenantList, entityManager);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred in populateAndPersistLifePlanMapList()"  + ex.getMessage(), ex);
			throw new GIException(ex);
		}
		finally {
			LOGGER.debug("populateAndPersistLifePlanMapList() End");
		}
	}

	/**
	 * Populate and Persist Life Plan from LifePlanDataVO POJO.
	 */
	public void populateAndPersistLifePlan(LifePlanDataVO lifePlanDataVO, Issuer existingIssuer, String state,
			Date startDate, List<Plan> savedPlanList, List<LifePlanDataVO> failedLifePlanVOList,
			List<Tenant> tenantList, EntityManager entityManager) throws GIException {

		LOGGER.debug("populateAndPersistLifePlan() Start");

		try {

			/*if (!CollectionUtils.isEmpty(duplicatePlansMap) && duplicatePlansMap.containsKey(issuerPlanNumber)) {
				duplicatePlansMap.put(issuerPlanNumber, true); // Override Key with flag value 
				continue;
			}
			duplicatePlansMap.put(issuerPlanNumber, false);*/

			int applicableYear = 0;
			// Appending Off Exchange Variant in Issuer Plan Number
			String issuerPlanNumber = lifePlanDataVO.getCarrierHIOSId() + state + lifePlanDataVO.getPlanId() + SerffConstants.OFF_EXCHANGE_VARIANT_SUFFIX;
			LOGGER.debug("Processing Life Insurance plan : " + issuerPlanNumber);
			// Start processing and persisting Life Insurance Plan data.
			processToModifyExistingPlans(issuerPlanNumber, applicableYear, startDate, entityManager);
			// Save Plan object.
			Plan savedPlan = savePlan(applicableYear, issuerPlanNumber, existingIssuer, lifePlanDataVO, state, startDate, entityManager);

			if (null != savedPlan) {
				// Save Plan Life Insurance Object.
				savePlanLife(savedPlan, lifePlanDataVO, entityManager);
				// Save Rates for Life Insurance plan.
				saveLifePlanRatesData(savedPlan, startDate, lifePlanDataVO, entityManager);
				// Set tenant
				serffUtils.saveTenantPlan(savedPlan, tenantList, entityManager);
				// Add Plan in saved plans list.
				LOGGER.debug("Processing of Life Insurance plan is successful. Saving plan in DB: " + issuerPlanNumber);
				savedPlanList.add(savedPlan);
			}
		}
		finally {
			LOGGER.debug("populateAndPersistLifePlan() End");
		}
	}

	/**
	 * Saves Parent Plan instance in PLAN table
	 * @return - Saved plan in DB
	 */
	private Plan savePlan(int applicableYear, String issuerPlanNumber, Issuer existingIssuer, LifePlanDataVO lifePlanDataVO,
			String state, Date startDate, EntityManager entityManager) {

		LOGGER.debug("savePlan() Start: " + issuerPlanNumber);
		Plan plan = null;

		try {
			if (StringUtils.isNotBlank(issuerPlanNumber)) {
				plan = new Plan();
				plan.setIssuerVerificationStatus(IssuerVerificationStatus.VERIFIED.toString());
				plan.setIssuer(existingIssuer);
				plan.setInsuranceType(Plan.PlanInsuranceType.LIFE.name());
				plan.setStatus(Plan.PlanStatus.CERTIFIED.toString());
				plan.setEnrollmentAvail(Plan.EnrollmentAvail.AVAILABLE.toString());
				plan.setStartDate(startDate);
				plan.setEnrollmentAvailEffDate(startDate);
				plan.setEndDate(dateFormat.parse(PLAN_END_DATE));
				plan.setEnrollmentEndDate(plan.getEndDate());
				plan.setIssuerPlanNumber(issuerPlanNumber);
				plan.setName(lifePlanDataVO.getPlanName());
				plan.setHsa(SerffConstants.NO);
				plan.setState(state);
				plan.setMarket(Plan.PlanMarket.INDIVIDUAL.toString());
				plan.setHiosProductId(issuerPlanNumber.substring(0, SerffConstants.LEN_HIOS_PRODUCT_ID));
				plan.setExchangeType(Plan.EXCHANGE_TYPE.OFF.toString());
				plan.setIsPUF(Plan.IS_PUF.N);
				plan.setIsDeleted(Plan.IS_DELETED.N.toString());
				plan.setApplicableYear(applicableYear);
				plan.setCreatedBy(SerffConstants.DEFAULT_USER_ID);
				plan.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);
				plan = (Plan) mergeEntityManager(entityManager, plan);
			}
		}
		catch (ParseException pe) {
			LOGGER.debug("Exception occurred while parsing date: " + pe.getMessage());
		}
		finally {
			LOGGER.debug("savePlan() End");
		}
		return plan;
	}

	/**
	 * Saves Life Insurance Plan instance in database.
	 * @return - Saved Life Insurance Plan instance
	 */
	private PlanLife savePlanLife(Plan savedPlan, LifePlanDataVO lifePlanDataVO, EntityManager entityManager) {

		LOGGER.debug("savePlanLife() Plan Name : " + lifePlanDataVO.getPlanName());
		PlanLife planLife = new PlanLife();

		try {
			planLife.setPlan(savedPlan);
			planLife.setBenefitURL(lifePlanDataVO.getBenefitDocument());
			planLife.setPlanDuration(Integer.parseInt(lifePlanDataVO.getPlanDuration()));
			planLife.setCoverageMin(Integer.parseInt(lifePlanDataVO.getMinCoverageAmount().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
			planLife.setCoverageMax(Integer.parseInt(lifePlanDataVO.getMaxCoverageAmount().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
			planLife.setAnnualPolicyFee(Double.parseDouble(lifePlanDataVO.getAnnualPolicyFee().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
			planLife.setCreatedBy(SerffConstants.DEFAULT_USER_ID);
			planLife.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);

			planLife = (PlanLife) mergeEntityManager(entityManager, planLife);
		}
		finally {
			LOGGER.debug("savePlanLife() End");
		}
		return planLife;
	}

	/**
	 * Method is used to save Plan Rates data.
	 */
	private void saveLifePlanRatesData(Plan savedPlan, Date startDate, LifePlanDataVO lifePlanDataVO, EntityManager entityManager) {

		LOGGER.debug("saveLifePlanRatesData() Start");

		try {

			for (LifePlanRatesVO lifePlanRatesVO : lifePlanDataVO.getLifePlanRateList()) {

				if (null == lifePlanRatesVO) {
					continue;
				}
				saveLifePlanRate(savedPlan, lifePlanRatesVO, CODE_NON_TOBACCO_MALE, startDate, entityManager);
				saveLifePlanRate(savedPlan, lifePlanRatesVO, CODE_TOBACCO_MALE, startDate, entityManager);
				saveLifePlanRate(savedPlan, lifePlanRatesVO, CODE_NON_TOBACCO_FEMALE, startDate, entityManager);
				saveLifePlanRate(savedPlan, lifePlanRatesVO, CODE_TOBACCO_FEMALE, startDate, entityManager);
			}
		}
		finally {
			LOGGER.debug("saveLifePlanRatesData() End");
		}
	}

	/**
	 * Method is used to save Plan Rate data.
	 */
	private void saveLifePlanRate(Plan savedPlan, LifePlanRatesVO lifePlanRatesVO, int tobaccoCode, Date startDate,
			EntityManager entityManager) {

		LOGGER.debug("saveLifePlanRate() Start");

		try {

			PlanRate planRate = new PlanRate();

			if (CODE_NON_TOBACCO_MALE == tobaccoCode) {
				planRate.setTobacco(SerffConstants.NO_ABBR);
				planRate.setGender(SerffConstants.MALE);
				planRate.setRate(Float.parseFloat(lifePlanRatesVO.getNonTobaccoMale().trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
			}
			else if (CODE_TOBACCO_MALE == tobaccoCode) {
				planRate.setTobacco(SerffConstants.YES_ABBR);
				planRate.setGender(SerffConstants.MALE);
				planRate.setRate(Float.parseFloat(lifePlanRatesVO.getTobaccoMale().trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
			}
			else if (CODE_NON_TOBACCO_FEMALE == tobaccoCode) {
				planRate.setTobacco(SerffConstants.NO_ABBR);
				planRate.setGender(SerffConstants.FEMALE);
				planRate.setRate(Float.parseFloat(lifePlanRatesVO.getNonTobaccoFemale().trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
			}
			else if (CODE_TOBACCO_FEMALE == tobaccoCode) {
				planRate.setTobacco(SerffConstants.YES_ABBR);
				planRate.setGender(SerffConstants.FEMALE);
				planRate.setRate(Float.parseFloat(lifePlanRatesVO.getTobaccoFemale().trim().replaceAll(SerffConstants.DOLLAR, StringUtils.EMPTY).replaceAll(SerffConstants.COMMA, StringUtils.EMPTY)));
			}
			planRate.setPlan(savedPlan);
			planRate.setMaxAge(Integer.parseInt(lifePlanRatesVO.getMaximumAge()));
			planRate.setMinAge(Integer.parseInt(lifePlanRatesVO.getMinimumAge()));
			planRate.setLastUpdatedBy(SerffConstants.DEFAULT_USER_ID);
			planRate.setRateOption(PlanRate.RATE_OPTION.A);
			planRate.setIsDeleted(PlanRate.IS_DELETED.N.toString());
			planRate.setEffectiveStartDate(startDate);
			planRate.setEffectiveEndDate(savedPlan.getEndDate());
			mergeEntityManager(entityManager, planRate);
		}
		finally {
			LOGGER.debug("saveLifePlanRate() End");
		}
	}

	/**
	 * Returns existing issuer from database for provided HiosId.
	 * 
	 * @return - Existing The Issuer instance from DB.
	 */
	private Issuer getIssuerFromDb(String issuerHiosId) {

		LOGGER.debug("getIssuerFromDb() satrt");
		Issuer existingIssuer = null;

		try {

			if (StringUtils.isNotBlank(issuerHiosId)) {
				existingIssuer = iIssuerRepository.getIssuerByHiosID(issuerHiosId);
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception while retrieving issuer from database.", e);
		}
		finally {
			LOGGER.debug("getIssuerFromDb() End: existingIssuer : " + existingIssuer);
		}
		return existingIssuer;
	}

	/**
	 * Process To Modify Existing Plans:
	 * 
	 * 1) Soft delete all Plans of future for StartDate [Existing Start Date >= New Start Date]
	 * 2) Modified End Date of existing Plans before Start Date [Existing Start Date < New Start Date AND Existing End Date >= New Start Date]
	 */
	private void processToModifyExistingPlans(String issuerPlanNumber, int applicableYear, Date startDate, EntityManager entityManager) {

		LOGGER.debug("processToModifyExistingPlans() Start");

		try {
			List<Plan> existingFuturePlansForStartDate = iPlanRepository.findFuturePlansForStartDate(issuerPlanNumber, startDate, applicableYear, Plan.PlanInsuranceType.LIFE.name());

			if (CollectionUtils.isEmpty(existingFuturePlansForStartDate)) {
				LOGGER.info("There is no existing Future Life Insurance plan found for " + issuerPlanNumber + " in database.");
			}
			else {
				LOGGER.info("Existing Future Life Insurance plan found for IssuerPlanNumber: " + issuerPlanNumber + ", Number of Plans: " + existingFuturePlansForStartDate.size());

				for (Plan existingLifePlan : existingFuturePlansForStartDate) {
					//Soft Delete all existing plans. In case any of them is CERTIFIED, skip first such plan and soft delete others (normally there should be single such plan) 
					if (null == existingLifePlan) {
						continue;
					}
		     		existingLifePlan.setIsDeleted(Plan.IS_DELETED.Y.toString());
	
					if (null != entityManager && entityManager.isOpen()) {
						entityManager.merge(existingLifePlan);
						LOGGER.info("Existing Future Life Insurance Plan["+ existingLifePlan.getIssuerPlanNumber() +"] has been soft deleted successfully.");
					}
				}
			}

			List<Plan> existingOverlappingPlanForStartDate = iPlanRepository.findOverlappingPlanForStartDate(issuerPlanNumber, startDate, applicableYear, Plan.PlanInsuranceType.LIFE.name());

			if (CollectionUtils.isEmpty(existingOverlappingPlanForStartDate)) {
				LOGGER.info("There is no existing Overlapping Life Insurance plan found for " + issuerPlanNumber + " in database.");
			}
			else {
				LOGGER.info("Existing Overlapping Life Insurance plan found for IssuerPlanNumber: " + issuerPlanNumber);

				if (1 < existingOverlappingPlanForStartDate.size()) {
					LOGGER.warn("There are more that one existing Overlapping Life Plans Before Start Date for IssuerPlanNumber: " + issuerPlanNumber);
				}

				Plan existingLifePlan = existingOverlappingPlanForStartDate.get(0);
				if (null != existingLifePlan) {
					existingLifePlan.setEndDate(DateUtil.addToDate(startDate, MINUS_ONE_DAY));
					existingLifePlan.setEnrollmentEndDate(existingLifePlan.getEndDate());

					if (null != entityManager && entityManager.isOpen()) {
						entityManager.merge(existingLifePlan);
						LOGGER.info("End Date has been modified successfully for existing Overlapping Life Insurance Plan: "+ existingLifePlan.getIssuerPlanNumber());
					}
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while searching existing Life Insurance plan in database for issuerPlanNumber : " + issuerPlanNumber, ex);
		}
		finally {
			LOGGER.info("processToModifyExistingPlans() End");
		}
	}

	/**
	 *  Method is used to update tracking record of SERFF Plan Management table.
	 * 
	 * @param isDataProcessingComplete - Whether data processing is completed
	 * @param savedPlanList - Plans those are saved successfully
	 * @param failedPlanVOList - Plans those are failed to persist
	 * @param trackingRecord - Serff Plan Mgmt record.
	 */
	private void updateTrackingRecord(boolean isDataProcessingComplete, List<Plan> savedPlanList,
			List<LifePlanDataVO> failedPlanVOList, SerffPlanMgmt trackingRecord) {

		LOGGER.debug("updateTrackingRecord() Start");

		try {
			if (null == trackingRecord) {
				return;
			}

			String errorMessages = createErrorMessage(failedPlanVOList);

			if (isDataProcessingComplete) {
				//Process completed. No plans saved.All Certified plans or All plan's validation failed.
				if (CollectionUtils.isEmpty(savedPlanList)) {
					trackingRecord.setRequestStateDesc("Process completed successfully. No Life Insurance Plans Loaded. Please see status message.");
				}
				//Process completed. All plans saved.
				else if (StringUtils.isBlank(errorMessages)) {
					trackingRecord.setRequestStateDesc("Life Insurance Plan has been Loaded Successfully.");
				}
				//Process completed. Few Plans saved and few plans are Not Saved in db.
				else {
					//Either more than one plans are certified or more than one plans with Validation errors present.
					trackingRecord.setRequestStateDesc("Life Insurance Plan has been Loaded with warnings. Please see status message.");
					trackingRecord.setPmResponseXml(errorMessages);
				}
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
				LOGGER.info(trackingRecord.getRequestStateDesc());
			}
			else {
				//Exception occurred while committing or merging entity.
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
				trackingRecord.setPmResponseXml(errorMessages);
				trackingRecord.setRequestStateDesc("Error occurred while saving Life Insurance plans. Check response for details.");
				LOGGER.error(trackingRecord.getRequestStateDesc());
			}
		}
		finally {

			if (null != trackingRecord && StringUtils.isBlank(trackingRecord.getPmResponseXml())
					&& StringUtils.isNotBlank(trackingRecord.getRequestStateDesc())) {
				trackingRecord.setPmResponseXml(trackingRecord.getRequestStateDesc());
			}
			LOGGER.debug("updateTrackingRecord() End");
		}
	}

	/**
	 * Creates error message by reading error messages set in failed list of Life Insurance Plan VO objects.
	 * 
	 * @param failedLifePlanVOList - Life Insurance plans those are failed to persist.
	 * @return - String representation of error messages.
	 */
//	private String createErrorMessage(List<LifePlanDataVO> failedLifePlanVOList, Map<String, Boolean> duplicatePlansMap) {
	private String createErrorMessage(List<LifePlanDataVO> failedLifePlanVOList) {

		LOGGER.debug("createErrorMessage() Start");
		StringBuffer errorMessage = new StringBuffer();

		if (!CollectionUtils.isEmpty(failedLifePlanVOList)) {

			for (LifePlanDataVO lifePlanDataVO : failedLifePlanVOList) {

				if (StringUtils.isBlank(lifePlanDataVO.getErrorMessages())) {
					continue;
				}
				errorMessage.append(lifePlanDataVO.getErrorMessages());
				errorMessage.append("\n");
			}
		}

		/*if (!CollectionUtils.isEmpty(duplicatePlansMap)) {
			List<String> duplicatePlansList = new ArrayList<String>();

			for (Map.Entry<String, Boolean> entryData : duplicatePlansMap.entrySet()) {

				if (null == entryData.getValue() || !entryData.getValue()) {
					continue;
				}
				duplicatePlansList.add(entryData.getKey());
			}

			if (!CollectionUtils.isEmpty(duplicatePlansList)) {
				errorMessage.append("Skipped duplicate IssuerPlanNumber(s) from Template: ");
				errorMessage.append(duplicatePlansList.toString());
			}
		}*/
		LOGGER.error(errorMessage.toString());
		LOGGER.debug("createErrorMessage() End");
		return errorMessage.toString();
	}

	/**
	 * Method is used to validate Null values for parameters.
	 */
	private boolean validateParamsForNotNull(Map<String, LifePlanDataVO> lifePlansMapData,
			SerffPlanMgmt trackingRecord, String defaultTenantIds) {

		boolean status = true;

		if (CollectionUtils.isEmpty(lifePlansMapData)) {
			LOGGER.error("PlanList for Adding Life Insurance Plan is empty !!");
			status = false;
		}

		if (null == trackingRecord) {
			LOGGER.error("Tracking Record(SerffPlanMgmt) is null.");
			status = false;
		}

		if (StringUtils.isBlank(defaultTenantIds)) {
			LOGGER.error("Default Tenant IDs is empty.");
			status = false;
		}
		return status;
	}

	/**
	 * Method is used to Commit/Rollback transaction depends on Status argument.
	 * 
	 * @param commitRequestStatus - Request to whether commit the transaction
	 * @param entityManager - The entity manager instance
	 * @return - Whether transaction is committed
	 * @throws GIException
	 */
	private boolean completeTransaction(boolean commitRequestStatus, EntityManager entityManager) throws GIException {

		LOGGER.debug("completeTransaction() Start");
		boolean commitResponseStatus = false;

		try {

			if (null != entityManager && entityManager.isOpen()) {

				if (commitRequestStatus) {
					entityManager.getTransaction().commit();
					commitResponseStatus = true;
					LOGGER.info("Transaction has been Commited");
				}
				else {
					entityManager.getTransaction().rollback();
					commitResponseStatus = false;
					LOGGER.info("Transaction has been Rollback");
				}
			}
		}
		catch (Exception ex) {
			commitResponseStatus = false;
			LOGGER.error("Error occured while commiting Transaction " + commitResponseStatus, ex);
			throw new GIException(ex);
		}
		finally {
			LOGGER.debug("completeTransaction() End with Commit Status : "  + commitResponseStatus);
		}
		return commitResponseStatus;
	}

	/**
	 * Merges entities by using provided entity manager.
	 * 
	 * @param entityManager  -The entity manager instance.
	 * @param saveObject - Entity to be saved in database.
	 * @return - Entity saved in database.
	 */
	private Object mergeEntityManager(EntityManager entityManager, Object saveObject) {

		LOGGER.debug("mergeEntityManager() Start");
		Object savedObject = null;

		if (null != saveObject && null != entityManager && entityManager.isOpen()) {
			savedObject = entityManager.merge(saveObject);
			LOGGER.debug(saveObject.getClass() + " bean has been Persisted Successfully.");
		}
		LOGGER.debug("mergeEntityManager() End");
		return savedObject;
	}
}
