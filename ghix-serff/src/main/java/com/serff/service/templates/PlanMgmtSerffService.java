/**
 * PlanMgmtSerffSerivice.java
 * @author chalse_v
 * @version 1.0
 * @since Apr 4, 2013
 */
package com.serff.service.templates;

import java.util.Map;

import javax.persistence.EntityManager;

import com.getinsured.hix.model.DrugList;
import com.getinsured.hix.model.Formulary;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.platform.util.exception.GIException;
import com.serff.template.csr.extension.CSRAdvancePaymentDeterminationType;
import com.serff.template.plan.PrescriptionDrugTemplateVO;
import com.serff.util.PlanUpdateStatistics;


/**
 * PlanManagement SERFF Service interface.
 *
 * @author chalse_v
 * @version 1.1
 * @since May 15, 2013
 *
 */
public interface PlanMgmtSerffService {
	/**
	 * Method to populate bean (Issuer, Network etc) from a given SERFF request instance.
	 *
	 * @param reqTemplates The template instances provided by SERFF to persist.
	 * @return status The boolean status of operation.
	 * @throws GIException
	 */
	boolean populateAndPersistAllBeansFromSERFF(Map<String, Object> reqTemplates, String exchangeTypeFilter, StringBuffer responseMessage, 
			String tenantCode, PlanUpdateStatistics uploadStatistics, byte[] logoBytes) throws GIException;

	boolean populateAndPersistCSRAmountFromSERFF(CSRAdvancePaymentDeterminationType advancePaymentDeterminationType) throws GIException;

	Plan getRequiredPlan(int issuerID, String issuerPlanNumber);

	PlanHealth getRequiredPlanHealthObject(int issuerID, int planID, String csType);

	String updateCsrAmount(Float csrAdvPayAmt, Integer issuerID, Integer planID, String csType);

	Plan getPlanByHIOSID(String hiosId);

	Map<String, Formulary> populateAndPersistPrescriptionDrugBean(int applicableYear, EntityManager entityManager, Issuer issuer, PrescriptionDrugTemplateVO prescriptionDrugTemplateVO);

	Map<Integer, DrugList> loadDrugList(EntityManager entityManager, Issuer issuerObj, PrescriptionDrugTemplateVO prescriptionDrugTemplateVO);
}
