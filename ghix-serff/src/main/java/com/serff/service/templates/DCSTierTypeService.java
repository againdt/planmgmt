/**
 * File 'DCSTierTypeService' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:19:18 AM
 */
package com.serff.service.templates;

import com.getinsured.hix.model.DCSTierType;

/**
 * Service interface for handling DCSTierType persistence etc. related requests.
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:19:18 AM
 *
 */
public interface DCSTierTypeService {
	DCSTierType saveDCSTierType(DCSTierType dcsTierType);
}
