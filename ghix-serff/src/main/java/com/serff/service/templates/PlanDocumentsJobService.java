package com.serff.service.templates;

import java.util.List;
import java.util.Map;

import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanDocumentsJob;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.plandoc.PlanDocumnetExcelVO;

/**
 * Interface is used to provide services for Plan Documents [SBC, EOC, Brochure, etc].
 * @author vardekar_s
 */
public interface PlanDocumentsJobService {

	int persistPlanDocumentRecord(PlanDocumentsJob documentJob);

	Long getPlanCount(@Param("planId") String planId, @Param("applicableYear") Integer applicableYear);

	Long getPlanHealthCount(@Param("planId") String planId, @Param("applicableYear") Integer applicableYear);

	Long getPlanDentalCount(@Param("planId") String planId, @Param("applicableYear") Integer applicableYear);

	Map<String, String> updatePlanDocuments(Map<String, String[]> planIdBrochureMap);

	Object getEnvironmentTestResult(String env);

	boolean populateAndPersistPlanDocumentData(List<PlanDocumnetExcelVO> planDocumnetExcelList, SerffPlanMgmt trackingRecord) throws GIException;
}
