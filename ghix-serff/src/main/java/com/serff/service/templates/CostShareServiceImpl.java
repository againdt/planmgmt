/**
 * File 'CostShareServiceImpl.java' for defining persistence contract for 'COST_SHARE' records.
 *
 * @author chalse_v
 * @since Jun 14, 2013
 *
 */
package com.serff.service.templates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.CostShare;
import com.serff.repository.templates.ICostShareRepository;

/**
 * Service implementor class 'CostShareServiceImpl' for defining persistence contract for 'CostShare' data.
 *
 * @author chalse_v
 * @version 1.0
 * @since Jun 14, 2013
 *
 */
@Repository
@Transactional
public class CostShareServiceImpl implements CostShareService {
	private static final Logger LOGGER = LoggerFactory.getLogger(FormularyCostShareBenefitServiceImpl.class);

	@Autowired(required = true)
	private ICostShareRepository costShareRepository;

	/**
	 * Default constructor.
	 */
	public CostShareServiceImpl() {
	}

	/**
	 * @see com.serff.service.templates.CostShareService#saveCostShare(com.getinsured.hix.model.CostShare)
	 */
	@Override
	public CostShare saveCostShare(CostShare costShare) {
		LOGGER.info("About to persist CostShare data.");
		return costShareRepository.save(costShare);
	}

}
