package com.serff.service.templates;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Network;
import com.serff.repository.templates.INetworkRepository;

@Service("providerNetworkService")
@Repository
@Transactional
public class ProviderNetworkServiceImpl implements ProviderNetworkService{

	// private static final Logger logger = LoggerFactory.getLogger(ProviderNetworkServiceImpl.class);
	
	@Autowired private INetworkRepository iNetworkRepository;
	
	@Override
	@Transactional
	public Network saveNetwork(Network network){
		return iNetworkRepository.save(network);
	}
}
