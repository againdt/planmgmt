package com.serff.service.templates;

import java.util.Map;

import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.vision.VisionPlanDataVO;

/**
 * Interface is used to persist Vision Plans to database using
 * VisionPlanMgmtSerffServiceImpl class.
 * 
 * @author Bhavin Parmar
 * @since January 27, 2015
 */
public interface VisionPlanMgmtSerffService {

	boolean populateAndPersistVisionData(Map<String, VisionPlanDataVO> visionPlansMap,
			VisionPlanDataVO visionExcelHeader, String defaultTenantIds, SerffPlanMgmt trackingRecord) throws GIException;
}
