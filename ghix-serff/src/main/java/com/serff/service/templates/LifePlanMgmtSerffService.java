package com.serff.service.templates;

import java.util.Map;

import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.life.LifePlanDataVO;

/**
 *-----------------------------------------------------------------------------
 * HIX-84392 PHIX - Load and persist Life plans.
 *-----------------------------------------------------------------------------
 * 
 * Interface is used to persist Life Insurance Plans in database using LifePlanMgmtSerffServiceImpl class.
 * 
 * @author Bhavin Parmar
 * @since May 2, 2016
 */
public interface LifePlanMgmtSerffService {

	boolean populateAndPersistLifeData(Map<String, LifePlanDataVO> lifePlansMapData, SerffPlanMgmt trackingRecord, String defaultTenantIds) throws GIException;
}
