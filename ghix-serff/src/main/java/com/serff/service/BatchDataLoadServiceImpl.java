package com.serff.service;

import static com.serff.util.SerffConstants.FTP_PLAN_DOCS_FILE;
import static com.serff.util.SerffConstants.TRANSFER_DRUGS_JSON;
import static com.serff.util.SerffConstants.TRANSFER_PRESCRIPTION_DRUG;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.activation.DataHandler;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.prescription.DrugPlanDTO;
import com.getinsured.hix.dto.prescription.DrugPlanListJsonDTO;
import com.getinsured.hix.model.Drug;
import com.getinsured.hix.model.DrugList;
import com.getinsured.hix.model.DrugTier;
import com.getinsured.hix.model.Formulary;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmt.REQUEST_STATE;
import com.getinsured.hix.model.serff.SerffPlanMgmt.REQUEST_STATUS;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.model.serff.SerffTemplates;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.serff.planmanagementexchangeapi.common.model.pm.Attachment;
import com.serff.planmanagementexchangeapi.common.model.pm.Attachments;
import com.serff.planmanagementexchangeapi.common.model.pm.SerffStatus;
import com.serff.planmanagementexchangeapi.common.model.pm.SupportingDocument;
import com.serff.planmanagementexchangeapi.common.model.pm.SupportingDocuments;
import com.serff.planmanagementexchangeapi.exchange.model.pm.AccreditationInfo;
import com.serff.planmanagementexchangeapi.exchange.model.pm.CompanyInfo;
import com.serff.planmanagementexchangeapi.exchange.model.pm.Insurer;
import com.serff.planmanagementexchangeapi.exchange.model.pm.InsurerAttachment;
import com.serff.planmanagementexchangeapi.exchange.model.pm.MarketType;
import com.serff.planmanagementexchangeapi.exchange.model.pm.Plan;
import com.serff.planmanagementexchangeapi.exchange.model.pm.PlanMetalLevel;
import com.serff.planmanagementexchangeapi.exchange.model.pm.TransferDataTemplate;
import com.serff.planmanagementexchangeapi.exchange.model.pm.TransferDataTemplates;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlan;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlanResponse;
import com.serff.repository.ISerffPlanMgmt;
import com.serff.repository.ISerffTemplates;
import com.serff.repository.IserffPlamMgmtBatch;
import com.serff.repository.templates.IDrugTierRepository;
import com.serff.repository.templates.IFormularyRepository;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.repository.templates.ITenantRepository;
import com.serff.service.templates.FormularyService;
import com.serff.service.templates.PlanMgmtSerffService;
import com.serff.service.validation.PrescriptionDrugValidator;
import com.serff.template.admin.extension.InsuranceCompanyType;
import com.serff.template.admin.extension.IssuerType;
import com.serff.template.admin.extension.PayloadType;
import com.serff.template.admin.hix.core.OrganizationAugmentationType;
import com.serff.template.admin.hix.core.OrganizationType;
import com.serff.template.admin.hix.core.PersonType;
import com.serff.template.admin.hix.pm.InsurancePlanLevelCodeType;
import com.serff.template.admin.niem.core.AddressType;
import com.serff.template.admin.niem.core.ContactInformationType;
import com.serff.template.admin.niem.core.FullTelephoneNumberType;
import com.serff.template.admin.niem.core.IdentificationType;
import com.serff.template.admin.niem.core.LocationType;
import com.serff.template.admin.niem.core.PersonNameType;
import com.serff.template.admin.niem.core.ProperNameTextType;
import com.serff.template.admin.niem.core.StreetType;
import com.serff.template.admin.niem.core.StructuredAddressType;
import com.serff.template.admin.niem.core.TelephoneNumberType;
import com.serff.template.admin.niem.core.TextType;
import com.serff.template.admin.niem.proxy.AnyURI;
import com.serff.template.admin.niem.usps.states.USStateCodeSimpleType;
import com.serff.template.admin.niem.usps.states.USStateCodeType;
import com.serff.template.plan.FormularyVO;
import com.serff.template.plan.PlanAndBenefitsPackageVO;
import com.serff.util.PlanUpdateStatistics;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

@Service("batchDataLoadService")
public class BatchDataLoadServiceImpl implements BatchDataLoadService {

	private static final Logger LOGGER = Logger.getLogger(BatchDataLoadServiceImpl.class);
	
	private static boolean isJobRunning = false;
	//Constants
	private String serviceURl;
	
//	@Autowired private IPrescriptionDrugValidator prescriptionDrugValidatorV4;
	@Autowired private ISerffPlanMgmt iSerffPlanMgmt;
	// @Autowired private ISerffDocument iSerffDocument;
	@Autowired private IserffPlamMgmtBatch iSerffPlanMgmtBatch;
	@Autowired private IIssuersRepository iIssuerRepository;
	@Autowired private ITenantRepository iTenantRepository;
	@Autowired private GHIXSFTPClient ftpClient;	
	@Autowired private SerffUtils serffUtils;
	@Autowired private ISerffPlanRepository iSerffPlanRepository;
	@Autowired private IFormularyRepository iSerffFormularyRepository;
	@Autowired private IDrugTierRepository iSerffDrugTierRepository;
//	@Autowired private IFormularyCostShareBenefitRepository iformualryCostShareBrnefit;
//	@Autowired private ICostShareRepository iCostShare;
	@Autowired private PlanMgmtSerffService planMgmtService;
	@Autowired private SerffService serffService;
	@Autowired private ContentManagementService ecmService;
//	@Autowired private IDrugListRepository iDrugList;
	@Autowired private ISerffTemplates iSerffTemplates;
	@Autowired private PrescriptionDrugValidator prescriptionDrugValidator;
	@Autowired private SerffResourceUtil serffResourceUtil;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private Gson platformGson;
	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private FormularyService formularyService;

	@Value("#{configProp.serffServiceURL}")
	public void serServiceUrl(String serviceUrl){
		serviceURl = serviceUrl;
	}
	
	private static final String SOAP_HEADER = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
								"<soapenv:Header/>"+
								"<soapenv:Body>";	
	
	private static final String SOAP_FOOTER = "</soapenv:Body></soapenv:Envelope>";
	
	//UI Messages. Plan Job.
    private static final String NO_JOB_WAITING_MESSAGE = "There is no job in waiting state to be processed !!";
    private static final String ALL_SERVER_BUSY_MESSAGE = "All Server are busy, please try gain later !!";
//  private static final String SERFF_SERVICE_ERROR_MESSAGE = "Error with SERFF-Service Server list configuration !! ";
    private static final String SERFF_SERVICE_LIST_ERROR_MESSAGE = "SERFF-Service Server list is not configured !! ";
    private static final String JOB_QUEUE_MESSAGE = "Job has been queued to run later...";
    private static final String MESSAGE_MARKING_TEXT = " ================== ";
    private static final String JOB_STARTED_TEXT = " Starting SERFF Job # ";
    private static final String SERFF_JOB_NUMBER_TEXT = " SERFF Job # ";
    private static final String JOB_ENDED_TEXT = " Completed SERFF Job # ";
    private static final String STATUS_FLAG_TEXT = " SUCCESS = ";
    private static final String MSG_ISSUER_ID = " : Issuer id : ";
    //UI Messages. Prescription Drug Job.
    private static final String MSG_INCOMPATIBLE_TEMPLATES = "Error occurred while processing prescription drug templates due to incompatible template types.";
    private static final String MSG_INVALID_TEMPLATE = "Prescription Drug Batch has been failed due to invalid template count or template type.";
    private static final String MSG_ISSUER_NOT_AVAILABLE = "Prescription Drug Batch has been failed due to Issuer is not available in DB with HIOS ID ";
    private static final String MSG_ERROR_OCCURRED = "Error occurred while processing prescription drug templates.";
    private static final String MSG_INVALID_REQUEST = "Invalid Request.";
    private static final String MSG_VALIDATION_ERROR = "Prescription Drug Batch has been failed. Vaidation errors present in template.";
    private static final String MSG_PROCESS_COMPLETED_SUCCESS = "Process completed successfully.";
    private static final String MSG_PROCESS_FAILED = "Templates Processing Failed.";
	//This is default idle time value, if specified in config properties as SERFF_BatchServerIdleTime, that overrides this default
    
    private static final String MSG_FILE_NOT_FOUND = "Error occurred while processing prescription drug JSON, No file found.";
    private static final String MSG_JSON_PROCESS_FAILED = "Prescription Drugs JSON Processing Failed.";
    private static final String MSG_INCOMPATIBLE_JSON = "Error occurred while processing prescription drug JSON due to incompatible type.";
    private static final String MSG_ERROR_MISSING_DATA = "Error occurred while processing prescription drug JSON as Drug data is not found in DB.";
    private static final String MSG_ERROR_OCCURRED_PROCESSING_DRUG_JSON = "Error occurred while processing prescription drug templates.";

	private static final String MSG_DRUG_LIST_VALIDATION_ERROR = "Prescription DrugList Batch has been failed. Vaidation errors present in template.";
	private static final String MSG_DRUG_LIST_ERROR_OCCURRED = "Error occurred while processing drug-list from prescription drug templates."; 
	private static final String MSG_DRUGLIST_INVALID_TEMPLATE = "Prescription DrugList Batch has been failed due to invalid template type.";
    private static final String MSG_DRUGLIST_ISSUER_NOT_AVAILABLE = "Prescription DrugList Batch has failed as Issuer is not available in DB with HIOS ID ";

	private static final int CUTOFF = 60;
    private static final int ONE_MINS_IN_MS = 60000;
    private static final int ONE_SECOND = 1000;
    private static int CUTOFF_TIME = CUTOFF * ONE_MINS_IN_MS;
    private static final int DEFAULT_YEAR = 2015;

	private static final int HTTP_SUCCESS_CODE = 200;
	//This is default Timeout value, if specified in config properties as SERFF_BatchWSCallTimeout, that overrides this default
	private static final int CONN_TIMEOUT = 150000;
	private static int DEFAULT_CONN_TIMEOUT = CONN_TIMEOUT;
	private boolean useFTPForTemplates = false;

	public enum STATUS_KEY {
		PASS_COUNT, FAILED_COUNT, STATUS_MESSAGE, IS_BREAK, IS_BATCH_SUCCESS ;
	}
	
	public enum STATUS_MESSAGE_KEY {
		UI_MESSAGE, RESPONSE_XML_MESSAGE, PROCESS_DESCRIPTION_MESSAGE, REMARKS_MESSAGE ;
	}
	
	private static void setStaticDefaultConnTimeout(int connTimeout) {
		DEFAULT_CONN_TIMEOUT = connTimeout;
	}
	
	private static void setStaticCutOffTime(int cutOffTime) {
		CUTOFF_TIME = cutOffTime;
	}
	
	private static void setJobRunning(boolean jobRunning) {
		isJobRunning = jobRunning;
	}
	
	/**
	 * Method is used to Load plans from FTP to SERFF. 
	 * @return Status message of SERFF Batch.
	 */
	@Override
	public String processSerffPlanMgmtBatch() {
		
		LOGGER.info("processSerffPlanMgmtBatch() Begin.");
		String message = NO_JOB_WAITING_MESSAGE;
		int failCount = 0, passCount = 0;
		
		if(!isJobRunning) {
			setJobRunning(true);
			useFTPForTemplates = (SerffConstants.PHIX_PROFILE.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE))
								|| (! SerffConstants.CA_STATE_CODE.equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE))));
			//isPHIXProfile = false;
			try {
				
				if (SerffConstants.getSerffBatchWSCallTimeout() != null) {
					setStaticDefaultConnTimeout(Integer.parseInt(SerffConstants.getSerffBatchWSCallTimeout()));
				}
				
				if (SerffConstants.getSerffBatchServerIdleTime() != null) {
					setStaticCutOffTime(Integer.parseInt(SerffConstants.getSerffBatchServerIdleTime()));
				}
				boolean skipPlanJobs = false;
				boolean isBreak = false;
				
				while(isJobRunning) {
					LOGGER.info("processSerffPlanMgmtBatch() : Posting new data to SERFF");
					//1. Search in SERFF_PLAN_MGMT_BATCH table and find ready to process job (First in First out), Don't pick deleted rows
					//get WAITING batch here where there is no other batch for SAME issuer already running
					SerffPlanMgmtBatch waitingPlanBatch = null;
					SerffPlanMgmtBatch waitingPrescriptionDrugBatch = null;
					SerffPlanMgmtBatch waitingDrugsJsonBatch = null;
					SerffPlanMgmtBatch waitingDrugsXMLBatch = null;
					boolean foundRec = false;
					if(!skipPlanJobs){
						if(SerffConstants.POSTGRES.equalsIgnoreCase(SerffConstants.getDatabaseType())) {
							waitingPlanBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(SerffPlanMgmtBatch.BATCH_STATUS.WAITING.name(), SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.toString());
						} else {
							waitingPlanBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(SerffPlanMgmtBatch.BATCH_STATUS.WAITING, SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.toString());
						}
					}
					
					if(null != waitingPlanBatch) {
						foundRec = true;
					} else {
						LOGGER.info("processSerffPlanMgmtBatch() : Waiting batch not found for Plan. Checking Prescription Drug batch.");
						if(SerffConstants.POSTGRES.equalsIgnoreCase(SerffConstants.getDatabaseType())) {
							waitingPrescriptionDrugBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(SerffPlanMgmtBatch.BATCH_STATUS.WAITING.name(), SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.toString());
						} else {
							waitingPrescriptionDrugBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(SerffPlanMgmtBatch.BATCH_STATUS.WAITING, SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.toString());
						}
					}

					if(foundRec || null != waitingPrescriptionDrugBatch) {
						foundRec = true;
					} else {
						if(SerffConstants.POSTGRES.equalsIgnoreCase(SerffConstants.getDatabaseType())) {
							waitingDrugsJsonBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(SerffPlanMgmtBatch.BATCH_STATUS.WAITING.name(), SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_JSON.toString());
						} else {
							waitingDrugsJsonBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(SerffPlanMgmtBatch.BATCH_STATUS.WAITING, SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_JSON.toString());
						}
					}
					
					if(foundRec || null != waitingDrugsJsonBatch) {
						foundRec = true;
					} else {
						if(SerffConstants.POSTGRES.equalsIgnoreCase(SerffConstants.getDatabaseType())) {
							waitingDrugsXMLBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(SerffPlanMgmtBatch.BATCH_STATUS.WAITING.name(), SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_XML.toString());
						} else {
							waitingDrugsXMLBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(SerffPlanMgmtBatch.BATCH_STATUS.WAITING, SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_XML.toString());
						}
					}
					
					if(null != waitingPlanBatch) {
						
						Map<STATUS_KEY, Object> loadPlanJobStatus = processPlanLoadJob(waitingPlanBatch);
				
						skipPlanJobs = (boolean)loadPlanJobStatus.get(STATUS_KEY.IS_BREAK);
						message = loadPlanJobStatus.get(STATUS_KEY.STATUS_MESSAGE).toString();
						
					}else if(null != waitingPrescriptionDrugBatch){
						
						Map<STATUS_KEY, Object> loadPrescriptionJobStatus = processPrescriptionDrugLoadJob(waitingPrescriptionDrugBatch);
						message = loadPrescriptionJobStatus.get(STATUS_KEY.STATUS_MESSAGE).toString();
						
					}else if(null != waitingDrugsJsonBatch){
						
						Map<STATUS_KEY, Object> loadDrugsJsonJobStatus = processDrugsJsonLoadJob(waitingDrugsJsonBatch);
						message = loadDrugsJsonJobStatus.get(STATUS_KEY.STATUS_MESSAGE).toString();
						
					}else if(null != waitingDrugsXMLBatch){
						
						Map<STATUS_KEY, Object> loadDrugsXMLJobStatus = processDrugsXMLLoadJob(waitingDrugsXMLBatch);
						message = loadDrugsXMLJobStatus.get(STATUS_KEY.STATUS_MESSAGE).toString();
						
					}else {
						isBreak = true;
					}
					
					if (isBreak) {
						break;
					}
				}
				setJobRunning(false);
			}
			finally {
				setJobRunning(false);
				LOGGER.info(  " : executeSerffBatch() End");
			}
		}
		else {
			LOGGER.info("processSerffPlanMgmtBatch() : Job already running.");
            message = JOB_QUEUE_MESSAGE;
		}
		
		if ((failCount + passCount) == 0) {
			return message;
		}
		else { 
			return "Total Jobs executed = " + (failCount + passCount) + " Passed:" + passCount + " Failed:" + failCount ;
		}
	}
	
	private Map<STATUS_KEY, Object> processDrugsJsonLoadJob(SerffPlanMgmtBatch waitingDrugsJsonBatch) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("processDrugsJsonLoadJob() Start : batch id : " + waitingDrugsJsonBatch.getId());
		}
		
		Map<STATUS_KEY, Object> statusMap = new HashMap<STATUS_KEY, Object>();
		Map<STATUS_MESSAGE_KEY, String> statusMessagesMap = new HashMap<STATUS_MESSAGE_KEY, String>();
		List<Long> existingDrugListIds = null;
		List<Long> newDrugListIds = null;
		String diffReportEcmDocId = null;
		
		int errorCode = 0;
		List<String> errorMessages = new ArrayList<String>();
		SerffPlanMgmt serffPlanMgmtRecord = null;
		Date startTime = new Date(System.currentTimeMillis());
		String folderPath = SerffConstants.getFTPUploadPlanPath() + waitingDrugsJsonBatch.getIssuerId() 
				+ waitingDrugsJsonBatch.getState() + waitingDrugsJsonBatch.getId() + "/";
		EntityManager entityManager = null;
		boolean commitStatus = false;
		try{
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("processDrugsJsonLoadJob() : Start of Loading drugs JSON from location " + folderPath + ". Issuer HIOS ID : " + waitingDrugsJsonBatch.getIssuerId());
			}
			//Update row that job is picked up, set status to IN_PROGRESS
			updateBatchJobStatus(waitingDrugsJsonBatch, SerffPlanMgmtBatch.BATCH_STATUS.IN_PROGRESS, new java.sql.Date((startTime).getTime()), null);
			//Create record in SERFF_PLAN_MGMT table
			serffPlanMgmtRecord = serffUtils.createSerffPlanMgmtRecord(TRANSFER_DRUGS_JSON);
			serffPlanMgmtRecord = serffService.saveSerffPlanMgmt(serffPlanMgmtRecord);
			waitingDrugsJsonBatch.setSerffReqId(serffPlanMgmtRecord);
			
			Issuer issuer = iIssuerRepository.getIssuerByHiosID(waitingDrugsJsonBatch.getIssuerId());
		
			if (null != issuer) {
				Map<String, String> fileMap = getAllJSONFilelist(folderPath);
				StringBuilder attachmentList = new StringBuilder();
				String drugsJson = null;
				//Add templates in ECM.
				if(!CollectionUtils.isEmpty(fileMap)){
					
					for(Entry<String, String> entryTemplate : fileMap.entrySet()){
						if(LOGGER.isDebugEnabled()) {
							LOGGER.debug("processDrugsJsonLoadJob() : Adding JSON file to ECM : " + entryTemplate.getValue());
						}
						addPrescriptionDrugTemplateInECM(serffPlanMgmtRecord, folderPath, entryTemplate.getValue(), entryTemplate.getKey());
						attachmentList.append(entryTemplate.getKey() + ",");
						if(drugsJson == null) {
							byte[] contents = getFileContents(folderPath + entryTemplate.getValue());
							if(null != contents) {
								drugsJson = new String(contents);
								if(LOGGER.isDebugEnabled()) {
									LOGGER.debug("processDrugsJsonLoadJob() : Done reading JSON file ");
								}
							}
						} else {
							if(LOGGER.isInfoEnabled()) {
								LOGGER.info("processDrugsJsonLoadJob() : Skipping to process extra file as one file already found : " + entryTemplate.getValue());
							}
						}
					}
				}
				//Set issuer id and template list in tracking record.
				serffPlanMgmtRecord.setIssuerId(String.valueOf(issuer.getId()));
				serffPlanMgmtRecord.setAttachmentsList(attachmentList.toString());
				serffPlanMgmtRecord = serffService.saveSerffPlanMgmt(serffPlanMgmtRecord);
				if(null != drugsJson) {
					Type type = new TypeToken<List<DrugPlanListJsonDTO>>() {}.getType();
					//Read File and create Object
					List<DrugPlanListJsonDTO> drugsDataDTO = platformGson.fromJson(drugsJson, type);
					if(null != drugsDataDTO){
						LOGGER.debug("processDrugsJsonLoadJob() : Coverted JSON to Object");
						Map<String, DrugTier> existingPlansTierDrugListMap = new HashMap<String, DrugTier>();
						Map<Long, DrugList> drugListIdObjMap = new HashMap<Long, DrugList>();
						Map<String, DrugPlanDTO> drugsKeyObjMap = new HashMap<String, DrugPlanDTO>();
						Map<Long, List<String>> tierIdDrugKeysMap = new HashMap<Long, List<String>>();
						Map<Long, List<Formulary>> drugListIdFormularyMap = new HashMap<Long, List<Formulary>>();
						Map<Long, List<DrugTier>> drugListIdDrugTiersMap = new HashMap<Long, List<DrugTier>>();

						boolean status = getExistingDrugDetails(issuer.getId(), drugsDataDTO.get(0).getPlans().get(0).getYears().get(0), 
								existingPlansTierDrugListMap, drugListIdFormularyMap, drugListIdDrugTiersMap);
						if(LOGGER.isDebugEnabled()) {
							LOGGER.debug("processDrugsJsonLoadJob() : getExistingDrugDetails returned " + status);
						}
						if(status && !existingPlansTierDrugListMap.isEmpty()) {
							readAndPrepairDrugData(waitingDrugsJsonBatch.getIssuerId() + waitingDrugsJsonBatch.getState(),
									Integer.valueOf(waitingDrugsJsonBatch.getDefaultTenant()), drugsDataDTO, existingPlansTierDrugListMap,
									drugsKeyObjMap, tierIdDrugKeysMap, drugListIdObjMap, errorMessages);
							LOGGER.debug("processDrugsJsonLoadJob() : Done processing JSON data to persist in DB");

							if(errorMessages.isEmpty()) {
								entityManager = entityManagerFactory.createEntityManager();
								entityManager.getTransaction().begin();
								existingDrugListIds = new ArrayList<Long>();
								newDrugListIds = new ArrayList<Long>();
								updateDrugsData(drugsKeyObjMap, drugListIdFormularyMap, drugListIdDrugTiersMap, tierIdDrugKeysMap, drugListIdObjMap, 
										errorMessages, entityManager, existingDrugListIds, newDrugListIds);
								//If errorMessages are not empty means process failed.
								if(!CollectionUtils.isEmpty(errorMessages)){
									errorCode = 5; // MSG_VALIDATION_ERROR
								} else {
									commitStatus = true;
									LOGGER.debug("processDrugsJsonLoadJob() : Done merging Drug data to DB");
								}
								completeTransaction("processDrugsJsonLoadJob() ", commitStatus, entityManager);
								if(commitStatus && !CollectionUtils.isEmpty(existingDrugListIds) && !CollectionUtils.isEmpty(newDrugListIds)) {
									HSSFWorkbook workbook = null;
									workbook = formularyService.getFormularyDrugListDiffReport(existingDrugListIds, newDrugListIds);
									if(null != workbook) {
										diffReportEcmDocId = getEcmIdForDiffReport(workbook, serffPlanMgmtRecord, issuer.getHiosIssuerId());
									}
								}
								
							} else {
								errorCode = 5; // MSG_VALIDATION_ERROR
							} 
						} else {
							errorCode = 6; // exiting data missing
						}
					}else{
						errorCode = 3; // MSG_INCOMPATIBLE_TEMPLATES
					}
				} else {
					errorCode = 2; // MSG_ERROR_OCCURRED
				}
			} else {
				errorCode = 1; // MSG_ISSUER_NOT_AVAILABLE
			}
		
			if(errorCode > 0) {
				switch (errorCode) {
					case 1: {
						LOGGER.debug("processDrugsJsonLoadJob() : Drug JSON load Batch has been failed due to Issuer is not available with HIOS ID " + waitingDrugsJsonBatch.getIssuerId());
						//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
						populateStatusMessagesMap(MSG_ISSUER_NOT_AVAILABLE + waitingDrugsJsonBatch.getIssuerId(), MSG_INVALID_TEMPLATE, MSG_INVALID_REQUEST, 
								MSG_INVALID_REQUEST, statusMessagesMap);
						break;
					}
					case 2: {
						LOGGER.error("processDrugsJsonLoadJob() : No file found for Drug JSON load at : " + folderPath);
						//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
						populateStatusMessagesMap(MSG_FILE_NOT_FOUND, MSG_FILE_NOT_FOUND + folderPath, MSG_INVALID_REQUEST, 
								MSG_JSON_PROCESS_FAILED, statusMessagesMap);
						break;
					}
					case 3: {
						LOGGER.error("processDrugsJsonLoadJob() : Incompatible JSON found.");
						//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
						populateStatusMessagesMap(MSG_INCOMPATIBLE_JSON, MSG_INCOMPATIBLE_JSON, MSG_INVALID_REQUEST, 
								MSG_INVALID_REQUEST, statusMessagesMap);
						break;
					}
					case 6: {
						LOGGER.error("processDrugsJsonLoadJob() : Existing drug data not found." );
						//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
						populateStatusMessagesMap(MSG_ERROR_MISSING_DATA, MSG_ERROR_MISSING_DATA, MSG_JSON_PROCESS_FAILED, 
								MSG_JSON_PROCESS_FAILED, statusMessagesMap);
						break;
					}
					default: {
						LOGGER.error("processDrugsJsonLoadJob() : loadDrugsData finished with errors.");
						
						StringBuilder errorString = new StringBuilder();
						for(String errorMsg : errorMessages){
							errorString.append(errorMsg);
							errorString.append(SerffConstants.NEW_LINE);
						}
						//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
						populateStatusMessagesMap(MSG_JSON_PROCESS_FAILED, errorString.toString(), MSG_INVALID_REQUEST, 
								MSG_JSON_PROCESS_FAILED, statusMessagesMap);
					}
				}
			}
		}catch(Exception ex){
			LOGGER.error("processDrugsJsonLoadJob() : Error while processing Drug JSON load : " + ex.getMessage(), ex);
			//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
			populateStatusMessagesMap(MSG_ERROR_OCCURRED_PROCESSING_DRUG_JSON, MSG_ERROR_OCCURRED_PROCESSING_DRUG_JSON, ex.getMessage(), 
					MSG_JSON_PROCESS_FAILED, statusMessagesMap);
		}finally{
			if(commitStatus){
				LOGGER.info("processDrugsJsonLoadJob() End.");
				//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
				populateStatusMessagesMap(MSG_PROCESS_COMPLETED_SUCCESS, MSG_PROCESS_COMPLETED_SUCCESS, MSG_PROCESS_COMPLETED_SUCCESS, 
						MSG_PROCESS_COMPLETED_SUCCESS, statusMessagesMap);
				updateBatchJobStatus(waitingDrugsJsonBatch, SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
			}else{
				updateBatchJobStatus(waitingDrugsJsonBatch, SerffPlanMgmtBatch.BATCH_STATUS.FAILED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
			}
			
			if (null != serffPlanMgmtRecord) {
				//Save tracking record.
				if(null != diffReportEcmDocId) {
					serffPlanMgmtRecord.setPmResponseXml(diffReportEcmDocId);
				}
   				persistTrackingRecord(serffPlanMgmtRecord, null, commitStatus, statusMessagesMap);
			}
		}
	
		statusMap.put(STATUS_KEY.STATUS_MESSAGE, statusMessagesMap.get(STATUS_MESSAGE_KEY.UI_MESSAGE));	
		return statusMap;
	}

	/**
	 * Method is used to validate Issuer Plan Number with Selected HIOS Issuer ID & State code.
	 */
	private boolean validateIssuerPlanNumber(String issuerPlanNumber, String selectedHiosIdWithState) {

		LOGGER.debug("validateIssuerPlanNumber() Start");
		boolean hasValidData = true;
		try {

			final int LEN_HIOS_PLAN_ID = 14;
			final int LEN_HIOS_WITH_STATE = 7;
			final String PATTERN_PLAN_ID = "(^[0-9]{5})([A-Za-z]{2})([0-9]{7})$";

			if (StringUtils.isBlank(issuerPlanNumber)) {
				hasValidData = false;
			}
			else if (LEN_HIOS_PLAN_ID != StringUtils.length(issuerPlanNumber)) {
				hasValidData = false;
			}
			else if (!issuerPlanNumber.matches(PATTERN_PLAN_ID)) {
				hasValidData = false;
			}
			else if (!issuerPlanNumber.substring(0, LEN_HIOS_WITH_STATE).equals(selectedHiosIdWithState)) {
				hasValidData = false;
			}
		}
		finally {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("validateIssuerPlanNumber() End with Valid Data: " + hasValidData);
			}
		}
		return hasValidData;
	}

	private void updateDrugsData(Map<String, DrugPlanDTO> drugsKeyObjMap,
			Map<Long, List<Formulary>> drugListIdFormularyMap, Map<Long, List<DrugTier>> drugListIdDrugTiersMap,
			Map<Long, List<String>> tierIdDrugKeysMap, Map<Long, DrugList> drugListIdObjMap,
			List<String> errorMessages, EntityManager entityManager, List<Long> existingDrugListIds, List<Long> newDrugListIds) {
		if(null != drugListIdObjMap && null != drugListIdFormularyMap) {
			//Find All Drug List that needs to be refreshed using drugListIdObjMap
			for (DrugList existingDrugList : drugListIdObjMap.values()) {
				DrugList drugList = new DrugList();
				drugList.setIssuer(existingDrugList.getIssuer());
				drugList.setFormularyDrugListId(existingDrugList.getFormularyDrugListId());
				drugList.setIsDeleted(SerffConstants.NO_ABBR);
				//Create new Drug List with New Drug Tiers, and Drug using tierIdDrugKeysMap & drugsKeyObjMap
				drugList.setDrugsTiers(new ArrayList<DrugTier>());
				if(!createDrugTiers(drugListIdDrugTiersMap.get(existingDrugList.getId()), drugList, drugsKeyObjMap, tierIdDrugKeysMap, errorMessages)) {
					errorMessages.add("Failed to update Drug Tiers data. ");
					break;
				}
				//Update Formulary with new drugList 
				List<Formulary> existingFormulary = drugListIdFormularyMap.get(existingDrugList.getId());
				drugList.setFormulary(existingFormulary);
				if(null != existingFormulary) {
					for (Formulary formulary : existingFormulary) {
						formulary.setDrugList(drugList);
					}
				}
				//Mark existing Drug List as deleted
				drugList = entityManager.merge(drugList);
				existingDrugList.setIsDeleted(SerffConstants.YES_ABBR);
				entityManager.merge(existingDrugList);
				existingDrugListIds.add(existingDrugList.getId());
				newDrugListIds.add(drugList.getId());
			}
		} else {
			errorMessages.add("Existing Formulary data is missing. ");
		}
	}

	private boolean createDrugTiers(List<DrugTier> existingDrugTierList, DrugList drugList, 
			Map<String, DrugPlanDTO> drugsKeyObjMap, Map<Long, List<String>> tierIdDrugKeysMap, List<String> errorMessages) {
		boolean status = false;
		if(null != existingDrugTierList) {
			status = true;
			for (DrugTier existingDrugTier : existingDrugTierList) {
				DrugTier drugTier = new DrugTier(); 
				drugTier.setDrugTierLevel(existingDrugTier.getDrugTierLevel());
				drugTier.setDrugTierType1(existingDrugTier.getDrugTierType1());
				drugTier.setDrugTierType2(existingDrugTier.getDrugTierType2());
				drugTier.setDrugList(drugList);
				drugList.getDrugsTiers().add(drugTier);
				drugTier.setDrugs(new ArrayList<Drug>());
				if(!createDrugs(drugTier, drugsKeyObjMap, tierIdDrugKeysMap.get(existingDrugTier.getId()), errorMessages)) {
					status = false;
					break;
				}
			}
		} else {
			errorMessages.add("Existing Drug Tier data is missing. ");
		}
		return status;
	}

	private boolean createDrugs(DrugTier drugTier, Map<String, DrugPlanDTO> drugsKeyObjMap, List<String> drugKeysList, List<String> errorMessages) {
		boolean status = true;
		if(null != drugKeysList) {
			Set<String> drugKeySet = new HashSet<String>(drugKeysList);
			for (String key : drugKeySet) {
				DrugPlanDTO drugDTO = drugsKeyObjMap.get(key);
				if(null != drugDTO) {
					Drug drug = new Drug(); 
					drug.setRxcui(drugDTO.getRxnormId());
					drug.setAuthRequired(drugDTO.getPriorAuthorization() ? SerffConstants.YES_ABBR : SerffConstants.NO_ABBR);
					drug.setStepTherapyRequired(drugDTO.getStepTherapy() ? SerffConstants.YES_ABBR : SerffConstants.NO_ABBR);
					drug.setDrugTier(drugTier);
					drugTier.getDrugs().add(drug);
				} else {
					LOGGER.warn("Could not find drug details for: " + key);
					errorMessages.add("No Drug data found for : " + key + ". ");
					status = false;
					break;
				}
			}
		} else {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("No Drug data to add for tier: " + drugTier.getDrugTierType1() + " Tier Level:" + drugTier.getDrugTierLevel() + ". ");
			}
		}
		return status;
	}
	
	private void readAndPrepairDrugData(String selectedHiosIdWithState, int selectedYear,
			List<DrugPlanListJsonDTO> drugsDataDTO, Map<String, DrugTier> existingPlansTierDrugListMap,
			Map<String, DrugPlanDTO> drugsKeyObjMap, Map<Long, List<String>> tierIdDrugKeysMap,
			Map<Long, DrugList> drugListIdObjMap, List<String> errorMessages) {

		if (null != drugsDataDTO) {

			for (DrugPlanListJsonDTO drugData : drugsDataDTO) {
				List<DrugPlanDTO> plans = drugData.getPlans();
				String rxcui = drugData.getRxnormId();

				if (null != plans && StringUtils.isNotBlank(rxcui)) {

					for (DrugPlanDTO plan : plans) {

						// Validate Issuer Plan Number
						String drugsPlanID = plan.getPlanId();
						if (!validateIssuerPlanNumber(drugsPlanID, selectedHiosIdWithState)) {
							LOGGER.error("Plan ID["+ drugsPlanID +"] does not match with option selected Plan ID & State: " + selectedHiosIdWithState + " or its format is not correct. ");
							errorMessages.add("Plan ID["+ drugsPlanID +"] does not match with option selected Plan ID & State: " + selectedHiosIdWithState + " or its format is not correct. ");
						}
						// Validate Plan Year
						int drugsYear = plan.getYears().get(0);
						if (selectedYear != drugsYear) {
							LOGGER.error("Plan Year["+ drugsYear +"] does not match with option selected Year: " + selectedYear);
							errorMessages.add("Plan Year["+ drugsYear +"] does not match with option selected Year: " + selectedYear + ". ");
						}
						//add Drug_key, Drug Obj to a map
						String key = rxcui + "-" + plan.getKey();
						plan.setRxnormId(rxcui);
						drugsKeyObjMap.put(key, plan);

						//Add Tier_id, Drug_key to map
						DrugTier dt = existingPlansTierDrugListMap.get(drugsPlanID + ":" + plan.getDrugTier());
						if(dt != null) {
							List<String> drugKeyList = tierIdDrugKeysMap.get(dt.getId());
							if(null == drugKeyList) {
								drugKeyList = new ArrayList<String>();
								tierIdDrugKeysMap.put(dt.getId(), drugKeyList);
							}
							drugKeyList.add(key);
							//Add Drug_list_ID, Drug_list obj to map of to be processed list 
							if(null == drugListIdObjMap.get(dt.getDrugList().getId())) {
								drugListIdObjMap.put(dt.getDrugList().getId(), dt.getDrugList());
							}
						} else {
							LOGGER.error("Skipping record, Did not find Drug Tier for " + drugsPlanID + ":" + plan.getDrugTier());
							errorMessages.add("Skipping record, Did not find Drug Tier for " + drugsPlanID + ":" + plan.getDrugTier() + ". ");
						}
					}
				} else {
					LOGGER.error("Did not find required plan and rxcui data in JSON ");
					errorMessages.add("Did not find required plan and rxcui data in JSON ");
				}
			}
		}
	}

	private boolean getExistingDrugDetails(int issuerId, int applicableYear, Map<String, DrugTier> existingPlansTierDrugListMap, 
			Map<Long, List<Formulary>> drugListIdFormularyMap, Map<Long, List<DrugTier>> drugListIdDrugTiersMap) {
		boolean status = true;
		List<Object[]> existingPlanFormularyList = iPlanRepository.getPlanFormularyDetails(issuerId, applicableYear);
		if(!CollectionUtils.isEmpty(existingPlanFormularyList)) {
			List<Formulary> existingFormularyList = iSerffFormularyRepository.findByIssuer_IdAndApplicableYear(issuerId, applicableYear);
			if(!CollectionUtils.isEmpty(existingFormularyList)) {
				Map<String, Long> formularyDrugListIdMap = new HashMap<String, Long>(); 
				Long drugListId;
				for (Formulary formulary : existingFormularyList) {
					drugListId = formulary.getDrugList().getId();
					formularyDrugListIdMap.put(String.valueOf(formulary.getId()), drugListId);
					List<Formulary> formularyList = drugListIdFormularyMap.get(drugListId);
					if(null == formularyList) {
						formularyList = new ArrayList<Formulary>();
						drugListIdFormularyMap.put(drugListId, formularyList);
					}
					formularyList.add(formulary);
				} 

				List<DrugTier> existingDrugTierList = null;
				existingDrugTierList = iSerffDrugTierRepository.findByDrugList_IdIn(new ArrayList<Long> (formularyDrugListIdMap.values()));
				List<DrugTier> tempDrugTierList = null;

				if(!CollectionUtils.isEmpty(existingDrugTierList)) {
					for (DrugTier dt : existingDrugTierList) {
						drugListId = dt.getDrugList().getId();
						tempDrugTierList = drugListIdDrugTiersMap.get(drugListId);
						if(null == tempDrugTierList) {
							tempDrugTierList = new ArrayList<DrugTier>();
							drugListIdDrugTiersMap.put(drugListId, tempDrugTierList);
						}
						tempDrugTierList.add(dt);
					}
				} else {
					LOGGER.error("Did not find existing Drug Tier List for Issuer " + issuerId + " Year:" + applicableYear);
					status = false;
				}

				for (Object[] formularyDetail : existingPlanFormularyList) {
					String planId = (String) formularyDetail[0];
					String formularyId = (String) formularyDetail[1];
					drugListId = formularyDrugListIdMap.get(formularyId);
					if(null != drugListId) {
						tempDrugTierList = drugListIdDrugTiersMap.get(drugListId);
						for (DrugTier dt : tempDrugTierList) {
							existingPlansTierDrugListMap.put(planId + ":" + getDrugTierName(dt.getDrugTierType1(), dt.getDrugTierType2()), dt);
						}
					} else {
						LOGGER.error("Did not find existing Drug Tier List for Issuer " + issuerId + " Year:" + applicableYear + " drugListId:" + drugListId);
						status = false;
					}
				}
			} else {
				LOGGER.error("Did not find existing Formulary data for Issuer " + issuerId + " Year:" + applicableYear);
				status = false;
			}
		} else {
			LOGGER.error("Did not find existing Plans Formulary List for Issuer " + issuerId + " Year:" + applicableYear);
			status = false;
		}
		return status;
	}

	
	private String getDrugTierName(String tireType1, String tireType2) {
		String tierName = null;
		switch(tireType1.trim()) {
		case "Generic":
			tierName = "GENERIC";
			break;
		case "Non-Preferred Generic":
			tierName = "NON-PREFERRED-GENERIC";
			break;
		case "Preferred Generic":
			tierName = "PREFERRED-GENERIC";
			break;
		case "Non-Preferred Brand":
			if(null == tireType2) {
				tierName = "NON-PREFERRED-BRAND";
			} else if("Specialty Drugs".equals(tireType2.trim())) {
				tierName = "NON-PREFERRED-SPECIALTY-DRUGS";
			} else {
				tierName = "NON-PREFERRED-BRAND";
			}
			break;
		case "Preferred Brand":
			tierName = "PREFERRED-BRAND";
			break;
		case "Specialty Drugs":
			tierName = "PREFERRED-SPECIALTY-DRUGS";
			break;
		case "Zero Cost Share Preventive Drugs":
			//ZERO-COST-SHARE-PREVENTIVE
			tierName = "PREVENTIVE-DRUGS";
			break;
		case "Medical Service Drugs":
			tierName = "MEDICAL-SERVICE";
			break;
		case "Brand":
			tierName = "BRAND";
			break;
		}
		
		return tierName;
	}
	
	/**
	 * 
	 * This method executes Plan loading task into the database by retrieving waiting batch.
	 * 
	 * @param waitingBatch Represents a particular batch which is created while uploading Plan. 
	 * @param server Server name where webservice is running to accept Plan data in form of SOAP XML.
	 * @return Result of the overall Process occurred while processing and saving Plan data.
	 */
	private boolean executePlanLoadJob(SerffPlanMgmtBatch waitingBatch, String server) {
		Date startTime = new Date(System.currentTimeMillis());
		String folderPath = null;
		if(useFTPForTemplates) {
			folderPath = SerffConstants.getFTPUploadPlanPath() + waitingBatch.getIssuerId() 
					+ waitingBatch.getState() + waitingBatch.getId() + "/";
		} else {
			folderPath = SerffConstants.DISK_SERFF_FILES_PATH + SerffConstants.FTP_UPLOAD_PLAN_PATH + waitingBatch.getIssuerId() 
			+ waitingBatch.getState() + waitingBatch.getId() + "/";
		}
		LOGGER.info("Executing executePlanLoadJob() : Server : " + server + " : batch id : " + waitingBatch.getId());
		LOGGER.debug("executePlanLoadJob() : Start of Loading plans from location " + folderPath + " and server " + server + " Issuer HIOS ID : " + waitingBatch.getIssuerId());
		//Update row that job is picked up, set status to IN_QUEUE
		SerffPlanMgmtBatch newWaitingBatch = updateBatchJobStatus(waitingBatch, SerffPlanMgmtBatch.BATCH_STATUS.IN_QUEUE, new java.sql.Date((startTime).getTime()), null);

		Issuer issuer = iIssuerRepository.getIssuerByHiosID(newWaitingBatch.getIssuerId());
		boolean status = true;
		if (null != issuer) {
			Map<String, String> fileMap = getAllTemplatesFilelist(folderPath, null);
			Map<String, byte[]> uploadedTemplatesMap = null;
			LOGGER.debug("executePlanLoadJob() : Found " + fileMap.size() + " templates for loading plans from location " + folderPath );
			try {
				uploadedTemplatesMap = getAllTemplatesForPlans(issuer, folderPath, fileMap, newWaitingBatch.getState() );
			} catch (GIException ex) {
				LOGGER.error("executePlanLoadJob() : Error while executing getAllTemplatesForPlans() : " + ex.getMessage(), ex);
				updateBatchJobStatus(newWaitingBatch, SerffPlanMgmtBatch.BATCH_STATUS.FAILED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
				LOGGER.info("executePlanLoadJob() : Error while executing. Returning false.");
				status = false;
			}
			//Form SOAP UI project
			StringEntity soapRequest = null;
			if(status) {
				LOGGER.debug("executePlanLoadJob() : Creating payload for webservice call for Job ID " + newWaitingBatch.getId() );
				try {
					soapRequest = createSOAPPayload(issuer, fileMap, uploadedTemplatesMap, newWaitingBatch); 
				} catch (Exception ex) {
					updateBatchJobStatus(newWaitingBatch, SerffPlanMgmtBatch.BATCH_STATUS.FAILED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
					LOGGER.error("executePlanLoadJob() : Error while creating SOAP Request : " + ex.getMessage(), ex );
					status = false;
				}
			}
			//post to available server
			LOGGER.debug("executePlanLoadJob() : Making webservice call for Job ID " + newWaitingBatch.getId() );
			if(status) {
				try {
					if(!postToSerffService(server, soapRequest)) {
						LOGGER.debug("executePlanLoadJob() : postToSerffService failed for " + newWaitingBatch.getId() );
						status = false;
						//The Post may fail due to connection error / server error 500, so the Job Status will remain IN_QUEUE
						//updateBatchJobStatus(newWaitingBatch, SerffPlanMgmtBatch.BATCH_STATUS.WAITING, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
					}
				} catch (Exception e) {
					updateBatchJobStatus(newWaitingBatch, SerffPlanMgmtBatch.BATCH_STATUS.FAILED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
					LOGGER.error(  " : Error occurred while submiting SOAPRequest to server : " + e.getMessage(), e);				
					status = false;
				}
			}
		}
		else {
			LOGGER.debug("executePlanLoadJob() : SERFF Batch has been failed due to Issuer is not available with HIOS ID " + newWaitingBatch.getIssuerId());
			updateBatchJobStatus(newWaitingBatch, SerffPlanMgmtBatch.BATCH_STATUS.FAILED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
			status = false;
		}
		return status;
	}
	
	/**
	 * @param batch
	 * @param status
	 * @param startTime
	 * @param endTime
	 * @return
	 * This method updates the SerffPlanMgmtBatch record with the passed Status and start / End time
	 */
	private SerffPlanMgmtBatch updateBatchJobStatus(SerffPlanMgmtBatch batch, SerffPlanMgmtBatch.BATCH_STATUS status, Date startTime, Date endTime) {
		SerffPlanMgmtBatch savedBatch = null;
		LOGGER.debug("Plan loading Batch record status to be updated to : " + status.name());
		if(iSerffPlanMgmtBatch != null) {
			SerffPlanMgmtBatch.BATCH_STATUS curStatus = batch.getBatchStatus();
			if(curStatus != SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Updating Plan loading Batch record status from: " + curStatus.name() + " to " + status.name());
				}
				batch.setBatchStatus(status);
				if(null != startTime) {
					batch.setBatchStartTime(startTime);
				}
				if(null != endTime) {
					batch.setBatchEndTime(endTime);
				}
				savedBatch = iSerffPlanMgmtBatch.save(batch);
			} else {
				savedBatch = batch;
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Skipped to Update Plan loading Batch record status from: " + curStatus.name() + " to " + status.name());
				}
			}
		}
		return savedBatch;
	}
	
	/**
	 * This method extracts templates that are present in FTP directory.
	 * 
	 * @param folderPath
	 *        FTP directory path to seek template files.
	 *        
	 * @return List of template files present in FTP directory.
	 */
	private Map<String, String> getAllTemplatesFilelist(String folderPath, String defaultType) {
		
		LOGGER.info("Executing getAllTemplatesFilelist() : folderPath : " + folderPath);
		
		Map<String, String> filesMap = null;
		
		if (null != folderPath) {
			
			List<String> fileList = null;
			if(useFTPForTemplates) {
				if(null != ftpClient) {
					fileList = ftpClient.getFileNames(folderPath);
				}
			} else {
				fileList = getFileListFromDiskPath(folderPath);
			}
			filesMap = new HashMap<String, String>();			
			String fileNameLowerCase = null;
			
			for (String fileName : fileList) {
				
				if(StringUtils.isNotBlank(fileName)) {
					
					fileNameLowerCase = fileName.toLowerCase();
					
					if(null != defaultType) {
						filesMap.put(defaultType, fileName);
						if(LOGGER.isInfoEnabled() ) {
							LOGGER.info("Adding unknown file for plan load : " + folderPath + fileName + " Type:" + defaultType);
						}
					} else if (fileNameLowerCase.endsWith(SerffConstants.NETWORK_ID_FILENAME_SUFFIX)) {
						filesMap.put(SerffConstants.DATA_NETWORK_ID, fileName);
					}
					else if (fileNameLowerCase.endsWith(SerffConstants.BENEFITS_FILENAME_SUFFIX)) {
						filesMap.put(SerffConstants.DATA_BENEFITS, fileName);
					}
					else if (fileNameLowerCase.endsWith(SerffConstants.RATING_TABLE_FILENAME_SUFFIX)) {
						filesMap.put(SerffConstants.DATA_RATING_TABLE, fileName);
					}
					else if (fileNameLowerCase.endsWith(SerffConstants.SERVICE_AREA_FILENAME_SUFFIX)) {
						filesMap.put(SerffConstants.DATA_SERVICE_AREA, fileName);
					}
					else if (fileNameLowerCase.endsWith(SerffConstants.PRESCRIPTION_DRUG_FILENAME_SUFFIX)) {
						filesMap.put(SerffConstants.DATA_PRESCRIPTION_DRUG, fileName);
					}
					else if (fileNameLowerCase.endsWith(SerffConstants.BUSINESS_RULE_FILENAME_SUFFIX)) {
						filesMap.put(SerffConstants.DATA_RATING_RULES, fileName);
					} 
					else if (fileNameLowerCase.endsWith(SerffConstants.UNIFIED_RATE_FILENAME_SUFFIX)) {
						filesMap.put(SerffConstants.DATA_UNIFIED_RATE, fileName);
					}
					else if (fileNameLowerCase.endsWith(SerffConstants.URAC_FILENAME_SUFFIX)) {
						filesMap.put(SerffConstants.DATA_URAC_DATA, fileName);
					}
					else if (fileNameLowerCase.endsWith(SerffConstants.NCQA_FILENAME_SUFFIX)) {
						filesMap.put(SerffConstants.DATA_NCQA_DATA, fileName);
					}
					else if (fileNameLowerCase.endsWith(SerffConstants.POSTFIX_SBC.toLowerCase())) {
						filesMap.put(fileName, fileName);
					}
					else if (fileNameLowerCase.endsWith(SerffConstants.POSTFIX_EOC.toLowerCase())) {
						filesMap.put(fileName, fileName);
					}
					else if (fileNameLowerCase.endsWith(SerffConstants.POSTFIX_BROCHURE.toLowerCase())) {
						filesMap.put(fileName, fileName);
					} else {
						if(LOGGER.isInfoEnabled() ) {
							LOGGER.info("Ignoring unknown file for plan load : " + folderPath + fileName);
						}
					}
				}
			}
		}
		
		LOGGER.debug("getAllTemplatesFilelist() : Created filesMap : " + filesMap);
		if(null!=ftpClient){
			ftpClient.close();
		}
		return filesMap;
	}

	
	/**
	 * This method extracts files that are present in FTP directory.
	 * 
	 * @param folderPath
	 *        FTP directory path to seek template files.
	 *        
	 * @return List of files present in FTP directory.
	 */
	private Map<String, String> getAllJSONFilelist(String folderPath) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Executing getAllJSONFilelist() : folderPath : " + folderPath);
		}
		
		Map<String, String> filesMap = null;
		
		if (null != folderPath) {
			
			List<String> fileList = null;
			if(useFTPForTemplates) {
				if(null != ftpClient) {
					fileList = ftpClient.getFileNames(folderPath);
				}
			} else {
				fileList = getFileListFromDiskPath(folderPath);
			}
			filesMap = new HashMap<String, String>();			
			String fileNameLowerCase = null;
			
			for (String fileName : fileList) {
				
				if(StringUtils.isNotBlank(fileName)) {
					
					fileNameLowerCase = fileName.toLowerCase();
					
					if (fileNameLowerCase.endsWith(".json")) {
						filesMap.put(fileNameLowerCase, fileName);
					}
					else {
						LOGGER.warn("getAllJSONFilelist: Ignoring unknown non-json file for : " + folderPath + fileName);
					}
				}
			}
		}
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("getAllJSONFilelist() : Created filesMap of size : " + filesMap.size());
		}
		if(null!=ftpClient){
			ftpClient.close();
		}
		return filesMap;
	}
	
	private List<String> getFileListFromDiskPath(String folderPath) {
		List<String> fileList = null;
		File folder = new File(folderPath);
		if(null != folder) {
			fileList = new ArrayList<String>();
			File[] listOfFiles = folder.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					fileList.add(listOfFiles[i].getName());
				} 
			}
		}
		return fileList;
	}

	/**
	 * This method returns data templates values in byte stream from existing template list present on FTP server.
	 * 
	 * @param issuer
	 *        Plan Issuer instance.
	 *        
	 * @param folderPath
	 *        FTP path where templates are stored for the Plan.
	 * 
	 * @param filesMap
	 *        Map of template names.
	 * 
	 * @param issuerState
	 *        State name to which Issuer belongs.
	 * 
	 * @return Map with data templates in the form of byte stream. 
	 * @throws GIException 
	 */
	private Map<String, byte[]> getAllTemplatesForPlans(Issuer issuer, String folderPath, Map<String, String> filesMap, String issuerState) throws GIException{
		
		LOGGER.info("Executing getAllTemplatesForPlans() : folderPath : " + folderPath + " : issuer id :" + issuer.getId());
		
		Map<String, byte[]> templateMap = new HashMap<String, byte[]>();
		if(null != filesMap) {
			
				for (Entry<String, String> fileEntry : filesMap.entrySet()) {
					 try {
						byte[] contents = getFileContents(folderPath + fileEntry.getValue());
						templateMap.put(fileEntry.getKey(), contents);
					} catch (Exception e) {
						LOGGER.error("getAllTemplatesForPlans() : Error while reading file for plan load from FTP " + folderPath + fileEntry.getValue(), e);
						if(null!=ftpClient){
							ftpClient.close();
						}
						throw new GIException(Integer.parseInt(SerffConstants.IO_ERROR_CODE), SerffConstants.IO_ERROR_MESSAGE_FTP_ERROR_IN_READING_DATA_MESSAGE, "");
					}
				}
				//Admin template is made optional since 2017, so it is not required now.
				//templateMap.put(SerffConstants.DATA_ADMIN_DATA, getIssuerTemplate(issuer, issuerState).getBytes());
				//filesMap.put(SerffConstants.DATA_ADMIN_DATA, "admin.xml");			
		}		
		LOGGER.debug("getAllTemplatesForPlans() : Returning templateMap : " + templateMap.size());
		if(null!=ftpClient){
			ftpClient.close();
		}
		return templateMap;
	}

	private byte[] getFileContents(String filePath) throws IOException {
		byte[] contents = null;
		if(StringUtils.isNoneBlank(filePath)) {
	 		if(useFTPForTemplates) {
	 			if(null != ftpClient) {
	 				contents = ftpClient.getFileContent(filePath);
	 			}
			} else {
				Path path = Paths.get(filePath);
				contents = Files.readAllBytes(path);
			}
		}
		return contents;
	}

	/**
	 * This method returns TransferPlanResponse which is the result of submitting soapRequest to the webservice.
	 * 
	 * 
	 * @param server
	 *        Server name where webservice is running.
	 *        
	 * @param soapRequest
	 *        Request that is to be submitted to webservice.
	 * 
	 * @return boolean 
	 * 
	 * @throws Exception
	 */
	private boolean postToSerffService(String server, StringEntity soapRequest) throws GIException {
		
		LOGGER.info("Executing postToSerffService() : server : " + server);
		final HttpParams httpParams = new BasicHttpParams();
	    HttpConnectionParams.setConnectionTimeout(httpParams, DEFAULT_CONN_TIMEOUT);
	    HttpConnectionParams.setSoTimeout(httpParams, DEFAULT_CONN_TIMEOUT);
		HttpClient httpclient = new DefaultHttpClient(httpParams);
		
		String postToURL = "";
		if(server != null) {
			String tempSvr = server.toLowerCase().trim();
			if(!tempSvr.startsWith(SerffConstants.HTTP_STRING) && !tempSvr.startsWith(SerffConstants.HTTPS_STRING)){
				postToURL = SerffConstants.HTTP_STRING;
			} else if(tempSvr.startsWith(SerffConstants.HTTPS_STRING)) {
				try {
					SSLContext ctx = SSLContext.getInstance("SSL");
			        //Implementation of a trust manager for X509 certificates
					X509TrustManager tm = new X509TrustManager() {
						private X509Certificate[] xcs;
						
						public void checkClientTrusted(X509Certificate[] xcs,
								String string) throws CertificateException {
							this.xcs = xcs;
							LOGGER.debug("X509Certificate[] is null: " + (null == xcs) + "string is null: " + (null == string));
						}
						
						public void checkServerTrusted(X509Certificate[] xcs,
								String string) throws CertificateException {
							this.xcs = xcs;
							LOGGER.debug("X509Certificate[] is null: " + (null == xcs) + "string is null: " + (null == string));
						}
						
						public X509Certificate[] getAcceptedIssuers() {
							return xcs;
						}
					};		
					ctx.init(null, new TrustManager[] { tm }, null);
					SSLSocketFactory ssf = new SSLSocketFactory(ctx, SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	
					ClientConnectionManager ccm = httpclient.getConnectionManager();
		                        //register https protocol in httpclient's scheme registry
					SchemeRegistry sr = ccm.getSchemeRegistry();
					String serverHttpsPort = SerffConstants.getServiceServerHTTPSPort();
					int port = SerffConstants.DEFAULT_HTTPS_PORT;
					try {
						port = Integer.parseInt(serverHttpsPort);
					} catch (NumberFormatException e) {
						LOGGER.warn("postToSerffService() : using default HTTPS port: " + port);
					}
					sr.register(new Scheme("https", port, ssf));
				} catch (NoSuchAlgorithmException e) {
					LOGGER.error("postToSerffService() : Error setting up SSLContext : " + e.getMessage(), e);
				} catch (Exception ex) {
					LOGGER.error("postToSerffService() : Error setting up SSL Context : " + ex.getMessage(), ex);
				}		
			}
		}
		postToURL = postToURL + server + serviceURl; 
		LOGGER.debug("postToSerffService() : serviceURL : " + postToURL);
		HttpPost httppost = new HttpPost(postToURL);
		httppost.setEntity(soapRequest);
		//httppost.setConfig(requestConfig);
		httppost.addHeader("Accept", "text/xml");
		HttpResponse response;
		
		try {
			response = httpclient.execute(httppost);
			
			if(null != response) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("postToSerffService() : response : " + SecurityUtil.sanitizeForLogging(response.toString()));
					//LOGGER.info("ResP: " + response.getEntity().getContentLength() + " ----- " + IOUtils.toString(response.getEntity().getContent()));
				}
				if(response.getStatusLine().getStatusCode() != HTTP_SUCCESS_CODE) {
					LOGGER.info("postToSerffService() : returned error response " + response.getStatusLine().getStatusCode() );
					return false;
				} else {
					String errorCode = getErrorCodeFromResponse(response);
					//Since HTTP STATUS is Success, return true if errorCode is null or something other than IO_ERROR_CODE, 
					//the service might have already set the status in DB
					if(SerffConstants.IO_ERROR_CODE.equals(errorCode) ) {
						LOGGER.info("objResponse contains error code: " + errorCode);
						return false;
					}
				}
			} else {
				LOGGER.debug("postToSerffService() : returned null response " );
			}
		}
		catch (ClientProtocolException e) {
			throw new GIException(e);
		}
		catch (java.net.SocketTimeoutException e) {
			//We don't want to wait for Job to complete, so after the timeout proceed to next Job without waiting.
			LOGGER.info("Plan load request timed out after set timeout of " + DEFAULT_CONN_TIMEOUT/ONE_SECOND + " seconds, the server might be still processing the request. ", e);
		}
		catch (IOException e) {
			throw new GIException(e);
		}
		return true;
	}

	
	private String getErrorCodeFromResponse(HttpResponse response) throws GIException {
		HttpEntity entity = response.getEntity();
		String errorCode = null;
		TransferPlanResponse serffResponse = null;
		if (null != entity) {
			try {
				XMLInputFactory xif = XMLInputFactory.newFactory();
				//XMLInputFactory xif = XMLInputFactory.newInstance();
				XMLStreamReader xsr = xif.createXMLStreamReader(entity.getContent());
				
				if (null != xsr) {
			        xsr.nextTag();
			        
			        while(!xsr.getLocalName().equals(TransferPlanResponse.XML_ROOT_ELEMENT)) {
			            xsr.nextTag();
			        }
					
					Object objResponse = serffUtils.streamReaderToObject(xsr, TransferPlanResponse.class);
					xsr.close();

					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("postToSerffService() : objResponse : " + SecurityUtil.sanitizeForLogging(objResponse.toString()));
					}
					if (objResponse instanceof TransferPlanResponse) {
						LOGGER.debug("objResponse instanceof TransferPlanResponse : true ");
						serffResponse = (TransferPlanResponse) objResponse;
						if (null != serffResponse.getValidationErrors()
								&& null != serffResponse.getValidationErrors().getErrors()
								&& !serffResponse.getValidationErrors().getErrors().isEmpty()) {
							errorCode = serffResponse.getValidationErrors().getErrors().get(0).getCode();
						}
					} else {
						LOGGER.debug("objResponse instanceof TransferPlanResponse : false ");
					}
				}
			} catch (JAXBException e) {
				LOGGER.error("Error reading Exchange-Hapi response XML");
				//No need to throw exception if unable to read response, the service already sets the status
				//throw new GIException(e);
			}
			catch (XMLStreamException e) {
				LOGGER.error("Error reading Exchange-Hapi response Stream");
				//No need to throw exception if unable to read response, the service already sets the status
				//throw new GIException(e);
			} catch (IllegalStateException e) {
				LOGGER.error("Error reading Exchange-Hapi response: " + e.getMessage());
				//No need to throw exception if unable to read response, the service already sets the status
				//throw new GIException(e);
			} catch (IOException e) {
				LOGGER.error("I/O Error reading Exchange-Hapi response: " + e.getMessage());
				//No need to throw exception if unable to read response, the service already sets the status
				//throw new GIException(e);
			}
		}
		return errorCode;
	}
	
	/**
	 * This method returns list of servers available for processing.
	 * 
	 * @param servers
	 *        List of servers in String format.
	 *        
	 * @return idleServer name which is available. 
	 */
	private String getIdleServerFromList(String[] servers) {
		LOGGER.info("Executing getIdleServerFromList() Start");
		String idleServer = null;
		
		if (null != servers) {
			LOGGER.info("Server List : " + Arrays.asList(servers));
			boolean isBreak = false;
			
			for (String server : servers) {
				
				String server1 = server.substring(server.indexOf(SerffConstants.AT_THE_RATE) + 1);
				SerffPlanMgmt serffPlanRequest = iSerffPlanMgmt.getRecentPlanRequestByProcessIP(server1);
				
				if (null != serffPlanRequest) {
					
					REQUEST_STATUS status = serffPlanRequest.getRequestStatus();
					REQUEST_STATE state = serffPlanRequest.getRequestState();
					LOGGER.debug("getIdleServerFromList() : status : " + status);
					LOGGER.debug("getIdleServerFromList() : state : " + state);
					
					if (state == REQUEST_STATE.E && status != REQUEST_STATUS.P) {
						//There is most recent record for job completed by this server so it is idle
						idleServer = server;
						isBreak = true;
					}
					else {
						//There is most recent record for job not completed by this server 
						//check the time when this record was created, if more than 15 mins old, assume this server as idle
						Date startedAt = serffPlanRequest.getCreatedOn();
						Date cutOffTime = new Date(System.currentTimeMillis() - (CUTOFF_TIME));
						if(startedAt.before(cutOffTime)) {
							idleServer = server;
							isBreak = true;
						}
					}
				}
				else {
					// There are no record for this server so it is idle
					idleServer = server;
					isBreak = true;
				}
				
				if (isBreak) {
					break;
				}
			}
		}else{
			LOGGER.error("getIdleServerFromList() : Servers list passed is null.");
		}
		
		LOGGER.debug("getIdleServerFromList() : idleServer : " + idleServer);
		return idleServer;
	}

	
	/**
	 * 
	 * This method creates SOAP request XML using TransferPlan instance.
	 * 
	 * @param issuer
     *        The Plan Issuer instance.
     *        
     * @param fileMap
     *        Map of data template values. 
     * 
     * @param uploadedTemplatesMap
	 *        Contains Map value as Byte stream of data templates.  
	 * 
	 * @param exchangeType
	 *        Selected exchange type while uploading Plan.
	 * 
	 * @param tenantCode
	 *        Selected tenant code while uploading Plan.
	 *        
     * @return New TransferPlan instance.
     * 
     * @throws Exception
     */
	private StringEntity createSOAPPayload(Issuer issuer, Map<String, String> fileMap, Map<String, byte[]> uploadedTemplatesMap, SerffPlanMgmtBatch waitingBatch) throws GIException, JAXBException {
		LOGGER.info("Executing createSOAPPayload() : fileMap : " + fileMap + MSG_ISSUER_ID + issuer.getId() + " : exchangeType : " + waitingBatch.getExchangeType() 
				+ " : tenantCode : " + waitingBatch.getDefaultTenant() + " State: " + waitingBatch.getState());
		StringEntity stringentity = null;
		String soapRequest = null;
		try {
			TransferPlan transferPlanRequest = createTransferPlan(issuer, fileMap, uploadedTemplatesMap, waitingBatch);
			soapRequest = serffUtils.objectToXmlString(transferPlanRequest);
			soapRequest = soapRequest.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
			stringentity = new StringEntity(SOAP_HEADER+soapRequest+SOAP_FOOTER);
			stringentity.setChunked(true);
			stringentity.setContentType("text/xml");			
		} catch (IOException e) {
			LOGGER.error("createSOAPPayload() : Error occurred : " + e.getMessage());
			throw new GIException(e);
		}
		return stringentity;
	}

    /**
     * This method creates TransferPlan instance by wrapping Plan instance internally.
     * 
     * @param issuer
     *        The Plan Issuer instance.
     *        
     * @param fileMap
     *        Map of data template values. 
     * 
    	 * @param uploadedTemplatesMap
	 *        Contains Map value as Byte stream of data templates.  
	 * 
	 * @param exchangeType
	 *        Selected exchange type while uploading Plan.
	 * 
	 * @param tenantCode
	 *        Selected tenant code while uploading Plan.
	 *        
     * @return New TransferPlan instance.
     * 
     * @throws Exception
     */
	private TransferPlan createTransferPlan(Issuer issuer, Map<String, String> fileMap, Map<String, byte[]> uploadedTemplatesMap, SerffPlanMgmtBatch waitingBatch) throws GIException {
		LOGGER.info("Executing createTransferPlan() : fileMap : " + fileMap + MSG_ISSUER_ID + issuer.getId() + " : exchangeType : " + waitingBatch.getExchangeType() 
				+ " : tenantCode : " + waitingBatch.getDefaultTenant() + " stateCode: " + waitingBatch.getState() + " Batch ID:" + waitingBatch.getId());
		GregorianCalendar c = new GregorianCalendar();
		c.setTimeInMillis(System.currentTimeMillis());
		XMLGregorianCalendar currentTime = null;
		try {
			currentTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			LOGGER.error("createTransferPlan() : Error occurred : " + e.getMessage());
			throw new GIException(e);
		}
		TransferPlan transferPlan = new TransferPlan();
		transferPlan.setRequestId("PHIX-" + System.currentTimeMillis());
		transferPlan.setTransferRequestTimestamp(currentTime);
		transferPlan.setPlan(createPlan(issuer, currentTime, fileMap, uploadedTemplatesMap, waitingBatch));
		LOGGER.debug("createTransferPlan() : Returning transfer plan with request id : " + transferPlan.getRequestId());
		return transferPlan;
	}

	/**
	 * This is a utility method to create Plan instance from provided Plan attributes. 
	 * 
	 * @param issuer
	 *        Plan Issuer instance.
	 * 
	 * @param currentTime
	 *        Current timestamp.
	 * 
	 * @param fileMap
	 *        Map of data templates information.
	 * 
	 * @param uploadedTemplatesMap
	 *        Contains Map value as Byte stream of data templates.  
	 * 
	 * @param exchangeType
	 *        Selected exchange type while uploading Plan.
	 * 
	 * @param tenantCode
	 *        Selected tenant code while uploading Plan.
	 * 
	 * @return New Plan instance.
	 */
	private Plan createPlan(Issuer issuer, XMLGregorianCalendar currentTime, Map<String, String> fileMap, Map<String, byte[]> uploadedTemplatesMap, SerffPlanMgmtBatch waitingBatch) {
		LOGGER.info("Executing createPlan() : fileMap : " + fileMap + MSG_ISSUER_ID + issuer.getId() + " : exchangeType : " + waitingBatch.getExchangeType() 
				+ " : tenantCode : " + waitingBatch.getDefaultTenant() + " State Code: " + waitingBatch.getState());
		Plan plan = new Plan();
		plan.setStandardComponentId(issuer.getHiosIssuerId() + waitingBatch.getState() + "1234567");
		//Plan Year is not used by plan loading for now, so sending any default value is OK 
		plan.setPlanYear((short)DEFAULT_YEAR);
		//Dental flag is not used by plan loading for now, so sending any default value is OK 
		plan.setDental(false);
		//Add Batch ID to tracking number, so the service can update status in DB. 
		//Tracking number generated from BATCH has SERFF_BT prefix
		plan.setSerffTrackingNumber("SERFF_BT-" + waitingBatch.getId());
		plan.setStateTrackingNumber("STATE-" + currentTime.normalize());
		plan.setHiosProductId(issuer.getHiosIssuerId());
		//FOR PHIX-This contains the exchange type filter (ON, OFF, ANY) as prefix
		plan.setPlanName("Exchange PHIX Plan");
		plan.setMarketType(MarketType.INDIVIDUAL);
		//Following values are not used by plan loading for now, so sending any default value is OK 
		plan.setPlanLevel(PlanMetalLevel.BRONZE);
		plan.setSerffStatus(SerffStatus.SUBMITTED);
		plan.setSerffStatusChangeDate(currentTime);
		plan.setStatePlanStatus("StateStatus");
		plan.setStatePlanStatusChangeDate(currentTime);
		plan.setPlanDispositionStatus("DispositionStatus");
		plan.setPlanDispositionStatusChangeDate(currentTime);
		plan.setCertificationStatus(false);
		plan.setCertificationStatusChangeDate(currentTime);
		plan.setExchangeWorkflowStatus("Complete");
		plan.setExchangeWorkflowStatusChangeDate(currentTime);
		plan.setStateAbbreviation(SerffConstants.DEFAULT_PHIX_STATE_CODE);
		plan.setDateSubmitted(currentTime);
		plan.setInsurer(getInsurer(issuer));
		Map<String, String> dataMap = new HashMap<String, String>();
		Map<String, String> documentMap = new HashMap<String, String>();
		for (Entry<String, String> fileEntry : fileMap.entrySet()) {
			if(fileEntry.getKey().startsWith("DATA_")) {
				dataMap.put(fileEntry.getKey(), fileEntry.getValue());
			} else {
				documentMap.put(fileEntry.getKey(), fileEntry.getValue());
			}
		}
		plan.setDataTemplates(getDataTemplates(issuer, currentTime, dataMap, uploadedTemplatesMap));
		plan.setSupportingDocuments(getSupportingDocuments(issuer, currentTime, documentMap, uploadedTemplatesMap));
		return plan;
	}

	
	/**
	 * This is utility method to create SupportingDocuments instance.
	 * 
	 * @param issuer
	 *        Plan Issuer instance.
	 *        
	 * @param currentTime
	 *        Current timestamp.
	 * @param uploadedTemplatesMap 
	 * @param dataMap 
	 *        
	 * @return New SupportingDocuments instance.
	 */
	private SupportingDocuments getSupportingDocuments(Issuer issuer, XMLGregorianCalendar currentTime, Map<String, String> documentMap, Map<String, byte[]> uploadedTemplatesMap) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Executing getSupportingDocuments() : Issuer id : " + issuer.getId() + " time:" + currentTime + " Doc Count:" + documentMap.size());
		}
		SupportingDocuments suppDocsTemplates = new SupportingDocuments();
		if(!CollectionUtils.isEmpty(documentMap)){
			SupportingDocument suppDoc = new SupportingDocument();
			Attachments attachments = new Attachments();
			suppDocsTemplates.getSupportingDocument().add(suppDoc);
			suppDoc.setDateLastModified(currentTime);
			suppDoc.setName("SupportingDocs");
			suppDoc.setUserAdded(true);
			suppDoc.setComment("Supporting Documents");
			suppDoc.setScheduleItemStatus("SupportingDocs");
			suppDoc.setScheduleItemStatusChangeDate(currentTime);
			suppDoc.setAttachments(attachments);
			for (Entry<String, String> fileEntry : documentMap.entrySet()) {
				byte[] bytes = uploadedTemplatesMap.get(fileEntry.getKey());
				if(bytes != null) {
					ByteArrayDataSource ds = new ByteArrayDataSource(bytes);
					DataHandler documentData = new DataHandler(ds);
					Attachment attachment = new Attachment();
					attachment.setFileName(fileEntry.getValue());
					attachment.setDateLastModified(currentTime);
					attachment.setAttachmentData(documentData);
					attachments.getAttachment().add(attachment);
				}
			}
		}		
		return suppDocsTemplates ;
	}

	
	/**
	 * This method adds DataTemplates that are present in fileMap in to  TransferDataTemplates instance.
	 * 
	 * @param issuer
	 *        Plan Issuer instance.
	 * 
	 * @param currentTime
	 *        Current timestamp.
	 * 
	 * @param fileMap
	 *        Map holding datatemplates values.
	 *        
	 * @param uploadedTemplatesMap
	 *        Contains Map value as Byte stream of data templates.  
	 * 
	 * @return TransferDataTemplates instance.
	 */
	private TransferDataTemplates getDataTemplates(Issuer issuer, XMLGregorianCalendar currentTime, Map<String, String> fileMap,
			Map<String, byte[]> uploadedTemplatesMap) {
		LOGGER.info("Executing getDataTemplates() : fileMap : " + fileMap + MSG_ISSUER_ID + issuer.getId());
		TransferDataTemplates tfrDataTemplates = new TransferDataTemplates();
		List<TransferDataTemplate> dataTemplate = new ArrayList<TransferDataTemplate>();		
		if(null != fileMap){
			LOGGER.debug("getDataTemplates() : fileMap : " + fileMap);
			for (Entry<String, String> fileEntry : fileMap.entrySet()) {
				byte[] bytes = uploadedTemplatesMap.get(fileEntry.getKey());
				if(bytes != null) {
					ByteArrayDataSource ds = new ByteArrayDataSource(bytes);
					DataHandler templateData = new DataHandler(ds);
					TransferDataTemplate template = new TransferDataTemplate();
					template.setComment("PHIX plan template " + fileEntry.getKey());
					template.setDataTemplateId(Integer.toString(issuer.getId()));
					template.setDataTemplateType(fileEntry.getKey());
					template.setDateSubmitted(currentTime);
					template.setScheduleItemStatus("schedStat");
					template.setScheduleItemStatusChangeDate(currentTime);
					InsurerAttachment attachment = new InsurerAttachment();
					attachment.setFileName(fileEntry.getValue());
					attachment.setDateLastModified(currentTime);
					attachment.setInsurerSuppliedData(templateData);
					template.setInsurerAttachment(attachment);
					dataTemplate.add(template);
				}
			}
		}		
		tfrDataTemplates.setDataTemplate(dataTemplate);
		return tfrDataTemplates;
	}

	
	/**
	 * This method creates and returns Insurer instance from passed Issuer instance.
	 * 
	 * @param issuer
	 *        The Plan Issuer instance.
	 *        
	 * @return new Insurer instance.
	 */
	private Insurer getInsurer(Issuer issuer) {
		LOGGER.info("Executing getInsurer() : Issuer id : " + issuer.getId());
		Insurer insurer = new Insurer();
		insurer.setIssuerId(issuer.getId());
		insurer.setCompanyInfo(new CompanyInfo());
		insurer.setAccreditationInfo(new AccreditationInfo());
		return insurer;
	}

	
	/**
	 * Method is used to generate Admin Template XML from Issuer.
	 * @param issuerId 
	 *        issuerId in String format
	 * 
	 * @exception GIException
	 */
	public String getIssuerTemplate(Issuer issuer, String issuerState) throws GIException{
		
		LOGGER.info("Executing getIssuerTemplate() Start");
		String issuerData = null;
		
			if (null != issuer) {
				LOGGER.info("Issuer id: " + issuer.getId() + " and Issuer State: " + issuerState);
				
				PayloadType adminTemplate = new PayloadType();
				adminTemplate.setIssuer(new IssuerType());
				// Issuer Individual Market Contact--OrgChart
				PersonType pt = new PersonType();
				pt.setPersonName(new PersonNameType());
				adminTemplate.getIssuer().setIssuerIndividualMarketContact(pt);
				adminTemplate.getIssuer().setIssuerSmallGroupMarketContact(pt);
				adminTemplate.getIssuer().setIssuerIdentification(new IdentificationType());
				adminTemplate.getIssuer().getIssuerIdentification().setIdentificationID(new com.serff.template.admin.niem.proxy.String());
				adminTemplate.getIssuer().getIssuerIdentification().getIdentificationID().setValue(issuer.getHiosIssuerId());
				adminTemplate.getIssuer().setIssuerStateCode(new USStateCodeType());
				adminTemplate.getIssuer().setIssuerProposedExchangeMarketCoverageCode(new InsurancePlanLevelCodeType());
				if (null != issuer.getStateOfDomicile()) {
					adminTemplate.getIssuer().getIssuerStateCode().setValue(USStateCodeSimpleType.valueOf(issuer.getStateOfDomicile()));
				} else if (null != issuer.getState()) {
					adminTemplate.getIssuer().getIssuerStateCode().setValue(USStateCodeSimpleType.valueOf(issuer.getState()));
				} else {
					adminTemplate.getIssuer().getIssuerStateCode().setValue(USStateCodeSimpleType.valueOf(issuerState));
				}
				
				adminTemplate.getIssuer().setIssuerSHOPCustomerService(getContactInformationType());
				adminTemplate.getIssuer().setIssuerIndividualMarketCustomerService(getContactInformationType());
				
				ContactInformationType contactInformationType = null;
				// Customer Service - Shop
				contactInformationType = adminTemplate.getIssuer().getIssuerSHOPCustomerService();
				if (null != issuer.getShopCustServicePhone()) {
					contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getShopCustServicePhone());
					if (null != issuer.getShopCustServicePhoneExt()) {
						contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID().setValue(issuer.getShopCustServicePhoneExt());
					}
				}
				
				if (null != issuer.getShopCustServiceTTY()) {
					contactInformationType.getContactTTYTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getShopCustServiceTTY());
				}
				
				if (null != issuer.getShopCustServiceTollFree()) {
					contactInformationType.getContactTollFreeTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getShopCustServiceTollFree());
				}
				
				if (null != issuer.getShopSiteUrl()) {
					contactInformationType.getContactWebsiteURI().setValue(SerffUtils.checkAndcorrectURL(issuer.getShopSiteUrl()));
				}
				
				// Customer Service - Individual Market
				contactInformationType = adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService();
				if (null != issuer.getIndvCustServicePhone()) {
					contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getIndvCustServicePhone());
					if (null != issuer.getIndvCustServicePhoneExt()) {
						contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID().setValue(issuer.getIndvCustServicePhoneExt());
					}
				}
				
				if (null != issuer.getIndvCustServiceTTY()) {
					contactInformationType.getContactTTYTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getIndvCustServiceTTY());
				}
				
				if (null != issuer.getIndvCustServiceTollFree()) {
					contactInformationType.getContactTollFreeTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getIndvCustServiceTollFree());
				}
				
				if (null != issuer.getIndvSiteUrl()) {
					contactInformationType.getContactWebsiteURI().setValue(SerffUtils.checkAndcorrectURL(issuer.getIndvSiteUrl()));
				}
				
				OrganizationType organizationType = null;
				organizationType = new OrganizationType();
				adminTemplate.setInsuranceCompanyOrganization(organizationType);
				adminTemplate.getInsuranceCompanyOrganization().setOrganizationAugmentation(new OrganizationAugmentationType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().setOrganizationLegalName(new TextType());
				adminTemplate.getInsuranceCompanyOrganization().setOrganizationLocation(new LocationType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().setLocationAddress(new AddressType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().setStructuredAddress(new StructuredAddressType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationStreet(new StreetType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().setStreetFullText(new TextType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().setStreetExtensionText(new TextType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationCityName(new ProperNameTextType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationStateUSPostalServiceCode(new USStateCodeType());

				if (null != issuer.getCompanyLegalName()) {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationLegalName().setValue(issuer.getCompanyLegalName());
				} else {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationLegalName().setValue(issuer.getName());
				}
				
				if (null != issuer.getCompanyAddressLine1()) {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetFullText().setValue(issuer.getCompanyAddressLine1());
					if (null != issuer.getCompanyAddressLine2()) {
						adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetExtensionText().setValue(issuer.getCompanyAddressLine2());
					}
				} else {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetFullText().setValue(issuer.getAddressLine1());
					if (null != issuer.getAddressLine2()) {
						adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetExtensionText().setValue(issuer.getAddressLine2());
					}
				}
				
				if (null != issuer.getCompanyCity()) {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationCityName().setValue(issuer.getCompanyCity());
					if (null != issuer.getCompanyState()) {
						adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStateUSPostalServiceCode().setValue(USStateCodeSimpleType.valueOf(issuer.getCompanyState()));
					}
				} else {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationCityName().setValue(issuer.getCity());
					if (null != issuer.getState()) {
						adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStateUSPostalServiceCode().setValue(USStateCodeSimpleType.valueOf(issuer.getState()));
					}
				}
				
				if (null != issuer.getCompanyZip()) {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationPostalCode(new com.serff.template.admin.niem.proxy.String());
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationPostalCode().setValue(issuer.getCompanyZip());
				}
				
				adminTemplate.setInsuranceCompany(new InsuranceCompanyType());
				if (null != issuer.getFederalEin()) {
					adminTemplate.getInsuranceCompany().setInsuranceCompanyTINID(new TextType());
					adminTemplate.getInsuranceCompany().getInsuranceCompanyTINID().setValue(issuer.getFederalEin());
				}
				
				organizationType = new OrganizationType();
				adminTemplate.setIssuerOrganization(organizationType);
				adminTemplate.getIssuerOrganization().setOrganizationLocation(new LocationType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().setLocationAddress(new AddressType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().setStructuredAddress(new StructuredAddressType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationStreet(new StreetType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().setStreetFullText(new TextType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().setStreetExtensionText(new TextType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationCityName(new ProperNameTextType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationStateUSPostalServiceCode(new USStateCodeType());
				adminTemplate.getIssuerOrganization().setOrganizationAugmentation(new OrganizationAugmentationType());
				adminTemplate.getIssuerOrganization().getOrganizationAugmentation().setOrganizationMarketingName(new TextType());

				if (null != issuer.getAddressLine1()) {
					adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetFullText().setValue(issuer.getAddressLine1());
					if (null != issuer.getAddressLine2()) {
						adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetExtensionText().setValue(issuer.getAddressLine2());
					}
				}
				
				if (null != issuer.getCity()) {
					adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationCityName().setValue(issuer.getCity());
					if (null != issuer.getState()) {
						adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStateUSPostalServiceCode().setValue(USStateCodeSimpleType.valueOf(issuer.getState()));
					}
				}
				
				if (null != issuer.getZip()) {
					adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationPostalCode(new com.serff.template.admin.niem.proxy.String());
					adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationPostalCode().setValue(issuer.getZip());
				} 
				
				if (null != issuer.getMarketingName()) {
					adminTemplate.getIssuerOrganization().getOrganizationAugmentation().getOrganizationMarketingName().setValue(issuer.getMarketingName());
				} else {
					adminTemplate.getIssuerOrganization().getOrganizationAugmentation().getOrganizationMarketingName().setValue(issuer.getName());
				}
				// To generate orgChart from fields. Refer saveIssuerBean() method from PlanMgmtSerffServiceImpl.java.
				// issuer.getOrgChart();
				issuerData = serffUtils.objectToXmlString(adminTemplate);
			} 
			
			LOGGER.info("getIssuerTemplate END");
		return issuerData;
	}	
	
	/**
	 * This method create and returns a new ContactInformationType instance.
	 * 
	 * @return ContactInformationType instance.
	 */
	private ContactInformationType getContactInformationType() {
		
		LOGGER.info("Executing getContactInformationType()");
		
		ContactInformationType contactInformationType = new ContactInformationType();
		contactInformationType.setContactMainTelephoneNumber(new TelephoneNumberType());
		contactInformationType.getContactMainTelephoneNumber().setFullTelephoneNumber(new FullTelephoneNumberType());
		contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().setTelephoneNumberFullID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().setTelephoneSuffixID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.setContactTTYTelephoneNumber(new TelephoneNumberType());
		contactInformationType.getContactTTYTelephoneNumber().setFullTelephoneNumber(new FullTelephoneNumberType());
		contactInformationType.getContactTTYTelephoneNumber().getFullTelephoneNumber().setTelephoneNumberFullID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.getContactTTYTelephoneNumber().getFullTelephoneNumber().setTelephoneSuffixID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.setContactTollFreeTelephoneNumber(new TelephoneNumberType());
		contactInformationType.getContactTollFreeTelephoneNumber().setFullTelephoneNumber(new FullTelephoneNumberType());
		contactInformationType.getContactTollFreeTelephoneNumber().getFullTelephoneNumber().setTelephoneNumberFullID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.getContactTollFreeTelephoneNumber().getFullTelephoneNumber().setTelephoneSuffixID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.setContactWebsiteURI(new AnyURI());
		return contactInformationType;
	}

	/**
	 * Method is get list of Issuer Id and Name in Issuer object.
	 * @author Bhavin Parmar
	 */
	@Override
	public List<Issuer> getIssuerNameList() {
		LOGGER.info("Executing getIssuerNameList()");
		return iIssuerRepository.getIssuerNameList();
	}

	/**
	 * This method returns list of available Tenants.
	 * 
	 */
	@Override
	public List<Tenant> getTenantNameList() {	
		LOGGER.info("Executing getTenantNameList()");
		return iTenantRepository.findAll();
	}

	
	/**
	 * Processes plan loading job.
	 * 
	 * @param waitingPlanBatch - Plan batch
	 * @return - Map of different status messages
	 */
	private Map<STATUS_KEY, Object> processPlanLoadJob(SerffPlanMgmtBatch waitingPlanBatch){

		LOGGER.info(MESSAGE_MARKING_TEXT + JOB_STARTED_TEXT + waitingPlanBatch.getId() + MESSAGE_MARKING_TEXT);
		
		boolean isBatchSuccess = false;
		String[] servers;
		String serverList;
		String server;
		String message = "";
		Map<STATUS_KEY, Object> statusMap = new HashMap<STATUS_KEY, Object>();
		boolean isBreak = false;
		//Search in SERFF_PLAN_MGMT table and find idle SERFF system
		serverList = SerffConstants.getServiceServerList();
		
		if (StringUtils.isNotBlank(serverList)) {
			LOGGER.debug("processSerffPlanMgmtBatch() : Server List : " + serverList + " ConnTimeOut:" + DEFAULT_CONN_TIMEOUT + " ServerIdleTime:" + CUTOFF_TIME);
			
			servers = serverList.split(",");
			
//			if (null != servers) {
				server = getIdleServerFromList(servers);
				
				if (null != server) {
					isBatchSuccess = executePlanLoadJob(waitingPlanBatch, server);
					
					if (isBatchSuccess) {
					}
					else {
						LOGGER.info(MESSAGE_MARKING_TEXT + JOB_ENDED_TEXT + waitingPlanBatch.getId() + STATUS_FLAG_TEXT + isBatchSuccess + MESSAGE_MARKING_TEXT);
						isBreak = true;
					}
				}
				else {
					LOGGER.error(ALL_SERVER_BUSY_MESSAGE + SERFF_JOB_NUMBER_TEXT + waitingPlanBatch.getId());
					LOGGER.info(MESSAGE_MARKING_TEXT + JOB_ENDED_TEXT + waitingPlanBatch.getId() + STATUS_FLAG_TEXT + isBatchSuccess + MESSAGE_MARKING_TEXT);
					message = ALL_SERVER_BUSY_MESSAGE;
					isBreak = true;
				}
			/*}
			else {
				LOGGER.error(SERFF_SERVICE_ERROR_MESSAGE + SERFF_JOB_NUMBER_TEXT + waitingPlanBatch.getId());
				LOGGER.info(MESSAGE_MARKING_TEXT + JOB_ENDED_TEXT + waitingPlanBatch.getId() + STATUS_FLAG_TEXT + isBatchSuccess + MESSAGE_MARKING_TEXT);
				message = SERFF_SERVICE_ERROR_MESSAGE;
				isBreak = true;
			}*/
		}
		else {
			LOGGER.error(SERFF_SERVICE_LIST_ERROR_MESSAGE + SERFF_JOB_NUMBER_TEXT + waitingPlanBatch.getId());
			LOGGER.info(MESSAGE_MARKING_TEXT + JOB_ENDED_TEXT + waitingPlanBatch.getId() + STATUS_FLAG_TEXT + isBatchSuccess + MESSAGE_MARKING_TEXT);
			message = SERFF_SERVICE_LIST_ERROR_MESSAGE;
			isBreak = true;
		}
		
		if (!isBreak) {
			LOGGER.info(MESSAGE_MARKING_TEXT + JOB_ENDED_TEXT + waitingPlanBatch.getId() + STATUS_FLAG_TEXT + isBatchSuccess + MESSAGE_MARKING_TEXT);
		}
	
		statusMap.put(STATUS_KEY.IS_BREAK, isBreak);
		statusMap.put(STATUS_KEY.STATUS_MESSAGE, message);		
		
		return statusMap;		
	}
	
	
	/**
	 * Processes prescription drug batch.
	 * 
	 * @param waitingPrescriptionDrugBatch - The batch record for Prescription Drug job
	 * @return - Map of status key and status message.
	 */
	private Map<STATUS_KEY, Object> processPrescriptionDrugLoadJob(SerffPlanMgmtBatch waitingPrescriptionDrugBatch){
		
		LOGGER.info("processPrescriptionDrugLoadJob() Start : batch id : " + waitingPrescriptionDrugBatch.getId());
		
		Map<STATUS_KEY, Object> statusMap = new HashMap<STATUS_KEY, Object>();
		Map<STATUS_MESSAGE_KEY, String> statusMessagesMap = new HashMap<STATUS_MESSAGE_KEY, String>();
		
		boolean processStatus = true;
		PlanUpdateStatistics planUpdateStatistics = null;
		SerffPlanMgmt serffPlanMgmtRecord = null;
		Date startTime = new Date(System.currentTimeMillis());
		String folderPath = SerffConstants.getFTPUploadPlanPath() + waitingPrescriptionDrugBatch.getIssuerId() 
				+ waitingPrescriptionDrugBatch.getState() + waitingPrescriptionDrugBatch.getId() + "/";

		try{
			LOGGER.debug("executePlanLoadJob() : Start of Loading Prescription drug from location " + folderPath + ". Issuer HIOS ID : " + waitingPrescriptionDrugBatch.getIssuerId());
			//Update row that job is picked up, set status to IN_PROGRESS
			updateBatchJobStatus(waitingPrescriptionDrugBatch, SerffPlanMgmtBatch.BATCH_STATUS.IN_PROGRESS, new java.sql.Date((startTime).getTime()), null);
			//Create record in SERFF_PLAN_MGMT table
			serffPlanMgmtRecord = serffUtils.createSerffPlanMgmtRecord(TRANSFER_PRESCRIPTION_DRUG);
			serffPlanMgmtRecord = serffService.saveSerffPlanMgmt(serffPlanMgmtRecord);
			
			Issuer issuer = iIssuerRepository.getIssuerByHiosID(waitingPrescriptionDrugBatch.getIssuerId());
		
				if (null != issuer) {
					Map<String, String> fileMap = getAllTemplatesFilelist(folderPath, null);
					StringBuilder attachmentList = new StringBuilder();
					//Add templates in ECM.
					if(!CollectionUtils.isEmpty(fileMap)){
						
						for(Entry<String, String> entryTemplate : fileMap.entrySet()){
							LOGGER.debug("processPrescriptionDrugLoadJob() : Adding template in ECM : " + entryTemplate.getValue());
							addPrescriptionDrugTemplateInECM(serffPlanMgmtRecord, folderPath, entryTemplate.getValue(), entryTemplate.getKey());
							attachmentList.append(entryTemplate.getKey() + ",");
						}
					}
					//Set issuer id and template list in tracking record.
					serffPlanMgmtRecord.setIssuerId(String.valueOf(issuer.getId()));
					serffPlanMgmtRecord.setAttachmentsList(attachmentList.toString());
					serffPlanMgmtRecord = serffService.saveSerffPlanMgmt(serffPlanMgmtRecord);
					
					//Validate PrescriptionDrug templates.
					if(isValidPrescriptionDrugJobTemplateCount(fileMap)){
						
						Map<String, byte[]> uploadedPrescriptionDrugTemplatesMap = null;
						statusMap = new HashMap<STATUS_KEY, Object>();
						
						LOGGER.debug("processPrescriptionDrugLoadJob() : Found " + fileMap.size() + " templates for loading plans from location " + folderPath );
						uploadedPrescriptionDrugTemplatesMap = getAllTemplatesForPlans(issuer, folderPath, fileMap, waitingPrescriptionDrugBatch.getState());
	
						Object planBenefitTemplateVO = serffUtils.inputStreamToObject(new ByteArrayInputStream(uploadedPrescriptionDrugTemplatesMap.get(SerffConstants.DATA_BENEFITS)), 
							com.serff.template.plan.PlanBenefitTemplateVO.class); 
					
						Object prescriptionDrugTemplateVO = serffUtils.inputStreamToObject(new ByteArrayInputStream(uploadedPrescriptionDrugTemplatesMap.get(SerffConstants.DATA_PRESCRIPTION_DRUG)), 
							com.serff.template.plan.PrescriptionDrugTemplateVO.class);
					
						if(null != planBenefitTemplateVO && null != prescriptionDrugTemplateVO){
							List<String> errorCodes = new ArrayList<String>();
							List<String> errorMessages = new ArrayList<String>();
							List<String> processMessages = new ArrayList<String>();
							//Process prescription drug templates.		
							planUpdateStatistics  = populateAndPersistPrescriptionDrugJobTemplates((com.serff.template.plan.PlanBenefitTemplateVO)planBenefitTemplateVO,
									(com.serff.template.plan.PrescriptionDrugTemplateVO)prescriptionDrugTemplateVO, errorCodes, errorMessages, processMessages);
							
							//If errorCodes and errorMessages are not empty means process failed due to validation.
							if((!CollectionUtils.isEmpty(errorCodes)) || (!CollectionUtils.isEmpty(errorMessages))){
								processStatus = false;
								LOGGER.error("processPrescriptionDrugLoadJob() : populateAndPersistPrescriptionDrugTemplates finished with errors.");
								
								StringBuilder validationErrorString = new StringBuilder();
								for(String validationError : errorMessages){
									validationErrorString.append(validationError);
									validationErrorString.append(SerffConstants.NEW_LINE);
								}
								//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
								populateStatusMessagesMap(MSG_VALIDATION_ERROR, validationErrorString.toString(), MSG_INVALID_REQUEST, 
										MSG_PROCESS_FAILED, statusMessagesMap);
							}
							
							//If processMessages is not empty means something failed after validation e.g. persistence exception.
							if(!CollectionUtils.isEmpty(processMessages)){
								processStatus = false;
								LOGGER.error("processPrescriptionDrugLoadJob() : populateAndPersistPrescriptionDrugTemplates finished with exception.");
								//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
								populateStatusMessagesMap(MSG_ERROR_OCCURRED, processMessages.get(0) , processMessages.get(0), 
										MSG_PROCESS_FAILED, statusMessagesMap);
							}
							
						}else{
							LOGGER.error("processPrescriptionDrugLoadJob() : Incompatible template types found.");
							//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
							populateStatusMessagesMap(MSG_INCOMPATIBLE_TEMPLATES, MSG_INCOMPATIBLE_TEMPLATES, MSG_INVALID_REQUEST, 
									MSG_INVALID_REQUEST, statusMessagesMap);
							processStatus = false;
						}
					
				}else{
					LOGGER.debug("processPrescriptionDrugLoadJob() : Prescription Drug Batch has been failed due to invalid templates.");
					//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
					populateStatusMessagesMap(MSG_INVALID_TEMPLATE, MSG_INVALID_TEMPLATE, MSG_INVALID_REQUEST, 
							MSG_INVALID_REQUEST, statusMessagesMap);
					processStatus = false;
				}
			
			} else {
				LOGGER.debug("processPrescriptionDrugLoadJob() : Prescription Drug Batch has been failed due to Issuer is not available with HIOS ID " + waitingPrescriptionDrugBatch.getIssuerId());
				//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
				populateStatusMessagesMap(MSG_ISSUER_NOT_AVAILABLE + waitingPrescriptionDrugBatch.getIssuerId(), MSG_INVALID_TEMPLATE, MSG_INVALID_REQUEST, 
						MSG_INVALID_REQUEST, statusMessagesMap);
				processStatus = false;
			}
		
		}catch(Exception ex){
			LOGGER.error("processPrescriptionDrugLoadJob() : Error while processing prescription drug templates: " + ex.getMessage(), ex);
			//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
			populateStatusMessagesMap(MSG_ERROR_OCCURRED, MSG_ERROR_OCCURRED, ex.getMessage(), 
					MSG_PROCESS_FAILED, statusMessagesMap);
			processStatus = false;
		}finally{
			if(processStatus){
				LOGGER.info("processPrescriptionDrugLoadJob() End.");
				//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
				populateStatusMessagesMap(MSG_PROCESS_COMPLETED_SUCCESS, MSG_PROCESS_COMPLETED_SUCCESS, MSG_PROCESS_COMPLETED_SUCCESS, 
						MSG_PROCESS_COMPLETED_SUCCESS, statusMessagesMap);
				updateBatchJobStatus(waitingPrescriptionDrugBatch, SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
			}else{
				updateBatchJobStatus(waitingPrescriptionDrugBatch, SerffPlanMgmtBatch.BATCH_STATUS.FAILED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
			}
			
			if (null != serffPlanMgmtRecord) {
				//Save tracking record.
   				persistTrackingRecord(serffPlanMgmtRecord, planUpdateStatistics, processStatus, statusMessagesMap);
			}
		}
	
		statusMap.put(STATUS_KEY.STATUS_MESSAGE, statusMessagesMap.get(STATUS_MESSAGE_KEY.UI_MESSAGE));	
		return statusMap;
		
	}


	/**
	 * Set different message values in map.
	 * 
	 * @param uiMessage - Message to be displayed on UI
	 * @param responseXmlMessaage - Response message for tracking record
	 * @param processDescMessage - Process description for tracking record
	 * @param remarksMesage - Remarks message for tracking record
	 * @param statusMessagesMap - Map which holds different status messages.
	 */
	private void populateStatusMessagesMap(String uiMessage, String responseXmlMessaage, String processDescMessage, String remarksMesage, Map<STATUS_MESSAGE_KEY, String> statusMessagesMap){
		
		LOGGER.info("populateStatusMessagesMap() : Start");
		if(null != statusMessagesMap){
			
			if(null != uiMessage){
				statusMessagesMap.put(STATUS_MESSAGE_KEY.UI_MESSAGE, uiMessage);
			}
			
			if(null != uiMessage){
				statusMessagesMap.put(STATUS_MESSAGE_KEY.RESPONSE_XML_MESSAGE, responseXmlMessaage);
			}
			
			if(null != uiMessage){
				statusMessagesMap.put(STATUS_MESSAGE_KEY.PROCESS_DESCRIPTION_MESSAGE, processDescMessage);
			}
			
			if(null != uiMessage){
				statusMessagesMap.put(STATUS_MESSAGE_KEY.REMARKS_MESSAGE, remarksMesage);
			}
		}
		LOGGER.info("populateStatusMessagesMap() : End");
	}
	
	
	/**
	 * Persist tracking record with provided details.
	 * 
	 * @param serffPlanMgmtRecord - Tracking record
	 * @param planUpdateStatistics - PlanUpdateStatistics instance with update count details
	 * @param processStatus - Whether template processing is successful
	 * @param statusMessagesMap - Map of status key and values
	 */
	private void persistTrackingRecord(SerffPlanMgmt serffPlanMgmtRecord, PlanUpdateStatistics planUpdateStatistics, boolean processStatus, 
			Map<STATUS_MESSAGE_KEY, String> statusMessagesMap){
		LOGGER.info("persistTrackingRecord() : Start");
		//Set status message in tracking record.
		serffPlanMgmtRecord.setResponseXml(statusMessagesMap.get(STATUS_MESSAGE_KEY.RESPONSE_XML_MESSAGE));
		serffPlanMgmtRecord.setRequestStateDesc(statusMessagesMap.get(STATUS_MESSAGE_KEY.PROCESS_DESCRIPTION_MESSAGE));
		serffPlanMgmtRecord.setEndTime(new Date());
		serffPlanMgmtRecord.setRequestStatus(processStatus ? SerffPlanMgmt.REQUEST_STATUS.S : SerffPlanMgmt.REQUEST_STATUS.F);
		serffPlanMgmtRecord.setPlanStats(null != planUpdateStatistics ? planUpdateStatistics.toString() : null);
		serffPlanMgmtRecord.setRemarks(statusMessagesMap.get(STATUS_MESSAGE_KEY.REMARKS_MESSAGE));
		serffPlanMgmtRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
		serffService.saveSerffPlanMgmt(serffPlanMgmtRecord);
		LOGGER.info("persistTrackingRecord() : End");
	}
	
	/**
	 * Validates whether templates count for prescription drug job is 2.
	 * 
	 * @param uploadedPrescriptionDrugTemplatesMap - Files map present at FTP location for prescription drug job
	 * @return - is valid file count in prescription drug template job
	 */
	private boolean isValidPrescriptionDrugJobTemplateCount(Map<String, String> uploadedPrescriptionDrugTemplatesMap) {
		
		LOGGER.info("isValidPrescriptionDrugJobTemplateCount() : Start");
		
		if(null == uploadedPrescriptionDrugTemplatesMap || uploadedPrescriptionDrugTemplatesMap.size() != SerffConstants.PRESCRIPTION_DRUG_TEMPLATES_COUNT){
			LOGGER.error("Invalid Prescription Drug templates. Templates are empty or not equal to 2.");
			return false;
		}else if(null == uploadedPrescriptionDrugTemplatesMap.get(SerffConstants.DATA_BENEFITS) || null == uploadedPrescriptionDrugTemplatesMap.get(SerffConstants.DATA_PRESCRIPTION_DRUG)){
			LOGGER.error("Invalid Prescription Drug templates. Template type is not valid.");
			return false;
		}
		
		LOGGER.info("isValidPrescriptionDrugJobTemplateCount() : End");
		return true;
	}
	
	/**
	 * Populates and persist prescription drug template.
	 * 
	 * @param planBenefitTemplateVO - Plan template instance
	 * @param prescriptionDrugTemplateVO - Prescription drug instance
	 * @param errorCodes  - Error codes list
	 * @param errorMessages - Error messages list
	 * @param processMessages - Process messages list
	 * @return - plan update status
	 */
	private PlanUpdateStatistics populateAndPersistPrescriptionDrugJobTemplates(com.serff.template.plan.PlanBenefitTemplateVO planBenefitTemplateVO, 
			com.serff.template.plan.PrescriptionDrugTemplateVO prescriptionDrugTemplateVO, List<String> errorCodes, List<String> errorMessages, List<String> processMessages){

		LOGGER.info("populateAndPersistPrescriptionDrugJobTemplates() : Start");
		EntityManager entityManager = null;
		boolean commitStatus = true;
		PlanUpdateStatistics planUpdateStatistics = null;
		
		try{
			//Create record in SERFF_PLAN_MGMT table
			Map<String, String> planIdAndFormularyIdMap = new HashMap<String, String>();
			String stateCode =  serffUtils.getGlobalStateCode();
			//This will do initial data check in both templates e.g. HIOS id matching etc. Also populate Map "Plan Id <--> FormularyName" e.g. "12312CA1234567 = CAF001".
			if(prescriptionDrugValidator.isValidPrescriptionDrugJobTemplates(stateCode, planBenefitTemplateVO, 
					prescriptionDrugTemplateVO, planIdAndFormularyIdMap, errorCodes, errorMessages)){
			
			//Validate Prescription drug template for valid data using validator.
			serffUtils.validatePrescriptionDrugJobTemplate(prescriptionDrugTemplateVO, errorCodes, errorMessages);
			
			if(CollectionUtils.isEmpty(errorCodes) && CollectionUtils.isEmpty(errorMessages)){
				//--------------------------------Begin Transaction--------------------------------
				entityManager = entityManagerFactory.createEntityManager();
				entityManager.getTransaction().begin();
				
				//Valid templates. Proceed with persistence.
				PlanAndBenefitsPackageVO planAndBenefitsPackageVO = planBenefitTemplateVO.getPackagesList().getPackages().get(0);
				String effectiveStartdate = planAndBenefitsPackageVO.getPlansList().getPlans().get(0).getPlanAttributes().getPlanEffectiveDate().getCellValue();
				int applicableYear = Integer.parseInt(effectiveStartdate.split("['/']")[SerffConstants.APPLICABLE_YEAR_INDEX]);
				LOGGER.info("populateAndPersistPrescriptionDrugJobTemplates() : effectiveStartdate :" + effectiveStartdate + " applicableYear : " + applicableYear);
				String issuerHiosIdInPlanTemplate = planAndBenefitsPackageVO.getHeader().getIssuerId().getCellValue();
				Map<String, List<com.getinsured.hix.model.Plan>> planIdAndExistingPlans = getExistingPlansForEachPlanId(planIdAndFormularyIdMap, applicableYear);
				
				//Persist prescription drug data.
				Issuer issuer = iIssuerRepository.getIssuerByHiosID(issuerHiosIdInPlanTemplate);
				if(null != issuer){
					LOGGER.info("populateAndPersistPrescriptionDrugJobTemplates() : fetching formulary list for issuer.getId() : " + issuer.getId());
					List<Formulary> existingFormularyList = iSerffFormularyRepository.findByIssuer_IdAndApplicableYear(issuer.getId(), applicableYear);
					//Save new formulary List.
					Map<String, Formulary> savedFormulary = planMgmtService.populateAndPersistPrescriptionDrugBean(applicableYear, entityManager, issuer, prescriptionDrugTemplateVO);
					//Merge existing Formularies present in db with newly saved formularies.
					savedFormulary = mergeExistingAndNewlySavedFormulary(existingFormularyList, savedFormulary, issuer);
					//Link Formulary id to plan.
					planUpdateStatistics = updatePlansWithFormularyId(planIdAndFormularyIdMap, savedFormulary, planIdAndExistingPlans, entityManager);
				}else{
					commitStatus = false;
					errorMessages.add(MSG_ISSUER_NOT_AVAILABLE + issuerHiosIdInPlanTemplate + ". ");
					LOGGER.error("populateAndPersistPrescriptionDrugJobTemplates() : issuer is null.");
				}
			}else{
				//As error messages added in message list in case of validation error no need to check here.
				commitStatus = false;
				LOGGER.error("populateAndPersistPrescriptionDrugTemplates() : Prescription Drug template data is not valid. Validation errors present.");
			}
		  }else{
				//Template data is invalid in sanity check.
				commitStatus = false;
				LOGGER.error("populateAndPersistPrescriptionDrugTemplates() : Prescription Drug template data is not valid. Validation failed in sanity check.");
		  }
			
		}catch(Exception e){
			commitStatus = false;
			processMessages.add(MSG_ERROR_OCCURRED + e.getMessage());
			LOGGER.error("populateAndPersistPrescriptionDrugTemplates() : Exception occurred : ", e);
		}finally{
			LOGGER.info("populateAndPersistPrescriptionDrugTemplates() : End");
			completeTransaction("populateAndPersistPrescriptionDrugTemplates() ", commitStatus, entityManager);
		}
		
		return planUpdateStatistics;
	}
	
	/**
	 * Finishes transaction on the basis of commitStatus.
	 * 
	 * @param commitStatus - If this is true transaction is committed else rolled back
	 * @param entityManager - Entity manager instance
	 */
	private void completeTransaction(String caller, boolean commitStatus, EntityManager entityManager){

		LOGGER.info("completeTransaction() : Start");
		
		if (null != entityManager && entityManager.isOpen()) {
			if (commitStatus) {
				entityManager.getTransaction().commit();
				LOGGER.info(caller + ": Transaction Commited.");
			} else {
				entityManager.getTransaction().rollback();
				LOGGER.error(caller + " : Transaction Rolledback.");
			}
			entityManager.clear();
			entityManager.close();
		} else  {
			LOGGER.error("Connection already closed");
		}
		
		LOGGER.info("completeTransaction() : End");
	}
	
	/**
	 * 	Merges existing formularies from db and new formularies saved.
	 * 
	 * @param existingFormularyList - Existing formulary list in db
	 * @param newlySavedFormularies - Saved formularies
	 * @param issuer - The issuer instance
	 * @param entityManager - Entity manager instance
	 * @return - Merged map of existing and newly saved formularies.
	 */
	private Map<String, Formulary> mergeExistingAndNewlySavedFormulary(List<Formulary> existingFormularyList, Map<String, Formulary> newlySavedFormularies, Issuer issuer){
		
		LOGGER.info("mergeExistingAndNewlySavedFormulary() : Start");
		
		if(!CollectionUtils.isEmpty(existingFormularyList)) {
			for (Formulary existingFormulary : existingFormularyList) {
				existingFormulary.setIssuer(issuer);
				LOGGER.debug("mergeExistingAndNewlySavedFormulary() : Existing Formulary : " + existingFormulary.getFormularyId());
				if(null == newlySavedFormularies.get(existingFormulary.getFormularyId())){
					LOGGER.info("mergeExistingAndNewlySavedFormulary() : Existing formulary not found in newly saved formulary. Formulary Id : " + existingFormulary.getFormularyId());
					newlySavedFormularies.put(existingFormulary.getFormularyId(), existingFormulary);
					LOGGER.debug("mergeExistingAndNewlySavedFormulary() : Existing Formulary Added in newly saved formulary : " + existingFormulary.getFormularyId());
				}
			}		
		}
		LOGGER.debug("mergeExistingAndNewlySavedFormulary() : savedFormulary : " + newlySavedFormularies);
		LOGGER.info("mergeExistingAndNewlySavedFormulary() : End");
		return newlySavedFormularies;
	}
		
	/**
	 * Fetches plans present in db for provided planid/componentid.
	 * 
	 * @param planIdAndFormularyIdMap - Map of plan id and formulary id. This is created by reading values from Plan template
	 * @param applicableYear - Plan applicable year
	 * @return - Map containing plan id and corresponding Plan list present in db
	 */
	private Map<String, List<com.getinsured.hix.model.Plan>> getExistingPlansForEachPlanId(Map<String, String> planIdAndFormularyIdMap, int applicableYear){
		
		LOGGER.info("getExistingPlansForEachPlanId() : Start");
		Map<String, List<com.getinsured.hix.model.Plan>> planIdAndExistingPlansMap = new HashMap<String, List<com.getinsured.hix.model.Plan>>();
		LOGGER.debug("getExistingPlansForEachPlanId() : planIdAndFormularyIdMap : " + planIdAndFormularyIdMap);
	
		if(!CollectionUtils.isEmpty(planIdAndFormularyIdMap)){
			
			for(String planId : planIdAndFormularyIdMap.keySet()){
				List<com.getinsured.hix.model.Plan> existingPlansForPlanId = iSerffPlanRepository.getHealthPlansByApplicableYearAndComponentId(planId, applicableYear);
				
				if(CollectionUtils.isEmpty(existingPlansForPlanId)){
					LOGGER.info("getExistingPlansForEachPlanId() : No Plan present in db for plan id : " + planId + " and applicable year : " + applicableYear);
				}else{
					planIdAndExistingPlansMap.put(planId, existingPlansForPlanId);
				}
			}
			
			LOGGER.info("getExistingPlansForEachComponentId() : End");
		}
		
		return planIdAndExistingPlansMap;
	}
	
	/**
	 * Updated plan in db with the new formulary id created after template processing.
	 * 
	 * @param planIdAndFormularyIdMap - Map of plan id and formulary id. This is created by reading values from Plan template
	 * @param savedFormulary - Formularies saved in DB by processing prescription drug template
	 * @param existingPlansForComponentId - Plan in db for provided planid/componentid
	 * @param entityManager - Entity manager instance
	 * @return - Plan update details in PlanUpdateStatistics instance
	 */
	private PlanUpdateStatistics updatePlansWithFormularyId(Map<String, String> planIdAndFormularyIdMap, Map<String, Formulary> savedFormulary, Map<String, List<com.getinsured.hix.model.Plan>> existingPlansForComponentId, EntityManager entityManager){
		
		LOGGER.info("updatePlansWithFormularyId() : Start");
		
		PlanUpdateStatistics planUpdateStatistics = new PlanUpdateStatistics();
		
		LOGGER.debug("updatePlansWithFormularyId() : planIdAndFormularyIdMap is null: " + CollectionUtils.isEmpty(planIdAndFormularyIdMap));
		LOGGER.debug("updatePlansWithFormularyId() : All formularies is null: " + CollectionUtils.isEmpty(savedFormulary));
		LOGGER.debug("updatePlansWithFormularyId() : Component id and variances is null: " + CollectionUtils.isEmpty(existingPlansForComponentId));
		
		for(Entry<String, List<com.getinsured.hix.model.Plan>> existingPlansEntry : existingPlansForComponentId.entrySet()) {
			
			List<com.getinsured.hix.model.Plan> existingPlans = existingPlansEntry.getValue();
			
			if(!CollectionUtils.isEmpty(existingPlans)){
				
				String formularyId = planIdAndFormularyIdMap.get(existingPlansEntry.getKey());
				Formulary formularyToUpdateInPlan = savedFormulary.get(formularyId);
				LOGGER.debug("ComponentId : " + existingPlansEntry.getKey() + " : FormularyName : " + formularyId + "Plans List : " + existingPlans.size());
				
				if(null != formularyToUpdateInPlan){
					
					for(com.getinsured.hix.model.Plan plan : existingPlans){
						LOGGER.debug("Plan Updated :" + plan.getIssuerPlanNumber() + " Formulary Id : " + formularyToUpdateInPlan.getFormularyId() + " Id : " + formularyToUpdateInPlan.getId());
						plan.setFormularlyId(String.valueOf(formularyToUpdateInPlan.getId()));
						entityManager.merge(plan);
						planUpdateStatistics.planUpdated(false);
					}
					//Update serff template for plan.
					updateSerffTemplateForPlan(existingPlans.get(0).getIssuerPlanNumber().substring(0, SerffConstants.LEN_HIOS_PLAN_ID), 
							existingPlans.get(0).getApplicableYear(), entityManager);
				}
			}
			
		}
		
		LOGGER.info("updatePlansWithFormularyId() : End");
		
		return planUpdateStatistics;
	}
	
	/**
	 * Updates last updated timestamp in serff_templates for the plans updated Formulary id. 
	 * 
	 * @param planId - Plan id whose formulary id is updated
	 * @param applicableyear - Applicable year to feetch plan from serff_templates table.
	 * @param entityManager - Entity manager instance.
	 */
	private void updateSerffTemplateForPlan(String planId, int applicableyear, EntityManager entityManager){
		
		LOGGER.info("updateSerffTemplateForPlan() : Start");
		//Find Serff template for this plan.
		SerffTemplates serffTemplateForPlan = iSerffTemplates.findByPlanIdAndApplicableYear(planId, applicableyear);
		if(null != serffTemplateForPlan){
			//serffTemplateForPlan.setPresDrugLastModifiedDate(new Date());//coming through serff templates.
			serffTemplateForPlan.setPresDrugLastUpdated(new Date());
			entityManager.merge(serffTemplateForPlan);
			LOGGER.info("updateSerffTemplateForPlan() : Serff template updated for standard component id : " + planId);
		}
		LOGGER.info("updateSerffTemplateForPlan() : End");
	}
	
	/**
	 * Uploads templates in ECM for the prescription drug job.
	 * 
	 * @param trackingRecord - Tracking record
	 * @param folderPath - FTP path where templates uploaded
	 * @param fileName - template file name
	 * @return - whether templates uploaded in ECM
	 * @throws IOException - exception while accessing FTP or templates 
	 */
	private boolean addPrescriptionDrugTemplateInECM(SerffPlanMgmt trackingRecord, String folderPath, String fileName, String templateName) throws IOException {
		
		LOGGER.info("addPrescriptionDrugTemplateInECM() Start.");
		boolean isSuccess = false;
		InputStream inputStream = null;

		try {
			inputStream = ftpClient.getFileInputStream(folderPath + fileName);

			if (null != inputStream) {
				LOGGER.debug("Uploading File Name with path to ECM:" + SerffConstants.SERF_ECM_BASE_PATH + FTP_PLAN_DOCS_FILE + fileName);
				// Uploading templates in ECM.
				SerffDocument attachment = serffUtils.addAttachmentInECM(ecmService, trackingRecord, SerffConstants.SERF_ECM_BASE_PATH, FTP_PLAN_DOCS_FILE, templateName + ".xml", IOUtils.toByteArray(inputStream), true);

				if (null != attachment) {
					serffService.saveSerffDocument(attachment);
					isSuccess = true;
				}
			}
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.info("addPrescriptionDrugTemplateInECM() End.");
		}
		return isSuccess;
	}

	/**
	 * Method is used to check waiting plans to load.
	 */
	@Override
	public boolean checkWaitingPlansToLoad() {

		boolean hasWaitingPlansToLoad = false;

		SerffPlanMgmtBatch waitingPlanBatch = null;
		SerffPlanMgmtBatch waitingPrescriptionDrugBatch = null;

		if (SerffConstants.POSTGRES.equalsIgnoreCase(SerffConstants.getDatabaseType())) {
			waitingPlanBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(
					SerffPlanMgmtBatch.BATCH_STATUS.WAITING.name(), SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.toString());
		}
		else {
			waitingPlanBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(
					SerffPlanMgmtBatch.BATCH_STATUS.WAITING, SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.toString());
		}

		if (null == waitingPlanBatch) {

			if (SerffConstants.POSTGRES.equalsIgnoreCase(SerffConstants.getDatabaseType())) {
				waitingPrescriptionDrugBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(
						SerffPlanMgmtBatch.BATCH_STATUS.WAITING.name(),
						SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.toString());
			}
			else {
				waitingPrescriptionDrugBatch = iSerffPlanMgmtBatch.getBatchByStatusAndOperationType(
						SerffPlanMgmtBatch.BATCH_STATUS.WAITING,
						SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.toString());
			}
		}

		if (null != waitingPlanBatch || null != waitingPrescriptionDrugBatch) {
			hasWaitingPlansToLoad = true;
		}
		return hasWaitingPlansToLoad;
	}
	
	private Map<STATUS_KEY, Object> processDrugsXMLLoadJob(SerffPlanMgmtBatch waitingDrugsXMLBatch) {
		
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("processDrugsXMLLoadJob() Start : batch id : " + waitingDrugsXMLBatch.getId());
		}
		
		Map<STATUS_KEY, Object> statusMap = new HashMap<STATUS_KEY, Object>();
		Map<STATUS_MESSAGE_KEY, String> statusMessagesMap = new HashMap<STATUS_MESSAGE_KEY, String>();
		List<Long> existingDrugListIds = null;
		List<Long> newDrugListIds = null;
		String diffReportEcmDocId = null;
		
		boolean processStatus = false;
		SerffPlanMgmt serffPlanMgmtRecord = null;
		Date startTime = new Date(System.currentTimeMillis());
		String folderPath = SerffConstants.getFTPUploadPlanPath() + waitingDrugsXMLBatch.getIssuerId() 
				+ waitingDrugsXMLBatch.getState() + waitingDrugsXMLBatch.getId() + "/";

		try{
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("processDrugsXMLLoadJob() : Start of Loading Prescription drug from location " + folderPath 
						+ ". Issuer HIOS ID : " + waitingDrugsXMLBatch.getIssuerId());
			}
			//Update row that job is picked up, set status to IN_PROGRESS
			updateBatchJobStatus(waitingDrugsXMLBatch, SerffPlanMgmtBatch.BATCH_STATUS.IN_PROGRESS, new java.sql.Date((startTime).getTime()), null);
			//Create record in SERFF_PLAN_MGMT table
			serffPlanMgmtRecord = serffUtils.createSerffPlanMgmtRecord(SerffConstants.TRANSFER_DRUGS_XML);
			serffPlanMgmtRecord = serffService.saveSerffPlanMgmt(serffPlanMgmtRecord);
			waitingDrugsXMLBatch.setSerffReqId(serffPlanMgmtRecord);
			
			Issuer issuer = iIssuerRepository.getIssuerByHiosID(waitingDrugsXMLBatch.getIssuerId());
		
			if (null != issuer) {
				serffPlanMgmtRecord.setIssuerId(String.valueOf(issuer.getId()));
				Map<String, String> fileMap = getAllTemplatesFilelist(folderPath, SerffConstants.DATA_PRESCRIPTION_DRUG);
				StringBuilder attachmentList = new StringBuilder();
				//Add templates in ECM.
				if(!CollectionUtils.isEmpty(fileMap) && fileMap.size() == 1){
					
					for(Entry<String, String> entryTemplate : fileMap.entrySet()){
						if(LOGGER.isDebugEnabled()) {
							LOGGER.debug("processDrugsXMLLoadJob() : Adding template in ECM : " + entryTemplate.getValue());
						}
						addPrescriptionDrugTemplateInECM(serffPlanMgmtRecord, folderPath, entryTemplate.getValue(), entryTemplate.getKey());
						attachmentList.append(entryTemplate.getKey() + ",");
					}
				
					//Set issuer id and template list in tracking record.
					serffPlanMgmtRecord.setAttachmentsList(attachmentList.toString());
					serffPlanMgmtRecord = serffService.saveSerffPlanMgmt(serffPlanMgmtRecord);
					Map<String, byte[]> uploadedPrescriptionDrugTemplatesMap = null;
					statusMap = new HashMap<STATUS_KEY, Object>();

					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("processDrugsXMLLoadJob() : Found " + fileMap.size() + " templates for loading drug list from location " + folderPath );
					}
					uploadedPrescriptionDrugTemplatesMap = getAllTemplatesForPlans(issuer, folderPath, fileMap, waitingDrugsXMLBatch.getState());

					Object prescriptionDrugTemplateVO = serffUtils.inputStreamToObject(new ByteArrayInputStream(uploadedPrescriptionDrugTemplatesMap.get(SerffConstants.DATA_PRESCRIPTION_DRUG)), 
						com.serff.template.plan.PrescriptionDrugTemplateVO.class);

					if(null != prescriptionDrugTemplateVO){
						List<String> errorCodes = new ArrayList<String>();
						List<String> errorMessages = new ArrayList<String>();
						List<String> processMessages = new ArrayList<String>();

						com.serff.template.plan.PrescriptionDrugTemplateVO drugTemplateVO = (com.serff.template.plan.PrescriptionDrugTemplateVO)prescriptionDrugTemplateVO;
						String drugsIssuerId = drugTemplateVO.getHeader().getIssuerId().getCellValue();
						// Validate HIOS Issuer ID
						if (null == drugsIssuerId || !waitingDrugsXMLBatch.getIssuerId().equals(drugsIssuerId)) {
							LOGGER.error("Issuer ID["+ drugsIssuerId +"] does not match with option selected Issuer ID: " + waitingDrugsXMLBatch.getIssuerId());
							errorMessages.add("Issuer ID["+ drugsIssuerId +"] does not match with option selected Issuer ID: " + waitingDrugsXMLBatch.getIssuerId() + ". ");
						}
						// Validate State Code
						String drugsStateCode = drugTemplateVO.getHeader().getStatePostalCode().getCellValue();
						if (null == drugsStateCode || !waitingDrugsXMLBatch.getState().equals(drugsStateCode)) {
							LOGGER.error("State Code ["+ drugsStateCode +"] does not match with option selected State Code : " + waitingDrugsXMLBatch.getState());
							errorMessages.add("State Code ["+ drugsStateCode +"] does not match with option selected State Code : " + waitingDrugsXMLBatch.getState() + ". ");
						}

						if (errorMessages.isEmpty()) {
							existingDrugListIds = new ArrayList<Long>();
							newDrugListIds = new ArrayList<Long>();
							//Process prescription drug templates.
							populateAndPersistDrugList(drugTemplateVO, issuer, waitingDrugsXMLBatch.getDefaultTenant(), errorCodes, errorMessages, 
									processMessages, existingDrugListIds, newDrugListIds);
						}
						
						//If errorCodes and errorMessages are not empty means process failed due to validation.
						if((!CollectionUtils.isEmpty(errorCodes)) || (!CollectionUtils.isEmpty(errorMessages))){
							LOGGER.error("processDrugsXMLLoadJob() : populateAndPersistDrugList finished with errors.");
							StringBuilder validationErrorString = new StringBuilder();
							for(String validationError : errorMessages){
								validationErrorString.append(validationError);
								validationErrorString.append(SerffConstants.NEW_LINE);
							}
							//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
							populateStatusMessagesMap(MSG_DRUG_LIST_VALIDATION_ERROR, validationErrorString.toString(), MSG_INVALID_REQUEST, 
									MSG_PROCESS_FAILED, statusMessagesMap);
						} else if(!CollectionUtils.isEmpty(processMessages)){ 
						//If processMessages is not empty means something failed after validation e.g. persistence exception.
							LOGGER.error("processDrugsXMLLoadJob() : populateAndPersistDrugList finished with exception.");
							//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
							populateStatusMessagesMap(MSG_DRUG_LIST_ERROR_OCCURRED, processMessages.get(0) , processMessages.get(0), 
									MSG_PROCESS_FAILED, statusMessagesMap);
						} else {
							processStatus = true;
							if(!CollectionUtils.isEmpty(existingDrugListIds) && !CollectionUtils.isEmpty(newDrugListIds)) {
								HSSFWorkbook workbook = null;
								workbook = formularyService.getFormularyDrugListDiffReport(existingDrugListIds, newDrugListIds);
								if(null != workbook) {
									diffReportEcmDocId = getEcmIdForDiffReport(workbook, serffPlanMgmtRecord, drugsIssuerId);
								}
							}
						}
						
					}else{
						LOGGER.error("processDrugsXMLLoadJob() : Incompatible template types found.");
						//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
						populateStatusMessagesMap(MSG_INCOMPATIBLE_TEMPLATES, MSG_INCOMPATIBLE_TEMPLATES, MSG_INVALID_REQUEST, 
								MSG_INVALID_REQUEST, statusMessagesMap);
					}
					
				}else{
					if(null != fileMap) {
						LOGGER.error("processDrugsXMLLoadJob() : Found " + fileMap.size() + " templates for loading drug list from location " + folderPath );
					}
					LOGGER.error("processDrugsXMLLoadJob() : Prescription DrugList Batch has been failed due to invalid template.");
					//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
					populateStatusMessagesMap(MSG_DRUGLIST_INVALID_TEMPLATE, MSG_DRUGLIST_INVALID_TEMPLATE, MSG_INVALID_REQUEST, 
							MSG_INVALID_REQUEST, statusMessagesMap);
				}
			
			} else {
				LOGGER.error("processDrugsXMLLoadJob() : Prescription DrugList Batch has been failed due to Issuer is not available with HIOS ID " + waitingDrugsXMLBatch.getIssuerId());
				//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
				populateStatusMessagesMap(MSG_DRUGLIST_ISSUER_NOT_AVAILABLE + waitingDrugsXMLBatch.getIssuerId(), MSG_DRUGLIST_ISSUER_NOT_AVAILABLE, MSG_INVALID_REQUEST, 
						MSG_INVALID_REQUEST, statusMessagesMap);
			}
		
		}catch(Exception ex){
			LOGGER.error("processDrugsXMLLoadJob() : Error while processing prescription DrugList templates: " + ex.getMessage(), ex);
			//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
			populateStatusMessagesMap(MSG_DRUG_LIST_ERROR_OCCURRED, MSG_DRUG_LIST_ERROR_OCCURRED, ex.getMessage(), 
					MSG_PROCESS_FAILED, statusMessagesMap);
		}finally{
			if(processStatus){
				LOGGER.info("processPrescriptionDrugLoadJob() End.");
				//Param sequence : Ui Message >> Response XML Message >> Process Description Message >> Remarks >> StatusMessagesMap
				populateStatusMessagesMap(MSG_PROCESS_COMPLETED_SUCCESS, MSG_PROCESS_COMPLETED_SUCCESS, MSG_PROCESS_COMPLETED_SUCCESS, 
						MSG_PROCESS_COMPLETED_SUCCESS, statusMessagesMap);
				updateBatchJobStatus(waitingDrugsXMLBatch, SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
			}else{
				updateBatchJobStatus(waitingDrugsXMLBatch, SerffPlanMgmtBatch.BATCH_STATUS.FAILED, null, new java.sql.Date((new Date(System.currentTimeMillis())).getTime()));
			}
			
			if (null != serffPlanMgmtRecord) {
				if(null != diffReportEcmDocId) {
					serffPlanMgmtRecord.setPmResponseXml(diffReportEcmDocId);
				}
				//Save tracking record.
   				persistTrackingRecord(serffPlanMgmtRecord, null, processStatus, statusMessagesMap);
			}
		}
	
		statusMap.put(STATUS_KEY.STATUS_MESSAGE, statusMessagesMap.get(STATUS_MESSAGE_KEY.UI_MESSAGE));	
		return statusMap;
	}


	/**
	 * Populates and persist drug list from prescription drug template.
	 * 
	 * @param prescriptionDrugTemplateVO - Prescription drug instance
	 * @param issuerObj 
	 * @param applicableYear 
	 * @param errorCodes  - Error codes list
	 * @param errorMessages - Error messages list
	 * @param processMessages - Process messages list
	 * @return - void
	 */
	private void populateAndPersistDrugList(com.serff.template.plan.PrescriptionDrugTemplateVO prescriptionDrugTemplateVO, 
			Issuer issuerObj, String applicableYear, List<String> errorCodes, List<String> errorMessages, List<String> processMessages, 
			List<Long> existingDrugListIds, List<Long> newDrugListIds){

		LOGGER.info("populateAndPersistDrugList() : Start");
		EntityManager entityManager = null;
		boolean commitStatus = false;
		//--------------------------------Begin Transaction--------------------------------
		entityManager = entityManagerFactory.createEntityManager();
		entityManager.getTransaction().begin();
		
		try{
			Map<String, Integer> formularyIdAndDrugListIdMap = new HashMap<String, Integer>();
			//Validate Prescription drug template for valid data using validator.
			serffUtils.validatePrescriptionDrugJobTemplate(prescriptionDrugTemplateVO, errorCodes, errorMessages);
			
			if(CollectionUtils.isEmpty(errorCodes) && CollectionUtils.isEmpty(errorMessages)){
				
				//Valid templates. Proceed with persistence.
				if(null != issuerObj){
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("populateAndPersistDrugList() : issuerHiosId :" + issuerObj.getHiosIssuerId() + " applicableYear : " + applicableYear);
					}
					//Persist prescription drug data.
					LOGGER.info("populateAndPersistDrugList() : fetching formulary list for issuer.getId() : " + issuerObj.getId());
					List<Formulary> existingFormularyList = iSerffFormularyRepository.findByIssuer_IdAndApplicableYear(issuerObj.getId(), Integer.valueOf(applicableYear));
					if(!CollectionUtils.isEmpty(existingFormularyList)) {
						List<FormularyVO> formularyVOs = prescriptionDrugTemplateVO.getFormularyList().getFormularyVO();
						Map<Integer, DrugList> drugListMap = null;
						Validate.notNull(formularyVOs, "No FORMULARY data is found for the request.");
						Validate.notEmpty(formularyVOs, "FORMULARY data is missing in the request.");
						for(FormularyVO formularyVO: formularyVOs) {
							if(null != formularyVO) {
								String fromularyId = formularyVO.getFormularyID().getCellValue();
								formularyIdAndDrugListIdMap.put(fromularyId, Integer.valueOf(formularyVO.getDrugListID().getCellValue().trim()));
							}
						}
						//Save new formulary List.
						if(!formularyIdAndDrugListIdMap.isEmpty()) {
							drugListMap = planMgmtService.loadDrugList(entityManager, issuerObj, prescriptionDrugTemplateVO);
						}
						
						if(null != drugListMap) {
							boolean mismatchFound = false;
							Map<Long, DrugList> drugListToSoftDelete = new HashMap<Long, DrugList>();
							List<Long> drugListIdToKeep = new ArrayList<Long>();

							for (Formulary formulary : existingFormularyList) {
								String formularyId = formulary.getFormularyId();
								Integer drugListId = formularyIdAndDrugListIdMap.get(formularyId);
								if(null != drugListId) {
									DrugList dl = drugListMap.get(drugListId);
									if(null != dl && formulary.getNoOfTiers() == dl.getDrugsTiers().size()) {
										//Add old DrugList to map for soft-deleting later after checking that it is not referenced anywhere else
										DrugList oldDrugList = formulary.getDrugList();
										drugListToSoftDelete.put(oldDrugList.getId(), oldDrugList);
										formulary.setDrugList(dl);
										entityManager.merge(formulary);
										if(!existingDrugListIds.contains(oldDrugList.getId())) {
											existingDrugListIds.add(oldDrugList.getId());
											newDrugListIds.add(dl.getId());
										}
									} else {
										//Either druglist is missing or contains different count of drugtier. Since we are not changing formulary, we can't change tier count
										errorMessages.add(MSG_DRUG_LIST_ERROR_OCCURRED + " Drug List details or Drug Tier count does not match with existing data in DB.");
										LOGGER.error("populateAndPersistDrugList() : Drug List details or Drug Tier count does not match with existing data in DB.");
										mismatchFound = true;
										break;
									}
								} else {
									//Since there is a formulary that is not present in new template, let's preserve its druglist as it is still being used
									drugListIdToKeep.add(formulary.getDrugList().getId());
									LOGGER.warn("populateAndPersistDrugList() : Skipping to update formulary as drug list not found. FormularyId: " + formularyId );
								}
							}
							if(!mismatchFound) {
								for (Long drugListId : drugListToSoftDelete.keySet()) {
									//soft-delete only if it is not marked to be preserved
									if(!drugListIdToKeep.contains(drugListId)) {
										DrugList oldDrugList = drugListToSoftDelete.get(drugListId); 
										oldDrugList.setIsDeleted(SerffConstants.YES_ABBR);
										entityManager.merge(oldDrugList);
									}
								}
								commitStatus = true;
							}
						}
					}else{
						errorMessages.add(MSG_DRUG_LIST_ERROR_OCCURRED + " Formulary data is not present in DB. ");
						LOGGER.error("populateAndPersistDrugList() : Formulary data is not present in DB.");
					}
				}else{
					errorMessages.add(MSG_DRUG_LIST_ERROR_OCCURRED + " Invalid parameters. ");
					LOGGER.error("populateAndPersistDrugList() : issuer is null.");
				}
			}else{
				//As error messages added in message list in case of validation error no need to check here.
				LOGGER.error("populateAndPersistDrugList() : Prescription Drug List template data is not valid. Validation errors present.");
			}
			
		}catch(Exception e){
			commitStatus = false;
			processMessages.add(MSG_ERROR_OCCURRED + e.getMessage());
			LOGGER.error("populateAndPersistDrugList() : Exception occurred : ", e);
		}finally{
			LOGGER.info("populateAndPersistDrugList() : End");
			completeTransaction("populateAndPersistDrugList() ", commitStatus, entityManager);
		}
	}
	
	private String getEcmIdForDiffReport(HSSFWorkbook workbook, SerffPlanMgmt serffPlanMgmtRecord, String issuerHiosId) throws GIException {

		String ecmDocId = null;

		try (final ByteArrayOutputStream bytesOut = new ByteArrayOutputStream()) {

			/*FileOutputStream outFile = new FileOutputStream("E:\\samplefile.xls");
			workbook.write(outFile);
			outFile.close();*/

	        workbook.write(bytesOut);
	        final byte[] byteArray = bytesOut.toByteArray();

			ecmDocId = serffResourceUtil.uploadDocumentAtECM(ecmService, serffPlanMgmtRecord, SerffConstants.SERF_ECM_BASE_PATH, SerffConstants.FTP_DRUG_LIST_DIFF_REPORT, 
					issuerHiosId + "-DrugListDiffReport-" + DateUtil.dateToString(new Date(), "MM-dd-yyyy") + "_" +serffPlanMgmtRecord.getSerffReqId() + ".xls", byteArray, false);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Added DrugList difference report to ECM for Record:" + serffPlanMgmtRecord.getSerffReqId() + ", ECM ID is : " + ecmDocId);
			}
		}
		catch (IOException ex) {
			LOGGER.error("Error occuer while uploading document at ECM", ex);
		}
		return ecmDocId;
	}
}
