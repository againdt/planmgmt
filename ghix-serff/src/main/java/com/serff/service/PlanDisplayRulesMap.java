package com.serff.service;

import static com.serff.util.SerffConstants.UNDERSCORE;

import java.util.Hashtable;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.PlanDisplayRules;
import com.serff.service.validation.SerffValidator;
import com.serff.util.SerffConstants;

/**
 * Class is used to adding code to Plan Display Rules.
 * 
 * @author Bhavin Parmar
 * @since November 25, 2014
 */
public class PlanDisplayRulesMap extends Hashtable<String, PlanDisplayRules> {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(PlanDisplayMappingImpl.class);
	private static final String EMSG_EMPTY_KEY = "Key is empty.";
	private static final String EMSG_KEY_NOT_AVAILABLE = "Plan Display Rules is not available for generated key, getting data from Default Key:=";
	private static final String EMSG_EMPTY_MAP = "Plan Display Rules Map is empty.";

	public static final String DEFAULT_HIOS_ID = "00000";
	private static final String DEFAULT_RULE_KEY = "Y_UNKNOWN_X_UNKNOWN";
	private static final String NULL_VALUE = "NULL";
	private static final String KEY_COINS_PERC = "Y";
	private static final String KEY_COPAY_AMT = "X";

	private static final Double HUNDRED = 100d;
	private static final Double ZERO = 0d;

	public PlanDisplayRulesMap() {
		super();
	}

	/**
	 * Method is used to get Plan Display Rules from Hashtable.
	 */
	public PlanDisplayRules getPlanDisplayRules(String coinsValue, String coinsAttr, String copayValue, String copayAttr) {

		LOGGER.debug("Beginning executing getPlanDisplayRules().");
		PlanDisplayRules planDisplayRules = null;

		try {

			// Generate Key from copay & coinsurnace data.
			String key = generateKey(coinsValue, coinsAttr, copayValue, copayAttr);

			if (StringUtils.isBlank(key)) {
				LOGGER.error(EMSG_EMPTY_KEY);
				return planDisplayRules;
			}

			if (!CollectionUtils.isEmpty(this)) {
				planDisplayRules = this.get(key);

				if (null == planDisplayRules) {
					LOGGER.warn(key + " " + EMSG_KEY_NOT_AVAILABLE + DEFAULT_RULE_KEY);
					planDisplayRules = this.get(DEFAULT_RULE_KEY);
				}

				if (null == planDisplayRules) {
					LOGGER.error("Default Key["+ DEFAULT_RULE_KEY +"] is not set PlanDisplayRules table.");
				}
			}
			else {
				LOGGER.error(EMSG_EMPTY_MAP);
			}
		}
		finally {
			LOGGER.debug("End execution getPlanDisplayRules(): Returning PlanDisplayRules is null: " + (null == planDisplayRules));
		}
		return planDisplayRules;
	}

	/**
	 * Method is used to generate Key from:=
	 * CoInsurance Value, CoInsurance Attribute, CoPay Value, CoPay Attribute
	 */
	public static String generateKey(String coinsValue, String coinsAttr, String copayValue, String copayAttr) {

		LOGGER.debug("Beginning executing generateKey().");
		StringBuilder key = null;

		try {

			SerffValidator serffValidator = new SerffValidator();
			String coinsValueKey = getCoinsuranceValueKey(coinsValue, serffValidator);

			String coinsAttrKey = getAttributeKey(coinsAttr);

			String copayValueKey = getCopayValueKey(copayValue, serffValidator);

			String copayAttrKey = getAttributeKey(copayAttr);
			LOGGER.debug("Coinsurance Value Key:" + coinsValueKey + " :: Coinsurance Attribute Key:" + coinsAttrKey +
					" :: Copay Value Key:" + copayValueKey + " :: Copay Attribute Key:" + copayAttrKey);

			if (StringUtils.isNotBlank(coinsValueKey) && StringUtils.isNotBlank(coinsAttrKey)
					&& StringUtils.isNotBlank(copayValueKey) && StringUtils.isNotBlank(copayAttrKey)) {
				key = new StringBuilder();
				key.append(coinsValueKey).append(UNDERSCORE);
				key.append(coinsAttrKey).append(UNDERSCORE);
				key.append(copayValueKey).append(UNDERSCORE);
				key.append(copayAttrKey);
			}
			else {
				LOGGER.error("Getting NULL value while generating Key.");
			}
		}
		finally {
			LOGGER.debug("End execution generateKey(): Final value of Key: " + key + " for Coins Value[" + coinsValue + "], Coins Attr[" + coinsAttr + "], Copay Value[" + copayValue + "] & Copay Attr[" + copayAttr + "]");
		}

		if (StringUtils.isNotBlank(key)) {
			return key.toString();
		}
		return null;
	}

	/**
	 * Method is used to get Key of Coinsurance Code Value.
	 */
	private static String getCoinsuranceValueKey(String coinsValue, SerffValidator serffValidator) {

		String coinsValueKey;

		if (StringUtils.isBlank(coinsValue) || coinsValue.equalsIgnoreCase(NULL_VALUE)) {
			coinsValueKey = NULL_VALUE;
		}
		else {
			Double value = serffValidator.getPercentageValueInDouble(coinsValue);

			if (null != value) {

				if (ZERO.equals(value)) {
					coinsValueKey = ZERO.toString();
				}
				else if (HUNDRED.equals(value)) {
					coinsValueKey = HUNDRED.toString();
				}
				else {
					coinsValueKey = KEY_COINS_PERC;
				}
			}
			else {
				coinsValueKey = KEY_COINS_PERC;
			}
		}
		return coinsValueKey.trim();
	}

	/**
	 * Method is used to get Key of Copay Code Value.
	 */
	private static String getCopayValueKey(String copayValue, SerffValidator serffValidator) {

		String copayValueKey;

		if (StringUtils.isBlank(copayValue) || copayValue.equalsIgnoreCase(NULL_VALUE)) {
			copayValueKey = NULL_VALUE;
		}
		else {
			Double value = serffValidator.getDollarValueInDouble(copayValue);

			if (null != value) {

				if (ZERO.equals(value)) {
					copayValueKey = ZERO.toString();
				}
				else {
					copayValueKey = KEY_COPAY_AMT;
				}
			}
			else {
				copayValueKey = KEY_COPAY_AMT;
			}
		}
		return copayValueKey;
	}

	/**
	 * Method is used to get Key of Attribute.
	 */
	private static String getAttributeKey(String attribute) {

		String coinsAttrKey;

		if (StringUtils.isBlank(attribute) || attribute.equalsIgnoreCase(NULL_VALUE)) {
			coinsAttrKey = NULL_VALUE;
		}
		else {
			coinsAttrKey = attribute.replaceAll(SerffConstants.SPACE, StringUtils.EMPTY).toUpperCase();
		}
		return coinsAttrKey.trim();
	}
}
