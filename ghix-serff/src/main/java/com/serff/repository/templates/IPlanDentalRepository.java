package com.serff.repository.templates;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.PlanDental;

@Repository("iSerffPlanDentalRepository")
public interface IPlanDentalRepository extends JpaRepository<PlanDental, Integer>{
	
	 @Query("SELECT planDental.id FROM PlanDental planDental WHERE planDental.plan.id = :planId ")
	 Integer getPlanDentalId(@Param("planId") Integer planId);
}