package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.RatingArea;

public interface IRatingAreaRepository extends JpaRepository<RatingArea, Integer>{


	 RatingArea findByRatingArea(String ratingArea);

	 List<RatingArea> findByState(String state);
}

