package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.DrugTier;

@Repository("iSerffDrugTierRepository")
public interface IDrugTierRepository extends JpaRepository<DrugTier, Integer> {
	List<DrugTier> findByDrugList_IdIn(List<Long> drugListIds);
}
