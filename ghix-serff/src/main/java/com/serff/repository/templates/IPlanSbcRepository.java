package com.serff.repository.templates;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.PlanSbcScenario;

public interface IPlanSbcRepository extends JpaRepository<PlanSbcScenario, Integer> {

}
