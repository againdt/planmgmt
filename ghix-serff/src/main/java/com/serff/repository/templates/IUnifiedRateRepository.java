package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.UnifiedRate;

public interface IUnifiedRateRepository extends
JpaRepository<UnifiedRate, Integer> {

	List<UnifiedRate> findByIssuerAndApplicableYearAndIsDeleted(Issuer issuer, int applicableYear, String noAbbr);	
	
}
