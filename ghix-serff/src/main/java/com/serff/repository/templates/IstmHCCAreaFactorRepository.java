package com.serff.repository.templates;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.STMHCCPlansAreaFactor;

public interface IstmHCCAreaFactorRepository extends JpaRepository<STMHCCPlansAreaFactor ,Integer>{

	@Query("SELECT a from STMHCCPlansAreaFactor a where a.isDeleted ='N'")
	List<STMHCCPlansAreaFactor> findAllAreaFactors();
	
	List<STMHCCPlansAreaFactor> findByCarrierHiosIdAndIsDeleted(String hiosId , String isDeleted);
	
	
	@Query("SELECT aF FROM STMHCCPlansAreaFactor aF where aF.carrierHiosId =:hiosId and aF.effectiveStartDate >=:newEffectiveStartDate AND aF.isDeleted='N'")
	List<STMHCCPlansAreaFactor> findAreaFactorAfterNewStartDate(@Param("hiosId") String hiosId, @Param("newEffectiveStartDate") Date newEffectiveStartDate);
	

	@Query("SELECT aF FROM STMHCCPlansAreaFactor aF where aF.effectiveStartDate < :newEffectiveStartDate AND aF.effectiveEndDate >=:newEffectiveStartDate AND aF.carrierHiosId =:hiosId AND aF.isDeleted='N'")
	List<STMHCCPlansAreaFactor> findAreaFactorBeforeNewStartDate(@Param("hiosId") String hiosId, @Param("newEffectiveStartDate") Date newEffectiveStartDate);
}
