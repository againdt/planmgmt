package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Tenant;

public interface ITenantRepository extends JpaRepository<Tenant, Integer>{
	 Tenant findByCode(String tenantCode);
	 @Query("SELECT t FROM Tenant t WHERE t.id IN (:tenantIdList)")
	 List<Tenant> getTenantInList(@Param("tenantIdList") List<Long> tenantIdList);
}
