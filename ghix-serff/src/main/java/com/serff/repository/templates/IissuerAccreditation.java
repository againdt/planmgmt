package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.IssuerAccreditation;

public interface IissuerAccreditation extends JpaRepository<IssuerAccreditation, Integer>{

	List <IssuerAccreditation> findByUracAppNumAndMarketTypeAndIssuer_Id(String uracAppNo , String marketType , int issuerId);
	
	List <IssuerAccreditation> findByNcqaOrgIdAndNcqaSubIdAndMarketTypeAndProductIdAndIssuer_Id(String ncqaOrgId , String ncqaSubId , String marketType , String productId , int issuerId);

	
}

