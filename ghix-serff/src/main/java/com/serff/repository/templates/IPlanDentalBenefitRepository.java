package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.PlanDentalBenefit;

@Repository("iSerffPlanDentalBenefitRepository")
public interface IPlanDentalBenefitRepository extends JpaRepository<PlanDentalBenefit, Integer> {
	
	@Query("SELECT p FROM PlanDentalBenefit p WHERE p.plan.id=:planDentalId")
	List<PlanDentalBenefit> findByPlanDentalId(@Param("planDentalId")  Integer planDentalId);
	
	@Query("SELECT pdb from PlanDentalBenefit pdb where "
			+ "pdb.networkT1display is null and pdb.networkT2display is null and pdb.outOfNetworkDisplay is null and pdb.networkT1TileDisplay is null " +
			" and pdb.plan.plan.isDeleted='N' and pdb.plan.plan.issuer.id=:issuerId")
	List<PlanDentalBenefit> findAllPlanDentalBenefitsByIssuerId(@Param("issuerId") Integer issuerId);
	
	@Query("SELECT pdb from PlanDentalBenefit pdb where " +
			" pdb.plan.plan.isDeleted='N' and (LOWER(pdb.plan.plan.issuerPlanNumber) LIKE LOWER(:issuerPlanNumber) || '%') and pdb.plan.plan.applicableYear=:applicableYear")
	List<PlanDentalBenefit> findAllPlanDentalBenefitsByIssuerPlanNumberLike(@Param("issuerPlanNumber") String issuerPlanNumber, @Param("applicableYear") Integer applicableYear);
	
	@Query("SELECT pdb from PlanDentalBenefit pdb where " +
			" pdb.plan.plan.isDeleted='N' and (LOWER(pdb.plan.plan.issuerPlanNumber) LIKE LOWER(:issuerPlanNumber) || '%') and pdb.name ='PRIMARY_VISIT' and pdb.plan.plan.applicableYear=:applicableYear")
	List<PlanDentalBenefit> findAllPlanDentalBenefitsByIssuerPlanNumberAndName(@Param("issuerPlanNumber") String issuerPlanNumber, @Param("applicableYear") Integer applicableYear);
}
