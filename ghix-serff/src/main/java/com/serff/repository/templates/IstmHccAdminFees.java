package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.getinsured.hix.model.STMHCCPlansAdminFees;

public interface IstmHccAdminFees extends JpaRepository<STMHCCPlansAdminFees ,Integer>{

	@Query("SELECT a from STMHCCPlansAdminFees a where a.isDeleted ='N'")
	List<STMHCCPlansAdminFees> findAllAdminFees();
	
}
