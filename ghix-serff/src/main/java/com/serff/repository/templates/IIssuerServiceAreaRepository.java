package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.IssuerServiceArea;

public interface IIssuerServiceAreaRepository extends JpaRepository<IssuerServiceArea, Integer>{

	@Query("FROM IssuerServiceArea sa where (LOWER(sa.issuerState) like lower(:issuerState) || '%') and sa.hiosIssuerId=:hiosIssuerId and sa.serffServiceAreaId=:serffServiceAreaId and sa.applicableYear = :applicableYear")
	List<IssuerServiceArea> findbyStateIssuerAndServAreaAndApplicableYear(@Param("issuerState") String issuerState,
			@Param("hiosIssuerId") String hiosIssuerId, @Param("serffServiceAreaId") String serffServiceAreaId,
			@Param("applicableYear") Integer applicableYear);

	@Query("FROM IssuerServiceArea sa where (LOWER(sa.issuerState) like lower(:issuerState) || '%') and sa.hiosIssuerId=:hiosIssuerId and sa.applicableYear = :applicableYear")
	List<IssuerServiceArea> findbyStateAndIssuerAndApplicableYear(@Param("issuerState") String issuerState,
			@Param("hiosIssuerId") String hiosIssuerId, @Param("applicableYear") Integer applicableYear);
}
