package com.serff.repository.templates;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.TenantPlan;

public interface ITenantPlanRepository extends JpaRepository<TenantPlan, Integer>{
	
}
