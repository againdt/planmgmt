package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ExtWSCallLogs;

/**
 * Interface is used provide JpaRepository of table ExtWSCallLogs.
 * @author Bhavin Parmar
 */
public interface IExtWSCallLogsRepository extends JpaRepository<ExtWSCallLogs, Integer> {

	List<ExtWSCallLogs> findByParentReqIdOrId(int parentReqId, int id);

    @Query("SELECT extWSCallLogs.request FROM ExtWSCallLogs extWSCallLogs WHERE extWSCallLogs.id = :id")
	String getQuotitRequest(@Param("id") Integer id);

    @Query("SELECT extWSCallLogs.response FROM ExtWSCallLogs extWSCallLogs WHERE extWSCallLogs.id = :id")
	String getQuotitResponse(@Param("id") Integer id);
}
