package com.serff.repository.templates;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.STMHCCPlansRate;

public interface IstmHccRepository extends JpaRepository<STMHCCPlansRate ,Integer>{
	
	List<STMHCCPlansRate> findByPlanIdAndIsDeleted(Integer PlanID , String IsDeleted);
	
	@Query("SELECT pr FROM STMHCCPlansRate pr where pr.startDate >= :newEffectiveStartDate AND pr.planId = :planId AND pr.isDeleted='N'")
	List<STMHCCPlansRate> findRatesInFutureForEffectiveStartDate(@Param("planId") int planId, @Param("newEffectiveStartDate") Date newEffectiveStartDate);
	
	@Query("SELECT pr FROM STMHCCPlansRate pr where pr.startDate < :newEffectiveStartDate AND pr.endDate >= :newEffectiveStartDate AND pr.planId = :planId AND pr.isDeleted='N'")
	List<STMHCCPlansRate> findRatesBeforeEffectiveStartDate(@Param("planId") int planId, @Param("newEffectiveStartDate") Date newEffectiveStartDate);
}
