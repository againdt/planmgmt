/**
 * File 'IFormularyRepository' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 18, 2013 6:33:34 PM
 */
package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.Formulary;

/**
 * Repository class 'IFormularyRepository' to handle persistence of 'Formulary' records.
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 18, 2013 6:33:34 PM
 */
@Repository("iSerffFormularyRepository")
public interface IFormularyRepository extends JpaRepository<Formulary, Integer> {
	
	List<Formulary> findByIssuer_IdAndFormularyIdAndApplicableYear(int issuerId, String formularyId, Integer applicableYear);
	List<Formulary> findByIssuer_IdAndApplicableYear(int issuerId, Integer applicableYear);
}
