package com.serff.repository.templates;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.PlanDocumentsJob;

/**
 * This class is repository for PlanDocumentsJob entity.
 * 
 * @author vardekar_s
 * 
 */
public interface IPlanDocumentsJobRepository extends
		JpaRepository<PlanDocumentsJob, Integer> {

}
