/**
 * 
 */
package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.serff.PufHealthVO;

/**
 * @author sharma_va
 *
 */
public interface IPufHealthRepository extends JpaRepository<PufHealthVO , Integer>{
	List <PufHealthVO> findBystateAndIsDelete(String state, String isDelete);
    @Query("SELECT p FROM PufHealthVO p WHERE (LOWER(p.planIdStandardComponent) LIKE LOWER(:planId) || '%') AND p.isDelete='N'")
    List <PufHealthVO> getPlanDetailsForPlanID(@Param("planId") String planId);
    @Query("SELECT distinct p.county FROM PufHealthVO p WHERE (LOWER(p.planIdStandardComponent) LIKE LOWER(:hiodId) || '%') AND p.isDelete='N'")
    List <String> getServiceAreaDetailsForIssuer(@Param("hiodId") String hiodId);
}
