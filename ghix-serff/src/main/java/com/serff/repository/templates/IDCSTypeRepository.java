/**
 * File 'IDCSTypeRepository' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:41:42 AM
 */
package com.serff.repository.templates;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.DCSType;

/**
 * Class 'IDCSTypeRepository' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:41:42 AM
 *
 */
public interface IDCSTypeRepository extends JpaRepository<DCSType, Integer> {
}
