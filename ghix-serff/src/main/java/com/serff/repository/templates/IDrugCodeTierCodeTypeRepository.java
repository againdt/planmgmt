/**
 * File 'IDrugCodeTierCodeTypeRepository' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:33:42 AM
 */
package com.serff.repository.templates;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.DrugCodeTierCodeType;

/**
 * Class 'IDrugCodeTierCodeTypeRepository' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:33:42 AM
 *
 */
public interface IDrugCodeTierCodeTypeRepository extends JpaRepository<DrugCodeTierCodeType, Integer> {
}
