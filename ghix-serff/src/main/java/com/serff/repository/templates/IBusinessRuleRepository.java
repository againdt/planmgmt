package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.BusinessRule;
import com.getinsured.hix.model.Issuer;

public interface IBusinessRuleRepository extends
		JpaRepository<BusinessRule, Integer> {
	List<BusinessRule> findByIssuerAndIsObsoleteAndApplicableYear(Issuer issuer, String isObsolete, Integer applicableYear);
}
