/**
 * File 'IDCSTierTypeRepository' to .
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:23:03 AM
 */
package com.serff.repository.templates;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.DCSTierType;

/**
 * Repository class 'IDCSTierTypeRepository' to handle persistence of 'DCSTierType' records.
 *
 * @author chalse_v
 * @version 1.0
 * @since Apr 19, 2013 10:23:03 AM
 *
 */
public interface IDCSTierTypeRepository extends JpaRepository<DCSTierType, Integer> {
}
