package com.serff.repository.templates;
/**
 * Repository class for PlanBenefitEdit
 */
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.PlanBenefitEdit;

@Repository("iSerffPlanBenefitEditRepository")
public interface IPlanBenefitEditRepository extends JpaRepository<PlanBenefitEdit, Integer>, RevisionRepository<PlanBenefitEdit, Integer, Integer> {
	
	@Query("SELECT pbe FROM PlanBenefitEdit pbe WHERE pbe.applicableYear=:applicableYear and pbe.planHiosId=:planHiosId")
	List<PlanBenefitEdit> getBenefitsEditedByPlanHiosId(@Param("applicableYear") String applicableYear, @Param("planHiosId") String planHiosId);
	
	@Query("select distinct pbe.applicableYear from PlanBenefitEdit pbe")
	List<String> getAllApplicableYear();
	
	@Query("SELECT distinct pbe.planHiosId FROM PlanBenefitEdit pbe , Plan p where p.issuerPlanNumber = pbe.planHiosId and p.isDeleted='N' and p.id<>pbe.plan.id and pbe.applicableYear= p.applicableYear||'' and pbe.applicableYear=:applicableYear")
	List<String> getAllPlansForApplicableYear(@Param("applicableYear") String applicableYear);
	
}
