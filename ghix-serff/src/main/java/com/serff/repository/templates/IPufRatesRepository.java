/**
 * 
 */
package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.serff.PUFPlanRates;

/**
 * @author sharma_va
 *
 */
public interface IPufRatesRepository extends JpaRepository<PUFPlanRates , Integer>{
    @Query("SELECT distinct p.planIdStandardComponent, p.ratingArea, p.age, p.premium, p.tobaccoPremium FROM PUFPlanRates p WHERE (LOWER(p.planIdStandardComponent) LIKE LOWER(:planId) || '%') AND p.premium is not null order by p.age")
    List <Object[]> getPlanRatesForIssuerPlanId(@Param("planId") String planId);
}
