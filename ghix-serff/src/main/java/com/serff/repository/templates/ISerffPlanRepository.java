package com.serff.repository.templates;

import static com.serff.util.SerffConstants.PUF_DATASOURCE;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;

public interface ISerffPlanRepository extends JpaRepository<Plan, Integer>, RevisionRepository<Plan, Integer, Integer> {
    @Query("SELECT p FROM Plan p WHERE p.issuer.id=:issuerID and p.issuerPlanNumber=:issuerPlanNumber")
    Plan getRequiredPlan(@Param("issuerID") int issuerID,@Param("issuerPlanNumber") String issuerPlanNumber);
    
    @Query("SELECT p FROM Plan p WHERE p.issuerPlanNumber=:HIOSID")
    Plan getPlanByHIOSID(@Param("HIOSID") String hiosId);
    
    List<Plan> findByStateAndIsDeleted(String state, String isDeleted);
    
    Plan findByIssuerPlanNumberAndIsDeletedAndApplicableYear(String issuerPlanNumber, String isDeleted, Integer applicableYear);
    
    Plan findByIssuer_IdAndIssuerPlanNumberAndIsDeletedAndApplicableYear(int issuerId, String issuerPlanNumber, String isDeleted, Integer applicableYear);
    
    List<Plan> findByIssuer_IdAndIsDeletedAndApplicableYear(int issuerId, String isDeleted, Integer applicableYear);
    
    @Query("SELECT plan FROM Plan plan WHERE plan.issuer.id=:issuerId AND plan.insuranceType=:insuranceType AND plan.planMedicare.cmsIsSnp=:cmsIsSnp AND plan.isDeleted=:isDeleted AND plan.applicableYear=:applicableYear")
    List<Plan> findByIssuerIdAndInsuranceTypeAndCmsIsSnpAndIsDeletedAndApplicableYear(@Param("issuerId") Integer issuerId,
    		@Param("insuranceType") String insuranceType, @Param("cmsIsSnp") String cmsIsSnp, @Param("isDeleted") String isDeleted,
    		@Param("applicableYear") Integer applicableYear);
    
    @Query("SELECT distinct plan.issuer.id FROM Plan plan WHERE plan.planMedicare.dataSource='"+ PUF_DATASOURCE +"' AND plan.insuranceType=:insuranceType AND plan.planMedicare.cmsIsSnp=:cmsIsSnp AND plan.isDeleted=:isDeleted AND plan.applicableYear=:applicableYear")
    List<Integer> findByInsuranceTypeAndCmsIsSnpAndIsDeletedAndApplicableYear(@Param("insuranceType") String insuranceType,
    		@Param("cmsIsSnp") String cmsIsSnp, @Param("isDeleted") String isDeleted, @Param("applicableYear") Integer applicableYear);
    
	List<Plan> findByIssuerPlanNumberStartingWithAndIsDeletedAndApplicableYear(String planNumber, String isDeleted, Integer applicableYear);
	
    List<Plan> findByIssuerPlanNumberStartingWithAndIsDeletedAndApplicableYearAndInsuranceType(String planNumber, String isDeleted, Integer applicableYear, String insuranceType);
    
    List<Plan> findByIssuerPlanNumberAndIsDeletedAndApplicableYearAndInsuranceType(String issuerPlanNumber, String isDeleted, Integer applicableYear, String insuranceType);
    
    List<Plan> findByIssuerAndIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYearAndInsuranceType(Issuer issuer,  String isDeleted, String planId, Integer applicableYear, String insuranceType);
    
    List<Plan> findByIssuerAndIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYearAndStatus(Issuer issuer,  String isDeleted, String planId, Integer applicableYear, String status);

    @Query("SELECT COUNT(p) FROM Plan p WHERE (LOWER(p.issuerPlanNumber) LIKE LOWER(:planId) || '%') AND p.isDeleted='N' AND p.applicableYear=:applicableYear")
	Long getPlanCountWithApplicableYear(@Param("planId") String planId, @Param("applicableYear") Integer applicableYear);
    
	List<Plan> findByIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYear(@Param("isDeleted") String isDeleted,
			@Param("issuerPlanNumber") String issuerPlanNumber,
			@Param("applicableYear") Integer applicableYear);
	
	@Query("SELECT p FROM Plan p WHERE p.issuerPlanNumber=:issuerPlanNumber AND p.isDeleted =:isDeleted")
	Plan getPlanIdbyIssuerPlanNumberAndIsDeleted(@Param("issuerPlanNumber") String issuerPlanNumber , @Param("isDeleted") String isDeleted);
	
	@Query("SELECT p FROM Plan p WHERE (LOWER(p.issuerPlanNumber) LIKE LOWER(:componentId)  || '%') AND p.insuranceType='HEALTH' AND p.isDeleted='N' AND p.applicableYear=:applicableYear")
    List<Plan> getHealthPlansByApplicableYearAndComponentId(@Param("componentId") String componentId , @Param("applicableYear") Integer applicableYear);

	@Query("SELECT count(p.id) from Plan p WHERE p.issuer.id =:issuerId AND p.isDeleted =:isDeleted" )
	Long getPlanCountForIssuerId(@Param("issuerId")Integer issuerId, @Param("isDeleted") String isDeleted);

	@Query("SELECT plan FROM Plan plan where plan.startDate >= :newStartDate AND plan.issuerPlanNumber = :issuerPlanNumber AND plan.isDeleted = 'N' AND plan.applicableYear = :applicableYear AND plan.insuranceType = :insuranceType")
	List<Plan> findFuturePlansForStartDate(@Param("issuerPlanNumber") String issuerPlanNumber, @Param("newStartDate") Date newStartDate, @Param("applicableYear") Integer applicableYear, @Param("insuranceType") String insuranceType);

	@Query("SELECT plan FROM Plan plan where plan.startDate < :newStartDate AND plan.endDate >= :newStartDate AND plan.issuerPlanNumber = :issuerPlanNumber AND plan.isDeleted = 'N' AND plan.applicableYear = :applicableYear AND plan.insuranceType = :insuranceType")
	List<Plan> findOverlappingPlanForStartDate(@Param("issuerPlanNumber") String issuerPlanNumber, @Param("newStartDate") Date newStartDate, @Param("applicableYear") Integer applicableYear, @Param("insuranceType") String insuranceType);
	
	@Query("SELECT distinct SUBSTRING(p.issuerPlanNumber, 1, 14), p.formularlyId FROM Plan p WHERE p.issuer.id = :issuerId AND p.applicableYear = :applicableYear AND p.isDeleted ='N' AND p.insuranceType='HEALTH'")
	List<Object[]> getPlanFormularyDetails(@Param("issuerId")Integer issuerId, @Param("applicableYear") Integer applicableYear);
}
