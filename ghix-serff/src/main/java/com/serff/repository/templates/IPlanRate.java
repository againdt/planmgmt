package com.serff.repository.templates;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanRate;
import com.getinsured.hix.model.RatingArea;

public interface IPlanRate extends JpaRepository<PlanRate, Integer> {

	PlanRate findByRatingAreaAndPlanAndMinAgeAndMaxAgeAndTobaccoAndEffectiveStartDateAndEffectiveEndDateAndIsDeleted(RatingArea ratingArea, Plan plan, int minAge, int maxAge, String tobacco, Date effectiveStartDate, Date effectiveEndDate, String isDeleted);

	List<PlanRate> findByPlan(Plan plan);
	
	List<PlanRate> findByPlan_IdAndIsDeleted(int planId, String isDeleted);
	
	@Query("SELECT pr FROM PlanRate pr where pr.effectiveStartDate >= :newEffectiveStartDate AND pr.plan.id = :planId AND pr.isDeleted='N'")
	List<PlanRate> findRatesInFutureForEffectiveStartDate(@Param("planId") int planId, @Param("newEffectiveStartDate") Date newEffectiveStartDate);
	
	@Query("SELECT pr FROM PlanRate pr where pr.effectiveStartDate < :newEffectiveStartDate AND pr.effectiveEndDate >= :newEffectiveStartDate AND pr.plan.id = :planId AND pr.isDeleted='N'")
	List<PlanRate> findRatesBeforeEffectiveStartDate(@Param("planId") int planId, @Param("newEffectiveStartDate") Date newEffectiveStartDate);
}
