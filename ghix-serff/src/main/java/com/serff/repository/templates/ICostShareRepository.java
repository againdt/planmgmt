/**
 * File 'ICostShareRepository.java' for persisting COST_SHARE records.
 *
 * @author chalse_v
 * @since Jun 14, 2013
 *
 */
package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.CostShare;
import com.getinsured.hix.model.Formulary;

/**
 * Repository 'ICostShareRepository' for persisting CostShare data.
 *
 * @author chalse_v
 * @version 1.0
 * @since Jun 14, 2013
 *
 */
public interface ICostShareRepository extends JpaRepository<CostShare, Integer> {
	List<CostShare> findByFormularyAndIsDeleted(Formulary formulary, String isDeleted);
}
