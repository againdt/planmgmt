package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Network;

public interface INetworkRepository extends JpaRepository<Network, Integer> {

	@Query("SELECT n FROM Network n where n.issuer.id = :issuerId and n.applicableYear = :applicableYear")
    List<Network> getProviderNetworkByIssuerIdAndApplicableYear(@Param("issuerId") int issuerId, @Param("applicableYear") Integer applicableYear);
	
	@Query("SELECT n FROM Network n where n.issuer.id = :issuerId and n.networkID = :networkId and n.applicableYear = :applicableYear")
	List<Network> getProviderNetworkByIssuerIdNetworkIdApplicableYear(@Param("issuerId") int issuerId,
			@Param("networkId") String networkId, @Param("applicableYear") Integer applicableYear);
}
