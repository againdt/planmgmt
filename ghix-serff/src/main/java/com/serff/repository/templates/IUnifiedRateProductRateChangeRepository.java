package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.URRTProductRateChange;

public interface IUnifiedRateProductRateChangeRepository  extends
JpaRepository<URRTProductRateChange, Integer> {

	@Query("SELECT product FROM UnifiedRate unifiedRate, URRTProductRateChange product WHERE unifiedRate=product.unifiedRate " +
			"AND (LOWER(product.hiosInsuranceProductIdentifier) LIKE LOWER(:hiosInsuranceProductIdentifier) || '%') " +
			"AND unifiedRate.applicableYear=:applicableYear " +
			"AND product.isObsolete='N'")
	List<URRTProductRateChange> getProductRateChangeListByApplicableYear(@Param("hiosInsuranceProductIdentifier") String hiosInsuranceProductIdentifier, @Param("applicableYear") int applicableYear);
	
}
