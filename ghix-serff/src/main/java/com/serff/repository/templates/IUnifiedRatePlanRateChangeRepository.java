package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.URRTInsurancePlanRateChange;

public interface IUnifiedRatePlanRateChangeRepository extends
JpaRepository<URRTInsurancePlanRateChange, Integer> {
	
	@Query("SELECT rateChange FROM URRTInsurancePlanRateChange rateChange WHERE (LOWER(rateChange.basedInsurancePlanIdentifier) LIKE LOWER(:basedInsurancePlanIdentifier) || '%') AND rateChange.isObsolete=:isObsolete ")
	List<URRTInsurancePlanRateChange> getInsuranceRateChangeList(@Param("basedInsurancePlanIdentifier") String basedInsurancePlanIdentifier, @Param("isObsolete") String isObsolete);
	
	@Query("SELECT planRateChange FROM UnifiedRate unifiedRate, URRTProductRateChange productRateChange, URRTInsurancePlanRateChange planRateChange "
			+ "WHERE unifiedRate=productRateChange.unifiedRate "
			+ "and productRateChange=planRateChange.unifiedProductRateChange "
			+ "and (LOWER(planRateChange.basedInsurancePlanIdentifier) LIKE LOWER(:basedInsurancePlanIdentifier) || '%') "
			+ "and unifiedRate.applicableYear=:applicableYear "
			+ "and planRateChange.isObsolete='N'")
	List<URRTInsurancePlanRateChange> getInsuranceRateChangeListByApplicableYear(@Param("basedInsurancePlanIdentifier") String basedInsurancePlanIdentifier, @Param("applicableYear") int applicableYear);
}