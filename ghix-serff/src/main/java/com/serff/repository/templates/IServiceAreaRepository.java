package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.IssuerServiceArea;
import com.getinsured.hix.model.IssuerServiceAreaExt;
import com.getinsured.hix.model.ServiceArea;

public interface IServiceAreaRepository extends JpaRepository<ServiceArea, Integer>{

	List<ServiceArea> findByServiceAreaIdAndServiceAreaExtIdAndIsDeleted(IssuerServiceArea issuerServiceArea, IssuerServiceAreaExt issuerServiceAreaExt, String isDeleted);

}
