package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.PlanHealthBenefit;

@Repository("iSerffPlanHealthBenefitRepository")
public interface IPlanHealthBenefitRepository extends JpaRepository<PlanHealthBenefit, Integer> {

	@Query("SELECT p FROM PlanHealthBenefit p WHERE p.plan.id=:planHealthId")
	List<PlanHealthBenefit> findByPlanHealthId(@Param("planHealthId") Integer planHealthId);

	@Query("SELECT phb from PlanHealthBenefit phb where "
			+ "phb.networkT1display is null and phb.networkT2display is null and phb.outOfNetworkDisplay is null and phb.networkT1TileDisplay is null " +
			" and phb.plan.plan.isDeleted='N' and phb.plan.plan.issuer.id=:issuerId")
	List<PlanHealthBenefit> findAllPlanHealthBenefitsByIssuerId(@Param("issuerId") Integer issuerId);

	@Query("SELECT phb from PlanHealthBenefit phb where " +
			" phb.plan.plan.isDeleted='N' and (LOWER(phb.plan.plan.issuerPlanNumber) LIKE LOWER(:issuerPlanNumber) || '%') and phb.plan.plan.applicableYear=:applicableYear")
	List<PlanHealthBenefit> findAllPlanHealthBenefitsByIssuerPlanNumberLike(@Param("issuerPlanNumber") String issuerPlanNumber, @Param("applicableYear") Integer applicableYear);
	
	@Query("SELECT phb from PlanHealthBenefit phb where " +
			" phb.plan.plan.isDeleted='N' and (LOWER(phb.plan.plan.issuerPlanNumber) LIKE LOWER(:issuerPlanNumber) || '%') and phb.name ='PRIMARY_VISIT' and phb.plan.plan.applicableYear=:applicableYear")
	List<PlanHealthBenefit> findAllPlanHealthBenefitsByIssuerPlanNumberAndName(@Param("issuerPlanNumber") String issuerPlanNumber, @Param("applicableYear") Integer applicableYear);
}
