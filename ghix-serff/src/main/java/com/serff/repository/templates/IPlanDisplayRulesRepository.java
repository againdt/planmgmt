package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanDisplayRules;

/**
 * Interface is used provide JpaRepository of table PlanDisplayRules.
 * 
 * @author Bhavin Parmar
 * @since November 27, 2014
 */
public interface IPlanDisplayRulesRepository extends JpaRepository<PlanDisplayRules, Integer> {

	List<PlanDisplayRules> findByHiosIssuerIdAndIsActive(String hiosIssuerId, String isActive);
	
	@Query("SELECT p FROM PlanDisplayRules p WHERE p.id=:ruleId")
	PlanDisplayRules findByPlanDisplayRulesId(@Param("ruleId")Long ruleId);

	@Query("SELECT distinct p.hiosIssuerId FROM PlanDisplayRules p WHERE p.isActive='Y' ORDER BY p.hiosIssuerId")
	List<String> getIssuerIdsHavingActiveDisplayRules();
}
