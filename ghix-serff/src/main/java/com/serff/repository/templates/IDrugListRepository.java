package com.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.DrugList;
import com.getinsured.hix.model.Issuer;

public interface IDrugListRepository extends JpaRepository<DrugList, Integer> {
	List<DrugList> findByIssuerAndIsDeleted(Issuer issuer, String isDeleted);
}
