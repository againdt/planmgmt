package com.serff.repository.templates;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanAMEPremiumFactor;

/**
 * Interface is used provide JpaRepository of table PlanAMEPremiumFactor.
 * @author Bhavin Parmar
 */
public interface IPlanAMEPremiumFactorRepository extends JpaRepository<PlanAMEPremiumFactor, Integer> {
	
	@Query("SELECT planAMEPremiumFactor FROM PlanAMEPremiumFactor planAMEPremiumFactor WHERE planAMEPremiumFactor.issuerHiosId=:issuerHiosId AND planAMEPremiumFactor.state=:state AND planAMEPremiumFactor.isDeleted='N'")
	PlanAMEPremiumFactor getIssuerHiosIdAndState(@Param("issuerHiosId") String issuerHiosId, @Param("state") String state);
}
