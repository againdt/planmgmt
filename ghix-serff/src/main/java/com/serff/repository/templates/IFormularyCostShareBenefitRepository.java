/**
 * File 'IFormularyCostShareBenefitRepository.java' for persisting FORMULARY_COST_SHARE_BENEFIT records.
 *
 * @author chalse_v
 * @since Jun 14, 2013
 *
 */
package com.serff.repository.templates;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Formulary;
import com.getinsured.hix.model.FormularyCostShareBenefit;

/**
 * Repository 'IFormularyCostShareBenefitRepository' for persisting FormularyCostShareBenefit data.
 *
 * @author chalse_v
 * @version 1.0
 * @since Jun 14, 2013
 *
 */
public interface IFormularyCostShareBenefitRepository extends JpaRepository<FormularyCostShareBenefit, Integer> {
	List<FormularyCostShareBenefit> findByFormularyAndIsDeleted(Formulary formulary, String isDeleted);
}