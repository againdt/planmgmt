package com.serff.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.serff.SerffDocument;

/**
 * Interface is used provide JpaRepository of table SERFF_DOCUMENT.
 * @author Bhavin Parmar
 */
@Repository
public interface ISerffDocument extends JpaRepository<SerffDocument, Long> {

}
