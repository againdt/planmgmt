package com.serff.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.serff.SerffTemplates;

/**
 * Interface is used provide JpaRepository of table SERFF_TEMPLATES.
 * @author Geetha Chandran
 * @since 2-Aug-2013
 */
@Repository
public interface ISerffTemplates extends JpaRepository<SerffTemplates, Long> {

	SerffTemplates findByPlanIdAndApplicableYear(String planId, Integer applicableYear);
}
