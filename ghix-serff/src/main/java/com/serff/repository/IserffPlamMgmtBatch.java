/**
 * 
 */
package com.serff.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;

/**
 * Interface is used provide JpaRepository of table SERFF_PLAN_MGMT_BATCH.
 * @author Vani Sharma
 *
 */
@Repository
public interface IserffPlamMgmtBatch extends JpaRepository< SerffPlanMgmtBatch, Long> {
	//Get record where STATUS matches the passed in Status and there is no other batch with STATUS = IN_PROGRESS or IN_QUEUE 
	//for the same issuer which is not deleted and have Batch Start time falling in last 24 hours.
	//This ensure that only one Job per Issuer is processed at a time
    @Query("SELECT b FROM SerffPlanMgmtBatch b WHERE b.batchStatus=:batchStatus and b.isDeleted='N' and rownum<=1 " +
    		"and b.operationType=:operationType and b.issuerId NOT IN (SELECT r.issuerId FROM SerffPlanMgmtBatch r WHERE r.batchStatus IN ('IN_PROGRESS', 'IN_QUEUE') and r.isDeleted='N' and r.batchStartTime > SYSDATE-1)")
    SerffPlanMgmtBatch getBatchByStatusAndOperationType(@Param("batchStatus") SerffPlanMgmtBatch.BATCH_STATUS batchStatus, @Param("operationType") String operationType);

    @Query(value="SELECT * FROM Serff_Plan_Mgmt_Batch b WHERE b.batch_Status=:batchStatus and b.is_Deleted='N' " +
    		"and b.operation_Type=:operationType and b.issuer_Id NOT IN (SELECT r.issuer_Id FROM Serff_Plan_Mgmt_Batch r "
    		+ "WHERE r.batch_Status IN ('IN_PROGRESS', 'IN_QUEUE') and r.is_Deleted='N' and r.batch_Start_Time > now() - interval '1 day') limit 1", nativeQuery = true)
    SerffPlanMgmtBatch getBatchByStatusAndOperationType(@Param("batchStatus") String batchStatus, @Param("operationType") String operationType);

	SerffPlanMgmtBatch findById(long id);
}
