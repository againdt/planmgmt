/**
 * 
 */
package com.serff.admin;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.serff.planmanagementexchangeapi.common.model.pm.Attachment;
import com.serff.planmanagementexchangeapi.common.model.pm.SupportingDocument;
import com.serff.planmanagementexchangeapi.exchange.model.pm.TransferDataTemplate;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlan;
import com.serff.service.SerffService;
/**
 * This class enables SERFF Admin to download a SOAP UI project for a particular 
 * request
 * 
 * The SOAP UI project will contain all attachment data as well 
 * 
 * @author Nikhil Talreja
 * @since 05 July, 2013
 */
@Component
public class DownloadSOAPUIProject {
	
	public static final Logger LOGGER = Logger.getLogger(DownloadSOAPUIProject.class);
	
	//Constants
	public static final String SOAP_HEADER = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"
			+ "<soapenv:Header/>" + "<soapenv:Body>";
	public static final String SOAP_FOOTER = "</soapenv:Body></soapenv:Envelope>";
	
	private static final String SOAP_UI_ZIP_FILE_NAME = "SERFF_SOAPUI.zip";
	private static final String SEB_SOAP_UI_PROJECT_FILE_NAME = "/sbe-soapui-project.xml";
	private static final String RESPONSE_FILE_NAME = "/response.xml";
	private static final String TRANSFER_PLAN_FILE_NAME = "/transferPlan.xml";
	private static final String ATTACHMENT_FOLDER_NAME = "/attach/";
	private static final String ATTACHMENT = "/attach";
	private static final String USER_HOME = "user.home";
	
	private static final int READ_BYTES = 1024;
	
	//Services
	@Autowired private SerffService serffService ;
	@Autowired private ContentManagementService ecmService;
	
	@Value(value = "#{template.template}") 
	private String soapUiTemplate;
	
	public byte [] downloadSerffProject(long reqId){
		
		String currentDate = null;
		byte [] zipFile = null;
		try{
			
			LOGGER.info("Creating SOAP UI Project for request Id " + reqId);
			
			//Step 1 :  Fetch record from SERFF_PLAN_MGMT table
			SerffPlanMgmt record = serffService.getSerffPlanMgmtById(reqId);
			if(record == null){
				LOGGER.error("Invalid Request Id. Record does not exist");
				return zipFile;
			}
			
			/*
			 * Step 2 : Create SOAP UI Request from template
			 * Template is at src/main/resources/SERFF-SOAPUI.xml
			 */
			String soapRequest = record.getRequestXml();
			soapRequest = soapRequest.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
			
			LOGGER.info("Reading SOAP UI project from template");
			/*String soapProject = FileUtils.readFileToString(
			        FileUtils.toFile(
			            this.getClass().getClassLoader().getResource("SERFF-SOAPUI.xml")
			        )
			    );*/
			//LOGGER.info("Template " + soapUiTemplate);
			String soapProject = StringUtils.trim(soapUiTemplate);
			
			FileUtils.writeStringToFile(new File(System.getProperty(USER_HOME)+TRANSFER_PLAN_FILE_NAME), soapRequest);
			FileUtils.writeStringToFile(new File(System.getProperty(USER_HOME)+RESPONSE_FILE_NAME), record.getResponseXml());
			TransferPlan transferPlanRequest = (TransferPlan)inputStreamToObject(new FileInputStream(new File(System.getProperty(USER_HOME)+TRANSFER_PLAN_FILE_NAME)),TransferPlan.class);
			
			currentDate = "/" +DateUtil.dateToString(new Date(), "MMddyyyy")+"_";
			
			FileOutputStream fos = new FileOutputStream(System.getProperty(USER_HOME)+currentDate+SOAP_UI_ZIP_FILE_NAME);
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		byte[] buffer = new byte[READ_BYTES];
    		
    		ZipEntry ze=null;
    		FileInputStream in =null;
    		int len;
    		
			
			List<TransferDataTemplate> transDataTempList = transferPlanRequest.getPlan().getDataTemplates().getDataTemplate();
			List<SupportingDocument> supportingDocuments = transferPlanRequest.getPlan().getSupportingDocuments().getSupportingDocument();
			Set<SerffDocument> attachments = record.getSerffDocuments();
			
			LOGGER.info("Adding Attachments to SOAP UI Project");
			
			new File(System.getProperty(USER_HOME)+ATTACHMENT).mkdir();
			
			Map<String,String> ecmMap = new HashMap<String, String>();
			StringBuffer sb = new StringBuffer();
			for(SerffDocument attachment : attachments){
				ecmMap.put(attachment.getDocName(), attachment.getEcmDocId());
				
				sb.append("<con:attachment>");
				sb.append("<con:name>"+attachment.getDocName()+"</con:name>");
				sb.append("<con:contentType>application/octet-stream</con:contentType>");
				sb.append("<con:contentId>"+removeExtension(attachment.getDocName())+"</con:contentId>");
				sb.append("<con:part>"+removeExtension(attachment.getDocName())+"</con:part>");
				sb.append("<con:url>attach/"+attachment.getDocName()+"</con:url>");
				sb.append("</con:attachment>");
			}
			
			soapProject = soapProject.replace("ATTACHMENTS", sb.toString());
			
			LOGGER.info("Fetching templates from ECM");
			for(TransferDataTemplate template : transDataTempList){
				
				String ecmId = template.getDataTemplateType() + "_" + template.getDataTemplateId() + ".xml";
				byte[] bytes = ecmService.getContentDataById(ecmMap.get(ecmId));
				LOGGER.info("Fetched template " + ecmId);
				
				ByteArrayDataSource ds = new ByteArrayDataSource(bytes);
				DataHandler templateData = new DataHandler(ds);
				
				template.getInsurerAttachment().setInsurerSuppliedData(templateData);
				
				
				FileOutputStream fileOuputStream = new FileOutputStream(System.getProperty(USER_HOME)+ATTACHMENT_FOLDER_NAME+ecmId);
				fileOuputStream.write(bytes);
				fileOuputStream.close();
				
				ze= new ZipEntry(System.getProperty(USER_HOME)+ATTACHMENT_FOLDER_NAME+ecmId);
	    		zos.putNextEntry(ze);
	    		
	    		in = new FileInputStream(System.getProperty(USER_HOME)+ATTACHMENT_FOLDER_NAME+ecmId);
	    		
	    		while ((len = in.read(buffer)) > 0) {
	    			zos.write(buffer, 0, len);
	    		}
	 
	    		in.close();
	    		zos.closeEntry();
				
			}
			
			LOGGER.info("Fetching Supporting Documentation from ECM");
			for(SupportingDocument supportingDoc : supportingDocuments){
				
				for(Attachment attachment  : supportingDoc.getAttachments().getAttachment() ){
					String ecmId = attachment.getFileName();
					byte[] bytes = ecmService.getContentDataById(ecmMap.get(ecmId));
					LOGGER.info("Fetched document " + ecmId);
					
					ByteArrayDataSource ds = new ByteArrayDataSource(bytes);
					DataHandler docData = new DataHandler(ds);
					
					attachment.setAttachmentData(docData);
					
					FileOutputStream fileOuputStream = new FileOutputStream(System.getProperty(USER_HOME)+ATTACHMENT_FOLDER_NAME+ecmId);
					fileOuputStream.write(bytes);
					fileOuputStream.close();
					
					ze= new ZipEntry(System.getProperty(USER_HOME)+ATTACHMENT_FOLDER_NAME+ecmId);
		    		zos.putNextEntry(ze);
		    		
		    		in = new FileInputStream(System.getProperty(USER_HOME)+ATTACHMENT_FOLDER_NAME+ecmId);
		    		
		    		while ((len = in.read(buffer)) > 0) {
		    			zos.write(buffer, 0, len);
		    		}
		 
		    		in.close();
		    		zos.closeEntry();
				}
				
			}
			
			//Step 3 : Create SOAP request and add to SOAP UI project
			soapRequest = objectToXmlString(transferPlanRequest);
			soapRequest = soapRequest.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
			soapRequest = SOAP_HEADER+soapRequest+SOAP_FOOTER;
			soapProject= soapProject.replace("REQUEST", soapRequest);
			
			//Add SOAP UI project to zip file
			FileUtils.writeStringToFile(new File(System.getProperty(USER_HOME)+SEB_SOAP_UI_PROJECT_FILE_NAME), soapProject);
			ze= new ZipEntry(System.getProperty(USER_HOME)+SEB_SOAP_UI_PROJECT_FILE_NAME);
    		zos.putNextEntry(ze);
    		in = new FileInputStream(System.getProperty(USER_HOME)+SEB_SOAP_UI_PROJECT_FILE_NAME);
    		while ((len = in.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}
    		in.close();
    		
    		ze= new ZipEntry(System.getProperty(USER_HOME)+RESPONSE_FILE_NAME);
    		zos.putNextEntry(ze);
    		in = new FileInputStream(System.getProperty(USER_HOME)+RESPONSE_FILE_NAME);
    		while ((len = in.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}
    		in.close();
    		
			//Step 4 : Create zip file
			zos.close();
			zipFile = FileUtils.readFileToByteArray(new File(System.getProperty(USER_HOME)+currentDate+SOAP_UI_ZIP_FILE_NAME));
			LOGGER.info("zipFile is null: " + (null == zipFile));
			return zipFile;
			
		}catch (Exception e){
			LOGGER.error(e.getMessage(),e);
			return zipFile;
		}
		finally{
			
			//Remove all files from user's machine
			try { 
				
				FileUtils.forceDelete(new File(System.getProperty(USER_HOME)+currentDate+SOAP_UI_ZIP_FILE_NAME));
				FileUtils.forceDelete(new File(System.getProperty(USER_HOME)+SEB_SOAP_UI_PROJECT_FILE_NAME));
				FileUtils.forceDelete(new File(System.getProperty(USER_HOME)+TRANSFER_PLAN_FILE_NAME));
				FileUtils.forceDelete(new File(System.getProperty(USER_HOME)+ATTACHMENT));
			} catch (Exception e) {
				LOGGER.error("Error deleting files", e);
			}
		}
	}
	
	/**
	 * Converts an object to xml string
	 * @author Nikhil Talreja
	 * @since 08 March 2013
	 */

	public  String objectToXmlString(Object object){

		try {
			JAXBContext context = JAXBContext.newInstance(object.getClass());
			JAXBIntrospector introspector = context.createJAXBIntrospector();
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter stringWriter = new StringWriter();

			if(null == introspector.getElementName(object)) {
				JAXBElement<Object> jaxbElement = new JAXBElement<Object>(new QName("soap"), Object.class, object);
				marshaller.marshal(jaxbElement, stringWriter);
			}
			else{
				marshaller.marshal(object, stringWriter);
			}

			return stringWriter.toString();
		} catch (JAXBException e) {
			LOGGER.error("Error creating request xml",e);
			return "";
		}
	}
	
	/**
	 * Converts an object to xml string
	 * @throws JAXBException 
	 * @since 08 March 2013
	 */

	@SuppressWarnings("rawtypes")
	public  Object inputStreamToObject(InputStream in, Class c) throws Exception{
		return GhixUtils.inputStreamToObject(in, c);
	}
	
	/**
	 * Utility method to remove extension from a file name
	 * @author - Nikhil Talreja
	 * @since 05 July, 2013
	 * @param String docName - E.g. abc.pdf
	 * @return String - File name without extension E.g. abc
	 */
	private String removeExtension(String docName) {
		
		String indexes [] = {"."};
		
		return StringUtils.substring(docName, 0, StringUtils.lastIndexOfAny(docName, indexes));
		
	}
}
