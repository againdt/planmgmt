package com.serff.admin;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.serff.util.PlanManagmentReportsRecord;
import com.serff.util.SerffConstants;

/**
 * This class creates the plan management report for Data-wise statistics
 * HIX-26726
 * 
 * @author Nikhil Talreja
 * @since 08 January, 2013
 *
 */
@Component
public class PlanManagementRepots {
	
	private static final Logger LOGGER = Logger.getLogger(PlanManagementRepots.class);
	
	@PersistenceUnit 
	private EntityManagerFactory entityManagerFactory;
	
	private Map<String,HashMap<String,BigInteger>> values = new HashMap<String, HashMap<String,BigInteger>>();
	
	/**
	 * This method generates reports for a particular date filter
	 * 
	 * @author Nikhil Talreja
	 * @since 08 January, 2013
	 *
	 */
	@SuppressWarnings("unchecked")
	public List<PlanManagmentReportsRecord> getDatewiseStats(String dateFilter){
		
		String createdQuery = null, startDateQuery = null, endDateQuery = null;
		String updatedQuery = null, certifiedQuery = null, enrollmentQuery = null;
		boolean isPostgresDB = SerffConstants.POSTGRES.equalsIgnoreCase(SerffConstants.getDatabaseType());
		//Queries
		if(isPostgresDB) {
			createdQuery = "select to_char(creation_timestamp, 'YYYY-MM-DD'), count(*) as created_total from plan where creation_timestamp > now() - interval '"+dateFilter+" day' group by to_char(creation_timestamp, 'YYYY-MM-DD') order by to_char(creation_timestamp, 'YYYY-MM-DD')";	
			startDateQuery = "select to_char(start_date, 'YYYY-MM-DD'), count(*) as start_date_total from plan where start_Date > now() - interval '"+dateFilter+" day' group by to_char(start_date, 'YYYY-MM-DD') order by to_char(start_date, 'YYYY-MM-DD')";
			endDateQuery = "select to_char(end_date, 'YYYY-MM-DD'), count(*) as end_date_total from plan where end_date > now() - interval '"+dateFilter+" day' group by to_char(end_date, 'YYYY-MM-DD') order by to_char(end_date, 'YYYY-MM-DD')";
			
			updatedQuery = "select distinct date1, count(*) as total from(select to_char(last_update_timestamp, 'YYYY-MM-DD') date1 FROM PLAN where last_update_timestamp > now() - interval '"+dateFilter+" day') as planData GROUP BY DATE1 order by date1";
			certifiedQuery = "select distinct date1, count(*) as total from(select to_char(certified_on, 'YYYY-MM-DD') date1 from plan where certified_on > now() - interval '"+dateFilter+" day') as planData group by date1 order by date1";
			enrollmentQuery = "select distinct date1, count(*) as total from(select to_char(enrollment_avail_effdate, 'YYYY-MM-DD') date1 from plan where enrollment_avail_effdate > now() - interval '"+dateFilter+" day') as planData group by date1 order by date1";
		} else {
			createdQuery = "select trunc(creation_timestamp), count(*) as created_total from plan where creation_timestamp > systimestamp -"+dateFilter+" group by trunc(creation_timestamp) order by trunc(creation_timestamp)";	
			startDateQuery = "select trunc(start_Date), count(*) as start_date_total from plan where start_Date > sysdate -"+dateFilter+" group by trunc(start_date) order by trunc(start_date)";
			endDateQuery = "select trunc(end_date), count(*) as end_date_total from plan where end_date > sysdate -"+dateFilter+" group by trunc(end_date) order by trunc(end_date)";
			
			updatedQuery = "select unique date1, count(*) as total from(select trunc(last_update_timestamp) date1 FROM PLAN where last_update_timestamp > systimestamp - "+dateFilter+" ) GROUP BY DATE1 order by date1";
			certifiedQuery = "select unique date1, count(*) as total from(select trunc(certified_on) date1 from plan where certified_on > sysdate -"+dateFilter+") group by date1 order by date1";
			enrollmentQuery = "select unique date1, count(*) as total from(select trunc(enrollment_avail_effdate) date1 from plan where enrollment_avail_effdate > sysdate - "+dateFilter+") group by date1 order by date1";
		}
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		try{
			values.clear();
			
			//HIX-67615:Covert native query to prepare statement. Validation to prevent sql injection.
			if (!StringUtils.isNumeric(dateFilter)) {
				return null;
			}
			
			//Creation TimeStamp
			Query query = entityManager.createNativeQuery(createdQuery);
			List<Object[]> results =  query.getResultList();
			addValuesToMap(results, "creationCount");
			
			//Start Date
			query = entityManager.createNativeQuery(startDateQuery);
			results =  query.getResultList();
			addValuesToMap(results, "startDateCount");
			
			//End Date
			query = entityManager.createNativeQuery(endDateQuery);
			results =  query.getResultList();
			addValuesToMap(results, "endDateCount");
			
			//Updated Date
			query = entityManager.createNativeQuery(updatedQuery);
			results =  query.getResultList();
			addValuesToMap(results, "updationCount");
			
			//Certified Date
			query = entityManager.createNativeQuery(certifiedQuery);
			results =  query.getResultList();
			addValuesToMap(results, "certifiedDateCount");
			
			//Enrollment effective Date
			query = entityManager.createNativeQuery(enrollmentQuery);
			results =  query.getResultList();
			addValuesToMap(results, "enrollmentEffectiveDateCount");
			
			LOGGER.debug(values.toString());
		}catch(Exception e){
			LOGGER.error("getDatewiseStats() : Error occurred : " + e.getMessage(), e);
		}finally{
			if(null != entityManager){
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}

		return convertMapToList(values);
	}
	
	
	/**
	 * Adds the values to the map for a particular date type
	 * 
	 * @author Nikhil Talreja
	 * @since 08 January, 2013
	 *
	 */
	private void addValuesToMap(List<Object[]> results, String statTye){
		
		for(int i=0; i<results.size(); i++){
			Object[] record = results.get(i);
			String date = record[0].toString();
			if(values.get(date) == null){
				HashMap<String,BigInteger> value = new HashMap<String,BigInteger>();
				values.put(date, value);
			}
			values.get(date).put(statTye, (BigInteger)record[1]);
		}
	}
	
	/**
	 * Converts the map of values to List of records
	 * 
	 * @author Nikhil Talreja
	 * @since 08 January, 2013
	 *
	 */
	private List<PlanManagmentReportsRecord> convertMapToList(Map<String,HashMap<String,BigInteger>> records){
		
		List<PlanManagmentReportsRecord> reportRecords = new ArrayList<PlanManagmentReportsRecord>();
		for(Map.Entry<String, HashMap<String,BigInteger>> entry : records.entrySet()){
			PlanManagmentReportsRecord reportRecord = new PlanManagmentReportsRecord(entry.getKey().replaceAll(" 00:00:00.0",""),
					entry.getValue().get("creationCount"),entry.getValue().get("startDateCount"),entry.getValue().get("endDateCount"),entry.getValue().get("updationCount"),
					entry.getValue().get("certifiedDateCount"),entry.getValue().get("enrollmentEffectiveDateCount"));
			reportRecords.add(reportRecord);
		}
		
		LOGGER.debug(reportRecords.toString());
		return reportRecords;
		
	}
}
