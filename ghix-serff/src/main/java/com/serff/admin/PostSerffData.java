package com.serff.admin;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.activation.DataHandler;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.serff.planmanagementexchangeapi.common.model.pm.Attachment;
import com.serff.planmanagementexchangeapi.common.model.pm.SupportingDocument;
import com.serff.planmanagementexchangeapi.exchange.model.pm.TransferDataTemplate;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlan;
import com.serff.service.SerffService;
import com.getinsured.hix.platform.util.GhixUtils;

/**
 * This class enables SERFF Admin to post a particular request from one environment
 * to another
 * 
 * It will simplify debugging process especially in PROD environment 
 * 
 * @author Nikhil Talreja
 * @since 04 July, 2013
 */

@Component
public class PostSerffData {
	
	private static final Logger LOGGER = Logger.getLogger(PostSerffData.class);
	
	//Constants
	private String serviceURl;
	
	@Value("#{configProp.serffServiceURL}")
	public void setServiceUrl(String serviceUrl) {
		serviceURl = serviceUrl;
	}
	
	/*public static String getServiceUrl() {
		return serviceURl;
	}*/
	
	private static final String SOAP_HEADER = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
								"<soapenv:Header/>"+
								"<soapenv:Body>";	
	
	private static final String SOAP_FOOTER = "</soapenv:Body></soapenv:Envelope>";
	
	
	
	//Services
	@Autowired private SerffService serffService ;
	@Autowired private ContentManagementService ecmService;
	
	/**
	 * This method makes a POST call to SERFF in the target environment
	 * 
	 * @author - Nikhil Talreja
	 * @since 04 July, 2013
	 * @param long reqId - Request id from source environment pointing to a record in SERRF_PLAN_MGMT table
	 * @param String host - Host address of target environment
	 * @param String port - Port number for target environment
	 */
	public String postData(long reqId, String host, String port){
		
		String serviceURL = "http://host:port"+serviceURl;
		serviceURL = serviceURL.replace("host", host);
		serviceURL = serviceURL.replace("port", port);
		
		try{
		
			LOGGER.info("Posting Data for request Id " + reqId + " to " + serviceURL);
			
			//Step 1 :  Fetch record from SERFF_PLAN_MGMT table
			SerffPlanMgmt record = serffService.getSerffPlanMgmtById(reqId);
			if(record == null){
				return "Invalid Request Id. Record does not exist";
			}
			
			/*
			 * Step 2 : Add Attachments (Templates and Supporting docs) to the new request
			 * This needs to done since original request is stored without attachment data in DB
			 */
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(serviceURL);
			
			String soapRequest = record.getRequestXml();
			soapRequest = soapRequest.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
			
			
			StringEntity stringentity = new StringEntity(soapRequest);
			TransferPlan transferPlanRequest = (TransferPlan)inputStreamToObject(stringentity.getContent(),TransferPlan.class);
			
			List<TransferDataTemplate> transDataTempList = transferPlanRequest.getPlan().getDataTemplates().getDataTemplate();
			List<SupportingDocument> supportingDocuments = transferPlanRequest.getPlan().getSupportingDocuments().getSupportingDocument();
			Set<SerffDocument> attachments = record.getSerffDocuments();
			
			Map<String,String> ecmMap = new HashMap<String, String>();
			for(SerffDocument attachment : attachments){
				ecmMap.put(attachment.getDocName(), attachment.getEcmDocId());
			}
			
			if(!CollectionUtils.isEmpty(transDataTempList)){
				LOGGER.info("Fetching templates from ECM");
				for(TransferDataTemplate template : transDataTempList){
					
					String ecmId = template.getDataTemplateType() + "_" + template.getDataTemplateId() + ".xml";
					
					if(StringUtils.isBlank(ecmId)){
						continue;
					}
					
					byte[] bytes = ecmService.getContentDataById(ecmMap.get(ecmId));
					LOGGER.info("Fetched template " + ecmId);
					
					ByteArrayDataSource ds = new ByteArrayDataSource(bytes);
					DataHandler templateData = new DataHandler(ds);
					
					template.getInsurerAttachment().setInsurerSuppliedData(templateData);
					
				}
			}
			
			if(!CollectionUtils.isEmpty(supportingDocuments)){
				LOGGER.info("Fetching Supporting Documentation from ECM");
				for(SupportingDocument supportingDoc : supportingDocuments){
					
					for(Attachment attachment  : supportingDoc.getAttachments().getAttachment() ){
						String ecmId = attachment.getFileName();
						
						if(StringUtils.isBlank(ecmId)){
							continue;
						}
						
						byte[] bytes = ecmService.getContentDataById(ecmMap.get(ecmId));
						LOGGER.info("Fetched document " + ecmId);
						
						ByteArrayDataSource ds = new ByteArrayDataSource(bytes);
						DataHandler docData = new DataHandler(ds);
						
						attachment.setAttachmentData(docData);
					}
					
				}
			}
			
			//Step 3 : Create SOAP request
			soapRequest = objectToXmlString(transferPlanRequest);
			soapRequest = soapRequest.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
			
			stringentity = new StringEntity(SOAP_HEADER+soapRequest+SOAP_FOOTER);
			stringentity.setChunked(true);
			stringentity.setContentType("text/xml");
			
			httppost.setEntity(stringentity);
			
			//LOGGER.info("Request :\n" + soapRequest);
			
			httppost.addHeader("Accept", "text/xml");
			//httppost.addHeader("Authorization","Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==");
			
			// Step 4 : Execute and get the response.
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			
			String strresponse = null;
	        if(entity!=null) {
	            strresponse = EntityUtils.toString(entity);
	        }
			
	        if(LOGGER.isInfoEnabled()) {
	        	LOGGER.info("Success " + SecurityUtil.sanitizeForLogging(strresponse));
	        }
			
			return record.getResponseXml();
		}
		catch (Exception e){
			LOGGER.error(e.getMessage(),e);
			return e.getMessage();
		}
		
	}
	
	/**
	 * Converts an object to xml string
	 * @author Nikhil Talreja
	 * @since 08 March 2013
	 */

	public  String objectToXmlString(Object object){

		try {
			JAXBContext context = JAXBContext.newInstance(object.getClass());
			JAXBIntrospector introspector = context.createJAXBIntrospector();
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter stringWriter = new StringWriter();

			if(null == introspector.getElementName(object)) {
				JAXBElement<Object> jaxbElement = new JAXBElement<Object>(new QName("soap"), Object.class, object);
				marshaller.marshal(jaxbElement, stringWriter);
			}
			else{
				marshaller.marshal(object, stringWriter);
			}

			return stringWriter.toString();
		} catch (JAXBException e) {
			LOGGER.error("Error creating request xml",e);
			return "";
		}
	}
	
	/**
	 * Converts an object to xml string
	 * @throws JAXBException 
	 * @since 08 March 2013
	 */

	@SuppressWarnings("rawtypes")
	public  Object inputStreamToObject(InputStream in, Class c) throws Exception{
        //return jaxbUnmarshaller.unmarshal(in);
		return GhixUtils.inputStreamToObject(in, c);
	}
}
