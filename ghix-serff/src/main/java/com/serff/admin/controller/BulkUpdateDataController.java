package com.serff.admin.controller;

import java.util.ArrayList;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.serffadapter.bulkupload.BulkUploadService;
import com.serff.util.SerffConstants;

@Controller
public class BulkUpdateDataController {

	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private BulkUploadService bulkUploadService;

	
	private static final Logger LOGGER = LoggerFactory.getLogger(BulkUpdateDataController.class);
	private static final String RETURN_ISSUER_LOGO_UPDATE_PAGE = "updateIssuerlogo";
	private static final String RETURN_PLAN_BROCHURE_UPDATE_PAGE = "updatePlanBrochure";
	private static final String RETURN_MIGRATE_ISSUER_LOGO_PAGE = "migrateIssuerLogo";
	private static final String MSG_BULK_UPDATE = "msgbulkupdate";
	private static final String FOLDER_PREFIX = "LOGO_" ;
	private static final String BROCHURE_FOLDER_PREFIX = "BROCHURE_" ;
	private static final String EXCEPTIONS = "Exception: ";

	@RequestMapping(value = "/admin/updateIssuerLogo", method = { RequestMethod.GET, RequestMethod.POST })
	public String viewIssuerLogoBulkUpdatePage(Model model){
		
		String folderPath = null;
		boolean directory = false;
		try{
			folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getIssuerLogoUploadPath();
			List<String> folderList = ftpClient.getFileNames(folderPath);
			
			List<String> foldersToProcessList = new ArrayList<String>() ;
			
			if(CollectionUtils.isEmpty(folderList)){
				return RETURN_ISSUER_LOGO_UPDATE_PAGE;
			}else{
				for(String eachFolder : folderList){
					 directory = ftpClient.isRemoteDirectory(folderPath+SerffConstants.PATH_SEPERATOR+eachFolder);
					 if(eachFolder.startsWith(FOLDER_PREFIX) && directory ){
						 foldersToProcessList.add(eachFolder);
					 }
				}
			}
			
			model.addAttribute("folderList" , foldersToProcessList);
		}catch(Exception e){
			LOGGER.error("Exception occured while fetching folders from FTP :" , e);
		}
		return RETURN_ISSUER_LOGO_UPDATE_PAGE;
		
	}
	
	@RequestMapping(value ="/admin/processIssuerLogoBulkUpdate" , method = { RequestMethod.GET, RequestMethod.POST })
	public String processIssuerLogoBulkUpdate(Model model , @RequestParam String folderName){
		String basefolderPath= null;
		String folderToProcess = null;
		
		if(null!= folderName){
			basefolderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getIssuerLogoUploadPath();
			folderToProcess = basefolderPath+SerffConstants.PATH_SEPERATOR + folderName;
			model.addAttribute(MSG_BULK_UPDATE, bulkUploadService.processLogoBulkUpload(folderToProcess));
		}
		return RETURN_ISSUER_LOGO_UPDATE_PAGE;
		
	}
	
	
	@RequestMapping(value = "/admin/updatePlanBrochure", method = { RequestMethod.GET, RequestMethod.POST })
	public String viewPlanBrochureBulkUpdatePage(Model model){
		
		String folderPath = null;
		boolean directory = false;
		try{
			folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlanBrochureUploadPath();
			List<String> folderList = ftpClient.getFileNames(folderPath);
			
			List<String> foldersToProcessList = new ArrayList<String>() ;
			
			if(CollectionUtils.isEmpty(folderList)){
				return RETURN_PLAN_BROCHURE_UPDATE_PAGE;
			}else{
				for(String folder : folderList){
					 directory = ftpClient.isRemoteDirectory(folderPath + SerffConstants.PATH_SEPERATOR + folder);
					 if(folder.startsWith(BROCHURE_FOLDER_PREFIX) && directory ){
						 foldersToProcessList.add(folder);
					 }
				}
			}
			
			model.addAttribute("folderList", foldersToProcessList);
			
		}catch(Exception e){
			LOGGER.error("Exception occured while fetching folders from FTP :" , e);
		}
	
		return RETURN_PLAN_BROCHURE_UPDATE_PAGE;
		
	}
	
	@RequestMapping(value ="/admin/processPlanBrochureBulkUpdate" , method = { RequestMethod.GET, RequestMethod.POST })
	public String processPlanBrochureBulkUpdate(Model model , @RequestParam String folderName){
		String basefolderPath= null;
		String folderToProcess = null;
		
		if(null!= folderName){
			basefolderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlanBrochureUploadPath();
			folderToProcess = basefolderPath + SerffConstants.PATH_SEPERATOR + folderName;
			model.addAttribute(MSG_BULK_UPDATE, bulkUploadService.processPlanBrochureBulkUpload(folderToProcess));
		}
		
		return RETURN_PLAN_BROCHURE_UPDATE_PAGE;
		
	}
	
	/**
	 * @param model, Model Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/executeMigrateIssuerLogo", method = RequestMethod.POST)
	public String migrateIssuerLogo(Model model) {
		try {
			model.addAttribute(MSG_BULK_UPDATE, bulkUploadService.migrateIssuerLogoToCDN());
		}
		catch (Exception e) {
			model.addAttribute(MSG_BULK_UPDATE, "Exception occurred while issuer logo migration.");
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		return RETURN_MIGRATE_ISSUER_LOGO_PAGE;
	}
}
