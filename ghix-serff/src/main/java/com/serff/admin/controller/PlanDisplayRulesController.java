package com.serff.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.hix.model.PlanDisplayRules;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.serff.repository.templates.IPlanDisplayRulesRepository;
import com.serff.service.PlanDisplayMapping;

@Controller
public class PlanDisplayRulesController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDisplayRulesController.class);
	
	private static final String EDIT_DISPLAY_RULE_VIEW = "editDisplayRule";
	private static final String GET_DISPLAY_RULES_VIEW = "displayRuleTopPage";

	@Autowired private IPlanDisplayRulesRepository iPlanDisplayRulesRepository;
	@Autowired private PlanDisplayMapping planDisplayMapping;

	
	@RequestMapping(value = "/admin/saveDisplayRule", method = RequestMethod.POST)
	public String saveDispalyRule(@ModelAttribute PlanDisplayRules updatedDisplayRule, HttpServletRequest request) {
		
		LOGGER.info("Saving Display Rule : " + updatedDisplayRule.getId());
		if(null != updatedDisplayRule.getId()){
			
			PlanDisplayRules displayRule = iPlanDisplayRulesRepository.findByPlanDisplayRulesId(updatedDisplayRule.getId());
			//Set new values in display rule.
			displayRule.setTileDisplayTemplate(updatedDisplayRule.getTileDisplayTemplate());
			displayRule.setStandardDisplayTemplate(updatedDisplayRule.getStandardDisplayTemplate());
			displayRule.setPrimaryCareT1ForNumCopay(updatedDisplayRule.getPrimaryCareT1ForNumCopay());
			displayRule.setPrimaryCareT1ForNumVisits(updatedDisplayRule.getPrimaryCareT1ForNumVisits());
			displayRule.setPrimaryCareT1ForCopayAndVisits(updatedDisplayRule.getPrimaryCareT1ForCopayAndVisits());
			//Save display rule.
			iPlanDisplayRulesRepository.save(displayRule);
			request.setAttribute("hiosIssuerId", updatedDisplayRule.getHiosIssuerId());			
		}

		return EDIT_DISPLAY_RULE_VIEW;
	}

	@RequestMapping(value = "/admin/getDisplayRuleTopPage", method = { RequestMethod.GET, RequestMethod.POST }) 
	public String getDispalyRules(Model model, HttpServletRequest request) {
		String hiosId = request.getParameter("hiosIssuerId");
		boolean firstVisitToPage = false;
		if(!StringUtils.isNumeric(hiosId)) {
			firstVisitToPage = true;
			hiosId = "00000";
		}
		getDisplayRulesForIssuer(hiosId, model, firstVisitToPage);
		return GET_DISPLAY_RULES_VIEW;
	}
	
	private void getDisplayRulesForIssuer(String hiosId, Model model, boolean firstVisit) {
		//by default don't show Copy Form for first Visit
		boolean showCopyForm = !firstVisit;
		List<String> issuerIdsHavingActiveRules = null;
		List<PlanDisplayRules> displayRules = iPlanDisplayRulesRepository.findByHiosIssuerIdAndIsActive(hiosId, "Y");
		if(CollectionUtils.isEmpty(displayRules)) {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("No Display Rule found for Issuer Id: " + SecurityUtil.sanitizeForLogging(hiosId) );
			}
			if(showCopyForm) {
				//if no rules found, we may need list of issuers to copy rules from
				issuerIdsHavingActiveRules = iPlanDisplayRulesRepository.getIssuerIdsHavingActiveDisplayRules();
			}
		} else {
			showCopyForm = false;
		}
		model.addAttribute("hiosIssuerId", hiosId);
		model.addAttribute("planDisplayRule", displayRules);
		model.addAttribute("showCopyForm", showCopyForm);
		model.addAttribute("issuerIdsHavingActiveRules", issuerIdsHavingActiveRules);
	}
	
	@RequestMapping(value = "/admin/copyDisplayRules", method = { RequestMethod.GET, RequestMethod.POST }) 
	public String copyDispalyRules(Model model, HttpServletRequest request) {
		String hiosId = request.getParameter("hiosIssuerId");
		String copyFromHiosId = request.getParameter("copyFromIssuerId");
		if(!planDisplayMapping.copyActiveDisplayRulesForIssuer(copyFromHiosId, hiosId)) {
			model.addAttribute("ErrorMessage", "Failed to Copy rules from " + copyFromHiosId + " To " + hiosId );
		}
		getDisplayRulesForIssuer(hiosId, model, false);
		return GET_DISPLAY_RULES_VIEW;
	}	
}
