package com.serff.admin.controller;

import static com.serff.util.SerffConstants.BENEFITS_FILENAME_SUFFIX;
import static com.serff.util.SerffConstants.BREAK_LINE;
import static com.serff.util.SerffConstants.BUSINESS_RULE_FILENAME_SUFFIX;
import static com.serff.util.SerffConstants.FTP_AME_AGE_FILE;
import static com.serff.util.SerffConstants.FTP_AME_FAMILY_FILE;
import static com.serff.util.SerffConstants.FTP_AME_GENDER_FILE;
import static com.serff.util.SerffConstants.FTP_AREA_FACTOR_FILE;
import static com.serff.util.SerffConstants.FTP_HCC_FILE;
import static com.serff.util.SerffConstants.FTP_LIFE_FILE;
import static com.serff.util.SerffConstants.FTP_PLAN_DOCS_FILE;
import static com.serff.util.SerffConstants.FTP_PLAN_MEDICAID_FILE;
import static com.serff.util.SerffConstants.FTP_PLAN_MEDICARE_FILE;
import static com.serff.util.SerffConstants.FTP_STM_FILE;
import static com.serff.util.SerffConstants.FTP_UPLOAD_MAX_SIZE;
import static com.serff.util.SerffConstants.FTP_VISION_FILE;
import static com.serff.util.SerffConstants.NCQA_FILENAME_SUFFIX;
import static com.serff.util.SerffConstants.NETWORK_ID_FILENAME_SUFFIX;
import static com.serff.util.SerffConstants.PRESCRIPTION_DRUG_FILENAME_SUFFIX;
import static com.serff.util.SerffConstants.RATING_TABLE_FILENAME_SUFFIX;
import static com.serff.util.SerffConstants.SERVICE_AREA_FILENAME_SUFFIX;
import static com.serff.util.SerffConstants.TRANSFER_AME_AGE;
import static com.serff.util.SerffConstants.TRANSFER_AME_FAMILY;
import static com.serff.util.SerffConstants.TRANSFER_AME_GENDER;
import static com.serff.util.SerffConstants.TRANSFER_LIFE;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_AD;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_RX;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_SP;
import static com.serff.util.SerffConstants.TRANSFER_PLAN_DOCS;
import static com.serff.util.SerffConstants.TRANSFER_PLAN_MEDICAID;
import static com.serff.util.SerffConstants.TRANSFER_STM;
import static com.serff.util.SerffConstants.TRANSFER_VISION;
import static com.serff.util.SerffConstants.UNIFIED_RATE_FILENAME_SUFFIX;
import static com.serff.util.SerffConstants.UNSECURE_CHARS_REGEX;
import static com.serff.util.SerffConstants.URAC_FILENAME_SUFFIX;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.ExtWSCallLogs;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.quotit.client.QuotitService;
import com.getinsured.quotit.service.CarriersPlansBenefitsService;
import com.getinsured.quotit.util.QuotitConstants;
import com.getinsured.serffadapter.ame.AMEPlanReader;
import com.getinsured.serffadapter.hcc.HCCPlanReader;
import com.getinsured.serffadapter.life.LifeExcelReader;
import com.getinsured.serffadapter.medicaid.MedicaidExcelReader;
import com.getinsured.serffadapter.plandoc.PlanDocumentExcelReader;
import com.getinsured.serffadapter.puf.medicare.MedicarePlanReader;
import com.getinsured.serffadapter.stm.STMPlanReader;
import com.getinsured.serffadapter.vision.VisionExcelReader;
import com.serff.admin.DownloadSOAPUIProject;
import com.serff.service.BatchDataLoadService;
import com.serff.service.PlanBenefitEditService;
import com.serff.service.SerffService;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

/**
 * Controller class for PHIX Plan Data / SERFF Templates / Ancillary Templates / Invoke Quotit Service
 * and all other files Upload.
 * 
 * @author Bhavin Parmar
 * @since 23 August, 2013
 */
@Controller
public class PlanDataUploadController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDataUploadController.class);

	private static final String RETURN_UPLOAD_PAGE = "addQHPPlans";
	private static final String RETURN_BOTTOM_LEFT_PAGE = "serfBatchAdminBottomLeft";
	private static final String RETURN_SERFF_TOP_PAGE = "serfBatchAdminTop";
	private static final String RETURN_SERFF_UPLOAD_PAGE = "uploadPlans";
	private static final String RETURN_PLAN_EXCEL_UPLOAD_PAGE = "uploadSTMPlans";
	private static final String RETURN_PLAN_DOCS_STATUS_PAGE = "plansDocsStatusTop";
	private static final String RETURN_PLAN_MEDICAID_STATUS_PAGE = "medicaidPlansStatusTop";
	private static final String RETURN_INVOKE_QUOTIT_SERVICE_PAGE = "invokeQuotitService";
	private static final String RETURN_INVOKE_GET_ZIPCODE_INFO_QUOTIT_SERVICE_PAGE = "invokeGetZipCodeInfoQuotitService";
	private static final String RETURN_APPLY_EDIT_BENEFIT_PAGE = "applyBenefitEdits";
	private static final String RETURN_PROCESS_PAGE = "processDisplayData";
	private static final String RETURN_T1_PROCESS_PAGE ="processT1Display";
	private static final String RETURN_PROCESS_DENTAL_PAGE = "processDentalDisplayData";
	private static final String RETURN_EXCEL_UPLOAD_PAGE = "uploadSTMExcelForm";
	private static final String RETURN_EXCEL_PUF_UPLOAD_PAGE = "uploadPUFExcel";
	private static final String RETURN_EXCEL_PUF_STATUS_PAGE = "pufStatusTop";
	private static final String RETURN_QUOTIT_REQUEST_RESPONSE_TOP = "quotitRequestResponseTop";

	private static final String FTP_STATUS = "ftpStatus";
	private static final String EXCEPTIONS = "Exception: ";
	private static final String PUF_PLANS_STATUS = "pufAllPlansStatus";
	private static final String STATE_CODE = "state_code";
	private static final String HIOS_ID = "hios_id";
	//private static final String TEMPLATE_PATH = "src_path";
	private static final String MSG_PUF_EXCEL = "uploadPUFExcelMessage";
	private static final String MSG_ANCILLARY_EXCEL = "uploadSTMExcelMessage"; //Covers STM/AME/Vision/Life
	private static final String MSG_EXECUTE_BATCH = "executeBatchMessage";
	private static final String MSG_STM_UPLOAD_FAIL = "Failed to upload Excel to FTP server.";
	private static String PLANS_FOR = "plansFor";
	private static String MEDICAID = "medicaid";
	private static String ANCILLARY = "ancillary";
	private static String PLAN_DOCS = "uploadPlanDocs";
	private static String PAGE_NAME = "pageName";
	private static String MEDICAID_PAGE_HEADING = "Upload Medicaid (Excel File) to FTP Server";
	private static String ANCILLARY_PAGE_HEADING = "Upload AME/STM/HCC/VISION/LIFE Plans Data (Excel File) to FTP Server";
	private static String PLAN_DOCS_PAGE_HEADING = "Upload Plans Docs (Excel File) to FTP Server";

	private static final String CARRIER = "carrier";
	private static final String CARRIER_TEXT = "carrierText";
	private static final String STATE_CODES = "stateCodes";
	private static final String TENANT_TEXT = "tenantText";
	private static final String EXCHANGE_TYPE = "exchangeType";
	private static final int FILE_LIST_BY4 = 4;
	private static final int FILE_LIST_BY9 = 9;
	private static final int LEN_OF_CARRIER = 100;
	
	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private SerffService serffService;
	@Autowired private BatchDataLoadService batchDataLoadService;
	@Autowired private DownloadSOAPUIProject downloadSOAPUIProject;
	@Autowired private STMPlanReader stmPlanReader;
	@Autowired private AMEPlanReader amePlanReader;
	@Autowired private HCCPlanReader hccPlanReader;
	@Autowired private PlanDocumentExcelReader planDocumentExcelReader;
	@Autowired private VisionExcelReader visionExcelReader;
	@Autowired private MedicaidExcelReader medicaidExcelReader;
	@Autowired private MedicarePlanReader medicarePlanReader;
	@Autowired private LifeExcelReader lifeExcelReader;
	@Autowired private SerffUtils serffUtils;
	@Autowired private SerffResourceUtil serffResourceUtil;

	@Autowired private QuotitService planBenefitsClient;
	@Autowired private QuotitService zipCodeInfoClient;
	@Autowired private PlanBenefitEditService planBenefitEditService;
	@Autowired private CarriersPlansBenefitsService carriersPlansBenefitsService;

	/**
	 * Method is used to required data for upload plan details. 
	 * @param model, Model Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/loadUploadPlanDetails", method = RequestMethod.GET)
	public String loadUploadPlanDetails(Model model , final HttpServletRequest request) {
		
		LOGGER.info("loadUploadPlanDetails Begin");
		
		try {
			setFields(model);
		}
		catch (Exception e) {
			LOGGER.error("Failed in loadUploadPlanDetails", e);
		}
		finally {
			LOGGER.info("loadUploadPlanDetails End");
		}
		return RETURN_UPLOAD_PAGE;
	}
	
	/**
	 * Method is used to upload templates to FTP. 
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @param fileList, MultipartFile Array
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/uploadPlans", method = RequestMethod.POST)
	public String addTemplateAttachments(Model model, final HttpServletRequest request,
			@RequestParam("fileUploadPlans") MultipartFile[] fileList, @RequestParam(required = false) boolean pndCheckbox) {
		
		LOGGER.info("addTemplateAttachments Begin");
		String uploadStatus = null;
		
		try {
			uploadStatus = uploadTemplateAttachments(model, request, fileList , pndCheckbox);
			LOGGER.info("uploadStatus: " + uploadStatus);
		}
		finally {
			LOGGER.info("addTemplateAttachments End");
		}
		return RETURN_UPLOAD_PAGE;
	}
	
	/**
	 * Method is used to upload templates from one FTP server to another FTP server.
	 * This is used from SERFF Console to post plans to other servers.
	 * As on 21/Aug/2014 we disabled this feature in UI due to security reasons.
	 *  
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/postFTPPlans", method = RequestMethod.POST)
	@ResponseBody	
	public String postMultipartFTPRequest(Model model, final HttpServletRequest request,
			@RequestParam("fileUploadPlans") MultipartFile[] fileList) {
		
		LOGGER.info("postMultipartFTPRequest Begin");
		String uploadStatus = null;
		
		try {
			uploadStatus = uploadTemplateAttachments(model, request, fileList , false);
			LOGGER.info(" uploadStatus : " + uploadStatus);
		}
		finally {
			LOGGER.info("postMultipartFTPRequest End");
		}
		return uploadStatus;
	}
	
	/**
	 * Method is used to upload templates to FTP. 
	 * @return Status.
	 */
	private String uploadTemplateAttachments(Model model, final HttpServletRequest request,
			@RequestParam("fileUploadPlans") MultipartFile[] fileList, boolean pndCheckbox) {
		
		LOGGER.info("uploadTemplateAttachments Begin");
		SerffPlanMgmtBatch serffPlanMgmtBatch = null;
		boolean flag = false;
		String returnStatus = SerffConstants.PUF_STATUS.FAILED.name();
		try {
			
			setFields(model);
			
			// Validations
			if (isValidate(model, request, fileList , pndCheckbox)) {
				return returnStatus;
			}
			
			// Step1: Fetch ID from SERFF_PLAN_MGMT_BATCH TABLE and update status to “FTP started”, User_ID.
			serffPlanMgmtBatch = createBatchRecord(request);
			
			if (null != ftpClient && null != serffPlanMgmtBatch) {
			
				// Step2: Create Folder in FTP Location and Download all files to this location.
				// ISSUER_ID + STATE_CODE + SERFF_PLAN_MGMT_BATCH_ID
				final String ftpUploadPath = SerffConstants.getFTPUploadPlanPath()
						+ serffPlanMgmtBatch.getIssuerId()
						+ serffPlanMgmtBatch.getState()
						+ serffPlanMgmtBatch.getId() + "/";
				ftpClient.createDirectory(ftpUploadPath);
				LOGGER.info("Created Directory Name with path as : " + ftpUploadPath);
				
				if (null != fileList) {
					
					String fileName = null;
					
					for (MultipartFile multipartFile : fileList) {

						fileName = multipartFile.getOriginalFilename();
		                ftpClient.uplodadFile(multipartFile.getInputStream(), ftpUploadPath + fileName);
		                if(LOGGER.isInfoEnabled()) {
		                	LOGGER.info("FTP Uploaded File Name : " + SecurityUtil.sanitizeForLogging(fileName));
		                }
		    			// Fixed HP-FOD issue.
		    			serffResourceUtil.releaseInputStream(multipartFile.getInputStream());
		            }
		        }
				// Step3: Update SERFF_PLAN_MGMT_BATCH TABLE status to “FTP Completed”
				serffPlanMgmtBatch.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.COMPLETED);
				serffPlanMgmtBatch.setFtpEndTime(new Date());
				serffPlanMgmtBatch.setBatchStatus(SerffPlanMgmtBatch.BATCH_STATUS.WAITING);
				if(pndCheckbox){
					serffPlanMgmtBatch.setOperationType(SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.toString());
				}else{
					serffPlanMgmtBatch.setOperationType(SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.toString());
				}
				serffService.saveSerffPlanMgmtBatch(serffPlanMgmtBatch);
				flag = true;
			}
			
			String ftpSucess;
			// Step4: Display message to end user that files are submitted for process.
			if (flag) {
				returnStatus = SerffConstants.PUF_STATUS.COMPLETED.name();
				ftpSucess = "All templates have been uploaded successfully!!";
				model.addAttribute("ftpSucess", ftpSucess);
			}
			else {
				returnStatus = SerffConstants.PUF_STATUS.FAILED.name();
				ftpSucess = "Server Error has been occuerd during uploading!!";
				model.addAttribute(FTP_STATUS, ftpSucess);
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception in uploadTemplateAttachments ", e);
		}
		finally {
			LOGGER.info("uploadTemplateAttachments End");
			if(null!= ftpClient){
				ftpClient.close();
			}
		}
		return returnStatus;
	}
	
	/**
	 * Method is used to generate hios templates from puf files . 
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/getfilesTogenerate", method = RequestMethod.GET)
	public String generatePufTemplates(Model model ,HttpServletRequest request) {
		
		LOGGER.info("generatePufTemplates Begin");
		String state = request.getParameter(STATE_CODE);
		String hiosId = request.getParameter(HIOS_ID);
		
		try {
			serffService.generateTemplatesForPUF(state, hiosId);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("generateTemplatesForPUF for:"+SecurityUtil.sanitizeForLogging(state) +" hois id:"+ SecurityUtil.sanitizeForLogging(hiosId));
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception: generatePufTemplates ", e);
		}
		finally {
			LOGGER.info( "generatePufTemplates End");
		}
		return PUF_PLANS_STATUS;
	}
	
	@RequestMapping(value = "/admin/loadToQA", method = RequestMethod.GET)
	public String loadToQA(Model model ,HttpServletRequest request) {
		
		LOGGER.info("loadToQA called, but the method is not supported anymore.");
		/*String batchId = request.getParameter(STATE_CODE);
		String hiosId = request.getParameter(HIOS_ID);
		String path = request.getParameter(TEMPLATE_PATH);
		
		try {
			serffService.pushPUFToQA(batchId, hiosId, path);
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS+ e.getMessage(), e);
		}
		finally {
			LOGGER.info( "loadToQA End");
		}*/
		return PUF_PLANS_STATUS;
	}
	
	/**
	 *  Method is used to load files to Stage environment .  
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/loadtoStage", method = RequestMethod.GET)
	public String loadtoStage(Model model ,HttpServletRequest request) {
		
		LOGGER.info("loadtoStage called, but the method is not supported anymore.");
		
		/*String batchId = request.getParameter(STATE_CODE);
		String hiosId = request.getParameter(HIOS_ID);
		String path = request.getParameter(TEMPLATE_PATH);
		
		try {
			serffService.pushPUFToStage(batchId, hiosId, path);
		}
		catch (Exception e) {
			LOGGER.error("Exception in loadtoStage", e);
		}
		finally {
			LOGGER.info("loadtoStage End");
		}*/
		return PUF_PLANS_STATUS;
	}
		
	/**
	 * Method is used to check Validations for Upload UI page.
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @param fileList, MultipartFile Array
	 * @return boolean flag.
	 */
	private boolean isValidate(Model model, final HttpServletRequest request,
			final MultipartFile[] fileList , boolean pndCheckFlag) {
		
		boolean isError = false;
		StringBuffer error = new StringBuffer();
		
		if (!pndCheckFlag && (null == fileList || fileList.length < FILE_LIST_BY4)) {
			error.append("Required templates are missing.");
			isError = true;
		}
		else if(fileList.length > FILE_LIST_BY9) {
			
			if(isError) {
				error.append(BREAK_LINE);
			}
			else {
				isError = true;
			}
			error.append("Please attach "+ FILE_LIST_BY4 +" mandatory and maximum "+ (FILE_LIST_BY9 - FILE_LIST_BY4) +" optional Template XMLs.");
		}
		else {
			
			long maxSize = FTP_UPLOAD_MAX_SIZE / SerffConstants.BYTE_1MB_SIZE;
			
			for (MultipartFile multipartFile : fileList) {
				
				if (null != multipartFile
						&& multipartFile.getSize() > FTP_UPLOAD_MAX_SIZE) {
					
					if(isError) {
						error.append(BREAK_LINE);
					}
					else {
						isError = true;
					}
					error.append(SecurityUtil.sanitizeForLogging(multipartFile.getOriginalFilename()) + " File size is exceeding max size " + maxSize + "MB.");
				}
            }
		}
		
		String carriedData = request.getParameter(CARRIER);
		if (StringUtils.isBlank(carriedData) || "-1".equals(carriedData)) {
			
			if(isError) {
				error.append(BREAK_LINE);
			}
			else {
				isError = true;
			}
			error.append("Carrier is required field.");
		}
		
		if (StringUtils.isBlank(request.getParameter(STATE_CODES))) {
			
			if(isError) {
				error.append(BREAK_LINE);
			}
			else {
				isError = true;
			}
			error.append("State Code is required field.");
		}
		
		if (StringUtils.isBlank(request.getParameter(EXCHANGE_TYPE))) {
			
			if(isError) {
				error.append(BREAK_LINE);
			}
			else {
				isError = true;
			}
			error.append("Exchange Type must be required.");
		}
		
		if (StringUtils.isBlank(request.getParameter(TENANT_TEXT))
				|| StringUtils.isBlank(request.getParameter("tenant"))) {
			
			if(isError) {
				error.append(BREAK_LINE);
			}
			else {
				isError = true;
			}
			error.append("Tenant must be required.");
		}
		
		if (isError) {
			LOGGER.error("Validation Erros: " + error.toString());
			model.addAttribute(FTP_STATUS, error.toString());
		}
		return isError;
	}
	
	/**
	 * Method is used to add required data in Model Object for Upload UI.
	 * @param model, Model Object
	 * @throws Exception
	 */
	private void setFields(Model model )  {
		final StateHelper stateHelper = new StateHelper();
		setIssuerAndTenantList(model);
		model.addAttribute("statesList", stateHelper.getAllStates());
		model.addAttribute("exchangeTypeList", SerffPlanMgmtBatch.EXCHANGE_TYPE.values());
		model.addAttribute("DRUG_SUFFIX", PRESCRIPTION_DRUG_FILENAME_SUFFIX);
		model.addAttribute("NETWORK_SUFFIX", NETWORK_ID_FILENAME_SUFFIX);
		model.addAttribute("BENEFITS_SUFFIX", BENEFITS_FILENAME_SUFFIX);
		model.addAttribute("RATE_SUFFIX", RATING_TABLE_FILENAME_SUFFIX);
		model.addAttribute("SERVICE_AREA_SUFFIX", SERVICE_AREA_FILENAME_SUFFIX);
		model.addAttribute("BUSINESS_RULE_SUFFIX", BUSINESS_RULE_FILENAME_SUFFIX);
		model.addAttribute("UNIFIED_RATE_SUFFIX", UNIFIED_RATE_FILENAME_SUFFIX);
		model.addAttribute("URACSUFFIX", URAC_FILENAME_SUFFIX);
		model.addAttribute("NCQASUFFIX", NCQA_FILENAME_SUFFIX);
		
	}
	
	/**
	 * Method is used to Create new record for SERFF_PLAN_MGMT_BATCH table.
	 * @param request HttpServletRequest Object
	 * @return new Saved SerffPlanMgmtBatch Object
	 */
	private SerffPlanMgmtBatch createBatchRecord(final HttpServletRequest request) {
		String issuerId, stateCode, exchangeType, tenants;
		String carrierText = request.getParameter(CARRIER_TEXT);
		if (StringUtils.isNotBlank(carrierText) && carrierText.length() > LEN_OF_CARRIER) {
			carrierText = carrierText.substring(SerffConstants.INITIAL_INDEX, LEN_OF_CARRIER);
		}
		issuerId = request.getParameter(CARRIER);
		stateCode = request.getParameter(STATE_CODES);
		exchangeType = request.getParameter(EXCHANGE_TYPE);
		tenants = request.getParameter(TENANT_TEXT);
		SerffPlanMgmtBatch serffPlanMgmtBatch = serffUtils.createBatchRecord(issuerId, carrierText, stateCode, exchangeType, tenants, null, SerffConstants.DEFAULT_USER_ID);
		return serffService.saveSerffPlanMgmtBatch(serffPlanMgmtBatch);
	}
	
	
	/**
	 * Method is used to soft delete or re-process SERFF_PLAN_MGMT_BATCH record.
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @return
	 */
	@RequestMapping(value = "/admin/getBatchTopPage", method = RequestMethod.GET)
	public String cancelRequest(Model model, HttpServletRequest request) {
		
		LOGGER.info("cancelRequest Begin");
		SerffPlanMgmtBatch serffPlanMgmtBatch = null;
		
		try {
			String cancelId = request.getParameter("cancelid");
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("request id to cancel :" + SecurityUtil.sanitizeForLogging(cancelId));
			}
			
			if (StringUtils.isNotBlank(cancelId) && StringUtils.isNumeric(cancelId)) {
				serffPlanMgmtBatch = serffService.getSerffPlanMgmtBatchById(Long.parseLong(cancelId));
				serffPlanMgmtBatch.setIsDeleted(SerffPlanMgmtBatch.IS_DELETED.Y);
				// iSerffPlamMgmtBatch.save(serffPlanMgmtBatch);
				serffPlanMgmtBatch = serffService.saveSerffPlanMgmtBatch(serffPlanMgmtBatch);
				model.addAttribute("serffStatus", "Canceled!!");
			}
			
			String reprocessId = request.getParameter("reprocessId");
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("request id to reprocess :" + SecurityUtil.sanitizeForLogging(reprocessId));
			}
			
			if (StringUtils.isNotBlank(reprocessId) && StringUtils.isNumeric(reprocessId)) {
				serffPlanMgmtBatch = serffService.getSerffPlanMgmtBatchById(Long.parseLong(reprocessId));
				serffPlanMgmtBatch.setBatchStatus(SerffPlanMgmtBatch.BATCH_STATUS.WAITING);
				// iSerffPlamMgmtBatch.save(serffPlanMgmtBatch);
				serffPlanMgmtBatch = serffService.saveSerffPlanMgmtBatch(serffPlanMgmtBatch);
				model.addAttribute("serffStatusrepro", "reprocessed!!");
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception in cancelRequest", e);
		}
		finally {
			LOGGER.info("cancelRequest End");
		}
		return RETURN_SERFF_TOP_PAGE;
	}

	/**
	 * Method is used to returns TOPPage for post requests
	 */
	@RequestMapping(value = "/admin/getBatchTopPage", method = RequestMethod.POST)
	public String getTopPage(Model model, HttpServletRequest request) {
		return RETURN_SERFF_TOP_PAGE;
	}
	
	/**
	 * Method is used to show files in table view.
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @return destination page.
	 * @throws FileNotFoundException
	 */
	@RequestMapping(value = "/admin/getBatchBottomLeft", method = RequestMethod.GET)
	public String showFiles(Model model, HttpServletRequest request) throws FileNotFoundException {
		
		String fileID = request.getParameter("fileid");
		SerffPlanMgmtBatch serffPlanMgmtBatch = null;
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("file id " + SecurityUtil.sanitizeForLogging(fileID));
		}
		
		if (StringUtils.isNotBlank(fileID) && StringUtils.isNumeric(fileID)) {
		
			serffPlanMgmtBatch = serffService.getSerffPlanMgmtBatchById(Long.parseLong(fileID));
			
			if (null != serffPlanMgmtBatch) {
				
				final String ftpUploadPath = SerffConstants.getFTPUploadPlanPath()
						+ serffPlanMgmtBatch.getIssuerId()
						+ serffPlanMgmtBatch.getState()
						+ serffPlanMgmtBatch.getId() + "/";
				
				try {
					List<String> files = null;
					
					if (null != ftpClient) {
						LOGGER.info("Fetching files from FTP " + ftpUploadPath);
						files = ftpClient.getFileNames(ftpUploadPath);
						model.addAttribute("FILE_NAME", files);
						model.addAttribute("FOLDER_NAME", ftpUploadPath);
					}
				}
				catch (Exception e) {
					LOGGER.error("Exception in showFiles", e);
				}finally{
					if (null != ftpClient) {
						ftpClient.close();
					}
				}
			}
		}
		return RETURN_BOTTOM_LEFT_PAGE;
	}

	/**
	 * Method is used to process SOAP UI request and process SERFF templates. 
	 * @param model, Model Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/executeSerffBatch", method = RequestMethod.POST)
	public String executeSerffBatch(Model model) {
		
		LOGGER.info("executeSerffBatch Begin");
		
		try {
			model.addAttribute(MSG_EXECUTE_BATCH, batchDataLoadService.processSerffPlanMgmtBatch());
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("executeSerffBatch End");
		}
		return RETURN_SERFF_UPLOAD_PAGE;
	}
	
	/**
	 * HIX-26615
	 * This method is added for download utility in the form of SOAP UI project. This method will replace the functionality of postSerffData.jsp page.
	 *  
	 * @return Stream of SOAP UI project file.
	 */
	@RequestMapping(value = "/admin/downloadSOAPProjectFile", method = RequestMethod.GET)
	public void downloadSOAPProjectForRequest(@RequestParam("requestIdDownload") String requestIdDownload, HttpServletResponse response){
		
		try {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("downloadSOAPProjectForRequest() : requestIdDownload : " + SecurityUtil.sanitizeForLogging(requestIdDownload));
			}
			byte[] file = downloadSOAPUIProject.downloadSerffProject(Long.valueOf(requestIdDownload));
	
			if (file != null) {
				LOGGER.info("downloadSOAPProjectForRequest() : file length is  : " + file.length);
				response.setContentType("application/octet-stream");
				//Creating fileName - SERFF_SOAPUI_ipaddress_id_DDMMMYYYY_HHMM.zip
				InetAddress processIp = InetAddress.getLocalHost();
				SimpleDateFormat format = new SimpleDateFormat("ddMMMyyyy_HHmm");
				String fileName = "SERFF_SOAPUI_"+processIp.getHostAddress()+"_"+requestIdDownload+"_"+format.format(new Date())+".zip";
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("downloadSOAPProjectForRequest() : fileName : " + SecurityUtil.sanitizeForLogging(fileName));
				}
				response.setHeader("Content-Disposition", ("attachment;filename=" + fileName.replaceAll(UNSECURE_CHARS_REGEX, StringUtils.EMPTY).trim()));
				response.getOutputStream().write(file);
		        response.flushBuffer();
			}
			else {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("downloadSOAPProjectForRequest() : file is null. " + SecurityUtil.sanitizeForLogging(requestIdDownload));
				}
			}
		} catch (Exception e) {			
			LOGGER.error("Error occurred while downloading SOAP UI project for : " +SecurityUtil.sanitizeForLogging(requestIdDownload) + " : " + e.getMessage(), e);			
		}
	}
	
	/**
	 * Method is used to populate database column display value for benefits.
	 */
	@RequestMapping(value = "admin/processDisplayData", method = RequestMethod.POST)
	public String processDisplayData(@RequestParam("carrierSelect") String[] carrierList) {

		LOGGER.info("processDisplayData Begin");

		try {
			
			if (null != carrierList) {
				 serffService.getAllHealthBenefits(carrierList);
			}
			else {
				LOGGER.info("Carrier list is empty");
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("processDisplayData End");
		}
		return RETURN_PROCESS_PAGE;
	}
	
	/**
	 * Method is used to process dental display data
	 */
	@RequestMapping(value = "admin/processDentalDisplayData", method = RequestMethod.POST)
	public String processDentalDisplayData(@RequestParam("carrierSelect") String[] carrierList) {

		LOGGER.info("processDentalDisplayData Begin");

		try {
			
			if (null != carrierList) {
				 serffService.getAllDentalBenefits(carrierList);
			}
			else {
				LOGGER.info("Carrier list is empty");
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("processDisplayData End");
		}
		return RETURN_PROCESS_DENTAL_PAGE;
	}
	
	/**
	 * Method is used to re-process health plan display data
	 * @param reprocessID
	 * @return
	 */
	@RequestMapping(value = "admin/reprocessDisplayData", method = RequestMethod.POST)
	public String reprocessDisplayData(@RequestParam("reprocessID") String reprocessID, @RequestParam("applicableYear") Integer applicableYear) {

		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("reprocessDisplay Data Begin");
			LOGGER.info("reprocessID: " + SecurityUtil.sanitizeForLogging(reprocessID) + " and applicableYear: " + applicableYear);
		}

		try {
			
			if (StringUtils.isNotBlank(reprocessID) && null != applicableYear && 0 < applicableYear) {
				serffService.getAllHealthBenefitsByPlans(reprocessID , applicableYear, true);
			}
			else {
				LOGGER.error("ReprocessID(Issuer Plan Number Or HIOS Issuer Id) or Applicable Year is empty.");
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("reprocessDisplay Data End");
		}
		return RETURN_PROCESS_PAGE;
	}
	/**
	 * Method is used to re-process dental plan display data
	 * @param reprocessID
	 * @return
	 */
	@RequestMapping(value = "admin/reprocessDentalDisplayData", method = RequestMethod.POST)
	public String reprocessDentalDisplayData(@RequestParam("reprocessID") String reprocessID, @RequestParam("applicableYear") Integer applicableYear) {

		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("reprocessDentalDisplayData Begin");
			LOGGER.info("reprocessID: " + SecurityUtil.sanitizeForLogging(reprocessID) + " and applicableYear: " + applicableYear);
		}

		try {
			
			if (StringUtils.isNotBlank(reprocessID) && null != applicableYear && 0 < applicableYear) {
				serffService.getAllDentalBenefitsByPlans(reprocessID, applicableYear, true);
			}
			else {
				LOGGER.error("ReprocessID(Issuer Plan Number Or HIOS Issuer Id) or Applicable Year is empty.");
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("reprocessDisplayData End");
		}
		return RETURN_PROCESS_DENTAL_PAGE;
	}
	
	/**
	 * Method is used to process T1 display data for health plans
	 * @param reprocessID
	 * @return
	 */
	@RequestMapping(value = "admin/reprocessDisplayDataT1", method = RequestMethod.POST)
	public String reprocessDisplayDataT1(@RequestParam("reprocessID") String reprocessID, @RequestParam("applicableYear") Integer applicableYear) {

		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("reprocessDisplayDataT1 Begin");
			LOGGER.info("reprocessID: " + SecurityUtil.sanitizeForLogging(reprocessID) + " and applicableYear: " + applicableYear);
		}

		try {
			
			if (StringUtils.isNotBlank(reprocessID) && null != applicableYear && 0 < applicableYear) {
				serffService.getAllHealthBenefitsByPlans(reprocessID, applicableYear, false);
			}
			else {
				LOGGER.info("ReprocessID(Issuer Plan Number Or HIOS Issuer Id) or Applicable Year is empty.");
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("reprocessDisplayDataT1 End");
		}
		return RETURN_T1_PROCESS_PAGE;
	}
	
	/**
	 * Method is used to display data page
	 * @param reprocessID
	 * @return
	 */
	@RequestMapping(value = "admin/showDisplayDataT1", method = RequestMethod.POST)
	public String showDisplayDataT1(@RequestParam("reprocessID") String reprocessID) {

		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("showDisplayDataT1 Begin");
			LOGGER.info("reprocessID: " + SecurityUtil.sanitizeForLogging(reprocessID));
		}
	
		return RETURN_T1_PROCESS_PAGE;
	}
	
	
	/**
	 * Method is used to display required data for Upload Ancillary for STM/AME/Vision/Life/HCC/PLAN_DOCS Plans details. 
	 * @param model, Model Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/uploadSTMExcelForm", method = RequestMethod.GET)
	public String loadUploadExcelDetails(Model model) {
		
		LOGGER.info("Loading Ancillary for STM/AME/Vision/Life/HCC/PLAN_DOCS Plans Begin");
		
		try {
			setFields(model);
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("Loading Ancillary for STM/AME/Vision/Life/HCC/PLAN_DOCS Plans End");
		}
		return RETURN_EXCEL_UPLOAD_PAGE;
	}

	/**
	 * Method is used to load "Upload Ancillary" page. 
	 * @param model
	 * @param request
	 * @param uploadType
	 * @return
	 */
	@RequestMapping(value = "admin/uploadMain", method = { RequestMethod.GET, RequestMethod.POST })
	public String uploadMain(Model model, final HttpServletRequest request, @RequestParam("uploadType") String uploadType) {

		if ("ancillary".equalsIgnoreCase(uploadType)) {
			model.addAttribute(PAGE_NAME, "Upload AME/STM/HCC/VISION/LIFE Plans Data (Excel File) to FTP Server");
			model.addAttribute(PLANS_FOR, ANCILLARY);
			setIssuerAndTenantList(model);
		}
		else {
			model.addAttribute(PAGE_NAME, "Upload Medicaid (Excel File) to FTP Server");
			model.addAttribute(PLANS_FOR, MEDICAID);
		}
		return RETURN_EXCEL_UPLOAD_PAGE;
	}

	private void setIssuerAndTenantList(Model model) {
		model.addAttribute("issuerList", batchDataLoadService.getIssuerNameList());
		model.addAttribute("tenantList", batchDataLoadService.getTenantNameList());
		model.addAttribute("defaultTenantCode", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.DEFAULT_TENANT));
	}

	private String getAncillaryReturnPage(String returnPage, Model model) {

		String pageToReturn = null;
		
		if (StringUtils.isNotBlank(returnPage)) {
			
			if (returnPage.equalsIgnoreCase(MEDICAID)) {
				model.addAttribute(PAGE_NAME, MEDICAID_PAGE_HEADING);
				model.addAttribute(PLANS_FOR, MEDICAID);
				pageToReturn = RETURN_EXCEL_UPLOAD_PAGE;
			}
			else if (returnPage.equalsIgnoreCase(ANCILLARY)) {
				model.addAttribute(PAGE_NAME, ANCILLARY_PAGE_HEADING);
				model.addAttribute(PLANS_FOR, ANCILLARY);
				setIssuerAndTenantList(model);
				pageToReturn = RETURN_EXCEL_UPLOAD_PAGE;
			}
			else if (returnPage.equalsIgnoreCase(PLAN_DOCS)) {
				model.addAttribute(PAGE_NAME, PLAN_DOCS_PAGE_HEADING);
				model.addAttribute(PLANS_FOR, PLAN_DOCS);
				pageToReturn = returnPage;
			}
		}
		else {
			pageToReturn = RETURN_EXCEL_UPLOAD_PAGE;
		}
		return pageToReturn;
	}
	
	/**
	 * Method is used to Upload Ancillary for AME/STM/Vision/Life/HCC Excel to FTP. 
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @param fileList, MultipartFile Array
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/uploadSTMExcel", method = RequestMethod.POST)
	public String addSTMExcel(Model model, final HttpServletRequest request,
			@RequestParam(required = false)  String returnPage,
			@RequestParam("fileUploadExcel") MultipartFile[] fileList) {
		
		LOGGER.info("Uploading Ancillary for STM/AME/HCC/PLAN_DOCS/VISION/LIFE Begin");
		String pageToReturn = null;
		String planType = request.getParameter("planType");
		String issuerId = request.getParameter(CARRIER);
		String carrierText = request.getParameter(CARRIER_TEXT);
		String tenantList = request.getParameter(TENANT_TEXT);
		String description = request.getParameter("description");
		boolean ancillaryPage = StringUtils.isNotBlank(returnPage) && returnPage.equalsIgnoreCase(ANCILLARY);
		
		try {
			// Set Ancillary return page for STM/AME/HCC/PLAN_DOCS/VISION/LIFE
			pageToReturn = getAncillaryReturnPage(returnPage, model);
			
			if (StringUtils.isBlank(planType)) {
				model.addAttribute(MSG_ANCILLARY_EXCEL, "Please select Plan Type.");
				return pageToReturn;
			}
			
			boolean invalidParams = (StringUtils.isBlank(issuerId)
					 || StringUtils.isBlank(carrierText) || StringUtils.isBlank(tenantList));
			if (ancillaryPage && invalidParams) {
				model.addAttribute(MSG_ANCILLARY_EXCEL, "Required input parameters are missing.");
				return pageToReturn;
			}
			
			if (null == ftpClient) {
				model.addAttribute(MSG_ANCILLARY_EXCEL, "Not able to connect to GHIX SFTP Client.");
				return pageToReturn;
			}
			
			final String ftpUploadPath = getAncillaryUploadPath(planType);
			
			if(StringUtils.isBlank(ftpUploadPath)) {
				model.addAttribute(MSG_ANCILLARY_EXCEL, "Plan Type is not matched.");
			} else if(!ftpClient.isRemoteDirectoryExist(ftpUploadPath)) {
				ftpClient.createDirectory(ftpUploadPath);
				LOGGER.info("Created Directory Name with path: " + ftpUploadPath);
			}
			String operationType = getAncillaryOperationType(planType);
			
			if (StringUtils.isNotBlank(carrierText) && carrierText.length() > LEN_OF_CARRIER) {
				carrierText = carrierText.substring(SerffConstants.INITIAL_INDEX, LEN_OF_CARRIER);
			}
			SerffPlanMgmtBatch serffPlanMgmtBatch = serffUtils.createBatchRecord(issuerId, carrierText, null, SerffPlanMgmtBatch.EXCHANGE_TYPE.OFF.toString(), tenantList, operationType, SerffConstants.DEFAULT_USER_ID);
			serffPlanMgmtBatch = serffService.saveSerffPlanMgmtBatch(serffPlanMgmtBatch);
			uploadSTMExcels(model, planType, ftpUploadPath, fileList, description, serffPlanMgmtBatch);
		}
		catch (Exception e) {
			model.addAttribute(MSG_ANCILLARY_EXCEL, MSG_STM_UPLOAD_FAIL + e.getMessage());
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			LOGGER.info("Uploading Ancillary for STM/AME/HCC/PLAN_DOCS/Vision/Life End");
			if (null != ftpClient) {
				ftpClient.close();
			}
		}
		return pageToReturn;
	}
	
	private String getAncillaryUploadPath(String planType) {
		String ftpUploadPath = null;
		
		if (SerffConstants.PLAN_TYPE.STM.name().equalsIgnoreCase(planType)) {
			ftpUploadPath = SerffConstants.getSTMExcelUploadPath();
		}
		else if (SerffConstants.PLAN_TYPE.HCC.name().equalsIgnoreCase(planType)
				|| SerffConstants.PLAN_TYPE.AREA_FACTOR.name().equalsIgnoreCase(planType)) {
			ftpUploadPath = SerffConstants.getHCCExcelUploadPath();
		}
		else if (SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType)
				|| SerffConstants.PLAN_TYPE.AME_GENDER.name().equalsIgnoreCase(planType)
				|| SerffConstants.PLAN_TYPE.AME_AGE.name().equalsIgnoreCase(planType)) {
			ftpUploadPath = SerffConstants.getAMEExcelUploadPath();
		}
		else if (SerffConstants.PLAN_TYPE.PLAN_DOCS.name().equalsIgnoreCase(planType)) {
			ftpUploadPath = SerffConstants.getPlansDocUploadPath();
		}
		else if (SerffConstants.PLAN_TYPE.PLAN_MEDICAID.name().equalsIgnoreCase(planType)) {
			ftpUploadPath = SerffConstants.getPlansMedicaidUploadPath();
		}
		else if (SerffConstants.PLAN_TYPE.VISION.name().equalsIgnoreCase(planType)) {
			ftpUploadPath = SerffConstants.getPlansVisionUploadPath();
		}
		else if (SerffConstants.PLAN_TYPE.LIFE.name().equalsIgnoreCase(planType)) {
			ftpUploadPath = SerffConstants.getPlansLifeUploadPath();
		}
		return ftpUploadPath;
	}

	private String getAncillaryOperationType(String planType) {

		String operationType;
	
		if (SerffConstants.PLAN_TYPE.STM.name().equalsIgnoreCase(planType)) {
			operationType = SerffPlanMgmtBatch.OPERATION_TYPE.STM.toString();
		}
		else if (SerffConstants.PLAN_TYPE.HCC.name().equalsIgnoreCase(planType)) {
			operationType = SerffPlanMgmtBatch.OPERATION_TYPE.STM_HCC.toString();
		}
		else if (SerffConstants.PLAN_TYPE.AREA_FACTOR.name().equalsIgnoreCase(planType)) {
			operationType = SerffPlanMgmtBatch.OPERATION_TYPE.STM_AREA_FACTOR.toString();
		}
		else if (SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType)) {
			operationType = SerffPlanMgmtBatch.OPERATION_TYPE.AME_FAMILY.toString();
		}
		else if (SerffConstants.PLAN_TYPE.PLAN_DOCS.name().equalsIgnoreCase(planType)) {
			operationType = SerffPlanMgmtBatch.OPERATION_TYPE.PLAN_DOCS.toString();
		}
		else if (SerffConstants.PLAN_TYPE.PLAN_MEDICAID.name().equalsIgnoreCase(planType)) {
			operationType = SerffPlanMgmtBatch.OPERATION_TYPE.PLAN_MEDICAID.toString();
		}
		else if (SerffConstants.PLAN_TYPE.VISION.name().equalsIgnoreCase(planType)) {
			operationType = SerffPlanMgmtBatch.OPERATION_TYPE.VISION.toString();
		}
		else if (SerffConstants.PLAN_TYPE.AME_GENDER.name().equalsIgnoreCase(planType)) {
			operationType = SerffPlanMgmtBatch.OPERATION_TYPE.AME_GENDER.toString();
		}
		else if (SerffConstants.PLAN_TYPE.LIFE.name().equalsIgnoreCase(planType)) {
			operationType = SerffPlanMgmtBatch.OPERATION_TYPE.LIFE.toString();
		}
		else {
			operationType = SerffPlanMgmtBatch.OPERATION_TYPE.AME_AGE.toString();
		}
		return operationType;
	}

	/**
	 * Method is used to Upload Ancillary for STM/AME/HCC/PLAN_DOCS/VISION/LIFE Excel to FTP Server. 
	 */
	private void uploadSTMExcels(Model model, String planType, String ftpUploadPath, MultipartFile[] fileList, String description, SerffPlanMgmtBatch serffPlanMgmtBatch) throws UnknownHostException {
		SerffPlanMgmtBatch updatedSerffPlanMgmtBatch = serffPlanMgmtBatch;
		try {
			
			if (null == fileList) {
				model.addAttribute(MSG_ANCILLARY_EXCEL, "STM/AME/HCC/PLAN_DOCS/VISION/LIFE file List is empty.");
				return;
			}
			if (fileList.length > 1) {
				model.addAttribute(MSG_ANCILLARY_EXCEL, "Please load only one file at a time.");
				return;
			}
			
			StringBuilder fileName = null;
			
			for (MultipartFile multipartFile : fileList) {
				
				if (null == multipartFile) {
					continue;
				}
				
				fileName = new StringBuilder();
				fileName.append(serffPlanMgmtBatch.getId());
				fileName.append(SerffConstants.FTP_ANCILLARY_FILEID_SUFFIX);
				
				// Upload Excel sheet to FTP Server.
				if (SerffConstants.PLAN_TYPE.STM.name().equalsIgnoreCase(planType)) {
					fileName.append(FTP_STM_FILE);
				}
				else if(SerffConstants.PLAN_TYPE.HCC.name().equalsIgnoreCase(planType)) {
					fileName.append(FTP_HCC_FILE);
				}
				else if(SerffConstants.PLAN_TYPE.AREA_FACTOR.name().equalsIgnoreCase(planType)) {
					fileName.append(FTP_AREA_FACTOR_FILE);
				}
				else if (SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType)) {
					fileName.append(FTP_AME_FAMILY_FILE);
				}
				else if (SerffConstants.PLAN_TYPE.PLAN_DOCS.name().equalsIgnoreCase(planType)) {
					fileName.append(FTP_PLAN_DOCS_FILE);
				}
				else if (SerffConstants.PLAN_TYPE.PLAN_MEDICAID.name().equalsIgnoreCase(planType)) {
					fileName.append(FTP_PLAN_MEDICAID_FILE);
				}
				else if (SerffConstants.PLAN_TYPE.VISION.name().equalsIgnoreCase(planType)) {
					fileName.append(FTP_VISION_FILE);
				}
				else if (SerffConstants.PLAN_TYPE.AME_GENDER.name().equalsIgnoreCase(planType)) {
					fileName.append(FTP_AME_GENDER_FILE);
				}
				else if (SerffConstants.PLAN_TYPE.LIFE.name().equalsIgnoreCase(planType)) {
					fileName.append(FTP_LIFE_FILE);
				}
				else {
					fileName.append(FTP_AME_AGE_FILE);
				}
				fileName.append(multipartFile.getOriginalFilename());
                ftpClient.uplodadFile(multipartFile.getInputStream(), ftpUploadPath + fileName.toString());
                
				serffPlanMgmtBatch.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.COMPLETED);
				serffPlanMgmtBatch.setFtpEndTime(new Date());
				serffPlanMgmtBatch.setBatchStatus(SerffPlanMgmtBatch.BATCH_STATUS.WAITING);
				updatedSerffPlanMgmtBatch = serffService.saveSerffPlanMgmtBatch(serffPlanMgmtBatch);
				
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("FTP Uploaded File Name: " + SecurityUtil.sanitizeForLogging(fileName.toString()));
				}
    			// Fixed HP-FOD issue.
    			serffResourceUtil.releaseInputStream(multipartFile.getInputStream());
			}
			
			if (SerffConstants.PLAN_TYPE.PLAN_DOCS.name().equalsIgnoreCase(planType)) {
				model.addAttribute(MSG_ANCILLARY_EXCEL, "Plan Docs file Uploaded succesfully.");
				createProcessingRecord(TRANSFER_PLAN_DOCS, updatedSerffPlanMgmtBatch, fileName.toString(), description);
			}
			else if(SerffConstants.PLAN_TYPE.PLAN_MEDICAID.name().equalsIgnoreCase(planType)) {
				model.addAttribute(MSG_ANCILLARY_EXCEL, "Medicaid file Uploaded succesfully.");
				createProcessingRecord(TRANSFER_PLAN_MEDICAID, updatedSerffPlanMgmtBatch, fileName.toString(), description);
			}
			else {
				model.addAttribute(MSG_ANCILLARY_EXCEL, "AME/STM/HCC/VISION/LIFE file Uploaded succesfully.");
			}
		}
		catch (IOException e) {
			model.addAttribute(MSG_ANCILLARY_EXCEL, MSG_STM_UPLOAD_FAIL + e.getMessage());
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		catch (Exception e) {
			model.addAttribute(MSG_ANCILLARY_EXCEL, MSG_STM_UPLOAD_FAIL + e.getMessage());
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
	}

	
	private SerffPlanMgmt createProcessingRecord(String operationType, SerffPlanMgmtBatch serffPlanMgmtBatch, String fileName, String description) {
		SerffPlanMgmt trackingRecord = null;
		try {
			trackingRecord = serffUtils.createSerffPlanMgmtRecord(operationType,
					SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.P);
		} catch (UnknownHostException e) {
			LOGGER.error("Failed to create Serff tracking record: " + e.getMessage());
			return null;
		}
		trackingRecord.setRemarks(description);
        trackingRecord.setAttachmentsList(fileName);
        trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.W);
        trackingRecord.setRequestStateDesc("Uploaded succesfully and ready to load plans to DB.");
        trackingRecord = serffService.saveSerffPlanMgmt(trackingRecord);
		serffPlanMgmtBatch.setSerffReqId(trackingRecord);
		serffService.updateTrackingBatchRecordAsInProgress(serffPlanMgmtBatch);
        return trackingRecord;
	}
		
	
	/**
	 * Method is used to Upload PUF for Medicare Excel to FTP. 
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @param fileList, MultipartFile Array
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/uploadPUFExcel", method = RequestMethod.POST)
	public String uploadPUFExcel(Model model, final HttpServletRequest request,
			@RequestParam("operationType") String operationType,
			@RequestParam("fileUploadExcel") MultipartFile multipartFile,
			@RequestParam("description") String description) {
		
		LOGGER.info("Uploading PUF for Medicare Excel Begin");
		SerffPlanMgmt trackingRecord = null;
		
		try {

			if (StringUtils.isBlank(operationType)) {
				model.addAttribute(MSG_PUF_EXCEL, "Please select Medicare Plan Type.");
				return RETURN_EXCEL_PUF_UPLOAD_PAGE;
			}

			if (null == ftpClient) {
				model.addAttribute(MSG_PUF_EXCEL, "Not able to connect to GHIX SFTP Client.");
				return RETURN_EXCEL_PUF_UPLOAD_PAGE;
			}

			final String ftpUploadPath = SerffConstants.getPlansMedicareUploadPath();
			if (StringUtils.isNotBlank(ftpUploadPath) && !ftpClient.isRemoteDirectoryExist(ftpUploadPath)) {
				ftpClient.createDirectory(ftpUploadPath);
				LOGGER.info("Created Directory Name with path: " + ftpUploadPath);
			}

			trackingRecord = serffUtils.createSerffPlanMgmtRecord(operationType, SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.P);
			trackingRecord.setRemarks(description);
			trackingRecord = serffService.saveSerffPlanMgmt(trackingRecord);

			StringBuilder fileName = new StringBuilder();
			fileName.append(trackingRecord.getSerffReqId());
			fileName.append(FTP_PLAN_MEDICARE_FILE);
			fileName.append(multipartFile.getOriginalFilename());
			ftpClient.uplodadFile(multipartFile.getInputStream(), ftpUploadPath + fileName.toString());

			// Update tracking record from SERFF_PLAN_MGMT table.
			trackingRecord.setAttachmentsList(fileName.toString());
			trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.W);
			trackingRecord.setRequestStateDesc("Uploaded succesfully and ready to load plans to DB.");
			trackingRecord = serffService.saveSerffPlanMgmt(trackingRecord);
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("FTP Uploaded File Name: " + SecurityUtil.sanitizeForLogging(fileName.toString()));
			}
			model.addAttribute(MSG_PUF_EXCEL, "Plan Docs file Uploaded succesfully.");
		}
		catch (Exception e) {

			if (null != trackingRecord) {
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
				trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
				trackingRecord.setRequestStateDesc(MSG_STM_UPLOAD_FAIL);
				trackingRecord = serffService.saveSerffPlanMgmt(trackingRecord);
			}
			model.addAttribute(MSG_PUF_EXCEL, MSG_STM_UPLOAD_FAIL + e.getMessage());
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {

			if (null != ftpClient) {
				ftpClient.close();
			}
			LOGGER.info("Uploading PUF for Medicare Excel End");
		}
		return RETURN_EXCEL_PUF_UPLOAD_PAGE;
	}

	/**
	 * Method is used to process SERFF Medicaid Plan templates. 
	 * @param model, Model Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/executePUFBatch", method = { RequestMethod.GET, RequestMethod.POST })
	public String executePUFBatch(Model model, @RequestParam("serffReqId") String serffReqId) {

		LOGGER.info("Execute Batch for Medicare plans Begin");

		try {

			if (!StringUtils.isNumeric(serffReqId)) {
				model.addAttribute(MSG_EXECUTE_BATCH, "Please select Medicare Plan Type.");
				return RETURN_EXCEL_PUF_STATUS_PAGE;
			}
			String statusMessage = medicarePlanReader.processMedicareExcelData(serffReqId);
			model.addAttribute(MSG_EXECUTE_BATCH, statusMessage);
			LOGGER.info("Status Message: " + statusMessage);
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {

			if (null != ftpClient) {
				ftpClient.close();
			}
			LOGGER.info("Execute Medicare plans End");
		}
		return RETURN_EXCEL_PUF_STATUS_PAGE;
	}

	/**
	 * Method is used to process SERFF Medicaid Plan templates. 
	 * @param model, Model Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/executeSerffMedicaidBatch", method = { RequestMethod.GET, RequestMethod.POST })
	public String executeSerffMedicaidBatch(Model model, @RequestParam("planType") String planType,
			@RequestParam(required = false) String serffReqId) {
		
		LOGGER.info("Execute Batch for Medicaid plans Begin");

		try {
			String folderPath = null;
			
			if (StringUtils.isBlank(planType)) {
				model.addAttribute(MSG_EXECUTE_BATCH, "Please select Plan Type.");
				return RETURN_PLAN_MEDICAID_STATUS_PAGE;
			}
			
			if (SerffConstants.PLAN_TYPE.PLAN_MEDICAID.name().equalsIgnoreCase(planType)
					&& StringUtils.isNumeric(serffReqId)) {
				folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansMedicaidUploadPath();
				model.addAttribute(MSG_EXECUTE_BATCH, medicaidExcelReader.processMedicaidExcelData(serffReqId, folderPath));
			}
			else {
				model.addAttribute(MSG_EXECUTE_BATCH, "Plan Type must be Medicaid Plan !!");
			}
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info(SecurityUtil.sanitizeForLogging(planType) + " Plan FTP Folder Path: " + folderPath);
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {

			if (null != ftpClient) {
				ftpClient.close();
			}
			LOGGER.info("Execute Medicaid plans End");
		}
		return RETURN_PLAN_MEDICAID_STATUS_PAGE;
	}

	/**
	 * Method is used to process SERFF Ancillary for STM/AME/HCC/PLAN_DOCS/VISION/LIFE Plan templates. 
	 * @param model, Model Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/executeSerffSTMBatch", method = { RequestMethod.GET, RequestMethod.POST })
	public String executeSerffSTMBatch(Model model, @RequestParam("planType") String planType,
			@RequestParam(required = false) String serffReqId) {
		
		LOGGER.info("Execute Ancillary Batch for STM/AME/HCC/PLAN_DOCS/VISION/LIFE plans Begin");
		boolean isSerffReqIdExist = StringUtils.isNotBlank(serffReqId);

		try {
			String folderPath = null;
			
			if (StringUtils.isBlank(planType)) {
				model.addAttribute(MSG_EXECUTE_BATCH, "Please select Plan Type.");
				return isSerffReqIdExist ? RETURN_PLAN_DOCS_STATUS_PAGE : RETURN_PLAN_EXCEL_UPLOAD_PAGE;
			}
			
			if (SerffConstants.PLAN_TYPE.STM.name().equalsIgnoreCase(planType)) {
				folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getSTMExcelUploadPath();
				model.addAttribute(MSG_EXECUTE_BATCH, stmPlanReader.processAllExcelTemplate(folderPath));
			}
			else if(SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType) 
					|| SerffConstants.PLAN_TYPE.AME_AGE.name().equalsIgnoreCase(planType)
					|| SerffConstants.PLAN_TYPE.AME_GENDER.name().equalsIgnoreCase(planType)) {
				folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getAMEExcelUploadPath();
				model.addAttribute(MSG_EXECUTE_BATCH, amePlanReader.processAllExcelTemplate(folderPath, planType));
			}
			else if(SerffConstants.PLAN_TYPE.HCC.name().equalsIgnoreCase(planType)) {
				folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getHCCExcelUploadPath();
				model.addAttribute(MSG_EXECUTE_BATCH, hccPlanReader.processAllExcelTemplate(folderPath , false));
			}
			else if(SerffConstants.PLAN_TYPE.AREA_FACTOR.name().equalsIgnoreCase(planType)) {
				folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getHCCExcelUploadPath();
				model.addAttribute(MSG_EXECUTE_BATCH, hccPlanReader.processAllExcelTemplate(folderPath , true));
			}
			else if (SerffConstants.PLAN_TYPE.PLAN_DOCS.name().equalsIgnoreCase(planType)
					&& StringUtils.isNumeric(serffReqId)) {
				folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansDocUploadPath();
				//Pending
				model.addAttribute(MSG_EXECUTE_BATCH, planDocumentExcelReader.processPlanDocumentExcel(serffReqId, folderPath));
			}
			else if (SerffConstants.PLAN_TYPE.VISION.name().equalsIgnoreCase(planType)) {
				folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansVisionUploadPath();
				//Pending
				model.addAttribute(MSG_EXECUTE_BATCH, visionExcelReader.processAllVisionTemplate(folderPath));
			}
			else if (SerffConstants.PLAN_TYPE.LIFE.name().equalsIgnoreCase(planType)) {
				folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansLifeUploadPath();
				//Pending
				model.addAttribute(MSG_EXECUTE_BATCH, lifeExcelReader.processAllLifeTemplate(folderPath));
			}
			else {
				model.addAttribute(MSG_EXECUTE_BATCH, "Plan Type must be STM/AME/HCC/VISION/LIFE/PLAN_DOCS !!");
			}
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info(SecurityUtil.sanitizeForLogging(planType) + " Plan FTP Folder Path: " + folderPath);
			}
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			if(null!= ftpClient){
				ftpClient.close();
			}
			LOGGER.info("Execute Ancillary Batch for STM/AME/HCC/PLAN_DOCS/VISION/LIFE plans End");
		}
		return isSerffReqIdExist ? RETURN_PLAN_DOCS_STATUS_PAGE : RETURN_PLAN_EXCEL_UPLOAD_PAGE;
	}
	
	/**
	 * This method is download Ancillary for STM/AME/HCC/PLAN_DOCS/VISION/LIFE Excel sheet from ECM.
	 */
	@RequestMapping(value = "/admin/downloadSTMExcel", method = RequestMethod.GET)
	public void downloadSTMExcel(@RequestParam("serffReqId") String serffReqId, HttpServletResponse response) {
		
		LOGGER.info("Download Ancillary for STM/AME/HCC/PLAN_DOCS/VISION/LIFE Excel Begin");
		
		try {
			
			if (StringUtils.isBlank(serffReqId) || !StringUtils.isNumeric(serffReqId)) {
				return;
			}
			
			final int BUFFER_SIZE = 4096;
			SerffPlanMgmt trackingRecord = serffService.getSerffPlanMgmtById(Long.parseLong(serffReqId));
			
			if (null == trackingRecord) {
				LOGGER.error("Tracking entry is not found in SerffPlanMgmt table.");
				return;
			}
			
			String baseDirectoryPath;
			
			if (TRANSFER_STM.equalsIgnoreCase(trackingRecord.getOperation())) {
				baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getSTMExcelUploadPath();
			}
			else if(TRANSFER_AME_FAMILY.equalsIgnoreCase(trackingRecord.getOperation())
					|| TRANSFER_AME_GENDER.equalsIgnoreCase(trackingRecord.getOperation())
					|| TRANSFER_AME_AGE.equalsIgnoreCase(trackingRecord.getOperation())) {
				baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getAMEExcelUploadPath();
			}
			else if (TRANSFER_PLAN_DOCS.equalsIgnoreCase(trackingRecord.getOperation())) {
				baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansDocUploadPath();
			}
			else if (TRANSFER_PLAN_MEDICAID.equalsIgnoreCase(trackingRecord.getOperation())) {
				baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansMedicaidUploadPath();
			}
			else if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(trackingRecord.getOperation())
					|| TRANSFER_MEDICARE_SP.equalsIgnoreCase(trackingRecord.getOperation())
					|| TRANSFER_MEDICARE_RX.equalsIgnoreCase(trackingRecord.getOperation())) {
				baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansMedicareUploadPath();
			}
			else if (TRANSFER_VISION.equalsIgnoreCase(trackingRecord.getOperation())) {
				baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansVisionUploadPath();
			}
			else if (TRANSFER_LIFE.equalsIgnoreCase(trackingRecord.getOperation())) {
				baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansLifeUploadPath();
			}
			else {
				baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getHCCExcelUploadPath();
			}
			
			if (SerffPlanMgmt.REQUEST_STATUS.S.equals(trackingRecord.getRequestStatus())) {
				baseDirectoryPath += SerffConstants.FTP_SUCCESS + SerffConstants.PATH_SEPERATOR;
			}
			else if (SerffPlanMgmt.REQUEST_STATUS.F.equals(trackingRecord.getRequestStatus())) {
				baseDirectoryPath += SerffConstants.FTP_ERROR + SerffConstants.PATH_SEPERATOR;
			}
			
			String fileName = trackingRecord.getAttachmentsList();
			LOGGER.info("File Name with base path: " + baseDirectoryPath + fileName);
			InputStream inputStream = ftpClient.getFileInputStream(baseDirectoryPath + fileName);
			
			if (inputStream != null) {
				// LOGGER.info("File length: " + fileContent.length);
				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", fileName.replaceAll(UNSECURE_CHARS_REGEX, StringUtils.EMPTY).trim()));
				
				OutputStream outputStream = response.getOutputStream();
				byte[] buffer = new byte[BUFFER_SIZE];
				int bytesRead = -1;
		        // Write bytes read from the Input-stream into the Output-stream
		        while ((bytesRead = inputStream.read(buffer)) != -1) {
		        	outputStream.write(buffer, 0, bytesRead);
		        }
		        inputStream.close();
				outputStream.flush();
				outputStream.close();
				response.flushBuffer();
			}
			else {
				LOGGER.info("File is null: " + fileName);
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);			
		}
		finally {
			LOGGER.info("Download Ancillary for STM/AME/HCC/PLAN_DOCS/VISION/LIFE Excel End");
			if(null!=ftpClient){
				ftpClient.close();
			}
		}
	}
	
	/**
	 * Method is used to show pmResponseXml from STM/AME/HCC/PLAN_DOCS/VISION/LIFE plans.
	 */
	@RequestMapping(value = "/admin/getSTMPlansStatusBottom", method = RequestMethod.GET)
	public String getSTMPlansStatusBottom(Model model, HttpServletRequest request) {
		
		LOGGER.info("Get Ancillary status for STM/AME/HCC/PLAN_DOCS/VISION/LIFE Plans to Bottom page Begin");
		final String returnPage = "serffSTMPlanAdminBottom";
		
		try {
			String serffReqId = request.getParameter("serffReqId");
			
			if (StringUtils.isBlank(serffReqId) || !StringUtils.isNumeric(serffReqId)) {
				LOGGER.info("Tracking Record(SerffPlanMgmt) is empty.");
				return returnPage;
			}
			SerffPlanMgmt trackingRecord = serffService.getSerffPlanMgmtById(Long.parseLong(serffReqId));
			model.addAttribute("pmResponseXml", trackingRecord.getPmResponseXml());
		}
		finally {
			LOGGER.info("Get Ancillary status for STM/AME/HCC/PLAN_DOCS/VISION/LIFE Plans to Bottom page End");
		}
		return returnPage;
	}

	/**
	 * Method is used to load Quotit Service page.
	 */
	@RequestMapping(value = "/admin/loadInvokeQuotitService", method = RequestMethod.GET)
	public String loadInvokeQuotitService(Model model) {

		LOGGER.debug("loadInvokeQuotitService() Start");
		setServerCurrentDate(model);
		LOGGER.debug("loadInvokeQuotitService() End");
		return RETURN_INVOKE_QUOTIT_SERVICE_PAGE;
	}

	/**
	 * Adding Current Date model attribute.
	 */
	private void setServerCurrentDate(Model model) {
		String currentDate = null;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		currentDate = df.format(new Date());
		model.addAttribute("currentDate", currentDate);
	}

	/**
	 * Method is used to invoke Quotit Service.
	 */
	@RequestMapping(value = "/admin/invokeQuotitService", method = RequestMethod.POST)
	public String invokeQuotitService(@RequestParam("insuranceType") String insuranceType,
			@RequestParam("effectiveDate") String effectiveDate, Model model) {

		LOGGER.info("invokeQuotitService() Start");
		String statusMessage = null;

		try {
			Map<String, Object> inputParamsMap = new HashMap<String, Object>();
			inputParamsMap.put(QuotitConstants.PARAM_REQUESTED_BY, ExtWSCallLogs.REQUESTED_BY.UI);
			inputParamsMap.put(QuotitConstants.PARAM_INSURANCE_TYPE, insuranceType);
			inputParamsMap.put(QuotitConstants.PARAM_EFFECTIVE_DATE, effectiveDate);

			statusMessage = planBenefitsClient.invokeQuotitService(inputParamsMap);
		}
		finally {

			if (StringUtils.isBlank(statusMessage)) {
				statusMessage = "Webservice Call failed to load plans.";
			}

			if (StringUtils.isNotBlank(insuranceType)) {
				model.addAttribute(MSG_EXECUTE_BATCH, insuranceType + ":" + SerffConstants.SPACE + statusMessage);
			}
			else {
				model.addAttribute(MSG_EXECUTE_BATCH, statusMessage);				
			}
			LOGGER.info(statusMessage);
			setServerCurrentDate(model);
			LOGGER.info("invokeQuotitService() End");
		}
		return RETURN_INVOKE_QUOTIT_SERVICE_PAGE;
	}

	/**
	 * Method is used to invoke GetZipCodeInfo Quotit Service.
	 */
	@RequestMapping(value = "/admin/invokeGetZipCodeInfoQuotitService", method = RequestMethod.POST)
	public String invokeGetZipCodeInfoQuotitService(Model model) {

		LOGGER.info("invokeGetZipCodeInfoQuotitService() Start");

		try {
			Map<String, Object> inputParamsMap = new HashMap<String, Object>();
			inputParamsMap.put(QuotitConstants.PARAM_REQUESTED_BY, ExtWSCallLogs.REQUESTED_BY.UI);

			String statusMessage = zipCodeInfoClient.invokeQuotitService(inputParamsMap);
			model.addAttribute(MSG_EXECUTE_BATCH, statusMessage);
			LOGGER.info(statusMessage);
		}
		finally {
			LOGGER.info("invokeGetZipCodeInfoQuotitService() End");
		}
		return RETURN_INVOKE_GET_ZIPCODE_INFO_QUOTIT_SERVICE_PAGE;
	}

	/**
	 * Method is used to Re-Apply Plan Benefits.
	 */
	@RequestMapping(value = "/admin/applyBenefitEdits", method = RequestMethod.POST)
	public String applyPlanBenefitEdits(@RequestParam("applicableYear") String applicableYear, @RequestParam("planHiosID") String issuerPlanNumber, Model model) {

		LOGGER.info("applyPlanBenefitEdits() Start");

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Applicable Year: " + applicableYear + ", IssuerPlanNumber: " + SecurityUtil.sanitizeForLogging(issuerPlanNumber));
		}

		try {
			setBenefitFields(model);

			String message = StringUtils.EMPTY;

			if (!StringUtils.isNumeric(applicableYear) || !StringUtils.isAlphanumeric(issuerPlanNumber)) {
				message = "Invalid parameters passed.";
			}
			else {
				// Invoke Plan management REST API for Reapply Benefits to Plan
				message = serffService.reapplyEditBenefitsToPlan(applicableYear.trim(), issuerPlanNumber.trim());
			}
			model.addAttribute("applyBenefitsMessage", message);
		}
		catch (Exception e) {
			LOGGER.error("Error occurred: " + e.getMessage(), e);
		}
		finally {
			LOGGER.info("applyPlanBenefitEdits() End");
		}
		return RETURN_APPLY_EDIT_BENEFIT_PAGE;
	}

	/**
	 * 
	 * @param planYear
	 * @return
	 */
	@RequestMapping(value = "/admin/getValidPlans", method = RequestMethod.POST)
	@ResponseBody
	public List<String> getValidPlans(@RequestParam(value = "planYearSelected", required = false) String planYearSelected) {
		return planBenefitEditService.getAllPlansForApplicableYear(planYearSelected);
	}

	/**
	 * Method is used to fetch all applicable year from PlanBenefitEdit table and and set them in Model attribute.
	 */
	private void setBenefitFields(Model model) {

		List<String> allApplicableYearlist = planBenefitEditService.getAllApplicableYear();

		if (!CollectionUtils.isEmpty(allApplicableYearlist)) {
			model.addAttribute("applicableYear", allApplicableYearlist);
		}
	}
	
	/**
	 * Method is used to required data for upload plan details. 
	 * @param model, Model Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/reapplyplanBenefits", method = {RequestMethod.GET, RequestMethod.POST})
	public String applyBenefitEdits(Model model) {
		
		LOGGER.info("viewRepplyBenefitEdits Begin");
		
		try {
			setBenefitFields(model);
		}
		catch (Exception e) {
			LOGGER.error("Failed in viewRepplyBenefitEdits", e);
		}
		finally {
			LOGGER.info("viewRepplyBenefitEdits End");
		}
		return RETURN_APPLY_EDIT_BENEFIT_PAGE;
	}
	
	/**
	 * Method is used to required data for upload plan details. 
	 */
	@RequestMapping(value = "/admin/showQMCPlanLoadStatusTop", method = { RequestMethod.GET, RequestMethod.POST })
	public String showQMCPlanLoadStatusSummary(@RequestParam(value = "parentTrackingID") String parentTrackingID, Model model) {

		LOGGER.debug("showQMCPlanLoadStatusSummary Begin");
		String qtStatsFormat = null;

		try {

			if (StringUtils.isNumeric(parentTrackingID)) {
				qtStatsFormat = carriersPlansBenefitsService.getTotalCountsOfQTStats(Integer.valueOf(parentTrackingID));
			}
			else {
				LOGGER.warn("Parent Tracking ID must be numeric.");
			}
		}
		catch (Exception e) {
			LOGGER.error("Failed in get Plan load status data: ", e);
		}
		finally {
			model.addAttribute("totalCountsOfQTStats", qtStatsFormat);
			LOGGER.debug("showQMCPlanLoadStatusSummary End with summary value : " + qtStatsFormat);
		}
		return RETURN_QUOTIT_REQUEST_RESPONSE_TOP;
	}
	
	@RequestMapping(value = "/admin/getValidIssuers", method = RequestMethod.POST)
	@ResponseBody
	public Map <String , String> getValidIssuers(@RequestParam(value = "stateCodeSelected", required = false) String stateCodeSelected) {
		List <Issuer>issuerList  = null;
		Map <String , String> issuerMap = null;
		issuerList = serffService.getAllIssuerForState(stateCodeSelected);
		String issuerShortName = null;
		String hiosIssuerId = null;
		if(!CollectionUtils.isEmpty(issuerList)){
			issuerMap = new HashMap<String, String> ();
			for(Issuer eachIssuer : issuerList){
				issuerShortName = eachIssuer.getShortName();
				hiosIssuerId= eachIssuer.getHiosIssuerId();
				if(null==issuerShortName){
					issuerShortName = SerffConstants.SPACE;
				}
				issuerMap.put(hiosIssuerId, issuerShortName);
			}
		}
		return issuerMap;
	}
	
	@RequestMapping(value = "/admin/getPlansForIssuer", method = RequestMethod.POST)
	@ResponseBody
	public boolean getAllPlansForIssuer(@RequestParam(value = "selectedIssuer") String hiosId){
		boolean isPlanExist = false;
		Long planCount = null;
		planCount = serffService.getPlanCountforIssuer(hiosId ,SerffConstants.NO_ABBR);
		if(null!=planCount && planCount.longValue()>0){
			isPlanExist = true;
		}
		return isPlanExist;
	}

	/**
	 * Method is used to get Quotit Request/Response depends on value of showRequestData argument. 
	 * @param model, Model Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/displayQuotitRequestResponse", method = RequestMethod.GET)
	public String getQuotitRequestResponse(Model model, @RequestParam("id") Integer id,
			@RequestParam("showRequestData") Boolean showRequestData) {

		LOGGER.info("getQuotitRequestResponse Begin");
		String quotitData = null;

		try {
			if (null != id && null != showRequestData) {
				quotitData = carriersPlansBenefitsService.getQuotitRequestResponse(id, showRequestData);
			}
			else {
				quotitData = "Invalid input parameters.";
				LOGGER.error(quotitData);
			}
			model.addAttribute("quotitData", quotitData);
		}
		catch (Exception e) {
			LOGGER.error("Failed to get Quotit Request/Response: ", e);
		}
		finally {
			LOGGER.info("getQuotitRequestResponse End");
		}
		return "displayQuotitRequestResponse";
	}
}
