package com.serff.admin.controller;

import static com.serff.util.SerffConstants.UNSECURE_CHARS_REGEX;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.serff.service.templates.IssuerService;
import com.serff.service.templates.PlanDocumentsJobService;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUploadBrochureUtils;

/**
 * This controller is for Brochure upload functionality.
 * 
 * @author vardekar_s
 */

@Controller
public class SerffUploadBrochureController {

	private static final Logger LOGGER = Logger.getLogger(SerffUploadBrochureController.class);
	
	private static final String CLASS_NAME = "SerffUploadBrochureController";
	private static final String RETURN_UPLOAD_BROCHURE_PAGE = "uploadBrochure";
	private static final String FTP_HOST_NOT_REACHABLE = "FTP host is not reachanble.";
	private static final String ECM_UPLOAD_SUCCESS = "Process completed successfully. Please check uploaded pdf status for details.";
	private static final String ECM_UPLOAD_FAILED = "Process completed with error. Please check uploaded pdf status for details.";
	private static final String FTP_DIRECTORY_DOES_NOT_EXIST = " does not exist. Please check configuration.";
	private static final String FTP_DIRECTORY = "Configured FTP directory ";
	
	private static final String UPLOAD_BROCHURE_MESSAGE = "uploadBrochureMessage";
	private static final String MSG_ANCILLARY_DOCUMENT = "uploadDocumentMessage";
	private static final String ERROR_MSG_ANCILLARY_DOCUMENT = "uploadDocumentErrorMessage";
	private static final String MSG_UPLOAD_ANCILLARY_FAIL = "Failed to upload Ancillary Document(PDF) at ECM server. ";
	private static final String RETURN_UPLOAD_ANCILLARY_PAGE = "uploadAncillaryDocs";
	private static final String RETURN_ECM_TEMPLATE_PAGE = "getEcmEmailTemplates";
	private static final String ERROR_MSG_DOWNLOAD_TEMPLATE = "downloadMsgTemplate";
	private static final String MSG_DOWNLOAD_ECM_FAIL = "Failed to download template from ECM server as file does not exist on ECM";
	private static final String MSG_DOWNLOAD_CONFIG_FAIL = "Failed to download template as file is not present in configuration.";
	private static final String ECM_DOWNLOAD ="Downloading file from ECM " ;
	private static final String ECM_ID = "ecmId" ;
	private static final String FILE_UPLOAD_MSG_PREFIX = "There are " ;
	private static final String FILE_UPLOAD_MSG_SUFFIX = " files to process." ;
	private static final String ECM_STATUS ="ecmStatus";
	private static final String ECM_RESULT = "ecmRsesult" ;
	private static final String ECM_MSG = "ecmMessage" ;
	private static final String DB_RESULT = "dbResult" ;
	private static final String DB_MSG = "dbMessage";

	@Autowired private ContentManagementService ecmService;
	@Autowired private IssuerService serffIssuerService;
	@Autowired private PlanDocumentsJobService planDocumentsJobService;
	@Autowired private GHIXSFTPClient ftpClient;

	@Autowired 
	private ApplicationContext appContext;
	@RequestMapping(value = "/admin/getUploadBrochureCount", method = RequestMethod.GET)
	public String getUploadBrochure(Model model) {
		LOGGER.info(CLASS_NAME + ".getfile count Begin");
		int uploadFileCount = 0;

		// Check whether ftpclient is reachable and configured directory is
		// present on FTP server.
		try {
			String ftpDirectory = SerffConstants.getSerffCsrHomePath()
					+ SerffConstants.getFtpServerUploadSubpath() + "/"
					+ SerffConstants.FTP_BROCHURE_PATH;
			if (null != ftpClient && !ftpClient.isRemoteDirectoryExist(ftpDirectory)) {
				LOGGER.error( FTP_DIRECTORY+FTP_DIRECTORY_DOES_NOT_EXIST);
				model.addAttribute(UPLOAD_BROCHURE_MESSAGE, FTP_DIRECTORY
						+ SerffConstants.getSerffCsrHomePath()
						+ SerffConstants.getFtpServerUploadSubpath() + "/"
						+ SerffConstants.FTP_BROCHURE_PATH
						+ FTP_DIRECTORY_DOES_NOT_EXIST);
				return RETURN_UPLOAD_BROCHURE_PAGE;
			}
		} catch (Exception e) {
			LOGGER.error(FTP_HOST_NOT_REACHABLE, e);
			model.addAttribute(UPLOAD_BROCHURE_MESSAGE, FTP_HOST_NOT_REACHABLE);
			return RETURN_UPLOAD_BROCHURE_PAGE;
		}

		try {
			SerffUploadBrochureUtils serffUploadBrochureUtils = SerffUploadBrochureUtils
					.getSerffUploadBrochureUtilsSingleton();
			// Get count of files to be uploaded in ECM.
			String dropboxPath = SerffConstants.getSerffCsrHomePath()
					+ SerffConstants.getFtpServerUploadSubpath() + "/"
					+ SerffConstants.FTP_BROCHURE_PATH;
			LOGGER.info(" Getting files to be uploaded from location "
					+ dropboxPath);
			uploadFileCount = serffUploadBrochureUtils.getFileCount(
					dropboxPath, ftpClient);
			model.addAttribute(UPLOAD_BROCHURE_MESSAGE, FILE_UPLOAD_MSG_PREFIX
					+ uploadFileCount + FILE_UPLOAD_MSG_SUFFIX);
			model.addAttribute("uploadFileCount", uploadFileCount);
			// model.addAttribute("remoteFileList", remoteFileList);
		} catch (Exception e) {
			LOGGER.error(FTP_HOST_NOT_REACHABLE , e);
			model.addAttribute(UPLOAD_BROCHURE_MESSAGE, FTP_HOST_NOT_REACHABLE);
			LOGGER.error("Exception occured : " , e);
		}finally{
			if(null!= ftpClient){
				ftpClient.close();
			}
		}

		return RETURN_UPLOAD_BROCHURE_PAGE;
	}

	@RequestMapping(value = "/admin/uploadBrochure", method = RequestMethod.GET)
	public String uploadBrochure(Model model) {
		LOGGER.info(CLASS_NAME + ". uploadBrochure Begin");
		String ftpDirectory = null;
		
		try {
			if (SerffUploadBrochureUtils.isUploadRunning()) {
				model.addAttribute(UPLOAD_BROCHURE_MESSAGE,
						"Upload process is already running.");
				return RETURN_UPLOAD_BROCHURE_PAGE;
			} else {
				// Check whether ftpclient is reachable.
				try {
						ftpDirectory = SerffConstants.getSerffCsrHomePath()
								+ SerffConstants.getFtpServerUploadSubpath() + "/"
								+ SerffConstants.FTP_BROCHURE_PATH;
					 
					if (!ftpClient.isRemoteDirectoryExist(ftpDirectory)) {
						LOGGER.error( FTP_DIRECTORY+FTP_DIRECTORY_DOES_NOT_EXIST);
						model.addAttribute(
								UPLOAD_BROCHURE_MESSAGE,
								FTP_DIRECTORY
										+ SerffConstants.getSerffCsrHomePath()
										+ SerffConstants.getFtpServerUploadSubpath()
										+ "/"
										+ SerffConstants.FTP_BROCHURE_PATH
										+ FTP_DIRECTORY_DOES_NOT_EXIST);
						return RETURN_UPLOAD_BROCHURE_PAGE;
					}
				} catch (Exception e) {
					LOGGER.error(
							FTP_HOST_NOT_REACHABLE , e);
					model.addAttribute(UPLOAD_BROCHURE_MESSAGE,
							FTP_HOST_NOT_REACHABLE);
					return RETURN_UPLOAD_BROCHURE_PAGE;
				}
				SerffUploadBrochureUtils serffUploadBrochureUtils = SerffUploadBrochureUtils
						.getSerffUploadBrochureUtilsSingleton();
				// Check whether files to upload count is zero.
			
				int uploadFileCount = serffUploadBrochureUtils.getFileCount(
						ftpDirectory, ftpClient);
				if (uploadFileCount == 0) {
					// Do not initiate upload process.
					LOGGER.info(CLASS_NAME
							+ ".Not initiating upload process due to zero file count.");
					model.addAttribute(UPLOAD_BROCHURE_MESSAGE, FILE_UPLOAD_MSG_PREFIX
							+ uploadFileCount + FILE_UPLOAD_MSG_SUFFIX);
					return RETURN_UPLOAD_BROCHURE_PAGE;
				} else if (uploadFileCount > 0) {
					// Initiate upload process.
					boolean uploadStatus = serffUploadBrochureUtils
							.uploadBrochure(ecmService,
									planDocumentsJobService, ftpClient , ftpDirectory);
					if (uploadStatus) {
						// All files upload process completed successfully as a
						// unit.
						LOGGER.info(CLASS_NAME + ".upload end successfully.");
						model.addAttribute(UPLOAD_BROCHURE_MESSAGE,
								ECM_UPLOAD_SUCCESS);
					} else {
						// There is some error while uploading files to ECM.
						LOGGER.info(CLASS_NAME + ".upload end with error.");
						model.addAttribute(UPLOAD_BROCHURE_MESSAGE,
								ECM_UPLOAD_FAILED);
					}
				}
				model.addAttribute("uploadFile Count", uploadFileCount);
			}

		} catch (Exception e) {
			// There is some error while uploading files to ECM.
			model.addAttribute(UPLOAD_BROCHURE_MESSAGE, ECM_UPLOAD_FAILED);
			LOGGER.error("Exception: " , e);
		}finally{
			if(null!= ftpClient){
				ftpClient.close();
			}
		}
		return RETURN_UPLOAD_BROCHURE_PAGE;
	}

	@RequestMapping(value = "/admin/uploadBrochure", method = RequestMethod.POST)
	public String uploadBrochureforPost(Model model) {
		LOGGER.info(CLASS_NAME + ".uploadBrochureforPost Begin");

		return RETURN_UPLOAD_BROCHURE_PAGE;
	}
	
	
	@RequestMapping(value = "/admin/checkECMStatus", method = RequestMethod.POST)
	public ModelAndView checkECMStatus() {
		LOGGER.info(CLASS_NAME + ".checkECMStatus Begin");
		
		ModelAndView model = new ModelAndView(ECM_STATUS);		
		
		try{	
			model.addObject("ecmServer", SerffConstants.getSerfEcmAtompuburl());
			
			Object testObject = planDocumentsJobService.getEnvironmentTestResult("ecm");
			
			if( null != testObject ){				
				if(testObject instanceof Issuer){					
					model.addObject(ECM_RESULT, "true");
					model.addObject(ECM_MSG, "SUCCESS!");
					model.addObject("issuer", testObject);
					model.addObject("issuerId", ((Issuer)testObject).getHiosIssuerId());
				}else{					
					model.addObject(ECM_RESULT, "false");
					model.addObject(ECM_MSG, testObject);
				}				
			}else{				
				LOGGER.info(CLASS_NAME + ".checkECMStatus. Unlikely to get value null here.");
			}			
		}catch(Exception e){			
			LOGGER.info(CLASS_NAME + ".checkECMStatus. Error occurred " , e);
		}
 
		return model;
	}
	
	
	@RequestMapping(value = "/admin/checkDBStatus", method = RequestMethod.POST)
	public ModelAndView checkDBStatus() {
		LOGGER.info(CLASS_NAME + ".checkDBStatus Begin");

		ModelAndView model = new ModelAndView(ECM_STATUS);		
		
		try{				
			Object testObject = planDocumentsJobService.getEnvironmentTestResult("db");							
					
			if( null != testObject ){		
					model.addObject("dbResultText",testObject.toString());		
					model.addObject(DB_RESULT, "true");
					model.addObject(DB_MSG, "SUCCESS!");
					model.addObject("dbTestInstance", testObject);					
				}else{					
					model.addObject(DB_RESULT, "false");
//					model.addObject(DB_MSG, testObject);
				}		
			
		}catch(Exception e){
			LOGGER.info(CLASS_NAME + ".checkDBStatus. Error occurred " , e);
		}
		
		return model;
	}
	
	/**
	 * This method downloads logo from ECM.
	 * @param issuerId
	 * @return
	 */
	@RequestMapping(value = "/admin/serff/logo/{id}", method = RequestMethod.GET)
	/*@PreAuthorize(PlanMgmtConstants.MANAGE_ISSUER)*/
	@ResponseBody public byte[] getIssuerLogoById(@PathVariable("id") String issuerId) {
		byte[] bytes = null;
		
		try {
			String encodedFileName = serffIssuerService.getIssuerByHiosID(issuerId).getCompanyLogo();
			
			if (StringUtils.isNotBlank(encodedFileName)) {
				bytes = ecmService.getContentDataById(encodedFileName);			
			}
		}
		catch (Exception e) {
			LOGGER.info( "checkDBStatus. Error occurred " ,e);
		}
		return bytes;
	}
	
	/**
	 * This method downloads PM report excel from ECM.
	 * 
	 * @param response
	 * @param pmReportId
	 * @return
	 * @throws ContentManagementServiceException
	 * @throws IOException
	 */
	@RequestMapping(value = "/admin/serff/filedownloadbyid", method = RequestMethod.GET)
	public ModelAndView downloadFileById(HttpServletResponse response,
			@ModelAttribute("documentId") String pmReportId) {

		byte[] bytes = null;
		try {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info(ECM_DOWNLOAD + SecurityUtil.sanitizeForLogging(pmReportId));
			}
			bytes = ecmService.getContentDataById(pmReportId);

			if(null != bytes){
				Content content = ecmService.getContentById(pmReportId);
				response.setHeader("Content-Disposition", "attachment;filename=" + content.getTitle().replaceAll(UNSECURE_CHARS_REGEX, StringUtils.EMPTY).trim());
				response.setContentType("application/octet-stream");
				FileCopyUtils.copy(bytes, response.getOutputStream());
			}
			
		} catch (Exception e) {
			LOGGER.error("Error occurred while downloading file from ECM ", e);
		}

		return null;
	}
	
	/**
	 * Method is used to Upload Ancillary Document(PDF) to ECM Server. 
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @param fileList, MultipartFile Array
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/uploadAncillaryDocs", method = RequestMethod.POST)
	public String uploadAncillaryDocument(Model model, final HttpServletRequest request,
			@RequestParam("fileUploadDocument") MultipartFile fileDocument) {
		
		LOGGER.info("Uploading Ancillary Document for AME/STM/HCC Begin");
		String uploadDocumentMessage = MSG_UPLOAD_ANCILLARY_FAIL;
		
		try {
			
			if (null == fileDocument) {
				uploadDocumentMessage = "Ancillary Document file is empty.";
				return RETURN_UPLOAD_ANCILLARY_PAGE;
			}
			
			if (StringUtils.isBlank(fileDocument.getOriginalFilename())) {
				uploadDocumentMessage = "Ancillary Document file Name is empty.";
				return RETURN_UPLOAD_ANCILLARY_PAGE;
			}
			
			SerffUploadBrochureUtils utils = SerffUploadBrochureUtils.getSerffUploadBrochureUtilsSingleton();
			
			if (null == utils) {
				uploadDocumentMessage = "SerffUploadBrochureUtils is null.";
				return RETURN_UPLOAD_ANCILLARY_PAGE;
			}
			uploadDocumentMessage = utils.uploadAncillaryDocumentToECM(fileDocument.getInputStream(), fileDocument.getOriginalFilename(),
					planDocumentsJobService, ecmService);
		}
		catch (Exception ex) {
			uploadDocumentMessage = MSG_UPLOAD_ANCILLARY_FAIL + ex.getMessage();
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			
			if (StringUtils.isNotBlank(uploadDocumentMessage)) {
				model.addAttribute(ERROR_MSG_ANCILLARY_DOCUMENT, uploadDocumentMessage);
			}
			else {
				uploadDocumentMessage = "Ancillary Document has been uploaded successfully !!";
				model.addAttribute(MSG_ANCILLARY_DOCUMENT, uploadDocumentMessage);
				LOGGER.info(uploadDocumentMessage);
			}
			// Fixed HP-FOD issue.
			try {
				if (null != fileDocument) {
					SerffResourceUtil.closeInputStream(fileDocument.getInputStream());
				}
			}
			catch (IOException e) {
				LOGGER.error("Exception occured while closing File Input Stream.");
			}
			LOGGER.info("Uploading Ancillary for AME/STM/HCC End");
		}
		return RETURN_UPLOAD_ANCILLARY_PAGE;
	}
	
	
	/**
	 * This method is used to download Email templates from ECM.
	 */
	@RequestMapping(value = "/admin/emailTemplate/Download", method = RequestMethod.GET)
	public String downloadMailTemplates(Model model, String templateLocation, HttpServletResponse response) {

		LOGGER.info("downloadMailTemplates() Starts");

		try {

			String attachmentName = "attachment.html";
			if (!StringUtils.isEmpty(templateLocation)) {

				if (templateLocation.contains("/")) {
					attachmentName = templateLocation.split("/")[1];
				}
				else {
					attachmentName = templateLocation;
				}
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Attachment Filename : " + SecurityUtil.sanitizeForLogging(attachmentName));
			}

			if ("Y".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE))) {
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Downloading mail template from ECM. Filename : " + SecurityUtil.sanitizeForLogging(templateLocation));
				}

				String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH);
				ecmTemplateFolderPath += templateLocation;
				byte[] ecmContent = ecmService.getContentDataByPath(ecmTemplateFolderPath);

				if (null != ecmContent) {
					response.setHeader("Content-Disposition", "attachment;filename=" + attachmentName.replaceAll(UNSECURE_CHARS_REGEX, StringUtils.EMPTY).trim());
					response.setContentType("application/octet-stream");
					FileCopyUtils.copy(ecmContent, response.getOutputStream());
					response.flushBuffer();
				}
				else {
					model.addAttribute("notPresent");
					model.addAttribute(ERROR_MSG_DOWNLOAD_TEMPLATE, MSG_DOWNLOAD_ECM_FAIL);
					LOGGER.info(" downloadMailTemplates ends");
				}
			}
			else {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Downloading mail template from ghix-config.jar. Location : " + SecurityUtil.sanitizeForLogging(templateLocation));
				}
				Resource resource = appContext.getResource("classpath:" + templateLocation);
				if (resource.exists()) {
					InputStream inputStream = null;

					try {
						inputStream = resource.getInputStream();
						String templateContent = IOUtils.toString(inputStream, "UTF-8");
						response.setHeader("Content-Disposition", "attachment;filename=" + attachmentName.replaceAll(UNSECURE_CHARS_REGEX, StringUtils.EMPTY).trim());
						response.setContentType("application/octet-stream");
						FileCopyUtils.copy(templateContent.getBytes(), response.getOutputStream());
					}
					catch (IOException e) {
						LOGGER.error("Exception occurred while downloading mail templates.", e);
					}
					finally {

						if (inputStream != null) {
							inputStream.close();
						}
					}
				}
				else {
					model.addAttribute("notPresent");
					model.addAttribute(ERROR_MSG_DOWNLOAD_TEMPLATE, MSG_DOWNLOAD_CONFIG_FAIL);
					LOGGER.info("downloadMailTemplates ends");
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("downloadMailTemplates() End");
		}
		return RETURN_ECM_TEMPLATE_PAGE;
	}
	
	/**
	 * This method is used to get contents of Email templates from ECM
	 * @param model
	 * @param request
	 * @return
	 */
	
	@RequestMapping(value = "/admin/getEcmTemplateBottomPage", method = RequestMethod.GET)
	@ResponseBody public byte[] getECMTemplatesBottom(Model model, HttpServletRequest request) {
		byte[] bytes = null;
		LOGGER.info(" getECMTemplatesBottom Start");
		String ecmDocId = request.getParameter(ECM_ID);
		try {
			
			if (StringUtils.isNotBlank(ecmDocId)) {
				bytes = ecmService.getContentDataById(ecmDocId);			
			}
		}
		catch (Exception e) {
			LOGGER.error(" Error occurre",e);
		}
		LOGGER.info(" getECMTemplatesBottom ends");
		return bytes;
	}
	
	
@RequestMapping(value = "/admin/crosswalk/DownloadExcel", method = RequestMethod.GET)
	
	@ResponseBody public byte[] getCrosswalkFile(HttpServletRequest request ,HttpServletResponse response) {
		byte[] bytes = null;
		try {
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Disposition", ("attachment; filename="+"CrosswalkPlans.xlsx"));
			String ecmDocId = request.getParameter(ECM_ID);
			if(null!=ecmDocId){
				bytes = ecmService.getContentDataById(ecmDocId);
			}else{
				LOGGER.error("Failed to read contents from ECM");
			}
		
		} catch (Exception e) {
			LOGGER.error( " Exception at " ,e);
		}
		return bytes;
	}
	
}
