package com.serff.admin.controller;

import static com.serff.util.SerffConstants.PATH_SEPERATOR;
import static com.serff.util.SerffConstants.UNSECURE_CHARS_REGEX;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.platform.util.SecurityUtil;
import com.serff.util.unixshell.SSHServices;
import com.serff.util.unixshell.SSHVo;

/**
 * Controller class for Unix Shell Commands.
 * 
 * @author Bhavin Parmar
 * @since 28 May, 2014
 */
@Controller
public class UnixShellController {

	private static final Logger LOGGER = Logger.getLogger(UnixShellController.class);
	private static final String EXCEPTIONS = "Exception: ";
	private static final String RETURN_UPLOAD_PAGE = "serffUnixShell";
	private static final String CONSOLE_STATUS = "consoleStatus";
	private static final String PARAM_COMMAND_NAME = "commandName";
	private static final String PARAM_LOG_FILE = "selectLogFile";
	private static final String PARAM_GREP_CMD = "grepCmd";
	private static final String PARAM_SERVER_IP = "ServerIP";
	private static final String PARAM_SERVER_NAME = "ServerName";
	private static final String PARAM_SYSTEM_DATE = "systemDate";
	private static final String PARAM_CMD = "cmd";
	private static final String PARAM_CMD_RESPONSE = "cmdResponse";
	private static final String PARAM_CMD_ERROR = "cmdError";
	private static final String PATH_TOMCAT_WEBAPPS = "tomcat/webapps/";
	private static final String PATH_GHIX_LOGS = "ghixhome/ghix-setup/logs/";
	private static final int ZERO = 0;
	private static final int READ_LINES = 1000;

	@Autowired private SSHServices sshServices;

	/**
	 * Method is used to load Unix Console page with server details. 
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @return destination page.
	 */
	@RequestMapping(value = "/admin/displayUnixConsole", method = RequestMethod.GET)
	public String loadUnixConsole(Model model, HttpServletRequest request) {
		
		LOGGER.debug("loadUnixConsole Begin");
		
		try {
			
			if (sshServices.validSERFFCredentials()) {
				model.addAttribute(CONSOLE_STATUS, " SERFF Monitoring user credentials are valid.");
				// set Server Details [IP, Name & System Date]
				setServerDetails(model);
			}
			else {
				model.addAttribute(CONSOLE_STATUS, " Please provide SERFF Monitoring user credentials at configuration.properties.");
			}
		}
		finally {
			LOGGER.debug("loadUnixConsole End");
		}
		return RETURN_UPLOAD_PAGE;
	}

	/**
	 * Method is used to execute command on Unix prompt and return results.
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @param response, HttpServletResponse Object
	 * @return Unix Commannd and it's result.
	 */
	@RequestMapping(value = "/admin/executeCommand", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> executeCommand(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		LOGGER.debug("executeCommand Begin");
		Map<String, Object> responseData = null;
		SSHVo sshVo = null;
		
		try {
			responseData = new HashMap<String, Object>();
			String commandName = request.getParameter(PARAM_COMMAND_NAME);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Command Name: " + SecurityUtil.sanitizeForLogging(commandName));
			}
			
			if (StringUtils.isBlank(commandName)) {
				responseData.put(PARAM_CMD, StringUtils.EMPTY);
				responseData.put(PARAM_CMD_RESPONSE, "Command Name is empty.");
				LOGGER.error(responseData);
				return responseData;
			}
			// set Server Details [IP, Name & System Date]
			setServerDetails(model);
			
			// /home/tomcat/phix1/tomcat/webapps/exchange-hapi
			String realPath = request.getSession().getServletContext().getRealPath(PATH_SEPERATOR);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Real Path: " + SecurityUtil.sanitizeForLogging(realPath));
			}
			
			if (StringUtils.isBlank(realPath)) {
				responseData.put(PARAM_CMD, StringUtils.EMPTY);
				responseData.put(PARAM_CMD_RESPONSE, "Real Path is empty.");
				LOGGER.error(responseData);
				return responseData;
			}
			
			String serverPath = realPath.substring(ZERO, realPath.lastIndexOf(PATH_TOMCAT_WEBAPPS));
			String serverLogsPath = serverPath + PATH_GHIX_LOGS;
			String serverWarPath = serverPath + PATH_TOMCAT_WEBAPPS;
			String selectedLogFile = request.getParameter(PARAM_LOG_FILE);
			
			if (StringUtils.isNotBlank(selectedLogFile)) {
				model.addAttribute(PARAM_LOG_FILE, selectedLogFile);
			}
			
			String grepCmd = request.getParameter(PARAM_GREP_CMD);
			
			if (StringUtils.isNotBlank(grepCmd)) {
				model.addAttribute(PARAM_GREP_CMD, grepCmd);
			}
			
			switch (SSHVo.COMMAND_NAME.valueOf(commandName)) {
					
				case HEAD:
					sshVo = sshServices.getHead(serverLogsPath + selectedLogFile, READ_LINES);
					break;
					
				case TAIL:
					sshVo = sshServices.getTail(serverLogsPath + selectedLogFile, READ_LINES);
					break;
					
				case FILE_INFO:
					sshVo = sshServices.getFileInfo(serverLogsPath + selectedLogFile);
					break;
					
				case GREP:
					sshVo = sshServices.getGrep(grepCmd, serverPath);
					break;
					
				case TOTAL_WAIT_THREADS:
					sshVo = sshServices.getTotalWaitThreads();
					break;
					
				case ESTABLISHED_CONNECTIONS:
					sshVo = sshServices.getTotalEstablishedConnection();
					break;
					
				case ORACLE_CONNECTIONS:
					sshVo = sshServices.getTotalDBConnections();
					break;
					
				case PING_ORACLE:
					sshVo = sshServices.getPingOracle();
					break;
					
				case PING_ECM:
					sshVo = sshServices.getPingECM();
					break;
					
				case CPU:
					sshVo = sshServices.getCPU();
					break;
					
				case CPU_DETAILS:
					sshVo = sshServices.getCPUDetails();
					break;
					
				case OS:
					sshVo = sshServices.getOSInfo();
					break;
					
				case DATE:
					sshVo = sshServices.getSystemDate();
					break;
					
				case MEMORY:
					sshVo = sshServices.getMemory();
					break;
					
				case MEMORY_DETAILS:
					sshVo = sshServices.getFullMemoryDetails();
					break;
					
				case MEMORY_PROFILE:
					sshVo = sshServices.getFullMemoryProfile();
					break;
					
				case FREE_DISK_SPACE:
					sshVo = sshServices.getFreeDiskSpace();
					break;
					
				case JAVA_PROCESSES:
					sshVo = sshServices.getJavaProcessList();
					break;
					
				case DEAMON_PROCESSES:
					sshVo = sshServices.getDaemonDetails();
					break;
					
				case SERFF_WAR:
					sshVo = sshServices.getSERFFWarInfo(serverWarPath);
					break;
					
				case PM_WAR:
					sshVo = sshServices.getPlanMgmtWarInfo(serverWarPath);
					break;
				
				default:
					LOGGER.error("Invalid Command Name.");
			}
			setModel(model, sshVo);
		}
		catch (Exception e) {
			LOGGER.error(EXCEPTIONS + e.getMessage(), e);
		}
		finally {
			
			if (null != sshVo) {
				responseData.put(PARAM_CMD, sshVo.getCommand());
				responseData.put(PARAM_CMD_RESPONSE, sshVo.getResponse());
			}
			LOGGER.debug("executeCommand End");
		}
		return responseData;
	}

	/**
	 * This method is download log file from Unix Server.
	 * @param model, Model Object
	 * @param request, HttpServletRequest Object
	 * @param response, HttpServletResponse Object
	 */
	@RequestMapping(value = "/admin/downloadFile", method = RequestMethod.POST)
	public void downloadFile(Model model, HttpServletRequest request, HttpServletResponse response) {
		
		LOGGER.debug("downloadFile Begin");
		
		try {
			String realPath = request.getSession().getServletContext().getRealPath(PATH_SEPERATOR);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Real Path: " + SecurityUtil.sanitizeForLogging(realPath));
			}
			
			if (StringUtils.isBlank(realPath)) {
				LOGGER.error("Real Path is empty.");
				return;
			}
			
			String serverPath = realPath.substring(ZERO, realPath.lastIndexOf(PATH_TOMCAT_WEBAPPS));
			String serverLogsPath = serverPath + PATH_GHIX_LOGS;
			String selectedLogFile = request.getParameter(PARAM_LOG_FILE);
			
			if (StringUtils.isBlank(selectedLogFile)) {
				LOGGER.error("File name is Empty.");
				return;
			}
			model.addAttribute(PARAM_LOG_FILE, selectedLogFile);
			
			final int BUFFER_SIZE = 4096;
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Downloading File Name with base path: " + SecurityUtil.sanitizeForLogging(serverLogsPath + selectedLogFile));
			}
			InputStream inputStream = sshServices.getFileInputStream(serverLogsPath + selectedLogFile);
			
			if (inputStream != null) {
				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", selectedLogFile.replaceAll(UNSECURE_CHARS_REGEX, StringUtils.EMPTY).trim()));
				
				OutputStream outputStream = response.getOutputStream();
				byte[] buffer = new byte[BUFFER_SIZE];
				int bytesRead = -1;
		        // Write bytes read from the Input-stream into the Output-stream
		        while ((bytesRead = inputStream.read(buffer)) != -1) {
		        	outputStream.write(buffer, ZERO, bytesRead);
		        }
		        inputStream.close();
				outputStream.flush();
				outputStream.close();
				response.flushBuffer();
			}
			else {		
				LOGGER.error("File is null: " + SecurityUtil.sanitizeForLogging(selectedLogFile));			
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);			
		}
		finally {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("downloadFile End");
			}
		}
	}

	private void setModel(Model model, SSHVo sshVo) {
		
		if (null != sshVo) {
			model.addAttribute(PARAM_CMD, sshVo.getCommand());
			model.addAttribute(PARAM_CMD_RESPONSE, sshVo.getResponse());
			model.addAttribute(PARAM_CMD_ERROR, sshVo.getErrorMsg());
		}
	}

	private void setServerDetails(Model model) {
		
		SSHVo sshVo = sshServices.getServerName();
		model.addAttribute(PARAM_SERVER_NAME, sshVo.getResponse());
		sshVo = null;
		
		sshVo = sshServices.getServerIP();
		model.addAttribute(PARAM_SERVER_IP, sshVo.getResponse());
		sshVo = null;
		
		sshVo = sshServices.getSystemDate();
		model.addAttribute(PARAM_SYSTEM_DATE, sshVo.getResponse());
		sshVo = null;
	}
}
