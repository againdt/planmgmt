package com.serff.admin.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.serff.service.BatchDataLoadService;

/**
 * Controller class is used to implement REST APIs for SERFF. 
 * @since Oct 16, 2017
 */
@Controller("serffRestController")
@RequestMapping(value = "/serff")
public class SerffRestController {

	private static final Logger LOGGER = Logger.getLogger(SerffRestController.class);

	@Autowired private BatchDataLoadService batchDataLoadService;

	public SerffRestController() {
	}

	/**
	 * For testing purpose
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to GHIX-SERFF module, invoke SerffRestController()");
		return "Welcome to GHIX-SERFF module, invoke SerffRestController";
	}

	/**
	 * Method is used to check waiting plans to load.
	 */
	@RequestMapping(value = "/checkWaitingPlansToLoad", method = RequestMethod.GET)
	@ResponseBody
	public Boolean checkWaitingPlansToLoad() {

		LOGGER.debug("checkWaitingPlansToLoad() Start");
		Boolean hasWaitingPlans = Boolean.FALSE;

		try {
			hasWaitingPlans = batchDataLoadService.checkWaitingPlansToLoad();
		}
		finally {
			LOGGER.debug("checkWaitingPlansToLoad() End with hasWaitingPlans: " + hasWaitingPlans);
		}
		return hasWaitingPlans;
	}

	/**
	 * Method is used to execute waiting batch.
	 */
	@RequestMapping(value = "/executeBatch", method = RequestMethod.GET)
	@ResponseBody
	public String executeBatch() {

		LOGGER.debug("executeBatch() Start");
		String executeBatchResponse = null;

		try {
			executeBatchResponse = batchDataLoadService.processSerffPlanMgmtBatch();
		}
		finally {
			LOGGER.debug("executeBatch() End with executeBatchResponse: " + executeBatchResponse);
		}
		return executeBatchResponse;
	}
}
