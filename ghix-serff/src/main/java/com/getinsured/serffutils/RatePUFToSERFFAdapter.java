package com.getinsured.serffutils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.QhpApplicationRateGroupVO;
import com.serff.template.plan.QhpApplicationRateHeaderVO;
import com.serff.template.plan.QhpApplicationRateItemVO;

/**
 * Class is used to generate Rates Template in XML format from PUF Excel data.
 * 
 * @author Bhavin Parmar
 * @since Aug 8, 2016
 */
public class RatePUFToSERFFAdapter extends PUFToSERFFAdapter {

	private static final int DATA_OFFSET = 6;
	private static final int DATA_ISSUER_HIOS_ID_COL_INDEX = 0;
	private static final int DATA_FEDRAL_TIN_COL_INDEX = 1;
	private static final int DATA_RATE_EFFECTIVE_DATE_COL_INDEX = 2;
	private static final int DATA_RATE_EXPIRATION_DATE_COL_INDEX = 3;
	private static final int DATA_PLAN_ID_COL_INDEX = 4;
	private static final int DATA_RATING_AREA_ID_COL_INDEX = 5;
	private static final int DATA_TOBACCO_COL_INDEX = 6;
	private static final int DATA_AGE_COL_INDEX = 7;
	private static final int DATA_INDIVIDUAL_RATE_COL_INDEX = 8;
	private static final int DATA_INDIVIDUAL_TOBACCO_RATE_COL_INDEX = 9;
	private static final int DATA_COUPLE_COL_INDEX = 10;
	private static final int DATA_PRIMARY_SUBSCRIBER_AND_ONE_DEPENDENT_COL_INDEX = 11;
	private static final int DATA_PRIMARY_SUBSCRIBER_AND_TWO_DEPENDENTS_COL_INDEX = 12;
	private static final int DATA_PRIMARY_SUBSCRIBER_AND_THREE_OR_MORE_DEPENDENTS_COL_INDEX = 13;
	private static final int DATA_COUPLE_AND_ONE_DEPENDENT_COL_INDEX = 14;
	private static final int DATA_COUPLE_AND_TWO_DEPENDENTS_COL_INDEX = 15;
	private static final int DATA_COUPLE_AND_THREE_OR_MORE_DEPENDENTS_COL_INDEX = 16;

	public RatePUFToSERFFAdapter(String pufFilePath) {

		super(pufFilePath, 
				new int[] {DATA_ISSUER_HIOS_ID_COL_INDEX + DATA_OFFSET,
				DATA_FEDRAL_TIN_COL_INDEX + DATA_OFFSET,
				DATA_RATE_EFFECTIVE_DATE_COL_INDEX + DATA_OFFSET,
				DATA_RATE_EXPIRATION_DATE_COL_INDEX + DATA_OFFSET,
				DATA_PLAN_ID_COL_INDEX + DATA_OFFSET,
				DATA_RATING_AREA_ID_COL_INDEX + DATA_OFFSET,
				DATA_TOBACCO_COL_INDEX + DATA_OFFSET,
				DATA_AGE_COL_INDEX + DATA_OFFSET,
				DATA_INDIVIDUAL_RATE_COL_INDEX + DATA_OFFSET,
				DATA_INDIVIDUAL_TOBACCO_RATE_COL_INDEX + DATA_OFFSET,
				DATA_COUPLE_COL_INDEX + DATA_OFFSET,
				DATA_PRIMARY_SUBSCRIBER_AND_ONE_DEPENDENT_COL_INDEX + DATA_OFFSET,
				DATA_PRIMARY_SUBSCRIBER_AND_TWO_DEPENDENTS_COL_INDEX + DATA_OFFSET,
				DATA_PRIMARY_SUBSCRIBER_AND_THREE_OR_MORE_DEPENDENTS_COL_INDEX + DATA_OFFSET,
				DATA_COUPLE_AND_ONE_DEPENDENT_COL_INDEX + DATA_OFFSET,
				DATA_COUPLE_AND_TWO_DEPENDENTS_COL_INDEX + DATA_OFFSET,
				DATA_COUPLE_AND_THREE_OR_MORE_DEPENDENTS_COL_INDEX + DATA_OFFSET},
				null,
				null);
		setDateFormat("yyyy-MM-dd");
	}

	@Override
	protected void setValidationRules() {

		List<String[]> pufRateData = getPufData();
		setTemplateFileName(getWholeNumberPart(pufRateData.get(0)[DATA_ISSUER_HIOS_ID_COL_INDEX]) + "-Rate.xml"); 

		if (CollectionUtils.isEmpty(pufRateData)) {
			return;
		}
		addValidator(DATA_ISSUER_HIOS_ID_COL_INDEX, "HIOS Issuer ID", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufRateData.get(0)[DATA_ISSUER_HIOS_ID_COL_INDEX]));
		addValidator(DATA_FEDRAL_TIN_COL_INDEX, "Fedral TIN", Validators.getValidatorRule(Validators.TYPE.NOT_NULL, null, null, null));
	}

	@Override
	public Object createSERFFTemplateObj() {

		QhpApplicationRateGroupVO qhpAppRateGroupVO = null;
		List<String[]> pufRateData = getPufData();

		if (CollectionUtils.isNotEmpty(pufRateData)) {
			qhpAppRateGroupVO = new QhpApplicationRateGroupVO();

			QhpApplicationRateHeaderVO qhpAppRateHeaderVO = new QhpApplicationRateHeaderVO();
			qhpAppRateHeaderVO.setIssuerId(createExcelCell(getWholeNumberPart(pufRateData.get(0)[DATA_ISSUER_HIOS_ID_COL_INDEX])));
			qhpAppRateHeaderVO.setTin(createExcelCell(pufRateData.get(0)[DATA_FEDRAL_TIN_COL_INDEX]));
			qhpAppRateGroupVO.setHeader(qhpAppRateHeaderVO);
			List<QhpApplicationRateItemVO> items = new ArrayList<QhpApplicationRateItemVO>();

			for (String[] rateData : pufRateData) {
				items.add(addRateItems(rateData));
			}
			qhpAppRateGroupVO.getItems().addAll(items);
		}
		return qhpAppRateGroupVO;
	}

	private QhpApplicationRateItemVO addRateItems(String[] rateData) {

		debug("Adding Rate's Item data Begin");
		QhpApplicationRateItemVO qhpApplicationRateItemVO = new QhpApplicationRateItemVO();

		try {

			final String TOBACCO_VALUE = "Tobacco User/Non-Tobacco User";
			final String FAMILY_OPTION_VALUE = "Family Option";

			qhpApplicationRateItemVO.setEffectiveDate(createExcelCell(rateData[DATA_RATE_EFFECTIVE_DATE_COL_INDEX]));
			qhpApplicationRateItemVO.setExpirationDate(createExcelCell(rateData[DATA_RATE_EXPIRATION_DATE_COL_INDEX]));
			qhpApplicationRateItemVO.setPlanId(createExcelCell(rateData[DATA_PLAN_ID_COL_INDEX]));
			qhpApplicationRateItemVO.setRateAreaId(createExcelCell(rateData[DATA_RATING_AREA_ID_COL_INDEX]));
			qhpApplicationRateItemVO.setAgeNumber(createExcelCell(getWholeNumberPart(rateData[DATA_AGE_COL_INDEX])));
			qhpApplicationRateItemVO.setPrimaryEnrollee(createExcelCell(rateData[DATA_INDIVIDUAL_RATE_COL_INDEX]));
			qhpApplicationRateItemVO.setTobacco(createExcelCell(rateData[DATA_TOBACCO_COL_INDEX]));

			if (TOBACCO_VALUE.equals(rateData[DATA_TOBACCO_COL_INDEX])) {
				qhpApplicationRateItemVO.setPrimaryEnrolleeTobacco(createExcelCell(rateData[DATA_INDIVIDUAL_TOBACCO_RATE_COL_INDEX]));
			}

			if (FAMILY_OPTION_VALUE.equals(qhpApplicationRateItemVO.getAgeNumber().getCellValue())) {
				qhpApplicationRateItemVO.setCoupleEnrollee(createExcelCell(rateData[DATA_COUPLE_COL_INDEX]));
				qhpApplicationRateItemVO.setPrimaryEnrolleeOneDependent(createExcelCell(rateData[DATA_PRIMARY_SUBSCRIBER_AND_ONE_DEPENDENT_COL_INDEX]));
				qhpApplicationRateItemVO.setPrimaryEnrolleeTwoDependent(createExcelCell(rateData[DATA_PRIMARY_SUBSCRIBER_AND_TWO_DEPENDENTS_COL_INDEX]));
				qhpApplicationRateItemVO.setPrimaryEnrolleeManyDependent(createExcelCell(rateData[DATA_PRIMARY_SUBSCRIBER_AND_THREE_OR_MORE_DEPENDENTS_COL_INDEX]));
				qhpApplicationRateItemVO.setCoupleEnrolleeOneDependent(createExcelCell(rateData[DATA_COUPLE_AND_ONE_DEPENDENT_COL_INDEX]));
				qhpApplicationRateItemVO.setCoupleEnrolleeTwoDependent(createExcelCell(rateData[DATA_COUPLE_AND_TWO_DEPENDENTS_COL_INDEX]));
				qhpApplicationRateItemVO.setCoupleEnrolleeManyDependent(createExcelCell(rateData[DATA_COUPLE_AND_THREE_OR_MORE_DEPENDENTS_COL_INDEX]));
			}
		}
		finally {
			debug("Adding Rate's Item data End");
		}
		return qhpApplicationRateItemVO;
	}

	private ExcelCellVO createExcelCell(String cellValue) {
		ExcelCellVO excelCellVO = new ExcelCellVO();
//		excelCellVO.setCellName(cellName);
		excelCellVO.setCellValue(cellValue);
		return excelCellVO;
	}

	public static void main(String[] args) {
//		PUFToSERFFAdapter adpt = new RatePUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\10091-Rates.xlsx");
		PUFToSERFFAdapter adpt = new RatePUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\21989-Rate_PUF.xlsx");
//		PUFToSERFFAdapter adpt = new RatePUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\validation\\21989-Rate_PUF_Error1.xlsx");
//		adpt.setDebugEnabled(true);
		adpt.generateSERFFTemplate(null);
	}
}
