package com.getinsured.serffutils;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.serff.template.rbrules.extension.PayloadType;
import com.serff.template.rbrules.hix.pm.AgeDeterminationCodeSimpleType;
import com.serff.template.rbrules.hix.pm.EnrolleeContractRateDeterminationCodeSimpleType;
import com.serff.template.rbrules.hix.pm.FamilyRelationshipCodeSimpleType;
import com.serff.template.rbrules.hix.pm.FamilyRelationshipCohabitationRuleType;
import com.serff.template.rbrules.hix.pm.InsurancePlanType;
import com.serff.template.rbrules.hix.pm.InsuranceProductType;
import com.serff.template.rbrules.hix.pm.IssuerType;
import com.serff.template.rbrules.hix.pm.PersonCountRuleCodeSimpleType;
import com.serff.template.rbrules.hix.pm.RateCategoryDeterminationRulesetType;
import com.serff.template.rbrules.niem.core.IdentificationType;
import com.serff.template.rbrules.niem.core.MeasurePointValueType;
import com.serff.template.rbrules.niem.core.TimeMeasureType;
import com.serff.template.rbrules.niem.unecerec20misc.TimeCodeSimpleType;
import com.serff.template.rbrules.niem.unecerec20misc.TimeCodeType;

/**
 * Class is used to generate Business Rules Template in XML format from PUF Excel data.
 * 
 * @author Bhavin Parmar
 * @since Aug 8, 2016
 */
public class BusinessRulesPUFToSERFFAdapter extends PUFToSERFFAdapter {

	private static final int DATA_OFFSET = 6;
	private static final int DATA_ISSUER_ID2_COL_INDEX = 0;
	private static final int DATA_TIN_COL_INDEX = 1;
	private static final int DATA_PRODUCT_ID_COL_INDEX = 2;
	private static final int DATA_STANDARD_COMPONENT_ID_COL_INDEX = 3;
	private static final int DATA_ENROLLEE_CONTRACT_RATE_DETER_RULE_COL_INDEX = 4;
	private static final int DATA_TWO_PARENT_FAMILY_MAX_DEPEND_RULE_COL_INDEX = 5;
	private static final int DATA_SINGLE_PARENT_FAMILY_MAX_DEPEND_RULE_COL_INDEX = 6;
	private static final int DATA_DEPENDENT_MAX_AG_RULE_COL_INDEX = 7;
	private static final int DATA_CHILD_ONLY_CONTRACT_MAX_CHILD_RULE_COL_INDEX = 8;
	private static final int DATA_DOMESTIC_PARTNER_AS_SPOUSE_INDICATOR_COL_INDEX = 9;
	private static final int DATA_SAME_SEX_PARTNER_AS_SPOUSE_INDICATOR_COL_INDEX = 10;
	private static final int DATA_AGE_DETERMINATION_RULE_COL_INDEX = 11;
	private static final int DATA_MINIMUM_TOBACCO_FREE_MONTHS_RULE_COL_INDEX = 12;
	private static final int DATA_COHABITATION_RULE_COL_INDEX = 13;

	private static final String COMMA = ",";
	private static final String SEMICOLON = ";";
	private static final String YES_OPTION = "yes";
	private static final String NOT_APPLICABLE = "not applicable";

	public BusinessRulesPUFToSERFFAdapter(String pufFilePath) {

		super(pufFilePath, 
				new int[] {DATA_ISSUER_ID2_COL_INDEX + DATA_OFFSET,
				DATA_TIN_COL_INDEX + DATA_OFFSET,
				DATA_PRODUCT_ID_COL_INDEX + DATA_OFFSET,
				DATA_STANDARD_COMPONENT_ID_COL_INDEX + DATA_OFFSET,
				DATA_ENROLLEE_CONTRACT_RATE_DETER_RULE_COL_INDEX + DATA_OFFSET,
				DATA_TWO_PARENT_FAMILY_MAX_DEPEND_RULE_COL_INDEX + DATA_OFFSET,
				DATA_SINGLE_PARENT_FAMILY_MAX_DEPEND_RULE_COL_INDEX + DATA_OFFSET,
				DATA_DEPENDENT_MAX_AG_RULE_COL_INDEX + DATA_OFFSET,
				DATA_CHILD_ONLY_CONTRACT_MAX_CHILD_RULE_COL_INDEX + DATA_OFFSET,
				DATA_DOMESTIC_PARTNER_AS_SPOUSE_INDICATOR_COL_INDEX + DATA_OFFSET,
				DATA_SAME_SEX_PARTNER_AS_SPOUSE_INDICATOR_COL_INDEX + DATA_OFFSET,
				DATA_AGE_DETERMINATION_RULE_COL_INDEX + DATA_OFFSET,
				DATA_MINIMUM_TOBACCO_FREE_MONTHS_RULE_COL_INDEX + DATA_OFFSET,
				DATA_COHABITATION_RULE_COL_INDEX + DATA_OFFSET},
				null,
				null);
	}

	@Override
	protected void setValidationRules() {

		List<String[]> pufRateData = getPufData();

		if (CollectionUtils.isEmpty(pufRateData)) {
			return;
		}
		addValidator(DATA_ISSUER_ID2_COL_INDEX, "HIOS Issuer ID", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufRateData.get(0)[DATA_ISSUER_ID2_COL_INDEX]));
		addValidator(DATA_STANDARD_COMPONENT_ID_COL_INDEX, "Standard Component ID", Validators.getValidatorRule(Validators.TYPE.NOT_NULL, null, null, null));
	}

	@Override
	public Object createSERFFTemplateObj() {

		PayloadType businessRule = null;
		List<String[]> pufRateData = getPufData();

		if (CollectionUtils.isNotEmpty(pufRateData)) {
			businessRule = new PayloadType();
			businessRule.setIssuer(new IssuerType());
			com.serff.template.rbrules.niem.proxy.xsd.String idValue = new com.serff.template.rbrules.niem.proxy.xsd.String();
			idValue.setValue(getWholeNumberPart(pufRateData.get(0)[DATA_ISSUER_ID2_COL_INDEX]));
			setTemplateFileName(idValue.getValue() + "-businessrule.xml"); 
			IdentificationType identificationType = new IdentificationType();
			// <IssuerIdentification><IdentificationID>
			identificationType.setIdentificationID(idValue);
			businessRule.getIssuer().setIssuerIdentification(identificationType);

			idValue = new com.serff.template.rbrules.niem.proxy.xsd.String();
			idValue.setValue(pufRateData.get(0)[DATA_TIN_COL_INDEX]);
			identificationType = new IdentificationType();
			// <IssuerFederalTaxIdentification><IdentificationID>
			identificationType.setIdentificationID(idValue);
			businessRule.getIssuer().setIssuerFederalTaxIdentification(identificationType);

			InsuranceProductType insuranceProduct = new InsuranceProductType();
			businessRule.getIssuer().getInsuranceProduct().add(insuranceProduct);

			for (String[] rateData : pufRateData) {
				insuranceProduct.getInsurancePlan().add(addInsurancePlanType(rateData));
			}
			// Adding Rate Category Determination Rule set Type
			businessRule.getIssuer().setIssuerRateCategoryDeterminationRuleset(getRateCategoryDeterminationRulesetType(pufRateData.get(0)));
		}
		return businessRule;
	}

	private InsurancePlanType addInsurancePlanType(String[] rateData) {

		InsurancePlanType planType = new InsurancePlanType();
		IdentificationType identificationType = new IdentificationType();
		com.serff.template.rbrules.niem.proxy.xsd.String strValue = new com.serff.template.rbrules.niem.proxy.xsd.String();
		strValue.setValue(rateData[DATA_STANDARD_COMPONENT_ID_COL_INDEX]);
		identificationType.setIdentificationID(strValue);
		// <InsurancePlanIdentification><IdentificationID>
		planType.setInsurancePlanIdentification(identificationType);
		// Adding Rate Category Determination Rule set Type <InsurancePlanRateCategoryDeterminationRuleset>
		planType.setInsurancePlanRateCategoryDeterminationRuleset(getRateCategoryDeterminationRulesetType(rateData));
		return planType;
	}

	private RateCategoryDeterminationRulesetType getRateCategoryDeterminationRulesetType(String[] rateData) {

		RateCategoryDeterminationRulesetType ruleset = new RateCategoryDeterminationRulesetType();

		TimeMeasureType timeMeasureType = null;
		TimeCodeType timeCodeType = null;
		MeasurePointValueType measurePointValueType = null;

		if (!NOT_APPLICABLE.equalsIgnoreCase(rateData[DATA_DEPENDENT_MAX_AG_RULE_COL_INDEX])) {
			timeMeasureType = new TimeMeasureType();
			timeCodeType = new TimeCodeType();
			measurePointValueType = new MeasurePointValueType();
			// <MeasurePointValue>
			timeCodeType.setValue(TimeCodeSimpleType.ANN);
			// <TimeUnitCode>
			timeMeasureType.setTimeUnitCode(timeCodeType);
			measurePointValueType.setValue(BigDecimal.valueOf(Long.valueOf(getWholeNumberPart(rateData[DATA_DEPENDENT_MAX_AG_RULE_COL_INDEX]))));
			timeMeasureType.setMeasurePointValue(measurePointValueType);
			// <RateCategoryDeterminationRulesetDependentMaximumAgeMeasure>
			ruleset.setRateCategoryDeterminationRulesetDependentMaximumAgeMeasure(timeMeasureType);
		}

		if (!NOT_APPLICABLE.equalsIgnoreCase(rateData[DATA_MINIMUM_TOBACCO_FREE_MONTHS_RULE_COL_INDEX])) {
			timeMeasureType = new TimeMeasureType();
			timeCodeType = new TimeCodeType();
			measurePointValueType = new MeasurePointValueType();
			// <MeasurePointValue>
			timeCodeType.setValue(TimeCodeSimpleType.MON);
			// <TimeUnitCode>
			timeMeasureType.setTimeUnitCode(timeCodeType);
			measurePointValueType.setValue(BigDecimal.valueOf(Long.valueOf(getWholeNumberPart(rateData[DATA_MINIMUM_TOBACCO_FREE_MONTHS_RULE_COL_INDEX]))));
			timeMeasureType.setMeasurePointValue(measurePointValueType);
			// <RateCategoryDeterminationRulesetMinimumTobaccoFreeMonthsMeasure>
			ruleset.setRateCategoryDeterminationRulesetMinimumTobaccoFreeMonthsMeasure(timeMeasureType);
		}

		boolean flag = false;
		if (YES_OPTION.equalsIgnoreCase(rateData[DATA_DOMESTIC_PARTNER_AS_SPOUSE_INDICATOR_COL_INDEX])) {
			flag = true;
		}
		com.serff.template.rbrules.niem.proxy.xsd.Boolean booleanFlag = new com.serff.template.rbrules.niem.proxy.xsd.Boolean();
		booleanFlag.setValue(flag);
		// <RateCategoryDeterminationRulesetDomesticPartnerAsSpouseIndicator>
		ruleset.setRateCategoryDeterminationRulesetDomesticPartnerAsSpouseIndicator(booleanFlag);

		flag = false;
		if (YES_OPTION.equalsIgnoreCase(rateData[DATA_SAME_SEX_PARTNER_AS_SPOUSE_INDICATOR_COL_INDEX])) {
			flag = true;
		}
		booleanFlag = new com.serff.template.rbrules.niem.proxy.xsd.Boolean();
		booleanFlag.setValue(flag);
		// <RateCategoryDeterminationRulesetSameSexPartnerAsSpouseIndicator>
		ruleset.setRateCategoryDeterminationRulesetSameSexPartnerAsSpouseIndicator(booleanFlag);
		// <RateCategoryDeterminationRulesetChildrenOnlyContractMaxChildrenRuleCode>
		ruleset.setRateCategoryDeterminationRulesetChildrenOnlyContractMaxChildrenRuleCode(getPersonCountRuleCodeSimpleEnum(rateData[DATA_CHILD_ONLY_CONTRACT_MAX_CHILD_RULE_COL_INDEX]));
		// <RateCategoryDeterminationRulesetAgeDeterminationRuleCode>
		ruleset.setRateCategoryDeterminationRulesetAgeDeterminationRuleCode(getAgeDetermCodeSimpleEnum(rateData[DATA_AGE_DETERMINATION_RULE_COL_INDEX]));
		// <RateCategoryDeterminationRulesetEnrolleeContractRateDeterminationRuleCode>
		ruleset.setRateCategoryDeterminationRulesetEnrolleeContractRateDeterminationRuleCode(getEnrollContRateDetermCodeSimpleEnum(rateData[DATA_ENROLLEE_CONTRACT_RATE_DETER_RULE_COL_INDEX]));
		// <RateCategoryDeterminationRulesetSingleParentFamilyMaxDependentsRuleCode>
		ruleset.setRateCategoryDeterminationRulesetSingleParentFamilyMaxDependentsRuleCode(getPersonCountRuleCodeSimpleEnum(rateData[DATA_SINGLE_PARENT_FAMILY_MAX_DEPEND_RULE_COL_INDEX]));
		// <RateCategoryDeterminationRulesetTwoParentFamilyMaxDependentsRuleCode>
		ruleset.setRateCategoryDeterminationRulesetTwoParentFamilyMaxDependentsRuleCode(getPersonCountRuleCodeSimpleEnum(rateData[DATA_TWO_PARENT_FAMILY_MAX_DEPEND_RULE_COL_INDEX]));

		if (StringUtils.isNotBlank(rateData[DATA_COHABITATION_RULE_COL_INDEX])) {

			String[] cohabitationRules = rateData[DATA_COHABITATION_RULE_COL_INDEX].split(SEMICOLON);
			FamilyRelationshipCohabitationRuleType familyRelationshipType;

			for (String cohabitationRule : cohabitationRules) {

				if (StringUtils.isNotBlank(cohabitationRule)) {
					String[] familyRelationships = cohabitationRule.trim().split(COMMA);

					flag = false;
					if (YES_OPTION.equalsIgnoreCase(familyRelationships[1].trim())) {
						flag = true;
					}
					booleanFlag = new com.serff.template.rbrules.niem.proxy.xsd.Boolean();
					booleanFlag.setValue(flag);
					familyRelationshipType = new FamilyRelationshipCohabitationRuleType();
					familyRelationshipType.setCohabitationRequiredIndicator(booleanFlag);
					familyRelationshipType.setFamilyRelationshipCohabitationRuleCode(getFamilyRelationshipEnum(familyRelationships[0].trim()));
					// <RateCategoryDeterminationRulesetFamilyRelationshipCohabitationRule>
					ruleset.getRateCategoryDeterminationRulesetFamilyRelationshipCohabitationRule().add(familyRelationshipType);
				}
			}
		}
		return ruleset;
	}

	private PersonCountRuleCodeSimpleType getPersonCountRuleCodeSimpleEnum(String actualValue) {

		PersonCountRuleCodeSimpleType enumValue;

		switch (actualValue.toUpperCase()) {
			case "1" : enumValue = PersonCountRuleCodeSimpleType.ONE; break;
			case "2" : enumValue = PersonCountRuleCodeSimpleType.TWO; break;
			case "3 OR MORE" : enumValue = PersonCountRuleCodeSimpleType.THREE_OR_MORE; break;

			default : enumValue = null; break;
		}
		return enumValue;
	}

	private AgeDeterminationCodeSimpleType getAgeDetermCodeSimpleEnum(String actualValue) {

		AgeDeterminationCodeSimpleType enumValue;

		switch (actualValue.toUpperCase()) {
			case "Age on effective date" : enumValue = AgeDeterminationCodeSimpleType.AGE_ON_EFFECTIVE_DATE; break;
			case "Age on January 1st of the effective date year" : enumValue = AgeDeterminationCodeSimpleType.AGE_ON_JANUARY_1_ST_OF_EFFECTIVE_DATE_YEAR; break;
			case "Age on insurance date (age on birthday nearest the effective date)" : enumValue = AgeDeterminationCodeSimpleType.AGE_ON_NEAREST_BIRTHDAY_TO_EFFECTIVE_DATE; break;
			case "Age on January 1st or July 1st" : enumValue = AgeDeterminationCodeSimpleType.AGE_ON_JANUARY_1_ST_OR_JULY_1_ST_OF_EFFECTIVE_DATE_YEAR; break;

			default : enumValue = null; break;
		}
		return enumValue;
	}

	private EnrolleeContractRateDeterminationCodeSimpleType getEnrollContRateDetermCodeSimpleEnum(String actualValue) {

		EnrolleeContractRateDeterminationCodeSimpleType enumValue;

		switch (actualValue.toUpperCase()) {
			case "A DIFFERENT RATE (SPECIFICALLY FOR PARTIES OF TWO OR MORE)FOR EACH ENROLLEE IS ADDED TOGETHER" : enumValue = EnrolleeContractRateDeterminationCodeSimpleType.ADD_INDIVIDUAL_RATES; break;
			case "THERE ARE RATES SPECIFICALLY FOR COUPLES AND FOR FAMILIES (NOT JUST ADDITION OF INDIVIDUAL RATES)" : enumValue = EnrolleeContractRateDeterminationCodeSimpleType.USE_GROUP_RATE; break;

			default : enumValue = null; break;
		}
		return enumValue;
	}

	private FamilyRelationshipCodeSimpleType getFamilyRelationshipEnum(String actualValue) {

		FamilyRelationshipCodeSimpleType enumValue;

		switch (actualValue.toUpperCase()) {
			case "SPOUSE" : enumValue = FamilyRelationshipCodeSimpleType.SPOUSE; break;
			case "FATHER OR MOTHER" : enumValue = FamilyRelationshipCodeSimpleType.PARENT; break;
			case "GRANDFATHER OR GRANDMOTHER" : enumValue = FamilyRelationshipCodeSimpleType.GRANDPARENT; break;
			case "GRANDSON OR GRANDDAUGHTER" : enumValue = FamilyRelationshipCodeSimpleType.GRANDCHILD; break;
			case "UNCLE OR AUNT" : enumValue = FamilyRelationshipCodeSimpleType.PARENT_SIBLING; break;
			case "NEPHEW OR NIECE" : enumValue = FamilyRelationshipCodeSimpleType.SIBLING_CHILD; break;
			case "COUSIN" : enumValue = FamilyRelationshipCodeSimpleType.COUSIN; break;
			case "ADOPTED CHILD" : enumValue = FamilyRelationshipCodeSimpleType.ADOPTED_CHILD; break;
			case "FOSTER CHILD" : enumValue = FamilyRelationshipCodeSimpleType.FOSTER_CHILD; break;
			case "SON-IN-LAW OR DAUGHTER-IN-LAW" : enumValue = FamilyRelationshipCodeSimpleType.CHILD_IN_LAW; break;
			case "BROTHER-IN-LAW OR SISTER-IN-LAW" : enumValue = FamilyRelationshipCodeSimpleType.SIBLING_IN_LAW; break;
			case "BROTHER OR SISTER" : enumValue = FamilyRelationshipCodeSimpleType.SIBLING; break;
			case "WARD" : enumValue = FamilyRelationshipCodeSimpleType.WARD; break;
			case "STEPPARENT" : enumValue = FamilyRelationshipCodeSimpleType.STEP_PARENT; break;
			case "STEPSON OR STEPDAUGHTER" : enumValue = FamilyRelationshipCodeSimpleType.STEP_CHILD; break;
			case "CHILD" : enumValue = FamilyRelationshipCodeSimpleType.CHILD; break;
			case "SPONSORED DEPENDENT" : enumValue = FamilyRelationshipCodeSimpleType.SPONSORED_DEPENDENT; break;
			case "DEPENDENT ON A MINOR DEPENDENT" : enumValue = FamilyRelationshipCodeSimpleType.DEPENDENT_OF_MINOR_DEPENDENT; break;
			case "EX-SPOUSE" : enumValue = FamilyRelationshipCodeSimpleType.EX_SPOUSE; break;
			case "GUARDIAN" : enumValue = FamilyRelationshipCodeSimpleType.GUARDIAN; break;
			case "COURT APPOINTED GUARDIAN" : enumValue = FamilyRelationshipCodeSimpleType.COURT_APPOINTED_GUARDIAN; break;
			case "COLLATERAL DEPENDENT" : enumValue = FamilyRelationshipCodeSimpleType.COLLATERAL_DEPENDENT; break;
			case "LIFE PARTNER" : enumValue = FamilyRelationshipCodeSimpleType.LIFE_PARTNER; break;
			case "ANNULTANT" : enumValue = FamilyRelationshipCodeSimpleType.ANNULTANT; break;
			case "TRUSTEE" : enumValue = FamilyRelationshipCodeSimpleType.TRUSTEE; break;
			case "SELF" : enumValue = FamilyRelationshipCodeSimpleType.SELF; break;
			case "FATHER-IN-LAW OR MOTHER-IN-LAW" : enumValue = FamilyRelationshipCodeSimpleType.PARENT_IN_LAW; break;
			case "OTHER RELATIONSHIP" : enumValue = FamilyRelationshipCodeSimpleType.OTHER_RELATIONSHIP; break;
			case "OTHER RELATIVE" : enumValue = FamilyRelationshipCodeSimpleType.OTHER_RELATIVE; break;

			default : enumValue = null; break;
		}
		return enumValue;
	}

	public static void main(String[] args) {
		PUFToSERFFAdapter adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\BusinessRules\\11339-Business_Rules_PUF.xlsx");
		adpt.generateSERFFTemplate(null);
//		adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\BusinessRules\\11324-Business_Rules_PUF.xlsx");
//		adpt.generateSERFFTemplate(null);
//		adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\BusinessRules\\11269-Business_Rules_PUF.xlsx");
//		adpt.generateSERFFTemplate(null);
//		adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\BusinessRules\\11103-Business_Rules_PUF.xlsx");
//		adpt.generateSERFFTemplate(null);
//		adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\BusinessRules\\11083-Business_Rules_PUF.xlsx");
//		adpt.generateSERFFTemplate(null);
//		adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\BusinessRules\\10207-Business_Rules_PUF.xlsx");
//		adpt.generateSERFFTemplate(null);
//		adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\BusinessRules\\10191-Business_Rules_PUF.xlsx");
//		adpt.generateSERFFTemplate(null);
//		adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\BusinessRules\\10064-Business_Rules_PUF.xlsx");
//		adpt.generateSERFFTemplate(null);
//		adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\BusinessRules\\10046-Business_Rules_PUF.xlsx");
//		adpt.generateSERFFTemplate(null);
//		adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\BusinessRules\\10091-Business_Rules_PUF.xlsx");
//		adpt.generateSERFFTemplate(null);
//		adpt = new BusinessRulesPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\validation\\10091-Business_Rules_PUF-Error1.xlsx");
//		adpt.generateSERFFTemplate(null);
//		adpt.setDebugEnabled(true);
	}
}
