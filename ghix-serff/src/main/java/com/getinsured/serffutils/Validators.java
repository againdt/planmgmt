package com.getinsured.serffutils;

/**
 * Abstract Class is used to validate PUF Excel data.
 * 
 * @author Bhavin Parmar
 * @since Aug 12, 2016
 */
public abstract class Validators {

	protected Integer value1;
	protected Integer value2;
	protected String strValue;
	protected String errorMessage;

	public static enum TYPE {
		EQUAL, NOT_NULL, RANGE;
	}

	protected abstract boolean validate(String value);

	protected abstract String getErrorMessage(int lineNumber, String fieldName, String value);

	public static Validators getValidatorRule(TYPE type, Integer value1, Integer value2, String strValue) {

		if (type.equals(TYPE.EQUAL)) {
			return new ValidateEqual(strValue);
		}
		else if (type.equals(TYPE.NOT_NULL)) {
			return new ValidateNotNull();
		}
		else if (type.equals(TYPE.RANGE)) {
			return new ValidateRange(value1, value2);
		}
		else {
			return null;
		}
	}
}
