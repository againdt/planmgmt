package com.getinsured.serffutils;

import org.apache.commons.lang3.StringUtils;

/**
 * Class is used to validate equal string data.
 * 
 * @author Bhavin Parmar
 * @since Aug 12, 2016
 */
public class ValidateEqual extends Validators {

	public ValidateEqual(String strValue) {
		super.strValue = strValue;
	}

	@Override
	protected boolean validate(String strValue) {

		if (StringUtils.isNotBlank(super.strValue)
				&& StringUtils.isNotBlank(strValue)
				&& super.strValue.equals(strValue)) {
			return true;
		}
		return false;
	}

	@Override
	protected String getErrorMessage(int lineNumber, String fieldName, String value) {
		return "\n- Invalid value of "+ fieldName +". Value ["+ value +"] at line "+ lineNumber +" is not equals to "+ super.strValue +" value.";
	}
}
