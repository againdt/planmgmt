package com.getinsured.serffutils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.util.CollectionUtils;

import com.serff.template.plan.CostShareVarianceVO;

public abstract class PUFToSERFFAdapter {

	private List<String[]> pufData;
	private List<String[]> pufAdditionalData;
	private String pufFilePath;
	private String pufAdditionalFilePath;
	private int[] columnIndex;
	private int[] additionalColumnIndex;
	private static final int DATA_SHEET_INDEX = 0;
	private boolean debugEnabled = false;
	private List<ValidatorRule> validatorRuleList; 
	private List<ValidatorRule> validatorAdditionalRuleList;
	private String templateFileName; 
	private String dateFormat; 

	public PUFToSERFFAdapter(String pufFilePath, int[] columnIndex, String pufAdditionalFilePath, int[] additionalColumnIndex) {

		this.pufFilePath = pufFilePath;
		this.pufAdditionalFilePath = pufAdditionalFilePath;
		this.columnIndex = columnIndex;
		this.additionalColumnIndex = additionalColumnIndex;
		this.validatorRuleList = new ArrayList<ValidatorRule>();
		this.validatorAdditionalRuleList = new ArrayList<ValidatorRule>();

		if (isDebugEnabled()) {
			log("Converting PUF file " + pufFilePath);

			if (StringUtils.isNotBlank(pufAdditionalFilePath)) {
				log("Additional PUF file " + pufAdditionalFilePath);
			}
		}
	}

	protected abstract void setValidationRules();

	protected abstract Object createSERFFTemplateObj();

	protected boolean readPUFFile() {

		boolean readDone = false;
		ExcelReader pufReader = null;

		try {
			//Read file 1 and File 2 and put data in string[]
			if (StringUtils.isNotBlank(pufFilePath)) {
				debug("Reading PUF file " + pufFilePath);
				pufReader = new PUFExcelReader(dateFormat);
				pufReader.loadFile(pufFilePath);
				XSSFSheet sheet = pufReader.getSheet(DATA_SHEET_INDEX);

				if (null != sheet) {
					pufData = new ArrayList<String[]>();
					readDone = readPUFDataFromSheet(pufReader, sheet, pufData, columnIndex);
					debug("Status of reading PUF file " + readDone);
				}
				else {
					error("Failed to read PUF data from " + pufFilePath);
				}
			}

			if (StringUtils.isNotBlank(pufAdditionalFilePath)) {
				debug("Reading Additional PUF file " + pufAdditionalFilePath);
				readDone = false;
				pufReader.loadFile(pufAdditionalFilePath);
				XSSFSheet sheet = pufReader.getSheet(DATA_SHEET_INDEX);

				if (null != sheet) {
					pufAdditionalData = new ArrayList<String[]>();
					readDone = readPUFDataFromSheet(pufReader, sheet, pufAdditionalData, additionalColumnIndex);
					debug("Status of reading Additional PUF file " + readDone);
				}
				else {
					error("Failed to read additional PUF data from " + pufAdditionalFilePath);
				}
			}
		}
		catch (IOException e) {

			if (isDebugEnabled()) {
				e.printStackTrace();
			}
			else {
				error(e.getMessage());
			}
		}
		return readDone;
	}

	private boolean readPUFDataFromSheet(ExcelReader pufReader, XSSFSheet sheet, List<String[]> pufData, int[] columnsIndex) {

		boolean headerRow = true;
		boolean readDone = false;
		String[] rowData = null;

		if (null == sheet || null == pufData) {
			error("Invalid params passed to reading PUF data from sheet");
		}
		else {

			if (null == columnsIndex || columnsIndex.length < 1) {
				error("Invalid column index passed to reading PUF data from sheet");
			}
			else {

				Iterator<Row> rowIterator = sheet.iterator();
				while (rowIterator.hasNext()) {

					Row excelRow = rowIterator.next();
					if (headerRow) {
						headerRow = false;
						continue;
					}
					debug("Reading next row of data....");
					rowData = readRow(pufReader, excelRow, columnsIndex);

					if (null != rowData) {
						pufData.add(rowData);
					}
					else {
						debug("Reached last row of data file.");
						break;
					}
				}

				if (pufData.size() >= 1) {
					debug("Done reading data from file, rows of data read: " + pufData.size());
					readDone = true;
				}
				else {
					error("Failed to read any data.");
				}
			}
		}
		return readDone;
	}

	private String[] readRow(ExcelReader pufReader, Row excelRow, int[] columnsIndex) {

		String[] rowData = null;

		if (null != excelRow && null != columnsIndex) {
			if(StringUtils.isNotBlank(pufReader.getCellValueTrim(excelRow.getCell(columnsIndex[0])))) {
				StringBuffer sb = new StringBuffer();
				rowData = new String[columnsIndex.length];
	
				for (int i = 0; i < columnsIndex.length; i++) {
					rowData[i] = pufReader.getCellValueTrim(excelRow.getCell(columnsIndex[i]));
	
					if (isDebugEnabled()) {
						sb.append(i).append(" : [").append(rowData[i]).append("], ");
					}
				}
				debug(sb.toString());
			} else {
				debug("Got blank first column, marking this as blank row");
			}
		}
		return rowData;
	}

	protected List<String[]> getPufData() {
		return pufData;
	}

	protected String getPufFilePath() {
		return pufFilePath;
	}

	protected List<String[]> getAdditionalPufData() {
		return pufAdditionalData;
	}

	private boolean validatePUFData(StringBuilder errorSB) {

		if (CollectionUtils.isEmpty(validatorRuleList) && CollectionUtils.isEmpty(pufData)) {
			return false;
		}
		return validateData(pufData, validatorRuleList, errorSB);
	}

	private boolean validateAdditionalPUFData(StringBuilder errorSB) {

		if (CollectionUtils.isEmpty(validatorAdditionalRuleList) || CollectionUtils.isEmpty(pufAdditionalData)) {
			return true;
		}

		return validateData(pufAdditionalData, validatorAdditionalRuleList, errorSB);
	}

	private boolean validateData(List<String[]> dataToValidate, List<ValidatorRule> rules, StringBuilder errorSB) {
		boolean isValid = true;
		int rowCount = 1;

		for (String[] data : dataToValidate) {
			rowCount++;		// Row Count

			for (ValidatorRule rule : rules) {

				if (rule.getValidator().validate(data[rule.getIndex()])) {
					continue;
				}

				if (isValid) {
					isValid = false;	// set Valid flag false for invalid data
				}
				// Generate Error Message for each row for invalid data
				errorSB.append(rule.getValidator().getErrorMessage(rowCount, rule.getFieldName(), data[rule.getIndex()]));
			}
		}
		return isValid;
	}
	
	protected void addValidator(int colIndex, String columnName, Validators validatorObj) {

		ValidatorRule rule = new ValidatorRule();
		rule.setIndex(colIndex);
		rule.setFieldName(columnName);
		rule.setValidator(validatorObj);
		validatorRuleList.add(rule);
	}
	
	protected void addAdditionalValidator(int colIndex, String columnName, Validators validatorObj) {

		ValidatorRule rule = new ValidatorRule();
		rule.setIndex(colIndex);
		rule.setFieldName(columnName);
		rule.setValidator(validatorObj);
		validatorAdditionalRuleList.add(rule);
	}

	protected void setTemplateFileName(String name) {
		this.templateFileName = name;
		log("Out file name set to " + name);
	}

	public String getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	public boolean generateSERFFTemplate(String outFile) {

		boolean status = false;
		String outFileName = null;

		if (!readPUFFile()) {
			error("Failed to read PUF Source File " + pufFilePath);
		}
		else {
			setValidationRules();
			StringBuilder errorSB = new StringBuilder();

			if (!validatePUFData(errorSB) || !validateAdditionalPUFData(errorSB)) {
				error("Failed to validate PUF Source data from " + pufFilePath);
				error(errorSB.toString());
			}
			else {
				Object voObj = createSERFFTemplateObj();

				if (null == voObj) {
					error("Failed to creare SERFF template object.");
				}
				else {
					if (StringUtils.isNotBlank(outFile)) {
						outFileName = outFile;
					}
					else {
						outFileName = getOutputFileName();
					}
					status = writeVOToXMLFile(voObj, outFileName) ;
				} 
			} 
		}

		if (status) {
			log("Completed writing to template " + outFileName );
		}
		else {
			error("Failed generating template " + outFileName );
		}
		return status;
	}

	private String getOutputFileName() {
		String outPath;
		if(StringUtils.isBlank(templateFileName)) {
			outPath = pufFilePath + ".xml";
		} else {
			String inputPath = getPufFilePath();
			int ind = inputPath.lastIndexOf("\\");
			if(ind == -1) {
				ind = inputPath.lastIndexOf("/");
			}
			if(ind > 0) {
				outPath = inputPath.substring(0, ind+1);
			} else {
				outPath = "";
			}
			outPath = outPath + templateFileName;
		}
		return outPath;	
	}
	
	private boolean writeVOToXMLFile(Object voObj, String fileName) {

		boolean status = false;
		OutputStream outFile;
		Class classObj = voObj.getClass();

		try {
		    JAXBContext rootContext = JAXBContext.newInstance(classObj);
		    Marshaller m = rootContext.createMarshaller();
			outFile = new FileOutputStream(fileName);
		    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		    m.marshal(voObj, outFile);
			status = true;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return status;
	}

	protected String getWholeNumberPart(String value) {

		if (StringUtils.isNotBlank(value) && value.contains(".")) {
			return value.substring(0, value.indexOf('.'));
		}
		return value;
	}

	protected boolean isDebugEnabled() {
		return debugEnabled;
	}

	protected void setDebugEnabled(boolean debugEnabled) {
		this.debugEnabled = debugEnabled;
	}

	protected void debug(String message) {

		if (debugEnabled) {
			System.out.println("DEBUG: " + message);
		}
	}

	protected void log(String message) {
		System.out.println("INFO: " + message);
	}

	protected void error(String message) {
		System.err.println("ERROR: " + message);
	}
}

class PlanCostShareVarianceComparator implements Comparator<CostShareVarianceVO> {
	@Override
	public int compare(CostShareVarianceVO arg0, CostShareVarianceVO arg1) {
		String csv0 = arg0.getPlanId().getCellValue().substring(16);
		String csv1 = arg1.getPlanId().getCellValue().substring(16);
		return Integer.valueOf(csv0) - Integer.valueOf(csv1);
	}
}