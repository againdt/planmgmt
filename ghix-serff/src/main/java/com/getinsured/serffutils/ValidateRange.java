package com.getinsured.serffutils;

import org.apache.commons.lang3.StringUtils;

/**
 * Class is used to validate value should be in range of between two integer values.
 * 
 * @author Bhavin Parmar
 * @since Aug 12, 2016
 */
public class ValidateRange extends Validators {

	public ValidateRange(Integer value1, Integer value2) {
		super.value1 = value1;
		super.value2 = value2;
	}

	@Override
	protected boolean validate(String value) {

		if (null != super.value1 && null != super.value2
				&& StringUtils.isNumeric(value)) {

			int intVal = Integer.valueOf(value);

			if (intVal >= super.value1 && intVal <= super.value2) {
				return true;
			}
		}
		return false;
	}

	@Override
	protected String getErrorMessage(int lineNumber, String columnName, String value) {
		return "\n- Invalid value of "+ columnName +". Value ["+ value +"] at line "+ lineNumber +" is not between "+ super.value1 +" to "+ super.value2 +".";
	}
}
