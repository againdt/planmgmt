package com.getinsured.serffutils;
import com.getinsured.serffutils.Validators;

/**
 * POJO class is used to add validator for required PUF Excel data.
 * 
 * @author Bhavin Parmar
 * @since Aug 17, 2016
 */
public class ValidatorRule {

	private int index;
	private String fieldName;
	private Validators validator;

	public ValidatorRule() {}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Validators getValidator() {
		return validator;
	}

	public void setValidator(Validators validator) {
		this.validator = validator;
	}
}
