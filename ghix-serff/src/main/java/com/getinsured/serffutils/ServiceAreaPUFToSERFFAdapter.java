package com.getinsured.serffutils;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.serff.template.svcarea.extension.PayloadType;
import com.serff.template.svcarea.hix.pm.GeographicAreaType;
import com.serff.template.svcarea.hix.pm.IssuerType;
import com.serff.template.svcarea.hix.pm.ServiceAreaType;
import com.serff.template.svcarea.niem.core.IdentificationType;
import com.serff.template.svcarea.niem.core.ObjectFactory;
import com.serff.template.svcarea.niem.core.ProperNameTextType;
import com.serff.template.svcarea.niem.core.TextType;
import com.serff.template.svcarea.niem.fips.USCountyCodeType;
import com.serff.template.svcarea.niem.proxy.xsd.Boolean;
import com.serff.template.svcarea.niem.usps.states.USStateCodeSimpleType;
import com.serff.template.svcarea.niem.usps.states.USStateCodeType;

/**
 * Class is used to generate Service Area Template in XML format from PUF Excel data.
 * 
 * @author Bhavin Parmar
 * @since Aug 8, 2016
 */
public class ServiceAreaPUFToSERFFAdapter extends PUFToSERFFAdapter {

	private static final int DATA_OFFSET = 6;
	private static final int DATA_ISSUER_ID2_COL_INDEX = 0;
	private static final int DATA_STATE_CODE2_COL_INDEX = 1;
	private static final int DATA_SERVICE_AREA_ID_COL_INDEX = 2;
	private static final int DATA_SERVICE_AREA_NAME_COL_INDEX = 3;
	private static final int DATA_COVER_ENTIRE_STATE_COL_INDEX = 4;
	private static final int DATA_COUNTY_COL_INDEX = 5;
	private static final int DATA_PARTIAL_COUNTY_COL_INDEX = 6;
	private static final int DATA_ZIP_CODES_COL_INDEX = 7;
	private static final int DATA_PARTIAL_COUNTY_JUSTIFICATION_COL_INDEX = 8;

	public ServiceAreaPUFToSERFFAdapter(String pufFilePath) {

		super(pufFilePath, 
				new int[] {DATA_ISSUER_ID2_COL_INDEX + DATA_OFFSET,
				DATA_STATE_CODE2_COL_INDEX + DATA_OFFSET,
				DATA_SERVICE_AREA_ID_COL_INDEX + DATA_OFFSET,
				DATA_SERVICE_AREA_NAME_COL_INDEX + DATA_OFFSET,
				DATA_COVER_ENTIRE_STATE_COL_INDEX + DATA_OFFSET,
				DATA_COUNTY_COL_INDEX + DATA_OFFSET,
				DATA_PARTIAL_COUNTY_COL_INDEX + DATA_OFFSET,
				DATA_ZIP_CODES_COL_INDEX + DATA_OFFSET,
				DATA_PARTIAL_COUNTY_JUSTIFICATION_COL_INDEX + DATA_OFFSET},
				null,
				null);
	}

	@Override
	protected void setValidationRules() {

		List<String[]> pufRateData = getPufData();

		if (CollectionUtils.isEmpty(pufRateData)) {
			return;
		}
		addValidator(DATA_ISSUER_ID2_COL_INDEX, "HIOS Issuer ID", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufRateData.get(0)[DATA_ISSUER_ID2_COL_INDEX]));
		addValidator(DATA_STATE_CODE2_COL_INDEX, "State", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufRateData.get(0)[DATA_STATE_CODE2_COL_INDEX]));
		addValidator(DATA_SERVICE_AREA_ID_COL_INDEX, "Service Area ID", Validators.getValidatorRule(Validators.TYPE.NOT_NULL, null, null, null));
		addValidator(DATA_SERVICE_AREA_NAME_COL_INDEX, "Service Area Name", Validators.getValidatorRule(Validators.TYPE.NOT_NULL, null, null, null));
	}

	@Override
	public Object createSERFFTemplateObj() {

		PayloadType serviceArea = null;
		List<String[]> pufRateData = getPufData();

		if (CollectionUtils.isNotEmpty(pufRateData)) {
			serviceArea = new PayloadType();
			serviceArea.setIssuer(new IssuerType());

			com.serff.template.svcarea.niem.proxy.xsd.String idValue = new com.serff.template.svcarea.niem.proxy.xsd.String();
			idValue.setValue(getWholeNumberPart(pufRateData.get(0)[DATA_ISSUER_ID2_COL_INDEX]));
			IdentificationType identificationType = new IdentificationType();
			identificationType.setIdentificationID(idValue);
			serviceArea.getIssuer().setIssuerIdentification(identificationType);
			serviceArea.getIssuer().setIssuerStateCode(createUSStateCodeType(pufRateData.get(0)[DATA_STATE_CODE2_COL_INDEX]));
			setTemplateFileName(idValue.getValue() + "-" + pufRateData.get(0)[DATA_STATE_CODE2_COL_INDEX]  + "-ServiceArea.xml"); 

			for (String[] rateData : pufRateData) {
				serviceArea.getIssuer().getServiceArea().add(addServiceArea(rateData));
			}
		}
		return serviceArea;
	}

	private USStateCodeType createUSStateCodeType(String usStateCode) {
		debug("Creating USStateCodeType : usStateCode : " + usStateCode);
		USStateCodeType usStateCodeType = new USStateCodeType();
		usStateCodeType.setValue(USStateCodeSimpleType.fromValue(usStateCode));
		return usStateCodeType;
	}

	private ServiceAreaType addServiceArea(String[] rateData) {

		final String YES_OPTION = "yes";
		ServiceAreaType serviceArea = new ServiceAreaType();
		IdentificationType serviceAreaIdentification = new IdentificationType();
		com.serff.template.svcarea.niem.proxy.xsd.String svcAreaIDValue = new com.serff.template.svcarea.niem.proxy.xsd.String();
		svcAreaIDValue.setValue(rateData[DATA_SERVICE_AREA_ID_COL_INDEX]);
		serviceAreaIdentification.setIdentificationID(svcAreaIDValue);
		serviceArea.setServiceAreaIdentification(serviceAreaIdentification);

		ProperNameTextType svceAreaName = new ProperNameTextType();
		svceAreaName.setValue(rateData[DATA_SERVICE_AREA_NAME_COL_INDEX]);
		serviceArea.setServiceAreaName(svceAreaName);

		boolean entireState = false;
		if (YES_OPTION.equalsIgnoreCase(rateData[DATA_COVER_ENTIRE_STATE_COL_INDEX])) {
			entireState = true;
		}

		boolean partialCounty = false;
		if (YES_OPTION.equalsIgnoreCase(rateData[DATA_PARTIAL_COUNTY_COL_INDEX])) {
			partialCounty = true;
		}
		// Adding Geographic Area Type
		serviceArea.getGeographicArea().add(addGeographicArea(rateData, entireState, partialCounty));
		return serviceArea;
	}

	private GeographicAreaType addGeographicArea(String[] rateData, boolean entireState, boolean partialCounty) {

		final String COMMA = ",";
		GeographicAreaType geoArea = new GeographicAreaType();
		Boolean entireStateIndicator = new Boolean();
		entireStateIndicator.setValue(entireState);
		geoArea.setGeographicAreaEntireStateIndicator(entireStateIndicator);

		if (!entireState) {

			USCountyCodeType usCountyCode = new USCountyCodeType();
			String countyCode = getWholeNumberPart(rateData[DATA_COUNTY_COL_INDEX]);
			if(StringUtils.isNotBlank(countyCode) && countyCode.length() == 5) {
				countyCode = countyCode.substring(2);
			}
			usCountyCode.setValue(countyCode);

			ObjectFactory countyCodeObject = new com.serff.template.svcarea.niem.core.ObjectFactory();
			JAXBElement<USCountyCodeType> countyCodeValue = countyCodeObject.createLocationCountyCode(usCountyCode);
			geoArea.setLocationCounty(countyCodeValue);

			Boolean partialCountyIndicator = new Boolean();
			partialCountyIndicator.setValue(partialCounty);
			geoArea.setGeographicAreaPartialCountyIndicator(partialCountyIndicator);

			if (partialCounty) {
				List<com.serff.template.svcarea.niem.proxy.xsd.String> postalCodesList = geoArea.getLocationPostalCode();

				if (StringUtils.isNotBlank(rateData[DATA_ZIP_CODES_COL_INDEX])) {

					String[] postalCodeArray = rateData[DATA_ZIP_CODES_COL_INDEX].split(COMMA);

					for (String postalCode : postalCodeArray) {
						com.serff.template.svcarea.niem.proxy.xsd.String postalCodeObject = new com.serff.template.svcarea.niem.proxy.xsd.String();
						postalCodeObject.setValue(postalCode.trim()); // Trim Postal Code
						postalCodesList.add(postalCodeObject);
					}
				}
				TextType partialCountyJustificationText = new TextType();
				partialCountyJustificationText.setValue(rateData[DATA_PARTIAL_COUNTY_JUSTIFICATION_COL_INDEX]);
				geoArea.setGeographicAreaPartialCountyJustificationText(partialCountyJustificationText);
			}
		}
		return geoArea;
	}

	public static void main(String[] args) {
//		PUFToSERFFAdapter adpt = new ServiceAreaPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\10091-ServiceArea_PUF.xlsx");
		PUFToSERFFAdapter adpt = new ServiceAreaPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\33709-ServiceArea_PUF.xlsx");
//		PUFToSERFFAdapter adpt = new ServiceAreaPUFToSERFFAdapter("C:\\Users\\parmar_b\\Documents\\aaCode\\PUF-2017\\10091\\validation\\33709-ServiceArea_PUF_Error1.xlsx");
//		adpt.setDebugEnabled(true);
		adpt.generateSERFFTemplate(null);
	}
}
