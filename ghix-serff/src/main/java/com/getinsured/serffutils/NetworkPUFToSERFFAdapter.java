package com.getinsured.serffutils;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.serff.template.networkProv.extension.PayloadType;
import com.serff.template.networkProv.hix.core.OrganizationAugmentationType;
import com.serff.template.networkProv.hix.core.OrganizationType;
import com.serff.template.networkProv.hix.pm.HealthcareProviderNetworkType;
import com.serff.template.networkProv.hix.pm.IssuerType;
import com.serff.template.networkProv.niem.core.ContactInformationType;
import com.serff.template.networkProv.niem.core.IdentificationType;
import com.serff.template.networkProv.niem.core.TextType;
import com.serff.template.networkProv.structures.ReferenceType;
import com.serff.template.networkProv.usps.states.USStateCodeSimpleType;
import com.serff.template.networkProv.usps.states.USStateCodeType;
import com.serff.template.networkProv.xsd.AnyURI;

/**
 * Class is used to generate Network Template in XML format from PUF Excel data.
 */
public class NetworkPUFToSERFFAdapter extends PUFToSERFFAdapter {

	private static final int DATA_OFFSET = 6;
	
	private static final int DATA_ISSUER_HIOS_ID_COL_INDEX = 0;
	private static final int DATA_STATE_CODE_COL_INDEX = 1;
	private static final int DATA_NETWORK_NAME_COL_INDEX = 2;
	private static final int DATA_NETWORK_ID_COL_INDEX = 3;
	private static final int DATA_NETWORK_URL_COL_INDEX = 4;

	public NetworkPUFToSERFFAdapter(String pufFilePath) {
		super(pufFilePath, 
				new int[] {DATA_ISSUER_HIOS_ID_COL_INDEX + DATA_OFFSET,
						DATA_STATE_CODE_COL_INDEX + DATA_OFFSET,
						DATA_NETWORK_NAME_COL_INDEX + DATA_OFFSET,
						DATA_NETWORK_ID_COL_INDEX + DATA_OFFSET,
						DATA_NETWORK_URL_COL_INDEX + DATA_OFFSET},
				null, 
				null);
	}
	
	@Override
	protected void setValidationRules() {

		List<String[]> pufNetworkData = getPufData();

		if (CollectionUtils.isEmpty(pufNetworkData)) {
			return;
		}
		addValidator(DATA_ISSUER_HIOS_ID_COL_INDEX, "HIOS Issuer ID", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufNetworkData.get(0)[DATA_ISSUER_HIOS_ID_COL_INDEX]));
		addValidator(DATA_STATE_CODE_COL_INDEX, "State", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufNetworkData.get(0)[DATA_STATE_CODE_COL_INDEX]));
		addValidator(DATA_NETWORK_ID_COL_INDEX, "Network ID", Validators.getValidatorRule(Validators.TYPE.NOT_NULL, null, null, null));
	}

	@Override
	public Object createSERFFTemplateObj() {
		PayloadType network = null;
		List<String[]> pufNetworkData = getPufData();
		if(CollectionUtils.isNotEmpty(pufNetworkData)) {
			network = new PayloadType();
			String hiosId = getWholeNumberPart(pufNetworkData.get(0)[DATA_ISSUER_HIOS_ID_COL_INDEX]);
			setTemplateFileName(hiosId +  "-" + pufNetworkData.get(0)[DATA_STATE_CODE_COL_INDEX] + "-Network.xml"); 
			network.setIssuer(getIssuer(hiosId, pufNetworkData.get(0)[DATA_STATE_CODE_COL_INDEX]));
			for (String[] networkData : pufNetworkData) {
				addNetwork(network, networkData[DATA_NETWORK_ID_COL_INDEX], null, 
						networkData[DATA_NETWORK_URL_COL_INDEX], networkData[DATA_NETWORK_NAME_COL_INDEX]);
			}
		}
		
		return network;
	}
	
	private void addNetwork(PayloadType network, String networkID, String roleOfOrganizationReference, String websiteURI,
			String marketingName) {
		debug("adding Network : networkID : " + networkID + " : roleOfOrganizationReference : "
			+ roleOfOrganizationReference + " : websiteURI : " + websiteURI + " : marketingName : " + marketingName);
		OrganizationType organizationType = new OrganizationType();
		organizationType
				.setOrganizationPrimaryContactInformation(createOrganizationPrimaryContactInformation(websiteURI));
		organizationType
				.setOrganizationAugmentation(createOrganizationAugmentationType(marketingName));
		network.getOrganization().add(organizationType);
		network.getIssuer()
				.getHealthcareProviderNetwork()
				.add(createHealthcareProviderNetworkType(networkID,
						roleOfOrganizationReference));
	}
	
	
	private ContactInformationType createOrganizationPrimaryContactInformation(String websiteURI) {
		debug("creating OrganizationPrimaryContactInformation : websiteURI : " + websiteURI);
		ContactInformationType contactInformation = new ContactInformationType();
		AnyURI uri = new AnyURI();
		uri.setValue(websiteURI);
		contactInformation.setContactWebsiteURI(uri);

		return contactInformation;
	}

	private OrganizationAugmentationType createOrganizationAugmentationType(String marketingName) {
		debug("creating OrganizationAugmentationType : marketingName : " + marketingName);
		OrganizationAugmentationType organizationAugmentationType = new OrganizationAugmentationType();
		TextType text = new TextType();
		text.setValue(marketingName);
		organizationAugmentationType.setOrganizationMarketingName(text);

		return organizationAugmentationType;
	}

	private HealthcareProviderNetworkType createHealthcareProviderNetworkType(
			String networkID, String roleOfOrganizationReference) {
		debug("creating HealthcareProviderNetworkType: networkID : "
				+ networkID + " : roleOfOrganizationReference : " + roleOfOrganizationReference);
		HealthcareProviderNetworkType healthcareProviderNetworkType = new HealthcareProviderNetworkType();
		healthcareProviderNetworkType
				.setProviderNetworkIdentification(createIdentificationType(networkID));
		healthcareProviderNetworkType
				.setRoleOfOrganizationReference(createReferenceType(roleOfOrganizationReference));
		return healthcareProviderNetworkType;
	}

	private IssuerType getIssuer(String issuerIdentification, String issuerStateCode) {
		debug("Creating Issuer() : issuerIdentification : " + issuerIdentification + " : issuerStateCode : " + issuerStateCode);
		IssuerType issuer = new IssuerType();
		issuer.setIssuerIdentification(createIdentificationType(issuerIdentification));
		issuer.setIssuerStateCode(createUSStateCodeType(issuerStateCode));
		return issuer;
	}

	private USStateCodeType createUSStateCodeType(String usStateCode) {
		debug("creating USStateCodeType : usStateCode : " + usStateCode);
		USStateCodeType usStateCodeType = new USStateCodeType();
		usStateCodeType.setValue(USStateCodeSimpleType.fromValue(usStateCode));

		return usStateCodeType;
	}

	private IdentificationType createIdentificationType(String identificationId) {
		debug("Create IdentificationType : identificationId : " + identificationId);
		IdentificationType identificationType = new IdentificationType();
		com.serff.template.networkProv.xsd.String identificationTypeValue = new com.serff.template.networkProv.xsd.String();
		identificationTypeValue.setValue(identificationId);
		identificationType.setIdentificationID(identificationTypeValue);

		return identificationType;
	}

	private ReferenceType createReferenceType(Object reference) {
		ReferenceType referenceType = new ReferenceType();
		referenceType.setRef(reference);
		return referenceType;
	}

	public static void main(String[] args) {
		PUFToSERFFAdapter adpt = new NetworkPUFToSERFFAdapter("D:\\Ritesh\\Official\\Projects\\Vimo\\Docs\\PUF\\2016-updated\\Sample\\10091-Ind-Network.xlsx");
		//adpt.setDebugEnabled(true);
		adpt.generateSERFFTemplate(null);
	}
}
