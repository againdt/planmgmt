package com.getinsured.serffutils;

import static com.serff.util.SerffConstants.DOLLARSIGN;
import static com.serff.util.SerffConstants.PERCENTAGE;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Read Given Excel File for any issuer.
 * 
 * Reference: http://viralpatel.net/blogs/java-read-write-excel-file-apache-poi/
 * 
 * Assumption: We are reading only xlsx files.
 * 
 * @author polimetla_b
 * @since 11/15/2013
 */
public abstract class ExcelReader {

	private FileInputStream excelFile = null;
	private XSSFWorkbook xssfWorkbook = null;
	private static final String PERCENT_FORMAT = "0%";
	private static final String CURRENCY_FORMAT = "\"$\"";
	private static final int HUNDRED = 100;
	protected String dateFormatStr = "dd/MM/yyyy"; 
	
	public void loadFile(String fileNameGiven) throws IOException {
		System.out.println("Loading Excel File: " + fileNameGiven);
		// fileName = fileNameGiven;
		excelFile = new FileInputStream(new File(fileNameGiven));
		// Get the workbook instance for XLS file
		xssfWorkbook = new XSSFWorkbook(excelFile);
	}

	public void loadFile(InputStream fileInputStream) throws IOException {
		xssfWorkbook = new XSSFWorkbook(fileInputStream);
	}

	public void closeFile() throws IOException {

		if (null != excelFile) {
			excelFile.close();
		}
	}

	public int getNumberOfSheets() {
		return xssfWorkbook.getNumberOfSheets();
	}

	public XSSFSheet getSheet(int tab) throws IOException {

		try {
			// Get first sheet from the workbook
			return xssfWorkbook.getSheetAt(tab);
		}
		catch (IllegalArgumentException e) {
			System.err.println("No sheet available: " + e.getMessage());
			return null;
		}
	}

	public XSSFSheet getSheet(String tabName) throws IOException {
		// Get first sheet from the workbook
		return xssfWorkbook.getSheetAt(xssfWorkbook.getSheetIndex(tabName));
	}

	public int getSheetIndex(String tabName) {
		// Get first sheet from the workbook
		return xssfWorkbook.getSheetIndex(tabName);
	}

	public String getCellValueTrim(Cell cell) {

		String returnValue = getCellValue(cell);

		if (StringUtils.isNotEmpty(returnValue)) {
			return returnValue.trim();
		}
		return returnValue;
	}

	public String getCellValue(Cell cell) {
		return getCellValue(cell, false); 
	}

	public String getCellValueTrim(Cell cell, boolean ignoreDecimal) {

		String returnValue = getCellValue(cell, ignoreDecimal);

		if (StringUtils.isNotEmpty(returnValue)) {
			return returnValue.trim();
		}
		return returnValue;
	}

	public String getCellValue(Cell cell, boolean ignoreDecimal) {

		String returnValue = null;

		if (null == cell) {
			return returnValue;
		}

		if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
			returnValue = getCellNumericValue(cell, ignoreDecimal);
		}
		else if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
			returnValue = cell.getStringCellValue();
		}
		else if (Cell.CELL_TYPE_FORMULA == cell.getCellType()) {
			returnValue = cell.getStringCellValue();
		}
		return returnValue;
	}

	private String getCellNumericValue(Cell cell, boolean ignoreDecimal) {

		String returnValue = null;

		if (null == cell) {
			return returnValue;
		}

		if (DateUtil.isCellDateFormatted(cell)) {
			Date today = cell.getDateCellValue();
			DateFormat dateFormat = new SimpleDateFormat(dateFormatStr);
			returnValue = dateFormat.format(today);
		}
		else {
			double val = cell.getNumericCellValue();
			String cellFormat = cell.getCellStyle().getDataFormatString();

			if (cellFormat.equals(PERCENT_FORMAT)) {
				val = val * HUNDRED;

				if (ignoreDecimal) {
					returnValue = getWholeNumberValue(val) + PERCENTAGE;
				}
				else {
					returnValue = val + PERCENTAGE;
				}
			}
			else if (cellFormat.startsWith(CURRENCY_FORMAT)) {

				if (ignoreDecimal) {
					returnValue = DOLLARSIGN + getWholeNumberValue(val);
				}
				else {
					returnValue = DOLLARSIGN + val;
				}
			}
			else {

				if (ignoreDecimal) {
					returnValue = StringUtils.EMPTY + getWholeNumberValue(val);
				}
				else {
					returnValue = StringUtils.EMPTY + val;
				}
			}
		}
		return returnValue;
	}

	private String getWholeNumberValue(double value) {
		return Integer.toString(Double.valueOf(value).intValue());
	}
}
