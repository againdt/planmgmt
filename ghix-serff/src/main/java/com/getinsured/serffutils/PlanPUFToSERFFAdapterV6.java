package com.getinsured.serffutils;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.serff.template.plan.AvCalculatorVO;
import com.serff.template.plan.BenefitAttributeVO;
import com.serff.template.plan.BenefitsListVO;
import com.serff.template.plan.CostShareVarianceVO;
import com.serff.template.plan.CostShareVariancesListVO;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.HeaderVO;
import com.serff.template.plan.HsaVO;
import com.serff.template.plan.MoopListVO;
import com.serff.template.plan.MoopVO;
import com.serff.template.plan.PackageListVO;
import com.serff.template.plan.PlanAndBenefitsPackageVO;
import com.serff.template.plan.PlanAndBenefitsVO;
import com.serff.template.plan.PlanAttributeVO;
import com.serff.template.plan.PlanBenefitTemplateVO;
import com.serff.template.plan.PlanDeductibleListVO;
import com.serff.template.plan.PlanDeductibleVO;
import com.serff.template.plan.PlansListVO;
import com.serff.template.plan.SBCVO;
import com.serff.template.plan.ServiceVisitListVO;
import com.serff.template.plan.ServiceVisitVO;
import com.serff.template.plan.UrlVO;

public class PlanPUFToSERFFAdapterV6 extends PUFToSERFFAdapter {
	private static final int DATA_ISSUER_HIOS_ID_COL_INDEX = 0;
	private static final int DATA_STATE_CODE_COL_INDEX = 1;
	private static final int DATA_MARKET_COVERAGE_COL_INDEX = 2;
	private static final int DATA_DENTAL_ONLY_COL_INDEX = 3;
	private static final int DATA_TIN_COL_INDEX = 4;
	private static final int DATA_STD_COMP_ID_COL_INDEX = 5;
	private static final int DATA_PLAN_NAME_COL_INDEX = 6;
	private static final int DATA_HIOS_PROD_ID_COL_INDEX = 7;
	private static final int DATA_HPID_COL_INDEX = 8;
	private static final int DATA_NETWORK_ID_COL_INDEX = 9;
	private static final int DATA_SVC_AREA_ID_COL_INDEX = 10;
	private static final int DATA_FORMULARY_ID_COL_INDEX = 11;
	private static final int DATA_IS_NEW_PLAN_COL_INDEX = 12;
	private static final int DATA_PLAN_TYPE_COL_INDEX = 13;
	private static final int DATA_METAL_LEVEL_COL_INDEX = 14;
	private static final int DATA_UNIQUE_PLAN_DESIGN_COL_INDEX = 15;
	private static final int DATA_QHP_NONQHP_COL_INDEX = 16;
	private static final int DATA_IS_NOTICE_REQD_FOR_PREG_COL_INDEX = 17;
	private static final int DATA_IS_REFF_REQD_FOR_SP_COL_INDEX = 18;
	private static final int DATA_SPC_REQ_REFERRAL_COL_INDEX = 19;
	private static final int DATA_PLAN_LEVEL_EXCLUSION_COL_INDEX = 20;
	private static final int DATA_INDIAN_PLAN_COL_INDEX = 21;
	private static final int DATA_COMP_RATING_OFFERED_COL_INDEX = 22;
	private static final int DATA_CHILD_ONLY_OFFERING_COL_INDEX = 23;
	private static final int DATA_CHILD_ONLY_PLAN_ID_COL_INDEX = 24;
	private static final int DATA_WELLNESS_PROG_OFFERED_COL_INDEX = 25;
	private static final int DATA_DISEASE_MGMT_PROG_OFFERED_COL_INDEX = 26;
	private static final int DATA_EHB_PCT_TOTAL_PREMIUM_COL_INDEX = 27;
	private static final int DATA_EHB_PEDT_DENTAL_APPT_QTY_COL_INDEX = 28;
	private static final int DATA_IS_GURANTEED_RATE_COL_INDEX = 29;
	private static final int DATA_SP_DRUG_MAX_COINS_COL_INDEX = 30;
	private static final int DATA_IN_PTNT_COPAY_MAX_DAYS_COL_INDEX = 31;
	private static final int DATA_BGN_PRIMCARE_CS_AFTER_NUM_OF_VISIT_COL_INDEX = 32;
	private static final int DATA_BGN_PRIMCARE_CS_DEDT_COINS_AFTER_NUM_OF_COPAY_COL_INDEX = 33;
	private static final int DATA_PLAN_EFFECTIVE_DATE_COL_INDEX = 34;
	private static final int DATA_PLAN_EXP_DATE_COL_INDEX = 35;
	private static final int DATA_OUT_OF_COUNTRY_COVERAGE_COL_INDEX = 36;
	private static final int DATA_OUT_OF_COUNTRY_COVERAGE_DESC_COL_INDEX = 37;
	private static final int DATA_OUT_OF_SVC_AREA_COVERAGE_COL_INDEX = 38;
	private static final int DATA_OUT_OF_SVC_AREA_COVERAGE_DESC_COL_INDEX = 39;
	private static final int DATA_NATIONAL_NETWORK_COL_INDEX = 40;
	private static final int DATA_URL_ENROL_PAYMENT_COL_INDEX = 41;
	private static final int DATA_FORMULARY_URL_COL_INDEX = 42;
	private static final int DATA_PLAN_ID_COL_INDEX = 43;
	private static final int DATA_CSR_VARIATION_TYPE_COL_INDEX = 44;
	private static final int DATA_ISSUER_ACTUARIAL_VAL_COL_INDEX = 45;
	private static final int DATA_AV_CALC_OUT_NUM_COL_INDEX = 46;
	private static final int DATA_MED_DRUG_DEDT_INTG_COL_INDEX = 47;
	private static final int DATA_MED_DRUG_MOOP_INTG_COL_INDEX = 48;
	private static final int DATA_MULTI_IN_NETWORK_TIERS_COL_INDEX = 49;
	private static final int DATA_FIRST_TIER_UTIL_COL_INDEX = 50;
	private static final int DATA_SECOND_TIER_UTIL_COL_INDEX = 51;
	private static final int DATA_SBC_HV_BABY_DEDUCTIBLE_COL_INDEX = 52;
	private static final int DATA_SBC_HV_BABY_COPAY_COL_INDEX = 53;
	private static final int DATA_SBC_HV_BABY_COINS_COL_INDEX = 54;
	private static final int DATA_SBC_HV_BABY_LIMIT_COL_INDEX = 55;
	private static final int DATA_SBC_HV_DIABETES_DEDUCTIBLE_COL_INDEX = 56;
	private static final int DATA_SBC_HV_DIABETES_COPAY_COL_INDEX = 57;
	private static final int DATA_SBC_HV_DIABETES_COINS_COL_INDEX = 58;
	private static final int DATA_SBC_HV_DIABETES_LIMIT_COL_INDEX = 59;
	private static final int DATA_MEHB_INN_T1_IND_MOOP_COL_INDEX = 60;
	private static final int DATA_MEHB_INN_T1_FMLY_PP_MOOP_COL_INDEX = 61;
	private static final int DATA_MEHB_INN_T1_FMLY_PGRP_MOOP_COL_INDEX = 62;
	private static final int DATA_MEHB_INN_T2_IND_MOOP_COL_INDEX = 63;
	private static final int DATA_MEHB_INN_T2_FMLY_PP_MOOP_COL_INDEX = 64;
	private static final int DATA_MEHB_INN_T2_FMLY_PGRP_MOOP_COL_INDEX = 65;
	private static final int DATA_MEHB_OON_IND_MOOP_COL_INDEX = 66;
	private static final int DATA_MEHB_OON_FMLY_PP_MOOP_COL_INDEX = 67;
	private static final int DATA_MEHB_OON_FMLY_PGRP_MOOP_COL_INDEX = 68;
	private static final int DATA_MEHB_COMBINED_IN_OUT_IND_MOOP_COL_INDEX = 69;
	private static final int DATA_MEHB_COMBINED_IN_OUT_FMLY_PP_MOOP_COL_INDEX = 70;
	private static final int DATA_MEHB_COMBINED_IN_OUT_FMLY_PGRP_MOOP_COL_INDEX = 71;
	private static final int DATA_DEHB_INN_T1_IND_MOOP_COL_INDEX = 72;
	private static final int DATA_DEHB_INN_T1_FMLY_PP_MOOP_COL_INDEX = 73;
	private static final int DATA_DEHB_INN_T1_FMLY_PGRP_MOOP_COL_INDEX = 74;
	private static final int DATA_DEHB_INN_T2_IND_MOOP_COL_INDEX = 75;
	private static final int DATA_DEHB_INN_T2_FMLY_PP_MOOP_COL_INDEX = 76;
	private static final int DATA_DEHB_INN_T2_FMLY_PGRP_MOOP_COL_INDEX = 77;
	private static final int DATA_DEHB_OON_IND_MOOP_COL_INDEX = 78;
	private static final int DATA_DEHB_OON_FMLY_PP_MOOP_COL_INDEX = 79;
	private static final int DATA_DEHB_OON_FMLY_PGRP_MOOP_COL_INDEX = 80;
	private static final int DATA_DEHB_COMBINED_IN_OUT_IND_MOOP_COL_INDEX = 81;
	private static final int DATA_DEHB_COMBINED_IN_OUT_FMLY_PP_MOOP_COL_INDEX = 82;
	private static final int DATA_DEHB_COMBINED_IN_OUT_FMLY_PGRP_MOOP_COL_INDEX = 83;
	private static final int DATA_TEHB_INN_T1_IND_MOOP_COL_INDEX = 84;
	private static final int DATA_TEHB_INN_T1_FMLY_PP_MOOP_COL_INDEX = 85;
	private static final int DATA_TEHB_INN_T1_FMLY_PGRP_MOOP_COL_INDEX = 86;
	private static final int DATA_TEHB_INN_T2_IND_MOOP_COL_INDEX = 87;
	private static final int DATA_TEHB_INN_T2_FMLY_PP_MOOP_COL_INDEX = 88;
	private static final int DATA_TEHB_INN_T2_FMLY_PGRP_MOOP_COL_INDEX = 89;
	private static final int DATA_TEHB_OON_IND_MOOP_COL_INDEX = 90;
	private static final int DATA_TEHB_OON_FMLY_PP_MOOP_COL_INDEX = 91;
	private static final int DATA_TEHB_OON_FMLY_PGRP_MOOP_COL_INDEX = 92;
	private static final int DATA_TEHB_COMBINED_IN_OUT_IND_MOOP_COL_INDEX = 93;
	private static final int DATA_TEHB_COMBINED_IN_OUT_FMLY_PP_MOOP_COL_INDEX = 94;
	private static final int DATA_TEHB_COMBINED_IN_OUT_FMLY_PGRP_MOOP_COL_INDEX = 95;
	private static final int DATA_MEHB_DEDT_INN_T1_IND_COL_INDEX = 96;
	private static final int DATA_MEHB_DEDT_INN_T1_FMLY_PP_COL_INDEX = 97;
	private static final int DATA_MEHB_DEDT_INN_T1_FMLY_PGRP_COL_INDEX = 98;
	private static final int DATA_MEHB_DEDT_INN_T1_COINS_COL_INDEX = 99;
	private static final int DATA_MEHB_DEDT_INN_T2_IND_COL_INDEX = 100;
	private static final int DATA_MEHB_DEDT_INN_T2_FMLY_PP_COL_INDEX = 101;
	private static final int DATA_MEHB_DEDT_INN_T2_FMLY_PGRP_COL_INDEX = 102;
	private static final int DATA_MEHB_DEDT_INN_T2_COINS_COL_INDEX = 103;
	private static final int DATA_MEHB_DEDT_OON_IND_COL_INDEX = 104;
	private static final int DATA_MEHB_DEDT_OON_FMLY_PP_COL_INDEX = 105;
	private static final int DATA_MEHB_DEDT_OON_FMLY_PGRP_COL_INDEX = 106;
	private static final int DATA_MEHB_DEDT_COMBINED_IN_OUT_IND_COL_INDEX = 107;
	private static final int DATA_MEHB_DEDT_COMBINED_IN_OUT_FMLY_PP_COL_INDEX = 108;
	private static final int DATA_MEHB_DEDT_COMBINED_IN_OUT_FMLY_PGRP_COL_INDEX = 109;
	private static final int DATA_DEHB_DEDT_INN_T1_IND_COL_INDEX = 110;
	private static final int DATA_DEHB_DEDT_INN_T1_FMLY_PP_COL_INDEX = 111;
	private static final int DATA_DEHB_DEDT_INN_T1_FMLY_PGRP_COL_INDEX = 112;
	private static final int DATA_DEHB_DEDT_INN_T1_COINS_COL_INDEX = 113;
	private static final int DATA_DEHB_DEDT_INN_T2_IND_COL_INDEX = 114;
	private static final int DATA_DEHB_DEDT_INN_T2_FMLY_PP_COL_INDEX = 115;
	private static final int DATA_DEHB_DEDT_INN_T2_FMLY_PGRP_COL_INDEX = 116;
	private static final int DATA_DEHB_DEDT_INN_T2_COINS_COL_INDEX = 117;
	private static final int DATA_DEHB_DEDT_OON_IND_COL_INDEX = 118;
	private static final int DATA_DEHB_DEDT_OON_FMLY_PP_COL_INDEX = 119;
	private static final int DATA_DEHB_DEDT_OON_FMLY_PGRP_COL_INDEX = 120;
	private static final int DATA_DEHB_DEDT_COMBINED_IN_OUT_IND_COL_INDEX = 121;
	private static final int DATA_DEHB_DEDT_COMBINED_IN_OUT_FMLY_PP_COL_INDEX = 122;
	private static final int DATA_DEHB_DEDT_COMBINED_IN_OUT_FMLY_PGRP_COL_INDEX = 123;
	private static final int DATA_TEHB_DEDT_INN_T1_IND_COL_INDEX = 124;
	private static final int DATA_TEHB_DEDT_INN_T1_FMLY_PP_COL_INDEX = 125;
	private static final int DATA_TEHB_DEDT_INN_T1_FMLY_PGRP_COL_INDEX = 126;
	private static final int DATA_TEHB_DEDT_INN_T1_COINS_COL_INDEX = 127;
	private static final int DATA_TEHB_DEDT_INN_T2_IND_COL_INDEX = 128;
	private static final int DATA_TEHB_DEDT_INN_T2_FMLY_PP_COL_INDEX = 129;
	private static final int DATA_TEHB_DEDT_INN_T2_FMLY_PGRP_COL_INDEX = 130;
	private static final int DATA_TEHB_DEDT_INN_T2_COINS_COL_INDEX = 131;
	private static final int DATA_TEHB_DEDT_OON_IND_COL_INDEX = 132;
	private static final int DATA_TEHB_DEDT_OON_FMLY_PP_COL_INDEX = 133;
	private static final int DATA_TEHB_DEDT_OON_FMLY_PGRP_COL_INDEX = 134;
	private static final int DATA_TEHB_DEDT_COMBINED_IN_OUT_IND_COL_INDEX = 135;
	private static final int DATA_TEHB_DEDT_COMBINED_IN_OUT_FMLY_PP_COL_INDEX = 136;
	private static final int DATA_TEHB_DEDT_COMBINED_IN_OUT_FMLY_PGRP_COL_INDEX = 137;
	private static final int DATA_IS_HSA_ELIGIBLE_COL_INDEX = 138;
	private static final int DATA_HAS_HRA_EMPLR_CONTRI_COL_INDEX = 139;
	private static final int DATA_HAS_HRA_EMPLR_CONTRI_AMT_COL_INDEX = 140;
	private static final int DATA_URL_FOR_SBC_COL_INDEX = 141;
	private static final int DATA_PLAN_BROCHURE_COL_INDEX = 142;
	private static final int DATA_DESIGN_TYPE_COL_INDEX = 143;
	private static final int DATA_PLAN_VAR_MARKETING_NAME_COL_INDEX = 144; //100
	private static final int DATA_SBC_HV_SIMPLE_FRACTURE_DEDUCTIBLE_COL_INDEX = 145; //84
	private static final int DATA_SBC_HV_SIMPLE_FRACTURE_COPAY_COL_INDEX = 146; //84
	private static final int DATA_SBC_HV_SIMPLE_FRACTURE_COINS_COL_INDEX = 147; //84
	private static final int DATA_SBC_HV_SIMPLE_FRACTURE_LIMIT_COL_INDEX = 148; //84
	

	private static final int AD_DATA_ISSUER_HIOS_ID_COL_INDEX = 0;
	private static final int AD_DATA_STATE_CODE_COL_INDEX = 1;
	private static final int AD_DATA_STD_COMP_ID_COL_INDEX = 2;
	private static final int AD_DATA_PLAN_ID_COL_INDEX = 3;
	private static final int AD_DATA_BENEFIT_NAME_COL_INDEX = 4;
	private static final int AD_DATA_COPAY_INN_T1_COL_INDEX = 5;
	private static final int AD_DATA_COPAY_INN_T2_COL_INDEX = 6;
	private static final int AD_DATA_COPAY_OON_COL_INDEX = 7;
	private static final int AD_DATA_COINS_INN_T1_COL_INDEX = 8;
	private static final int AD_DATA_COINS_INN_T2_COL_INDEX = 9;
	private static final int AD_DATA_COINS_OON_COL_INDEX = 10;
	private static final int AD_DATA_IS_EHB_COL_INDEX = 11;
	private static final int AD_DATA_IS_STATE_MANDATE_COL_INDEX = 12;
	private static final int AD_DATA_IS_COVERED_COL_INDEX = 13;
	private static final int AD_DATA_QTY_LIMIT_ON_SVC_COL_INDEX = 14;
	private static final int AD_DATA_LIMIT_QTY_COL_INDEX = 15;
	private static final int AD_DATA_LIMIT_UNIT_COL_INDEX = 16;
	private static final int AD_DATA_MIN_STAY_COL_INDEX = 17;
	private static final int AD_DATA_EXCLUSIONS_COL_INDEX = 18;
	private static final int AD_DATA_EXPLANATION_COL_INDEX = 19;
	private static final int AD_DATA_EHB_VAR_REASON_COL_INDEX = 20;
	private static final int AD_DATA_IS_EXCLD_FROM_INN_MOOP_COL_INDEX = 21;
	private static final int AD_DATA_IS_EXCLD_FROM_OON_MOOP_COL_INDEX = 22;

	private static final String DOT_ZERO = ".0";
	private static final String DOT_ZERO_ZERO = ".00";
	private static final String DOT_ZERO_ZERO_PERCENT = ".00%";
	private static final String DOT_ZERO_PERCENT = ".0%";
	
	private static int[] colIndex = {DATA_ISSUER_HIOS_ID_COL_INDEX + 2,
			DATA_STATE_CODE_COL_INDEX,
			DATA_MARKET_COVERAGE_COL_INDEX + 3,
			DATA_DENTAL_ONLY_COL_INDEX + 3,
			DATA_TIN_COL_INDEX + 3,
			DATA_STD_COMP_ID_COL_INDEX + 3,
			DATA_PLAN_NAME_COL_INDEX + 3,
			DATA_HIOS_PROD_ID_COL_INDEX + 3,
			DATA_HPID_COL_INDEX + 3,
			DATA_NETWORK_ID_COL_INDEX + 3,
			DATA_SVC_AREA_ID_COL_INDEX + 3,
			DATA_FORMULARY_ID_COL_INDEX + 3,
			DATA_IS_NEW_PLAN_COL_INDEX + 3,
			DATA_PLAN_TYPE_COL_INDEX + 3,
			DATA_METAL_LEVEL_COL_INDEX + 3,
			DATA_UNIQUE_PLAN_DESIGN_COL_INDEX + 4,
			DATA_QHP_NONQHP_COL_INDEX + 4,
			DATA_IS_NOTICE_REQD_FOR_PREG_COL_INDEX + 4,
			DATA_IS_REFF_REQD_FOR_SP_COL_INDEX + 4,
			DATA_SPC_REQ_REFERRAL_COL_INDEX + 4,
			DATA_PLAN_LEVEL_EXCLUSION_COL_INDEX + 4,
			DATA_INDIAN_PLAN_COL_INDEX + 4,
			DATA_COMP_RATING_OFFERED_COL_INDEX + 4,
			DATA_CHILD_ONLY_OFFERING_COL_INDEX + 4,
			DATA_CHILD_ONLY_PLAN_ID_COL_INDEX + 4,
			DATA_WELLNESS_PROG_OFFERED_COL_INDEX + 4,
			DATA_DISEASE_MGMT_PROG_OFFERED_COL_INDEX + 4,
			DATA_EHB_PCT_TOTAL_PREMIUM_COL_INDEX + 4,
			DATA_EHB_PEDT_DENTAL_APPT_QTY_COL_INDEX + 4,
			DATA_IS_GURANTEED_RATE_COL_INDEX + 4,
			DATA_SP_DRUG_MAX_COINS_COL_INDEX + 35,
			DATA_IN_PTNT_COPAY_MAX_DAYS_COL_INDEX + 35,
			DATA_BGN_PRIMCARE_CS_AFTER_NUM_OF_VISIT_COL_INDEX + 35,
			DATA_BGN_PRIMCARE_CS_DEDT_COINS_AFTER_NUM_OF_COPAY_COL_INDEX + 35,
			DATA_PLAN_EFFECTIVE_DATE_COL_INDEX,
			DATA_PLAN_EXP_DATE_COL_INDEX,
			DATA_OUT_OF_COUNTRY_COVERAGE_COL_INDEX,
			DATA_OUT_OF_COUNTRY_COVERAGE_DESC_COL_INDEX,
			DATA_OUT_OF_SVC_AREA_COVERAGE_COL_INDEX,
			DATA_OUT_OF_SVC_AREA_COVERAGE_DESC_COL_INDEX,
			DATA_NATIONAL_NETWORK_COL_INDEX,
			DATA_URL_ENROL_PAYMENT_COL_INDEX,
			DATA_FORMULARY_URL_COL_INDEX,
			DATA_PLAN_ID_COL_INDEX,
			DATA_CSR_VARIATION_TYPE_COL_INDEX + 1,
			DATA_ISSUER_ACTUARIAL_VAL_COL_INDEX + 1,
			DATA_AV_CALC_OUT_NUM_COL_INDEX + 1,
			DATA_MED_DRUG_DEDT_INTG_COL_INDEX + 1,
			DATA_MED_DRUG_MOOP_INTG_COL_INDEX + 1,
			DATA_MULTI_IN_NETWORK_TIERS_COL_INDEX + 1,
			DATA_FIRST_TIER_UTIL_COL_INDEX + 1,
			DATA_SECOND_TIER_UTIL_COL_INDEX + 1,
			DATA_SBC_HV_BABY_DEDUCTIBLE_COL_INDEX + 1,
			DATA_SBC_HV_BABY_COPAY_COL_INDEX + 1,
			DATA_SBC_HV_BABY_COINS_COL_INDEX + 1,
			DATA_SBC_HV_BABY_LIMIT_COL_INDEX + 1,
			DATA_SBC_HV_DIABETES_DEDUCTIBLE_COL_INDEX + 1,
			DATA_SBC_HV_DIABETES_COPAY_COL_INDEX + 1,
			DATA_SBC_HV_DIABETES_COINS_COL_INDEX + 1,
			DATA_SBC_HV_DIABETES_LIMIT_COL_INDEX + 1,
			DATA_MEHB_INN_T1_IND_MOOP_COL_INDEX + 9,
			DATA_MEHB_INN_T1_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_MEHB_INN_T1_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_MEHB_INN_T2_IND_MOOP_COL_INDEX + 9,
			DATA_MEHB_INN_T2_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_MEHB_INN_T2_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_MEHB_OON_IND_MOOP_COL_INDEX + 9,
			DATA_MEHB_OON_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_MEHB_OON_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_MEHB_COMBINED_IN_OUT_IND_MOOP_COL_INDEX + 9,
			DATA_MEHB_COMBINED_IN_OUT_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_MEHB_COMBINED_IN_OUT_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_DEHB_INN_T1_IND_MOOP_COL_INDEX + 9,
			DATA_DEHB_INN_T1_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_DEHB_INN_T1_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_DEHB_INN_T2_IND_MOOP_COL_INDEX + 9,
			DATA_DEHB_INN_T2_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_DEHB_INN_T2_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_DEHB_OON_IND_MOOP_COL_INDEX + 9,
			DATA_DEHB_OON_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_DEHB_OON_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_DEHB_COMBINED_IN_OUT_IND_MOOP_COL_INDEX + 9,
			DATA_DEHB_COMBINED_IN_OUT_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_DEHB_COMBINED_IN_OUT_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_TEHB_INN_T1_IND_MOOP_COL_INDEX + 9,
			DATA_TEHB_INN_T1_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_TEHB_INN_T1_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_TEHB_INN_T2_IND_MOOP_COL_INDEX + 9,
			DATA_TEHB_INN_T2_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_TEHB_INN_T2_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_TEHB_OON_IND_MOOP_COL_INDEX + 9,
			DATA_TEHB_OON_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_TEHB_OON_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_TEHB_COMBINED_IN_OUT_IND_MOOP_COL_INDEX + 9,
			DATA_TEHB_COMBINED_IN_OUT_FMLY_PP_MOOP_COL_INDEX + 9,
			DATA_TEHB_COMBINED_IN_OUT_FMLY_PGRP_MOOP_COL_INDEX + 9,
			DATA_MEHB_DEDT_INN_T1_IND_COL_INDEX + 9,
			DATA_MEHB_DEDT_INN_T1_FMLY_PP_COL_INDEX + 9,
			DATA_MEHB_DEDT_INN_T1_FMLY_PGRP_COL_INDEX + 9,
			DATA_MEHB_DEDT_INN_T1_COINS_COL_INDEX + 9,
			DATA_MEHB_DEDT_INN_T2_IND_COL_INDEX + 9,
			DATA_MEHB_DEDT_INN_T2_FMLY_PP_COL_INDEX + 9,
			DATA_MEHB_DEDT_INN_T2_FMLY_PGRP_COL_INDEX + 9,
			DATA_MEHB_DEDT_INN_T2_COINS_COL_INDEX + 9,
			DATA_MEHB_DEDT_OON_IND_COL_INDEX + 9,
			DATA_MEHB_DEDT_OON_FMLY_PP_COL_INDEX + 9,
			DATA_MEHB_DEDT_OON_FMLY_PGRP_COL_INDEX + 9,
			DATA_MEHB_DEDT_COMBINED_IN_OUT_IND_COL_INDEX + 9,
			DATA_MEHB_DEDT_COMBINED_IN_OUT_FMLY_PP_COL_INDEX + 9,
			DATA_MEHB_DEDT_COMBINED_IN_OUT_FMLY_PGRP_COL_INDEX + 9,
			DATA_DEHB_DEDT_INN_T1_IND_COL_INDEX + 9,
			DATA_DEHB_DEDT_INN_T1_FMLY_PP_COL_INDEX + 9,
			DATA_DEHB_DEDT_INN_T1_FMLY_PGRP_COL_INDEX + 9,
			DATA_DEHB_DEDT_INN_T1_COINS_COL_INDEX + 9,
			DATA_DEHB_DEDT_INN_T2_IND_COL_INDEX + 9,
			DATA_DEHB_DEDT_INN_T2_FMLY_PP_COL_INDEX + 9,
			DATA_DEHB_DEDT_INN_T2_FMLY_PGRP_COL_INDEX + 9,
			DATA_DEHB_DEDT_INN_T2_COINS_COL_INDEX + 9,
			DATA_DEHB_DEDT_OON_IND_COL_INDEX + 9,
			DATA_DEHB_DEDT_OON_FMLY_PP_COL_INDEX + 9,
			DATA_DEHB_DEDT_OON_FMLY_PGRP_COL_INDEX + 9,
			DATA_DEHB_DEDT_COMBINED_IN_OUT_IND_COL_INDEX + 9,
			DATA_DEHB_DEDT_COMBINED_IN_OUT_FMLY_PP_COL_INDEX + 9,
			DATA_DEHB_DEDT_COMBINED_IN_OUT_FMLY_PGRP_COL_INDEX + 9,
			DATA_TEHB_DEDT_INN_T1_IND_COL_INDEX + 9,
			DATA_TEHB_DEDT_INN_T1_FMLY_PP_COL_INDEX + 9,
			DATA_TEHB_DEDT_INN_T1_FMLY_PGRP_COL_INDEX + 9,
			DATA_TEHB_DEDT_INN_T1_COINS_COL_INDEX + 9,
			DATA_TEHB_DEDT_INN_T2_IND_COL_INDEX + 9,
			DATA_TEHB_DEDT_INN_T2_FMLY_PP_COL_INDEX + 9,
			DATA_TEHB_DEDT_INN_T2_FMLY_PGRP_COL_INDEX + 9,
			DATA_TEHB_DEDT_INN_T2_COINS_COL_INDEX + 9,
			DATA_TEHB_DEDT_OON_IND_COL_INDEX + 9,
			DATA_TEHB_DEDT_OON_FMLY_PP_COL_INDEX + 9,
			DATA_TEHB_DEDT_OON_FMLY_PGRP_COL_INDEX + 9,
			DATA_TEHB_DEDT_COMBINED_IN_OUT_IND_COL_INDEX + 9,
			DATA_TEHB_DEDT_COMBINED_IN_OUT_FMLY_PP_COL_INDEX + 9,
			DATA_TEHB_DEDT_COMBINED_IN_OUT_FMLY_PGRP_COL_INDEX + 9,
			DATA_IS_HSA_ELIGIBLE_COL_INDEX + 9,
			DATA_HAS_HRA_EMPLR_CONTRI_COL_INDEX + 9,
			DATA_HAS_HRA_EMPLR_CONTRI_AMT_COL_INDEX + 9,
			DATA_URL_FOR_SBC_COL_INDEX + 9,
			DATA_PLAN_BROCHURE_COL_INDEX + 9,
			DATA_DESIGN_TYPE_COL_INDEX - 125,
			DATA_PLAN_VAR_MARKETING_NAME_COL_INDEX - 100,
			DATA_SBC_HV_SIMPLE_FRACTURE_DEDUCTIBLE_COL_INDEX - 84,
			DATA_SBC_HV_SIMPLE_FRACTURE_COPAY_COL_INDEX - 84,
			DATA_SBC_HV_SIMPLE_FRACTURE_COINS_COL_INDEX - 84,
			DATA_SBC_HV_SIMPLE_FRACTURE_LIMIT_COL_INDEX - 84
			};
	
	private static int[] additionalColIndex = {AD_DATA_ISSUER_HIOS_ID_COL_INDEX + 2,
			AD_DATA_STATE_CODE_COL_INDEX,
			AD_DATA_STD_COMP_ID_COL_INDEX + 3,
			AD_DATA_PLAN_ID_COL_INDEX + 3,
			AD_DATA_BENEFIT_NAME_COL_INDEX + 3,
			AD_DATA_COPAY_INN_T1_COL_INDEX + 3,
			AD_DATA_COPAY_INN_T2_COL_INDEX + 3,
			AD_DATA_COPAY_OON_COL_INDEX + 3,
			AD_DATA_COINS_INN_T1_COL_INDEX + 3,
			AD_DATA_COINS_INN_T2_COL_INDEX + 3,
			AD_DATA_COINS_OON_COL_INDEX + 3,
			AD_DATA_IS_EHB_COL_INDEX + 3,
			AD_DATA_IS_STATE_MANDATE_COL_INDEX + 2,
			AD_DATA_IS_COVERED_COL_INDEX + 2,
			AD_DATA_QTY_LIMIT_ON_SVC_COL_INDEX + 2,
			AD_DATA_LIMIT_QTY_COL_INDEX + 2,
			AD_DATA_LIMIT_UNIT_COL_INDEX + 2,
			AD_DATA_MIN_STAY_COL_INDEX + 1,
			AD_DATA_EXCLUSIONS_COL_INDEX + 1,
			AD_DATA_EXPLANATION_COL_INDEX + 1,
			AD_DATA_EHB_VAR_REASON_COL_INDEX + 1,
			AD_DATA_IS_EXCLD_FROM_INN_MOOP_COL_INDEX + 1,
			AD_DATA_IS_EXCLD_FROM_OON_MOOP_COL_INDEX + 1};

	// BenefitAttribute
	private static final String BENEFIT_TYPE_CODE = "benefitTypeCode";
	private static final String IS_EHB = "isEHB";
	private static final String IS_STATE_MANDATE = "isStateMandate";
	private static final String IS_BENEFIT_COVERED = "isBenefitCovered";
	private static final String SERVICE_LIMIT = "serviceLimit";
	private static final String QUANTITY_LIMIT = "quantityLimit";
	private static final String UNIT_LIMIT = "unitLimit";
	private static final String MINIMUM_STAY = "minimumStay";
	private static final String EXCLUSION = "exclusion";
	private static final String EXPLANATION = "explanation";
	private static final String EHB_VARIANCE_REASON = "ehbVarianceReason";
	//private static final String SUBJECT_TO_DEDUCTIBLE_TIER1 = "subjectToDeductibleTier1";
	//private static final String SUBJECT_TO_DEDUCTIBLE_TIER2 = "subjectToDeductibleTier2";
	private static final String EXCLUDED_IN_NETWORK_MOOP = "excludedInNetworkMOOP";
	private static final String EXCLUDED_OUT_OF_NETWORK_MOOP = "excludedOutOfNetworkMOOP";
	// ServiceVisitVO
	private static final String VISIT_TYPE = "visitType";
	private static final String COPAY_IN_NETWORK_TIER1 = "copayInNetworkTier1";
	private static final String COPAY_IN_NETWORK_TIER2 = "copayInNetworkTier2";
	private static final String COPAY_OUT_OF_NETWORK = "copayOutOfNetwork";
	private static final String COINSURANCE_IN_NETWORK_TIER1 = "coInsuranceInNetworkTier1";
	private static final String COINSURANCE_IN_NETWORK_TIER2 = "coInsuranceInNetworkTier2";
	private static final String COINSURANCE_OUT_OF_NETWORK = "coInsuranceOutOfNetwork";

	// PlanAttributeVO
	private static final String STANDARD_COMPONENT_ID = "standardComponentID";
	private static final String PLAN_MARKETING_NAME = "planMarketingName";
	private static final String HIOS_PRODUCT_ID = "hiosProductID";
	private static final String HP_ID = "hpid";
	private static final String NETWORK_ID = "networkID";
	private static final String SERVICE_AREA_ID = "serviceAreaID";
	private static final String FORMULARY_ID = "formularyID";
	private static final String FORMULARY_URL = "formularyURL";
	private static final String IS_NEW_PLAN = "isNewPlan";
	private static final String PLAN_TYPE = "planType";
	private static final String METAL_LEVEL = "metalLevel";
	private static final String UNIQUE_PLAN_DESIGN = "uniquePlanDesign";
	private static final String QHP_OR_NON_QHP = "qhpOrNonQhp";
	private static final String INSURANCE_PLAN_PREGNANCY_NOTICE_REQ_IND = "insurancePlanPregnancyNoticeReqInd";
	private static final String IS_SPECIALIST_REFERRAL_REQUIRED = "isSpecialistReferralRequired";
	private static final String HEALTHCARE_SPECIALIST_REFERRAL_TYPE = "healthCareSpecialistReferralType";
	private static final String INSURANCE_PLAN_BENEFIT_EXCLUSION_TEXT = "insurancePlanBenefitExclusionText";
	private static final String INDIAN_PLAN_VARIATION = "indianPlanVariation";
	private static final String INSURANCE_PLAN_CMP_PRM_AVBL_IND = "insurancePlanCompositePremiumAvailableIndicator";
	private static final String HSA_ELIGIBILITY = "hsaEligibility";
	private static final String EMPLOYER_HSAHRA_CONTRIBUTION_INDICATOR = "employerHSAHRAContributionIndicator";
	private static final String CHILD_ONLY_OFFERING = "childOnlyOffering";
	private static final String CHILD_ONLY_PLAN_ID = "childOnlyPlanID";
	private static final String IS_WELNESS_PROGRAM_OFFERED = "isWellnessProgramOffered";
	private static final String IS_DISEASE_MGMT_PROGRAM_OFFERED = "isDiseaseMgmtProgramsOffered";
	private static final String EHB_PERCENT_PREMIUM = "ehbPercentPremium";
	private static final String EHB_APPOINTMENT_FOR_PEDIATRIC_DENTAL = "ehbApportionmentForPediatricDental";
	private static final String GUARANTEE_VS_ESTIMATED_RATE = "guaranteedVsEstimatedRate";
	private static final String MAXIMUM_COINSURANCE_FOR_SPECIALTY_DRUGS = "maximumCoinsuranceForSpecialtyDrugs";
	private static final String MAX_NUM_DAYS_FOR_CHARGING_INPATIENT_COPAY = "maxNumDaysForChargingInpatientCopay";
	private static final String BEGIN_PRIMARY_CARE_COST_SHARING_AFTER_SET_NUMBER_VISITS = "BeginPrimaryCareCostSharingAfterSetNumberVisits";
	private static final String BEGIN_PRIMARY_CARE_DEDUCTIBLE_AFTER_SET_NUMBER_OF_COPAYS = "beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays";
	private static final String PLAN_EFFECTIVE_DATE = "planEffectiveDate";
	private static final String PLAN_EXPIRATION_DATE = "planExpirationDate";
	private static final String OUT_OF_COUNTRY_COVERAGE = "outOfCountryCoverage";
	private static final String OUT_OF_COUNTRY_COVERAGE_DESCRIPTION = "outOfCountryCoverageDescription";
	private static final String OUT_OF_SERVICE_AREA_COVERAGE = "outOfServiceAreaCoverage";
	private static final String OUT_OF_SERVICE_AREA_COVERAGE_DESCRIPTION = "outOfServiceAreaCoverageDescription";
	private static final String NATIONAL_NETWORK = "nationalNetwork";
	private static final String SUMMARY_BENEFIT_AND_COVERAGE_URL = "summaryBenefitAndCoverageURL";
	private static final String ENROLLMENT_PAYMENT_URL = "enrollmentPaymentURL";
	private static final String PLAN_BROCHURE = "planBrochure";
	private static final String EMP_CONTRIBUTION_AMOUNT_FOR_HSA_OR_HRA = "empContributionAmountForHSAOrHRA";
	private static final String PLAN_DESIGN_TYPE = "planDesignType";
	// CostShareVarianceVO
	private static final String PLAN_ID = "planId";
	private static final String CSR_VARIATION_TYPE = "csrVariationType";
	private static final String ISSUER_ACTUARIAL_VALUE = "issuerActuarialValue";
	private static final String AV_CALCULATOR_OUTPUT_NUMBER = "avCalculatorOutputNumber";
	private static final String MEDICAL_AND_DRUG_DEDUCTIBLES_INTEGRATED = "medicalAndDrugDeductiblesIntegrated";
	private static final String MEDICAL_AND_DRUG_MAX_OUT_OF_PACKET_INTEGRATED = "medicalAndDrugMaxOutOfPocketIntegrated";
	private static final String MULTIPLE_PROVIDER_TIERS = "multipleProviderTiers";
	private static final String FIRST_TIER_UTILIZATION = "firstTierUtilization";
	private static final String SECOND_TIER_UTILIZATION = "secondTierUtilization";
	//private static final String DEFAULT_COPAY_IN_NETWORK = "defaultCopayInNetwork";
	//private static final String DEFAULT_COPAY_OUT_OF_NETWORK = "defaultCopayOutOfNetwork";
	//private static final String DEFAULT_COINSURANCE_IN_NETWORK = "defaultCoInsuranceInNetwork";
	//private static final String DEFAULT_COINSURANCE_OUT_OF_NETWORK = "defaultCoInsuranceOutOfNetwork";
	// SBCVO
	private static final String HAVING_BABY_DEDUCTIBLE = "havingBabyDeductible";
	private static final String HAVING_BABY_COINSURANCE = "havingBabyCoInsurance";
	private static final String HAVING_BABY_COPAYMENT = "havingBabyCoPayment";
	private static final String HAVING_BABY_LIMIT = "havingBabyLimit";
	private static final String HAVING_DIABETES_DEDUCTIBLE = "havingDiabetesDeductible";
	private static final String HAVING_DIABETES_COPAY = "havingDiabetesCopay";
	private static final String HAVING_DIABETES_COINSURANCE = "havingDiabetesCoInsurance";
	private static final String HAVING_DIABETES_LIMIT = "havingDiabetesLimit";
	private static final String HAVING_SIMPLE_FRACTURE_DEDUCTIBLE = "havingSimplefractureDeductible";
	private static final String HAVING_SIMPLE_FRACTURE_COPAY = "havingSimplefractureCopayment";
	private static final String HAVING_SIMPLE_FRACTURE_COINSURANCE = "havingSimplefractureCoinsurance";
	private static final String HAVING_SIMPLE_FRACTURE_LIMIT = "havingSimplefractureLimit";
	// MoopVO
	private static final String NAME = "name";
	private static final String IN_NETWORK_TIER1_INDIVIDUAL_AMOUNT = "inNetworkTier1IndividualAmount";
	private static final String IN_NETWORK_TIER1_FAMILY_AMOUNT = "inNetworkTier1FamilyAmount";
	private static final String IN_NETWORK_TIER2_INDIVIDUAL_AMOUNT = "inNetworkTier2IndividualAmount";
	private static final String IN_NETWORK_TIER2_FAMILY_AMOUNT = "inNetworkTier2FamilyAmount";
	private static final String OUT_OF_NETWORK_INDIVIDUAL_AMOUNT = "outOfNetworkIndividualAmount";
	private static final String OUT_OF_NETWORK_FAMILY_AMOUNT = "outOfNetworkFamilyAmount";
	private static final String COMBINED_IN_OUT_NETWORK_INDIVIDUAL_AMOUNT = "CombinedInOutNetworkIndividualAmount";
	private static final String COMBINED_IN_OUT_NETWORK_FAMILY_AMOUNT = "combinedInOutNetworkFamilyAmount";
	// PlanDeductibleVO
	private static final String DEDUCTIBLE_TYPE = "DeductibleType";
	private static final String IN_NETWORK_TIER1_INDIVIDUAL = "inNetworkTier1Individual";
	private static final String IN_NETWORK_TIER1_FAMILY = "inNetworkTier1Family";
	private static final String IN_NETWORK_TIER1_COINSURANCE = "coinsuranceInNetworkTier1";
	private static final String IN_NETWORK_TIER_TWO_INDIVIDUAL = "inNetworkTierTwoIndividual";
	//private static final String IN_NETWORK_TIER_TWO_FAMILY = "inNetworkTierTwoFamily";
	private static final String IN_NETWORK_TIER_TWO_COINSURANCE = "coinsuranceInNetworkTier2";
	private static final String OUT_OF_NETWORK_INDIVIDUAL = "outOfNetworkIndividual";
	private static final String OUT_OF_NETWORK_FAMILY = "outOfNetworkFamily";
	private static final String COMBINED_IN_OUT_NETWORK_INDIVIDUAL = "combinedInOrOutNetworkIndividual";
	private static final String COMBINED_IN_OR_OUT_NETWORK_FAMILY = "combinedInOrOutNetworkFamily";
	private static final String PLAN_VARIANT_MARKETING_NAME = "planVariantMarketingName";
	//private static final String COMBINED_IN_OR_OUT_TIER2 = "combinedInOrOutTier2";

	
	private Map<String, PlanAndBenefitsVO> plansMap = null;
	private Map<String, Map<String, BenefitAttributeVO>> planBenefitsMap = null;
	private Map<String, ServiceVisitListVO> serviceVisitMap = null;
	DecimalFormat df = new DecimalFormat("###.##");
	
	public PlanPUFToSERFFAdapterV6(String pufFilePath, String pufAdditionalFilePath) {
		super(pufFilePath, PlanPUFToSERFFAdapterV6.colIndex, pufAdditionalFilePath, PlanPUFToSERFFAdapterV6.additionalColIndex);
		plansMap = new HashMap<String, PlanAndBenefitsVO>();
		planBenefitsMap = new HashMap<String, Map<String, BenefitAttributeVO>>();
		serviceVisitMap = new HashMap<String, ServiceVisitListVO>();
		setDateFormat("MM/dd/yyyy");
	}

	@Override
	protected void setValidationRules() {
		List<String[]> pufData = getPufData();

		if (CollectionUtils.isEmpty(pufData)) {
			return;
		}
		addValidator(DATA_ISSUER_HIOS_ID_COL_INDEX, "HIOS Issuer ID", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufData.get(0)[DATA_ISSUER_HIOS_ID_COL_INDEX]));
		addValidator(DATA_STATE_CODE_COL_INDEX, "State", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufData.get(0)[DATA_STATE_CODE_COL_INDEX]));
		addValidator(DATA_MARKET_COVERAGE_COL_INDEX, "Market Coverage", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufData.get(0)[DATA_MARKET_COVERAGE_COL_INDEX]));
		addValidator(DATA_DENTAL_ONLY_COL_INDEX, "Dental Only Indicator", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufData.get(0)[DATA_DENTAL_ONLY_COL_INDEX]));
		addValidator(DATA_TIN_COL_INDEX, "TIN", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufData.get(0)[DATA_TIN_COL_INDEX]));
		addValidator(DATA_STD_COMP_ID_COL_INDEX, "Standard Component ID", Validators.getValidatorRule(Validators.TYPE.NOT_NULL, null, null, null));
		addAdditionalValidator(AD_DATA_ISSUER_HIOS_ID_COL_INDEX, "Additional: HIOS Issuer ID", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufData.get(0)[DATA_ISSUER_HIOS_ID_COL_INDEX]));
		addAdditionalValidator(AD_DATA_STATE_CODE_COL_INDEX, "Additional: State", Validators.getValidatorRule(Validators.TYPE.EQUAL, null, null, pufData.get(0)[DATA_STATE_CODE_COL_INDEX]));
		addAdditionalValidator(AD_DATA_STD_COMP_ID_COL_INDEX, "Additional: Standard Component ID", Validators.getValidatorRule(Validators.TYPE.NOT_NULL, null, null, null));
		addAdditionalValidator(AD_DATA_PLAN_ID_COL_INDEX, "Additional: Plan ID", Validators.getValidatorRule(Validators.TYPE.NOT_NULL, null, null, null));
		addAdditionalValidator(AD_DATA_BENEFIT_NAME_COL_INDEX, "Additional: Benefit Name", Validators.getValidatorRule(Validators.TYPE.NOT_NULL, null, null, null));
	}

	@Override
	public Object createSERFFTemplateObj() {
		debug("Creating Template... ");
		PlanBenefitTemplateVO planBenefitTemplate = null;
		List<String[]> pufPlanData = getPufData();
		List<String[]> pufBenefitsData = getAdditionalPufData();
		if(CollectionUtils.isNotEmpty(pufPlanData) && CollectionUtils.isNotEmpty(pufBenefitsData)) {
			debug("Creating Template Root... ");
			PlanCostShareVarianceComparator planCostShareVarianceComparator= new PlanCostShareVarianceComparator();
			planBenefitTemplate = new PlanBenefitTemplateVO();
			PackageListVO packageList = new PackageListVO();
			planBenefitTemplate.setPackagesList(packageList);
			processAdditionalData();
			processPlanData();
			PlanAndBenefitsPackageVO planPackage = null;
			String hiosId = getWholeNumberPart(pufPlanData.get(0)[DATA_ISSUER_HIOS_ID_COL_INDEX]);
			String statePostalCode = pufPlanData.get(0)[DATA_STATE_CODE_COL_INDEX];
			String marketCoverage = pufPlanData.get(0)[DATA_MARKET_COVERAGE_COL_INDEX];
			String dentalPlanOnlyInd = pufPlanData.get(0)[DATA_DENTAL_ONLY_COL_INDEX];
			String tin = pufPlanData.get(0)[DATA_TIN_COL_INDEX];
			setTemplateFileName(hiosId + "-" + statePostalCode + "-Plans.xml");
			PlanAndBenefitsVO planAndBenefitsVO = null;
			for (String planId: plansMap.keySet()) {
				planPackage = createPackage(hiosId, statePostalCode, marketCoverage, dentalPlanOnlyInd, tin);
				planAndBenefitsVO = plansMap.get(planId); 
				Collections.sort(planAndBenefitsVO.getCostShareVariancesList().getCostShareVariance(), planCostShareVarianceComparator);
				planPackage.getPlansList().getPlans().add(planAndBenefitsVO);
				planPackage.setBenefitsList(createBenefitsListVO(planBenefitsMap.get(planId)));
				packageList.getPackages().add(planPackage);
			}
		}
		return planBenefitTemplate;
	}
	
	private BenefitsListVO createBenefitsListVO(Map<String, BenefitAttributeVO> benefitsMap) {
		BenefitsListVO benefitsListVo = null;
		if(null != benefitsMap && !benefitsMap.isEmpty()) {
			benefitsListVo = new BenefitsListVO();
			benefitsListVo.getBenefits().addAll(benefitsMap.values());
		}
		return benefitsListVo;
	}
	
	private void processPlanData() {
		//Create a Map of Plans objects
		List<String[]> plansData = getPufData();
		debug("Processing Plan Data, count: " + plansData.size());
		for (String[] planData : plansData) {
			PlanAndBenefitsVO planVO = plansMap.get(planData[DATA_STD_COMP_ID_COL_INDEX]);
			if(null == planVO) {
				planVO = new PlanAndBenefitsVO();
				plansMap.put(planData[DATA_STD_COMP_ID_COL_INDEX], planVO);
				planVO.setCostShareVariancesList(new CostShareVariancesListVO());
				setPlanAttributes(planVO, planData);
				debug("Created Plan and Attributesc set for PlanID: " + planData[DATA_STD_COMP_ID_COL_INDEX]);
			}
			planVO.getCostShareVariancesList().getCostShareVariance().add(createCostShareVariant(planData));
		}
		debug("Done Processing Plan Data, count: " + plansData.size());
	}
	
	private CostShareVarianceVO createCostShareVariant(String[] planData) {
		debug("Adding CSV to Plan ");
		CostShareVarianceVO csvVO = new CostShareVarianceVO();
		setCostShareVariantAttributes(csvVO, planData);
		if("No".equalsIgnoreCase(planData[DATA_DENTAL_ONLY_COL_INDEX])) {
			setSBC(csvVO, planData);
		}
		setHSA(csvVO, planData);
		setURL(csvVO, planData);
		setMOOPList(csvVO, planData);
		setDeductibleList(csvVO, planData);
		setAVCalc(csvVO, planData);
		ServiceVisitListVO svcVisitList = serviceVisitMap.get(planData[DATA_PLAN_ID_COL_INDEX]);
		csvVO.setServiceVisitList(svcVisitList);
		return csvVO;
	}

	private void setMOOPList(CostShareVarianceVO costShareVariance, String[] planData) {
		debug("Adding MOOP for: " + costShareVariance.getPlanId().getCellValue());
		MoopListVO moopList = new MoopListVO();
		costShareVariance.setMoopList(moopList);
		if(StringUtils.isNotBlank(planData[DATA_MEHB_INN_T1_IND_MOOP_COL_INDEX])) {
			moopList.getMoop().add(createMoop("Maximum Out of Pocket for Medical EHB Benefits", 
					planData[DATA_MEHB_INN_T1_IND_MOOP_COL_INDEX], planData[DATA_MEHB_INN_T1_FMLY_PP_MOOP_COL_INDEX], planData[DATA_MEHB_INN_T1_FMLY_PGRP_MOOP_COL_INDEX],
					planData[DATA_MEHB_INN_T2_IND_MOOP_COL_INDEX], planData[DATA_MEHB_INN_T2_FMLY_PP_MOOP_COL_INDEX], planData[DATA_MEHB_INN_T2_FMLY_PGRP_MOOP_COL_INDEX],
					planData[DATA_MEHB_OON_IND_MOOP_COL_INDEX], planData[DATA_MEHB_OON_FMLY_PP_MOOP_COL_INDEX], planData[DATA_MEHB_OON_FMLY_PGRP_MOOP_COL_INDEX],
					planData[DATA_MEHB_COMBINED_IN_OUT_IND_MOOP_COL_INDEX], planData[DATA_MEHB_COMBINED_IN_OUT_FMLY_PP_MOOP_COL_INDEX], planData[DATA_MEHB_COMBINED_IN_OUT_FMLY_PGRP_MOOP_COL_INDEX]));
		}
		if(StringUtils.isNotBlank(planData[DATA_DEHB_INN_T1_IND_MOOP_COL_INDEX])) {
			moopList.getMoop().add(createMoop("Maximum Out of Pocket for Drug EHB Benefits", 
					planData[DATA_DEHB_INN_T1_IND_MOOP_COL_INDEX], planData[DATA_DEHB_INN_T1_FMLY_PP_MOOP_COL_INDEX], planData[DATA_DEHB_INN_T1_FMLY_PGRP_MOOP_COL_INDEX],
					planData[DATA_DEHB_INN_T2_IND_MOOP_COL_INDEX], planData[DATA_DEHB_INN_T2_FMLY_PP_MOOP_COL_INDEX], planData[DATA_DEHB_INN_T2_FMLY_PGRP_MOOP_COL_INDEX],
					planData[DATA_DEHB_OON_IND_MOOP_COL_INDEX], planData[DATA_DEHB_OON_FMLY_PP_MOOP_COL_INDEX], planData[DATA_DEHB_OON_FMLY_PGRP_MOOP_COL_INDEX],
					planData[DATA_DEHB_COMBINED_IN_OUT_IND_MOOP_COL_INDEX], planData[DATA_DEHB_COMBINED_IN_OUT_FMLY_PP_MOOP_COL_INDEX], planData[DATA_DEHB_COMBINED_IN_OUT_FMLY_PGRP_MOOP_COL_INDEX]));
		}
		if(StringUtils.isNotBlank(planData[DATA_TEHB_INN_T1_IND_MOOP_COL_INDEX])) {
			moopList.getMoop().add(createMoop("Maximum Out of Pocket for Medical and Drug EHB Benefits (Total)", 
					planData[DATA_TEHB_INN_T1_IND_MOOP_COL_INDEX], planData[DATA_TEHB_INN_T1_FMLY_PP_MOOP_COL_INDEX], planData[DATA_TEHB_INN_T1_FMLY_PGRP_MOOP_COL_INDEX],
					planData[DATA_TEHB_INN_T2_IND_MOOP_COL_INDEX], planData[DATA_TEHB_INN_T2_FMLY_PP_MOOP_COL_INDEX], planData[DATA_TEHB_INN_T2_FMLY_PGRP_MOOP_COL_INDEX],
					planData[DATA_TEHB_OON_IND_MOOP_COL_INDEX], planData[DATA_TEHB_OON_FMLY_PP_MOOP_COL_INDEX], planData[DATA_TEHB_OON_FMLY_PGRP_MOOP_COL_INDEX],
					planData[DATA_TEHB_COMBINED_IN_OUT_IND_MOOP_COL_INDEX], planData[DATA_TEHB_COMBINED_IN_OUT_FMLY_PP_MOOP_COL_INDEX], planData[DATA_TEHB_COMBINED_IN_OUT_FMLY_PGRP_MOOP_COL_INDEX]));
		}
	}
	
	private MoopVO createMoop(String name, String inNT1IndividualAmount, String inNT1FamilyPPAmount, String inNT1FamilyGrpAmount, 
			String inNT2IndividualAmount, String inNT2FamilyPPAmount, String inNT2FamilyGrpAmount,
			String ooNIndividualAmount, String ooNFamilyPPAmount, String ooNFamilyGrpAmount, 
			String combinedInOutNetworkIndividualAmount, String combinedInOutNetworkFamilyPPAmount, String combinedInOutNetworkFamilyGrpAmount) {
		MoopVO moop = new MoopVO();
		moop.setName(createExcelCell(NAME, name));
		
		moop.setInNetworkTier1IndividualAmount(createExcelCell(IN_NETWORK_TIER1_INDIVIDUAL_AMOUNT, stripDecimalZero(inNT1IndividualAmount)));
		if(StringUtils.isNotBlank(inNT1FamilyPPAmount)) {
			moop.setInNetworkTier1FamilyAmount(createExcelCell(IN_NETWORK_TIER1_FAMILY_AMOUNT, inNT1FamilyPPAmount + " | " + inNT1FamilyGrpAmount));
		}
		
		moop.setInNetworkTier2IndividualAmount(createExcelCell(IN_NETWORK_TIER2_INDIVIDUAL_AMOUNT, stripDecimalZero(inNT2IndividualAmount)));
		if(StringUtils.isNotBlank(inNT2FamilyPPAmount)) {
			moop.setInNetworkTier2FamilyAmount(createExcelCell(IN_NETWORK_TIER2_FAMILY_AMOUNT, inNT2FamilyPPAmount + " | " + inNT2FamilyGrpAmount));
		}
		
		moop.setOutOfNetworkIndividualAmount(createExcelCell(OUT_OF_NETWORK_INDIVIDUAL_AMOUNT, stripDecimalZero(ooNIndividualAmount)));
		if(StringUtils.isNotBlank(ooNFamilyPPAmount)) {
			moop.setOutOfNetworkFamilyAmount(createExcelCell(OUT_OF_NETWORK_FAMILY_AMOUNT, ooNFamilyPPAmount + " | " + ooNFamilyGrpAmount));
		}

		moop.setCombinedInOutNetworkIndividualAmount(createExcelCell(COMBINED_IN_OUT_NETWORK_INDIVIDUAL_AMOUNT, stripDecimalZero(combinedInOutNetworkIndividualAmount)));
		if(StringUtils.isNotBlank(combinedInOutNetworkFamilyPPAmount)) {
			moop.setCombinedInOutNetworkFamilyAmount(createExcelCell(COMBINED_IN_OUT_NETWORK_FAMILY_AMOUNT, combinedInOutNetworkFamilyPPAmount + " | " + combinedInOutNetworkFamilyGrpAmount));
		}
		return moop;
	}
	
	private void setDeductibleList(CostShareVarianceVO costShareVariance, String[] planData) {
		debug("Adding Deductible for: " + costShareVariance.getPlanId().getCellValue());
		PlanDeductibleListVO deductibleList = new PlanDeductibleListVO();
		costShareVariance.setPlanDeductibleList(deductibleList);
		if(StringUtils.isNotBlank(planData[DATA_MEHB_DEDT_INN_T1_IND_COL_INDEX])) {
			deductibleList.getPlanDeductible().add(createDeductible("Medical EHB Deductible", 
					planData[DATA_MEHB_DEDT_INN_T1_IND_COL_INDEX], planData[DATA_MEHB_DEDT_INN_T1_FMLY_PP_COL_INDEX], 
					planData[DATA_MEHB_DEDT_INN_T1_FMLY_PGRP_COL_INDEX], planData[DATA_MEHB_DEDT_INN_T1_COINS_COL_INDEX],
					planData[DATA_MEHB_DEDT_INN_T2_IND_COL_INDEX], planData[DATA_MEHB_DEDT_INN_T2_FMLY_PP_COL_INDEX], 
					planData[DATA_MEHB_DEDT_INN_T2_FMLY_PGRP_COL_INDEX], planData[DATA_MEHB_DEDT_INN_T2_COINS_COL_INDEX],
					planData[DATA_MEHB_DEDT_OON_IND_COL_INDEX], planData[DATA_MEHB_DEDT_OON_FMLY_PP_COL_INDEX], planData[DATA_MEHB_DEDT_OON_FMLY_PGRP_COL_INDEX],
					planData[DATA_MEHB_DEDT_COMBINED_IN_OUT_IND_COL_INDEX], planData[DATA_MEHB_DEDT_COMBINED_IN_OUT_FMLY_PP_COL_INDEX], planData[DATA_MEHB_DEDT_COMBINED_IN_OUT_FMLY_PGRP_COL_INDEX]));
		}
		if(StringUtils.isNotBlank(planData[DATA_DEHB_DEDT_INN_T1_IND_COL_INDEX])) {
			deductibleList.getPlanDeductible().add(createDeductible("Drug EHB Deductible", 
					planData[DATA_DEHB_DEDT_INN_T1_IND_COL_INDEX], planData[DATA_DEHB_DEDT_INN_T1_FMLY_PP_COL_INDEX], 
					planData[DATA_DEHB_DEDT_INN_T1_FMLY_PGRP_COL_INDEX], planData[DATA_DEHB_DEDT_INN_T1_COINS_COL_INDEX],
					planData[DATA_DEHB_DEDT_INN_T2_IND_COL_INDEX], planData[DATA_DEHB_DEDT_INN_T2_FMLY_PP_COL_INDEX], 
					planData[DATA_DEHB_DEDT_INN_T2_FMLY_PGRP_COL_INDEX], planData[DATA_DEHB_DEDT_INN_T2_COINS_COL_INDEX],
					planData[DATA_DEHB_DEDT_OON_IND_COL_INDEX], planData[DATA_DEHB_DEDT_OON_FMLY_PP_COL_INDEX], planData[DATA_DEHB_DEDT_OON_FMLY_PGRP_COL_INDEX],
					planData[DATA_DEHB_DEDT_COMBINED_IN_OUT_IND_COL_INDEX], planData[DATA_DEHB_DEDT_COMBINED_IN_OUT_FMLY_PP_COL_INDEX], planData[DATA_DEHB_DEDT_COMBINED_IN_OUT_FMLY_PGRP_COL_INDEX]));
		}
		if(StringUtils.isNotBlank(planData[DATA_TEHB_DEDT_INN_T1_IND_COL_INDEX])) {
			deductibleList.getPlanDeductible().add(createDeductible("Combined Medical and Drug EHB Deductible", 
					planData[DATA_TEHB_DEDT_INN_T1_IND_COL_INDEX], planData[DATA_TEHB_DEDT_INN_T1_FMLY_PP_COL_INDEX], 
					planData[DATA_TEHB_DEDT_INN_T1_FMLY_PGRP_COL_INDEX], planData[DATA_TEHB_DEDT_INN_T1_COINS_COL_INDEX],
					planData[DATA_TEHB_DEDT_INN_T2_IND_COL_INDEX], planData[DATA_TEHB_DEDT_INN_T2_FMLY_PP_COL_INDEX], 
					planData[DATA_TEHB_DEDT_INN_T2_FMLY_PGRP_COL_INDEX], planData[DATA_TEHB_DEDT_INN_T2_COINS_COL_INDEX],
					planData[DATA_TEHB_DEDT_OON_IND_COL_INDEX], planData[DATA_TEHB_DEDT_OON_FMLY_PP_COL_INDEX], planData[DATA_TEHB_DEDT_OON_FMLY_PGRP_COL_INDEX],
					planData[DATA_TEHB_DEDT_COMBINED_IN_OUT_IND_COL_INDEX], planData[DATA_TEHB_DEDT_COMBINED_IN_OUT_FMLY_PP_COL_INDEX], planData[DATA_TEHB_DEDT_COMBINED_IN_OUT_FMLY_PGRP_COL_INDEX]));
		}
	}

	private PlanDeductibleVO createDeductible(String name, String inNT1IndividualAmount, String inNT1FamilyPPAmount, String inNT1FamilyGrpAmount, String inNT1CoinsAmount, 
			String inNT2IndividualAmount, String inNT2FamilyPPAmount, String inNT2FamilyGrpAmount, String inNT2CoinsAmount,
			String ooNIndividualAmount, String ooNFamilyPPAmount, String ooNFamilyGrpAmount, 
			String combinedInOutNetworkIndividualAmount, String combinedInOutNetworkFamilyPPAmount, String combinedInOutNetworkFamilyGrpAmount) {
		PlanDeductibleVO deductible = new PlanDeductibleVO();
		deductible.setDeductibleType(createExcelCell(DEDUCTIBLE_TYPE, name));
		
		deductible.setInNetworkTier1Individual(createExcelCell(IN_NETWORK_TIER1_INDIVIDUAL, stripDecimalZero(inNT1IndividualAmount)));
		deductible.setCoinsuranceInNetworkTier1(createExcelCell(IN_NETWORK_TIER1_COINSURANCE, stripDecimalZero(inNT1CoinsAmount)));
		if(StringUtils.isNotBlank(inNT1FamilyPPAmount)) {
			deductible.setInNetworkTier1Family(createExcelCell(IN_NETWORK_TIER1_FAMILY, inNT1FamilyPPAmount + " | " + inNT1FamilyGrpAmount));
		}
		
		deductible.setInNetworkTierTwoIndividual(createExcelCell(IN_NETWORK_TIER_TWO_INDIVIDUAL, stripDecimalZero(inNT2IndividualAmount)));
		deductible.setCoinsuranceInNetworkTier2(createExcelCell(IN_NETWORK_TIER_TWO_COINSURANCE, stripDecimalZero(inNT2CoinsAmount)));
		if(StringUtils.isNotBlank(inNT2FamilyPPAmount)) {
			deductible.setInNetworkTierTwoFamily(createExcelCell(IN_NETWORK_TIER1_FAMILY, inNT2FamilyPPAmount + " | " + inNT2FamilyGrpAmount));
		}
		
		deductible.setOutOfNetworkIndividual(createExcelCell(OUT_OF_NETWORK_INDIVIDUAL, stripDecimalZero(ooNIndividualAmount)));
		if(StringUtils.isNotBlank(ooNFamilyPPAmount)) {
			deductible.setOutOfNetworkFamily(createExcelCell(OUT_OF_NETWORK_FAMILY, ooNFamilyPPAmount + " | " + ooNFamilyGrpAmount));
		}

		deductible.setCombinedInOrOutNetworkIndividual(createExcelCell(COMBINED_IN_OUT_NETWORK_INDIVIDUAL, stripDecimalZero(combinedInOutNetworkIndividualAmount)));
		if(StringUtils.isNotBlank(combinedInOutNetworkFamilyPPAmount)) {
			deductible.setCombinedInOrOutNetworkFamily(createExcelCell(COMBINED_IN_OR_OUT_NETWORK_FAMILY, combinedInOutNetworkFamilyPPAmount + " | " + combinedInOutNetworkFamilyGrpAmount));
		}

		//deductible.setCoinsuranceOutofNetwork(value);
		//deductible.setCombinedInOrOutTier2(value);
		return deductible;
	}

	private void setCostShareVariantAttributes(CostShareVarianceVO costShareVariance, String[] planData) {
		costShareVariance.setPlanId(createExcelCell(PLAN_ID, planData[DATA_PLAN_ID_COL_INDEX]));
		costShareVariance.setPlanVariantMarketingName(createExcelCell(PLAN_VARIANT_MARKETING_NAME, planData[DATA_PLAN_VAR_MARKETING_NAME_COL_INDEX]));
		costShareVariance.setMetalLevel(createExcelCell(METAL_LEVEL, planData[DATA_METAL_LEVEL_COL_INDEX]));
		costShareVariance.setCsrVariationType(createExcelCell(CSR_VARIATION_TYPE, planData[DATA_CSR_VARIATION_TYPE_COL_INDEX]));
		costShareVariance.setMedicalAndDrugDeductiblesIntegrated(createExcelCell(MEDICAL_AND_DRUG_DEDUCTIBLES_INTEGRATED, planData[DATA_MED_DRUG_DEDT_INTG_COL_INDEX], true));
		costShareVariance.setMedicalAndDrugMaxOutOfPocketIntegrated(createExcelCell(MEDICAL_AND_DRUG_MAX_OUT_OF_PACKET_INTEGRATED, planData[DATA_MED_DRUG_MOOP_INTG_COL_INDEX], true));
		costShareVariance.setMultipleProviderTiers(createExcelCell(MULTIPLE_PROVIDER_TIERS, planData[DATA_MULTI_IN_NETWORK_TIERS_COL_INDEX], true));
		costShareVariance.setFirstTierUtilization(createExcelCell(FIRST_TIER_UTILIZATION, stripDecimalZero(planData[DATA_FIRST_TIER_UTIL_COL_INDEX])));
		costShareVariance.setSecondTierUtilization(createExcelCell(SECOND_TIER_UTILIZATION, stripDecimalZero(planData[DATA_SECOND_TIER_UTIL_COL_INDEX]), true));
		//costShareVariancesetDefaultCopayInNetwork(createExcelCell(DEFAULT_COPAY_IN_NETWORK, planData[]));
		//costShareVariance.setDefaultCopayOutOfNetwork(createExcelCell(DEFAULT_COPAY_OUT_OF_NETWORK, planData[]));
		//costShareVariance.setDefaultCoInsuranceInNetwork(createExcelCell(DEFAULT_COINSURANCE_IN_NETWORK, planData[]));
		//costShareVariance.setDefaultCoInsuranceOutOfNetwork(createExcelCell(DEFAULT_COINSURANCE_OUT_OF_NETWORK, planData[]));		
	}

	private void setAVCalc(CostShareVarianceVO csvVO, String[] planData) {
		AvCalculatorVO avCalcVo = new AvCalculatorVO();
		avCalcVo.setAvCalculatorOutputNumber(createExcelCell(AV_CALCULATOR_OUTPUT_NUMBER, planData[DATA_AV_CALC_OUT_NUM_COL_INDEX], true));
		avCalcVo.setBeginPrimaryCareCostSharingAfterSetNumberVisits(createExcelCell(BEGIN_PRIMARY_CARE_COST_SHARING_AFTER_SET_NUMBER_VISITS, getWholeNumberPart(planData[DATA_BGN_PRIMCARE_CS_AFTER_NUM_OF_VISIT_COL_INDEX]), true));
		avCalcVo.setBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays(createExcelCell(BEGIN_PRIMARY_CARE_DEDUCTIBLE_AFTER_SET_NUMBER_OF_COPAYS, getWholeNumberPart(planData[DATA_BGN_PRIMCARE_CS_DEDT_COINS_AFTER_NUM_OF_COPAY_COL_INDEX]), true));
		avCalcVo.setIssuerActuarialValue(createExcelCell(ISSUER_ACTUARIAL_VALUE, fractionToPercentage(planData[DATA_ISSUER_ACTUARIAL_VAL_COL_INDEX]), true));
		avCalcVo.setMaximumCoinsuranceForSpecialtyDrugs(createExcelCell(MAXIMUM_COINSURANCE_FOR_SPECIALTY_DRUGS, getWholeNumberPart(planData[DATA_SP_DRUG_MAX_COINS_COL_INDEX]), true));
		avCalcVo.setMaxNumDaysForChargingInpatientCopay(createExcelCell(MAX_NUM_DAYS_FOR_CHARGING_INPATIENT_COPAY, getWholeNumberPart(planData[DATA_IN_PTNT_COPAY_MAX_DAYS_COL_INDEX]), true));
		csvVO.setAvCalculator(avCalcVo);
	}

	private String fractionToPercentage(String fractionStr) {
		if(StringUtils.isNotBlank(fractionStr)) {
			Double db = Double.parseDouble(fractionStr);
			return df.format(db*100) + "%";
		}
		return fractionStr;
	}

	private void setSBC(CostShareVarianceVO costShareVariance, String[] planData) {
		debug("Adding SBC for: " + costShareVariance.getPlanId().getCellValue());
		SBCVO sbcVO = new SBCVO();
		sbcVO.setHavingBabyCoInsurance(createExcelCell(HAVING_BABY_COINSURANCE, getWholeNumberPart(planData[DATA_SBC_HV_BABY_COINS_COL_INDEX]), true));
		sbcVO.setHavingBabyCoPayment(createExcelCell(HAVING_BABY_COPAYMENT, getWholeNumberPart(planData[DATA_SBC_HV_BABY_COPAY_COL_INDEX]), true));
		sbcVO.setHavingBabyDeductible(createExcelCell(HAVING_BABY_DEDUCTIBLE, getWholeNumberPart(planData[DATA_SBC_HV_BABY_DEDUCTIBLE_COL_INDEX]), true));
		sbcVO.setHavingBabyLimit(createExcelCell(HAVING_BABY_LIMIT, getWholeNumberPart(planData[DATA_SBC_HV_BABY_LIMIT_COL_INDEX]), true));
		sbcVO.setHavingDiabetesCoInsurance(createExcelCell(HAVING_DIABETES_COINSURANCE, getWholeNumberPart(planData[DATA_SBC_HV_DIABETES_COINS_COL_INDEX]), true));
		sbcVO.setHavingDiabetesCopay(createExcelCell(HAVING_DIABETES_COPAY, getWholeNumberPart(planData[DATA_SBC_HV_DIABETES_COPAY_COL_INDEX]), true));
		sbcVO.setHavingDiabetesDeductible(createExcelCell(HAVING_DIABETES_DEDUCTIBLE, getWholeNumberPart(planData[DATA_SBC_HV_DIABETES_DEDUCTIBLE_COL_INDEX]), true));
		sbcVO.setHavingDiabetesLimit(createExcelCell(HAVING_DIABETES_LIMIT, getWholeNumberPart(planData[DATA_SBC_HV_DIABETES_LIMIT_COL_INDEX]), true));
		sbcVO.setHavingSimplefractureCoInsurance(createExcelCell(HAVING_SIMPLE_FRACTURE_COINSURANCE, getWholeNumberPart(planData[DATA_SBC_HV_SIMPLE_FRACTURE_COINS_COL_INDEX]), true));
		sbcVO.setHavingSimplefractureCopayment(createExcelCell(HAVING_SIMPLE_FRACTURE_COPAY, getWholeNumberPart(planData[DATA_SBC_HV_SIMPLE_FRACTURE_COPAY_COL_INDEX]), true));
		sbcVO.setHavingSimplefractureDeductible(createExcelCell(HAVING_SIMPLE_FRACTURE_DEDUCTIBLE, getWholeNumberPart(planData[DATA_SBC_HV_SIMPLE_FRACTURE_DEDUCTIBLE_COL_INDEX]), true));
		sbcVO.setHavingSimplefractureLimit(createExcelCell(HAVING_SIMPLE_FRACTURE_LIMIT, getWholeNumberPart(planData[DATA_SBC_HV_SIMPLE_FRACTURE_LIMIT_COL_INDEX]), true));
		costShareVariance.setSbc(sbcVO);
	}
	
	private void setHSA(CostShareVarianceVO costShareVariance, String[] planData) {
		debug("Adding HSA for: " + costShareVariance.getPlanId().getCellValue());
		HsaVO hsaVO = new HsaVO(); 
		hsaVO.setEmpContributionAmountForHSAOrHRA(createExcelCell(EMP_CONTRIBUTION_AMOUNT_FOR_HSA_OR_HRA, planData[DATA_HAS_HRA_EMPLR_CONTRI_AMT_COL_INDEX], true));
		hsaVO.setEmployerHSAHRAContributionIndicator(createExcelCell(EMPLOYER_HSAHRA_CONTRIBUTION_INDICATOR, planData[DATA_HAS_HRA_EMPLR_CONTRI_COL_INDEX], true));
		hsaVO.setHsaEligibility(createExcelCell(HSA_ELIGIBILITY, planData[DATA_IS_HSA_ELIGIBLE_COL_INDEX], true));
		costShareVariance.setHsa(hsaVO);
	}

	private void setURL(CostShareVarianceVO costShareVariance, String[] planData) {
		debug("Adding URL for: " + costShareVariance.getPlanId().getCellValue());
		UrlVO urlVO = new UrlVO();
		urlVO.setPlanBrochure(createExcelCell(PLAN_BROCHURE, planData[DATA_PLAN_BROCHURE_COL_INDEX], true));
		urlVO.setSummaryBenefitAndCoverageURL(createExcelCell(SUMMARY_BENEFIT_AND_COVERAGE_URL, planData[DATA_URL_FOR_SBC_COL_INDEX], true));
		costShareVariance.setUrl(urlVO);
	}

	private void setPlanAttributes(PlanAndBenefitsVO planVO, String[] planData) {
		PlanAttributeVO planAttribute = new PlanAttributeVO();
		planVO.setPlanAttributes(planAttribute);
		planAttribute.setStandardComponentID((createExcelCell(STANDARD_COMPONENT_ID, planData[DATA_STD_COMP_ID_COL_INDEX])));
		planAttribute.setPlanMarketingName((createExcelCell(PLAN_MARKETING_NAME, planData[DATA_PLAN_NAME_COL_INDEX])));
		planAttribute.setHiosProductID((createExcelCell(HIOS_PRODUCT_ID, planData[DATA_HIOS_PROD_ID_COL_INDEX])));
		planAttribute.setHpid((createExcelCell(HP_ID, planData[DATA_HPID_COL_INDEX], true)));
		planAttribute.setNetworkID((createExcelCell(NETWORK_ID, planData[DATA_NETWORK_ID_COL_INDEX])));
		planAttribute.setServiceAreaID((createExcelCell(SERVICE_AREA_ID, planData[DATA_SVC_AREA_ID_COL_INDEX])));
		planAttribute.setFormularyID((createExcelCell(FORMULARY_ID, planData[DATA_FORMULARY_ID_COL_INDEX], true)));
		planAttribute.setIsNewPlan((createExcelCell(IS_NEW_PLAN, planData[DATA_IS_NEW_PLAN_COL_INDEX])));
		planAttribute.setPlanType((createExcelCell(PLAN_TYPE, planData[DATA_PLAN_TYPE_COL_INDEX])));
		planAttribute.setMetalLevel((createExcelCell(METAL_LEVEL, planData[DATA_METAL_LEVEL_COL_INDEX])));
		planAttribute.setPlanDesignType(createExcelCell(PLAN_DESIGN_TYPE, planData[DATA_DESIGN_TYPE_COL_INDEX]));
		planAttribute.setUniquePlanDesign((createExcelCell(UNIQUE_PLAN_DESIGN, planData[DATA_UNIQUE_PLAN_DESIGN_COL_INDEX])));
		planAttribute.setQhpOrNonQhp((createExcelCell(QHP_OR_NON_QHP, planData[DATA_QHP_NONQHP_COL_INDEX], true)));
		planAttribute.setInsurancePlanPregnancyNoticeReqInd((createExcelCell(INSURANCE_PLAN_PREGNANCY_NOTICE_REQ_IND, planData[DATA_IS_NOTICE_REQD_FOR_PREG_COL_INDEX], true)));
		planAttribute.setIsSpecialistReferralRequired((createExcelCell(IS_SPECIALIST_REFERRAL_REQUIRED, planData[DATA_IS_REFF_REQD_FOR_SP_COL_INDEX], true)));
		planAttribute.setHealthCareSpecialistReferralType((createExcelCell(HEALTHCARE_SPECIALIST_REFERRAL_TYPE, planData[DATA_SPC_REQ_REFERRAL_COL_INDEX], true)));
		planAttribute.setInsurancePlanBenefitExclusionText((createExcelCell(INSURANCE_PLAN_BENEFIT_EXCLUSION_TEXT, planData[DATA_PLAN_LEVEL_EXCLUSION_COL_INDEX], true)));
		planAttribute.setIndianPlanVariation((createExcelCell(INDIAN_PLAN_VARIATION, planData[DATA_INDIAN_PLAN_COL_INDEX], true)));
		planAttribute.setInsurancePlanCompositePremiumAvailableIndicator((createExcelCell(INSURANCE_PLAN_CMP_PRM_AVBL_IND, planData[DATA_COMP_RATING_OFFERED_COL_INDEX], true)));
		planAttribute.setChildOnlyOffering((createExcelCell(CHILD_ONLY_OFFERING, planData[DATA_CHILD_ONLY_OFFERING_COL_INDEX], true)));
		planAttribute.setChildOnlyPlanID((createExcelCell(CHILD_ONLY_PLAN_ID, planData[DATA_CHILD_ONLY_PLAN_ID_COL_INDEX], true)));
		planAttribute.setIsWellnessProgramOffered((createExcelCell(IS_WELNESS_PROGRAM_OFFERED, planData[DATA_WELLNESS_PROG_OFFERED_COL_INDEX], true)));
		planAttribute.setIsDiseaseMgmtProgramsOffered((createExcelCell(IS_DISEASE_MGMT_PROGRAM_OFFERED, planData[DATA_DISEASE_MGMT_PROG_OFFERED_COL_INDEX], true)));
		planAttribute.setEhbPercentPremium((createExcelCell(EHB_PERCENT_PREMIUM, planData[DATA_EHB_PCT_TOTAL_PREMIUM_COL_INDEX], true)));
		planAttribute.setEhbApportionmentForPediatricDental((createExcelCell(EHB_APPOINTMENT_FOR_PEDIATRIC_DENTAL, planData[DATA_EHB_PEDT_DENTAL_APPT_QTY_COL_INDEX], true)));
		planAttribute.setGuaranteedVsEstimatedRate((createExcelCell(GUARANTEE_VS_ESTIMATED_RATE, planData[DATA_IS_GURANTEED_RATE_COL_INDEX], true)));
		planAttribute.setPlanEffectiveDate((createExcelCell(PLAN_EFFECTIVE_DATE, planData[DATA_PLAN_EFFECTIVE_DATE_COL_INDEX])));
		planAttribute.setPlanExpirationDate((createExcelCell(PLAN_EXPIRATION_DATE, planData[DATA_PLAN_EXP_DATE_COL_INDEX], true)));
		planAttribute.setOutOfCountryCoverage((createExcelCell(OUT_OF_COUNTRY_COVERAGE, planData[DATA_OUT_OF_COUNTRY_COVERAGE_COL_INDEX])));
		planAttribute.setOutOfCountryCoverageDescription((createExcelCell(OUT_OF_COUNTRY_COVERAGE_DESCRIPTION, planData[DATA_OUT_OF_COUNTRY_COVERAGE_DESC_COL_INDEX], true)));
		planAttribute.setOutOfServiceAreaCoverage((createExcelCell(OUT_OF_SERVICE_AREA_COVERAGE, planData[DATA_OUT_OF_SVC_AREA_COVERAGE_COL_INDEX])));
		planAttribute.setOutOfServiceAreaCoverageDescription((createExcelCell(OUT_OF_SERVICE_AREA_COVERAGE_DESCRIPTION, planData[DATA_OUT_OF_SVC_AREA_COVERAGE_DESC_COL_INDEX], true)));
		planAttribute.setNationalNetwork((createExcelCell(NATIONAL_NETWORK, planData[DATA_NATIONAL_NETWORK_COL_INDEX])));
		planAttribute.setEnrollmentPaymentURL((createExcelCell(ENROLLMENT_PAYMENT_URL, planData[DATA_URL_ENROL_PAYMENT_COL_INDEX], true)));
		planAttribute.setFormularyURL((createExcelCell(FORMULARY_URL, planData[DATA_FORMULARY_URL_COL_INDEX])));
		//planAttribute.setHsaEligibility((createExcelCell(HSA_ELIGIBILITY, planAttributes.getHsaEligibility(), planData[DATA_H])));
		//planAttribute.setEmployerHSAHRAContributionIndicator((createExcelCell(EMPLOYER_HSAHRA_CONTRIBUTION_INDICATOR, planData[])));
		//planAttribute.setEmpContributionAmountForHSAOrHRA((createExcelCell(EMP_CONTRIBUTION_AMOUNT_FOR_HSA_OR_HRA, planData[])));
		//planAttribute.setSummaryBenefitAndCoverageURL((createExcelCell(SUMMARY_BENEFIT_AND_COVERAGE_URL, planData[])));
		//planAttribute.setPlanBrochure((createExcelCell(PLAN_BROCHURE, planData[])));
	}

	private void processAdditionalData() {
		List<String[]> benefitsData = getAdditionalPufData();
		debug("Processing Benefits, count: " + benefitsData.size());
		for (String[] benefitData : benefitsData) {
			//Create a Map of Plan-ID --> Benefits list
			Map<String, BenefitAttributeVO> benefitsMap = planBenefitsMap.get(benefitData[AD_DATA_STD_COMP_ID_COL_INDEX]);
			if(null == benefitsMap) {
				benefitsMap = new HashMap<String, BenefitAttributeVO>();
				planBenefitsMap.put(benefitData[AD_DATA_STD_COMP_ID_COL_INDEX], benefitsMap);
			}
			if(null == benefitsMap.get(benefitData[AD_DATA_BENEFIT_NAME_COL_INDEX])) {
				benefitsMap.put(benefitData[AD_DATA_BENEFIT_NAME_COL_INDEX], createBenefit(benefitData));
			}

			//Create a Map of Plan-CSV-ID --> Service Visit list
			ServiceVisitListVO serviceVisitListVO = serviceVisitMap.get(benefitData[AD_DATA_PLAN_ID_COL_INDEX]);
			if(null == serviceVisitListVO) {
				serviceVisitListVO = new ServiceVisitListVO();
				serviceVisitMap.put(benefitData[AD_DATA_PLAN_ID_COL_INDEX], serviceVisitListVO);
			}
			if(StringUtils.isNotBlank(benefitData[AD_DATA_COINS_INN_T1_COL_INDEX])) {
				serviceVisitListVO.getServiceVisit().add(createServiceVisit(benefitData));
			}
		}
		debug("Done Processing Benefits, count: " + benefitsData.size());
	}

	private BenefitAttributeVO createBenefit(String[] benefitData) {
		BenefitAttributeVO benefitAttribute = new BenefitAttributeVO();
		benefitAttribute.setBenefitTypeCode(createExcelCell(BENEFIT_TYPE_CODE, benefitData[AD_DATA_BENEFIT_NAME_COL_INDEX]));
		benefitAttribute.setIsEHB(createExcelCell(IS_EHB, benefitData[AD_DATA_IS_EHB_COL_INDEX], true));
		//benefitAttribute.setIsStateMandate(createExcelCell(IS_STATE_MANDATE, benefitData[AD_DATA_IS_STATE_MANDATE_COL_INDEX], true));
		benefitAttribute.setIsBenefitCovered(createExcelCell(IS_BENEFIT_COVERED, benefitData[AD_DATA_IS_COVERED_COL_INDEX], true));
		benefitAttribute.setServiceLimit(createExcelCell(SERVICE_LIMIT, getWholeNumberPart(benefitData[AD_DATA_QTY_LIMIT_ON_SVC_COL_INDEX]), true));
		benefitAttribute.setQuantityLimit(createExcelCell(QUANTITY_LIMIT, getWholeNumberPart(benefitData[AD_DATA_LIMIT_QTY_COL_INDEX]), true));
		benefitAttribute.setUnitLimit(createExcelCell(UNIT_LIMIT, benefitData[AD_DATA_LIMIT_UNIT_COL_INDEX], true));
		//benefitAttribute.setMinimumStay(createExcelCell(MINIMUM_STAY, getWholeNumberPart(benefitData[AD_DATA_MIN_STAY_COL_INDEX]), true));
		benefitAttribute.setExclusion(createExcelCell(EXCLUSION, benefitData[AD_DATA_EXCLUSIONS_COL_INDEX], true));
		benefitAttribute.setExplanation(createExcelCell(EXPLANATION, benefitData[AD_DATA_EXPLANATION_COL_INDEX], true));
		benefitAttribute.setEhbVarianceReason(createExcelCell(EHB_VARIANCE_REASON, benefitData[AD_DATA_EHB_VAR_REASON_COL_INDEX], true));
		//benefitAttribute.setSubjectToDeductibleTier1(createExcelCell(SUBJECT_TO_DEDUCTIBLE_TIER1, benefitData[AD_DATA_S]));
		//benefitAttribute.setSubjectToDeductibleTier2(createExcelCell(SUBJECT_TO_DEDUCTIBLE_TIER2, benefitData[AD_DATA_IS_EHB_COL_INDEX]));
		benefitAttribute.setExcludedInNetworkMOOP(createExcelCell(EXCLUDED_IN_NETWORK_MOOP, benefitData[AD_DATA_IS_EXCLD_FROM_INN_MOOP_COL_INDEX], true));
		benefitAttribute.setExcludedOutOfNetworkMOOP(createExcelCell(EXCLUDED_OUT_OF_NETWORK_MOOP, benefitData[AD_DATA_IS_EXCLD_FROM_OON_MOOP_COL_INDEX], true));
		return benefitAttribute;
	}
	
	private ServiceVisitVO createServiceVisit(String[] benefitData) {
		ServiceVisitVO serviceVisit = new ServiceVisitVO();
		serviceVisit.setCoInsuranceInNetworkTier1(createExcelCell(COINSURANCE_IN_NETWORK_TIER1, stripDecimalZero(benefitData[AD_DATA_COINS_INN_T1_COL_INDEX])));
		serviceVisit.setCoInsuranceInNetworkTier2(createExcelCell(COINSURANCE_IN_NETWORK_TIER2, stripDecimalZero(benefitData[AD_DATA_COINS_INN_T2_COL_INDEX])));
		serviceVisit.setCoInsuranceOutOfNetwork(createExcelCell(COINSURANCE_OUT_OF_NETWORK, stripDecimalZero(benefitData[AD_DATA_COINS_OON_COL_INDEX])));
		serviceVisit.setCopayInNetworkTier1(createExcelCell(COPAY_IN_NETWORK_TIER1, stripDecimalZero(benefitData[AD_DATA_COPAY_INN_T1_COL_INDEX])));
		serviceVisit.setCopayInNetworkTier2(createExcelCell(COPAY_IN_NETWORK_TIER2, stripDecimalZero(benefitData[AD_DATA_COPAY_INN_T2_COL_INDEX])));
		serviceVisit.setCopayOutOfNetwork(createExcelCell(COPAY_OUT_OF_NETWORK, stripDecimalZero(benefitData[AD_DATA_COPAY_OON_COL_INDEX])));
		serviceVisit.setVisitType(createExcelCell(VISIT_TYPE, benefitData[AD_DATA_BENEFIT_NAME_COL_INDEX]));
		return serviceVisit;
	}

	public PlanAndBenefitsPackageVO createPackage(String issuerId, String statePostalCode,
			String marketCoverage, String dentalPlanOnlyInd, String tin) {
		debug("createPackage() : issuerId : " + issuerId + " : statePostalCode : " + statePostalCode);
		PlanAndBenefitsPackageVO planAndBenefitsPackage = new PlanAndBenefitsPackageVO();
		HeaderVO header = new HeaderVO();
		header.setTemplateVersion("v6.1");
		header.setIssuerId(createExcelCell("issuerId", issuerId));
		header.setStatePostalCode(createExcelCell("statePostalCode", statePostalCode));
		header.setMarketCoverage(createExcelCell("marketCoverage", marketCoverage));
		header.setDentalPlanOnlyInd(createExcelCell("dentalPlanOnlyInd", dentalPlanOnlyInd));
		header.setTin(createExcelCell("tin", tin));
		planAndBenefitsPackage.setHeader(header);
		//planAndBenefitsPackage.setBenefitsList(new BenefitsListVO());
		planAndBenefitsPackage.setPlansList(new PlansListVO());
		return planAndBenefitsPackage;
	}
	
	public ExcelCellVO createExcelCell(String cellName, String cellValue) {
		return createExcelCell(cellName, cellValue, false);
	}
	
	public ExcelCellVO createExcelCell(String cellName, String cellValue, boolean allowBlank) {
		debug("Creating excel cell : cellName : " + cellName + " cellValue : " + cellValue);
		ExcelCellVO excelCellVo = null;
		
		if(allowBlank || StringUtils.isNotBlank(cellValue)) {
			excelCellVo = new ExcelCellVO();
			if(StringUtils.isEmpty(cellValue)) {
				excelCellVo.setCellValue("");
			} else {
				excelCellVo.setCellValue(cellValue);
			}
		}
		return excelCellVo;
	}

	public static void main(String[] args) {
		String pufFile = "E:\\GetInsured\\PUF\\2017-Sample\\12028-VA-SERFF\\Plan.xlsx";
		String additionalFile = "E:\\GetInsured\\PUF\\2017-Sample\\12028-VA-SERFF\\Benefits.xlsx";
		PUFToSERFFAdapter adpt = new PlanPUFToSERFFAdapterV6(pufFile, additionalFile);
		adpt.generateSERFFTemplate(null);
	}
	
	private String stripDecimalZero(String value) {
		String returnVal = null;
		if(StringUtils.isNotBlank(value)) {
			returnVal = value.trim();
			if(returnVal.endsWith(DOT_ZERO_ZERO)) {
				returnVal = returnVal.replace(DOT_ZERO_ZERO, StringUtils.EMPTY);
			} else if(returnVal.endsWith(DOT_ZERO)) {
				returnVal = returnVal.replace(DOT_ZERO, StringUtils.EMPTY);
			} else if(returnVal.endsWith(DOT_ZERO_ZERO_PERCENT)) {
				returnVal = returnVal.replace(DOT_ZERO_ZERO, StringUtils.EMPTY);
			} else if(returnVal.endsWith(DOT_ZERO_PERCENT)) {
				returnVal = returnVal.replace(DOT_ZERO, StringUtils.EMPTY);
			}
		} else {
			returnVal = value;
		}
		return returnVal;
	}
}

