package com.getinsured.serffutils;

import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import com.serff.template.plan.ExcelCellVO;

public class SERFFTemplateUtil {
	private final static String PLAN = "PLAN"; 
	private final static String NETWORK = "NETWORK"; 
	private final static String SERVICE = "SERVICE"; 
	private final static String RATE = "RATE"; 
	private final static String RULES = "RULES";
	private final static String DEBUG = "DEBUG";
	private static int debugFlag;
	public static void main(String argv[]) {
		/*if(argv.length == 3 && !PLAN.equalsIgnoreCase(argv[0]) ) {
		SERFFTemplateUpgrade converter = new SERFFTemplateUpgrade();
		converter.convertTemplate(argv);
		} else {*/
			if(isValidParams(argv)) {
				PUFToSERFFAdapter adapter = null;
				switch(argv[0]) {
					case PLAN: {
						adapter = new PlanPUFToSERFFAdapterV6(argv[1], argv[2]);
						break;
					}
					case RATE: {
						adapter = new RatePUFToSERFFAdapter(argv[1]);
						break;
					}
					case SERVICE: {
						adapter = new ServiceAreaPUFToSERFFAdapter(argv[1]);
						break;
					}
					case NETWORK: {
						adapter = new NetworkPUFToSERFFAdapter(argv[1]);
						break;
					}
					case RULES: {
						adapter = new BusinessRulesPUFToSERFFAdapter(argv[1]);
						break;
					}
				}
				if(debugFlag == 1) {
					adapter.setDebugEnabled(true);
				}
				adapter.generateSERFFTemplate(null);
			} else {
				SERFFTemplateUtil.class.getSimpleName();
				System.out.println("Invalid params:");
				System.out.println("Usage: " + SERFFTemplateUtil.class.getSimpleName() + " <Template Type> <PUF_CSV_File_Name_with_path> [Additional_PUF_CSV_File_Name_with_path] [DEBUG]");
				System.out.println("Template Type: NETWORK / SERVICE / PLAN / RATE / RULES");
				System.out.println("PUF_CSV_File_Name_with_path: PUF CSV file to be converted to template");
				System.out.println("Additional_PUF_CSV_File_Name_with_path: Benefits PUF CSV file in case of PLAN template");
			}
		//}
	}

	private static boolean isValidParams(String[] argv) {
		debugFlag = DEBUG.equals(argv[argv.length-1]) ? 1 : 0;
		if((PLAN.equalsIgnoreCase(argv[0]) && argv.length == (3 + debugFlag)) || argv.length == (2 + debugFlag)) {
				switch(argv[0]) {
				case PLAN:
				case RATE:
				case SERVICE:
				case NETWORK:
				case RULES: {
					return true;
				}
			}
		}
		return false;
	}

	static public void writeVOToXMLFile(Class classObj, Object voObj, String fileName) {
		OutputStream outFile;
		try {
		    JAXBContext rootContext = JAXBContext.newInstance(classObj);
		    Marshaller m = rootContext.createMarshaller();
			outFile = new FileOutputStream(fileName);
		    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		    m.marshal(voObj, outFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static public ExcelCellVO getExcelCellWithValue(ExcelCellVO cell, String value)
	{
		ExcelCellVO newCell = null;
		if(null == cell) {
			newCell = new ExcelCellVO();
		} else {
			newCell = cell;
		}
		
		newCell.setCellValue(value);
		return newCell;
	}
	
}

