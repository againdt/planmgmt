package com.getinsured.serffutils;

import org.apache.commons.lang3.StringUtils;

/**
 * Class is used to validate empty data.
 * 
 * @author Bhavin Parmar
 * @since Aug 12, 2016
 */
public class ValidateNotNull extends Validators {

	public ValidateNotNull() {}

	@Override
	protected boolean validate(String value) {

		if (StringUtils.isNotBlank(value)) {
			return true;
		}
		return false;
	}

	@Override
	protected String getErrorMessage(int lineNumber, String columnName, String value) {
		return "\n- " + columnName + " Value at line "+ lineNumber +" should not be empty.";
	}
}
