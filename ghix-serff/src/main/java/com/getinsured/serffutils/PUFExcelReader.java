package com.getinsured.serffutils;


import org.apache.commons.lang.StringUtils;

public class PUFExcelReader extends ExcelReader {

	PUFExcelReader(String dateFormatStr) {
		if(StringUtils.isNotBlank(dateFormatStr)) {
			this.dateFormatStr = dateFormatStr;
		} else {
			this.dateFormatStr = "dd/MM/yyyy";
		}
	}
}
