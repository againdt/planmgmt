package com.getinsured.serffutils;

import java.io.FileReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.serff.template.plan.AvCalculatorVO;
import com.serff.template.plan.BenefitAttributeVO;
import com.serff.template.plan.CostShareVarianceVO;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.HsaVO;
import com.serff.template.plan.MoopVO;
import com.serff.template.plan.PlanAndBenefitsPackageVO;
import com.serff.template.plan.PlanAndBenefitsVO;
import com.serff.template.plan.PlanBenefitTemplateVO;
import com.serff.template.plan.PlanDeductibleVO;
import com.serff.template.plan.PlansListVO;
import com.serff.template.plan.UrlVO;

public class SERFFTemplateUpgrade {
private boolean convertV1toV5 = false, convertV5toV6 = false;  
public void convertTemplate(String argv[]) {
	if(argv.length == 3 && isValidParams(argv)) {
		String inputPath = argv[0]; //"E:\\GetInsured\\Logs\\01017_KS_Plans.xml";
		int ind = inputPath.lastIndexOf("\\");
		if(ind == -1) {
			ind = inputPath.lastIndexOf("/");
		}
		String outPath = inputPath.substring(0, ind+1) + "New_" + inputPath.substring(ind+1);
		if(convertTemplate(inputPath, outPath))
		{
			System.out.println("Conversion completed for " + inputPath + ". New file saved as " + outPath);
		} 
	} else {
		SERFFTemplateUpgrade.class.getSimpleName();
		System.out.println("Invalid params:");
		System.out.println("Usage: " + SERFFTemplateUpgrade.class.getSimpleName() + " <File_Name_with_path> <from_version> <to_version>");
		System.out.println("from_version: 1 or 5");
		System.out.println("to_version: 5 or 6");
	}

}

private boolean isValidParams(String argv[]) {
	if(argv[1].equals("1") || argv[1].equals("5")) {
		if(argv[2].equals("5") || argv[2].equals("6")) {
			if(argv[1].equals("1")) {
				convertV1toV5 = true;
			} else {
				convertV1toV5 = false;
			}
			if(argv[2].equals("6")) {
				convertV5toV6 = true;
			} else {
				convertV5toV6 = false;
			}
			
			if(convertV1toV5 || convertV5toV6) {
				return true;
			} else {
				System.out.println("Conversion not supported from " + argv[1] + " to " + argv[2]);
			}
		} else {
			System.out.println("Invalid to_version. Valid value 5 or 6. Actual value " + argv[2]);
		}
		
	} else {
		System.out.println("Invalid from_version. Valid value 1 or 5. Actual value " + argv[1]);
	}
	return false;
}



public boolean convertTemplate(String file, String outPath) {
	boolean hasErrors = false;
   try {
	    JAXBContext rootContext = JAXBContext.newInstance(PlanBenefitTemplateVO.class);
	    Unmarshaller um = rootContext.createUnmarshaller();
	    PlanBenefitTemplateVO planBenefitsTemplate = (PlanBenefitTemplateVO) um.unmarshal(new FileReader(file));
	    if(convertV1toV5) {
	    	hasErrors = updateTemplateforV5(planBenefitsTemplate);
	    } 
	    if(!hasErrors && convertV5toV6) {
		    hasErrors = updateTemplateforV6(planBenefitsTemplate);
	    }
	    //planBenefitsTemplate.
	    if(!hasErrors) {
	    	SERFFTemplateUtil.writeVOToXMLFile(PlanBenefitTemplateVO.class, planBenefitsTemplate, outPath);
	    }
	    return !hasErrors;
	  } catch (Exception e) {
	     e.printStackTrace();
		   return false;
	  }
   }

	private boolean updateTemplateforV5(PlanBenefitTemplateVO planBenefitsTemplate) {
	    boolean hasErrors = false;
		if(null != planBenefitsTemplate.getPackagesList() && CollectionUtils.isNotEmpty(planBenefitsTemplate.getPackagesList().getPackages()))
	    {
	    	for (PlanAndBenefitsPackageVO planPackage : planBenefitsTemplate.getPackagesList().getPackages()) {
	    		String sourceVer = planPackage.getHeader().getTemplateVersion();
	    		if(!StringUtils.isBlank(sourceVer)) {
	    			System.out.println("Invalid version of source file: " + sourceVer + " Expected version tag to be absent.");
	    	    	hasErrors = true;
	    	    	break;
	    		}	    		
	    		//Update Header
	    		planPackage.getHeader().setTemplateVersion("v5.12");
	    		if(null != planPackage.getBenefitsList() && CollectionUtils.isNotEmpty(planPackage.getBenefitsList().getBenefits())) {
	    	    	for (BenefitAttributeVO benefit : planPackage.getBenefitsList().getBenefits()) {
	    	    		benefit.setSubjectToDeductibleTier1(null);
	    	    		benefit.setSubjectToDeductibleTier2(null);
	    	    	}
	    		}
	    		hasErrors = !convertPlanListForV5(planPackage.getPlansList());
			}
	    } else {
	    	System.out.println("Either there is no <packagesList> or <packages> are missing");
	    	hasErrors = true;
	    }
		return hasErrors;
	}
 
	private boolean updateTemplateforV6(PlanBenefitTemplateVO planBenefitsTemplate) {
	    boolean hasErrors = false;
		if(null != planBenefitsTemplate.getPackagesList() && CollectionUtils.isNotEmpty(planBenefitsTemplate.getPackagesList().getPackages()))
	    {
	    	for (PlanAndBenefitsPackageVO planPackage : planBenefitsTemplate.getPackagesList().getPackages()) {
	    		
	    		//Update Header
	    		String sourceVer = planPackage.getHeader().getTemplateVersion();
	    		if(StringUtils.isBlank(sourceVer) ||  !sourceVer.startsWith("v5")) {
	    			System.out.println("Invalid version of source file: " + sourceVer + " Expected version v5.x");
	    	    	hasErrors = true;
	    	    	break;
	    		}
	    		planPackage.getHeader().setTemplateVersion("v6.1");
	    		if(null != planPackage.getBenefitsList() && CollectionUtils.isNotEmpty(planPackage.getBenefitsList().getBenefits())) {
	    	    	for (BenefitAttributeVO benefit : planPackage.getBenefitsList().getBenefits()) {
	    	    		benefit.setIsStateMandate(null);
	    	    		benefit.setMinimumStay(null);
	    	    	}
	    		}
	    		hasErrors = !convertPlanListForV6(planPackage.getPlansList());
			}
	    } else {
	    	System.out.println("Either there is no <packagesList> or <packages> are missing");
	    	hasErrors = true;
	    }
		return hasErrors;
	}
	private boolean convertPlanListForV6(PlansListVO planPackage) {
		if(CollectionUtils.isNotEmpty(planPackage.getPlans())) {
			ExcelCellVO maximumCoinsuranceForSpecialtyDrugs = null, maxNumDaysForChargingInpatientCopay = null;
			ExcelCellVO beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays = null, beginPrimaryCareCostSharingAfterSetNumberVisits = null; 
			ExcelCellVO issuerActuarialValue = null, avCalculatorOutputNumber = null;//enrollmentPaymentURL = null, 
	    	for (PlanAndBenefitsVO plan : planPackage.getPlans()) {
	    		if(null != plan.getPlanAttributes() && null != plan.getCostShareVariancesList() &&
	    				CollectionUtils.isNotEmpty(plan.getCostShareVariancesList().getCostShareVariance())) {
	    			maximumCoinsuranceForSpecialtyDrugs = plan.getPlanAttributes().getMaximumCoinsuranceForSpecialtyDrugs();
	    			maxNumDaysForChargingInpatientCopay = plan.getPlanAttributes().getMaxNumDaysForChargingInpatientCopay();
	    			beginPrimaryCareCostSharingAfterSetNumberVisits = plan.getPlanAttributes().getBeginPrimaryCareCostSharingAfterSetNumberVisits();
	    			beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays = plan.getPlanAttributes().getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays();

	    			plan.getPlanAttributes().setMaximumCoinsuranceForSpecialtyDrugs(null);
		    		plan.getPlanAttributes().setMaxNumDaysForChargingInpatientCopay(null);
		    		plan.getPlanAttributes().setBeginPrimaryCareCostSharingAfterSetNumberVisits(null);
		    		plan.getPlanAttributes().setBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays(null);
		    		plan.getPlanAttributes().getEhbApportionmentForPediatricDental().setCellValue("1.00");
		    		plan.getPlanAttributes().setPlanDesignType(getExcelCellWithValue(plan.getPlanAttributes().getPlanDesignType(), "Not Applicable"));
			    	for (CostShareVarianceVO csv : plan.getCostShareVariancesList().getCostShareVariance()) {
			    		csv.setPlanVariantMarketingName(csv.getPlanMarketingName());
			    		csv.setPlanMarketingName(null);
			    		issuerActuarialValue = csv.getIssuerActuarialValue();
			    		avCalculatorOutputNumber = csv.getAvCalculatorOutputNumber();
			    		csv.setIssuerActuarialValue(null);
			    		csv.setAvCalculatorOutputNumber(null);
	    		
			    		//ADD new SBC, is it ok to add $0
			    		if(null != csv.getSbc()) { //it is null for Dental plans
				    		csv.getSbc().setHavingSimplefractureCoInsurance(getExcelCellWithValue(csv.getSbc().getHavingSimplefractureCoInsurance(), "$0"));
				    		csv.getSbc().setHavingSimplefractureCopayment(getExcelCellWithValue(csv.getSbc().getHavingSimplefractureCopayment(), "$0"));
				    		csv.getSbc().setHavingSimplefractureDeductible(getExcelCellWithValue(csv.getSbc().getHavingSimplefractureDeductible(), "$0"));
				    		csv.getSbc().setHavingSimplefractureLimit(getExcelCellWithValue(csv.getSbc().getHavingSimplefractureLimit(), "$0"));
				    		if(StringUtils.isBlank(csv.getSbc().getHavingBabyCoInsurance().getCellValue())) {
					    		csv.getSbc().getHavingBabyCoInsurance().setCellValue("$0");
				    		}
				    		if(StringUtils.isBlank(csv.getSbc().getHavingBabyCoPayment().getCellValue())) {
					    		csv.getSbc().getHavingBabyCoPayment().setCellValue("$0");
				    		}
				    		if(StringUtils.isBlank(csv.getSbc().getHavingBabyDeductible().getCellValue())) {
					    		csv.getSbc().getHavingBabyDeductible().setCellValue("$0");
				    		}
				    		if(StringUtils.isBlank(csv.getSbc().getHavingBabyLimit().getCellValue())) {
					    		csv.getSbc().getHavingBabyLimit().setCellValue("$0");
				    		}
				    		if(StringUtils.isBlank(csv.getSbc().getHavingDiabetesCoInsurance().getCellValue())) {
					    		csv.getSbc().getHavingDiabetesCoInsurance().setCellValue("$0");
				    		}
				    		if(StringUtils.isBlank(csv.getSbc().getHavingDiabetesCopay().getCellValue())) {
					    		csv.getSbc().getHavingDiabetesCopay().setCellValue("$0");
				    		}
				    		if(StringUtils.isBlank(csv.getSbc().getHavingDiabetesDeductible().getCellValue())) {
					    		csv.getSbc().getHavingDiabetesDeductible().setCellValue("$0");
				    		}
				    		if(StringUtils.isBlank(csv.getSbc().getHavingDiabetesLimit().getCellValue())) {
					    		csv.getSbc().getHavingDiabetesLimit().setCellValue("$0");
				    		}
			    		}
			    		
			    		//ADD new <avCalculator>
			    		if(null  == csv.getAvCalculator()) {
			    			csv.setAvCalculator(new AvCalculatorVO());
			    		}
			    		csv.getAvCalculator().setBeginPrimaryCareCostSharingAfterSetNumberVisits(beginPrimaryCareCostSharingAfterSetNumberVisits);
			    		csv.getAvCalculator().setBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays(beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays);
			    		csv.getAvCalculator().setMaximumCoinsuranceForSpecialtyDrugs(maximumCoinsuranceForSpecialtyDrugs);
			    		csv.getAvCalculator().setMaxNumDaysForChargingInpatientCopay(maxNumDaysForChargingInpatientCopay);
			    		csv.getAvCalculator().setAvCalculatorOutputNumber(avCalculatorOutputNumber);
			    		csv.getAvCalculator().setIssuerActuarialValue(issuerActuarialValue);
			    	}
	    		}
	    	}
		} else {
	    	System.out.println("<plans> are missing from <plansList>");
			return false;		
		}
		return true;		
	}
	
	private boolean convertPlanListForV5(PlansListVO planPackage) {
		if(CollectionUtils.isNotEmpty(planPackage.getPlans())) {
			ExcelCellVO hsaEligibility = null, employerHSAHRAContributionIndicator = null, empContributionAmountForHSAOrHRA = null;
			ExcelCellVO summaryBenefitAndCoverageURL = null, planBrochure = null; //enrollmentPaymentURL = null, 
	    	for (PlanAndBenefitsVO plan : planPackage.getPlans()) {
	    		if(null != plan.getPlanAttributes() && null != plan.getCostShareVariancesList() &&
	    				CollectionUtils.isNotEmpty(plan.getCostShareVariancesList().getCostShareVariance())) {
		    		hsaEligibility = plan.getPlanAttributes().getHsaEligibility();
		    		employerHSAHRAContributionIndicator = plan.getPlanAttributes().getEmployerHSAHRAContributionIndicator();
		    		empContributionAmountForHSAOrHRA = plan.getPlanAttributes().getEmpContributionAmountForHSAOrHRA();
		    		summaryBenefitAndCoverageURL = plan.getPlanAttributes().getSummaryBenefitAndCoverageURL();
		    		planBrochure = plan.getPlanAttributes().getPlanBrochure();
		    		plan.getPlanAttributes().setHsaEligibility(null);
		    		plan.getPlanAttributes().setEmployerHSAHRAContributionIndicator(null);
		    		plan.getPlanAttributes().setEmpContributionAmountForHSAOrHRA(null);
		    		plan.getPlanAttributes().setInsurancePlanCompositePremiumAvailableIndicator(getExcelCellWithValue(plan.getPlanAttributes().getInsurancePlanCompositePremiumAvailableIndicator(), "No"));
		    		plan.getPlanAttributes().setEhbPercentPremium(getExcelCellWithValue(plan.getPlanAttributes().getEhbPercentPremium(), ""));
		    		plan.getPlanAttributes().setSummaryBenefitAndCoverageURL(null);
		    		plan.getPlanAttributes().setPlanBrochure(null);
			    	for (CostShareVarianceVO csv : plan.getCostShareVariancesList().getCostShareVariance()) {
			    		if(null == csv.getHsa()) {
			    			csv.setHsa(new HsaVO());
			    		}
			    		if(null == csv.getUrl()) {
			    			csv.setUrl(new UrlVO());
			    		}
			    		csv.getHsa().setHsaEligibility(hsaEligibility);
			    		csv.getHsa().setEmployerHSAHRAContributionIndicator(employerHSAHRAContributionIndicator);
			    		csv.getHsa().setEmpContributionAmountForHSAOrHRA(empContributionAmountForHSAOrHRA);
			    		csv.getUrl().setSummaryBenefitAndCoverageURL(summaryBenefitAndCoverageURL);
			    		csv.getUrl().setPlanBrochure(planBrochure);
			    		if(null != csv.getMoopList() && CollectionUtils.isNotEmpty(csv.getMoopList().getMoop())) {
				    		for(MoopVO moop : csv.getMoopList().getMoop()) {
				    			updateFamilyCostValue(moop.getCombinedInOutNetworkFamilyAmount());
				    			updateFamilyCostValue(moop.getInNetworkTier1FamilyAmount());
				    			updateFamilyCostValue(moop.getInNetworkTier2FamilyAmount());
				    			updateFamilyCostValue(moop.getOutOfNetworkFamilyAmount());
				    		}
			    		}
			    		if(null != csv.getPlanDeductibleList() && CollectionUtils.isNotEmpty(csv.getPlanDeductibleList().getPlanDeductible())) {
				    		for(PlanDeductibleVO deductible : csv.getPlanDeductibleList().getPlanDeductible()) {
				    			updateFamilyCostValue(deductible.getCombinedInOrOutNetworkFamily());
				    			updateFamilyCostValue(deductible.getInNetworkTier1Family());
				    			updateFamilyCostValue(deductible.getInNetworkTierTwoFamily());
				    			updateFamilyCostValue(deductible.getOutOfNetworkFamily());
				    		}
			    		}
			    	}
	    		}
	    		
	    	}
		} else {
	    	System.out.println("<plans> are missing from <plansList>");
			return false;		
		}
		return true;		
	}
	
	private ExcelCellVO getExcelCellWithValue(ExcelCellVO cell, String value)
	{
		return SERFFTemplateUtil.getExcelCellWithValue(cell, value);
	}
	
	private void updateFamilyCostValue(ExcelCellVO cell) {
		if(null != cell && StringUtils.isNotBlank(cell.getCellValue())) {
			String currentValue = cell.getCellValue();
			if("not applicable".equalsIgnoreCase(currentValue)) {
				cell.setCellValue("per person not applicable | per group not applicable");
			} else {
				cell.setCellValue(currentValue + " per person | " + currentValue + " per group");
			}
		}
	}
}

