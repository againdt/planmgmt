/**
 * 
 */
package com.getinsured.hix.dozer.converters;

import javax.xml.datatype.XMLGregorianCalendar;

import org.dozer.DozerConverter;

/**
 * @author talreja_n
 * 
 */
public class XMLGregorianCalendarConverter extends
		DozerConverter<XMLGregorianCalendar, XMLGregorianCalendar> {

	public XMLGregorianCalendarConverter() {
		super(XMLGregorianCalendar.class, XMLGregorianCalendar.class);
	}

	@Override
	public XMLGregorianCalendar convertFrom(XMLGregorianCalendar src,
			XMLGregorianCalendar dest) {
		return src;
	}

	@Override
	public XMLGregorianCalendar convertTo(XMLGregorianCalendar src,
			XMLGregorianCalendar dest) {
		return dest;
	}

}
