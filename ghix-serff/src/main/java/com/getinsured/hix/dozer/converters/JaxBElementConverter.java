package com.getinsured.hix.dozer.converters;

import javax.xml.bind.JAXBElement;

import org.dozer.DozerConverter;

@SuppressWarnings("rawtypes")
public class JaxBElementConverter extends DozerConverter<JAXBElement, JAXBElement> {

	

	public JaxBElementConverter() {
		super(JAXBElement.class, JAXBElement.class);
	}

	@Override
	public JAXBElement convertFrom(JAXBElement src, JAXBElement dest) {
		if(src != null){
			return src;
		}
		return dest;
	}

	@Override
	public JAXBElement convertTo(JAXBElement src, JAXBElement dest) {
		
		if(dest != null){
			return dest;
		}
		return src;
	}
}
