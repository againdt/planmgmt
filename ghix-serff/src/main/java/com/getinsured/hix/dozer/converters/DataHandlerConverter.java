package com.getinsured.hix.dozer.converters;

import javax.activation.DataHandler;

import org.dozer.DozerConverter;

public class DataHandlerConverter extends DozerConverter<DataHandler, DataHandler> {
	
	public DataHandlerConverter() {
		super(DataHandler.class, DataHandler.class);
	}
	
	@Override
	public DataHandler convertFrom(DataHandler src,
			DataHandler dest) {
		return src;
	}

	@Override
	public DataHandler convertTo(DataHandler src,
			DataHandler dest) {
		return dest;
	}
	

}
