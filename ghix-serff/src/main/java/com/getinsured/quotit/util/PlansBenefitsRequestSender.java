package com.getinsured.quotit.util;

import static com.getinsured.quotit.util.QuotitConstants.ACA_FACTORY;
import static com.getinsured.quotit.util.QuotitConstants.GENERATED_FACTORY;
import static com.getinsured.quotit.util.QuotitConstants.INDEX_ZERO;
import generated.ArrayOfGetCarriersPlansBenefitsAddOn;
import generated.ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter;
import generated.GetCarriersPlansBenefitsAddOn;
import generated.GetCarriersPlansBenefitsLevelOfDetail;
import generated.GetCarriersPlansBenefitsRequest;
import generated.GetCarriersPlansBenefitsRequestAccessKey;
import generated.GetCarriersPlansBenefitsRequestCarrierPlanFilter;
import generated.GetCarriersPlansBenefitsRequestInput;
import generated.InsuranceType;

import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.quotit.services.actws.aca._2.GetCarriersPlansBenefits;
import com.serff.util.SerffConstants;

@Component
public class PlansBenefitsRequestSender {

	private static final Logger LOGGER = Logger.getLogger(PlansBenefitsRequestSender.class);

	/**
	 * Method is used generate GetCarriersPlansBenefitsRequest and return the same.
	 */
	public GetCarriersPlansBenefits getCarriersPlansRequest(PlansBenefitsRequestVO requestVO) {

		LOGGER.info("getCarriersPlansRequest() Start");
		GetCarriersPlansBenefits wsRequestList = null;

		try {
			if (null == requestVO || !validateInputParams(requestVO)) {
				LOGGER.error("Input parameters are empty.");
				return wsRequestList;
			}
			wsRequestList = ACA_FACTORY.createGetCarriersPlansBenefits();

			GetCarriersPlansBenefitsRequest wsRequest = GENERATED_FACTORY.createGetCarriersPlansBenefitsRequest();
			wsRequest.setAccessKeys(getCarriersPlansBenefitsRequestAccessKey(requestVO));
			wsRequest.setInputs(getCarriersPlansRequestInputs(requestVO));
			wsRequestList.setRequest(ACA_FACTORY.createGetCarriersPlansBenefitsRequest(wsRequest));
		}
		finally {
			LOGGER.info("getCarriersPlansRequest() End with GetCarriersPlansBenefits wsRequestList: " + (null == wsRequestList));
		}
		return wsRequestList;
	}

	/**
	 * Method is return GetCarriersPlansBenefitsRequestAccessKey.
	 */
	private GetCarriersPlansBenefitsRequestAccessKey getCarriersPlansBenefitsRequestAccessKey(PlansBenefitsRequestVO requestVO) {
		LOGGER.debug("getCarriersPlansBenefitsRequestAccessKey() Start");

		GetCarriersPlansBenefitsRequestAccessKey accessKey = GENERATED_FACTORY.createGetCarriersPlansBenefitsRequestAccessKey();
		accessKey.setRemoteAccessKey(requestVO.getQuotitRemoteAccessKey());
		LOGGER.debug("accessKey.getRemoteAccessKey(): " + accessKey.getRemoteAccessKey());
		accessKey.setWebsiteAccessKey(requestVO.getQuotitWebsiteAccessKey());
		LOGGER.debug("accessKey.getWebsiteAccessKey(): " + accessKey.getWebsiteAccessKey());
		accessKey.setBrokerId(requestVO.getBrokerID());
		LOGGER.debug("accessKey.getBrokerId(): " + accessKey.getBrokerId());
		LOGGER.debug("getCarriersPlansBenefitsRequestAccessKey() End");
		return accessKey;
	}

	/**
	 * Method is used generate GetCarriersPlansBenefitsRequestInput and return the same.
	 */
	private GetCarriersPlansBenefitsRequestInput getCarriersPlansRequestInputs(PlansBenefitsRequestVO requestVO) {

		LOGGER.debug("getCarriersPlansRequestInputs() Start");
		GetCarriersPlansBenefitsRequestInput inputs = GENERATED_FACTORY.createGetCarriersPlansBenefitsRequestInput();

		try {
			inputs.setEffectiveDate(requestVO.getEffectiveCalendar());
			inputs.getInsuranceCategory().add(requestVO.getInsuranceCategory());
			inputs.setLevelOfDetails(requestVO.getLevelOfDetails());
			inputs.getDataSet().add(getDataSet(requestVO.getDataSetList()));

			if (!CollectionUtils.isEmpty(requestVO.getAddOnList())) {
				inputs.setAddOns(getAddOnsList(requestVO.getAddOnList()));
			}
			inputs.setCarrierPlanFilters(getCarrierPlanFilters(requestVO));
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("getCarriersPlansRequestInputs() End");
		}
		return inputs;
	}

	/**
	 * Method is used to get DataSet List.
	 */
	private JAXBElement<List<String>> getDataSet(String dataSetList) {
		LOGGER.debug("getDataSet() Start");
		JAXBElement<List<String>> dataList = null;

		if (StringUtils.isBlank(dataSetList)) {
			LOGGER.error("Data set is empty.");
			return dataList;
		}
		List<String> valueList = Arrays.asList(dataSetList.split(SerffConstants.COMMA));
		dataList = GENERATED_FACTORY.createGetCarriersPlansBenefitsRequestInputDataSet(valueList);
		LOGGER.debug("getDataSet() End");
		return dataList;
	}

	/**
	 * Method is used to get AddOns List.
	 */
	private JAXBElement<ArrayOfGetCarriersPlansBenefitsAddOn> getAddOnsList(List<GetCarriersPlansBenefitsAddOn> addOnList) {

		LOGGER.debug("getAddOnsList() Start");
		JAXBElement<ArrayOfGetCarriersPlansBenefitsAddOn> addOnsList = null;

		try {
			ArrayOfGetCarriersPlansBenefitsAddOn arrayOfAddOn = GENERATED_FACTORY.createArrayOfGetCarriersPlansBenefitsAddOn();
			List<GetCarriersPlansBenefitsAddOn> carriersPlansBenefitsAddOnList = arrayOfAddOn.getGetCarriersPlansBenefitsAddOn();

			for (GetCarriersPlansBenefitsAddOn addOn : addOnList) {
				if (null != addOn) {
					carriersPlansBenefitsAddOnList.add(addOn);
				}
			}
			addOnsList = GENERATED_FACTORY.createGetCarriersPlansBenefitsRequestInputAddOns(arrayOfAddOn);
		}
		finally {
			LOGGER.debug("getAddOnsList() End");
		}
		return addOnsList;
	}

	/**
	 * Method is used to get Carrier Plans Filters.
	 */
	private JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter> getCarrierPlanFilters(PlansBenefitsRequestVO requestVO) {

		LOGGER.debug("getCarrierPlanFilters() Start with Level Of Details: " + requestVO.getLevelOfDetails().name());
		JAXBElement<ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter> addFilterList = null;

		try {
			ArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter addFilter = GENERATED_FACTORY.createArrayOfGetCarriersPlansBenefitsRequestCarrierPlanFilter();
			List<GetCarriersPlansBenefitsRequestCarrierPlanFilter> planFilterList = null;
			GetCarriersPlansBenefitsRequestCarrierPlanFilter planFilter = null;

			// Get Carrier Level Filter
			if (GetCarriersPlansBenefitsLevelOfDetail.CARRIERS.equals(requestVO.getLevelOfDetails())) {
				planFilterList = addFilter.getGetCarriersPlansBenefitsRequestCarrierPlanFilter();
				planFilter = getCarrierPlanFiltersList(requestVO.getInsuranceType(), null, null);
				planFilterList.add(planFilter);
			}
			else if (GetCarriersPlansBenefitsLevelOfDetail.PLANS.equals(requestVO.getLevelOfDetails())
					&& !CollectionUtils.isEmpty(requestVO.getCarrierIdList())) {

				requestVO.setCurrentIndex(INDEX_ZERO);
				LOGGER.info("Set Current Index of Filter: " + requestVO.getCurrentIndex());
				planFilterList = addFilter.getGetCarriersPlansBenefitsRequestCarrierPlanFilter();

				for (Integer carrierId : requestVO.getCarrierIdList()) {
					planFilter = getCarrierPlanFiltersList(requestVO.getInsuranceType(), carrierId, null);
					planFilterList.add(planFilter);
				}
			}
			else if (GetCarriersPlansBenefitsLevelOfDetail.BENEFITS.equals(requestVO.getLevelOfDetails())
					&& !CollectionUtils.isEmpty(requestVO.getCarriersPlansMap())) {
				Integer carrierId = null;
				int counter = INDEX_ZERO;
				LOGGER.info("Current Index of Filter: " + requestVO.getCurrentIndex());

				for (int iterator = requestVO.getCurrentIndex(); iterator < requestVO.getCarrierIdList().size(); iterator++) {
					carrierId = requestVO.getCarrierIdList().get(iterator);
					planFilterList = addFilter.getGetCarriersPlansBenefitsRequestCarrierPlanFilter();

					for (String planId : requestVO.getCarriersPlansMap().get(carrierId)) {

						if (StringUtils.isNotBlank(planId)) {
							planFilter = getCarrierPlanFiltersList(requestVO.getInsuranceType(), carrierId, planId);
							planFilterList.add(planFilter);
							counter++;
						}
					}
					requestVO.setCurrentIndex(iterator + 1);

					if (counter > QuotitConstants.getPlanLoadSize()) {
						LOGGER.info("Breaking to get bunch of plans with Current Index of Filter: " + requestVO.getCurrentIndex());
						break;
					}
				}
				requestVO.setPlansCounter(counter);
			}
			addFilterList = GENERATED_FACTORY.createGetCarriersPlansBenefitsRequestInputCarrierPlanFilters(addFilter);
		}
		finally {
			LOGGER.debug("getCarrierPlanFilters() End");
		}
		return addFilterList;
    }

	/**
	 * Method is used to get Carrier Plans Filters List.
	 */
	private GetCarriersPlansBenefitsRequestCarrierPlanFilter getCarrierPlanFiltersList(InsuranceType insuranceTypeEnum,
			Integer carrierId, String planId) {

		LOGGER.debug("getCarrierPlanFiltersList() Start");
		GetCarriersPlansBenefitsRequestCarrierPlanFilter planFilter = GENERATED_FACTORY.createGetCarriersPlansBenefitsRequestCarrierPlanFilter();

		try {
			planFilter.setInsuranceType(GENERATED_FACTORY.createGetCarriersPlansBenefitsRequestCarrierPlanFilterInsuranceType(insuranceTypeEnum));
			LOGGER.debug("planFilter.getInsuranceType(): " + planFilter.getInsuranceType().getValue().value());

			if (null != carrierId) {
				planFilter.setCarrierId(GENERATED_FACTORY.createGetCarriersPlansBenefitsRequestCarrierPlanFilterCarrierId(carrierId));
			}

			if (StringUtils.isNotBlank(planId)) {
				planFilter.setPlanId(GENERATED_FACTORY.createGetCarriersPlansBenefitsRequestCarrierPlanFilterPlanId(planId));
			}
			/*planFilter.setState(GENERATED_FACTORY.createGetCarriersPlansBenefitsRequestCarrierPlanFilterState(state));
			LOGGER.info("planFilter.getState(): " + planFilter.getState().getValue());*/
		}
		finally {
			LOGGER.debug("getCarrierPlanFiltersList() End");
		}
		return planFilter;
	}

	/**
	 * Method is used to validate input parameters.
	 */
	private boolean validateInputParams(PlansBenefitsRequestVO requestVO) {

		LOGGER.debug("validateInputParams() Start");
		boolean isValid = true;

		try {
			if (null == requestVO.getInsuranceType()) {
				isValid = false;
				LOGGER.error("Insurance Type is empty.");
			}

			if (null == requestVO.getLevelOfDetails()) {
				isValid = false;
				LOGGER.error("Level Of Detail is empty.");
			}

			if (null != requestVO.getLevelOfDetails() &&
					GetCarriersPlansBenefitsLevelOfDetail.PLANS.equals(requestVO.getLevelOfDetails()) &&
					CollectionUtils.isEmpty(requestVO.getCarrierIdList())) {
				isValid = false;
				LOGGER.error("ArrayList<CarrierID> is empty.");
			}

			if (null != requestVO.getLevelOfDetails() &&
					GetCarriersPlansBenefitsLevelOfDetail.BENEFITS.equals(requestVO.getLevelOfDetails()) &&
					CollectionUtils.isEmpty(requestVO.getCarriersPlansMap())) {
				isValid = false;
				LOGGER.error("Map<CarrierID, PlanID> is empty.");
			}
		}
		finally {
			LOGGER.debug("validateInputParams() End");
		}
		return isValid;
	}

	/**
	 * Method is used to Garbage(Clear) Request object.
	 */
	public void garbageRequest(GetCarriersPlansBenefits wsRequestList) {

		LOGGER.debug("garbageRequest() Start");

		try {
			if (null == wsRequestList) {
				return;
			}
			GetCarriersPlansBenefitsRequest wsRequest = wsRequestList.getRequest().getValue();
			wsRequest.setAccessKeys(null);

			GetCarriersPlansBenefitsRequestInput inputs = wsRequest.getInputs();

			if (!CollectionUtils.isEmpty(inputs.getInsuranceCategory())) {
				inputs.getInsuranceCategory().clear();
			}

			if (!CollectionUtils.isEmpty(inputs.getDataSet())) {
				inputs.getDataSet().clear();
			}

			if (null != inputs.getAddOns()
					&& !CollectionUtils.isEmpty(inputs.getAddOns().getValue().getGetCarriersPlansBenefitsAddOn())) {
				inputs.getAddOns().getValue().getGetCarriersPlansBenefitsAddOn().clear();
				inputs.setAddOns(null);
			}

			if (null != inputs.getCarrierPlanFilters()
					&& !CollectionUtils.isEmpty(inputs.getCarrierPlanFilters().getValue().getGetCarriersPlansBenefitsRequestCarrierPlanFilter())) {
				inputs.getCarrierPlanFilters().getValue().getGetCarriersPlansBenefitsRequestCarrierPlanFilter().clear();
			}
			inputs.setCarrierPlanFilters(null);
			wsRequest.setInputs(null);
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (null != wsRequestList) {
				wsRequestList.setRequest(null);
			}
			LOGGER.debug("garbageRequest() End");
		}
	}
}
