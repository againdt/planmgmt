package com.getinsured.quotit.util;

import generated.GetCarriersPlansBenefitsAddOn;
import generated.GetCarriersPlansBenefitsLevelOfDetail;
import generated.InsuranceType;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;

public class PlansBenefitsRequestVO {

	private String quotitRemoteAccessKey;
	private String quotitWebsiteAccessKey;
	private Integer brokerID;
	private XMLGregorianCalendar effectiveCalendar; // Date must be in YYYY-MM-DD format.
	private String insuranceCategory;
	private GetCarriersPlansBenefitsLevelOfDetail levelOfDetails;
	private String dataSetList; // Data Set list must be comma separated form.
	private List<GetCarriersPlansBenefitsAddOn> addOnList;
	private InsuranceType insuranceType;
	private List<Integer> carrierIdList;
	private Map<Integer, List<String>> carriersPlansMap;

	private int currentIndex;
	private int plansCounter;
	private StringBuilder errorMessages = new StringBuilder();

	public PlansBenefitsRequestVO() {
	}

	public String getQuotitRemoteAccessKey() {
		return quotitRemoteAccessKey;
	}

	public void setQuotitRemoteAccessKey(String quotitRemoteAccessKey) {
		this.quotitRemoteAccessKey = quotitRemoteAccessKey;
	}

	public String getQuotitWebsiteAccessKey() {
		return quotitWebsiteAccessKey;
	}

	public void setQuotitWebsiteAccessKey(String quotitWebsiteAccessKey) {
		this.quotitWebsiteAccessKey = quotitWebsiteAccessKey;
	}

	public Integer getBrokerID() {
		return brokerID;
	}

	public void setBrokerID(Integer brokerID) {
		this.brokerID = brokerID;
	}

	public XMLGregorianCalendar getEffectiveCalendar() {
		return effectiveCalendar;
	}

	public void setEffectiveCalendar(XMLGregorianCalendar effectiveCalendar) {
		this.effectiveCalendar = effectiveCalendar;
	}

	public Date getStartDate() {
		return null != effectiveCalendar ? effectiveCalendar.toGregorianCalendar().getTime() : null;
	}

	public Date getEndDate() {

		if (null != effectiveCalendar) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.MONTH, Calendar.DECEMBER);
			cal.set(Calendar.DAY_OF_MONTH, 31);
			cal.set(Calendar.YEAR, getApplicableYear());
			return cal.getTime();
		}
		else {
			return null;
		}
	}

	public int getApplicableYear() {
		return null != effectiveCalendar ? effectiveCalendar.toGregorianCalendar().get(Calendar.YEAR) : 0;
	}

	public String getInsuranceCategory() {
		return insuranceCategory;
	}

	public void setInsuranceCategory(String insuranceCategory) {
		this.insuranceCategory = insuranceCategory;
	}

	public GetCarriersPlansBenefitsLevelOfDetail getLevelOfDetails() {
		return levelOfDetails;
	}

	public void setLevelOfDetails(
			GetCarriersPlansBenefitsLevelOfDetail levelOfDetails) {
		this.levelOfDetails = levelOfDetails;
	}

	public String getDataSetList() {
		return dataSetList;
	}

	public void setDataSetList(String dataSetList) {
		this.dataSetList = dataSetList;
	}

	public List<GetCarriersPlansBenefitsAddOn> getAddOnList() {
		return addOnList;
	}

	public void setAddOnList(List<GetCarriersPlansBenefitsAddOn> addOnList) {
		this.addOnList = addOnList;
	}

	public InsuranceType getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}

	public List<Integer> getCarrierIdList() {
		return carrierIdList;
	}

	public void setCarrierIdList(List<Integer> carrierIdList) {
		this.carrierIdList = carrierIdList;
	}

	public Map<Integer, List<String>> getCarriersPlansMap() {
		return carriersPlansMap;
	}

	public void setCarriersPlansMap(Map<Integer, List<String>> carriersPlansMap) {
		this.carriersPlansMap = carriersPlansMap;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public void setCurrentIndex(int currentIndex) {
		this.currentIndex = currentIndex;
	}

	public int getPlansCounter() {
		return plansCounter;
	}

	public void setPlansCounter(int plansCounter) {
		this.plansCounter = plansCounter;
	}

	public String getErrorMessages() {
		return errorMessages.toString();
	}

	public void setErrorMessages(String errorMessages) {

		if (StringUtils.isNotBlank(errorMessages)) {

			if (StringUtils.isNotBlank(this.errorMessages)) {
				this.errorMessages.append("\n");
			}
			this.errorMessages.append(errorMessages);
		}
	}

	public String getRequestParams() {
		StringBuilder params = new StringBuilder();
		params.append("InsuranceType=");
		params.append(insuranceType.value());
		params.append(", EffectiveDate=");
		params.append(getStartDate().toString());
		params.append(", LevelOfDetails=");
		params.append(levelOfDetails.value());

		/*if (!CollectionUtils.isEmpty(carriersPlansMap)) {
			params.append(", PlanIdList [");
			int counter = 0;

			for (Map.Entry<Integer, List<String>> entryData : carriersPlansMap.entrySet()) {

				if (counter < currentIndex) {
					continue;
				}

				for (String planId : entryData.getValue()) {
					params.append(planId);
					params.append(SerffConstants.COMMA);
				}
			}
			params.append(params.substring(0, params.length() -1));
			params.append("]");
		}*/
		return params.toString();
	}
}
