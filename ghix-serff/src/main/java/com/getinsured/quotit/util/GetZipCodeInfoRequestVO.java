package com.getinsured.quotit.util;

/**
 * ----------------------------------------------------------------------------
 * HIX-60437 : GetZipCodeInfo API Call and Response processing
 * ----------------------------------------------------------------------------
 * 
 * This class is a POJO to hold request attributes to call GetZipInfo 
 * Quotit webservice.
 * 
 * @author vardekar_s
 *
 */
public class GetZipCodeInfoRequestVO {

	private String stateCode;
	private String stateFips;
	private String stateName;

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getStateFips() {
		return stateFips;
	}

	public void setStateFips(String stateFips) {
		this.stateFips = stateFips;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

}
