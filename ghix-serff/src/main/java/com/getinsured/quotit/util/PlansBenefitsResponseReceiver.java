package com.getinsured.quotit.util;

import generated.ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan;
import generated.GetCarriersPlansBenefitsResponseCarrier;
import generated.GetCarriersPlansBenefitsResponseCarrierPlan;
import generated.GetCarriersPlansBenefitsResponseCarrierPlanBenefit;
import generated.GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.ExtWSCallLogs;
import com.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse;

@Component
public class PlansBenefitsResponseReceiver {

	private static final Logger LOGGER = Logger.getLogger(PlansBenefitsResponseReceiver.class);
	private static final String EMSG_CARRIER_LIST_EMPTY = "Carrier List is empty.";

	/**
	 * Method is used populate Carrier-ID and Plan-ID Map from Plans Benefits API Response.
	 */
	public Map<Integer, List<String>> getCarriersPlansMap(GetCarriersPlansBenefitsResponse wsResponse, ExtWSCallLogs trackingRecord) {

		LOGGER.info("getCarriersPlansMap() Start");
		Map<Integer, List<String>> carriersPlansMap = null;

		try {
			List<GetCarriersPlansBenefitsResponseCarrier> carriersList = getCarrierList(wsResponse, trackingRecord);
			if (CollectionUtils.isEmpty(carriersList)) {
				LOGGER.error(EMSG_CARRIER_LIST_EMPTY);
				return carriersPlansMap;
			}

			carriersPlansMap = new HashMap<Integer, List<String>>();
			for (GetCarriersPlansBenefitsResponseCarrier carrier : carriersList) {

				if (null == carrier || null == carrier.getPlans()) {
					continue;
				}
				// Adding Carrier-ID and Plan-ID in Map.
				addValueInCarriersPlansMap(carriersPlansMap, carrier);
			}
		}
		finally {
			LOGGER.info("getCarriersPlansMap() End");
		}
		return carriersPlansMap;
	}

	/**
	 * Method is used to add Carrier-ID and Plan-ID in Map.
	 */
	private void addValueInCarriersPlansMap(Map<Integer, List<String>> carriersPlansMap, GetCarriersPlansBenefitsResponseCarrier carrier) {

		LOGGER.debug("addValueInCarriersPlansMap() Start");

		try {
			List<GetCarriersPlansBenefitsResponseCarrierPlan> carriersPlansList = getCarriersPlansList(carrier.getPlans());
			if (CollectionUtils.isEmpty(carriersPlansList)) {
				return;
			}
			List<String> planList = null;

			// Check if Map has duplicate Carrier ID.
			if (CollectionUtils.isEmpty(carriersPlansMap.get(carrier.getCarrierId()))) {
				planList = new ArrayList<String>();
			}
			else {
				LOGGER.info("Carrier Id is duplicate in CarriersPlansMap.");
				planList = carriersPlansMap.get(carrier.getCarrierId());
			}

			for (GetCarriersPlansBenefitsResponseCarrierPlan plan : carriersPlansList) {

				if (null == plan) {
					continue;
				}
				planList.add(plan.getPlanId());
			}

			if (!CollectionUtils.isEmpty(planList)) {
				carriersPlansMap.put(carrier.getCarrierId(), planList);
			}
		}
		finally {
			LOGGER.debug("addValueInCarriersPlansMap() End");
		}
	}

	/**
	 * Method is used populate Carrier-ID List from Plans Benefits API Response.
	 */
	public List<Integer> getCarrierIdList(GetCarriersPlansBenefitsResponse wsResponse, ExtWSCallLogs trackingRecord) {

		LOGGER.debug("getCarrierIdList() Start");
		List<Integer> carrierIdList = null;

		try {
			List<GetCarriersPlansBenefitsResponseCarrier> carriersList = getCarrierList(wsResponse, trackingRecord);

			if (CollectionUtils.isEmpty(carriersList)) {
				return carrierIdList;
			}

			carrierIdList = new ArrayList<Integer>();
			for (GetCarriersPlansBenefitsResponseCarrier carrier : carriersList) {

				if (null == carrier) {
					continue;
				}
				carrierIdList.add(carrier.getCarrierId());
			}
		}
		finally {
			LOGGER.debug("getCarrierIdList() End");
		}
		return carrierIdList;
	}

	/**
	 * Method is used populate Carriers List from Plans Benefits API Response.
	 */
	private List<GetCarriersPlansBenefitsResponseCarrier> getCarrierList(GetCarriersPlansBenefitsResponse wsResponse, ExtWSCallLogs trackingRecord) {

		LOGGER.debug("getCarrierList() Start");
		List<GetCarriersPlansBenefitsResponseCarrier> carrierList = null;

		try {

			if (null == wsResponse) {
				LOGGER.error("GetCarriersPlansBenefitsResponse is empty.");
				return carrierList;
			}

			generated.GetCarriersPlansBenefitsResponse carriersResult = wsResponse.getGetCarriersPlansBenefitsResult().getValue();
			LOGGER.debug("<GetCarriersPlansBenefitsResult> is null: " + (null == carriersResult));

			if (null == carriersResult) {
				LOGGER.error("<GetCarriersPlansBenefitsResult> is empty.");
				return carrierList;
			}

			trackingRecord.setExternalReqId(carriersResult.getRequestId());
			trackingRecord.setExternalProdId(carriersResult.getPodId());
			trackingRecord.setExternalExecTime(carriersResult.getTotalMilliseconds());
			LOGGER.debug("<Carriers> is null: " + (null == carriersResult.getCarriers()));

			if (null != carriersResult.getCarriers()
					&& null != carriersResult.getCarriers().getValue()
					&& !CollectionUtils.isEmpty(carriersResult.getCarriers().getValue().getGetCarriersPlansBenefitsResponseCarrier())) {
				carrierList = carriersResult.getCarriers().getValue().getGetCarriersPlansBenefitsResponseCarrier();
			}
		}
		finally {
			LOGGER.debug("getCarrierList() End");
		}
		return carrierList;
	}

	/**
	 * Method is used populate Plans List from Plans Benefits API Response.
	 */
	private List<GetCarriersPlansBenefitsResponseCarrierPlan> getCarriersPlansList(JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan> planArrayList) {

		LOGGER.debug("getCarriersPlansList() Start");
		List<GetCarriersPlansBenefitsResponseCarrierPlan> carriersPlansList = null;

		try {
			if (null == planArrayList
					|| null == planArrayList.getValue()
					||CollectionUtils.isEmpty(planArrayList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlan())) {
				LOGGER.error("Plan List is empty.");
				return carriersPlansList;
			}

			carriersPlansList = planArrayList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlan();
		}
		finally {
			LOGGER.debug("getCarriersPlansList() End");
		}
		return carriersPlansList;
	}

	/**
	 * Method is used to Garbage(Clear) Response object.
	 */
	public QuotitResponseLogVO getQuotitResponseVO(GetCarriersPlansBenefitsResponse wsResponse) {

		LOGGER.debug("getQuotitResponseVO() Start");
		QuotitResponseLogVO quotitResponse = null;

		try {
			if (null == wsResponse) {
				return quotitResponse;
			}

			generated.GetCarriersPlansBenefitsResponse carriersResult = wsResponse.getGetCarriersPlansBenefitsResult().getValue();
			LOGGER.debug("<GetCarriersPlansBenefitsResult> is null: " + (null == carriersResult));

			if (null == carriersResult) {
				return quotitResponse;
			}

			if (null != carriersResult.getCarriers()
					&& !CollectionUtils.isEmpty(carriersResult.getCarriers().getValue().getGetCarriersPlansBenefitsResponseCarrier())) {
				List<GetCarriersPlansBenefitsResponseCarrier> carriersList = carriersResult.getCarriers().getValue().getGetCarriersPlansBenefitsResponseCarrier();
				quotitResponse = new QuotitResponseLogVO();
				quotitResponse.setRequestId(String.valueOf(carriersResult.getRequestId()));
				quotitResponse.setPodId(carriersResult.getPodId());
				quotitResponse.setTotalMilliseconds(String.valueOf(carriersResult.getTotalMilliseconds()));
				quotitResponse.setLoadSequenceId(String.valueOf(carriersResult.getLoadSequenceId()));

				for (GetCarriersPlansBenefitsResponseCarrier carrier : carriersList) {

					if (null == carrier) {
						continue;
					}
					QuotitCarrierResponseLogVO carrierResponse = new QuotitCarrierResponseLogVO();

					if (null != carrier.getPlans()) {
						JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan> planList = carrier.getPlans();

						if (null == planList
								|| null == planList.getValue()
								|| CollectionUtils.isEmpty(planList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlan())) {
							continue;
						}
						carrierResponse.setCarrierId(String.valueOf(carrier.getCarrierId()));
						carrierResponse.setInsuranceType(carrier.getInsuranceType().value());
						carrierResponse.setState(carrier.getState());

						for (GetCarriersPlansBenefitsResponseCarrierPlan plan : planList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlan()) {

							if (null == plan) {
								continue;
							}
							carrierResponse.setPlanId(plan.getPlanId());
						}
						quotitResponse.setCarrier(carrierResponse);
					}
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("getQuotitResponseVO() End");
		}
		return quotitResponse;
	}

	/**
	 * Method is used to Garbage(Clear) Response object.
	 */
	public void garbageResponse(GetCarriersPlansBenefitsResponse wsResponse) {

		LOGGER.debug("garbageResponse() Start");

		try {
			if (null == wsResponse) {
				return;
			}

			generated.GetCarriersPlansBenefitsResponse carriersResult = wsResponse.getGetCarriersPlansBenefitsResult().getValue();
			LOGGER.debug("<GetCarriersPlansBenefitsResult> is null: " + (null == carriersResult));

			if (null == carriersResult) {
				return;
			}

			if (null != carriersResult.getCarriers()
					&& !CollectionUtils.isEmpty(carriersResult.getCarriers().getValue().getGetCarriersPlansBenefitsResponseCarrier())) {
				List<GetCarriersPlansBenefitsResponseCarrier> carriersList = carriersResult.getCarriers().getValue().getGetCarriersPlansBenefitsResponseCarrier();

				for (GetCarriersPlansBenefitsResponseCarrier carrier : carriersList) {

					if (null == carrier) {
						continue;
					}

					if (null != carrier.getPlans()) {
						JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan> planList = carrier.getPlans();

						if (null == planList
								|| null == planList.getValue()
								|| CollectionUtils.isEmpty(planList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlan())) {
							continue;
						}

						for (GetCarriersPlansBenefitsResponseCarrierPlan plan : planList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlan()) {

							if (null == plan) {
								continue;
							}

							if (null != plan.getCoveredRxCUIs()
									&& null != plan.getCoveredRxCUIs().getValue()
									&& !CollectionUtils.isEmpty(plan.getCoveredRxCUIs().getValue().getRxCUI())) {
								plan.getCoveredRxCUIs().getValue().getRxCUI().clear();
								plan.setCoveredRxCUIs(null);
							}

							if (null != plan.getPlanData()
									&& null != plan.getPlanData().getValue()
									&& !CollectionUtils.isEmpty(plan.getPlanData().getValue().getGetCarriersPlansBenefitsResponseItem())) {
								plan.getPlanData().getValue().getGetCarriersPlansBenefitsResponseItem().clear();
								plan.setPlanData(null);	
							}

							if (null != plan.getBenefits()
									&& null != plan.getBenefits().getValue()
									&& !CollectionUtils.isEmpty(plan.getBenefits().getValue().getGetCarriersPlansBenefitsResponseCarrierPlanBenefit())) {

								for (GetCarriersPlansBenefitsResponseCarrierPlanBenefit planBenefit : plan.getBenefits().getValue().getGetCarriersPlansBenefitsResponseCarrierPlanBenefit()) {

									if (null == planBenefit) {
										continue;
									}
									planBenefit.getFlagValue().clear();

									if (null != planBenefit.getBenefitData()
											&& null != planBenefit.getBenefitData().getValue()
											&& !CollectionUtils.isEmpty(planBenefit.getBenefitData().getValue().getGetCarriersPlansBenefitsResponseItem())) {
										planBenefit.getBenefitData().getValue().getGetCarriersPlansBenefitsResponseItem().clear();
										planBenefit.setBenefitData(null);
									}

									if (null != planBenefit.getCoverages()
											&& null != planBenefit.getCoverages().getValue()
											&& !CollectionUtils.isEmpty(planBenefit.getCoverages().getValue().getGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage())) {

										for (GetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage coverage : planBenefit.getCoverages().getValue().getGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage()) {

											if (null == coverage) {
												continue;
											}

											if (null != coverage.getViewPoints()
													&& null != coverage.getViewPoints().getValue()
													&& !CollectionUtils.isEmpty(coverage.getViewPoints().getValue().getGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint())) {
												coverage.getViewPoints().getValue().getGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverageViewPoint().clear();
												coverage.setViewPoints(null);
											}
										}
										planBenefit.getCoverages().getValue().getGetCarriersPlansBenefitsResponseCarrierPlanBenefitCoverage().clear();
										planBenefit.setCoverages(null);
									}
								}
							}
							plan.setBenefits(null);
							plan = null;
						}
						carrier.setPlans(null);
					}
					carrier = null;
				}
				carriersList.clear();
			}
			carriersResult.setCarriers(null);
			carriersResult = null;
			wsResponse.setGetCarriersPlansBenefitsResult(null);
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("garbageResponse() End");
		}
	}
}
