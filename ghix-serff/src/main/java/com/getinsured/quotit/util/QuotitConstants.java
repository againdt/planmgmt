package com.getinsured.quotit.util;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * This class contains constants used for Quotit services.
 * 
 * @author Bhavin Parmar
 * @since January 29, 2015
 */
@Component
public class QuotitConstants {

	private static final Logger LOGGER = Logger.getLogger(QuotitConstants.class);
	// Object Factory
	public static final com.quotit.services.actws.aca._2.ObjectFactory ACA_FACTORY = new com.quotit.services.actws.aca._2.ObjectFactory();
	public static final generated.ObjectFactory GENERATED_FACTORY = new generated.ObjectFactory();

	// Common Data
	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd").withLocale(Locale.US);

	public static final int INDEX_ZERO = 0;
	public static final int INDEX_ONE = 1;
	public static final int INDEX_TWO = 2;
	public static final int SYSTEM_IP_LENGTH = 50;
	public static final String PREFIX_HIOS_ISSUER_ID = "QT_";
	public static final String XML_TYPE = ".xml";
	public static final String SERFF_ECM_QUOTIT_BASE_PATH = "serff/quotit/";

	private static final String DEFAULT_REMOTE_ACCESS_KEY = "CE7C9822-EA67-4C92-B6BB-4B6D01023652";
	private static final String DEFAULT_WEBSITE_ACCESS_KEY = "CE7C9822-EA67-4C92-B6BB-4B6D01023652";
	private static final String DEFAULT_BROKER_ID = "23460451";
	private static final String DEFAULT_WSDL_DEFAULT_URI = "https://ws.quotit.net/ActWS/ACA/v2/ACA.svc";
	private static final String DEFAULT_WSDL_GET_ZIP_CODE_INFO_URL = "http://www.quotit.com/Services/ActWS/ACA/2/IACA/GetZipCodeInfo";
	private static final String DEFAULT_WSDL_GET_CARRIERS_PLANS_BENEFITS_URL = "http://www.quotit.com/Services/ActWS/ACA/2/IACA/GetCarriersPlansBenefits";
	private static final int DEFAULT_PLAN_LOAD_SIZE = 10;

	public static final String METHOD_CARRIERS_PLANS_BENEFITS = "GetCarriersPlansBenefits";
	public static final String METHOD_GET_ZIP_CODE_INFO = "GetZipCodeInfo";

	public static final String PARAM_REQUESTED_BY = "PARAM_REQUESTED_BY";
	public static final String PARAM_INSURANCE_TYPE = "PARAM_INSURANCE_TYPE";
	public static final String PARAM_EFFECTIVE_DATE = "PARAM_EFFECTIVE_DATE";
	public static final String PARAM_LEVEL_OF_DETAIL = "PARAM_LEVEL_OF_DETAIL";
	public static final String PARAM_CARRIER_ID_LIST = "PARAM_CARRIER_ID_LIST";
	public static final String PARAM_MAP_CARRIER_ID_WITH_PLAN_ID = "PARAM_MAP_CARRIER_ID_WITH_PLAN_ID";
	public static final String QUOTIT = "QUOTIT";

	private static String remoteAccessKey;
	private static String websiteAccessKey;
	private static String brokerId;
	private static String wsdlDefaultUri;
	private static String wsdlGetZipCodeInfoUrl;
	private static String wsdlGetCarrierPlanBenefitsUrl;
	private static int planLoadSize;

	public QuotitConstants() {
	}

	public static String getRemoteAccessKey() {

		if (StringUtils.isBlank(remoteAccessKey)) {
			remoteAccessKey = DEFAULT_REMOTE_ACCESS_KEY;
		}
		LOGGER.debug("getRemoteAccessKey("+ remoteAccessKey +")");
		return remoteAccessKey.trim();
	}

	@Value("#{configProp['serff.medicareBatch.remoteAccessKey']}")
	public void setRemoteAccessKey(String remoteAccessKey) {
		setStaticRemoteAccessKey(remoteAccessKey);
	}

	private static void setStaticRemoteAccessKey(String remoteAccessKey) {
		QuotitConstants.remoteAccessKey = remoteAccessKey;
	}

	public static String getWebsiteAccessKey() {

		if (StringUtils.isBlank(websiteAccessKey)) {
			websiteAccessKey = DEFAULT_WEBSITE_ACCESS_KEY;
		}
		LOGGER.debug("getWebsiteAccessKey("+ websiteAccessKey +")");
		return websiteAccessKey.trim();
	}

	@Value("#{configProp['serff.medicareBatch.websiteAccessKey']}")
	public void setWebsiteAccessKey(String websiteAccessKey) {
		setStaticWebsiteAccessKey(websiteAccessKey);
	}

	private static void setStaticWebsiteAccessKey(String websiteAccessKey) {
		QuotitConstants.websiteAccessKey = websiteAccessKey;
	}

	public static String getBrokerId() {

		if (StringUtils.isBlank(brokerId)) {
			brokerId = DEFAULT_BROKER_ID;
		}
		LOGGER.debug("getBrokerId("+ brokerId +")");
		return brokerId.trim();
	}

	@Value("#{configProp['serff.medicareBatch.brokerId']}")
	public void setBrokerId(String brokerId) {
		setStaticBrokerId(brokerId);
	}

	private static void setStaticBrokerId(String brokerId) {
		QuotitConstants.brokerId = brokerId;
	}

	public static String getWsdlDefaultUri() {

		if (StringUtils.isBlank(wsdlDefaultUri)) {
			wsdlDefaultUri = DEFAULT_WSDL_DEFAULT_URI;
		}
		LOGGER.debug("getWsdlDefaultUri("+ wsdlDefaultUri +")");
		return wsdlDefaultUri.trim();
	}

	@Value("#{configProp['serff.medicareBatch.endPoint']}")
	public void setWsdlDefaultUri(String endPoint) {
		setStaticWsdlDefaultUri(endPoint);
	}

	private static void setStaticWsdlDefaultUri(String endPoint) {
		QuotitConstants.wsdlDefaultUri = endPoint;
	}

	public static String getWsdlGetZipCodeInfoUrl() {

		if (StringUtils.isBlank(wsdlGetZipCodeInfoUrl)) {
			wsdlGetZipCodeInfoUrl = DEFAULT_WSDL_GET_ZIP_CODE_INFO_URL;
		}
		LOGGER.debug("getWsdlGetZipCodeInfoUrl("+ wsdlGetZipCodeInfoUrl +")");
		return wsdlGetZipCodeInfoUrl.trim();
	}

	@Value("#{configProp['serff.medicareBatch.getZipCodeInfoEndPoint']}")
	public void setWsdlGetZipCodeInfoUrl(String wsdlGetZipCodeInfoUrl) {
		setStaticWsdlGetZipCodeInfoUrl(wsdlGetZipCodeInfoUrl);
	}

	private static void setStaticWsdlGetZipCodeInfoUrl(String wsdlGetZipCodeInfoUrl) {
		QuotitConstants.wsdlGetZipCodeInfoUrl = wsdlGetZipCodeInfoUrl;
	}

	public static String getWsdlGetCarrierPlanBenefitsUrl() {

		if (StringUtils.isBlank(wsdlGetCarrierPlanBenefitsUrl)) {
			wsdlGetCarrierPlanBenefitsUrl = DEFAULT_WSDL_GET_CARRIERS_PLANS_BENEFITS_URL;
		}
		LOGGER.debug("getWsdlGetCarrierPlanBenefitsUrl("+ wsdlGetCarrierPlanBenefitsUrl +")");
		return wsdlGetCarrierPlanBenefitsUrl.trim();
	}

	@Value("#{configProp['serff.medicareBatch.getCarrierPlanBenefitsEndPoint']}")
	public void setWsdlGetCarrierPlanBenefitsUrl(String wsdlGetCarrierPlanBenefitsUrl) {
		setStaticWsdlGetCarrierPlanBenefitsUrl(wsdlGetCarrierPlanBenefitsUrl);
	}

	private static void setStaticWsdlGetCarrierPlanBenefitsUrl(String wsdlGetCarrierPlanBenefitsUrl) {
		QuotitConstants.wsdlGetCarrierPlanBenefitsUrl = wsdlGetCarrierPlanBenefitsUrl;
	}

	public static int getPlanLoadSize() {

		if (0 == planLoadSize) {
			planLoadSize = DEFAULT_PLAN_LOAD_SIZE;
		}
		LOGGER.debug("getPlanLoadSize("+ planLoadSize +")");
		return planLoadSize;
	}

	@Value("#{configProp['serff.medicareBatch.planLoadSize']}")
	public void setPlanLoadSize(String planLoadSize) {
		
		if (StringUtils.isNotBlank(planLoadSize) && StringUtils.isNumeric(planLoadSize.trim())) {
			setStaticPlanLoadSize(Integer.valueOf(planLoadSize));
		}
	}

	private static void setStaticPlanLoadSize(int planLoadSize) {
		QuotitConstants.planLoadSize = planLoadSize;
	}

	public enum ResponseItemEnum {
		// Plan Data Enums
		PLAN_DATA_CONTRACT_ID("contractID"),
		PLAN_DATA_CMS_PLAN_ID("cmsPlanID"),
		PLAN_DATA_INCLUDE_RX("IncludeRx"),
		PLAN_DATA_CMS_PLAN_NAME("CMS plan name"),
		PLAN_DATA_SEGMENT_ID("SegmentID"),
		PLAN_DATA_IS_SNP("IsSNP"),
		PLAN_DATA_IS_MEDICAID("IsMedicaid"),
		PLAN_DATA_NETWORK_ONLY("SelectNetworkOnly"),
		PLAN_DATA_RATE_TYPE("RateType"),
		// Benefit Data Enums
		BENEFIT_DATA_NAME("Name"),
		BENEFIT_DATA_SHORT_NAME("ShortName"),
		BENEFIT_DATA_TINY_NAME("TinyName");

		private final String value;

		ResponseItemEnum(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}
}
