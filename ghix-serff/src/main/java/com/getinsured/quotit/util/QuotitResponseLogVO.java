package com.getinsured.quotit.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Class is used to generate Quotit Response.
 * 
 * @author Bhavin Parmar
 * @since Mar 1, 2016
 */
public class QuotitResponseLogVO {

	String requestId;
	String podId;
	String totalMilliseconds;
	String loadSequenceId;
	List <QuotitCarrierResponseLogVO> carrierList;

	public QuotitResponseLogVO() {
		carrierList = new ArrayList<QuotitCarrierResponseLogVO>();
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getPodId() {
		return podId;
	}

	public void setPodId(String podId) {
		this.podId = podId;
	}

	public String getTotalMilliseconds() {
		return totalMilliseconds;
	}

	public void setTotalMilliseconds(String totalMilliseconds) {
		this.totalMilliseconds = totalMilliseconds;
	}

	public String getLoadSequenceId() {
		return loadSequenceId;
	}

	public void setLoadSequenceId(String loadSequenceId) {
		this.loadSequenceId = loadSequenceId;
	}

	public List<QuotitCarrierResponseLogVO> getCarrierList() {
		return carrierList;
	}

	public void setCarrier(QuotitCarrierResponseLogVO carrierResponse) {
		carrierList.add(carrierResponse);
	}
}
