package com.getinsured.quotit.util;

import static com.getinsured.quotit.util.QuotitConstants.ACA_FACTORY;
import static com.getinsured.quotit.util.QuotitConstants.GENERATED_FACTORY;
import generated.ArrayOfGetZipCodeInfoResponseZipCodeInfo;
import generated.ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty;
import generated.GetZipCodeInfoRequest;
import generated.GetZipCodeInfoRequestAccessKey;
import generated.GetZipCodeInfoRequestInput;
import generated.GetZipCodeInfoResponseZipCodeInfo;
import generated.GetZipCodeInfoResponseZipCodeInfoCounty;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.quotit.services.actws.aca._2.GetZipCodeInfo;
import com.quotit.services.actws.aca._2.GetZipCodeInfoResponse;


/**
 * ----------------------------------------------------------------------------
 * HIX-60437 : GetZipCodeInfo API Call and Response processing
 * ----------------------------------------------------------------------------
 * This utility class is used to create GetZipInfo web service request 
 * object and garbage collect request/response data while invoking the same.  
 * 
 * @author vardekar_s
 *
 */
@Component
public class GetZipCodeInfoRequestSender {
	
	private static final Logger LOGGER = Logger.getLogger(GetZipCodeInfoRequestSender.class);
	
	/**
	 * Creates web service request object by setting values from request pojo. Web service
	 * request object is created by using web service factory instance.
	 * 
	 * @param requestVO - Request POJO
	 * @return - web service request object
	 * 
	 */
	public GetZipCodeInfo getZipCodeInfoRequest(GetZipCodeInfoRequestVO requestVO) {

		LOGGER.info("getZipCodeInfoRequest() Start");
		GetZipCodeInfo getZipCodeInfoRequest = null;
		try {
			if (null == requestVO) {
				LOGGER.error("Input parameters are empty.");
				return getZipCodeInfoRequest;
			}
			getZipCodeInfoRequest = ACA_FACTORY.createGetZipCodeInfo();
			GetZipCodeInfoRequest wsRequest = GENERATED_FACTORY.createGetZipCodeInfoRequest();
			wsRequest.setAccessKeys(getZipCodeInfoRequestAccessKey());
			wsRequest.setInputs(getZipCodeInfoRequestInputs(requestVO));
			getZipCodeInfoRequest.setGetZipCodeInfoRequest(ACA_FACTORY.createGetZipCodeInfoGetZipCodeInfoRequest(wsRequest));
			
			}
			finally {
				LOGGER.info("getZipCodeInfoRequest() End with GetZipCodeInfo wsRequestList: " + (null == getZipCodeInfoRequest));
			}
		LOGGER.info("getZipCodeInfoRequest() End");
		
		return getZipCodeInfoRequest;
	}

	/**
	 * Nullifies data present in web service request in order to make request object available for
	 * garbage collection.
	 * 
	 * @param wsRequest - The web service request instance.
	 */
	public void garbageRequest(GetZipCodeInfo wsRequest) {
		LOGGER.info("garbageRequest() Start");
		
		if(null != wsRequest.getGetZipCodeInfoRequest()){
			GetZipCodeInfoRequest request = wsRequest.getGetZipCodeInfoRequest().getValue();
			
				if(null != request){
					request.setInputs(null);
					request.getAccessKeys().setRemoteAccessKey(null);
					wsRequest.setGetZipCodeInfoRequest(null);
				}
		}
		
		LOGGER.info("garbageRequest() End");
	}
	
	/**
	 * Nullifies data present in web service response in order to make response object available for
	 * garbage collection.
	 * 
	 * @param zipCodeInfoResponse - The web service response instance.
	 */
	public void garbageResponse(GetZipCodeInfoResponse zipCodeInfoResponse){
		LOGGER.info("garbageResponse() Start");
		
		if(null != zipCodeInfoResponse && null != zipCodeInfoResponse.getGetZipCodeInfoResult()){
			
			generated.GetZipCodeInfoResponse response = zipCodeInfoResponse.getGetZipCodeInfoResult().getValue();
			
			if(null != response && null != response.getZipCodes()){
				ArrayOfGetZipCodeInfoResponseZipCodeInfo zipcodes = response.getZipCodes().getValue();
				
				List<GetZipCodeInfoResponseZipCodeInfo> zipCodeList = zipcodes.getGetZipCodeInfoResponseZipCodeInfo();
				
				if(!CollectionUtils.isEmpty(zipCodeList)){
					
					for(GetZipCodeInfoResponseZipCodeInfo getZipCodeInfoResponseZipCodeInfo : zipCodeList){
						
						getZipCodeInfoResponseZipCodeInfo.setAreaCodes(null);
						getZipCodeInfoResponseZipCodeInfo.setCities(null);
						getZipCodeInfoResponseZipCodeInfo.setIsQuotableState(false);
						getZipCodeInfoResponseZipCodeInfo.setLatitude(null);
						getZipCodeInfoResponseZipCodeInfo.setLongitude(null);
						getZipCodeInfoResponseZipCodeInfo.setState(null);
						getZipCodeInfoResponseZipCodeInfo.setStateName(null);
						getZipCodeInfoResponseZipCodeInfo.setTimeZone(null);
						getZipCodeInfoResponseZipCodeInfo.setZipCode(0);
						
						ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty counties = getZipCodeInfoResponseZipCodeInfo.getCounties();

						if(null != counties && null != counties.getGetZipCodeInfoResponseZipCodeInfoCounty()){
							garbageCounties( counties.getGetZipCodeInfoResponseZipCodeInfoCounty());
						}
					}
					zipCodeList.clear();
				}
				
			}
		}
		
		LOGGER.info("garbageResponse() End");
	}
	
	/**
	 * Utility method to garbage collect counties list.
	 * 
	 * @param counties - County list from web service response.
	 */
	private void garbageCounties(List<GetZipCodeInfoResponseZipCodeInfoCounty> counties){
		
		if(!CollectionUtils.isEmpty(counties)){
			short zero = 0;
			for(GetZipCodeInfoResponseZipCodeInfoCounty county : counties){
				county.setCountyFIPS(null);
				county.setCountyId(zero);
				county.setCountyName(null);
				county.setIsPreferred(null);
			}
			counties.clear();
		}
	}
	
	/**
	 * Creates GetZipCodeInfoRequestInput instance from request POJO for web service request..
	 * 
	 * @param requestVO - request POJO
	 * @return - GetZipCodeInfoRequestInput instance.
	 */
	private GetZipCodeInfoRequestInput getZipCodeInfoRequestInputs(GetZipCodeInfoRequestVO requestVO){
		LOGGER.debug("getZipCodeInfoRequestInputs() Start");
		GetZipCodeInfoRequestInput getZipCodeInfoRequestInput = GENERATED_FACTORY.createGetZipCodeInfoRequestInput();
		getZipCodeInfoRequestInput.setState(GENERATED_FACTORY.createGetZipCodeInfoRequestInputState(requestVO.getStateCode()));
		LOGGER.debug("getZipCodeInfoRequestInputs() End");
		return getZipCodeInfoRequestInput;
	}
	
	/**
	 * Creates GetZipCodeInfoRequestAccessKey instance having access key to used for authentication
	 * purpose.
	 * 
	 * @return - GetZipCodeInfoRequestAccessKey instance.
	 */
	private GetZipCodeInfoRequestAccessKey getZipCodeInfoRequestAccessKey() {
		LOGGER.debug("getZipCodeInfoRequestAccessKey() Start");
		GetZipCodeInfoRequestAccessKey accessKey = GENERATED_FACTORY.createGetZipCodeInfoRequestAccessKey();
		accessKey.setRemoteAccessKey(QuotitConstants.getRemoteAccessKey());
		LOGGER.debug("accessKey.getRemoteAccessKey(): " + accessKey.getRemoteAccessKey());
		LOGGER.debug("getZipCodeInfoRequestAccessKey() End");
		return accessKey;
	}

}
