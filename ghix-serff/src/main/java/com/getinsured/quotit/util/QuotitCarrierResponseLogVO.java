package com.getinsured.quotit.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Class is used to generate Quotit Carriers Response.
 * 
 * @author Bhavin Parmar
 * @since Mar 1, 2016
 */
public class QuotitCarrierResponseLogVO {

	String carrierId;
	String insuranceType;
	String state;
	List <String> planIdList;

	public QuotitCarrierResponseLogVO() {
		planIdList = new ArrayList<String>();
	}

	public String getCarrierId() {
		return carrierId;
	}

	public void setCarrierId(String carrierId) {
		this.carrierId = carrierId;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<String> getPlanIdList() {
		return planIdList;
	}

	public void setPlanId(String planId) {
		planIdList.add(planId);
	}
}
