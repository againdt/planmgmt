package com.getinsured.quotit.client;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.quotit.service.ZipCodesMedicareService;
import com.getinsured.quotit.util.GetZipCodeInfoRequestSender;
import com.getinsured.quotit.util.GetZipCodeInfoRequestVO;
import com.getinsured.quotit.util.QuotitConstants;
import com.quotit.services.actws.aca._2.GetZipCodeInfo;
import com.quotit.services.actws.aca._2.GetZipCodeInfoResponse;

/**
 * ----------------------------------------------------------------------------
 * HIX-60437 : GetZipCodeInfo API Call and Response processing
 * ----------------------------------------------------------------------------
 * Class used to call Quotit webservice by using rest template and pass the 
 * response to ZipCodesMedicareService for processing further. The states list 
 * is populated in static array with statecode and statefips. Zipcode list is
 * retrieved and processed for all states present in array. 
 * 
 * @author vardekar_s
 */
@Component("zipCodeInfoClient")
public class ZipCodeInfoServiceClient extends QuotitServiceClient implements QuotitService {

	private static final Logger LOGGER = Logger.getLogger(ZipCodeInfoServiceClient.class);

	private static final String stateAndFipsArr[][] = {		
		{"Alaska","AK","02"}, {"Alabama","AL","01"}, {"Arkansas","AR","05"}, {"Arizona","AZ","04"}, {"California","CA","06"},
    	{"Colorado","CO","08"}, {"Connecticut","CT","09"}, {"District of Columbia","DC","11"}, {"Delaware","DE","10"}, {"Florida","FL","12"},
    	{"Georgia","GA","13"}, {"Hawaii","HI","15"}, {"Iowa","IA","19"}, {"Idaho","ID","16"}, {"Illinois","IL","17"}, 
    	{"Indiana","IN","18"}, {"Kansas","KS","20"}, {"Kentucky","KY","21"}, {"Louisiana","LA","22"}, {"Massachusetts","MA","25"},
    	{"Maryland","MD","24"}, {"Maine","ME","23"}, {"Michigan","MI","26"}, {"Minnesota","MN","27"}, {"Missouri","MO","29"},
    	{"Mississippi","MS","28"}, {"Montana","MT","30"}, {"North Carolina","NC","37"}, {"North Dakota","ND","38"}, {"Nebraska","NE","31"},
    	{"New Hampshire","NH","33"}, {"New Jersey","NJ","34"}, {"New Mexico","NM","35"}, {"Nevada","NV","32"}, {"New York","NY","36"},
    	{"Ohio","OH","39"}, {"Oklahoma","OK","40"}, {"Oregon","OR","41"}, {"Pennsylvania","PA","42"}, {"Rhode Island","RI","44"},
    	{"South Carolina","SC","45"}, {"South Dakota","SD","46"}, {"Tennessee","TN","47"}, {"Texas","TX","48"}, {"Utah","UT","49"},
    	{"Virginia","VA","51"}, {"Vermont","VT","50"}, {"Washington","WA","53"}, {"Wisconsin","WI","55"}, {"West Virginia","WV","54"}, {"Wyoming","WY","56"}};
	
	@Autowired private GetZipCodeInfoRequestSender getZipCodeInfoRequestSender;
	@Autowired private ZipCodesMedicareService zipCodesMedicareService;
//	@Autowired private SerffUtils serffUtils;

	@Autowired
	public ZipCodeInfoServiceClient(@Qualifier("quotitServiceTemplate") WebServiceTemplate quotitServiceTemplate) {
		super(quotitServiceTemplate);
	}

	/**
	 * Reads state codes from static array - stateAndFipsArr and calls GetZipCodeInfo web service for each state.
	 * After receiving response it is further processed by ZipCodesMedicareService for persistence.
	 */
	@Override
	public String invokeQuotitService(final Map<String, Object> inputParamsMap) {

		LOGGER.info("invokeQuotitService() Start");
		boolean isStatus = false;
		String statusMessage = null;
		GetZipCodeInfoRequestVO requestVO = new GetZipCodeInfoRequestVO();

		for(int i = 0 ; i < stateAndFipsArr.length ; i++){    		
			LOGGER.info("invokeQuotitService() : Calling 'GetZipCodeInfo' webservice for state : " + (stateAndFipsArr[i][1]));
			
			requestVO.setStateName(stateAndFipsArr[i][QuotitConstants.INDEX_ZERO]);
    		requestVO.setStateCode(stateAndFipsArr[i][QuotitConstants.INDEX_ONE]);
    		requestVO.setStateFips(stateAndFipsArr[i][QuotitConstants.INDEX_TWO]);
    		
    		GetZipCodeInfoResponse getZipCodeInfoResponse = getZipCodeInfoWebServiceResponse(quotitServiceTemplate, requestVO);
    		if(null != getZipCodeInfoResponse){
    			try {
					zipCodesMedicareService.populateAndPersistResponse(getZipCodeInfoResponse, requestVO);

					if (!isStatus) {
						isStatus = true;
					}
				}
    			catch(GIException e) {
					LOGGER.error("Error occurred while persisting GetZipCodeInfo Web Service response.", e);
				}
    			finally{
					getZipCodeInfoRequestSender.garbageResponse(getZipCodeInfoResponse);
				}
    		}else{
    			LOGGER.error("GetZipCodeInfo Web Service response is null for state " + requestVO.getStateName());
    		}
    	}

		if (isStatus) {
			statusMessage = "Zip codes has been loaded successfully in database.";
		}
		else {
			statusMessage = "Failed to load Zip codes in database.";
		}
		LOGGER.info("invokeQuotitService() End");
		return statusMessage;
	}

	/**
	 * Populates ZipCode list response by calling Quotit Webservice.
	 * 
	 * @param quotitServiceTemplate - Rest template instance
	 * @param requestVO - Request POJO
	 * @return - GetZipCodeInfo web service response
	 */
	private GetZipCodeInfoResponse getZipCodeInfoWebServiceResponse(WebServiceTemplate quotitServiceTemplate,
			GetZipCodeInfoRequestVO requestVO) {

		LOGGER.info("populateGetZipCodeInfoResponse() Start");
		GetZipCodeInfo wsRequest = null;
		GetZipCodeInfoResponse wsResponse = null;

		try {
			wsRequest = getZipCodeInfoRequestSender.getZipCodeInfoRequest(requestVO);

			if (null == wsRequest) {
				LOGGER.error("GetZipCodeInfo sender object is empty.");
				return wsResponse;
			}
			//LOGGER.debug("GetZipCodeInfoResponse request Xml : "  + serffUtils.objectToXmlString(wsRequest));
			LOGGER.info("Invoking Quotit Service for GetZipCodeInfoResponse. StateCode : "+ requestVO.getStateCode());
			wsResponse = (GetZipCodeInfoResponse) invokeService(quotitServiceTemplate, wsRequest, QuotitConstants.getWsdlGetZipCodeInfoUrl());
  		    //LOGGER.debug("GetZipCodeInfoResponse response Xml : " + serffUtils.objectToXmlString(wsResponse));
		}catch(Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}finally {

			if (null != wsRequest) {
				getZipCodeInfoRequestSender.garbageRequest(wsRequest);
			}
			LOGGER.info("populateGetZipCodeInfoResponse() End");
		}
		
		return wsResponse;
	}

}
