package com.getinsured.quotit.client;

import static com.getinsured.quotit.util.QuotitConstants.INDEX_ZERO;
import static com.getinsured.quotit.util.QuotitConstants.SYSTEM_IP_LENGTH;
import static com.serff.util.SerffConstants.AT_THE_RATE;
import static com.serff.util.SerffConstants.COMMA;
import static com.serff.util.SerffConstants.DEFAULT_USER_ID;
import static com.serff.util.SerffConstants.ORC_COLUMN_MAX_LEN;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.getinsured.hix.model.ExtWSCallLogs;
import com.getinsured.quotit.service.QuotitPlanStatistics;
import com.getinsured.quotit.util.QuotitConstants;
import com.serff.repository.templates.IExtWSCallLogsRepository;
import com.serff.util.SerffConstants;

/**
 * Class is used to call Quotit Services.
 * 
 * @author Bhavin Parmar
 * @since January 29, 2015
 */
public abstract class QuotitServiceClient {

	private static final Logger LOGGER = Logger.getLogger(QuotitServiceClient.class);

	// Quotit WebService Template
	protected WebServiceTemplate quotitServiceTemplate;
	@Autowired private IExtWSCallLogsRepository trackingRepository;

	/**
	 * Generate WebServiceTemplate using Quotit Service endpoint.
	 */
	protected QuotitServiceClient(WebServiceTemplate quotitServiceTemplate) {
		LOGGER.info("Invoked constructor of QuotitServiceClient");
//		quotitServiceTemplate.setDefaultUri(QuotitConstants.getWsdlDefaultUri());
		this.quotitServiceTemplate = quotitServiceTemplate;
	}

	/**
	 * Method is used to invoke respective Quotit Service request and return response.
	 */
	protected Object invokeService(WebServiceTemplate quotitServiceTemplate,
			Object wsRequest, String soapAction) throws WebServiceIOException {

		LOGGER.debug("invokeService() Start");
		Object wsResponse = null;

		if (null != quotitServiceTemplate) {
			this.quotitServiceTemplate.setDefaultUri(QuotitConstants.getWsdlDefaultUri());
			LOGGER.debug("Default URI: " + this.quotitServiceTemplate.getDefaultUri());

			wsResponse = quotitServiceTemplate.marshalSendAndReceive(wsRequest, new SoapActionCallback(soapAction));
			LOGGER.info("WebService Template Response is null: " + (null == wsResponse));
			LOGGER.debug("invokeService() End");
		}
		else {
			LOGGER.error("Quotit WebServiceTemplate is null.");
		}
		return wsResponse;
	}

	/**
	 * Method is used to create tracking record in ExtWSCallLogs table for Quotit Service API call.
	 */
	protected ExtWSCallLogs createTrackingRecord(Integer parentReqId, String methodName, String requestParams, ExtWSCallLogs.REQUESTED_BY requestedBy) {

		LOGGER.debug("createExtWSCallLogsRecord() Start parentReqId: " + parentReqId);
		ExtWSCallLogs trackingRecord = new ExtWSCallLogs();

		try {

			if (null != parentReqId) {
				trackingRecord.setParentReqId(parentReqId);
				trackingRecord.setBatchState(ExtWSCallLogs.BATCH_STATE.BETWEEN.name());
			}
			else {
				trackingRecord.setBatchState(ExtWSCallLogs.BATCH_STATE.BEGIN.name());
			}
			trackingRecord.setMethodName(methodName);
			trackingRecord.setEndPoint(QuotitConstants.getWsdlDefaultUri());

			String serverList = SerffConstants.getServiceServerList();
			String[] servers = null;

			if (null != serverList) {
				servers = serverList.split(COMMA);
				LOGGER.debug("createSerffPlanMgmtRecord() : Servers List Size : " + servers.length);
			}

			if (null != servers && servers.length > 0 && null != servers[0]) {
				
				LOGGER.debug("server: " + servers[0]);
				String server = servers[0].substring(servers[0].indexOf(AT_THE_RATE) + 1);
				
				if (SYSTEM_IP_LENGTH < server.length()) {
					LOGGER.info("SystemIP: " + server);
					trackingRecord.setSystemIp(server.substring(0, SYSTEM_IP_LENGTH - 1));
				}
				else {
					LOGGER.debug("SystemIP: " + server);
					trackingRecord.setSystemIp(server);
				}
			}
			trackingRecord.setStartTime(new Date());
			trackingRecord.setStatus(ExtWSCallLogs.STATUS.IN_PROGRESS.name());
			trackingRecord.setRequestParams(requestParams);
			trackingRecord.setExecutedBy(DEFAULT_USER_ID);

			if (null != requestedBy && ExtWSCallLogs.REQUESTED_BY.BATCH == requestedBy) {
				trackingRecord.setRequestedBy(ExtWSCallLogs.REQUESTED_BY.BATCH.name());
			}
			else {
				trackingRecord.setRequestedBy(ExtWSCallLogs.REQUESTED_BY.UI.name());
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("createExtWSCallLogsRecord() End");
		}
		return trackingRecord;
	}

	/**
	 * Method is used to save tracking record in ExtWSCallLogs table for Quotit Service API call.
	 */
	protected ExtWSCallLogs saveTrackingRecord(ExtWSCallLogs trackingRecord, boolean status, QuotitPlanStatistics qtStats) {

		LOGGER.info("saveTrackingRecord() Start");
		ExtWSCallLogs savedTrackingRecord = null;

		try {
			if (status) {
				trackingRecord.setStatus(ExtWSCallLogs.STATUS.COMPLETED.name());	
			}
			else {
				trackingRecord.setStatus(ExtWSCallLogs.STATUS.FAILED.name());	
			}

			if (StringUtils.isNotBlank(trackingRecord.getRequestParams())
					&& ORC_COLUMN_MAX_LEN < trackingRecord.getRequestParams().length()) {
				trackingRecord.setRequestParams(trackingRecord.getRequestParams().substring(INDEX_ZERO, ORC_COLUMN_MAX_LEN));
			}

			if (StringUtils.isNotBlank(trackingRecord.getExceptionMessage())
					&& ORC_COLUMN_MAX_LEN < trackingRecord.getExceptionMessage().length()) {
				trackingRecord.setExceptionMessage(trackingRecord.getExceptionMessage().substring(INDEX_ZERO, ORC_COLUMN_MAX_LEN));
			}

			if (StringUtils.isNotBlank(trackingRecord.getRequest())
					&& ORC_COLUMN_MAX_LEN < trackingRecord.getRequest().length()) {
				trackingRecord.setRequest(trackingRecord.getRequest().substring(INDEX_ZERO, ORC_COLUMN_MAX_LEN));
			}

			if (StringUtils.isNotBlank(trackingRecord.getResponse())
					&& ORC_COLUMN_MAX_LEN < trackingRecord.getResponse().length()) {
				trackingRecord.setResponse(trackingRecord.getResponse().substring(INDEX_ZERO, ORC_COLUMN_MAX_LEN));
			}
			trackingRecord.setRemarks(qtStats.toString()); // Statistics format: [S:xx#F:xx#CS:xx#D:xx#TP:xx#TC:xx]
			trackingRecord.setEndTime(new Date());
			savedTrackingRecord = trackingRepository.save(trackingRecord);
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("saveTrackingRecord() End");
		}
		return savedTrackingRecord;
	}
}
