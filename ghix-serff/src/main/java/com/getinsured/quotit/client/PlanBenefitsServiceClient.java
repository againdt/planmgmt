package com.getinsured.quotit.client;

import static com.getinsured.quotit.util.QuotitConstants.METHOD_CARRIERS_PLANS_BENEFITS;
import static com.getinsured.quotit.util.QuotitConstants.PARAM_EFFECTIVE_DATE;
import static com.getinsured.quotit.util.QuotitConstants.PARAM_INSURANCE_TYPE;
import static com.getinsured.quotit.util.QuotitConstants.PARAM_REQUESTED_BY;
import static com.getinsured.quotit.util.QuotitConstants.PREFIX_HIOS_ISSUER_ID;
import static com.serff.util.SerffConstants.HYPHEN;
import generated.GetCarriersPlansBenefitsAddOn;
import generated.GetCarriersPlansBenefitsLevelOfDetail;
import generated.InsuranceType;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.hix.model.ExtWSCallLogs;
import com.getinsured.hix.model.ExtWSCallLogs.REQUESTED_BY;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.quotit.service.CarriersPlansBenefitsService;
import com.getinsured.quotit.service.QuotitPlanStatistics;
import com.getinsured.quotit.util.PlansBenefitsRequestSender;
import com.getinsured.quotit.util.PlansBenefitsRequestVO;
import com.getinsured.quotit.util.PlansBenefitsResponseReceiver;
import com.getinsured.quotit.util.QuotitConstants;
import com.quotit.services.actws.aca._2.GetCarriersPlansBenefits;
import com.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.util.SerffUtils;

/**
 * Class is used to invoke GetCarriersPlansBenefits requests from Quotit Services call
 * and process following steps:
 * 
 * 1) process CARRIERS level request and generate to PLANS level request from CARRIERS's response.
 * 2) process PLANS level request and generate to BENEFITS level request from PLANS's response.
 * 3) process BENEFITS level request and populates & persist all data in DB using CarriersPlansBenefitsService class.
 * 
 * @author Bhavin Parmar
 * @since January 29, 2015
 */
@Component("planBenefitsClient")
public class PlanBenefitsServiceClient extends QuotitServiceClient implements QuotitService {

	private static final Logger LOGGER = Logger.getLogger(PlanBenefitsServiceClient.class);
	private static final int LAST_DAY_HOUR = -24;
	private static final String EMSG_PARAM_EMPTY = "Input Parameter Map is empty.";
	private static final String EMSG_RESPONSE_EMPTY = "GetCarriersPlansBenefits API's Response is empty.";
	private static final String MSG_FAILED_TO_LOAD_PLANS = "Failed to load Medicare Plans in database.";
	private static final String MSG_SUCCEED_TO_LOAD_PLANS = "All Medicare Plans has been Loaded Successfully.";
	private static final String MSG_SUCCEED_WITH_WARN_TO_LOAD_PLANS = "Medicare Plans has been Loaded Successfully with warnings.";
	private static final String EMSG_EXCEPTION = "Webservice Call failed with following Exception: ";
	private static final String EMSG_WS_IO_EXCEPTION = "Webservice Call failed with following WebServiceIOException: ";
	private static final String EMSG_SOAP_FAULT_EXCEPTION = "Webservice Call failed with following SoapFaultClientException: ";

	@Autowired private PlansBenefitsRequestSender plansBenefitsRequestSender;
	@Autowired private PlansBenefitsResponseReceiver plansBenefitsResponseReceiver;
	@Autowired private CarriersPlansBenefitsService carriersPlansBenefitsService;
	@Autowired private SerffUtils serffUtils;
	@Autowired private IIssuersRepository iIssuerRepository;

	private static final String INSURANCE_CATEGORY = "Family";
	private static final GetCarriersPlansBenefitsAddOn ADD_ON_ARRAY[] = GetCarriersPlansBenefitsAddOn.values();
	private static final String DATA_SET_LIST = "Current";
	private DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
	private static final int ZERO_VAL = 0;

	/**
	 * Generate WebServiceTemplate using super constructor of QuotitServiceClient.
	 */
	@Autowired
	public PlanBenefitsServiceClient(@Qualifier("quotitServiceTemplate") WebServiceTemplate quotitServiceTemplate) {
		super(quotitServiceTemplate);
	}

	/**
	 * Method is used to invoke GetCarriersPlansBenefits => MedicareAdvantage Service.
	 * WS URL: https://ws.quotit.net/ActWS/ACA/v2/ACA.svc
	 */
	@Override
	public String invokeQuotitService(final Map<String, Object> inputParamsMap) {

		LOGGER.info("invokeQuotitService() Start");
		StringBuilder statusMessage = new StringBuilder();
		PlansBenefitsRequestVO requestVO = null;
		ExtWSCallLogs trackingRecord = null;
		Integer parentReqId = null;
		QuotitPlanStatistics qtStats = new QuotitPlanStatistics();

		try {

			if (!validateInputParams(inputParamsMap, statusMessage)) {
				return statusMessage.toString();
			}

			REQUESTED_BY requestedBy = (REQUESTED_BY) inputParamsMap.get(PARAM_REQUESTED_BY);
			InsuranceType insuranceTypeEnum = InsuranceType.valueOf(String.valueOf(inputParamsMap.get(PARAM_INSURANCE_TYPE)));
			String effectiveDate = (String) inputParamsMap.get(PARAM_EFFECTIVE_DATE);

			trackingRecord = createTrackingRecord(null, METHOD_CARRIERS_PLANS_BENEFITS + HYPHEN + GetCarriersPlansBenefitsLevelOfDetail.CARRIERS.value(), null, requestedBy);
			// Create PlansBenefitsRequestVO object.
			requestVO = createRequestVO(insuranceTypeEnum, effectiveDate, GetCarriersPlansBenefitsLevelOfDetail.CARRIERS);
			// Create List<String-CarrierID> format
			List<Integer> carrierIdList = getCarriersIdListFromResponse(quotitServiceTemplate, requestVO, qtStats, trackingRecord);
			if (CollectionUtils.isEmpty(carrierIdList)) {
				statusMessage.append(trackingRecord.getExceptionMessage());
				return statusMessage.toString();
			}
			parentReqId = trackingRecord.getId();
			//Since the stats are already saved for previous call, reset for new call
			qtStats.reset();

			trackingRecord = createTrackingRecord(parentReqId, METHOD_CARRIERS_PLANS_BENEFITS + HYPHEN + GetCarriersPlansBenefitsLevelOfDetail.PLANS.value(), null, requestedBy);
			// Update PlansBenefitsRequestVO object.
			updateRequestVO(requestVO, GetCarriersPlansBenefitsLevelOfDetail.PLANS, carrierIdList, null);
			// Create Map<Integer-CarrierID, List<String-PlanID>> format
			Map<Integer, List<String>> carriersPlansMap = getCarriersPlanMapFromResponse(quotitServiceTemplate, requestVO, qtStats, trackingRecord);
			//Since the stats are already saved for previous call, reset for new call
			qtStats.reset();
			if (!CollectionUtils.isEmpty(carriersPlansMap)) {
				// Update PlansBenefitsRequestVO object.
				updateRequestVO(requestVO, GetCarriersPlansBenefitsLevelOfDetail.BENEFITS, null, carriersPlansMap);
				// Populate And Persist Carriers Plans Data in Database.
				statusMessage.append(populateAndPersistCarriersPlansData(requestVO, parentReqId, requestedBy));
			}
			else {
				statusMessage.append(trackingRecord.getExceptionMessage());
			}
		}
		catch (SoapFaultClientException ex) {

			if (null != trackingRecord) {
				trackingRecord.setExceptionMessage(EMSG_SOAP_FAULT_EXCEPTION + ex.getMessage());
				trackingRecord.setBatchState(ExtWSCallLogs.BATCH_STATE.END.name());
				trackingRecord.setRequestParams(requestVO.getRequestParams());
				trackingRecord = saveTrackingRecord(trackingRecord, false, qtStats);
			}
			LOGGER.error(ex.getMessage(), ex);
		}
		catch (Exception ex) {

			if (null != trackingRecord) {
				trackingRecord.setExceptionMessage(EMSG_EXCEPTION + ex.getMessage());
				trackingRecord.setBatchState(ExtWSCallLogs.BATCH_STATE.END.name());
				trackingRecord.setRequestParams(requestVO.getRequestParams());
				trackingRecord = saveTrackingRecord(trackingRecord, false, qtStats);
			}
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (StringUtils.isBlank(statusMessage) && null != trackingRecord) {

				if (StringUtils.isNotBlank(trackingRecord.getExceptionMessage())) {
					statusMessage.append(trackingRecord.getExceptionMessage());
				}
				else {
					statusMessage.append(MSG_FAILED_TO_LOAD_PLANS);
				}
			}
			else if (StringUtils.isBlank(statusMessage)) {
				statusMessage.append(MSG_FAILED_TO_LOAD_PLANS);
			}

			if (null != requestVO) {

				if (!CollectionUtils.isEmpty(requestVO.getCarrierIdList())) {
					requestVO.getCarrierIdList().clear();
				}

				if (!CollectionUtils.isEmpty(requestVO.getCarriersPlansMap())) {
					requestVO.getCarriersPlansMap().clear();
				}
				requestVO = null;
			}
			LOGGER.info("invokeQuotitService() End");
		}
		return statusMessage.toString();
	}

	/**
	 * Method is used to populate and persist Carriers & Plans Data in database.
	 */
	private String populateAndPersistCarriersPlansData(PlansBenefitsRequestVO requestVO, Integer parentReqId, REQUESTED_BY requestedBy) {

		LOGGER.debug("populateAndPersistCarriersPlansData() Start with GetCarriersPlansBenefits.BENEFITS Requests parentReqId: " + parentReqId);
		String statusMessage = null;
		ExtWSCallLogs trackingRecord = null;
		boolean anyPlanSaved = false;
		boolean anyPlanFailed = false;
		boolean planSaved = false;

		try {
			GetCarriersPlansBenefitsResponse wsResponse = null;
			int carrierListSize = requestVO.getCarrierIdList().size();
//			requestVO.setCurrentIndex(carrierListSize - 50); For testing purpose only
			QuotitPlanStatistics qtStats = new QuotitPlanStatistics();

			while (requestVO.getCurrentIndex() < carrierListSize) {
				qtStats.reset();
				trackingRecord = createTrackingRecord(parentReqId, METHOD_CARRIERS_PLANS_BENEFITS + HYPHEN + GetCarriersPlansBenefitsLevelOfDetail.BENEFITS.value(), null, requestedBy);
				wsResponse = invokePlansBenefitWSCall(quotitServiceTemplate, requestVO, trackingRecord);
				planSaved = false;

				if (null != wsResponse) {
					planSaved = carriersPlansBenefitsService.populateAndPersistResponse(wsResponse, requestVO, qtStats, trackingRecord);
					// Garbage Carriers Plans Response object
					plansBenefitsResponseReceiver.garbageResponse(wsResponse);
					wsResponse = null;

					if (planSaved) {
						anyPlanSaved = true;
					}
					else {
						anyPlanFailed = true;
					}
				}
				else {
					anyPlanFailed = true;
					qtStats.setFailedPlanCount(requestVO.getPlansCounter());
				}
				trackingRecord.setBatchState(ExtWSCallLogs.BATCH_STATE.BETWEEN.name());
				trackingRecord.setRequestParams(requestVO.getRequestParams());
				trackingRecord = saveTrackingRecord(trackingRecord, planSaved, qtStats);
			}

			if (null != trackingRecord && requestVO.getCurrentIndex() >= carrierListSize) {
				trackingRecord.setBatchState(ExtWSCallLogs.BATCH_STATE.END.name());
				trackingRecord = saveTrackingRecord(trackingRecord, planSaved, qtStats);
			}
		}
		catch (GIException ex) {
			statusMessage = "Error occured while populating and persisting Response.";
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (anyPlanSaved && !anyPlanFailed) {
				statusMessage = MSG_SUCCEED_TO_LOAD_PLANS;
			}
			else if (anyPlanSaved) {
				statusMessage = MSG_SUCCEED_WITH_WARN_TO_LOAD_PLANS;
			}
			else if (StringUtils.isBlank(statusMessage)) {
				statusMessage = MSG_FAILED_TO_LOAD_PLANS;
			}
			LOGGER.info("populateAndPersistCarriersPlansData() End with Status Message: " + statusMessage);
		}
		return statusMessage;
	}

	/**
	 * Method is used to get Carriers and Plans from response to Map<Integer-CarrierID, List<String-PlanID>> format.
	 */
	private Map<Integer, List<String>> getCarriersPlanMapFromResponse(WebServiceTemplate quotitServiceTemplate, PlansBenefitsRequestVO requestVO,
			QuotitPlanStatistics qtStats, ExtWSCallLogs trackingRecord) {

		LOGGER.info("getCarriersPlanMapFromResponse() Start with GetCarriersPlansBenefits.PLANS Request");
		Map<Integer, List<String>> carriersPlansMap = null;
		GetCarriersPlansBenefitsResponse wsResponse = null;
		boolean status = false;
		int planListSize = ZERO_VAL;

		try {
			wsResponse = invokePlansBenefitWSCall(quotitServiceTemplate, requestVO, trackingRecord);

			if (null == wsResponse) {
				return carriersPlansMap;
			}
			carriersPlansMap = plansBenefitsResponseReceiver.getCarriersPlansMap(wsResponse, trackingRecord);

			if (!CollectionUtils.isEmpty(carriersPlansMap)) {
				for (Map.Entry<Integer, List<String>> entryData : carriersPlansMap.entrySet()) {
					planListSize += entryData.getValue().size();
				}
				qtStats.setReceivedPlanCount(planListSize);
			}
		}
		catch (Exception ex) {
			trackingRecord.setExceptionMessage(ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (CollectionUtils.isEmpty(carriersPlansMap)) {

				if (StringUtils.isBlank(trackingRecord.getExceptionMessage())) {
					trackingRecord.setExceptionMessage("No plan returned for any of the carrier.");
					trackingRecord.setBatchState(ExtWSCallLogs.BATCH_STATE.END.name());
				}
				LOGGER.error(trackingRecord.getExceptionMessage());
			}
			else {
				trackingRecord.setExceptionMessage("Received list of Carrier Plans having total count of " + planListSize);
				LOGGER.info(trackingRecord.getExceptionMessage());
				status = true;
				trackingRecord.setBatchState(ExtWSCallLogs.BATCH_STATE.BETWEEN.name());
			}
			trackingRecord.setRequestParams(requestVO.getRequestParams());
			trackingRecord = saveTrackingRecord(trackingRecord, status, qtStats);
			// Garbage Carriers Plans Response object
			plansBenefitsResponseReceiver.garbageResponse(wsResponse);
			wsResponse = null;
			LOGGER.info("getCarriersPlanMapFromResponse() End with CarriersPlansMap is null: " + (CollectionUtils.isEmpty(carriersPlansMap)));
		}
		return carriersPlansMap;
	}

	/**
	 * Method is used to get Carriers Id from response to List<Integer-CarrierID> format.
	 */
	private List<Integer> getCarriersIdListFromResponse(WebServiceTemplate quotitServiceTemplate, PlansBenefitsRequestVO requestVO,
			QuotitPlanStatistics qtStats, ExtWSCallLogs trackingRecord) {

		LOGGER.info("getCarriersIdListFromResponse() Start with GetCarriersPlansBenefits.CARRIERS Request");
		List<Integer> carrierIdList = null;
		GetCarriersPlansBenefitsResponse wsResponse = null;
		boolean status = false;

		try {
			wsResponse = invokePlansBenefitWSCall(quotitServiceTemplate, requestVO, trackingRecord);

			if (null != wsResponse) {
				carrierIdList = plansBenefitsResponseReceiver.getCarrierIdList(wsResponse, trackingRecord);
				carrierIdList = removeRecentlyLoadedCarriers(carrierIdList, qtStats, trackingRecord);
			}
		}
		catch (Exception ex) {
			carrierIdList = null;
			trackingRecord.setExceptionMessage(ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (!CollectionUtils.isEmpty(carrierIdList)) {

				if (ZERO_VAL < qtStats.getSkippedCarrierCount()) {
					trackingRecord.setExceptionMessage("Received list of Carriers count: " + carrierIdList.size() + " and skipped Carriers count: " + qtStats.getSkippedCarrierCount());
					LOGGER.warn(trackingRecord.getExceptionMessage());
				}
				else {
					trackingRecord.setExceptionMessage("Received list of Carriers count: " + carrierIdList.size());
					LOGGER.info(trackingRecord.getExceptionMessage());
				}
				status = true;
			}
			trackingRecord.setRequestParams(requestVO.getRequestParams());
			trackingRecord = saveTrackingRecord(trackingRecord, status, qtStats);
			// Garbage Carriers Plans Response object
			plansBenefitsResponseReceiver.garbageResponse(wsResponse);
			wsResponse = null;
			LOGGER.info("getCarriersIdListFromResponse() End with carrierIdList is null: " + (CollectionUtils.isEmpty(carrierIdList)));
		}
		return carrierIdList;
	}

	/**
	 * Method is used to skipped existing issuer because its Last updated date is time in last 24 hours.
	 */
	private List<Integer> removeRecentlyLoadedCarriers(List<Integer> carrierIdList, QuotitPlanStatistics qtStats, ExtWSCallLogs trackingRecord) {

		LOGGER.debug("removeRecentlyLoadedCarriers() Start");
		List<Integer> finalCarrierIdList = null;
		int totalCarrierCount = ZERO_VAL;

		try {
			if (CollectionUtils.isEmpty(carrierIdList)) {

				if (StringUtils.isBlank(trackingRecord.getExceptionMessage())) {
					trackingRecord.setExceptionMessage("Carrier Id List is empty.");
				}
				return finalCarrierIdList;
			}

			Issuer issuer;
			Calendar calLastDayTime = Calendar.getInstance(); // creates calendar
			calLastDayTime.setTime(new Date()); // sets calendar time/date
			calLastDayTime.add(Calendar.HOUR_OF_DAY, LAST_DAY_HOUR);
			LOGGER.info("Calendar Last Day Time: " + calLastDayTime);
			finalCarrierIdList = new ArrayList<Integer>();

			for (Integer carrierID : carrierIdList) {

				if (null == carrierID) {
					continue;
				}
				totalCarrierCount++;
				// Get existing Issuer from Database.
				issuer = iIssuerRepository.getIssuerByHiosID(PREFIX_HIOS_ISSUER_ID + carrierID);

				if (null != issuer && null != issuer.getIssuerExt()
						&& issuer.getIssuerExt().getLastUpdateTimestamp().after(calLastDayTime.getTime())) {
					qtStats.skippedCarrier();
					LOGGER.info("Skipped existing issuer["+ PREFIX_HIOS_ISSUER_ID + carrierID +"] because its Last updated date is time in last 24 hours.");
				}
				else {
					finalCarrierIdList.add(carrierID);
				}
			}
			qtStats.setReceivedCarrierCount(totalCarrierCount);

			if (CollectionUtils.isEmpty(finalCarrierIdList)) {
				trackingRecord.setExceptionMessage("All Plan data is already up-todate.");
				LOGGER.info(trackingRecord.getExceptionMessage());
			}
			carrierIdList.clear();
		}
		finally {

			if (StringUtils.isNotBlank(trackingRecord.getExceptionMessage())) {
				LOGGER.error(trackingRecord.getExceptionMessage());
			}
			LOGGER.debug("removeRecentlyLoadedCarriers() End");
		}
		return finalCarrierIdList;
	}

	/**
	 * Method is used to invoke GetCarriersPlansBenefits API [Level: CARRIERS, PLANS or BENEFITS].
	 */
	private GetCarriersPlansBenefitsResponse invokePlansBenefitWSCall(WebServiceTemplate quotitServiceTemplate,
			PlansBenefitsRequestVO requestVO, ExtWSCallLogs trackingRecord) {

		LOGGER.debug("invokePlansBenefitWSCall() Start");
		GetCarriersPlansBenefits wsRequest = null;
		GetCarriersPlansBenefitsResponse wsResponse = null;
		String objectToJSON = null;

		try {
			objectToJSON = serffUtils.objectToJSONString(requestVO);
			trackingRecord.setRequest(objectToJSON);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Request Object To JSON String:= " + objectToJSON);
			}
			// Generating Request for Quotit Service
			wsRequest = plansBenefitsRequestSender.getCarriersPlansRequest(requestVO);

			if (null == wsRequest) {
				LOGGER.error("CarriersPlansRequest sender object is empty.");
				return wsResponse;
			}

			LOGGER.info("Invoking Quotit Service for GetCarriersPlansBenefits."+ requestVO.getLevelOfDetails().name() +" Request.");
			wsResponse = (GetCarriersPlansBenefitsResponse) invokeService(quotitServiceTemplate, wsRequest, QuotitConstants.getWsdlGetCarrierPlanBenefitsUrl());

			if (null != wsResponse) {
				objectToJSON = serffUtils.objectToJSONString(plansBenefitsResponseReceiver.getQuotitResponseVO(wsResponse));
				trackingRecord.setResponse(objectToJSON);

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Response Object To JSON String:= " + objectToJSON);
				}
			}
		}
		catch (WebServiceIOException ex) {
			trackingRecord.setExceptionMessage(EMSG_WS_IO_EXCEPTION + ex.getMessage());
			requestVO.setErrorMessages(trackingRecord.getExceptionMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		catch (SoapFaultClientException ex) {
			trackingRecord.setExceptionMessage(EMSG_SOAP_FAULT_EXCEPTION + ex.getMessage());
			requestVO.setErrorMessages(trackingRecord.getExceptionMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		catch (Exception ex) {
			trackingRecord.setExceptionMessage(EMSG_EXCEPTION + ex.getMessage());
			requestVO.setErrorMessages(trackingRecord.getExceptionMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (null == wsResponse) {
				if (StringUtils.isBlank(trackingRecord.getExceptionMessage())) {
					trackingRecord.setExceptionMessage(EMSG_RESPONSE_EMPTY);
				}
			}
			objectToJSON = null;
			// Garbage Carriers Plans Request
			plansBenefitsRequestSender.garbageRequest(wsRequest);
			LOGGER.debug("invokePlansBenefitWSCall() End");
		}
		return wsResponse;
	}

	/**
	 * Method is used to validate input parameters.
	 */
	private boolean validateInputParams(Map<String, Object> inputParamsMap, StringBuilder errorMessage) {

		LOGGER.debug("validateInputParams() Start");
		boolean isValid = false;

		try {

			if (CollectionUtils.isEmpty(inputParamsMap)) {
				errorMessage.append(EMSG_PARAM_EMPTY);
				return isValid;
			}
			isValid = true;
			List<String> errorList = new ArrayList<>();

			if (null == inputParamsMap.get(PARAM_REQUESTED_BY)) {
				errorList.add("Executed By");
				isValid = false;
			}

			if (null == inputParamsMap.get(PARAM_INSURANCE_TYPE)
					|| StringUtils.isBlank(String.valueOf(inputParamsMap.get(PARAM_INSURANCE_TYPE)))
					|| null == InsuranceType.valueOf(String.valueOf(inputParamsMap.get(PARAM_INSURANCE_TYPE)))) {
				errorList.add("Insurance Type");
				isValid = false;
			}

			if (!validateDate(String.valueOf(inputParamsMap.get(PARAM_EFFECTIVE_DATE)))) {
				errorList.add("Effective Date");
				isValid = false;
			}

			if (!CollectionUtils.isEmpty(errorList)) {
				errorMessage.append("Invalid Input Parameters: ");
				errorMessage.append(errorList.toString());
			}
		}
		finally {

			if (!isValid) {
				LOGGER.error(errorMessage.toString());
			}
			LOGGER.debug("validateInputParams() End");
		}
		return isValid;
	}

	/**
	 * Method is used to validate Effective Date.
	 */
	private boolean validateDate(String effectiveDate) {

		boolean isValid = true;

		try {
			if (null == effectiveDate) {
				isValid = false;
			}
			else {
				Date date = formatter.parse(String.valueOf(effectiveDate));
				Calendar currentCal = Calendar.getInstance();
				currentCal.set(Calendar.HOUR_OF_DAY, ZERO_VAL);
				currentCal.set(Calendar.MINUTE, ZERO_VAL);
				currentCal.set(Calendar.SECOND, ZERO_VAL);
				currentCal.set(Calendar.MILLISECOND, ZERO_VAL);

				if (date.compareTo(currentCal.getTime()) < ZERO_VAL) {
					isValid = false;
				}
			}
		}
		catch (ParseException e) {
			isValid = false;
		}
		return isValid;
	}

	/**
	 * Method is used to create Request POJO for GetCarriersPlansBenefits API.
	 */
	private PlansBenefitsRequestVO createRequestVO(InsuranceType insuranceTypeEnum, String effectiveDate,
			GetCarriersPlansBenefitsLevelOfDetail levelOfDetails) {

		LOGGER.debug("Creating PlansBenefitsRequestVO object for " + levelOfDetails.value());
		PlansBenefitsRequestVO requestVO = new PlansBenefitsRequestVO();
		requestVO.setQuotitRemoteAccessKey(QuotitConstants.getRemoteAccessKey());
		requestVO.setQuotitWebsiteAccessKey(QuotitConstants.getWebsiteAccessKey());

		if (StringUtils.isNumeric(QuotitConstants.getBrokerId())) {
			requestVO.setBrokerID(Integer.valueOf(QuotitConstants.getBrokerId()));
		}
		requestVO.setEffectiveCalendar(getXMLGregorianCalendar(effectiveDate));
		requestVO.setInsuranceCategory(INSURANCE_CATEGORY);
		requestVO.setLevelOfDetails(levelOfDetails);
		requestVO.setDataSetList(DATA_SET_LIST);

		if (GetCarriersPlansBenefitsLevelOfDetail.BENEFITS.equals(levelOfDetails)) {
			requestVO.setAddOnList(Arrays.asList(ADD_ON_ARRAY));
		}
		requestVO.setInsuranceType(insuranceTypeEnum);
		return requestVO;
	}

	/**
	 * Method is used to update Request POJO for GetCarriersPlansBenefits API.
	 */
	private void updateRequestVO(PlansBenefitsRequestVO requestVO, GetCarriersPlansBenefitsLevelOfDetail levelOfDetails,
			List<Integer> carrierIdList, Map<Integer, List<String>> carriersPlansMap) {
		LOGGER.debug("Updating PlansBenefitsRequestVO object for " + levelOfDetails.name());
		requestVO.setLevelOfDetails(levelOfDetails);

		if (GetCarriersPlansBenefitsLevelOfDetail.BENEFITS.equals(levelOfDetails)) {
			requestVO.setAddOnList(Arrays.asList(ADD_ON_ARRAY));
		}

		if (!CollectionUtils.isEmpty(carrierIdList)) {
			requestVO.setCarrierIdList(carrierIdList);
		}

		if (!CollectionUtils.isEmpty(carriersPlansMap)) {
			requestVO.setCarriersPlansMap(carriersPlansMap);
		}
	}

	/**
	 * Method is used to get date from Effective Date or get 1st date of next current month.
	 */
	private XMLGregorianCalendar getXMLGregorianCalendar(String effectiveDate) {

		LOGGER.debug("getXMLGregorianCalendar() Start");
		XMLGregorianCalendar gregorianDate = null;
		GregorianCalendar calendar = new GregorianCalendar();

		try {
			// Get date from Effective Date
			if (StringUtils.isNotBlank(effectiveDate)) {
				calendar.setTime(formatter.parse(effectiveDate));
			}
			// Get 1st date of next current month
			else {
				LOGGER.warn("Set 1st date of next current month because Effective Date is empty.");
				calendar.add(Calendar.MONTH, 1); // Set next month of current Date.
				calendar.set(Calendar.DATE, 1); // Set 1st date of the Month
			}
			gregorianDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("getXMLGregorianCalendar() End");
		}
		return gregorianDate;
	}
}
