package com.getinsured.quotit.client;

import java.util.Map;


/**
 * Interface is used to call Quotit Services using QuotitServiceClient class.
 * 
 * @author Bhavin Parmar
 * @since January 29, 2015
 */
public interface QuotitService {

	/**
	 * Method is used to invoke respective API request of Quotit Service.
	 * @param inputParamsMap, Input parameters for API request.
	 * @return Status message.
	 */
	String invokeQuotitService(final Map<String, Object> inputParamsMap);
}
