package com.getinsured.quotit.service;

import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.quotit.util.GetZipCodeInfoRequestVO;
import com.quotit.services.actws.aca._2.GetZipCodeInfoResponse;

/**
 * ----------------------------------------------------------------------------
 * HIX-60437 : GetZipCodeInfo API Call and Response processing
 * ----------------------------------------------------------------------------
 * This interface manages all the response processing and persisting task.
 * 
 * @author vardekar_s
 *
 */
public interface ZipCodesMedicareService {
	
	/**
	 * Processed web service response data and save new zipcode records in database.
	 * 
	 * @param gettZipCodeInfoResponse - The GetZipCodeInfo web service response
	 * @param requestVO - Request pojo
	 * @throws GIException - Exception thown if any error occurred in data processing and persisiting.
	 */
	void populateAndPersistResponse(GetZipCodeInfoResponse gettZipCodeInfoResponse,
			GetZipCodeInfoRequestVO requestVO) throws GIException;
	
}
