package com.getinsured.quotit.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Class is used to collect statistics on the count of Successfully Processed
 * plan / Failed plan / Skipped Carrier / Deleted plan / Received Plan / Received Carrier.
 * 
 * Statistics format: [S:xx#F:xx#CS:xx#D:xx#TP:xx#TC:xx]
 * 
 * @author Bhavin Parmar
 * @since May 4, 2015
 */
public class QuotitPlanStatistics {

	private static final Logger LOGGER = Logger.getLogger(QuotitPlanStatistics.class);
	private static final int START_INDEX_DEGIT2 = 2;
	private static final int START_INDEX_DEGIT3 = 3;
	private static final String EMSG_INVALID_STATS_VALUE = "Invalid Stats value: ";
	private static final String EMSG_EMPTY_STATS_VALUE = "Empty Stats value.";

	private int succeedPlanCount;
	private int failedPlanCount;
	private int skippedCarrierCount;
	private int deletedPlanCount;
	private int receivedPlanCount;
	private int receivedCarrierCount;

	/**
	 * Enum is used to identify Symbol of Quotit Statistics format[S:xx#F:xx#CS:xx#D:xx#TP:xx#TC:xx].
	 */
	private enum QT_SYMBOL {

		SUCCEED_PLANS("S:"), // Successfully Processed plan count.
		FAILED_PLANS("F:"), // Failed plan count.
		SKIPPED_CARRIERS("CS:"), // Skipped Carrier count.
		DELETED_PLANS("D:"), // Deleted plan count.
		TOTAL_PLANS("TP:"), // Total plan count.
		TOTAL_CARRIERS("TC:"), // Total carrier count.
		COLON_SEPARATOR(":"), // Separate each value for Statistics.
		HASH_SEPARATOR("#"); // Statistics format.

		private String value;

		QT_SYMBOL(String value) {
			this.value = value;
		}
	}

	public void reset() {
		succeedPlanCount = 0;
		failedPlanCount = 0;
		skippedCarrierCount = 0;
		deletedPlanCount = 0;
		receivedPlanCount = 0;
		receivedCarrierCount = 0;
	}

	/**
	 * Method is used set Counters data from Stats Format [S:xx#F:xx#CS:xx#D:xx#TP:xx#TC:xx]
	 */
	public void setCountersFromStatsFormat(String statsFormat) {

		if (StringUtils.isBlank(statsFormat)) {
			LOGGER.error(EMSG_EMPTY_STATS_VALUE);
			return;
		}

		try {
			statsFormat = statsFormat.trim();

			int start = statsFormat.indexOf(QT_SYMBOL.SUCCEED_PLANS.value) + START_INDEX_DEGIT2;
			int end = statsFormat.indexOf(QT_SYMBOL.HASH_SEPARATOR.value + QT_SYMBOL.FAILED_PLANS.value);
			succeedPlanCount = Integer.valueOf(statsFormat.substring(start, end));
			LOGGER.debug(succeedPlanCount);

			start = statsFormat.indexOf(QT_SYMBOL.FAILED_PLANS.value) + START_INDEX_DEGIT2;
			end = statsFormat.indexOf(QT_SYMBOL.HASH_SEPARATOR.value + QT_SYMBOL.SKIPPED_CARRIERS.value);
			failedPlanCount = Integer.valueOf(statsFormat.substring(start, end));
			LOGGER.debug(failedPlanCount);

			start = statsFormat.indexOf(QT_SYMBOL.SKIPPED_CARRIERS.value) + START_INDEX_DEGIT3;
			end = statsFormat.indexOf(QT_SYMBOL.HASH_SEPARATOR.value + QT_SYMBOL.DELETED_PLANS.value);
			skippedCarrierCount = Integer.valueOf(statsFormat.substring(start, end));
			LOGGER.debug(skippedCarrierCount);

			start = statsFormat.indexOf(QT_SYMBOL.DELETED_PLANS.value) + START_INDEX_DEGIT2;
			end = statsFormat.indexOf(QT_SYMBOL.HASH_SEPARATOR.value + QT_SYMBOL.TOTAL_PLANS.value);
			deletedPlanCount = Integer.valueOf(statsFormat.substring(start, end));
			LOGGER.debug(deletedPlanCount);

			start = statsFormat.indexOf(QT_SYMBOL.TOTAL_PLANS.value) + START_INDEX_DEGIT3;
			end = statsFormat.indexOf(QT_SYMBOL.HASH_SEPARATOR.value + QT_SYMBOL.TOTAL_CARRIERS.value);
			receivedPlanCount = Integer.valueOf(statsFormat.substring(start, end));
			LOGGER.debug(receivedPlanCount);

			start = statsFormat.indexOf(QT_SYMBOL.TOTAL_CARRIERS.value) + START_INDEX_DEGIT3;
			end = statsFormat.length();
			receivedCarrierCount = Integer.valueOf(statsFormat.substring(start, end));
			LOGGER.debug(receivedCarrierCount);
		}
		catch (NumberFormatException ex) {
			LOGGER.error(EMSG_INVALID_STATS_VALUE + ex.getMessage());
		}
	}

	@Override
	public String toString() {
		return "S:" + succeedPlanCount + "#F:" + failedPlanCount + "#CS:" + skippedCarrierCount + "#D:" + deletedPlanCount + "#TP:"
				+ receivedPlanCount + "#TC:" + receivedCarrierCount;
	}

	public String getQuotitStatsForm() {
		return "Quotit Plan Statistics [Plans loaded:" + succeedPlanCount + ", Plans failed:" + failedPlanCount
				+ ", Carriers skipped:" + skippedCarrierCount + ", Plans deleted:" + deletedPlanCount
				+ ", Total Plans Received: " + receivedPlanCount + " and Total Carriers Received: " + receivedCarrierCount + "]";
	}

	/**
	 * Adjust statistics when a single plan got added to DB.
	 */
	public void planAdded() {
		succeedPlanCount++;
	}

	/**
	 * Adjust statistics when a single plan got failed to DB.
	 */
	public void planFailed() {
		failedPlanCount++;
	}

	/**
	 * Adjust statistics when a carrier skipped to load in DB.
	 */
	public void skippedCarrier() {
		skippedCarrierCount++;
	}

	/**
	 * Adjust statistics when a single plan got deleted to DB.
	 */
	public void planDeleted() {
		deletedPlanCount++;
	}

	public int getSucceedPlanCount() {
		return succeedPlanCount;
	}

	public void setSucceedPlanCount(int succeedPlanCount) {
		this.succeedPlanCount = succeedPlanCount;
	}

	public int getFailedPlanCount() {
		return failedPlanCount;
	}

	public void setFailedPlanCount(int failedPlanCount) {
		this.failedPlanCount = failedPlanCount;
	}

	public int getSkippedCarrierCount() {
		return skippedCarrierCount;
	}

	public void setSkippedCarrierCount(int skippedCarrierCount) {
		this.skippedCarrierCount = skippedCarrierCount;
	}

	public int getDeletedPlanCount() {
		return deletedPlanCount;
	}

	public void setDeletedPlanCount(int deletedPlanCount) {
		this.deletedPlanCount = deletedPlanCount;
	}

	public int getReceivedPlanCount() {
		return receivedPlanCount;
	}

	public void setReceivedPlanCount(int receivedPlanCount) {
		this.receivedPlanCount = receivedPlanCount;
	}

	public int getReceivedCarrierCount() {
		return receivedCarrierCount;
	}

	public void setReceivedCarrierCount(int receivedCarrierCount) {
		this.receivedCarrierCount = receivedCarrierCount;
	}
}
