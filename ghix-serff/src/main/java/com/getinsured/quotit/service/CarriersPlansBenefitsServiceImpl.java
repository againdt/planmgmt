package com.getinsured.quotit.service;

import static com.getinsured.quotit.util.QuotitConstants.INDEX_ZERO;
import static com.getinsured.quotit.util.QuotitConstants.PREFIX_HIOS_ISSUER_ID;
import static com.serff.util.SerffConstants.DEFAULT_USER_ID;
import static com.serff.util.SerffConstants.NO;
import static com.serff.util.SerffConstants.NO_ABBR;
import static com.serff.util.SerffConstants.YES;
import static com.serff.util.SerffConstants.YES_ABBR;
import generated.ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan;
import generated.ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit;
import generated.ArrayOfGetCarriersPlansBenefitsResponseItem;
import generated.GetCarriersPlansBenefitsResponseCarrier;
import generated.GetCarriersPlansBenefitsResponseCarrierPlan;
import generated.GetCarriersPlansBenefitsResponseCarrierPlanBenefit;
import generated.GetCarriersPlansBenefitsResponseItem;
import generated.InsuranceType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.xml.bind.JAXBElement;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.ExtWSCallLogs;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerExt;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.model.PlanMedicare;
import com.getinsured.hix.model.PlanMedicareBenefit;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration.PlanmgmtConfigurationEnum;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.quotit.util.PlansBenefitsRequestVO;
import com.getinsured.quotit.util.QuotitConstants;
import com.getinsured.quotit.util.QuotitConstants.ResponseItemEnum;
import com.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse;
import com.serff.repository.templates.IExtWSCallLogsRepository;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.repository.templates.ITenantRepository;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * Class used to populates Medicare plans data from DTOs and persist into Database.
 * 
 * @author Bhavin Parmar
 * @since February 09, 2015
 */
@Service("carriersPlansBenefitsService")
public class CarriersPlansBenefitsServiceImpl implements CarriersPlansBenefitsService {

	private static final Logger LOGGER = Logger.getLogger(CarriersPlansBenefitsServiceImpl.class);

	private static final int FULL_NAME_MAX_LEN = 4000;

	private static final String MSG_TRANSACTION_COMMIT = "Transaction Commit has been Commited";
	private static final String MSG_TRANSACTION_ROLLBACK = "Transaction Commit has been Rollback";
	private static final String EMSG_COMMIT_TRANSACTION = "Error occured while commiting Transaction ";
	private static final String EMSG_PERSISTING_CARRIERS = "Error occured while persisting Carriers & Plan Data ";
	private static final String EMSG_PERSISTING_PLANS = "Error occured while persisting Plans Benefits Data ";
	private static final String EMSG_PARAM_EMPTY = "Input Parameter Map is empty ";
	private static final String EMSG_CARRIER_LIST_EMPTY = "Carrier List is empty.";
	private static final String EMSG_CONFIG_BENEFITS_EMPTY = "List of Medicare Benefits is empty in configurations.";

	private Map<String,String> medicareBenefitMap;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private SerffUtils serffUtils;
	@Autowired private IIssuersRepository iIssuerRepository;
	@Autowired private ISerffPlanRepository iPlanRepository;
	@Autowired private IExtWSCallLogsRepository trackingRepository;
	@Autowired private ITenantRepository iTenantRepository;


	/**
	 * Method is used to make a total of counts of Quotit Plans Statistics.
	 */
	@Override
	public String getTotalCountsOfQTStats(Integer parentTrackingID) {

		LOGGER.info("getTotalCountsOfQTStats() Start with Parent Tracking ID: " + parentTrackingID);
		String qtStatsFormat = null;

		try {
			if (null == parentTrackingID) {
				return qtStatsFormat;
			}

			List<ExtWSCallLogs> trackingRecordsList = trackingRepository.findByParentReqIdOrId(parentTrackingID, parentTrackingID);
			QuotitPlanStatistics qtStats = new QuotitPlanStatistics();
			QuotitPlanStatistics finalQTStats = new QuotitPlanStatistics();

			if (!CollectionUtils.isEmpty(trackingRecordsList)) {

				for (ExtWSCallLogs trackingRecord : trackingRecordsList) {

					if (null == trackingRecord || StringUtils.isBlank(trackingRecord.getRemarks())) {
						continue;
					}
					qtStats.setCountersFromStatsFormat(trackingRecord.getRemarks());
					finalQTStats.setSucceedPlanCount(finalQTStats.getSucceedPlanCount() + qtStats.getSucceedPlanCount());
					finalQTStats.setFailedPlanCount(finalQTStats.getFailedPlanCount() + qtStats.getFailedPlanCount());
					finalQTStats.setDeletedPlanCount(finalQTStats.getDeletedPlanCount() + qtStats.getDeletedPlanCount());
					finalQTStats.setSkippedCarrierCount(finalQTStats.getSkippedCarrierCount() + qtStats.getSkippedCarrierCount());
					finalQTStats.setReceivedPlanCount(finalQTStats.getReceivedPlanCount() + qtStats.getReceivedPlanCount());
					finalQTStats.setReceivedCarrierCount(finalQTStats.getReceivedCarrierCount() + qtStats.getReceivedCarrierCount());
					qtStats.reset();
				}
				qtStatsFormat = finalQTStats.getQuotitStatsForm();
			}
			else {
				LOGGER.info("There is no tracking data found for Parent Tracking ID: " + parentTrackingID);
			}
		}
		finally {
			LOGGER.info("getTotalCountsOfQTStats() End with Total Quotit Stats Format: " + qtStatsFormat);
		}
		return qtStatsFormat;
	}

	/**
	 * Method is used populate and persist Response of Carriers Plans Benefits API in database.
	 */
	@Override
	public boolean populateAndPersistResponse(GetCarriersPlansBenefitsResponse wsResponse, PlansBenefitsRequestVO requestVO,
			QuotitPlanStatistics qtStats, ExtWSCallLogs trackingRecord) throws GIException {

		LOGGER.debug("populateAndPersistResponse() Start with wsResponse is null: " + (null == wsResponse));
		boolean savedStatus = false;

		if (null == wsResponse || null == requestVO) {
			LOGGER.info("populateAndPersistResponse() End");
			return savedStatus;
		}
		EntityManager entityManager = null;

		try {

			generated.GetCarriersPlansBenefitsResponse carriersResult = wsResponse.getGetCarriersPlansBenefitsResult().getValue();
			LOGGER.debug("<GetCarriersPlansBenefitsResult> is null: " + (null == carriersResult));

			if (null != carriersResult) {
				entityManager = entityManagerFactory.createEntityManager(); // Get DB connection using Entity Manager Factory
				entityManager.getTransaction().begin(); // Begin Transaction

				trackingRecord.setExternalReqId(carriersResult.getRequestId());
				trackingRecord.setExternalProdId(carriersResult.getPodId());
				trackingRecord.setExternalExecTime(carriersResult.getTotalMilliseconds());
				List<Tenant> defaultTenantList = new ArrayList<Tenant>();
				defaultTenantList.add(iTenantRepository.findByCode(SerffConstants.TENANT_GINS));
				LOGGER.debug("<Carriers> is null: " + (null == carriersResult.getCarriers()));

				List<Issuer> savedIssuerList = processCarriersList(carriersResult.getCarriers().getValue().getGetCarriersPlansBenefitsResponseCarrier(),
						requestVO, qtStats, entityManager, defaultTenantList);
				if (!CollectionUtils.isEmpty(savedIssuerList)) {
					savedStatus = true;
				}
				else {
					requestVO.setErrorMessages("Failed to save Carriers and Plans Data into Database.");
				}
			}
		}
		catch (Exception ex) {
			savedStatus = false;
			requestVO.setErrorMessages(EMSG_PERSISTING_CARRIERS + ex.getMessage());
			LOGGER.error(EMSG_PERSISTING_CARRIERS + ex.getMessage(), ex);
		}
		finally {

			savedStatus = getTransactionCommit(savedStatus, entityManager, requestVO, qtStats);
			updateTrackingRecord(savedStatus, trackingRecord, requestVO.getErrorMessages());

			if (null != entityManager && entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			LOGGER.info("populateAndPersistResponse() End");
		}
		return savedStatus;
	}

	/**
	 * Method is used to process Carrier List data.
	 */
	private List<Issuer> processCarriersList(List<GetCarriersPlansBenefitsResponseCarrier> carriersList, PlansBenefitsRequestVO requestVO,
			QuotitPlanStatistics qtStats, EntityManager entityManager, List<Tenant> defaultTenantList) {

		LOGGER.info("processCarriersList() Start");
		List<Issuer> savedIssuerList = null;
		boolean carrierLoadFailed = true;
		int totalPlansCount = 0;

		try {
			if (CollectionUtils.isEmpty(carriersList)) {
				requestVO.setErrorMessages(EMSG_CARRIER_LIST_EMPTY);
				LOGGER.error(EMSG_CARRIER_LIST_EMPTY);
				return savedIssuerList;
			}
			medicareBenefitMap = serffUtils.getPropertyList(PlanmgmtConfigurationEnum.MEDICARE_BENEFITS_LIST);

			if (CollectionUtils.isEmpty(medicareBenefitMap)) {
				requestVO.setErrorMessages(EMSG_CONFIG_BENEFITS_EMPTY);
				LOGGER.error(EMSG_CONFIG_BENEFITS_EMPTY);
				return savedIssuerList;
			}

			Issuer issuer = null;
			List<Plan> existingPlanList = null;
			savedIssuerList = new ArrayList<Issuer>();
			carrierLoadFailed = false;

			for (GetCarriersPlansBenefitsResponseCarrier carrier : carriersList) {

				if (null == carrier) {
					continue;
				}

				if (!carrierLoadFailed) {
					// Get existing Issuer from Database.
					issuer = iIssuerRepository.getIssuerByHiosID(PREFIX_HIOS_ISSUER_ID + carrier.getCarrierId());

					if (null == issuer) {
						issuer = new Issuer();
					}
					else {
						LOGGER.debug("Existing Issuer HIOS-ID: " + issuer.getHiosIssuerId());
						// Get existing Plan from Database.
						existingPlanList = iPlanRepository.findByIssuer_IdAndIsDeletedAndApplicableYear(issuer.getId(), Plan.IS_DELETED.N.name(), requestVO.getApplicableYear());
					}
					carrierLoadFailed = saveCarriersPlansBenefitsData(carrier, requestVO, issuer, savedIssuerList, existingPlanList, qtStats, entityManager, defaultTenantList);
				}

				if (carrierLoadFailed) {
					LOGGER.debug("Failed to load plans for : " + carrier.getCarrierId());

					if (null != carrier.getPlans()
							&& null != carrier.getPlans().getValue()
							&& !CollectionUtils.isEmpty(carrier.getPlans().getValue().getGetCarriersPlansBenefitsResponseCarrierPlan())) {
						totalPlansCount += carrier.getPlans().getValue().getGetCarriersPlansBenefitsResponseCarrierPlan().size();
					}
				}
				else {
					totalPlansCount += qtStats.getSucceedPlanCount();
				}
			}
		}
		catch (Exception ex) {
			carrierLoadFailed = true;
			requestVO.setErrorMessages(EMSG_PERSISTING_CARRIERS + ex.getMessage());
			LOGGER.error(EMSG_PERSISTING_CARRIERS + ex.getMessage(), ex);
		}
		finally {

			if (0 < totalPlansCount) {

				if (carrierLoadFailed) {
					qtStats.setSucceedPlanCount(0);
					qtStats.setFailedPlanCount(totalPlansCount);
					savedIssuerList = null; // Reset savedIssuerList so that these are not committed
					LOGGER.error("Total Failed Plans Count: " + totalPlansCount);
				}
				else {
					qtStats.setSucceedPlanCount(totalPlansCount);
					qtStats.setFailedPlanCount(0);
					LOGGER.debug("Total Succeed Plans Count: " + totalPlansCount);
				}
			}
			else {
				qtStats.setSucceedPlanCount(0);
				qtStats.setFailedPlanCount(0);
				savedIssuerList = null; // Reset savedIssuerList so that these are not committed
				LOGGER.error("Carriers & Plans data are invalid/empty.");
			}
			LOGGER.info("processCarriersList() End");
		}
		return savedIssuerList;
	}

	/**
	 * Method is used to save Carriers and Plans data in database.
	 */
	private boolean saveCarriersPlansBenefitsData(GetCarriersPlansBenefitsResponseCarrier carrier, PlansBenefitsRequestVO requestVO, Issuer issuer,
			List<Issuer> savedIssuerList, List<Plan> existingPlanList, QuotitPlanStatistics qtStats, EntityManager entityManager, List<Tenant> defaultTenantList) {

		LOGGER.info("saveCarriersPlansBenefitsData() Start");
		boolean anyPlanFailed = false;

		try {
			if (null == carrier) {
				return anyPlanFailed;
			}

			List<String> existingPlansNotUpdated = null;
			Issuer savedIssuer = saveIssuer(carrier, issuer, entityManager);
			if (null == savedIssuer) {
				return anyPlanFailed;
			}

			PlanInsuranceType planInsuranceType = getPlanInsuranceType(carrier.getInsuranceType());
			if (null != planInsuranceType) {
				existingPlansNotUpdated = getListOfExistingPlanId(existingPlanList);
				LOGGER.debug("<Carrier.Plans> is null: " + (null == carrier.getPlans()));
				anyPlanFailed = processPlanList(carrier.getPlans(), savedIssuer, planInsuranceType, existingPlanList, existingPlansNotUpdated,
						requestVO, qtStats, entityManager, defaultTenantList);
				LOGGER.debug("</Carrier.Plans>");
			}

			if (!anyPlanFailed) {
				savedIssuerList.add(savedIssuer);
				deleteOldPlans(existingPlansNotUpdated, savedIssuer.getId(), requestVO.getApplicableYear(), qtStats, entityManager);

				if (!CollectionUtils.isEmpty(existingPlansNotUpdated)) {
					existingPlansNotUpdated.clear();
					existingPlansNotUpdated = null;
				}
			} 
		}
		catch (Exception ex) {
			requestVO.setErrorMessages(EMSG_PERSISTING_CARRIERS + ex.getMessage());
			LOGGER.error(EMSG_PERSISTING_CARRIERS + ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("saveCarriersPlansBenefitsData() End");
		}
		return anyPlanFailed;
	}
	/**
	 * Method is used to save Carrier data in Issuer & IssuerExt tables.
	 */
	private Issuer saveIssuer(GetCarriersPlansBenefitsResponseCarrier carrier, Issuer issuer, EntityManager entityManager) {

		LOGGER.debug("Persisting Issuer/IssuerExt Object Start : " + carrier.getCarrierId());
		Issuer savedIssuer = null;

		try {
			issuer.setHiosIssuerId(PREFIX_HIOS_ISSUER_ID + carrier.getCarrierId());
			issuer.setName(carrier.getName());
			issuer.setCompanyLegalName(carrier.getName());
			issuer.setLogoURL(null != carrier.getLogoFileMediumTransparent() ? carrier.getLogoFileMediumTransparent().getValue() : null);
			issuer.setShortName(carrier.getName());
			issuer.setMarketingName(carrier.getName());
			issuer.setState(carrier.getState());
			issuer.setStateOfDomicile(carrier.getState());
			issuer.setCompanyState(carrier.getState());
			issuer.setD2C(NO_ABBR);
			issuer.setLastUpdatedBy(DEFAULT_USER_ID);
			issuer.setSendEnrollmentEndDateFlag("true"); // Default Value of send enrollment end date flag
			savedIssuer = (Issuer) mergeEntityManager(entityManager, issuer);

			if (null != savedIssuer) {

				IssuerExt issuerExt = savedIssuer.getIssuerExt();
				if (null == issuerExt) {
					LOGGER.debug("Creating new IssuerExt record.");
					issuerExt = new IssuerExt();
				}
				issuerExt.setCarrierId(carrier.getCarrierId());
				issuerExt.setInsuranceType(carrier.getInsuranceType().value());
				issuerExt.setLogoLarge(null != carrier.getLogoFileLarge() ? carrier.getLogoFileLarge().getValue() : null);
				issuerExt.setLogoMedium(null != carrier.getLogoFileMedium() ? carrier.getLogoFileMedium().getValue() : null);
				issuerExt.setLogoSmall(null != carrier.getLogoFileSmall() ? carrier.getLogoFileSmall().getValue() : null);
				issuerExt.setDataPartnerName(IssuerExt.DataPartnerName.QUOTIT.name());
				issuerExt.setVersion(carrier.getVersion());
				issuerExt.setIssuer(savedIssuer);
				issuerExt.setCreatedBy(DEFAULT_USER_ID);
				issuerExt.setLastUpdatedBy(DEFAULT_USER_ID);
				issuerExt.setLastUpdateTimestamp(new Date()); // For Medicare recently uploaded (within 24hrs) logic.
				issuerExt = (IssuerExt) mergeEntityManager(entityManager, issuerExt);

				if (null == issuerExt) {
					LOGGER.error("Failed to persist IssuerExt object.");
				}
			}
			else {
				LOGGER.error("Failed to persist Issuer object.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting Issuer/IssuerExt object", ex);
		}
		finally {
			LOGGER.debug("Persisting Issuer/IssuerExt Object Done");
		}
		return savedIssuer;
	}

	/**
	 * Method is used to process Plan List data.
	 */
	private boolean processPlanList(JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlan> quotitPlanList,
			Issuer savedIssuer, PlanInsuranceType planInsuranceType, List<Plan> existingPlanList, List<String> existingPlansNotUpdated,
			PlansBenefitsRequestVO requestVO, QuotitPlanStatistics qtStats, EntityManager entityManager, List<Tenant> defaultTenantList) {

		LOGGER.debug("processPlanList() Start");
		boolean anyPlanFailed = false;
		int totalPlansCount = 0;

		try {
			if (null == quotitPlanList
					|| null == savedIssuer
					|| null == requestVO
					|| null == quotitPlanList.getValue()
					|| CollectionUtils.isEmpty(quotitPlanList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlan())) {
				LOGGER.error(EMSG_PARAM_EMPTY + "for Process Plan List.");
				anyPlanFailed = true;
				return anyPlanFailed;
			}

			Plan plan = null;
			boolean benefitsToBeCleaned = false;
			totalPlansCount = quotitPlanList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlan().size();
			for (GetCarriersPlansBenefitsResponseCarrierPlan quotitPlan : quotitPlanList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlan()) {

				if (null == quotitPlan) {
					continue;
				}
				// Get existing Plan from Database.
				plan = getExistingPlan(existingPlanList, quotitPlan.getPlanId());

				if (null != plan) {
					benefitsToBeCleaned = true;
					LOGGER.debug("Existing Plan to be updated for IssuerPlanNumber: " + plan.getIssuerPlanNumber());
				}
				else {
					benefitsToBeCleaned = false;
					plan = new Plan();
				}
				plan = savePlansBenefitsData(quotitPlan, requestVO, plan, savedIssuer, planInsuranceType, benefitsToBeCleaned, entityManager);

				if (null != plan) {

					if (!CollectionUtils.isEmpty(existingPlansNotUpdated)) {
						// Removing Quotit Plan ID from List
						boolean deletedFlag = existingPlansNotUpdated.remove(plan.getIssuerPlanNumber());
						LOGGER.debug("Removed Plan ID["+ plan.getIssuerPlanNumber() +"] from List completed with status: " + deletedFlag);
					}
					
					//If there is already tenant attached (in case of reload), don't attach default tenant again
					//HIX-78519
					if(CollectionUtils.isEmpty(plan.getTenantPlan() )) {
						serffUtils.saveTenantPlan(plan, defaultTenantList, entityManager);
					} 
				}
				else {
					anyPlanFailed = true;
					break;
				}
			}
		}
		catch (Exception ex) {
			anyPlanFailed = true;
			LOGGER.error("Error occured while persisting Plan Data objects.", ex);
		}
		finally {

			if (anyPlanFailed) {
				qtStats.setSucceedPlanCount(0);
				qtStats.setFailedPlanCount(totalPlansCount);
				LOGGER.error("Failed Plans Count: " + totalPlansCount);
			}
			else {
				qtStats.setSucceedPlanCount(totalPlansCount);
				qtStats.setFailedPlanCount(0);
				LOGGER.debug("Succeed Plans Count: " + totalPlansCount);
			}
			LOGGER.debug("processPlanList() End with isAnyPlanFailed: " + anyPlanFailed);
		}
		return anyPlanFailed;
	}

	
	/**
	 * Method is used to save Plans and Benefits data in database.
	 */
	private Plan savePlansBenefitsData(GetCarriersPlansBenefitsResponseCarrierPlan quotitPlan, PlansBenefitsRequestVO requestVO, Plan plan,
			Issuer savedIssuer, PlanInsuranceType planInsuranceType, boolean isBenefitsDeleted, EntityManager entityManager) {

		LOGGER.info("savePlansBenefitsData() Start");
		Plan savedPlan = null;

		try {
			PlanMedicare planMedicare = null == plan.getPlanMedicare() ? new PlanMedicare() : plan.getPlanMedicare();
			savedPlan = savePlan(quotitPlan, savedIssuer, plan, planInsuranceType, requestVO, entityManager);

			if (null != savedPlan) {
				LOGGER.debug("<Carrier.Plan.PlanData> is null: " + (null == quotitPlan.getPlanData()));
				planMedicare.setPlan(savedPlan);
				planMedicare = savePlanMedicare(quotitPlan, planMedicare, quotitPlan.getPlanData(), entityManager);
				LOGGER.debug("</Carrier.Plan.PlanData>");

				if (null != planMedicare) {
					LOGGER.debug("<Carrier.Plan.Benefits> is null: " + (null == quotitPlan.getBenefits()));
					if(!processPlanBenefitList(isBenefitsDeleted, planMedicare, quotitPlan.getBenefits(), entityManager)) {
						savedPlan = null;
					}
					LOGGER.debug("</Carrier.Plan.Benefits>");
				}
			}
		}
		catch (Exception ex) {
			requestVO.setErrorMessages(EMSG_PERSISTING_PLANS + ex.getMessage());
			LOGGER.error(EMSG_PERSISTING_PLANS + ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("savePlansBenefitsData() End");
		}
		return savedPlan;
	}

	/**
	 * Method is used to return Plan from Existing Plan List which is match with quotit Plan-ID.
	 */
	private Plan getExistingPlan(List<Plan> existingPlanList, String quotitPlanID) {

		Plan existingPlan = null;

		if (CollectionUtils.isEmpty(existingPlanList)) {
			return existingPlan;
		}

		for (Plan plan : existingPlanList) {

			if (plan != null &&
					plan.getIssuerPlanNumber().equalsIgnoreCase(quotitPlanID)) {
				existingPlan = plan;
				break;
			}
		}
		return existingPlan;
	}

	/**
	 * Method is used to save Plan table.
	 */
	private Plan savePlan(GetCarriersPlansBenefitsResponseCarrierPlan quotitPlan, Issuer savedIssuer,
			Plan plan, PlanInsuranceType planInsuranceType, PlansBenefitsRequestVO requestVO, EntityManager entityManager) {

		LOGGER.debug("Persisting Plan Object Start : " + quotitPlan.getPlanId());
		Plan savedPlan = null;

		try {
			plan.setStartDate(requestVO.getStartDate()); // Derive from EffectiveDate
			plan.setEndDate(requestVO.getEndDate()); // Derive from EffectiveDate
			plan.setApplicableYear(requestVO.getApplicableYear()); // Derive from EffectiveDate
			plan.setInsuranceType(planInsuranceType.name());
			plan.setIssuerPlanNumber(quotitPlan.getPlanId());
			plan.setMarket(Plan.PlanMarket.INDIVIDUAL.name());
			plan.setName(quotitPlan.getName());
			plan.setNetworkType(quotitPlan.getPlanType());
			plan.setStatus(Plan.PlanStatus.CERTIFIED.name());
			plan.setIssuer(savedIssuer);
			plan.setBrochure(null != quotitPlan.getBenefitsLink() ? quotitPlan.getBenefitsLink().getValue() : null);
			plan.setHsa(Plan.HSA.NO.name());
			plan.setIssuerVerificationStatus(Plan.IssuerVerificationStatus.VERIFIED.name());
			plan.setHiosProductId(quotitPlan.getPlanId());
			plan.setState(quotitPlan.getState());
			plan.setIsDeleted(Plan.IS_DELETED.N.name());
			plan.setExchangeType(Plan.EXCHANGE_TYPE.OFF.name());
			plan.setIsPUF(Plan.IS_PUF.N);
			plan.setCreatedBy(DEFAULT_USER_ID);
			plan.setLastUpdatedBy(DEFAULT_USER_ID);
			savedPlan = (Plan) mergeEntityManager(entityManager, plan);
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting Plan object", ex);
		}
		finally {

			if (null == savedPlan) {
				LOGGER.error("Failed to persist Plan object.");
			}
			LOGGER.debug("Persisting Plan Object Done");
		}
		return savedPlan;
	}

	/**
	 * Method is used to save PlanMedicare table.
	 */
	private PlanMedicare savePlanMedicare(GetCarriersPlansBenefitsResponseCarrierPlan quotitPlan, PlanMedicare planMedicare,
			JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseItem> planDataList, EntityManager entityManager) {

		LOGGER.debug("Persisting PlanMedicare Object Start");
		PlanMedicare savedPlanMedicare = null;

		try {

			if (null != planDataList
					&& null != planDataList.getValue()
					&& !CollectionUtils.isEmpty(planDataList.getValue().getGetCarriersPlansBenefitsResponseItem())) {
				// Set Response Items in CMS Contract ID, CMS Plan ID, CMS Include RX, CMS Plan Name, CMS Segment ID, Is SNP OR Is Medicaid
				setMedicareResponseItem(planDataList.getValue().getGetCarriersPlansBenefitsResponseItem(), planMedicare);
			}
			planMedicare.setBenefitURL(null != quotitPlan.getBenefitsLink() ? quotitPlan.getBenefitsLink().getValue() : null);
			planMedicare.setCreatedBy(DEFAULT_USER_ID);
			planMedicare.setLastUpdatedBy(DEFAULT_USER_ID);
			planMedicare.setDataSource(QuotitConstants.QUOTIT);
			savedPlanMedicare = (PlanMedicare) mergeEntityManager(entityManager, planMedicare);
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting PlanMedicare object", ex);
		}
		finally {

			if (null == savedPlanMedicare) {
				LOGGER.error("Failed to persist PlanMedicare object.");
			}
			LOGGER.debug("Persisting PlanMedicare Object Done");
		}
		return savedPlanMedicare;
	}

	/**
	 * Method is used to set Response Items in CMS Contract ID, CMS Plan ID, CMS Include RX,
	 * CMS Plan Name, CMS Segment ID, Is SNP OR Is Medicaid.
	 */
	private void setMedicareResponseItem(List<GetCarriersPlansBenefitsResponseItem> responseItemList, PlanMedicare planMedicare) {

		LOGGER.debug("Set Medicare Response Items size: " + responseItemList.size());
		for (GetCarriersPlansBenefitsResponseItem item : responseItemList) {

			if (null == item || null == item.getValue()) {
				continue;
			}

			if (ResponseItemEnum.PLAN_DATA_CONTRACT_ID.getValue().equalsIgnoreCase(item.getName())) {
				planMedicare.setCmsContractId(item.getValue().getValue());
			}
			else if (ResponseItemEnum.PLAN_DATA_CMS_PLAN_ID.getValue().equalsIgnoreCase(item.getName())) {
				planMedicare.setCmsPlanId(item.getValue().getValue());
			}
			else if (ResponseItemEnum.PLAN_DATA_INCLUDE_RX.getValue().equalsIgnoreCase(item.getName())) {
				planMedicare.setCmsIncludeRx(item.getValue().getValue());
			}
			else if (ResponseItemEnum.PLAN_DATA_CMS_PLAN_NAME.getValue().equalsIgnoreCase(item.getName())) {
				planMedicare.setCmsPlanName(item.getValue().getValue());
			}
			else if (ResponseItemEnum.PLAN_DATA_SEGMENT_ID.getValue().equalsIgnoreCase(item.getName())) {
				planMedicare.setCmsSegmentId(item.getValue().getValue());
			}
			else if (ResponseItemEnum.PLAN_DATA_IS_SNP.getValue().equalsIgnoreCase(item.getName())) {
				planMedicare.setCmsIsSnp(getBooleanAbbr(item.getValue().getValue()));
			}
			else if (ResponseItemEnum.PLAN_DATA_IS_MEDICAID.getValue().equalsIgnoreCase(item.getName())) {
				planMedicare.setIsMedicaid(getBooleanAbbr(item.getValue().getValue()));
			}
			else if (ResponseItemEnum.PLAN_DATA_NETWORK_ONLY.getValue().equalsIgnoreCase(item.getName())) {
				planMedicare.setSelectNetowrkOnly(item.getValue().getValue());
			}
			else if (ResponseItemEnum.PLAN_DATA_RATE_TYPE.getValue().equalsIgnoreCase(item.getName())) {
				planMedicare.setRateType(item.getValue().getValue());
			}
		}
	}

	/**
	 * Method is used to identified Y/N from values YES/NO.
	 */
	private String getBooleanAbbr(String value) {
		String abbr = null;

		if (YES.equalsIgnoreCase(value)) {
			abbr = YES_ABBR;
		}
		else if (NO.equalsIgnoreCase(value)) {
			abbr = NO_ABBR;
		}
		return abbr;
	}

	/**
	 * Method is used to process List of Plan Medicare Benefits data.
	 */
	private boolean processPlanBenefitList(boolean isBenefitsDeleted, PlanMedicare savedPlanMedicare,
			JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseCarrierPlanBenefit> planBenefitList,
			EntityManager entityManager) {

		LOGGER.debug("processPlanBenefitList() Start");
		boolean isSavedBenefits = false;

		try {
			if (null == planBenefitList
					|| null == planBenefitList.getValue()
					|| CollectionUtils.isEmpty(planBenefitList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlanBenefit())) {
				LOGGER.error(EMSG_PARAM_EMPTY + "for Process Plan Benefit List.");
				return isSavedBenefits;
			}
			// Delete existing medicare benefits.
			deleteMedicareBenefits(isBenefitsDeleted, savedPlanMedicare, entityManager);

			PlanMedicareBenefit savedMedicareBenefit = null;
			List<PlanMedicareBenefit> planMedicareBenefitList = new ArrayList<PlanMedicareBenefit>();

			for (GetCarriersPlansBenefitsResponseCarrierPlanBenefit planBenefit : planBenefitList.getValue().getGetCarriersPlansBenefitsResponseCarrierPlanBenefit()) {

				if (null == planBenefit) {
					continue;
				}
				savedMedicareBenefit = savePlanMedicareBenefit(savedPlanMedicare, planBenefit, entityManager);

				if (null != savedMedicareBenefit) {
					planMedicareBenefitList.add(savedMedicareBenefit);
				}
			}

			if (!CollectionUtils.isEmpty(planMedicareBenefitList)) {
				isSavedBenefits = true;
			}
			else {
				LOGGER.error("Failed to persist all PlanMedicareBenefits for Plan ID: " + savedPlanMedicare.getPlan().getIssuerPlanNumber());
			}
		}
		finally {
			LOGGER.debug("processPlanBenefitList() End with isSavedBenefits: " + isSavedBenefits);
		}
		return isSavedBenefits;
	}

	/**
	 * Method is used to delete existing Medicare Benefits from Database.
	 */
	private void deleteMedicareBenefits(boolean isBenefitsDeleted, PlanMedicare savedPlanMedicare, EntityManager entityManager) {

		if (isBenefitsDeleted) {
			Query deleteQuery = entityManager.createQuery("delete from PlanMedicareBenefit benefit where benefit.planMedicare.id = :planMedicareId");
			deleteQuery.setParameter("planMedicareId", savedPlanMedicare.getId());
			int deleteCount = deleteQuery.executeUpdate();
			LOGGER.debug("Deleted existing Benefits["+ deleteCount +"] for PlanID: " + savedPlanMedicare.getPlan().getIssuerPlanNumber());
		}
	}

	/**
	 * Method is used to save PlanMedicareBenefit table.
	 */
	private PlanMedicareBenefit savePlanMedicareBenefit(PlanMedicare savedPlanMedicare,
			GetCarriersPlansBenefitsResponseCarrierPlanBenefit planBenefit,
			EntityManager entityManager) {

		LOGGER.debug("Persisting PlanMedicareBenefit Object Start");
		PlanMedicareBenefit savedMedicareBenefit = null;

		try {
			if (null == planBenefit
					|| CollectionUtils.isEmpty(medicareBenefitMap)
					|| StringUtils.isEmpty(planBenefit.getEnum())) {
				LOGGER.error(EMSG_PARAM_EMPTY + "for PlanMedicareBenefit.");
				return savedMedicareBenefit;
			}

			String benefitKey = medicareBenefitMap.get(planBenefit.getEnum().toLowerCase());
			if (StringUtils.isBlank(benefitKey)) {
				LOGGER.error("Skipping Plan Medicare Benefit: No mapping found in <Enum>" + planBenefit.getEnum() + "</Enum>");
				return savedMedicareBenefit;
			}

			PlanMedicareBenefit planMedicareBenefit = new PlanMedicareBenefit();
			planMedicareBenefit.setPlanMedicare(savedPlanMedicare);
			planMedicareBenefit.setCategory(benefitKey);
			planMedicareBenefit.setCategoryEnum(planBenefit.getEnum());
			planMedicareBenefit.setCategoryName(null != planBenefit.getCategory() ? planBenefit.getCategory().getValue() : null);
			planMedicareBenefit.setSortOrder(planBenefit.getSortOrder());

			if (null != planBenefit.getFullValue()) {
				String fullValue = planBenefit.getFullValue().getValue();

				if (FULL_NAME_MAX_LEN < fullValue.length()) {
					planMedicareBenefit.setFullName(fullValue.substring(INDEX_ZERO, FULL_NAME_MAX_LEN));

					int remainingLen = fullValue.length() - FULL_NAME_MAX_LEN;
					planMedicareBenefit.setFullValueExt(fullValue.substring
							(FULL_NAME_MAX_LEN, remainingLen > FULL_NAME_MAX_LEN ? FULL_NAME_MAX_LEN * 2 : fullValue.length()));
				}
				else {
					planMedicareBenefit.setFullName(fullValue);
				}
			}
			planMedicareBenefit.setTinyValue(null != planBenefit.getTinyValue() ? planBenefit.getTinyValue().getValue() : null);
			planMedicareBenefit.setFlagValue(getFlagValue(planBenefit.getFlagValue()));
			planMedicareBenefit.setCopaymentAmount(null != planBenefit.getAmount() ? Integer.valueOf(planBenefit.getAmount().getValue().intValue()) : null);
			planMedicareBenefit.setCoinsuranceAmount(null != planBenefit.getCost() ? Integer.valueOf(planBenefit.getCost().getValue().intValue()) : null );

			if (null != planBenefit.getBenefitData()
					&& null != planBenefit.getBenefitData().getValue()) {
				// Set Response Items in Benefit Name, Short Name and Tiny Name.
				setBenefitsResponseItem(planBenefit.getBenefitData().getValue().getGetCarriersPlansBenefitsResponseItem(), planMedicareBenefit);
			}
			planMedicareBenefit.setCreatedBy(DEFAULT_USER_ID);
			planMedicareBenefit.setLastUpdatedBy(DEFAULT_USER_ID);
			savedMedicareBenefit = (PlanMedicareBenefit) mergeEntityManager(entityManager, planMedicareBenefit);
		}
		catch (Exception ex) {
			LOGGER.error("Error occured while persisting PlanMedicare object", ex);
		}
		finally {

			if (null == savedMedicareBenefit) {
				LOGGER.error("Failed to persist PlanMedicareBenefit object.");
			}
			LOGGER.debug("Persisting PlanMedicareBenefit Object Done");
		}
		return savedMedicareBenefit;
	}

	/**
	 * Method is used to set Response Items in Benefit Name, Short Name and Tiny Name.
	 */
	private void setBenefitsResponseItem(List<GetCarriersPlansBenefitsResponseItem> responseItemList, PlanMedicareBenefit planMedicareBenefit) {

		if (CollectionUtils.isEmpty(responseItemList)
				|| null == planMedicareBenefit) {
			return;
		}

		LOGGER.debug("Set Medicare Benefits Response Items size: " + responseItemList.size());
		for (GetCarriersPlansBenefitsResponseItem item : responseItemList) {

			if (null == item || null == item.getValue()) {
				continue;
			}

			if (ResponseItemEnum.BENEFIT_DATA_NAME.getValue().equalsIgnoreCase(item.getName())) {
				planMedicareBenefit.setName(item.getValue().getValue());
			}
			else if (ResponseItemEnum.BENEFIT_DATA_SHORT_NAME.getValue().equalsIgnoreCase(item.getName())) {
				planMedicareBenefit.setShortName(item.getValue().getValue());
			}
			else if (ResponseItemEnum.BENEFIT_DATA_TINY_NAME.getValue().equalsIgnoreCase(item.getName())) {
				planMedicareBenefit.setTinyName(item.getValue().getValue());
			}
		}
	}

	/**
	 * Method is used to get Flag Value for Benefit.
	 */
	private int getFlagValue(List<JAXBElement<List<String>>> flagValue) {
		int flag = -1;
		String value = null;

		if (!CollectionUtils.isEmpty(flagValue) && null != flagValue.get(0)
				&& null != flagValue.get(0).getValue()
				&& StringUtils.isNotBlank(flagValue.get(0).getValue().get(0))) {
			value = flagValue.get(0).getValue().get(0);
		}

		if (YES.equalsIgnoreCase(value)) {
			flag = 1;
		}
		else if (NO.equalsIgnoreCase(value)) {
			flag = 0;
		}
		/*else if ("NA".equalsIgnoreCase(value)) {
			flag = -1;
		}*/
		return flag;
	}

	/**
	 * Method is used to merge Table to Entity Manager.
	 */
	private Object mergeEntityManager(EntityManager entityManager, Object saveObject) {

		Object savedObject = null;

		if (null != saveObject && null != entityManager && entityManager.isOpen()) {
			savedObject = entityManager.merge(saveObject);
			LOGGER.debug(saveObject.getClass() + " bean has been Persisted Successfully.");
		}
		return savedObject;
	}

	/**
	 * Method is used to Commit/Rollback transaction depends on Status argument.
	 */
	private boolean getTransactionCommit(boolean status, EntityManager entityManager, PlansBenefitsRequestVO requestVO,
			QuotitPlanStatistics qtStats) throws GIException {

		LOGGER.debug("getTransactionCommit() Start");
		boolean commitStatus = status;

		try {

			if (null != entityManager && entityManager.isOpen()) {

				if (status) {
					entityManager.getTransaction().commit();
					LOGGER.info(MSG_TRANSACTION_COMMIT);
				}
				else {
					entityManager.getTransaction().rollback();
					requestVO.setErrorMessages(MSG_TRANSACTION_ROLLBACK);
					LOGGER.error(MSG_TRANSACTION_ROLLBACK);
				}
			}
		}
		catch (Exception ex) {
			qtStats.setSucceedPlanCount(0);
			qtStats.setFailedPlanCount(qtStats.getSucceedPlanCount() + qtStats.getFailedPlanCount());
			commitStatus = Boolean.FALSE;
			LOGGER.error(EMSG_COMMIT_TRANSACTION + commitStatus, ex);
			requestVO.setErrorMessages(EMSG_COMMIT_TRANSACTION + ex.getMessage());
		}
		finally {
			LOGGER.debug("getTransactionCommit() End");
		}
		return commitStatus;
	}

	/**
	 * Method is used to soft delete old plans.
	 */
	private void deleteOldPlans(List<String> existingPlansNotUpdated, int issuerId, int applicableYear, QuotitPlanStatistics qtStats, EntityManager entityManager) {

		Plan existingPlan = null;

		if (CollectionUtils.isEmpty(existingPlansNotUpdated)) {
			LOGGER.info("Old Plans are not available.");
			return;
		}
		LOGGER.info("Soft deleting old Plans for Issuer-ID: " + issuerId);

		for (String planId : existingPlansNotUpdated) {

			if (StringUtils.isBlank(planId)) {
				continue;
			}
			existingPlan = iPlanRepository.findByIssuer_IdAndIssuerPlanNumberAndIsDeletedAndApplicableYear(issuerId, planId,
					Plan.IS_DELETED.N.toString(), applicableYear);

			if (null != existingPlan) {
				qtStats.planDeleted();
				existingPlan.setIsDeleted(Plan.IS_DELETED.Y.toString());
				mergeEntityManager(entityManager, existingPlan);
			}
		}
	}

	/**
	 * Method is used to update tracking record.
	 */
	private void updateTrackingRecord(boolean savedStatus, ExtWSCallLogs trackingRecord, String errorMessage) {
	
		if (savedStatus) {

			if (StringUtils.isBlank(errorMessage)) {
				trackingRecord.setExceptionMessage(null);
			}
			else {
				trackingRecord.setExceptionMessage(errorMessage);
				LOGGER.warn(trackingRecord.getExceptionMessage());
			}
		}
		else {
			trackingRecord.setExceptionMessage(errorMessage);
			LOGGER.error("Failed to load Medicare Plan: " + errorMessage);
		}
	}

	/**
	 * Method is used to get Deleted Plan List.
	 */
	private List<String> getListOfExistingPlanId(List<Plan> existingPlanList) {

		List<String> existingPlansNotUpdated = null;

		if (!CollectionUtils.isEmpty(existingPlanList)) {
			existingPlansNotUpdated = new ArrayList<String>();

			for (Plan plan : existingPlanList) {

				if (plan != null) {
					existingPlansNotUpdated.add(plan.getIssuerPlanNumber());
				}
			}
		}
		else {
			LOGGER.warn("Existing Plans data are not available for existing Issuer.");
		}
		return existingPlansNotUpdated;
	}

	/**
	 * Method is used to get PlanInsuranceType Enum.
	 */
	private PlanInsuranceType getPlanInsuranceType(InsuranceType insuranceType) {

		LOGGER.debug("getPlanInsuranceType() Start");
		PlanInsuranceType planInsuranceType = null;

		if (InsuranceType.MEDICARE_ADVANTAGE.equals(insuranceType)) {
			planInsuranceType = Plan.PlanInsuranceType.MC_ADV;
		}
		else if (InsuranceType.MEDIGAP.equals(insuranceType)) {
			planInsuranceType = Plan.PlanInsuranceType.MC_SUP;
		}
		else if (InsuranceType.RX.equals(insuranceType)) {
			planInsuranceType = Plan.PlanInsuranceType.MC_RX;
		}
		else {
			LOGGER.error("Insurance Type is not found.");
		}
		LOGGER.debug("getPlanInsuranceType() End");
		return planInsuranceType;
	}

	@Override
	public String getQuotitRequestResponse(int id, boolean showRequestData) {

		LOGGER.debug("getQuotitRequestResponse() Start");
		String data = null;

		if (showRequestData) {
			data = trackingRepository.getQuotitRequest(id);
		}
		else {
			data = trackingRepository.getQuotitResponse(id);
		}
		LOGGER.debug("getQuotitRequestResponse() End");
		return data;
	}
}
