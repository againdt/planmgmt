package com.getinsured.quotit.service;

import com.getinsured.hix.model.ExtWSCallLogs;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.quotit.util.PlansBenefitsRequestVO;
import com.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse;

/**
 * Interface used to populates Medicare plans data from DTOs and persist into Database.
 * 
 * @author Bhavin Parmar
 * @since February 09, 2015
 */
public interface CarriersPlansBenefitsService {

	String getTotalCountsOfQTStats(Integer parentTrackingID);

	boolean populateAndPersistResponse(GetCarriersPlansBenefitsResponse wsResponse, PlansBenefitsRequestVO requestVO,
			QuotitPlanStatistics qtStats, ExtWSCallLogs trackingRecord) throws GIException;

	String getQuotitRequestResponse(int id, boolean showRequestData);
}
