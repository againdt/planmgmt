package com.getinsured.quotit.service;

import generated.ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty;
import generated.GetZipCodeInfoResponseZipCodeInfo;
import generated.GetZipCodeInfoResponseZipCodeInfoCounty;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.ZipCodesMedicare;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.quotit.util.GetZipCodeInfoRequestVO;
import com.quotit.services.actws.aca._2.GetZipCodeInfoResponse;

/**
 * ----------------------------------------------------------------------------
 * HIX-60437 : GetZipCodeInfo API Call and Response processing
 * ----------------------------------------------------------------------------
 * This class is an implementation of ZipCodesMedicareService. This class manages
 * all the response processing and persisting task. The deletion of old zipcodes
 * and insertion of new zipcodes is handled within single transaction to ensure
 * data consistency.
 * 
 * @author vardekar_s
 *
 */
@Service("zipCodesMedicareService")
public class ZipCodesMedicareServiceImpl implements ZipCodesMedicareService {

	private static final Logger LOGGER = Logger.getLogger(ZipCodesMedicareServiceImpl.class);
	private static final String MEDICARE_ZIPCODES_DELETE_QUERY = "Delete from ZipCodesMedicare zipCodesMedicare WHERE zipCodesMedicare.state = :stateCode";

	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	
	/**
	 * Reads data from web service response, creates ZipCodesMedicare entities and persist in database.
	 * The whole process of reading and persisting data is handled in single transaction hence there is not 
	 * any possibility of data deletion without data insertion. So there will not be any accidental
	 * zipcodes deletion for given state.
	 * 
	 */
	@Override
	public void populateAndPersistResponse(GetZipCodeInfoResponse zipCodeInfoResponse, GetZipCodeInfoRequestVO requestVO) throws GIException {

		LOGGER.info("populateAndPersistResponse() Start");
		EntityManager entityManager = null;
		generated.GetZipCodeInfoResponse response = zipCodeInfoResponse.getGetZipCodeInfoResult().getValue();

		if (null != response && null != response.getZipCodes() && !response.getZipCodes().isNil()) {
			try {
				
				List <GetZipCodeInfoResponseZipCodeInfo> stateZipCodesListFromWebService = response.getZipCodes().getValue().getGetZipCodeInfoResponseZipCodeInfo();
				//Create ZipCode list from response.
				List <ZipCodesMedicare> stateZipCodesListToPersist = createZipCodesListForState(requestVO, stateZipCodesListFromWebService);
				//Create and start transaction.
				entityManager = entityManagerFactory.createEntityManager(); // Get DB connection using Entity Manager Factory
				if(null != entityManager){
					entityManager.getTransaction().begin(); // Begin Transaction
					//Delete exisiting ZipCodes from table.
					deleteExistingZipCodesOfState(requestVO, entityManager);
					//Persist new zipcodes.
					boolean isPersistSuccessful = persistNewZipCodes(stateZipCodesListToPersist, entityManager);
					//Commit/rollback transaction
					completeTransaction(isPersistSuccessful, entityManager);
				}else{
					LOGGER.error("Error occurred while persisting medicare zipcodes. Entity manager is null !!");
				}

			} catch (Exception e) {

				LOGGER.error("Error occurred while persisting medicare zipcodes." + e.getMessage(), e);
				throw new GIException("Error occurred while persisting medicare zipcodes.");

			} finally {

				if (null != entityManager && entityManager.isOpen()) {
					entityManager.clear();
					entityManager.close();
					entityManager = null;
				}

			}
		} else {
			LOGGER.error("Web Service response is null or Zip code list in web service response is empty.");
		}

		LOGGER.info("populateAndPersistResponse() End");
	}

	/**
	 * Persists ZipCodesMedicare entities in database. Records will be committed in database when 
	 * the transaction committed associated with this Entity Manager.
	 * 
	 * @param stateZipCodesListToPersist - 
	 * @param entityManager - The Entity Manager instance.
	 * @return - Whether ZipCodesMedicare entities persisted successfully.
	 */
	private boolean persistNewZipCodes(List <ZipCodesMedicare> stateZipCodesListToPersist, EntityManager entityManager) {

		LOGGER.info("persistNewZipCodes() Start");
		boolean isNewZipCodeListPeristed = false;

		try {

			if(!CollectionUtils.isEmpty(stateZipCodesListToPersist)){
				for(ZipCodesMedicare zipCode : stateZipCodesListToPersist){
					entityManager.persist(zipCode);
				}
				isNewZipCodeListPeristed = true;
			}

		} catch (Exception e) {
			LOGGER.error("Error occurred while merging new zipcodes.", e);
		}
		LOGGER.info("persistNewZipCodes() End");

		return isNewZipCodeListPeristed;
	}

	/**
	 * Create ZipCodesMedicare entity list by reading data from Zip Codes present in web service response.
	 * 
	 * @param requestVO - The request VO.
	 * @param stateZipCodesList - Zip code list from web service response
	 * @return - List of Zipcode entities created from web service zip codes.
	 */
	private List <ZipCodesMedicare> createZipCodesListForState(GetZipCodeInfoRequestVO requestVO, List <GetZipCodeInfoResponseZipCodeInfo> stateZipCodesList) {

		LOGGER.info("createZipCodesListForState Start()");

		List <ZipCodesMedicare> zipCodesList = new ArrayList <ZipCodesMedicare>();

		for (GetZipCodeInfoResponseZipCodeInfo zipCodeFromWebService: stateZipCodesList) {

			ArrayOfGetZipCodeInfoResponseZipCodeInfoCounty counties = zipCodeFromWebService.getCounties();
			List <GetZipCodeInfoResponseZipCodeInfoCounty> countyList = counties.getGetZipCodeInfoResponseZipCodeInfoCounty();

			for (GetZipCodeInfoResponseZipCodeInfoCounty county: countyList) {

				ZipCodesMedicare zipCode = new ZipCodesMedicare();

				zipCode.setAreaCode(zipCodeFromWebService.getAreaCodes().getValue());
				zipCode.setCounty(county.getCountyName());
				zipCode.setCountyFips(county.getCountyFIPS());
				zipCode.setLatitude(zipCodeFromWebService.getLatitude().getValue());
				zipCode.setLongitude(zipCodeFromWebService.getLongitude().getValue());
				zipCode.setState(requestVO.getStateCode());
				zipCode.setStateFips(requestVO.getStateFips());
				zipCode.setTimeZone(zipCodeFromWebService.getTimeZone().getValue());
				zipCode.setZipcode(String.format("%05d", zipCodeFromWebService.getZipCode()));

				zipCodesList.add(zipCode);
			}

		}

		LOGGER.info("Zipcodes list size for State " + requestVO.getStateName() + " is : " + zipCodesList.size());
		LOGGER.info("createZipCodesListForState End()");

		return zipCodesList;
	}

	/**
	 * Deletes existing zipcodes for given state.
	 * 
	 * @param requestVO - Request instance having statecode value
	 * @param entityManager - The Entity Manager instance
	 */
	private void deleteExistingZipCodesOfState(GetZipCodeInfoRequestVO requestVO, EntityManager entityManager) {

		LOGGER.info("deleteExistingZipCodesOfState Start()");

		Query deleteQuery = entityManager.createQuery(MEDICARE_ZIPCODES_DELETE_QUERY);
		deleteQuery.setParameter("stateCode", requestVO.getStateCode());
		int deleteCount = deleteQuery.executeUpdate();
		LOGGER.info(deleteCount + " ZipCode Records deleted for State " + requestVO.getStateName());

		LOGGER.info("deleteExistingZipCodesOfState End()");
	}


	/**
	 * Method is used to Commit/Rollback transaction depends on Status argument.
	 * 
	 * @param commitRequestStatus - Request to whether commit the transaction
	 * @param entityManager - The entity manager instance
	 * @return - Whether transaction is commited
	 * @throws GIException
	 */
	private boolean completeTransaction(boolean commitRequestStatus, EntityManager entityManager) throws GIException {

		LOGGER.info("completeTransaction() Start");
		boolean commitResponseStatus = false;

		try {

			if (null != entityManager && entityManager.isOpen()) {

				if (commitRequestStatus) {
					entityManager.getTransaction().commit();
					commitResponseStatus = true;
					LOGGER.info("Transaction has been Commited");
				} else {
					entityManager.getTransaction().rollback();
					commitResponseStatus = false;
					LOGGER.info("Transaction has been Rollback");
				}
			}
		} catch (Exception ex) {
			commitResponseStatus = false;
			LOGGER.error("Error occured while commiting Transaction " + commitResponseStatus, ex);
			throw new GIException(ex);
		} finally {
			LOGGER.info("commitStatus : " + commitResponseStatus);
			LOGGER.info("completeTransaction() End");
		}

		return commitResponseStatus;
	}

}