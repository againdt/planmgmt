package com.getinsured.serffadapter.excel;

import static com.serff.util.SerffConstants.DOLLARSIGN;
import static com.serff.util.SerffConstants.PERCENTAGE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.CollectionUtils;

/**
 * Read Given Excel File for any issuer.
 * 
 * Reference: http://viralpatel.net/blogs/java-read-write-excel-file-apache-poi/
 * 
 * Assumption: We are reading only xlsx files.
 * 
 * @author polimetla_b
 * @since 11/15/2013
 */
public abstract class ExcelReader {

	private static final Logger LOGGER = Logger.getLogger(ExcelReader.class);
	private static final String EMSG_SHEET_NOT_AVAILABLE = " sheet is not available.";
	// private String fileName;
	private FileInputStream excelFile = null;
	private XSSFWorkbook xssfWorkbook = null;
	private static final String PERCENT_FORMAT = "0%";
	private static final String CURRENCY_FORMAT = "\"$\"";
	private static final int HUNDRED = 100;

	public void loadFile(String fileNameGiven) throws IOException {
		LOGGER.info("Loading Excel File: " + fileNameGiven);
		// fileName = fileNameGiven;
		excelFile = new FileInputStream(new File(fileNameGiven));
		// Get the workbook instance for XLS file
		xssfWorkbook = new XSSFWorkbook(excelFile);
	}
	
	public void loadFile(InputStream fileInputStream) throws IOException {
		xssfWorkbook = new XSSFWorkbook(fileInputStream);
	}

	public void closeFile() throws IOException {
		if (null != excelFile) {
			excelFile.close();
		}
	}

	public int getNumberOfSheets() {
		return xssfWorkbook.getNumberOfSheets();
	}

	public XSSFSheet getSheet(int tab) throws IOException {
		
		try {
			// Get first sheet from the workbook
			return xssfWorkbook.getSheetAt(tab);
		}
		catch (IllegalArgumentException e) {
			LOGGER.error("No sheet available: ", e);
			return null;
		}
	}

	public XSSFSheet getSheet(String tabName) throws IOException {
		// Get first sheet from the workbook
		return xssfWorkbook.getSheetAt(xssfWorkbook.getSheetIndex(tabName));
	}

	public int getSheetIndex(String tabName) {
		// Get first sheet from the workbook
		return xssfWorkbook.getSheetIndex(tabName);
	}

	public String getCellValueTrim(Cell cell) {
		
		String returnValue = getCellValue(cell);
		
		if (StringUtils.isNotEmpty(returnValue)) {
			return returnValue.trim();
		}
		return returnValue;
	}

	public String getCellValue(Cell cell) {
		return getCellValue(cell, false); 
	}

	public String getCellValueTrim(Cell cell, boolean ignoreDecimal) {
		
		String returnValue = getCellValue(cell, ignoreDecimal);
		
		if (StringUtils.isNotEmpty(returnValue)) {
			return returnValue.trim();
		}
		return returnValue;
	}

	public String getCellValue(Cell cell, boolean ignoreDecimal) {
		
		String returnValue = null;
		
		if (null == cell) {
			return returnValue;
		}
		
		if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
			returnValue = getCellNumericValue(cell, ignoreDecimal);
		}
		else if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
			returnValue = cell.getStringCellValue();
		}
		else if (Cell.CELL_TYPE_FORMULA == cell.getCellType()) {
			returnValue = cell.getStringCellValue();
		}
		return returnValue;
	}
	
	private String getCellNumericValue(Cell cell, boolean ignoreDecimal) {
		
		String returnValue = null;
		
		if (null == cell) {
			return returnValue;
		}

		if (DateUtil.isCellDateFormatted(cell)) {
			Date today = cell.getDateCellValue();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			returnValue = dateFormat.format(today);
		}
		else {
			
			double val = cell.getNumericCellValue();
			String cellFormat = cell.getCellStyle().getDataFormatString();
			
			if(cellFormat.equals(PERCENT_FORMAT)) {
				val = val * HUNDRED;
				
				if(ignoreDecimal) {
					returnValue = getWholeNumberValue(val) + PERCENTAGE;
				}
				else {
					returnValue = val + PERCENTAGE;
				}
			}
			else if (cellFormat.startsWith(CURRENCY_FORMAT)) {
			
				if(ignoreDecimal) {
					returnValue = DOLLARSIGN + getWholeNumberValue(val);
				}
				else {
					returnValue = DOLLARSIGN + val;
				}
			}
			else {
				
				if(ignoreDecimal) {
					returnValue = StringUtils.EMPTY + getWholeNumberValue(val);
				}
				else {
					returnValue = StringUtils.EMPTY + val;
				}
			}
		}
		return returnValue;
	}
	
	private String getWholeNumberValue(double value) {
		return Integer.toString(Double.valueOf(value).intValue());
	}

	/**
	 * Method is used to get Column Value at Index.
	 */
	public String getColumnValueAtIndex(Row row, int index) {

		String cellValue = null;
		if (null == row) {
			return cellValue;
		}

		Cell excelCell;
		excelCell = row.getCell(index);

		if (excelCell != null) {
			cellValue = getCellValue(excelCell, true);
		}
		return cellValue;
	}

	/**
	 * Method is used to Valid Sheet.
	 */
	public boolean isValidSheet(int sheetIndex, String sheetName) {

		LOGGER.debug("isValidSheet() Strat");
		boolean isValid = true;

		try {
			XSSFSheet sheet = getSheet(sheetIndex);

			if (null == sheet
					|| null == sheet.iterator() // To check for blank sheet
					|| !sheet.getSheetName().equalsIgnoreCase(sheetName)) {
				isValid = false;
				LOGGER.error(sheetName + EMSG_SHEET_NOT_AVAILABLE);
			}
		}
		catch (IOException ex) {
			isValid = false;
			LOGGER.error("excelValidations() Exception occurred: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("isValidSheet() End");
		}
		return isValid;
	}

	/**
	 * Method is used to write data to Excel Sheet.
	 * 
	 * @param outputStream, OutputStream of Write Excel file.
	 * @param writeData, Write data object in Map format Example.
	 * writeData.put(0, new Object[]{"Row1-Column 1", "Row1-Column 2"});
	 * writeData.put(1, new Object[]{"Row2-Column 1", "Row2-Column 2"});
	 * 
	 * @param sheet, Write Excel Sheet.
	 */
	public void writeDataWithXSSF(OutputStream outputStream, Map<Integer, Object[]> writeData, XSSFSheet sheet) {

		LOGGER.info("writeDataWithXSSF() Start");

		try {

			if (null == outputStream || CollectionUtils.isEmpty(writeData) || null == sheet) {
				LOGGER.error("Input data are empty for Writing Excel.");
				return;
			}

			Set<Integer> keyset = writeData.keySet();
			int rownum = 0;

			for (Integer key : keyset) {
				Row row = sheet.createRow(rownum);
				rownum++;
				Object[] objectArray = writeData.get(key);

				if (null == objectArray) {
					continue;
				}

				for (int cellnum = 0; cellnum < objectArray.length; cellnum++) {
					setCellValue(cellnum, row, objectArray[cellnum]);
				}
			}
			LOGGER.info("Writing data to Excel.");
			xssfWorkbook.write(outputStream);
			outputStream.close();
		}
		catch (FileNotFoundException ex) {
			LOGGER.error("FileNotFoundException Error: " + ex.getMessage(), ex);
		}
		catch (Exception ex) {
			LOGGER.error("Write Excel Error: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("writeDataWithXSSF() End");
		}
	}

	private void setCellValue(int cellnum, Row row, Object object) {
		
		LOGGER.debug("setCellValue() Start");

		if (null == object || null == row) {
			return;
		}

		Cell cell = row.createCell(cellnum);
		if (object instanceof Date) {
			cell.setCellValue((Date) object);
		}
		else if (object instanceof Boolean) {
			cell.setCellValue((Boolean) object);
		}
		else if (object instanceof String) {
			cell.setCellValue((String) object);
		}
		else if (object instanceof Double) {
			cell.setCellValue((Double) object);
		}
		LOGGER.debug("setCellValue() End");
	}
}
