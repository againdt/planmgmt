package com.getinsured.serffadapter.life;

/**
 *-----------------------------------------------------------------------------
 * HIX-84392 PHIX - Load and persist Life plans.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to generate getter/setter on the basis of Plan Rates of Life Excel.
 * 
 * @author Bhavin Parmar
 * @since April 26, 2016
 */
public class LifePlanRatesVO {

	private String carrierHIOSId;
	private String planID;
//	private String planName;
	private String minimumAge;
	private String maximumAge;
	private String nonTobaccoMale;
	private String tobaccoMale;
	private String nonTobaccoFemale;
	private String tobaccoFemale;

	public LifePlanRatesVO() {
	}

	public String getCarrierHIOSId() {
		return carrierHIOSId;
	}

	public void setCarrierHIOSId(String carrierHIOSId) {
		this.carrierHIOSId = carrierHIOSId;
	}

	public String getPlanID() {
		return planID;
	}

	public void setPlanID(String planID) {
		this.planID = planID;
	}

	/*public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}*/

	public String getMinimumAge() {
		return minimumAge;
	}

	public void setMinimumAge(String minimumAge) {
		this.minimumAge = minimumAge;
	}

	public String getMaximumAge() {
		return maximumAge;
	}

	public void setMaximumAge(String maximumAge) {
		this.maximumAge = maximumAge;
	}

	public String getNonTobaccoMale() {
		return nonTobaccoMale;
	}

	public void setNonTobaccoMale(String nonTobaccoMale) {
		this.nonTobaccoMale = nonTobaccoMale;
	}

	public String getTobaccoMale() {
		return tobaccoMale;
	}

	public void setTobaccoMale(String tobaccoMale) {
		this.tobaccoMale = tobaccoMale;
	}

	public String getNonTobaccoFemale() {
		return nonTobaccoFemale;
	}

	public void setNonTobaccoFemale(String nonTobaccoFemale) {
		this.nonTobaccoFemale = nonTobaccoFemale;
	}

	public String getTobaccoFemale() {
		return tobaccoFemale;
	}

	public void setTobaccoFemale(String tobaccoFemale) {
		this.tobaccoFemale = tobaccoFemale;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("LifePlanRatesVO [carrierHIOSId=");
		sb.append(carrierHIOSId);
		sb.append(", planID=");
		sb.append(planID);
		/*sb.append(", planName=");
		sb.append(planName);*/
		sb.append(", minimumAge=");
		sb.append(minimumAge);
		sb.append(", maximumAge=");
		sb.append(maximumAge);
		sb.append(", nonTobaccoMale=");
		sb.append(nonTobaccoMale);
		sb.append(", tobaccoMale=");
		sb.append(tobaccoMale);
		sb.append(", nonTobaccoFemale=");
		sb.append(nonTobaccoFemale);
		sb.append(", tobaccoFemale=");
		sb.append(tobaccoFemale);
		sb.append("]");
		return sb.toString();
	}
}
