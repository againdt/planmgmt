package com.getinsured.serffadapter.life;

/**
 *-----------------------------------------------------------------------------
 * HIX-84392 PHIX - Load and persist Life plans.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to generate Enum on the basis of Life Plan Data
 * and Life Plan Rates sheets of Life Plan Excel.
 * 
 * @author Bhavin Parmar
 * @since April 26, 2016
 */
public class LifeExcelRow {

	private static final int NUMBER_TYPE = 0;
	private static final int FLOAT_TYPE = 1;
	private static final int STRING_TYPE = 2;
	private static final int DATE_TYPE = 3;

	public static final String SHEET_PLAN_DATA = "Life plan data";
	public static final String SHEET_PLAN_RATES = "Life plan rates";
	public static final String SHEET_PREMIMUM_FACTORS = "Premium Factors";
	public static final int SHEET_INDEX = 0;
	public static final int SHEET_INDEX_PLAN_DATA = 1;
	public static final int SHEET_INDEX_PLAN_RATES = 2;

	public static final int PLAN_DATA_START_COLUMN = 1;
	public static final int RATE_DATA_START_COLUMN = 1;
	public static final int PLANDATA_ROW_COUNT = 13;
	public static final int PLANRATE_COLUMN_COUNT = 9;

	public enum LifePlanDataRowEnum {

		CARRIER_NAME("Carrier name", "CARRIER_NAME", 0, STRING_TYPE),
		CARRIER_HIOS_ID("Carrier HIOS ID", "CARRIER_HIOS_ID", 1, STRING_TYPE),
		STATE_LIST("State List", "STATE_LIST", 2, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 3, STRING_TYPE),
		PLAN_NAME("Plan Name", "PLAN_NAME", 4, STRING_TYPE),
		START_DATE("Start Date", "START_DATE", 5, DATE_TYPE),
		PLAN_TYPE("Plan Type", "PLAN_TYPE", 6, STRING_TYPE),
		BENEFIT_DOCUMENT("Benefit Document", "BENEFIT_DOCUMENT", 7, STRING_TYPE),
		PLAN_DURATION("Plan Duration (Years)", "PLAN_DURATION", 8, NUMBER_TYPE),
		MIN_COVERAGE_AMOUNT("Min Coverage Amount", "MIN_COVERAGE_AMOUNT", 9, FLOAT_TYPE),
		MAX_COVERAGE_AMOUNT("Max Coverage Amount", "MAX_COVERAGE_AMOUNT", 10, FLOAT_TYPE),
		ANNUAL_POLICY_FEE("Annual Policy Fee", "ANNUAL_POLICY_FEE", 11, FLOAT_TYPE);

		private String rowName;
		private String rowConstName;
		private int rowIndex;
		private int dataType;

		private LifePlanDataRowEnum(String name, String rowConstName, int index, int dataType) {
			this.rowName = name;
			this.rowConstName = rowConstName;
			this.rowIndex = index;
			this.dataType = dataType;
		}

		public String getRowName() {
			return rowName;
		}

		public String getRowConstName() {
			return rowConstName;
		}

		public int getRowIndex() {
			return rowIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum LifePlanRatesColumnEnum {

		CARRIER_HIOS_ID("Carrier HIOS ID", "CARRIER_HIOS_ID", 0, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 1, STRING_TYPE),
		PLAN_NAME("Plan Name", "PLAN_NAME", 2, STRING_TYPE),
		MIN_AGE("Plan Name", "MIN_AGE", 3, STRING_TYPE),
		MAX_AGE("MAX Age", "MAX_AGE", 4, NUMBER_TYPE),
		NON_TOBACCO_MALE("Non-Tobacco (Male)", "NON_TOBACCO_MALE", 5, FLOAT_TYPE),
		TOBACCO_MALE("Tobacco (Male)", "TOBACCO_MALE", 6, FLOAT_TYPE),
		NON_TOBACCO_FEMALE("Non-Tobacco (Female)", "NON_TOBACCO_FEMALE", 7, FLOAT_TYPE),
		TOBACCO_FEMALE("Tobacco (Female)", "TOBACCO_FEMALE", 8, FLOAT_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private LifePlanRatesColumnEnum(String name, String columnConstName, int index, int dataType) {
			this.columnName = name;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}
}
