package com.getinsured.serffadapter.life;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *-----------------------------------------------------------------------------
 * HIX-84392 PHIX - Load and persist Life plans.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to generate getter/setter on the basis of Plan Data of Life Excel.
 * 
 * @author Bhavin Parmar
 * @since April 26, 2016
 */
public class LifePlanDataVO {

//	private String carrierName;
	private String carrierHIOSId;
	private Set<String> stateList;
	private String planId;
	private String planName;
	private String startDate;
	private String planType;
	private String benefitDocument;
	private String planDuration;
	private String minCoverageAmount;
	private String maxCoverageAmount;
	private String annualPolicyFee;

	private List<LifePlanRatesVO> lifePlanRateList;
	private boolean isNotValid;
	private String errorMessages;

	public LifePlanDataVO() {
	}

	/*public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}*/

	public String getCarrierHIOSId() {
		return carrierHIOSId;
	}

	public void setCarrierHIOSId(String carrierHIOSId) {
		this.carrierHIOSId = carrierHIOSId;
	}

	public Set<String> getStateList() {
		return stateList;
	}

	public void setStateList(Set<String> stateList) {
		this.stateList = stateList;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getBenefitDocument() {
		return benefitDocument;
	}

	public void setBenefitDocument(String benefitDocument) {
		this.benefitDocument = benefitDocument;
	}

	public String getPlanDuration() {
		return planDuration;
	}

	public void setPlanDuration(String planDuration) {
		this.planDuration = planDuration;
	}

	public String getMinCoverageAmount() {
		return minCoverageAmount;
	}

	public void setMinCoverageAmount(String minCoverageAmount) {
		this.minCoverageAmount = minCoverageAmount;
	}

	public String getMaxCoverageAmount() {
		return maxCoverageAmount;
	}

	public void setMaxCoverageAmount(String maxCoverageAmount) {
		this.maxCoverageAmount = maxCoverageAmount;
	}

	public String getAnnualPolicyFee() {
		return annualPolicyFee;
	}

	public void setAnnualPolicyFee(String annualPolicyFee) {
		this.annualPolicyFee = annualPolicyFee;
	}

	public List<LifePlanRatesVO> getLifePlanRateList() {
		return lifePlanRateList;
	}

	public void setLifePlanRatesVO(LifePlanRatesVO lifePlanRatesVO) {

		if (null != lifePlanRatesVO) {

			if (null == lifePlanRateList) {
				lifePlanRateList = new ArrayList<LifePlanRatesVO>();
			}
			lifePlanRateList.add(lifePlanRatesVO);
		}
	}

	public boolean isNotValid() {
		return isNotValid;
	}

	public void setNotValid(boolean isNotValid) {
		this.isNotValid = isNotValid;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
//		sb.append("LifePlanDataVO [carrierName=");
//		sb.append(carrierName);
		sb.append("LifePlanDataVO [carrierHIOSId=");
		sb.append(carrierHIOSId);
		sb.append(", stateList=(");
		sb.append(stateList.toString());
		sb.append("), planId=");
		sb.append(planId);
		sb.append(", planName=");
		sb.append(planName);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", planType=");
		sb.append(planType);
		sb.append(", benefitDocument=");
		sb.append(benefitDocument);
		sb.append(", planDuration=");
		sb.append(planDuration);
		sb.append(", minCoverageAmount=");
		sb.append(minCoverageAmount);
		sb.append(", maxCoverageAmount=");
		sb.append(maxCoverageAmount);
		sb.append(", annualPolicyFee=");
		sb.append(annualPolicyFee);
		sb.append(", lifePlanRateList=(");
		sb.append(lifePlanRateList.toString());
		sb.append("), isNotValid=");
		sb.append(isNotValid);
		sb.append(", errorMessages=");
		sb.append(errorMessages);
		sb.append("]");
		return sb.toString();
	}
}
