package com.getinsured.serffadapter.life;

import static com.getinsured.serffadapter.life.LifeExcelRow.PLANDATA_ROW_COUNT;
import static com.getinsured.serffadapter.life.LifeExcelRow.PLAN_DATA_START_COLUMN;
import static com.getinsured.serffadapter.life.LifeExcelRow.SHEET_INDEX;
import static com.getinsured.serffadapter.life.LifeExcelRow.SHEET_INDEX_PLAN_DATA;
import static com.getinsured.serffadapter.life.LifeExcelRow.SHEET_INDEX_PLAN_RATES;
import static com.getinsured.serffadapter.life.LifeExcelRow.SHEET_PLAN_DATA;
import static com.getinsured.serffadapter.life.LifeExcelRow.SHEET_PLAN_RATES;
import static com.serff.util.SerffConstants.DEFAULT_PHIX_STATE_CODE;
import static com.serff.util.SerffConstants.FTP_LIFE_FILE;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.excel.ExcelReader;
import com.getinsured.serffadapter.life.LifeExcelRow.LifePlanDataRowEnum;
import com.getinsured.serffadapter.life.LifeExcelRow.LifePlanRatesColumnEnum;
import com.serff.service.SerffService;
import com.serff.service.templates.LifePlanMgmtSerffService;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

/**
 *-----------------------------------------------------------------------------
 * HIX-84392 PHIX - Load and persist Life Insurance plans.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to read Life Insurance Excel data to LifePlanDataVO and LifePlanRatesVO DTO Objects.
 * 
 * @author Bhavin Parmar
 * @since April 26, 2016
 */
@Service("lifeExcelReader")
public class LifeExcelReader extends ExcelReader {

	private static final Logger LOGGER = Logger.getLogger(LifeExcelReader.class);

	private static final String MSG_GET_EACH_ROWS = "Get each rows from ";
	private static final String EMSG_INVALID_EXCEL = "Invalid Life Insurance excel template.";
	private static final String EMSG_LOAD_PLANS = "Failed to load Life Insurance Plans. Reason: ";
	private static final String EMSG_NO_EXCEL_FOUND = "No Life Insurance Excel Template is available for processing.";
	private static final String EMSG_MISSING_TRACKING_REC= "Unable to read tracking record for the template.";
	private static final String EMSG_DUPLICATE_PLAN = "Duplicate plan in the sheet ID: ";
	private static final String EMSG_LIFE = "Life Insurance Loading Error: ";
	private static final String EMSG_LIFE_PROCESS_COMPLETED = "Process loading Life Insurance Plans completed";
	private static final int ERROR_MAX_LEN = 1000;

	@Autowired private SerffResourceUtil serffResourceUtil;
	@Autowired private SerffService serffService;
	@Autowired private LifePlanMgmtSerffService lifePlanMgmtSerffService;
	@Autowired private SerffUtils serffUtils;
	@Autowired private GHIXSFTPClient ftpClient;

	/**
	 * Takes directory path which is location on FTP server and processes all available Life Insurance excel templates. 
	 * 
	 * @param ftpFolderPath - FTP location where Life Insurance template is Uploaded
	 * @return - Status message after processing Life Insurance template
	 */
	public String processAllLifeTemplate(String ftpFolderPath) {

		LOGGER.info("processAllLifeTemplate() Start");
		StringBuilder uiMessage = new StringBuilder();

		try {
			List<String> ftpFileList = serffResourceUtil.getFileNameListFromFTP(ftpFolderPath);

			if (null == ftpFileList || ftpFileList.isEmpty()) {
				uiMessage.append("No Life Insurance Excel Template is available for processing.");
				return uiMessage.toString();
			}
			LOGGER.info("Number of Excel Template is " + ftpFileList.size());
			boolean isEmptyDirectory = true;
			SerffPlanMgmtBatch trackingBatchRecord = null;

			for (String ftpFileName : ftpFileList) {

				if (StringUtils.isBlank(ftpFileName)
						|| ftpFileName.equalsIgnoreCase(SerffConstants.FTP_SUCCESS)
						|| ftpFileName.equalsIgnoreCase(SerffConstants.FTP_ERROR)) {
					continue;
				}
				isEmptyDirectory = false;
				LOGGER.debug("Processing Life Insurance file : Name : " + ftpFileName);
				trackingBatchRecord = serffService.getTrackingBatchRecord(ftpFileName, FTP_LIFE_FILE, uiMessage);

				if (null != trackingBatchRecord) {
					processSelectedTemplate(trackingBatchRecord, ftpFileName, ftpFolderPath, uiMessage);
				}
			}

			if (isEmptyDirectory) {
				uiMessage.append(EMSG_NO_EXCEL_FOUND);
			}
		}
		finally {

			if (StringUtils.isEmpty(uiMessage.toString())) {
				uiMessage.append(EMSG_LIFE_PROCESS_COMPLETED);
			}
			LOGGER.info(uiMessage);
			LOGGER.info("getAllTemplatesForPlans() End");
		}
		return uiMessage.toString();
	}

	/**
	 * Method is used to process selected template from FTP server.
	 */
	private void processSelectedTemplate(SerffPlanMgmtBatch trackingBatchRecord, String ftpFileName, String ftpFolderPath, StringBuilder uiMessage) {

		SerffPlanMgmt serffTrackingRecord = null;
		SerffPlanMgmtBatch updatedTrackingBatchRecord = serffService.updateTrackingBatchRecordAsInProgress(trackingBatchRecord);

		try {
			serffTrackingRecord = serffUtils.createSerffPlanMgmtRecord(SerffConstants.TRANSFER_LIFE, SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.W);

			if (null != serffTrackingRecord) {
				serffTrackingRecord.setAttachmentsList(ftpFileName);
		        serffTrackingRecord.setRequestStateDesc("Ready to load plans to DB.");
		        serffTrackingRecord = serffService.saveSerffPlanMgmt(serffTrackingRecord);
		        loadExcelTemplate(serffTrackingRecord, ftpFolderPath, uiMessage, updatedTrackingBatchRecord.getDefaultTenant());
			}
		}
		catch (UnknownHostException e) {
			uiMessage.append(EMSG_MISSING_TRACKING_REC);
			LOGGER.error(e.getMessage(), e);
		}
		finally {
			SerffPlanMgmtBatch.BATCH_STATUS status = null;

			if (null != serffTrackingRecord && serffTrackingRecord.getRequestStatus().equals(SerffPlanMgmt.REQUEST_STATUS.S)) {
				//set status as success
				status = SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED;
			}
			else {
				status = SerffPlanMgmtBatch.BATCH_STATUS.FAILED;
			}
			updatedTrackingBatchRecord = serffService.updateTrackingBatchRecordAsCompleted(updatedTrackingBatchRecord, serffTrackingRecord, status);
		}
	}

	/**
	 * Loads excel sheet by reading excel sheet input stream.
	 */
	private void loadExcelTemplate(SerffPlanMgmt trackingRecord, String ftpFolderPath, StringBuilder uiMessage, String defaultTenantIds) {

		LOGGER.debug("loadExcelTemplate() Start");
		boolean isSuccess = false;
		InputStream inputStream = null;

		try {
			inputStream = ftpClient.getFileInputStream(ftpFolderPath + trackingRecord.getAttachmentsList());

			if (null == inputStream) {
				LOGGER.info("Life Insurance Excel is not found at FTP server : " + ftpFolderPath + trackingRecord.getAttachmentsList());
				trackingRecord.setPmResponseXml("Life Insurance Excel is not found at FTP server.");
				trackingRecord.setRequestStateDesc(trackingRecord.getPmResponseXml());
				uiMessage.append(trackingRecord.getPmResponseXml());
				return;
			}
			// Upload Excel File to ECM
			isSuccess = serffResourceUtil.uploadFileFromFTPToECM(trackingRecord, ftpFolderPath, FTP_LIFE_FILE, trackingRecord.getAttachmentsList(), true);

			if (!isSuccess) {
				LOGGER.error("Failed to upload Life Insurance file in ECM : " + ftpFolderPath + trackingRecord.getAttachmentsList());
				trackingRecord.setPmResponseXml("Failed upload Excel file to ECM.");
				trackingRecord.setRequestStateDesc(trackingRecord.getPmResponseXml());
				uiMessage.append(trackingRecord.getPmResponseXml());
				return;
			}
			isSuccess = false;
			// Load Excel File
			loadFile(inputStream);

			if (!isValidExcelTemplate()) {
				trackingRecord.setPmResponseXml(EMSG_INVALID_EXCEL);
				trackingRecord.setRequestStateDesc(EMSG_INVALID_EXCEL);
				uiMessage.append(EMSG_INVALID_EXCEL);
				return;
			}
			// Populate and Persist Life Insurance Excel
			isSuccess = populateAndPersistLifeExcel(trackingRecord, uiMessage, defaultTenantIds);
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			uiMessage.append(EMSG_LOAD_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
     		updateTrackingRecord(isSuccess, trackingRecord, uiMessage);
			String baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansLifeUploadPath();
			serffResourceUtil.moveFileToDirectory(isSuccess, trackingRecord.getAttachmentsList(), baseDirectoryPath);
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("loadExcelTemplate() End");
		}
	}

	/**
	 * Populates and persist Life Insurance plan data in database.
	 * 
	 * @param trackingRecord - Serff Planmgmt Record
	 * @return - Status of plan loading process
	 */
	private boolean populateAndPersistLifeExcel(SerffPlanMgmt trackingRecord, StringBuilder uiMessage, String defaultTenantIds) {

		LOGGER.debug("populateAndPersistLifeExcel() Start");
		Map<String, LifePlanDataVO> lifePlansMapData = null;
		boolean isSuccess = false;

		try {
			XSSFSheet indexSheet = getSheet(SHEET_INDEX);
			String indexCarrierID = getCellValueTrim(indexSheet.getRow(2).getCell(2));
			if (StringUtils.isBlank(indexCarrierID)) {
				uiMessage.append("Carrier ID is must be required in Index sheet.");
				LOGGER.error(uiMessage.toString());
				return isSuccess;
			}

			// Get row data from the sheets.
			Row[] lifePlanDataRows = getLifePlanDataRows(getSheet(SHEET_INDEX_PLAN_DATA));
			lifePlansMapData = getLifeExcelData(indexCarrierID, lifePlanDataRows, getSheet(SHEET_INDEX_PLAN_RATES), uiMessage);

			// Validate Plans Map Data
			boolean validData = validatePlansMapData(lifePlansMapData, indexCarrierID, uiMessage);

			if (validData) {
				LOGGER.info("Number of Life Insurance plans available : " + lifePlansMapData.size());
				isSuccess = lifePlanMgmtSerffService.populateAndPersistLifeData(lifePlansMapData, trackingRecord, defaultTenantIds);
   			    uiMessage.append(trackingRecord.getRequestStateDesc());
			}
		}
		catch (IOException ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		catch (GIException e) {
			trackingRecord.setRequestStateDesc(e.getMessage());
			LOGGER.error(e.getMessage(), e);
		}
		finally {

			if (!CollectionUtils.isEmpty(lifePlansMapData)) {
				lifePlansMapData.clear();
				lifePlansMapData = null;
			}
			LOGGER.debug("populateAndPersistLifeExcel() End");
		}
		return isSuccess;
	}

	private boolean validatePlansMapData(Map<String, LifePlanDataVO> lifePlansMapData, String indexCarrierID, StringBuilder uiMessage) {

		LOGGER.debug("validatePlansMapData() Start");
		boolean validData = true;

		try {

			if (CollectionUtils.isEmpty(lifePlansMapData)) {
				validData = false;
				return validData;
			}
 
			for (Map.Entry<String, LifePlanDataVO> lifeEntry : lifePlansMapData.entrySet()) {

				if (!indexCarrierID.equals(lifeEntry.getValue().getCarrierHIOSId())) {
					uiMessage.append("Carrier ID["+ indexCarrierID +"] of Index sheet is mismatched with Carrier ID["+ lifeEntry.getValue().getCarrierHIOSId() +"].");
					validData = false;
					break;
				}

				if (CollectionUtils.isEmpty(lifeEntry.getValue().getLifePlanRateList())) {
					uiMessage.append("'"+ SHEET_PLAN_RATES +"' are missing for Issuer Plan Numer: " + lifeEntry.getKey());
					uiMessage.append("\n");
					validData = false;
					break;
				}
			}
		}
		finally {

			if (!validData) {
				LOGGER.error(uiMessage.toString());
			}
			LOGGER.debug("validatePlansMapData() End");
		}
		return validData;
	}

	/**
	 * Reads data from excel sheet row by row.
	 * 
	 * @return - Map containing HIOS ID & Plan Number as key and corresponding Plan Data VO as value
	 */
	private Map<String, LifePlanDataVO> getLifeExcelData(String indexCarrierID, Row[] lifePlanDataRows, XSSFSheet sheetLifePlanRates,
			StringBuilder uiMessage) {

		LOGGER.debug("getLifeExcelData() Start");
		Map<String, LifePlanDataVO> lifePlansMap = null;

		try {

			if (null != lifePlanDataRows) {

				lifePlansMap = new HashMap<String, LifePlanDataVO>();
				LOGGER.debug(MSG_GET_EACH_ROWS + "'" + SHEET_PLAN_DATA + "' sheet[1].");
				readLifePlanData(lifePlansMap, lifePlanDataRows, uiMessage);

				if (CollectionUtils.isEmpty(lifePlansMap)) {
					uiMessage.append("'"+ SHEET_PLAN_DATA +"' sheet is empty.");
					LOGGER.error(uiMessage.toString());
					return null;
				}

				LOGGER.debug(MSG_GET_EACH_ROWS + "'" + SHEET_PLAN_RATES + "' sheet[2].");
				Iterator<Row> rowIterator = sheetLifePlanRates.iterator();
				boolean isFirstRow = true;
				LifePlanDataVO lifePlanDataVO = null;
				String carrierHIOSId = null;
				String planID = null;

				while (rowIterator.hasNext()) {

					Row excelRow = rowIterator.next();

					if (isFirstRow) {
						isFirstRow = false;
						continue;
					}
					carrierHIOSId = getCellValueTrim(excelRow.getCell(LifePlanRatesColumnEnum.CARRIER_HIOS_ID.getColumnIndex()));
					planID = getCellValueTrim(excelRow.getCell(LifePlanRatesColumnEnum.PLAN_ID.getColumnIndex()));

					if (StringUtils.isBlank(carrierHIOSId) || StringUtils.isBlank(planID)) {
						break;
					}
					lifePlanDataVO = lifePlansMap.get(carrierHIOSId + DEFAULT_PHIX_STATE_CODE + planID);  // Map Key

					if (null == lifePlanDataVO) {

						if (indexCarrierID.equals(carrierHIOSId)) {
							LOGGER.warn("Plan number["+ planID +"] is missing for Carrier ID ["+ carrierHIOSId +"] from '" + SHEET_PLAN_DATA + "' sheet.");
						}
						else {
							uiMessage.append("Carrier ID["+ indexCarrierID +"] of Index sheet is mismatched with Carrier ID["+ carrierHIOSId +"] in '" + SHEET_PLAN_DATA + "' sheet.");
							LOGGER.error(uiMessage.toString());
							lifePlansMap = null;
							break;
						}
					}
					else {
						readLifeRateData(lifePlanDataVO, excelRow, carrierHIOSId, planID);
					}
				}
			}
			else {
				uiMessage.append(EMSG_INVALID_EXCEL);
			}
		}
		catch (Exception ex) {
			lifePlansMap = null;
			uiMessage.append("Error occured while fetching data from Excel.");
			LOGGER.error(uiMessage.toString(), ex);
		}
		finally {
			LOGGER.debug("getLifeExcelData() End");
		}
		return lifePlansMap;
	}

	/**
	 * Reads excel data and populates Map with Plan Number and Plan VO details.
	 */
	private void readLifePlanData(Map<String, LifePlanDataVO> lifePlansMap, Row[] excelRows, StringBuilder uiMessage) {

		LOGGER.debug("readLifePlanData() Start");
		if (null == excelRows) {
			return;
		}

		try {
			short maxColIx = excelRows[LifePlanDataRowEnum.CARRIER_HIOS_ID.getRowIndex()].getLastCellNum();
			String hiosId, allStates, planId;

			for (short colIx = PLAN_DATA_START_COLUMN; colIx < maxColIx; colIx++) {

				hiosId = getColumnValueAtIndex(excelRows[LifePlanDataRowEnum.CARRIER_HIOS_ID.getRowIndex()], colIx);
				allStates = getColumnValueAtIndex(excelRows[LifePlanDataRowEnum.STATE_LIST.getRowIndex()], colIx);
				planId = getColumnValueAtIndex(excelRows[LifePlanDataRowEnum.PLAN_ID.getRowIndex()], colIx);

				if (StringUtils.isBlank(hiosId)
						|| StringUtils.isBlank(allStates)
						|| StringUtils.isBlank(planId)) {
					LOGGER.warn(EMSG_LIFE + "Empty " + LifePlanDataRowEnum.CARRIER_HIOS_ID.getRowName() + "[" + hiosId + "] " + "Or "
							+ LifePlanDataRowEnum.STATE_LIST.getRowName() + "[" + allStates + "] " + "Or "
							+ LifePlanDataRowEnum.PLAN_ID.getRowName() + "[" + planId + "].");
					break;
				}
				LOGGER.debug("Carrier HIOS ID ["+ hiosId +"] or Plan number["+ planId +"]");
				addLifePlanDataToMap(lifePlansMap, excelRows, hiosId, allStates, planId, colIx);
			}
		}
		catch (Exception ex) {
			lifePlansMap = null;
			uiMessage.append("Error occured while iterating " + SHEET_PLAN_DATA);
			LOGGER.error(uiMessage.toString(), ex);
		}
		finally {
			LOGGER.debug("readLifePlanData() End");
		}
	}

	/**
	 * Reads data from excel sheet.
	 */
	private Row[] getLifePlanDataRows(XSSFSheet lifePlanSheet) {
		LOGGER.debug("getLifePlanDataRows() Start");
		Row[] planDataRows = null;

		if (null == lifePlanSheet) {
			return planDataRows;
		}
		planDataRows = new Row[PLANDATA_ROW_COUNT];
		planDataRows[LifePlanDataRowEnum.CARRIER_NAME.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.CARRIER_NAME.getRowIndex());
		planDataRows[LifePlanDataRowEnum.CARRIER_HIOS_ID.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.CARRIER_HIOS_ID.getRowIndex());
		planDataRows[LifePlanDataRowEnum.STATE_LIST.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.STATE_LIST.getRowIndex());
		planDataRows[LifePlanDataRowEnum.PLAN_ID.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.PLAN_ID.getRowIndex());
		planDataRows[LifePlanDataRowEnum.PLAN_NAME.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.PLAN_NAME.getRowIndex());
		planDataRows[LifePlanDataRowEnum.START_DATE.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.START_DATE.getRowIndex());
		planDataRows[LifePlanDataRowEnum.PLAN_TYPE.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.PLAN_TYPE.getRowIndex());
		planDataRows[LifePlanDataRowEnum.BENEFIT_DOCUMENT.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.BENEFIT_DOCUMENT.getRowIndex());
		planDataRows[LifePlanDataRowEnum.PLAN_DURATION.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.PLAN_DURATION.getRowIndex());
		planDataRows[LifePlanDataRowEnum.MIN_COVERAGE_AMOUNT.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.MIN_COVERAGE_AMOUNT.getRowIndex());
		planDataRows[LifePlanDataRowEnum.MAX_COVERAGE_AMOUNT.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.MAX_COVERAGE_AMOUNT.getRowIndex());
		planDataRows[LifePlanDataRowEnum.ANNUAL_POLICY_FEE.getRowIndex()] = lifePlanSheet.getRow(LifePlanDataRowEnum.ANNUAL_POLICY_FEE.getRowIndex());
		LOGGER.debug("getLifePlanDataRows() End");
		return planDataRows;
	}

	/**
	 * Add Life Plan Rates Data To Map.
	 */
	private void addLifePlanDataToMap(Map<String, LifePlanDataVO> lifePlansMap, Row[] excelRows, String hiosId,
			String allStates,String planId, int colIx) {

		LOGGER.debug("addLifePlanDataToMap() Start");
		String mapKey = hiosId + DEFAULT_PHIX_STATE_CODE + planId;
		LifePlanDataVO lifePlanDataVO = lifePlansMap.get(mapKey);

		if (null == lifePlanDataVO) {
			lifePlanDataVO = new LifePlanDataVO();
//			lifePlanDataVO.setCarrierName(getCellValueTrim(excelRows[LifePlanDataRowEnum.CARRIER_NAME.getRowIndex()].getCell(colIx)));
			lifePlanDataVO.setCarrierHIOSId(hiosId);
			lifePlanDataVO.setStateList(new HashSet<String>(Arrays.asList(allStates.replace(SerffConstants.SPACE, StringUtils.EMPTY).split(SerffConstants.COMMA))));
			lifePlanDataVO.setPlanId(planId);
			lifePlanDataVO.setPlanName(getCellValueTrim(excelRows[LifePlanDataRowEnum.PLAN_NAME.getRowIndex()].getCell(colIx)));
			lifePlanDataVO.setStartDate(getCellValueTrim(excelRows[LifePlanDataRowEnum.START_DATE.getRowIndex()].getCell(colIx)));
			lifePlanDataVO.setPlanType(getCellValueTrim(excelRows[LifePlanDataRowEnum.PLAN_TYPE.getRowIndex()].getCell(colIx)));
			lifePlanDataVO.setBenefitDocument(getCellValueTrim(excelRows[LifePlanDataRowEnum.BENEFIT_DOCUMENT.getRowIndex()].getCell(colIx)));
			lifePlanDataVO.setPlanDuration(getCellValueTrim(excelRows[LifePlanDataRowEnum.PLAN_DURATION.getRowIndex()].getCell(colIx), true));
			lifePlanDataVO.setMinCoverageAmount(getCellValueTrim(excelRows[LifePlanDataRowEnum.MIN_COVERAGE_AMOUNT.getRowIndex()].getCell(colIx), true));
			lifePlanDataVO.setMaxCoverageAmount(getCellValueTrim(excelRows[LifePlanDataRowEnum.MAX_COVERAGE_AMOUNT.getRowIndex()].getCell(colIx), true));
			lifePlanDataVO.setAnnualPolicyFee(getCellValueTrim(excelRows[LifePlanDataRowEnum.ANNUAL_POLICY_FEE.getRowIndex()].getCell(colIx)));
			lifePlansMap.put(mapKey, lifePlanDataVO);
		}
		else {
			LOGGER.warn(EMSG_LIFE + EMSG_DUPLICATE_PLAN + mapKey);
		}
		LOGGER.debug("addLifePlanDataToMap() End");
	}

	/**
	 * Reads excel Rate data and populates corresponding Plan VO with rate details.
	 */
	private void readLifeRateData(LifePlanDataVO lifePlanDataVO, Row excelRow, String carrierHIOSId, String planID) {

		LOGGER.debug("readLifeRateData() Start");
		if (null == excelRow || null == lifePlanDataVO) {
			return;
		}

		try {
			LifePlanRatesVO lifePlanRatesVO = new LifePlanRatesVO();
			lifePlanRatesVO.setCarrierHIOSId(carrierHIOSId);
			lifePlanRatesVO.setPlanID(planID);
//			lifePlanRatesVO.setPlanName(getCellValueTrim(excelRow.getCell(LifePlanRatesColumnEnum.PLAN_NAME.getColumnIndex())));
			lifePlanRatesVO.setMinimumAge(getCellValueTrim(excelRow.getCell(LifePlanRatesColumnEnum.MIN_AGE.getColumnIndex()), true));
			lifePlanRatesVO.setMaximumAge(getCellValueTrim(excelRow.getCell(LifePlanRatesColumnEnum.MAX_AGE.getColumnIndex()), true));
			lifePlanRatesVO.setNonTobaccoMale(getCellValueTrim(excelRow.getCell(LifePlanRatesColumnEnum.NON_TOBACCO_MALE.getColumnIndex())));
			lifePlanRatesVO.setTobaccoMale(getCellValueTrim(excelRow.getCell(LifePlanRatesColumnEnum.TOBACCO_MALE.getColumnIndex())));
			lifePlanRatesVO.setNonTobaccoFemale(getCellValueTrim(excelRow.getCell(LifePlanRatesColumnEnum.NON_TOBACCO_FEMALE.getColumnIndex())));
			lifePlanRatesVO.setTobaccoFemale(getCellValueTrim(excelRow.getCell(LifePlanRatesColumnEnum.TOBACCO_FEMALE.getColumnIndex())));
			// Setting LifePlanRatesVO POJO in LifePlanDataVO.
			lifePlanDataVO.setLifePlanRatesVO(lifePlanRatesVO);
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred in readLifeRateData()", ex);
		}
		finally {
			LOGGER.debug("readLifeRateData() End");
		}
	}

	/**
	 * Method is used to validated Excel Sheets.
	 * 
	 * @return - Returns whether sheet is valid
	 */
	private boolean isValidExcelTemplate() {

		LOGGER.debug("isValidExcelTemplate() Start");
		boolean isValid = true;

		try {
			isValid = isValidSheet(SHEET_INDEX_PLAN_DATA, SHEET_PLAN_DATA);
			isValid = isValid && isValidSheet(SHEET_INDEX_PLAN_RATES, SHEET_PLAN_RATES);
		}
		finally {
			LOGGER.debug("isValidExcelTemplate() End");
		}
		return isValid;
	}

	
	/**
	 * Method is used to update tracking record of SERFF Plan Management table.
	 * 
	 * @param isSuccess - Success flag
	 * @param trackingRecord - SERFF Plan Mgmt Record
	 * @param message - Process message
	 */
	private void updateTrackingRecord(boolean isSuccess, SerffPlanMgmt trackingRecord, StringBuilder message) {

		LOGGER.debug("updateTrackingRecord() Start");
		try {
			if (null != trackingRecord) {

				if (isSuccess) {
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
				}
				else {
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
				}

				if (StringUtils.isNotBlank(message.toString())) {
					trackingRecord.setRequestStateDesc(message.toString());
				}

				if (trackingRecord.getRequestStateDesc().length() > ERROR_MAX_LEN) {
					trackingRecord.setRequestStateDesc(trackingRecord.getRequestStateDesc().substring(0, ERROR_MAX_LEN));
				}

				if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
					trackingRecord.setPmResponseXml(trackingRecord.getRequestStateDesc());
				}
				trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
				trackingRecord.setEndTime(new Date());
				serffService.saveSerffPlanMgmt(trackingRecord);
			}
			else {
				message.append("Tracking Record is not found.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred in updateTrackingRecord() ", ex);
		}
		finally {
			LOGGER.debug("updateTrackingRecord() End");
		}
	}
}
