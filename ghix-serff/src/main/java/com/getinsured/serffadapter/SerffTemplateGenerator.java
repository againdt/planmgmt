package com.getinsured.serffadapter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;

//import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.vo.BenefitVO;
import com.getinsured.serffadapter.vo.CostShareVarianceVO;
import com.getinsured.serffadapter.vo.DeductibleVO;
import com.getinsured.serffadapter.vo.IssuerVO;
import com.getinsured.serffadapter.vo.NetworkVO;
import com.getinsured.serffadapter.vo.PlanAttributeVO;
import com.getinsured.serffadapter.vo.PlanRateVO;
import com.getinsured.serffadapter.vo.ServiceAreaVO;
import com.getinsured.serffadapter.xmlwriter.NetworkTemplateXMLWriter;
import com.getinsured.serffadapter.xmlwriter.PlanBenefitTemplateXMLWriter;
import com.getinsured.serffadapter.xmlwriter.RatesTemplateXMLWriter;
import com.getinsured.serffadapter.xmlwriter.ServiceAreaTemplateXMLWriter;
import com.serff.template.plan.PlanAndBenefitsPackageVO;
import com.serff.util.SerffConstants;

public class SerffTemplateGenerator {

	//@Autowired private GHIXSFTPClient ftpClient;
	private static final Logger LOGGER = Logger.getLogger(SerffTemplateGenerator.class);
	private static final String APPLICATION_ID = "applica";
	private static final String GENERATOR_NAME = "generator";
	//private static final int FTP_PORT = 22;
	private CarrierTemplateAdapter adpt = null;
	private static Map<String, BenefitVO> onExchangeBenefitsMap = new HashMap<String, BenefitVO>();
	private static String[] onExchangeBenefitsNames = { "Primary Care Visit to Treat an Injury or Illness", "Specialist Visit",
			"Other Practitioner Office Visit (Nurse, Physician Assistant)", "Outpatient Facility Fee (e.g.,  Ambulatory Surgery Center)",
			"Outpatient Surgery Physician/Surgical Services", "Hospice Services", "Non-Emergency Care When Traveling Outside the U.S.",
			"Routine Dental Services (Adult)", "Infertility Treatment", "Long-Term/Custodial Nursing Home Care", "Private-Duty Nursing",
			"Routine Eye Exam (Adult)", "Urgent Care Centers or Facilities", "Home Health Care Services", "Emergency Room Services",
			"Emergency Transportation/Ambulance", "Inpatient Hospital Services (e.g., Hospital Stay)", "Inpatient Physician and Surgical Services",
			"Bariatric Surgery", "Cosmetic Surgery", "Skilled Nursing Facility", "Prenatal and Postnatal Care",
			"Delivery and All Inpatient Services for Maternity Care", "Mental/Behavioral Health Outpatient Services",
			"Mental/Behavioral Health Inpatient Services", "Substance Abuse Disorder Outpatient Services",
			"Substance Abuse Disorder Inpatient Services", "Generic Drugs", "Preferred Brand Drugs", "Non-Preferred Brand Drugs", "Specialty Drugs",
			"Outpatient Rehabilitation Services", "Habilitation Services", "Chiropractic Care", "Durable Medical Equipment", "Hearing Aids",
			"Imaging (CT/PET Scans, MRIs)", "Preventive Care/Screening/Immunization", "Routine Foot Care", "Acupuncture", "Weight Loss Programs",
			"Routine Eye Exam for Children", "Eye Glasses for Children", "Dental Check-Up for Children", "Rehabilitative Speech Therapy",
			"Rehabilitative Occupational and Rehabilitative Physical Therapy", "Well Baby Visits and Care",
			"Laboratory Outpatient and Professional Services", "X-rays and Diagnostic Imaging", "Basic Dental Care - Child", "Orthodontia - Child",
			"Major Dental Care - Child", "Basic Dental Care - Adult", "Orthodontia - Adult", "Major Dental Care - Adult",
			"Abortion for Which Public Funding is Prohibited", "Transplant", "Accidental Dental", "Dialysis", "Allergy Testing", "Chemotherapy",
			"Radiation", "Diabetes Education", "Prosthetic Devices", "Infusion Therapy", "Treatment for Temporomandibular Joint Disorders",
			"Nutritional Counseling", "Reconstructive Surgery", "Inherited Metabolic Disorder - PKU", "Dental Anesthesia",
			"Prescription Drugs Other", "Organ Transplants", "Bones/Joints", "Autism Spectrum Disorders", "Breast Implant Removal",
			"Multiple Sclerosis", "Clinical Trials", "Diabetes Care Management" };

	static {
		for (String benefitName : onExchangeBenefitsNames) {
			BenefitVO benefit = new BenefitVO();
			benefit.setBenefitTypeCode(benefitName);
			benefit.setIsBenefitCovered("Not Covered");
			onExchangeBenefitsMap.put(benefitName, benefit);
		}
	}

	public Map<String, String> generateSerffTemplatesOnFTP(CarrierTemplateAdapter adaptor, String ftpPath) throws GIException {
		return generateSerffTemplates(adaptor, ftpPath, false);
	}
	
	public Map<String, String> generateSerffTemplates(CarrierTemplateAdapter adaptor, String path) throws GIException {
		return generateSerffTemplates(adaptor, path, true);
	}
	
	private Map<String, String> generateSerffTemplates(CarrierTemplateAdapter adaptor, String path, boolean isLocalPath) throws GIException {
		adpt = adaptor;
		String contents;
		Map<String, String> filePathMap = null;
		List<IssuerVO> issuersList = adpt.getIssuerList();
		LOGGER.info("Starting generating templates at " + path);
		if(issuersList != null && !issuersList.isEmpty()) {
			filePathMap = new HashMap<String, String>();
			for (IssuerVO issuerVO : issuersList) {
				// Create Network Template
				StringBuilder filePath = new StringBuilder();
				filePath.append(path);
				filePath.append(issuerVO.getIssuerHIOSId());
				filePath.append(SerffConstants.PATH_SEPERATOR);
	
				contents = generateNetworkTemplateXML(issuerVO);
				createTemplateFile(contents, issuerVO.getIssuerHIOSId() + "_network.xml", filePath.toString(), isLocalPath);
				// Create Service Area Template
				contents = generateServiceAreaTemplateXML(issuerVO);
				createTemplateFile(contents, issuerVO.getIssuerHIOSId() + "_servicearea.xml", filePath.toString(), isLocalPath);
				// Create Prescription Drug Template
				
				//Uncomment this if prescription drug templates are required.
				//contents = generatePrescriptionDrugTemplateXML(issuerVO);
				//createTemplateFile(contents, issuerVO.getIssuerHIOSId() + "_prescriptiondrug.xml", filePath, isLocalPath);
				// Create Rates Template
				contents = generateRatesTemplateXML(issuerVO);
				createTemplateFile(contents, issuerVO.getIssuerHIOSId() + "_rate.xml", filePath.toString(), isLocalPath);
				// Create Plan & Benefits Template
				contents = generatePlanAndBenefitTemplateXML(issuerVO);
				createTemplateFile(contents, issuerVO.getIssuerHIOSId() + "_plans.xml", filePath.toString(), isLocalPath);
				LOGGER.info("Done generating templates at " + filePath);
				filePathMap.put(issuerVO.getIssuerHIOSId(), filePath.toString()) ;
			}
		}
		return filePathMap;
	}

	private String generateNetworkTemplateXML(IssuerVO issuer) throws GIException {
		NetworkTemplateXMLWriter writer = new NetworkTemplateXMLWriter();
		writer.setIssuer(issuer.getIssuerHIOSId(), issuer.getIssuerStateCode());

		List<NetworkVO> networks = adpt.getNetworkList(issuer);
		for (NetworkVO networkVO : networks) {
			writer.addNetwork(networkVO.getNetworkID(), networkVO.getRoleOfOrganizationReference(), networkVO.getWebsiteURI(),
					networkVO.getMarketingName());
		}

		return writer.generateNetworkTemplateXML();
	}

	private String generateServiceAreaTemplateXML(IssuerVO issuer) throws GIException {
		ServiceAreaTemplateXMLWriter svcAreaXMLWriter = new ServiceAreaTemplateXMLWriter();
		svcAreaXMLWriter.setIssuerIdentification(issuer.getIssuerHIOSId(), issuer.getIssuerStateCode());
		List<ServiceAreaVO> serviceAreas = adpt.getServiceAreaList(issuer.getIssuerHIOSId(), issuer.getIssuerStateCode());
		for (ServiceAreaVO serviceAreaVO : serviceAreas) {
			if (serviceAreaVO.isEntireState()) {
				svcAreaXMLWriter.addServiceAreaForFullState(serviceAreaVO.getServiceAreaId(), serviceAreaVO.getServiceAreaName());
			} else if (!serviceAreaVO.isPartialCounty()) {
				svcAreaXMLWriter.addServiceAreaForFullCounty(serviceAreaVO.getServiceAreaId(), serviceAreaVO.getServiceAreaName(),
						serviceAreaVO.getCountyCode());
			} else {
				svcAreaXMLWriter.addServiceAreaForPartialCounty(serviceAreaVO.getServiceAreaId(), serviceAreaVO.getServiceAreaName(),
						serviceAreaVO.getCountyCode(), serviceAreaVO.getPartialCountyReason(), serviceAreaVO.getPostalCodes());
			}
		}

		return svcAreaXMLWriter.generateServiceAreaTemplateXML();
	}

	private String generateRatesTemplateXML(IssuerVO issuer) throws GIException {
		RatesTemplateXMLWriter rw = new RatesTemplateXMLWriter();
		rw.setQhpApplicationRateHeader(issuer.getIssuerHIOSId(), issuer.getIssuerTinCode());
		List<String> planIdList = adpt.getPlanIdList(issuer.getIssuerHIOSId());
		if (planIdList != null) {
			for (String planId : planIdList) {
				List<PlanRateVO> rates = adpt.getPlanRatesList(issuer.getIssuerHIOSId(), planId);
				if(rates != null) {
					for (PlanRateVO planRateVO : rates) {
						if (planRateVO.getFamilyOption()) {
							rw.addQhpApplicationRateItem(planId, planRateVO.getRateAreaId(), planRateVO.getTobacco(), planRateVO.getAgeNumber(),
									planRateVO.getEffectiveDate(), planRateVO.getExpirationDate(), planRateVO.getPrimaryEnrollee(),
									planRateVO.getPrimaryEnrolleeOneDependent(), planRateVO.getPrimaryEnrolleeTwoDependent(),
									planRateVO.getPrimaryEnrolleeManyDependent(), planRateVO.getCoupleEnrollee(),
									planRateVO.getCoupleEnrolleeOneDependent(), planRateVO.getCoupleEnrolleeTwoDependent(),
									planRateVO.getCoupleEnrolleeManyDependent(), planRateVO.getIsIssuerData());
						} else {
							rw.addQhpApplicationRateItem(planId, planRateVO.getRateAreaId(), planRateVO.getTobacco(), planRateVO.getAgeNumber(),
									planRateVO.getEffectiveDate(), planRateVO.getExpirationDate(), planRateVO.getPrimaryEnrollee(),
									planRateVO.getPrimaryEnrolleeTobacco(), planRateVO.getIsIssuerData());
						}
					}
				}
			}
		}
		return rw.generateRatesTemplateXML();
	}

	private String generatePlanAndBenefitTemplateXML(IssuerVO issuer) throws GIException {
		PlanBenefitTemplateXMLWriter writer = new PlanBenefitTemplateXMLWriter(APPLICATION_ID, GENERATOR_NAME, null);

		List<String> planIdList = adpt.getPlanIdList(issuer.getIssuerHIOSId());
		if (planIdList != null) {
			for (String planId : planIdList) {
				PlanAttributeVO planAttributes = adpt.getPlanAttributes(issuer.getIssuerHIOSId(), issuer.getIssuerStateCode(), planId);
				PlanAndBenefitsPackageVO packageObj = writer.createPackage(issuer.getIssuerHIOSId(), issuer.getIssuerStateCode(), null,
						planAttributes.getMarketCoverage(), planAttributes.getDentalPlanOnlyInd(), null, issuer.getIssuerTinCode());

				writer.addPlanToPackage(packageObj, planAttributes);
				List<BenefitVO> benefitsList = addCSVToPlan(issuer, planId, writer);
				List<String> coveredBenefitsList = new ArrayList<String>();
				if(benefitsList != null) {
					for (BenefitVO benefitVO : benefitsList) {
						writer.addBenefitToPackage(packageObj, benefitVO);
						coveredBenefitsList.add(benefitVO.getBenefitTypeCode());
					}
	
					for (String benefitName : onExchangeBenefitsMap.keySet()) {
						if (!coveredBenefitsList.contains(benefitName)) {
							 writer.addBenefitToPackage(packageObj,
							 onExchangeBenefitsMap.get(benefitName));
						}
					}
				}
			}
		}
		return writer.generatePlanAndBenefitTemplateXML();
	}

	private List<BenefitVO> addCSVToPlan(IssuerVO issuer, String planId, PlanBenefitTemplateXMLWriter writer) {
		List<BenefitVO> benefitsList = null;
		List<CostShareVarianceVO> csvList = adpt.getCostShareVarianceForPlan(issuer.getIssuerHIOSId(), planId);
		for (CostShareVarianceVO costShareVarianceVO : csvList) {
			benefitsList = adpt.getPlanBenefitsList(issuer.getIssuerHIOSId(), costShareVarianceVO.getPlanId());
			writer.setCostShareVariance(costShareVarianceVO);
			List<DeductibleVO> moopList = costShareVarianceVO.getMoopList();
			for (DeductibleVO moopVO : moopList) {
				writer.addMoopToCostShareVariance(costShareVarianceVO.getPlanId(), moopVO);
			}
			List<DeductibleVO> deductibleList = costShareVarianceVO.getPlanDeductibleList();
			for (DeductibleVO deductibleVO : deductibleList) {
				writer.addPlanDeductibleToCostShareVariance(costShareVarianceVO.getPlanId(), deductibleVO);
			}
			for (BenefitVO benefitVO : benefitsList) {
				writer.addServiceVisitToCostShareVariance(costShareVarianceVO.getPlanId(), benefitVO);
			}
		}
		return benefitsList;
	}
	
	/*private String generatePrescriptionDrugTemplateXML(IssuerVO issuerVO) {
		PrescriptionDrugTemplateXMLWriter writer = new PrescriptionDrugTemplateXMLWriter();
		writer.setHeaderToPrescriptionDrug(APPLICATION_ID, SerffConstants.NO, issuerVO.getIssuerHIOSId(), null, issuerVO.getIssuerStateCode(), null, issuerVO.getIssuerTinCode());
		return writer.generatePrescriptionDrugXML();
	}*/

	private void createTemplateFile(String contents, String fileName, String filePath, boolean isLocalPath) {
		if (contents == null) {
			return;
		}
		if(isLocalPath) {
			File file = new File(filePath);
			boolean folderExist = false;
			if(null != file && !file.exists()) {
				if(file.mkdir()) {
					folderExist = true;
					LOGGER.info("Created folder for template files at " + filePath);
				} else {
					LOGGER.error("Failed to create folder for template files at " + filePath);
				}
				file = null;
			} else {
				folderExist = true;
			}
			
			if(folderExist) {
				Writer writer = null;
				
				try {
					writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath + fileName), "utf-8"));
					writer.write(contents);
					LOGGER.debug("Create template file " + filePath + fileName);
				} catch (IOException ex) {
					LOGGER.error("Failed to create template file " + filePath + fileName + " Error: " + ex.getMessage(), ex);
				} finally {
					try {
						if (null != writer) {
							writer.close();
						}
					} catch (Exception ex) {
						LOGGER.error("Exception while closingfile", ex);
					}
				}
			} else {
				LOGGER.error("Failed to create template files at " + filePath + " as the folder does not exist.");
			}

		} else {
			LOGGER.warn("Implementaion is incomplete, Failed in Putting file to FTP " + fileName);
			/*if(ftpClient == null) {
				// Should get from autowire
				ftpClient = new GHIXSFTPClient("74.205.108.146", "csruser", "GHIX123#",FTP_PORT);
				LOGGER.info("Initializing ftpClient");
			}
			try{
				String tempFilePath = System.getProperty("java.io.tmpdir") + fileName;
				File file = new File(tempFilePath);
				file.createNewFile();
				FileOutputStream fr = new FileOutputStream(file);
				BufferedOutputStream bdr = new BufferedOutputStream(fr);			
				bdr.write(contents.getBytes());
				bdr.flush();
				bdr.close();
				fr.flush();
				fr.close();			
				LOGGER.info("Temp file created to Put onto FTP " +  filePath + fileName);
				if(!ftpClient.isRemoteDirectoryExist(filePath)) {
					ftpClient.createDirectory(filePath);
					LOGGER.debug("Folder created onto FTP " +  filePath);
				}
				ftpClient.uplodadFile(new FileInputStream(file), filePath + fileName);	//"/home/csruser/dev001/AetnaTemplate/"
				LOGGER.debug("Temp file uploaded to FTP " + fileName);
				file.delete();
			}catch(Exception e){
				LOGGER.error("Error occurred while uploading template file to ftp location." + e.getMessage());
			}finally{
				if(null!= ftpClient){
					ftpClient.close();
				}
			}*/
		}
	}	
}
