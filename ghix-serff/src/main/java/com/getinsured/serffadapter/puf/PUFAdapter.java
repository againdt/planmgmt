package com.getinsured.serffadapter.puf;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.serff.PufHealthVO;
import com.getinsured.hix.platform.repository.IZipCodeRepository;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.serffadapter.CarrierTemplateAdapter;
import com.getinsured.serffadapter.SerffAdapterUtil;
import com.getinsured.serffadapter.vo.BenefitVO;
import com.getinsured.serffadapter.vo.CostShareVarianceVO;
import com.getinsured.serffadapter.vo.DeductibleVO;
import com.getinsured.serffadapter.vo.IssuerVO;
import com.getinsured.serffadapter.vo.NetworkVO;
import com.getinsured.serffadapter.vo.PlanAttributeVO;
import com.getinsured.serffadapter.vo.PlanRateVO;
import com.getinsured.serffadapter.vo.ServiceAreaVO;
import com.serff.repository.templates.IPufHealthRepository;
import com.serff.repository.templates.IPufRatesRepository;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

@Component
public class PUFAdapter implements CarrierTemplateAdapter {
	public static final int ISSUER_INFO_HIOSID_INDEX = 0;
	public static final int ISSUER_INFO_STATECODE_INDEX = 1;
	public static final int ISSUER_INFO_NAME_INDEX = 2;
	public static final int ISSUER_INFO_NETWORK_URL_INDEX = 3;
	public static final int ISSUER_INFO_TIN_INDEX = 4;
	public static final int HIOS_PRODUCT_ID_LEN = 10;
	public static final int CSV_VARIANT_TYPE1 = 1;
	public static final int CSV_VARIANT_TYPE2 = 2;
	public static final int CSV_VARIANT_TYPE3 = 3;
	public static final int CSV_VARIANT_TYPE4 = 4;
	public static final int CSV_VARIANT_TYPE5 = 5;
	public static final int CSV_VARIANT_TYPE6 = 6;
	public static final String DEFAULT_EFFECTIVE_DATE = "01/01/2014";
	public static final String DEFAULT_EXPIRY_DATE = "12/31/2014";
	public static final String DEFAULT_RATE_EFFECTIVE_DATE = "2014-01-01";
	public static final String DEFAULT_RATE_EXPIRY_DATE = "2014-12-31";
	public static final String HUNDRED_PERCENT = "100%";
	public static final String INDIVIDUAL = "Individual";
	public static final String ZERO_DOLLAR = "$0";
	public static final String ZERO_PERCENT = "0%";
	public static final String INTEGRATED_WITH_MED = "Included in Medical";
	public static final String MEDICAL_EHB_DEDUCTIBLE = "Medical EHB Deductible";
	public static final String DRUG_EHB_DEDUCTIBLE = "Drug EHB Deductible";
	public static final String COMBINED_EHB_DEDUCTIBLE = "Combined Medical and Drug EHB Deductible";
	public static final String COMBINED_MOOP = "Maximum Out of Pocket for Medical and Drug EHB Benefits (Total)";
	public static final String MEDICAL_EHB_MOOP = "Maximum Out of Pocket for Medical EHB Benefits";
	public static final String DRUG_EHB_MOOP = "Maximum Out of Pocket for Drug EHB Benefits";
	public static final String NOT_APPLICABLE = "Not Applicable";
	public static final String NO_DATA = "N/A";
	public static final String NO_DATA_TEXT = "No Data";
	public static final String SILVER_PLAN = "Silver";
	private static final int RATING_AREA_INDEX = 1;
	private static final int AGE_INDEX = 2;
	private static final int PREMIUM_INDEX = 3;
	private static final int TOBACCO_PREMIUM_INDEX = 4;
	public static final String TOBACCO_OPTION = "Tobacco User/Non-Tobacco User";
	public static final String MSG_ISSUER_HIOD_ID = " issuerHIOSId: ";
	// private static final double TOBACCO_FACTOR = 1.8;
	private static final String NO_CHARGE = "No Charge";
	private static final String ON_THE_EXCHANGE = "On the Exchange";
	private PUFDataHolder data;
	private IPufHealthRepository iPufHealthRepository;
	private IPufRatesRepository iPufRatesRepository;
	private IZipCodeRepository iZipCodeRepository;
	private static final Logger LOGGER = Logger.getLogger(PUFAdapter.class);
	private static final DecimalFormat RATE_FORMATTER = new DecimalFormat("#.##");

	enum VariantType {
		CSV01("-01", CSV_VARIANT_TYPE1), CSV02("-02", CSV_VARIANT_TYPE2), CSV03("-03", CSV_VARIANT_TYPE3), CSV04("-04",
				CSV_VARIANT_TYPE4), CSV05("-05", CSV_VARIANT_TYPE5), CSV06("-06", CSV_VARIANT_TYPE6);
		private String csvSuffix;
		private int csvType;

		private VariantType(String csvSuffix, int csvType) {
			this.csvSuffix = csvSuffix;
			this.csvType = csvType;
		}

		public String getCsvSuffix() {
			return csvSuffix;
		}

		public int getCsvType() {
			return csvType;
		}
	}

	enum Benefits {
		PRIMARY_CARE_PHYSICIAN("Primary Care Visit to Treat an Injury or Illness"), SPECIALIST("Specialist Visit"), EMERGENCY_ROOM(
				"Emergency Room Services"), INPATIENT_FACILITY("Inpatient Hospital Services (e.g., Hospital Stay)"), INPATIENT_PHYSICIAN(
				"Inpatient Physician"), GENERIC_DRUGS("Generic Drugs"), PREFERRED_BRAND_DRUGS("Preferred Brand Drugs"), NON_PREFERRED_BRAND_DRUGS(
				"Non-Preferred Brand Drugs"), SPECIALTY_DRUGS("Specialty Drugs");
		private String benefitName;

		private Benefits(String name) {
			this.benefitName = name;
		}

		/**
		 * @return the benefitName
		 */
		public String getBenefitName() {
			return benefitName;
		}
	}

	@Override
	public List<NetworkVO> getNetworkList(IssuerVO issuerVO) {
		LOGGER.info("Getting Network List for issuer HIOS ID " + issuerVO.getIssuerHIOSId());
		List<NetworkVO> networks = new ArrayList<NetworkVO>();
		NetworkVO network = new NetworkVO();
		network.setMarketingName(issuerVO.getDefaultMarketingName());
		network.setNetworkID(issuerVO.getIssuerStateCode() + DEFAULT_NETWORK_SUFFIX);
		// network.setRoleOfOrganizationReference();
		network.setWebsiteURI(SerffUtils.checkAndcorrectURL(issuerVO.getDefaultNetworkURL()));
		networks.add(network);
		return networks;
	}

	@Override
	public List<IssuerVO> getIssuerList() {
		List<IssuerVO> issuers = new ArrayList<IssuerVO>();
		if (data != null) {
			LOGGER.info("Getting Issuer List ...");
			List<String[]> issuerInfo = data.getIssuerInfoList();
			if (!issuerInfo.isEmpty()) {
				for (String[] info : issuerInfo) {
					LOGGER.debug("Adding Issuer to List - HIOS ID: " + info[ISSUER_INFO_HIOSID_INDEX]);
					IssuerVO issuer = new IssuerVO();
					issuer.setIssuerHIOSId(info[ISSUER_INFO_HIOSID_INDEX]);
					issuer.setIssuerStateCode(info[ISSUER_INFO_STATECODE_INDEX]);
					issuer.setIssuerTinCode(info[ISSUER_INFO_TIN_INDEX]);
					issuer.setDefaultMarketingName(info[ISSUER_INFO_NAME_INDEX]);
					issuer.setDefaultNetworkURL(info[ISSUER_INFO_NETWORK_URL_INDEX]);
					issuers.add(issuer);
				}
				LOGGER.info("Issuer List created with " + issuers.size() + " records.");
			}
		} else {
			LOGGER.error("Issuer List cannot be created, data is not loaded yet");
		}
		return issuers;
	}

	@Override
	public List<ServiceAreaVO> getServiceAreaList(String issuerHIOSId, String issuerState) {
		List<ServiceAreaVO> svcAreaList = null;
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Getting Service Area List for HiosID / State: " + SecurityUtil.sanitizeForLogging(issuerHIOSId + issuerState));
		}
		if (issuerHIOSId != null && issuerState != null) {
			String stateName = SerffAdapterUtil.STATE_CODE_NAME_MAP.get(issuerState.toUpperCase());
			if (stateName != null) {
				svcAreaList = new ArrayList<ServiceAreaVO>();
				List<ZipCode> zipCodeList = iZipCodeRepository.findByState(issuerState.toUpperCase());
				Map<String, String> countyNameCodeMap = new HashMap<String, String>();
				for (ZipCode zipCode : zipCodeList) {
					countyNameCodeMap.put(zipCode.getCounty().toUpperCase(), zipCode.getCountyFips());
				}
				List<String> countyList = iPufHealthRepository.getServiceAreaDetailsForIssuer(issuerHIOSId
						+ issuerState.toUpperCase());
				for (String county : countyList) {
					if (countyNameCodeMap.containsKey(county)) {
						ServiceAreaVO svcArea = new ServiceAreaVO();
						svcArea.setEntireState(false);
						svcArea.setPartialCounty(false);
						svcArea.setServiceAreaId(issuerState + DEFAULT_SERVICEAREA_SUFFIX);
						svcArea.setServiceAreaName(issuerState + " PUF Area");
						svcArea.setCountyCode(countyNameCodeMap.get(county));
						svcAreaList.add(svcArea);
					}
				}
			} else {
				LOGGER.error("Service Area List cannot be created, StateName not found for code " + SecurityUtil.sanitizeForLogging(issuerState));
			}
		} else {
			LOGGER.error("Invalid parameters HiosID / State: " + SecurityUtil.sanitizeForLogging(issuerHIOSId + issuerState));
		}
		return svcAreaList;
	}

	@Override
	public List<PlanRateVO> getPlanRatesList(String issuerHIOSId, String planId) {
		List<PlanRateVO> rateList = null;
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Getting Rate List for planId " + SecurityUtil.sanitizeForLogging(planId));
		}
		if (issuerHIOSId != null && planId != null) {
			List<Object[]> pufRatesList = iPufRatesRepository.getPlanRatesForIssuerPlanId(planId);
			// System.out.println("Rates size " + pufRatesList.size() +
			// " planId " + planId );

			if (pufRatesList != null) {
				rateList = new ArrayList<PlanRateVO>();
				double primaryEnrollee, primaryEnrolleeTobacco;
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Got " + pufRatesList.size() + " records for plan " + SecurityUtil.sanitizeForLogging(planId));
				}
				for (Object[] pufRateVO : pufRatesList) {
					
					if (null == pufRateVO) {
						continue;
					}
					primaryEnrollee = Double.parseDouble(RATE_FORMATTER.format(pufRateVO[PREMIUM_INDEX]));
					primaryEnrolleeTobacco = Double
							.parseDouble(RATE_FORMATTER.format(pufRateVO[TOBACCO_PREMIUM_INDEX]));
					PlanRateVO rate = new PlanRateVO();
					rate.setAgeNumber((String) pufRateVO[AGE_INDEX]);
					rate.setEffectiveDate(DEFAULT_RATE_EFFECTIVE_DATE);
					rate.setExpirationDate(DEFAULT_RATE_EXPIRY_DATE);
					rate.setFamilyOption(false);
					rate.setPlanId(planId);
					rate.setPrimaryEnrollee(primaryEnrollee);
					rate.setPrimaryEnrolleeTobacco(primaryEnrolleeTobacco);
					rate.setRateAreaId((String) pufRateVO[RATING_AREA_INDEX]);
					rate.setTobacco(TOBACCO_OPTION);
					rateList.add(rate);
				}
			}
		}
		return rateList;
	}

	@Override
	public List<String> getPlanIdList(String issuerHIOSId) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Getting PlanId List for HiosID " + SecurityUtil.sanitizeForLogging(issuerHIOSId));
		}
		return data.getPlanIdList(issuerHIOSId);
	}

	@Override
	public List<BenefitVO> getPlanBenefitsList(String issuerHIOSId, String planVariantId) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Getting Benefits List for planId " + SecurityUtil.sanitizeForLogging(planVariantId));
		}
		List<BenefitVO> benefitList = null;
		if (issuerHIOSId != null && planVariantId != null) {
			String[] planIdComp = planVariantId.split(SerffConstants.HYPHEN);
			if (planIdComp.length == 2) {
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Plan Variant " + SecurityUtil.sanitizeForLogging(planIdComp[1]));
				}
				PufHealthVO planOverview = data.getPlanAttributeDetails(issuerHIOSId, planIdComp[0]);
				if (planOverview != null) {
					benefitList = new ArrayList<BenefitVO>();
					int variant = Integer.parseInt(planIdComp[1]);
					switch (variant) {
					case CSV_VARIANT_TYPE1:
						captureBenefits(benefitList, planOverview.getPrimaryPhysicianStandard(),
								planOverview.getSpecialistStandard(), planOverview.getEmergencyRoomStandard(),
								planOverview.getInpatientFacilityStandard(),
								planOverview.getInpatientPhysicianStandard(), planOverview.getGenericdrugsStandard(),
								planOverview.getPreferredBrandDrugsStandard(),
								planOverview.getNonprefrrdbranddrugsStandard(),
								planOverview.getSpecialtyDrugsStandard());
						break;
					case CSV_VARIANT_TYPE4:
						captureBenefits(benefitList, planOverview.getPrimaryPhysician73Pct(),
								planOverview.getSpecialist73Pct(), planOverview.getEmergencyRoom73Pct(),
								planOverview.getInpatientFacility73Pct(), planOverview.getInpatientPhysician73Pct(),
								planOverview.getGenericDrugs73Pct(), planOverview.getPreferredbranddrugs73Pct(),
								planOverview.getNonprefrrdbranddrugs73Pct(), planOverview.getSpecialtyDrugs73Pct());
						break;
					case CSV_VARIANT_TYPE5:
						captureBenefits(benefitList, planOverview.getPrimaryPhysician87Pct(),
								planOverview.getSpecialist87Pct(), planOverview.getEmergencyRoom87Pct(),
								planOverview.getInpatientFacility87Pct(), planOverview.getInpatientPhysician87Pct(),
								planOverview.getGenericDrugs87Pct(), planOverview.getPreferredbranddrugs87Pct(),
								planOverview.getNonprefrrdbranddrugs87Pct(), planOverview.getSpecialtyDrugs87Pct());
						break;
					case CSV_VARIANT_TYPE6:
						captureBenefits(benefitList, planOverview.getPrimaryPhysician94Pct(),
								planOverview.getSpecialist94Pct(), planOverview.getEmergencyRoom94Pct(),
								planOverview.getInpatientFacility94Pct(), planOverview.getInpatientPhysician94Pct(),
								planOverview.getGenericDrugs94Pct(), planOverview.getPreferredbranddrugs94Pct(),
								planOverview.getNonprefrrdbranddrugs94Pct(), planOverview.getSpecialtyDrugs94Pct());
						break;
					default:
						break;
					}
				}
			}
			//Fixed the HP FOD Null dereference issue
			if (!CollectionUtils.isEmpty(benefitList)) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Created Benefits List for planId " + SecurityUtil.sanitizeForLogging(planVariantId) + " Count: " + benefitList.size());
				}
			}
			else{
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Benefit List for following planId is null " + SecurityUtil.sanitizeForLogging(planVariantId));
				}
			}
		} else {
			LOGGER.error("Could not get Benefits List for planId " + SecurityUtil.sanitizeForLogging(planVariantId + " HIOSID: " + issuerHIOSId));
		}
		return benefitList;
	}

	private void captureBenefits(List<BenefitVO> benefitList, String primaryCarePhysician, String specialist,
			String emergencyRoom, String inpatientFacility, String inpatientPhysician, String genericDrugs,
			String prefBrandDrugs, String nonPrefBrandDrugs, String specialtyDrugs) {
		LOGGER.debug("Capturing benefits start with count: " + benefitList.size());
		benefitList.add(createBenefit(Benefits.PRIMARY_CARE_PHYSICIAN, primaryCarePhysician));
		benefitList.add(createBenefit(Benefits.SPECIALIST, specialist));
		benefitList.add(createBenefit(Benefits.EMERGENCY_ROOM, emergencyRoom));
		benefitList.add(createBenefit(Benefits.INPATIENT_FACILITY, inpatientFacility));
		benefitList.add(createBenefit(Benefits.INPATIENT_PHYSICIAN, inpatientPhysician));
		benefitList.add(createBenefit(Benefits.GENERIC_DRUGS, genericDrugs));
		benefitList.add(createBenefit(Benefits.PREFERRED_BRAND_DRUGS, prefBrandDrugs));
		benefitList.add(createBenefit(Benefits.NON_PREFERRED_BRAND_DRUGS, nonPrefBrandDrugs));
		benefitList.add(createBenefit(Benefits.SPECIALTY_DRUGS, specialtyDrugs));
		LOGGER.debug("Capturing benefits end with count: " + benefitList.size());
	}

	private BenefitVO createBenefit(Benefits benefitEnum, String value) {
		LOGGER.debug("Creating benefit: " + benefitEnum.getBenefitName() + " value: " + value);
		BenefitVO benefit = new BenefitVO();
		benefit.setVisitType(benefitEnum.getBenefitName());
		benefit.setBenefitTypeCode(benefitEnum.getBenefitName());
		if (value != null && !value.trim().equalsIgnoreCase(SerffConstants.BENEFIT_NOT_COVERED)) {
			String benefitValue = value.trim();
			if (benefitValue.startsWith(SerffConstants.DOLLARSIGN)) {
				benefit.setCopayInNetworkTier1(benefitValue);
				benefit.setCoInsuranceInNetworkTier1(NO_CHARGE);
			} else if (benefitValue.contains(SerffConstants.PERCENTAGE)) {
				benefit.setCopayInNetworkTier1(NO_CHARGE);
				benefit.setCoInsuranceInNetworkTier1(benefitValue);
			} else {
				benefit.setCoInsuranceInNetworkTier1(NO_CHARGE);
				benefit.setCopayInNetworkTier1(NO_CHARGE);
				LOGGER.info("Benefit Value does not have $ or % so assuming it to be NO Charge - " + benefitEnum.getBenefitName() + " value: " + value);
			}
			benefit.setCopayOutOfNetwork(NO_CHARGE); // Setting default value
			benefit.setCoInsuranceOutOfNetwork(NO_CHARGE); // Setting default value
			benefit.setSubjectToDeductibleTier1(NO_DATA); // Setting default value
			benefit.setSubjectToDeductibleTier2(NO_DATA); // Setting default value
			benefit.setIsBenefitCovered(SerffAdapterUtil.BENEFIT_COVERED);
			benefit.setExcludedInNetworkMOOP(NO_DATA); // Setting default value
			benefit.setExcludedOutOfNetworkMOOP(NO_DATA); // Setting default value
			// String limit = planBenefit.getLimitationsINN();
			// String deductibleApplyInNetwork = planBenefit.getIsDeductibleAppliesINN();
			// String deductibleApplyOutOfNetwork = planBenefit.getIsDeductibleAppliesOON();
		} else {
			LOGGER.debug("Setting benefit " + benefitEnum.getBenefitName() + " as Not Covered");
			benefit.setIsBenefitCovered(SerffConstants.BENEFIT_NOT_COVERED);
		}
		return benefit;
	}

	@Override
	public PlanAttributeVO getPlanAttributes(String issuerHIOSId, String issuerState, String planId) {
		PlanAttributeVO planAttributeVO = null;
		if (issuerHIOSId != null && planId != null) {
			PufHealthVO planOverview = data.getPlanAttributeDetails(issuerHIOSId, planId);
			if (planOverview != null) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Setting plan attributes for planid " + SecurityUtil.sanitizeForLogging(planId));
				}
				planAttributeVO = new PlanAttributeVO();
				planAttributeVO.setStandardComponentID(planId);
				planAttributeVO.setServiceAreaID(issuerState + DEFAULT_SERVICEAREA_SUFFIX);
				planAttributeVO.setPlanMarketingName(planOverview.getPlanMarketingName());
				planAttributeVO.setDentalPlanOnlyInd(SerffAdapterUtil.NO_STRING);
				planAttributeVO.setPlanType(planOverview.getPlanType());
				planAttributeVO.setPlanEffectiveDate(DEFAULT_EFFECTIVE_DATE);
				planAttributeVO.setNetworkID(issuerState + DEFAULT_NETWORK_SUFFIX);
				planAttributeVO.setFormularyID(issuerState + DEFAULT_FORMULARY_SUFFIX);
				planAttributeVO.setHsaEligibility(SerffAdapterUtil.YES_STRING);
				planAttributeVO.setHsaEligibility(NO_DATA); // added new option
															// : no-data
				planAttributeVO.setHiosProductID(planId.substring(0, HIOS_PRODUCT_ID_LEN));
				planAttributeVO.setMetalLevel(planOverview.getMetalLevel());
				planAttributeVO.setUniquePlanDesign(SerffAdapterUtil.NO_STRING);
				planAttributeVO.setQhpOrNonQhp(ON_THE_EXCHANGE);
				planAttributeVO.setInsurancePlanPregnancyNoticeReqInd(NO_DATA); // added new option : no-data
				planAttributeVO.setIsSpecialistReferralRequired(NO_DATA); // added new option : no-data
				planAttributeVO.setOutOfCountryCoverage(NO_DATA); // added new option : no-data
				planAttributeVO.setOutOfServiceAreaCoverage(NO_DATA); // added new option : no-data
				planAttributeVO.setIsWellnessProgramOffered(NO_DATA); // added new option : no-data
				planAttributeVO.setIsDiseaseMgmtProgramsOffered(NO_DATA); // added new option : no-data
				planAttributeVO.setChildOnlyOffering(planOverview.getChildOnlyOffering());
				planAttributeVO.setMarketCoverage(INDIVIDUAL);
				planAttributeVO.setNationalNetwork(NO_DATA); // added new option : no-data
				/*
				 * if(specialistReferralsRequired.equalsIgnoreCase(SerffAdapterUtil
				 * .YES_STRING)) {
				 * planAttributeVO.setHealthCareSpecialistReferralType
				 * (SEE_PLAN_BROCHURE); // -- default null [Row 244] }
				 */
				// planAttributeVO.setMaximumCoinsuranceForSpecialtyDrugs() --
				// default null [Row 246]
				// planAttributeVO.setMaxNumDaysForChargingInpatientCopay()--
				// default null [Row 247]
				// planAttributeVO.setBeginPrimaryCareCostSharingAfterSetNumberVisits()
				// -- default null [Row 248]
				// planAttributeVO.setBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays()
				// -- default null [Row 249]
				// planAttributeVO.set() -- default null [Row 246]
				// planAttributeVO.setSummaryBenefitAndCoverageURL() -- default
				// null [Row 229]
				// planAttributeVO.setEnrollmentPaymentURL() -- default null
				// [Row 230]
			} else {
				LOGGER.error("Failed to get plan details for planid " + SecurityUtil.sanitizeForLogging(planId + MSG_ISSUER_HIOD_ID + issuerHIOSId
						+ " State: " + issuerState));
			}
		} else {
			LOGGER.error("Could not Set plan attributes for planid " + SecurityUtil.sanitizeForLogging(planId + MSG_ISSUER_HIOD_ID + issuerHIOSId
					+ " State: " + issuerState));
		}
		return planAttributeVO;
	}

	@Override
	public List<CostShareVarianceVO> getCostShareVarianceForPlan(String issuerHIOSId, String planId) {
		List<CostShareVarianceVO> costShareVarianceList = null;
		if (issuerHIOSId != null && planId != null) {
			costShareVarianceList = new ArrayList<CostShareVarianceVO>();
			PufHealthVO planOverview = data.getPlanAttributeDetails(issuerHIOSId, planId);
			if (planOverview != null) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Setting CostShareVariance for planid " + SecurityUtil.sanitizeForLogging(planId));
				}
				CostShareVarianceVO csv = createCSV(planOverview, VariantType.CSV01);
				costShareVarianceList.add(csv);
				if (SILVER_PLAN.equalsIgnoreCase(planOverview.getMetalLevel())) {
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Setting CostShareVariance for Silver plan - planid " + SecurityUtil.sanitizeForLogging(planId));
					}
					csv = createCSV(planOverview, VariantType.CSV04);
					costShareVarianceList.add(csv);
					csv = createCSV(planOverview, VariantType.CSV05);
					costShareVarianceList.add(csv);
					csv = createCSV(planOverview, VariantType.CSV06);
					costShareVarianceList.add(csv);
				}
			} else {
				LOGGER.error("Failed to get plan details for planid " + SecurityUtil.sanitizeForLogging(planId + MSG_ISSUER_HIOD_ID + issuerHIOSId));
			}
		} else {
			LOGGER.error("Could not Set CostShareVariance for planid " + SecurityUtil.sanitizeForLogging(planId + MSG_ISSUER_HIOD_ID + issuerHIOSId));
		}
		return costShareVarianceList;
	}

	private CostShareVarianceVO createCSV(PufHealthVO planOverview, VariantType variant) {
		CostShareVarianceVO csv = null;
		if (planOverview != null) {
			LOGGER.info("Creating CostShareVariance object for planid " + planOverview.getPlanIdStandardComponent());
			csv = new CostShareVarianceVO();
			csv.setMultipleProviderTiers(SerffAdapterUtil.NO_STRING);
			csv.setFirstTierUtilization(HUNDRED_PERCENT);
			csv.setMetalLevel(planOverview.getMetalLevel());
			csv.setPlanMarketingName(planOverview.getPlanMarketingName());
			// csv.setCsrVariationType(planOverview.getVariantType());
			csv.setPlanId(planOverview.getPlanIdStandardComponent() + variant.csvSuffix);
			String drugDeductibleIndv = null;
			String drugDeductibleFmly = null;
			String medDeductibleIndv = null;
			String medDeductibleFmly = null;
			String drugMoopIndv = null;
			String drugMoopFmly = null;
			String medMoopIndv = null;
			String medMoopFmly = null;
			boolean dedCombined = false, moopCombined = false;
			switch (variant) {
			case CSV01:
				LOGGER.debug("CostShareVariance requested is 01");
				drugDeductibleIndv = planOverview.getDrugDeductibleIStandard();
				drugDeductibleFmly = planOverview.getDrugDeductibleFStandard();
				medDeductibleIndv = planOverview.getMedicalDeductibleIStandard();
				medDeductibleFmly = planOverview.getMedicalDeductibleFStandard();
				drugMoopIndv = planOverview.getDrugMaxoutfpcktIStandard();
				drugMoopFmly = planOverview.getDrugMaxoutfpcktFStandard();
				medMoopIndv = planOverview.getMedicalMaxoutfpcktIStandard();
				medMoopFmly = planOverview.getMedicalMaxoutfpcktFStandard();
				break;
			case CSV02:
				LOGGER.debug("CostShareVariance requested is 02, but no values to set");
				break;
			case CSV03:
				LOGGER.debug("CostShareVariance requested is 03, but no values to set");
				break;
			case CSV04:
				LOGGER.debug("CostShareVariance requested is 04");
				drugDeductibleIndv = planOverview.getDrugDeductibleI73Pct();
				drugDeductibleFmly = planOverview.getDrugDeductibleF73Pct();
				medDeductibleIndv = planOverview.getMedicalDeductibleI73Pct();
				medDeductibleFmly = planOverview.getMedicalDeductibleF73Pct();
				drugMoopIndv = planOverview.getDrugMaxoutfpcktI73Pct();
				drugMoopFmly = planOverview.getDrugMaxoutfpcktF73Pct();
				medMoopIndv = planOverview.getMedicalMaxoutfpcktI73Pct();
				medMoopFmly = planOverview.getMedicalMaxoutfpcktF73Pct();
				break;
			case CSV05:
				LOGGER.debug("CostShareVariance requested is 05");
				drugDeductibleIndv = planOverview.getDrugDeductibleI87Pct();
				drugDeductibleFmly = planOverview.getDrugDeductibleF87Pct();
				medDeductibleIndv = planOverview.getMedicalDeductibleI87Pct();
				medDeductibleFmly = planOverview.getMedicalDeductibleF87Pct();
				drugMoopIndv = planOverview.getDrugMaxoutfpcktI87Pct();
				drugMoopFmly = planOverview.getDrugMaxoutfpcktF87Pct();
				medMoopIndv = planOverview.getMedicalMaxoutfpcktI87Pct();
				medMoopFmly = planOverview.getMedicalMaxoutfpcktF87Pct();
				break;
			case CSV06:
				LOGGER.debug("CostShareVariance requested is 06");
				drugDeductibleIndv = planOverview.getDrugDeductibleI94Pct();
				drugDeductibleFmly = planOverview.getDrugDeductibleF94Pct();
				medDeductibleIndv = planOverview.getMedicalDeductibleI94Pct();
				medDeductibleFmly = planOverview.getMedicalDeductibleF94Pct();
				drugMoopIndv = planOverview.getDrugMaxoutfpcktI94Pct();
				drugMoopFmly = planOverview.getDrugMaxoutfpcktF94Pct();
				medMoopIndv = planOverview.getMedicalMaxoutfpcktI94Pct();
				medMoopFmly = planOverview.getMedicalMaxoutfpcktF94Pct();
				break;
				
			default : LOGGER.info("CostShareVariance is out of scope");
			}
			// csv.setAvCalculatorOutputNumber(); -- default null [Row 231]
			// csv.setSecondTierUtilization() -- default null [Row 234]
			// csv.setDefaultCoInsuranceInNetwork(defaultCoInsuranceInNetwork)
			// csv.setDefaultCopayInNetwork(defaultCopayInNetwork)
			// csv.setDefaultCopayOutOfNetwork(defaultCopayOutOfNetwork)
			// csv.setIssuerActuarialValue(issuerActuarialValue) -- default null
			// [Row 204]

			if (drugDeductibleIndv != null) {
				drugDeductibleIndv = drugDeductibleIndv.trim();
				if (drugDeductibleIndv.equalsIgnoreCase(INTEGRATED_WITH_MED)) {
					LOGGER.debug("Drug Deductible is integrated with Medical");
					csv.setMedicalAndDrugDeductiblesIntegrated(SerffAdapterUtil.YES_STRING);
					dedCombined = true;
				} else {
					csv.setMedicalAndDrugDeductiblesIntegrated(SerffAdapterUtil.NO_STRING);
				}
			}

			if (drugMoopIndv != null && drugMoopIndv.trim().equalsIgnoreCase(INTEGRATED_WITH_MED)) {
				LOGGER.debug("Drug MOOP is integrated with Medical");
				csv.setMedicalAndDrugMaxOutOfPocketIntegrated(SerffAdapterUtil.YES_STRING);
				moopCombined = true;
			} else {
				csv.setMedicalAndDrugMaxOutOfPocketIntegrated(SerffAdapterUtil.NO_STRING);
			}
			setDeductiblesForCSV(csv, drugDeductibleIndv, drugDeductibleFmly, medDeductibleIndv, medDeductibleFmly,
					dedCombined);
			setMoopForCSV(csv, drugMoopIndv, drugMoopFmly, medMoopIndv, medMoopFmly, moopCombined);
		}
		return csv;
	}

	private void setMoopForCSV(CostShareVarianceVO csv, String drugMoopIndv, String drugMoopFmly, String medMoopIndv,
			String medMoopFmly, boolean moopCombined) {
		LOGGER.debug("Creating MOOP for CSV " + csv.getPlanId());
		DeductibleVO medMoop = new DeductibleVO();
		if (moopCombined) {
			medMoop.setName(COMBINED_MOOP);
		} else {
			LOGGER.debug("Drug MOOP values " + drugMoopIndv + " and Fmly: " + drugMoopFmly);
			medMoop.setName(MEDICAL_EHB_MOOP);
			DeductibleVO drugMoop = new DeductibleVO();
			drugMoop.setName(DRUG_EHB_MOOP);
			drugMoop.setInNetworkTier1Individual(drugMoopIndv);
			drugMoop.setInNetworkTier1Family(drugMoopFmly);
			drugMoop.setOutOfNetworkIndividual(NOT_APPLICABLE);
			drugMoop.setOutOfNetworkFamily(NOT_APPLICABLE);
			csv.getMoopList().add(drugMoop);
		}
		LOGGER.debug("Medical MOOP values " + medMoopIndv + " and " + medMoopFmly);
		medMoop.setInNetworkTier1Individual(medMoopIndv);
		medMoop.setInNetworkTier1Family(medMoopFmly);
		medMoop.setOutOfNetworkIndividual(NOT_APPLICABLE);
		medMoop.setOutOfNetworkFamily(NOT_APPLICABLE);
		csv.getMoopList().add(medMoop);
	}

	private void setDeductiblesForCSV(CostShareVarianceVO csv, String drugDeductibleIndv, String drugDeductibleFmly,
			String medDeductibleIndv, String medDeductibleFmly, boolean dedCombined) {
		LOGGER.debug("Creating Deductible for CSV " + csv.getPlanId());
		DeductibleVO medDudct = new DeductibleVO();
		if (dedCombined) {
			medDudct.setName(COMBINED_EHB_DEDUCTIBLE);
			medDudct.setDeductibleType(COMBINED_EHB_DEDUCTIBLE);
		} else {
			LOGGER.debug("Drug Deductible values " + drugDeductibleIndv + " and " + drugDeductibleFmly);
			medDudct.setName(MEDICAL_EHB_DEDUCTIBLE);
			medDudct.setDeductibleType(MEDICAL_EHB_DEDUCTIBLE);
			DeductibleVO drugDudct = new DeductibleVO();
			drugDudct.setName(DRUG_EHB_DEDUCTIBLE);
			drugDudct.setDeductibleType(DRUG_EHB_DEDUCTIBLE);
			drugDudct.setInNetworkTier1Individual(drugDeductibleIndv);
			drugDudct.setInNetworkTier1Family(drugDeductibleFmly);
			drugDudct.setOutOfNetworkIndividual(NOT_APPLICABLE);
			drugDudct.setOutOfNetworkFamily(NOT_APPLICABLE);
			drugDudct.setCombinedInOutNetworkIndividual(NOT_APPLICABLE);
			drugDudct.setCombinedInOutNetworkFamily(NOT_APPLICABLE);
			csv.getPlanDeductibleList().add(drugDudct);
		}
		LOGGER.debug("Medical Deductible values " + medDeductibleIndv + " and " + medDeductibleFmly);
		medDudct.setInNetworkTier1Individual(medDeductibleIndv);
		medDudct.setInNetworkTier1Family(medDeductibleFmly);
		medDudct.setOutOfNetworkIndividual(NOT_APPLICABLE);
		medDudct.setOutOfNetworkFamily(NOT_APPLICABLE);
		medDudct.setCombinedInOutNetworkIndividual(NOT_APPLICABLE);
		medDudct.setCombinedInOutNetworkFamily(NOT_APPLICABLE);
		csv.getPlanDeductibleList().add(medDudct);
	}

	public boolean loadTemplateData(String planPrefix, IPufHealthRepository iPufHealthRepository,
			IPufRatesRepository iPufRatesRepository, IZipCodeRepository iZipCodeRepository) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Loading PUF data for plan prefix " + SecurityUtil.sanitizeForLogging(planPrefix));
		}
		this.iPufHealthRepository = iPufHealthRepository;
		this.iPufRatesRepository = iPufRatesRepository;
		this.iZipCodeRepository = iZipCodeRepository;
		return loadTemplateData(planPrefix);
	}

	@Override
	public boolean loadTemplateData(String planPrefix) {
		List<PufHealthVO> pufPlansList = iPufHealthRepository.getPlanDetailsForPlanID(planPrefix);
		if (pufPlansList != null && !pufPlansList.isEmpty()) {
			data = new PUFDataHolder();
			// System.out.println(pufPlansList.size());
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("PUF data found for planPrefix " + SecurityUtil.sanitizeForLogging(planPrefix));
			}
			for (PufHealthVO pufHealthVO : pufPlansList) {
				String planId = pufHealthVO.getPlanIdStandardComponent();
				if (planId != null) {
					data.addPufQHPToMap(pufHealthVO, planId.substring(0, SerffConstants.LEN_HIOS_ISSUER_ID));
				}
			}
		} else {
			LOGGER.error("No PUF data found for planPrefix " + SecurityUtil.sanitizeForLogging(planPrefix));
		}
		return true;
	}

}
