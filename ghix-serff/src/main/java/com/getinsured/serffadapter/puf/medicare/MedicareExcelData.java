package com.getinsured.serffadapter.puf.medicare;

import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_AD;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_RX;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_SP;

import org.apache.log4j.Logger;

/**
 * Class is used to generate Enum on the basis of Plans Info, Plan Benefits, Other Services
 * and Service Area sheets of Medicaid Excel.
 * 
 * @author Bhavin Parmar
 * @since Apr 15, 2015
 */
public class MedicareExcelData {

	private static final Logger LOGGER = Logger.getLogger(MedicareExcelData.class);

	public static final int START_ROW_MA = 6;
	public static final int START_ROW_SP = 5;
	public static final int START_ROW_RX = 4;
	public static final int INVALID_INDEX = -1;
//	private static final int NUMBER_TYPE = 0;
	private static final int FLOAT_TYPE = 1;
	private static final int STRING_TYPE = 2;

	public enum MedicareColumnEnum {

		STATE("STATE"),
		COUNTY("COUNTY"),
		ORGANIZATION_NAME("ORGANIZATION_NAME"),
		PLAN_NAME("PLAN_NAME"),
		MEDICARE_TYPE("MEDICARE_TYPE"),
		MONTHLY_CONSOLIDATED_PREMIUM("MONTHLY_CONSOLIDATED_PREMIUM"),
		ANNUAL_DRUG_DEDUCTIBLE("ANNUAL_DRUG_DEDUCTIBLE"),
		DRUG_BENEFIT_TYPE("DRUG_BENEFIT_TYPE"),
		ADDITIONAL_COVERAGE_OFFERED_IN_GAP("ADDITIONAL_COVERAGE_OFFERED_IN_GAP"),
		DRUG_BENEFIT_TYPE_DETAIL("DRUG_BENEFIT_TYPE_DETAIL"),
		CONTRACT_ID("CONTRACT_ID"),
		PLAN_ID("PLAN_ID"),
		SEGMENT_ID("SEGMENT_ID"),
		IN_NETWORK_MOOP_AMT("IN_NETWORK_MOOP_AMT"), // In MA only
		OVERALL_STAR_RATING("OVERALL_STAR_RATING"),
		BENEFIT_TYPE("BENEFIT_TYPE"),
		PREMIUM_LOW_INCOME_SUBSIDY("PREMIUM_LOW_INCOME_SUBSIDY"),
		BENEFIT_TYPE_DETAIL("BENEFIT_TYPE_DETAIL"),
		SPECIAL_NEEDS_PLAN_TYPE("SPECIAL_NEEDS_PLAN_TYPE"), // In SNP only
		CARDIOVASCULAR_DISORDERS("CARDIOVASCULAR_DISORDERS"),
		CHRONIC_HEART_FAILURE("CHRONIC_HEART_FAILURE"),
		DEMENTIA("DEMENTIA"),
		DIABETES_MELLITUS("DIABETES_MELLITUS"),
		END_STAGE_RENAL_DISEASE("END_STAGE_RENAL_DISEASE"),
		HIV_AIDS("HIV_AIDS"),
		CHRONIC_LUNG_DISORDERS("CHRONIC_LUNG_DISORDERS"),
		CHRONIC_AND_DISABLING_MENTAL_HEALTH("CHRONIC_AND_DISABLING_MENTAL_HEALTH"),
		CARDIOVASCULAR_AND_CHRONIC_HEART_FAILURE("CARDIOVASCULAR_AND_CHRONIC_HEART_FAILURE"),
		CARDIOVASCULAR_DISORDERS_AND_DIABETES("CARDIOVASCULAR_DISORDERS_AND_DIABETES"),
		CHRONIC_HEART_FAILURE_AND_DIABETES("CHRONIC_HEART_FAILURE_AND_DIABETES"),
		CARDIOVASCULAR_CHRONIC_AND_DIABETES("CARDIOVASCULAR_CHRONIC_AND_DIABETES");

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private MedicareColumnEnum(String columnConstName) {
			this.columnConstName = columnConstName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public String getColumnConstName(String medicareType) {

			String columnConstName = null;

			try {
				if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(medicareType)) {
					columnConstName = MedicareAdvColumnEnum.valueOf(this.columnConstName).getColumnConstName();
				}
				else if (TRANSFER_MEDICARE_SP.equalsIgnoreCase(medicareType)) {
					columnConstName = MedicareGapColumnEnum.valueOf(this.columnConstName).getColumnConstName();
				}
				else if (TRANSFER_MEDICARE_RX.equalsIgnoreCase(medicareType)) {
					columnConstName = MedicareDrugColumnEnum.valueOf(this.columnConstName).getColumnConstName();
				}
			}
			catch (IllegalArgumentException ex) {

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(ex.getMessage(), ex);
				}
				LOGGER.warn(ex.getMessage());
			}
			return columnConstName;
		}

		public String getColumnName(String medicareType) {

			try {
				if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(medicareType)) {
					columnName = MedicareAdvColumnEnum.valueOf(columnConstName).getColumnName();
				}
				else if (TRANSFER_MEDICARE_SP.equalsIgnoreCase(medicareType)) {
					columnName = MedicareGapColumnEnum.valueOf(columnConstName).getColumnName();
				}
				else if (TRANSFER_MEDICARE_RX.equalsIgnoreCase(medicareType)) {
					columnName = MedicareDrugColumnEnum.valueOf(columnConstName).getColumnName();
				}
			}
			catch (IllegalArgumentException ex) {

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(ex.getMessage(), ex);
				}
				LOGGER.warn(ex.getMessage());
			}
			return columnName;
		}

		public int getColumnIndex(String medicareType) {

			try {
				columnIndex = INVALID_INDEX;

				if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(medicareType)) {
					columnIndex = MedicareAdvColumnEnum.valueOf(columnConstName).getColumnIndex();
				}
				else if (TRANSFER_MEDICARE_SP.equalsIgnoreCase(medicareType)) {
					columnIndex = MedicareGapColumnEnum.valueOf(columnConstName).getColumnIndex();
				}
				else if (TRANSFER_MEDICARE_RX.equalsIgnoreCase(medicareType)) {
					columnIndex = MedicareDrugColumnEnum.valueOf(columnConstName).getColumnIndex();
				}
			}
			catch (IllegalArgumentException ex) {

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(ex.getMessage(), ex);
				}
				LOGGER.warn(ex.getMessage());
			}
			return columnIndex;
		}

		public int getDataType(String medicareType) {

			try {
				if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(medicareType)) {
					dataType = MedicareAdvColumnEnum.valueOf(columnConstName).getDataType();
				}
				else if (TRANSFER_MEDICARE_SP.equalsIgnoreCase(medicareType)) {
					dataType = MedicareGapColumnEnum.valueOf(columnConstName).getDataType();
				}
				else if (TRANSFER_MEDICARE_RX.equalsIgnoreCase(medicareType)) {
					dataType = MedicareDrugColumnEnum.valueOf(columnConstName).getDataType();
				}
			}
			catch (IllegalArgumentException ex) {

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(ex.getMessage(), ex);
				}
				LOGGER.warn(ex.getMessage());
			}
			return dataType;
		}
	}

	public enum MedicareAdvColumnEnum {

		STATE("State", "STATE", 0, STRING_TYPE),
		COUNTY("County", "COUNTY", 1, STRING_TYPE),
		ORGANIZATION_NAME("Organization Name", "ORGANIZATION_NAME", 2, STRING_TYPE),
		PLAN_NAME("Plan Name", "PLAN_NAME", 3, STRING_TYPE),
		MEDICARE_TYPE("Type of Medicare Health Plan", "MEDICARE_TYPE", 4, STRING_TYPE),
		MONTHLY_CONSOLIDATED_PREMIUM("Monthly Premium", "MONTHLY_CONSOLIDATED_PREMIUM", 5, FLOAT_TYPE),
		ANNUAL_DRUG_DEDUCTIBLE("Drug Deductible", "ANNUAL_DRUG_DEDUCTIBLE", 6, FLOAT_TYPE),
		DRUG_BENEFIT_TYPE("Drug Benefit Type", "DRUG_BENEFIT_TYPE", 7, STRING_TYPE),
		ADDITIONAL_COVERAGE_OFFERED_IN_GAP("Coverage Gap", "ADDITIONAL_COVERAGE_OFFERED_IN_GAP", 8, STRING_TYPE),
		DRUG_BENEFIT_TYPE_DETAIL("Drug Benefit Type Detail", "DRUG_BENEFIT_TYPE_DETAIL", 9, STRING_TYPE),
		CONTRACT_ID("Contract ID", "CONTRACT_ID", 10, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 11, STRING_TYPE),
		SEGMENT_ID("Segment ID", "SEGMENT_ID", 12, STRING_TYPE),
		IN_NETWORK_MOOP_AMT("Out-Of-Pocket Spending Limit", "IN_NETWORK_MOOP_AMT", 13, FLOAT_TYPE),
		OVERALL_STAR_RATING("Overall Plan Rating", "OVERALL_STAR_RATING", 14, STRING_TYPE),
		SPECIAL_NEEDS_PLAN_TYPE("Special Needs Plan Type", "SPECIAL_NEEDS_PLAN_TYPE", INVALID_INDEX, STRING_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private MedicareAdvColumnEnum(String columnName, String columnConstName,
				int index, int dataType) {
			this.columnName = columnName;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum MedicareDrugColumnEnum {

		STATE("State", "STATE", 0, STRING_TYPE),
		ORGANIZATION_NAME("Company Name", "COMPANY_NAME", 1, STRING_TYPE),
		PLAN_NAME("Plan Name", "PLAN_NAME", 2, STRING_TYPE),
		BENEFIT_TYPE("Benefit Type", "BENEFIT_TYPE", 3, STRING_TYPE),
		PREMIUM_LOW_INCOME_SUBSIDY("$0 Premium with Full Low-Income Subsidy?", "PREMIUM_LOW_INCOME_SUBSIDY", 4, STRING_TYPE),
		MONTHLY_CONSOLIDATED_PREMIUM("Monthly Premium", "MONTHLY_DRUG_PREMIUM", 5, FLOAT_TYPE),
		ANNUAL_DRUG_DEDUCTIBLE("Drug Deductible", "ANNUAL_DRUG_DEDUCTIBLE", 6, FLOAT_TYPE),
		ADDITIONAL_COVERAGE_OFFERED_IN_GAP("Coverage Gap", "ADDITIONAL_COVERAGE_OFFERED_IN_GAP", 7, STRING_TYPE),
		CONTRACT_ID("Contract ID", "CONTRACT_ID", 8, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 9, STRING_TYPE),
		BENEFIT_TYPE_DETAIL("Benefit Type Detail", "BENEFIT_TYPE_DETAIL", 10, STRING_TYPE),
		OVERALL_STAR_RATING("Overall Plan Rating", "SUMMARY_STAR_RATING", 11, STRING_TYPE),
		COUNTY("County", "COUNTY", INVALID_INDEX, STRING_TYPE),
		SEGMENT_ID("Segment ID", "SEGMENT_ID", INVALID_INDEX, STRING_TYPE),
		SPECIAL_NEEDS_PLAN_TYPE("Special Needs Plan Type", "SPECIAL_NEEDS_PLAN_TYPE", INVALID_INDEX, STRING_TYPE),
		IN_NETWORK_MOOP_AMT("Out-Of-Pocket Spending Limit", "IN_NETWORK_MOOP_AMT", INVALID_INDEX, FLOAT_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private MedicareDrugColumnEnum(String columnName, String columnConstName,
				int index, int dataType) {
			this.columnName = columnName;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum MedicareGapColumnEnum {

		STATE("State", "STATE", 0, STRING_TYPE),
		COUNTY("County", "COUNTY", 1, STRING_TYPE),
		ORGANIZATION_NAME("Organization Name", "ORGANIZATION_NAME", 2, STRING_TYPE),
		PLAN_NAME("Plan Name", "PLAN_NAME", 3, STRING_TYPE),
		MEDICARE_TYPE("Type of Medicare Health Plan", "MEDICARE_TYPE", 4, STRING_TYPE),
		SPECIAL_NEEDS_PLAN_TYPE("Special Needs Plan Type", "SPECIAL_NEEDS_PLAN_TYPE", 5, STRING_TYPE),
		MONTHLY_CONSOLIDATED_PREMIUM("Monthly Premium", "MONTHLY_CONSOLIDATED_PREMIUM", 6, FLOAT_TYPE),
		ANNUAL_DRUG_DEDUCTIBLE("Drug Deductible", "ANNUAL_DRUG_DEDUCTIBLE", 7, FLOAT_TYPE),
		DRUG_BENEFIT_TYPE("Drug Benefit Type", "DRUG_BENEFIT_TYPE", 8, STRING_TYPE),
		ADDITIONAL_COVERAGE_OFFERED_IN_GAP("Coverage Gap", "ADDITIONAL_COVERAGE_OFFERED_IN_GAP", 9, STRING_TYPE),
		DRUG_BENEFIT_TYPE_DETAIL("Drug Benefit Type Detail", "DRUG_BENEFIT_TYPE_DETAIL", 10, STRING_TYPE),
		CONTRACT_ID("Contract ID", "CONTRACT_ID", 11, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 12, STRING_TYPE),
		SEGMENT_ID("Segment ID", "SEGMENT_ID", 13, STRING_TYPE),
		CARDIOVASCULAR_DISORDERS("Cardiovascular Disorders", "CARDIOVASCULAR_DISORDERS", 14, STRING_TYPE),
		CHRONIC_HEART_FAILURE("Chronic Heart Failure", "CHRONIC_HEART_FAILURE", 15, STRING_TYPE),
		DEMENTIA("Dementia", "DEMENTIA", 16, STRING_TYPE),
		DIABETES_MELLITUS("Diabetes Mellitus", "DIABETES_MELLITUS", 17, STRING_TYPE),
		END_STAGE_RENAL_DISEASE("End-stage Renal Disease Requiring Dialysis (any mode of dialysis)", "END_STAGE_RENAL_DISEASE", 18, STRING_TYPE),
		HIV_AIDS("HIV/AIDS", "HIV_AIDS", 19, STRING_TYPE),
		CHRONIC_LUNG_DISORDERS("Chronic Lung Disorders", "CHRONIC_LUNG_DISORDERS", 20, STRING_TYPE),
		CHRONIC_AND_DISABLING_MENTAL_HEALTH("Chronic and Disabling Mental Health Conditions", "CHRONIC_AND_DISABLING_MENTAL_HEALTH", 21, STRING_TYPE),
		CARDIOVASCULAR_AND_CHRONIC_HEART_FAILURE("Cardiovascular Disorders and Chronic Heart Failure", "CARDIOVASCULAR_AND_CHRONIC_HEART_FAILURE", 22, STRING_TYPE),
		CARDIOVASCULAR_DISORDERS_AND_DIABETES("Cardiovascular Disorders and Diabetes", "CARDIOVASCULAR_DISORDERS_AND_DIABETES", 23, STRING_TYPE),
		CHRONIC_HEART_FAILURE_AND_DIABETES("Chronic Heart Failure and Diabetes", "CHRONIC_HEART_FAILURE_AND_DIABETES", 24, STRING_TYPE),
		CARDIOVASCULAR_CHRONIC_AND_DIABETES("Cardiovascular Disorders, Chronic Heart Failure and Diabetes", "CARDIOVASCULAR_CHRONIC_AND_DIABETES", 25, STRING_TYPE),
		OVERALL_STAR_RATING("Overall Plan Rating", "OVERALL_STAR_RATING", 26, STRING_TYPE),
		IN_NETWORK_MOOP_AMT("Out-Of-Pocket Spending Limit", "IN_NETWORK_MOOP_AMT", INVALID_INDEX, FLOAT_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private MedicareGapColumnEnum(String columnName, String columnConstName,
				int index, int dataType) {
			this.columnName = columnName;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}
}
