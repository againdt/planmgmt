package com.getinsured.serffadapter.puf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.getinsured.hix.model.serff.PufHealthVO;
import com.getinsured.hix.platform.util.SecurityUtil;

public class PUFDataHolder {
	public static final String DEFAULT_ISSUER_TIN = "00-0000000";
	private Map<String, Map<String, List<PufHealthVO>>> issuerPlansMap;
	private static final Logger LOGGER = Logger.getLogger(PUFDataHolder.class);

	public PUFDataHolder() {
		super();
		issuerPlansMap = new HashMap<String, Map<String, List<PufHealthVO>>>();
	}

	public void addPufQHPToMap(PufHealthVO pufHealthVO, String hiosId) {
		if (pufHealthVO != null && issuerPlansMap != null && hiosId != null) {
			String planId = pufHealthVO.getPlanIdStandardComponent();
			List<PufHealthVO> planDetailsList = null;
			Map<String, List<PufHealthVO>> plansMap = issuerPlansMap
					.get(hiosId);
			if (plansMap != null) {
				planDetailsList = plansMap.get(planId);
			} else {
				plansMap = new HashMap<String, List<PufHealthVO>>();
				issuerPlansMap.put(hiosId, plansMap);
			}

			if (planDetailsList == null) {
				planDetailsList = new ArrayList<PufHealthVO>();
				plansMap.put(planId, planDetailsList);
			}
			planDetailsList.add(pufHealthVO);
		}
	}

	public List<String[]> getIssuerInfoList() {
		List<String[]> issuerInfoList = new ArrayList<String[]>();
		for (Iterator<String> iterator = issuerPlansMap.keySet().iterator(); iterator
				.hasNext();) {
			String hiodId = iterator.next();
			Map<String, List<PufHealthVO>> pufPlansMap = issuerPlansMap
					.get(hiodId);
			if (pufPlansMap != null && !pufPlansMap.isEmpty()) {
				List<PufHealthVO> planDetailsList = pufPlansMap.get(pufPlansMap
						.keySet().iterator().next());
				if (planDetailsList != null && !planDetailsList.isEmpty()) {
					PufHealthVO pufHealthVO = planDetailsList.get(0);
					if (pufHealthVO != null) {
						String[] s = new String[] { hiodId,
								pufHealthVO.getState(),
								pufHealthVO.getIssuerName(), 
								pufHealthVO.getNetworkUrl(), DEFAULT_ISSUER_TIN };
						issuerInfoList.add(s);
					}
				}
			}
		}
		return issuerInfoList;
	}

	public List<String> getPlanIdList(String issuerHiosId) {
		List<String> planList = new ArrayList<String>();
		if (issuerHiosId != null) {
			Map<String, List<PufHealthVO>> pufPlansMap = issuerPlansMap
					.get(issuerHiosId);
			if (pufPlansMap != null && !pufPlansMap.isEmpty()) {
				for (Iterator<String> iterator = pufPlansMap.keySet()
						.iterator(); iterator.hasNext();) {
					planList.add(iterator.next());
				}
			}
		}
		return planList;
	}

	public PufHealthVO getPlanAttributeDetails(String issuerHIOSId,
			String planId) {
		PufHealthVO planAttrDetails = null;
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Getting Plan Attribute Details for planID: " + SecurityUtil.sanitizeForLogging(planId));
		}
		if (issuerHIOSId != null) {
			Map<String, List<PufHealthVO>> pufPlansMap = issuerPlansMap
					.get(issuerHIOSId);
			if (pufPlansMap != null && !pufPlansMap.isEmpty()) {
				List<PufHealthVO> planDetailsList = pufPlansMap.get(planId);
				if (planDetailsList != null && !planDetailsList.isEmpty()) {
					planAttrDetails = planDetailsList.get(0);
				}
			}
		}
		return planAttrDetails;
	}

	public List<PufHealthVO> getPUFPlanDetails(String issuerHIOSId,
			String planId) {
		List<PufHealthVO> planDetailsList = null;
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Getting PUF Plan Details for planID: " + SecurityUtil.sanitizeForLogging(planId));
		}
		if (issuerHIOSId != null) {
			Map<String, List<PufHealthVO>> pufPlansMap = issuerPlansMap
					.get(issuerHIOSId);
			if (pufPlansMap != null && !pufPlansMap.isEmpty()) {
				planDetailsList = pufPlansMap.get(planId);
			}
		}
		return planDetailsList;
	}
}
