package com.getinsured.serffadapter.puf.medicare;

import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.INVALID_INDEX;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.START_ROW_MA;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.START_ROW_RX;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.START_ROW_SP;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.ADDITIONAL_COVERAGE_OFFERED_IN_GAP;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.ANNUAL_DRUG_DEDUCTIBLE;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.CONTRACT_ID;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.COUNTY;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.IN_NETWORK_MOOP_AMT;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.MEDICARE_TYPE;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.MONTHLY_CONSOLIDATED_PREMIUM;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.ORGANIZATION_NAME;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.OVERALL_STAR_RATING;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.PLAN_ID;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.PLAN_NAME;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.SEGMENT_ID;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.SPECIAL_NEEDS_PLAN_TYPE;
import static com.getinsured.serffadapter.puf.medicare.MedicareExcelData.MedicareColumnEnum.STATE;
import static com.serff.util.SerffConstants.ASTERISK;
import static com.serff.util.SerffConstants.FTP_PLAN_MEDICARE_FILE;
import static com.serff.util.SerffConstants.NEGATIVE_INDEX_OF;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_AD;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_RX;
import static com.serff.util.SerffConstants.TRANSFER_MEDICARE_SP;
import static com.serff.util.SerffConstants.UNDERSCORE;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.serffadapter.excel.ExcelReader;
import com.getinsured.serffadapter.puf.medicare.MedicareVO.MedicareCode;
import com.serff.service.SerffService;
import com.serff.service.templates.MedicarePlanMgmtSerffService;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

/**
 * Class is used to read Medicare Excel data (PUF) to MedicarePlanExcelVO DTO Objects.
 * 
 * @author Bhavin Parmar
 * @since Apr 14, 2015
 */
@Service("medicarePlanReader")
public class MedicarePlanReader extends ExcelReader {

	private static final Logger LOGGER = Logger.getLogger(MedicarePlanReader.class);

	private static final String EMSG_INVALID_EXCEL = "Invalid Medicare excel template.";
	private static final String EMSG_LOAD_PLANS = "Failed to load Medicare Plans. Reason: ";
	private static final String EMSG_NO_EXCEL_FOUND = "No Medicare Excel Template is available for processing.";
	private static final int ERROR_MAX_LEN = 1000;

	private static final String PREFIX_HIOS_ISSUER_ID = "PF_";
	private static final String DEFAULT_SEGMENT_ID = "0";
	private static final String SHEET_SANCTIONED = "sanctioned";
	private static final int LEN_CONTRACT_ID = 5;
	private static final int LEN_PLAN_ID = 3;
	private static final int LEN_STATE_CODE = 2;

	@Autowired private SerffService serffService;
//	@Autowired private ContentManagementService ecmService;
	@Autowired private SerffResourceUtil serffResourceUtil;
	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private SerffUtils serffUtils;
	@Autowired private MedicarePlanMgmtSerffService medicarePlanMgmtSerffService;

	/**
	 * Method is used process uploaded Medicare Excel.
	 */
	public String processMedicareExcelData(String serffReqId) {

		LOGGER.info("processMedicareExcelData() Start");
		StringBuilder message = new StringBuilder();
		SerffPlanMgmt trackingRecord = null;
		boolean isSuccess = false;

		try {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SERFF Request ID: "+ SecurityUtil.sanitizeForLogging(serffReqId));
			}

			if (!StringUtils.isNumeric(serffReqId)) {
				message.append("No SERFF Req Id found to process Medicare Excel Template.").append(serffReqId);
				return message.toString();
			}
			String folderPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansMedicareUploadPath();
			LOGGER.debug("Medicare Plan FTP Folder Path: " + folderPath);
			// Get Tracking record from SERFF Plan Management table
			trackingRecord = getTrackingRecord(serffReqId, message);

			if (null != trackingRecord
					&& StringUtils.isNotBlank(trackingRecord.getAttachmentsList())
					&& trackingRecord.getAttachmentsList().contains(FTP_PLAN_MEDICARE_FILE)) {
				LOGGER.info("Medicare File Name: " + folderPath + "/" + trackingRecord.getAttachmentsList());
				// Get Plan Document Excel from FTP server
				isSuccess = loadExcelTemplate(trackingRecord, folderPath, message);
			}
			else if (null != trackingRecord) {
				message.append(EMSG_NO_EXCEL_FOUND + " Request:"  + serffReqId);
			}
		}
		catch (Exception ex) {

			if (null != trackingRecord) {
				trackingRecord.setRequestStateDesc(ex.getMessage());
			}
			message.append(EMSG_LOAD_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (null != trackingRecord) {
				updateTrackingRecord(isSuccess, trackingRecord, message);
				message.append(" Plan Count Statistics: ").append(trackingRecord.getPlanStats());

				String baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansMedicareUploadPath();
				serffResourceUtil.moveFileToDirectory(isSuccess, trackingRecord.getAttachmentsList(), baseDirectoryPath);
			}

			if (null != ftpClient) {
				ftpClient.close();
			}
			LOGGER.info("processMedicareExcelData() End with message: " + message);
		}
		return message.toString();
	}

	/**
	 * Loads Medicare excel sheet from FTP server.
	 */
	private boolean loadExcelTemplate(SerffPlanMgmt trackingRecord, String ftpFolderPath, StringBuilder message) {

		LOGGER.info("loadExcelTemplate() Start");
		boolean isSuccess = false;
		InputStream inputStream = null;

		try {
			inputStream = ftpClient.getFileInputStream(ftpFolderPath + trackingRecord.getAttachmentsList());
			if (null == inputStream) {
				trackingRecord.setPmResponseXml("Medicare Excel is not found at FTP server.");
				trackingRecord.setRequestStateDesc(trackingRecord.getPmResponseXml());
				message.append(trackingRecord.getPmResponseXml());
				return isSuccess;
			}

			// Upload Excel File to ECM
			isSuccess = serffResourceUtil.uploadFileFromFTPToECM(trackingRecord, ftpFolderPath, FTP_PLAN_MEDICARE_FILE, trackingRecord.getAttachmentsList(), true);
			if (!isSuccess) {
				trackingRecord.setPmResponseXml("Failed upload Excel file to ECM.");
				trackingRecord.setRequestStateDesc(trackingRecord.getPmResponseXml());
				message.append(trackingRecord.getPmResponseXml());
				return isSuccess;
			}
			isSuccess = false;
			// Load Excel File
			loadFile(inputStream);
			int totalSheets = getNumberOfSheets();

			if (0 == totalSheets) {
				trackingRecord.setPmResponseXml(EMSG_INVALID_EXCEL);
				trackingRecord.setRequestStateDesc(EMSG_INVALID_EXCEL);
				message.append(EMSG_INVALID_EXCEL);
				return isSuccess;
			}
			Map<String, Map<String, MedicareVO>> medicareIssuerPlansMap = readMedicareDataFromExcel(trackingRecord, totalSheets);
			// logMedicareMapList(medicareIssuerPlansMap); For logging purpose
			// Persist Medicare plans in database
			isSuccess = medicarePlanMgmtSerffService.populateAndPersistMedicareData(medicareIssuerPlansMap, trackingRecord);
			message.append(trackingRecord.getRequestStateDesc());
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			message.append(EMSG_LOAD_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.info("loadExcelTemplate() End with isSuccess: " + isSuccess);
		}
		return isSuccess;
	}

	/**
	 * Method is used for logging to medicareIssuerPlansMap.
	 */
	@SuppressWarnings("unchecked")
	public void logMedicareMapList(Map<String, Map<String, MedicareVO>> medicareIssuerPlansMap) {

		if (!CollectionUtils.isEmpty(medicareIssuerPlansMap)) {

			Entry<String, MedicareVO> mapEntry;
			Map<String, MedicareVO> medicarePlansMap;
			Iterator<Entry<String, Map<String, MedicareVO>>> mapEntries = medicareIssuerPlansMap.entrySet().iterator();

			while (mapEntries.hasNext()) {
				mapEntry = (Entry) mapEntries.next();
				LOGGER.debug("=========================================================================================");
				LOGGER.debug("HIOS Issuer ID:= " + mapEntry.getKey());
				medicarePlansMap = (Map<String, MedicareVO>) mapEntry.getValue();

				for (Map.Entry<String, MedicareVO> entryData : medicarePlansMap.entrySet()) {
					LOGGER.debug("IssuerPlanNumber: " + entryData.getKey());
					LOGGER.debug("MedicareVO Object: " + entryData.getValue().toString());
				}
			}
			LOGGER.debug("=========================================================================================");
		}
	}

	/**
	 * Method is used to read Medicare data from PUF Excel sheet and
	 * return Map<StateCode + Contract-ID, Map<IssuerPlanNumber, MedicareVO>>.
	 */
	private Map<String, Map<String, MedicareVO>> readMedicareDataFromExcel(SerffPlanMgmt trackingRecord, int totalSheets) {

		LOGGER.debug("getMedicareVO() Start");
		Map<String, Map<String, MedicareVO>> medicareIssuerPlansMap = new HashMap<>();

		try {
			XSSFSheet sheet;
			Row excelRow;

			int startRowCnt = 0;
			int count = 0;
			if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(trackingRecord.getOperation())) {
				startRowCnt = START_ROW_MA;
			}
			else if (TRANSFER_MEDICARE_SP.equalsIgnoreCase(trackingRecord.getOperation())) {
				startRowCnt = START_ROW_SP;
			}
			else if (TRANSFER_MEDICARE_RX.equalsIgnoreCase(trackingRecord.getOperation())) {
				startRowCnt = START_ROW_RX;
			}

			for (int indexCnt = 0; indexCnt < totalSheets; indexCnt++) {
				sheet = getSheet(indexCnt);
				excelRow = null;

				if (null == sheet || SHEET_SANCTIONED.equalsIgnoreCase(sheet.getSheetName())
						|| null == sheet.iterator()) {
					continue;
				}

				Iterator<Row> rowIterator = sheet.iterator();
				count = startRowCnt;

				while (rowIterator.hasNext()) {

					if (0 < count) {
						rowIterator.next();
						count--;
						continue;
					}
					excelRow = rowIterator.next();
					readMedicareDataFromExcelRow(excelRow, medicareIssuerPlansMap, trackingRecord);
				}
			}
		}
		catch (IOException ex) {
			trackingRecord.setRequestStateDesc(EMSG_LOAD_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (StringUtils.isBlank(trackingRecord.getRequestStateDesc())
					&& CollectionUtils.isEmpty(medicareIssuerPlansMap)) {
				trackingRecord.setRequestStateDesc(EMSG_INVALID_EXCEL);
			}
			LOGGER.debug("getMedicareVO() End");
		}
		return medicareIssuerPlansMap;
	}

	/**
	 * Method is used to read Medicare data from Excel sheet's row and
	 * generate Map<StateCode + Contract-ID, Map<IssuerPlanNumber, MedicareVO>>.
	 */
	private void readMedicareDataFromExcelRow(Row excelRow, Map<String, Map<String, MedicareVO>> medicareIssuerPlansMap, SerffPlanMgmt trackingRecord) {

		LOGGER.debug("Reading Medicare plan Data for request " + trackingRecord.getSerffReqId());

		try {
			if (null == excelRow) {
				LOGGER.warn("Excel sheet's row is empty for Record:" + trackingRecord.getSerffReqId());
				return;
			}

			boolean ignoreDecimal = true;
			String medicarePlanType = trackingRecord.getOperation();
			int index = COUNTY.getColumnIndex(medicarePlanType);
			String county = INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index));
			index = CONTRACT_ID.getColumnIndex(medicarePlanType);
			String contractID = getCellValueTrim(excelRow.getCell(index));
			index = SEGMENT_ID.getColumnIndex(medicarePlanType);
			String segmentID = INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index), ignoreDecimal);
			index = PLAN_ID.getColumnIndex(medicarePlanType);
			String planID = INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index), ignoreDecimal);
			index = STATE.getColumnIndex(medicarePlanType);
			String stateName = INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index));
			String stateCode = null;

			if (StringUtils.isNotBlank(stateName)) {
				stateCode = serffUtils.getStateCode(stateName);
			}

			if (!validateRequiredExcelData(contractID, planID, segmentID, stateCode)) {
				trackingRecord.setRequestStateDesc("Required excel data [ContractID/PlanID/SegmentID/State] has been empty/invalid to process Medicare PUF file." + contractID + " : " + planID + " : " + stateCode + ": State=" + stateName);
				if(null != contractID || null != planID) {
					LOGGER.error(trackingRecord.getRequestStateDesc());
				}
				return;
			}

			String hiosIssuerID = PREFIX_HIOS_ISSUER_ID + stateCode + contractID;
			Map<String, MedicareVO> medicarePlansMap = medicareIssuerPlansMap.get(hiosIssuerID);

			if (CollectionUtils.isEmpty(medicarePlansMap)) {
				medicarePlansMap = new HashMap<String, MedicareVO>();
				medicareIssuerPlansMap.put(hiosIssuerID, medicarePlansMap);
				LOGGER.debug("HIOS Issuer ID: " + hiosIssuerID);
			}

			String issuerPlanNumber = generateIssuerPlanNumber(medicarePlanType, stateCode, contractID, planID, segmentID);
			MedicareVO medicareVO = medicarePlansMap.get(issuerPlanNumber);

			if (null != medicareVO) {
				LOGGER.debug("Adding new county for Issuer Plan Number["+ issuerPlanNumber +"] and county["+ county +"].");
				medicareVO.addCountyToList(county);
			}
			else {
				LOGGER.debug("Retrieving new record for Issuer Plan Number["+ issuerPlanNumber +"] and county["+ county +"].");
				medicareVO = new MedicareVO();

				if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(medicarePlanType)) {
					medicareVO.setInsuranceType(MedicareVO.InsuranceType.MA);
					medicareVO.setSegmentID(segmentID);
				}
				else if (TRANSFER_MEDICARE_SP.equalsIgnoreCase(medicarePlanType)) {
					medicareVO.setInsuranceType(MedicareVO.InsuranceType.SNP);
					medicareVO.setSegmentID(segmentID);
				}
				else if (TRANSFER_MEDICARE_RX.equalsIgnoreCase(medicarePlanType)) {
					medicareVO.setInsuranceType(MedicareVO.InsuranceType.RX);
					medicareVO.setSegmentID(DEFAULT_SEGMENT_ID);
				}
				medicareVO.setHiosIssuerID(hiosIssuerID);
				medicareVO.setIssuerPlanNumber(issuerPlanNumber);
				medicareVO.setStateCode(stateCode);
				medicareVO.addCountyToList(county);
				index = ORGANIZATION_NAME.getColumnIndex(medicarePlanType);
				medicareVO.setOrganizationName(INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index)));
				index = PLAN_NAME.getColumnIndex(medicarePlanType);
				medicareVO.setPlanName(INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index)));

				index = MEDICARE_TYPE.getColumnIndex(medicarePlanType);
				medicareVO.setMedicareType(INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index)));
				if (StringUtils.isNotBlank(medicareVO.getMedicareType())
						&& NEGATIVE_INDEX_OF < medicareVO.getMedicareType().indexOf(ASTERISK)) {
					medicareVO.setMedicareType(medicareVO.getMedicareType().replace(ASTERISK, StringUtils.EMPTY).trim());
				}
				index = SPECIAL_NEEDS_PLAN_TYPE.getColumnIndex(medicarePlanType);
				medicareVO.setSpecialNeedsPlanType(INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index)));
				index = MONTHLY_CONSOLIDATED_PREMIUM.getColumnIndex(medicarePlanType);
				medicareVO.setMonthlyPremium(INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index)));
				index = ANNUAL_DRUG_DEDUCTIBLE.getColumnIndex(medicarePlanType);
				medicareVO.setAnnualDrugDeductible(INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index)));
				index = ADDITIONAL_COVERAGE_OFFERED_IN_GAP.getColumnIndex(medicarePlanType);
				medicareVO.setAdditionalCoverageInGap(INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index)));
				medicareVO.setContractID(contractID);
				medicareVO.setPlanID(planID);
				index = IN_NETWORK_MOOP_AMT.getColumnIndex(medicarePlanType);
				medicareVO.setInNetworkMOOPAmount(INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index)));
				index = OVERALL_STAR_RATING.getColumnIndex(medicarePlanType);
				medicareVO.setOverallStarRating(INVALID_INDEX == index ? null : getCellValueTrim(excelRow.getCell(index)));
				// Adding Medicare VO in Map
				medicarePlansMap.put(issuerPlanNumber, medicareVO);
			}
		}
		finally {
			LOGGER.debug("readMedicareDataFromExcelRow() End");
		}
	}

	/**
	 * Method is used to generate IssuerPlanNumber from State code, Contract-ID, Plan-ID & Segment-ID.
	 * Format: [State][type]_[ContractId]_[PlanId]_[SegmentId]
	 */
	public String generateIssuerPlanNumber(String medicareType, String stateCode, String contractID, String planID, String segmentID) {

		LOGGER.debug("medicareType["+ medicareType +"], stateCode["+ stateCode +"], contractID["+ contractID +"], planID["+ planID +"], segmentID["+ segmentID +"]");
		StringBuilder issuerPlanNumber = new StringBuilder();
		if (TRANSFER_MEDICARE_AD.equalsIgnoreCase(medicareType)) {
			issuerPlanNumber.append(stateCode).append(MedicareCode.M.name()).append(UNDERSCORE).append(contractID).append(UNDERSCORE).append(planID).append(UNDERSCORE).append(segmentID);
		}
		else if (TRANSFER_MEDICARE_SP.equalsIgnoreCase(medicareType)) {
			issuerPlanNumber.append(stateCode).append(MedicareCode.M.name()).append(UNDERSCORE).append(contractID).append(UNDERSCORE).append(planID).append(UNDERSCORE).append(segmentID);
		}
		else if (TRANSFER_MEDICARE_RX.equalsIgnoreCase(medicareType)) {
			issuerPlanNumber.append(stateCode).append(MedicareCode.R.name()).append(UNDERSCORE).append(contractID).append(UNDERSCORE).append(planID).append(UNDERSCORE).append(DEFAULT_SEGMENT_ID);
		}
		return issuerPlanNumber.toString();
	}

	/**
	 * Method is used to validate required excel data to process PUF file.
	 */
	public boolean validateRequiredExcelData(String contractID, String planID, String segmentID, String stateCode) {

		boolean isValid = true;

		if (StringUtils.isBlank(contractID)
				|| StringUtils.isBlank(planID)
				|| StringUtils.isBlank(stateCode)) {
			LOGGER.debug("Some of the required field (ContractID/PlanID/State) are blank : " + contractID + " : " + planID + " : " + stateCode);
			isValid = false;
		}
		else if (LEN_CONTRACT_ID != contractID.trim().length()
				|| LEN_PLAN_ID != planID.trim().length()
				|| (StringUtils.isNotBlank(segmentID) && !StringUtils.isNumeric(segmentID))
				|| LEN_STATE_CODE != stateCode.trim().length()) {
			LOGGER.debug("Some of the required field (ContractID/PlanID/State) have invalid value : " + contractID + " : " + planID + " : " + stateCode);
			isValid = false;
		}
		return isValid;
	}

	/**
	 * Method is used to get Tracking Record  from SERFF Plan Management Table.
	 */
	private SerffPlanMgmt getTrackingRecord(String serffReqId, StringBuilder message) {

		LOGGER.debug("getting Tracking Record for " + serffReqId);
		SerffPlanMgmt trackingRecord = null;

		if (StringUtils.isNumeric(serffReqId)) {
			// Get tracking record from SERFF_PLAN_MGMT table. 
			trackingRecord = serffService.getSerffPlanMgmtById(Long.parseLong(serffReqId));

			if (null == trackingRecord) {
				message.append("Tracking record is not available for serffReqId: ");
				message.append(serffReqId);
			}
		}
		else {
			message.append("Tracking record Id is not available for serffReqId: ");
			message.append(serffReqId);
		}
		return trackingRecord;
	}

	/**
	 * Method is used to update tracking record of SERFF Plan Management table.
	 */
	private void updateTrackingRecord(boolean isSuccess, SerffPlanMgmt trackingRecord, StringBuilder message) {

		LOGGER.debug("updateTrackingRecord() Start");

		try {
			if (null != trackingRecord) {

				if (isSuccess) {
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
				}
				else {
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
				}

				if (StringUtils.isNotBlank(message.toString())) {
					trackingRecord.setRequestStateDesc(message.toString());
				}

				if (trackingRecord.getRequestStateDesc().length() > ERROR_MAX_LEN) {
					trackingRecord.setRequestStateDesc(trackingRecord.getRequestStateDesc().substring(0, ERROR_MAX_LEN));
				}

				if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
					trackingRecord.setPmResponseXml(trackingRecord.getRequestStateDesc());
				}
				trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
				trackingRecord.setEndTime(new Date());
				serffService.saveSerffPlanMgmt(trackingRecord);
			}
			else {
				message.append("Tracking Record is not found.");
			}
		}
		finally {
			LOGGER.debug("updateTrackingRecord() Start");
		}
	}
}
