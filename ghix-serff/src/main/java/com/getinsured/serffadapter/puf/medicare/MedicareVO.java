package com.getinsured.serffadapter.puf.medicare;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

/**
 * Class is used to generate getter/setter on the basis of Medicare MA, SNP & PDP Excels.
 * 
 * @author Bhavin Parmar
 * @since Apr 13, 2015
 */
public class MedicareVO {

	public static enum MedicareCode {
		M, R;
	}

	public static enum InsuranceType {
		MA, SNP, RX;
	}
	private InsuranceType insuranceType;
	private String hiosIssuerID;
	private String issuerPlanNumber;
	private String stateCode;
	private String organizationName; // In PDP(Drug) companyName
	private String planName;
	private String medicareType;
	private String monthlyPremium;
	private String annualDrugDeductible;
	private String specialNeedsPlanType;
	private String additionalCoverageInGap;
	private String contractID;
	private String planID;
	private String segmentID;
	private String inNetworkMOOPAmount;
	private String overallStarRating; // In PDP(Drug) summaryStarRating

	private List<String> countyList;

	private boolean isNotValid;
	private String errorMessages;

	public MedicareVO() {
	}

	public InsuranceType getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(InsuranceType insuranceType) {
		this.insuranceType = insuranceType;
	}

	public String getHiosIssuerID() {
		return hiosIssuerID;
	}

	public void setHiosIssuerID(String hiosIssuerID) {
		this.hiosIssuerID = hiosIssuerID;
	}

	public String getIssuerPlanNumber() {
		return issuerPlanNumber;
	}

	public void setIssuerPlanNumber(String issuerPlanNumber) {
		this.issuerPlanNumber = issuerPlanNumber;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getMedicareType() {
		return medicareType;
	}

	public void setMedicareType(String medicareType) {
		this.medicareType = medicareType;
	}

	public String getMonthlyPremium() {
		return monthlyPremium;
	}

	public void setMonthlyPremium(String monthlyPremium) {
		this.monthlyPremium = monthlyPremium;
	}

	public String getAnnualDrugDeductible() {
		return annualDrugDeductible;
	}

	public void setAnnualDrugDeductible(String annualDrugDeductible) {
		this.annualDrugDeductible = annualDrugDeductible;
	}

	public String getSpecialNeedsPlanType() {
		return specialNeedsPlanType;
	}

	public void setSpecialNeedsPlanType(String specialNeedsPlanType) {
		this.specialNeedsPlanType = specialNeedsPlanType;
	}

	public String getAdditionalCoverageInGap() {
		return additionalCoverageInGap;
	}

	public void setAdditionalCoverageInGap(String additionalCoverageInGap) {
		this.additionalCoverageInGap = additionalCoverageInGap;
	}

	public String getContractID() {
		return contractID;
	}

	public void setContractID(String contractID) {
		this.contractID = contractID;
	}

	public String getPlanID() {
		return planID;
	}

	public void setPlanID(String planID) {
		this.planID = planID;
	}

	public String getSegmentID() {
		return segmentID;
	}

	public void setSegmentID(String segmentID) {
		this.segmentID = segmentID;
	}

	public String getInNetworkMOOPAmount() {
		return inNetworkMOOPAmount;
	}

	public void setInNetworkMOOPAmount(String inNetworkMOOPAmount) {
		this.inNetworkMOOPAmount = inNetworkMOOPAmount;
	}

	public String getOverallStarRating() {
		return overallStarRating;
	}

	public void setOverallStarRating(String overallStarRating) {
		this.overallStarRating = overallStarRating;
	}

	public void setCountyList(List<String> countyList) {
		this.countyList = countyList;
	}

	public List<String> getCountyList() {
		return countyList;
	}

	public void addCountyToList(String county) {

		if (StringUtils.isBlank(county)) {
			return;
		}

		if (CollectionUtils.isEmpty(this.countyList)) {
			this.countyList = new ArrayList<String>();
		}
		this.countyList.add(county);
	}

	public boolean isNotValid() {
		return isNotValid;
	}

	public void setNotValid(boolean isNotValid) {
		this.isNotValid = isNotValid;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("MedicareVO [insuranceType=");

		if (null != insuranceType) {
			sb.append(insuranceType);
		}
		sb.append(", hiosIssuerID=");
		sb.append(hiosIssuerID);
		sb.append(", issuerPlanNumber=");
		sb.append(issuerPlanNumber);
		sb.append(", stateCode=");
		sb.append(stateCode);
		sb.append(", organizationName=");
		sb.append(organizationName);
		sb.append(", planName=");
		sb.append(planName);
		sb.append(", medicareType=");
		sb.append(medicareType);
		sb.append(", monthlyPremium=");
		sb.append(monthlyPremium);
		sb.append(", annualDrugDeductible=");
		sb.append(annualDrugDeductible);
		sb.append(", specialNeedsPlanType=");
		sb.append(specialNeedsPlanType);
		sb.append(", additionalCoverageInGap=");
		sb.append(additionalCoverageInGap);
		sb.append(", contractID=");
		sb.append(contractID);
		sb.append(", planID=");
		sb.append(planID);
		sb.append(", segmentID=");
		sb.append(segmentID);
		sb.append(", inNetworkMOOPAmount=");
		sb.append(inNetworkMOOPAmount);
		sb.append(", overallStarRating=");
		sb.append(overallStarRating);

		if (!CollectionUtils.isEmpty(countyList)) {
			sb.append(", CountyList [");

			for (String county : countyList) {
				sb.append(county);
				sb.append(", ");
			}
			sb.append(" ]");
		}
		sb.append(" ]");
		return sb.toString();
	}
}
