package com.getinsured.serffadapter;

import java.util.HashMap;
import java.util.Map;

public final class SerffAdapterUtil {
	public static final String BENEFIT_COVERED = "Covered";
	public static final String YES_STRING = "Yes";
	public static final String NO_STRING = "No";
	public static final Map<String, String> STATE_CODE_NAME_MAP;
	static {
		STATE_CODE_NAME_MAP = new HashMap<String, String>();

		STATE_CODE_NAME_MAP.put("AL", "Alabama");
		STATE_CODE_NAME_MAP.put("AK", "Alaska");
		STATE_CODE_NAME_MAP.put("AZ", "Arizona");
		STATE_CODE_NAME_MAP.put("AR", "Arkansas");
		STATE_CODE_NAME_MAP.put("CA", "California");
		STATE_CODE_NAME_MAP.put("CO", "Colorado");
		STATE_CODE_NAME_MAP.put("CT", "Connecticut");
		STATE_CODE_NAME_MAP.put("DE", "Delaware");
		STATE_CODE_NAME_MAP.put("FL", "Florida");
		STATE_CODE_NAME_MAP.put("GA", "Georgia");
		STATE_CODE_NAME_MAP.put("HI", "Hawaii");
		STATE_CODE_NAME_MAP.put("ID", "Idaho");
		STATE_CODE_NAME_MAP.put("IL", "Illinois");
		STATE_CODE_NAME_MAP.put("IN", "Indiana");
		STATE_CODE_NAME_MAP.put("IA", "Iowa");
		STATE_CODE_NAME_MAP.put("KS", "Kansas");
		STATE_CODE_NAME_MAP.put("KY", "Kentucky");
		STATE_CODE_NAME_MAP.put("LA", "Louisiana");
		STATE_CODE_NAME_MAP.put("ME", "Maine");
		STATE_CODE_NAME_MAP.put("MD", "Maryland");
		STATE_CODE_NAME_MAP.put("MA", "Massachusetts");
		STATE_CODE_NAME_MAP.put("MI", "Michigan");
		STATE_CODE_NAME_MAP.put("MN", "Minnesota");
		STATE_CODE_NAME_MAP.put("MS", "Mississippi");
		STATE_CODE_NAME_MAP.put("MO", "Missouri");
		STATE_CODE_NAME_MAP.put("MT", "Montana");
		STATE_CODE_NAME_MAP.put("NE", "Nebraska");
		STATE_CODE_NAME_MAP.put("NV", "Nevada");
		STATE_CODE_NAME_MAP.put("NH", "New Hampshire");
		STATE_CODE_NAME_MAP.put("NJ", "New Jersey");
		STATE_CODE_NAME_MAP.put("NM", "New Mexico");
		STATE_CODE_NAME_MAP.put("NY", "New York");
		STATE_CODE_NAME_MAP.put("NC", "North Carolina");
		STATE_CODE_NAME_MAP.put("ND", "North Dakota");
		STATE_CODE_NAME_MAP.put("OH", "Ohio");
		STATE_CODE_NAME_MAP.put("OK", "Oklahoma");
		STATE_CODE_NAME_MAP.put("OR", "Oregon");
		STATE_CODE_NAME_MAP.put("PA", "Pennsylvania");
		STATE_CODE_NAME_MAP.put("RI", "Rhode Island");
		STATE_CODE_NAME_MAP.put("SC", "South Carolina");
		STATE_CODE_NAME_MAP.put("SD", "South Dakota");
		STATE_CODE_NAME_MAP.put("TN", "Tennessee");
		STATE_CODE_NAME_MAP.put("TX", "Texas");
		STATE_CODE_NAME_MAP.put("UT", "Utah");
		STATE_CODE_NAME_MAP.put("VT", "Vermont");
		STATE_CODE_NAME_MAP.put("VA", "Virginia");
		STATE_CODE_NAME_MAP.put("WA", "Washington");
		STATE_CODE_NAME_MAP.put("WV", "West Virginia");
		STATE_CODE_NAME_MAP.put("WI", "Wisconsin");
		STATE_CODE_NAME_MAP.put("WY", "Wyoming");
		
	}

	private SerffAdapterUtil() {
		super();
	}
}
