package com.getinsured.serffadapter.plandoc;

/**
 * Class is used to generate getter/setter on the basis of Plan Document's Excel Column.
 * 
 * @author Bhavin Parmar
 * @since January 08, 2015
 */
public class PlanDocumnetExcelVO {

	private String planId;
	private String applicableYear;
	private String brochureDocumentName;
	private String brochureDocumentFileName;
	private String brochureType;
	private String sbcName;
	private String sbcFileName;
	private String sbcType;
	private String eocDocumentName;
	private String eocDocumentFileName;
	private String eocType;

	private boolean isNotValid;
	private String errorMessages;

	public PlanDocumnetExcelVO() {
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getApplicableYear() {
		return applicableYear;
	}

	public void setApplicableYear(String applicableYear) {
		this.applicableYear = applicableYear;
	}

	public String getBrochureDocumentName() {
		return brochureDocumentName;
	}

	public void setBrochureDocumentName(String brochureDocumentName) {
		this.brochureDocumentName = brochureDocumentName;
	}

	public String getBrochureDocumentFileName() {
		return brochureDocumentFileName;
	}

	public void setBrochureDocumentFileName(String brochureDocumentFileName) {
		this.brochureDocumentFileName = brochureDocumentFileName;
	}

	public String getBrochureType() {
		return brochureType;
	}

	public void setBrochureType(String brochureType) {
		this.brochureType = brochureType;
	}

	public String getSbcName() {
		return sbcName;
	}

	public void setSbcName(String sbcName) {
		this.sbcName = sbcName;
	}

	public String getSbcFileName() {
		return sbcFileName;
	}

	public void setSbcFileName(String sbcFileName) {
		this.sbcFileName = sbcFileName;
	}

	public String getSbcType() {
		return sbcType;
	}

	public void setSbcType(String sbcType) {
		this.sbcType = sbcType;
	}

	public String getEocDocumentName() {
		return eocDocumentName;
	}

	public void setEocDocumentName(String eocDocumentName) {
		this.eocDocumentName = eocDocumentName;
	}

	public String getEocDocumentFileName() {
		return eocDocumentFileName;
	}

	public void setEocDocumentFileName(String eocDocumentFileName) {
		this.eocDocumentFileName = eocDocumentFileName;
	}

	public String getEocType() {
		return eocType;
	}

	public void setEocType(String eocType) {
		this.eocType = eocType;
	}

	public boolean isNotValid() {
		return isNotValid;
	}

	public void setNotValid(boolean isNotValid) {
		this.isNotValid = isNotValid;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("PlanDocumnetExcelVO [planId=");
		sb.append(planId);
		sb.append(", applicableYear=");
		sb.append(applicableYear);
		sb.append(", brochureDocumentName=");
		sb.append(brochureDocumentName);
		sb.append(", brochureDocumentFileName=");
		sb.append(brochureDocumentFileName);
		sb.append(", brochureType=");
		sb.append(brochureType);
		sb.append(", sbcName=");
		sb.append(sbcName);
		sb.append(", sbcFileName=");
		sb.append(sbcFileName);
		sb.append(", sbcType=");
		sb.append(sbcType);
		sb.append(", eocDocumentName=");
		sb.append(eocDocumentName);
		sb.append(", eocDocumentFileName=");
		sb.append(eocDocumentFileName);
		sb.append(", eocType=");
		sb.append(eocType);
		return sb.toString();
	}
}
