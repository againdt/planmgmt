package com.getinsured.serffadapter.plandoc;

import static com.getinsured.serffadapter.plandoc.PlanDocumnetExcelColumn.PlanDocumnetExcelColumnEnum.APPLICABLE_YEAR;
import static com.getinsured.serffadapter.plandoc.PlanDocumnetExcelColumn.PlanDocumnetExcelColumnEnum.BROCHURE_DOC_NAME;
import static com.getinsured.serffadapter.plandoc.PlanDocumnetExcelColumn.PlanDocumnetExcelColumnEnum.BROCHURE_TYPE;
import static com.getinsured.serffadapter.plandoc.PlanDocumnetExcelColumn.PlanDocumnetExcelColumnEnum.EOC_DOCUMENT_NAME;
import static com.getinsured.serffadapter.plandoc.PlanDocumnetExcelColumn.PlanDocumnetExcelColumnEnum.EOC_TYPE;
import static com.getinsured.serffadapter.plandoc.PlanDocumnetExcelColumn.PlanDocumnetExcelColumnEnum.PLAN_ID;
import static com.getinsured.serffadapter.plandoc.PlanDocumnetExcelColumn.PlanDocumnetExcelColumnEnum.SBC_DOCUMENT_NAME;
import static com.getinsured.serffadapter.plandoc.PlanDocumnetExcelColumn.PlanDocumnetExcelColumnEnum.SBC_TYPE;
import static com.serff.util.SerffConstants.FTP_PLAN_DOCS_FILE;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.serffadapter.excel.ExcelReader;
import com.serff.service.SerffService;
import com.serff.service.templates.PlanDocumentsJobService;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

/**
 * Class is used to read & write Plan Document Excel data to PlanDocumnetExcelVO DTO Objects.
 * 
 * @author Bhavin Parmar
 * @since January 08, 2015
 */
@Service("planDocumentExcelReader")
public class PlanDocumentExcelReader extends ExcelReader {

	private static final Logger LOGGER = Logger.getLogger(PlanDocumentExcelReader.class);

	private static final String EMSG_ERROR_DOCUMENTS = "Failed to load Plan Document Excel.";
	private static final String EMSG_INVALID_EXCEL = "Invalid Plan Document Excel.";
	private static final String EMSG_NO_EXCEL_FOUND = "No Plan Document Excel Template is available for processing.";
	private static final String MSG_ERROR_ECM = "Failed to upload Plan Document at ECM. Reason: ";

	private static final String DOCUMENT_LIST_SHEET = "DocumentList";
	private static final String RESULTS_SHEET = "Result";
	private static final int DOCUMENT_LIST_SHEET_INDEX = 1;
	private static final int RESULTS_SHEET_INDEX = 2;
	private static final int ERROR_MAX_LEN = 1000;
	private static final int WRITE_DATA = 2;
	private static final int ZERO = 0;
	private static final int ONE = 1;

	@Autowired private PlanDocumentsJobService planDocumentsJobService;
	@Autowired private SerffService serffService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private SerffUtils serffUtils;
	@Autowired private SerffResourceUtil serffResourceUtil;

	/**
	 * Method is used process uploaded Plan Document Excel.
	 */
	public String processPlanDocumentExcel(String serffReqId, String folderPath) {

		LOGGER.info("processPlanDocumentExcel() Start");
		StringBuilder message = new StringBuilder();
		boolean updateStatus = false;
		SerffPlanMgmt trackingRecord = null;
		SerffPlanMgmtBatch trackingBatchRecord = null;
		InputStream inputStream = null;

		try {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SERFF Request ID: " + SecurityUtil.sanitizeForLogging(serffReqId));
				LOGGER.debug("Plan Document FTP Folder Path: " + folderPath);
			}

			if (!StringUtils.isNumeric(serffReqId) || StringUtils.isBlank(folderPath) ) {
				message.append(EMSG_NO_EXCEL_FOUND);
				return message.toString();
			}

			// Get Tracking record from SERFF Plan Management table
			trackingRecord = getTrackingRecord(serffReqId, message);

			if (null != trackingRecord
					&& StringUtils.isNotBlank(trackingRecord.getAttachmentsList())
					&& trackingRecord.getAttachmentsList().contains(FTP_PLAN_DOCS_FILE)) {

				LOGGER.debug("Plan Document File Name: " + folderPath + "/" + trackingRecord.getAttachmentsList());
				// Get Plan Document Excel from FTP server
				inputStream = ftpClient.getFileInputStream(folderPath + trackingRecord.getAttachmentsList());

				if (null == inputStream) {
					message.append("Plan Document Excel is not found at FTP server.");
					return message.toString();
				}
				trackingBatchRecord = serffService.getTrackingBatchRecord(trackingRecord.getAttachmentsList(), FTP_PLAN_DOCS_FILE, message);
				if(trackingBatchRecord != null) {
					updateStatus = readAndWritePlanDocumentExcel(folderPath, message, trackingRecord, inputStream);
				} else {
					message.append("Failed to get Plan Document Batch tracking record.");
				}
			}
			else if (null != trackingRecord) {
				message.append(EMSG_NO_EXCEL_FOUND);
			}
		}
		catch (Exception ex) {
			message.append(EMSG_ERROR_DOCUMENTS + " Reason: " + ex.getMessage());
			LOGGER.error("processPlanDocumentExcel() Exception occurred: " + ex.getMessage(), ex);
		}
		finally {

			if (null != trackingRecord) {
				updateTrackingRecord(updateStatus, trackingRecord, message);

				if (StringUtils.isNotBlank(trackingRecord.getAttachmentsList())) {
					moveFileToDirectory(updateStatus, trackingRecord.getAttachmentsList());
				}
				SerffPlanMgmtBatch.BATCH_STATUS status = null;
				if (trackingRecord.getRequestStatus().equals(SerffPlanMgmt.REQUEST_STATUS.S)) {
					//set status as success
					status = SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED;
				} else {
					status = SerffPlanMgmtBatch.BATCH_STATUS.FAILED;
				}
				serffService.updateTrackingBatchRecordAsCompleted(trackingBatchRecord, trackingRecord, status);
			}
			ftpClient.close();
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.info("processPlanDocumentExcel() End - " + message);
		}
		return message.toString();
	}

	/**
	 * Method is used read and write Plan Document Excel data.
	 */
	private boolean readAndWritePlanDocumentExcel(String folderPath, StringBuilder message, SerffPlanMgmt trackingRecord, InputStream inputStream) {
	
		LOGGER.debug("readAndWritePlanDocumentExcel() Start");
		boolean updateStatus = false;

		try {
			// Load Plan Document Excel File
			loadFile(inputStream);

			// Check Plan Document Excel Validations
			if (!excelValidations()) {
				LOGGER.error(EMSG_INVALID_EXCEL);
				message.append(EMSG_INVALID_EXCEL);
				return updateStatus;
			}

			// Upload Plan Document Excel in ECM 
			if (addExcelInECM(message, trackingRecord, folderPath, trackingRecord.getAttachmentsList())) {
				// Load Plan Document List
				List<PlanDocumnetExcelVO> planDocumnetExcelList = loadPlanDocumentExcelData(getSheet(DOCUMENT_LIST_SHEET_INDEX));
				// Populate and Persist Plan Document Excel Data
				updateStatus = planDocumentsJobService.populateAndPersistPlanDocumentData(planDocumnetExcelList, trackingRecord);
				LOGGER.info("Update Status: " + updateStatus);
				// Writing Result Sheet Data.
				writeResultsSheet(planDocumnetExcelList, trackingRecord, folderPath, message);
			}
			else {
				LOGGER.error("Failed upload Excel file to ECM.");
			}
		}
		catch (Exception ex) {
			message.append(EMSG_ERROR_DOCUMENTS + " Reason: " + ex.getMessage());
			LOGGER.error("readAndWritePlanDocumentExcel() Exception occurred: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug(message);
			LOGGER.debug("readAndWritePlanDocumentExcel() End with updateStatus: " + updateStatus);
		}
		return updateStatus;
	}

	/**
	 * Method is used to update tracking record of SERFF Plan Management table.
	 */
	private void updateTrackingRecord(boolean isSuccess, SerffPlanMgmt trackingRecord, StringBuilder message) {
		
		LOGGER.debug("updateTrackingRecord() Start");

		try {
			if (null != trackingRecord) {

				if (isSuccess) {
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
				}
				else {
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
				}

				if (StringUtils.isNotBlank(message.toString())) {
					trackingRecord.setRequestStateDesc(message.toString());
				}

				if (trackingRecord.getRequestStateDesc().length() > ERROR_MAX_LEN) {
					trackingRecord.setRequestStateDesc(trackingRecord.getRequestStateDesc().substring(0, ERROR_MAX_LEN));
				}

				if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
					trackingRecord.setPmResponseXml(trackingRecord.getRequestStateDesc());
				}
				trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
				trackingRecord.setEndTime(new Date());
				serffService.saveSerffPlanMgmt(trackingRecord);
			}
			else {
				message.append("Tracking Record is not found.");
			}
		}
		finally {
			LOGGER.debug("updateTrackingRecord() Start");
		}
	}

	/**
	 * Method is used to validated Excel Sheets.
	 */
	private boolean excelValidations() {

		LOGGER.debug("excelValidations() Start");
		boolean isValid = true;

		try {
			XSSFSheet sheet = getSheet(DOCUMENT_LIST_SHEET_INDEX);
			if (null == sheet
					|| null == sheet.iterator()
					|| !sheet.getSheetName().equalsIgnoreCase(DOCUMENT_LIST_SHEET)) {
				isValid = false;
				LOGGER.error("Document List sheet is not available.");
			}

			sheet = getSheet(RESULTS_SHEET_INDEX);
			if (null == sheet || !sheet.getSheetName().equalsIgnoreCase(RESULTS_SHEET)) {
				isValid = false;
				LOGGER.error("Results sheet is not available.");
			}
		}
		catch (IOException ex) {
			isValid = false;
			LOGGER.error("excelValidations() Exception occurred: " + ex.getMessage(), ex);
		}
		catch (Exception ex) {
			isValid = false;
			LOGGER.error("excelValidations() Exception occurred: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("excelValidations() End");
		}
		return isValid;
	}

	/**
	 * Method is used to write data in Result sheet of Plan Document Excel.
	 */
	private void writeResultsSheet(List<PlanDocumnetExcelVO> planDocumnetExcelList, SerffPlanMgmt trackingRecord, String folderPath, StringBuilder message) {

		LOGGER.info("writeResultsSheet() Start");

		try {
			Map<Integer, Object[]> writeData = generateResultSheetData(planDocumnetExcelList);
			if (CollectionUtils.isEmpty(writeData)) {
				message.append("Plan Documnet Excel List is empty !!");
				LOGGER.error(message);
				return;
			}

			OutputStream outputStream = ftpClient.putFileOutputStream(folderPath + trackingRecord.getAttachmentsList());
			if (null != outputStream) {

				XSSFSheet resultsSheet = getSheet(RESULTS_SHEET_INDEX);
				if (null != resultsSheet) {
					writeDataWithXSSF(outputStream, writeData, resultsSheet);
				}
			}
			else {
				message.append("OutputStream is null for file ");
				message.append(trackingRecord.getAttachmentsList());
				LOGGER.error(message);
			}
		}
		catch(Exception ex) {
			message.append(EMSG_ERROR_DOCUMENTS + " Reason: " + ex.getMessage());
			LOGGER.error("writeResultsSheet() Exception occurred: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("writeResultsSheet() End - " + message);
		}
	}

	/**
	 * Method is used to generate Result sheet data for Plan Document Excel.
	 */
	private Map<Integer, Object[]> generateResultSheetData(List<PlanDocumnetExcelVO> planDocumnetExcelList) {

		LOGGER.debug("generateResultSheetData() Strat");
		Map<Integer, Object[]> writeData = null;

		try {
			if (CollectionUtils.isEmpty(planDocumnetExcelList)) {
				return writeData;
			}

			writeData = new HashMap<Integer, Object[]>();
			writeData.put(ZERO, new Object[]{"PLAN ID (With Varient)", "Erro Message"});

			Object[] objArray = null;
			for (int key = ONE; key <= planDocumnetExcelList.size(); key++) {

				PlanDocumnetExcelVO excelVO = planDocumnetExcelList.get(key-1);
				if (null != excelVO) {
					objArray = new Object[WRITE_DATA];
					objArray[ZERO] = excelVO.getPlanId();
					objArray[ONE] = excelVO.getErrorMessages();
				}
				else {
					objArray = null;
				}
				writeData.put(key, objArray);
			}
		}
		finally {
			LOGGER.debug("generateResultSheetData() End");
		}
		return writeData;
	}

	/**
	 * Method is used to upload Excel at ECM server.
	 */
	private boolean addExcelInECM(StringBuilder message, SerffPlanMgmt trackingRecord, String folderPath, String fileName) throws IOException {
		
		LOGGER.debug("addExcelInECM() Strat");
		boolean isSuccess = false;
		InputStream inputStream = null;

		try {
			inputStream = ftpClient.getFileInputStream(folderPath + fileName);
			LOGGER.debug("Uploading File Name with path to ECM:" + SerffConstants.SERF_ECM_BASE_PATH + FTP_PLAN_DOCS_FILE + fileName);
			// Uploading AME Excel at ECM.
			SerffDocument attachment = serffUtils.addAttachmentInECM(ecmService, trackingRecord, SerffConstants.SERF_ECM_BASE_PATH, FTP_PLAN_DOCS_FILE, fileName, IOUtils.toByteArray(inputStream), false);
			serffService.saveSerffDocument(attachment);
			isSuccess = true;
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			message.append(MSG_ERROR_ECM + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("addExcelInECM() End");
		}
		return isSuccess;
	}

	/**
	 * Method is used to get Tracking Record  from SERFF Plan Management Table.
	 */
	private SerffPlanMgmt getTrackingRecord(String serffReqId, StringBuilder message) {

		LOGGER.debug("getTrackingRecord() Strat");
		SerffPlanMgmt trackingRecord = null;

		if (StringUtils.isNumeric(serffReqId)) {
			// Get tracking record from SERFF_PLAN_MGMT table. 
			trackingRecord = serffService.getSerffPlanMgmtById(Long.parseLong(serffReqId));

			if (null == trackingRecord) {
				message.append("Tracking record is not available for serffReqId: ");
				message.append(serffReqId);
			}
		}
		else {
			message.append("Tracking record Id is not available for serffReqId: ");
			message.append(SecurityUtil.sanitizeForLogging(serffReqId));
		}
		LOGGER.debug("getTrackingRecord() End");
		return trackingRecord;
	}

	/**
	 * Method is used to load data from Document List sheet of Excel to ArrayList[PlanDocumnetExcelVO].
	 */
	private List<PlanDocumnetExcelVO> loadPlanDocumentExcelData(XSSFSheet planDocumentSheet) {

		LOGGER.info("loadPlanDocumentExcelData() Start");
		List<PlanDocumnetExcelVO> planDocumnetExcelList = null;

		try {

			if (null == planDocumentSheet) {
				return planDocumnetExcelList;
			}

			Iterator<Row> planRowIterator = planDocumentSheet.iterator();
			PlanDocumnetExcelVO planDocumnetExcelVO = null;
			planDocumnetExcelList = new ArrayList<PlanDocumnetExcelVO>();
			boolean isFirstRow = true;

			while (planRowIterator.hasNext()) {
				Row planExcelRow = planRowIterator.next();

				// Avoid to read first header row.
				if (isFirstRow) {
					isFirstRow = false;
					continue;
				}

				if (null != planExcelRow) {
					planDocumnetExcelVO = new PlanDocumnetExcelVO();
					planDocumnetExcelVO.setPlanId(getCellValueTrim(planExcelRow.getCell(PLAN_ID.getColumnIndex())));
					planDocumnetExcelVO.setApplicableYear(getCellValueTrim(planExcelRow.getCell(APPLICABLE_YEAR.getColumnIndex()), true));
					planDocumnetExcelVO.setBrochureDocumentName(getCellValueTrim(planExcelRow.getCell(BROCHURE_DOC_NAME.getColumnIndex())));
					planDocumnetExcelVO.setBrochureType(getCellValueTrim(planExcelRow.getCell(BROCHURE_TYPE.getColumnIndex())));
					planDocumnetExcelVO.setSbcName(getCellValueTrim(planExcelRow.getCell(SBC_DOCUMENT_NAME.getColumnIndex())));
					planDocumnetExcelVO.setSbcType(getCellValueTrim(planExcelRow.getCell(SBC_TYPE.getColumnIndex())));
					planDocumnetExcelVO.setEocDocumentName(getCellValueTrim(planExcelRow.getCell(EOC_DOCUMENT_NAME.getColumnIndex())));
					planDocumnetExcelVO.setEocType(getCellValueTrim(planExcelRow.getCell(EOC_TYPE.getColumnIndex())));
					planDocumnetExcelList.add(planDocumnetExcelVO);
				}
				else {
					LOGGER.warn("Plan Document Excel's row is empty.");
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("loadPlanDocumentExcelData() Exception: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.info("loadPlanDocumentExcelData() End");
		}
		return planDocumnetExcelList;
	}

	/**
	 * Method is used to Move File from uploadPlansDocs to success/error directory.
	 */
	private void moveFileToDirectory(boolean isSuccess, String fileName) {

		LOGGER.debug("moveFileToDirectory() Start");
		String moveToDirectoryPath = null;

		try {
			String baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansDocUploadPath();
			moveToDirectoryPath = baseDirectoryPath;

			if (isSuccess) {
				moveToDirectoryPath += SerffConstants.FTP_SUCCESS + SerffConstants.PATH_SEPERATOR;
			}
			else {
				moveToDirectoryPath += SerffConstants.FTP_ERROR + SerffConstants.PATH_SEPERATOR;
			}

			if (!ftpClient.isRemoteDirectoryExist(moveToDirectoryPath)) {
				ftpClient.createDirectory(moveToDirectoryPath);
			}
			LOGGER.debug("FTP Folder Path: " + baseDirectoryPath);
			LOGGER.info("Move To Directory Path: " + moveToDirectoryPath);
			ftpClient.moveFile(baseDirectoryPath, fileName, moveToDirectoryPath, fileName);
		}
		catch (Exception ex) {
			LOGGER.info("Skipping file beacause it is already exist in directory: " + moveToDirectoryPath);
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("moveFileToDirectory() End");
		}
	}
}
