package com.getinsured.serffadapter.plandoc;

/**
 * Class is used to generate Enum on the basis of Plan Documents Excel Column.
 */
public class PlanDocumnetExcelColumn {

	private static final int STRING_TYPE = 1;

	public enum PlanDocumnetExcelColumnEnum {

		PLAN_ID("Plan Id", "PLAN_ID", 0, STRING_TYPE),
		APPLICABLE_YEAR("Applicable Year", "ALLICABLE_YEAR", 1, STRING_TYPE),
		BROCHURE_DOC_NAME("Brochure Document Name", "BROUCHER_DOC_NAME", 2, STRING_TYPE),
		BROCHURE_TYPE("Broucher Type", "BROUCHER_TYPE", 3, STRING_TYPE),
		SBC_DOCUMENT_NAME("SBC Document Name", "SBC_NAME", 4, STRING_TYPE),
		SBC_TYPE("SBC Type", "SBC_TYPE", 5, STRING_TYPE),
		EOC_DOCUMENT_NAME("EOC Document Name", "EOC_DOCUMENT_NAME", 6, STRING_TYPE),
		EOC_TYPE("EOC Type", "EOC_TYPE", 7, STRING_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private PlanDocumnetExcelColumnEnum(String name,
				String columnConstName, int index, int dataType) {
			this.columnName = name;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}
}
