package com.getinsured.serffadapter.bulkupload;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.getinsured.serffadapter.excel.ExcelReader;

@Component
public class BulkUploadExcelReader extends ExcelReader {
	
	private static final Logger LOGGER = Logger.getLogger(BulkUploadExcelReader.class);
	private static int EXCEL_SHEET_INDEX = 1;
	private static int PLAN_YEAR_INDEX = 0;
	private static int RECORD_ID_INDEX = 1;
	private static int DOCUMENT_NAME_INDEX = 2;

	/**
	 * This method is used to load and parse the excel file 
	 * @param inputStream
	 * @return List logoBulkUploadVo
	 */
	public List<BulkUploadEntryVO> readLogoBulkUploadList(InputStream inputStream ) {
		
		LOGGER.debug("readLogoBulkUploadExcel () : Starts ");
		
		XSSFSheet issuerLogoSheet = null;
		BulkUploadEntryVO logoBulkUploadEntryVO = null;
		List<BulkUploadEntryVO> logoBulkUploadEntryList = new ArrayList<BulkUploadEntryVO>();
		
		try {
			//loading workbook
			loadFile(inputStream);
			issuerLogoSheet = getSheet(EXCEL_SHEET_INDEX);
			Iterator<Row> rowIterator = issuerLogoSheet.iterator();
			boolean isFirstRow = true;
			while(rowIterator.hasNext()){
				Row excelRow = rowIterator.next();
				
				//ignoring first row
				if (isFirstRow) {
					isFirstRow = false;
					continue;
				}
				
				logoBulkUploadEntryVO = getLogoBulkUploadVO(excelRow);
				if(null != logoBulkUploadEntryVO) {
					logoBulkUploadEntryList.add(logoBulkUploadEntryVO);
				}
			}
			
		} catch (IOException e) {
			LOGGER.error("Exception occured while reaidng Excel file", e);
		}
		LOGGER.debug("readLogoBulkUploadExcel () : Ends ");
		return logoBulkUploadEntryList;
	}

	/***
	 * This method reads Excel rows and set the values in logoBulkUploadVo object
	 * @param excelRow
	 * @return logoBulkUploadVo 
	 */
	private BulkUploadEntryVO getLogoBulkUploadVO(Row excelRow) {
		LOGGER.debug("getLogoBulkUploadExcelVO () : Starts ");
		BulkUploadEntryVO logoBulkUploadVo = null;
		String recordIdentifier, documentName, recordYear;
		if (null != excelRow) {
			recordYear = getCellValueTrim(excelRow.getCell(PLAN_YEAR_INDEX), true);
			recordIdentifier = getCellValueTrim(excelRow.getCell(RECORD_ID_INDEX), true);
			documentName = getCellValueTrim(excelRow.getCell(DOCUMENT_NAME_INDEX));
			if(!StringUtils.isEmpty(recordIdentifier)) {
				logoBulkUploadVo = new BulkUploadEntryVO();
				
				logoBulkUploadVo.setRecordYear(recordYear);
				logoBulkUploadVo.setRecordIdentifier(recordIdentifier);
				logoBulkUploadVo.setFileName(documentName);
			}
		}
		LOGGER.debug("getLogoBulkUploadExcelVO () : Ends ");
		return logoBulkUploadVo;
	}

}
