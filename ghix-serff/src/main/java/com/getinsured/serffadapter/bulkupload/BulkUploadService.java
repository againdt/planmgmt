package com.getinsured.serffadapter.bulkupload;

import static com.serff.util.SerffConstants.FTP_BROCHURE_FILE;
import static com.serff.util.SerffConstants.PATH_SEPERATOR;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.external.cloud.service.ExternalCloudService;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.serff.repository.templates.IIssuersRepository;
import com.serff.repository.templates.ISerffPlanRepository;
import com.serff.service.SerffService;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

@Service("issuerLogoService")
public class BulkUploadService {

	private static final Logger LOGGER = Logger.getLogger(BulkUploadService.class);

	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private SerffUtils serffUtils;
	@Autowired private SerffService serffService;
	@Autowired private BulkUploadExcelReader bulkUploadExcelReader;
	@Autowired private BulkUploadValidator bulkUploadValidator;
	@Autowired private ContentManagementService ecmService;
	@Autowired private IIssuersRepository issuerReporitory;
	@Autowired private ISerffPlanRepository planReporitory;
	@Autowired private SerffResourceUtil serffResourceUtil;
	@Autowired private ExternalCloudService externalCloudService;

	private static String FILE_EXTENSION_TYPE1 = "Xlsx";
	//private static String FILE_EXTENSION_TYPE2 = "xls";
	private static String RENAME_FOLDER_PRFX ="PROCESSED_";
	private static String LOGO_BULK_UPLOAD_OPERATION_TYPE = "TRANSFER_ISSUER_LOGO";
	private static String PLAN_BROCHURE_BULK_UPLOAD_OPERATION_TYPE = "TRANSFER_PLAN_BROCHURE";
	private static int FILE_INDEX = 1;

	private static final String EMSG_PROCESSING_BULK_UPLOAD = "Exception occured while reading processing bulk upload..";

	/**
	 * This method is used to process bulk logo uploads
	 * @param folderPath
	 * @return message
	 */
	public String processLogoBulkUpload(String folderPath) {
		
		LOGGER.info("processBulkLogoUpload() Start");
		StringBuilder message = new StringBuilder();
		SerffPlanMgmt trackingRecord = null;
		String excelFileName = null;
		List<BulkUploadEntryVO> logoBulkUploadVOEntryList = null;
		List<String> logoFileListOnFTP = null;
		
		trackingRecord = createTrackingRecordForLogoBulkUpload(LOGO_BULK_UPLOAD_OPERATION_TYPE);
		if(null == trackingRecord) {
			message.append("Exception occured while processing bulk upload. ");
			return message.toString();
		}
		InputStream inputStream = null;

		try{
			logoFileListOnFTP = ftpClient.getFileNames(folderPath);
			excelFileName = getExcelFileToProcess(logoFileListOnFTP);
			if(null == excelFileName){
				message.append("Either No or Multiple Logo Bulk Upload Excel Template is available for processing..");
			} else {
				LOGGER.debug("processing bulk logo upload excel file :" + folderPath + PATH_SEPERATOR + excelFileName);
				inputStream = ftpClient.getFileInputStream(folderPath + PATH_SEPERATOR + excelFileName);
				if (null == inputStream) {
					message.append("Logo bulk upload excel not found at FTP location.");
				} else {
					//reading Issuer logo Excel
					logoBulkUploadVOEntryList = bulkUploadExcelReader.readLogoBulkUploadList(inputStream);
					boolean anyRecordProcessed = processLogoBulkUploadEntries(logoBulkUploadVOEntryList, trackingRecord, folderPath, logoFileListOnFTP) ;
					if(!anyRecordProcessed) {
						message.append("No Valid Logo is available for processing..");
					} else {
						prepareResponseMessage(logoBulkUploadVOEntryList, message);
						markFolderAsProcessed(folderPath, SerffConstants.getIssuerLogoUploadPath());
					}
				}
			}
		}
		catch (Exception e) {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(e.getMessage(), e);
			}
			LOGGER.error(EMSG_PROCESSING_BULK_UPLOAD);
			message.append(EMSG_PROCESSING_BULK_UPLOAD);
		}
		finally {
			updateTrackingRecord(message, trackingRecord);
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.info("processBulkLogoUpload() Ends");
		}
		return message.toString();
	}


	/**
	 * 
	 * @param bulkUploadVOEntryList
	 * @param message
	 */
	private void prepareResponseMessage(List<BulkUploadEntryVO> bulkUploadVOEntryList, StringBuilder message) {
		StringBuffer missingFiles = new StringBuffer();
		StringBuffer failedECMUpload = new StringBuffer();
		StringBuffer failedUpdateEntry = new StringBuffer();
		for (BulkUploadEntryVO bulkUploadEntryVO : bulkUploadVOEntryList) {
			if(!bulkUploadEntryVO.isFileExist()) {
				missingFiles.append(bulkUploadEntryVO.getRecordIdentifier());
				missingFiles.append(" - ");
				missingFiles.append(bulkUploadEntryVO.getFileName());
				missingFiles.append("\n");
			} else if(!bulkUploadEntryVO.isFileUploadedToECM()) {
				failedECMUpload.append(bulkUploadEntryVO.getRecordIdentifier());
				failedECMUpload.append(" - ");
				failedECMUpload.append(bulkUploadEntryVO.getFileName());
				failedECMUpload.append("\n");
			} else if(!bulkUploadEntryVO.isRecordUpdated()) {
				failedUpdateEntry.append(bulkUploadEntryVO.getRecordIdentifier());
				failedUpdateEntry.append(" - ");
				failedUpdateEntry.append(bulkUploadEntryVO.getFileName());
				if(StringUtils.isNotBlank(bulkUploadEntryVO.getFailedMessage())){
					failedUpdateEntry.append(" - ");
					failedUpdateEntry.append(bulkUploadEntryVO.getFailedMessage());
				}
				failedUpdateEntry.append("\n");
			}
		}
		
		if(missingFiles.length() > 0) {
			message.append("Validation error exists for the following entries, as the document might be missing \n ");
			message.append(missingFiles);
		}
		
		if(failedECMUpload.length() > 0) {
			message.append("Failed to upload document to ECM for the following entries\n ");
			message.append(failedECMUpload);
		}
		
		if(failedUpdateEntry.length() > 0) {
			message.append("Failed to update DB for the following entries\n ");
			message.append(failedUpdateEntry);
		}
	}


	/**
	 * 
	 * @param logoBulkUploadVOEntryList
	 * @param trackingRecord
	 * @param folderPath
	 * @param logoFileListOnFTP
	 * @return
	 */
	private boolean processLogoBulkUploadEntries(List<BulkUploadEntryVO> logoBulkUploadVOEntryList, SerffPlanMgmt trackingRecord, String folderPath, List<String> logoFileListOnFTP) {
		boolean hasRecordToProcess = false;
		//validating Excel entries and files available on FTP 
		hasRecordToProcess = bulkUploadValidator.validateBulkUploadEntries(logoBulkUploadVOEntryList, logoFileListOnFTP);
		if(hasRecordToProcess) {
			Map<String, SerffDocument> fileSerffDocumentMap = new HashMap<String, SerffDocument>();
			String hiosIssuerId = null;
			String logofileName = null;
			SerffDocument serffDocument = null;
			
			for (BulkUploadEntryVO logoBulkUploadVOEntry : logoBulkUploadVOEntryList) {
				//Process only if fileExist flag is set by validator
				if (logoBulkUploadVOEntry.isFileExist()) {
					hiosIssuerId= logoBulkUploadVOEntry.getRecordIdentifier();
					logofileName = logoBulkUploadVOEntry.getFileName();
					serffDocument = fileSerffDocumentMap.get(logofileName);
					
					if (null == serffDocument) {

						Issuer existingIssuer = issuerReporitory.getIssuerByHiosID(hiosIssuerId);
						if (null == existingIssuer) {
							logoBulkUploadVOEntry.setRecordUpdated(false);
							LOGGER.warn("Issuer does not exist with HIOS ID :  " + hiosIssuerId);
						}
						else {
							// Upload each file in CDN
							uploadLogoAndSetDetails(logoBulkUploadVOEntry, existingIssuer, trackingRecord, folderPath,
								logofileName, hiosIssuerId);
						}
					}
				}
			}
		}
		return hasRecordToProcess;
	}

	/**
	 * Method is used upload Company LOGO at CDN server and set LOGO Contents(BLOG) and CDN URL in Issuer table.
	 */
	private void uploadLogoAndSetDetails(BulkUploadEntryVO logoBulkUploadVOEntry, Issuer existingIssuer, SerffPlanMgmt trackingRecord,
			String folderPath, String fileName, String recordIdentifier) {

		LOGGER.info("Execution of uploadLogoAndSetDetails() start");
		InputStream inputStream = null;

		try {

			if (StringUtils.isBlank(fileName)) {
				LOGGER.info("Empty File Name in Execl for Record Identifier : " + recordIdentifier);
				return;
			}

			if (!ftpClient.isRemoteDirectoryExist(folderPath + PATH_SEPERATOR + fileName)) {
				LOGGER.debug("Failed to read file from FTP for : " + folderPath + PATH_SEPERATOR + fileName);
			}
			else {
				inputStream = ftpClient.getFileInputStream(folderPath + PATH_SEPERATOR + fileName);

				if (null == inputStream) {
					LOGGER.debug("Content is not valid for file : " + fileName);
				}
				else {
					String cdnFileName = existingIssuer.getId() + "_"+ existingIssuer.getHiosIssuerId() + fileName;
					LOGGER.debug("Uploading File Name with path to CDN:" + cdnFileName);
					// Uploading Logo at CDN.
					byte[] logoContent = IOUtils.toByteArray(inputStream);
					boolean cdnEnable = StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(SerffConstants.CDN_GLOBAL_URL));

					if (cdnEnable) {
						SerffDocument attachment = serffResourceUtil.addAttachmentInCDN(trackingRecord, cdnFileName, logoContent);

						if (null != attachment) {
							attachment = serffService.saveSerffDocument(attachment);
	
							if (StringUtils.isNotBlank(attachment.getEcmDocId())) {
								logoBulkUploadVOEntry.setFileUploadedToECM(true);
								existingIssuer.setLogoURL(attachment.getEcmDocId());
							}
						}
					}
					existingIssuer.setLogo(logoContent);

					Issuer updatedIssuer = issuerReporitory.save(existingIssuer);
					if (null != updatedIssuer) {
						logoBulkUploadVOEntry.setRecordUpdated(true);
					}
					else {
						logoBulkUploadVOEntry.setRecordUpdated(false);
					}
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error uploading Document in CDN for File : " + fileName + ex.getMessage(), ex);
		}
		finally {
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("Execution of uploadLogoAndSetDetails() end");
		}
	}

	/**
	 * 
	 * @param fileListOnFTP
	 * @return
	 */
	private String getExcelFileToProcess(List<String> fileListOnFTP) {
		String excelFileName = null;
		String fileExtension = null;
		int excelFileCount = 0;
		if (null == fileListOnFTP || fileListOnFTP.isEmpty()) {
			LOGGER.warn("Empty Folder for processing bulk upload..");
		} else {
			for(String file : fileListOnFTP){
				if(null!= file){
					fileExtension = file.split("[.]")[FILE_INDEX];
					if(FILE_EXTENSION_TYPE1.equalsIgnoreCase(fileExtension)){ 
						excelFileCount++;
						excelFileName = file;
					}
				}
			}
			if(excelFileCount > 1){
				excelFileName = null;
			}
		}
		return excelFileName;
	}
	
	/**
	 * 
	 * @return
	 */
	private SerffPlanMgmt createTrackingRecordForLogoBulkUpload(String operationType) {
		SerffPlanMgmt trackingRecord = null;
		try {
			trackingRecord = serffUtils.createSerffPlanMgmtRecord(operationType, SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.P);
			trackingRecord = serffService.saveSerffPlanMgmt(trackingRecord);
		}
		catch(Exception e) {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(e.getMessage(), e);
			}
			LOGGER.error("exception occured while processing bulk upload..");
		}

		return trackingRecord;
	}

	/**
	 * 
	 * @param message
	 * @param trackingRecord
	 */
	private void updateTrackingRecord(StringBuilder message,SerffPlanMgmt trackingRecord) {
		if(null != trackingRecord) {
			if (StringUtils.isEmpty(message.toString())) {
				trackingRecord.setRemarks("Completed Successfully!");
				message.append("Bulk upload completed successfully!");
			} else {
				trackingRecord.setRemarks("Issuer Records updated with warnings!");
			}
			trackingRecord.setPmResponseXml(message.toString());
			trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
			trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
			trackingRecord.setEndTime(new Date());
			serffService.saveSerffPlanMgmt(trackingRecord);
		}
	}

	/**
	 * This method is used to rename the folders which gets processed successfully
	 * @param folderPath
	 */
	private void markFolderAsProcessed(String folderPath, String bulkUploadDirectory) {
		
		String basefolderPath = SerffConstants.getSerffCsrHomePath() + bulkUploadDirectory;
		String oldFileName = null;
		String newFileName = null;
		if(null!=folderPath &&  null!= basefolderPath ){
			
			oldFileName = folderPath.substring(folderPath.lastIndexOf(SerffConstants.PATH_SEPERATOR)).replace(SerffConstants.PATH_SEPERATOR,"");
			newFileName = RENAME_FOLDER_PRFX+oldFileName;
			try {
				ftpClient.moveFile(basefolderPath, oldFileName, basefolderPath, newFileName);
			} catch (Exception e) {
				LOGGER.error("Exception occured while renaming folder : " + oldFileName , e);
			}
		}
		
	}

	/**
	 * This method is used to upload the given file in ECM
	 * @param trackingRecord
	 * @param folderPath
	 * @param fileName
	 * @param recordIdentifier
	 * @return SerffDocument
	 * @throws IOException
	 */
	private SerffDocument uploadFilesInECM(SerffPlanMgmt trackingRecord, String folderPath, String fileName, String recordIdentifier, String ecmFolderName) throws IOException {
		LOGGER.debug("uploadFilesInECM () : Starts");
		SerffDocument attachment = null;
		InputStream inputStream = null;

		try {
			if(null== fileName) {
				LOGGER.info("Empty File Name in Execl for Record Identifier : " + recordIdentifier);	
			} else {
				if(!ftpClient.isRemoteDirectoryExist(folderPath + PATH_SEPERATOR + fileName)){
					LOGGER.debug("Failed to read file from FTP for : " + folderPath + PATH_SEPERATOR + fileName);
				} else {
					inputStream = ftpClient.getFileInputStream(folderPath + PATH_SEPERATOR + fileName);
					if (null == inputStream) {
						LOGGER.debug("Content is not valid for file : " + fileName);
					} else {
						LOGGER.debug("Uploading File Name with path to ECM:" + SerffConstants.SERF_ECM_BASE_PATH + ecmFolderName + fileName);
						// Uploading Excel at ECM.
						attachment = serffUtils.addAttachmentInECM(ecmService, trackingRecord, SerffConstants.SERF_ECM_BASE_PATH, ecmFolderName, fileName, IOUtils.toByteArray(inputStream), false);
						if(null != attachment) {
							attachment = serffService.saveSerffDocument(attachment);
						}
					}
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error uploading Document in ECM for File : " + fileName + ex.getMessage(), ex);
		}
		finally {
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("uploadFilesInECM () : Ends");
		}
		return attachment;
	}
	
	/**
	 * This method is used to update issuer record with ECM ID
	 * @param attachment
	 * @param hiosIssuerId
	 * @param trackingRecord
	 *//*
	private boolean updateIssuerLogo(SerffDocument attachment, String hiosIssuerId, SerffPlanMgmt trackingRecord) {
		LOGGER.debug("updateIssuerLogo() : Starts");
		boolean updateDone = false;
		Issuer existingIssuer = issuerReporitory.getIssuerByHiosID(hiosIssuerId);
		if(null == existingIssuer){
			LOGGER.warn("Issuer does not exist with HIOS ID :  " + hiosIssuerId);
		}else{
			existingIssuer.setCompany Logo(attachment.getEcmDocId());
			issuerReporitory.save(existingIssuer);
			updateDone = true;
		}
		LOGGER.debug("updateIssuerLogo() : ends");
		return updateDone;
	}*/
		

	private boolean updatePlanBrochure(SerffDocument attachment, String planId, String recordYear, SerffPlanMgmt trackingRecord, BulkUploadEntryVO bulkUploadEntryVO) {
		LOGGER.debug("updatePlanBrochure() : Starts");
		boolean updateDone = false;
		try{
			int planYear = Integer.parseInt(recordYear);
			Plan activePlan = planReporitory.findByIssuerPlanNumberAndIsDeletedAndApplicableYear(planId, SerffConstants.NO_ABBR, planYear);
			if(null == activePlan){
				LOGGER.warn("Plan does not exist with Plan ID :  " + planId);
				bulkUploadEntryVO.setFailedMessage("Plan does not exist " + planId + "\n");
			}else{
				activePlan.setBrochureUCmId(attachment.getEcmDocId());
				activePlan.setBrochureDocName(attachment.getDocName());
				planReporitory.save(activePlan);
				updateDone = true;
			}
		}catch(NumberFormatException nfe){
			LOGGER.error("Invalid plan year " + recordYear , nfe);
			bulkUploadEntryVO.setFailedMessage("Invalid plan year " + recordYear + "\n");
		}catch(Exception e){
			LOGGER.error("Error occurred for plan " + planId + ". " + e.getMessage(), e);
			bulkUploadEntryVO.setFailedMessage("Error occurred for plan " + planId + "-" + e.getMessage() + "\n");
		}
		LOGGER.debug("updatePlanBrochure() : ends");
		return updateDone;
	}
	
	public String processPlanBrochureBulkUpload(String folderPath) {
		
		LOGGER.info("processPlanBrochureBulkUpload() Start");
		StringBuilder message = new StringBuilder();
		SerffPlanMgmt trackingRecord = null;
		String excelFileName = null;
		List<BulkUploadEntryVO> brochureBulkUploadVOEntryList = null;
		List<String> brochureFileListOnFTP = null;
		InputStream inputStream = null;
		
		trackingRecord = createTrackingRecordForLogoBulkUpload(PLAN_BROCHURE_BULK_UPLOAD_OPERATION_TYPE);
		if(null == trackingRecord) {
			message.append("Exception occured while processing brochure bulk upload.");
			return message.toString();
		} 

		try{
			brochureFileListOnFTP = ftpClient.getFileNames(folderPath);
			excelFileName = getExcelFileToProcess(brochureFileListOnFTP);
			if(null == excelFileName){
				message.append("No Brochure Bulk Upload Excel Template is available for processing..");
			} else {
				LOGGER.debug("processing bulk brochure upload excel file :" + folderPath + PATH_SEPERATOR + excelFileName );
				inputStream = ftpClient.getFileInputStream(folderPath + PATH_SEPERATOR + excelFileName);
				if (null == inputStream) {
					message.append("Brochure bulk upload excel not found at FTP location.");
				} else {
					//reading Issuer logo Excel
					brochureBulkUploadVOEntryList = bulkUploadExcelReader.readLogoBulkUploadList(inputStream);
					boolean anyRecordProcessed = processBrochureBulkUploadEntries(brochureBulkUploadVOEntryList, trackingRecord, folderPath, brochureFileListOnFTP, message) ;
					if(!anyRecordProcessed) {
						message.append("No Valid Brochure is available for processing..");
					} else {
						prepareResponseMessage(brochureBulkUploadVOEntryList, message);
						markFolderAsProcessed(folderPath, SerffConstants.getPlanBrochureUploadPath());
					}
				}
			}
		}
		catch (Exception e) {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(e.getMessage(), e);
			}
			LOGGER.error(EMSG_PROCESSING_BULK_UPLOAD);
			message.append(EMSG_PROCESSING_BULK_UPLOAD);
		}
		finally {
			updateTrackingRecord(message, trackingRecord);
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
		}
		LOGGER.info("processPlanBrochureBulkUpload() Ends");
		return message.toString();
	}

	
	private boolean processBrochureBulkUploadEntries(List<BulkUploadEntryVO> brochureBulkUploadVOEntryList, SerffPlanMgmt trackingRecord, String folderPath, List<String> brochureFileListOnFTP, StringBuilder message) {
		boolean hasRecordToProcess = false;
		//validating Excel entries and files available on FTP 
		hasRecordToProcess = bulkUploadValidator.validateBulkUploadEntries(brochureBulkUploadVOEntryList, brochureFileListOnFTP);
		if(hasRecordToProcess) {
			Map<String, SerffDocument> fileSerffDocumentMap = new HashMap<String, SerffDocument>();
			String planId = null;
			String recordYear = null;
			String brochureFileName = null;
			SerffDocument serffDocument = null;
			boolean recordUpdated = false;
			//folder name on ECM
			String ecmFolderName = trackingRecord.getSerffReqId() + FTP_BROCHURE_FILE + Calendar.getInstance().getTimeInMillis();
			
			for (BulkUploadEntryVO brochureBulkUploadVOEntry : brochureBulkUploadVOEntryList) {
				//Process only if fileExist flag is set by validator
				if(brochureBulkUploadVOEntry.isFileExist()) {
					planId= brochureBulkUploadVOEntry.getRecordIdentifier();
					brochureFileName = brochureBulkUploadVOEntry.getFileName();
					serffDocument = fileSerffDocumentMap.get(brochureFileName);
					recordYear = brochureBulkUploadVOEntry.getRecordYear();
					
					if(null == serffDocument) {
						//upload each file in ECM
						try {
							serffDocument = uploadFilesInECM(trackingRecord, folderPath, brochureFileName, planId, ecmFolderName);
							fileSerffDocumentMap.put(brochureFileName, serffDocument);
						} catch (IOException e) {
							brochureBulkUploadVOEntry.setFileUploadedToECM(false);
							LOGGER.error("Failed to upload Brochure to ECM. " + brochureFileName, e);
						}
					}
					
					if(null == serffDocument){
						LOGGER.info("Failed to upload Brochure to ECM. " + brochureFileName);
					} else {
						brochureBulkUploadVOEntry.setFileUploadedToECM(true);
						//updating plan records
						recordUpdated = updatePlanBrochure(serffDocument, planId, recordYear, trackingRecord, brochureBulkUploadVOEntry);
						brochureBulkUploadVOEntry.setRecordUpdated(recordUpdated);
					}
				}
			}
		}
		return hasRecordToProcess;
	}

	public String migrateIssuerLogoToCDN() {
		boolean cdnEnabled = StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(SerffConstants.CDN_GLOBAL_URL));
		LOGGER.debug("==== Starting Issuer Logo Migration activity with CDN Enabled:  " + cdnEnabled );
		String message = null;
		List<Issuer> issuerList = issuerReporitory.getIssuersForLogoMigration();
		int failedCount = 0, passCount = 0;
		boolean logoMigrated = false;
		
		if(CollectionUtils.isEmpty(issuerList)){
			message = "No Issuer is pending for logo migration";
			LOGGER.info(message);
			return message;
		}

		for (Issuer issuer : issuerList) {
			String logoEcmID = issuer.getCompanyLogo();
			Content logoContents;
			try {
				logoContents = ecmService.getContentById(logoEcmID);
				if(null == logoContents) {
					LOGGER.error("Logo not found in ECM for Issuer HIOS ID :  " + issuer.getHiosIssuerId());
				} else {
					byte[] logoBytes = ecmService.getContentDataById(logoEcmID);
					if(cdnEnabled) {
						URL docUrl = externalCloudService.upload(SerffConstants.CDN_ISSUER_LOGO_PATH, issuer.getId() + "_"+ issuer.getHiosIssuerId() + logoContents.getOriginalFileName(), logoBytes);
						LOGGER.debug("Done upload to CDN At: " + docUrl.toString() );
						issuer.setLogoURL(docUrl.toString());
					} else {
						issuer.setLogoURL(null);
					}
					issuer.setLogo(logoBytes);
					issuerReporitory.save(issuer);
					logoMigrated = true;
					logoBytes = null;
					LOGGER.debug("Logo migrated for Issuer HIOS ID :  " + issuer.getHiosIssuerId());
				}
				logoContents = null;
			}
			catch (ContentManagementServiceException e) {

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(e.getMessage(), e);
				}
				LOGGER.error("Failed to get logo from ECM for Issuer HIOS ID :  " + issuer.getHiosIssuerId());
			}
			catch (Exception e) {

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug(e.getMessage(), e);
				}
				LOGGER.error("Error while fetching logo for Issuer HIOS ID :  " + issuer.getHiosIssuerId());
			}

			if(logoMigrated) {
				logoMigrated = false;
				passCount++;
			} else {
				failedCount++;
			}
		}
		
		message = "Completed migration for " + passCount + " issuer logo. Migration failed for " + failedCount + " issuer logo. Check logs for details.";
		LOGGER.info(message);
		return message;
	}
}

