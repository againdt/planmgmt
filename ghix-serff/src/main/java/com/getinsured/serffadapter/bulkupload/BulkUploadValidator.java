package com.getinsured.serffadapter.bulkupload;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

@Component
public class BulkUploadValidator {
	private static final Logger LOGGER = Logger.getLogger(BulkUploadValidator.class);

	/**
	 * This method is used to validate physical files with excel mapping
	 * @param logoBulkUploaVoMap
	 * @param fileList
	 * @return
	 */
	public boolean validateBulkUploadEntries(List<BulkUploadEntryVO> bulkUploadVOList, List<String> ftpFileList) {
		LOGGER.debug("validateBulkUploadEntries () : Starts");
		boolean hasValidEntry = false;
		if(!CollectionUtils.isEmpty(bulkUploadVOList)){
			for (BulkUploadEntryVO fileEntry : bulkUploadVOList ){
				 if(CollectionUtils.isEmpty(ftpFileList) || !ftpFileList.contains(fileEntry.getFileName())){
					 fileEntry.setFileExist(false);
				} else {
					 fileEntry.setFileExist(true);
					 hasValidEntry = true;
				}
			}
		}
		LOGGER.debug("validateBulkUploadEntries () : Ends");
		return hasValidEntry;
	}
}
