package com.getinsured.serffadapter.bulkupload;
/**
 * This is VO class for IssuerLogo
 * @author sharma_va
 *
 */
public class BulkUploadEntryVO {

	private String recordIdentifier;
	private String fileName;
	private String recordYear;
	private boolean fileExist;
	private boolean fileUploadedToECM;
	private boolean recordUpdated;
	private String failedMessage;
	
	public BulkUploadEntryVO() {
		fileExist = false;
		fileUploadedToECM = false;
		recordUpdated = false;
	}
	
	public String getRecordIdentifier() {
		return recordIdentifier;
	}

	public void setRecordIdentifier(String recordIdentifier) {
		this.recordIdentifier = recordIdentifier;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public boolean isFileExist() {
		return fileExist;
	}

	public void setFileExist(boolean isFileExist) {
		this.fileExist = isFileExist;
	}

	public boolean isFileUploadedToECM() {
		return fileUploadedToECM;
	}

	public void setFileUploadedToECM(boolean isFileUploadedToECM) {
		this.fileUploadedToECM = isFileUploadedToECM;
	}

	public boolean isRecordUpdated() {
		return recordUpdated;
	}

	public void setRecordUpdated(boolean isRecordUpdated) {
		this.recordUpdated = isRecordUpdated;
	}

	public String getRecordYear() {
		return recordYear;
	}

	public void setRecordYear(String recordYear) {
		this.recordYear = recordYear;
	}

	public String getFailedMessage() {
		return failedMessage;
	}

	public void setFailedMessage(String failedMessage) {
		this.failedMessage = failedMessage;
	}
	
}
