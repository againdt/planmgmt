package com.getinsured.serffadapter.xmlwriter;


import org.apache.log4j.Logger;

import com.getinsured.hix.platform.util.exception.GIException;
import com.serff.template.networkProv.extension.PayloadType;
import com.serff.template.networkProv.hix.core.OrganizationAugmentationType;
import com.serff.template.networkProv.hix.core.OrganizationType;
import com.serff.template.networkProv.hix.pm.HealthcareProviderNetworkType;
import com.serff.template.networkProv.hix.pm.IssuerType;
import com.serff.template.networkProv.niem.core.ContactInformationType;
import com.serff.template.networkProv.niem.core.IdentificationType;
import com.serff.template.networkProv.niem.core.TextType;
import com.serff.template.networkProv.structures.ReferenceType;
import com.serff.template.networkProv.usps.states.USStateCodeSimpleType;
import com.serff.template.networkProv.usps.states.USStateCodeType;
import com.serff.template.networkProv.xsd.AnyURI;
import com.serff.util.SerffUtils;

public class NetworkTemplateXMLWriter {

	private static final Logger LOGGER = Logger
			.getLogger(NetworkTemplateXMLWriter.class);

	private PayloadType network;

	/**
	 * 
	 * @return
	 * @throws GIException 
	 */
	public String generateNetworkTemplateXML() throws GIException {
		return new SerffUtils().objectToXmlString(network);
	}

	/**
	 * 
	 * @return
	 */
	public NetworkTemplateXMLWriter() {
		network = new PayloadType();
	}

	/**
	 * 
	 * @param networkID
	 * @param roleOfOrganizationReference
	 * @return
	 */
	private HealthcareProviderNetworkType createHealthcareProviderNetworkType(
			String networkID, String roleOfOrganizationReference) {
		LOGGER.info("createHealthcareProviderNetworkType() : networkID : "
				+ networkID + " : roleOfOrganizationReference : "
				+ roleOfOrganizationReference);
		HealthcareProviderNetworkType healthcareProviderNetworkType = new HealthcareProviderNetworkType();
		healthcareProviderNetworkType
				.setProviderNetworkIdentification(createIdentificationType(networkID));
		healthcareProviderNetworkType
				.setRoleOfOrganizationReference(createReferenceType(roleOfOrganizationReference));
		return healthcareProviderNetworkType;
	}

	/**
	 * 
	 * @param issuerIdentification
	 * @param issuerStateCode
	 * @return
	 */
	public void setIssuer(String issuerIdentification, String issuerStateCode) {
		LOGGER.info("setIssuer() : issuerIdentification : "
				+ issuerIdentification + " : issuerStateCode : "
				+ issuerStateCode);
		IssuerType issuer = new IssuerType();
		issuer.setIssuerIdentification(createIdentificationType(issuerIdentification));
		issuer.setIssuerStateCode(createUSStateCodeType(issuerStateCode));
		network.setIssuer(issuer);
		/*
		 * if (null != healthcareProviderNetworkList) {
		 * issuer.getHealthcareProviderNetwork
		 * ().addAll(healthcareProviderNetworkList); }
		 */
	}

	/**
	 * 
	 * @param usStateCode
	 * @return
	 */
	private USStateCodeType createUSStateCodeType(String usStateCode) {
		LOGGER.info("createUSStateCodeType() : usStateCode : " + usStateCode);
		USStateCodeType usStateCodeType = new USStateCodeType();
		usStateCodeType.setValue(USStateCodeSimpleType.fromValue(usStateCode));

		return usStateCodeType;
	}

	/**
	 * 
	 * @param identificationId
	 * @return
	 */
	private IdentificationType createIdentificationType(
			java.lang.String identificationId) {
		LOGGER.info("createIdentificationType() : identificationId : "
				+ identificationId);
		IdentificationType identificationType = new IdentificationType();
		com.serff.template.networkProv.xsd.String identificationTypeValue = new com.serff.template.networkProv.xsd.String();
		identificationTypeValue.setValue(identificationId);
		identificationType.setIdentificationID(identificationTypeValue);

		return identificationType;
	}

	private ReferenceType createReferenceType(Object reference) {
		LOGGER.info("createReferenceType() : reference : " + reference);
		ReferenceType referenceType = new ReferenceType();
		referenceType.setRef(reference);
		return referenceType;
	}

	/**
	 * 
	 * @param contactInformation
	 * @param organizationAugmentationType
	 * @return
	 */
	public void addNetwork(String networkID,
			String roleOfOrganizationReference, String websiteURI,
			String marketingName) {
		LOGGER.info("addNetwork() : networkID : " + networkID
				+ " : roleOfOrganizationReference : "
				+ roleOfOrganizationReference + " : websiteURI : " + websiteURI
				+ " : marketingName : " + marketingName);
		OrganizationType organizationType = new OrganizationType();
		organizationType
				.setOrganizationPrimaryContactInformation(createOrganizationPrimaryContactInformation(websiteURI));
		organizationType
				.setOrganizationAugmentation(createOrganizationAugmentationType(marketingName));
		network.getOrganization().add(organizationType);
		network.getIssuer()
				.getHealthcareProviderNetwork()
				.add(createHealthcareProviderNetworkType(networkID,
						roleOfOrganizationReference));
	}

	/**
	 * 
	 * @param websiteURI
	 * @return
	 */
	private ContactInformationType createOrganizationPrimaryContactInformation(
			String websiteURI) {
		LOGGER.info("createOrganizationPrimaryContactInformation() : websiteURI : "
				+ websiteURI);
		ContactInformationType contactInformation = new ContactInformationType();
		AnyURI uri = new AnyURI();
		uri.setValue(websiteURI);
		contactInformation.setContactWebsiteURI(uri);

		return contactInformation;
	}

	/**
	 * 
	 * @param marketingName
	 * @return
	 */
	private OrganizationAugmentationType createOrganizationAugmentationType(
			String marketingName) {
		LOGGER.info("createOrganizationAugmentationType() : marketingName : "
				+ marketingName);
		OrganizationAugmentationType organizationAugmentationType = new OrganizationAugmentationType();
		TextType text = new TextType();
		text.setValue(marketingName);
		organizationAugmentationType.setOrganizationMarketingName(text);

		return organizationAugmentationType;
	}

}
