package com.getinsured.serffadapter.xmlwriter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.getinsured.hix.platform.util.exception.GIException;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.QhpApplicationRateGroupVO;
import com.serff.template.plan.QhpApplicationRateHeaderVO;
import com.serff.template.plan.QhpApplicationRateItemVO;
import com.serff.util.SerffUtils;

public class RatesTemplateXMLWriter {

	private static final Logger LOGGER = Logger
			.getLogger(RatesTemplateXMLWriter.class);
	private QhpApplicationRateHeaderVO qhpAppRateHeaderVO;
	private List<QhpApplicationRateItemVO> items;
	private static final String EFFECTIVE_DATE = "effectiveDate";
	private static final String EXPIRATION_DATE = "expirationDate";
	private static final String TOBACCO = "tobacco";
	private static final String PLAN_ID = "planId";
	private static final String RATEAREA_ID = "rateAreaId";
	private static final String AGE_NUMBER = "ageNumber";
	private static final String PRIMARY_ENROLLEE = "primaryEnrollee";
	private static final String PRIMARY_ENROLLEE_TOBACCO = "primaryEnrolleeTobacco";
	private static final String PRIMARY_ENROLLEE_MANY_DEPENDENT = "primaryEnrolleeManyDependent";
	private static final String PRIMARY_ENROLLEE_ONE_DEPENDENT = "primaryEnrolleeOneDependent";
	private static final String PRIMARY_ENROLLEE_TWO_DEPENDENT = "primaryEnrolleeTwoDependent";
	private static final String COUPLE_ENROLLEE = "coupleEnrollee";
	private static final String COUPLE_ENROLLEE_MANY_DEPENDENT = "coupleEnrolleeManyDependent";
	private static final String COUPLE_ENROLLEE_ONE_DEPENDENT = "coupleEnrolleeOneDependent";
	private static final String COUPLE_ENROLLEE_TWO_DEPENDENT = "coupleEnrolleeTwoDependent";
	private static final String ISSUER_ID = "issuerId";
	private static final String TIN = "tin";

	public RatesTemplateXMLWriter() {
		super();
		qhpAppRateHeaderVO = new QhpApplicationRateHeaderVO();
		items = new ArrayList<QhpApplicationRateItemVO>();
	}

	public String generateRatesTemplateXML() throws GIException {
		QhpApplicationRateGroupVO qhpAppRateGroupVO = new QhpApplicationRateGroupVO();
		qhpAppRateGroupVO.setHeader(qhpAppRateHeaderVO);
		if (null != items) {
			qhpAppRateGroupVO.getItems().addAll(items);
		}
		return new SerffUtils().objectToXmlString(qhpAppRateGroupVO);
	}

	public void addQhpApplicationRateItem(String planId, String rateAreaId,
			String tobacco, String ageNumber, String effectiveDate,
			String expirationDate, Double primaryEnrollee,
			Double primaryEnrolleeTobacco, String isIssuerData) {

		LOGGER.info("addQhpApplicationRateItem() : planId : " + planId
				+ " : rateAreaId : " + rateAreaId);
		QhpApplicationRateItemVO qhpAppRateItemVO = new QhpApplicationRateItemVO();
		qhpAppRateItemVO.setEffectiveDate(createExcelCell(EFFECTIVE_DATE,
				effectiveDate));
		qhpAppRateItemVO.setExpirationDate(createExcelCell(EXPIRATION_DATE,
				expirationDate));
		qhpAppRateItemVO.setTobacco(createExcelCell(TOBACCO, tobacco));
		qhpAppRateItemVO.setPlanId(createExcelCell(PLAN_ID, planId));
		qhpAppRateItemVO
				.setRateAreaId(createExcelCell(RATEAREA_ID, rateAreaId));
		qhpAppRateItemVO.setAgeNumber(createExcelCell(AGE_NUMBER, ageNumber));
		qhpAppRateItemVO.setPrimaryEnrollee(createExcelCell(PRIMARY_ENROLLEE,
				primaryEnrollee.toString()));
		qhpAppRateItemVO.setPrimaryEnrolleeTobacco(createExcelCell(
				PRIMARY_ENROLLEE_TOBACCO, primaryEnrolleeTobacco.toString()));
		qhpAppRateItemVO.setIsIssuerData(isIssuerData);
		items.add(qhpAppRateItemVO);
	}

	public void addQhpApplicationRateItem(String planId, String rateAreaId,
			String tobacco, String ageNumber, String effectiveDate,
			String expirationDate, Double primaryEnrollee,
			Double primaryEnrolleeOneDependent,
			Double primaryEnrolleeTwoDependent,
			Double primaryEnrolleeManyDependent, Double coupleEnrollee,
			Double coupleEnrolleeOneDependent,
			Double coupleEnrolleeTwoDependent,
			Double coupleEnrolleeManyDependent, String isIssuerData) {
		LOGGER.info("addQhpApplicationRateItem() : planId : " + planId
				+ " : rateAreaId : " + rateAreaId);
		QhpApplicationRateItemVO qhpAppRateItemVO = new QhpApplicationRateItemVO();
		qhpAppRateItemVO.setEffectiveDate(createExcelCell(EFFECTIVE_DATE,
				effectiveDate));
		qhpAppRateItemVO.setExpirationDate(createExcelCell(EXPIRATION_DATE,
				expirationDate));
		qhpAppRateItemVO.setTobacco(createExcelCell(TOBACCO, tobacco));
		qhpAppRateItemVO.setPlanId(createExcelCell(PLAN_ID, planId));
		qhpAppRateItemVO
				.setRateAreaId(createExcelCell(RATEAREA_ID, rateAreaId));
		qhpAppRateItemVO.setAgeNumber(createExcelCell(AGE_NUMBER, ageNumber));
		qhpAppRateItemVO.setPrimaryEnrollee(createExcelCell(PRIMARY_ENROLLEE,
				primaryEnrollee.toString()));
		// qhpAppRateItemVO.setPrimaryEnrolleeTobacco(createExcelCellVO(PRIMARY_ENROLLEE_TOBACCO,
		// primaryEnrolleeTobacco));
		qhpAppRateItemVO.setPrimaryEnrolleeManyDependent(createExcelCell(
				PRIMARY_ENROLLEE_MANY_DEPENDENT,
				primaryEnrolleeManyDependent.toString()));
		qhpAppRateItemVO.setPrimaryEnrolleeOneDependent(createExcelCell(
				PRIMARY_ENROLLEE_ONE_DEPENDENT,
				primaryEnrolleeOneDependent.toString()));
		qhpAppRateItemVO.setPrimaryEnrolleeTwoDependent(createExcelCell(
				PRIMARY_ENROLLEE_TWO_DEPENDENT,
				primaryEnrolleeTwoDependent.toString()));
		qhpAppRateItemVO.setCoupleEnrollee(createExcelCell(COUPLE_ENROLLEE,
				coupleEnrollee.toString()));
		qhpAppRateItemVO.setCoupleEnrolleeManyDependent(createExcelCell(
				COUPLE_ENROLLEE_MANY_DEPENDENT,
				coupleEnrolleeManyDependent.toString()));
		qhpAppRateItemVO.setCoupleEnrolleeOneDependent(createExcelCell(
				COUPLE_ENROLLEE_ONE_DEPENDENT,
				coupleEnrolleeOneDependent.toString()));
		qhpAppRateItemVO.setCoupleEnrolleeTwoDependent(createExcelCell(
				COUPLE_ENROLLEE_TWO_DEPENDENT,
				coupleEnrolleeTwoDependent.toString()));
		qhpAppRateItemVO.setIsIssuerData(isIssuerData);

		/*
		 * qhpAppRateItemVO.setAgeNumberValue(value);
		 * qhpAppRateItemVO.setCoupleEnrolleeValue(value)
		 * qhpAppRateItemVO.setCoupleEnrolleeManyDependentValue(value)
		 * qhpAppRateItemVO.setCoupleEnrolleeOneDependentValue(value)
		 * qhpAppRateItemVO.setCoupleEnrolleeTwoDependentValue(value)
		 * qhpAppRateItemVO.setPrimaryEnrolleeValue(value)
		 * qhpAppRateItemVO.setPrimaryEnrolleeTobaccoValue(value)
		 * qhpAppRateItemVO.setPrimaryEnrolleeManyDependentValue(value)
		 * qhpAppRateItemVO.setPrimaryEnrolleeOneDependentValue(value)
		 * qhpAppRateItemVO.setPrimaryEnrolleeTwoDependentValue(value)
		 * qhpAppRateItemVO.setEffectiveDateValue(value)
		 * qhpAppRateItemVO.setExpirationDateValue(value)
		 * qhpAppRateItemVO.setPlanIdValue(value)
		 * qhpAppRateItemVO.setRateAreaIdValue(value)
		 * qhpAppRateItemVO.setTobaccoValue(value)
		 */
		items.add(qhpAppRateItemVO);
	}

	public void setQhpApplicationRateHeader(String issuerID, String tin) {
		LOGGER.info("setQhpApplicationRateHeader() : issuerID : " + issuerID
				+ " : tin : " + tin);
		qhpAppRateHeaderVO.setIssuerId(createExcelCell(ISSUER_ID, issuerID));
		qhpAppRateHeaderVO.setTin(createExcelCell(TIN, tin));
	}

	/**
	 * 
	 * @param cellName
	 * @param cellValue
	 * @param rowNumber
	 * @param colNumber
	 * @param cellLocation
	 * @return
	 */
	public ExcelCellVO createExcelCell(String cellName, String cellValue) {
		LOGGER.info("createExcelCell(): cellName: " + cellName + ", cellValue: " + cellValue);
		ExcelCellVO excelCellVO = new ExcelCellVO();
		// excelCellVO.setCellName(cellName);
		excelCellVO.setCellValue(cellValue);
		return excelCellVO;
	}

}
