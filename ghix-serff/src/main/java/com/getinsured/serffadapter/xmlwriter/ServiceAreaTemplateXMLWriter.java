package com.getinsured.serffadapter.xmlwriter;

import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.log4j.Logger;

import com.getinsured.hix.platform.util.exception.GIException;
import com.serff.template.svcarea.extension.PayloadType;
import com.serff.template.svcarea.hix.pm.GeographicAreaType;
import com.serff.template.svcarea.hix.pm.IssuerType;
import com.serff.template.svcarea.hix.pm.ServiceAreaType;
import com.serff.template.svcarea.niem.core.IdentificationType;
import com.serff.template.svcarea.niem.core.ObjectFactory;
import com.serff.template.svcarea.niem.core.ProperNameTextType;
import com.serff.template.svcarea.niem.core.TextType;
import com.serff.template.svcarea.niem.fips.USCountyCodeType;
import com.serff.template.svcarea.niem.proxy.xsd.Boolean;
import com.serff.template.svcarea.niem.usps.states.USStateCodeSimpleType;
import com.serff.template.svcarea.niem.usps.states.USStateCodeType;
import com.serff.util.SerffUtils;

public class ServiceAreaTemplateXMLWriter {

	private PayloadType rootElement;

	private static final Logger LOGGER = Logger
			.getLogger(ServiceAreaTemplateXMLWriter.class);

	public ServiceAreaTemplateXMLWriter() {
		super();
		rootElement = new PayloadType();
		rootElement.setIssuer(new IssuerType());
	}

	public String generateServiceAreaTemplateXML() throws GIException {
		return new SerffUtils().objectToXmlString(rootElement);
	}

	public void setIssuerIdentification(String hiosID, String stateCode) {

		LOGGER.info("setIssuerIdentification() : hiosID : " + hiosID
				+ " : stateCode : + " + stateCode);
		com.serff.template.svcarea.niem.proxy.xsd.String idValue = new com.serff.template.svcarea.niem.proxy.xsd.String();
		idValue.setValue(hiosID);
		IdentificationType identificationType = new IdentificationType();
		identificationType.setIdentificationID(idValue);
		rootElement.getIssuer().setIssuerIdentification(identificationType);
		USStateCodeType stateValue = new USStateCodeType();
		stateValue.setValue(USStateCodeSimpleType.fromValue(stateCode));
		rootElement.getIssuer().setIssuerStateCode(stateValue);
	}

	public void addServiceAreaForFullState(String serviceAreaId,
			String serviceAreaName) {
		addServiceAreaForFullCounty(serviceAreaId, serviceAreaName, true,
				false, null, null, null);
	}

	public void addServiceAreaForFullCounty(String serviceAreaId,
			String serviceAreaName, String countyCode) {
		addServiceAreaForFullCounty(serviceAreaId, serviceAreaName, false,
				false, countyCode, null, null);
	}

	public void addServiceAreaForPartialCounty(String serviceAreaId,
			String serviceAreaName, String countyCode,
			String partialCountyJustification, List<String> postalCodes) {
		addServiceAreaForFullCounty(serviceAreaId, serviceAreaName, false,
				true, countyCode, partialCountyJustification, postalCodes);
	}

	private void addServiceAreaForFullCounty(String serviceAreaId,
			String serviceAreaName, boolean entireState, boolean partialCounty,
			String countyCode, String partialCountyJustification,
			List<String> postalCodes) {

		LOGGER.info("addServiceAreaForFullCounty() : serviceAreaId : "
				+ serviceAreaId + " : serviceAreaName : + " + serviceAreaName
				+ " : partialCounty : " + partialCounty + " : countyCode : "
				+ countyCode);

		ServiceAreaType serviceArea = new ServiceAreaType();
		com.serff.template.svcarea.niem.proxy.xsd.String svcAreaIDValue = new com.serff.template.svcarea.niem.proxy.xsd.String();
		svcAreaIDValue.setValue(serviceAreaId);
		IdentificationType serviceAreaIdentification = new IdentificationType();
		serviceAreaIdentification.setIdentificationID(svcAreaIDValue);
		serviceArea.setServiceAreaIdentification(serviceAreaIdentification);

		ProperNameTextType svceAreaName = new ProperNameTextType();
		svceAreaName.setValue(serviceAreaName);
		serviceArea.setServiceAreaName(svceAreaName);

		GeographicAreaType geoArea = new GeographicAreaType();
		Boolean entireStateIndicator = new Boolean();
		entireStateIndicator.setValue(entireState);
		geoArea.setGeographicAreaEntireStateIndicator(entireStateIndicator);

		if (!entireState) {
			USCountyCodeType usCountyCode = new USCountyCodeType();
			usCountyCode.setValue(countyCode);
			ObjectFactory objFact = new com.serff.template.svcarea.niem.core.ObjectFactory();
			JAXBElement<USCountyCodeType> countyCodeValue = objFact
					.createLocationCountyCode(usCountyCode);
			geoArea.setLocationCounty(countyCodeValue);

			Boolean partialCountyIndicator = new Boolean();
			partialCountyIndicator.setValue(partialCounty);
			geoArea.setGeographicAreaPartialCountyIndicator(partialCountyIndicator);

			if (partialCounty) {
				List<com.serff.template.svcarea.niem.proxy.xsd.String> postalCodesList = geoArea
						.getLocationPostalCode();

				if (postalCodes != null) {
					for (String postalCode : postalCodes) {
						com.serff.template.svcarea.niem.proxy.xsd.String pCode = new com.serff.template.svcarea.niem.proxy.xsd.String();
						pCode.setValue(postalCode);
						postalCodesList.add(pCode);
					}
				}

				TextType partialCountyJustificationText = new TextType();
				partialCountyJustificationText
						.setValue(partialCountyJustification);
				geoArea.setGeographicAreaPartialCountyJustificationText(partialCountyJustificationText);
			}
		}
		serviceArea.getGeographicArea().add(geoArea);
		rootElement.getIssuer().getServiceArea().add(serviceArea);
	}

}
