package com.getinsured.serffadapter.xmlwriter;

import java.util.HashMap;
import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.vo.BenefitVO;
import com.getinsured.serffadapter.vo.DeductibleVO;
import com.serff.template.plan.BenefitAttributeVO;
import com.serff.template.plan.BenefitAttributesListVO;
import com.serff.template.plan.BenefitsListVO;
import com.serff.template.plan.CostShareVarianceVO;
import com.serff.template.plan.CostShareVariancesListVO;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.HeaderVO;
import com.serff.template.plan.MoopListVO;
import com.serff.template.plan.MoopVO;
import com.serff.template.plan.PackageListVO;
import com.serff.template.plan.PlanAndBenefitsPackageVO;
import com.serff.template.plan.PlanAndBenefitsVO;
import com.serff.template.plan.PlanAttributeVO;
import com.serff.template.plan.PlanBenefitTemplateVO;
import com.serff.template.plan.PlanDeductibleListVO;
import com.serff.template.plan.PlanDeductibleVO;
import com.serff.template.plan.PlansListVO;
import com.serff.template.plan.SBCVO;
import com.serff.template.plan.ServiceVisitListVO;
import com.serff.template.plan.ServiceVisitVO;
import com.serff.util.SerffConstants;
import com.serff.util.SerffUtils;

/**
 * 
 * @author vardekar_s
 * 
 */
public class PlanBenefitTemplateXMLWriter {

	private PlanBenefitTemplateVO planBenefitTemplate;
	private Map<String, CostShareVarianceVO> planVariantMap;
	private Map<String, PlanAndBenefitsVO> plansMap;
	private static final Logger LOGGER = Logger
			.getLogger(PlanBenefitTemplateXMLWriter.class);
	// ServiceVisitVO
	private static final String VISIT_TYPE = "visitType";
	private static final String COPAY_IN_NETWORK_TIER1 = "copayInNetworkTier1";
	private static final String COPAY_IN_NETWORK_TIER2 = "copayInNetworkTier2";
	private static final String COPAY_OUT_OF_NETWORK = "copayOutOfNetwork";
	private static final String COINSURANCE_IN_NETWORK_TIER1 = "coInsuranceInNetworkTier1";
	private static final String COINSURANCE_IN_NETWORK_TIER2 = "coInsuranceInNetworkTier2";
	private static final String COINSURANCE_OUT_OF_NETWORK = "coInsuranceOutOfNetwork";
	// MoopVO
	private static final String NAME = "name";
	private static final String IN_NETWORK_TIER1_INDIVIDUAL_AMOUNT = "inNetworkTier1IndividualAmount";
	private static final String IN_NETWORK_TIER1_FAMILY_AMOUNT = "inNetworkTier1FamilyAmount";
	private static final String IN_NETWORK_TIER2_INDIVIDUAL_AMOUNT = "inNetworkTier2IndividualAmount";
	private static final String IN_NETWORK_TIER2_FAMILY_AMOUNT = "inNetworkTier2FamilyAmount";
	private static final String OUT_OF_NETWORK_INDIVIDUAL_AMOUNT = "outOfNetworkIndividualAmount";
	private static final String OUT_OF_NETWORK_FAMILY_AMOUNT = "outOfNetworkFamilyAmount";
	private static final String COMBINED_IN_OUT_NETWORK_INDIVIDUAL_AMOUNT = "CombinedInOutNetworkIndividualAmount";
	private static final String COMBINED_IN_OUT_NETWORK_FAMILY_AMOUNT = "combinedInOutNetworkFamilyAmount";
	// PlanDeductibleVO
	private static final String DEDUCTIBLE_TYPE = "DeductibleType";
	private static final String IN_NETWORK_TIER1_INDIVIDUAL = "inNetworkTier1Individual";
	private static final String IN_NETWORK_TIER1_FAMILY = "inNetworkTier1Family";
	private static final String IN_NETWORK_TIER_TWO_INDIVIDUAL = "inNetworkTierTwoIndividual";
	private static final String IN_NETWORK_TIER_TWO_FAMILY = "inNetworkTierTwoFamily";
	private static final String OUT_OF_NETWORK_INDIVIDUAL = "outOfNetworkIndividual";
	private static final String OUT_OF_NETWORK_FAMILY = "outOfNetworkFamily";
	private static final String COMBINED_IN_OUT_NETWORK_INDIVIDUAL = "combinedInOrOutNetworkIndividual";
	private static final String COMBINED_IN_OR_OUT_NETWORK_FAMILY = "combinedInOrOutNetworkFamily";
	private static final String COMBINED_IN_OR_OUT_TIER2 = "combinedInOrOutTier2";
	// SBCVO
	private static final String HAVING_BABY_DEDUCTIBLE = "havingBabyDeductible";
	private static final String HAVING_BABY_COINSURANCE = "havingBabyCoInsurance";
	private static final String HAVING_BABY_COPAYMENT = "havingBabyCoPayment";
	private static final String HAVING_BABY_LIMIT = "havingBabyLimit";
	private static final String HAVING_DIABETES_DEDUCTIBLE = "havingDiabetesDeductible";
	private static final String HAVING_DIABETES_COPAY = "havingDiabetesCopay";
	private static final String HAVING_DIABETES_COINSURANCE = "havingDiabetesCoInsurance";
	private static final String HAVING_DIABETES_LIMIT = "havingDiabetesLimit";
	// PlanAttributeVO
	private static final String STANDARD_COMPONENT_ID = "standardComponentID";
	private static final String PLAN_MARKETING_NAME = "planMarketingName";
	private static final String HIOS_PRODUCT_ID = "hiosProductID";
	private static final String HP_ID = "hpid";
	private static final String NETWORK_ID = "networkID";
	private static final String SERVICE_AREA_ID = "serviceAreaID";
	private static final String FORMULARY_ID = "formularyID";
	private static final String IS_NEW_PLAN = "isNewPlan";
	private static final String PLAN_TYPE = "planType";
	private static final String METAL_LEVEL = "metalLevel";
	private static final String UNIQUE_PLAN_DESIGN = "uniquePlanDesign";
	private static final String QHP_OR_NON_QHP = "qhpOrNonQhp";
	private static final String INSURANCE_PLAN_PREGNANCY_NOTICE_REQ_IND = "insurancePlanPregnancyNoticeReqInd";
	private static final String IS_SPECIALIST_REFERRAL_REQUIRED = "isSpecialistReferralRequired";
	private static final String HEALTHCARE_SPECIALIST_REFERRAL_TYPE = "healthCareSpecialistReferralType";
	private static final String INSURANCE_PLAN_BENEFIT_EXCLUSION_TEXT = "insurancePlanBenefitExclusionText";
	private static final String INDIAN_PLAN_VARIATION = "indianPlanVariation";
	private static final String HSA_ELIGIBILITY = "hsaEligibility";
	private static final String EMPLOYER_HSAHRA_CONTRIBUTION_INDICATOR = "employerHSAHRAContributionIndicator";
	private static final String CHILD_ONLY_OFFERING = "childOnlyOffering";
	private static final String CHILD_ONLY_PLAN_ID = "childOnlyPlanID";
	private static final String IS_WELNESS_PROGRAM_OFFERED = "isWellnessProgramOffered";
	private static final String IS_DISEASE_MGMT_PROGRAM_OFFERED = "isDiseaseMgmtProgramsOffered";
	private static final String EHB_APPOINTMENT_FOR_PEDIATRIC_DENTAL = "ehbApportionmentForPediatricDental";
	private static final String GUARANTEE_VS_ESTIMATED_RATE = "guaranteedVsEstimatedRate";
	private static final String MAXIMUM_COINSURANCE_FOR_SPECIALTY_DRUGS = "maximumCoinsuranceForSpecialtyDrugs";
	private static final String MAX_NUM_DAYS_FOR_CHARGING_INPATIENT_COPAY = "maxNumDaysForChargingInpatientCopay";
	private static final String BEGIN_PRIMARY_CARE_COST_SHARING_AFTER_SET_NUMBER_VISITS = "BeginPrimaryCareCostSharingAfterSetNumberVisits";
	private static final String BEGIN_PRIMARY_CARE_DEDUCTIBLE_AFTER_SET_NUMBER_OF_COPAYS = "beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays";
	private static final String PLAN_EFFECTIVE_DATE = "planEffectiveDate";
	private static final String PLAN_EXPIRATION_DATE = "planExpirationDate";
	private static final String OUT_OF_COUNTRY_COVERAGE = "outOfCountryCoverage";
	private static final String OUT_OF_COUNTRY_COVERAGE_DESCRIPTION = "outOfCountryCoverageDescription";
	private static final String OUT_OF_SERVICE_AREA_COVERAGE = "outOfServiceAreaCoverage";
	private static final String OUT_OF_SERVICE_AREA_COVERAGE_DESCRIPTION = "outOfServiceAreaCoverageDescription";
	private static final String NATIONAL_NETWORK = "nationalNetwork";
	private static final String SUMMARY_BENEFIT_AND_COVERAGE_URL = "summaryBenefitAndCoverageURL";
	private static final String ENROLLMENT_PAYMENT_URL = "enrollmentPaymentURL";
	private static final String PLAN_BROCHURE = "planBrochure";
	private static final String EMP_CONTRIBUTION_AMOUNT_FOR_HSA_OR_HRA = "empContributionAmountForHSAOrHRA";
	// BenefitAttribute
	private static final String BENEFIT_TYPE_CODE = "benefitTypeCode";
	private static final String IS_EHB = "isEHB";
	private static final String IS_STATE_MANDATE = "isStateMandate";
	private static final String IS_BENEFIT_COVERED = "isBenefitCovered";
	private static final String SERVICE_LIMIT = "serviceLimit";
	private static final String QUANTITY_LIMIT = "quantityLimit";
	private static final String UNIT_LIMIT = "unitLimit";
	private static final String MINIMUM_STAY = "minimumStay";
	private static final String EXCLUSION = "exclusion";
	private static final String EXPLANATION = "explanation";
	private static final String EHB_VARIANCE_REASON = "ehbVarianceReason";
	private static final String SUBJECT_TO_DEDUCTIBLE_TIER1 = "subjectToDeductibleTier1";
	private static final String SUBJECT_TO_DEDUCTIBLE_TIER2 = "subjectToDeductibleTier2";
	private static final String EXCLUDED_IN_NETWORK_MOOP = "excludedInNetworkMOOP";
	private static final String EXCLUDED_OUT_OF_NETWORK_MOOP = "excludedOutOfNetworkMOOP";
	// CostShareVarianceVO
	private static final String PLAN_ID = "planId";
	private static final String CSR_VARIATION_TYPE = "csrVariationType";
	private static final String ISSUER_ACTUARIAL_VALUE = "issuerActuarialValue";
	private static final String AV_CALCULATOR_OUTPUT_NUMBER = "avCalculatorOutputNumber";
	private static final String MEDICAL_AND_DRUG_DEDUCTIBLES_INTEGRATED = "medicalAndDrugDeductiblesIntegrated";
	private static final String MEDICAL_AND_DRUG_MAX_OUT_OF_PACKET_INTEGRATED = "medicalAndDrugMaxOutOfPocketIntegrated";
	private static final String MULTIPLE_PROVIDER_TIERS = "multipleProviderTiers";
	private static final String FIRST_TIER_UTILIZATION = "firstTierUtilization";
	private static final String SECOND_TIER_UTILIZATION = "secondTierUtilization";
	private static final String DEFAULT_COPAY_IN_NETWORK = "defaultCopayInNetwork";
	private static final String DEFAULT_COPAY_OUT_OF_NETWORK = "defaultCopayOutOfNetwork";
	private static final String DEFAULT_COINSURANCE_IN_NETWORK = "defaultCoInsuranceInNetwork";
	private static final String DEFAULT_COINSURANCE_OUT_OF_NETWORK = "defaultCoInsuranceOutOfNetwork";
	private static final String STATE_POSTAL_NAME = "statePostalName";

	/**
	 * 
	 */
	public PlanBenefitTemplateXMLWriter() {

	}

	/**
	 * 
	 * @param applicationId
	 * @param lastModifiedBy
	 * @param lastModifiedDate
	 */
	public PlanBenefitTemplateXMLWriter(String applicationId,
			String lastModifiedBy, XMLGregorianCalendar lastModifiedDate) {
		LOGGER.info("PlanBenefitTemplateXMLWriter() : applicationId : "
				+ applicationId);
		planBenefitTemplate = new PlanBenefitTemplateVO();
		planBenefitTemplate.setApplicationId(applicationId);
		planBenefitTemplate.setLastModifiedBy(lastModifiedBy);
		planBenefitTemplate.setLastModifiedDate(lastModifiedDate);
		planBenefitTemplate.setPackagesList(new PackageListVO());
		planVariantMap = new HashMap<String, CostShareVarianceVO>();
		plansMap = new HashMap<String, PlanAndBenefitsVO>();
	}

	/**
	 * 
	 * @param issuerId
	 * @param statePostalCode
	 * @param statePostalName
	 * @param marketCoverage
	 * @param dentalPlanOnlyInd
	 * @param applicationId
	 * @param tin
	 * @return
	 */
	public PlanAndBenefitsPackageVO createPackage(String issuerId,
			String statePostalCode, String statePostalName,
			String marketCoverage, String dentalPlanOnlyInd,
			String applicationId, String tin) {
		LOGGER.info("createPackage() : issuerId : " + issuerId
				+ " : statePostalCode : " + statePostalCode
				+ " : statePostalName : " + statePostalName);
		PlanAndBenefitsPackageVO planAndBenefitsPackage = new PlanAndBenefitsPackageVO();
		HeaderVO header = new HeaderVO();
		header.setApplicationId(applicationId);
		header.setDentalPlanOnlyInd(createExcelCell("dentalPlanOnlyInd",
				dentalPlanOnlyInd, "", "", ""));
		header.setIssuerId(createExcelCell("issuerId", issuerId, "", "", ""));
		header.setStatePostalCode(createExcelCell("statePostalCode",
				statePostalCode, "", "", ""));
		header.setMarketCoverage(createExcelCell("marketCoverage",
				marketCoverage, "", "", ""));
		header.setStatePostalName(createExcelCell(STATE_POSTAL_NAME,
				statePostalName, "", "", ""));
		header.setTin(createExcelCell("tin", tin, "", "", ""));
		planAndBenefitsPackage.setHeader(header);
		planAndBenefitsPackage.setBenefitsList(new BenefitsListVO());
		planAndBenefitsPackage.setPlansList(new PlansListVO());
		planBenefitTemplate.getPackagesList().getPackages()
				.add(planAndBenefitsPackage);
		return planAndBenefitsPackage;
	}

	/**
	 * 
	 * @return
	 * @throws GIException 
	 */
	public String generatePlanAndBenefitTemplateXML() throws GIException {
		return new SerffUtils().objectToXmlString(planBenefitTemplate);
	}

	/**
	 * 
	 * @param cellName
	 * @param cellValue
	 * @param rowNumber
	 * @param colNumber
	 * @param cellLocation
	 * @return
	 */
	public ExcelCellVO createExcelCell(String cellName, String cellValue,
			String rowNumber, String colNumber, String cellLocation) {

		LOGGER.debug("Creating excel cell : cellValue : " + cellValue);
		String value = cellValue;
		ExcelCellVO excelCell = new ExcelCellVO();
		if (null == value) {
			value = "";
		}
		// excelCell.setCellLocation(cellLocation);
		// excelCell.setCellName(cellName);
		excelCell.setCellValue(value);

		return excelCell;
	}

	/**
	 * 
	 * @param packageObj
	 * @param issuerId
	 * @param statePostalCode
	 * @param statePostalName
	 * @param marketCoverage
	 * @param dentalPlanOnlyInd
	 * @param applicationId
	 * @param tin
	 */
/*	public void setHeaderToPackage(PlanAndBenefitsPackageVO packageObj,
			String issuerId, String statePostalCode, String statePostalName,
			String marketCoverage, String dentalPlanOnlyInd,
			String applicationId, String tin) {

		LOGGER.info("setHeaderToPackage() : issuerId : " + issuerId
				+ " : statePostalCode : " + statePostalCode
				+ " : statePostalName : " + statePostalName);
		HeaderVO header = packageObj.getHeader();
		header.setApplicationId(applicationId);
		header.setDentalPlanOnlyInd(createExcelCell("statePostalCode",
				statePostalCode, "", "", ""));
		header.setIssuerId(createExcelCell("issuerId", issuerId, "", "", ""));
		header.setMarketCoverage(createExcelCell(STATE_POSTAL_NAME,
				statePostalName, "", "", ""));
		header.setStatePostalCode(createExcelCell(STATE_POSTAL_NAME,
				statePostalName, "", "", ""));
		header.setStatePostalName(createExcelCell(STATE_POSTAL_NAME,
				statePostalName, "", "", ""));
		header.setTin(createExcelCell(STATE_POSTAL_NAME, statePostalName, "",
				"", ""));
	}*/

	/**
	 * 
	 * @param packageObj
	 * @param benefitVO
	 * @return
	 */
	public boolean addBenefitToPackage(PlanAndBenefitsPackageVO packageObj,
			BenefitVO benefitVO) {

		if (null != benefitVO && null != packageObj) {

			LOGGER.info("addBenefitToPackage() : BenefitTypeCode : "
					+ benefitVO.getBenefitTypeCode() + " : IsBenefitCovered : "
					+ benefitVO.getIsBenefitCovered());

			BenefitAttributeVO benefitAttribute = new BenefitAttributeVO();

			benefitAttribute.setBenefitTypeCode(createExcelCell(
					BENEFIT_TYPE_CODE, benefitVO.getBenefitTypeCode(), "", "",
					""));
			benefitAttribute.setIsEHB(createExcelCell(IS_EHB,
					benefitVO.getIsEHB(), "", "", ""));
			benefitAttribute
					.setIsStateMandate(createExcelCell(IS_STATE_MANDATE,
							benefitVO.getIsStateMandate(), "", "", ""));
			benefitAttribute.setIsBenefitCovered(createExcelCell(
					IS_BENEFIT_COVERED, benefitVO.getIsBenefitCovered(), "",
					"", ""));
			benefitAttribute.setServiceLimit(createExcelCell(SERVICE_LIMIT,
					benefitVO.getServiceLimit(), "", "", ""));
			benefitAttribute.setQuantityLimit(createExcelCell(QUANTITY_LIMIT,
					benefitVO.getQuantityLimit(), "", "", ""));
			benefitAttribute.setUnitLimit(createExcelCell(UNIT_LIMIT,
					benefitVO.getUnitLimit(), "", "", ""));
			benefitAttribute.setMinimumStay(createExcelCell(MINIMUM_STAY,
					benefitVO.getMinimumStay(), "", "", ""));
			benefitAttribute.setExclusion(createExcelCell(EXCLUSION,
					benefitVO.getExclusion(), "", "", ""));
			benefitAttribute.setExplanation(createExcelCell(EXPLANATION,
					benefitVO.getExplanation(), "", "", ""));
			benefitAttribute.setEhbVarianceReason(createExcelCell(
					EHB_VARIANCE_REASON, benefitVO.getEhbVarianceReason(), "",
					"", ""));
			benefitAttribute.setSubjectToDeductibleTier1(createExcelCell(
					SUBJECT_TO_DEDUCTIBLE_TIER1,
					benefitVO.getSubjectToDeductibleTier1(), "", "", ""));
			benefitAttribute.setSubjectToDeductibleTier2(createExcelCell(
					SUBJECT_TO_DEDUCTIBLE_TIER2,
					benefitVO.getSubjectToDeductibleTier2(), "", "", ""));
			benefitAttribute.setExcludedInNetworkMOOP(createExcelCell(
					EXCLUDED_IN_NETWORK_MOOP,
					benefitVO.getExcludedInNetworkMOOP(), "", "", ""));
			benefitAttribute.setExcludedOutOfNetworkMOOP(createExcelCell(
					EXCLUDED_OUT_OF_NETWORK_MOOP,
					benefitVO.getExcludedOutOfNetworkMOOP(), "", "", ""));

			if (null != packageObj.getBenefitsList()) {
				packageObj.getBenefitsList().getBenefits()
						.add(benefitAttribute);
				return true;
			} else {
				LOGGER.error("addBenefitToPackage() : null object passed in packageObj.getBenefitsList().");
				return false;
			}
		} else {
			LOGGER.error("addBenefitToPackage() : null object passed : "
					+ ((null == benefitVO) ? "benefitDetails" : "packageObj"));
			return false;
		}

	}

	/**
	 * 
	 * @param packageObj
	 * @param planAttributes
	 * @return
	 */
	public boolean addPlanToPackage(PlanAndBenefitsPackageVO packageObj,
			com.getinsured.serffadapter.vo.PlanAttributeVO planAttributes) {

		if (null != planAttributes) {
			LOGGER.info("addPlanToPackage() : planAttributes : "
					+ planAttributes);
			PlanAndBenefitsVO planObject = new PlanAndBenefitsVO();
			planObject.setBenefitAttributesList(new BenefitAttributesListVO());
			planObject
					.setCostShareVariancesList(new CostShareVariancesListVO());
			planObject.setPlanAttributes(new PlanAttributeVO());
			packageObj.getPlansList().getPlans().add(planObject);
			setPlanAttribute(planObject, planAttributes);
			plansMap.put(planAttributes.getStandardComponentID(), planObject);
			return true;
		} else {
			LOGGER.error("addPlanToPackage() : planAttributes is null.");
			return false;
		}

	}

	/**
	 * 
	 * @param planObj
	 * @param planAttributes
	 * @return
	 */
	public boolean setPlanAttribute(PlanAndBenefitsVO planObj,
			com.getinsured.serffadapter.vo.PlanAttributeVO planAttributes) {

		if (null != planObj && null != planAttributes) {

			LOGGER.info("setPlanAttribute() : planAttributes  : "
					+ planAttributes);

			PlanAttributeVO planAttribute = planObj.getPlanAttributes();

			if (null != planAttribute) {

				planAttribute.setStandardComponentID((createExcelCell(
						STANDARD_COMPONENT_ID,
						planAttributes.getStandardComponentID(), "", "", "")));
				planAttribute.setPlanMarketingName((createExcelCell(
						PLAN_MARKETING_NAME,
						planAttributes.getPlanMarketingName(), "", "", "")));
				planAttribute.setHiosProductID((createExcelCell(
						HIOS_PRODUCT_ID, planAttributes.getHiosProductID(), "",
						"", "")));
				planAttribute.setHpid((createExcelCell(HP_ID,
						planAttributes.getHpid(), "", "", "")));
				planAttribute.setNetworkID((createExcelCell(NETWORK_ID,
						planAttributes.getNetworkID(), "", "", "")));
				planAttribute.setServiceAreaID((createExcelCell(
						SERVICE_AREA_ID,
						planAttributes.getServiceAreaID(), "", "", "")));
				planAttribute.setFormularyID((createExcelCell(
						FORMULARY_ID,
						planAttributes.getFormularyID(), "", "", "")));
				planAttribute.setIsNewPlan((createExcelCell(IS_NEW_PLAN,
						planAttributes.getIsNewPlan(), "", "", "")));
				planAttribute.setPlanType((createExcelCell(PLAN_TYPE,
						planAttributes.getPlanType(), "", "", "")));
				planAttribute.setMetalLevel((createExcelCell(METAL_LEVEL,
						planAttributes.getMetalLevel(), "", "", "")));
				planAttribute.setUniquePlanDesign((createExcelCell(
						UNIQUE_PLAN_DESIGN,
						planAttributes.getUniquePlanDesign(), "", "", "")));
				planAttribute.setQhpOrNonQhp((createExcelCell(QHP_OR_NON_QHP,
						planAttributes.getQhpOrNonQhp(), "", "", "")));
				planAttribute
						.setInsurancePlanPregnancyNoticeReqInd((createExcelCell(
								INSURANCE_PLAN_PREGNANCY_NOTICE_REQ_IND,
								planAttributes
										.getInsurancePlanPregnancyNoticeReqInd(),
								"", "", "")));
				planAttribute.setIsSpecialistReferralRequired((createExcelCell(
						IS_SPECIALIST_REFERRAL_REQUIRED,
						planAttributes.getIsSpecialistReferralRequired(), "",
						"", "")));
				planAttribute
						.setHealthCareSpecialistReferralType((createExcelCell(
								HEALTHCARE_SPECIALIST_REFERRAL_TYPE,
								planAttributes
										.getHealthCareSpecialistReferralType(),
								"", "", "")));
				planAttribute
						.setInsurancePlanBenefitExclusionText((createExcelCell(
								INSURANCE_PLAN_BENEFIT_EXCLUSION_TEXT,
								planAttributes
										.getInsurancePlanBenefitExclusionText(),
								"", "", "")));
				planAttribute.setIndianPlanVariation((createExcelCell(
						INDIAN_PLAN_VARIATION,
						planAttributes.getIndianPlanVariation(), "", "", "")));
				planAttribute.setHsaEligibility((createExcelCell(
						HSA_ELIGIBILITY, planAttributes.getHsaEligibility(),
						"", "", "")));
				planAttribute
						.setEmployerHSAHRAContributionIndicator((createExcelCell(
								EMPLOYER_HSAHRA_CONTRIBUTION_INDICATOR,
								planAttributes
										.getEmployerHSAHRAContributionIndicator(),
								"", "", "")));
				planAttribute
						.setEmpContributionAmountForHSAOrHRA((createExcelCell(
								EMP_CONTRIBUTION_AMOUNT_FOR_HSA_OR_HRA,
								planAttributes
										.getEmpContributionAmountForHSAOrHRA(),
								"", "", "")));
				planAttribute.setChildOnlyOffering((createExcelCell(
						CHILD_ONLY_OFFERING,
						planAttributes.getChildOnlyOffering(), "", "", "")));
				planAttribute.setChildOnlyPlanID((createExcelCell(
						CHILD_ONLY_PLAN_ID,
						planAttributes.getChildOnlyPlanID(), "", "", "")));
				planAttribute.setIsWellnessProgramOffered((createExcelCell(
						IS_WELNESS_PROGRAM_OFFERED,
						planAttributes.getIsWellnessProgramOffered(), "", "",
						"")));
				planAttribute.setIsDiseaseMgmtProgramsOffered((createExcelCell(
						IS_DISEASE_MGMT_PROGRAM_OFFERED,
						planAttributes.getIsDiseaseMgmtProgramsOffered(), "",
						"", "")));
				planAttribute
						.setEhbApportionmentForPediatricDental((createExcelCell(
								EHB_APPOINTMENT_FOR_PEDIATRIC_DENTAL,
								planAttributes
										.getEhbApportionmentForPediatricDental(),
								"", "", "")));
				planAttribute.setGuaranteedVsEstimatedRate((createExcelCell(
						GUARANTEE_VS_ESTIMATED_RATE,
						planAttributes.getGuaranteedVsEstimatedRate(), "", "",
						"")));
				planAttribute
						.setMaximumCoinsuranceForSpecialtyDrugs((createExcelCell(
								MAXIMUM_COINSURANCE_FOR_SPECIALTY_DRUGS,
								planAttributes
										.getMaximumCoinsuranceForSpecialtyDrugs(),
								"", "", "")));
				planAttribute
						.setMaxNumDaysForChargingInpatientCopay((createExcelCell(
								MAX_NUM_DAYS_FOR_CHARGING_INPATIENT_COPAY,
								planAttributes
										.getMaxNumDaysForChargingInpatientCopay(),
								"", "", "")));
				planAttribute
						.setBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays((createExcelCell(
								BEGIN_PRIMARY_CARE_DEDUCTIBLE_AFTER_SET_NUMBER_OF_COPAYS,
								planAttributes
										.getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays(),
								"", "", "")));
				planAttribute
						.setBeginPrimaryCareCostSharingAfterSetNumberVisits((createExcelCell(
								BEGIN_PRIMARY_CARE_COST_SHARING_AFTER_SET_NUMBER_VISITS,
								planAttributes
										.getBeginPrimaryCareCostSharingAfterSetNumberVisits(),
								"", "", "")));
				planAttribute.setPlanEffectiveDate((createExcelCell(
						PLAN_EFFECTIVE_DATE,
						planAttributes.getPlanEffectiveDate(), "", "", "")));
				planAttribute.setPlanExpirationDate((createExcelCell(
						PLAN_EXPIRATION_DATE,
						planAttributes.getPlanExpirationDate(), "", "", "")));
				planAttribute.setOutOfCountryCoverage((createExcelCell(
						OUT_OF_COUNTRY_COVERAGE,
						planAttributes.getOutOfCountryCoverage(), "", "", "")));
				planAttribute
						.setOutOfCountryCoverageDescription((createExcelCell(
								OUT_OF_COUNTRY_COVERAGE_DESCRIPTION,
								planAttributes
										.getOutOfCountryCoverageDescription(),
								"", "", "")));
				planAttribute.setOutOfServiceAreaCoverage((createExcelCell(
						OUT_OF_SERVICE_AREA_COVERAGE,
						planAttributes.getOutOfServiceAreaCoverage(), "", "",
						"")));
				planAttribute
						.setOutOfServiceAreaCoverageDescription((createExcelCell(
								OUT_OF_COUNTRY_COVERAGE_DESCRIPTION,
								planAttributes
										.getOutOfServiceAreaCoverageDescription(),
								"", "", "")));
				planAttribute.setNationalNetwork((createExcelCell(
						NATIONAL_NETWORK, planAttributes.getNationalNetwork(),
						"", "", "")));
				planAttribute.setSummaryBenefitAndCoverageURL((createExcelCell(
						SUMMARY_BENEFIT_AND_COVERAGE_URL,
						planAttributes.getSummaryBenefitAndCoverageURL(), "",
						"", "")));
				planAttribute.setEnrollmentPaymentURL((createExcelCell(
						ENROLLMENT_PAYMENT_URL,
						planAttributes.getEnrollmentPaymentURL(), "", "", "")));
				planAttribute.setPlanBrochure((createExcelCell(PLAN_BROCHURE,
						planAttributes.getPlanBrochure(), "", "", "")));
				planAttribute
						.setOutOfServiceAreaCoverageDescription((createExcelCell(
								OUT_OF_SERVICE_AREA_COVERAGE_DESCRIPTION,
								planAttributes
										.getOutOfServiceAreaCoverageDescription(),
								"", "", "")));
			} else {
				LOGGER.error("addPlanToPackage() : planObj.getPlanAttributes() is null.");
				return false;
			}

			return true;
		} else {
			LOGGER.error("addPlanToPackage() : null value passed : "
					+ ((null == planObj) ? "planObj" : "planAttributes"));
			return false;
		}

	}

	/**
	 * 
	 * @param planStdComponentID
	 * @param benefit
	 * @return
	 */
	public boolean addBenefitToPlan(String planStdComponentID, BenefitVO benefit) {
		if (null != planStdComponentID
				&& planStdComponentID.length() == SerffConstants.LEN_HIOS_PLAN_ID) {
			LOGGER.info("addBenefitToPlan() : planStdComponentID : "
					+ planStdComponentID);
			PlanAndBenefitsVO planObj = plansMap.get(planStdComponentID);
			if (null != planObj) {
				if (null != planObj.getBenefitAttributesList()) {
					LOGGER.info("addBenefitToPlan() : planAttributes size : "
							+ planObj.getBenefitAttributesList()
									.getBenefitAttributes().size());
					BenefitAttributeVO benefitAttributeVO = new BenefitAttributeVO();

					benefitAttributeVO.setBenefitTypeCode(createExcelCell(
							BENEFIT_TYPE_CODE, benefit.getBenefitTypeCode(),
							"", "", ""));
					benefitAttributeVO.setIsEHB(createExcelCell(IS_EHB,
							benefit.getIsEHB(), "", "", ""));
					benefitAttributeVO.setIsStateMandate(createExcelCell(
							IS_STATE_MANDATE, benefit.getIsStateMandate(), "",
							"", ""));
					benefitAttributeVO.setIsBenefitCovered(createExcelCell(
							IS_BENEFIT_COVERED, benefit.getIsBenefitCovered(),
							"", "", ""));
					benefitAttributeVO.setServiceLimit(createExcelCell(
							SERVICE_LIMIT, benefit.getServiceLimit(), "", "",
							""));
					benefitAttributeVO.setQuantityLimit(createExcelCell(
							QUANTITY_LIMIT, benefit.getQuantityLimit(), "", "",
							""));
					benefitAttributeVO.setUnitLimit(createExcelCell(UNIT_LIMIT,
							benefit.getUnitLimit(), "", "", ""));
					benefitAttributeVO
							.setMinimumStay(createExcelCell(MINIMUM_STAY,
									benefit.getMinimumStay(), "", "", ""));
					benefitAttributeVO.setExclusion(createExcelCell(EXCLUSION,
							benefit.getExclusion(), "", "", ""));
					benefitAttributeVO.setExplanation(createExcelCell(
							EXPLANATION, benefit.getExplanation(), "", "", ""));
					benefitAttributeVO.setEhbVarianceReason(createExcelCell(
							EHB_VARIANCE_REASON,
							benefit.getEhbVarianceReason(), "", "", ""));
					benefitAttributeVO
							.setSubjectToDeductibleTier1(createExcelCell(
									SUBJECT_TO_DEDUCTIBLE_TIER1,
									benefit.getSubjectToDeductibleTier1(), "",
									"", ""));
					benefitAttributeVO
							.setSubjectToDeductibleTier2(createExcelCell(
									SUBJECT_TO_DEDUCTIBLE_TIER2,
									benefit.getSubjectToDeductibleTier2(), "",
									"", ""));
					benefitAttributeVO
							.setExcludedInNetworkMOOP(createExcelCell(
									EXCLUDED_IN_NETWORK_MOOP,
									benefit.getExcludedInNetworkMOOP(), "", "",
									""));
					benefitAttributeVO
							.setExcludedOutOfNetworkMOOP(createExcelCell(
									EXCLUDED_OUT_OF_NETWORK_MOOP,
									benefit.getExcludedOutOfNetworkMOOP(), "",
									"", ""));
					planObj.getBenefitAttributesList().getBenefitAttributes()
							.add(benefitAttributeVO);
					return true;
				} else {
					LOGGER.error("addBenefitToPlan() : planObj.getBenefitAttributesList() is null.");
					return false;
				}
			}
		} else {
			LOGGER.error("addBenefitToPlan() : planObj is null.");
			return false;
		}

		return false;
	}

	/**
	 * 
	 * @param costShareVarianceVO
	 * @return
	 */
	public boolean setCostShareVariance(
			com.getinsured.serffadapter.vo.CostShareVarianceVO costShareVarianceVO) {

		PlanAndBenefitsVO planObj = null;
		String planVariantID = null;
		if (null != costShareVarianceVO) {
			planVariantID = costShareVarianceVO.getPlanId();
			LOGGER.info("setCostShareVariance() : planVariantID : "
					+ planVariantID);
		} else {
			LOGGER.error("setCostShareVariance() : planVariantID is null.");
		}

		if (null != planVariantID) {
			String planStdComponentID = planVariantID.split("-")[0];
			if (null != planStdComponentID
					&& planStdComponentID.length() == SerffConstants.LEN_HIOS_PLAN_ID) {
				planObj = plansMap.get(planStdComponentID);
			}
		}

		if (null != planObj) {
			CostShareVarianceVO costShareVariance = planVariantMap
					.get(planVariantID);
			if (null == costShareVariance) {
				costShareVariance = new CostShareVarianceVO();
				costShareVariance.setSbc(new SBCVO());
				setSBCToCSV(costShareVariance.getSbc(),
						new com.getinsured.serffadapter.vo.SBCVO());
				costShareVariance.setMoopList(new MoopListVO());
				costShareVariance
						.setPlanDeductibleList(new PlanDeductibleListVO());
				costShareVariance.setServiceVisitList(new ServiceVisitListVO());
				planObj.getCostShareVariancesList().getCostShareVariance()
						.add(costShareVariance);
			}
			// Set attributes.
			costShareVariance.setPlanId(createExcelCell(PLAN_ID, planVariantID,
					"", "", ""));
			costShareVariance.setPlanMarketingName(createExcelCell(
					PLAN_MARKETING_NAME,
					costShareVarianceVO.getPlanMarketingName(), "", "", ""));
			costShareVariance.setMetalLevel(createExcelCell(METAL_LEVEL,
					costShareVarianceVO.getMetalLevel(), "", "", ""));
			costShareVariance.setCsrVariationType(createExcelCell(
					CSR_VARIATION_TYPE,
					costShareVarianceVO.getCsrVariationType(), "", "", ""));
			costShareVariance.setIssuerActuarialValue(createExcelCell(
					ISSUER_ACTUARIAL_VALUE,
					costShareVarianceVO.getIssuerActuarialValue(), "", "", ""));
			costShareVariance.setAvCalculatorOutputNumber(createExcelCell(
					AV_CALCULATOR_OUTPUT_NUMBER,
					costShareVarianceVO.getAvCalculatorOutputNumber(), "", "",
					""));
			costShareVariance
					.setMedicalAndDrugDeductiblesIntegrated(createExcelCell(
							MEDICAL_AND_DRUG_DEDUCTIBLES_INTEGRATED,
							costShareVarianceVO
									.getMedicalAndDrugDeductiblesIntegrated(),
							"", "", ""));
			costShareVariance
					.setMedicalAndDrugMaxOutOfPocketIntegrated(createExcelCell(
							MEDICAL_AND_DRUG_MAX_OUT_OF_PACKET_INTEGRATED,
							costShareVarianceVO
									.getMedicalAndDrugMaxOutOfPocketIntegrated(),
							"", "", ""));
			costShareVariance
					.setMultipleProviderTiers(createExcelCell(
							MULTIPLE_PROVIDER_TIERS,
							costShareVarianceVO.getMultipleProviderTiers(), "",
							"", ""));
			costShareVariance.setFirstTierUtilization(createExcelCell(
					FIRST_TIER_UTILIZATION,
					costShareVarianceVO.getFirstTierUtilization(), "", "", ""));
			costShareVariance
					.setSecondTierUtilization(createExcelCell(
							SECOND_TIER_UTILIZATION,
							costShareVarianceVO.getSecondTierUtilization(), "",
							"", ""));
			costShareVariance
					.setDefaultCopayInNetwork(createExcelCell(
							DEFAULT_COPAY_IN_NETWORK,
							costShareVarianceVO.getDefaultCopayInNetwork(), "",
							"", ""));
			costShareVariance.setDefaultCopayOutOfNetwork(createExcelCell(
					DEFAULT_COPAY_OUT_OF_NETWORK,
					costShareVarianceVO.getDefaultCoInsuranceOutOfNetwork(),
					"", "", ""));
			costShareVariance.setDefaultCoInsuranceInNetwork(createExcelCell(
					DEFAULT_COINSURANCE_IN_NETWORK,
					costShareVarianceVO.getDefaultCoInsuranceInNetwork(), "",
					"", ""));
			costShareVariance
					.setDefaultCoInsuranceOutOfNetwork(createExcelCell(
							DEFAULT_COINSURANCE_OUT_OF_NETWORK,
							costShareVarianceVO
									.getDefaultCoInsuranceOutOfNetwork(), "",
							"", ""));
			planVariantMap.put(planVariantID, costShareVariance);
			return true;
		} else {
			LOGGER.error("setCostShareVariance() : planObj is null.");
		}
		return false;
	}

	/**
	 * 
	 * @param planVariantID
	 * @param moopVO
	 * @return
	 */
	public boolean addMoopToCostShareVariance(String planVariantID,
			DeductibleVO moopVO) {
		CostShareVarianceVO costShareVarianceVO = null;

		LOGGER.info("addMoopToCostShareVariance() : planVariantID : "
				+ planVariantID);

		if (null != planVariantID) {
			costShareVarianceVO = planVariantMap.get(planVariantID);
		}
		if (null != costShareVarianceVO) {
			MoopVO moop = new MoopVO();
			moop.setCombinedInOutNetworkFamilyAmount(createExcelCell(
					COMBINED_IN_OUT_NETWORK_FAMILY_AMOUNT,
					moopVO.getCombinedInOutNetworkFamily(), "", "", ""));
			moop.setCombinedInOutNetworkIndividualAmount(createExcelCell(
					COMBINED_IN_OUT_NETWORK_INDIVIDUAL_AMOUNT,
					moopVO.getCombinedInOutNetworkIndividual(), "", "", ""));
			moop.setInNetworkTier1FamilyAmount(createExcelCell(
					IN_NETWORK_TIER1_FAMILY_AMOUNT,
					moopVO.getInNetworkTier1Family(), "", "", ""));
			moop.setInNetworkTier1IndividualAmount(createExcelCell(
					IN_NETWORK_TIER1_INDIVIDUAL_AMOUNT,
					moopVO.getInNetworkTier1Individual(), "", "", ""));
			moop.setInNetworkTier2FamilyAmount(createExcelCell(
					IN_NETWORK_TIER2_FAMILY_AMOUNT,
					moopVO.getInNetworkTier2Individual(), "", "", ""));
			moop.setInNetworkTier2IndividualAmount(createExcelCell(
					IN_NETWORK_TIER2_INDIVIDUAL_AMOUNT,
					moopVO.getInNetworkTier2Family(), "", "", ""));
			moop.setName(createExcelCell(NAME, moopVO.getName(), "", "", ""));
			moop.setOutOfNetworkFamilyAmount(createExcelCell(
					OUT_OF_NETWORK_FAMILY_AMOUNT,
					moopVO.getOutOfNetworkFamily(), "", "", ""));
			moop.setOutOfNetworkIndividualAmount(createExcelCell(
					OUT_OF_NETWORK_INDIVIDUAL_AMOUNT,
					moopVO.getOutOfNetworkIndividual(), "", "", ""));

			if (null != costShareVarianceVO.getMoopList()) {
				costShareVarianceVO.getMoopList().getMoop().add(moop);
				return true;
			} else {
				LOGGER.error("addMoopToCostShareVariance() : costShareVarianceVO.getMoopList() is null.");
				return false;
			}
		} else {
			LOGGER.error("addMoopToCostShareVariance() : costShareVarianceVO is null.");
		}
		return false;
	}

	/**
	 * 
	 * @param planVariantID
	 * @param deductibleVO
	 * @return
	 */
	public boolean addPlanDeductibleToCostShareVariance(String planVariantID,
			DeductibleVO deductibleVO) {
		CostShareVarianceVO costShareVariance = null;

		LOGGER.info("addPlanDeductibleToCostShareVariance() : planVariantID : "
				+ planVariantID);

		if (null != planVariantID) {
			costShareVariance = planVariantMap.get(planVariantID);
		}
		if (null != costShareVariance) {
			PlanDeductibleVO planDeductible = new PlanDeductibleVO();

			planDeductible.setCoinsuranceInNetworkTier1(createExcelCell(
					COINSURANCE_IN_NETWORK_TIER1,
					deductibleVO.getCoinsuranceInNetworkTier1(), "", "", ""));
			planDeductible.setCoinsuranceInNetworkTier2(createExcelCell(
					COINSURANCE_IN_NETWORK_TIER2,
					deductibleVO.getCoinsuranceInNetworkTier2(), "", "", ""));
			planDeductible.setCoinsuranceOutofNetwork(createExcelCell(
					COINSURANCE_OUT_OF_NETWORK,
					deductibleVO.getCoinsuranceOutofNetwork(), "", "", ""));
			planDeductible.setCombinedInOrOutNetworkFamily(createExcelCell(
					COMBINED_IN_OR_OUT_NETWORK_FAMILY,
					deductibleVO.getCombinedInOutNetworkFamily(), "", "", ""));
			planDeductible.setCombinedInOrOutNetworkIndividual(createExcelCell(
					COMBINED_IN_OUT_NETWORK_INDIVIDUAL,
					deductibleVO.getCombinedInOutNetworkIndividual(), "", "",
					""));
			planDeductible.setCombinedInOrOutTier2(createExcelCell(
					COMBINED_IN_OR_OUT_TIER2,
					deductibleVO.getCombinedInOrOutTier2(), "", "", ""));
			planDeductible.setDeductibleType(createExcelCell(DEDUCTIBLE_TYPE,
					deductibleVO.getDeductibleType(), "", "", ""));
			planDeductible.setInNetworkTier1Family(createExcelCell(
					IN_NETWORK_TIER1_FAMILY,
					deductibleVO.getInNetworkTier1Family(), "", "", ""));
			planDeductible.setInNetworkTier1Individual(createExcelCell(
					IN_NETWORK_TIER1_INDIVIDUAL,
					deductibleVO.getInNetworkTier1Individual(), "", "", ""));
			planDeductible.setInNetworkTierTwoFamily(createExcelCell(
					IN_NETWORK_TIER_TWO_FAMILY,
					deductibleVO.getInNetworkTier2Family(), "", "", ""));
			planDeductible.setInNetworkTierTwoIndividual(createExcelCell(
					IN_NETWORK_TIER_TWO_INDIVIDUAL,
					deductibleVO.getInNetworkTier2Individual(), "", "", ""));
			planDeductible.setOutOfNetworkFamily(createExcelCell(
					OUT_OF_NETWORK_FAMILY,
					deductibleVO.getOutOfNetworkFamily(), "", "", ""));
			planDeductible.setOutOfNetworkIndividual(createExcelCell(
					OUT_OF_NETWORK_INDIVIDUAL,
					deductibleVO.getOutOfNetworkIndividual(), "", "", ""));

			costShareVariance.getPlanDeductibleList().getPlanDeductible()
					.add(planDeductible);
			return true;
		} else {
			LOGGER.info("addPlanDeductibleToCostShareVariance() : costShareVariance is null.");
			return false;
		}

	}

	/**
	 * 
	 * @param planVariantID
	 * @param sbcDetails
	 * @return
	 */
	public boolean setSBCToCostShareVariance(String planVariantID,
			com.getinsured.serffadapter.vo.SBCVO sbcDetails) {
		CostShareVarianceVO costShareVariance = null;

		LOGGER.info("setSBCToCostShareVariance() : planVariantID : "
				+ planVariantID);
		if (null != planVariantID) {
			costShareVariance = planVariantMap.get(planVariantID);
		}
		if (null != costShareVariance) {
			setSBCToCSV(costShareVariance.getSbc(), sbcDetails);
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param sbc
	 * @param sbcDetails
	 */
	private void setSBCToCSV(SBCVO sbc,
			com.getinsured.serffadapter.vo.SBCVO sbcDetails) {

		if (null != sbcDetails) {
			LOGGER.info("setSBCToCSV() : HavingBabyCoInsurance : "
					+ HAVING_BABY_COINSURANCE);
			sbc.setHavingBabyCoInsurance(createExcelCell(
					HAVING_BABY_COINSURANCE,
					sbcDetails.getHavingBabyCoInsurance(), "", "", ""));
			sbc.setHavingBabyCoPayment(createExcelCell(HAVING_BABY_COPAYMENT,
					sbcDetails.getHavingBabyCoPayment(), "", "", ""));
			sbc.setHavingBabyDeductible(createExcelCell(HAVING_BABY_DEDUCTIBLE,
					sbcDetails.getHavingBabyDeductible(), "", "", ""));
			sbc.setHavingBabyLimit(createExcelCell(HAVING_BABY_LIMIT,
					sbcDetails.getHavingBabyLimit(), "", "", ""));
			sbc.setHavingDiabetesCoInsurance(createExcelCell(
					HAVING_DIABETES_COINSURANCE,
					sbcDetails.getHavingDiabetesCoInsurance(), "", "", ""));
			sbc.setHavingDiabetesCopay(createExcelCell(HAVING_DIABETES_COPAY,
					sbcDetails.getHavingDiabetesCopay(), "", "", ""));
			sbc.setHavingDiabetesDeductible(createExcelCell(
					HAVING_DIABETES_DEDUCTIBLE,
					sbcDetails.getHavingDiabetesDeductible(), "", "", ""));
			sbc.setHavingDiabetesLimit(createExcelCell(HAVING_DIABETES_LIMIT,
					sbcDetails.getHavingDiabetesLimit(), "", "", ""));
		} else {
			LOGGER.error("setSBCToCSV() : sbcDetails is null.");
		}

	}

	/**
	 * 
	 * @param planVariantID
	 * @param benefitVO
	 * @return
	 */
	public boolean addServiceVisitToCostShareVariance(String planVariantID,
			BenefitVO benefitVO) {
		CostShareVarianceVO costShareVariance = null;

		LOGGER.info("addServiceVisitToCostShareVariance() : planVariantID : "
				+ planVariantID);
		if(benefitVO.getIsBenefitCovered().equalsIgnoreCase(SerffConstants.BENEFIT_COVERED)) {
			if (null != planVariantID) {
				costShareVariance = planVariantMap.get(planVariantID);
			}
			if (null != costShareVariance) {
				ServiceVisitVO serviceVisit = new ServiceVisitVO();
	
				serviceVisit.setCoInsuranceInNetworkTier1(createExcelCell(
						COINSURANCE_IN_NETWORK_TIER1,
						benefitVO.getCoInsuranceInNetworkTier1(), "", "", ""));
				serviceVisit.setCoInsuranceInNetworkTier2(createExcelCell(
						COINSURANCE_IN_NETWORK_TIER2,
						benefitVO.getCoInsuranceInNetworkTier2(), "", "", ""));
				serviceVisit.setCoInsuranceOutOfNetwork(createExcelCell(
						COINSURANCE_OUT_OF_NETWORK,
						benefitVO.getCoInsuranceOutOfNetwork(), "", "", ""));
				serviceVisit.setCopayInNetworkTier1(createExcelCell(
						COPAY_IN_NETWORK_TIER1,
						benefitVO.getCopayInNetworkTier1(), "", "", ""));
				serviceVisit.setCopayInNetworkTier2(createExcelCell(
						COPAY_IN_NETWORK_TIER2,
						benefitVO.getCopayInNetworkTier2(), "", "", ""));
				serviceVisit.setCopayOutOfNetwork(createExcelCell(
						COPAY_OUT_OF_NETWORK, benefitVO.getCopayOutOfNetwork(), "",
						"", ""));
				serviceVisit.setVisitType(createExcelCell(VISIT_TYPE,
						benefitVO.getVisitType(), "", "", ""));
	
				costShareVariance.getServiceVisitList().getServiceVisit()
						.add(serviceVisit);
				return true;
			} else {
				LOGGER.error("addServiceVisitToCostShareVariance() : costShareVariance is null.");
			}
		}
		return false;
	}

}
