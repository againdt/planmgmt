package com.getinsured.serffadapter.xmlwriter;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.getinsured.hix.platform.util.exception.GIException;
import com.serff.template.plan.CostSharingTypeVO;
import com.serff.template.plan.CostSharingTypeVOList;
import com.serff.template.plan.DrugListVO;
import com.serff.template.plan.DrugListVOList;
import com.serff.template.plan.DrugTierLevelVO;
import com.serff.template.plan.DrugTierLevelVOList;
import com.serff.template.plan.DrugVO;
import com.serff.template.plan.DrugVOList;
import com.serff.template.plan.ExcelCellVO;
import com.serff.template.plan.FormularyCostSharingTypeVO;
import com.serff.template.plan.FormularyCostSharingTypeVOList;
import com.serff.template.plan.FormularyVO;
import com.serff.template.plan.FormularyVOList;
import com.serff.template.plan.HeaderVO;
import com.serff.template.plan.PrescriptionDrugTemplateVO;
import com.serff.util.SerffUtils;

public class PrescriptionDrugTemplateXMLWriter {

	private HeaderVO header;
	private DrugListVOList prescriptionDrugList;
	private FormularyVOList prescriptionFormularyList;
	private Map<String, FormularyVO> formulariesMap;
	private Map<String, DrugListVO> drugListMap;
	private Map<String, FormularyCostSharingTypeVO> formularyCostSharingMap;
	private static final Logger LOGGER = Logger
			.getLogger(PrescriptionDrugTemplateXMLWriter.class);

	/**
	 * 
	 */
	public PrescriptionDrugTemplateXMLWriter() {

		header = new HeaderVO();
		prescriptionDrugList = new DrugListVOList();
		prescriptionFormularyList = new FormularyVOList();
		formulariesMap = new HashMap<String, FormularyVO>();
		drugListMap = new HashMap<String, DrugListVO>();
		formularyCostSharingMap = new HashMap<String, FormularyCostSharingTypeVO>();
	}

	/**
	 * 
	 * @return
	 * @throws GIException 
	 */
	public String generatePrescriptionDrugXML() throws GIException {

		PrescriptionDrugTemplateVO prescriptionDrug = new PrescriptionDrugTemplateVO();
		prescriptionDrug.setHeader(header);
		prescriptionDrug.setFormularyList(prescriptionFormularyList);
		prescriptionDrug.setDrugList(prescriptionDrugList);

		return new SerffUtils().objectToXmlString(prescriptionDrug);
	}

	/**
	 * 
	 * @param applicationId
	 * @param dentalPlanOnly
	 * @param issuerId
	 * @param marketCoverage
	 * @param statePostalCode
	 * @param statePostalName
	 * @param tin
	 */
	public void setHeaderToPrescriptionDrug(String applicationId,
			String dentalPlanOnly, String issuerId, String marketCoverage,
			String statePostalCode, String statePostalName, String tin) {

		LOGGER.info("setHeaderToPrescriptionDrug() : applicationId : "
				+ applicationId + " : issuerId : " + issuerId);
		header.setApplicationId(applicationId);
		header.setDentalPlanOnlyInd(createExcelCell("", dentalPlanOnly, ""));
		header.setIssuerId(createExcelCell("", issuerId, ""));
		header.setMarketCoverage(createExcelCell("", marketCoverage, ""));
		header.setStatePostalCode(createExcelCell("", statePostalCode, ""));
		header.setStatePostalName(createExcelCell("", statePostalName, ""));
		header.setTin(createExcelCell("", tin, ""));
	}

	/**
	 * 
	 * @param drugListId
	 * @param drugTierLevel
	 * @param drugTierType
	 * @return
	 */
	public boolean addDrugTierToDrugList(String drugListId,
			String drugTierLevel, String drugTierType) {

		LOGGER.info("addDrugTierToDrugList() : drugListId : " + drugListId);
		DrugListVO drugList = drugListMap.get(drugListId);

		if (null != drugList) {
			DrugTierLevelVO drugTier = new DrugTierLevelVO();
			drugTier.setDrugTierLevel(createExcelCell("", drugTierLevel, ""));
			drugTier.setDrugTierType(createExcelCell("", drugTierType, ""));
			drugList.getDrugTierLevelList().getDrugTierLevelVO().add(drugTier);
			return true;
		} else {
			LOGGER.error("addDrugTierToDrugList() : drugList is null.");
			return false;
		}
	}

	/**
	 * 
	 * @param drugListId
	 */
	public void addDrugListToPrescriptionDrug(String drugListId) {
		LOGGER.info("addDrugListToPrescriptionDrug() : drugListId : "
				+ drugListId);
		DrugListVO drugList = new DrugListVO();
		drugList.setDrugListID(createExcelCell("", drugListId, ""));
		drugList.setDrugTierLevelList(new DrugTierLevelVOList());
		drugList.setDrugVOList(new DrugVOList());
		this.prescriptionDrugList.getDrugListVO().add(drugList);
		drugListMap.put(drugListId, drugList);
	}

	/**
	 * 
	 * @param formularyID
	 * @param formularyUrl
	 * @param drugListID
	 * @param numberTiers
	 */
	public void addFormulary(String formularyID, String formularyUrl,
			String drugListID, String numberTiers) {

		LOGGER.info("addFormulary() : formularyID : " + formularyID
				+ " : drugListID : " + drugListID);

		FormularyVO formulary = new FormularyVO();
		formulary.setDrugListID(createExcelCell("", drugListID, ""));
		formulary
				.setFormularyCostSharingTypeList(new FormularyCostSharingTypeVOList());
		formulary.setFormularyID(createExcelCell("", formularyID, ""));
		formulary.setFormularyUrl(createExcelCell("", formularyUrl, ""));
		formulary.setNumberTiers(createExcelCell("", numberTiers, ""));
		this.prescriptionFormularyList.getFormularyVO().add(formulary);
		formulariesMap.put(formularyID, formulary);
	}

	/**
	 * 
	 * @param formularyId
	 * @param drugTierLevel
	 * @param oneMonthOutNetworkRetailOfferedIndicator
	 * @param threeMonthInNetworkMailOfferedIndicator
	 * @param threeMonthOutNetworkMailOfferedIndicator
	 * @return
	 */
	public boolean addCostSharingToFormulary(String formularyId,
			String drugTierLevel,
			String oneMonthOutNetworkRetailOfferedIndicator,
			String threeMonthInNetworkMailOfferedIndicator,
			String threeMonthOutNetworkMailOfferedIndicator) {

		LOGGER.info("addCostSharingToFormulary() : formularyId : "
				+ formularyId + " : drugTierLevel : " + drugTierLevel);

		FormularyVO formulary = formulariesMap.get(formularyId);
		if (null != formulary) {

			FormularyCostSharingTypeVO formularyCostSharing = new FormularyCostSharingTypeVO();
			formularyCostSharing.setDrugTierLevel(createExcelCell("",
					drugTierLevel, ""));
			formularyCostSharing
					.setCostSharingTypeList(new CostSharingTypeVOList());
			formularyCostSharing
					.setOneMonthOutNetworkRetailOfferedIndicator(createExcelCell(
							"", oneMonthOutNetworkRetailOfferedIndicator, ""));
			formularyCostSharing
					.setThreeMonthInNetworkMailOfferedIndicator(createExcelCell(
							"", threeMonthInNetworkMailOfferedIndicator, ""));
			formularyCostSharing
					.setThreeMonthOutNetworkMailOfferedIndicator(createExcelCell(
							"", threeMonthOutNetworkMailOfferedIndicator, ""));
			formulary.getFormularyCostSharingTypeList()
					.getFormularyCostSharingTypeVO().add(formularyCostSharing);
			formularyCostSharingMap.put(formularyId + drugTierLevel,
					formularyCostSharing);

			return true;
		} else {
			LOGGER.error("addCostSharingToFormulary() : formulary is null");
			return false;
		}

	}

	/**
	 * 
	 * @param formularyId
	 * @param drugTierLevel
	 * @param networkCostType
	 * @param drugPrescriptionPeriodType
	 * @param costSharingType
	 * @param coPayment
	 * @param coInsurance
	 * @return
	 */
	public boolean addCostSharingToFormularyLevel(String formularyId,
			String drugTierLevel, String networkCostType,
			String drugPrescriptionPeriodType, String costSharingType,
			String coPayment, String coInsurance) {
		FormularyVO formulary = formulariesMap.get(formularyId);

		if (null != formulary) {

			FormularyCostSharingTypeVO formularyCostSharing = formularyCostSharingMap
					.get(formularyId + drugTierLevel);

			if (null != formularyCostSharing) {
				LOGGER.info("addCostSharingToFormularyLevel() : formularyId : "
						+ formularyId + " : drugTierLevel : " + drugTierLevel);
				CostSharingTypeVO costSharing = new CostSharingTypeVO();
				costSharing
						.setDrugPrescriptionPeriodType(drugPrescriptionPeriodType);
				costSharing.setNetworkCostType(networkCostType);
				costSharing
						.setCoInsurance(createExcelCell("", coInsurance, ""));
				costSharing.setCoPayment(createExcelCell("", coPayment, ""));
				costSharing.setCostSharingType(createExcelCell("",
						costSharingType, ""));
				if (null != formularyCostSharing.getCostSharingTypeList()) {
					formularyCostSharing.getCostSharingTypeList()
							.getCostSharingTypeVO().add(costSharing);
					return true;
				} else {
					LOGGER.error("addCostSharingToFormularyLevel() : formularyCostSharing.getCostSharingTypeList() is null");
					return false;
				}

			} else {
				LOGGER.error("addCostSharingToFormularyLevel() : formularyCostSharing is null");
				return false;
			}
		} else {
			LOGGER.error("addCostSharingToFormularyLevel() : formulary is null");
			return false;
		}

	}

	/**
	 * 
	 * @param drugListId
	 */
	public void createDrugList(String drugListId) {

		LOGGER.info("createDrugList() : drugListId : " + drugListId);

		DrugListVO drugList = new DrugListVO();

		drugList.setDrugListID(createExcelCell("", drugListId, ""));
		drugList.setDrugTierLevelList(new DrugTierLevelVOList());
		drugList.setDrugVOList(new DrugVOList());

		drugListMap.put(drugListId, drugList);
	}

	/**
	 * 
	 * @param drugListId
	 * @param drugTierLevel
	 * @param drugTierType
	 * @return
	 */
	public boolean addDrugTierLevelToDrugList(String drugListId,
			String drugTierLevel, String drugTierType) {

		LOGGER.info("addDrugTierLevelToDrugList() : drugListId : " + drugListId
				+ " : drugTierLevel : " + drugTierLevel);

		DrugTierLevelVO tierLevel = new DrugTierLevelVO();
		tierLevel.setDrugTierLevel(createExcelCell("", drugTierLevel, ""));
		tierLevel.setDrugTierType(createExcelCell("", drugTierType, ""));
		DrugListVO drugListVO = drugListMap.get(drugListId);
		if (null != drugListVO && null != drugListVO.getDrugTierLevelList()) {
			drugListVO.getDrugTierLevelList().getDrugTierLevelVO()
					.add(tierLevel);
			return true;
		} else {
			LOGGER.error("addDrugTierLevelToDrugList() : drugListVO is null.");
			return false;
		}
	}

	/**
	 * 
	 * @param drugListId
	 * @param rxcui
	 * @param tier
	 * @param authorizationRequired
	 * @param stepTherapyRequired
	 * @return
	 */
	public boolean addDrugToDrugList(String drugListId, String rxcui,
			String tier, String authorizationRequired,
			String stepTherapyRequired) {

		LOGGER.info("addDrugToDrugList() : drugListId : " + drugListId
				+ " : rxcui : " + rxcui + " : tier : " + tier);
		DrugVO drug = new DrugVO();
		drug.setRXCUI(createExcelCell("", rxcui, ""));
		drug.setTier(createExcelCell("", tier, ""));
		drug.setAuthorizationRequired(createExcelCell("",
				authorizationRequired, ""));
		drug.setStepTherapyRequired(createExcelCell("", stepTherapyRequired, ""));
		DrugListVO drugListVO = drugListMap.get(drugListId);

		if (null != drugListVO && null != drugListVO.getDrugVOList()) {
			drugListVO.getDrugVOList().getDrugVO().add(drug);
			return true;
		} else {
			LOGGER.info("addDrugToDrugList() : drugListVO is null.");
			return false;
		}

	}

	/**
	 * 
	 * @param cellName
	 * @param cellValue
	 * @param cellLocation
	 * @return
	 */
	public ExcelCellVO createExcelCell(String cellName, String cellValue,
			String cellLocation) {
		LOGGER.info("createExcelCell(): cellName: " + cellName + ", cellValue: " + cellValue);
		ExcelCellVO excelCell = new ExcelCellVO();

		excelCell.setCellLocation(cellLocation);
		excelCell.setCellValue(cellValue);

		return excelCell;
	}

}
