package com.getinsured.serffadapter.vo;

public class PlanRateVO {
	private String planId;
	private String rateAreaId;
	private String tobacco;
	private String ageNumber;
	private String effectiveDate;
	private String expirationDate;
	private Double primaryEnrollee;
	private Double primaryEnrolleeTobacco;
	private Double primaryEnrolleeOneDependent;
	private Double primaryEnrolleeTwoDependent;
	private Double primaryEnrolleeManyDependent;
	private Double coupleEnrollee;
	private Double coupleEnrolleeOneDependent;
	private Double coupleEnrolleeTwoDependent;
	private Double coupleEnrolleeManyDependent;
	private String isIssuerData;
	private boolean isFamilyOption;

	/**
	 * @return the planId
	 */
	public String getPlanId() {
		return planId;
	}

	/**
	 * @param planId
	 *            the planId to set
	 */
	public void setPlanId(String planId) {
		this.planId = planId;
	}

	/**
	 * @return the rateAreaId
	 */
	public String getRateAreaId() {
		return rateAreaId;
	}

	/**
	 * @param rateAreaId
	 *            the rateAreaId to set
	 */
	public void setRateAreaId(String rateAreaId) {
		this.rateAreaId = rateAreaId;
	}

	/**
	 * @return the tobacco
	 */
	public String getTobacco() {
		return tobacco;
	}

	/**
	 * @param tobacco
	 *            the tobacco to set
	 */
	public void setTobacco(String tobacco) {
		this.tobacco = tobacco;
	}

	/**
	 * @return the ageNumber
	 */
	public String getAgeNumber() {
		return ageNumber;
	}

	/**
	 * @param ageNumber
	 *            the ageNumber to set
	 */
	public void setAgeNumber(String ageNumber) {
		this.ageNumber = ageNumber;
	}

	/**
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * @return the expirationDate
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * @param expirationDate
	 *            the expirationDate to set
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * @return the primaryEnrollee
	 */
	public Double getPrimaryEnrollee() {
		return primaryEnrollee;
	}

	/**
	 * @param primaryEnrollee
	 *            the primaryEnrollee to set
	 */
	public void setPrimaryEnrollee(Double primaryEnrollee) {
		this.primaryEnrollee = primaryEnrollee;
	}

	/**
	 * @return the primaryEnrolleeTobacco
	 */
	public Double getPrimaryEnrolleeTobacco() {
		return primaryEnrolleeTobacco;
	}

	/**
	 * @param primaryEnrolleeTobacco
	 *            the primaryEnrolleeTobacco to set
	 */
	public void setPrimaryEnrolleeTobacco(Double primaryEnrolleeTobacco) {
		this.primaryEnrolleeTobacco = primaryEnrolleeTobacco;
	}

	/**
	 * @return the primaryEnrolleeOneDependent
	 */
	public Double getPrimaryEnrolleeOneDependent() {
		return primaryEnrolleeOneDependent;
	}

	/**
	 * @param primaryEnrolleeOneDependent
	 *            the primaryEnrolleeOneDependent to set
	 */
	public void setPrimaryEnrolleeOneDependent(
			Double primaryEnrolleeOneDependent) {
		this.primaryEnrolleeOneDependent = primaryEnrolleeOneDependent;
	}

	/**
	 * @return the primaryEnrolleeTwoDependent
	 */
	public Double getPrimaryEnrolleeTwoDependent() {
		return primaryEnrolleeTwoDependent;
	}

	/**
	 * @param primaryEnrolleeTwoDependent
	 *            the primaryEnrolleeTwoDependent to set
	 */
	public void setPrimaryEnrolleeTwoDependent(
			Double primaryEnrolleeTwoDependent) {
		this.primaryEnrolleeTwoDependent = primaryEnrolleeTwoDependent;
	}

	/**
	 * @return the primaryEnrolleeManyDependent
	 */
	public Double getPrimaryEnrolleeManyDependent() {
		return primaryEnrolleeManyDependent;
	}

	/**
	 * @param primaryEnrolleeManyDependent
	 *            the primaryEnrolleeManyDependent to set
	 */
	public void setPrimaryEnrolleeManyDependent(
			Double primaryEnrolleeManyDependent) {
		this.primaryEnrolleeManyDependent = primaryEnrolleeManyDependent;
	}

	/**
	 * @return the coupleEnrollee
	 */
	public Double getCoupleEnrollee() {
		return coupleEnrollee;
	}

	/**
	 * @param coupleEnrollee
	 *            the coupleEnrollee to set
	 */
	public void setCoupleEnrollee(Double coupleEnrollee) {
		this.coupleEnrollee = coupleEnrollee;
	}

	/**
	 * @return the coupleEnrolleeOneDependent
	 */
	public Double getCoupleEnrolleeOneDependent() {
		return coupleEnrolleeOneDependent;
	}

	/**
	 * @param coupleEnrolleeOneDependent
	 *            the coupleEnrolleeOneDependent to set
	 */
	public void setCoupleEnrolleeOneDependent(Double coupleEnrolleeOneDependent) {
		this.coupleEnrolleeOneDependent = coupleEnrolleeOneDependent;
	}

	/**
	 * @return the coupleEnrolleeTwoDependent
	 */
	public Double getCoupleEnrolleeTwoDependent() {
		return coupleEnrolleeTwoDependent;
	}

	/**
	 * @param coupleEnrolleeTwoDependent
	 *            the coupleEnrolleeTwoDependent to set
	 */
	public void setCoupleEnrolleeTwoDependent(Double coupleEnrolleeTwoDependent) {
		this.coupleEnrolleeTwoDependent = coupleEnrolleeTwoDependent;
	}

	/**
	 * @return the coupleEnrolleeManyDependent
	 */
	public Double getCoupleEnrolleeManyDependent() {
		return coupleEnrolleeManyDependent;
	}

	/**
	 * @param coupleEnrolleeManyDependent
	 *            the coupleEnrolleeManyDependent to set
	 */
	public void setCoupleEnrolleeManyDependent(
			Double coupleEnrolleeManyDependent) {
		this.coupleEnrolleeManyDependent = coupleEnrolleeManyDependent;
	}

	/**
	 * @return the isIssuerData
	 */
	public String getIsIssuerData() {
		return isIssuerData;
	}

	/**
	 * @param isIssuerData
	 *            the isIssuerData to set
	 */
	public void setIsIssuerData(String isIssuerData) {
		this.isIssuerData = isIssuerData;
	}

	/**
	 * @return the isFamilyOption
	 */
	public boolean getFamilyOption() {
		return isFamilyOption;
	}

	/**
	 * @param isFamilyOption
	 *            the isFamilyOption to set
	 */
	public void setFamilyOption(boolean isFamilyOption) {
		this.isFamilyOption = isFamilyOption;
	}

}
