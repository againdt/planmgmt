package com.getinsured.serffadapter.vo;

import java.util.ArrayList;
import java.util.List;

public class CostShareVarianceVO {
	private String planId;
	private String planMarketingName;
	private String metalLevel;
	private String csrVariationType;
	private String issuerActuarialValue;
	private String avCalculatorOutputNumber;
	private String medicalAndDrugDeductiblesIntegrated;
	private String medicalAndDrugMaxOutOfPocketIntegrated;
	private String multipleProviderTiers;
	private String firstTierUtilization;
	private String secondTierUtilization;
	private String defaultCopayInNetwork;
	private String defaultCopayOutOfNetwork;
	private String defaultCoInsuranceInNetwork;
	private String defaultCoInsuranceOutOfNetwork;
	private SBCVO sbc;
	private List<DeductibleVO> moopList;
	private List<DeductibleVO> planDeductibleList;

	/**
	 * @return the planId
	 */
	public String getPlanId() {
		return planId;
	}

	/**
	 * @param planId
	 *            the planId to set
	 */
	public void setPlanId(String planId) {
		this.planId = planId;
	}

	/**
	 * @return the planMarketingName
	 */
	public String getPlanMarketingName() {
		return planMarketingName;
	}

	/**
	 * @param planMarketingName
	 *            the planMarketingName to set
	 */
	public void setPlanMarketingName(String planMarketingName) {
		this.planMarketingName = planMarketingName;
	}

	/**
	 * @return the metalLevel
	 */
	public String getMetalLevel() {
		return metalLevel;
	}

	/**
	 * @param metalLevel
	 *            the metalLevel to set
	 */
	public void setMetalLevel(String metalLevel) {
		this.metalLevel = metalLevel;
	}

	/**
	 * @return the csrVariationType
	 */
	public String getCsrVariationType() {
		return csrVariationType;
	}

	/**
	 * @param csrVariationType
	 *            the csrVariationType to set
	 */
	public void setCsrVariationType(String csrVariationType) {
		this.csrVariationType = csrVariationType;
	}

	/**
	 * @return the issuerActuarialValue
	 */
	public String getIssuerActuarialValue() {
		return issuerActuarialValue;
	}

	/**
	 * @param issuerActuarialValue
	 *            the issuerActuarialValue to set
	 */
	public void setIssuerActuarialValue(String issuerActuarialValue) {
		this.issuerActuarialValue = issuerActuarialValue;
	}

	/**
	 * @return the avCalculatorOutputNumber
	 */
	public String getAvCalculatorOutputNumber() {
		return avCalculatorOutputNumber;
	}

	/**
	 * @param avCalculatorOutputNumber
	 *            the avCalculatorOutputNumber to set
	 */
	public void setAvCalculatorOutputNumber(String avCalculatorOutputNumber) {
		this.avCalculatorOutputNumber = avCalculatorOutputNumber;
	}

	/**
	 * @return the medicalAndDrugDeductiblesIntegrated
	 */
	public String getMedicalAndDrugDeductiblesIntegrated() {
		return medicalAndDrugDeductiblesIntegrated;
	}

	/**
	 * @param medicalAndDrugDeductiblesIntegrated
	 *            the medicalAndDrugDeductiblesIntegrated to set
	 */
	public void setMedicalAndDrugDeductiblesIntegrated(
			String medicalAndDrugDeductiblesIntegrated) {
		this.medicalAndDrugDeductiblesIntegrated = medicalAndDrugDeductiblesIntegrated;
	}

	/**
	 * @return the medicalAndDrugMaxOutOfPocketIntegrated
	 */
	public String getMedicalAndDrugMaxOutOfPocketIntegrated() {
		return medicalAndDrugMaxOutOfPocketIntegrated;
	}

	/**
	 * @param medicalAndDrugMaxOutOfPocketIntegrated
	 *            the medicalAndDrugMaxOutOfPocketIntegrated to set
	 */
	public void setMedicalAndDrugMaxOutOfPocketIntegrated(
			String medicalAndDrugMaxOutOfPocketIntegrated) {
		this.medicalAndDrugMaxOutOfPocketIntegrated = medicalAndDrugMaxOutOfPocketIntegrated;
	}

	/**
	 * @return the multipleProviderTiers
	 */
	public String getMultipleProviderTiers() {
		return multipleProviderTiers;
	}

	/**
	 * @param multipleProviderTiers
	 *            the multipleProviderTiers to set
	 */
	public void setMultipleProviderTiers(String multipleProviderTiers) {
		this.multipleProviderTiers = multipleProviderTiers;
	}

	/**
	 * @return the firstTierUtilization
	 */
	public String getFirstTierUtilization() {
		return firstTierUtilization;
	}

	/**
	 * @param firstTierUtilization
	 *            the firstTierUtilization to set
	 */
	public void setFirstTierUtilization(String firstTierUtilization) {
		this.firstTierUtilization = firstTierUtilization;
	}

	/**
	 * @return the secondTierUtilization
	 */
	public String getSecondTierUtilization() {
		return secondTierUtilization;
	}

	/**
	 * @param secondTierUtilization
	 *            the secondTierUtilization to set
	 */
	public void setSecondTierUtilization(String secondTierUtilization) {
		this.secondTierUtilization = secondTierUtilization;
	}

	/**
	 * @return the defaultCopayInNetwork
	 */
	public String getDefaultCopayInNetwork() {
		return defaultCopayInNetwork;
	}

	/**
	 * @param defaultCopayInNetwork
	 *            the defaultCopayInNetwork to set
	 */
	public void setDefaultCopayInNetwork(String defaultCopayInNetwork) {
		this.defaultCopayInNetwork = defaultCopayInNetwork;
	}

	/**
	 * @return the defaultCopayOutOfNetwork
	 */
	public String getDefaultCopayOutOfNetwork() {
		return defaultCopayOutOfNetwork;
	}

	/**
	 * @param defaultCopayOutOfNetwork
	 *            the defaultCopayOutOfNetwork to set
	 */
	public void setDefaultCopayOutOfNetwork(String defaultCopayOutOfNetwork) {
		this.defaultCopayOutOfNetwork = defaultCopayOutOfNetwork;
	}

	/**
	 * @return the defaultCoInsuranceInNetwork
	 */
	public String getDefaultCoInsuranceInNetwork() {
		return defaultCoInsuranceInNetwork;
	}

	/**
	 * @param defaultCoInsuranceInNetwork
	 *            the defaultCoInsuranceInNetwork to set
	 */
	public void setDefaultCoInsuranceInNetwork(
			String defaultCoInsuranceInNetwork) {
		this.defaultCoInsuranceInNetwork = defaultCoInsuranceInNetwork;
	}

	/**
	 * @return the defaultCoInsuranceOutOfNetwork
	 */
	public String getDefaultCoInsuranceOutOfNetwork() {
		return defaultCoInsuranceOutOfNetwork;
	}

	/**
	 * @param defaultCoInsuranceOutOfNetwork
	 *            the defaultCoInsuranceOutOfNetwork to set
	 */
	public void setDefaultCoInsuranceOutOfNetwork(
			String defaultCoInsuranceOutOfNetwork) {
		this.defaultCoInsuranceOutOfNetwork = defaultCoInsuranceOutOfNetwork;
	}

	/**
	 * @return the sbc
	 */
	public SBCVO getSbc() {
		return sbc;
	}

	/**
	 * @param sbc
	 *            the sbc to set
	 */
	public void setSbc(SBCVO sbc) {
		this.sbc = sbc;
	}

	/**
	 * @return the moopList
	 */
	public List<DeductibleVO> getMoopList() {
		if (moopList == null) {
			moopList = new ArrayList<DeductibleVO>();
		}
		return moopList;
	}

	/**
	 * @return the planDeductibleList
	 */
	public List<DeductibleVO> getPlanDeductibleList() {
		if (planDeductibleList == null) {
			planDeductibleList = new ArrayList<DeductibleVO>();
		}
		return planDeductibleList;
	}
}
