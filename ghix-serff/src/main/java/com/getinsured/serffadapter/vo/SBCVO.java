package com.getinsured.serffadapter.vo;

public class SBCVO {
	private String havingBabyDeductible;
	private String havingBabyCoInsurance;
	private String havingBabyCoPayment;
	private String havingBabyLimit;
	private String havingDiabetesDeductible;
	private String havingDiabetesCopay;
	private String havingDiabetesCoInsurance;
	private String havingDiabetesLimit;

	/**
	 * @return the havingBabyDeductible
	 */
	public String getHavingBabyDeductible() {
		return havingBabyDeductible;
	}

	/**
	 * @param havingBabyDeductible
	 *            the havingBabyDeductible to set
	 */
	public void setHavingBabyDeductible(String havingBabyDeductible) {
		this.havingBabyDeductible = havingBabyDeductible;
	}

	/**
	 * @return the havingBabyCoInsurance
	 */
	public String getHavingBabyCoInsurance() {
		return havingBabyCoInsurance;
	}

	/**
	 * @param havingBabyCoInsurance
	 *            the havingBabyCoInsurance to set
	 */
	public void setHavingBabyCoInsurance(String havingBabyCoInsurance) {
		this.havingBabyCoInsurance = havingBabyCoInsurance;
	}

	/**
	 * @return the havingBabyCoPayment
	 */
	public String getHavingBabyCoPayment() {
		return havingBabyCoPayment;
	}

	/**
	 * @param havingBabyCoPayment
	 *            the havingBabyCoPayment to set
	 */
	public void setHavingBabyCoPayment(String havingBabyCoPayment) {
		this.havingBabyCoPayment = havingBabyCoPayment;
	}

	/**
	 * @return the havingBabyLimit
	 */
	public String getHavingBabyLimit() {
		return havingBabyLimit;
	}

	/**
	 * @param havingBabyLimit
	 *            the havingBabyLimit to set
	 */
	public void setHavingBabyLimit(String havingBabyLimit) {
		this.havingBabyLimit = havingBabyLimit;
	}

	/**
	 * @return the havingDiabetesDeductible
	 */
	public String getHavingDiabetesDeductible() {
		return havingDiabetesDeductible;
	}

	/**
	 * @param havingDiabetesDeductible
	 *            the havingDiabetesDeductible to set
	 */
	public void setHavingDiabetesDeductible(String havingDiabetesDeductible) {
		this.havingDiabetesDeductible = havingDiabetesDeductible;
	}

	/**
	 * @return the havingDiabetesCopay
	 */
	public String getHavingDiabetesCopay() {
		return havingDiabetesCopay;
	}

	/**
	 * @param havingDiabetesCopay
	 *            the havingDiabetesCopay to set
	 */
	public void setHavingDiabetesCopay(String havingDiabetesCopay) {
		this.havingDiabetesCopay = havingDiabetesCopay;
	}

	/**
	 * @return the havingDiabetesCoInsurance
	 */
	public String getHavingDiabetesCoInsurance() {
		return havingDiabetesCoInsurance;
	}

	/**
	 * @param havingDiabetesCoInsurance
	 *            the havingDiabetesCoInsurance to set
	 */
	public void setHavingDiabetesCoInsurance(String havingDiabetesCoInsurance) {
		this.havingDiabetesCoInsurance = havingDiabetesCoInsurance;
	}

	/**
	 * @return the havingDiabetesLimit
	 */
	public String getHavingDiabetesLimit() {
		return havingDiabetesLimit;
	}

	/**
	 * @param havingDiabetesLimit
	 *            the havingDiabetesLimit to set
	 */
	public void setHavingDiabetesLimit(String havingDiabetesLimit) {
		this.havingDiabetesLimit = havingDiabetesLimit;
	}

}
