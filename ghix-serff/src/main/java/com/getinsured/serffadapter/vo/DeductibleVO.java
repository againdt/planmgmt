package com.getinsured.serffadapter.vo;

public class DeductibleVO {
	private String name;
	private String inNetworkTier1Individual;
	private String inNetworkTier1Family;
	private String inNetworkTier2Individual;
	private String inNetworkTier2Family;
	private String outOfNetworkIndividual;
	private String outOfNetworkFamily;
	private String combinedInOutNetworkIndividual;
	private String combinedInOutNetworkFamily;
	private String coinsuranceInNetworkTier1;
	private String coinsuranceInNetworkTier2;
	private String coinsuranceOutofNetwork;
	private String combinedInOrOutTier2;
	private String deductibleType;

	/**
	 * 
	 * @return
	 */
	public String getDeductibleType() {
		return deductibleType;
	}

	/**
	 * 
	 * @param deductibleType
	 */
	public void setDeductibleType(String deductibleType) {
		this.deductibleType = deductibleType;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the inNetworkTier1Individual
	 */
	public String getInNetworkTier1Individual() {
		return inNetworkTier1Individual;
	}

	/**
	 * @param inNetworkTier1Individual
	 *            the inNetworkTier1Individual to set
	 */
	public void setInNetworkTier1Individual(String inNetworkTier1Individual) {
		this.inNetworkTier1Individual = inNetworkTier1Individual;
	}

	/**
	 * @return the inNetworkTier1Family
	 */
	public String getInNetworkTier1Family() {
		return inNetworkTier1Family;
	}

	/**
	 * @param inNetworkTier1Family
	 *            the inNetworkTier1Family to set
	 */
	public void setInNetworkTier1Family(String inNetworkTier1Family) {
		this.inNetworkTier1Family = inNetworkTier1Family;
	}

	/**
	 * @return the inNetworkTier2Individual
	 */
	public String getInNetworkTier2Individual() {
		return inNetworkTier2Individual;
	}

	/**
	 * @param inNetworkTier2Individual
	 *            the inNetworkTier2Individual to set
	 */
	public void setInNetworkTier2Individual(String inNetworkTier2Individual) {
		this.inNetworkTier2Individual = inNetworkTier2Individual;
	}

	/**
	 * @return the inNetworkTier2Family
	 */
	public String getInNetworkTier2Family() {
		return inNetworkTier2Family;
	}

	/**
	 * @param inNetworkTier2Family
	 *            the inNetworkTier2Family to set
	 */
	public void setInNetworkTier2Family(String inNetworkTier2Family) {
		this.inNetworkTier2Family = inNetworkTier2Family;
	}

	/**
	 * @return the outOfNetworkIndividual
	 */
	public String getOutOfNetworkIndividual() {
		return outOfNetworkIndividual;
	}

	/**
	 * @param outOfNetworkIndividual
	 *            the outOfNetworkIndividual to set
	 */
	public void setOutOfNetworkIndividual(String outOfNetworkIndividual) {
		this.outOfNetworkIndividual = outOfNetworkIndividual;
	}

	/**
	 * @return the outOfNetworkFamily
	 */
	public String getOutOfNetworkFamily() {
		return outOfNetworkFamily;
	}

	/**
	 * @param outOfNetworkFamily
	 *            the outOfNetworkFamily to set
	 */
	public void setOutOfNetworkFamily(String outOfNetworkFamily) {
		this.outOfNetworkFamily = outOfNetworkFamily;
	}

	/**
	 * @return the combinedInOutNetworkIndividual
	 */
	public String getCombinedInOutNetworkIndividual() {
		return combinedInOutNetworkIndividual;
	}

	/**
	 * @param combinedInOutNetworkIndividual
	 *            the combinedInOutNetworkIndividual to set
	 */
	public void setCombinedInOutNetworkIndividual(
			String combinedInOutNetworkIndividual) {
		this.combinedInOutNetworkIndividual = combinedInOutNetworkIndividual;
	}

	/**
	 * @return the combinedInOutNetworkFamily
	 */
	public String getCombinedInOutNetworkFamily() {
		return combinedInOutNetworkFamily;
	}

	/**
	 * @param combinedInOutNetworkFamily
	 *            the combinedInOutNetworkFamily to set
	 */
	public void setCombinedInOutNetworkFamily(String combinedInOutNetworkFamily) {
		this.combinedInOutNetworkFamily = combinedInOutNetworkFamily;
	}

	/**
	 * @return the coinsuranceInNetworkTier1
	 */
	public String getCoinsuranceInNetworkTier1() {
		return coinsuranceInNetworkTier1;
	}

	/**
	 * @param coinsuranceInNetworkTier1
	 *            the coinsuranceInNetworkTier1 to set
	 */
	public void setCoinsuranceInNetworkTier1(String coinsuranceInNetworkTier1) {
		this.coinsuranceInNetworkTier1 = coinsuranceInNetworkTier1;
	}

	/**
	 * @return the coinsuranceInNetworkTier2
	 */
	public String getCoinsuranceInNetworkTier2() {
		return coinsuranceInNetworkTier2;
	}

	/**
	 * @param coinsuranceInNetworkTier2
	 *            the coinsuranceInNetworkTier2 to set
	 */
	public void setCoinsuranceInNetworkTier2(String coinsuranceInNetworkTier2) {
		this.coinsuranceInNetworkTier2 = coinsuranceInNetworkTier2;
	}

	/**
	 * @return the coinsuranceOutofNetwork
	 */
	public String getCoinsuranceOutofNetwork() {
		return coinsuranceOutofNetwork;
	}

	/**
	 * @param coinsuranceOutofNetwork
	 *            the coinsuranceOutofNetwork to set
	 */
	public void setCoinsuranceOutofNetwork(String coinsuranceOutofNetwork) {
		this.coinsuranceOutofNetwork = coinsuranceOutofNetwork;
	}

	/**
	 * @return the combinedInOrOutTier2
	 */
	public String getCombinedInOrOutTier2() {
		return combinedInOrOutTier2;
	}

	/**
	 * @param combinedInOrOutTier2
	 *            the combinedInOrOutTier2 to set
	 */
	public void setCombinedInOrOutTier2(String combinedInOrOutTier2) {
		this.combinedInOrOutTier2 = combinedInOrOutTier2;
	}

}
