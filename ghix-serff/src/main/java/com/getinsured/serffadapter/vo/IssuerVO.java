package com.getinsured.serffadapter.vo;

import com.serff.util.SerffUtils;

public class IssuerVO {

	private String issuerHIOSId;
	private String issuerStateCode;
	private String issuerTinCode;
	private String defaultMarketingName;
	private String defaultNetworkURL;

	/**
	 * @return the issuerHIOSId
	 */
	public String getIssuerHIOSId() {
		return issuerHIOSId;
	}

	/**
	 * @param issuerHIOSId
	 *            the issuerHIOSId to set
	 */
	public void setIssuerHIOSId(String issuerHIOSId) {
		this.issuerHIOSId = issuerHIOSId;
	}

	/**
	 * @return the issuerStateCode
	 */
	public String getIssuerStateCode() {
		return issuerStateCode;
	}

	/**
	 * @param issuerStateCode
	 *            the issuerStateCode to set
	 */
	public void setIssuerStateCode(String issuerStateCode) {
		this.issuerStateCode = issuerStateCode;
	}

	/**
	 * @return the issuerTinCode
	 */
	public String getIssuerTinCode() {
		return issuerTinCode;
	}

	/**
	 * @param issuerTinCode
	 *            the issuerTinCode to set
	 */
	public void setIssuerTinCode(String issuerTinCode) {
		this.issuerTinCode = issuerTinCode;
	}

	/**
	 * @return the defaultMarketingName
	 */
	public String getDefaultMarketingName() {
		return defaultMarketingName;
	}

	/**
	 * @param defaultMarketingName the defaultMarketingName to set
	 */
	public void setDefaultMarketingName(String defaultMarketingName) {
		this.defaultMarketingName = defaultMarketingName;
	}

	/**
	 * @return the defaultNetworkURL
	 */
	public String getDefaultNetworkURL() {
		return defaultNetworkURL;
	}

	/**
	 * @param defaultNetworkURL the defaultNetworkURL to set
	 */
	public void setDefaultNetworkURL(String defaultNetworkURL) {
		this.defaultNetworkURL = SerffUtils.checkAndcorrectURL(defaultNetworkURL);
	}
}
