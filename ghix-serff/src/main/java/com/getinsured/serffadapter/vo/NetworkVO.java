package com.getinsured.serffadapter.vo;

public class NetworkVO {
	private String networkID;
	private String roleOfOrganizationReference;
	private String websiteURI;
	private String marketingName;

	public String getNetworkID() {
		return networkID;
	}

	public void setNetworkID(String networkID) {
		this.networkID = networkID;
	}

	public String getRoleOfOrganizationReference() {
		return roleOfOrganizationReference;
	}

	public void setRoleOfOrganizationReference(
			String roleOfOrganizationReference) {
		this.roleOfOrganizationReference = roleOfOrganizationReference;
	}

	public String getWebsiteURI() {
		return websiteURI;
	}

	public void setWebsiteURI(String websiteURI) {
		this.websiteURI = websiteURI;
	}

	public String getMarketingName() {
		return marketingName;
	}

	public void setMarketingName(String marketingName) {
		this.marketingName = marketingName;
	}

}
