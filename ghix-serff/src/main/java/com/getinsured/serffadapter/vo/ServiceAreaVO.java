package com.getinsured.serffadapter.vo;

import java.util.List;

public class ServiceAreaVO {

	private String serviceAreaId;
	private String serviceAreaName;
	private String countyCode;
	private String partialCountyReason;
	private List<String> postalCodes;
	private boolean entireState;
	private boolean partialCounty;

	/**
	 * @return the serviceAreaId
	 */
	public String getServiceAreaId() {
		return serviceAreaId;
	}

	/**
	 * @param serviceAreaId
	 *            the serviceAreaId to set
	 */
	public void setServiceAreaId(String serviceAreaId) {
		this.serviceAreaId = serviceAreaId;
	}

	/**
	 * @return the serviceAreaName
	 */
	public String getServiceAreaName() {
		return serviceAreaName;
	}

	/**
	 * @param serviceAreaName
	 *            the serviceAreaName to set
	 */
	public void setServiceAreaName(String serviceAreaName) {
		this.serviceAreaName = serviceAreaName;
	}

	/**
	 * @return the countyCode
	 */
	public String getCountyCode() {
		return countyCode;
	}

	/**
	 * @param countyCode
	 *            the countyCode to set
	 */
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	/**
	 * @return the partialCountyReason
	 */
	public String getPartialCountyReason() {
		return partialCountyReason;
	}

	/**
	 * @param partialCountyReason
	 *            the partialCountyReason to set
	 */
	public void setPartialCountyReason(String partialCountyReason) {
		this.partialCountyReason = partialCountyReason;
	}

	/**
	 * @return the postalCodes
	 */
	public List<String> getPostalCodes() {
		return postalCodes;
	}

	/**
	 * @param postalCodes
	 *            the postalCodes to set
	 */
	public void setPostalCodes(List<String> postalCodes) {
		this.postalCodes = postalCodes;
	}

	/**
	 * @return the entireState
	 */
	public boolean isEntireState() {
		return entireState;
	}

	/**
	 * @param entireState
	 *            the entireState to set
	 */
	public void setEntireState(boolean entireState) {
		this.entireState = entireState;
	}

	/**
	 * @return the partialCounty
	 */
	public boolean isPartialCounty() {
		return partialCounty;
	}

	/**
	 * @param partialCounty
	 *            the partialCounty to set
	 */
	public void setPartialCounty(boolean partialCounty) {
		this.partialCounty = partialCounty;
	}

}
