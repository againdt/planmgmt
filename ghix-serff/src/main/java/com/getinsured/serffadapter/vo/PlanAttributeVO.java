package com.getinsured.serffadapter.vo;

import com.serff.util.SerffUtils;

public class PlanAttributeVO {
	private String standardComponentID;
	private String planMarketingName;
	private String hiosProductID;
	private String hpid;
	private String networkID;
	private String serviceAreaID;
	private String formularyID;
	private String isNewPlan;
	private String planType;
	private String metalLevel;
	private String uniquePlanDesign;
	private String qhpOrNonQhp;
	private String insurancePlanPregnancyNoticeReqInd;
	private String isSpecialistReferralRequired;
	private String healthCareSpecialistReferralType;
	private String insurancePlanBenefitExclusionText;
	private String indianPlanVariation;
	private String hsaEligibility;
	private String employerHSAHRAContributionIndicator;
	private String empContributionAmountForHSAOrHRA;
	private String childOnlyOffering;
	private String childOnlyPlanID;
	private String isWellnessProgramOffered;
	private String isDiseaseMgmtProgramsOffered;
	private String ehbApportionmentForPediatricDental;
	private String guaranteedVsEstimatedRate;
	private String maximumCoinsuranceForSpecialtyDrugs;
	private String maxNumDaysForChargingInpatientCopay;
	private String beginPrimaryCareCostSharingAfterSetNumberVisits;
	private String beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays;
	private String planEffectiveDate;
	private String planExpirationDate;
	private String outOfCountryCoverage;
	private String outOfCountryCoverageDescription;
	private String outOfServiceAreaCoverage;
	private String outOfServiceAreaCoverageDescription;
	private String nationalNetwork;
	private String summaryBenefitAndCoverageURL;
	private String enrollmentPaymentURL;
	private String planBrochure;
	private String marketCoverage;
	private String dentalPlanOnlyInd;

	/**
	 * @return the standardComponentID
	 */
	public String getStandardComponentID() {
		return standardComponentID;
	}

	/**
	 * @param standardComponentID
	 *            the standardComponentID to set
	 */
	public void setStandardComponentID(String standardComponentID) {
		this.standardComponentID = standardComponentID;
	}

	/**
	 * @return the planMarketingName
	 */
	public String getPlanMarketingName() {
		return planMarketingName;
	}

	/**
	 * @param planMarketingName
	 *            the planMarketingName to set
	 */
	public void setPlanMarketingName(String planMarketingName) {
		this.planMarketingName = planMarketingName;
	}

	/**
	 * @return the hiosProductID
	 */
	public String getHiosProductID() {
		return hiosProductID;
	}

	/**
	 * @param hiosProductID
	 *            the hiosProductID to set
	 */
	public void setHiosProductID(String hiosProductID) {
		this.hiosProductID = hiosProductID;
	}

	/**
	 * @return the hpid
	 */
	public String getHpid() {
		return hpid;
	}

	/**
	 * @param hpid
	 *            the hpid to set
	 */
	public void setHpid(String hpid) {
		this.hpid = hpid;
	}

	/**
	 * @return the networkID
	 */
	public String getNetworkID() {
		return networkID;
	}

	/**
	 * @param networkID
	 *            the networkID to set
	 */
	public void setNetworkID(String networkID) {
		this.networkID = networkID;
	}

	/**
	 * @return the serviceAreaID
	 */
	public String getServiceAreaID() {
		return serviceAreaID;
	}

	/**
	 * @param serviceAreaID
	 *            the serviceAreaID to set
	 */
	public void setServiceAreaID(String serviceAreaID) {
		this.serviceAreaID = serviceAreaID;
	}

	/**
	 * @return the formularyID
	 */
	public String getFormularyID() {
		return formularyID;
	}

	/**
	 * @param formularyID
	 *            the formularyID to set
	 */
	public void setFormularyID(String formularyID) {
		this.formularyID = formularyID;
	}

	/**
	 * @return the isNewPlan
	 */
	public String getIsNewPlan() {
		return isNewPlan;
	}

	/**
	 * @param isNewPlan
	 *            the isNewPlan to set
	 */
	public void setIsNewPlan(String isNewPlan) {
		this.isNewPlan = isNewPlan;
	}

	/**
	 * @return the planType
	 */
	public String getPlanType() {
		return planType;
	}

	/**
	 * @param planType
	 *            the planType to set
	 */
	public void setPlanType(String planType) {
		this.planType = planType;
	}

	/**
	 * @return the metalLevel
	 */
	public String getMetalLevel() {
		return metalLevel;
	}

	/**
	 * @param metalLevel
	 *            the metalLevel to set
	 */
	public void setMetalLevel(String metalLevel) {
		this.metalLevel = metalLevel;
	}

	/**
	 * @return the uniquePlanDesign
	 */
	public String getUniquePlanDesign() {
		return uniquePlanDesign;
	}

	/**
	 * @param uniquePlanDesign
	 *            the uniquePlanDesign to set
	 */
	public void setUniquePlanDesign(String uniquePlanDesign) {
		this.uniquePlanDesign = uniquePlanDesign;
	}

	/**
	 * @return the qhpOrNonQhp
	 */
	public String getQhpOrNonQhp() {
		return qhpOrNonQhp;
	}

	/**
	 * @param qhpOrNonQhp
	 *            the qhpOrNonQhp to set
	 */
	public void setQhpOrNonQhp(String qhpOrNonQhp) {
		this.qhpOrNonQhp = qhpOrNonQhp;
	}

	/**
	 * @return the insurancePlanPregnancyNoticeReqInd
	 */
	public String getInsurancePlanPregnancyNoticeReqInd() {
		return insurancePlanPregnancyNoticeReqInd;
	}

	/**
	 * @param insurancePlanPregnancyNoticeReqInd
	 *            the insurancePlanPregnancyNoticeReqInd to set
	 */
	public void setInsurancePlanPregnancyNoticeReqInd(
			String insurancePlanPregnancyNoticeReqInd) {
		this.insurancePlanPregnancyNoticeReqInd = insurancePlanPregnancyNoticeReqInd;
	}

	/**
	 * @return the isSpecialistReferralRequired
	 */
	public String getIsSpecialistReferralRequired() {
		return isSpecialistReferralRequired;
	}

	/**
	 * @param isSpecialistReferralRequired
	 *            the isSpecialistReferralRequired to set
	 */
	public void setIsSpecialistReferralRequired(
			String isSpecialistReferralRequired) {
		this.isSpecialistReferralRequired = isSpecialistReferralRequired;
	}

	/**
	 * @return the healthCareSpecialistReferralType
	 */
	public String getHealthCareSpecialistReferralType() {
		return healthCareSpecialistReferralType;
	}

	/**
	 * @param healthCareSpecialistReferralType
	 *            the healthCareSpecialistReferralType to set
	 */
	public void setHealthCareSpecialistReferralType(
			String healthCareSpecialistReferralType) {
		this.healthCareSpecialistReferralType = healthCareSpecialistReferralType;
	}

	/**
	 * @return the insurancePlanBenefitExclusionText
	 */
	public String getInsurancePlanBenefitExclusionText() {
		return insurancePlanBenefitExclusionText;
	}

	/**
	 * @param insurancePlanBenefitExclusionText
	 *            the insurancePlanBenefitExclusionText to set
	 */
	public void setInsurancePlanBenefitExclusionText(
			String insurancePlanBenefitExclusionText) {
		this.insurancePlanBenefitExclusionText = insurancePlanBenefitExclusionText;
	}

	/**
	 * @return the indianPlanVariation
	 */
	public String getIndianPlanVariation() {
		return indianPlanVariation;
	}

	/**
	 * @param indianPlanVariation
	 *            the indianPlanVariation to set
	 */
	public void setIndianPlanVariation(String indianPlanVariation) {
		this.indianPlanVariation = indianPlanVariation;
	}

	/**
	 * @return the hsaEligibility
	 */
	public String getHsaEligibility() {
		return hsaEligibility;
	}

	/**
	 * @param hsaEligibility
	 *            the hsaEligibility to set
	 */
	public void setHsaEligibility(String hsaEligibility) {
		this.hsaEligibility = hsaEligibility;
	}

	/**
	 * @return the employerHSAHRAContributionIndicator
	 */
	public String getEmployerHSAHRAContributionIndicator() {
		return employerHSAHRAContributionIndicator;
	}

	/**
	 * @param employerHSAHRAContributionIndicator
	 *            the employerHSAHRAContributionIndicator to set
	 */
	public void setEmployerHSAHRAContributionIndicator(
			String employerHSAHRAContributionIndicator) {
		this.employerHSAHRAContributionIndicator = employerHSAHRAContributionIndicator;
	}

	/**
	 * @return the empContributionAmountForHSAOrHRA
	 */
	public String getEmpContributionAmountForHSAOrHRA() {
		return empContributionAmountForHSAOrHRA;
	}

	/**
	 * @param empContributionAmountForHSAOrHRA
	 *            the empContributionAmountForHSAOrHRA to set
	 */
	public void setEmpContributionAmountForHSAOrHRA(
			String empContributionAmountForHSAOrHRA) {
		this.empContributionAmountForHSAOrHRA = empContributionAmountForHSAOrHRA;
	}

	/**
	 * @return the childOnlyOffering
	 */
	public String getChildOnlyOffering() {
		return childOnlyOffering;
	}

	/**
	 * @param childOnlyOffering
	 *            the childOnlyOffering to set
	 */
	public void setChildOnlyOffering(String childOnlyOffering) {
		this.childOnlyOffering = childOnlyOffering;
	}

	/**
	 * @return the childOnlyPlanID
	 */
	public String getChildOnlyPlanID() {
		return childOnlyPlanID;
	}

	/**
	 * @param childOnlyPlanID
	 *            the childOnlyPlanID to set
	 */
	public void setChildOnlyPlanID(String childOnlyPlanID) {
		this.childOnlyPlanID = childOnlyPlanID;
	}

	/**
	 * @return the isWellnessProgramOffered
	 */
	public String getIsWellnessProgramOffered() {
		return isWellnessProgramOffered;
	}

	/**
	 * @param isWellnessProgramOffered
	 *            the isWellnessProgramOffered to set
	 */
	public void setIsWellnessProgramOffered(String isWellnessProgramOffered) {
		this.isWellnessProgramOffered = isWellnessProgramOffered;
	}

	/**
	 * @return the isDiseaseMgmtProgramsOffered
	 */
	public String getIsDiseaseMgmtProgramsOffered() {
		return isDiseaseMgmtProgramsOffered;
	}

	/**
	 * @param isDiseaseMgmtProgramsOffered
	 *            the isDiseaseMgmtProgramsOffered to set
	 */
	public void setIsDiseaseMgmtProgramsOffered(
			String isDiseaseMgmtProgramsOffered) {
		this.isDiseaseMgmtProgramsOffered = isDiseaseMgmtProgramsOffered;
	}

	/**
	 * @return the ehbApportionmentForPediatricDental
	 */
	public String getEhbApportionmentForPediatricDental() {
		return ehbApportionmentForPediatricDental;
	}

	/**
	 * @param ehbApportionmentForPediatricDental
	 *            the ehbApportionmentForPediatricDental to set
	 */
	public void setEhbApportionmentForPediatricDental(
			String ehbApportionmentForPediatricDental) {
		this.ehbApportionmentForPediatricDental = ehbApportionmentForPediatricDental;
	}

	/**
	 * @return the guaranteedVsEstimatedRate
	 */
	public String getGuaranteedVsEstimatedRate() {
		return guaranteedVsEstimatedRate;
	}

	/**
	 * @param guaranteedVsEstimatedRate
	 *            the guaranteedVsEstimatedRate to set
	 */
	public void setGuaranteedVsEstimatedRate(String guaranteedVsEstimatedRate) {
		this.guaranteedVsEstimatedRate = guaranteedVsEstimatedRate;
	}

	/**
	 * @return the maximumCoinsuranceForSpecialtyDrugs
	 */
	public String getMaximumCoinsuranceForSpecialtyDrugs() {
		return maximumCoinsuranceForSpecialtyDrugs;
	}

	/**
	 * @param maximumCoinsuranceForSpecialtyDrugs
	 *            the maximumCoinsuranceForSpecialtyDrugs to set
	 */
	public void setMaximumCoinsuranceForSpecialtyDrugs(
			String maximumCoinsuranceForSpecialtyDrugs) {
		this.maximumCoinsuranceForSpecialtyDrugs = maximumCoinsuranceForSpecialtyDrugs;
	}

	/**
	 * @return the maxNumDaysForChargingInpatientCopay
	 */
	public String getMaxNumDaysForChargingInpatientCopay() {
		return maxNumDaysForChargingInpatientCopay;
	}

	/**
	 * @param maxNumDaysForChargingInpatientCopay
	 *            the maxNumDaysForChargingInpatientCopay to set
	 */
	public void setMaxNumDaysForChargingInpatientCopay(
			String maxNumDaysForChargingInpatientCopay) {
		this.maxNumDaysForChargingInpatientCopay = maxNumDaysForChargingInpatientCopay;
	}

	/**
	 * @return the beginPrimaryCareCostSharingAfterSetNumberVisits
	 */
	public String getBeginPrimaryCareCostSharingAfterSetNumberVisits() {
		return beginPrimaryCareCostSharingAfterSetNumberVisits;
	}

	/**
	 * @param beginPrimaryCareCostSharingAfterSetNumberVisits
	 *            the beginPrimaryCareCostSharingAfterSetNumberVisits to set
	 */
	public void setBeginPrimaryCareCostSharingAfterSetNumberVisits(
			String beginPrimaryCareCostSharingAfterSetNumberVisits) {
		this.beginPrimaryCareCostSharingAfterSetNumberVisits = beginPrimaryCareCostSharingAfterSetNumberVisits;
	}

	/**
	 * @return the beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays
	 */
	public String getBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays() {
		return beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays;
	}

	/**
	 * @param beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays
	 *            the
	 *            beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays to
	 *            set
	 */
	public void setBeginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays(
			String beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays) {
		this.beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays = beginPrimaryCareDeductibleOrCoinsuranceAfterSetNumberCopays;
	}

	/**
	 * @return the planEffectiveDate
	 */
	public String getPlanEffectiveDate() {
		return planEffectiveDate;
	}

	/**
	 * @param planEffectiveDate
	 *            the planEffectiveDate to set
	 */
	public void setPlanEffectiveDate(String planEffectiveDate) {
		this.planEffectiveDate = planEffectiveDate;
	}

	/**
	 * @return the planExpirationDate
	 */
	public String getPlanExpirationDate() {
		return planExpirationDate;
	}

	/**
	 * @param planExpirationDate
	 *            the planExpirationDate to set
	 */
	public void setPlanExpirationDate(String planExpirationDate) {
		this.planExpirationDate = planExpirationDate;
	}

	/**
	 * @return the outOfCountryCoverage
	 */
	public String getOutOfCountryCoverage() {
		return outOfCountryCoverage;
	}

	/**
	 * @param outOfCountryCoverage
	 *            the outOfCountryCoverage to set
	 */
	public void setOutOfCountryCoverage(String outOfCountryCoverage) {
		this.outOfCountryCoverage = outOfCountryCoverage;
	}

	/**
	 * @return the outOfCountryCoverageDescription
	 */
	public String getOutOfCountryCoverageDescription() {
		return outOfCountryCoverageDescription;
	}

	/**
	 * @param outOfCountryCoverageDescription
	 *            the outOfCountryCoverageDescription to set
	 */
	public void setOutOfCountryCoverageDescription(
			String outOfCountryCoverageDescription) {
		this.outOfCountryCoverageDescription = outOfCountryCoverageDescription;
	}

	/**
	 * @return the outOfServiceAreaCoverage
	 */
	public String getOutOfServiceAreaCoverage() {
		return outOfServiceAreaCoverage;
	}

	/**
	 * @param outOfServiceAreaCoverage
	 *            the outOfServiceAreaCoverage to set
	 */
	public void setOutOfServiceAreaCoverage(String outOfServiceAreaCoverage) {
		this.outOfServiceAreaCoverage = outOfServiceAreaCoverage;
	}

	/**
	 * @return the outOfServiceAreaCoverageDescription
	 */
	public String getOutOfServiceAreaCoverageDescription() {
		return outOfServiceAreaCoverageDescription;
	}

	/**
	 * @param outOfServiceAreaCoverageDescription
	 *            the outOfServiceAreaCoverageDescription to set
	 */
	public void setOutOfServiceAreaCoverageDescription(
			String outOfServiceAreaCoverageDescription) {
		this.outOfServiceAreaCoverageDescription = outOfServiceAreaCoverageDescription;
	}

	/**
	 * @return the nationalNetwork
	 */
	public String getNationalNetwork() {
		return nationalNetwork;
	}

	/**
	 * @param nationalNetwork
	 *            the nationalNetwork to set
	 */
	public void setNationalNetwork(String nationalNetwork) {
		this.nationalNetwork = nationalNetwork;
	}

	/**
	 * @return the summaryBenefitAndCoverageURL
	 */
	public String getSummaryBenefitAndCoverageURL() {
		return summaryBenefitAndCoverageURL;
	}

	/**
	 * @param summaryBenefitAndCoverageURL
	 *            the summaryBenefitAndCoverageURL to set
	 */
	public void setSummaryBenefitAndCoverageURL(
			String summaryBenefitAndCoverageURL) {
		this.summaryBenefitAndCoverageURL = SerffUtils.checkAndcorrectURL(summaryBenefitAndCoverageURL);
	}

	/**
	 * @return the enrollmentPaymentURL
	 */
	public String getEnrollmentPaymentURL() {
		return enrollmentPaymentURL;
	}

	/**
	 * @param enrollmentPaymentURL
	 *            the enrollmentPaymentURL to set
	 */
	public void setEnrollmentPaymentURL(String enrollmentPaymentURL) {
		this.enrollmentPaymentURL = SerffUtils.checkAndcorrectURL(enrollmentPaymentURL);
	}

	/**
	 * @return the planBrochure
	 */
	public String getPlanBrochure() {
		return planBrochure;
	}

	/**
	 * @param planBrochure
	 *            the planBrochure to set
	 */
	public void setPlanBrochure(String planBrochure) {
		this.planBrochure = planBrochure;
	}

	/**
	 * @return the marketCoverage
	 */
	public String getMarketCoverage() {
		return marketCoverage;
	}

	/**
	 * @param marketCoverage
	 *            the marketCoverage to set
	 */
	public void setMarketCoverage(String marketCoverage) {
		this.marketCoverage = marketCoverage;
	}

	/**
	 * @return the dentalPlanOnlyInd
	 */
	public String getDentalPlanOnlyInd() {
		return dentalPlanOnlyInd;
	}

	/**
	 * @param dentalPlanOnlyInd
	 *            the dentalPlanOnlyInd to set
	 */
	public void setDentalPlanOnlyInd(String dentalPlanOnlyInd) {
		this.dentalPlanOnlyInd = dentalPlanOnlyInd;
	}
}
