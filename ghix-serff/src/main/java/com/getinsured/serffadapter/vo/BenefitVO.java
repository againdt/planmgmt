package com.getinsured.serffadapter.vo;

public class BenefitVO {
	private String benefitTypeCode;
	private String isEHB;
	private String isStateMandate;
	private String isBenefitCovered;
	private String serviceLimit;
	private String quantityLimit;
	private String unitLimit;
	private String minimumStay;
	private String exclusion;
	private String explanation;
	private String ehbVarianceReason;
	private String subjectToDeductibleTier1;
	private String subjectToDeductibleTier2;
	private String excludedInNetworkMOOP;
	private String excludedOutOfNetworkMOOP;

	private String copayInNetworkTier1;
	private String copayInNetworkTier2;
	private String copayOutOfNetwork;
	private String coInsuranceInNetworkTier1;
	private String coInsuranceInNetworkTier2;
	private String coInsuranceOutOfNetwork;
	private String visitType;

	/**
	 * 
	 * @return
	 */
	public String getVisitType() {
		return visitType;
	}

	/**
	 * 
	 * @param visitType
	 */
	public void setVisitType(String visitType) {
		this.visitType = visitType;
	}

	/**
	 * @return the benefitTypeCode
	 */
	public String getBenefitTypeCode() {
		return benefitTypeCode;
	}

	/**
	 * @param benefitTypeCode
	 *            the benefitTypeCode to set
	 */
	public void setBenefitTypeCode(String benefitTypeCode) {
		this.benefitTypeCode = benefitTypeCode;
	}

	/**
	 * @return the isEHB
	 */
	public String getIsEHB() {
		return isEHB;
	}

	/**
	 * @param isEHB
	 *            the isEHB to set
	 */
	public void setIsEHB(String isEHB) {
		this.isEHB = isEHB;
	}

	/**
	 * @return the isStateMandate
	 */
	public String getIsStateMandate() {
		return isStateMandate;
	}

	/**
	 * @param isStateMandate
	 *            the isStateMandate to set
	 */
	public void setIsStateMandate(String isStateMandate) {
		this.isStateMandate = isStateMandate;
	}

	/**
	 * @return the isBenefitCovered
	 */
	public String getIsBenefitCovered() {
		return isBenefitCovered;
	}

	/**
	 * @param isBenefitCovered
	 *            the isBenefitCovered to set
	 */
	public void setIsBenefitCovered(String isBenefitCovered) {
		this.isBenefitCovered = isBenefitCovered;
	}

	/**
	 * @return the serviceLimit
	 */
	public String getServiceLimit() {
		return serviceLimit;
	}

	/**
	 * @param serviceLimit
	 *            the serviceLimit to set
	 */
	public void setServiceLimit(String serviceLimit) {
		this.serviceLimit = serviceLimit;
	}

	/**
	 * @return the quantityLimit
	 */
	public String getQuantityLimit() {
		return quantityLimit;
	}

	/**
	 * @param quantityLimit
	 *            the quantityLimit to set
	 */
	public void setQuantityLimit(String quantityLimit) {
		this.quantityLimit = quantityLimit;
	}

	/**
	 * @return the unitLimit
	 */
	public String getUnitLimit() {
		return unitLimit;
	}

	/**
	 * @param unitLimit
	 *            the unitLimit to set
	 */
	public void setUnitLimit(String unitLimit) {
		this.unitLimit = unitLimit;
	}

	/**
	 * @return the minimumStay
	 */
	public String getMinimumStay() {
		return minimumStay;
	}

	/**
	 * @param minimumStay
	 *            the minimumStay to set
	 */
	public void setMinimumStay(String minimumStay) {
		this.minimumStay = minimumStay;
	}

	/**
	 * @return the exclusion
	 */
	public String getExclusion() {
		return exclusion;
	}

	/**
	 * @param exclusion
	 *            the exclusion to set
	 */
	public void setExclusion(String exclusion) {
		this.exclusion = exclusion;
	}

	/**
	 * @return the explanation
	 */
	public String getExplanation() {
		return explanation;
	}

	/**
	 * @param explanation
	 *            the explanation to set
	 */
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}

	/**
	 * @return the ehbVarianceReason
	 */
	public String getEhbVarianceReason() {
		return ehbVarianceReason;
	}

	/**
	 * @param ehbVarianceReason
	 *            the ehbVarianceReason to set
	 */
	public void setEhbVarianceReason(String ehbVarianceReason) {
		this.ehbVarianceReason = ehbVarianceReason;
	}

	/**
	 * @return the subjectToDeductibleTier1
	 */
	public String getSubjectToDeductibleTier1() {
		return subjectToDeductibleTier1;
	}

	/**
	 * @param subjectToDeductibleTier1
	 *            the subjectToDeductibleTier1 to set
	 */
	public void setSubjectToDeductibleTier1(String subjectToDeductibleTier1) {
		this.subjectToDeductibleTier1 = subjectToDeductibleTier1;
	}

	/**
	 * @return the subjectToDeductibleTier2
	 */
	public String getSubjectToDeductibleTier2() {
		return subjectToDeductibleTier2;
	}

	/**
	 * @param subjectToDeductibleTier2
	 *            the subjectToDeductibleTier2 to set
	 */
	public void setSubjectToDeductibleTier2(String subjectToDeductibleTier2) {
		this.subjectToDeductibleTier2 = subjectToDeductibleTier2;
	}

	/**
	 * @return the excludedInNetworkMOOP
	 */
	public String getExcludedInNetworkMOOP() {
		return excludedInNetworkMOOP;
	}

	/**
	 * @param excludedInNetworkMOOP
	 *            the excludedInNetworkMOOP to set
	 */
	public void setExcludedInNetworkMOOP(String excludedInNetworkMOOP) {
		this.excludedInNetworkMOOP = excludedInNetworkMOOP;
	}

	/**
	 * @return the excludedOutOfNetworkMOOP
	 */
	public String getExcludedOutOfNetworkMOOP() {
		return excludedOutOfNetworkMOOP;
	}

	/**
	 * @param excludedOutOfNetworkMOOP
	 *            the excludedOutOfNetworkMOOP to set
	 */
	public void setExcludedOutOfNetworkMOOP(String excludedOutOfNetworkMOOP) {
		this.excludedOutOfNetworkMOOP = excludedOutOfNetworkMOOP;
	}

	/**
	 * @return the copayInNetworkTier1
	 */
	public String getCopayInNetworkTier1() {
		return copayInNetworkTier1;
	}

	/**
	 * @param copayInNetworkTier1
	 *            the copayInNetworkTier1 to set
	 */
	public void setCopayInNetworkTier1(String copayInNetworkTier1) {
		this.copayInNetworkTier1 = copayInNetworkTier1;
	}

	/**
	 * @return the copayInNetworkTier2
	 */
	public String getCopayInNetworkTier2() {
		return copayInNetworkTier2;
	}

	/**
	 * @param copayInNetworkTier2
	 *            the copayInNetworkTier2 to set
	 */
	public void setCopayInNetworkTier2(String copayInNetworkTier2) {
		this.copayInNetworkTier2 = copayInNetworkTier2;
	}

	/**
	 * @return the copayOutOfNetwork
	 */
	public String getCopayOutOfNetwork() {
		return copayOutOfNetwork;
	}

	/**
	 * @param copayOutOfNetwork
	 *            the copayOutOfNetwork to set
	 */
	public void setCopayOutOfNetwork(String copayOutOfNetwork) {
		this.copayOutOfNetwork = copayOutOfNetwork;
	}

	/**
	 * @return the coInsuranceInNetworkTier1
	 */
	public String getCoInsuranceInNetworkTier1() {
		return coInsuranceInNetworkTier1;
	}

	/**
	 * @param coInsuranceInNetworkTier1
	 *            the coInsuranceInNetworkTier1 to set
	 */
	public void setCoInsuranceInNetworkTier1(String coInsuranceInNetworkTier1) {
		this.coInsuranceInNetworkTier1 = coInsuranceInNetworkTier1;
	}

	/**
	 * @return the coInsuranceInNetworkTier2
	 */
	public String getCoInsuranceInNetworkTier2() {
		return coInsuranceInNetworkTier2;
	}

	/**
	 * @param coInsuranceInNetworkTier2
	 *            the coInsuranceInNetworkTier2 to set
	 */
	public void setCoInsuranceInNetworkTier2(String coInsuranceInNetworkTier2) {
		this.coInsuranceInNetworkTier2 = coInsuranceInNetworkTier2;
	}

	/**
	 * @return the coInsuranceOutOfNetwork
	 */
	public String getCoInsuranceOutOfNetwork() {
		return coInsuranceOutOfNetwork;
	}

	/**
	 * @param coInsuranceOutOfNetwork
	 *            the coInsuranceOutOfNetwork to set
	 */
	public void setCoInsuranceOutOfNetwork(String coInsuranceOutOfNetwork) {
		this.coInsuranceOutOfNetwork = coInsuranceOutOfNetwork;
	}

}
