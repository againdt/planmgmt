package com.getinsured.serffadapter.stm;

import static com.serff.util.SerffConstants.FTP_STM_FILE;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.serffadapter.excel.ExcelReader;
import com.getinsured.serffadapter.stm.STMExcelColumn.STMExcelColumnEnum;
import com.serff.service.SerffService;
import com.serff.service.templates.STMPlanMgmtSerffService;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

/**
 * Class is used to read STM Plan Excel data to STMExcelVO POJO class.
 * 
 * @since May 3, 2014
 */
@Service("stmPlanReader")
public class STMPlanReader extends ExcelReader {
	
	private static final Logger LOGGER = Logger.getLogger(STMPlanReader.class);
	private static final String MACHINE_READABLE = "Machine Readable";
	private static final String MSG_ERROR_STM_PLANS = "STM Plans has not been Loaded Successfully because ";
	private static final String MSG_STM_PLANS_SUCCESS = "STM Plans has been Loaded Successfully.";
	private static final String MSG_FILE = "File: ";
	
	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private STMPlanMgmtSerffService stmPlanMgmtSerffService;
	@Autowired private SerffService serffService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private SerffUtils serffUtils;
	@Autowired private SerffResourceUtil serffResourceUtil;
	
	public String processAllExcelTemplate(String folderPath) {
		
		LOGGER.info("processAllExcelTemplate() Start");
		StringBuilder message = new StringBuilder();
		
		try {
			LOGGER.debug("STM Plan FTP Folder Path: " + folderPath);
			List<String> fileList = ftpClient.getFileNames(folderPath);
			LOGGER.debug("fileList is NULL: " + (null == fileList));
			
			if (null == fileList || fileList.isEmpty()) {
				message.append("No STM Excel Template is available for processing.");
				return message.toString();
			}
			
			LOGGER.info("Number of Excel Template is " + fileList.size());
			SerffPlanMgmtBatch trackingBatchRecord = null;
			boolean isEmpty = true;
			
			for (String fileName : fileList) {
				
				if (StringUtils.isBlank(fileName)
						|| fileName.equalsIgnoreCase(SerffConstants.FTP_SUCCESS)
						|| fileName.equalsIgnoreCase(SerffConstants.FTP_ERROR)) {
					continue;
				}
				isEmpty = false;
				LOGGER.info("File Name: " + fileName);
				
				trackingBatchRecord = serffService.getTrackingBatchRecord(fileName, FTP_STM_FILE, message);
				if(null != trackingBatchRecord) {
					//set status as in progress
					trackingBatchRecord = processSelectedBatch(trackingBatchRecord, fileName, folderPath, message);
				}
			}
			
			if (isEmpty) {
				message.append("No STM Excel Template is available for processing.");
			}
		}
		finally {
			
			if (StringUtils.isEmpty(message.toString())) {
				message.append(MSG_STM_PLANS_SUCCESS);
			}
			LOGGER.info(message);
			LOGGER.info("processAllExcelTemplate() End");
		}
		return message.toString();
	}
	
	private SerffPlanMgmtBatch processSelectedBatch(SerffPlanMgmtBatch trackingBatchRecord, String fileName, String folderPath, StringBuilder message) {
		SerffPlanMgmt serffTrackingRecord = null;
		SerffPlanMgmtBatch updatedTrackingBatchRecord = serffService.updateTrackingBatchRecordAsInProgress(trackingBatchRecord);
		try {
			serffTrackingRecord = serffUtils.createSerffPlanMgmtRecord(SerffConstants.TRANSFER_STM,
					SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.W);
			if (null != serffTrackingRecord) {
				serffTrackingRecord.setAttachmentsList(fileName);
		        serffTrackingRecord.setRequestStateDesc("Uploaded succesfully and ready to load plans to DB.");
		        serffTrackingRecord = serffService.saveSerffPlanMgmt(serffTrackingRecord);
				message.append(loadAllExcelTemplate(serffTrackingRecord, fileName, folderPath, trackingBatchRecord.getDefaultTenant()));
			}
		} catch (UnknownHostException e) {
				message.append(MSG_ERROR_STM_PLANS + e.getMessage());
				LOGGER.error(e.getMessage(), e);
		} finally {
			SerffPlanMgmtBatch.BATCH_STATUS status = null;
			if (null != serffTrackingRecord && serffTrackingRecord.getRequestStatus().equals(SerffPlanMgmt.REQUEST_STATUS.S)) {
				//set status as success
				status = SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED;
			} else {
				status = SerffPlanMgmtBatch.BATCH_STATUS.FAILED;
			}
			updatedTrackingBatchRecord = serffService.updateTrackingBatchRecordAsCompleted(updatedTrackingBatchRecord, serffTrackingRecord, status);
		}
		return updatedTrackingBatchRecord;
	}

	private String loadAllExcelTemplate(SerffPlanMgmt trackingRecord, String fileName, String folderPath, String defaultTenantIds) {
		
		LOGGER.debug("loadAllExcelTemplate() Start");
		StringBuilder message = new StringBuilder();
		boolean isSuccess = false;
		InputStream inputStream = null;
		
		try {
			inputStream = ftpClient.getFileInputStream(folderPath + fileName);
			
			if (null == inputStream) {
				trackingRecord.setPmResponseXml("STM Excel is not found at FTP server.");
				message.append(trackingRecord.getPmResponseXml());
				trackingRecord.setRequestStateDesc(message.toString());
				return message.toString();
			}
			
			// Upload Excel File to ECM
			isSuccess = addExcelInECM(message, trackingRecord, folderPath, fileName);
			
			if (!isSuccess) {
				LOGGER.error("Failed upload Excel file to ECM.");
				return message.toString();
			}
			
			// Load Excel File
			loadFile(inputStream);
			
			// Get first sheet from the workbook
			XSSFSheet sheet = getSheet(0);
			String sheetname = sheet.getSheetName();
			
			if (!sheetname.contains(MACHINE_READABLE)) {
				message.append("Machine Readable sheet is not found in STM File.");
//				message.append(fileName);
			}
			// Read and Persist STM data from Excel
			isSuccess = readAndPersistSTMData(sheet, trackingRecord, defaultTenantIds);
			
			if (isSuccess) {
				
				
				if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
					trackingRecord.setPmResponseXml(MSG_STM_PLANS_SUCCESS);
					trackingRecord.setRequestStateDesc(MSG_STM_PLANS_SUCCESS);
				}
				else {
					trackingRecord.setRequestStateDesc("STM Plans has been Loaded with warnings. Please see status message.");
					message.append(trackingRecord.getRequestStateDesc());
				}
			}
			else {
				message.append("Failed to load STM Plans.");
				trackingRecord.setRequestStateDesc(message.toString());
			}
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			message.append(MSG_ERROR_STM_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			
			if (!isSuccess) {
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
			} else {
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
			}
			trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
			
			String fileMessage = MSG_FILE + fileName + ":=\n";
			if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
				trackingRecord.setPmResponseXml(fileMessage + trackingRecord.getRequestStateDesc());
			}
			else {
				trackingRecord.setPmResponseXml(fileMessage + trackingRecord.getPmResponseXml());
			}
			trackingRecord.setEndTime(new Date());
			serffService.saveSerffPlanMgmt(trackingRecord);
			moveFileToDirectory(isSuccess, fileName);
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("loadAllExcelTemplate() End");
		}
		return message.toString();
	}
	
	private boolean addExcelInECM(StringBuilder message, SerffPlanMgmt trackingRecord, String folderPath, String fileName) throws IOException {
		
		boolean isSuccess = false;
		InputStream inputStream = null;
		
		try {
			inputStream = ftpClient.getFileInputStream(folderPath + fileName);
			
			if (null != inputStream) {
				LOGGER.info("Uploading File Name with path to ECM:" + SerffConstants.SERF_ECM_BASE_PATH + SerffConstants.FTP_STM_FILE + fileName);
				// Uploading STM Excel at ECM.
				SerffDocument attachment = serffUtils.addAttachmentInECM(ecmService, trackingRecord, SerffConstants.SERF_ECM_BASE_PATH, SerffConstants.FTP_STM_FILE, fileName, IOUtils.toByteArray(inputStream), true);
				serffService.saveSerffDocument(attachment);
				isSuccess = true;
			}
			else {
				message.append("Content is not valid for file: ");
				message.append(fileName);
				trackingRecord.setRequestStateDesc(message.toString());
			}
		}
		catch (Exception ex) {
			message.append(MSG_ERROR_STM_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
		}
		return isSuccess;
	}
	
	public boolean readAndPersistSTMData(XSSFSheet sheet, SerffPlanMgmt trackingRecord, String defaultTenantIds){
		return readAndPersistSTMData(sheet, trackingRecord, null, null, defaultTenantIds);
	}
	
	public boolean readAndPersistSTMData(XSSFSheet sheet, SerffPlanMgmt trackingRecord, EntityManager hccEntityManager, Map<String, Plan> hccPlanMap, String defaultTenantIds) {
		
		LOGGER.debug("readAndPersistSTMData() Start");
		boolean status = Boolean.FALSE;
		
		try {
			
			if (null == sheet) {
				LOGGER.info("XSSFSheet is null.");
				return status;
			}
			// Iterate through each rows from first sheet
			List<STMExcelVO> stmList = new ArrayList<STMExcelVO>();
			Iterator<Row> rowIterator = sheet.iterator();
			boolean isFirstRow = true;
			STMExcelVO stmExcelVO = null;
			
			while (rowIterator.hasNext()) {
				
				Row excelRow = rowIterator.next();
				
				if (isFirstRow) {
					isFirstRow = false;
					stmList.add(getSTMExcelVOHeader(excelRow));
					continue;
				}
				stmExcelVO = getSTMExcelVO(excelRow);
				
				if (null != stmExcelVO) {
					stmList.add(stmExcelVO);
				}
			}
			// persist STM plans from STMExcelVO object to database
			status = stmPlanMgmtSerffService.populateAndPersistSTMPlans(stmList, trackingRecord, hccEntityManager, hccPlanMap, defaultTenantIds);
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("readAndPersistSTMData() End");
		}
		return status;
	}
	
	public void moveFileToDirectory(boolean isSuccess, String fileName) {
		
		LOGGER.debug("moveFileToDirectory() Start");
		String moveToDirectoryPath = null;
		
		try {
			String baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getSTMExcelUploadPath();
			moveToDirectoryPath = baseDirectoryPath;
			
			if (isSuccess) {
				moveToDirectoryPath += SerffConstants.FTP_SUCCESS + SerffConstants.PATH_SEPERATOR;
			}
			else {
				moveToDirectoryPath += SerffConstants.FTP_ERROR + SerffConstants.PATH_SEPERATOR;
			}
			
			if (!ftpClient.isRemoteDirectoryExist(moveToDirectoryPath)) {
				ftpClient.createDirectory(moveToDirectoryPath);
			}
			LOGGER.debug("STM Plan FTP Folder Path: " + baseDirectoryPath);
			LOGGER.info("Move To Directory Path: " + moveToDirectoryPath);
			ftpClient.moveFile(baseDirectoryPath, fileName, moveToDirectoryPath, fileName);
		}
		catch (Exception ex) {
			LOGGER.info("Skipping file beacause it is already exist in directory: " + moveToDirectoryPath);
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("moveFileToDirectory() End");
		}
	}
	
	/**
	 * Method is used to store STM Excel sheet data in STMExcelVO Object.
	 * 1) STM Excel Header(First row) will store in STMExcelVO Object.
	 */
	private STMExcelVO getSTMExcelVOHeader(Row excelRow) {
		STMExcelVO stmExcelVO = null;
		
		if (null != excelRow) {
			stmExcelVO = new STMExcelVO();
			stmExcelVO.setCarrierCode(STMExcelColumnEnum.CARRIER_CODE.getColumnConstName());
			stmExcelVO.setIssuerName(STMExcelColumnEnum.ISSUER_NAME.getColumnConstName());
			stmExcelVO.setHiosId(STMExcelColumnEnum.HIOS_ID.getColumnConstName());
			stmExcelVO.setPlanName(STMExcelColumnEnum.PLAN_NAME.getColumnConstName());
			stmExcelVO.setPlanResponseName(STMExcelColumnEnum.PLAN_RESPONSE_NAME.getColumnConstName());
			stmExcelVO.setPlanNumber(STMExcelColumnEnum.ISSUER_PLAN_NUMBER.getColumnConstName());
			stmExcelVO.setNetworkType(STMExcelColumnEnum.NETWORK_TYPE.getColumnConstName());
			stmExcelVO.setPlanState(STMExcelColumnEnum.PLAN_STATE.getColumnConstName());
			stmExcelVO.setDurationNumber(STMExcelColumnEnum.DURATION_NUMBER.getColumnConstName());
			stmExcelVO.setDurationUnit(STMExcelColumnEnum.DURATION_UNIT.getColumnConstName());
			stmExcelVO.setCoinsurance(STMExcelColumnEnum.COINSURANCE.getColumnConstName());
			stmExcelVO.setOfficeVisits(STMExcelColumnEnum.OFFICE_VISITS.getColumnConstName());
			stmExcelVO.setOfficeVisitsNumber(STMExcelColumnEnum.OFFICE_VISITS_NUMBER.getColumnConstName());
			stmExcelVO.setOfficeVisitsForPrimDoct(STMExcelColumnEnum.OFFICE_VISITS_FOR_PRIM_DOCT.getColumnConstName());
			stmExcelVO.setOfficeVisitsForSepcialist(STMExcelColumnEnum.OFFICE_VISITS_FOR_SEPCIALIST.getColumnConstName());
			stmExcelVO.setDeductibleIndividual(STMExcelColumnEnum.DEDUCTIBLE_INDIVIDUAL.getColumnConstName());
			stmExcelVO.setDeductibleIndividualDesc(STMExcelColumnEnum.DEDUCTIBLE_INDIVIDUAL_DESC.getColumnConstName());
			stmExcelVO.setDeductibleFamilyDesc(STMExcelColumnEnum.DEDUCTIBLE_FAMILY_DESC.getColumnConstName());
			stmExcelVO.setOutOfPocketMaxIndividual(STMExcelColumnEnum.OUT_OF_POCKET_MAX_INDIVIDUAL.getColumnConstName());
			stmExcelVO.setOutOfPocketMaxIndividualDesc(STMExcelColumnEnum.OUT_OF_POCKET_MAX_INDIVIDUAL_DESC.getColumnConstName());
			stmExcelVO.setOutOfPocketLimitMaxFamily(STMExcelColumnEnum.OUT_OF_POCKET_LIMIT_MAX_FAMILY_DESC.getColumnConstName());
			stmExcelVO.setPolicyMaximum(STMExcelColumnEnum.POLICY_MAXIMUM.getColumnConstName());
			stmExcelVO.setIsHsa(STMExcelColumnEnum.IS_HSA.getColumnConstName());
			stmExcelVO.setOutNetwkCoverage(STMExcelColumnEnum.OUT_NETWK_COVERAGE.getColumnConstName());
			stmExcelVO.setOutCountyCoverage(STMExcelColumnEnum.OUT_COUNTY_COVERAGE.getColumnConstName());
			stmExcelVO.setPreventiveCareCoverage(STMExcelColumnEnum.PREVENTIVE_CARE_COVERAGE.getColumnConstName());
			stmExcelVO.setPeriodicHealthExam(STMExcelColumnEnum.PERIODIC_HEALTH_EXAM.getColumnConstName());
			stmExcelVO.setPeriodicObGynExam(STMExcelColumnEnum.PERIODIC_OB_GYN_EXAM.getColumnConstName());
			stmExcelVO.setWellBabyCare(STMExcelColumnEnum.WELL_BABY_CARE.getColumnConstName());
			stmExcelVO.setDrugCoverage(STMExcelColumnEnum.DRUG_COVERAGE.getColumnConstName());
			stmExcelVO.setGenericDrugs(STMExcelColumnEnum.GENERIC_DRUGS.getColumnConstName());
			stmExcelVO.setBrandDrugs(STMExcelColumnEnum.BRAND_DRUGS.getColumnConstName());
			stmExcelVO.setNonFormularyDrugsCoverage(STMExcelColumnEnum.NON_FORMULARY_DRUGS_COVERAGE.getColumnConstName());
			stmExcelVO.setMailOrderForDrug(STMExcelColumnEnum.MAIL_ORDER_FOR_DRUG.getColumnConstName());
			stmExcelVO.setSepDrugDeductible(STMExcelColumnEnum.SEP_DRUG_DEDUCTIBLE.getColumnConstName());
			stmExcelVO.setHospitalServicesCoverage(STMExcelColumnEnum.HOSPITAL_SERVICES_COVERAGE.getColumnConstName());
			stmExcelVO.setUrgentCare(STMExcelColumnEnum.URGENT_CARE.getColumnConstName());
			stmExcelVO.setEmergencyRoom(STMExcelColumnEnum.EMERGENCY_ROOM.getColumnConstName());
			stmExcelVO.setOutpatientLabXray(STMExcelColumnEnum.OUTPATIENT_LAB_XRAY.getColumnConstName());
			stmExcelVO.setOutpatientSurgery(STMExcelColumnEnum.OUTPATIENT_SURGERY.getColumnConstName());
			stmExcelVO.setHospitalization(STMExcelColumnEnum.HOSPITALIZATION.getColumnConstName());
			stmExcelVO.setAdditionalCoverage(STMExcelColumnEnum.ADDITIONAL_COVERAGE.getColumnConstName());
			stmExcelVO.setChiropracticCoverage(STMExcelColumnEnum.CHIROPRACTIC_COVERAGE.getColumnConstName());
			stmExcelVO.setMentalHealthCoverage(STMExcelColumnEnum.MENTAL_HEALTH_COVERAGE.getColumnConstName());
			stmExcelVO.setAdditionalInfo(STMExcelColumnEnum.ADDITIONAL_INFO.getColumnConstName());
			stmExcelVO.setAmBestRating(STMExcelColumnEnum.A_M_BEST_RATING.getColumnConstName());
			stmExcelVO.setElectSignAppAvbl(STMExcelColumnEnum.ELECT_SIGN_APP_AVBL.getColumnConstName());
			stmExcelVO.setApplicationFee(STMExcelColumnEnum.APPLICATION_FEE.getColumnConstName());
			stmExcelVO.setPlanBrochure(STMExcelColumnEnum.PLAN_BROCHURE.getColumnConstName());
			stmExcelVO.setExclusionsAndLimitations(STMExcelColumnEnum.EXCLUSIONS_AND_LIMITATIONS.getColumnConstName());
			stmExcelVO.setMaternityBenefitCoverage(STMExcelColumnEnum.MATERNITY_BENEFIT_COVERAGE.getColumnConstName());
			stmExcelVO.setPrePostnatalOfficeVisit(STMExcelColumnEnum.PRE_POSTNATAL_OFFICE_VISIT.getColumnConstName());
			stmExcelVO.setLaborDeliveryHospitalStay(STMExcelColumnEnum.LABOR_DELIVERY_HOSPITAL_STAY.getColumnConstName());
			stmExcelVO.setPrimCarePhysnReq(STMExcelColumnEnum.PRIM_CARE_PHYSN_REQ.getColumnConstName());
			stmExcelVO.setSpeciReferralsReq(STMExcelColumnEnum.SPECI_REFERRALS_REQ.getColumnConstName());
			stmExcelVO.setMailOrderGeneric(STMExcelColumnEnum.MAIL_ORDER_GENERIC.getColumnConstName());
			stmExcelVO.setMailOrderBrand(STMExcelColumnEnum.MAIL_ORDER_BRAND.getColumnConstName());
			stmExcelVO.setMailOrderNonFormulary(STMExcelColumnEnum.MAIL_ORDER_NON_FORMULARY.getColumnConstName());
			stmExcelVO.setMailOrderAnnualDeductible(STMExcelColumnEnum.MAIL_ORDER_ANNUAL_DEDUCTIBLE.getColumnConstName());
			stmExcelVO.setMailOrderOtherCoverage(STMExcelColumnEnum.MAIL_ORDER_OTHER_COVERAGE.getColumnConstName());
		}
		return stmExcelVO;
	}
	
	/**
	 * Method is used to store STM Excel sheet data in STMExcelVO Object.
	 * 1) STM Excel Data will store in STMExcelVO Object from second to end of row of excel sheet.
	 */
	private STMExcelVO getSTMExcelVO(Row excelRow) {
		STMExcelVO stmExcelVO = null;
		
		if (null != excelRow) {
			
			String carrierCode = getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.CARRIER_CODE.getColumnIndex()));
			
			if (StringUtils.isNotBlank(carrierCode)) {
				stmExcelVO = new STMExcelVO();
				stmExcelVO.setCarrierCode(carrierCode);
				stmExcelVO.setIssuerName(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.ISSUER_NAME.getColumnIndex())));
				stmExcelVO.setHiosId(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.HIOS_ID.getColumnIndex())));
				stmExcelVO.setPlanName(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.PLAN_NAME.getColumnIndex())));
				stmExcelVO.setPlanResponseName(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.PLAN_RESPONSE_NAME.getColumnIndex())));
				stmExcelVO.setPlanNumber(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.ISSUER_PLAN_NUMBER.getColumnIndex())));
				stmExcelVO.setNetworkType(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.NETWORK_TYPE.getColumnIndex())));
				stmExcelVO.setPlanState(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.PLAN_STATE.getColumnIndex())));
				stmExcelVO.setDurationNumber(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.DURATION_NUMBER.getColumnIndex())));
				stmExcelVO.setDurationUnit(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.DURATION_UNIT.getColumnIndex())));
				stmExcelVO.setCoinsurance(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.COINSURANCE.getColumnIndex())));
				stmExcelVO.setOfficeVisits(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OFFICE_VISITS.getColumnIndex())));
				stmExcelVO.setOfficeVisitsNumber(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OFFICE_VISITS_NUMBER.getColumnIndex())));
				stmExcelVO.setOfficeVisitsForPrimDoct(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OFFICE_VISITS_FOR_PRIM_DOCT.getColumnIndex())));
				stmExcelVO.setOfficeVisitsForSepcialist(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OFFICE_VISITS_FOR_SEPCIALIST.getColumnIndex())));
				stmExcelVO.setDeductibleIndividual(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.DEDUCTIBLE_INDIVIDUAL.getColumnIndex())));
				stmExcelVO.setDeductibleIndividualDesc(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.DEDUCTIBLE_INDIVIDUAL_DESC.getColumnIndex())));
				stmExcelVO.setDeductibleFamilyDesc(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.DEDUCTIBLE_FAMILY_DESC.getColumnIndex())));
				stmExcelVO.setOutOfPocketMaxIndividual(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OUT_OF_POCKET_MAX_INDIVIDUAL.getColumnIndex())));
				stmExcelVO.setOutOfPocketMaxIndividualDesc(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OUT_OF_POCKET_MAX_INDIVIDUAL_DESC.getColumnIndex())));
				stmExcelVO.setOutOfPocketLimitMaxFamily(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OUT_OF_POCKET_LIMIT_MAX_FAMILY_DESC.getColumnIndex())));
				stmExcelVO.setPolicyMaximum(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.POLICY_MAXIMUM.getColumnIndex())));
				stmExcelVO.setIsHsa(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.IS_HSA.getColumnIndex())));
				stmExcelVO.setOutNetwkCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OUT_NETWK_COVERAGE.getColumnIndex())));
				stmExcelVO.setOutCountyCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OUT_COUNTY_COVERAGE.getColumnIndex())));
				stmExcelVO.setPreventiveCareCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.PREVENTIVE_CARE_COVERAGE.getColumnIndex())));
				stmExcelVO.setPeriodicHealthExam(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.PERIODIC_HEALTH_EXAM.getColumnIndex())));
				stmExcelVO.setPeriodicObGynExam(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.PERIODIC_OB_GYN_EXAM.getColumnIndex())));
				stmExcelVO.setWellBabyCare(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.WELL_BABY_CARE.getColumnIndex())));
				stmExcelVO.setDrugCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.DRUG_COVERAGE.getColumnIndex())));
				stmExcelVO.setGenericDrugs(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.GENERIC_DRUGS.getColumnIndex())));
				stmExcelVO.setBrandDrugs(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.BRAND_DRUGS.getColumnIndex())));
				stmExcelVO.setNonFormularyDrugsCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.NON_FORMULARY_DRUGS_COVERAGE.getColumnIndex())));
				stmExcelVO.setMailOrderForDrug(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.MAIL_ORDER_FOR_DRUG.getColumnIndex())));
				stmExcelVO.setSepDrugDeductible(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.SEP_DRUG_DEDUCTIBLE.getColumnIndex())));
				stmExcelVO.setHospitalServicesCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.HOSPITAL_SERVICES_COVERAGE.getColumnIndex())));
				stmExcelVO.setUrgentCare(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.URGENT_CARE.getColumnIndex())));
				stmExcelVO.setEmergencyRoom(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.EMERGENCY_ROOM.getColumnIndex())));
				stmExcelVO.setOutpatientLabXray(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OUTPATIENT_LAB_XRAY.getColumnIndex())));
				stmExcelVO.setOutpatientSurgery(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.OUTPATIENT_SURGERY.getColumnIndex())));
				stmExcelVO.setHospitalization(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.HOSPITALIZATION.getColumnIndex())));
				stmExcelVO.setAdditionalCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.ADDITIONAL_COVERAGE.getColumnIndex())));
				stmExcelVO.setChiropracticCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.CHIROPRACTIC_COVERAGE.getColumnIndex())));
				stmExcelVO.setMentalHealthCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.MENTAL_HEALTH_COVERAGE.getColumnIndex())));
				stmExcelVO.setAdditionalInfo(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.ADDITIONAL_INFO.getColumnIndex())));
				stmExcelVO.setAmBestRating(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.A_M_BEST_RATING.getColumnIndex())));
				stmExcelVO.setElectSignAppAvbl(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.ELECT_SIGN_APP_AVBL.getColumnIndex())));
				stmExcelVO.setApplicationFee(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.APPLICATION_FEE.getColumnIndex())));
				stmExcelVO.setPlanBrochure(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.PLAN_BROCHURE.getColumnIndex())));
				stmExcelVO.setExclusionsAndLimitations(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.EXCLUSIONS_AND_LIMITATIONS.getColumnIndex())));
				stmExcelVO.setMaternityBenefitCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.MATERNITY_BENEFIT_COVERAGE.getColumnIndex())));
				stmExcelVO.setPrePostnatalOfficeVisit(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.PRE_POSTNATAL_OFFICE_VISIT.getColumnIndex())));
				stmExcelVO.setLaborDeliveryHospitalStay(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.LABOR_DELIVERY_HOSPITAL_STAY.getColumnIndex())));
				stmExcelVO.setPrimCarePhysnReq(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.PRIM_CARE_PHYSN_REQ.getColumnIndex())));
				stmExcelVO.setSpeciReferralsReq(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.SPECI_REFERRALS_REQ.getColumnIndex())));
				stmExcelVO.setMailOrderGeneric(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.MAIL_ORDER_GENERIC.getColumnIndex())));
				stmExcelVO.setMailOrderBrand(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.MAIL_ORDER_BRAND.getColumnIndex())));
				stmExcelVO.setMailOrderNonFormulary(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.MAIL_ORDER_NON_FORMULARY.getColumnIndex())));
				stmExcelVO.setMailOrderAnnualDeductible(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.MAIL_ORDER_ANNUAL_DEDUCTIBLE.getColumnIndex())));
				stmExcelVO.setMailOrderOtherCoverage(getCellValueTrim(excelRow.getCell(STMExcelColumnEnum.MAIL_ORDER_OTHER_COVERAGE.getColumnIndex())));
			}
		}
		return stmExcelVO;
	}
}
