package com.getinsured.serffadapter.stm;

/**
 * Class is used to generate getter/setter on the basis of STM Plan Excel Column.
 * 
 * @author Bhavin Parmar
 * @since May 3, 2014
 */
public class STMExcelVO {

	private String carrierCode;
	private String issuerName;
	private String hiosId;
	private String planName;
	private String planResponseName;
	private String planNumber;
	private String networkType;
	private String planState;
	private String durationNumber;
	private String durationUnit;
	private String coinsurance;
	private String officeVisits;
	private String officeVisitsNumber;
	private String officeVisitsForPrimDoct;
	private String officeVisitsForSepcialist;
	private String deductibleIndividual;
	private String deductibleIndividualDesc;
	private String deductibleFamilyDesc;
	private String outOfPocketMaxIndividual;
	private String outOfPocketMaxIndividualDesc;
	private String outOfPocketLimitMaxFamily;
	private String policyMaximum;
	private String isHsa;
	private String outNetwkCoverage;
	private String outCountyCoverage;
	private String preventiveCareCoverage;
	private String periodicHealthExam;
	private String periodicObGynExam;
	private String wellBabyCare;
	private String drugCoverage;
	private String genericDrugs;
	private String brandDrugs;
	private String nonFormularyDrugsCoverage;
	private String mailOrderForDrug;
	private String sepDrugDeductible;
	private String hospitalServicesCoverage;
	private String urgentCare;
	private String emergencyRoom;
	private String outpatientLabXray;
	private String outpatientSurgery;
	private String hospitalization;
	private String additionalCoverage;
	private String chiropracticCoverage;
	private String mentalHealthCoverage;
	private String additionalInfo;
	private String amBestRating;
	private String electSignAppAvbl;
	private String applicationFee;
	private String planBrochure;
	private String exclusionsAndLimitations;
	private String maternityBenefitCoverage;
	private String prePostnatalOfficeVisit;
	private String laborDeliveryHospitalStay;
	private String primCarePhysnReq;
	private String speciReferralsReq;
	private String mailOrderGeneric;
	private String mailOrderBrand;
	private String mailOrderNonFormulary;
	private String mailOrderAnnualDeductible;
	private String mailOrderOtherCoverage;

	
	private boolean isNotValid;
	private String errorMessages;

	public STMExcelVO() {
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanResponseName() {
		return planResponseName;
	}

	public void setPlanResponseName(String planResponseName) {
		this.planResponseName = planResponseName;
	}

	public String getPlanNumber() {
		return planNumber;
	}

	public void setPlanNumber(String planNumber) {
		this.planNumber = planNumber;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	public String getPlanState() {
		return planState;
	}

	public void setPlanState(String planState) {
		this.planState = planState;
	}

	public String getDurationNumber() {
		return durationNumber;
	}

	public void setDurationNumber(String durationNumber) {
		this.durationNumber = durationNumber;
	}

	public String getDurationUnit() {
		return durationUnit;
	}

	public void setDurationUnit(String durationUnit) {
		this.durationUnit = durationUnit;
	}

	public String getCoinsurance() {
		return coinsurance;
	}

	public void setCoinsurance(String coinsurance) {
		this.coinsurance = coinsurance;
	}

	public String getOfficeVisits() {
		return officeVisits;
	}

	public void setOfficeVisits(String officeVisits) {
		this.officeVisits = officeVisits;
	}

	public String getOfficeVisitsNumber() {
		return officeVisitsNumber;
	}

	public void setOfficeVisitsNumber(String officeVisitsNumber) {
		this.officeVisitsNumber = officeVisitsNumber;
	}

	public String getOfficeVisitsForPrimDoct() {
		return officeVisitsForPrimDoct;
	}

	public void setOfficeVisitsForPrimDoct(String officeVisitsForPrimDoct) {
		this.officeVisitsForPrimDoct = officeVisitsForPrimDoct;
	}

	public String getOfficeVisitsForSepcialist() {
		return officeVisitsForSepcialist;
	}

	public void setOfficeVisitsForSepcialist(String officeVisitsForSepcialist) {
		this.officeVisitsForSepcialist = officeVisitsForSepcialist;
	}

	public String getDeductibleIndividual() {
		return deductibleIndividual;
	}

	public void setDeductibleIndividual(String deductibleIndividual) {
		this.deductibleIndividual = deductibleIndividual;
	}

	public String getDeductibleIndividualDesc() {
		return deductibleIndividualDesc;
	}

	public void setDeductibleIndividualDesc(String deductibleIndividualDesc) {
		this.deductibleIndividualDesc = deductibleIndividualDesc;
	}

	public String getDeductibleFamilyDesc() {
		return deductibleFamilyDesc;
	}

	public void setDeductibleFamilyDesc(String deductibleFamilyDesc) {
		this.deductibleFamilyDesc = deductibleFamilyDesc;
	}

	public String getOutOfPocketMaxIndividual() {
		return outOfPocketMaxIndividual;
	}

	public void setOutOfPocketMaxIndividual(String outOfPocketMaxIndividual) {
		this.outOfPocketMaxIndividual = outOfPocketMaxIndividual;
	}

	public String getOutOfPocketMaxIndividualDesc() {
		return outOfPocketMaxIndividualDesc;
	}

	public void setOutOfPocketMaxIndividualDesc(String outOfPocketMaxIndividualDesc) {
		this.outOfPocketMaxIndividualDesc = outOfPocketMaxIndividualDesc;
	}

	public String getOutOfPocketLimitMaxFamily() {
		return outOfPocketLimitMaxFamily;
	}

	public void setOutOfPocketLimitMaxFamily(String outOfPocketLimitMaxFamily) {
		this.outOfPocketLimitMaxFamily = outOfPocketLimitMaxFamily;
	}

	public String getPolicyMaximum() {
		return policyMaximum;
	}

	public void setPolicyMaximum(String policyMaximum) {
		this.policyMaximum = policyMaximum;
	}

	public String getIsHsa() {
		return isHsa;
	}

	public void setIsHsa(String isHsa) {
		this.isHsa = isHsa;
	}

	public String getOutNetwkCoverage() {
		return outNetwkCoverage;
	}

	public void setOutNetwkCoverage(String outNetwkCoverage) {
		this.outNetwkCoverage = outNetwkCoverage;
	}

	public String getOutCountyCoverage() {
		return outCountyCoverage;
	}

	public void setOutCountyCoverage(String outCountyCoverage) {
		this.outCountyCoverage = outCountyCoverage;
	}

	public String getPreventiveCareCoverage() {
		return preventiveCareCoverage;
	}

	public void setPreventiveCareCoverage(String preventiveCareCoverage) {
		this.preventiveCareCoverage = preventiveCareCoverage;
	}

	public String getPeriodicHealthExam() {
		return periodicHealthExam;
	}

	public void setPeriodicHealthExam(String periodicHealthExam) {
		this.periodicHealthExam = periodicHealthExam;
	}

	public String getPeriodicObGynExam() {
		return periodicObGynExam;
	}

	public void setPeriodicObGynExam(String periodicObGynExam) {
		this.periodicObGynExam = periodicObGynExam;
	}

	public String getWellBabyCare() {
		return wellBabyCare;
	}

	public void setWellBabyCare(String wellBabyCare) {
		this.wellBabyCare = wellBabyCare;
	}

	public String getDrugCoverage() {
		return drugCoverage;
	}

	public void setDrugCoverage(String drugCoverage) {
		this.drugCoverage = drugCoverage;
	}

	public String getGenericDrugs() {
		return genericDrugs;
	}

	public void setGenericDrugs(String genericDrugs) {
		this.genericDrugs = genericDrugs;
	}

	public String getBrandDrugs() {
		return brandDrugs;
	}

	public void setBrandDrugs(String brandDrugs) {
		this.brandDrugs = brandDrugs;
	}

	public String getNonFormularyDrugsCoverage() {
		return nonFormularyDrugsCoverage;
	}

	public void setNonFormularyDrugsCoverage(String nonFormularyDrugsCoverage) {
		this.nonFormularyDrugsCoverage = nonFormularyDrugsCoverage;
	}

	public String getMailOrderForDrug() {
		return mailOrderForDrug;
	}

	public void setMailOrderForDrug(String mailOrderForDrug) {
		this.mailOrderForDrug = mailOrderForDrug;
	}

	public String getSepDrugDeductible() {
		return sepDrugDeductible;
	}

	public void setSepDrugDeductible(String sepDrugDeductible) {
		this.sepDrugDeductible = sepDrugDeductible;
	}

	public String getHospitalServicesCoverage() {
		return hospitalServicesCoverage;
	}

	public void setHospitalServicesCoverage(String hospitalServicesCoverage) {
		this.hospitalServicesCoverage = hospitalServicesCoverage;
	}

	public String getUrgentCare() {
		return urgentCare;
	}

	public void setUrgentCare(String urgentCare) {
		this.urgentCare = urgentCare;
	}

	public String getEmergencyRoom() {
		return emergencyRoom;
	}

	public void setEmergencyRoom(String emergencyRoom) {
		this.emergencyRoom = emergencyRoom;
	}

	public String getOutpatientLabXray() {
		return outpatientLabXray;
	}

	public void setOutpatientLabXray(String outpatientLabXray) {
		this.outpatientLabXray = outpatientLabXray;
	}

	public String getOutpatientSurgery() {
		return outpatientSurgery;
	}

	public void setOutpatientSurgery(String outpatientSurgery) {
		this.outpatientSurgery = outpatientSurgery;
	}

	public String getHospitalization() {
		return hospitalization;
	}

	public void setHospitalization(String hospitalization) {
		this.hospitalization = hospitalization;
	}

	public String getAdditionalCoverage() {
		return additionalCoverage;
	}

	public void setAdditionalCoverage(String additionalCoverage) {
		this.additionalCoverage = additionalCoverage;
	}

	public String getChiropracticCoverage() {
		return chiropracticCoverage;
	}

	public void setChiropracticCoverage(String chiropracticCoverage) {
		this.chiropracticCoverage = chiropracticCoverage;
	}

	public String getMentalHealthCoverage() {
		return mentalHealthCoverage;
	}

	public void setMentalHealthCoverage(String mentalHealthCoverage) {
		this.mentalHealthCoverage = mentalHealthCoverage;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getAmBestRating() {
		return amBestRating;
	}

	public void setAmBestRating(String amBestRating) {
		this.amBestRating = amBestRating;
	}

	public String getElectSignAppAvbl() {
		return electSignAppAvbl;
	}

	public void setElectSignAppAvbl(String electSignAppAvbl) {
		this.electSignAppAvbl = electSignAppAvbl;
	}

	public String getApplicationFee() {
		return applicationFee;
	}

	public void setApplicationFee(String applicationFee) {
		this.applicationFee = applicationFee;
	}

	public String getPlanBrochure() {
		return planBrochure;
	}

	public void setPlanBrochure(String planBrochure) {
		this.planBrochure = planBrochure;
	}

	public String getExclusionsAndLimitations() {
		return exclusionsAndLimitations;
	}

	public void setExclusionsAndLimitations(String exclusionsAndLimitations) {
		this.exclusionsAndLimitations = exclusionsAndLimitations;
	}

	public String getMaternityBenefitCoverage() {
		return maternityBenefitCoverage;
	}

	public void setMaternityBenefitCoverage(String maternityBenefitCoverage) {
		this.maternityBenefitCoverage = maternityBenefitCoverage;
	}

	public String getPrePostnatalOfficeVisit() {
		return prePostnatalOfficeVisit;
	}

	public void setPrePostnatalOfficeVisit(String prePostnatalOfficeVisit) {
		this.prePostnatalOfficeVisit = prePostnatalOfficeVisit;
	}

	public String getLaborDeliveryHospitalStay() {
		return laborDeliveryHospitalStay;
	}

	public void setLaborDeliveryHospitalStay(String laborDeliveryHospitalStay) {
		this.laborDeliveryHospitalStay = laborDeliveryHospitalStay;
	}

	public String getPrimCarePhysnReq() {
		return primCarePhysnReq;
	}

	public void setPrimCarePhysnReq(String primCarePhysnReq) {
		this.primCarePhysnReq = primCarePhysnReq;
	}

	public String getSpeciReferralsReq() {
		return speciReferralsReq;
	}

	public void setSpeciReferralsReq(String speciReferralsReq) {
		this.speciReferralsReq = speciReferralsReq;
	}

	public String getMailOrderGeneric() {
		return mailOrderGeneric;
	}

	public void setMailOrderGeneric(String mailOrderGeneric) {
		this.mailOrderGeneric = mailOrderGeneric;
	}

	public String getMailOrderBrand() {
		return mailOrderBrand;
	}

	public void setMailOrderBrand(String mailOrderBrand) {
		this.mailOrderBrand = mailOrderBrand;
	}

	public String getMailOrderNonFormulary() {
		return mailOrderNonFormulary;
	}

	public void setMailOrderNonFormulary(String mailOrderNonFormulary) {
		this.mailOrderNonFormulary = mailOrderNonFormulary;
	}

	public String getMailOrderAnnualDeductible() {
		return mailOrderAnnualDeductible;
	}

	public void setMailOrderAnnualDeductible(String mailOrderAnnualDeductible) {
		this.mailOrderAnnualDeductible = mailOrderAnnualDeductible;
	}

	public String getMailOrderOtherCoverage() {
		return mailOrderOtherCoverage;
	}

	public void setMailOrderOtherCoverage(String mailOrderOtherCoverage) {
		this.mailOrderOtherCoverage = mailOrderOtherCoverage;
	}

	public boolean isNotValid() {
		return isNotValid;
	}

	public void setNotValid(boolean isNotValid) {
		this.isNotValid = isNotValid;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}
}
