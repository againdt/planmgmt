package com.getinsured.serffadapter.stm;

/**
 * Class is used to generate Enum on the basis of STM Plan Excel Column.
 * 
 * @author Bhavin Parmar
 * @since May 3, 2014
 */
public class STMExcelColumn {
	
	private static final int NUMBER_TYPE = 0;
	private static final int FLOAT_TYPE = 1;
	private static final int STRING_TYPE = 2;

	public enum STMExcelColumnEnum {
		CARRIER_CODE("Carrier code (internal)", "CARRIER_CODE", 0, STRING_TYPE),
		ISSUER_NAME("Carrier - Global Display", "ISSUER_NAME", 1, STRING_TYPE),
		HIOS_ID("Carrier HIOS ID", "HIOS_ID", 2, STRING_TYPE),
		PLAN_RESPONSE_NAME("Plan Response Name", "PLAN_RESPONSE_NAME", 3, STRING_TYPE),
		PLAN_NAME("Plan Display Name", "PLAN_DISP_NAME", 4, STRING_TYPE),
		ISSUER_PLAN_NUMBER("Plan ID", "ISSUER_PLAN_NUMBER", 5, STRING_TYPE),
		NETWORK_TYPE("Plan Type", "NETWORK_TYPE", 6, STRING_TYPE),
		PLAN_STATE("States", "PLAN_STATE", 7, STRING_TYPE),
		//START_DATE("Start Date" ,"START_DATE" ,8 , STRING_TYPE),
		//END_DATE("End Date", "END_DATE" ,9 ,STRING_TYPE),
		DURATION_NUMBER("Max Duration (Number)", "DURATION_NUMBER", 8, NUMBER_TYPE),
		DURATION_UNIT("Max Duration (unit)", "DURATION_UNIT", 9, STRING_TYPE),
		COINSURANCE("Coinsurance", "COINSURANCE", 10, STRING_TYPE),
		OFFICE_VISITS("Office Visits", "OFFICE_VISIT", 11, STRING_TYPE),  // Benefit
		OFFICE_VISITS_NUMBER("Office Visits - Number only", "OFFICE_VISITS_NUMBER", 12, STRING_TYPE),  // Benefit
		OFFICE_VISITS_FOR_PRIM_DOCT("Office Visit for Primary Doctor", "PRIMARY_VISIT", 13, STRING_TYPE),  // Benefit
		OFFICE_VISITS_FOR_SEPCIALIST("Office Visit for Specialist", "SPECIAL_VISIT", 14, STRING_TYPE),  // Benefit
		DEDUCTIBLE_INDIVIDUAL("Deductible Individual (Value) ", "DEDUCTIBLE_INDIVIDUAL", 15, NUMBER_TYPE),
		DEDUCTIBLE_INDIVIDUAL_DESC("Deductible Individual (Description) ", "DEDUCTIBLE_INDIVIDUAL_DESC", 16, NUMBER_TYPE),
		DEDUCTIBLE_FAMILY_DESC("Deductible Family (Description)", "DEDUCTIBLE_FAMILY_DESC", 17, STRING_TYPE),
		OUT_OF_POCKET_MAX_INDIVIDUAL("Out of Pocket Max Individual (Value)", "OUT_OF_POCKET_MAX_INDIVIDUAL", 18, NUMBER_TYPE),
		OUT_OF_POCKET_MAX_INDIVIDUAL_DESC("Out of Pocket Max Individual (Description)", "OUT_OF_POCKET_MAX_INDIVIDUAL_DESC", 19, STRING_TYPE),
		OUT_OF_POCKET_LIMIT_MAX_FAMILY_DESC("Out of Pocket Limit Max Family (Description)", "OUT_OF_POCKET_LIMIT_MAX_FAMILY_DESC", 20, STRING_TYPE),
		POLICY_MAXIMUM("Policy Maximum", "POLICY_MAXIMUM", 21, NUMBER_TYPE),
		IS_HSA("Health Saving Account (HSA) Eligible", "IS_HSA", 22, STRING_TYPE),
		OUT_NETWK_COVERAGE("Out of Network Coverage", "OUT_NETWK_COVERAGE", 23, STRING_TYPE),
		OUT_COUNTY_COVERAGE("Out of Country Coverage", "OUT_COUNTY_COVERAGE", 24, STRING_TYPE),
		PREVENTIVE_CARE_COVERAGE("Preventive Care Coverage", "PREVENT_SCREEN_IMMU", 25, STRING_TYPE),  // Benefit
		PERIODIC_HEALTH_EXAM("Periodic Health Exam", "HEALTH_EXAM", 26, STRING_TYPE),  // Benefit
		PERIODIC_OB_GYN_EXAM("Periodic OB-GYN Exam", "OB_GYN_EXAM", 27, STRING_TYPE),  // Benefit
		WELL_BABY_CARE("Well Baby Care", "WELL_BABY", 28, STRING_TYPE),  // Benefit
		DRUG_COVERAGE("Prescription Drug Coverage", "PRESCRIPTION_DRUG_OTHER", 29, STRING_TYPE),  // Benefit
		GENERIC_DRUGS("Generic Prescription Drugs", "GENERIC", 30, STRING_TYPE),  // Benefit
		BRAND_DRUGS("Brand Prescription Drugs", "PREFERRED_BRAND", 31, STRING_TYPE),  // Benefit
		NON_FORMULARY_DRUGS_COVERAGE("Non-formulary Prescription Drugs Coverage", "NON_FORMULARY_DRUG", 32, STRING_TYPE),  // Benefit
		MAIL_ORDER_FOR_DRUG("Mail Order for Prescription Drugs", "MAIL_ORDER_PRESCRIPTION_DRUG", 33, STRING_TYPE),  // Benefit
		SEP_DRUG_DEDUCTIBLE("Separate Prescription Drugs Deductible", "SEP_DRUG_DEDUCTIBLE", 34, STRING_TYPE),
		HOSPITAL_SERVICES_COVERAGE("Hospital Services Coverage", "INPATIENT_HOSPITAL_SERVICE", 35, STRING_TYPE),  // Benefit
		URGENT_CARE("Urgent Care", "URGENT_CARE", 36, STRING_TYPE),  // Benefit
		EMERGENCY_ROOM("Emergency Room", "EMERGENCY_SERVICES", 37, STRING_TYPE),  // Benefit
		OUTPATIENT_LAB_XRAY("Outpatient Lab/X-Ray", "LABORATORY_SERVICES", 38, STRING_TYPE),  // Benefit
		OUTPATIENT_SURGERY("Outpatient Surgery", "OUTPATIENT_SURGERY_SERVICES", 39, STRING_TYPE),  // Benefit
		HOSPITALIZATION("Hospitalization", "HOSPITALIZATION", 40, STRING_TYPE),  // Benefit
		ADDITIONAL_COVERAGE("Additional Coverage", "ADDITIONAL_COVERAGE", 41, STRING_TYPE),  // Benefit
		CHIROPRACTIC_COVERAGE("Chiropractic Coverage", "CHIROPRACTIC", 42, STRING_TYPE),  // Benefit
		MENTAL_HEALTH_COVERAGE("Mental Health Coverage", "MENTAL_HEALTH_OUT", 43, STRING_TYPE),  // Benefit
		ADDITIONAL_INFO("Additional Information", "ADDITIONAL_INFO", 44, STRING_TYPE),
		A_M_BEST_RATING("A.M. Best Rating", "A_M_BEST_RATING", 45, STRING_TYPE),
		ELECT_SIGN_APP_AVBL("Electronic Signature for Application Available", "ELECT_SIGN_APP_AVBL", 46, STRING_TYPE),
		APPLICATION_FEE("Application Fee", "APPLICATION_FEE", 47, FLOAT_TYPE),
		PLAN_BROCHURE("Plan Brochure", "PLAN_BROCHURE", 48, STRING_TYPE),
		EXCLUSIONS_AND_LIMITATIONS("Exclusions and Limitations", "EXCLUSIONS_AND_LIMITATIONS", 49, STRING_TYPE),
		MATERNITY_BENEFIT_COVERAGE("Maternity Benefit Coverage", "MATERNITY_SERVICES", 50, STRING_TYPE),  // Benefit
		PRE_POSTNATAL_OFFICE_VISIT("Pre & Postnatal office Visit", "PRENATAL_POSTNATAL", 51, STRING_TYPE),  // Benefit
		LABOR_DELIVERY_HOSPITAL_STAY("Labor & Delivery Hospital Stay", "DELIVERY_IMP_MATERNITY_SERVICES", 52, STRING_TYPE),  // Benefit
		PRIM_CARE_PHYSN_REQ("Primary Care Physician Required", "PRIMARY_PHY_REQD", 53, STRING_TYPE),  // Benefit
		SPECI_REFERRALS_REQ("Specialist Referrals Required", "SPECIALIST_REFERRAL_REQD", 54, STRING_TYPE),  // Benefit
		MAIL_ORDER_GENERIC("Mail Order (Generic)", "MAIL_ORDER_GENERIC", 55, STRING_TYPE),  // Benefit
		MAIL_ORDER_BRAND("Mail Order (Brand)", "MAIL_ORDER_BRAND", 56, STRING_TYPE),  // Benefit
		MAIL_ORDER_NON_FORMULARY("Mail Order (Non-Formulary)", "MAIL_ORDER_NON_FORMULARY", 57, STRING_TYPE),  // Benefit
		MAIL_ORDER_ANNUAL_DEDUCTIBLE("Mail Order (Annual Deductible)", "MAIL_ORDER_ANNUAL_DEDT", 58, STRING_TYPE),  // Benefit
		MAIL_ORDER_OTHER_COVERAGE("Mail Order Other Coverage", "MAIL_ORDER_OTHER", 59, STRING_TYPE);  // Benefit
		
		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;
		
		private STMExcelColumnEnum(String name, String columnConstName, int index, int dataType) {
			this.columnName = name;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}
		
		public String getColumnName() {
			return columnName;
		}
		
		public String getColumnConstName() {
			return columnConstName;
		}
		
		public int getColumnIndex() {
			return columnIndex;
		}
		
		public int getDataType() {
			return dataType;
		}
	}
}
