package com.getinsured.serffadapter.vision;

import static com.getinsured.serffadapter.vision.VisionExcelRow.PLANDATA_ROW_COUNT;
import static com.getinsured.serffadapter.vision.VisionExcelRow.PLANRATE_ROW_COUNT;
import static com.getinsured.serffadapter.vision.VisionExcelRow.PLAN_DATA_START_COLUMN;
import static com.getinsured.serffadapter.vision.VisionExcelRow.RATE_DATA_START_COLUMN;
import static com.getinsured.serffadapter.vision.VisionExcelRow.SHEET_INDEX_PLAN_DATA;
import static com.getinsured.serffadapter.vision.VisionExcelRow.SHEET_INDEX_PLAN_RATES;
import static com.getinsured.serffadapter.vision.VisionExcelRow.SHEET_PLAN_DATA;
import static com.getinsured.serffadapter.vision.VisionExcelRow.SHEET_PLAN_RATES;
import static com.serff.util.SerffConstants.FTP_VISION_FILE;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.serffadapter.excel.ExcelReader;
import com.getinsured.serffadapter.vision.VisionExcelRow.VisionPlanDataRowEnum;
import com.getinsured.serffadapter.vision.VisionExcelRow.VisionPlanRatesRowEnum;
import com.serff.service.SerffService;
import com.serff.service.templates.VisionPlanMgmtSerffService;
import com.serff.service.validation.VisionPlanDataValidator;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

/**
 *-----------------------------------------------------------------------------
 * HIX-59162 PHIX - Load and persist vision plans.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to read Vision Excel data to VisionPlanDataVO and VisionPlanRatesVO DTO Objects.
 * 
 * @author Bhavin Parmar
 * @since January 27, 2015
 */
@Service("visionExcelReader")
public class VisionExcelReader extends ExcelReader {

	private static final Logger LOGGER = Logger.getLogger(VisionExcelReader.class);

	private static final String MSG_GET_EACH_ROWS = "Get each rows from ";
	private static final String EMSG_INVALID_EXCEL = "Invalid Vision excel template.";
//	private static final String EMSG_SHEET_NOT_AVAILABLE = " sheet is not available.";
	private static final String EMSG_LOAD_PLANS = "Failed to load Vision Plans. Reason: ";
//	private static final String EMSG_ERROR_ECM = "Failed to upload Vision Excel at ECM. Reason: ";
	private static final String EMSG_NO_EXCEL_FOUND = "No Vision Excel Template is available for processing.";
	private static final String EMSG_MISSING_TRACKING_REC= "Unable to read tracking record for the template.";
	private static final String EMSG_INVALID_PLAN = "Invalid HIOS Plan Number: ";
	private static final String EMSG_DUPLICATE_PLAN = "Duplicate plan in the sheet ID: ";
	private static final String EMSG_VISION = "Vision Loading Error: ";
	private static final String EMSG_VISION_PROCESS_COMPLETED = "Process loading Vision Plans completed";
	private static final int ERROR_MAX_LEN = 1000;

	@Autowired private SerffResourceUtil serffResourceUtil;
	@Autowired private SerffService serffService;
	@Autowired private VisionPlanMgmtSerffService visionPlanMgmtSerffService;
	@Autowired private VisionPlanDataValidator visionPlanDataValidator;
	@Autowired private SerffUtils serffUtils;
	@Autowired private GHIXSFTPClient ftpClient;

	/**
	 * Takes directory path which is location on FTP server and processes all available Vision excel templates. 
	 * 
	 * @param ftpFolderPath - FTP location where Vision template is Uploaded
	 * @return - Status message after processing Vision template
	 */
	public String processAllVisionTemplate(String ftpFolderPath) {

		LOGGER.info("processAllVisionTemplate() Start");
		StringBuilder uiMessage = new StringBuilder();

		try {
			List<String> ftpFileList = serffResourceUtil.getFileNameListFromFTP(ftpFolderPath);

			if (null == ftpFileList || ftpFileList.isEmpty()) {
				uiMessage.append("No Vision Excel Template is available for processing.");
				return uiMessage.toString();
			}
			LOGGER.info("Number of Excel Template is " + ftpFileList.size());
			boolean isEmptyDirectory = true;
			SerffPlanMgmtBatch trackingBatchRecord = null;
			
			for (String ftpFileName : ftpFileList) {

				if (StringUtils.isBlank(ftpFileName)
						|| ftpFileName.equalsIgnoreCase(SerffConstants.FTP_SUCCESS)
						|| ftpFileName.equalsIgnoreCase(SerffConstants.FTP_ERROR)) {
					continue;
				}
				isEmptyDirectory = false;
				LOGGER.info("Processing Vision file : Name : " + ftpFileName);
				//serffRequestId = ftpFileName.substring(0, ftpFileName.indexOf(FTP_VISION_FILE));
				trackingBatchRecord = serffService.getTrackingBatchRecord(ftpFileName, FTP_VISION_FILE, uiMessage);
				//trackingRecord = serffService.getTrackingRecord(serffRequestId, uiMessage, ftpFileName);

				if (null != trackingBatchRecord) {
					processSelectedTemplate(trackingBatchRecord, ftpFileName, ftpFolderPath, uiMessage);
				}
			}
			
			if (isEmptyDirectory) {
				uiMessage.append(EMSG_NO_EXCEL_FOUND);
			}
		} finally {
			if (StringUtils.isEmpty(uiMessage.toString())) {
				uiMessage.append(EMSG_VISION_PROCESS_COMPLETED);
			}
			LOGGER.info(uiMessage);
			LOGGER.info("getAllTemplatesForPlans() End");
		}
		
		return uiMessage.toString();
	}

	private void processSelectedTemplate(SerffPlanMgmtBatch trackingBatchRecord, String ftpFileName, String ftpFolderPath, StringBuilder uiMessage) {
		SerffPlanMgmt serffTrackingRecord = null;
		SerffPlanMgmtBatch updatedTrackingBatchRecord;
		updatedTrackingBatchRecord = serffService.updateTrackingBatchRecordAsInProgress(trackingBatchRecord);
		try {
			serffTrackingRecord = serffUtils.createSerffPlanMgmtRecord(SerffConstants.TRANSFER_VISION,
					SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.W);
			if (null != serffTrackingRecord) {
				serffTrackingRecord.setAttachmentsList(ftpFileName);
		        serffTrackingRecord.setRequestStateDesc("Uploaded succesfully and ready to load plans to DB.");
		        serffTrackingRecord = serffService.saveSerffPlanMgmt(serffTrackingRecord);
		        loadExcelTemplate(serffTrackingRecord, ftpFolderPath, uiMessage, updatedTrackingBatchRecord.getDefaultTenant());
			}
		} catch (UnknownHostException e) {
			uiMessage.append(EMSG_MISSING_TRACKING_REC);
			LOGGER.error(e.getMessage(), e);
		} finally {
			SerffPlanMgmtBatch.BATCH_STATUS status = null;
			if (null != serffTrackingRecord && serffTrackingRecord.getRequestStatus().equals(SerffPlanMgmt.REQUEST_STATUS.S)) {
				//set status as success
				status = SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED;
			} else {
				status = SerffPlanMgmtBatch.BATCH_STATUS.FAILED;
			}
			updatedTrackingBatchRecord = serffService.updateTrackingBatchRecordAsCompleted(updatedTrackingBatchRecord, serffTrackingRecord, status);
		}
	}
	/**
	 * Loads excel sheet by reading excel sheet input stream.
	 * 
	 * @param trackingRecord - Serff PlanMgmt Record
	 * @param ftpFolderPath - FTP location where Vision template is uploaded
	 * @param uiMessage - Status message of excel loading process
	 */
	private void loadExcelTemplate(SerffPlanMgmt trackingRecord, String ftpFolderPath, StringBuilder uiMessage, String defaultTenantIds) {

		LOGGER.debug("loadExcelTemplate() Start");
		boolean isSuccess = false;
		InputStream inputStream = null;

		try {
			inputStream = ftpClient.getFileInputStream(ftpFolderPath + trackingRecord.getAttachmentsList());

			if (null == inputStream) {
				LOGGER.info("Vision Excel is not found at FTP server : " + ftpFolderPath + trackingRecord.getAttachmentsList());
				trackingRecord.setPmResponseXml("Vision Excel is not found at FTP server.");
				trackingRecord.setRequestStateDesc(trackingRecord.getPmResponseXml());
				uiMessage.append(trackingRecord.getPmResponseXml());
				return;
			}
			// Upload Excel File to ECM
			isSuccess = serffResourceUtil.uploadFileFromFTPToECM(trackingRecord, ftpFolderPath, FTP_VISION_FILE, trackingRecord.getAttachmentsList(), true);
			if (!isSuccess) {
				LOGGER.info("Failed to upload Vision file in ECM : " + ftpFolderPath + trackingRecord.getAttachmentsList());
				trackingRecord.setPmResponseXml("Failed upload Excel file to ECM.");
				trackingRecord.setRequestStateDesc(trackingRecord.getPmResponseXml());
				uiMessage.append(trackingRecord.getPmResponseXml());
				return;
			}
			isSuccess = false;
			// Load Excel File
			loadFile(inputStream);

			if (!isValidExcelTemplate()) {
				trackingRecord.setPmResponseXml(EMSG_INVALID_EXCEL);
				trackingRecord.setRequestStateDesc(EMSG_INVALID_EXCEL);
				uiMessage.append(EMSG_INVALID_EXCEL);
				return;
			}
			// Populate and Persist Vision Excel
			isSuccess = populateAndPersistVisionExcel(trackingRecord, uiMessage, defaultTenantIds);
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			uiMessage.append(EMSG_LOAD_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
     		updateTrackingRecord(isSuccess, trackingRecord, uiMessage);
			String baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansVisionUploadPath();
			serffResourceUtil.moveFileToDirectory(isSuccess, trackingRecord.getAttachmentsList(), baseDirectoryPath);
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("loadExcelTemplate() End");
		}
	}

	/**
	 * Populates and persist Vision plan data in database.
	 * 
	 * @param trackingRecord - Serff Planmgmt Record
	 * @return - Status of plan loading process
	 */
	private boolean populateAndPersistVisionExcel(SerffPlanMgmt trackingRecord, StringBuilder uiMessage, String defaultTenantIds) {

		LOGGER.debug("populateAndPersistVisionExcel() Start");
		boolean isSuccess = false;

		try {
			// Get row data from the sheets.
			Row[] visionPlanDataRows = getVisionPlanDataRows(getSheet(SHEET_INDEX_PLAN_DATA));
			Row[] visionPlanRatesRows = getVisionPlanRatesRows(getSheet(SHEET_INDEX_PLAN_RATES));

			Map<String, VisionPlanDataVO> visionPlansMap = getVisionExcelData(visionPlanDataRows, visionPlanRatesRows);

			if (!CollectionUtils.isEmpty(visionPlansMap)) {
				LOGGER.info("Number of Vision plans available : " + visionPlansMap.size());
   			    isSuccess = visionPlanMgmtSerffService.populateAndPersistVisionData(visionPlansMap, getVisionExcelVOHeader(), defaultTenantIds, trackingRecord);
   			    uiMessage.append(trackingRecord.getRequestStateDesc());
			}
		}
		catch (IOException ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		} catch (GIException e) {
			trackingRecord.setRequestStateDesc(e.getMessage());
			LOGGER.error(e.getMessage(), e);
		}
		finally {
			LOGGER.debug("populateAndPersistVisionExcel() End");
		}
		return isSuccess;
	}

	/**
	 * Reads data from excel sheet row by row.
	 * 
	 * @param visionPlanDataRows - Excel data rows for Plan Data
	 * @param visionPlanRatesRows -  Excel data rows for Plan Rate Data
	 * @return - Map containing Plan Issuer Number as key and corresponding Plan Data VO as value
	 */
	private Map<String, VisionPlanDataVO> getVisionExcelData(Row[] visionPlanDataRows,
			Row[] visionPlanRatesRows) {

		LOGGER.debug("getVisionExcelData() Start");
		Map<String, VisionPlanDataVO> visionPlansMap = new HashMap<String, VisionPlanDataVO>();

		try {
			if (null != visionPlanDataRows && null != visionPlanRatesRows) {
				LOGGER.debug(MSG_GET_EACH_ROWS + "'" + SHEET_PLAN_DATA + "' sheet[1].");
				readVisionPlanData(visionPlansMap, visionPlanDataRows);

				LOGGER.debug(MSG_GET_EACH_ROWS + "'" + SHEET_PLAN_RATES + "' sheet[2].");
				readVisionRateData(visionPlansMap, visionPlanRatesRows);
			}
			else {
				LOGGER.error("Unable to read data from Excel file.");
			}
		}
		finally {
			LOGGER.debug("getVisionExcelData() End");
		}
		return visionPlansMap;
	}

	/**
	 * Reads excel Rate data and populates corresponding Plan VO with rate details.
	 * 
	 * @param visionPlansMap - Map containing Plan Issuer Number as key and corresponding Plan Data VO as value
	 * @param excelRows - Excel rate data rows
	 */
	private void readVisionRateData(Map<String, VisionPlanDataVO> visionPlansMap, Row[] excelRows) {

		LOGGER.debug("readVisionRateData() Start");
		if (null == excelRows) {
			return;
		}

		try {
			VisionPlanDataVO visionPlanDataVO;
			String hiosId, allStates, planId;
			short maxColIx = excelRows[VisionPlanRatesRowEnum.CARRIER_HIOS_ID.getRowIndex()].getLastCellNum();

			for (short colIx = RATE_DATA_START_COLUMN; colIx < maxColIx; colIx++) {

				hiosId = allStates = planId = null;
				hiosId = getColumnValueAtIndex(excelRows[VisionPlanRatesRowEnum.CARRIER_HIOS_ID.getRowIndex()], colIx);
				allStates = getColumnValueAtIndex(excelRows[VisionPlanRatesRowEnum.STATE.getRowIndex()],colIx);
				planId = getColumnValueAtIndex(excelRows[VisionPlanRatesRowEnum.PLAN_ID.getRowIndex()], colIx);

				if(StringUtils.isBlank(hiosId) || StringUtils.isBlank(allStates) || StringUtils.isBlank(planId)) {
					continue;
				}
				
				if(allStates.contains(",")){
					//Multiple states present in excel cell value.
					String[] uniqueStates = removeDuplicateStates(allStates.split(","));
					for(int index = 0 ; index < uniqueStates.length; index++){
						String stateCode = uniqueStates[index];
						visionPlanDataVO = visionPlansMap.get(hiosId + stateCode + planId);
						if(null != visionPlanDataVO){
							visionPlanDataVO.setVisionPlanRatesVO(getVisionPlanRatesVO(excelRows, colIx));
						}
					}
				}else{
					//Only one state is present in excel cell value.
					visionPlanDataVO = visionPlansMap.get(hiosId + allStates + planId);
					if(null != visionPlanDataVO){
						visionPlanDataVO.setVisionPlanRatesVO(getVisionPlanRatesVO(excelRows, colIx));
					}
				}
			}
		}catch(Exception ex){
			LOGGER.error("Error occurred in readVisionRateData()", ex);
		}
		finally {
			LOGGER.debug("readVisionRateData() End");
		}
	}

	/**
	 * Reads excel data and populates Map with Plan Number and Plan VO details.
	 * 
	 * @param visionPlansMap - Map containing Plan Issuer Number as key and corresponding Plan Data VO as value
	 * @param excelRows - Excel Plan data rows
	 */
	private void readVisionPlanData(Map<String, VisionPlanDataVO> visionPlansMap,
			Row[] excelRows) {

		LOGGER.debug("readVisionPlanData() Start");
		if (null == excelRows) {
			return;
		}

		try {
			String hiosId, allStates, planId;
			short maxColIx = excelRows[VisionPlanDataRowEnum.CARRIER_HIOS_ID
					.getRowIndex()].getLastCellNum();

			for (short colIx = PLAN_DATA_START_COLUMN; colIx < maxColIx; colIx++) {

				hiosId = allStates = planId = null;
				hiosId = getColumnValueAtIndex(excelRows[VisionPlanDataRowEnum.CARRIER_HIOS_ID.getRowIndex()], colIx);
				allStates = getColumnValueAtIndex(excelRows[VisionPlanDataRowEnum.STATE.getRowIndex()], colIx);
				planId = getColumnValueAtIndex(excelRows[VisionPlanDataRowEnum.PLAN_ID.getRowIndex()], colIx);
				
				if (StringUtils.isBlank(hiosId)
						|| StringUtils.isBlank(allStates)
						|| StringUtils.isBlank(planId)) {
					LOGGER.error(EMSG_VISION + "Empty " + VisionPlanDataRowEnum.CARRIER_HIOS_ID.getRowName() + "[" + hiosId + "] " + "Or "
							+ VisionPlanDataRowEnum.STATE.getRowName() + "[" + allStates + "] " + "Or "
							+ VisionPlanDataRowEnum.PLAN_ID.getRowName() + "[" + planId + "].");
					continue;
				}
				
				if(allStates.contains(",")){
						//Multiple states present in excel cell value.
						String[] uniqueStates = removeDuplicateStates(allStates.split(","));
						for(int index = 0 ; index < uniqueStates.length; index++){
							String stateCode = uniqueStates[index];
							if (visionPlanDataValidator.isValidHIOSPlanNumber(hiosId + stateCode + planId)){
								LOGGER.info("Multiple States : hiosId : "  + hiosId + ", stateCode : " + stateCode + ", planID : " + planId);
								addVisionPlanDataToMap(visionPlansMap, excelRows, hiosId, stateCode, hiosId + stateCode + planId, colIx);
							}else {
								LOGGER.error(EMSG_VISION + EMSG_INVALID_PLAN + hiosId + stateCode + planId);
						}
					}
				}else{
						//Only one state is present in excel cell value.
						if (visionPlanDataValidator.isValidHIOSPlanNumber(hiosId + allStates + planId)){
							LOGGER.info("Single State : hiosId : "  + hiosId + ", stateCode : " + allStates + ", planID : " + planId);
							addVisionPlanDataToMap(visionPlansMap, excelRows, hiosId, allStates, hiosId + allStates + planId, colIx);
						}else {
							LOGGER.error(EMSG_VISION + EMSG_INVALID_PLAN + hiosId + allStates + planId);
						}
				}
			}
		}catch(Exception ex){
			LOGGER.error("Error occurred in readVisionPlanData()", ex);
		}
		finally {
			LOGGER.debug("readVisionPlanData() End");
		}
	}

	/**
	 * Adds Vision VO data in Map.
	 * 
	 * @param visionPlansMap - Map containing Plan Issuer Number as key and corresponding Plan Data VO as value
	 * @param excelRows - Excel data rows
	 * @param hiosId - Issuer Id
	 * @param stateCode - State 
	 * @param mapKey - Combination of IssuerId + State Code + Plan Number from template
	 * @param colIx - Column number
	 */
	private void addVisionPlanDataToMap(Map<String, VisionPlanDataVO> visionPlansMap, Row[] excelRows, String hiosId, String stateCode, String mapKey, int colIx) {

		LOGGER.debug("addVisionPlanDataToMap() Start");
		VisionPlanDataVO visionPlanDataVO = visionPlansMap.get(mapKey);

		if (null == visionPlanDataVO) {
			visionPlanDataVO = new VisionPlanDataVO();
			visionPlanDataVO.setPlanId(mapKey);
			visionPlanDataVO.setState(stateCode);
			visionPlanDataVO.setCarrierHIOSId(hiosId);
			visionPlanDataVO.setCarrierName(getCellValueTrim(excelRows[VisionPlanDataRowEnum.CARRIER_NAME.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setPlanName(getCellValueTrim(excelRows[VisionPlanDataRowEnum.PLAN_NAME.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setStartDate(getCellValueTrim(excelRows[VisionPlanDataRowEnum.START_DATE.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setEyeExamCopayVal(getCellValueTrim(excelRows[VisionPlanDataRowEnum.EYE_EXAM_COPAY_VAL.getRowIndex()].getCell(colIx), true));
			visionPlanDataVO.setEyeExamCopayAttr(getCellValueTrim(excelRows[VisionPlanDataRowEnum.EYE_EXAM_COPAY_ATTR.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setEyeExamDetails(getCellValueTrim(excelRows[VisionPlanDataRowEnum.EYE_EXAM_DETAILS.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setGlassesCopayVal(getCellValueTrim(excelRows[VisionPlanDataRowEnum.GLASSES_COPAY_VAL.getRowIndex()].getCell(colIx), true));
			visionPlanDataVO.setGlassesCopayAttr(getCellValueTrim(excelRows[VisionPlanDataRowEnum.GLASSES_COPAY_ATTR.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setGlassesDetails(getCellValueTrim(excelRows[VisionPlanDataRowEnum.GLASSES_DETAILS.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setFramesCopayVal(getCellValueTrim(excelRows[VisionPlanDataRowEnum.FRAMES_COPAY_VAL.getRowIndex()].getCell(colIx), true));
			visionPlanDataVO.setFramesCopayAttr(getCellValueTrim(excelRows[VisionPlanDataRowEnum.FRAMES_COPAY_ATTR.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setFramesDetails(getCellValueTrim(excelRows[VisionPlanDataRowEnum.FRAMES_DETAILS.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setContactsCopayVal(getCellValueTrim(excelRows[VisionPlanDataRowEnum.CONTACTS_COPAY_VAL.getRowIndex()].getCell(colIx), true));
			visionPlanDataVO.setContactsCopayAttr(getCellValueTrim(excelRows[VisionPlanDataRowEnum.CONTACTS_COPAY_ATTR.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setContactsDetails(getCellValueTrim(excelRows[VisionPlanDataRowEnum.CONTACTS_DETAILS.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setLaserVisionCorrectionDetails(getCellValueTrim(excelRows[VisionPlanDataRowEnum.LASER_VISION_CORRECTION_DETAILS.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setSunglassesandExtraGlassesDetails(getCellValueTrim(excelRows[VisionPlanDataRowEnum.SUNGLASSESAND_EXTRA_GLASSES_DETAILS.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setOutOfNetworkAvailability(getCellValueTrim(excelRows[VisionPlanDataRowEnum.OUT_OF_NETWORK_AVAILABILITY.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setOutOfNetworkCoverage(getCellValueTrim(excelRows[VisionPlanDataRowEnum.OUT_OF_NETWORK_COVERAGE.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setProviderNetworkURL(getCellValueTrim(excelRows[VisionPlanDataRowEnum.PROVIDER_NETWORK_URL.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setPlanBrochureURL(getCellValueTrim(excelRows[VisionPlanDataRowEnum.PLAN_BROCHURE_URL.getRowIndex()].getCell(colIx)));
			visionPlanDataVO.setPremiumPayment(getCellValueTrim(excelRows[VisionPlanDataRowEnum.PREMIUM_PAYMENT.getRowIndex()].getCell(colIx)));
			visionPlansMap.put(mapKey, visionPlanDataVO);
		}
		else {
			LOGGER.error(EMSG_VISION + EMSG_DUPLICATE_PLAN + mapKey);
		}
		LOGGER.debug("addVisionPlanDataToMap() End");
	}

	/**
	 * Extracts Rates data from excel data rows.
	 * 
	 * @param excelRows  -Excel data rows
	 * @param colIx - Column number
	 * @return - Rates VO populated by excel row data
	 */
	private VisionPlanRatesVO getVisionPlanRatesVO(Row[] excelRows, int colIx) {

		LOGGER.debug("getVisionPlanRatesVO() Start");
		VisionPlanRatesVO visionPlanRatesVO = null;

		if(null != excelRows) {
			visionPlanRatesVO = new VisionPlanRatesVO();
			visionPlanRatesVO.setPlanID(getCellValueTrim(excelRows[VisionPlanRatesRowEnum.PLAN_ID.getRowIndex()].getCell(colIx)));
			visionPlanRatesVO.setCouple(getCellValueTrim(excelRows[VisionPlanRatesRowEnum.COUPLE.getRowIndex()].getCell(colIx)));
			visionPlanRatesVO.setOneAdultOneKid(getCellValueTrim(excelRows[VisionPlanRatesRowEnum.ONE_ADULT_ONE_KID.getRowIndex()].getCell(colIx)));
			visionPlanRatesVO.setOneAdultTwoKid(getCellValueTrim(excelRows[VisionPlanRatesRowEnum.ONE_ADULT_TWO_KID.getRowIndex()].getCell(colIx)));
			visionPlanRatesVO.setOneAdultThreePlusKid(getCellValueTrim(excelRows[VisionPlanRatesRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowIndex()].getCell(colIx)));
			visionPlanRatesVO.setTwoAdultOneKid(getCellValueTrim(excelRows[VisionPlanRatesRowEnum.TWO_ADULT_ONE_KID.getRowIndex()].getCell(colIx)));
			visionPlanRatesVO.setTwoAdultTwoKid(getCellValueTrim(excelRows[VisionPlanRatesRowEnum.TWO_ADULT_TWO_KID.getRowIndex()].getCell(colIx)));
			visionPlanRatesVO.setTwoAdultThreePlusKid(getCellValueTrim(excelRows[VisionPlanRatesRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowIndex()].getCell(colIx)));
			visionPlanRatesVO.setIndividual(getCellValueTrim(excelRows[VisionPlanRatesRowEnum.INDIVIDUAL.getRowIndex()].getCell(colIx)));
		}
		LOGGER.debug("getVisionPlanRatesVO() End");
		return visionPlanRatesVO;
	}

	/**
	 * Reads data from excel sheet.
	 * 
	 * @param visionPlanSheet - Excel sheet having Plan data
	 * @return - Array of data rows
	 */
	private Row[] getVisionPlanDataRows(XSSFSheet visionPlanSheet) {
		LOGGER.debug("getVisionPlanDataRows() Start");
		Row[] planDataRows = new Row[PLANDATA_ROW_COUNT];
		planDataRows[VisionPlanDataRowEnum.STATE.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.STATE.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.CARRIER_HIOS_ID.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.CARRIER_HIOS_ID.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.PLAN_ID.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.PLAN_ID.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.CARRIER_NAME.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.CARRIER_NAME.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.PLAN_NAME.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.PLAN_NAME.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.START_DATE.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.START_DATE.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.EYE_EXAM_COPAY_VAL.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.EYE_EXAM_COPAY_VAL.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.EYE_EXAM_COPAY_ATTR.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.EYE_EXAM_COPAY_ATTR.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.EYE_EXAM_DETAILS.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.EYE_EXAM_DETAILS.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.GLASSES_COPAY_VAL.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.GLASSES_COPAY_VAL.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.GLASSES_COPAY_ATTR.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.GLASSES_COPAY_ATTR.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.GLASSES_DETAILS.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.GLASSES_DETAILS.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.FRAMES_COPAY_VAL.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.FRAMES_COPAY_VAL.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.FRAMES_COPAY_ATTR.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.FRAMES_COPAY_ATTR.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.FRAMES_DETAILS.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.FRAMES_DETAILS.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.CONTACTS_COPAY_VAL.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.CONTACTS_COPAY_VAL.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.CONTACTS_COPAY_ATTR.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.CONTACTS_COPAY_ATTR.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.CONTACTS_DETAILS.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.CONTACTS_DETAILS.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.LASER_VISION_CORRECTION_DETAILS.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.LASER_VISION_CORRECTION_DETAILS.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.SUNGLASSESAND_EXTRA_GLASSES_DETAILS.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.SUNGLASSESAND_EXTRA_GLASSES_DETAILS.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.OUT_OF_NETWORK_AVAILABILITY.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.OUT_OF_NETWORK_AVAILABILITY.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.OUT_OF_NETWORK_COVERAGE.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.OUT_OF_NETWORK_COVERAGE.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.PREMIUM_PAYMENT.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.PREMIUM_PAYMENT.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.PROVIDER_NETWORK_URL.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.PROVIDER_NETWORK_URL.getRowIndex());
		planDataRows[VisionPlanDataRowEnum.PLAN_BROCHURE_URL.getRowIndex()] = visionPlanSheet.getRow(VisionPlanDataRowEnum.PLAN_BROCHURE_URL.getRowIndex());
		LOGGER.debug("getVisionPlanDataRows() End");
		return planDataRows;
	}

	/**
	 * Reads data from excel sheet.
	 * 
	 * @param visionPlanSheet - Rates data sheet
	 * @return - Rates data
	 */
	private Row[] getVisionPlanRatesRows(XSSFSheet visionPlanSheet) {
		LOGGER.debug("getVisionPlanRatesRows() Start");
		Row[] planDataRows = new Row[PLANRATE_ROW_COUNT];
		planDataRows[VisionPlanRatesRowEnum.STATE.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.STATE.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.CARRIER_HIOS_ID.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.CARRIER_HIOS_ID.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.PLAN_ID.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.PLAN_ID.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.CARRIER_NAME.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.CARRIER_NAME.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.PLAN_NAME.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.PLAN_NAME.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.COUPLE.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.COUPLE.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.ONE_ADULT_ONE_KID.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.ONE_ADULT_ONE_KID.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.ONE_ADULT_TWO_KID.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.ONE_ADULT_TWO_KID.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.TWO_ADULT_ONE_KID.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.TWO_ADULT_ONE_KID.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.TWO_ADULT_TWO_KID.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.TWO_ADULT_TWO_KID.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowIndex());
		planDataRows[VisionPlanRatesRowEnum.INDIVIDUAL.getRowIndex()] = visionPlanSheet.getRow(VisionPlanRatesRowEnum.INDIVIDUAL.getRowIndex());
		LOGGER.debug("getVisionPlanRatesRows() End");
		return planDataRows;
	}

	/**
	 * Method is used to validated Excel Sheets.
	 * 
	 * @return - Returns whether sheet is valid
	 */
	private boolean isValidExcelTemplate() {

		LOGGER.debug("isValidExcelTemplate() Start");
		boolean isValid = true;

		try {
			isValid = isValidSheet(SHEET_INDEX_PLAN_DATA, SHEET_PLAN_DATA);
			isValid = isValid && isValidSheet(SHEET_INDEX_PLAN_RATES, SHEET_PLAN_RATES);
		}
		finally {
			LOGGER.debug("isValidExcelTemplate() End");
		}
		return isValid;
	}

	
	/**
	 * Method is used to update tracking record of SERFF Plan Management table.
	 * 
	 * @param isSuccess - Success flag
	 * @param trackingRecord - Serff Plan Mgmt Record
	 * @param message - Process message
	 */
	private void updateTrackingRecord(boolean isSuccess, SerffPlanMgmt trackingRecord, StringBuilder message) {

		LOGGER.debug("updateTrackingRecord() Start");
		try {
			if (null != trackingRecord) {

				if (isSuccess) {
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
				}
				else {
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
				}

				if (StringUtils.isNotBlank(message.toString())) {
					trackingRecord.setRequestStateDesc(message.toString());
				}

				if (trackingRecord.getRequestStateDesc().length() > ERROR_MAX_LEN) {
					trackingRecord.setRequestStateDesc(trackingRecord.getRequestStateDesc().substring(0, ERROR_MAX_LEN));
				}

				if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
					trackingRecord.setPmResponseXml(trackingRecord.getRequestStateDesc());
				}
				trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
				trackingRecord.setEndTime(new Date());
				serffService.saveSerffPlanMgmt(trackingRecord);
			}
			else {
				message.append("Tracking Record is not found.");
			}
		}catch(Exception ex){
			LOGGER.error("Error occurred in updateTrackingRecord() ", ex);
		}
		finally {
			LOGGER.debug("updateTrackingRecord() End");
		}
	}
	
	/**
	 * Method is used to store Vision Excel Header sheet data in AMEExcelVO Object.
	 * 
	 * @return Plan Vision VO
	 */
	private VisionPlanDataVO getVisionExcelVOHeader() {

		LOGGER.debug("getVisionExcelVOHeader() Start");
		VisionPlanDataVO visionHeaderVO = new VisionPlanDataVO();
		visionHeaderVO.setState(VisionPlanDataRowEnum.STATE.getRowConstName());
		visionHeaderVO.setCarrierHIOSId(VisionPlanDataRowEnum.CARRIER_HIOS_ID.getRowConstName());
		visionHeaderVO.setPlanId(VisionPlanDataRowEnum.PLAN_ID.getRowConstName());
		visionHeaderVO.setCarrierName(VisionPlanDataRowEnum.CARRIER_NAME.getRowConstName());
		visionHeaderVO.setPlanName(VisionPlanDataRowEnum.PLAN_NAME.getRowConstName());
		visionHeaderVO.setStartDate(VisionPlanDataRowEnum.START_DATE.getRowConstName());
		visionHeaderVO.setEyeExamCopayVal(VisionPlanDataRowEnum.EYE_EXAM_COPAY_VAL.getRowConstName());
		visionHeaderVO.setEyeExamCopayAttr(VisionPlanDataRowEnum.EYE_EXAM_COPAY_ATTR.getRowConstName());
		visionHeaderVO.setEyeExamDetails(VisionPlanDataRowEnum.EYE_EXAM_DETAILS.getRowConstName());
		visionHeaderVO.setGlassesCopayVal(VisionPlanDataRowEnum.GLASSES_COPAY_VAL.getRowConstName());
		visionHeaderVO.setGlassesCopayAttr(VisionPlanDataRowEnum.GLASSES_COPAY_ATTR.getRowConstName());
		visionHeaderVO.setGlassesDetails(VisionPlanDataRowEnum.GLASSES_DETAILS.getRowConstName());
		visionHeaderVO.setFramesCopayVal(VisionPlanDataRowEnum.FRAMES_COPAY_VAL.getRowConstName());
		visionHeaderVO.setFramesCopayAttr(VisionPlanDataRowEnum.FRAMES_COPAY_ATTR.getRowConstName());
		visionHeaderVO.setFramesDetails(VisionPlanDataRowEnum.FRAMES_DETAILS.getRowConstName());
		visionHeaderVO.setContactsCopayVal(VisionPlanDataRowEnum.CONTACTS_COPAY_VAL.getRowConstName());
		visionHeaderVO.setContactsCopayAttr(VisionPlanDataRowEnum.CONTACTS_COPAY_ATTR.getRowConstName());
		visionHeaderVO.setContactsDetails(VisionPlanDataRowEnum.CONTACTS_DETAILS.getRowConstName());
		visionHeaderVO.setLaserVisionCorrectionDetails(VisionPlanDataRowEnum.LASER_VISION_CORRECTION_DETAILS.getRowConstName());
		visionHeaderVO.setSunglassesandExtraGlassesDetails(VisionPlanDataRowEnum.SUNGLASSESAND_EXTRA_GLASSES_DETAILS.getRowConstName());
		visionHeaderVO.setOutOfNetworkAvailability(VisionPlanDataRowEnum.OUT_OF_NETWORK_AVAILABILITY.getRowConstName());
		visionHeaderVO.setOutOfNetworkCoverage(VisionPlanDataRowEnum.OUT_OF_NETWORK_COVERAGE.getRowConstName());
		visionHeaderVO.setPremiumPayment(VisionPlanDataRowEnum.PREMIUM_PAYMENT.getRowConstName());
		visionHeaderVO.setProviderNetworkURL(VisionPlanDataRowEnum.PROVIDER_NETWORK_URL.getRowConstName());
		visionHeaderVO.setPlanBrochureURL(VisionPlanDataRowEnum.PLAN_BROCHURE_URL.getRowConstName());

		visionHeaderVO.setVisionPlanRatesVO(new VisionPlanRatesVO());
		visionHeaderVO.getVisionPlanRatesVO().setPlanID(VisionPlanRatesRowEnum.PLAN_ID.getRowConstName());
		visionHeaderVO.getVisionPlanRatesVO().setCouple(VisionPlanRatesRowEnum.COUPLE.getRowConstName());
		visionHeaderVO.getVisionPlanRatesVO().setOneAdultOneKid(VisionPlanRatesRowEnum.ONE_ADULT_ONE_KID.getRowConstName());
		visionHeaderVO.getVisionPlanRatesVO().setOneAdultTwoKid(VisionPlanRatesRowEnum.ONE_ADULT_TWO_KID.getRowConstName());
		visionHeaderVO.getVisionPlanRatesVO().setOneAdultThreePlusKid(VisionPlanRatesRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowConstName());
		visionHeaderVO.getVisionPlanRatesVO().setTwoAdultOneKid(VisionPlanRatesRowEnum.TWO_ADULT_ONE_KID.getRowConstName());
		visionHeaderVO.getVisionPlanRatesVO().setTwoAdultTwoKid(VisionPlanRatesRowEnum.TWO_ADULT_TWO_KID.getRowConstName());
		visionHeaderVO.getVisionPlanRatesVO().setTwoAdultThreePlusKid(VisionPlanRatesRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowConstName());
		visionHeaderVO.getVisionPlanRatesVO().setIndividual(VisionPlanRatesRowEnum.INDIVIDUAL.getRowConstName());
		LOGGER.debug("getVisionExcelVOHeader() End");
		
		return visionHeaderVO;
	}
	
	
	/**
	 * Method is used to remove duplicate States from STMExcelVO.getPlanState().
	 * 
	 * @param planStateArray - All states in array.
	 * @return - Unique state names list in array.
	 */
	public String[] removeDuplicateStates(String[] planStateArray) {
		
		LOGGER.debug("removeDuplicateStates() Start");
		String[] uniqueStateArray = null;
		
		if (null != planStateArray && 0 < planStateArray.length) {
			
			Set<String> stateList = new HashSet<String>();
			
			for (String value : planStateArray) {
				stateList.add(value.trim());
		    }
			uniqueStateArray = stateList.toArray(new String[]{});
			LOGGER.info("uniqueStateArray size: " + uniqueStateArray.length);
		}
		LOGGER.debug("removeDuplicateStates() End");
		
		return uniqueStateArray;
	}
	
	
}
