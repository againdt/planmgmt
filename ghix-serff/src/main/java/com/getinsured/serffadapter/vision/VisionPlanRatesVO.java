package com.getinsured.serffadapter.vision;

/**
 * Class is used to generate getter/setter on the basis of Plan Rates of Vision Excel.
 * 
 * @author Bhavin Parmar
 * @since January 27, 2015
 */
public class VisionPlanRatesVO {

	private String planID;
	private String couple;
	private String oneAdultOneKid;
	private String oneAdultTwoKid;
	private String oneAdultThreePlusKid;
	private String twoAdultOneKid;
	private String twoAdultTwoKid;
	private String twoAdultThreePlusKid;
	private String individual;

	public VisionPlanRatesVO() {
	}

	public String getPlanID() {
		return planID;
	}

	public void setPlanID(String planID) {
		this.planID = planID;
	}

	public String getCouple() {
		return couple;
	}

	public void setCouple(String couple) {
		this.couple = couple;
	}

	public String getOneAdultOneKid() {
		return oneAdultOneKid;
	}

	public void setOneAdultOneKid(String oneAdultOneKid) {
		this.oneAdultOneKid = oneAdultOneKid;
	}

	public String getOneAdultTwoKid() {
		return oneAdultTwoKid;
	}

	public void setOneAdultTwoKid(String oneAdultTwoKid) {
		this.oneAdultTwoKid = oneAdultTwoKid;
	}

	public String getOneAdultThreePlusKid() {
		return oneAdultThreePlusKid;
	}

	public void setOneAdultThreePlusKid(String oneAdultThreePlusKid) {
		this.oneAdultThreePlusKid = oneAdultThreePlusKid;
	}

	public String getTwoAdultOneKid() {
		return twoAdultOneKid;
	}

	public void setTwoAdultOneKid(String twoAdultOneKid) {
		this.twoAdultOneKid = twoAdultOneKid;
	}

	public String getTwoAdultTwoKid() {
		return twoAdultTwoKid;
	}

	public void setTwoAdultTwoKid(String twoAdultTwoKid) {
		this.twoAdultTwoKid = twoAdultTwoKid;
	}

	public String getTwoAdultThreePlusKid() {
		return twoAdultThreePlusKid;
	}

	public void setTwoAdultThreePlusKid(String twoAdultThreePlusKid) {
		this.twoAdultThreePlusKid = twoAdultThreePlusKid;
	}

	public String getIndividual() {
		return individual;
	}

	public void setIndividual(String individual) {
		this.individual = individual;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("VisionPlanRatesVO [PlanID=");
		sb.append(planID);
		sb.append(", Couple=");
		sb.append(couple);
		sb.append(", oneAdultOneKid=");
		sb.append(oneAdultOneKid);
		sb.append(", oneAdultTwoKid=");
		sb.append(oneAdultTwoKid);
		sb.append(", oneAdultThreePlusKid=");
		sb.append(oneAdultThreePlusKid);
		sb.append(", twoAdultOneKid=");
		sb.append(twoAdultOneKid);
		sb.append(", twoAdultTwoKid=");
		sb.append(twoAdultTwoKid);
		sb.append(", twoAdultThreePlusKid=");
		sb.append(twoAdultThreePlusKid);
		sb.append(", individual=");
		sb.append(individual);
		sb.append( "]");
		return sb.toString();
	}
}
