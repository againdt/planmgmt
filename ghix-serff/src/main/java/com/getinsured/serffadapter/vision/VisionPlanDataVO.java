package com.getinsured.serffadapter.vision;

/**
 * Class is used to generate getter/setter on the basis of Plan Data of Vision Excel.
 * 
 * @author Bhavin Parmar
 * @since January 27, 2015
 */
public class VisionPlanDataVO {

	private String state;
	private String carrierHIOSId;
	private String planId;
	private String carrierName;
	private String planName;
	private String startDate;
	private String eyeExamCopayVal;
	private String eyeExamCopayAttr;
	private String eyeExamDetails;
	private String glassesCopayVal;
	private String glassesCopayAttr;
	private String glassesDetails;
	private String framesCopayVal;
	private String framesCopayAttr;
	private String framesDetails;
	private String contactsCopayVal;
	private String contactsCopayAttr;
	private String contactsDetails;
	private String laserVisionCorrectionDetails;
	private String sunglassesandExtraGlassesDetails;
	private String outOfNetworkAvailability;
	private String outOfNetworkCoverage;
	private String providerNetworkURL;
	private String planBrochureURL;
	private String premiumPayment;

	private VisionPlanRatesVO visionPlanRatesVO;
	private boolean isNotValid;
	private String errorMessages;

	public VisionPlanDataVO() {
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCarrierHIOSId() {
		return carrierHIOSId;
	}

	public void setCarrierHIOSId(String carrierHIOSId) {
		this.carrierHIOSId = carrierHIOSId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEyeExamCopayVal() {
		return eyeExamCopayVal;
	}

	public void setEyeExamCopayVal(String eyeExamCopayVal) {
		this.eyeExamCopayVal = eyeExamCopayVal;
	}

	public String getEyeExamCopayAttr() {
		return eyeExamCopayAttr;
	}

	public void setEyeExamCopayAttr(String eyeExamCopayAttr) {
		this.eyeExamCopayAttr = eyeExamCopayAttr;
	}

	public String getEyeExamDetails() {
		return eyeExamDetails;
	}

	public void setEyeExamDetails(String eyeExamDetails) {
		this.eyeExamDetails = eyeExamDetails;
	}

	public String getGlassesCopayVal() {
		return glassesCopayVal;
	}

	public void setGlassesCopayVal(String glassesCopayVal) {
		this.glassesCopayVal = glassesCopayVal;
	}

	public String getGlassesCopayAttr() {
		return glassesCopayAttr;
	}

	public void setGlassesCopayAttr(String glassesCopayAttr) {
		this.glassesCopayAttr = glassesCopayAttr;
	}

	public String getGlassesDetails() {
		return glassesDetails;
	}

	public void setGlassesDetails(String glassesDetails) {
		this.glassesDetails = glassesDetails;
	}

	public String getFramesCopayVal() {
		return framesCopayVal;
	}

	public void setFramesCopayVal(String framesCopayVal) {
		this.framesCopayVal = framesCopayVal;
	}

	public String getFramesCopayAttr() {
		return framesCopayAttr;
	}

	public void setFramesCopayAttr(String framesCopayAttr) {
		this.framesCopayAttr = framesCopayAttr;
	}

	public String getFramesDetails() {
		return framesDetails;
	}

	public void setFramesDetails(String framesDetails) {
		this.framesDetails = framesDetails;
	}

	public String getContactsCopayVal() {
		return contactsCopayVal;
	}

	public void setContactsCopayVal(String contactsCopayVal) {
		this.contactsCopayVal = contactsCopayVal;
	}

	public String getContactsCopayAttr() {
		return contactsCopayAttr;
	}

	public void setContactsCopayAttr(String contactsCopayAttr) {
		this.contactsCopayAttr = contactsCopayAttr;
	}

	public String getContactsDetails() {
		return contactsDetails;
	}

	public void setContactsDetails(String contactsDetails) {
		this.contactsDetails = contactsDetails;
	}

	public String getLaserVisionCorrectionDetails() {
		return laserVisionCorrectionDetails;
	}

	public void setLaserVisionCorrectionDetails(String laserVisionCorrectionDetails) {
		this.laserVisionCorrectionDetails = laserVisionCorrectionDetails;
	}

	public String getSunglassesandExtraGlassesDetails() {
		return sunglassesandExtraGlassesDetails;
	}

	public void setSunglassesandExtraGlassesDetails(String sunglassesandExtraGlassesDetails) {
		this.sunglassesandExtraGlassesDetails = sunglassesandExtraGlassesDetails;
	}

	public String getOutOfNetworkAvailability() {
		return outOfNetworkAvailability;
	}

	public void setOutOfNetworkAvailability(String outOfNetworkAvailability) {
		this.outOfNetworkAvailability = outOfNetworkAvailability;
	}

	public String getOutOfNetworkCoverage() {
		return outOfNetworkCoverage;
	}

	public void setOutOfNetworkCoverage(String outOfNetworkCoverage) {
		this.outOfNetworkCoverage = outOfNetworkCoverage;
	}

	public String getProviderNetworkURL() {
		return providerNetworkURL;
	}

	public void setProviderNetworkURL(String providerNetworkURL) {
		this.providerNetworkURL = providerNetworkURL;
	}

	public String getPlanBrochureURL() {
		return planBrochureURL;
	}

	public void setPlanBrochureURL(String planBrochureURL) {
		this.planBrochureURL = planBrochureURL;
	}

	public String getPremiumPayment() {
		return premiumPayment;
	}

	public void setPremiumPayment(String premiumPayment) {
		this.premiumPayment = premiumPayment;
	}

	public VisionPlanRatesVO getVisionPlanRatesVO() {
		return visionPlanRatesVO;
	}

	public void setVisionPlanRatesVO(VisionPlanRatesVO visionPlanRatesVO) {
		this.visionPlanRatesVO = visionPlanRatesVO;
	}

	public boolean isNotValid() {
		return isNotValid;
	}

	public void setNotValid(boolean isNotValid) {
		this.isNotValid = isNotValid;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("VisionPlanDataVO [state=");
		sb.append(state);
		sb.append(", carrierHIOSId=");
		sb.append(carrierHIOSId);
		sb.append(", planId=");
		sb.append(planId);
		sb.append(", carrierName=");
		sb.append(carrierName);
		sb.append(", planName=");
		sb.append(planName);
		sb.append(", startDate=");
		sb.append(startDate);
		sb.append(", eyeExamCopayVal=");
		sb.append(eyeExamCopayVal);
		sb.append(", eyeExamCopayAttr=");
		sb.append(eyeExamCopayAttr);
		sb.append(", eyeExamDetails=");
		sb.append(eyeExamDetails);
		sb.append(", glassesCopayVal=");
		sb.append(glassesCopayVal);
		sb.append(", glassesCopayAttr=");
		sb.append(glassesCopayAttr);
		sb.append(", glassesDetails=");
		sb.append(glassesDetails);
		sb.append(", framesCopayVal=");
		sb.append(framesCopayVal);
		sb.append(", framesCopayAttr=");
		sb.append(framesCopayAttr);
		sb.append(", framesDetails=");
		sb.append(framesDetails);
		sb.append(", contactsCopayVal=");
		sb.append(contactsCopayVal);
		sb.append(", contactsCopayAttr=");
		sb.append(contactsCopayAttr);
		sb.append(", contactsDetails=");
		sb.append(contactsDetails);
		sb.append(", laserVisionCorrectionDetails=");
		sb.append(laserVisionCorrectionDetails);
		sb.append(", sunglassesandExtraGlassesDetails=");
		sb.append(sunglassesandExtraGlassesDetails);
		sb.append(", outOfNetworkAvailability=");
		sb.append(outOfNetworkAvailability);
		sb.append(", outOfNetworkCoverage=");
		sb.append(outOfNetworkCoverage);
		sb.append(", providerNetworkURL=");
		sb.append(providerNetworkURL);
		sb.append(", planBrochureURL=");
		sb.append(planBrochureURL);
		sb.append(", premiumPayment=");
		sb.append(premiumPayment);
		sb.append( "]");
		return sb.toString();
	}
}
