package com.getinsured.serffadapter.vision;

/**
 *-----------------------------------------------------------------------------
 * HIX-59162 PHIX - Load and persist vision plans.
 *-----------------------------------------------------------------------------
 * 
 * Class is used to generate Enum on the basis of Vision Plan Data
 * and Vision Plan Rates sheets of Vision Plan Excel.
 * 
 * @author Bhavin Parmar
 * @since January 27, 2015
 */
public class VisionExcelRow {

	private static final int NUMBER_TYPE = 0;
	private static final int FLOAT_TYPE = 1;
	private static final int STRING_TYPE = 2;
	private static final int DATE_TYPE = 3;

	public static final String SHEET_PLAN_DATA = "Vision plan data";
	public static final String SHEET_PLAN_RATES = "Vision plan rates";
	public static final int SHEET_INDEX_PLAN_DATA = 1;
	public static final int SHEET_INDEX_PLAN_RATES = 2;

	public static final int PLAN_DATA_START_COLUMN = 2;
	public static final int RATE_DATA_START_COLUMN = 2;
	public static final int PLANDATA_ROW_COUNT = 28;
	public static final int PLANRATE_ROW_COUNT = 14;

	public enum VisionPlanDataRowEnum {

		STATE("State", "STATE", 1, STRING_TYPE),
		CARRIER_HIOS_ID("Carrier HIOS ID", "CARRIER_HIOS_ID", 2, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 3, STRING_TYPE),
		CARRIER_NAME("Carrier name", "CARRIER_NAME", 4, STRING_TYPE),
		PLAN_NAME("Plan name", "PLAN_NAME", 5, STRING_TYPE),
		START_DATE("Start date", "START_DATE", 6, DATE_TYPE),
		EYE_EXAM_COPAY_VAL("Eye Exam (Copay Value)", "EYE_EXAM", 8, NUMBER_TYPE), // Benefit
		EYE_EXAM_COPAY_ATTR("Eye Exam (Copay Attribute)", "EYE_EXAM", 9, STRING_TYPE), // Benefit
		EYE_EXAM_DETAILS("Eye Exam (Benefit details)", "EYE_EXAM", 10, STRING_TYPE), // Benefit
		GLASSES_COPAY_VAL("Glasses (Copay Value)", "GLASSES", 11, NUMBER_TYPE), // Benefit
		GLASSES_COPAY_ATTR("Glasses (Copay Attribute)", "GLASSES", 12, STRING_TYPE), // Benefit
		GLASSES_DETAILS("Glasses (Benefit details)", "GLASSES", 13, STRING_TYPE), // Benefit
		FRAMES_COPAY_VAL("Frames (Copay Value)", "FRAMES", 14, NUMBER_TYPE), // Benefit
		FRAMES_COPAY_ATTR("Frames (Copay Attribute)", "FRAMES", 15, STRING_TYPE), // Benefit
		FRAMES_DETAILS("Frames (Benefit details)", "FRAMES", 16, STRING_TYPE), // Benefit
		CONTACTS_COPAY_VAL("Contacts (Copay Value)", "CONTACT_LENS", 17, NUMBER_TYPE), // Benefit
		CONTACTS_COPAY_ATTR("Contacts (Copay Attribute)", "CONTACT_LENS", 18, STRING_TYPE), // Benefit
		CONTACTS_DETAILS("Contacts (Benefit details)", "CONTACT_LENS", 19, STRING_TYPE), // Benefit
		LASER_VISION_CORRECTION_DETAILS("Laser Vision Correction (Benefit details)", "LASER_VISION_CORRECTION", 20, STRING_TYPE), // Benefit
		SUNGLASSESAND_EXTRA_GLASSES_DETAILS("Sunglasses and Extra Glasses (Benefit details)", "SUNGLASSES_AND_EXTRA_GLASSES", 21, STRING_TYPE), // Benefit
		OUT_OF_NETWORK_AVAILABILITY("Out-of-Network Availability", "OUT_OF_NETWORK_AVAILABILITY", 23, STRING_TYPE),
		OUT_OF_NETWORK_COVERAGE("Out-of-Network Coverage", "OUT_OF_NETWORK_COVERAGE", 24, STRING_TYPE),
		PREMIUM_PAYMENT("PremiumPayment", "PREMIUM_PAYMENT", 25, STRING_TYPE),
		PROVIDER_NETWORK_URL("Provider Network (URL)", "PROVIDER_NETWORK_URL", 26, STRING_TYPE),
		PLAN_BROCHURE_URL("Plan Brochure (URL)", "PLAN_BROCHURE_URL", 27, STRING_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private VisionPlanDataRowEnum(String name, String columnConstName, int index, int dataType) {
			this.columnName = name;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getRowName() {
			return columnName;
		}

		public String getRowConstName() {
			return columnConstName;
		}

		public int getRowIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum VisionPlanRatesRowEnum {

		STATE("State", "STATE", 1, STRING_TYPE),
		CARRIER_HIOS_ID("Carrier HIOS ID", "CARRIER_HIOS_ID", 2, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 3, STRING_TYPE),
		CARRIER_NAME("Carrier name", "CARRIER_NAME", 4, STRING_TYPE),
		PLAN_NAME("Plan name", "PLAN_NAME", 5, STRING_TYPE),
		COUPLE("couple", "COUPLE", 6, FLOAT_TYPE),
		ONE_ADULT_ONE_KID("oneAdultOnekid", "ONE_ADULT_ONE_KID", 7, FLOAT_TYPE),
		ONE_ADULT_TWO_KID("oneAdultTwokid", "ONE_ADULT_TWO_KID", 8, FLOAT_TYPE),
		ONE_ADULT_THREE_PLUS_KID("oneAdultThreePluskid", "ONE_ADULT_THREE_PLUS_KID", 9, FLOAT_TYPE),
		TWO_ADULT_ONE_KID("twoAdultOnekid", "TWO_ADULT_ONE_KID", 10, FLOAT_TYPE),
		TWO_ADULT_TWO_KID("twoAdultTwokid", "TWO_ADULT_TWO_KID", 11, FLOAT_TYPE),
		TWO_ADULT_THREE_PLUS_KID("twoAdultThreePluskid", "TWO_ADULT_THREE_PLUS_KID", 12, FLOAT_TYPE),
		INDIVIDUAL("individual", "INDIVIDUAL", 13, FLOAT_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private VisionPlanRatesRowEnum(String name, String columnConstName, int index, int dataType) {
			this.columnName = name;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getRowName() {
			return columnName;
		}

		public String getRowConstName() {
			return columnConstName;
		}

		public int getRowIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}
}
