package com.getinsured.serffadapter;

import java.util.List;

import com.getinsured.serffadapter.vo.BenefitVO;
import com.getinsured.serffadapter.vo.CostShareVarianceVO;
import com.getinsured.serffadapter.vo.IssuerVO;
import com.getinsured.serffadapter.vo.NetworkVO;
import com.getinsured.serffadapter.vo.PlanAttributeVO;
import com.getinsured.serffadapter.vo.PlanRateVO;
import com.getinsured.serffadapter.vo.ServiceAreaVO;

public interface CarrierTemplateAdapter {
	
	String DEFAULT_NETWORK_SUFFIX = "N001";
	String DEFAULT_SERVICEAREA_SUFFIX = "S001";
	String DEFAULT_FORMULARY_SUFFIX = "F001";
	
	List<NetworkVO> getNetworkList(IssuerVO issuerVO);
	List<IssuerVO> getIssuerList();
	List<ServiceAreaVO> getServiceAreaList(String issuerHIOSId, String issuerState);
	List<PlanRateVO> getPlanRatesList(String issuerHIOSId, String planId);
	List<String> getPlanIdList(String issuerHIOSId);
	List<BenefitVO> getPlanBenefitsList(String issuerHIOSId, String planVariantId);
	PlanAttributeVO getPlanAttributes(String issuerHIOSId, String issuerState, String planId);
	List<CostShareVarianceVO> getCostShareVarianceForPlan(String issuerHIOSId, String planId);
	boolean loadTemplateData(String sourcePath);
}
