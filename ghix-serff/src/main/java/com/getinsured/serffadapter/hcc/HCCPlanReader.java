/**
 * This class is used to process HCC Excel templates data
 */
package com.getinsured.serffadapter.hcc;

import static com.serff.util.SerffConstants.FTP_AREA_FACTOR_FILE;
import static com.serff.util.SerffConstants.FTP_HCC_FILE;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.STMHCCPlansAdminFees;
import com.getinsured.hix.model.STMHCCPlansAreaFactor;
import com.getinsured.hix.model.STMHCCPlansRate;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.serffadapter.excel.ExcelReader;
import com.getinsured.serffadapter.hcc.HCCAdminFeesExcelColumn.HCCAdminFeesExcelColumnEnum;
import com.getinsured.serffadapter.hcc.HCCAreaFactorExcelColumn.HCCAreaFactorExcelColumnEnum;
import com.getinsured.serffadapter.hcc.HCCAreaFactorExcelColumn.HCCAreaFactorExcelRowEnum;
import com.getinsured.serffadapter.hcc.HCCRatesExcelColumn.HCCRatesExcelColumnEnum;
import com.getinsured.serffadapter.stm.STMExcelColumn.STMExcelColumnEnum;
import com.getinsured.serffadapter.stm.STMPlanReader;
import com.serff.repository.templates.IstmHCCAreaFactorRepository;
import com.serff.repository.templates.IstmHccRepository;
import com.serff.service.SerffService;
import com.serff.service.validation.HCCSTMRateValidator;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

/**
 * @author sharma_va
 */
@Service("hCCPlanReader")
public class HCCPlanReader extends ExcelReader {
	
	private static final Logger LOGGER = Logger.getLogger(HCCPlanReader.class);
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	private static final String FOREVER_END_DATE = "31/12/2099";
	private static final String MSG_ERROR_HCC_PLANS = "Failed to load HCC Plans. Reason: ";
	private static final String MSG_HCC_PLANS_SUCCESS = "HCC Plans has been Loaded Successfully.";
	private static final String MSG_HCC_PLANS_INVALID = "HCC upload failed as data is invalid ";
	private static final String MSG_FILE = "File: ";
	private static final int PLAN_SHEET_INDEX = 0;
	private static final int RATE_SHEET_INDEX = 1;
	private static final int AREA_FACTOR_SHEET_INDEX= 1;
	private static final int ADMIN_FEES_SHEET_INDEX = 2;
	private static final String MACHINE_READABLE = "Machine Readable";
	private static final String HCC_RATE_DATA_SHEET_NAME = "Rate Sheet";
	private static final String HCC_AREA_FACTOR_SHEET_NAME = "HCC Area Factor";
	private static final String HCC_ADMIN_FEES_SHEET_NAME = "Admin Fees";
	private static final int ERROR_MAX_LEN = 1000;
	private static final int MIN_ZIP_INDEX =0;
	private static final int MAX_ZIP_INDEX =1;
	private static final int ROW_START_INDEX = 4;
	private static String ENTIRE_STATE_STRING = "All";
	
	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private SerffService serffService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private SerffUtils serffUtils;
	@Autowired private STMPlanReader stmPlanReader ;
	@Autowired private IstmHccRepository istmHccRepository ;
	@Autowired private HCCSTMRateValidator hccstmRateValidator;
	@Autowired private IstmHCCAreaFactorRepository hccAreaFactor;
	@Autowired private SerffResourceUtil serffResourceUtil;
	@PersistenceUnit private EntityManagerFactory entityManagerFactory;

	/**
	 * This method is used to process Excel from given path 
	 * @param folderPath
	 * @return message
	 */
	public String processAllExcelTemplate(String folderPath , boolean isAreaFactor) {

		LOGGER.info("HCC processAllExcelTemplate() Start");
		StringBuilder message = new StringBuilder();
		try {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("HCC Plan rates FTP Folder Path: " + SecurityUtil.sanitizeForLogging(folderPath));
			}
			List<String> fileList = ftpClient.getFileNames(folderPath);
			LOGGER.debug("fileList is NULL: " + (null == fileList));

			if (null == fileList || fileList.isEmpty()) {
				message.append("No HCC Excel Template is available for processing.");
				return message.toString();
			}

			LOGGER.info("Number of Excel Template is " + fileList.size());
			boolean isEmpty = true;
			SerffPlanMgmtBatch trackingBatchRecord = null;

			for (String fileName : fileList) {
				trackingBatchRecord = null;
				if (StringUtils.isBlank(fileName)
						|| SerffConstants.FTP_SUCCESS.equalsIgnoreCase(fileName)
						|| SerffConstants.FTP_ERROR.equalsIgnoreCase(fileName)) {
					continue;
				}
				isEmpty = false;
				if(fileName.contains(FTP_HCC_FILE) && !isAreaFactor) {
					trackingBatchRecord = serffService.getTrackingBatchRecord(fileName, FTP_HCC_FILE, message);
				} else if(fileName.contains(FTP_AREA_FACTOR_FILE) && isAreaFactor) {
					trackingBatchRecord = serffService.getTrackingBatchRecord(fileName, FTP_AREA_FACTOR_FILE, message);
				}

				if(null != trackingBatchRecord){
					processFile(trackingBatchRecord, fileName, folderPath, message , isAreaFactor);
				}else{
					isEmpty = true;
				}
			}

			if (isEmpty) {
				message.append("No HCC Excel Template is available for processing.");
			}
		}
		catch (Exception ex) {
			message.append(MSG_ERROR_HCC_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			
			if (StringUtils.isEmpty(message.toString())) {
				message.append(MSG_HCC_PLANS_SUCCESS);
			}
			LOGGER.info("HCC processAllExcelTemplate() End - " + message);
		}
		return message.toString();
	
	}

	/**
	 * 
	 * @param fileName
	 * @param folderPath
	 * @param message
	 */
	private SerffPlanMgmtBatch processFile(SerffPlanMgmtBatch trackingBatchRecord, String fileName, String folderPath, StringBuilder message , boolean isAreaFactor) {
		SerffPlanMgmt serffTrackingRecord = null;
		SerffPlanMgmtBatch updatedTrackingBatchRecord = serffService.updateTrackingBatchRecordAsInProgress(trackingBatchRecord);
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug(SecurityUtil.sanitizeForLogging("File Name: " + folderPath + "/" + fileName));
		}
		try {
			String requestType = null;
			if(isAreaFactor) {
				requestType = SerffConstants.TRANSFER_AREA_FACTOR;
			} else {
				requestType = SerffConstants.TRANSFER_HCC;
			}
			serffTrackingRecord = serffUtils.createSerffPlanMgmtRecord(requestType,
					SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.W);
			if (null != serffTrackingRecord) {
				serffTrackingRecord.setAttachmentsList(fileName);
		        serffTrackingRecord.setRequestStateDesc("Uploaded succesfully and ready to load plans to DB.");
		        serffTrackingRecord = serffService.saveSerffPlanMgmt(serffTrackingRecord);
		        loadAllExcelTemplate(serffTrackingRecord, fileName, folderPath, message, isAreaFactor, trackingBatchRecord.getDefaultTenant());
			}
		} catch (UnknownHostException e) {
				message.append("Failed to create Serff tracking record.");
				LOGGER.error(e.getMessage(), e);
		} finally {
			SerffPlanMgmtBatch.BATCH_STATUS status = null;
			if (null != serffTrackingRecord && serffTrackingRecord.getRequestStatus().equals(SerffPlanMgmt.REQUEST_STATUS.S)) {
				//set status as success
				status = SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED;
			} else {
				status = SerffPlanMgmtBatch.BATCH_STATUS.FAILED;
			}
			updatedTrackingBatchRecord = serffService.updateTrackingBatchRecordAsCompleted(updatedTrackingBatchRecord, serffTrackingRecord, status);
		}
		return updatedTrackingBatchRecord;
	}
	
	/**
	 * This method is used to get files input stream from specifies path and send to ECM and update Tracking record
	 * @param trackingRecord
	 * @param fileName
	 * @param folderPath
	 * @param message
	 */
	private void loadAllExcelTemplate(SerffPlanMgmt trackingRecord,
			String fileName, String folderPath, StringBuilder message , boolean isAreaFactor, String defaultTenantIds) {
		
		LOGGER.debug("loadAllExcelTemplate() Start");
		boolean isSuccess = false;
		
		boolean isAreaFactorSuccess = false;
		
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		
		if (null == entityManager) {
			LOGGER.debug("entityManager is null");
			return;
		}
		InputStream inputStream = null;

		try {
			inputStream = ftpClient.getFileInputStream(folderPath + fileName);
			
			if (null == inputStream) {
				trackingRecord.setPmResponseXml("HCC Excel is not found at FTP server.");
				message.append(trackingRecord.getPmResponseXml());
				trackingRecord.setRequestStateDesc(message.toString());
				return;
			}
		
			// Upload Excel File to ECM
			isSuccess = addExcelInECM(message, trackingRecord, folderPath, fileName);

			if (!isSuccess) {
				LOGGER.error("Failed upload Excel file to ECM.");
				return;
			}
			isSuccess = false;
			
			// Load Excel File
			loadFile(inputStream);
			
			//checking if only area factor is loaded
			if(isAreaFactor){
				isAreaFactorSuccess = processAreaFactorData(trackingRecord , message , entityManager);
			}else{
				isSuccess= processPlanHCCRatesDate(trackingRecord, message, defaultTenantIds, entityManager);
			}
			if(isSuccess || isAreaFactorSuccess){
				LOGGER.debug("commiting all the transactions sucessfully");
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);	
				trackingRecord.setRequestStateDesc("Plans have been uploaded successfully");
				entityManager.getTransaction().commit();
				isSuccess = true;
				
			}else{
				if(StringUtils.isBlank(message)){
					message.append(MSG_ERROR_HCC_PLANS + "as data is invalid ");
				}
				trackingRecord.setRequestStateDesc(message.toString());
				LOGGER.debug(" Transaction rollbacked");
				entityManager.getTransaction().rollback();
			}
		}catch(Exception ex){
			trackingRecord.setRequestStateDesc(ex.getMessage());
			isSuccess = false;
			LOGGER.error("Exception occured "+ex);
			if(entityManager.isOpen()){
				entityManager.getTransaction().rollback();
			}
		
		}finally {
			doFinally(isSuccess, trackingRecord , fileName);
			if (entityManager.isOpen()) {
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("loadAllExcelTemplate() End.");
		}
	}
	
	/**
	 * This method processes the PlanHHC Rates data
	 * @param trackingRecord
	 * @param message
	 * @param entityManager
	 * @return
	 */
	private boolean processPlanHCCRatesDate(SerffPlanMgmt trackingRecord, StringBuilder message, String defaultTenantIds, EntityManager entityManager) {
		
		boolean isRateSuccess = false;
		boolean isAdminFeeSuccess = false;
		boolean isPlanSuccess = false;
		boolean  isValidHiosId = false;
		boolean isSheetsValid = false;
		boolean isSuccess = false;

		XSSFSheet stmPlanSheet = null;
		XSSFSheet stmPlanHCCRateSheet = null;
		XSSFSheet stmPlanAdminFeesSheet = null;
		try{
			LOGGER.debug("processPlanHCCRatesDate() : Starts");
			 stmPlanSheet = getSheet(PLAN_SHEET_INDEX);
			 stmPlanHCCRateSheet = getSheet(RATE_SHEET_INDEX);
			 stmPlanAdminFeesSheet = getSheet(ADMIN_FEES_SHEET_INDEX);
		
			 isSheetsValid = validateSheets(stmPlanSheet, stmPlanHCCRateSheet,stmPlanAdminFeesSheet, message, trackingRecord);
			 entityManager.getTransaction().begin();
			 if(isSheetsValid) {
				 Map<String, Plan> planMap = new HashMap<String, Plan>();
				 isValidHiosId = validatePlanAndRateHiosID(stmPlanSheet , stmPlanHCCRateSheet, message);
				
				 if(isValidHiosId){
					 
					isPlanSuccess = stmPlanReader.readAndPersistSTMData(stmPlanSheet, trackingRecord, entityManager, planMap, defaultTenantIds);
					if(isPlanSuccess){
						isRateSuccess = readAndPersistHCCRatesData(stmPlanHCCRateSheet, trackingRecord, message , entityManager , planMap );
						if(isRateSuccess){
							isAdminFeeSuccess =  readAndPersistHCCAdminFeesData(stmPlanAdminFeesSheet ,  trackingRecord , message , entityManager);
						}
					}
				}			
				isSuccess = isRateSuccess && isAdminFeeSuccess;
				
			}
		}catch(Exception e){
			isSuccess = false;
			LOGGER.error("Excaption occured while processing processPlanHCCRatesDate" , e);
		}
		LOGGER.debug("processPlanHCCRatesDate() : Ends");
		return isSuccess;
	}
	
	/**
	 * This method process the area Factor sheet data
	 * @param trackingRecord
	 * @param message
	 * @param entityManager
	 * @return
	 */
	private boolean processAreaFactorData(SerffPlanMgmt trackingRecord, StringBuilder message, EntityManager entityManager) {
		boolean isAreaFactorSuccess = false;
		XSSFSheet stmPlanAreaFactorSheet = null;
		LOGGER.debug("processAreaFactorData() : starts");
		try{
			 stmPlanAreaFactorSheet = getSheet(AREA_FACTOR_SHEET_INDEX);
			 entityManager.getTransaction().begin();
			
			 if(null!= stmPlanAreaFactorSheet && HCC_AREA_FACTOR_SHEET_NAME.equalsIgnoreCase(stmPlanAreaFactorSheet.getSheetName())){
				 isAreaFactorSuccess = readAndPersistHCCAreaFactorData(stmPlanAreaFactorSheet , trackingRecord , message , entityManager);
			 }
		}catch(Exception e){
			LOGGER.error("Exception occured while processing area factor data", e);
			isAreaFactorSuccess = false;
		}
		LOGGER.debug("processAreaFactorData() : starts");
		return isAreaFactorSuccess;
	
	}

	/**
	 * This method does all finally stuff
	 * @param isSuccess
	 * @param trackingRecord
	 * @param fileName
	 */
	private void doFinally(boolean isSuccess ,SerffPlanMgmt trackingRecord ,String fileName) {
		if (!isSuccess) {
			trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
		}
		
		String fileMessage = MSG_FILE + fileName + ":=\n";
		if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
			trackingRecord.setPmResponseXml(fileMessage + trackingRecord.getRequestStateDesc());
		}
		else {
			trackingRecord.setPmResponseXml(fileMessage + trackingRecord.getPmResponseXml());
		}
		trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
		if(trackingRecord.getRequestStateDesc().length() > ERROR_MAX_LEN) {
			trackingRecord.setRequestStateDesc(trackingRecord.getRequestStateDesc().substring(0, ERROR_MAX_LEN));
		}
		trackingRecord.setEndTime(new Date());
		serffService.saveSerffPlanMgmt(trackingRecord);
		moveFileToDirectory(isSuccess, fileName);
		LOGGER.debug("loadAllExcelTemplate() End");
		
	}

	private boolean validateSheets(XSSFSheet stmPlanSheet,
			XSSFSheet stmPlanHCCRateSheet,
			XSSFSheet stmPlanAdminFeesSheet ,StringBuilder message ,SerffPlanMgmt trackingRecord) {
		boolean issheetsValid = false;
		boolean isValidPlanSheet = null != stmPlanSheet && MACHINE_READABLE.equalsIgnoreCase(stmPlanSheet.getSheetName());
		boolean isValidRateSheet = null != stmPlanHCCRateSheet && HCC_RATE_DATA_SHEET_NAME.equalsIgnoreCase(stmPlanHCCRateSheet.getSheetName());
		boolean isValidAdminFeesSheet = null != stmPlanAdminFeesSheet && HCC_ADMIN_FEES_SHEET_NAME.equalsIgnoreCase(stmPlanAdminFeesSheet.getSheetName());
		
		if ( !isValidPlanSheet || !isValidRateSheet ||!isValidAdminFeesSheet) {
			trackingRecord.setPmResponseXml( "Either " +MACHINE_READABLE+ " sheet[1] or "+ HCC_RATE_DATA_SHEET_NAME + " sheet[2] or " +
			HCC_ADMIN_FEES_SHEET_NAME+" sheet[3] is missing !! \n");
			message.append(MSG_ERROR_HCC_PLANS+"Required sheets are missing" );
		}else{
			issheetsValid = true;
		}
		return issheetsValid;
		
	}

	/**
	 * This method validates HIOS is in Plan and Rate sheet
	 * @param stmPlanSheet
	 * @param stmPlanHCCRateSheet
	 * @param message
	 * @return isValid
	 */
	private boolean validatePlanAndRateHiosID(XSSFSheet stmPlanSheet,
			XSSFSheet stmPlanHCCRateSheet, StringBuilder message) {
				
		LOGGER.debug("validatePlanAndRateHiosID () starts ");
		Iterator<Row> planRowIterator = stmPlanSheet.iterator();
		
		boolean isPlanFirstRow =true;
		boolean isValid = false;
		boolean isRateValidated = false;
		String planHiosId= null;
		String currentPlanHiosId= null;
		boolean breakFromLoop = false;
		while (planRowIterator.hasNext()) {
			
				Row planExcelRow = planRowIterator.next();
				if(null!=planExcelRow) {
					if (isPlanFirstRow) {
						isPlanFirstRow = false;
					}else{
						if(!isRateValidated) {
							Iterator<Row> rateRowIterator = stmPlanHCCRateSheet.iterator();
							if(StringUtils.isNotBlank(getCellValueTrim(planExcelRow.getCell(STMExcelColumnEnum.HIOS_ID.getColumnIndex())))){
								planHiosId = getCellValueTrim(planExcelRow.getCell(STMExcelColumnEnum.HIOS_ID.getColumnIndex()));
								
								isValid = processValidate(rateRowIterator, planHiosId);
							
								if(!isValid){
									message.append(MSG_ERROR_HCC_PLANS+ "as HIOS ids are different in plan and rates sheet ");
									breakFromLoop = true;
								} else {
									LOGGER.debug("HIOS id is valid in rate sheet for issuer  "+planHiosId);
									isRateValidated = true;
								}
							}
						
					} else {
						currentPlanHiosId = getCellValueTrim(planExcelRow.getCell(STMExcelColumnEnum.HIOS_ID.getColumnIndex()));
						
						if (StringUtils.isNotBlank(currentPlanHiosId)
								&& !currentPlanHiosId.equalsIgnoreCase(planHiosId)) {
							message.append(MSG_ERROR_HCC_PLANS+ "as HIOS ids are different in plan and rates sheet ");
							isValid = false;
							breakFromLoop = true;
						}
						isValid = true;
					}
						
					if(breakFromLoop) {
						break;
					}
				}
			}
		}
		LOGGER.debug("validatePlanAndRateHiosID () ends ");
		return isValid;
	}
	/**
	 * This method checks HIOS id in rate sheet
	 * @param rateRowIterator
	 * @param planHiosId
	 * @return isValidRate
	 */
	private boolean processValidate(Iterator<Row> rateRowIterator ,String planHiosId ) {
		String rateHiosId = null;
		boolean isRateFirstRow =true;
		boolean isValidRate = false;
		while(rateRowIterator.hasNext()){
			Row rateExcelRow = rateRowIterator.next();
			if (isRateFirstRow) {
				isRateFirstRow = false;
			}else{

				if(null!=rateExcelRow && StringUtils.isNotBlank(getCellValueTrim(rateExcelRow.getCell(HCCRatesExcelColumnEnum.CARRIER_HIOS_ID.getColumnIndex())))){
					rateHiosId = getCellValueTrim(rateExcelRow.getCell(HCCRatesExcelColumnEnum.CARRIER_HIOS_ID.getColumnIndex()));
				}
				if(null!=planHiosId && planHiosId.equalsIgnoreCase(rateHiosId)){
					isValidRate = true;
				}else{
					isValidRate = false;
					break;
				}
			}
		}
		return isValidRate;
	}
	
	
	/**
	 * This method read and persist HCCAdminFees Excel Data
	 * @param stmPlanAdminFeesSheet
	 * @param trackingRecord
	 * @param message
	 * @param entityManager
	 * @return
	 */
	private boolean readAndPersistHCCAdminFeesData(
			XSSFSheet stmPlanAdminFeesSheet, SerffPlanMgmt trackingRecord,StringBuilder message , EntityManager entityManager) {
		boolean status = Boolean.FALSE;
		LOGGER.debug("readAndPersistHCCAreaFactorData() Start");
	
		Iterator<Row> rowIterator = stmPlanAdminFeesSheet.iterator();
		boolean isFirstRow = true;
		List<STMHCCPlansAdminFees> stmHCCPlansAdminFeesList = new ArrayList<STMHCCPlansAdminFees>();
		
		while (rowIterator.hasNext()) {
			
			Row excelRow = rowIterator.next();
			
			if (isFirstRow) {
				isFirstRow = false;
			} else{
				if(StringUtils.isNotBlank( getCellValueTrim(excelRow.getCell(HCCAdminFeesExcelColumnEnum.STATE.getColumnIndex())))) {
					
					STMHCCPlansAdminFees stmHCCPlansAdminFees = createHCCAdminFeesObject( message, excelRow);
					if(null==stmHCCPlansAdminFees){
						LOGGER.debug("HCC AreaFactor object is null");
						status = Boolean.FALSE;
						trackingRecord.setPmResponseXml(MSG_HCC_PLANS_INVALID);
						message.append(MSG_HCC_PLANS_INVALID);
						return status;
						
					}else{
						stmHCCPlansAdminFeesList.add(stmHCCPlansAdminFees);
					}
				} else {
					LOGGER.debug("Found blank cell so skipping rest of the rows");
					break;
				}
			}
		}
		if(!CollectionUtils.isEmpty(stmHCCPlansAdminFeesList)){
			//Soft deleting existing records
			softDeleteExistingAdminFeesRecords(entityManager );
			for(STMHCCPlansAdminFees eachSTMHCCPlansAdminFees : stmHCCPlansAdminFeesList ){
				entityManager.merge(eachSTMHCCPlansAdminFees);
			}
			status = Boolean.TRUE;
		}else{
			LOGGER.info("Invalid HCC Area Factor Sheet");
			trackingRecord.setPmResponseXml("Invalid HCC Area Factor Sheet ");
			message.append(MSG_ERROR_HCC_PLANS+ "as Invalid HCC Area Factor Sheet");
		}
		return status;
	
	}
	/**
	 * This method soft deletes Existing Admin Fees Records
	 * @param entityManager
	 */
	private void softDeleteExistingAdminFeesRecords(
			EntityManager entityManager) {
		LOGGER.info("softDeleteExistingAdminFeesRecords starts:");
		List<STMHCCPlansAdminFees> stmHCCPlansAdminFeesList = serffService.getallAdminFees();
		if(!CollectionUtils.isEmpty(stmHCCPlansAdminFeesList)){
			for(STMHCCPlansAdminFees  stmHCCPlansAdminFees : stmHCCPlansAdminFeesList){
				stmHCCPlansAdminFees.setIsDeleted(SerffConstants.YES_ABBR);
				entityManager.merge(stmHCCPlansAdminFees);
			}
		}
		LOGGER.info("softDeleteExistingAdminFeesRecords ends:");
	}
	
	
	/**
	 * This method creates object of STMHCCPlansAdminFees
	 * @param message
	 * @param excelRow
	 * @return STMHCCPlansAdminFees object
	 */
	private STMHCCPlansAdminFees createHCCAdminFeesObject(StringBuilder message,Row excelRow) {
		LOGGER.debug("createHCCAdminFeesObject (): starts");
		STMHCCPlansAdminFees stmlansHCCAdminFees = null;
		try{
			String adminElecFee = null;
			String adminPaperFee = null;
			String associationFee = null;
			if(null!= excelRow){
				
				String state = getCellValueTrim(excelRow.getCell(HCCAdminFeesExcelColumnEnum.STATE.getColumnIndex()));
				if(StringUtils.isNotBlank(state)) {
					stmlansHCCAdminFees =  new STMHCCPlansAdminFees();
					
					stmlansHCCAdminFees.setState(state);
					stmlansHCCAdminFees.setDuration(getCellValueTrim(excelRow.getCell(HCCAdminFeesExcelColumnEnum.DURATION.getColumnIndex())));
					adminElecFee = getCellValue(excelRow.getCell(HCCAdminFeesExcelColumnEnum.ADMIN_ELEC_FEE.getColumnIndex()));
					if(null!=adminElecFee){
						stmlansHCCAdminFees.setAdminElecFee(Double.parseDouble(adminElecFee));
					}
					
					adminPaperFee = getCellValue(excelRow.getCell(HCCAdminFeesExcelColumnEnum.ADMIN_PAPER_FEE.getColumnIndex()));
					if(null!=adminPaperFee){
						stmlansHCCAdminFees.setAdminPaperFee(Double.parseDouble(adminPaperFee));
					}
					
					associationFee = getCellValue(excelRow.getCell(HCCAdminFeesExcelColumnEnum.ASSOC_FEE.getColumnIndex()));
					if(null!=associationFee){
						stmlansHCCAdminFees.setAssociationFee(Double.parseDouble(associationFee));
					}
					stmlansHCCAdminFees.setComments(getCellValueTrim(excelRow.getCell(HCCAdminFeesExcelColumnEnum.COMMENTS.getColumnIndex())));
					stmlansHCCAdminFees.setIsDeleted("N");
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured while persisting Admin fee data" + ex);
			message.append(MSG_ERROR_HCC_PLANS+" Exception occured while persisting Admin fee data");
			return null;
		}
		LOGGER.debug("createHCCAdminFeesObject() :ends ");
		return stmlansHCCAdminFees;
	}
	
	/**
	 * This method creates object of STMHCCPlansAreaFactor
	 * @param message
	 * @param excelRow
	 * @return STMHCCPlansAreaFactor object
	 */
	private STMHCCPlansAreaFactor createHCCAreaFactorObj(StringBuilder message, Row excelRow , String hiosId , String startDate) {
		LOGGER.debug("setHCCAreaFactor() :starts");
			STMHCCPlansAreaFactor stmHCCPlansAreaFactor = null;
			try{
				String minZip = null;
				String maxZip = null;
				String[] zipcodes = null;
				
				if (null != excelRow ) {
					
					String state = getCellValueTrim(excelRow.getCell(HCCAreaFactorExcelColumnEnum.STATE.getColumnIndex()));
					if(StringUtils.isNotBlank(state)) {
						stmHCCPlansAreaFactor = new STMHCCPlansAreaFactor();
						
						String zipcode = getCellValueTrim(excelRow.getCell(HCCAreaFactorExcelColumnEnum.ZIP_CODE.getColumnIndex()));
						
						stmHCCPlansAreaFactor.setState(state);
						
						if(zipcode.contains(SerffConstants.HYPHEN)){
							zipcodes =  zipcode.split("-");
							minZip = zipcodes[MIN_ZIP_INDEX];
							maxZip = zipcodes[MAX_ZIP_INDEX];
							stmHCCPlansAreaFactor.setMinZip(StringUtils.trim(minZip));
							stmHCCPlansAreaFactor.setMaxZip(StringUtils.trim(maxZip));
						}
						
						if(ENTIRE_STATE_STRING.equalsIgnoreCase(zipcode)){
							stmHCCPlansAreaFactor.setSupportFullState(SerffConstants.YES_ABBR);
						}else{
							stmHCCPlansAreaFactor.setSupportFullState(SerffConstants.NO_ABBR);
						}
					
						String factorialValue = getCellValue(excelRow.getCell(HCCAreaFactorExcelColumnEnum.FACTORIAL_VALUE.getColumnIndex()));
						if (StringUtils.isNotBlank(factorialValue)) {
							stmHCCPlansAreaFactor.setAreaFactorVal(Double.parseDouble(factorialValue));
						}
						stmHCCPlansAreaFactor.setIsDeleted(SerffConstants.NO_ABBR);	
						stmHCCPlansAreaFactor.setCarrierHiosId(hiosId);
						stmHCCPlansAreaFactor.setEffectiveStartDate(dateFormat.parse(startDate));
						stmHCCPlansAreaFactor.setEffectiveEndDate(dateFormat.parse(FOREVER_END_DATE));
					}else{
						LOGGER.info("Skipping blank row");
					}
				}
	
			}catch(Exception ex){
				LOGGER.error("Exception occured while persisting AreaFactor data" + ex);
				message.append(MSG_ERROR_HCC_PLANS+" Exception occured while persisting AreaFactor data");
				return null;
			}
			LOGGER.debug("setHCCAreaFactor() :ends");
		return stmHCCPlansAreaFactor;
	}

	
	/**
	 * 
	 * @param message
	 * @param excelRow
	 * @param hiosId
	 * @param startDate
	 */
	private void updateEffectiveDatesforExistingAreaFactor(EntityManager entityManager , StringBuilder message,  String hiosId , Date newStartDate){
		//boolean status = false;
		try{
			
			//Date newStartDate = dateFormat.parse(startDate);
			Date currentTimeStamp = new Date();
			LOGGER.info("createHCCAreaFactorObj() : newStartDate : " + newStartDate);
	
			//Find area factor in db which having effective start date after newRateStartDate.
			List<STMHCCPlansAreaFactor> existingSTMHCCPlansAreaFactor = hccAreaFactor.findAreaFactorAfterNewStartDate(hiosId , newStartDate);
			LOGGER.debug("Existing area factor having effective start date after newStartDate : " + (existingSTMHCCPlansAreaFactor == null ? "NULL" : existingSTMHCCPlansAreaFactor.size()));
			
			//Find area factor in db which having effective start date before newRateStartDate and end date after new start date.
			List<STMHCCPlansAreaFactor> existingPastDateAreaFactor = hccAreaFactor.findAreaFactorBeforeNewStartDate(hiosId, newStartDate);
			LOGGER.debug("Existing area factor having effective start date before newStartDate and end date after newStartDate : " + (existingPastDateAreaFactor == null ? "NULL" : existingPastDateAreaFactor.size()));
			
			if(!CollectionUtils.isEmpty(existingSTMHCCPlansAreaFactor)){
				for(STMHCCPlansAreaFactor areaFactor : existingSTMHCCPlansAreaFactor){
					areaFactor.setIsDeleted(SerffConstants.YES_ABBR);
					LOGGER.debug("area Factor set as deleted : Id : " + areaFactor.getId() + " : Issuer : " + areaFactor.getCarrierHiosId());
					areaFactor.setLastUpdateTimestamp(currentTimeStamp);
					areaFactor = (STMHCCPlansAreaFactor) mergeEntityManager(entityManager, areaFactor);
					
				}
			}
			Date revisedDate = DateUtil.addToDate(newStartDate, 0, 0, 0, -1);
			if(!CollectionUtils.isEmpty(existingPastDateAreaFactor)){
				for(STMHCCPlansAreaFactor areaFactor : existingPastDateAreaFactor){
					areaFactor.setEffectiveEndDate(revisedDate);
					LOGGER.debug("Changed end date of plan rate to " + areaFactor.getEffectiveEndDate() + 
							": Rate Id : " + areaFactor.getId() + " : Issuer : " + areaFactor.getCarrierHiosId());
					areaFactor.setLastUpdateTimestamp(currentTimeStamp);
					areaFactor = (STMHCCPlansAreaFactor) mergeEntityManager(entityManager, areaFactor);
				}
			}
		}catch (Exception e) {
			LOGGER.debug("Error occurred while processing future area factor : " + e.getMessage(), e);
			message.append("Error occurred while processing future area factor : ");
		}
	}
	
	/**
	 * This method reads and parse HCCAreaFactorData Excel
	 * @param stmPlanAreaFactorSheet
	 * @param trackingRecord
	 * @param message
	 * @param entityManager
	 * @return status
	 */
	private boolean readAndPersistHCCAreaFactorData(
			XSSFSheet stmPlanAreaFactorSheet, SerffPlanMgmt trackingRecord,StringBuilder message ,EntityManager entityManager) {
		boolean status = Boolean.FALSE;
		LOGGER.debug("readAndPersistHCCAreaFactorData() Start");
	
		Iterator<Row> rowIterator = stmPlanAreaFactorSheet.iterator();
		List<STMHCCPlansAreaFactor> stmHCCPlansAreaFactorList = new ArrayList<STMHCCPlansAreaFactor>();
		int row = 0;
		String hiosId = null;
		String startDate = null;
		
		Row excelRow = stmPlanAreaFactorSheet.getRow(HCCAreaFactorExcelRowEnum.CARRIER_HIOS_ID.getRowIndex());
		hiosId = getColumnValueAtIndex(excelRow, 1);
		excelRow = stmPlanAreaFactorSheet.getRow(HCCAreaFactorExcelRowEnum.START_DATE.getRowIndex());
		startDate = getColumnValueAtIndex(excelRow, 1);
		try{
			
			if(StringUtils.isNotBlank(hiosId) && StringUtils.isNotBlank(startDate)){
				
				Date newStartDate = dateFormat.parse(startDate);
				Date currentTimeStamp = new Date();
				
				if(newStartDate.after(currentTimeStamp)){
					
					//update existing effective date as per new data
					updateEffectiveDatesforExistingAreaFactor(entityManager, message, hiosId, newStartDate);
					while (rowIterator.hasNext()) {
						excelRow = rowIterator.next();
						
						if (row < ROW_START_INDEX) {
							row ++;
							continue;
						}else{
							if(StringUtils.isNotBlank( getCellValueTrim(excelRow.getCell(HCCAreaFactorExcelColumnEnum.STATE.getColumnIndex())))) {
								//persist  new area factor records
								STMHCCPlansAreaFactor stmHCCPlansAreaFactor = createHCCAreaFactorObj( message, excelRow , hiosId , startDate);
								if(null==stmHCCPlansAreaFactor){
									LOGGER.debug("HCC AreaFactor object is null");
									status = Boolean.FALSE;
									trackingRecord.setPmResponseXml(MSG_HCC_PLANS_INVALID);
									message.append(MSG_HCC_PLANS_INVALID);
									return status;
									
								}else{
									stmHCCPlansAreaFactorList.add(stmHCCPlansAreaFactor);
								}
							} else {
								LOGGER.debug("Found blank cell so skipping rest of the rows");
								break;
							}
						}
					}
					if(!CollectionUtils.isEmpty(stmHCCPlansAreaFactorList)){
						LOGGER.debug("merging STMHCCPlansAreaFactor ");
						for(STMHCCPlansAreaFactor stmHCCPlansAreaFactor : stmHCCPlansAreaFactorList ){
							entityManager.merge(stmHCCPlansAreaFactor);
						}
						status = Boolean.TRUE;
					}else{
						LOGGER.info("Invalid HCC Area Factor Sheet , please check the data!");
						trackingRecord.setPmResponseXml("Invalid HCC Area Factor Sheet ");
						message.append(MSG_ERROR_HCC_PLANS+ "as Invalid HCC Area Factor Sheet");
					}
					LOGGER.debug("readAndPersistHCCAreaFactorData() ends");
				}else{
					LOGGER.info("Invalid Start Date in Area Factor Sheet , please provide future date!");
					trackingRecord.setPmResponseXml("Invalid Start Date in Area Factor Sheet ");
					message.append(MSG_ERROR_HCC_PLANS+ "as Invalid Start Date in Area Factor Sheet");
				}
			}
			
		}catch(Exception e){
			LOGGER.error("Exception occuered while readAndPersistHCCAreaFactorData:" , e);
			status = false;
		}
		return status;
		
		
	}

	/**
	 * This method is used to parse the EXCEL sheet 
	 * @param rateSheet
	 * @param trackingRecord
	 * @param message
	 * @return status
	 */
	private boolean readAndPersistHCCRatesData(XSSFSheet rateSheet, SerffPlanMgmt trackingRecord, StringBuilder message ,EntityManager entityManager,
			 Map<String , Plan> hccPlanMap ) {
		
		LOGGER.debug("readAndPersistHCCData() Start");
		boolean status = Boolean.FALSE;
		
		// Iterate through each rows from first sheet
		Iterator<Row> rowIterator = rateSheet.iterator();
		boolean isFirstRow = true;
		boolean isPlanExistInDB = false;
		List<STMHCCPlansRate> stmHCCPlanRateList = new ArrayList<STMHCCPlansRate>();
		List<Integer> planIdListforRateSoftDelete = new ArrayList<Integer>();
		Map<String, List<Row>> certifiedPlanForRateUpdate = new HashMap<String, List<Row>>();
		List<String> planNumListWithInvalidRate = new ArrayList<String>();
		List<HCCRateExcelVO> failedRateList = new ArrayList<HCCRateExcelVO>();
		String issuerId  = null;
		Date foreverDate = null;

		try{
			foreverDate =  dateFormat.parse(FOREVER_END_DATE);

			while (rowIterator.hasNext()) {
				
				Row excelRow = rowIterator.next();
				
				if (isFirstRow) {
					isFirstRow = false;
				}else{
					String state = getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.STATE.getColumnIndex()));
					
					if(StringUtils.isNotBlank(state)) {
						HCCRateExcelVO rateVO = createHCCRateExcelVO(excelRow);
						if(hccstmRateValidator.validateHCCRate(rateVO, failedRateList)) {
							issuerId = getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.CARRIER_HIOS_ID.getColumnIndex()));
							
							String planId = getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.PLAN_ID.getColumnIndex()));
							String hccplanID = issuerId+state+planId+SerffConstants.OFF_EXCHANGE_VARIANT_SUFFIX;
							
							if(StringUtils.isNotBlank(issuerId) && StringUtils.isNotBlank(planId) ){
								isPlanExistInDB = false;
								Plan fethcedPlan = hccPlanMap.get(hccplanID);
								if(null == fethcedPlan ){
									fethcedPlan = serffService.getPlanIdbyIssuerPlanNumberAndIsDeleted(hccplanID ,SerffConstants.NO_ABBR);
									if(null!= fethcedPlan){
										hccPlanMap.put(hccplanID, fethcedPlan);
										isPlanExistInDB = true;					
									}
								}
								
								if(null!= fethcedPlan){
			
									if(Plan.PlanStatus.CERTIFIED.toString().equalsIgnoreCase(fethcedPlan.getStatus())) {
										preProcessRateForCertifiedPlan(excelRow, fethcedPlan, certifiedPlanForRateUpdate, planNumListWithInvalidRate);
									}else{
										if(isPlanExistInDB) {
											planIdListforRateSoftDelete.add(fethcedPlan.getId());
										}
										STMHCCPlansRate stmPlanHccRate = createHCCRatesObject( message, excelRow, fethcedPlan.getId(), state, issuerId, foreverDate );
										
										if(null==stmPlanHccRate){
											LOGGER.debug("HCC Rate object is null for plan "+hccplanID );
											status = Boolean.FALSE;
											trackingRecord.setPmResponseXml(MSG_HCC_PLANS_INVALID);
											message.append(MSG_HCC_PLANS_INVALID);
											return status;
											
										}else{
											stmHCCPlanRateList.add(stmPlanHccRate);
										}
									}
								}else{
									LOGGER.warn("Plan does not exist with planID : "+hccplanID);
									message.append(MSG_ERROR_HCC_PLANS +"["+ hccplanID+ "] Plan does not exist ");
									return status ;
								}
							}
						} else {
							LOGGER.warn("Rates template has invalid data : "+rateVO.toString());
							message.append(MSG_ERROR_HCC_PLANS +"Rates template has invalid data : "+ rateVO.toString());
							return status ;
						}
						
					} else {
						LOGGER.debug("Found blank cell so skipping rest of the rows");
						break;
					}
				}

			}
			
			//Ignore all plans that have invalid rates (Start date in past)
			if(!CollectionUtils.isEmpty(planNumListWithInvalidRate)) {
				StringBuilder pastDateMsg = new StringBuilder();
				if(trackingRecord.getPmResponseXml() != null) {
					pastDateMsg.append(trackingRecord.getPmResponseXml());
					pastDateMsg.append("\n");
				}
				pastDateMsg.append("Skipping Rates update for Certified STM Plan as the start date falls in the past. Plan # ");
				for(String planNum : planNumListWithInvalidRate ){
					pastDateMsg.append(planNum);
					pastDateMsg.append(SerffConstants.SPACE);
					certifiedPlanForRateUpdate.remove(planNum);
				}
				message.append(pastDateMsg.toString());
				trackingRecord.setPmResponseXml(pastDateMsg.toString());
			}
			
			status = processRateForCertifiedPlan(certifiedPlanForRateUpdate, hccPlanMap, entityManager);
			
			if(!CollectionUtils.isEmpty(stmHCCPlanRateList) && status){
			
				//Soft deleting existing records
				softDeleteExistingHCCRatesRecords(entityManager , planIdListforRateSoftDelete );
				
				for(STMHCCPlansRate planRate : stmHCCPlanRateList ){
					entityManager.merge(planRate);
				}
				status = Boolean.TRUE;
			}

			if(!status) {
				LOGGER.info("Invalid HCC Rates Sheet");
				trackingRecord.setPmResponseXml("Invalid HCC Rates Sheet ");
				message.append(MSG_ERROR_HCC_PLANS+ "as Invalid HCC Rates Sheet");
			}
		} catch (ParseException e) {
			LOGGER.error("Failed to parse date from String " + e.getMessage(), e);
		}catch(Exception ex){
			LOGGER.error(ex.getMessage(), ex);
		}
		return status;
	}

	private HCCRateExcelVO createHCCRateExcelVO(Row excelRow) {
		HCCRateExcelVO rateVO = new HCCRateExcelVO();
		rateVO.setPlanState(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.STATE.getColumnIndex())));
		rateVO.setHiosId(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.CARRIER_HIOS_ID.getColumnIndex()), true));
		rateVO.setPlanNumber(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.PLAN_ID.getColumnIndex())));
		rateVO.setDeductible(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.DEDUCTIBLE.getColumnIndex())));
		rateVO.setIssuerName(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.CARRIER_NAME.getColumnIndex())));
		rateVO.setMinAge(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.MIN_AGE.getColumnIndex()), true));
		rateVO.setMaxAge(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.MAX_AGE.getColumnIndex()), true));
		rateVO.setMaleRate(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.MALE.getColumnIndex())));
		rateVO.setFemaleRate(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.FEMALE.getColumnIndex())));
		rateVO.setChildRate(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.CHILD.getColumnIndex())));
		rateVO.setPlanMaxCoverage(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.PLAN_MAX_COVERAGE.getColumnIndex())));
		rateVO.setEffectiveStartDate(getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.EFFECTIVE_START_DATE.getColumnIndex())));
		return rateVO;
	}

	/**
	 * @param excelRow
	 * @param fethcedPlan
	 * @param certifiedPlanForUpdate
	 * @param planNumListWithInvalidRate
	 * @throws ParseException
	 */
	private void preProcessRateForCertifiedPlan(Row excelRow, Plan fethcedPlan, Map<String, List<Row>> certifiedPlanForUpdate, List<String> planNumListWithInvalidRate) throws ParseException {
		String startDateStr = getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.EFFECTIVE_START_DATE.getColumnIndex()));
		Date startDate = dateFormat.parse(startDateStr);
		Date currentTime = Calendar.getInstance().getTime();
		//validate for future rates
		if(startDate.after(currentTime)) {
			if(!certifiedPlanForUpdate.containsKey(fethcedPlan.getIssuerPlanNumber())) {
				certifiedPlanForUpdate.put(fethcedPlan.getIssuerPlanNumber(), new ArrayList<Row>());
			}
			certifiedPlanForUpdate.get(fethcedPlan.getIssuerPlanNumber()).add(excelRow);
		} else {
			LOGGER.info("Rate for CERTIFIED plan has a start date which was in the past, so rates cannot be updated: Plan Num: " +fethcedPlan.getIssuerPlanNumber() + " Start Date: " + startDateStr);
			if(!planNumListWithInvalidRate.contains(fethcedPlan.getIssuerPlanNumber())) {
				planNumListWithInvalidRate.add(fethcedPlan.getIssuerPlanNumber());
			}			
		}
	}
	
	private boolean processRateForCertifiedPlan(Map<String, List<Row>> certifiedPlanForUpdate, Map<String , Plan> hccPlanMap, EntityManager entityManager) throws ParseException {
		Date currentTime = Calendar.getInstance().getTime();
		Date foreverDate =  dateFormat.parse(FOREVER_END_DATE);
		for (Entry<String, List<Row>> planEntry : certifiedPlanForUpdate.entrySet()) {
			int planID = hccPlanMap.get(planEntry.getKey()).getId();
			List<Row> newRateList = planEntry.getValue();
			String startDateStr = getCellValueTrim(newRateList.get(0).getCell(HCCRatesExcelColumnEnum.EFFECTIVE_START_DATE.getColumnIndex()));
			Date newRateStartDate = dateFormat.parse(startDateStr);
			//Find plans in db which having effective start date after newRateStartDate.
			List<STMHCCPlansRate> existingPlanRates = istmHccRepository.findRatesInFutureForEffectiveStartDate(planID, newRateStartDate);
			
			//Find plans in db which having effective start date before newRateStartDate and end date after new start date.
			List<STMHCCPlansRate> existingPastDatePlanRates = istmHccRepository.findRatesBeforeEffectiveStartDate(planID, newRateStartDate);
			
			//softdelete future rates
			if(!CollectionUtils.isEmpty(existingPlanRates)){
				for(STMHCCPlansRate planRate : existingPlanRates){
					planRate.setIsDeleted(SerffConstants.YES_ABBR);
					planRate.setLastUpdateTimestamp(currentTime);
					planRate = (STMHCCPlansRate) mergeEntityManager(entityManager, planRate);
				}
			}
			
			//update previous rate
			if(!CollectionUtils.isEmpty(existingPastDatePlanRates)){
				for(STMHCCPlansRate planRate : existingPastDatePlanRates){
					planRate.setEndDate(DateUtil.addToDate(newRateStartDate, 0, 0, 0, -1));
					planRate.setLastUpdateTimestamp(currentTime);
					planRate = (STMHCCPlansRate) mergeEntityManager(entityManager, planRate);
				}
			}

			//add new rates
			for(Row planRateRow : newRateList ){
				entityManager.merge(createHCCRatesObject(null, planRateRow, planID, 
						getCellValueTrim(planRateRow.getCell(HCCRatesExcelColumnEnum.STATE.getColumnIndex())), 
						getCellValueTrim(planRateRow.getCell(HCCRatesExcelColumnEnum.CARRIER_HIOS_ID.getColumnIndex())), 
						foreverDate));
			}
		}
		return true;
	}

	/**
	 * Method is used to merge AME Table to Entity Manager.
	 */
	private Object mergeEntityManager(EntityManager entityManager, Object saveObject) {
		
		Object savedObject = null;
		
		if (null != entityManager && entityManager.isOpen()) {
			savedObject = entityManager.merge(saveObject);
			LOGGER.debug(saveObject.getClass() + " bean has been Persisted Successfully.");
		}
		return savedObject;
	}
	/**
	 * This method is used to soft delete Existing HCC Rates Records
	 * @param entityManager
	 * @param planIDlist
	 */

	private void softDeleteExistingHCCRatesRecords(EntityManager entityManager, List<Integer> planIDlist) {
		List<STMHCCPlansRate>  existingHccRates = null;
		LOGGER.debug("softDeleteExistingHCCRatesRecords () :starts");
		for(Integer planId : planIDlist){
		
			existingHccRates = serffService.getallratesbyPlanId(planId, SerffConstants.NO_ABBR);

			for(STMHCCPlansRate existingHccRate: existingHccRates ){
				if(null!= existingHccRate){
					existingHccRate.setIsDeleted(SerffConstants.YES_ABBR);
					entityManager.merge(existingHccRate);
				}
			}
			
		}
		LOGGER.debug("softDeleteExistingHCCRatesRecords () :ends");
	}

	/**
	 * This method is used to create HCCRates Object
	 * @param message
	 * @param excelRow
	 * @return STMHCCPlansRate object
	 */
	private STMHCCPlansRate createHCCRatesObject(StringBuilder message, Row excelRow, Integer planId, String state, String issuerId, Date foreverDate) {
		LOGGER.info("setHCCExcelData() :starts");
		STMHCCPlansRate planHccRate = null;
		try{
			if (null != excelRow && StringUtils.isNotBlank(issuerId)) {
					planHccRate = new STMHCCPlansRate();
					String minAge = getCellValue(excelRow.getCell(HCCRatesExcelColumnEnum.MIN_AGE.getColumnIndex()),true);
					String maxAge = getCellValue(excelRow.getCell(HCCRatesExcelColumnEnum.MAX_AGE.getColumnIndex()),true);
					String maleRate = getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.MALE.getColumnIndex()));
					String femaleRate = getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.FEMALE.getColumnIndex()));
					String childRate = getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.CHILD.getColumnIndex()));
					String startDateStr = getCellValueTrim(excelRow.getCell(HCCRatesExcelColumnEnum.EFFECTIVE_START_DATE.getColumnIndex()));
					if(StringUtils.isNotBlank(startDateStr) && isValidAgeOptions(minAge,maxAge)){
						
						planHccRate.setPlanId(planId);
						planHccRate.setState(state);
						planHccRate.setMinAge(Integer.parseInt(minAge));
						planHccRate.setMaxAge(Integer.parseInt(maxAge));
						if(StringUtils.isNotBlank(maleRate)){
							planHccRate.setMaleRate(Double.parseDouble(maleRate));
						}
						if(StringUtils.isNotBlank(femaleRate)){
							planHccRate.setFemaleRate(Double.parseDouble(femaleRate));
						}
						if(StringUtils.isNotBlank(childRate)){
							planHccRate.setChildRate(Double.parseDouble(childRate));
						}
						planHccRate.setIsDeleted(SerffConstants.NO_ABBR);
						//Adding Rate effective start and end dates
						planHccRate.setStartDate(dateFormat.parse(startDateStr));
						planHccRate.setEndDate(foreverDate);
						
					}else{
						LOGGER.error("One of Required filed is missing");
						return null;
					}
			}
		}catch(Exception e){
			LOGGER.error("Exception occured while persisting data" + e);
			message.append(MSG_ERROR_HCC_PLANS+" Exception occured while persisting data");
			return null;
		}
		LOGGER.info("setHCCExcelData() :ends");
		return planHccRate;
	}

	private boolean isValidAgeOptions(String minAge, String maxAge) {
		return StringUtils.isNotBlank(minAge) && StringUtils.isNotBlank(maxAge);
	}
	/**
	 * This method is used to store uploaded files in ECM 
	 * @param message
	 * @param trackingRecord
	 * @param folderPath
	 * @param fileName
	 * @return
	 * @throws IOException
	 */
	private boolean addExcelInECM(StringBuilder message, SerffPlanMgmt trackingRecord, String folderPath, String fileName) throws IOException {

		LOGGER.debug("addExcelInECM() Start.");
		boolean isSuccess = false;
		InputStream inputStream = null;
		
		try {
			inputStream = ftpClient.getFileInputStream(folderPath + fileName);
			
			if (null != inputStream) {
				LOGGER.debug("Uploading File Name with path to ECM:" + SerffConstants.SERF_ECM_BASE_PATH + FTP_HCC_FILE + fileName);
				// Uploading AME Excel at ECM.
				SerffDocument attachment = serffUtils.addAttachmentInECM(ecmService, trackingRecord, SerffConstants.SERF_ECM_BASE_PATH, FTP_HCC_FILE, fileName, IOUtils.toByteArray(inputStream), true);
				serffService.saveSerffDocument(attachment);
				isSuccess = true;
			}
			else {
				message.append("Content is not valid for file: ");
				message.append(fileName);
				trackingRecord.setRequestStateDesc(message.toString());
			}
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			message.append(MSG_ERROR_HCC_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("addExcelInECM() End.");
		}
		return isSuccess;
	}
		
	
	/**
	 * This method move uploaded file to specific directory
	 * @param isSuccess
	 * @param fileName
	 */
	public void moveFileToDirectory(boolean isSuccess, String fileName) {
		
		LOGGER.debug("moveFileToDirectory() Start");
		String moveToDirectoryPath = null;
		
		try {
			String baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getHCCExcelUploadPath();
			moveToDirectoryPath = baseDirectoryPath;
			
			if (isSuccess) {
				moveToDirectoryPath += SerffConstants.FTP_SUCCESS + SerffConstants.PATH_SEPERATOR;
			}
			else {
				moveToDirectoryPath += SerffConstants.FTP_ERROR + SerffConstants.PATH_SEPERATOR;
			}
			
			if (!ftpClient.isRemoteDirectoryExist(moveToDirectoryPath)) {
				ftpClient.createDirectory(moveToDirectoryPath);
			}
			LOGGER.debug("STM HCC Plan FTP Folder Path: " + baseDirectoryPath);
			LOGGER.info("Move To Directory Path: " + moveToDirectoryPath);
			ftpClient.moveFile(baseDirectoryPath, fileName, moveToDirectoryPath, fileName);
		}
		catch (Exception ex) {
			LOGGER.info("Skipping file beacause it is already exist in directory: " + moveToDirectoryPath);
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("moveFileToDirectory() End");
		}
	}
}