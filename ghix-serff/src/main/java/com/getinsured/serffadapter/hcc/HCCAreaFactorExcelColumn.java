/**
 * 
 */
package com.getinsured.serffadapter.hcc;

/**
 * @author sharma_va
 *
 */
public class HCCAreaFactorExcelColumn {

	private static final int FLOAT_TYPE = 1;
	private static final int STRING_TYPE = 2;
	public static final int AREA_FACTOR_START_ROW = 4;

	
	public enum HCCAreaFactorExcelColumnEnum{
		STATE("State", "STATE", 0, STRING_TYPE),
		ZIP_CODE("ZIP CODE" ,"ZIP_CODE" , 1 , STRING_TYPE),
		FACTORIAL_VALUE("FACTORIAL VALUE" , "FACTORIAL_VALUE" , 2 ,FLOAT_TYPE )
		;
		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;
		
		
		private HCCAreaFactorExcelColumnEnum(String name, String columnConstName, int index, int dataType){
			this.columnName = name;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}
		
		public String getColumnName() {
			return columnName;
		}
		
		public String getColumnConstName() {
			return columnConstName;
		}
		
		public int getColumnIndex() {
			return columnIndex;
		}
		
		public int getDataType() {
			return dataType;
		}
	}
	
	public enum HCCAreaFactorExcelRowEnum{
		CARRIER_HIOS_ID ("Carrier Id","CARRIER_HIOS_ID" ,0 , STRING_TYPE),
		START_DATE("Start Date" ,"START_DATE" ,1, STRING_TYPE);

		private String rowName;
		private String rowConstName;
		private int rowIndex;
		private int dataType;
		
		private HCCAreaFactorExcelRowEnum(String name, String rowConstName, int index, int dataType){
			this.rowName = name;
			this.rowConstName = rowConstName;
			this.rowIndex = index;
			this.dataType = dataType;
		}
		
		public String getRoeName() {
			return rowName;
		}
		
		public String getRowConstName() {
			return rowConstName;
		}
		
		public int getRowIndex() {
			return rowIndex;
		}
		public int getDataType() {
			return dataType;
		}
		
	}
	
}
