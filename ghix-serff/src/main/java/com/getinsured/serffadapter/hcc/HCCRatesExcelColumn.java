/**
 * 
 */
package com.getinsured.serffadapter.hcc;

/**
 *  Class is used to generate Enum on the basis of HCC plan rates Excel columns.
 * @author sharma_va
 *
 */
public class HCCRatesExcelColumn {

	private static final int NUMBER_TYPE = 0;
	private static final int FLOAT_TYPE = 1;
	private static final int STRING_TYPE = 2;
	
	public enum HCCRatesExcelColumnEnum{
		
		STATE("State", "STATE", 0, STRING_TYPE),
		CARRIER_HIOS_ID("Carrier HIOS ID", "CARRIER_HIOS_ID", 1, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 2, STRING_TYPE),
		DEDUCTIBLE("Deductible" , "DEDUCTIBLE", 3, NUMBER_TYPE),
		CARRIER_NAME("Carrier Name" , "CARRIER_NAME", 4, STRING_TYPE),
		MIN_AGE("Min Age" ,"MIN_AGE", 5, NUMBER_TYPE),
		MAX_AGE("Max Age" ,"MAX_AGE", 6, NUMBER_TYPE),
		MALE("Male" , "MALE", 7, FLOAT_TYPE),
		FEMALE("Female" , "FEMALE", 8, FLOAT_TYPE),
		CHILD("Child" , "CHILD", 9, FLOAT_TYPE ),
		PLAN_MAX_COVERAGE("Plan Max Coverage", "PLAN_MAX_COVERAGE", 10, NUMBER_TYPE ),
		EFFECTIVE_START_DATE("Effective Start Date", "EFFECTIVE_START_DATE", 11, STRING_TYPE )
		;
		
		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;
		
		private HCCRatesExcelColumnEnum(String name, String columnConstName, int index, int dataType) {
			this.columnName = name;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}
		
		public String getColumnName() {
			return columnName;
		}
		
		public String getColumnConstName() {
			return columnConstName;
		}
		
		public int getColumnIndex() {
			return columnIndex;
		}
		
		public int getDataType() {
			return dataType;
		}
		
	}
	

}
