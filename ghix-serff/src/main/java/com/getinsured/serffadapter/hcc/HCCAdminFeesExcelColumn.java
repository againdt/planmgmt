/**
 * 
 */
package com.getinsured.serffadapter.hcc;

/**
 * @author sharma_va
 *
 */
public class HCCAdminFeesExcelColumn {
	private static final int FLOAT_TYPE = 1;
	private static final int STRING_TYPE = 2;
	
	
	public enum HCCAdminFeesExcelColumnEnum{
		
		STATE("State", "STATE", 0, STRING_TYPE),
		DURATION("Duration" , "DURATION" , 1,STRING_TYPE ),
		ADMIN_ELEC_FEE("Admin Electronic Fee" , "ADMIN_ELEC_FEE" , 2, FLOAT_TYPE),
		ADMIN_PAPER_FEE("Admin Paper Fee" , "ADMIN_PAPER_FEE" , 3 ,FLOAT_TYPE),
		ASSOC_FEE("Association Fee" ,"ASSOC_FEE" ,4 ,FLOAT_TYPE ),
		COMMENTS("Comments" , "COMMENTS" , 5 , STRING_TYPE)
		;
		
		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;
		
		private HCCAdminFeesExcelColumnEnum(String name, String columnConstName, int index, int dataType) {
			this.columnName = name;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}
		
		public String getColumnName() {
			return columnName;
		}
		
		public String getColumnConstName() {
			return columnConstName;
		}
		
		public int getColumnIndex() {
			return columnIndex;
		}
		
		public int getDataType() {
			return dataType;
		}
	}
}
