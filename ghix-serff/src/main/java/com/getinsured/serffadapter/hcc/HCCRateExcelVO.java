package com.getinsured.serffadapter.hcc;

/**
 * Class is used to generate getter/setter on the basis of HCC Rates Excel Column.
 * 
 * @author Ritesh
 * @since Jan 14, 2015
 */
public class HCCRateExcelVO {

	private String planState;
	private String hiosId;
	private String planNumber;
	private String deductible;
	private String issuerName;
	private String minAge;
	private String maxAge;
	private String maleRate;
	private String femaleRate;
	private String childRate;
	private String planMaxCoverage;
	private String effectiveStartDate;
	
	private boolean isNotValid;
	private String errorMessages;

	public HCCRateExcelVO() {
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getHiosId() {
		return hiosId;
	}

	public void setHiosId(String hiosId) {
		this.hiosId = hiosId;
	}

	public String getPlanNumber() {
		return planNumber;
	}

	public void setPlanNumber(String planNumber) {
		this.planNumber = planNumber;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getPlanState() {
		return planState;
	}

	public void setPlanState(String planState) {
		this.planState = planState;
	}

	public String getMinAge() {
		return minAge;
	}

	public void setMinAge(String minAge) {
		this.minAge = minAge;
	}

	public String getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(String maxAge) {
		this.maxAge = maxAge;
	}

	public String getMaleRate() {
		return maleRate;
	}

	public void setMaleRate(String maleRate) {
		this.maleRate = maleRate;
	}

	public String getFemaleRate() {
		return femaleRate;
	}

	public void setFemaleRate(String femaleRate) {
		this.femaleRate = femaleRate;
	}

	public String getChildRate() {
		return childRate;
	}

	public void setChildRate(String childRate) {
		this.childRate = childRate;
	}

	public String getPlanMaxCoverage() {
		return planMaxCoverage;
	}

	public void setPlanMaxCoverage(String planMaxCoverage) {
		this.planMaxCoverage = planMaxCoverage;
	}

	public String getEffectiveStartDate() {
		return effectiveStartDate;
	}

	public void setEffectiveStartDate(String effectiveStartDate) {
		this.effectiveStartDate = effectiveStartDate;
	}

	public boolean isNotValid() {
		return isNotValid;
	}

	public void setNotValid(boolean isNotValid) {
		this.isNotValid = isNotValid;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "HCCRateExcelVO [planState=" + planState + ", hiosId=" + hiosId + ", planNumber=" + planNumber
				+ ", deductible=" + deductible + ", issuerName=" + issuerName + ", minAge=" + minAge + ", maxAge="
				+ maxAge + ", maleRate=" + maleRate + ", femaleRate=" + femaleRate + ", childRate=" + childRate
				+ ", planMaxCoverage=" + planMaxCoverage + ", effectiveStartDate=" + effectiveStartDate
				+ ", isNotValid=" + isNotValid + ", errorMessages=" + errorMessages + "]";
	}
	
	
	
}
