package com.getinsured.serffadapter.medicaid;

import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.PLAN_INFO_START_COL;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.SERVICE_AREA_EXT_START_ROW;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.SERVICE_AREA_START_COL;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidBenefitColumnEnum.ADULTS;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidBenefitColumnEnum.BENEFITS;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidBenefitColumnEnum.CHILDREN;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidBenefitColumnEnum.CHILDREN_AGE_19_20;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidOtherServiceColumnEnum.SERVICE_ATTRIBUTE;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidOtherServiceColumnEnum.SERVICE_NAME;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidOtherServiceColumnEnum.SERVICE_TYPE;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.COUNTIES_SERVED;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.ISSUER_HIOS_ID;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.ISSUER_NAME;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.MEMBER_NUMBER;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.PLAN_END_DATE;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.PLAN_NAME;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.PLAN_NUMBER;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.PLAN_START_DATE;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.PLAN_STATE;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.QUALITY_RATING;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidPlanInfoRowEnum.WEBSITE_URL;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidServiceAreaColumnEnum.COUNTY_NAME;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidServiceAreaColumnEnum.PARTIAL_COUNTY;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidServiceAreaColumnEnum.ZIPCODE_LIST;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidServiceAreaRowEnum.ENTIRE_STATE;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidServiceAreaRowEnum.SERVICE_AREA_ID;
import static com.getinsured.serffadapter.medicaid.MedicaidExcelData.MedicaidServiceAreaRowEnum.SERVICE_AREA_NAME;
import static com.serff.util.SerffConstants.FTP_PLAN_MEDICAID_FILE;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.serffadapter.excel.ExcelReader;
import com.serff.service.SerffService;
import com.serff.service.templates.MedicaidPlanMgmtSerffService;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

/**
 * Class is used to read Medicaid Excel data to PlanDocumnetExcelVO DTO Objects.
 * 
 * @author Bhavin Parmar
 * @since January 15, 2015
 */
@Service("medicaidExcelReader")
public class MedicaidExcelReader extends ExcelReader {

	private static final Logger LOGGER = Logger.getLogger(MedicaidExcelReader.class);

	private static final String EMSG_INVALID_EXCEL = "Invalid Medicaid excel template.";
	private static final String EMSG_LOAD_PLANS = "Failed to load Medicaid Plans. Reason: ";
	private static final String EMSG_ERROR_ECM = "Failed to upload Medicaid Excel at ECM. Reason: ";
	private static final String EMSG_NO_EXCEL_FOUND = "No Medicaid Excel Template is available for processing.";
	private static final int ERROR_MAX_LEN = 1000;

	private static final String SHEET_PLAN_INFO = "Plan_Info";
	private static final String SHEET_PLAN_BENEFITS = "Plan_Benefits";
	private static final String SHEET_OTHER_SERVICES = "Other_Services";
	private static final String SHEET_SERVICE_AREA = "Service Area";
	private static final int SHEET_INDEX_PLAN_INFO = 0;
	private static final int SHEET_INDEX_PLAN_BENEFITS = 1;
	private static final int SHEET_INDEX_OTHER_SERVICES = 2;
	private static final int SHEET_INDEX_SERVICE_AREA = 3;

	@Autowired private SerffService serffService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private SerffUtils serffUtils;
	@Autowired private SerffResourceUtil serffResourceUtil;
	@Autowired private MedicaidPlanMgmtSerffService medicaidPlanMgmtSerffService;

	/**
	 * Method is used process uploaded Medicaid Excel.
	 */
	public String processMedicaidExcelData(String serffReqId, String folderPath) {

		LOGGER.info("processMedicaidExcelData() Start with folder Path:" + folderPath);
		StringBuilder message = new StringBuilder();
		SerffPlanMgmt trackingRecord = null;
		SerffPlanMgmtBatch trackingBatchRecord = null;

		try {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SERFF Request ID: " + SecurityUtil.sanitizeForLogging(serffReqId));
				LOGGER.debug("Medicaid FTP Folder Path: " + folderPath);
			}

			if (!StringUtils.isNumeric(serffReqId) || StringUtils.isBlank(folderPath) ) {
				message.append(EMSG_NO_EXCEL_FOUND);
				return message.toString();
			}

			// Get Tracking record from SERFF Plan Management table
			trackingRecord = getTrackingRecord(serffReqId, message);

			if (null != trackingRecord
					&& StringUtils.isNotBlank(trackingRecord.getAttachmentsList())
					&& trackingRecord.getAttachmentsList().contains(FTP_PLAN_MEDICAID_FILE)) {

				LOGGER.debug("Medicaid File Name: " + folderPath + "/" + trackingRecord.getAttachmentsList());
				trackingBatchRecord = serffService.getTrackingBatchRecord(trackingRecord.getAttachmentsList(), FTP_PLAN_MEDICAID_FILE, message);
				// Get Plan Document Excel from FTP server
				if(trackingBatchRecord != null) {
					loadExcelTemplate(trackingRecord, folderPath, message);
				} else {
					message.append("Failed to get Medicaid Batch tracking record.");
				}
			}
			else if (null != trackingRecord) {
				message.append(EMSG_NO_EXCEL_FOUND);
			}
		}
		finally {

			if (null != ftpClient) {
				ftpClient.close();
			}
			SerffPlanMgmtBatch.BATCH_STATUS status = null;
			if (null != trackingRecord && trackingRecord.getRequestStatus().equals(SerffPlanMgmt.REQUEST_STATUS.S)) {
				//set status as success
				status = SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED;
			} else {
				status = SerffPlanMgmtBatch.BATCH_STATUS.FAILED;
			}			
			serffService.updateTrackingBatchRecordAsCompleted(trackingBatchRecord, trackingRecord, status);
			LOGGER.info("processMedicaidExcelData() End with message: " + message);
		}
		return message.toString();
	}

	private void loadExcelTemplate(SerffPlanMgmt trackingRecord, String folderPath, StringBuilder message) {

		LOGGER.debug("loadExcelTemplate() Start");
		boolean isSuccess = false;
		InputStream inputStream = null;

		try {
			inputStream = ftpClient.getFileInputStream(folderPath + trackingRecord.getAttachmentsList());

			if (null == inputStream) {
				trackingRecord.setPmResponseXml("Medicaid Excel is not found at FTP server.");
				trackingRecord.setRequestStateDesc(trackingRecord.getPmResponseXml());
				message.append(trackingRecord.getPmResponseXml());
				return;
			}

			// Upload Excel File to ECM
			isSuccess = addExcelInECM(message, trackingRecord, folderPath, trackingRecord.getAttachmentsList());
			if (!isSuccess) {
				trackingRecord.setPmResponseXml("Failed upload Excel file to ECM.");
				trackingRecord.setRequestStateDesc(trackingRecord.getPmResponseXml());
				message.append(trackingRecord.getPmResponseXml());
				return;
			}
			isSuccess = false;
			// Load Excel File
			loadFile(inputStream);

			if (!excelValidations()) {
				trackingRecord.setPmResponseXml(EMSG_INVALID_EXCEL);
				trackingRecord.setRequestStateDesc(EMSG_INVALID_EXCEL);
				message.append(EMSG_INVALID_EXCEL);
				return;
			}
			// Get first sheet from the workbook
			MedicaidPlanInfoVO planInfoVO = getPlanInfoVO(getSheet(SHEET_INDEX_PLAN_INFO)); // Get MedicaidPlanInfoVO Object
			planInfoVO.setPlanBenefitsList(getPlanBenefitsSheet(getSheet(SHEET_INDEX_PLAN_BENEFITS))); // Get List of MedicaidPlanBenefitsVO Object
			planInfoVO.setOtherServicesList(getOtherServicesSheet(getSheet(SHEET_INDEX_OTHER_SERVICES))); // Get List of MedicaidOtherServicesVO Object
			planInfoVO.setServiceAreaVO(getServiceAreaSheet(getSheet(SHEET_INDEX_SERVICE_AREA))); // Get MedicaidServiceAreaVO Object

			isSuccess = medicaidPlanMgmtSerffService.populateAndPersistMedicaidData(planInfoVO, trackingRecord);
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			message.append(EMSG_LOAD_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			updateTrackingRecord(isSuccess, trackingRecord, message);
			moveFileToDirectory(isSuccess, trackingRecord.getAttachmentsList());
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("loadExcelTemplate() End");
		}
	}

	/**
	 * Method is used to get PlanBenefitsSheet data.
	 */
	private List<MedicaidPlanBenefitsVO> getPlanBenefitsSheet(XSSFSheet planBenefitsSheet) {

		LOGGER.debug("getPlanBenefitsSheet() Start");
		List<MedicaidPlanBenefitsVO> planBenefitsList = null;

		try {

			if (null == planBenefitsSheet || null == planBenefitsSheet.iterator()) {
				return planBenefitsList;
			}
			Iterator<Row> rowIterator = planBenefitsSheet.iterator();
			planBenefitsList = new ArrayList<MedicaidPlanBenefitsVO>();
			MedicaidPlanBenefitsVO planBenefitsVO;

			boolean isFirst = true;
			Row excelRow = null;

			while (rowIterator.hasNext()) {
				excelRow = rowIterator.next();

				if (isFirst) {
					isFirst = false;
					continue;
				}
				planBenefitsVO = getPlanBenefitsVO(excelRow);

				if (null != planBenefitsVO) {
					planBenefitsList.add(planBenefitsVO);
				}
			}
//			LOGGER.info("planBenefitsList is null: " + (null == planBenefitsList));
		}
		finally {
			LOGGER.debug("getPlanBenefitsSheet() End");
		}
		return planBenefitsList;
	}

	/**
	 * Method is used to get OtherServicesSheet data.
	 */
	private List<MedicaidOtherServicesVO> getOtherServicesSheet(XSSFSheet otherServicesSheet) {

		LOGGER.debug("getOtherServicesSheet() Start");
		List<MedicaidOtherServicesVO> otherServicesList = null;

		try {

			if (null == otherServicesSheet || null == otherServicesSheet.iterator()) {
				return otherServicesList;
			}
			Iterator<Row> rowIterator = otherServicesSheet.iterator();
			otherServicesList = new ArrayList<MedicaidOtherServicesVO>();
			MedicaidOtherServicesVO planOtherServices;

			boolean isFirst = true;
			Row excelRow = null;

			while (rowIterator.hasNext()) {
				excelRow = rowIterator.next();

				if (isFirst) {
					isFirst = false;
					continue;
				}
				planOtherServices = getOtherServicesVO(excelRow);

				if (null != planOtherServices) {
					otherServicesList.add(planOtherServices);
				}
			}
//			LOGGER.info("otherServicesList is null: " + (null == otherServicesList));
		}
		finally {
			LOGGER.debug("getOtherServicesSheet() End");
		}
		return otherServicesList;
	}

	/**
	 * Method is used to get ServiceAreaSheet data.
	 */
	private MedicaidServiceAreaVO getServiceAreaSheet(XSSFSheet serviceAreaSheet) {

		LOGGER.debug("getServiceAreaSheet() Start");
		MedicaidServiceAreaVO serviceAreaVO = null;

		try {

			if (null == serviceAreaSheet || null == serviceAreaSheet.iterator()) {
				return serviceAreaVO;
			}

			serviceAreaVO = getServiceAreaVO(serviceAreaSheet); // Get MedicaidServiceAreaVO Object
			Iterator<Row> rowIterator = serviceAreaSheet.iterator();
			List<MedicaidServiceAreaExtVO> serviceAreaExtList = new ArrayList<MedicaidServiceAreaExtVO>();
			MedicaidServiceAreaExtVO serviceAreaExtVO;

			int row = 0;
			Row excelRow = null;

			while (rowIterator.hasNext()) {
				excelRow = rowIterator.next();

				if (SERVICE_AREA_EXT_START_ROW >= row) {
					row++;
					continue;
				}
				serviceAreaExtVO = getServiceAreaExtVO(excelRow);

				if (null != serviceAreaExtVO) {
					serviceAreaExtList.add(serviceAreaExtVO);
				}
			}
//			LOGGER.info("serviceAreaExtList is null: " + (null == serviceAreaExtList));
			LOGGER.info("serviceAreaVO is null: " + (null == serviceAreaVO));

			if (null != serviceAreaVO) {
				serviceAreaVO.setServiceAreaExtList(serviceAreaExtList);
			}
		}
		finally {
			LOGGER.debug("getServiceAreaSheet() End");
		}
		return serviceAreaVO;
	}

	/**
	 * Method is used to store Medicaid Plan Info Excel sheet data in MedicaidPlanInfoVO Object.
	 */
	private MedicaidPlanInfoVO getPlanInfoVO(XSSFSheet planInfoSheet) {

		MedicaidPlanInfoVO planInfoVO = null;

		if (null != planInfoSheet) {
			planInfoVO = new MedicaidPlanInfoVO();
			planInfoVO.setIssuerName(getCellValueTrim(planInfoSheet.getRow(ISSUER_NAME.getRowIndex()).getCell(PLAN_INFO_START_COL)));
			planInfoVO.setIssuerHiosId(getCellValueTrim(planInfoSheet.getRow(ISSUER_HIOS_ID.getRowIndex()).getCell(PLAN_INFO_START_COL), true));
			planInfoVO.setPlanName(getCellValueTrim(planInfoSheet.getRow(PLAN_NAME.getRowIndex()).getCell(PLAN_INFO_START_COL)));
			planInfoVO.setPlanNumber(getCellValueTrim(planInfoSheet.getRow(PLAN_NUMBER.getRowIndex()).getCell(PLAN_INFO_START_COL)));
			planInfoVO.setPlanStartDate(getCellValueTrim(planInfoSheet.getRow(PLAN_START_DATE.getRowIndex()).getCell(PLAN_INFO_START_COL)));
			planInfoVO.setPlanEndDate(getCellValueTrim(planInfoSheet.getRow(PLAN_END_DATE.getRowIndex()).getCell(PLAN_INFO_START_COL)));
			planInfoVO.setMemberNumber(getCellValueTrim(planInfoSheet.getRow(MEMBER_NUMBER.getRowIndex()).getCell(PLAN_INFO_START_COL)));
			planInfoVO.setWebsiteUrl(getCellValueTrim(planInfoSheet.getRow(WEBSITE_URL.getRowIndex()).getCell(PLAN_INFO_START_COL)));
			planInfoVO.setQualityRating(getCellValueTrim(planInfoSheet.getRow(QUALITY_RATING.getRowIndex()).getCell(PLAN_INFO_START_COL)));
			planInfoVO.setCountiesServed(getCellValueTrim(planInfoSheet.getRow(COUNTIES_SERVED.getRowIndex()).getCell(PLAN_INFO_START_COL)));
			planInfoVO.setPlanState(getCellValueTrim(planInfoSheet.getRow(PLAN_STATE.getRowIndex()).getCell(PLAN_INFO_START_COL)));
		}
		return planInfoVO;
	}

	/**
	 * Method is used to store Medicaid Plan Benefits Excel sheet data in MedicaidPlanBenefitsVO Object.
	 */
	private MedicaidPlanBenefitsVO getPlanBenefitsVO(Row excelRow) {

		MedicaidPlanBenefitsVO planBenefitsVO = null;

		if (null != excelRow) {
			planBenefitsVO = new MedicaidPlanBenefitsVO();
			planBenefitsVO.setBenefits(getCellValueTrim(excelRow.getCell(BENEFITS.getColumnIndex())));
//			planBenefitsVO.setKeyName(getCellValueTrim(excelRow.getCell(KEY_NAME.getColumnIndex())));
			planBenefitsVO.setChildren(getCellValueTrim(excelRow.getCell(CHILDREN.getColumnIndex())));
			planBenefitsVO.setChildrenAgeNinteenToTwenty(getCellValueTrim(excelRow.getCell(CHILDREN_AGE_19_20.getColumnIndex())));
			planBenefitsVO.setAdults(getCellValueTrim(excelRow.getCell(ADULTS.getColumnIndex())));
		}
		return planBenefitsVO;
	}

	/**
	 * Method is used to store Medicaid Other Services Excel sheet data in MedicaidPlanBenefitsVO Object.
	 */
	private MedicaidOtherServicesVO getOtherServicesVO(Row excelRow) {

		MedicaidOtherServicesVO planOtherServices = null;

		if (null != excelRow) {
			planOtherServices = new MedicaidOtherServicesVO();
			planOtherServices.setServiceType(getCellValueTrim(excelRow.getCell(SERVICE_TYPE.getColumnIndex())));
			planOtherServices.setServiceName(getCellValueTrim(excelRow.getCell(SERVICE_NAME.getColumnIndex())));
			planOtherServices.setServiceAttributes(getCellValueTrim(excelRow.getCell(SERVICE_ATTRIBUTE.getColumnIndex())));
		}
		return planOtherServices;
	}

	/**
	 * Method is used to store Medicaid Service Area Excel sheet data in MedicaidPlanBenefitsVO Object.
	 */
	private MedicaidServiceAreaVO getServiceAreaVO(XSSFSheet medicaidServiceAreaSheet) {

		MedicaidServiceAreaVO planServiceAreaVO = null;

		if (null != medicaidServiceAreaSheet) {
			planServiceAreaVO = new MedicaidServiceAreaVO();
			planServiceAreaVO.setServiceAreaId(getCellValueTrim(medicaidServiceAreaSheet.getRow(SERVICE_AREA_ID.getRowIndex()).getCell(SERVICE_AREA_START_COL)));
			planServiceAreaVO.setServiceAreaName(getCellValueTrim(medicaidServiceAreaSheet.getRow(SERVICE_AREA_NAME.getRowIndex()).getCell(SERVICE_AREA_START_COL)));
			planServiceAreaVO.setEntireState(getCellValueTrim(medicaidServiceAreaSheet.getRow(ENTIRE_STATE.getRowIndex()).getCell(SERVICE_AREA_START_COL)));
		}
		return planServiceAreaVO;
	}

	/**
	 * Method is used to store Medicaid Other Services Excel sheet data in MedicaidPlanBenefitsVO Object.
	 */
	private MedicaidServiceAreaExtVO getServiceAreaExtVO(Row excelRow) {

		MedicaidServiceAreaExtVO planOtherServicesExt = null;

		if (null != excelRow) {
			planOtherServicesExt = new MedicaidServiceAreaExtVO();
			planOtherServicesExt.setCountyName(getCellValueTrim(excelRow.getCell(COUNTY_NAME.getColumnIndex())));
			planOtherServicesExt.setPartialcounty(getCellValueTrim(excelRow.getCell(PARTIAL_COUNTY.getColumnIndex())));
			planOtherServicesExt.setZiplist(getCellValueTrim(excelRow.getCell(ZIPCODE_LIST.getColumnIndex()), true));
		}
		return planOtherServicesExt;
	}

	/**
	 * Method is used to validated Excel Sheets.
	 */
	private boolean excelValidations() {

		LOGGER.debug("excelValidations() Start");
		boolean isValid = true;

		try {
			isValid = isValidSheet(SHEET_INDEX_PLAN_INFO, SHEET_PLAN_INFO);
			isValid = isValid && isValidSheet(SHEET_INDEX_PLAN_BENEFITS, SHEET_PLAN_BENEFITS);
			isValid = isValid && isValidSheet(SHEET_INDEX_OTHER_SERVICES, SHEET_OTHER_SERVICES);
			isValid = isValid && isValidSheet(SHEET_INDEX_SERVICE_AREA, SHEET_SERVICE_AREA);
		}
		finally {
			LOGGER.debug("excelValidations() End");
		}
		return isValid;
	}

	/**
	 * Method is used to get Tracking Record  from SERFF Plan Management Table.
	 */
	private SerffPlanMgmt getTrackingRecord(String serffReqId, StringBuilder message) {

		LOGGER.debug("getTrackingRecord() Strat");
		SerffPlanMgmt trackingRecord = null;

		if (StringUtils.isNumeric(serffReqId)) {
			// Get tracking record from SERFF_PLAN_MGMT table. 
			trackingRecord = serffService.getSerffPlanMgmtById(Long.parseLong(serffReqId));

			if (null == trackingRecord) {
				message.append("Tracking record is not available for serffReqId: ");
				message.append(serffReqId);
			}
		}
		else {
			message.append("Tracking record Id is not available for serffReqId: ");
			message.append(serffReqId);
		}
		LOGGER.debug("getTrackingRecord() End");
		return trackingRecord;
	}

	/**
	 * Method is used to update tracking record of SERFF Plan Management table.
	 */
	private void updateTrackingRecord(boolean isSuccess, SerffPlanMgmt trackingRecord, StringBuilder message) {

		LOGGER.debug("updateTrackingRecord() Start");

		try {
			if (null != trackingRecord) {

				if (isSuccess) {
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
				}
				else {
					trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
				}

				if (StringUtils.isNotBlank(message.toString())) {
					trackingRecord.setRequestStateDesc(message.toString());
				}

				if (trackingRecord.getRequestStateDesc().length() > ERROR_MAX_LEN) {
					trackingRecord.setRequestStateDesc(trackingRecord.getRequestStateDesc().substring(0, ERROR_MAX_LEN));
				}

				if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
					trackingRecord.setPmResponseXml(trackingRecord.getRequestStateDesc());
				}
				trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
				trackingRecord.setEndTime(new Date());
				serffService.saveSerffPlanMgmt(trackingRecord);
			}
			else {
				message.append("Tracking Record is not found.");
			}
		}
		finally {
			LOGGER.debug("updateTrackingRecord() Start");
		}
	}

	/**
	 * Method is used to upload Excel at ECM server.
	 */
	private boolean addExcelInECM(StringBuilder message, SerffPlanMgmt trackingRecord, String folderPath, String fileName) throws IOException {
		
		LOGGER.debug("addExcelInECM() Strat");
		boolean isSuccess = false;
		InputStream inputStream = null;

		try {
			inputStream = ftpClient.getFileInputStream(folderPath + fileName);
			LOGGER.debug("Uploading File Name with path to ECM:" + SerffConstants.SERF_ECM_BASE_PATH + FTP_PLAN_MEDICAID_FILE + fileName);
			// Uploading Excel at ECM.
			SerffDocument attachment = serffUtils.addAttachmentInECM(ecmService, trackingRecord, SerffConstants.SERF_ECM_BASE_PATH, FTP_PLAN_MEDICAID_FILE, fileName, IOUtils.toByteArray(inputStream), true);
			serffService.saveSerffDocument(attachment);
			isSuccess = true;
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			message.append(EMSG_ERROR_ECM + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("addExcelInECM() End");
		}
		return isSuccess;
	}

	/**
	 * Method is used to Move File from uploadPlansDocs to success/error directory.
	 */
	private void moveFileToDirectory(boolean isSuccess, String fileName) {

		LOGGER.debug("moveFileToDirectory() Start");
		String moveToDirectoryPath = null;

		try {
			String baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getPlansMedicaidUploadPath();
			moveToDirectoryPath = baseDirectoryPath;

			if (isSuccess) {
				moveToDirectoryPath += SerffConstants.FTP_SUCCESS + SerffConstants.PATH_SEPERATOR;
			}
			else {
				moveToDirectoryPath += SerffConstants.FTP_ERROR + SerffConstants.PATH_SEPERATOR;
			}

			if (!ftpClient.isRemoteDirectoryExist(moveToDirectoryPath)) {
				ftpClient.createDirectory(moveToDirectoryPath);
			}
			LOGGER.debug("FTP Folder Path: " + baseDirectoryPath);
			LOGGER.info("Move To Directory Path: " + moveToDirectoryPath);
			ftpClient.moveFile(baseDirectoryPath, fileName, moveToDirectoryPath, fileName);
		}
		catch (Exception ex) {
			LOGGER.info("Skipping file beacause it is already exist in directory: " + moveToDirectoryPath);
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("moveFileToDirectory() End");
		}
	}
}
