package com.getinsured.serffadapter.medicaid;

/**
 * Class is used to generate getter/setter on the basis of Service Area sheet
 * for Medicaid Excel.
 * 
 * @author Vani Sharma
 * @since January 15, 2015
 */
public class MedicaidServiceAreaExtVO {

	private String countyName;
	private String partialcounty;
	private String ziplist;

	public MedicaidServiceAreaExtVO() {
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getPartialcounty() {
		return partialcounty;
	}

	public void setPartialcounty(String partialcounty) {
		this.partialcounty = partialcounty;
	}

	public String getZiplist() {
		return ziplist;
	}

	public void setZiplist(String ziplist) {
		this.ziplist = ziplist;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("MedicaidServiceAreaExtVO [countyName=");
		sb.append(countyName);
		sb.append(", partialcounty=");
		sb.append(partialcounty);
		sb.append(", ziplist=");
		sb.append(ziplist);
		return sb.toString();
	}
}
