package com.getinsured.serffadapter.medicaid;

import java.util.List;

/**
 * Class is used to generate getter/setter on the basis of Service Area sheet
 * for Medicaid Excel.
 * 
 * @author Vani Sharma
 * @since January 15, 2015
 */
public class MedicaidServiceAreaVO {

	private String serviceAreaId;
	private String serviceAreaName;
	private String entireState;

	private List<MedicaidServiceAreaExtVO> serviceAreaExtList;

	public MedicaidServiceAreaVO() {
	}

	public String getServiceAreaId() {
		return serviceAreaId;
	}

	public void setServiceAreaId(String serviceAreaId) {
		this.serviceAreaId = serviceAreaId;
	}

	public String getServiceAreaName() {
		return serviceAreaName;
	}

	public void setServiceAreaName(String serviceAreaName) {
		this.serviceAreaName = serviceAreaName;
	}

	public String getEntireState() {
		return entireState;
	}

	public void setEntireState(String entireState) {
		this.entireState = entireState;
	}

	public List<MedicaidServiceAreaExtVO> getServiceAreaExtList() {
		return serviceAreaExtList;
	}

	public void setServiceAreaExtList(List<MedicaidServiceAreaExtVO> serviceAreaExtList) {
		this.serviceAreaExtList = serviceAreaExtList;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("MedicaidServiceAreaVO [serviceAreaId=");
		sb.append(serviceAreaId);
		sb.append(", serviceAreaName=");
		sb.append(serviceAreaName);
		sb.append(", entireState=");
		sb.append(entireState);
		return sb.toString();
	}
}
