package com.getinsured.serffadapter.medicaid;

/**
 * Class is used to generate Enum on the basis of Plans Info, Plan Benefits, Other Services
 * and Service Area sheets of Medicaid Excel.
 * 
 * @author Vani Sharma
 * @since January 15, 2015
 */
public class MedicaidExcelData {

	private static final int STRING_TYPE = 1;
	public static final int PLAN_INFO_START_COL = 1;
	public static final int SERVICE_AREA_START_COL = 1;
	public static final int SERVICE_AREA_EXT_START_ROW = 4;

	public enum MedicaidPlanInfoRowEnum {

		ISSUER_NAME("Issuer Name", "ISSUER_NAME", 0, STRING_TYPE),
		ISSUER_HIOS_ID("Issuer HIOS ID", "ISSUER_HIOS_ID", 1, STRING_TYPE),
		PLAN_NAME("Plan Name", "PLAN_NAME", 2, STRING_TYPE),
		PLAN_NUMBER("Plan Number", "PLAN_NUMBER", 3, STRING_TYPE),
		PLAN_START_DATE("Plan Start Date", "PLAN_START_DATE", 4, STRING_TYPE),
		PLAN_END_DATE("Plan End Date", "PLAN_END_DATE", 5, STRING_TYPE),
		MEMBER_NUMBER("Member Number", "MEMBER_NUMBER", 6, STRING_TYPE),
		WEBSITE_URL("Website URL", "WEBSITE_URL", 7, STRING_TYPE),
		QUALITY_RATING("Quality Rating", "QUALITY_RATING", 8, STRING_TYPE),
		COUNTIES_SERVED("Counties Served", "COUNTIES_SERVED", 9, STRING_TYPE),
		PLAN_STATE("Plan State", "PLAN_STATE", 10, STRING_TYPE);

		private String rowName;
		private String rowConstName;
		private int rowIndex;
		private int dataType;

		private MedicaidPlanInfoRowEnum(String rowName, String rowConstName,
				int index, int dataType) {
			this.rowName = rowName;
			this.rowConstName = rowConstName;
			this.rowIndex = index;
			this.dataType = dataType;
		}

		public String getRowName() {
			return rowName;
		}

		public String getRowConstName() {
			return rowConstName;
		}

		public int getRowIndex() {
			return rowIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum MedicaidBenefitColumnEnum {

		BENEFITS("Benefits", "BENEFITS", 0, STRING_TYPE),
		KEY_NAME("Key Name", "KEY_NAME", 1, STRING_TYPE),
		CHILDREN("Children", "CHILDREN", 2, STRING_TYPE),
		CHILDREN_AGE_19_20("Children age 19-20", "CHILDREN_AGE_19_20", 3, STRING_TYPE),
		ADULTS("ADULTS", "Adults", 4, STRING_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private MedicaidBenefitColumnEnum(String columnName,
				String columnConstName, int index, int dataType) {
			this.columnName = columnName;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum MedicaidOtherServiceColumnEnum {

		SERVICE_TYPE("Service Type", "SERVICE_TYPE", 0, STRING_TYPE),
		SERVICE_NAME("Service Name", "SERVICE_NAME", 1, STRING_TYPE),
		SERVICE_ATTRIBUTE("Service Attribute", "SERVICE_ATTRIBUTE", 2, STRING_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private MedicaidOtherServiceColumnEnum(String columnName,
				String columnConstName, int index, int dataType) {
			this.columnName = columnName;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum MedicaidServiceAreaRowEnum {

		SERVICE_AREA_ID("Service Area ID", "SERVICE_AREA_ID", 0, STRING_TYPE),
		SERVICE_AREA_NAME("Service Area Name", "SERVICE_AREA_NAME", 1, STRING_TYPE),
		ENTIRE_STATE("Entire State", "ENTIRE_STATE", 2, STRING_TYPE);

		private String rowName;
		private String rowConstName;
		private int rowIndex;
		private int dataType;

		private MedicaidServiceAreaRowEnum(String rowName, String rowConstName,
				int index, int dataType) {
			this.rowName = rowName;
			this.rowConstName = rowConstName;
			this.rowIndex = index;
			this.dataType = dataType;
		}

		public String getRowName() {
			return rowName;
		}

		public String getRowConstName() {
			return rowConstName;
		}

		public int getRowIndex() {
			return rowIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum MedicaidServiceAreaColumnEnum {

		COUNTY_NAME("County Name", "COUNTY_NAME", 0, STRING_TYPE),
		PARTIAL_COUNTY("Partial County", "PARTIAL_COUNTY", 1, STRING_TYPE),
		ZIPCODE_LIST("Zip Codes List", "ZIPCODE_LIST", 2, STRING_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private MedicaidServiceAreaColumnEnum(String columnName,
				String columnConstName, int index, int dataType) {
			this.columnName = columnName;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}
}
