package com.getinsured.serffadapter.medicaid;

import java.util.List;

import com.serff.util.SerffUtils;

/**
 * Class is used to generate getter/setter on the basis of Plan Benefits sheet
 * for Medicaid Excel.
 * 
 * @author Vani Sharma
 * @since January 15, 2015
 */
public class MedicaidPlanInfoVO {

	private String issuerName;
	private String issuerHiosId;
	private String planName;
	private String planNumber;
	private String planStartDate;
	private String planEndDate;
	private String memberNumber;
	private String websiteUrl;
	private String qualityRating;
	private String countiesServed;
	private String planState;

	private List<MedicaidPlanBenefitsVO> planBenefitsList;
	private List<MedicaidOtherServicesVO> otherServicesList;
	private MedicaidServiceAreaVO serviceAreaVO;

	private boolean isNotValid;
	private String errorMessages;

	public MedicaidPlanInfoVO() {
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getIssuerHiosId() {
		return issuerHiosId;
	}

	public void setIssuerHiosId(String issuerHiosId) {
		this.issuerHiosId = issuerHiosId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanNumber() {
		return planNumber;
	}

	public void setPlanNumber(String planNumber) {
		this.planNumber = planNumber;
	}

	public String getPlanStartDate() {
		return planStartDate;
	}

	public void setPlanStartDate(String planStartDate) {
		this.planStartDate = planStartDate;
	}

	public String getPlanEndDate() {
		return planEndDate;
	}

	public void setPlanEndDate(String planEndDate) {
		this.planEndDate = planEndDate;
	}

	public String getMemberNumber() {
		return memberNumber;
	}

	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = SerffUtils.checkAndcorrectURL(websiteUrl);
	}

	public String getQualityRating() {
		return qualityRating;
	}

	public void setQualityRating(String qualityRating) {
		this.qualityRating = qualityRating;
	}

	public String getCountiesServed() {
		return countiesServed;
	}

	public void setCountiesServed(String countiesServed) {
		this.countiesServed = countiesServed;
	}

	public String getPlanState() {
		return planState;
	}

	public void setPlanState(String planState) {
		this.planState = planState;
	}

	public List<MedicaidPlanBenefitsVO> getPlanBenefitsList() {
		return planBenefitsList;
	}

	public void setPlanBenefitsList(List<MedicaidPlanBenefitsVO> planBenefitsList) {
		this.planBenefitsList = planBenefitsList;
	}

	public List<MedicaidOtherServicesVO> getOtherServicesList() {
		return otherServicesList;
	}

	public void setOtherServicesList(List<MedicaidOtherServicesVO> otherServicesList) {
		this.otherServicesList = otherServicesList;
	}

	public MedicaidServiceAreaVO getServiceAreaVO() {
		return serviceAreaVO;
	}

	public void setServiceAreaVO(MedicaidServiceAreaVO serviceAreaVO) {
		this.serviceAreaVO = serviceAreaVO;
	}

	public boolean isNotValid() {
		return isNotValid;
	}

	public void setNotValid(boolean isNotValid) {
		this.isNotValid = isNotValid;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("MedicaidPlanInfoVO [issuerName=");
		sb.append(issuerName);
		sb.append(", issuerHiosId=");
		sb.append(issuerHiosId);
		sb.append(", planName=");
		sb.append(planName);
		sb.append(", planNumber=");
		sb.append(planNumber);
		sb.append(", planStartDate=");
		sb.append(planStartDate);
		sb.append(", planEndDate=");
		sb.append(planEndDate);
		sb.append(", memberNumber=");
		sb.append(memberNumber);
		sb.append(", websiteUrl=");
		sb.append(websiteUrl);
		sb.append(", qualityRating=");
		sb.append(qualityRating);
		sb.append(", countiesServed=");
		sb.append(countiesServed);
		sb.append(", planState=");
		sb.append(planState);
		return sb.toString();
	}
}
