package com.getinsured.serffadapter.medicaid;

/**
 * Class is used to generate getter/setter on the basis of Other Services sheet
 * for Medicaid Excel.
 * 
 * @author Vani Sharma
 * @since January 15, 2015
 */
public class MedicaidOtherServicesVO {

	private String serviceType;
	private String serviceName;
	private String serviceAttributes;

	public MedicaidOtherServicesVO() {
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceAttributes() {
		return serviceAttributes;
	}

	public void setServiceAttributes(String serviceAttributes) {
		this.serviceAttributes = serviceAttributes;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("MedicaidOtherServicesVO [serviceType=");
		sb.append(serviceType);
		sb.append(", serviceName=");
		sb.append(serviceName);
		sb.append(", serviceAttributes=");
		sb.append(serviceAttributes);
		return sb.toString();
	}
}
