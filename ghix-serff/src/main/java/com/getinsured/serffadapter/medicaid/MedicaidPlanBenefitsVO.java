package com.getinsured.serffadapter.medicaid;

/**
 * Class is used to generate getter/setter on the basis of Plan Benefits sheet
 * for Medicaid Excel.
 * 
 * @author Vani Sharma
 * @since January 15, 2015
 */
public class MedicaidPlanBenefitsVO {

	private String benefits;
	private String keyName;
	private String children;
	private String childrenAgeNinteenToTwenty;
	private String adults;

	public MedicaidPlanBenefitsVO() {
	}

	public String getBenefits() {
		return benefits;
	}

	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}

	public String getKeyName() {
		return keyName;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}

	public String getChildren() {
		return children;
	}

	public void setChildren(String children) {
		this.children = children;
	}

	public String getChildrenAgeNinteenToTwenty() {
		return childrenAgeNinteenToTwenty;
	}

	public void setChildrenAgeNinteenToTwenty(String childrenAgeNinteenToTwenty) {
		this.childrenAgeNinteenToTwenty = childrenAgeNinteenToTwenty;
	}

	public String getAdults() {
		return adults;
	}

	public void setAdults(String adults) {
		this.adults = adults;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("MedicaidPlanBenefitsVO [benefits=");
		sb.append(benefits);
		sb.append(", keyName=");
		sb.append(keyName);
		sb.append(", children=");
		sb.append(children);
		sb.append(", childrenAgeNinteenToTwenty=");
		sb.append(childrenAgeNinteenToTwenty);
		sb.append(", adults=");
		sb.append(adults);
		return sb.toString();
	}
}
