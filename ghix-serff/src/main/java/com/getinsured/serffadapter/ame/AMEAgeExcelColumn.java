package com.getinsured.serffadapter.ame;

/**
 * Class is used to generate Enum on the basis of AME plan rates & AME Premium Factor sheets of AME Age Group Excel.
 * 
 * @author Bhavin Parmar
 * @since November 11, 2014
 */
public class AMEAgeExcelColumn {

	private static final int NUMBER_TYPE = 0;
	private static final int FLOAT_TYPE = 1;
	private static final int STRING_TYPE = 2;

	public enum AMEAgeRatesColumnEnum {

		STATE("State", "STATE", 0, STRING_TYPE),
		CARRIER_HIOS_ID("Carrier HIOS ID", "CARRIER_HIOS_ID", 1, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 2, STRING_TYPE),
		PLAN_NAME("Plan name", "PLAN_NAME", 3, STRING_TYPE),
		CARRIER_NAME("Carrier name", "CARRIER_NAME", 4, STRING_TYPE),
		BENEFIT_MAX("Benefit Max", "BENEFIT_MAX", 5, NUMBER_TYPE),
		DEDUCTIBLE("Deductible", "DEDUCTIBLE", 6, NUMBER_TYPE),
		MIN_AGE("Min Age", "MIN_AGE", 7, NUMBER_TYPE),
		MAX_AGE("Max Age", "MAX_AGE", 8, NUMBER_TYPE),
		APPLICANT("Applicant", "APPLICANT", 9, FLOAT_TYPE),
		SPOUSE("Spouse", "SPOUSE", 10, FLOAT_TYPE),
		CHILD("Child", "CHILD", 11, FLOAT_TYPE),
		APPLICANT_MALE("Applicant Male", "APPLICANT_MALE", 9, FLOAT_TYPE),
		APPLICANT_FEMALE("Applicant Female", "APPLICANT_FEMALE", 10, FLOAT_TYPE),
		SPOUSE_MALE("Spouse Male", "SPOUSE_MALE", 11, FLOAT_TYPE),
		SPOUSE_FEMALE("Spouse Female", "SPOUSE_FEMALE", 12, FLOAT_TYPE),
		CHILD_GENDERBASED("Child", "CHILD", 13, FLOAT_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private AMEAgeRatesColumnEnum(String columnName, String columnConstName, int index, int dataType) {
			this.columnName = columnName;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum AMEAgePremiumFactorColumnEnum {

		CARRIER_HIOS_ID("Carrier HIOS ID", "CARRIER_HIOS_ID", 0, STRING_TYPE),
		STATE("State", "STATE", 1, STRING_TYPE),
		ANNUAL("Annual", "PLAN_ID", 2, FLOAT_TYPE),
		SEMI_ANNUAL("Semi-Annual", "PLAN_NAME", 3, FLOAT_TYPE),
		QUARTERLY("Quarterly", "CARRIER_NAME", 4, FLOAT_TYPE),
		MONTHLY("Monthly", "BENEFIT_MAX", 5, FLOAT_TYPE);

		private String columnName;
		private String columnConstName;
		private int columnIndex;
		private int dataType;

		private AMEAgePremiumFactorColumnEnum(String columnName, String columnConstName, int index, int dataType) {
			this.columnName = columnName;
			this.columnConstName = columnConstName;
			this.columnIndex = index;
			this.dataType = dataType;
		}

		public String getColumnName() {
			return columnName;
		}

		public String getColumnConstName() {
			return columnConstName;
		}

		public int getColumnIndex() {
			return columnIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}
}
