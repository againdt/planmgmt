package com.getinsured.serffadapter.ame;

import static com.serff.util.SerffConstants.FTP_AME_AGE_FILE;
import static com.serff.util.SerffConstants.FTP_AME_FAMILY_FILE;
import static com.serff.util.SerffConstants.FTP_AME_GENDER_FILE;

import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.serffadapter.ame.AMEAgeExcelColumn.AMEAgePremiumFactorColumnEnum;
import com.getinsured.serffadapter.ame.AMEAgeExcelColumn.AMEAgeRatesColumnEnum;
import com.getinsured.serffadapter.ame.AMEExcelRow.AMEPlansExcelRowEnum;
import com.getinsured.serffadapter.ame.AMEExcelRow.AMERatesExcelRowEnum;
import com.getinsured.serffadapter.excel.ExcelReader;
import com.serff.service.SerffService;
import com.serff.service.templates.AMEPlanMgmtSerffService;
import com.serff.service.validation.AMEPlanDataValidator;
import com.serff.util.SerffConstants;
import com.serff.util.SerffResourceUtil;
import com.serff.util.SerffUtils;

/**
 * Class is used to read AME Plan Excel data to AMEExcelVO DTO Objects.
 * 
 * @author Bhavin Parmar
 * @since August 14, 2014
 */
@Service("amePlanReader")
public class AMEPlanReader extends ExcelReader {
	
	private static final Logger LOGGER = Logger.getLogger(AMEPlanReader.class);
	private static final String MSG_ERROR_AME_PLANS = "Failed to load AME Plans. Reason: ";
	private static final String MSG_AME_PLANS_SUCCESS = "AME Plans has been Loaded Successfully.";
	private static final String MSG_AME_PLANS_WARN = "AME Plans has been Loaded with warnings. Please see status message.";
	private static final String MSG_FILE = "File: ";

	private static final int PLAN_DATA_START_COLUMN = 2;
	private static final int RATE_DATA_START_COLUMN = 2;
	private static final int PLAN_DATA_SHEET_INDEX = 1;
	private static final int RATE_DATA_SHEET_INDEX = 2;
	private static final int PREMIUM_FACTOR_SHEET_INDEX = 3;
	private static final int ERROR_MAX_LEN = 1000;
	
	private static final String EMSG_DUPLICATE_PLAN = "Duplicate plan in the sheet ID: ";
	private static final String EMSG_INVALID_PLAN = "Invalid HIOS Plan Number: ";
	private static final String EMSG_MISSING_PLAN = "Missing plan in the Plans sheet ID: ";
	private static final String EMSG_GET_EACH_ROWS = "Get each rows from ";
	private static final String EMSG_NO_AME_EXCEL_FOUND = "No AME Excel Template is available for processing.";

	private static final String AME_PLAN_DATA_SHEET_NAME = "AME plan data";
	private static final String AME_PLAN_RATES_SHEET_NAME = "AME plan rates";
	private static final String AME_PREMIUM_FACTOR_SHEET_NAME = "AME Premium Factor";
	
	private static final String EMSG_AME = "AME Loading Error: ";
	
	@Autowired private GHIXSFTPClient ftpClient;
	@Autowired private AMEPlanMgmtSerffService amePlanMgmtSerffService;
	@Autowired private SerffService serffService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private SerffUtils serffUtils;
	@Autowired private AMEPlanDataValidator amePlanDataValidator;
	@Autowired private SerffResourceUtil serffResourceUtil;

	/**
	 * Method is used process uploaded AME Excel templates.
	 */
	public String processAllExcelTemplate(String folderPath, String planType) {

		LOGGER.info("AME processAllExcelTemplate() Start");
		StringBuilder message = new StringBuilder();

		try {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug(SecurityUtil.sanitizeForLogging("AME Plan FTP Folder Path: " + folderPath + " for planType: " + planType));
			}
			List<String> fileList = ftpClient.getFileNames(folderPath);
			LOGGER.debug("fileList is NULL: " + (null == fileList));

			if (null == fileList || fileList.isEmpty()) {
				message.append(EMSG_NO_AME_EXCEL_FOUND);
				return message.toString();
			}

			LOGGER.info("Number of Excel Template is " + fileList.size());
			boolean isEmpty = true;

			for (String fileName : fileList) {
				
				if (StringUtils.isBlank(fileName)
						|| SerffConstants.FTP_SUCCESS.equalsIgnoreCase(fileName)
						|| SerffConstants.FTP_ERROR.equalsIgnoreCase(fileName)) {
					continue;
				}
				boolean ifFamilyPlanTemplateExist = SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType) && fileName.contains(FTP_AME_FAMILY_FILE); 
				boolean ifAgePlanTemplateExist = SerffConstants.PLAN_TYPE.AME_AGE.name().equalsIgnoreCase(planType) && fileName.contains(FTP_AME_AGE_FILE); 
				boolean ifGenderPlanTemplateExist = SerffConstants.PLAN_TYPE.AME_GENDER.name().equalsIgnoreCase(planType) && fileName.contains(FTP_AME_GENDER_FILE); 
				if (!ifFamilyPlanTemplateExist && !ifAgePlanTemplateExist && !ifGenderPlanTemplateExist) {
					continue;
				}
				isEmpty = false;
				processFile(fileName, folderPath, message, planType);
			}

			if (isEmpty) {
				message.append(EMSG_NO_AME_EXCEL_FOUND);
			}
		}
		catch (Exception ex) {
			message.append(MSG_ERROR_AME_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			
			if (StringUtils.isEmpty(message.toString())) {
				message.append(MSG_AME_PLANS_SUCCESS);
			}
			LOGGER.info("AME processAllExcelTemplate() End - " + message);
		}
		return message.toString();
	}
	
	private void processFile(String fileName, String folderPath, StringBuilder message, String planType) {
		SerffPlanMgmt serffTrackingRecord = null;
		SerffPlanMgmtBatch trackingBatchRecord = null;
		LOGGER.debug("File Name: " + folderPath + "/" + fileName);
		//boolean isAMEFamilyRate = SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType);
		trackingBatchRecord = serffService.getTrackingBatchRecord(fileName, SerffConstants.FTP_AME_FILE_PREFIX, message);
		if(null != trackingBatchRecord) {
			trackingBatchRecord = serffService.updateTrackingBatchRecordAsInProgress(trackingBatchRecord);
			
			try {
				serffTrackingRecord = serffUtils.createSerffPlanMgmtRecord(getAMERequestType(planType),
						SerffPlanMgmt.REQUEST_STATUS.P, SerffPlanMgmt.REQUEST_STATE.W);
				if (null != serffTrackingRecord) {
					serffTrackingRecord.setAttachmentsList(fileName);
			        serffTrackingRecord.setRequestStateDesc("Uploaded succesfully and ready to load plans to DB.");
			        serffTrackingRecord = serffService.saveSerffPlanMgmt(serffTrackingRecord);
					loadAllExcelTemplate(serffTrackingRecord, fileName, folderPath, message, planType, trackingBatchRecord.getDefaultTenant());
				}
			} catch (UnknownHostException e) {
					message.append("Failed to create Serff Tracking Record.");
					LOGGER.error("Failed to create Serff Tracking Record: " + e.getMessage(), e);
			} finally {
				SerffPlanMgmtBatch.BATCH_STATUS status = null;
				if (null != serffTrackingRecord && serffTrackingRecord.getRequestStatus().equals(SerffPlanMgmt.REQUEST_STATUS.S)) {
					//set status as success
					status = SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED;
				} else {
					status = SerffPlanMgmtBatch.BATCH_STATUS.FAILED;
				}
				trackingBatchRecord = serffService.updateTrackingBatchRecordAsCompleted(trackingBatchRecord, serffTrackingRecord, status);
			}			
		}
	}
	
	private String getAMEPlanFileType(String planType) {
		String fileType;
		if(SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType)) {
			fileType = FTP_AME_FAMILY_FILE;
		} else if(SerffConstants.PLAN_TYPE.AME_AGE.name().equalsIgnoreCase(planType)) {
			fileType = FTP_AME_AGE_FILE;
		} else {
			fileType = FTP_AME_GENDER_FILE;
		}
		return fileType;
	}
	
	private String getAMERequestType(String planType) {
		String requestType;
		if(SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType)) {
			requestType = SerffConstants.TRANSFER_AME_FAMILY;
		} else if(SerffConstants.PLAN_TYPE.AME_AGE.name().equalsIgnoreCase(planType)) {
			requestType = SerffConstants.TRANSFER_AME_AGE;
		} else {
			requestType = SerffConstants.TRANSFER_AME_GENDER;
		}
		return requestType;
	}
	private void loadAllExcelTemplate(SerffPlanMgmt trackingRecord, String fileName, String folderPath, StringBuilder message, String planType, String defaultTenantIds) {
		LOGGER.debug("loadAllExcelTemplate() Start");
		boolean isSuccess = false;
		InputStream inputStream = null;

		try {
			inputStream = ftpClient.getFileInputStream(folderPath + fileName);
			
			if (null == inputStream) {
				trackingRecord.setPmResponseXml("AME Excel is not found at FTP server.");
				message.append(trackingRecord.getPmResponseXml());
				trackingRecord.setRequestStateDesc(message.toString());
				return;
			}
			
			// Upload Excel File to ECM
			isSuccess = addExcelInECM(message, trackingRecord, folderPath, fileName, planType);

			if (!isSuccess) {
				LOGGER.error("Failed upload Excel file to ECM.");
				return;
			}
			isSuccess = false;
			// Load Excel File
			loadFile(inputStream);
			// Get first sheet from the workbook
			XSSFSheet amePlanSheet = getSheet(PLAN_DATA_SHEET_INDEX); // Gets "AME plan data" sheet[1]
			XSSFSheet ameRateSheet = getSheet(RATE_DATA_SHEET_INDEX); // Gets "AME plan rates" sheet[2]
			XSSFSheet amePremimumFactorSheet = null;
			
			boolean isInvalidValidSheets = false;
			boolean isValidPlanSheet = null != amePlanSheet && AME_PLAN_DATA_SHEET_NAME.equalsIgnoreCase(amePlanSheet.getSheetName());
			boolean isValidRateSheet = null != ameRateSheet && AME_PLAN_RATES_SHEET_NAME.equalsIgnoreCase(ameRateSheet.getSheetName());
			boolean isAMEFamilyRate = SerffConstants.PLAN_TYPE.AME_FAMILY.name().equalsIgnoreCase(planType);
			
			if (isAMEFamilyRate) {
				isInvalidValidSheets = (!isValidPlanSheet || !isValidRateSheet);
			}
			else {
				amePremimumFactorSheet = getSheet(PREMIUM_FACTOR_SHEET_INDEX); // Gets "AME plan rates" sheet[3]
				
				boolean isValidPremiumFactorSheet = null != amePremimumFactorSheet && AME_PREMIUM_FACTOR_SHEET_NAME.equalsIgnoreCase(amePremimumFactorSheet.getSheetName());
				isInvalidValidSheets = (!isValidPlanSheet || !isValidRateSheet || !isValidPremiumFactorSheet);
			}
			
			if (isInvalidValidSheets) {
				
				if (isAMEFamilyRate) {
					trackingRecord.setPmResponseXml("Either " + AME_PLAN_DATA_SHEET_NAME + " sheet[1] or " + AME_PLAN_RATES_SHEET_NAME + " sheet[2] is missing !! \n");
				}
				else {
					trackingRecord.setPmResponseXml("Either " + AME_PLAN_DATA_SHEET_NAME + " sheet[1] or " + AME_PLAN_RATES_SHEET_NAME + " sheet[2] or "+ AME_PREMIUM_FACTOR_SHEET_NAME +" sheet[3] is missing !! \n");
				}
			}
			else {
				// Read and Persist AME data from Excel
				if (isAMEFamilyRate) {
					isSuccess = readAndPersistAMEFamilyRateData(amePlanSheet, ameRateSheet, trackingRecord, message, planType, defaultTenantIds);
				}
				else {
					Map<String, AMEExcelVO> amePlansMapData = readAndPersistAMEAgeGroupData(amePlanSheet, ameRateSheet, planType, message);
					Map<String, AMEPremiumFactorExcelVO> amePremiumFactorMapData = readAndPersistAMEPremimumFactorData(amePremimumFactorSheet, message);
					
					if (!CollectionUtils.isEmpty(amePlansMapData) && !CollectionUtils.isEmpty(amePremiumFactorMapData)) {
						// Persist AME data from Map<String, AMEExcelVO> & List<AMEPremiumFactorExcelVO>
						isSuccess = amePlanMgmtSerffService.populateAndPersistAMEData(amePlansMapData, getAMEExcelVOHeader(planType), amePremiumFactorMapData, trackingRecord, planType, defaultTenantIds);
						
						for (Map.Entry<String, AMEExcelVO> ameEntry : amePlansMapData.entrySet()) {
							LOGGER.debug("======Record: ameEntry.getKey(): " + ameEntry.getValue().toString());
						}
						
						for (Map.Entry<String, AMEPremiumFactorExcelVO> amePremiumEntry : amePremiumFactorMapData.entrySet()) {
							LOGGER.debug("======Record: amePremiumEntry.getKey(): " + amePremiumEntry.getValue().toString());
						}
					}
				}
			}
			
			if (null != amePlanSheet) {
				LOGGER.info("AME PlanSheet[1] Name: " + amePlanSheet.getSheetName());
			}
			
			if (null != ameRateSheet) {
				LOGGER.info("AME RateSheet[2] Name: " + ameRateSheet.getSheetName());
			}
			
			if (!isAMEFamilyRate && null != amePremimumFactorSheet) {
				LOGGER.info("AME PremimumFactorSheet[3] Name: " + amePremimumFactorSheet.getSheetName());
			}
			
			if (isSuccess) {
				
				if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
					trackingRecord.setPmResponseXml(MSG_AME_PLANS_SUCCESS);
					trackingRecord.setRequestStateDesc(MSG_AME_PLANS_SUCCESS);
				}
				else {
					trackingRecord.setRequestStateDesc(MSG_AME_PLANS_WARN);
				}
				message.append(trackingRecord.getRequestStateDesc());
				message.append("\n");
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.S);
			}
			else {
				message.append("Failed to load AME Plans.");
				trackingRecord.setRequestStateDesc(message.toString());
			}
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			message.append(MSG_ERROR_AME_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (!isSuccess) {
				trackingRecord.setRequestStatus(SerffPlanMgmt.REQUEST_STATUS.F);
			}
			
			String fileMessage = MSG_FILE + fileName + ":=\n";
			if (StringUtils.isBlank(trackingRecord.getPmResponseXml())) {
				trackingRecord.setPmResponseXml(fileMessage + trackingRecord.getRequestStateDesc());
			}
			else {
				trackingRecord.setPmResponseXml(fileMessage + trackingRecord.getPmResponseXml());
			}
			trackingRecord.setRequestState(SerffPlanMgmt.REQUEST_STATE.E);
			if(trackingRecord.getRequestStateDesc().length() > ERROR_MAX_LEN) {
				trackingRecord.setRequestStateDesc(trackingRecord.getRequestStateDesc().substring(0, ERROR_MAX_LEN));
			}
			trackingRecord.setEndTime(new Date());
			serffService.saveSerffPlanMgmt(trackingRecord);
			moveFileToDirectory(isSuccess, fileName);
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
			LOGGER.debug("loadAllExcelTemplate() End");
		}
	}
	
	private boolean addExcelInECM(StringBuilder message, SerffPlanMgmt trackingRecord, String folderPath, String fileName, String planType) throws IOException {
		
		boolean isSuccess = false;
		InputStream inputStream = null;
		
		try {
			inputStream = ftpClient.getFileInputStream(folderPath + fileName);
			
			if (null != inputStream) {
				LOGGER.debug("Uploading File Name with path to ECM:" + SerffConstants.SERF_ECM_BASE_PATH + getAMEPlanFileType(planType) + fileName);
				// Uploading AME Excel at ECM.
				SerffDocument attachment = serffUtils.addAttachmentInECM(ecmService, trackingRecord, SerffConstants.SERF_ECM_BASE_PATH, getAMEPlanFileType(planType), fileName, IOUtils.toByteArray(inputStream), true);
				serffService.saveSerffDocument(attachment);
				isSuccess = true;
			}
			else {
				message.append("Content is not valid for file: ");
				message.append(fileName);
				trackingRecord.setRequestStateDesc(message.toString());
			}
		}
		catch (Exception ex) {
			trackingRecord.setRequestStateDesc(ex.getMessage());
			message.append(MSG_ERROR_AME_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			// Fixed HP-FOD issue.
			serffResourceUtil.releaseInputStream(inputStream);
		}
		return isSuccess;
	}
	
	private Map<String, AMEPremiumFactorExcelVO> readAndPersistAMEPremimumFactorData(XSSFSheet amePremimumFactorSheet, StringBuilder message) {
		
		LOGGER.debug("readAndPersistAMEPremimumFactorData() Start");
		Map<String, AMEPremiumFactorExcelVO> amePremiumFactorMapData = null;
		
		try {
			
			if (null == amePremimumFactorSheet) {
				LOGGER.info(AME_PREMIUM_FACTOR_SHEET_NAME + " Excel Sheet is missing.");
				return amePremiumFactorMapData;
			}
			
			Iterator<Row> rowIterator = amePremimumFactorSheet.iterator();
			amePremiumFactorMapData = new HashMap<String, AMEPremiumFactorExcelVO>();
			boolean isFirst = true;
			Row excelRow = null;
			AMEPremiumFactorExcelVO amePremiumFactorVO = null;
			
			while (rowIterator.hasNext()) {
				excelRow = rowIterator.next();
				
				if (isFirst) {
					isFirst = false;
					continue;
				}
				amePremiumFactorVO = getAMEPremiumFactorExcelVO(excelRow);
				
				if (null != amePremiumFactorVO) {
					amePremiumFactorMapData.put(amePremiumFactorVO.getCarrierHIOS() + amePremiumFactorVO.getState(), amePremiumFactorVO);
				}
			}
		}
		catch (Exception ex) {
			message.append(MSG_ERROR_AME_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("readAndPersistAMEPremimumFactorData() End");
		}
		return amePremiumFactorMapData;
	}
	
	private Map<String, AMEExcelVO> readAndPersistAMEAgeGroupData(XSSFSheet amePlanSheet, XSSFSheet ameRateSheet, String planType, StringBuilder message) {
		
		LOGGER.debug("readAndPersistAMEAgeGroupData() Start");
		Map<String, AMEExcelVO> amePlansMapData = null;
		
		try {
			
			if (null == amePlanSheet || null == ameRateSheet) {
				LOGGER.info("One of the Excel Sheet is missing.");
				return amePlansMapData;
			}
			
			Row[] amePlanDataRows = getAMEPlanDataRows(amePlanSheet);
			Iterator<Row> rowIterator = ameRateSheet.iterator();
			List<AMEAgeGroupExcelVO> ameAgeGroupExcelList = new ArrayList<AMEAgeGroupExcelVO>();
			AMEAgeGroupExcelVO ameGroupRatesVO = null;
			boolean isFirst = true;
			Row excelRow = null;
			
			while (rowIterator.hasNext()) {
				excelRow = rowIterator.next();
				
				if (isFirst) {
					isFirst = false;
					continue;
				}
				ameGroupRatesVO = getAMEGroupRatesExcelVO(excelRow, planType);
				
				if (null != ameGroupRatesVO) {
					ameAgeGroupExcelList.add(ameGroupRatesVO);
				}
			}
			LOGGER.info("ameAgeGroupExcelList size: " + ameAgeGroupExcelList.size());
			amePlansMapData = getAMEPlansAndAgeBandRates(amePlanDataRows, ameAgeGroupExcelList);
		}
		catch (Exception ex) {
			message.append(MSG_ERROR_AME_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("readAndPersistAMEAgeGroupData() End");
		}
		return amePlansMapData;
	}
	
	private boolean readAndPersistAMEFamilyRateData(XSSFSheet amePlanSheet, XSSFSheet ameRateSheet, SerffPlanMgmt trackingRecord, StringBuilder message, String planType, String defaultTenantIds) {
		
		LOGGER.debug("readAndPersistAMEData() Start");
		boolean status = Boolean.FALSE;
		
		try {
			
			if (null == amePlanSheet || null == ameRateSheet) {
				LOGGER.info("One of the Excel Sheet is missing.");
				return status;
			}
			
			Row[] amePlanDataRows = getAMEPlanDataRows(amePlanSheet);
			Row[] amePlanRatesRows = getAMEPlanRatesRows(ameRateSheet);
			
			Map<String, AMEExcelVO> amePlansMapData = getAMEPlanData(amePlanDataRows, amePlanRatesRows);
			
			if (!CollectionUtils.isEmpty(amePlansMapData)) {
				
				// Persist AME data from Map<String, AMEExcelVO>
				status = amePlanMgmtSerffService.populateAndPersistAMEData(amePlansMapData, getAMEExcelVOHeader(planType), null, trackingRecord, planType, defaultTenantIds);
				
				for (Map.Entry<String, AMEExcelVO> ameEntry : amePlansMapData.entrySet()) {
					LOGGER.debug("======Record: getKey(): " + ameEntry.getValue().toString());
				}
			}
		}
		catch (Exception ex) {
			message.append(MSG_ERROR_AME_PLANS + ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("readAndPersistAMEData() End");
		}
		return status;
	}

	private Row[] getAMEPlanDataRows(XSSFSheet amePlanSheet) {
		Row[] planDataRows = new Row[AMEExcelRow.PLANDATA_ROW_COUNT];
		planDataRows[AMEPlansExcelRowEnum.STATE.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.STATE.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.PLAN_ID.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.PLAN_ID.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.CARRIER_NAME.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.CARRIER_NAME.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.PLAN_NAME.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.PLAN_NAME.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.START_DATE.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.START_DATE.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.PLAN_TYPE.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.PLAN_TYPE.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.DEDUCTIBLE_VALUE.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.DEDUCTIBLE_VALUE.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.DEDUCTIBLE_DESC.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.DEDUCTIBLE_DESC.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.MAX_BENEFIT_VALUE.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.MAX_BENEFIT_VALUE.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.MAX_BENEFIT_DESC.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.MAX_BENEFIT_DESC.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.SUPPLEMENTAL_ACCIDENT.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.SUPPLEMENTAL_ACCIDENT.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.EMERGENCY_TREATMENT.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.EMERGENCY_TREATMENT.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.FOLLOW_UP_TREATMENT.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.FOLLOW_UP_TREATMENT.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.AMBULANCE.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.AMBULANCE.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.INITIAL_HOSPITALIZATION.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.INITIAL_HOSPITALIZATION.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.HOSPITAL_STAY.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.HOSPITAL_STAY.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.SURGERY.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.SURGERY.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.ACCIDENTAL_DEATH.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.ACCIDENTAL_DEATH.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.ACCIDENTAL_DISMEMBERMENT.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.ACCIDENTAL_DISMEMBERMENT.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.OTHER_BENEFITS.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.OTHER_BENEFITS.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.PLAN_BROCHURE.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.PLAN_BROCHURE.getRowIndex());
		planDataRows[AMEPlansExcelRowEnum.EXCLUSIONS_AND_LIMITATIONS.getRowIndex()] = amePlanSheet.getRow(AMEPlansExcelRowEnum.EXCLUSIONS_AND_LIMITATIONS.getRowIndex());
		return planDataRows;
	}

	private Row[] getAMEPlanRatesRows(XSSFSheet ameRateSheet) {
		Row[] planRateRows = new Row[AMEExcelRow.PLANRATE_ROW_COUNT];
		planRateRows[AMERatesExcelRowEnum.STATE.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.STATE.getRowIndex());
		planRateRows[AMERatesExcelRowEnum.CARRIER_HIOS_ID.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.CARRIER_HIOS_ID.getRowIndex());
		planRateRows[AMERatesExcelRowEnum.PLAN_ID.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.PLAN_ID.getRowIndex());
		planRateRows[AMERatesExcelRowEnum.COUPLE.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.COUPLE.getRowIndex());
		planRateRows[AMERatesExcelRowEnum.ONE_ADULT_ONE_KID.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.ONE_ADULT_ONE_KID.getRowIndex());
		planRateRows[AMERatesExcelRowEnum.ONE_ADULT_TWO_KID.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.ONE_ADULT_TWO_KID.getRowIndex());
		planRateRows[AMERatesExcelRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowIndex());
		planRateRows[AMERatesExcelRowEnum.TWO_ADULT_ONE_KID.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.TWO_ADULT_ONE_KID.getRowIndex());
		planRateRows[AMERatesExcelRowEnum.TWO_ADULT_TWO_KID.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.TWO_ADULT_TWO_KID.getRowIndex());
		planRateRows[AMERatesExcelRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowIndex());
		planRateRows[AMERatesExcelRowEnum.INDIVIDUAL.getRowIndex()] = ameRateSheet.getRow(AMERatesExcelRowEnum.INDIVIDUAL.getRowIndex());
		return planRateRows;
	}

	private Map<String, AMEExcelVO> getAMEPlanData(Row[] amePlanDataRows, Row[] amePlanRatesRows) {
		
		Map<String, AMEExcelVO> amePlansMap = new HashMap<String, AMEExcelVO>();
		
		if(null != amePlanDataRows && null != amePlanRatesRows) {
			LOGGER.debug(EMSG_GET_EACH_ROWS + "'"+ AME_PLAN_DATA_SHEET_NAME +"' sheet[1].");
			readAMEPlanData(amePlansMap, amePlanDataRows);
			
			LOGGER.debug(EMSG_GET_EACH_ROWS + "'"+ AME_PLAN_RATES_SHEET_NAME +"' sheet[2].");
			readAMERateData(amePlansMap, amePlanRatesRows);
		}
		else {
			LOGGER.error(EMSG_AME + "Unable to read data from Excel file.");
		}
		return amePlansMap;
	}

	private Map<String, AMEExcelVO> getAMEPlansAndAgeBandRates(Row[] amePlanDataRows, List<AMEAgeGroupExcelVO> ameAgeGroupExcelList) {
		
		Map<String, AMEExcelVO> amePlansMap = new HashMap<String, AMEExcelVO>();
		
		if(null != amePlanDataRows && !CollectionUtils.isEmpty(ameAgeGroupExcelList)) {
			LOGGER.info("ameAgeGroupExcelList size: " + ameAgeGroupExcelList.size());
			
			LOGGER.debug(EMSG_GET_EACH_ROWS + "'"+ AME_PLAN_DATA_SHEET_NAME +"' sheet[1].");
			readAMEPlanData(amePlansMap, amePlanDataRows);
			
			LOGGER.debug(EMSG_GET_EACH_ROWS + "'"+ AME_PLAN_RATES_SHEET_NAME +"' sheet[2].");
			String hiosId, stateCode, planID;
			AMEExcelVO ameExcelVO;
			List<AMEAgeGroupExcelVO> ameGroupRatesList;
			
			for (AMEAgeGroupExcelVO ameGroupRatesVO : ameAgeGroupExcelList) {
				
				LOGGER.info("ameGroupRatesVO is null: " + (null == ameGroupRatesVO));
				
				if (null == ameGroupRatesVO) {
					continue;
				}
				hiosId = ameGroupRatesVO.getCarrierHIOS();
				stateCode = ameGroupRatesVO.getState();
				planID = ameGroupRatesVO.getPlanID();
				LOGGER.info("hiosId + stateCode + planID: " + (hiosId + stateCode + planID));

				if(StringUtils.isBlank(hiosId) || StringUtils.isBlank(stateCode) || StringUtils.isBlank(planID)) {
					ameExcelVO = null;
				}
				else {
					ameExcelVO = amePlansMap.get(hiosId + stateCode + planID);
					LOGGER.info("ameExcelVO is null: " + (null == ameExcelVO));
				}

				if(null != ameExcelVO) {
					
					LOGGER.info("ameExcelVO.getAmeAgeGroupExcelVO() is null: " + (null == ameExcelVO.getAmeAgeGroupExcelVO()));
					ameGroupRatesList = CollectionUtils.isEmpty(ameExcelVO.getAmeAgeGroupExcelVO()) ? new ArrayList<AMEAgeGroupExcelVO>() : ameExcelVO.getAmeAgeGroupExcelVO();
					
					LOGGER.debug("ameGroupRatesVO is: " + ameGroupRatesVO);
					ameGroupRatesList.add(ameGroupRatesVO);
					ameExcelVO.setAmeAgeGroupExcelVO(ameGroupRatesList);
				}
			}
		}
		else {
			LOGGER.error(EMSG_AME + "Unable to read data from Excel file.");
		}
		return amePlansMap;
	}

	/**
	 * Method is used to store AME Excel sheet data in AMEExcelVO Object.
	 * 1) AME Excel Data will store in AMEExcelVO Object from second to end of row of excel sheet.
	 */
	private void readAMEPlanData(Map<String, AMEExcelVO> amePlansMap, Row[] excelRows) {

		if (null != excelRows) {

			String hiosId, stateCode, planID;
			short maxColIx = excelRows[AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowIndex()].getLastCellNum();
			
			for (short colIx = PLAN_DATA_START_COLUMN; colIx < maxColIx; colIx++) {
				
				hiosId = stateCode = planID  = null;
				hiosId = getColumnValueAtIndex(excelRows[AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowIndex()], colIx);
				stateCode = getColumnValueAtIndex(excelRows[AMEPlansExcelRowEnum.STATE.getRowIndex()],colIx);
				planID = getColumnValueAtIndex(excelRows[AMEPlansExcelRowEnum.PLAN_ID.getRowIndex()], colIx);

				if(StringUtils.isBlank(hiosId) || StringUtils.isBlank(stateCode) || StringUtils.isBlank(planID)) {
					LOGGER.error(EMSG_AME + "Empty "+ AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowName() +"["+ hiosId +"] "
							+ "Or "+ AMEPlansExcelRowEnum.STATE.getRowName() +"["+ stateCode +"] "
							+ "Or "+ AMEPlansExcelRowEnum.PLAN_ID.getRowName() +"["+ planID +"].");
					continue;
				}
				stateCode = stateCode.trim().toUpperCase();
				//mapKey = hiosId + stateCode + planID;
				
				if(amePlanDataValidator.isValidHIOSPlanNumber(hiosId + stateCode + planID)) {
					addAMEExcelVOToMap(amePlansMap, excelRows, hiosId, stateCode, hiosId + stateCode + planID, colIx) ;
				}
				else {
					LOGGER.error(EMSG_AME + EMSG_INVALID_PLAN +hiosId + stateCode + planID );
				}
			}	 
		}
	}

	private void addAMEExcelVOToMap(Map<String, AMEExcelVO> amePlansMap, Row[] excelRows, String hiosId, String stateCode, String mapKey, int colIx) {
		AMEExcelVO ameExcelVO = amePlansMap.get(mapKey);
		
		if(null == ameExcelVO) {
			ameExcelVO = new AMEExcelVO();
			ameExcelVO.setPlanID(mapKey);
			ameExcelVO.setCarrierHIOS(hiosId);
			ameExcelVO.setState(stateCode);
			ameExcelVO.setCarrierName(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.CARRIER_NAME.getRowIndex()].getCell(colIx)));
			ameExcelVO.setPlanName(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.PLAN_NAME.getRowIndex()].getCell(colIx)));
			ameExcelVO.setPlanType(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.PLAN_TYPE.getRowIndex()].getCell(colIx)));
			ameExcelVO.setStartDate(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.START_DATE.getRowIndex()].getCell(colIx)));
			ameExcelVO.setDeductibleValue(getCellValue(excelRows[AMEPlansExcelRowEnum.DEDUCTIBLE_VALUE.getRowIndex()].getCell(colIx), true));
			ameExcelVO.setDeductibleDesc(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.DEDUCTIBLE_DESC.getRowIndex()].getCell(colIx)));
			ameExcelVO.setMaxBenefitValue(getCellValue(excelRows[AMEPlansExcelRowEnum.MAX_BENEFIT_VALUE.getRowIndex()].getCell(colIx), true));
			ameExcelVO.setMaxBenefitDesc(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.MAX_BENEFIT_DESC.getRowIndex()].getCell(colIx)));
			ameExcelVO.setSupplementalAccident(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.SUPPLEMENTAL_ACCIDENT.getRowIndex()].getCell(colIx)));
			ameExcelVO.setEmergencyTreatment(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.EMERGENCY_TREATMENT.getRowIndex()].getCell(colIx)));
			ameExcelVO.setFollowUpTreatment(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.FOLLOW_UP_TREATMENT.getRowIndex()].getCell(colIx)));
			ameExcelVO.setAmbulance(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.AMBULANCE.getRowIndex()].getCell(colIx)));
			ameExcelVO.setInitialHospitalization(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.INITIAL_HOSPITALIZATION.getRowIndex()].getCell(colIx)));
			ameExcelVO.setHospitalStay(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.HOSPITAL_STAY.getRowIndex()].getCell(colIx)));
			ameExcelVO.setSurgery(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.SURGERY.getRowIndex()].getCell(colIx)));
			ameExcelVO.setAccidentalDeath(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.ACCIDENTAL_DEATH.getRowIndex()].getCell(colIx)));
			ameExcelVO.setAccidentalDismemberment(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.ACCIDENTAL_DISMEMBERMENT.getRowIndex()].getCell(colIx)));
			ameExcelVO.setOtherBenefits(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.OTHER_BENEFITS.getRowIndex()].getCell(colIx)));
			ameExcelVO.setPlanBrochure(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.PLAN_BROCHURE.getRowIndex()].getCell(colIx)));
			ameExcelVO.setExclusionsAndLimitations(getCellValueTrim(excelRows[AMEPlansExcelRowEnum.EXCLUSIONS_AND_LIMITATIONS.getRowIndex()].getCell(colIx)));
			amePlansMap.put(mapKey, ameExcelVO);
		}
		else {
			LOGGER.error(EMSG_AME + EMSG_DUPLICATE_PLAN + mapKey);
		}
	}
	
	private void readAMERateData(Map<String, AMEExcelVO> amePlansMap, Row[] excelRows) {

		if (null != excelRows) {
			AMEExcelVO ameExcelVO;
			String hiosId, stateCode, planID;
			short maxColIx = excelRows[AMERatesExcelRowEnum.CARRIER_HIOS_ID.getRowIndex()].getLastCellNum();

			for (short colIx = RATE_DATA_START_COLUMN; colIx < maxColIx; colIx++) {

				hiosId = stateCode = planID = null;

				hiosId = getColumnValueAtIndex(excelRows[AMERatesExcelRowEnum.CARRIER_HIOS_ID.getRowIndex()], colIx);
				stateCode = getColumnValueAtIndex(excelRows[AMERatesExcelRowEnum.STATE.getRowIndex()],colIx);
				planID = getColumnValueAtIndex(excelRows[AMERatesExcelRowEnum.PLAN_ID.getRowIndex()], colIx);

				if(StringUtils.isBlank(hiosId) || StringUtils.isBlank(stateCode) || StringUtils.isBlank(planID)) {
					continue;
				}

				stateCode = stateCode.trim().toUpperCase();
				//mapKey = hiosId + stateCode + planID;
				ameExcelVO = amePlansMap.get(hiosId + stateCode + planID);

				if(null != ameExcelVO) {
					ameExcelVO.setCoupleRate(getCellValueTrim(excelRows[AMERatesExcelRowEnum.COUPLE.getRowIndex()].getCell(colIx)));
					ameExcelVO.setOneAdultOneDependentRate(getCellValueTrim(excelRows[AMERatesExcelRowEnum.ONE_ADULT_ONE_KID.getRowIndex()].getCell(colIx)));
					ameExcelVO.setOneAdultTwoDependentRate(getCellValueTrim(excelRows[AMERatesExcelRowEnum.ONE_ADULT_TWO_KID.getRowIndex()].getCell(colIx)));
					ameExcelVO.setOneAdultThreePlusDependentRate(getCellValueTrim(excelRows[AMERatesExcelRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowIndex()].getCell(colIx)));
					ameExcelVO.setTwoAdultOneDependentRate(getCellValueTrim(excelRows[AMERatesExcelRowEnum.TWO_ADULT_ONE_KID.getRowIndex()].getCell(colIx)));
					ameExcelVO.setTwoAdultTwoDependentRate(getCellValueTrim(excelRows[AMERatesExcelRowEnum.TWO_ADULT_TWO_KID.getRowIndex()].getCell(colIx)));
					ameExcelVO.setTwoAdultThreePlusDependentRate(getCellValueTrim(excelRows[AMERatesExcelRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowIndex()].getCell(colIx)));
					ameExcelVO.setIndividualRate(getCellValueTrim(excelRows[AMERatesExcelRowEnum.INDIVIDUAL.getRowIndex()].getCell(colIx)));
				}
				else {
					LOGGER.error(EMSG_AME + EMSG_MISSING_PLAN + hiosId + stateCode + planID);
				}
			}	 
		}
	}
	
	/**
	 * Method is used to store AME plan rates Excel sheet data in AMEAgeGroupExcelVO Object.
	 */
	private AMEAgeGroupExcelVO getAMEGroupRatesExcelVO(Row excelRow, String planType) {
		
		AMEAgeGroupExcelVO ameGroupRatesVO = null;
		
		if (null != excelRow) {
			
			String state = getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.STATE.getColumnIndex()));
			
			if (StringUtils.isNotBlank(state)) {
				ameGroupRatesVO = new AMEAgeGroupExcelVO();
				ameGroupRatesVO.setState(state);
				ameGroupRatesVO.setCarrierHIOS(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.CARRIER_HIOS_ID.getColumnIndex())));
				ameGroupRatesVO.setPlanID(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.PLAN_ID.getColumnIndex())));
				/*ameGroupRatesVO.setPlanName(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.PLAN_NAME.getColumnIndex())));
				ameGroupRatesVO.setCarrierName(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.CARRIER_NAME.getColumnIndex())));
				ameGroupRatesVO.setBenefitMax(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.BENEFIT_MAX.getColumnIndex())));
				ameGroupRatesVO.setDeductible(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.DEDUCTIBLE.getColumnIndex())));*/
				ameGroupRatesVO.setMinAge(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.MIN_AGE.getColumnIndex()), true));
				ameGroupRatesVO.setMaxAge(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.MAX_AGE.getColumnIndex()), true));
				if(SerffConstants.PLAN_TYPE.AME_GENDER.name().equalsIgnoreCase(planType)) {
					ameGroupRatesVO.setApplicant(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.APPLICANT_MALE.getColumnIndex())));
					ameGroupRatesVO.setApplicantFemale(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.APPLICANT_FEMALE.getColumnIndex())));
					ameGroupRatesVO.setSpouse(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.SPOUSE_MALE.getColumnIndex())));
					ameGroupRatesVO.setSpouseFemale(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.SPOUSE_FEMALE.getColumnIndex())));
					ameGroupRatesVO.setChild(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.CHILD_GENDERBASED.getColumnIndex())));
				} else {
					ameGroupRatesVO.setApplicant(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.APPLICANT.getColumnIndex())));
					ameGroupRatesVO.setSpouse(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.SPOUSE.getColumnIndex())));
					ameGroupRatesVO.setChild(getCellValueTrim(excelRow.getCell(AMEAgeRatesColumnEnum.CHILD.getColumnIndex())));
				}
			}
		}
		return ameGroupRatesVO;
	}
	
	/**
	 * Method is used to store AME Premium Factor Excel sheet data in AMEPremiumFactorExcelVO Object.
	 */
	private AMEPremiumFactorExcelVO getAMEPremiumFactorExcelVO(Row excelRow) {
		
		AMEPremiumFactorExcelVO amePremiumFactorVO = null;
		
		if (null != excelRow) {
			
			String carrierHIOS = getCellValueTrim(excelRow.getCell(AMEAgePremiumFactorColumnEnum.CARRIER_HIOS_ID.getColumnIndex()));
			
			if (StringUtils.isNotBlank(carrierHIOS)) {
				amePremiumFactorVO = new AMEPremiumFactorExcelVO();
				amePremiumFactorVO.setCarrierHIOS(carrierHIOS);
				amePremiumFactorVO.setState(getCellValueTrim(excelRow.getCell(AMEAgePremiumFactorColumnEnum.STATE.getColumnIndex())));
				amePremiumFactorVO.setAnnual(getCellValueTrim(excelRow.getCell(AMEAgePremiumFactorColumnEnum.ANNUAL.getColumnIndex())));
				amePremiumFactorVO.setSemiAnnual(getCellValueTrim(excelRow.getCell(AMEAgePremiumFactorColumnEnum.SEMI_ANNUAL.getColumnIndex())));
				amePremiumFactorVO.setQuarterly(getCellValueTrim(excelRow.getCell(AMEAgePremiumFactorColumnEnum.QUARTERLY.getColumnIndex())));
				amePremiumFactorVO.setMonthly(getCellValueTrim(excelRow.getCell(AMEAgePremiumFactorColumnEnum.MONTHLY.getColumnIndex())));
			}
		}
		return amePremiumFactorVO;
	}
	
	/**
	 * Method is used to store AME Excel sheet data in AMEExcelVO Object.
	 * 1) AME Excel Header(First row) will store in AMEExcelVO Object.
	 */
	private AMEExcelVO getAMEExcelVOHeader(String planType) {
		
		AMEExcelVO ameExcelVO = new AMEExcelVO();
		ameExcelVO.setState(AMEPlansExcelRowEnum.STATE.getRowConstName());
		ameExcelVO.setCarrierHIOS(AMEPlansExcelRowEnum.CARRIER_HIOS_ID.getRowConstName());
		ameExcelVO.setPlanID(AMEPlansExcelRowEnum.PLAN_ID.getRowConstName());
		
		ameExcelVO.setCarrierName(AMEPlansExcelRowEnum.CARRIER_NAME.getRowConstName());
		ameExcelVO.setPlanName(AMEPlansExcelRowEnum.PLAN_NAME.getRowConstName());
		ameExcelVO.setPlanType(AMEPlansExcelRowEnum.PLAN_TYPE.getRowConstName());
		ameExcelVO.setStartDate(AMEPlansExcelRowEnum.START_DATE.getRowConstName());
		ameExcelVO.setDeductibleValue(AMEPlansExcelRowEnum.DEDUCTIBLE_VALUE.getRowConstName());
		ameExcelVO.setDeductibleDesc(AMEPlansExcelRowEnum.DEDUCTIBLE_DESC.getRowConstName());
		ameExcelVO.setMaxBenefitValue(AMEPlansExcelRowEnum.MAX_BENEFIT_VALUE.getRowConstName());
		ameExcelVO.setMaxBenefitDesc(AMEPlansExcelRowEnum.MAX_BENEFIT_DESC.getRowConstName());
		ameExcelVO.setSupplementalAccident(AMEPlansExcelRowEnum.SUPPLEMENTAL_ACCIDENT.getRowConstName());
		ameExcelVO.setEmergencyTreatment(AMEPlansExcelRowEnum.EMERGENCY_TREATMENT.getRowConstName());
		ameExcelVO.setFollowUpTreatment(AMEPlansExcelRowEnum.FOLLOW_UP_TREATMENT.getRowConstName());
		ameExcelVO.setAmbulance(AMEPlansExcelRowEnum.AMBULANCE.getRowConstName());
		ameExcelVO.setInitialHospitalization(AMEPlansExcelRowEnum.INITIAL_HOSPITALIZATION.getRowConstName());
		ameExcelVO.setHospitalStay(AMEPlansExcelRowEnum.HOSPITAL_STAY.getRowConstName());
		ameExcelVO.setSurgery(AMEPlansExcelRowEnum.SURGERY.getRowConstName());
		ameExcelVO.setAccidentalDeath(AMEPlansExcelRowEnum.ACCIDENTAL_DEATH.getRowConstName());
		ameExcelVO.setAccidentalDismemberment(AMEPlansExcelRowEnum.ACCIDENTAL_DISMEMBERMENT.getRowConstName());
		ameExcelVO.setOtherBenefits(AMEPlansExcelRowEnum.OTHER_BENEFITS.getRowConstName());
		ameExcelVO.setPlanBrochure(AMEPlansExcelRowEnum.PLAN_BROCHURE.getRowConstName());
		ameExcelVO.setExclusionsAndLimitations(AMEPlansExcelRowEnum.EXCLUSIONS_AND_LIMITATIONS.getRowConstName());
		
		/*ameExcelVO.setState(AMERatesExcelRowEnum.STATE.getRowConstName());
		ameExcelVO.setCarrierHIOS(AMERatesExcelRowEnum.CARRIER_HIOS_ID.getRowConstName());
		ameExcelVO.setPlanID(AMERatesExcelRowEnum.PLAN_ID.getRowConstName());*/
		ameExcelVO.setCoupleRate(AMERatesExcelRowEnum.COUPLE.getRowConstName());
		ameExcelVO.setOneAdultOneDependentRate(AMERatesExcelRowEnum.ONE_ADULT_ONE_KID.getRowConstName());
		ameExcelVO.setOneAdultTwoDependentRate(AMERatesExcelRowEnum.ONE_ADULT_TWO_KID.getRowConstName());
		ameExcelVO.setOneAdultThreePlusDependentRate(AMERatesExcelRowEnum.ONE_ADULT_THREE_PLUS_KID.getRowConstName());
		ameExcelVO.setTwoAdultOneDependentRate(AMERatesExcelRowEnum.TWO_ADULT_ONE_KID.getRowConstName());
		ameExcelVO.setTwoAdultTwoDependentRate(AMERatesExcelRowEnum.TWO_ADULT_TWO_KID.getRowConstName());
		ameExcelVO.setTwoAdultThreePlusDependentRate(AMERatesExcelRowEnum.TWO_ADULT_THREE_PLUS_KID.getRowConstName());
		ameExcelVO.setIndividualRate(AMERatesExcelRowEnum.INDIVIDUAL.getRowConstName());
		
		List<AMEAgeGroupExcelVO> ameAgeGroupExcelList = new ArrayList<AMEAgeGroupExcelVO>();
		AMEAgeGroupExcelVO ameAgeGroupExcelVO = new AMEAgeGroupExcelVO();
		ameAgeGroupExcelVO.setState(AMEAgeRatesColumnEnum.STATE.getColumnConstName());
		ameAgeGroupExcelVO.setCarrierHIOS(AMEAgeRatesColumnEnum.CARRIER_HIOS_ID.getColumnConstName());
		ameAgeGroupExcelVO.setPlanID(AMEAgeRatesColumnEnum.PLAN_ID.getColumnConstName());
		/*ameAgeGroupExcelVO.setPlanName(AMEAgeRatesColumnEnum.PLAN_NAME.getColumnConstName());
		ameAgeGroupExcelVO.setCarrierName(AMEAgeRatesColumnEnum.CARRIER_NAME.getColumnConstName());
		ameAgeGroupExcelVO.setBenefitMax(AMEAgeRatesColumnEnum.BENEFIT_MAX.getColumnConstName());
		ameAgeGroupExcelVO.setDeductible(AMEAgeRatesColumnEnum.DEDUCTIBLE.getColumnConstName());*/
		ameAgeGroupExcelVO.setMinAge(AMEAgeRatesColumnEnum.MIN_AGE.getColumnConstName());
		ameAgeGroupExcelVO.setMaxAge(AMEAgeRatesColumnEnum.MAX_AGE.getColumnConstName());
		if(SerffConstants.PLAN_TYPE.AME_GENDER.name().equalsIgnoreCase(planType)) {
			ameAgeGroupExcelVO.setApplicant(AMEAgeRatesColumnEnum.APPLICANT_MALE.getColumnConstName());
			ameAgeGroupExcelVO.setApplicantFemale(AMEAgeRatesColumnEnum.APPLICANT_FEMALE.getColumnConstName());
			ameAgeGroupExcelVO.setSpouse(AMEAgeRatesColumnEnum.SPOUSE_MALE.getColumnConstName());
			ameAgeGroupExcelVO.setSpouseFemale(AMEAgeRatesColumnEnum.SPOUSE_FEMALE.getColumnConstName());
			ameAgeGroupExcelVO.setChild(AMEAgeRatesColumnEnum.CHILD_GENDERBASED.getColumnConstName());
			
		} else {
			ameAgeGroupExcelVO.setApplicant(AMEAgeRatesColumnEnum.APPLICANT.getColumnConstName());
			ameAgeGroupExcelVO.setSpouse(AMEAgeRatesColumnEnum.SPOUSE.getColumnConstName());
			ameAgeGroupExcelVO.setChild(AMEAgeRatesColumnEnum.CHILD.getColumnConstName());
		}
		ameAgeGroupExcelList.add(ameAgeGroupExcelVO);
		ameExcelVO.setAmeAgeGroupExcelVO(ameAgeGroupExcelList);
		return ameExcelVO;
	}

	public void moveFileToDirectory(boolean isSuccess, String fileName) {
		
		LOGGER.debug("moveFileToDirectory() Start");
		String moveToDirectoryPath = null;
		
		try {
			String baseDirectoryPath = SerffConstants.getSerffCsrHomePath() + SerffConstants.getAMEExcelUploadPath();
			moveToDirectoryPath = baseDirectoryPath;
			
			if (isSuccess) {
				moveToDirectoryPath += SerffConstants.FTP_SUCCESS + SerffConstants.PATH_SEPERATOR;
			}
			else {
				moveToDirectoryPath += SerffConstants.FTP_ERROR + SerffConstants.PATH_SEPERATOR;
			}
			
			if (!ftpClient.isRemoteDirectoryExist(moveToDirectoryPath)) {
				ftpClient.createDirectory(moveToDirectoryPath);
			}
			LOGGER.debug("AME Plan FTP Folder Path: " + baseDirectoryPath);
			LOGGER.info("Move To Directory Path: " + moveToDirectoryPath);
			ftpClient.moveFile(baseDirectoryPath, fileName, moveToDirectoryPath, fileName);
		}
		catch (Exception ex) {
			LOGGER.info("Skipping file beacause it is already exist in directory: " + moveToDirectoryPath);
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("moveFileToDirectory() End");
		}
	}
}
