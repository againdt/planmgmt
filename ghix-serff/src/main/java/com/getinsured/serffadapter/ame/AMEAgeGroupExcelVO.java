package com.getinsured.serffadapter.ame;

/**
 * Class is used to generate getter/setter on the basis of AME plan rates sheet's Excel Column.
 * 
 * @author Bhavin Parmar
 * @since November 12, 2014
 */
public class AMEAgeGroupExcelVO {

	private String state;
	private String carrierHIOS;
	private String planID;
	/* These fields are just for reference in the template not to be read.
	private String planName;
	private String carrierName;
	private String benefitMax;
	private String deductible;*/
	private String minAge;
	private String maxAge;
	private String applicant;
	private String spouse;
	private String applicantFemale;
	private String spouseFemale;
	private String child;

	public AMEAgeGroupExcelVO() {
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCarrierHIOS() {
		return carrierHIOS;
	}

	public void setCarrierHIOS(String carrierHIOS) {
		this.carrierHIOS = carrierHIOS;
	}

	public String getPlanID() {
		return planID;
	}

	public void setPlanID(String planID) {
		this.planID = planID;
	}

	public String getMinAge() {
		return minAge;
	}

	public void setMinAge(String minAge) {
		this.minAge = minAge;
	}

	public String getMaxAge() {
		return maxAge;
	}

	public void setMaxAge(String maxAge) {
		this.maxAge = maxAge;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getSpouse() {
		return spouse;
	}

	public void setSpouse(String spouse) {
		this.spouse = spouse;
	}

	public String getApplicantFemale() {
		return applicantFemale;
	}

	public void setApplicantFemale(String applicantFemale) {
		this.applicantFemale = applicantFemale;
	}

	public String getSpouseFemale() {
		return spouseFemale;
	}

	public void setSpouseFemale(String spouseFemale) {
		this.spouseFemale = spouseFemale;
	}

	public String getChild() {
		return child;
	}

	public void setChild(String child) {
		this.child = child;
	}

	@Override
	public String toString() {
		return "AMEAgeGroupExcelVO [state=" + state + ", carrierHIOS="
				+ carrierHIOS + ", planID=" + planID + ", minAge=" + minAge
				+ ", maxAge=" + maxAge + ", applicant=" + applicant
				+ ", spouse=" + spouse + ", child=" + child + "]";
	}
}
