package com.getinsured.serffadapter.ame;

/**
 * Class is used to generate Enum on the basis of AME plan data & AME plan rates Excel rows.
 * 
 * @author Bhavin Parmar
 * @since August 14, 2014
 */
public class AMEExcelRow {

	private static final int NUMBER_TYPE = 0;
	private static final int FLOAT_TYPE = 1;
	private static final int STRING_TYPE = 2;
	public static final int PLANDATA_ROW_COUNT = 31;
	public static final int PLANRATE_ROW_COUNT = 14;

	public enum AMEPlansExcelRowEnum {
		
		// Common row Enums of sheet[1] and sheet[2]
		STATE("State", "STATE", 1, STRING_TYPE),
		CARRIER_HIOS_ID("Carrier HIOS ID", "CARRIER_HIOS_ID", 2, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 3, STRING_TYPE),
		
		// Row Enums for 'AME plan data' sheet[1]
		CARRIER_NAME("Carrier name", "CARRIER_NAME", 4, STRING_TYPE),
		PLAN_NAME("Plan name", "PLAN_NAME", 5, STRING_TYPE),
		START_DATE("Start Date" , "START_DATE" , 6 , STRING_TYPE),
		PLAN_TYPE("Plan Type", "PLAN_TYPE", 8, STRING_TYPE),
		DEDUCTIBLE_VALUE("Deductible (Value)", "DEDUCTIBLE_VALUE", 9, NUMBER_TYPE),
		DEDUCTIBLE_DESC("Deductible (Description)", "DEDUCTIBLE_VALUE_DESC", 10, STRING_TYPE),
		MAX_BENEFIT_VALUE("Maximum Benefit (Value)", "MAX_BENEFIT_VALUE", 11, NUMBER_TYPE),
		MAX_BENEFIT_DESC("Maximum Benefit (Description)", "MAX_BENEFIT_VALUE_DESC", 12, STRING_TYPE),
		SUPPLEMENTAL_ACCIDENT("Supplemental Accident", "SUPPLEMENTAL_ACCIDENT", 14, STRING_TYPE),
		EMERGENCY_TREATMENT("Emergency Treatment", "EMERGENCY_TREATMENT", 15, STRING_TYPE),
		FOLLOW_UP_TREATMENT("Follow-Up Treatment", "FOLLOW_UP_TREATMENT", 16, STRING_TYPE),
		AMBULANCE("Ambulance", "AMBULANCE", 17, STRING_TYPE),
		INITIAL_HOSPITALIZATION("Initial Hospitalization", "INITIAL_HOSPITALIZATION", 19, STRING_TYPE),
		HOSPITAL_STAY("Hospital stay", "HOSPITAL_STAY", 20, STRING_TYPE),
		SURGERY("Surgery", "SURGERY", 21, STRING_TYPE),
		ACCIDENTAL_DEATH("Accidental Death", "ACCIDENTAL_DEATH", 23, STRING_TYPE),
		ACCIDENTAL_DISMEMBERMENT("Accidental Dismemberment", "ACCIDENTAL_DISMEMBERMENT", 24, STRING_TYPE),
		OTHER_BENEFITS("Other benefits", "OTHER_BENEFITS", 26, STRING_TYPE),
		PLAN_BROCHURE("Plan Brochure", "PLAN_BROCHURE", 28, STRING_TYPE),
		EXCLUSIONS_AND_LIMITATIONS("Exclusions and Limitations", "EXCLUSIONS_AND_LIMITATIONS", 29, STRING_TYPE);

		private String rowName;
		private String rowConstName;
		private int rowIndex;
		private int dataType;

		private AMEPlansExcelRowEnum(String rowName, String rowConstName, int index, int dataType) {
			this.rowName = rowName;
			this.rowConstName = rowConstName;
			this.rowIndex = index;
			this.dataType = dataType;
		}

		public String getRowName() {
			return rowName;
		}

		public String getRowConstName() {
			return rowConstName;
		}

		public int getRowIndex() {
			return rowIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}

	public enum AMERatesExcelRowEnum {
		
		// Common row Enums of sheet[1] and sheet[2]
		STATE("State", "STATE", 1, STRING_TYPE),
		CARRIER_HIOS_ID("Carrier HIOS ID", "CARRIER_HIOS_ID", 2, STRING_TYPE),
		PLAN_ID("Plan ID", "PLAN_ID", 3, STRING_TYPE),
		
		// Row Enums for 'AME plan rates' sheet[2]
		COUPLE("Couple", "COUPLE", 6, FLOAT_TYPE),
		ONE_ADULT_ONE_KID("1 adult 1 kid", "ONE_ADULT_ONE_KID", 7, FLOAT_TYPE),
		ONE_ADULT_TWO_KID("1 adult 2 kid", "ONE_ADULT_TWO_KID", 8, FLOAT_TYPE),
		ONE_ADULT_THREE_PLUS_KID("1 adult 3+ kid", "ONE_ADULT_THREE_PLUS_KID", 9, FLOAT_TYPE),
		TWO_ADULT_ONE_KID("2 adult 1 kid", "TWO_ADULT_ONE_KID", 10, FLOAT_TYPE),
		TWO_ADULT_TWO_KID("2 adult 2 kid", "TWO_ADULT_TWO_KID", 11, FLOAT_TYPE),
		TWO_ADULT_THREE_PLUS_KID("2 adult 3+ kid", "TWO_ADULT_THREE_PLUS_KID", 12, FLOAT_TYPE),
		INDIVIDUAL("Individual", "INDIVIDUAL", 13, FLOAT_TYPE);

		private String rowName;
		private String rowConstName;
		private int rowIndex;
		private int dataType;

		private AMERatesExcelRowEnum(String rowName, String rowConstName, int index, int dataType) {
			this.rowName = rowName;
			this.rowConstName = rowConstName;
			this.rowIndex = index;
			this.dataType = dataType;
		}

		public String getRowName() {
			return rowName;
		}

		public String getRowConstName() {
			return rowConstName;
		}

		public int getRowIndex() {
			return rowIndex;
		}

		public int getDataType() {
			return dataType;
		}
	}
}
