package com.getinsured.serffadapter.ame;

import java.util.List;

/**
 * Class is used to generate getter/setter on the basis of AME Plan Excel Row.
 * 
 * @author Bhavin Parmar
 * @since August 14, 2014
 */
public class AMEExcelVO {

	private String state;
	private String carrierHIOS;
	private String planID;
	private String carrierName;
	private String planName;
	private String planType;
	private String deductibleValue;
	private String deductibleDesc;
	private String maxBenefitValue;
	private String maxBenefitDesc;
	private String supplementalAccident;
	private String emergencyTreatment;
	private String followUpTreatment;
	private String ambulance;
	private String initialHospitalization;
	private String hospitalStay;
	private String surgery;
	private String accidentalDeath;
	private String accidentalDismemberment;
	private String otherBenefits;
	private String planBrochure;
	private String exclusionsAndLimitations;
	private String coupleRate;
	private String oneAdultOneDependentRate;
	private String oneAdultTwoDependentRate;
	private String oneAdultThreePlusDependentRate;
	private String twoAdultOneDependentRate;
	private String twoAdultTwoDependentRate;
	private String twoAdultThreePlusDependentRate;
	private String individualRate;
	private String startDate;
	private String endDate;
	
	private List<AMEAgeGroupExcelVO> ameAgeGroupExcelVO;

	private boolean isNotValid;
	private String errorMessages;

	public AMEExcelVO() {
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCarrierHIOS() {
		return carrierHIOS;
	}

	public void setCarrierHIOS(String carrierHIOS) {
		this.carrierHIOS = carrierHIOS;
	}

	public String getPlanID() {
		return planID;
	}

	public void setPlanID(String planID) {
		this.planID = planID;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanType() {
		return planType;
	}

	public void setPlanType(String planType) {
		this.planType = planType;
	}

	public String getDeductibleValue() {
		return deductibleValue;
	}

	public void setDeductibleValue(String deductibleValue) {
		this.deductibleValue = deductibleValue;
	}

	public String getDeductibleDesc() {
		return deductibleDesc;
	}

	public void setDeductibleDesc(String deductibleDesc) {
		this.deductibleDesc = deductibleDesc;
	}

	public String getMaxBenefitValue() {
		return maxBenefitValue;
	}

	public void setMaxBenefitValue(String maxBenefitValue) {
		this.maxBenefitValue = maxBenefitValue;
	}

	public String getMaxBenefitDesc() {
		return maxBenefitDesc;
	}

	public void setMaxBenefitDesc(String maxBenefitDesc) {
		this.maxBenefitDesc = maxBenefitDesc;
	}

	public String getSupplementalAccident() {
		return supplementalAccident;
	}

	public void setSupplementalAccident(String supplementalAccident) {
		this.supplementalAccident = supplementalAccident;
	}

	public String getEmergencyTreatment() {
		return emergencyTreatment;
	}

	public void setEmergencyTreatment(String emergencyTreatment) {
		this.emergencyTreatment = emergencyTreatment;
	}

	public String getFollowUpTreatment() {
		return followUpTreatment;
	}

	public void setFollowUpTreatment(String followUpTreatment) {
		this.followUpTreatment = followUpTreatment;
	}

	public String getAmbulance() {
		return ambulance;
	}

	public void setAmbulance(String ambulance) {
		this.ambulance = ambulance;
	}

	public String getInitialHospitalization() {
		return initialHospitalization;
	}

	public void setInitialHospitalization(String initialHospitalization) {
		this.initialHospitalization = initialHospitalization;
	}

	public String getHospitalStay() {
		return hospitalStay;
	}

	public void setHospitalStay(String hospitalStay) {
		this.hospitalStay = hospitalStay;
	}

	public String getSurgery() {
		return surgery;
	}

	public void setSurgery(String surgery) {
		this.surgery = surgery;
	}

	public String getAccidentalDeath() {
		return accidentalDeath;
	}

	public void setAccidentalDeath(String accidentalDeath) {
		this.accidentalDeath = accidentalDeath;
	}

	public String getAccidentalDismemberment() {
		return accidentalDismemberment;
	}

	public void setAccidentalDismemberment(String accidentalDismemberment) {
		this.accidentalDismemberment = accidentalDismemberment;
	}

	public String getOtherBenefits() {
		return otherBenefits;
	}

	public void setOtherBenefits(String otherBenefits) {
		this.otherBenefits = otherBenefits;
	}

	public String getPlanBrochure() {
		return planBrochure;
	}

	public void setPlanBrochure(String planBrochure) {
		this.planBrochure = planBrochure;
	}

	public String getExclusionsAndLimitations() {
		return exclusionsAndLimitations;
	}

	public void setExclusionsAndLimitations(String exclusionsAndLimitations) {
		this.exclusionsAndLimitations = exclusionsAndLimitations;
	}

	public String getCoupleRate() {
		return coupleRate;
	}

	public void setCoupleRate(String coupleRate) {
		this.coupleRate = coupleRate;
	}

	public String getOneAdultOneDependentRate() {
		return oneAdultOneDependentRate;
	}

	public void setOneAdultOneDependentRate(String oneAdultOneDependentRate) {
		this.oneAdultOneDependentRate = oneAdultOneDependentRate;
	}

	public String getOneAdultTwoDependentRate() {
		return oneAdultTwoDependentRate;
	}

	public void setOneAdultTwoDependentRate(String oneAdultTwoDependentRate) {
		this.oneAdultTwoDependentRate = oneAdultTwoDependentRate;
	}

	public String getOneAdultThreePlusDependentRate() {
		return oneAdultThreePlusDependentRate;
	}

	public void setOneAdultThreePlusDependentRate(String oneAdultThreePlusDependentRate) {
		this.oneAdultThreePlusDependentRate = oneAdultThreePlusDependentRate;
	}

	public String getTwoAdultOneDependentRate() {
		return twoAdultOneDependentRate;
	}

	public void setTwoAdultOneDependentRate(String twoAdultOneDependentRate) {
		this.twoAdultOneDependentRate = twoAdultOneDependentRate;
	}

	public String getTwoAdultTwoDependentRate() {
		return twoAdultTwoDependentRate;
	}

	public void setTwoAdultTwoDependentRate(String twoAdultTwoDependentRate) {
		this.twoAdultTwoDependentRate = twoAdultTwoDependentRate;
	}

	public String getTwoAdultThreePlusDependentRate() {
		return twoAdultThreePlusDependentRate;
	}

	public void setTwoAdultThreePlusDependentRate(String twoAdultThreePlusDependentRate) {
		this.twoAdultThreePlusDependentRate = twoAdultThreePlusDependentRate;
	}

	public String getIndividualRate() {
		return individualRate;
	}

	public void setIndividualRate(String individualRate) {
		this.individualRate = individualRate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public boolean isNotValid() {
		return isNotValid;
	}

	public void setNotValid(boolean isNotValid) {
		this.isNotValid = isNotValid;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	public List<AMEAgeGroupExcelVO> getAmeAgeGroupExcelVO() {
		return ameAgeGroupExcelVO;
	}

	public void setAmeAgeGroupExcelVO(List<AMEAgeGroupExcelVO> ameAgeGroupExcelVO) {
		this.ameAgeGroupExcelVO = ameAgeGroupExcelVO;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(" planID:");
		sb.append(planID);
		sb.append(" state:");
		sb.append(state);
		sb.append(" carrierHIOS:");
		sb.append(carrierHIOS);
		sb.append("\ncarrierName:");
		sb.append(carrierName);
		sb.append(" planName:");
		sb.append(planName);
		sb.append(" planType:");
		sb.append(planType);
		sb.append("\ndeductibleValue:");
		sb.append(deductibleValue);
		sb.append(" deductibleDesc:");
		sb.append(deductibleDesc);
		sb.append(" maxBenefitValue:");
		sb.append(maxBenefitValue);
		sb.append(" maxBenefitDesc:");
		sb.append(maxBenefitDesc);
		sb.append("\nsupplementalAccident:");
		sb.append(supplementalAccident);
		sb.append(" emergencyTreatment:");
		sb.append(emergencyTreatment);
		sb.append(" followUpTreatment:");
		sb.append(followUpTreatment);
		sb.append(" ambulance:");
		sb.append(ambulance);
		sb.append("\ninitialHospitalization:");
		sb.append(initialHospitalization);
		sb.append(" hospitalStay:");
		sb.append(hospitalStay);
		sb.append(" surgery:");
		sb.append(surgery);
		sb.append("\naccidentalDeath:");
		sb.append(accidentalDeath);
		sb.append(" accidentalDismemberment:");
		sb.append(accidentalDismemberment);
		sb.append(" otherBenefits:");
		sb.append(otherBenefits);
		sb.append(" planBrochure:");
		sb.append(planBrochure);
		sb.append(" exclusionsAndLimitations:");
		sb.append(exclusionsAndLimitations);
		sb.append("\ncoupleRate:");
		sb.append(coupleRate);
		sb.append(" oneAdultOneDependentRate:");
		sb.append(oneAdultOneDependentRate);
		sb.append(" oneAdultTwoDependentRate:");
		sb.append(oneAdultTwoDependentRate);
		sb.append(" oneAdultThreePlusDependentRate:");
		sb.append(oneAdultThreePlusDependentRate);
		sb.append(" twoAdultOneDependentRate:");
		sb.append(twoAdultOneDependentRate);
		sb.append(" twoAdultTwoDependentRate:");
		sb.append(twoAdultTwoDependentRate);
		sb.append(" twoAdultThreePlusDependentRate:");
		sb.append(twoAdultThreePlusDependentRate);
		sb.append(" individualRate:");
		sb.append(individualRate);
		return sb.toString();
	}
	
}
