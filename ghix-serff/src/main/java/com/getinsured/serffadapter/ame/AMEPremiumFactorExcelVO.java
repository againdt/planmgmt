package com.getinsured.serffadapter.ame;

/**
 * Class is used to generate getter/setter on the basis of AME Premium Factor sheet's Excel Column.
 * 
 * @author Bhavin Parmar
 * @since November 12, 2014
 */
public class AMEPremiumFactorExcelVO {

	private String carrierHIOS;
	private String state;
	private String annual;
	private String semiAnnual;
	private String quarterly;
	private String monthly;
	
	private boolean isNotValid;
	private String errorMessages;

	public AMEPremiumFactorExcelVO() {
	}

	public String getCarrierHIOS() {
		return carrierHIOS;
	}

	public void setCarrierHIOS(String carrierHIOS) {
		this.carrierHIOS = carrierHIOS;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getAnnual() {
		return annual;
	}

	public void setAnnual(String annual) {
		this.annual = annual;
	}

	public String getSemiAnnual() {
		return semiAnnual;
	}

	public void setSemiAnnual(String semiAnnual) {
		this.semiAnnual = semiAnnual;
	}

	public String getQuarterly() {
		return quarterly;
	}

	public void setQuarterly(String quarterly) {
		this.quarterly = quarterly;
	}

	public String getMonthly() {
		return monthly;
	}

	public void setMonthly(String monthly) {
		this.monthly = monthly;
	}

	public boolean isNotValid() {
		return isNotValid;
	}

	public void setNotValid(boolean isNotValid) {
		this.isNotValid = isNotValid;
	}

	public String getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(String errorMessages) {
		this.errorMessages = errorMessages;
	}

	@Override
	public String toString() {
		return "AMEGroupPremiumExcelVO [carrierHIOS=" + carrierHIOS
				+ ", state=" + state + ", annual=" + annual + ", semiAnnual="
				+ semiAnnual + ", quarterly=" + quarterly + ", monthly="
				+ monthly + "]";
	}
}
