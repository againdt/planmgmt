<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="generated.InsuranceType"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="session.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Invoke Get ZipCode Info Quotit Service and load Zipcodes into Database</title>
	</head>
<body>
	<p align="center" style="font-weight: bold;">Invoke Get ZipCode Info Quotit Service and load Zipcodes into Database [HIX-60438]</p>
	<p align="center">&nbsp;</p>
	<div align="center">
		<center>
			<form action="${pageContext.request.contextPath}/admin/invokeGetZipCodeInfoQuotitService" onsubmit="document.getElementById('submit').disabled=true;" method="POST">
				<table border="0" cellpadding="0" cellspacing="0" width="50%">
					<tr>
						<td>
							<df:csrfToken/>
							<p align="center">
								<input type="submit" id="submit" name="submit" value="Invoke Get ZipCode Info Quotit Service and load Zipcodes into Database" style="font-size: 12pt; color: #0066CC">
							</p>							
						</td>
					</tr>
					<c:if test="${executeBatchMessage != null}">
						<tr>
							<td align="center" style="font-size: 14pt;">
								<c:out value="${executeBatchMessage}" />
							</td>
						</tr>
					</c:if>
				</table>
			</form>
		</center>
	</div>
	<div style="font-size: 12pt;">
		Notes:
		Clicking above button will 
		<br>1. Invoke Get ZipCode Info Quotit Service.
		<br>2. Upload Request and Response to ECM.
		<br>3. Parse data and load to Database
		<br>4. Display success / error message
		<br>
	</div>
</body>
</html>
