<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@include file="datasource.jsp" %>

<head>
	<base target="batchmain_1">
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
</head>
<%
	String serffReqId = request.getParameter("serffReqId");
	if (null != serffReqId) {
%>
<table class="table" style="white-space: pre-wrap; width: 100%;" border="0">
	<thead>
		<tr class="graydrkbg">
			<th scope="col">Response Data</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><textarea id="pmResponseXml" name="pmResponseXml" readonly="readonly" rows="20" cols="100"><c:out value="${pmResponseXml}"/></textarea></td>
		</tr>
	</tbody>
</table>
<%
	}
%>