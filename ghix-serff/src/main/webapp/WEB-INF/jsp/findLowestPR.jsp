<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp"%>

<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Find Lowest Plan Rates</title>
<script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../resources/js/jquery-ui.js"></script>
<script>
	$( "#dob" ).datepicker();
	$( "#coverageStartDate" ).datepicker();
	
	function populateJson(){		
		$("#inputJSON").val('{"id":'+ $("#planId").val()+',"exchangeType":"'+ $("input[name=exchangeType]:checked").val() + '"}');	
	}
	
	function checkLength(val) {
	    var countMe = val;
	    var escapedStr = encodeURI(countMe);
	    if (escapedStr.indexOf("%") != -1) {
	        var count = escapedStr.split("%").length - 1;
	        if (count == 0) count++;  //perverse case; can't happen with real UTF-8
	        var tmp = escapedStr.length - (count * 3);
	        count = count + tmp;
	    } else {
	        count = escapedStr.length;
	    }
	    
	    $('output[name="count"]').val(count + " bytes");       
	    if (count > 0 ){
	    	var kb = count/1024;
	    	$('output[name="kb"]').val(kb + " KB");
		}
	}
	
	function submitData(val) {
		var start = new Date();
		var url = "";
		var defaultUrl = "/ghix-planmgmt/planratebenefit/findLowestPremiumPlanRate";
		var targetUrl = $('#targetURL').val();
		
		if (targetUrl != "") {
			url =  $("input[name=protocol]:checked").val() + "://" + targetUrl + "/" + defaultUrl;
		} else {
			alert("Please enter valid url, e.g. localhost:8080 ");
		}
		$.ajax({
			type : "POST",
			// dataType: "text/html",
			contentType : "application/json; charset=utf-8",
			url : url,
			data : val,
			crossDomain: true,
			success : function(responseText) {
				$("textarea[name='outputRaw']").val(responseText.disclaimerInfo);
				var end = new Date();
				var duration = end - start;
				$('output[name="duration"]').val(duration + " Milli Seconds");
				checkLength(responseText.disclaimerInfo);
			},
			error : function(responseText) {
				$("textarea[name='outputRaw']").val("Error occurred while getting plan info");
				var end = new Date();
				var duration = end - start;
				$('output[name="duration"]').val(duration + " Milli Seconds");
			}
		});
	}
</script>
</head>

<body>
<% 
      String hostName = request.getHeader("host");
%>
<table border="0" cellpadding="5" cellspacing="0" width="60%">
  <tr>
    <td width="100%" bgcolor="#FFCC00"><font face="Arial" size="2"><b>API: Find Lowest Premium Plan Rate</b></font></td>
  </tr>
  <tr>
    <td width="100%" bgcolor="#FFCC00"><font face="Arial" size="2"><b>Path</b>: <a href="../../../../ghix-planmgmt/planratebenefit/findLowestPremiumPlanRate">/ghix-planmgmt/planratebenefit/findLowestPremiumPlanRate</a>&nbsp;</font></td>
  </tr>
  <tr>
    <td width="100%" bgcolor="#C4FCB8">
      <form method="POST" action="">        
        <table style="font-family:Arial; font-size:small;">
          <tr>                              
            <td>
            	<label>DOB:</label>
            </td>
            <td>            	
            	<input type="date" class="datepicker" id="dob" name="dob" title="dd/MM/yyyy" />
            </td>
             <td>
            	<label>Tobacco:</label>
            </td>
           	<td>
           		<input type="checkbox" name="C1" value="ON"></td>
           	<td>  
          </tr>
          <tr>
            <td>
            	<label>Coverage Start Date:</label>
            </td>          
            <td>            	
            	<input type="date" class="datepicker" id="coverageStartDate" name="coverageStartDate" title="dd/MM/yyyy" />
            </td>
            <td>
            	<label>Employer Zip Code:</label> 
            </td>          
            <td>
            	<input type="text" name="empZipCode"  id="empZipCode" size="6" value="534006">
			<td>
		</tr>
        <tr>
        	<td>
        		<label>Primary Zip:</label>
        	</td>			
            <td>
               	<input type="text" name="T4" size="6" value="534006">
            </td>
            <td>
				<label>County:</label> 
            </td>
            <td>
              	<select size="1" name="D2">
					<option>County 1(12345)</option>
                	<option>County 2(34567)</option>
              	</select>
			</td>
		</tr>
        <tr>
            <td>
            	<label>Plan Market:</label>
            </td>
            <td>
            	<select id="planMarket">
            		<option value="INDIVIDUAL">INDIVIDUAL</option>
            		<option value="SHOP">SHOP</option>
            	</select>
            </td>
            <td>
              <label>Plan Level:</label>
            </td>
            <td>
            	<select id="planLevel">            	
            	</select>
            </td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>
              <p align="right">
              	<input type="submit" value="Populate" name="Populate"><input type="submit" value="Reset" name="Reset"> 
              </td>
        </tr>
        <tr>       
        	<td>          	
          	</td>     	
          	<td>Note: Modify employee list as required below</td>          	        
         </tr>
        </table>
      </form>
    </td>
  </tr>
  <tr>
		<td width="100%" bgcolor="#BDFFFF">
			<p><font face="Arial" size="2"><textarea rows="10" id="inputJSON" name="inputJSON" cols="80" style="background-color: #00FFFF">{  
   "url":null,
   "requestMethod":null,
   "requestParameters":{  
      "zip":"92250",
      "employerPrimaryZip":"92250",
      "planMarket":"",
      "planLevel":"",
      "dob":"1979-10-10",
      "tobacco":"",
      "coverageStartDate":"2014-05-05",
      "countyCode":"06025"
   }
}</textarea></font></p>
			<p align="left">
				<font face="Arial" size="2">Protocol:
					<input type="radio" id="protocol" name="protocol" value="http" checked="checked"> HTTP
					<input type="radio" id="protocol" name="protocol" value="https"> HTTPS
				</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<font face="Arial" size="2">URL: 
					<input type="text" name="targetURL" size="20" style="background-color: #BDFFFF" value="<%=hostName%>" id="targetURL"> 
					<input type="button" value="Post" name="Post" onclick="submitData(document.getElementById('inputJSON').value);">
					<input type="reset" value="Reset" name="B2"></font>
			</p>
		</td>
	<tr>  
  <tr>
    <td width="100%" bgcolor="#999999"><font face="Arial" size="2"><b>Duration</b>: 200 MilliSeconds&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <b>Size</b>: <output id="count" name="count"> </output> | <output id="kb" name="kb"> </output></td>
  </tr>
  <tr>
    <td width="100%" bgcolor="#CCCCCC"><font face="Arial" size="2">Raw Response:</font>
      <p><font face="Arial" size="2">
      	<textarea rows="20" name="outputRaw" cols="80" style="background-color: #BDFFFF"></textarea></font>
      </td>
  </tr>
  <tr>
    <td width="100%" bgcolor="#CCCCCC"><font face="Arial" size="2">JSON Response:</font>
      <p>&nbsp;</td>
  </tr>
</table>

</body>

</html>
