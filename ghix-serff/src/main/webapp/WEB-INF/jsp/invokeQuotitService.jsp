<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="generated.InsuranceType"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="session.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Invoke Quotit Service and load Plans into Database</title>

	<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />

	<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

	<script type="text/javascript">
		$(function() {
			var effectiveDate = "<%=request.getParameter("effectiveDate")%>";
		    if (effectiveDate && effectiveDate != "null") {
		    	$("#effectiveDate").val(effectiveDate);
			}

			$("#effectiveDate").datepicker({
				changeMonth: true,
				changeYear: true,
				minDate: $("#startDate").val()
			}).attr("readOnly", "true");
		});
	</script>
</head>

<body>
	<div align="center">
		<div style="font-weight: bold;">Invoke Quotit Service and load Plans into Database [HIX-60429]</div>
		<div class="header" style="height:40px"></div>
		<div class="control-group">
			<form action="${pageContext.request.contextPath}/admin/invokeQuotitService" onsubmit="document.getElementById('submit').disabled=true;" method="POST">
				<df:csrfToken/>

				<div><table align="center" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<label for="insuranceType" class="control-label">Insurance Type<span style="color: red;">*</span>&nbsp;</label>
						</td>
						<td>
							<select id="insuranceType" name="insuranceType">
								<option selected="selected" value="">Select</option>
								<option value="<%=InsuranceType.MEDICARE_ADVANTAGE.name()%>"><%=InsuranceType.MEDICARE_ADVANTAGE.value()%></option>
								<option value="<%=InsuranceType.MEDIGAP.name()%>"><%=InsuranceType.MEDIGAP.value()%></option>
								<option value="<%=InsuranceType.RX.name()%>"><%=InsuranceType.RX.value()%></option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<label for="effectiveDate" class="control-label">Effective Date<span style="color: red;">*</span>&nbsp;</label>
						</td>
						<td>
							<input type="text" id="effectiveDate" name="effectiveDate" size="10" maxlength="10">
							<br><input type="hidden" id="startDate" name="startDate" value="${currentDate}" size="10" maxlength="10">
						</td>
					</tr>
				</table></div>

				<div class="header" style="height:20px"></div>

				<div>
					<input type="submit" id="submit" name="submit" value="Invoke Quotit Service and load Plans into Database/ECM" style="font-size: 12pt; color: #0066CC">
				</div>

				<c:if test="${executeBatchMessage != null}"><div class="control-group">
					<c:out value="${executeBatchMessage}" />
				</div></c:if>
			</form>
		</div>
	</div>
	<div class="header" style="height:40px"></div>
	<div style="font-size: 12pt;">
		Notes:
		Clicking above button will 
		<br>1. Invoke Quotit Service.
		<br>2. Upload Request and Response to ECM.
		<br>3. Parse data and load to Database
		<br>4. Display success / error message
		<br>
	</div>
</body>
</html>
