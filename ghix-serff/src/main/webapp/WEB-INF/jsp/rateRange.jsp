<html>
<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>
<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8">

<sql:query dataSource="${jspDataSource}" var="result1">
select
count(case when rate <= 1 then 1 end) as lt_one,
count(case when rate > 1 and rate <=5 then 1 end) as bt_one_five,
count(case when rate > 5 and rate <=10 then 1 end) as bt_five_ten,
count(case when rate > 10 and rate <=1000 then 1 end) as bt_ten_1k,
count(case when rate > 1000 and rate <=2000 then 1 end) as bt_1k_2k,
count(case when rate > 2000 and rate <=5000 then 1 end) as bt_2k_5k,
count(case when rate > 5000 and rate <=10000 then 1 end) as bt_5k_10k,
count(case when rate > 10000 then 1 end) as gt_10k
from pm_plan_rate
</sql:query>

<sql:query dataSource="${jspDataSource}" var="result2">
select 
count(case when ppr.rate <= 1 then 1 end) as lt_one,
count(case when ppr.rate > 1 and ppr.rate <=5 then 1 end) as bt_one_five,
count(case when ppr.rate > 5 and ppr.rate <=10 then 1 end) as bt_five_ten,
count(case when ppr.rate > 10 and ppr.rate <=1000 then 1 end) as bt_ten_1k,
count(case when ppr.rate > 1000 and ppr.rate <=2000 then 1 end) as bt_1k_2k,
count(case when ppr.rate > 2000 and ppr.rate <=5000 then 1 end) as bt_2k_5k,
count(case when ppr.rate > 5000 and ppr.rate <=10000 then 1 end) as bt_5k_10k,
count(case when ppr.rate > 10000 then 1 end) as gt_10k
from plan p, issuers i, pm_plan_Rate ppr 
where p.issuer_id = i.id
and ppr.plan_id = p.id 
and p.STATUS = 'CERTIFIED' 
and p.ISSUER_VERIFICATION_STATUS = 'VERIFIED' 
and p.IS_DELETED='N' 
and i.CERTIFICATION_STATUS='CERTIFIED' 
and p.enrollment_avail='AVAILABLE'
and ppr.is_deleted= 'N' 
</sql:query>

<head>
<title>Rate Range Check</title>
</head>
<body>
<h3>Rate Range</h3>

 
<b>All Plan Rates Range from pm_plan_rate table</b>
<br>

<display:table name="${result1.rows}"  requestURI="rateRange" id="table1" export="false"  style="width:80%" defaultorder="descending" >
<display:column property="lt_one" title="<= 1"  />
	<display:column property="bt_one_five" title="> 1 and <= 5"  />
	<display:column property="bt_five_ten" title="> 5 and <= 10" />
	<display:column property="bt_ten_1k" title="> 10 and <= 1k" />
	<display:column property="bt_1k_2k" title="> 1k and <= 2k" />
	<display:column property="bt_2k_5k" title="> 2k and <= 5k" />
	<display:column property="bt_5k_10k" title="> 5k and <= 10k" />
	<display:column property="gt_10k" title="> 10k" />
</display:table>

<b>Valid Plan Rates Range from pm_plan_rate table</b>
<br>

<display:table name="${result2.rows}"  requestURI="rateRange" id="table1" export="false"  style="width:80%" defaultorder="descending" >
<display:column property="lt_one" title="<= 1"  />
	<display:column property="bt_one_five" title="> 1 and <= 5"  />
	<display:column property="bt_five_ten" title="> 5 and <= 10" />
	<display:column property="bt_ten_1k" title="> 10 and <= 1k" />
	<display:column property="bt_1k_2k" title="> 1k and <= 2k" />
	<display:column property="bt_2k_5k" title="> 2k and <= 5k" />
	<display:column property="bt_5k_10k" title="> 5k and <= 10k" />
	<display:column property="gt_10k" title="> 10k" />
</display:table>

<b>Note: We need to suspect plans which rates are extreme like <1$ or >10K</b>

<br><br><b>All Plan Rates Query:</b>
select
count(case when rate <= 1 then 1 end) as lt_one,
count(case when rate > 1 and rate <=5 then 1 end) as bt_one_five,
count(case when rate > 5 and rate <=10 then 1 end) as bt_five_ten,
count(case when rate > 10 and rate <=1000 then 1 end) as bt_ten_1k,
count(case when rate > 1000 and rate <=2000 then 1 end) as bt_1k_2k,
count(case when rate > 2000 and rate <=5000 then 1 end) as bt_2k_5k,
count(case when rate > 5000 and rate <=10000 then 1 end) as bt_5k_10k,
count(case when rate > 10000 then 1 end) as gt_10k
from pm_plan_rate

<br><br><b>Valid Plan Rates Query:</b>
select 
count(case when ppr.rate <= 1 then 1 end) as lt_one,
count(case when ppr.rate > 1 and ppr.rate <=5 then 1 end) as bt_one_five,
count(case when ppr.rate > 5 and ppr.rate <=10 then 1 end) as bt_five_ten,
count(case when ppr.rate > 10 and ppr.rate <=1000 then 1 end) as bt_ten_1k,
count(case when ppr.rate > 1000 and ppr.rate <=2000 then 1 end) as bt_1k_2k,
count(case when ppr.rate > 2000 and ppr.rate <=5000 then 1 end) as bt_2k_5k,
count(case when ppr.rate > 5000 and ppr.rate <=10000 then 1 end) as bt_5k_10k,
count(case when ppr.rate > 10000 then 1 end) as gt_10k
from plan p, issuers i, pm_plan_Rate ppr 
where p.issuer_id = i.id
and ppr.plan_id = p.id 
and p.STATUS = 'CERTIFIED' 
and p.ISSUER_VERIFICATION_STATUS = 'VERIFIED' 
and p.IS_DELETED='N' 
and i.CERTIFICATION_STATUS='CERTIFIED' 
and p.enrollment_avail='AVAILABLE'
and ppr.is_deleted= 'N' 

 </body> 
 </html>


