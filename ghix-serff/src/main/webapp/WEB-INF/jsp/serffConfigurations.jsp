<%@ include file="datasource.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<html>
<head>
	<title>List of SERFF configurations</title>
	<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>" />
</head>

<body>
	<h3>SERFF configurations</h3>

	<sql:query dataSource="${jspDataSource}" var="configData">
		SELECT ID, PROPERTY_KEY, PROPERTY_VALUE
		FROM GI_APP_CONFIG
		WHERE PROPERTY_KEY LIKE 'serff.%'
		ORDER BY PROPERTY_KEY
	</sql:query>

	<display:table name="${configData.rows}" id="configDataTable"
		requestURI="serffConfigurations" defaultsort="2" pagesize="1000"
		style="white-space: pre-wrap;width: 100%;" export="true">
		<display:column property="ID" title="ID" sortable="true" />
		<display:column property="PROPERTY_KEY" title="Property Key" sortable="true" />
		<display:column property="PROPERTY_VALUE" title="Property Value" sortable="true" />
		<display:setProperty name="export.xml.filename" value="serffConfigurations.xml" />
		<display:setProperty name="export.csv.filename" value="serffConfigurations.csv" />
		<display:setProperty name="export.excel.filename" value="serffConfigurations.xls" />
	</display:table>
</body>
</html>