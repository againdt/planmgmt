<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="session.jsp" %>
<%@ page import="com.serff.util.SerffConstants"%>

<html lang="en">
<head>

<!-- JQuery Start -->
<link rel="stylesheet"  type="text/css" href="<c:url value='/resources/css/bootstrap.css'/>"/>
<link rel="stylesheet"  type="text/css" href="<c:url value='/resources/css/ghixcustom.css'/>"/>
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/multiselect/jquery.multiselect.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/multiselect/jquery.multiselect.filter.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/multiselect/style.css'/>" />
<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>

<script type="text/javascript" src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/bootstrap-filestyle.js'/>"></script>

<script type="text/javascript" src="<c:url value='/resources/js/multiselect/jquery.multiselect.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js/multiselect/jquery.multiselect.filter.js'/>"></script>
<!-- JQuery End -->

<!-- <script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script> -->

<script type="text/javascript">

	$(document).ready(function() {
		$(":file").filestyle({'buttonText': 'Browse ...'});

		<c:if test="${plansFor == 'ancillary'}">
		// Set Defaults values
		$("#tenantText").val($("#tenant").val());

		$("#carrier").multiselect({
			enable : true,
			multiple: false,
			selectedList: 1
		}).multiselectfilter();

		$("#tenant").multiselect({
			selectedList: 10,

			click: function(e) {

				if ($(this).multiselect("widget").find("input:checked").length > 10) {
					return false;
				}
			}
		}).multiselectfilter();
		</c:if>
	});

	function doSubmit(formName, url) {
		
		if (isValidateForm() && formName && url) {
			document.forms[formName].method = "post";
			document.forms[formName].action = url;
			document.forms[formName].submit();
		}
	}
	
	function isValidateForm() {
		
		var flag = true;
		var loForm = document.forms["frmUploadPlans"];
		
		if (loForm) {
			
			var laFields = new Array();
			var li = 0;
			var planTypeNode = loForm["planType"];
			
			if (!planTypeNode.value) {
				laFields[li++] = "Please select Plan Type.";
			}
			
			<c:if test="${plansFor == 'ancillary'}">
				var nodeValue = loForm["carrier"].value;
				if (!nodeValue || !loForm["carrierText"].value || "-1" == nodeValue) {
					laFields[li++] = "Issuer must be required.";
				}
				
				nodeValue = loForm["tenant"].value;
				if (!nodeValue || !loForm["tenantText"].value) {
					laFields[li++] = "Tenant must be required.";
				}
			</c:if>			
			var fileUploadNode = loForm["fileUploadExcel"];
			
			if (fileUploadNode.files.length == 0) {
				laFields[li++] = "Please select an excel file to upload.";
			}
			else {
				var fileName = fileUploadNode.value;
				var ext = fileName.split('.').pop();
				
				var XLSX = "xlsx" ;
				var XLS ="xls";
				
				if(ext!=XLSX && ext!=XLS) {
					laFields[li++] = "File type must be Excel.";
				}
			}
			
			if (laFields.length > 0) {
			    
				if(laFields.length > 1) {
					
					for (var fi = 0; fi < laFields.length; fi++) {
						laFields[fi] = "- " + laFields[fi];
					}
				}
		       	alert(laFields.join('\n'));
		       	flag = false;
		    }
		}
		return flag;
	}
</script>

<style>
	ul.unstyled li{
		padding:10px;
	}
</style>
</head>

<body>
<div class="row-fluid" id="main">
	<div class="container">
		<div class="row-fluid">
			<div class="gutter10">
				<div class="row-fluid">
					<h1 id="">${pageName}</h1>
				</div><!-- end of .row-fluid -->
				
				<div class="row-fluid">
					<div id="sidebar" class="span3">
						<div class="header"><span>Steps</span></div>
						<c:if test="${plansFor == 'ancillary'}">
							<ul class="unstyled">
								<li><b>Step 1: </b>Select plan type</li>
								<li><b>Step 2: </b>Please select which issuer you are attempting to load plan data for.</li>
								<li><b>Step 3: </b>Please select Tenant.</li>
								<li><b>Step 4: </b>Choose file (Excel) to upload</li>
								<li><b>Step 5: </b>Provide file description (optional)</li>
								&nbsp;&nbsp;&nbsp;<a href="getUploadSTMPlans" target="main" style="font-size: 10pt; font-weight:bold; color: blue;">Upload Ancillary Util</a><br>
								&nbsp;&nbsp;&nbsp;<a href="getSTMPlansStatus" target="main" style="font-size: 10pt; font-weight:bold; color: blue;">Uploaded Ancillary Status</a>
								
								<li class="gutter10" style="font-size: 10pt; color: green;"><b>NOTE : To load Ancillary Plans proceed to above 'Upload Ancillary Util' link after files have been successfully uploaded.</b></li>
							</ul>
						</c:if>
						<c:if test="${plansFor == 'medicaid'}">
							<ul class="unstyled">
								<li><b>Step 1: </b>Select plan type</li>
								<li><b>Step 2: </b>Choose file (Excel) to upload</li>
								<li><b>Step 3: </b>Provide file description (optional)</li>
								
								<li class="gutter10" style="font-size: 10pt; color: green;"><b>NOTE : To load Medicaid Plans proceed to 'Upload Medicaid Plans' link after files have been successfully uploaded.</b></li>
							</ul>
						</c:if>
					</div><!-- end of .span3 -->

					<div class="span9" id="rightpanel">
						<div class="header" style="height:40px"></div>
						<form:form id="frmUploadPlans" name="frmUploadPlans" action="${pageContext.request.contextPath}/admin/uploadSTMExcel" modelAttribute="UploadExcel" enctype="multipart/form-data" method="POST">
							<df:csrfToken/>

							<c:if test="${plansFor == 'ancillary'}">
								<div class="control-group">
									<label class="control-label" for="planType"><b>Step 1:</b>&nbsp;Plan Type*&nbsp;&nbsp;</label>
									<div class="controls">
										<select id="planType" name="planType">
											<option value="">Select</option>
											<option value="<%=SerffConstants.PLAN_TYPE.AME_FAMILY.name()%>">AME [Family Rates]</option>
											<option value="<%=SerffConstants.PLAN_TYPE.AME_AGE.name()%>">AME [Age Band Rates]</option>
											<option value="<%=SerffConstants.PLAN_TYPE.AME_GENDER.name()%>">AME [Age-Gender based Rates]</option>
											<option value="<%=SerffConstants.PLAN_TYPE.STM.name()%>">STM</option>
											<option value="<%=SerffConstants.PLAN_TYPE.HCC.name()%>">STM for HCC With Rates</option>
											<option value="<%=SerffConstants.PLAN_TYPE.AREA_FACTOR.name()%>">STM HCC Area Factor</option>
											<option value="<%=SerffConstants.PLAN_TYPE.VISION.name()%>">VISION</option>
											<option value="<%=SerffConstants.PLAN_TYPE.LIFE.name()%>">LIFE</option>
										</select>
									</div>
									<input type="hidden" id="returnPage" name ="returnPage" value="ancillary">
								</div>
								<div class="control-group">
					                <label for="carrier" class="control-label"><b>Step 2:</b>&nbsp;Issuer*&nbsp;&nbsp;</label>
									<select id="carrier" name="carrier" onchange="$('#carrierText').val($('#carrier option:selected').text());">
										<option value="" selected="selected">Select Issuer</option>
										<c:if test="${issuerList != null}">
											<c:forEach var="issuer" items="${issuerList}">
												<c:if test="${issuer.hiosIssuerId != null}"> 
													<option value="${issuer.hiosIssuerId}">[${issuer.hiosIssuerId}] ${issuer.shortName}</option>
												</c:if> 
											</c:forEach>
										</c:if>
									</select>
								</div>
	
								<div class="control-group">
					                <label for="tenant" class="control-label"><b>Step 3:</b>&nbsp;Tenant*&nbsp;&nbsp;</label>
									<select id="tenant" name="tenant" multiple="multiple" onchange="$('#tenantText').val($('#tenant').val());">
										<%-- <option value="" selected="selected">Select Tenant</option> --%>
										<c:if test="${tenantList != null}">
										<c:forEach items="${tenantList}" var="tenant">
										    <c:if test="${tenant.code == defaultTenantCode}">
	    										<option selected="selected" value="<c:out value="${tenant.id}" />">
													<c:out value="${tenant.name}" />
												</option>
											</c:if>
											<c:if test="${tenant.code != defaultTenantCode}">
	    										<option value="<c:out value="${tenant.id}" />">
													<c:out value="${tenant.name}" />
												</option>
											</c:if>
	  									</c:forEach>
	                                    </c:if>
									</select>
								</div>
							</c:if>
							<c:if test="${plansFor == 'medicaid'}">
								<div class="control-group">
									<label class="control-label" for="planType"><b>Step 1:</b>&nbsp;Plan Type*&nbsp;&nbsp;</label>
									<div class="controls">
										<select id="planType" name="planType">
											<option value="">Select</option>
											<option value="PLAN_MEDICAID">Medicaid</option>
										</select>
									</div>
									<input type="hidden" id="returnPage" name ="returnPage" value="medicaid">
								</div>
							</c:if>
							<input type="hidden" id="carrierText" name="carrierText" />
							<input type="hidden" id="tenantText" name="tenantText" />
							<div class="control-group">
								<c:if test="${plansFor == 'medicaid'}">
									<label class="control-label" for="fileUploadExcel"><b>Step 2:</b>&nbsp;Choose Excel File*&nbsp;&nbsp;</label>
                                </c:if>
								<c:if test="${plansFor == 'ancillary'}">
									<label class="control-label" for="fileUploadExcel"><b>Step 4:</b>&nbsp;Choose Excel File*&nbsp;&nbsp;</label>
                                </c:if>
								<div class="controls">
									<input type="file" class="filestyle" id="fileUploadExcel" name="fileUploadExcel" data-classButton="btn">
								</div>
							</div>

							<div class="control-group">
								<c:if test="${plansFor == 'medicaid'}">
									<label class="control-label" for="description"><b>Step 3:</b>&nbsp;File Description</label>
                                </c:if>
								<c:if test="${plansFor == 'ancillary'}">
									<label class="control-label" for="description"><b>Step 5:</b>&nbsp;File Description</label>
                                </c:if>
								<div class="controls">
									<textarea rows="2" cols="70" id="description" name ="description"></textarea>
								</div>
							</div>

				            <div class="row-fluid gutter10-t">
								<div class="span2">
				            		<a href="javascript:document.forms['frmUploadPlans'].reset()" class="btn pull-left" id="cancel">Cancel</a>
				            	</div>
								<div class="span2 margin10-l">
				            		<a href="javascript:doSubmit('frmUploadPlans', '${pageContext.request.contextPath}/admin/uploadSTMExcel');" class="btn btn-primary pull-left" type="submit" id="save">Upload</a>
								</div>
							</div>
						</form:form>
					</div><!-- end of .span9 -->
					<c:if test="${uploadSTMExcelMessage != null}">
						<div align="left" style="font-size: 12pt; color: green; font-weight: bold;">
							<c:out value="${uploadSTMExcelMessage}" />
						</div>
					</c:if>
				</div><!-- end of .row-fluid -->
			</div> <!-- end of .gutter10 -->
		<!--row-fluid--> 
		</div>
	<!--container--> 
	</div>
<!--main-->
</div>
</body>
</html>