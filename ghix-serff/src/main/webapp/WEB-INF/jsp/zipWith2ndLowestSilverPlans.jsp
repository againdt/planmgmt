<%@ page import="com.getinsured.hix.model.Plan"%>

<%@ include file="datasource.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<html>
<head>
	<title>Display Zip with premium for 2nd lowest silver plans</title>
	<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script type="text/javascript">
	
		window.onload = function() {
			var zipCode = "<%=request.getParameter("zipCode")%>";
		    if (zipCode && zipCode != "null") {
		    	document.getElementById('zipCode').value = zipCode;
			}
		    
			var fips = "<%=request.getParameter("fips")%>";
		    if (fips && fips != "null") {
		    	document.getElementById('fips').value = fips;
			}
		    
			var hiosIssuerId = "<%=request.getParameter("hiosIssuerId")%>";
		    if (hiosIssuerId && hiosIssuerId != "null") {
		    	document.getElementById('hiosIssuerId').value = hiosIssuerId;
			}
		    
			var applicableYear = "<%=request.getParameter("applicableYear")%>";
		    if (applicableYear && applicableYear != "null") {
		    	document.getElementById('applicableYear').value = applicableYear;
			}
		    
			var insuranceType = "<%=request.getParameter("insuranceType")%>";
		    if (insuranceType && insuranceType != "null") {
		    	document.getElementById('insuranceType').value = insuranceType;
			}
		};
	
		function setWhereClause(type) {
			var whereClause = "";
			var filterStatement = "";
			
			switch (type) {
				case "B4" : 
					// Validate the dates
				    // validateDates();
				    // Step 1: Get List Box value, if it is not empty
					var zipCode = document.getElementById("zipCode").value;
					var fips = document.getElementById("fips").value;
					var hiosIssuerId = document.getElementById("hiosIssuerId").value;
					var applicableYear = document.getElementById("applicableYear").value;
					var insuranceType = document.getElementById("insuranceType").value;
					var errorMsg = "";
					
					if (zipCode) {
						whereClause += " AND PM_ZIP_PLANS.ZIP = '"+ zipCode + "' ";
					}
					else {
						errorMsg = "ZIP Code must be required.";
					}
					
					if (fips) {
						whereClause += " AND PM_ZIP_PLANS.FIPS = '"+ fips + "' ";
					}
					else {
						
						if (errorMsg) {
							errorMsg += "\n";
						}
						errorMsg += "FIPs must be required.";
					}
					
					if (errorMsg) {
						whereClause = "";
						alert(errorMsg);
						break;
					}
					
					if (hiosIssuerId) {
						whereClause += " AND PM_ZIP_PLANS.ISSUER_PLAN_NUMBER LIKE '"+ hiosIssuerId + "%' ";
					}
					
					if (applicableYear) {
						whereClause += " AND PM_ZIP_PLANS.APPLICABLE_YEAR = '"+ applicableYear +"' ";
					}
					
					if (insuranceType) {
						whereClause += " AND PM_ZIP_PLANS.INSURANCE_TYPE = '"+ insuranceType +"' ";
					}
					break;
			}
			document.getElementById("whereClause").value = whereClause; // escape(whereClause);
			document.getElementById("filterStatement").value = filterStatement;
		}
	</script>
</head>

<body>
<b>Display Zip with premium for 2nd lowest silver plans [HIX-51472]</b>
<%
	pageContext.setAttribute("insuranceTypeOptions", Plan.PlanInsuranceType.values());
%>
<form method="POST" action="${pageContext.request.contextPath}/admin/getZipWith2ndLowestSilverPlans">
	<df:csrfToken/>
	<table border="1" cellspacing="0" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#66FFCC">
				<p>
					<label for="zipCode" class="control-label">&nbsp;<b>ZIP Code*:</b></label>
					<input type="text" id="zipCode" name="zipCode" size="5" maxlength="5" />
					
					<label for="fips" class="control-label">&nbsp;<b>FIPs*:</b></label>
					<input type="text" id="fips" name="fips" size="5" maxlength="5" />
					
					<label for="hiosIssuerId" class="control-label">&nbsp;<b>Issuer Plan Number or HIOS ID:</b></label>
					<input type="text" id="hiosIssuerId" name="hiosIssuerId" size="16" maxlength="16" />
					
					<label for="applicableYear" class="control-label">&nbsp;<b>Applicable Year:</b></label>
					<select id="applicableYear" name="applicableYear">
						<option value="" selected="selected">Select</option>
						<option value="2014">2014</option>
						<option value="2015">2015</option>
					</select>
				</p>
			</td>
		</tr>
		<tr>
			<td bgcolor="#66FFCC">
				<p>
					<label for="insuranceTypeOptions" class="control-label">&nbsp;<b>Plan Type:</b></label>
					<select id="insuranceType" name="insuranceType">
						<option value="" selected="selected">Select</option>
						<c:forEach var="insuranceTypeOption" items="${insuranceTypeOptions}">
							<option value="${insuranceTypeOption}">${insuranceTypeOption}</option>
						</c:forEach>
					</select>
					
					<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
					<button type="reset" value="Reset">Reset</button>
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
</form>

<%
	String whereClause = request.getParameter("whereClause");
	// Not displaying any data on first time page loading.
	if (whereClause != null && whereClause.trim().length() > 0) {
%>
	<sql:query dataSource="${jspDataSource}" var="zipResults">
		SELECT PM_ZIP_PLANS.ZIP, PM_ZIP_PLANS.FIPS, PM_ZIP_PLANS.ISSUER_PLAN_NUMBER, PM_ZIP_PLANS.APPLICABLE_YEAR, RATE, PM_ZIP_PLANS.NAME, PM_ZIP_PLANS.INSURANCE_TYPE
		FROM PM_ZIP_PLANS, PLAN, PLAN_HEALTH, PM_PLAN_RATE
		WHERE PM_ZIP_PLANS.ISSUER_PLAN_NUMBER = PLAN.ISSUER_PLAN_NUMBER AND
		PM_ZIP_PLANS.APPLICABLE_YEAR = PLAN.APPLICABLE_YEAR AND
		PLAN.ID = PLAN_HEALTH.PLAN_ID AND PLAN.ID = PM_PLAN_RATE.PLAN_ID AND
		PM_ZIP_PLANS.PLAN_LEVEL = 'SILVER' AND
		PLAN_HEALTH.COST_SHARING = 'CS1' AND
		PM_PLAN_RATE.RATING_AREA_ID IN
		(SELECT RATING_AREA_ID FROM PM_ZIP_COUNTY_RATING_AREA
		WHERE ZIP = PM_ZIP_PLANS.ZIP OR
		PM_ZIP_PLANS.FIPS IN (SELECT (STATE_FIPS || COUNTY_FIPS) AS FIPS FROM ZIPCODES WHERE ZIPCODE = PM_ZIP_PLANS.ZIP)
		) AND 
		PLAN.IS_DELETED = 'N' AND PM_PLAN_RATE.IS_DELETED = 'N' AND
		PM_PLAN_RATE.MIN_AGE = 24 AND PM_PLAN_RATE.MAX_AGE = 24 AND PM_PLAN_RATE.TOBACCO = 'N' AND
		CURRENT_TIMESTAMP BETWEEN PM_PLAN_RATE.EFFECTIVE_START_DATE AND PM_PLAN_RATE.EFFECTIVE_END_DATE
		<%=whereClause%>
		ORDER BY RATE
	</sql:query>

	<display:table name="${zipResults.rows}" id="zipResultsTable"
		requestURI="getZipWith2ndLowestSilverPlans" defaultsort="1" pagesize="100"
		style="white-space: pre-wrap;width: 100%;" export="true">
		<display:column property="RATE" title="Rate" sortable="true" />
		<display:column property="ZIP" title="Zip Code" sortable="true" />
		<display:column property="FIPS" title="Fips" sortable="true" />
		<display:column property="ISSUER_PLAN_NUMBER" title="Issuer Plan Number" sortable="true" />
		<display:column property="APPLICABLE_YEAR" title="Applicable Year" sortable="true" />
		<display:column property="NAME" title="Name" sortable="true" />
		<display:column property="INSURANCE_TYPE" title="Insurance Type" sortable="true" />
		
		<display:setProperty name="export.xml.filename" value="zipWith2ndLowestSilverPlans.xml" />
		<display:setProperty name="export.csv.filename" value="zipWith2ndLowestSilverPlans.csv" />
		<display:setProperty name="export.excel.filename" value="zipWith2ndLowestSilverPlans.xls" />
	</display:table>
	<%
	}
	%>
</body>
</html>