<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>
<%
	String whereClause = "id is not null";
	String andClause = "";
	String searchCriteria = "";

	if(request.getParameter("condition1")!= null && request.getParameter("condition1").equalsIgnoreCase("Y")){
		if(isPostgresDB) {
			andClause += " AND now() between START_DATE and END_DATE";
		} else {
		andClause += " AND SYSDATE between START_DATE and END_DATE";
		}
		searchCriteria += "Plan is currently effective, ";
	}
	if(request.getParameter("condition2")!= null && request.getParameter("condition2").equalsIgnoreCase("Y")){
		andClause += " AND STATUS IN ('CERTIFIED', 'RECERTIFIED')";
		searchCriteria += "Plan is certified, ";
	}
	if(request.getParameter("condition3")!= null && request.getParameter("condition3").equalsIgnoreCase("Y")){
		andClause += " AND ENROLLMENT_AVAIL ='AVAILABLE'";
		searchCriteria += "Enrollment is available, ";
	}
	if(request.getParameter("condition4")!= null && request.getParameter("condition4").equalsIgnoreCase("Y")){
		if(isPostgresDB) {
			andClause += " AND now() >= ENROLLMENT_AVAIL_EFFDATE";
		} else {
		andClause += " AND SYSDATE >= ENROLLMENT_AVAIL_EFFDATE";
		}
		searchCriteria += "Enrollment has started, ";
	}
	if(request.getParameter("condition5")!= null && request.getParameter("condition5").equalsIgnoreCase("Y")){
		andClause += " AND ISSUER_VERIFICATION_STATUS ='VERIFIED'";
		searchCriteria += "Issuer is verified, ";
	}
	if(request.getParameter("condition6")!= null && request.getParameter("condition6").equalsIgnoreCase("Y")){
		andClause += " AND IS_DELETED='N'";
		searchCriteria += "Plan is not deleted, ";
	}	
	if(request.getParameter("condition7")!= null && request.getParameter("condition7").equalsIgnoreCase("Y")){
		andClause += " AND IS_PUF='Y'";
		searchCriteria += "Plan selected is PUF plan";
	}
	
	if(andClause.equals("")){
		if(isPostgresDB) {
			andClause = "AND now() between START_DATE and END_DATE AND STATUS IN ('CERTIFIED', 'RECERTIFIED') AND ENROLLMENT_AVAIL ='AVAILABLE' AND now() >= ENROLLMENT_AVAIL_EFFDATE AND ISSUER_VERIFICATION_STATUS ='VERIFIED' AND IS_DELETED='N'";
		} else {
		andClause = "AND SYSDATE between START_DATE and END_DATE AND STATUS IN ('CERTIFIED', 'RECERTIFIED') AND ENROLLMENT_AVAIL ='AVAILABLE' AND SYSDATE >= ENROLLMENT_AVAIL_EFFDATE AND ISSUER_VERIFICATION_STATUS ='VERIFIED' AND IS_DELETED='N'";
		}
		searchCriteria = "All Plans";
	}
	
%>

<sql:query dataSource="${jspDataSource}" var="result1">
select INSURANCE_TYPE,ISSUER_PLAN_NUMBER,MARKET,APPLICABLE_YEAR,NAME,STATUS,ISSUER_ID,BROCHURE,HSA,ENROLLMENT_AVAIL,ENROLLMENT_AVAIL_EFFDATE,ISSUER_VERIFICATION_STATUS,VERIFIED,HIOSPRODUCT_ID,STATE,IS_DELETED 
from plan 
where <%=whereClause%>
<%=andClause%>
</sql:query>

<html>
<head>
	<title>Search for plans</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>
<body>
	<h1>Search for plans</h1>
	<form method="POST" action="searchForPlansTop">
	<df:csrfToken/>
		<h3>Select search criteria</h3>
		<table>
			<tr>
				<td><input id="condition1" name="condition1" type="checkBox" value="Y" checked="checked"></td>
				<td>Plan is currently effective (SYSDATE between START_DATE and END_DATE)</td>
			</tr>	
			<tr>
				<td><input id="condition2" name="condition2" type="checkBox" value="Y" checked="checked"></td>
				<td>Plan is certified (STATUS IN ('CERTIFIED', 'RECERTIFIED'))</td>
			</tr>
			<tr>
				<td><input id="condition3" name="condition3" type="checkBox" value="Y" checked="checked"></td>
				<td>Enrollment is available (ENROLLMENT_AVAIL ='AVAILABLE')</td>
			</tr>
			<tr>
				<td><input id="condition4" name="condition4" type="checkBox" value="Y" checked="checked"></td>
				<td>Enrollment has started (SYSDATE &gt;= ENROLLMENT_AVAIL_EFFDATE)</td>
			</tr>
			<tr>
				<td><input id="condition5" name="condition5" type="checkBox" value="Y" checked="checked"></td>
				<td>Issuer is verified (ISSUER_VERIFICATION_STATUS ='VERIFIED')</td>
			</tr>
			<tr>
				<td><input id="condition6" name="condition6" type="checkBox" value="Y" checked="checked"></td>
				<td>Plan is not deleted (IS_DELETED='N')</td>
			</tr>
			<tr>
				<td><input id="condition7" name="condition7" type="checkBox" value="Y" checked="checked"></td>
				<td>Is Puf(IS_PUF='Y')</td>
			</tr>
		</table>
		<input type="submit" value="Search">
	</form>
	<h3>Selected Search Criteria: <span style="color:green"><%=searchCriteria%></span></h3>

<display:table name="${result1.rows}" requestURI="/admin/searchForPlansTop" id="table1" style="white-space: pre-wrap;" export="true" pagesize="50">
	<display:column property="INSURANCE_TYPE" title="Insurance Type" sortable="true" />
	<display:column property="ISSUER_PLAN_NUMBER" title="Issuer Plan Number" sortable="true" />
	<display:column property="MARKET" title="Market Type" sortable="true" />
	<display:column property="APPLICABLE_YEAR" title="Applicable Year" sortable="true" />
	<display:column property="NAME" title="Plan Name" sortable="true" />
	<display:column property="STATUS" title="Plan Status" sortable="true" />
	<display:column property="ISSUER_ID" title="Issuer Id" sortable="true" />
	<display:column property="BROCHURE" title="Brochure" sortable="true" />
	<display:column property="HSA" title="HSA" sortable="true" />
	<display:column property="ENROLLMENT_AVAIL" title="Is enrollment Available ?" sortable="true" />
	<display:column property="ENROLLMENT_AVAIL_EFFDATE" title="Enrollment effective Date" sortable="true" />
	<display:column property="ISSUER_VERIFICATION_STATUS" title="Issuer verification status" sortable="true" />
	<display:column property="VERIFIED" title="Is Plan Verified ?" sortable="true" />
	<display:column property="HIOSPRODUCT_ID" title="HIOS Product Id" sortable="true" />
	<display:column property="STATE" title="Plan State" sortable="true" />
	<display:column property="IS_DELETED" title="Is Plan Deleted" sortable="true" />
	<display:setProperty name="export.xml.filename" value="planSerach.xml"/>
	<display:setProperty name="export.csv.filename" value="planSerach.csv"/>
	<display:setProperty name="export.excel.filename" value="planSerach.xls"/>
</display:table> 
</body>
</html>