<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="session.jsp"%>
<html>
<head>
<title>Edit Display Rules :</title>
<style type="text/css" media="all">
@import url("css/maven-base.css");
@import url("css/maven-theme.css");
@import url("css/site.css");
@import url("css/screen.css");s
</style>
<link rel="stylesheet" href="./css/print.css" type="text/css"
	media="print" />
</head>
<frameset rows="50%,*">
	<frame name="topframe" scrolling="yes" target="contents_1"
		src="getDisplayRuleTopPage?hiosIssuerId=<%=request.getAttribute("hiosIssuerId")%>"
		frameborder="1">
	<frame name="main_1" src="getDisplayRuleBottomPage" frameborder="1">
	<noframes>
		<body>
			<p>This page uses frames, but your browser doesn't support them.</p>
		</body>
	</noframes>
</frameset>
</html>