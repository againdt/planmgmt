<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="datasource.jsp" %>

<head>
	<base target="main_1">
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
</head>

<% 
String req_Id=request.getParameter("attachments");
%>
<%
if(null == req_Id || req_Id == "")
  req_Id="0";
%>
Attachments for ID: <%= req_Id%>

<sql:query dataSource="${jspDataSource}" var="result1">
select serff_doc_id, ecm_doc_id, document_name, document_type, document_size,  
'<a href="getBottomRight?attachment='||ecm_doc_id||'&'||'type='||document_type||'" target="main_1">Show</a>' as attachment, 
to_char(CREATION_TIMESTAMP,'DD-MON-YY HH24:MI:SS') as createdon
from serff_document
where serff_req_id = <%= req_Id%>
order by serff_doc_id
</sql:query>

<display:table name="${result1.rows}" id="table1" style="white-space: nowrap;">
	<display:column property="serff_doc_id" title="ID" sortable="false" />
	<display:column property="document_name" title="Doc Name" sortable="false" />
	<display:column property="document_type" title="Type" sortable="false" />
	<display:column property="attachment" title="Attachment" sortable="false" />
	<display:column property="document_size" title="Size" sortable="false" />
	<display:column property="createdon" title="Created On" sortable="false" />
</display:table>
