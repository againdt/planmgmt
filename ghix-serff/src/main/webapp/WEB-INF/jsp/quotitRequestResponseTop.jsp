<%@ page import="org.apache.commons.lang3.StringUtils"%>
<%@ page import="com.getinsured.hix.model.ExtWSCallLogs.STATUS"%>
<%@ include file="datasource.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<html>
<head>
	<title>Quotit Services Status</title>
	<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />

	<script type="text/javascript">

		window.onload = function() {
			var parentTrackingID = "<%=request.getParameter("parentTrackingID")%>";
		    if (parentTrackingID && parentTrackingID != "null") {
		    	document.getElementById('parentTrackingID').value = parentTrackingID;
			}

			var qID = "<%=request.getParameter("qID")%>";
		    if (qID && qID != "null") {
		    	document.getElementById('qID').value = qID;
			}

			var externalRequestID = "<%=request.getParameter("externalRequestID")%>";
		    if (externalRequestID && externalRequestID != "null") {
		    	document.getElementById('externalRequestID').value = externalRequestID;
			}

			var externalPodID = "<%=request.getParameter("externalPodID")%>";
		    if (externalPodID && externalPodID != "null") {
		    	document.getElementById('externalPodID').value = externalPodID;
			}

			var status = "<%=request.getParameter("status")%>";
		    if (status && status != "null") {
		    	document.getElementById('status').value = status;
			}
		};

		function setWhereClause(type) {
			var whereClause = "";
			var filterStatement = "";

			switch (type) {
				case "B4" : 
					// Validate the dates
				    // validateDates();
				    // Step 1: Get List Box value, if it is not empty
					var qID = document.getElementById("qID").value;
					var externalRequestID = document.getElementById("externalRequestID").value;
					var externalPodID = document.getElementById("externalPodID").value;
					var status = document.getElementById("status").value;

					if (qID) {
						whereClause += " AND ID LIKE '"+ qID + "%' ";
					}

					if (externalRequestID) {
						whereClause += " AND EXTERNAL_REQ_ID LIKE '"+ externalRequestID + "%' ";
					}

					if (externalPodID) {
						whereClause += " AND EXTERNAL_POD_ID LIKE '"+ externalPodID + "%' ";
					}

					if (status) {
						whereClause += " AND STATUS LIKE '"+ status + "%' ";
					}
					break;
			}
			document.getElementById("whereClause").value = whereClause; // escape(whereClause);
			document.getElementById("filterStatement").value = filterStatement;
		}
	</script>
</head>

<body>
	<form method="POST" id="frmQuotitServicesStatus" name="frmQuotitServicesStatus" action="${pageContext.request.contextPath}/admin/showQMCPlanLoadStatusTop">
		<df:csrfToken/>
		<div>
			<a href="${pageContext.request.contextPath}/admin/loadQuotitServicesStatus" target="main">Back to Status page</a> &nbsp;
			<a href="${pageContext.request.contextPath}/admin/showQMCPlanLoadStatusTop?parentTrackingID=<%=request.getParameter("parentTrackingID")%>">Refresh</a>
		</div>
		<table border="1" cellspacing="0" style="border-color:#C0C0C0">
			<tbody><tr>
				<td bgcolor="#66FFCC">
					<p>
						<label for="qID" class="control-label">&nbsp;<b>Quotit ID:</b></label>
						<input type="text" id="qID" name="qID" size="10" maxlength="10" />
						<input type="hidden" id="parentTrackingID" name="parentTrackingID" />

						<label for="externalRequestID" class="control-label">&nbsp;<b>External Request ID:</b></label>
						<input type="text" id="externalRequestID" name="externalRequestID" size="10" maxlength="10" />

						<label for="externalPodID" class="control-label">&nbsp;<b>External POD ID:</b></label>
						<input type="text" id="externalPodID" name="externalPodID" size="10" maxlength="10" />

						<label for="status" class="control-label">&nbsp;<b>Status:</b></label>
						<select id="status" name="status">
							<option value="" selected="selected">Select</option>
							<option value="<%=STATUS.IN_PROGRESS%>"><%=STATUS.IN_PROGRESS%></option>
							<option value="<%=STATUS.COMPLETED%>"><%=STATUS.COMPLETED%></option>
							<option value="<%=STATUS.FAILED%>"><%=STATUS.FAILED%></option>
						</select>
					</p>
				</td>
			</tr>
			<tr>
				<td bgcolor="#66FFCC">
					<p>
						<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
						<button type="reset" value="Reset">Reset</button>
					</p>
				</td>
			</tr>
		</tbody></table>
		<c:if test="${totalCountsOfQTStats != null}">
			<div style="font-weight: bold;">
				<c:out value="${totalCountsOfQTStats}" />
			</div>
		</c:if>
		<input id="whereClause" name="whereClause" type="hidden">
		<input id="filterStatement" name="filterStatement" type="hidden">
	</form>
<%
	String whereClause = request.getParameter("whereClause");
	String parentTrackingID = request.getParameter("parentTrackingID");

	if (StringUtils.isNumeric(parentTrackingID)) {

		if (StringUtils.isBlank(whereClause)) {
			whereClause = StringUtils.EMPTY;
		}
		whereClause = "(E1.PARENT_REQ_ID=" + parentTrackingID + " OR E1.ID=" + parentTrackingID + ") "+ whereClause;
	}
	else {
		whereClause = StringUtils.EMPTY;
	}

	if (StringUtils.isNotBlank(whereClause)) {
%>
	<sql:query dataSource="${jspDataSource}" var="quotitRequestResponseResults">
		SELECT ID, EXTERNAL_REQ_ID, EXTERNAL_POD_ID, START_TIME, END_TIME, EXTERNAL_EXEC_TIME, STATUS,
		'<a href="displayQuotitRequestResponse?id='||ID||CHR(38)||'showRequestData=true" target="bottom">Request</a>' AS REQUEST,
		'<a href="displayQuotitRequestResponse?id='||ID||CHR(38)||'showRequestData=false" target="bottom">Response</a>' AS RESPONSE,
		EXCEPTION_MESSAGE, REMARKS, BATCH_STATE FROM EXT_WS_CALL_LOG E1
		WHERE <%=whereClause%>
		ORDER BY ID DESC
	</sql:query>

	<display:table name="${quotitRequestResponseResults.rows}" id="quotitRequestResponseTable" requestURI="showQMCPlanLoadStatusTop" pagesize="100"
		style="white-space: pre-wrap;width: 100%;" export="true">
		<display:column property="ID" title="Quotit ID" sortable="true" />
		<display:column property="EXTERNAL_REQ_ID" title="External Request ID" sortable="true" />
		<display:column property="EXTERNAL_POD_ID" title="External POD ID" sortable="true" />
		<display:column property="START_TIME" title="Start Time" sortable="true" />
		<display:column property="END_TIME" title="End Time" sortable="true" />
		<display:column property="EXTERNAL_EXEC_TIME" title="External Execution Time" sortable="true" />
		<display:column property="STATUS" title="Status" sortable="true" />
		<display:column property="REQUEST" title="Request" sortable="true" />
		<display:column property="RESPONSE" title="Response" sortable="true" />
		<display:column property="EXCEPTION_MESSAGE" title="Status Messages" sortable="true" />
		<display:column property="REMARKS" title="Statistics" sortable="true" />
		<display:column property="BATCH_STATE" title="Batch State" sortable="true" />

		<display:setProperty name="export.xml.filename" value="quotitRequestResponseResults.xml" />
		<display:setProperty name="export.csv.filename" value="quotitRequestResponseResults.csv" />
		<display:setProperty name="export.excel.filename" value="quotitRequestResponseResults.xls" />
	</display:table>
<%
	}
	else {
%>
	<div style="color: red; font-weight: bold;">Quotit ID is invalid/empty</div>
<%
	}
%>
</body>
</html>