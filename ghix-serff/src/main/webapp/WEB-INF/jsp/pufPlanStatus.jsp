<html>
<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>PUF Issuers</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<body>

<sql:query dataSource="${jspDataSource}" var="result1">
select '<a href="pufPlanStatus1" target="_blank" >'||plan_id_standard_component||'</a>' as plan_id, issuer_name, state, total,success, (success*100/total) as percentSuccess, failure
from(
select distinct plan_id_standard_component, issuer_name, state, count(*) as total,count(case when serff_error_code=0 then 1 end ) as success, count(case when serff_error_code!=0 then 1 end ) as failure
from puf_qhp
group by plan_id_standard_component, issuer_name, state
order by failure desc
)allRec order by percentSuccess desc

</sql:query> 

<br>

<b>All PUF Plans Status</b>
<display:table name="${result1.rows}"  requestURI="pufPlanStatus" id="table1" export="true"  style="width:100%">
	<display:column property="plan_id" title="Plan ID" sortable="true"/>
	<display:column property="issuer_name" title="Issuer Name" sortable="true"/>
	<display:column property="state" title="State" sortable="true"/>
	<display:column property="total" title="Total" sortable="true"/>	
	<display:column property="percentSuccess" title="Success %" format="{0,number,#.##}" sortable="true"/>
	<display:column property="success" title="Success" sortable="true"/>
	<display:column property="failure" title="Failure" sortable="true"/>
	
	<display:setProperty name="export.xml.filename" value="pufPlans.xml"/>
	<display:setProperty name="export.csv.filename" value="pufPlans.csv"/>
	<display:setProperty name="export.excel.filename" value="pufPlans.xls"/>
</display:table>


 </body> 
 </html>


