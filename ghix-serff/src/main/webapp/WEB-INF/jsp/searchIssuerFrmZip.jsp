<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<html>
<head>
	<title>Search for plans</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<%
String [] array = request.getParameterValues("selectzip");

String whereClause =null;
if(array!=null){
	StringBuilder createWhere= new StringBuilder("zip in (") ;
	for(int i=0 ;i<array.length;i++){
		String value = array[i];
		createWhere.append("'"+value +"',");
		
	}
	 whereClause =createWhere.substring(0,createWhere.length()-1);

	whereClause=whereClause+ ")";
}else{
	whereClause="zip is not null";
}
%>

<body>
<b>Search Issuers for given Zip code</b>
	<form method="POST" action="getIssuerFromZip">
	<df:csrfToken/>		
		<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
			       Zip Code:
				<input type ="text" id="selectzip" name="selectzip" multiple="multiple">
				 <input type ="hidden" id ='list' name =list value="">
				<input type="submit" value="Search">
			</td>
			</tr>
		</table>
</body>

<sql:query dataSource="${jspDataSource}" var="result1">
<%-- select hios_issuer_id , short_name from issuers where id in (
select issuer_id from plan where issuer_plan_number in (
select distinct p.issuer_plan_number from  plan p , pm_plan_rate ppr where p.id=ppr.plan_id and
ppr.rating_area_id in (select rating_area_id from pm_zip_county_rating_area where <%=whereClause %>) and 
p.service_area_id in (select service_area_id from pm_service_area where <%=whereClause %> ))) --%>

SELECT * FROM ISSUERS WHERE HIOS_ISSUER_ID IN (
SELECT HIOS_ISSUER_ID FROM PM_ISSUER_SERVICE_AREA WHERE ID IN
(SELECT SERVICE_AREA_ID FROM PM_SERVICE_AREA WHERE <%=whereClause %>))
</sql:query>

<display:table name="${result1.rows}" requestURI="/admin/getIssuerFromZip" id="table1" style="white-space: pre-wrap;" export="true" pagesize="50">
	<display:column property="hios_issuer_id" title="HIOS Issuer ID" sortable="true" />
	<display:column property="short_name" title="Issuer Short Name" sortable="true" />
	<display:setProperty name="export.xml.filename" value="ZipPlans.xml"/>
	<display:setProperty name="export.csv.filename" value="ZipPlans.csv"/>
	<display:setProperty name="export.excel.filename" value="ZipPlans.xls"/>
</display:table>

<br>
Note: Query to check records in DB <br>
select * from issuers where hios_issuer_id in (
select hios_issuer_id from pm_issuer_service_area where id in
(select service_area_id from pm_service_area where zip =?));<br>
</html>