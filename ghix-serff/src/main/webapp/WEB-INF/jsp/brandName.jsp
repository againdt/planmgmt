<html>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>Issuer D2C Info</title>
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<body>
<sql:query dataSource="${jspDataSource}" var="result1">
select i.hios_issuer_id , i.short_name , i.marketing_name ,pibn.brand_name ,pibn.brand_url from 
issuers i , pm_issuer_brand_name pibn  where i.brand_name_id = pibn.id and pibn.is_deleted ='N'
</sql:query> 
<b>Issuer D2C Info.</b>

<display:table name="${result1.rows}"  requestURI="issuerD2C" id="table1" export="true" pagesize="500" style="width:80%" defaultorder="descending" >
	<display:column property="hios_issuer_id" title="HIOS Issuer Id " sortable="true" />
	<display:column property="short_name" title="Issuer Name" sortable="true" />
	<display:column property="marketing_name" title="Issuer Marketing Name" sortable="true" />
	<display:column property="brand_name" title="Brand Name" sortable="true" />
	<display:column property="brand_url" title="Brand URL" sortable="true" />
	<display:setProperty name="export.xml.filename" value="IssuerD2C.xml"/>
	<display:setProperty name="export.csv.filename" value="IssuerD2C.csv"/>
	<display:setProperty name="export.excel.filename" value="IssuerD2C.xls"/>
</display:table>
<br>
Note: Query to check records in DB <br>
select i.hios_issuer_id , i.short_name , i.marketing_name ,pibn.brand_name ,pibn.brand_url from 
issuers i , pm_issuer_brand_name pibn  where i.brand_name_id = pibn.id and pibn.is_deleted ='N';
</body> 
</html>
