<%@page import="java.text.SimpleDateFormat"%>
<%@ include file="datasource.jsp" %>
<%@ page import = "java.util.*" %>
<%@ page import = "java.text.*" %>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<form action="">
<input type="radio" name="query" value="query1">All (Valid/Invalid)
<input type="radio" name="query" value="query2">On Shelf (All Valid)
<input type="radio" name="query" value="query4">PUF Only (Valid/Invalid) 
<input type="radio" name="query" value="query3">On Shelf PUF Only(All Valid)
<input type="submit" value="Submit">
</form>

<%
	String whereClause = "";
    String searchType = "All (Valid/Invalid)";
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm:ss", Locale.ENGLISH);
    String displayDate = sdf.format(new Date());
	
	if(request.getParameter("query")!= null && request.getParameter("query").equalsIgnoreCase("query1")){
		whereClause = "";
		searchType = "All (Valid/Invalid)";
	}
	if(request.getParameter("query")!= null && request.getParameter("query") .equalsIgnoreCase("query2")){
		if(isPostgresDB) {
			whereClause = " where status IN ('CERTIFIED', 'RECERTIFIED') AND enrollment_avail ='AVAILABLE' AND now() >= enrollment_avail_effdate AND issuer_verification_status ='VERIFIED' and is_deleted='N' ";
		} else {
		whereClause = " where status IN ('CERTIFIED', 'RECERTIFIED') AND enrollment_avail ='AVAILABLE' AND sysdate >= enrollment_avail_effdate AND issuer_verification_status ='VERIFIED' and is_deleted='N' ";
		}
		searchType = "On Shelf (All Valid)";
	}
	if(request.getParameter("query")!= null && request.getParameter("query").equalsIgnoreCase("query3")){
		if(isPostgresDB) {
			whereClause = " where status IN ('CERTIFIED', 'RECERTIFIED') AND enrollment_avail ='AVAILABLE' AND now() >= enrollment_avail_effdate AND issuer_verification_status ='VERIFIED' and is_deleted='N' AND IS_PUF='Y' ";
		} else {
		whereClause = " where status IN ('CERTIFIED', 'RECERTIFIED') AND enrollment_avail ='AVAILABLE' AND sysdate >= enrollment_avail_effdate AND issuer_verification_status ='VERIFIED' and is_deleted='N' AND IS_PUF='Y' ";
		}
		searchType = "On Shelf PUF Only(All Valid)";
	}	
	if(request.getParameter("query")!= null && request.getParameter("query").equalsIgnoreCase("query4")){
		whereClause = " where IS_PUF='Y' ";
		searchType = "PUF Only (Valid/Invalid)";
	}	
%>

<!-- Query for plan status  -->
<sql:query dataSource="jdbc/ghixDS" var="result1">
select status, count(*) as total from plan <%=whereClause%> group by status 
</sql:query> 

<!-- Query for plan is_deleted  -->
<sql:query dataSource="jdbc/ghixDS" var="result2">
select distinct is_deleted, count(*) as total from plan <%=whereClause%> group by is_deleted
</sql:query> 

<!-- Query for Issuer Status  -->
<sql:query dataSource="jdbc/ghixDS" var="result3">
select distinct issuer_verification_status, count(*) as total from plan <%=whereClause%> group by issuer_verification_status
</sql:query>

<!-- Query for Enrollment Availability  -->
<sql:query dataSource="jdbc/ghixDS" var="result4">
select distinct enrollment_avail, count(*) as total from plan <%=whereClause%> group by enrollment_avail
</sql:query>

<!-- Query for Insurance Type  -->
<sql:query dataSource="jdbc/ghixDS" var="result5">
select distinct INSURANCE_TYPE, count(*) as total from plan <%=whereClause%> group by INSURANCE_TYPE
</sql:query>

<!-- Query for Market  -->
<sql:query dataSource="jdbc/ghixDS" var="result6">
select distinct MARKET, count(*) as total from plan <%=whereClause%> group by MARKET
</sql:query>

<!-- Query for Exchange Type  -->
<sql:query dataSource="jdbc/ghixDS" var="result7">
select exchange_type, count(*) as total from plan <%=whereClause%> group by exchange_type
</sql:query>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>"/>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Pie Charts</title>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- <script type="text/javascript" src="../resources/js/jsapi.js"></script> -->

<script type="text/javascript">
    google.load('visualization', '1', {packages: ['corechart']});

      function drawVisualization() {
    	  		
    	// Plan Status
        var planStatusData = google.visualization.arrayToDataTable([
		                ['Status', 'Total'],
				<c:forEach var="row" items="${result1.rows}">
					['<c:out value="${row.status}"/>',<c:out value="${row.total}"/>],
				</c:forEach>
		       ]);
    	
     	// Is Deleted
        var isDeletedData = google.visualization.arrayToDataTable([
		                ['Is Deleted', 'Total'],
				<c:forEach var="row" items="${result2.rows}">
					['<c:out value="${row.is_deleted}"/>',<c:out value="${row.total}"/>],
				</c:forEach>
		       ]);
     	
     	// Issuer Status
        var issuerData = google.visualization.arrayToDataTable([
		                ['Issuer Status', 'Total'],
				<c:forEach var="row" items="${result3.rows}">
					['<c:out value="${row.issuer_verification_status}"/>',<c:out value="${row.total}"/>],
				</c:forEach>
		       ]);
     	
     	// Enrollment Availability
        var enrollData = google.visualization.arrayToDataTable([
		                ['Enrollment Availability', 'Total'],
				<c:forEach var="row" items="${result4.rows}">
					['<c:out value="${row.enrollment_avail}"/>',<c:out value="${row.total}"/>],
				</c:forEach>
		       ]);
            
     	// Insurance Type
        var insuranceTypeData = google.visualization.arrayToDataTable([
		                ['Insurance Type', 'Total'],
				<c:forEach var="row" items="${result5.rows}">
					['<c:out value="${row.INSURANCE_TYPE}"/>',<c:out value="${row.total}"/>],
				</c:forEach>
		       ]);
     
     	// Market
        var marketData = google.visualization.arrayToDataTable([
		                ['Market', 'Total'],
				<c:forEach var="row" items="${result6.rows}">
					['<c:out value="${row.MARKET}"/>',<c:out value="${row.total}"/>],
				</c:forEach>
		       ]);
     
    	 // Exchange Type
        var exchangeTypeData = google.visualization.arrayToDataTable([
		                ['Exchange Type', 'Total'],
				<c:forEach var="row" items="${result7.rows}">
					['<c:out value="${row.exchange_type}"/>',<c:out value="${row.total}"/>],
				</c:forEach>
		       ]);
              
       new google.visualization.PieChart(document.getElementById('planStatus')).draw(planStatusData, {title:"Plan Status", pieSliceText:"percentage"});
       new google.visualization.PieChart(document.getElementById('isDeleted')).draw(isDeletedData, {title:"Is Deleted", pieSliceText:"percentage"});
       new google.visualization.PieChart(document.getElementById('issuerData')).draw(issuerData, {title:"Issuer Status", pieSliceText:"percentage"});
       
       new google.visualization.PieChart(document.getElementById('enrollmentAvail')).draw(enrollData, {title:"Enrollment Availability", pieSliceText:"percentage"});
       new google.visualization.PieChart(document.getElementById('insuranceType')).draw(insuranceTypeData, {title:"Insurance Type", pieSliceText:"percentage"});
       new google.visualization.PieChart(document.getElementById('market')).draw(marketData, {title:"Market", pieSliceText:"percentage"});
       
       new google.visualization.PieChart(document.getElementById('exchange')).draw(exchangeTypeData, {title:"Exchange Type", pieSliceText:"percentage"});
      }
    

    google.setOnLoadCallback(drawVisualization);
</script>
</head>
<body style="font-family: Arial;border: 0 none;">
<h4></h4>
<h4>Plans Statistics : <%=searchType%>&nbsp;&nbsp;<%=displayDate%></h4>
	<table>
		<tr>
			<td><div id="planStatus" style="width: 600px"></div></td>
			<td><b>Plan Status</b> <display:table name="${result1.rows}"
					requestURI="pieCharts" id="table1" >
					<display:column property="status" title="Status" sortable="false" />
					<display:column property="total" title="Total" sortable="false" />
				</display:table></td>
		</tr>
		<tr>
			<td><div id="isDeleted" style="width: 600px"></div></td>
			<td><b>Is deleted</b> <display:table name="${result2.rows}"
					requestURI="pieCharts" id="table2" >
					<display:column property="is_deleted" title="Is Deleted" sortable="false" />
					<display:column property="total" title="Total" sortable="false" />
				</display:table></td>
		</tr>
		<tr>
			<td><div id="issuerData" style="width: 600px"></div></td>
			<td><b>Issuer Status</b> <display:table name="${result3.rows}"
					requestURI="pieCharts" id="table3" >
					<display:column property="issuer_verification_status" title="Issuer Status" sortable="false" />
					<display:column property="total" title="Total" sortable="false" />
				</display:table></td>
		</tr>
		
		<tr>
			<td><div id="enrollmentAvail" style="width: 600px"></div></td>
			<td><b>Enrollment Availability</b> <display:table name="${result4.rows}"
					requestURI="pieCharts" id="table4" >
					<display:column property="enrollment_avail" title="Enrollment Availability" sortable="false" />
					<display:column property="total" title="Total" sortable="false" />
				</display:table></td>
		</tr>
		<tr>
			<td><div id="insuranceType" style="width: 600px"></div></td>
			<td><b>Insurance Type</b> <display:table name="${result5.rows}"
					requestURI="pieCharts" id="table5" >
					<display:column property="INSURANCE_TYPE" title="Insurance Type" sortable="false" />
					<display:column property="total" title="Total" sortable="false" />
				</display:table></td>
		</tr>
		<tr>
			<td><div id="market" style="width: 600px"></div></td>
			<td><b>Market</b> <display:table name="${result6.rows}"
					requestURI="pieCharts" id="table6" >
					<display:column property="MARKET" title="Market" sortable="false" />
					<display:column property="total" title="Total" sortable="false" />
				</display:table></td>
		</tr>
		<tr>
			<td><div id="exchange" style="width: 600px"></div></td>
			<td><b>Exchange Type</b> <display:table name="${result7.rows}"
					requestURI="pieCharts" id="table7" >
					<display:column property="exchange_type" title="Exchange Type" sortable="false" />
					<display:column property="total" title="Total" sortable="false" />
				</display:table></td>
		</tr>
	</table>
</body>
</html>

