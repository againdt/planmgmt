<%@page import="com.serff.util.SerffInfoRec"%>
<%@page import="com.serff.util.SerffInfo"%>
<%@page import="com.getinsured.hix.platform.startup.*"%>
<%@include file="datasource.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>"/>

<%
	if(request.getSession().getAttribute("serffRecords") == null){
		SerffInfo serffInfo = (SerffInfo) ctx.getBean("serffBean");
		List<SerffInfoRec> records = serffInfo.getSerffInfo();
		request.getSession().setAttribute("serffRecords", records );
	}
%>
<h3>SERFF Information</h3>
<display:table name="${sessionScope.serffRecords}"  requestURI="serffInfo" id="table" style="white-space: pre-wrap;width: 100%;" export="true" pagesize="1000" defaultsort="1"> 
	<display:column property="group" title="Group" sortable="true" />
	<display:column property="attribute" title="Attribute" sortable="true" />
	<display:column property="value" title="Value" sortable="true" />
	<display:setProperty name="export.xml.filename" value="serffInfo.xml"/>
	<display:setProperty name="export.csv.filename" value="serffInfo.csv"/>
	<display:setProperty name="export.excel.filename" value="serffInfo.xls"/>
</display:table>
