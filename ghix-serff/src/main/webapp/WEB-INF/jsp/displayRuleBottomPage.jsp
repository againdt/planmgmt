<%@ page language="java" contentType="text/html; charset=ISO-8859-15" pageEncoding="UTF-8"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Plan Display Rules</title>
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="/resources/demos/style.css" />
<link rel="stylesheet" href="../resources/css/displaytag_serff.css"
	type="text/css" charset="utf-8" />
<script>
	function validateInput() {
		var ruleId = document.getElementById("ruleId").value;
	    if(null == ruleId || isNaN(ruleId) || "" == ruleId){
			 alert("Invalid data. No rule has been selected to save.");
			 document.getElementById("tileDisplayTemplate").value = "";
			 document.getElementById("standardDisplayTemplate").value = "";
			 document.getElementById("primaryCareT1ForNumCopay").value = "";
			 document.getElementById("primaryCareT1ForNumVisits").value = "";
			 document.getElementById("primaryCareT1ForCopayAndVisits").value = "";
			 return false;
		 }
	}
</script>
</head>
<body>
    <form method="POST" action="saveDisplayRule" name="saveDisplayRuleForm" id="saveDisplayRuleId" target="main">
    <table width="100%" cellpadding="1px" cellspacing="0px" bordercolor="#FFCC00" border="1px">
		<tr>
			<td width="25%" bgcolor="#FFCC00" align="left"><font
				face="Arial" size="2"><b>Copay Value </b></font></td>
			<td width="25%" bgcolor="#FFCC00" align="left"><font
				face="Arial" size="2"><b>Copay Attribute</b></font></td>
			<td width="25%" bgcolor="#FFCC00" align="left"><font
				face="Arial" size="2"><b>Coinsurance Value</b></font></td>
			<td width="25%" bgcolor="#FFCC00" align="left"><font
				face="Arial" size="2"><b>Coinsurance Attribute</b></font></td>
		</tr>
		<tr>
			<td width="25%"  align="left"><font
				face="Arial" size="2"><b><input size="30" type="text" readonly="true" id="copayValue" style="border: none;" /></b></font></td>
			<td width="25%"  align="left"><font
				face="Arial" size="2"><b><input size="30" type="text" readonly="true"  id="copayAttribute" style="border: none;" /></b></font></td>
			<td width="25%"  align="left"><font
				face="Arial" size="2"><b><input size="30" type="text" readonly="true"  id="coinsuranceValue" style="border: none;" /></b></font></td>
			<td width="25%"  align="left"><font
				face="Arial" size="2"><b><input size="30" type="text" readonly="true"  id="coinsuranceAttribute" style="border: none;" /></b></font></td>
		</tr>
	</table>
	<table width="100%" cellspacing="1px" >
		<tr>
			<td width="40%" bgcolor="#FFCC00" align="left"><font
				face="Arial" size="2"><b>Tile Display </b></font></td>

			<td width="60%"  align="left"><font
				face="Arial" size="2"><input size="100" maxlength="250" style="height:22px" type="text" id="tileDisplayTemplate"  name="tileDisplayTemplate" value="<c:out value='${param.tileDisplayTemplate}'/>"></font></td>
		</tr>
		<tr>
			<td width="50%" bgcolor="#FFCC00" align="left"><font
				face="Arial" size="2"><b>Standard Display</b></font></td>

			<td width="50%"  align="left"><font
				face="Arial" size="2"><input size="100"  maxlength="250" style="height:22px" type="text" id="standardDisplayTemplate" name="standardDisplayTemplate" value="<c:out value='${param.standardDisplayTemplate}'/>"></font></td>
		</tr>
		<tr>
			<td width="40%" bgcolor="#FFCC00" align="left"><font
				face="Arial" size="2"><b>Primary Care Tier1 Display For Number Of Copay</b></font></td>

			<td width="60%" align="center"><font
				face="Arial" size="2"><input size="100"  maxlength="250" style="height:22px" type="text" id="primaryCareT1ForNumCopay" name="primaryCareT1ForNumCopay" value="<c:out value='${param.primaryCareT1ForNumCopay}'/>"></font></td>
		</tr>
		<tr>
			<td width="40%" bgcolor="#FFCC00" align="left"><font
				face="Arial" size="2"><b>Primary Care Tier1 Display For Number Of Visits</b></font></td>

			<td width="60%" align="center"><font
				face="Arial" size="2"><input size="100"  maxlength="250" style="height:22px" type="text" id="primaryCareT1ForNumVisits" name="primaryCareT1ForNumVisits" value="<c:out value='${param.primaryCareT1ForNumVisits}'/>"></font></td>
		</tr>
		<tr>
			<td width="40%" bgcolor="#FFCC00" align="left"><font
				face="Arial" size="2"><b>Primary Care Tier1 Display For Number Of Copay And Visits</b></font></td>

			<td width="60%" align="center"><font
				face="Arial" size="2"><input size="100"  maxlength="250" style="height:22px" type="text" id="primaryCareT1ForCopayAndVisits" name="primaryCareT1ForCopayAndVisits" value="<c:out value='${param.primaryCareT1ForCopayAndVisits}'/>"></font></td>
		</tr>	
		<tr>
			<td></td><td width="60%" style="text-align: right;"><input type="submit" onclick="return validateInput();" value="Save" style="height: 25px; width: 70px;background-color:#FFCC00;" align="right" id="saveRuleButton" name="saveRuleButton" disabled="true"></td>
		</tr>		
	</table>
	<input id="hiosIssuerId" name="hiosIssuerId" value="<%=request.getParameter("hiosIssuerIdForEdit")%>" type="hidden"/>
	<input id="ruleId" name="id" value="<%=request.getParameter("ruleId")%>" type="hidden"/>
	</form>
	
	<table style="font-size:12px; font-family:arial; color:blue" width="100%" border="1" cellpadding="0" cellspacing="0">
		<tr style="color:white; background-color:blue; font-weight:bold" >
			<td width="7%">Use</td>
			<td width="18%">To Display</td>
			<td width="7%">Use</td>
			<td width="18%">To Display</td>
			<td width="7%">Use</td>
			<td width="18%">To Display</td>
			<td width="7%">Use</td>
			<td width="18%">To Display</td>
		</tr>
		<tr>
			<td>%1$s</td>
			<td>Coinsurance Value</td>
			<td>%2$s</td>
			<td>Coinsurance Attribute</td>
			<td>%3$s</td>
			<td>Copay Value</td>
			<td>%4$s</td>
			<td>Copay Attribute</td>
		</tr>
		<tr>
			<td>%5$s</td>
			<td>Number Of Visits</td>
			<td>%6$s</td>
			<td>Number Of Copay</td>
			<td>%%</td>
			<td>%</td>
			<td>$</td>
			<td>$</td>
		</tr>
	</table>
</body>
</html>