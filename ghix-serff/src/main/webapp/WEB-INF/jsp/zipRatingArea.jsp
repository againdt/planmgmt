<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Zip Rating Area</title>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="/resources/demos/style.css" />
<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
<script>
</script>
</head>
<style>
table#table2 {
	border: 1px solid #666;
	width: 100%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}

table#table3 {
	border: 1px solid #666;
	width: 100%;
	margin: 20px 0 20px 0 !important;
	font-size: 14px;
}
</style>
<body>

<sql:query dataSource="${jspDataSource}" var="stateList">
select distinct pra.state as state from pm_zip_county_rating_area pcra, pm_rating_area pra 
where pra.id=pcra.rating_area_id order by state
</sql:query> 

<form method="GET" action="zipRatingArea">       
<table>
	<tr>
    	<td width="100%"><font size="4"><b>Zip Rating Area : </b></font></td>
    </tr>
</table>
<br>

<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#66FFCC">
				<p>
					&nbsp;Select State :
					<select name="state" id="state">
					<option value="">Select</option>
					<c:forEach var="row" items="${stateList.rows}">
					<option value="${row.state}">${row.state}</option>
					</c:forEach>
					</select>
					&nbsp;
				</p>
			</td>
			
			<td bgcolor="#66FFCC">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" value="Submit" name="submit">&nbsp;&nbsp;&nbsp;
			</td>
			
		</tr>
</tbody></table>
</form>
</body>

<sql:query dataSource="${jspDataSource}" var="result1">
select pcra.zip as zip, pcra.county as county,pra.rating_area as ratingArea
from pm_zip_county_rating_area pcra, pm_rating_area pra 
where pra.id=pcra.rating_area_id 
and pra.state='<%=request.getParameter("state")%>'	
</sql:query> 

<sql:query dataSource="${jspDataSource}" var="result2">
select distinct pcra.zip as zip
from pm_zip_county_rating_area pcra, pm_rating_area pra 
where pra.id=pcra.rating_area_id 
and pra.state= '<%=request.getParameter("state")%>'	
<%if(isPostgresDB) {%>
except
<%} else {%>
minus 
<%}%> 
select zipcode from zipcodes where state='<%=request.getParameter("state")%>'	
order by zip
</sql:query> 

<sql:query dataSource="${jspDataSource}" var="result3">
select zipcode as zip from zipcodes where state='<%=request.getParameter("state")%>'
<%if(isPostgresDB) {%>
except
<%} else {%>
minus 
<%}%> 
select distinct pcra.zip 
from pm_zip_county_rating_area pcra, pm_rating_area pra 
where pra.id=pcra.rating_area_id 
and pra.state='<%=request.getParameter("state")%>'	
order by zip
</sql:query> 
<br>		

<table>
	<tr>
    	<td width="100%"><font size="4"><b>Rating Area And Zips: </b></font></td>
    </tr>
</table>
<display:table name="${result1.rows}"  requestURI="zipRatingArea" id="table1" export="true"  style="width:80%" defaultorder="descending" pagesize="10">
<display:column property="zip" title="Zip"  />
<display:column property="county" title="County"  />
<display:column property="ratingArea" title="Rating Area"  />
<display:setProperty name="export.xml.filename" value="ZipAndCountyRatingArea.xml"/>
<display:setProperty name="export.csv.filename" value="ZipAndCountyRatingArea.csv"/>
<display:setProperty name="export.excel.filename" value="ZipAndCountyRatingArea.xls"/>
</display:table>
<%
if(null != request.getParameter("state")){
	%>
	<b>SQL Query :</b> select pcra.zip as zip, pcra.county as county,pra.rating_area as ratingArea
from pm_zip_county_rating_area pcra, pm_rating_area pra 
where pra.id=pcra.rating_area_id 
and pra.state='<%=request.getParameter("state")%>'
<%
}
%>
<br><br>

<table>
	<tr>
    	<td width="100%"><font size="4"><b>Zips in Rating Area but not in Zipcodes: </b></font></td>
    </tr>
</table>
<display:table name="${result2.rows}"  requestURI="zipRatingArea" id="table2" export="true"  style="width:20%" defaultorder="descending" pagesize="10">
<display:column property="zip" title="Zip"  />
<display:setProperty name="export.xml.filename" value="ZipsNotInRatingArea.xml"/>
<display:setProperty name="export.csv.filename" value="ZipsNotInRatingArea.csv"/>
<display:setProperty name="export.excel.filename" value="ZipsNotInRatingArea.xls"/>
</display:table>
<%
if(null != request.getParameter("state")){
	%>
	<b>SQL Query :</b> select distinct pcra.zip as zip
	from pm_zip_county_rating_area pcra, pm_rating_area pra 
where pra.id=pcra.rating_area_id 
and pra.state= '<%=request.getParameter("state")%>'	
<%if(isPostgresDB) {%>
except
<%} else {%>
minus 
<%}%> 
select zipcode from zipcodes where state='<%=request.getParameter("state")%>'
<%
}
%>
<br><br>

<table>
	<tr>
    	<td width="100%"><font size="4"><b>Zips in Zipcodes but not in Rating Area: </b></font></td>
    </tr>
</table>
<display:table name="${result3.rows}"  requestURI="zipRatingArea" id="table3" export="true"  style="width:20%" defaultorder="descending" pagesize="10">
<display:column property="zip" title="Zip"  />
<display:setProperty name="export.xml.filename" value="ZipsNotInZipcodes.xml"/>
<display:setProperty name="export.csv.filename" value="ZipsNotInZipcodes.csv"/>
<display:setProperty name="export.excel.filename" value="ZipsNotInZipcodes.xls"/>
</display:table>
<%
if(null != request.getParameter("state")){
	%>
	<b>SQL Query :</b> select zipcode as zip from zipcodes where state='<%=request.getParameter("state")%>'
<%if(isPostgresDB) {%>
except
<%} else {%>
minus 
<%}%> 
select distinct pcra.zip 
from pm_zip_county_rating_area pcra, pm_rating_area pra 
where pra.id=pcra.rating_area_id 
and pra.state='<%=request.getParameter("state")%>'	
order by zip
<%
}
%>

</html>