<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@include file="datasource.jsp" %>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Plan Management View Data</title>
<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />

<script type="text/javascript">

	function querySelected(listbox)
	{
	 var selected = listbox.options[listbox.selectedIndex].value;  
	 var displayWhere = "";
	 var query = "";
	  
		if (selected == "pmq1")
		{
			displayWhere = "HIOS_ISSUER_ID";
			query = "Select accreditation_exp_date, accrediting_entity, address_line1, address_line2, certification_doc, "+
					"certification_status, certified_on, city, comment_id, company_address_line1, company_address_line2, company_city, "+
					"company_legal_name, company_logo, company_site_url, company_state, company_zip, contact_person, "+
					"creation_timestamp, decertified_on, effective_end_date, effective_start_date, EMAIL_ADDRESS, enrollment_url, "+
					"FEDERAL_EIN, hios_issuer_id, indv_cust_service_phone, indv_cust_service_phone_ext, "+
					"indv_cust_service_tty, indv_cust_service_toll_free, indv_site_url, initial_payment, "+
					"issuer_accreditation, last_update_timestamp, last_updated_by, license_number, license_status, "+
					"marketing_name, naic_company_code, naic_group_code, name, org_chart, PHONE_NUMBER, "+
					"recurring_payment, shop_cust_service_phone, shop_cust_service_phone_ext, shop_cust_service_tty, "+
					"shop_cust_service_toll_free, shop_site_url, short_name, site_url, state, state_of_domicile, txn_820_Version, "+
					"txn_834_Version, zip, id from issuers "+
					"WHERE ISSUERS.HIOS_ISSUER_ID=";
		}
		else if (selected == "pmq2")
		{
			displayWhere = "ISSUER_ID";
			query = "select Issuers.state,network.name,network.id,network.network_url "+
					"from Issuers , network "+
					"where network.issuer_id=";
		}
		else if (selected == "pmq3")
		{
			displayWhere = "HIOS_ISSUER_ID";
			query="Select PM_ISSUER_SERVICE_AREA.Issuer_state,PM_ISSUER_SERVICE_AREA.HIOS_ISSUER_ID, "+
					"PM_ISSUER_SERVICE_AREA.serff_service_area_id,PM_ISSUER_SERVICE_AREA.service_area_name, "+
					"pm_service_area.zip, pm_service_area.county " +
					"from PM_ISSUER_SERVICE_AREA, pm_service_area "+
					"where pm_service_area.service_Area_id =  PM_ISSUER_SERVICE_AREA.id "+
					"and  PM_ISSUER_SERVICE_AREA.HIOS_ISSUER_ID=";
			
		}else if (selected == "pmq4")
		{
			displayWhere = "HIOS_ISSUER_ID";
			query="Select id,ncqa_org_id, ncqa_sub_id, id ,accreditation_status, expiration_Date, hios_issuer_id from NCQA where hios_issuer_id=";
			
		}else if (selected == "pmq5")
		{
			displayWhere = "HIOS_ISSUER_ID";
			query="Select id ,accreditation_status, app_num, expiration_Date, hios_issuer_id from URAC Where hios_Issuer_id=";
			
		}else if (selected == "pmq6")
		{
			displayWhere = "ISSUER_ID";
			query="Select brochure, category, certified_on, certifiedby, comment_id, createdby, creation_timestamp, end_date, "+ 
					"enrollment_avail, enrollment_avail_effdate, formularly_id, formularly_url, hiosProduct_id, hpid, hsa, "+
					"insurance_type, issuer_id, issuer_plan_number, issuer_verification_status, last_update_timestamp, last_updated_by, " + 
					"market, max_enrolee, name, provider_network_id, network_id, network_type, start_date, status, support_file, verified, id from Plan where issuer_id=";
			
		}else if (selected == "pmq7")
		{
			displayWhere = "PLAN_ID";
			query="Select acturial_value_certificate, av_calc_output_number, benefit_effective_date, benefit_file, benefits_url, child_only_offering, comment_id, "+
					"cost_sharing, created_by, creation_timestamp, csr_adv_pay_amt, disease_mgmt_program_offered, eb_ambulatorty_patient, "+
					"eb_benchmark, eb_emergency, eb_hospitalization, eb_laboratory, eb_maternity, eb_Mental_Health, eb_pediatric, "+
					"eb_Prescription, eb_preventive_wellness, eb_rehabilitative, effective_end_date, effective_start_date, "+
					"ehb_covered, enrollment_url, is_qhp, last_update_timestamp, last_updated_by, multiple_network_tiers, "+
					"parent_plan_id, plan_id, plan_level, plan_level_exclusions, pregnancy_notice, rate_effective_date, "+
					"rate_file, specialist_referal, status, tier1_util, tier2_util, unique_plan_design, "+
					"wellness_program_offered, id from plan_health where Plan_id=";
			
		}
		else if (selected == "pmq8")
		{
			displayWhere = "PLAN_HEALTH_ID";
			query="Select combined_in_and_out_net_attr, combined_in_and_out_of_network, effective_end_date, effective_start_date, excluded_from_in_net_moop, "+
				"excluded_from_out_of_net_moop, explanation, is_covered, is_ehb, min_stay, name, network_attr, network_exceptions, "+
				"network_limitation, network_limitation_attr, network_tier_2, network_value, nonnetwork_attr, nonnetwork_exceptions, "+
				"nonnetwork_limitation, nonnetwork_limitation_attr, nonnetwork_value, plan_health_id, subject_to_in_net_deductible, "+
				"subject_to_out_net_deductible, id from plan_health_benefit  where plan_health_id=";
			
		}
		else if (selected == "pmq9")
		{
			displayWhere = "ISSUER_ID";
			query="Select CREATED, FORMULARY_ID, FORMULARY_URL, ISSUER_ID, UPDATED, id from FORMULARY  where issuer_id=";
			
		}
		else if (selected == "pmq10")
		{
			displayWhere = "FORMULARY_ID";
			query="Select CREATED, FORMULARY_ID, TIER_ID, UPDATED, id from DCS_TIER_TYPE where formulary_id=";
			
		}
		else if (selected == "pmq11")
		{
			displayWhere = "DCS_TIER_TYPE_ID";
			query="Select CREATED, DCS_TIER_TYPE_ID, NAME, UPDATED, id from DRUG_CODE_TIER_CODE_TYPE  where DCS_TIER_TYPE_ID=";
			
		}else if (selected == "pmq12")
		{
			displayWhere = "DCS_TIER_TYPE_ID";
			query="Select BENEFIT_OFFERED, CREATED, DCS_COINSURANCE_PERCENT, DCS_COPAYMENT_AMOUNT, DCS_CODE, DCS_TIER_TYPE_ID, IN_NETWORK_INDICATOR, MEASURE_POINT_VALUE, "+ 
					"PHARMACY_CODE, TIME_UNIT_CODE, UPDATED, id from DCS_TYPE  where DCS_TIER_TYPE_ID=";
			
		}
		document.form1.whereText.value = displayWhere;
		document.form1.query.value = query;
		
	}

	function validateId(){
		
		var selectedQuery = document.getElementById('selectedQuery').value;
		
		if(!selectedQuery){
			alert('Please select a query');
			return false;
		}
		
		var id = document.getElementById('valueText').value;
		
		if(!id || id.replace(/^\s+|\s+$/g, "").length == 0){
			alert('Please enter an Id');
			return false;
		}
		
		/* if(!id || isNaN(parseInt(id))){
			alert('Please enter a valid Id');
			return false;
		} */
		
		document.getElementById('query').value += '\'' +id + '\'';
		document.getElementById('queryName').value = document.getElementById('selectedQuery').options[document.getElementById('selectedQuery').selectedIndex].text;
	}

	function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
            	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
            }
		}
	}
</script>
</head>
<body bgcolor="#000000">

<%
	String query = request.getParameter("query");
	if( query == null || query == ""){
		if(isPostgresDB) {
			query = "select now()";
		} else {
		query = "select sysdate from dual";
	}
	}
	else{	
		//ie8Trim();
		query = query.trim();
	}
	String queryName = request.getParameter("queryName");
	if(queryName == null || queryName == ""){
		queryName = "Sample Query";
	}
%>

<sql:query dataSource="${jspDataSource}" var="result">
	<%=query%>
</sql:query>

<p>SERFF Plan Management View Data</p>
<table border="1" cellpadding="0" cellspacing="0" width="90%" bordercolor="#999999">
  <tr>
    <td bgcolor="#E4E4E4">
    	Database Details:&nbsp;<br>
    	DRIVER : <%=prop.getProperty("driver") %><br>
    	URL : <%=prop.getProperty("url") %><br>
    	USER : <%=prop.getProperty("username") %><br>
    </td>
  </tr>
  
  <tr>
    <td valign="top" align="left">    
      <form method="GET" action="PlanMgmtViewData" name="form1" onsubmit="javascript: return validateId()">    
      Select Query: <br>
        <select size="5" id="selectedQuery" name="selectedQuery" onchange="querySelected(this)">
			<option value="pmq1">Issuer Query</option>
			<option value="pmq2">Network Query</option>
			<option value="pmq3">Service Area Query</option>
			<option value="pmq4">NCQA Query</option>
			<option value="pmq5">URAC Query</option>
			<option value="pmq6">P&amp;B Query1</option>
			<option value="pmq7">P&amp;B Query2</option>
			<option value="pmq8">P&amp;B Query3</option>
			<option value="pmq9">Prescription Drug Query1</option>
			<option value="pmq10">Prescription Drug Query2</option>
			<option value="pmq11">Prescription Drug Query3</option>
			<option value="pmq12">Prescription Drug Query4</option>
        </select> 
        Where Clause <input type="text" id="whereText" size="20" style="background-color: #E4E4E4" readonly="readonly"> 
        Value:<input type="text" id="valueText" size="20" maxlength="10">&nbsp;
      <input type="submit" value="Run SQL" name="B2">      
      <input type="hidden" id="query" name="query" >
      <input type="hidden" id="queryName" name="queryName">
       </form>  
     </td>
  </tr>
  <tr>
    <td bgcolor="#BFFFFF">Final Query:<br>
    	<textarea rows="5" id="finalQuery" name="finalQuery" cols="80" style="background-color: #BFFFFF" readonly="readonly">
    		<%=query%>
    	</textarea>
      &nbsp;
    </td>
  </tr>
</table>

<!-- Query Result -->
Result:
<display:table name="${result.rows}" style="white-space: pre-wrap;" pagesize="10" requestURI="PlanMgmtViewData">
    		
</display:table>

<br><br>Reference<br>



<table border="1" cellpadding="0" cellspacing="0" width="90%">
  <tr>
    <td width="10%" bgcolor="#E4E4E4">
    	<b>Query Name</b></td>
    	<td bgcolor="#E4E4E4"><b>Query</b></td>
  </tr>
  <tr>
    	<td width="10%"><%=queryName%></td>
    	<td><%=query%></td>
  </tr>
</table>



</body>

</html>
