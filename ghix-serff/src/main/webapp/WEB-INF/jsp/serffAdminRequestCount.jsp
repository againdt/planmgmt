<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@include file="datasource.jsp" %>
<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8">

<head>
<title>SERFF Traffic in Last 24 hours</title>
</head>
<!-- Total requests -->
<sql:query dataSource="${jspDataSource}" var="result1">
select serff_track_num as tester, count(serff_track_num) as total
from(
select lower(serff_track_num) as serff_track_num
from
(
select 
 case
  when instr(serff_track_num, '-') > 0 then substr(serff_track_num, 0, instr(serff_track_num, '-')-1)  
  else 'other'
 end serff_track_num
from serff_plan_mgmt where serff_operation='TRANSFER_PLAN'
))
group by serff_track_num
order by serff_track_num
</sql:query>
Requests Received by SERFF as of today:
<display:table name="${result1.rows}" id="table1">
	<display:column property="tester" title="Resource Name" sortable="false" />
	<display:column property="total" title="Total Requests" sortable="false"  />
</display:table>


<!-- Total requests in last 24 hours -->
<sql:query dataSource="${jspDataSource}" var="result2">
select serff_track_num as tester, count(serff_track_num) as total
from(
select lower(serff_track_num) as serff_track_num
from
(
select 
 case
  when instr(serff_track_num, '-') > 0 then substr(serff_track_num, 0, instr(serff_track_num, '-')-1)  
  else 'other'
 end serff_track_num
from serff_plan_mgmt where serff_operation='TRANSFER_PLAN'
))
group by serff_track_num
order by serff_track_num
</sql:query>
Requests Received by SERFF in last 24 hours:
<display:table name="${result2.rows}" id="table2" style="width:50%">
	<display:column property="tester" title="Resource Name" sortable="false" />
	<display:column property="total" title="Total Requests" sortable="false"  />
</display:table>


<!-- Total requests in last 24 hours -->
<%-- <br>
<sql:query dataSource="${jspDataSource}" var="result3">
select date1, count(date1) AS total
from(
select to_char(created_on,'DD-MON-YYYY DY') as date1
from serff_plan_mgmt where serff_operation='TRANSFER_PLAN'
where created_on > sysdate -10
)
group by date1
order by date1
</sql:query>
Total requests received by SERFF in last 10 days:
<display:table name="${result3.rows}" id="table3" style="width:10%">
	<display:column property="date1" title="Date" sortable="false" />
	<display:column property="total" title="Total Requests" sortable="false"  />
</display:table> --%>

