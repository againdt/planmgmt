<!DOCTYPE html>
<%@ page import="com.getinsured.hix.model.serff.SerffPlanMgmtBatch"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="session.jsp" %>

<html lang="en">
<head>

<!-- JQuery Start -->
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/multiselect/jquery.multiselect.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/multiselect/jquery.multiselect.filter.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/multiselect/style.css'/>" />

<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>

<script type="text/javascript" src="<c:url value='/resources/js//multiselect/jquery.multiselect.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js//multiselect/jquery.multiselect.filter.js'/>"></script>
<!-- JQuery End -->

<link rel="stylesheet"  type="text/css" href="../resources/css/bootstrap.css"/>
<link rel="stylesheet"  type="text/css" href="../resources/css/bootstrap.css"/>
<link rel="stylesheet"  type="text/css" href="../resources/css/ghixcustom.css"/>

<script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../resources/js/bootstrap-filestyle.js"></script>
<!-- <script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script> -->

<script type="text/javascript">

	function doSubmit(formName, url) {
	
		if (isValidateForm() && formName && url) {
			document.forms[formName].method = "post";
			document.forms[formName].action = url;
			document.forms[formName].submit();
		}
	}
	
	function isValidateForm() {
		
		var flag = true;
		var loForm = document.forms["frmUploadPlans"];
		
		if (loForm) {
			
			var laFields = new Array();
			var li = 0;
			
			var nodeValue = loForm["carrier"].value;
			if (!nodeValue || !loForm["carrierText"].value || "-1" == nodeValue) {
				laFields[li++] = "Issuer must be required.";
			}
			
			nodeValue = loForm["stateCodes"].value;
			if (!nodeValue) {
				laFields[li++] = "State Code must be required.";
			}
			
			nodeValue = loForm["exchangeType"].value;
			if (!nodeValue) {
				laFields[li++] = "Exchange Type must be required.";
			}
			
			nodeValue = loForm["tenant"].value;
			if (!nodeValue || !loForm["tenantText"].value) {
				laFields[li++] = "Tenant must be required.";
			}
			
			var pndCheckValue = loForm["pndCheckbox"].checked;
			var nodeFileNode = loForm["fileUploadPlans"];
			
			if (nodeFileNode.files.length == 0) {
				laFields[li++] = "Please attach required Templates XML files.";
			}
			
			if (pndCheckValue) {
			
				if (nodeFileNode.files.length < 2 || nodeFileNode.files.length > 2) {
					laFields[li++] = "Please attach only 2 mandatory Template XMLs.";
				}else{
					var fileName = null;
					var fileList = "";
					
					for(var fi = 0; fi < nodeFileNode.files.length; fi++) {
						if(nodeFileNode.files[fi].type != "text/xml") {
							laFields[li++] = "Attachment file must have XML format.";
							break;
						}
						
						fileName = nodeFileNode.files[fi].name.toLowerCase();
						if(fileName.lastIndexOf("${DRUG_SUFFIX}") > -1) {
							continue;
						}
						else if(fileName.lastIndexOf("${BENEFITS_SUFFIX}") > -1) {
							continue;
						}
						else {
							fileList += fileName + ", ";
						}
						if(fileList) {
							laFields[li++] = "Attached files name ("+ fileList.substring(0, fileList.length - 2) +")  should be match with templates name.";
						}
					}
				}
			}
			else {
				
				if (nodeFileNode.files.length < 4 || nodeFileNode.files.length > 9) {
					laFields[li++] = "Please attach 4 mandatory and maximum 5 optional Template XMLs.";
				}
				else {
					
					var fileName = null;
					var fileList = "";
					var errorMsg = "Required template is missing. ";
					var cntOptional = 0;
					
					for(var fi = 0; fi < nodeFileNode.files.length; fi++) {
						
						if(nodeFileNode.files[fi].type != "text/xml") {
							laFields[li++] = "Attachment file must have XML format.";
							break;
						}
						
						fileName = nodeFileNode.files[fi].name.toLowerCase();
						
						if(fileName.lastIndexOf("${NETWORK_SUFFIX}") > -1) {
							continue;
						}
						else if(fileName.lastIndexOf("${BENEFITS_SUFFIX}") > -1) {
							continue;
						}
						else if(fileName.lastIndexOf("${RATE_SUFFIX}") > -1) {
							continue;
						}
						else if(fileName.lastIndexOf("${SERVICE_AREA_SUFFIX}") > -1) {
							continue;
						}
						else if(fileName.lastIndexOf("${DRUG_SUFFIX}") > -1) {
							
							cntOptional++;
							continue;
						}
						else if(fileName.lastIndexOf("${BUSINESS_RULE_SUFFIX}") > -1) {
							
							cntOptional++;
							continue;
						}
						else if(fileName.lastIndexOf("${UNIFIED_RATE_FILENAME_SUFFIX}") > -1) {
							
							cntOptional++;
							continue;
						}
						else if(fileName.lastIndexOf("${URACSUFFIX}") > -1) {
							
							cntOptional++;
						    continue;
						}
						else if(fileName.lastIndexOf("${NCQASUFFIX}") > -1) {
							
							cntOptional++;
						    continue;
						}
						else {
							fileList += fileName + ", ";
						}
					}
					
					if ((nodeFileNode.files.length - cntOptional) < 4) {
						laFields[li++] = errorMsg;
					}
					
					if(fileList) {
						laFields[li++] = "Attached files name ("+ fileList.substring(0, fileList.length - 2) +")  should be match with templates name.";
					}
				}
			}
			
			if (laFields.length > 0) {
			    
				if(laFields.length > 1) {
					
					for (var fi = 0; fi < laFields.length; fi++) {
						laFields[fi] = "- " + laFields[fi];
					}
				}
		       	alert(laFields.join('\n'));
		       	flag = false;
		    }
		}
		return flag;
	}
	
	function getValidIssuers(){	
		var result = "<option value='-1' >Select Issuer</option>";
		$('#carrier').attr("disabled",false).html(result);
		$('#pndCheckbox').attr("checked", false);
		$('#pndCheckbox').prop("disabled",true);
		$.ajax({
			type :"POST",
			url: "<c:url value='/admin/getValidIssuers'/>",
			data:{"stateCodeSelected": $("#stateCodes").val() ,"csrftoken": $("#csrftoken").val()},
			success: function(options){	
				$("#stateCodes").multiselect();
         		issuerData(options);
	       	} 	
		});
	}
	
	function issuerData(options){
		var result = "<option value='-1' >Select Issuer</option>";

		if(options){
			$.each(options, function(key, value) {
            	result += "<option value='"+ key +"' >"+"[" +key +"] "+value+"</option>" ;
             });
	  		$('#carrier').attr("disabled",false).html(result);
		}
  		$("#carrier").multiselect({
  			enable : true,
 			multiple: false,
 			selectedList: 1
 		}).multiselectfilter();
	}
	
	$(document).ready(function() {
		// Set Defaults values
		$("#tenantText").val($("#tenant").val());

		$(":file").filestyle({'buttonText': 'Browse ...'});

		$("#stateCodes").multiselect({
			enable : true,
			multiple: false,
			selectedList: 1
		}).multiselectfilter();

		$("#tenant").multiselect({
			selectedList: 10,

			click: function(e) {

				if ($(this).multiselect("widget").find("input:checked").length > 10) {
					return false;
				}
			}
		}).multiselectfilter();
	});

	function checkPlans() {
		$.ajax({
			type :"POST",
			url: "<c:url value='/admin/getPlansForIssuer'/>",
			data:{"selectedIssuer": $("#carrier").val() ,"csrftoken": $("#csrftoken").val()},

			success: function(values) {
         		pndData(values);
	       	}
		});
	}
	
	function pndData(values) {
		$('#pndCheckbox').attr("checked", false);

		if (values) {
			$('#pndCheckbox').prop("disabled",false);
		}
		else {
			$('#pndCheckbox').prop("disabled",true);
			 
		}
	}
</script>

<style>
	ul.unstyled li{
		padding:10px;
	}
</style>
</head>

<body>
<div class="row-fluid" id="main">
	<div class="container">
		<div class="row-fluid">
			<div class="gutter10">
				<div class="row-fluid">
					<h1 id="">Upload QHP or SADP Plan Files</h1>
				</div><!-- end of .row-fluid -->
				
				<div class="row-fluid">
					<div id="sidebar" class="span3">
						<div class="header"><span>About Plan Files</span></div>
						<ul class="unstyled">
							<li><b>Step 1: </b>Please select the State you are attempting to load plan data for.</li>
							<li><b>Step 2: </b>Please select which issuer you are attempting to load plan data for.</li>
							<li><b>Step 3: </b>Please select the Exchange Type. </li>
							<li><b>Step 4: </b>Please select Tenant.</li>
							</br>
							<li><b>Step 5: </b>Please upload the required XML files.</li>
							<li><b>Step 6:</b> Load Prescription Template Only .</li>
							<li class="gutter10"><b>Click Save: </b>Proceed to clicking Save after all required XML files have been successfully uploaded.</li>
						</ul>	
					</div><!-- end of .span3 -->
					
					<div class="span9" id="rightpanel">
						<div class="header" style="height:40px"></div>
						<form:form id="frmUploadPlans" name="frmUploadPlans" action="${pageContext.request.contextPath}/admin/uploadPlans" modelAttribute="uploadPlans" enctype="multipart/form-data" method="POST">
							<df:csrfToken/>
							<div class="control-group">
							<c:if test="${ftpStatus != null}">
				            		<div style="color: red; font-weight: bold;">${ftpStatus}<br /><br /></div>
				            	</c:if>
				            	<c:if test="${ftpSucess != null}">
				            		<div style="color: green; font-weight: bold;">${ftpSucess}<br /><br /></div>
				            	</c:if>
				            	
				                <label for="stateCodes" class="control-label"><b>Step 1:</b> State*&nbsp;&nbsp;</label>
								<select id="stateCodes" name="stateCodes" onchange="getValidIssuers();">
									<option value="" selected="selected">Select state</option>
							 		<c:if test="${statesList != null}">
										<c:forEach var="stateName" items="${statesList}">
											<option value="<c:out value="${stateName.code}" />">
												<c:out value="${stateName.name}" />
											</option>
										</c:forEach>
									</c:if>
								</select>
								<input type="hidden" id="stateCodeText" name="stateCodeText" />
							</div>
							
							<div class="control-group">
				                <label for="carrier" class="control-label"><b>Step 2:</b> Issuer*&nbsp;&nbsp;</label>
								<select id="carrier" name="carrier" onchange="$('#carrierText').val($('#carrier option:selected').text()); checkPlans();" disabled="disabled">
								</select>
								<input type="hidden" id="carrierText" name="carrierText" />
							</div>
							
							<div class="control-group">
				                <label for="exchangeType" class="control-label"><b>Step 3:</b> Exchange Type*&nbsp;&nbsp;</label>
								<select id="exchangeType" name="exchangeType" onchange="$('exchangeTypeText').val($('exchangeType').val());">
									<option value="" selected="selected">Select Exchange Type</option>
									<c:if test="${exchangeTypeList != null}">
									<c:set var="PUF" value="<%=SerffPlanMgmtBatch.EXCHANGE_TYPE.PUF%>"></c:set>
									<c:forEach items="${exchangeTypeList}" var="exchangeType">
										<c:if test="${exchangeType != PUF}">
    									<option value="<c:out value="${exchangeType}" />">
											<c:out value="${exchangeType}" />
										</option>
										</c:if>
  									</c:forEach>
                                    </c:if>
								</select>
								<input type="hidden" id="exchangeTypeText" name="exchangeTypeText" />
							</div>
							
							<div class="control-group">
				                <label for="tenant" class="control-label"><b>Step 4:</b> Tenant*&nbsp;&nbsp;</label>
								<select id="tenant" name="tenant" multiple="multiple" onchange="$('#tenantText').val($('#tenant').val());">
									<%-- <option value="" selected="selected">Select Tenant</option> --%>
									<c:if test="${tenantList != null}">
									<c:forEach items="${tenantList}" var="tenant">
									    <c:if test="${tenant.code == defaultTenantCode}">
    										<option selected="selected" value="<c:out value="${tenant.id}" />">
												<c:out value="${tenant.name}" />
											</option>
										</c:if>
										<c:if test="${tenant.code != defaultTenantCode}">
    										<option value="<c:out value="${tenant.id}" />">
												<c:out value="${tenant.name}" />
											</option>
										</c:if>
  									</c:forEach>
                                    </c:if>
								</select>
								<input type="hidden" id="tenantText" name="tenantText" />
							</div>
							
							<div class="control-group">
								<label class="control-label" for="fileUploadPlans"><b>Step 5:</b> Templates XML* </label>
								<div class="controls">
									<input type="file" class="filestyle" multiple="multiple" id="fileUploadPlans" name="fileUploadPlans" data-classButton="btn">
								</div>
							</div>
							
							<div class="control-group">
								<div id="pnD">
									<label class="pull-left" for="pndCheckbox"><b>Step 6:</b> Load Prescription Template Only </label>
									&nbsp; &nbsp;
									<input type="checkbox" class="filestyle" id="pndCheckbox" name="pndCheckbox" class="pull-left" disabled="disabled" />
								</div>
							</div>
							
				            <div class="row-fluid gutter10-t">
								<div class="span2">
									<a href="javascript:document.forms['frmUploadPlans'].reset()" class="btn pull-left" id="cancel">Cancel</a>
								</div>
								<div class="span2">
									<a href="javascript:doSubmit('frmUploadPlans', '${pageContext.request.contextPath}/admin/uploadPlans');" class="btn btn-primary pull-left" type="submit" id="save">Save</a>
								</div>
							</div>
							
							<div class="row-fluid gutter10-t">
								<label><b>Note:</b></label>
								<label>- Plan and Benefit, Rates, Network & Service Area XMLs are mandatory.</label>
								<label>- Suffix for mandatory templates: plans.xml, rate.xml, network.xml & servicearea.xml</label>
								<label>- Prescription Drug, Business Rule, Unified Rate, URAC & NCQA XMLs are optional.</label>
								<label>- Suffix for optional templates: drug.xml, businessrule.xml, urrt.xml, urac.xml & ncqa.xml</label>
							</div>
						</form:form>
					</div><!-- end of .span9 -->
				</div><!-- end of .row-fluid -->
			</div> <!-- end of .gutter10 -->
		<!--row-fluid--> 
		</div>
	<!--container--> 
	</div>
<!--main-->
</div>
</body>
</html>