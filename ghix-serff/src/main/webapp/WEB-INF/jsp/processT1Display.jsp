<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="datasource.jsp"%>

<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="../resources/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="../resources/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="../resources/css/ghixcustom.css"/>
<link rel="stylesheet" type="text/css" href="../resources/css/displaytag_serff.css" charset="utf-8" />

<script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../resources/js/bootstrap-filestyle.js"></script>
<script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script>

<script type="text/javascript">

	function doSubmit(formName, url) {
		document.forms[formName].method = "POST";
		document.forms[formName].action = url;
		document.forms[formName].submit();
	}
	
	function isValidateIssuerOrPlanId () {
		
		var reprocessID = document.getElementById("reprocessID").value;
		var errorMsg = "";
		
		if (reprocessID) {
			
			if (reprocessID.length < 5) {
				errorMsg = "Invalid length of Issuer Plan Number Or HIOS Issuer Id !!";
			}
			else if (reprocessID.length == 5 && isNaN(reprocessID)) {
				errorMsg = "Invalid HIOS Issuer Id !!";
			}
			else if (reprocessID.length == 14) {
				
				var patterPlanId = new RegExp("(^[0-9]{5})([A-Za-z]{2})([0-9]{7})$");
				
				if (!patterPlanId.test(reprocessID)) {
					errorMsg = "Invalid Issuer Plan Number !!";
				}
			}
			else if (reprocessID.length > 5) {
				errorMsg = "Invalid length of Issuer Plan Number Or HIOS Issuer Id !!";
			}
		}
		else {
			errorMsg = "Issuer Plan Number Or HIOS Issuer Id is required !!";
		}
		
		if (errorMsg) {
			alert(errorMsg);
		}
		else {
			doSubmit('frmUploadPlans', '${pageContext.request.contextPath}/admin/reprocessDisplayDataT1');
		}
	}
	
	
	function isValidateIssuerOrPlanIdPage(){
		
		var reprocessID = document.getElementById("reprocessID").value;
		var errorMsg = "";
		
		if (reprocessID) {
			
			if (reprocessID.length < 5) {
				errorMsg = "Invalid length of Issuer Plan Number Or HIOS Issuer Id !!";
			}
			else if (reprocessID.length == 5 && isNaN(reprocessID)) {
				errorMsg = "Invalid HIOS Issuer Id !!";
			}
			else if (reprocessID.length == 14) {
				
				var patterPlanId = new RegExp("(^[0-9]{5})([A-Za-z]{2})([0-9]{7})$");
				
				if (!patterPlanId.test(reprocessID)) {
					errorMsg = "Invalid Issuer Plan Number !!";
				}
			}
			else if (reprocessID.length > 5) {
				errorMsg = "Invalid length of Issuer Plan Number Or HIOS Issuer Id !!";
			}
		}
		else {
			errorMsg = "Issuer Plan Number Or HIOS Issuer Id is required !!";
		}
		
		if (errorMsg) {
			alert(errorMsg);
		}
	else{
		doSubmit('frmUploadPlans', '${pageContext.request.contextPath}/admin/showDisplayDataT1');
	}
		
	}
</script>

</head>

<body>
<%
		String whereClause = "id is not null";
		
	if (request.getParameter("reprocessID") != null)
		{
			String processId = request.getParameter("reprocessID");
			whereClause = "hios_issuer_id ='"+processId+"'";

		
	%>
<sql:query dataSource="${jspDataSource}" var="result1">
		select  i.hios_issuer_id ,i.short_name , p.issuer_plan_number,phb.network_t1_display from plan p ,plan_health ph ,plan_health_benefit phb , issuers i where i.id = p.issuer_id and p.id =ph.plan_id and 
ph.id =phb.plan_health_id and phb.name='PRIMARY_VISIT' AND p.is_deleted ='N' and p.issuer_id=(select id from issuers where <%=whereClause%>)
</sql:query>

<%

		}
%>
<div class="row-fluid" id="main">

	<div class="container">
		<div class="row-fluid">
			<div class="gutter10">

				<div class="row-fluid">
					<h1 id="">Process Plan T1 Display data</h1>
				</div><!-- end of .row-fluid -->
	
				<div class="row-fluid">
					
					
					
					<div class="span9" id="rightpanel">
						<form:form id="frmUploadPlans" name="frmUploadPlans" action="javascript: void(0);" modelAttribute="uploadPlans" enctype="multipart/form-data" method="POST">
							<df:csrfToken/>
							
				            <div class="gutter10">
					            
				            </div>
				            <div class="header" style="height:5px"></div>
				            
				            <div class="control-group">
				                <label for="reprocessID" class="control-label"><b>Re-Process Plans by entering Issuer Plan Number Or HIOS Issuer Id:</b></label>
								<input type="text" name="reprocessID" id="reprocessID" />
							</div>
				            
				            <div class="control-group">
				                <label for="applicableYear" class="control-label"><b>Applicable Year:</b></label>
								<select id="applicableYear" name="applicableYear">
									<option value="0" selected="selected">Select Applicable Year</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>
								</select>
							</div>
							
							<div class="gutter10">
					            <a href="javascript: isValidateIssuerOrPlanIdPage();" class="btn btn-primary offset1" type="submit" id="reProcess">Display Benefits</a>
								<a href="javascript: isValidateIssuerOrPlanId();" class="btn btn-primary offset1" type="submit" id="reProcess">Process Benefits</a>
							</div>
							
							<div class="gutter10">
								<display:table name="${result1.rows}"  id="table1"  style="white-space: pre-wrap;"  defaultorder="descending" >
									<display:column property="hios_issuer_id" title="Issuer Id "  />
									<display:column property="short_name" title="Issuer Name "  />
									<display:column property="issuer_plan_number" title="Plan Number "  />
									<display:column property="network_t1_display" title="T1 display data "  />
					            </display:table>
				            </div>
				          <br>
				        	 Note: http://jira.getinsured.com/browse/HIX-38225<br><br>Query to check records in DB :<br>
				        	 select i.hios_issuer_id ,i.short_name , p.issuer_plan_number,phb.network_t1_display from plan p ,plan_health ph ,plan_health_benefit phb , 
				        	 issuers i where i.id = p.issuer_id and p.id =ph.plan_id and ph.id =phb.plan_health_id and phb.name='PRIMARY_VISIT' AND p.is_deleted ='N' and p.issuer_id=(select id from issuers where hios_issuer_id ='?');
				          <br>
				            
				            <div class="header" style="height:5px"></div>
						</form:form>
					</div><!-- end of .span9 -->
				</div><!-- end of .row-fluid -->
			</div> <!-- end of .gutter10 -->
		<!--row-fluid--> 
		</div>
	<!--container--> 
	</div>
<!--main-->
</div>
</body>
</html>