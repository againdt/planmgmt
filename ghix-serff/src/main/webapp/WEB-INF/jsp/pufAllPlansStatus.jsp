<html>
<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>PUF Issuers</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<body>
	<form method="GET" id="frmpufPlans" name="frmpufPlans" action="pufAllPlansStatus">
			<sql:query dataSource="${jspDataSource}" var="result1">
				select STATE_CODE,TEMPLATE_PATH,
				CASE 
				WHEN TEMPLATE_PATH  is null THEN 
				'<a href="${pageContext.request.contextPath}/admin/getfilesTogenerate?state_code='||STATE_CODE||'" >Generate All</a>&nbsp;|&nbsp;<a href="${pageContext.request.contextPath}/admin/getfilesTogenerate?hios_id='||hios_issuer_id||'&state_code='||STATE_CODE||'" >Generate</a>'
				ELSE
				'<a href="${pageContext.request.contextPath}/admin/getfilesTogenerate?state_code='||STATE_CODE||'" >Generate All</a>&nbsp;|&nbsp;<a href="${pageContext.request.contextPath}/admin/getfilesTogenerate?hios_id='||hios_issuer_id||'&state_code='||STATE_CODE||'">Re-generate</a>'
				END as generate,
				hios_issuer_id,issuer_name ,
				
				CASE
				WHEN TEMPLATE_PATH  is not null THEN 
				'<a href="${pageContext.request.contextPath}/admin/loadToQA?hios_id='||hios_issuer_id||'&state_code='||STATE_CODE||'&src_path='||TEMPLATE_PATH||'">load to QA</a>'
				ELSE
				''
				END as loadtoQA,
				CASE
				WHEN TEMPLATE_PATH  is not null THEN 
				'<a href="${pageContext.request.contextPath}/admin/loadtoStage?hios_id='||hios_issuer_id||'&state_code='||STATE_CODE||'&src_path='||TEMPLATE_PATH||'">load to Stage</a>'
				ELSE
				''
				END as loadtoStage,
				QA_POST_STATUS,STAGE_POST_STATUS from puf_plan_status where id is not null 
			</sql:query> 
			<br>
			<b>All PUF Plans Status</b>
			<display:table name="${result1.rows}"  requestURI="pufAllPlansStatus" id="table1" export="true"  style="width:100%" pagesize="100"   >
			<display:column property="STATE_CODE" title="State" sortable="true"/>
			<display:column property="generate" title="Generate" />
			<display:column property="hios_issuer_id" title="Hios Id" sortable="true"/>
			<display:column property="issuer_name" title="Issuer Name" sortable="true"/>
			<display:column property="loadtoQA" title="Load to QA "  />
			<display:column property="loadtoStage" title="Load to Stage"/>
			<display:column property="QA_POST_STATUS" title="QA status" sortable="true"/>
			<display:column property="STAGE_POST_STATUS" title="Stage status" sortable="true"/>
			<display:setProperty name="export.xml.filename" value="pufAllPlans.xml"/>
			<display:setProperty name="export.csv.filename" value="pufAllPlans.csv"/>
			<display:setProperty name="export.excel.filename" value="pufAllPlans.xls"/>
			
			</display:table>

		</form>
 </body> 
 </html>
