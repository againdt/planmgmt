<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<title>GHIX SERFF Admin Console</title>
</head>
<body>
	<h2>GHIX SERFF Admin Console</h2>

	<br>
	<a href="<c:url value='/admin/getMain' />">SERFF Admin</a>
	<br>
	<a href="<c:url value='/admin/loggeradmin' />">Log4j Admin</a>
	<br>
	<a href="<c:url value='/admin/sysinfo' />">System Info</a>
	<br>
        <a href="<c:url value='/admin/serffInfo' />">Serff Info</a>
	<br>
	<br>
	<a href="<c:url value='/admin/getReqCount' />">Serff Traffic</a>
	<br>
	<br> Note: This is only for internal purpose.
</body>
</html>
