<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="com.serff.util.unixshell.SSHVo" %>
<%@ include file="session.jsp" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
	<meta name="ProgId" content="FrontPage.Editor.Document">
	<title>SERFF Unix Console</title>
	
	<script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript">
	
		$(document).ready(function() {
			var logFileValue = "${selectLogFile}";
			
			if (!logFileValue) {
				logFileValue = "select";
			}
			$("#selectLogFile").val(logFileValue);
		});
		
		function doSubmit(formName, url) {
		
			if (formName && url) {
				document.forms[formName].method = "post";
				document.forms[formName].action = url;
				document.forms[formName].submit();
			}
		}
		
		function executeCommand(cmdName) {
			
			$("#cmdResponse").val("Processing...");
			$("#cmd").val("");
			
			if (cmdName) {
				
				var postURL = "${pageContext.request.contextPath}/admin/executeCommand";
				$("#commandName").val(cmdName);
				
				$.post(postURL,
					{ commandName:cmdName, selectLogFile:$("#selectLogFile").val(), grepCmd:$("#grepCmd").val() },
					function(data) {
					
					if(data != null) {
						
						$.each(data, function(index, value) {
							
							switch (index) {
								case "cmdResponse" : $("#cmdResponse").val(value); break;
								case "cmd": $("#cmd").val(value); break;
							}
						});
					}
					else {
						$("#cmdResponse").val("");
					}
				});
			}
		}
		
		function downloadFile() {
			$("#cmdResponse").val("");
			$("#commandName").val("sftp");
			doSubmit("frmUnixShell", "${pageContext.request.contextPath}/admin/downloadFile");
		}
	</script>
	
	<style type="text/css">
		.button1 {
			-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
			-webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
			box-shadow:inset 0px 1px 0px 0px #bbdaf7;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #79bbff), color-stop(1, #378de5) );
			background:-moz-linear-gradient( center top, #79bbff 5%, #378de5 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#79bbff', endColorstr='#378de5');
			background-color:#79bbff;
			-webkit-border-top-left-radius:20px;
			-moz-border-radius-topleft:20px;
			border-top-left-radius:20px;
			-webkit-border-top-right-radius:20px;
			-moz-border-radius-topright:20px;
			border-top-right-radius:20px;
			-webkit-border-bottom-right-radius:20px;
			-moz-border-radius-bottomright:20px;
			border-bottom-right-radius:20px;
			-webkit-border-bottom-left-radius:20px;
			-moz-border-radius-bottomleft:20px;
			border-bottom-left-radius:20px;
			text-indent:0;
			border:1px solid #84bbf3;
			display:inline-block;
			color:#ffffff;
			font-family:Courier New;
			font-size:16px;
			font-weight:bold;
			font-style:normal;
			line-height:34px;
			text-decoration:none;
			text-align:center;
			text-shadow:1px 1px 0px #528ecc;
			height:30px;
			width:250px;
		}
		.button1:hover {
			background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #378de5), color-stop(1, #79bbff) );
			background:-moz-linear-gradient( center top, #378de5 5%, #79bbff 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff');
			background-color:#378de5;
		}.button1:active {
			position:relative;
			top:1px;
		}
		
		.button2 {
			-moz-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
			-webkit-box-shadow:inset 0px 1px 0px 0px #bbdaf7;
			box-shadow:inset 0px 1px 0px 0px #bbdaf7;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #79bbff), color-stop(1, #378de5) );
			background:-moz-linear-gradient( center top, #79bbff 5%, #378de5 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#79bbff', endColorstr='#378de5');
			background-color:#79bbff;
			-webkit-border-top-left-radius:20px;
			-moz-border-radius-topleft:20px;
			border-top-left-radius:20px;
			-webkit-border-top-right-radius:20px;
			-moz-border-radius-topright:20px;
			border-top-right-radius:20px;
			-webkit-border-bottom-right-radius:20px;
			-moz-border-radius-bottomright:20px;
			border-bottom-right-radius:20px;
			-webkit-border-bottom-left-radius:20px;
			-moz-border-radius-bottomleft:20px;
			border-bottom-left-radius:20px;
			text-indent:0;
			border:1px solid #84bbf3;
			display:inline-block;
			color:#ffffff;
			font-family:Courier New;
			font-size:16px;
			font-weight:bold;
			font-style:normal;
			line-height:34px;
			text-decoration:none;
			text-align:center;
			text-shadow:1px 1px 0px #528ecc;
			height:30px;
		}
		.button2:hover {
			background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #378de5), color-stop(1, #79bbff) );
			background:-moz-linear-gradient( center top, #378de5 5%, #79bbff 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#378de5', endColorstr='#79bbff');
			background-color:#378de5;
		}
		.button2:active {
			position:relative;
			top:1px;
		}
	</style>
</head>

<body>
	<form id="frmUnixShell" name="frmUnixShell" method="POST" action="--WEBBOT-SELF--">
		<div align="left">
			<input type="hidden" id="commandName" name="commandName" />
			
			<table border="0" width="80%" align="left" cellpadding="3" cellspacing="0">
				<tr>
					<td width="0%" valign="top" align="left" bgcolor="#C0C0C0" colspan="2">
						<table border="0" width="100%" bgcolor="#3399FF" cellpadding="0">
							<tr>
								<td width="33%">
									<p align="left"><font face="Arial" size="3">Host Name/IP: ${ServerName}/${ServerIP}</font></td>
								<td width="33%">
									<p align="center"><font face="Arial" size="3"><b>Unix Console</b></font></td>
								<td width="34%">
									<p align="right"><font face="Arial" size="3">${systemDate}</font>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td rowspan="4" width="0%" valign="top" align="left" bgcolor="#C0C0C0">
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.TOTAL_WAIT_THREADS.name() %>');" value="Total Wait Threads" name="B9" class="button1"> 
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.ESTABLISHED_CONNECTIONS.name() %>');" value="Established Connections" name="B10" class="button1">
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.ORACLE_CONNECTIONS.name() %>');" value="Oracle Connections" name="B11" class="button1"> 
						<p></p>
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.PING_ORACLE.name() %>');" value="Ping Oracle" name="B13" class="button1">
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.PING_ECM.name() %>');" value="Ping ECM" name="B14" class="button1">
						<p></p>
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.CPU.name() %>');" value="CPU" name="B7" class="button1"> 
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.CPU_DETAILS.name() %>');" value="CPU Details" name="B7" class="button1"> 
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.OS.name() %>');" value="OS" name="B7" class="button1"> 
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.DATE.name() %>');" value="Date" name="B7" class="button1"> 
						<p></p>
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.MEMORY.name() %>');" value="Memory" name="B8" class="button1">
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.MEMORY_DETAILS.name() %>');" value="Memory Details" name="B8" class="button1">
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.MEMORY_PROFILE.name() %>');" value="Memory Profile" name="B8" class="button1">
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.FREE_DISK_SPACE.name() %>');" value="Free Disk Space" name="B8" class="button1">
						<p></p>
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.JAVA_PROCESSES.name() %>');" value="Java Processes" name="B8" class="button1">
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.DEAMON_PROCESSES.name() %>');" value="Daemon Processes" name="B8" class="button1">
						<p></p>
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.SERFF_WAR.name() %>');" value="SERFF War Info" name="B8" class="button1">
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.PM_WAR.name() %>');" value="PM War Info" name="B8" class="button1">
					</td>
				    <td valign="top" align="left" width="100%" bgcolor="#C0C0C0">
						<select size="1" id="selectLogFile" name="selectLogFile">
							<option selected value="select">Select Log File</option>
							<option value="ghix-serff.log" selected="selected">SERFF Log File</option>
							<option value="ghix-planmgmt.log">Plan Management Log</option>
							<option value="ghix.log">Ghix Log</option>			
						</select> 
						<input type="button" onclick="$('#cmdResponse').val(''); if ($('#selectLogFile').val() != 'select') downloadFile();" value="Download" name="B3" class="button2"> 
						<input type="button" onclick="$('#cmdResponse').val(''); if ($('#selectLogFile').val() != 'select') executeCommand('<%=SSHVo.COMMAND_NAME.TAIL.name() %>');" value="Tail 1000" name="B4" class="button2">
						<input type="button" onclick="$('#cmdResponse').val(''); if ($('#selectLogFile').val() != 'select') executeCommand('<%=SSHVo.COMMAND_NAME.HEAD.name() %>');" value="Head 1000" name="B5" class="button2"> 
						<input type="button" onclick="$('#cmdResponse').val(''); if ($('#selectLogFile').val() != 'select') executeCommand('<%=SSHVo.COMMAND_NAME.FILE_INFO.name() %>');" value="File Info" name="B5" class="button2"> 
						
						<input type="text" id="grepCmd" name="grepCmd" size="20" />
						<input type="button" onclick="executeCommand('<%=SSHVo.COMMAND_NAME.GREP.name() %>');" value="Grep" name="B6" class="button2">
				    </td>
				</tr>
				
				<tr>
					<td valign="top" align="left" width="100%" bgcolor="#C0C0C0"><font face="Arial" size="2">Command Issued: <input type="text" id="cmd" name="cmd" readonly="readonly" size="100" value="${cmd}" style="background-color: #C0C0C0"></font></td>
				</tr>
				
				<tr>
					<td valign="top" align="left" width="100%" bgcolor="#000000">
						<font face="Arial" size="2">
							<textarea id="cmdResponse" name="cmdResponse" readonly="readonly" rows="30" name="S1" cols="90" style="background-color: #000000; color: #FFFFFF; font-size: 12pt; font-family: Courier New">
								<c:out value="${cmdResponse}" />
							</textarea>
						</font>
					</td>
				</tr>
				
				<tr>
					<td width="0%" valign="top" align="left" bgcolor="#C0C0C0"></td>
				</tr>
				
				<tr>
					<td width="0%" valign="top" align="left" colspan="2">
						Unauthorized use prohibited.${consoleStatus}
					</td>
				</tr>
			</table>
		</div>
	</form>
<br>
</body>
</html>
