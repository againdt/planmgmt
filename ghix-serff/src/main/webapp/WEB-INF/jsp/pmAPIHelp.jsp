<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<link type="text/css" rel="stylesheet" href="./css/json_tool.css">
<script type="text/javascript" charset="utf-8" src="./js/json_tool.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script> 
$(document).ready(function(){
  $("#flip").click(function(){
    $("#panel").slideToggle("slow");
  });
});
</script>
 
<style> 
#panel,#flip
{
padding:5px;
text-align:left;
background-color:#e5eecc;
border:solid 1px #c3c3c3;
}
#panel
{
padding:5px;
display:none;
}
</style>


<title>API Name</title>
</head>

<body>
<p><b><font face="Arial" size="2">SQL Statements for APIs</font></b></p>
<p><font face="Arial" size="2"><b>API:</b> crosssell/getdentalplans<br>
<b>Description:</b>&nbsp;This API Returns Dental plans data on the basis of JSON input provided. As this API works for Dental plans only, Plan Type is default to Dental.<br>
<b>SQLs:</b>&nbsp;SELECT p.id AS plan_id, p.name AS plan_name, sum(pre) AS total_premium, i.company_logo as issuer_logo, pdental_id  FROM plan p, issuers i, ( ( SELECT prate.rate AS pre, pdental.plan_id, pdental.id AS pdental_id, 1 as memberctr FROM  plan_dental pdental, pm_service_area psarea, pm_rating_area prarea, pm_plan_rate prate, plan p2, pm_zip_county_rating_area pzcrarea WHERE p2.id = pdental.plan_id AND p2.service_area_id = psarea.service_area_id AND psarea.zip = '83452' AND p2.id = prate.plan_id AND p2.is_deleted = 'N' AND prate.is_deleted = 'N' AND psarea.is_deleted = 'N' AND prarea.id = prate.rating_area_id AND pzcrarea.rating_area_id = prate.rating_area_id AND (pzcrarea.zip = 83452 OR pzcrarea.zip IS NULL) AND ( (25 >= prate.min_age AND 25 <= prate.max_age)  OR (1002 >= prate.min_age AND 1002 <= prate.max_age) )  AND (prate.tobacco = UPPER('N') OR prate.tobacco is NULL)  AND prate.rate > 0 AND TO_DATE ('2014-8-11', 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date AND psarea.fips = '16081' AND pzcrarea.county_fips = '16081'  )  UNION ALL ( SELECT prate.rate AS pre, pdental.plan_id, pdental.id AS pdental_id, 1 as memberctr FROM  plan_dental pdental, pm_service_area psarea, pm_rating_area prarea, pm_plan_rate prate, plan p2, pm_zip_county_rating_area pzcrarea WHERE p2.id = pdental.plan_id AND p2.service_area_id = psarea.service_area_id AND psarea.zip = '83452' AND p2.id = prate.plan_id AND p2.is_deleted = 'N' AND prate.is_deleted = 'N' AND psarea.is_deleted = 'N' AND prarea.id = prate.rating_area_id AND pzcrarea.rating_area_id = prate.rating_area_id AND (pzcrarea.zip = 83452 OR pzcrarea.zip IS NULL) AND ( (19 >= prate.min_age AND 19 <= prate.max_age)  OR (1002 >= prate.min_age AND 1002 <= prate.max_age) )  AND (prate.tobacco = UPPER('N') OR prate.tobacco is NULL)  AND prate.rate > 0 AND TO_DATE ('2014-8-11', 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date AND psarea.fips = '16081' AND pzcrarea.county_fips = '16081'  ) ) temp  WHERE  p.id = temp.plan_id  AND p.issuer_id = i.id  AND TO_DATE ('2014-8-11', 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date  AND p.status IN ('CERTIFIED', 'RECERTIFIED')  AND p.issuer_verification_status='VERIFIED'  AND ( ( p.enrollment_avail='AVAILABLE' AND TO_DATE ('2014-8-11', 'YYYY-MM-DD') >= p.enrollment_avail_effdate  AND ( TO_DATE ('2014-8-11', 'YYYY-MM-DD') < p.future_erl_avail_effdate OR  p.future_erl_avail_effdate is null) )  OR  ( p.future_enrollment_avail = 'AVAILABLE' AND TO_DATE ('2014-8-11', 'YYYY-MM-DD') >= p.future_erl_avail_effdate) )  AND p.market = 'INDIVIDUAL'  AND p.exchange_type = 'ON'  AND p.available_for IN ('ADULTONLY', 'ADULTANDCHILD')  GROUP BY p.id, p.name, i.company_logo, pdental_id HAVING sum(memberctr) = 2<br>
<br>
<b>API:</b>	crosssell/getlowestdentalpremium<br>
<b>Description:</b>&nbsp;<br>
<b>SQLs:</b>&nbsp;<br>
<br>
<b>API:</b>	issuerInfo/byId<br>
<b>Description:</b>&nbsp;This API returns all the Issuer information details on the basis of provided Issuer Id.<br>
<b>SQLs:</b>&nbsp;SELECT * FROM issuers WHERE id=?<br>
<br>
<b>API:</b>	issuerInfo/byzipcode<br>
<b>Description:</b>&nbsp;This API returns all the Issuer information details on the basis of provided zipcode.<br>
<b>SQLs:</b>&nbsp;SELECT DISTINCT i.name FROM pm_service_area ps, pm_issuer_service_area isa, issuers i WHERE i.hios_issuer_id = isa.hios_issuer_id AND isa.id = ps.service_area_id AND ps.is_deleted='N' AND ps.zip = (?) AND ps.fips = (?) <br>
<br>
<b>API:</b>	issuerInfo/disclaimerInfo<br>
<b>Description:</b>&nbsp;This API returns Issuer disclaimer information on the basis of exchange type i.e. 'ON' or 'OFF' provided.<br>
<b>SQLs:</b>&nbsp;select issuer0_.ON_EXCHANGE_DISCLAIMER as col_0_0_, issuer0_.OFF_EXCHANGE_DISCLAIMER as col_1_0_ from issuers issuer0_ where issuer0_.id=?<br>
<br>
<b>API:</b>	issuerInfo/getIssuerD2CInfo<br>
<b>Description:</b>&nbsp;This API returns All Issuers information that is present in 'ISSUER_D2C' table. <br>
<b>SQLs:</b>&nbsp;SELECT HIOS_ISSUER_ID, APPLICABLE_YEAR FROM ISSUER_D2C WHERE D2CFLAG ='Y'<br>
<br>
<b>API:</b>	plan/checkPlanAvailabilityForEmployees<br>
<b>Description:</b>&nbsp;<br>
<b>SQLs:</b>&nbsp;<br>
<br>
<b>API:</b>	plan/checkindvhealthplanavailability<br>
<b>Description:</b>&nbsp;This API checks whether any Health Plan is AVAILABLE for the given criteria. It checks whether health plan is available in provided zip and county for given effective date. It returns either true or false. <br>
<b>SQLs:</b>&nbsp;SELECT count(distinct p.id ) FROM plan p, pm_service_area psarea, plan_health phealth, pm_zip_county_rating_area pzcrarea, pm_plan_rate prate WHERE p.service_area_id = psarea.service_area_id AND p.id = phealth.plan_id AND pzcrarea.rating_area_id = prate.rating_area_id AND p.id = prate.plan_id AND p.insurance_type = 'HEALTH' AND psarea.zip = '91001' AND (pzcrarea.zip = '91001'  OR pzcrarea.zip IS NULL) AND psarea.fips = '06037' AND pzcrarea.county_fips =  '06037' AND p.is_deleted = 'N' AND psarea.is_deleted = 'N' AND prate.is_deleted = 'N' AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date AND p.status IN ('CERTIFIED', 'RECERTIFIED') AND p.issuer_verification_status = 'VERIFIED'  AND ( ( p.enrollment_avail='AVAILABLE' AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') >= p.enrollment_avail_effdate  AND ( TO_DATE ('2014-09-10', 'YYYY-MM-DD') < p.future_erl_avail_effdate OR  p.future_erl_avail_effdate is null) )  OR  ( p.future_enrollment_avail = 'AVAILABLE' AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') >= p.future_erl_avail_effdate) ) AND p.market = 'INDIVIDUAL' AND p.exchange_type = 'ON'  AND phealth.parent_plan_id = 0  GROUP BY (pzcrarea.rating_area_id)<br>
<br>
<b>API:</b>	plan/getPlanIssuerInfo<br>
<b>Description:</b>&nbsp;This API returns All Issuers information associated with particular plan.<br>
<b>SQLs:</b>&nbsp;select * from ( select plan0_.id as col_0_0_, plan0_.name as col_1_0_, plan0_.issuer_id as col_2_0_, issuer1_.name as col_3_0_, issuer1_.FEDERAL_EIN as col_4_0_, planhealth3_.csr_adv_pay_amt as col_5_0_, planhealth3_.plan_level as col_6_0_, plan0_.issuer_plan_number as col_7_0_, plan0_.insurance_type as col_8_0_, plan0_.market as col_9_0_ from plan plan0_, issuers issuer1_, plan_health planhealth3_ where plan0_.issuer_id=issuer1_.id and plan0_.id=planhealth3_.plan_id and plan0_.id=? ) where rownum <= ? <br>

select * from ( select plan0_.id as col_0_0_, plan0_.name as col_1_0_, plan0_.issuer_id as col_2_0_, issuer1_.name as col_3_0_, issuer1_.FEDERAL_EIN as col_4_0_, plandental3_.plan_level as col_5_0_, plan0_.issuer_plan_number as col_6_0_, plan0_.insurance_type as col_7_0_, plan0_.market as col_8_0_ from plan plan0_, issuers issuer1_, plan_dental plandental3_ where plan0_.issuer_id=issuer1_.id and plan0_.id=plandental3_.plan_id and plan0_.id=? ) where rownum <= ?<br>
<br>
<b>API:</b>	plan/getbenefitcostdata<br>
<b>Description:</b>&nbsp;This API returns benefit details for the provided plan data.<br>
<b>SQLs:</b>&nbsp;SELECT P.ID AS PLAN_ID, PH.ID AS PLAN_HEALTH_ID, TO_CHAR(P.START_DATE,'YYYY-MM-DD') AS EFFECTIVE_DATE, P.NAME AS PLAN_NAME, P.INSURANCE_TYPE, P.NETWORK_TYPE, P.PROVIDER_NETWORK_ID, P.ISSUER_PLAN_NUMBER, P.ISSUER_ID, P.MARKET, I.NAME AS ISSUER_NAME, I.COMPANY_LOGO, I.HIOS_ISSUER_ID FROM PLAN P INNER JOIN ISSUERS I ON P.ISSUER_ID = I.ID, PLAN_HEALTH PH WHERE P.IS_DELETED = 'N' AND P.id = PH.plan_id AND PH.plan_id IN (248) AND P.STATUS IN ('CERTIFIED', 'RECERTIFIED') AND P.ISSUER_VERIFICATION_STATUS ='VERIFIED' AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') BETWEEN P.START_DATE AND P.END_DATE  AND ( ( P.enrollment_avail='AVAILABLE' AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') >= P.enrollment_avail_effdate  AND ( TO_DATE ('2014-09-10', 'YYYY-MM-DD') < P.future_erl_avail_effdate OR  P.future_erl_avail_effdate is null) )  OR  ( P.future_enrollment_avail='AVAILABLE' AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') >= P.future_erl_avail_effdate) ) ORDER BY PH.plan_id ASC<br>
<br>
<b>API:</b>	plan/getdetails<br>
<b>Description:</b>&nbsp;This API returns all the Plan details for the provided plan id.<br>
<b>SQLs:</b>&nbsp;select plan0_.id as id137_0_, plan0_.applicable_year as applicable2_137_0_, plan0_.available_for as available3_137_0_, plan0_.brochure as brochure137_0_, plan0_.brochure_doc_name as brochure5_137_0_, plan0_.brochure_ucm_id as brochure6_137_0_, plan0_.BUSINESS_RULE_ID as BUSINESS7_137_0_, plan0_.certified_on as certified8_137_0_, plan0_.certifiedby as certifie9_137_0_, plan0_.comment_id as comment10_137_0_, plan0_.CREATED_BY as CREATED11_137_0_, plan0_.creation_timestamp as creation12_137_0_, plan0_.decertification_eff_date as decerti13_137_0_, plan0_.end_date as end14_137_0_, plan0_.enrollment_avail as enrollment15_137_0_, plan0_.enrollment_avail_effdate as enrollment16_137_0_, plan0_.enrollment_end_date as enrollment17_137_0_, plan0_.exchange_type as exchange18_137_0_, plan0_.formularly_id as formularly19_137_0_, plan0_.formularly_url as formularly20_137_0_, plan0_.future_enrollment_avail as future21_137_0_, plan0_.future_erl_avail_effdate as future22_137_0_, plan0_.hiosProduct_id as hiosPro23_137_0_, plan0_.hpid as hpid137_0_, plan0_.hsa as hsa137_0_, plan0_.insurance_type as insurance26_137_0_, plan0_.is_deleted as is27_137_0_, plan0_.is_puf as is28_137_0_, plan0_.issuer_id as issuer46_137_0_, plan0_.issuer_plan_number as issuer29_137_0_, plan0_.issuer_verification_status as issuer30_137_0_, plan0_.last_update_timestamp as last31_137_0_, plan0_.last_updated_by as last32_137_0_, plan0_.market as market137_0_, plan0_.name as name137_0_, plan0_.provider_network_id as provider37_137_0_, plan0_.network_id as network35_137_0_, plan0_.network_type as network36_137_0_, plan0_.service_area_id as service38_137_0_, plan0_.start_date as start39_137_0_, plan0_.state as state137_0_, plan0_.status as status137_0_, plan0_.support_file as support42_137_0_, plan0_.tp_id as tp43_137_0_, plan0_.pm_urrt_prc_id as pm44_137_0_, plan0_.verified as verified137_0_ from plan plan0_ where plan0_.id=?
<br>
<br>
<b>API:</b>	plan/getinfo<br>
<b>Description:</b>&nbsp;This API returns all the Plan details for the provided plan id.<br>
<b>SQLs:</b>&nbsp;select plan0_.id as id137_0_, plan0_.applicable_year as applicable2_137_0_, plan0_.available_for as available3_137_0_, plan0_.brochure as brochure137_0_, plan0_.brochure_doc_name as brochure5_137_0_, plan0_.brochure_ucm_id as brochure6_137_0_, plan0_.BUSINESS_RULE_ID as BUSINESS7_137_0_, plan0_.certified_on as certified8_137_0_, plan0_.certifiedby as certifie9_137_0_, plan0_.comment_id as comment10_137_0_, plan0_.CREATED_BY as CREATED11_137_0_, plan0_.creation_timestamp as creation12_137_0_, plan0_.decertification_eff_date as decerti13_137_0_, plan0_.end_date as end14_137_0_, plan0_.enrollment_avail as enrollment15_137_0_, plan0_.enrollment_avail_effdate as enrollment16_137_0_, plan0_.enrollment_end_date as enrollment17_137_0_, plan0_.exchange_type as exchange18_137_0_, plan0_.formularly_id as formularly19_137_0_, plan0_.formularly_url as formularly20_137_0_, plan0_.future_enrollment_avail as future21_137_0_, plan0_.future_erl_avail_effdate as future22_137_0_, plan0_.hiosProduct_id as hiosPro23_137_0_, plan0_.hpid as hpid137_0_, plan0_.hsa as hsa137_0_, plan0_.insurance_type as insurance26_137_0_, plan0_.is_deleted as is27_137_0_, plan0_.is_puf as is28_137_0_, plan0_.issuer_id as issuer46_137_0_, plan0_.issuer_plan_number as issuer29_137_0_, plan0_.issuer_verification_status as issuer30_137_0_, plan0_.last_update_timestamp as last31_137_0_, plan0_.last_updated_by as last32_137_0_, plan0_.market as market137_0_, plan0_.name as name137_0_, plan0_.provider_network_id as provider37_137_0_, plan0_.network_id as network35_137_0_, plan0_.network_type as network36_137_0_, plan0_.service_area_id as service38_137_0_, plan0_.start_date as start39_137_0_, plan0_.state as state137_0_, plan0_.status as status137_0_, plan0_.support_file as support42_137_0_, plan0_.tp_id as tp43_137_0_, plan0_.pm_urrt_prc_id as pm44_137_0_, plan0_.verified as verified137_0_ from plan plan0_ where plan0_.id=?<br>
<br>
<b>API:</b>	plan/getplansbyissuer<br>
<b>Description:</b>&nbsp;This API returns Plan information details for the Issuer provided. It checks for plan availability in given Zip and County for the given Issuer.<br>
<b>SQLs:</b>&nbsp;SELECT DISTINCT p.name FROM pm_service_area ps, plan p, issuers i, plan_health ph WHERE p.issuer_id = i.id AND p.is_deleted='N' AND ph.plan_id = p.id AND i.name = (?) AND ph.plan_level = (?) AND ps.is_deleted='N' AND ps.zip = (?) AND ps.fips = (?) <br>
<br>
<b>API:</b>	plan/getplansdata<br>
<b>Description:</b>&nbsp;This API returns Plan information details for multiple Plans. It reurns data for the Plan Ids provided in JSON request list.<br>
<b>SQLs:</b>&nbsp;SELECT P.ID, P.NAME, P.MARKET, P.INSURANCE_TYPE, P.NETWORK_TYPE, P.PROVIDER_NETWORK_ID, P.ISSUER_ID, I.NAME, I.PHONE_NUMBER, I.INDV_SITE_URL, I.INDV_CUST_SERVICE_TOLL_FREE, I.SHOP_SITE_URL, I.SHOP_CUST_SERVICE_TOLL_FREE, I.FEDERAL_EIN, I.HIOS_ISSUER_ID, I.PAYMENT_URL, I.APPLICATION_URL, P.EXCHANGE_TYPE, P.HSA, I.COMPANY_LOGO, P.pm_urrt_prc_id FROM PLAN P, ISSUERS I WHERE P.ID IN (167,168) AND P.ISSUER_ID = I.ID ORDER BY P.ID ASC<br>
<br>
<b>API:</b>	plan/getrefplan<br>
<b>Description:</b>&nbsp;This API returns Health Plan details for provided Issuer Zip and County Code. It returns list of Plan Id and Tier Name of the plans.<br>
<b>SQLs:</b>&nbsp;SELECT p.id As plan_id, phealth.plan_level AS plan_level  FROM  issuers i, plan p, plan_health phealth, pm_service_area psarea  WHERE  p.id = phealth.plan_id  AND p.issuer_id = i.id  AND phealth.parent_plan_id = 0  AND p.is_deleted = 'N'  AND psarea.is_deleted = 'N'  AND p.service_area_id = psarea.service_area_id  AND psarea.zip = '91746' AND psarea.fips = '06037' AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date  AND p.status IN ('CERTIFIED', 'RECERTIFIED') AND p.issuer_verification_status='VERIFIED' AND ( ( p.enrollment_avail='AVAILABLE' AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') >= p.enrollment_avail_effdate  AND ( TO_DATE ('2014-09-10', 'YYYY-MM-DD') < p.future_erl_avail_effdate OR  p.future_erl_avail_effdate is null) )  OR  ( p.future_enrollment_avail = 'AVAILABLE' AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') >= p.future_erl_avail_effdate) )  AND p.market = 'SHOP' AND p.available_for IN ('ADULTANDCHILD') <br>
<br>
<b>API:</b>	planratebenefit/findLowestPremiumPlanRate<br>
<b>Description:</b>&nbsp;<br>
<b>SQLs:</b>&nbsp;SELECT MIN(prate.rate) AS pre FROM plan p, plan_health phealth, pm_service_area psarea, pm_plan_rate prate, pm_zip_county_rating_area pzcrarea WHERE p.id = phealth.plan_id AND phealth.parent_plan_id = 0 AND p.service_area_id = psarea.service_area_id AND psarea.zip = '91841' AND p.id = prate.plan_id AND p.is_deleted = 'N' AND prate.is_deleted = 'N' AND psarea.is_deleted = 'N' AND pzcrarea.rating_area_id = prate.rating_area_id AND (pzcrarea.zip = 91841 OR pzcrarea.zip IS NULL) AND ( (49 >= prate.min_age AND 49 <= prate.max_age) OR (1008 >= prate.min_age AND 1008 <= prate.max_age) ) AND phealth.plan_level = 'PLATINUM' AND p.market = 'SHOP' AND TO_DATE ('7-14-2014', 'MM-DD-YYYY') between p.start_date and p.end_date AND p.status IN ('CERTIFIED', 'RECERTIFIED') AND p.issuer_verification_status='VERIFIED' AND prate.tobacco = 'N' AND psarea.fips = '06037' AND pzcrarea.county_fips = '06037' <br>
<br>
<b>API:</b>	planratebenefit/getemployerminmaxrate<br>
<b>Description:</b>&nbsp;<br>
<b>SQLs:</b>&nbsp;<br>
<br>
<b>API:</b>	planratebenefit/getemployerplanrate<br>
<b>Description:</b>&nbsp;This API returns Plan rate data for Employer Plan selection. It computes rate for each reference plan for employee list.<br>
<b>SQLs:</b>&nbsp;SELECT p.id AS plan_id, sum(pre) AS total_premium, sum(indvpre) AS indv_premium FROM plan p, ( ( SELECT phealth.plan_id, prate.rate AS pre, 1 AS memberctr,  0 AS indvpre  FROM plan_health phealth, pm_service_area psarea, pm_plan_rate prate, plan p2, pm_zip_county_rating_area pzcrarea WHERE p2.id = phealth.plan_id AND phealth.parent_plan_id = 0 AND p2.service_area_id = psarea.service_area_id AND psarea.zip = 'null' AND p2.id = prate.plan_id AND p2.is_deleted = 'N' AND prate.is_deleted = 'N' AND psarea.is_deleted = 'N' AND pzcrarea.rating_area_id = prate.rating_area_id AND (prate.tobacco = 'N' OR prate.tobacco is NULL)  AND ( (49 >= prate.min_age AND 49 <= prate.max_age)  OR (1008 >= prate.min_age AND 1008 <= prate.max_age) ) AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date AND (pzcrarea.zip = 'null' OR pzcrarea.zip IS NULL)  ) ) temp WHERE p.id = temp.plan_id AND  p.id = 252 AND p.available_for IN ('ADULTONLY', 'ADULTANDCHILD')  GROUP BY p.id  HAVING sum(memberctr) = 1<br>
<br>
<b>API:</b>	planratebenefit/household<br>
<b>Description:</b>&nbsp;<br>
<b>SQLs:</b>&nbsp;SELECT p.id AS plan_id, p.name AS plan_name, plan_level, sum(pre) AS total_premium, p.network_type AS network_type, i.name AS issuer_name, phealth_id, parent_id, ehb_covered, i.id as issuer_id,  listagg(memberid, ',')  within group (order by memberid) as memberlist , listagg(pre, ',')  within group (order by memberid) as memberprelist, listagg(rname, ',')  within group (order by memberid) as memberrelist, sbc_ucm_id, p.brochure AS brochure, benefits_url, net.network_url as network_url, i.company_logo as issuer_logo, p.brochure_ucm_id as brochure_ucm, max_coins_spdrug, max_chr_cpy, pmrycare_no_vsit, pmrycare_after_copay, p.formularly_id, p.tp_id, p.is_puf, rate_option, p.hsa, net.network_key, net.has_provider_data, p.issuer_plan_number  FROM plan p, issuers i, network net,  ( ( SELECT phealth.id AS phealth_id, phealth.parent_plan_id AS parent_id, phealth.plan_id, prate.rate AS pre, phealth.plan_level AS plan_level, phealth.ehb_covered as ehb_covered, 2222 as memberid, prarea.rating_area as rname, 1 as memberctr, phealth.sbc_ucm_id as sbc_ucm_id, phealth.benefits_url as benefits_url, phealth.max_coinsurance_for_sp_drug as max_coins_spdrug, phealth.max_chrging_inpatient_cpy as max_chr_cpy, phealth.pmry_care_cs_aftr_no_visits as pmrycare_no_vsit, phealth.pmry_care_dedtcoins_aftr_cpy as pmrycare_after_copay, prate.rate_option as rate_option  FROM plan_health phealth, pm_service_area psarea, pm_plan_rate prate, plan p2, pm_rating_area prarea, pm_zip_county_rating_area pzcrarea  WHERE  p2.id = phealth.plan_id  AND phealth.parent_plan_id = 0  AND p2.service_area_id = psarea.service_area_id  AND psarea.zip = null AND prarea.id = prate.rating_area_id  AND p2.id = prate.plan_id  AND p2.is_deleted = 'N'  AND prate.is_deleted = 'N'  AND psarea.is_deleted = 'N'  AND pzcrarea.rating_area_id = prate.rating_area_id  AND (prate.tobacco = UPPER('N') OR prate.tobacco is NULL)  AND ( (31 >= prate.min_age AND 31 <= prate.max_age)  OR (1008 >= prate.min_age AND 1008 <= prate.max_age) )   AND prate.rate > 0  AND (pzcrarea.zip = null OR pzcrarea.zip IS NULL)  AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date  AND phealth.plan_level = 'GOLD' AND phealth.ehb_covered = 'PARTIAL' ) ) temp WHERE p.id = temp.plan_id AND net.id = p.provider_network_id AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date  AND p.status IN ('CERTIFIED', 'RECERTIFIED')  AND p.enrollment_avail='AVAILABLE'  AND TO_DATE ('2014-09-10', 'YYYY-MM-DD') >= p.enrollment_avail_effdate  AND p.market = 'SHOP' AND p.issuer_id = i.id  AND p.id in (251)  AND p.issuer_Plan_number = '59666CA001200102'  AND p.issuer_id = 3 AND p.exchange_type = 'ON'  AND p.available_for IN ('ADULTONLY', 'ADULTANDCHILD')  GROUP BY  p.id, p.name, net.network_url, phealth_id, parent_id, plan_level, i.name, i.company_logo, p.network_type, ehb_covered, i.id, sbc_ucm_id, p.brochure, p.brochure_ucm_id, benefits_url, max_coins_spdrug, max_chr_cpy, pmrycare_no_vsit, pmrycare_after_copay, p.formularly_id, p.tp_id, p.is_puf, rate_option, p.hsa, net.network_key, net.has_provider_data, p.issuer_plan_number HAVING sum(memberctr) = 1<br>
<br>
<b>API:</b>	planratebenefit/memberlevelemployeerate<br>
<b>Description:</b>&nbsp;<br>
<b>SQLs:</b>&nbsp;<br>
<br>
<b>API:</b>	planratebenefit/memberlevelpremium<br>
<b>Description:</b>&nbsp;<br>
<b>SQLs:</b>&nbsp;<br>
<br>
<b>API:</b>	premiumRestService/houseHoldReq<br>
<b>Description:</b>&nbsp;<br>
<b>SQLs:</b>&nbsp;<br>
<br>
<b>API:</b>	stmplanratebenefit/stmplan<br>
<b>Description:</b>&nbsp;<br>
<b>SQLs:</b>&nbsp;<br>
<br>
<b>API:</b>	teaserPlanRestService/teaserPlan/eligiblityRequest<br>
<b>Description:</b>&nbsp;<br>
<b>SQLs:</b> </font></p>


</body>

</html>
