<html>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>Issuers Certification Status</title>
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<body>
<sql:query dataSource="${jspDataSource}" var="result1">
select issuer_id,state,company_legal_name, count(*) as total,
sum(case when (issuer_verification_status='VERIFIED') then 1 else 0 end) as verified_count,
sum(case when (issuer_verification_status='PENDING') then 1 else 0 end) as pending_count,
sum(case when (issuer_verification_status is null) then 1 else 0 end) as other
from(
select p.issuer_id,p.state,i.company_legal_name,p.issuer_verification_status
from plan p, issuers i
where p.issuer_id=i.id
order by p.issuer_id,P.STATE
) allRec 
group by issuer_id,state,company_legal_name
order by issuer_id,state
</sql:query> 
<b>Issuers Plans Status.</b>
<br>

<display:table name="${result1.rows}"  requestURI="allIssuersStatus" id="table1" export="true" pagesize="500" style="width:80%" defaultorder="descending" defaultsort="4">
	<display:column property="issuer_id" title="Issuer Id " sortable="true" />
	<display:column property="STATE" title="State" sortable="true"/>
	<display:column property="company_legal_name" title="Company Legal Name" sortable="true"/>
	<display:column property="pending_count" title="Pending Count" sortable="true"  />
	<display:column property="verified_count" title="Verified Count" sortable="true"/>
	<display:column property="other" title="Other" sortable="true"/>
	<display:column property="total" title="Total" sortable="true"/>
	
	<display:setProperty name="export.xml.filename" value="allIssuerStatus.xml"/>
	<display:setProperty name="export.csv.filename" value="allIssuerStatus.csv"/>
	<display:setProperty name="export.excel.filename" value="allIssuerStatus.xls"/>
</display:table>
</body> 
</html>
