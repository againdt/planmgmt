<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page contentType="text/html" import="java.util.*"%>
<%@ include file="datasource.jsp"%>



<html>
<script type="text/javascript"
	src="../resources/js/jquery-1.10.2.min.js"></script>
<link rel="stylesheet"
	href="https://rawgithub.com/yesmeck/jquery-jsonview/master/dist/jquery.jsonview.css" />
<script type="text/javascript"
	src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript"
	src="https://rawgithub.com/yesmeck/jquery-jsonview/master/dist/jquery.jsonview.js"></script>

<script>
	
	$("#effDate").datepicker();
	
</script>

<script type="text/javascript">

$(document).ready(function() {
	$("#memone").children().attr("disabled","disabled");
	$("#memtwo").children().attr("disabled","disabled");
	$("#memthree").children().attr("disabled","disabled");
	$("#memfour").children().attr("disabled","disabled");
	
});

function enableMember(){
	var memCheck1 = $("#mem1:checked").val();
	var memCheck2 = $("#mem2:checked").val();
	var memCheck3 = $("#mem3:checked").val();
	var memCheck4 = $("#mem4:checked").val();
	//alert(memCheck1);
	if(memCheck1=='on'){
		$("#memone").children().removeAttr("disabled");
	}else{
		$("#memone").children().attr("disabled","disabled");
	}
	
	if(memCheck2=='on'){
		$("#memtwo").children().removeAttr("disabled");
	}else{
		$("#memtwo").children().attr("disabled","disabled");
	}
	if(memCheck3=='on'){
		$("#memthree").children().removeAttr("disabled");
	}else{
		$("#memthree").children().attr("disabled","disabled");
	}
	if(memCheck4=='on'){
		$("#memfour").children().removeAttr("disabled");
	}else{
		$("#memfour").children().attr("disabled","disabled");
	}
}

function appendLevel(){
	$("#planLevel").empty();
	if($("#insType:checked").val() == 'HEALTH'){
		$("#planLevel").append($("<option></option>").val("PLATINUM").text("PLATINUM"));
		$("#planLevel").append($("<option></option>").val("GOLD").text("GOLD"));
		$("#planLevel").append($("<option></option>").val("SILVER").text("SILVER"));
		$("#planLevel").append($("<option></option>").val("CATASTROPHIC").text("CATASTROPHIC"));
		$("#planLevel").append($("<option></option>").val("BRONZE").text("BRONZE"));
	}else{
		$("#planLevel").append($("<option></option>").val("HIGH").text("HIGH"));
		$("#planLevel").append($("<option></option>").val("LOW").text("LOW"));
	}
	
}

	function getValue(val) {

		var id = null;
		var zip = null;
		var countycode = null;
		var age = null;
		var tobacco = null;
		var relation = null;
		var mem1 = null;
		var mem2 = null;
		var mem3 = null;
		var mem4 = null;
		
		zip = $("#zipCode").val();
		countycode = $("#countyCode").val();
				
		var memCheck1 = $("#mem1:checked").val();
		var memCheck2 = $("#mem2:checked").val();
		var memCheck3 = $("#mem3:checked").val();
		var memCheck4 = $("#mem4:checked").val();
		
		if(memCheck1=='on'){
			id = $("#id").val();
			age = $("#age").val();
			tobacco = $("#tobacco:checked").val();
			relation = $("#relation").val();
			mem1 = "{\"id\":\"" + id + "\"," 
					+ "\"zip\":\"" + zip + "\"," 
					+ "\"countycode\":\"" + countycode + "\"," 
					+ "\"age\":\"" + age + "\"," 
					+ "\"tobacco\":\"" + tobacco + "\"," 
					+ "\"relation\":\"" + relation + "\"}";
			
		}
		
		if(memCheck2=='on'){
			id2 = $("#id").val();
			age2 = $("#age").val();
			tobacco2 = $("#tobacco:checked").val();
			relation2 = $("#relation2").val();
			mem2 = ",{\"id\":\"" + id2 + "\"," 
					+ "\"zip\":\"" + zip + "\"," 
					+ "\"countycode\":\"" + countycode + "\"," 
					+ "\"age\":\"" + age2 + "\"," 
					+ "\"tobacco\":\"" + tobacco2 + "\"," 
					+ "\"relation\":\"" + relation2 + "\"}";
		}
		
		if(memCheck3=='on'){
			id3 = $("#id").val();
			age3 = $("#age").val();
			tobacco3 = $("#tobacco:checked").val();
			relation3 = $("#relation3").val();
			mem3 = ",{\"id\":\"" + id3 + "\"," 
				+ "\"zip\":\"" + zip + "\"," 
				+ "\"countycode\":\"" + countycode + "\"," 
				+ "\"age\":\"" + age3 + "\"," 
				+ "\"tobacco\":\"" + tobacco3 + "\"," 
				+ "\"relation\":\"" + relation3 + "\"}";
		}
		
		if(memCheck4=='on'){
			id4 = $("#id").val();
			age4 = $("#age").val();
			tobacco4 = $("#tobacco:checked").val();
			relation4 = $("#relation4").val();
			mem4 = ",{\"id\":\"" + id4 + "\"," 
				+ "\"zip\":\"" + zip + "\"," 
				+ "\"countycode\":\"" + countycode + "\"," 
				+ "\"age\":\"" + age4 + "\"," 
				+ "\"tobacco\":\"" + tobacco4 + "\"," 
				+ "\"relation\":\"" + relation4 + "\"}";
		}
		
		
		var insType =null;
		var effectiveDate = null;
		var costSharing = null;
		var planIdStr = null;
		var showCatastrophicPlan = null;
		var planLevel = null;
		var ehbCovered = null;
		var providers = null;
		var marketType = null;
		var isSpecialEnrollment = null;
		var groupId = null;
		var issuerId = null;
		var issuerVerifiedFlag = null;
		var exchangeType = null;
		var tenant = null;
		//Map providerMap = new HashMap();
		insType =$("#insType:checked").val();
		effectiveDate = $("#effDate").val();
		costSharing = $("#costSharing").val();
		planIdStr = $("#planIds").val();
		showCatastrophicPlan = $("#showCatastrophic:checked").val();
		planLevel = $("#planLevel").val();
		ehbCovered = $("#ehbCovered:checked").val();
		providers =$("#providers").val();
			//$("#providers").val();
		marketType = $("#market").val();
		isSpecialEnrollment = $("#specialEnrol:checked").val();
		groupId=$("#groupId").val();
		//issuerId = $("#effDate").val();
		//issuerVerifiedFlag = $("#effDate").val();
		exchangeType = $("#exType:checked").val();
		//tenant = $("#effDate").val();
		
		var str1 = "{\"requestParameters\":{\"memberList\":[" ;
		if(mem1 != null){
			str1=str1+mem1;
		}
		if(mem2 != null){
			str1=str1+mem2;
		}
		if(mem3 != null){
			str1=str1+mem3;
		}
		if(mem4 != null){
			str1=str1+mem4;
		}
		str1=str1+"],";
			
		
		var str2 = "\"insType\":\"" + insType + "\"," 
			+ "\"effectiveDate\":\"" + effectiveDate + "\"," 
			+ "\"costSharing\":\"" + costSharing + "\","
			+ "\"planIdStr\":\"" + planIdStr + "\"," 
			+ "\"showCatastrophicPlan\":" + showCatastrophicPlan + ","
			+ "\"planLevel\":\"" + planLevel + "\"," 
			+ "\"groupId\":" + groupId + "," 
			+ "\"ehbCovered\":\"" + ehbCovered + "\","
			+ "\"providers\":" + providers + "," 
			+ "\"marketType\":\"" + marketType + "\","
			+ "\"isSpecialEnrollment\":\"" + isSpecialEnrollment + "\"," 
			//+ "\"exchangeType\":\"" + exchangeType + "\","
			+ "\"exchangeType\":\"" + exchangeType + "\"}}";
			
			var jsonStr = str1+str2;
		
		
		$("textarea[name='inputJSON']").val(jsonStr);
	}
	
	

	function submitData(val) {
		
		var json = "";
		var start = new Date();
		var url = "";
		var defaultUrl = "ghix-planmgmt/planratebenefit/household";
		var targetUrl = $('#targetURL').val();
		//var jsonview = "";
		if (targetUrl != "") {
			url = $('#protocol').val() + "://" + targetUrl + "/" + defaultUrl;
		} else {
			alert("Please enter valid url, e.g. localhost:8080 ");
		}
		//alert(url);
		$.ajax({
			type : "POST",
			contentType : "application/json; charset=utf-8",
			url : url,
			data : val,
			header: { 'Access-Control-Allow-Origin': '*'},
		    crossDomain: true,
			success : function(responseText) {
				$("textarea[name='outputRaw']").val(responseText);
				var end = new Date();
				var duration = end - start;
				$('output[name="duration"]').val(duration + " Milli Seconds");
				
				$('#json').JSONView(responseText);
				//alert(json);
				checkLength(responseText);
				

			},
			error : function(responseText) {
				//alert(responseText);
				$("textarea[name='outputRaw']").val(
						"Error occurred while getting plan info");
				var end = new Date();
				var duration = end - start;
				$('output[name="duration"]').val(duration + " Milli Seconds");
				$('#json').JSONView(responseText);

			}

		});

	}
	
	function checkLength(val) {
        var countMe = val;
        var escapedStr = encodeURI(countMe)
        if (escapedStr.indexOf("%") != -1) {
            var count = escapedStr.split("%").length - 1
            if (count == 0) count++  //perverse case; can't happen with real UTF-8
            var tmp = escapedStr.length - (count * 3)
            count = count + tmp
        } else {
            count = escapedStr.length
            
        }
        $('output[name="count"]').val(count + " bytes");
       
        if (count > 0 ){
        	var kb = count/1024 + "KB";
        	 $('output[name="kb"]').val(kb);
        	}
       	
     }
	
	
	function doCollapse(){
		//alert(1);
		$('#collapse-btn').on('click', function() {
		    $('#json').JSONView('collapse');
		  });
		
		
	}
	
	function doExpand(){
		//alert(1);
		 $('#expand-btn').on('click', function() {
			    $('#json').JSONView('expand');
			  });
	}
	
	function getCountyFips(){
		var whereClause = "";
		var zipCode = $('#zipCode').val();
		whereClause = "zip="+zipCode;
		document.getElementById('whereClause').value = whereClause;
		//alert(whereClause);
		$('#load').submit();
				
	}
	
</script>

<% 
List<Map<String, List<String>>> providers = new ArrayList<Map<String,List<String>>>();
	
%>

<% 
      String hostName = request.getHeader("host");
	
%>
<%
	String whereClause = request.getParameter("whereClause");
	 if( whereClause == null || whereClause == ""){
		whereClause = "zip='00000'";
		
	} 
	 
	String zip= request.getParameter("zipCode");
	if(zip != null)
	request.setAttribute("zipCode",zip);
	 
	
%>

<sql:query dataSource="${jspDataSource}" var="result1">
		select distinct(fips) from pm_service_area where  <%=whereClause %>
</sql:query>



<body>

	<form method="POST" id="load">
		<table >
			<tr>
				<td width="50%" bgcolor="#FFCC00" align="left"><font
					face="Arial" size="2"><b>API: Get Household Information</b></font></td>

				<td width="50%" bgcolor="#FFCC00" align="left"><font
					face="Arial" size="2"><b>Path:</b> <a
						href="http://localhost:8080/ghix-planmgmt/planratebenefit/household">/ghix-planmgmt/planratebenefit/household</a>
						&nbsp;</font></td>
			</tr>


			<tr>
				<td bgcolor="#C4FCB8">Zip Code <input type="number"
					id="zipCode" name="zipCode" value="${zipCode}" maxlength="5">
					<input type="button" value="loadFips" name="loadFips"
					onclick="getCountyFips()">
				</td>

				<td bgcolor="#C4FCB8">County Fips: <select id="countyCode"
					name="countyCode">
						<option value="">Select</option>
						<c:forEach var="servicearea" items="${result1.rows}">
							<option value="${servicearea.fips}">${servicearea.fips}
							</option>
						</c:forEach>

				</select>
				</td>
			</tr>

			<tr>
				<td width="50%" bgcolor="#C4FCB8">Cost sharing: <select
					id="costSharing" name="costSharing">
						<option value="">Select</option>
						<option value="CS0">CS0</option>
						<option value="CS1">CS1</option>
						<option value="CS2">CS2</option>
						<option value="CS3">CS3</option>
						<option value="CS4">CS4</option>
						<option value="CS5">CS5</option>
						<option value="CS6">CS6</option>
				</select>
				</td>
				<td width="50%" bgcolor="#C4FCB8">Effective date: <input
					type="date" class="datepicker" id="effDate" name="effDate" />

				</td>
			</tr>


			<tr>
				<td width="50%" bgcolor="#C4FCB8">Group Id: <input type="text"
					id="groupId" name="groupId" />

				</td>
				<td width="50%" bgcolor="#C4FCB8">Insurance Type: <input
					type="radio" id="insType" name="insType" value="HEALTH"
					onclick="appendLevel();">Health <input type="radio"
					id="insType" name="insType" value="DENTAL" onclick="appendLevel();">
					Dental


				</td>
			</tr>



			<tr>
				<td width="50%" bgcolor="#C4FCB8">Plan Id's: <input type="text"
					id="planIds" name="planIds">
				</td>
				<td width="50%" bgcolor="#C4FCB8">Show Catastrophic: <input
					type="radio" id="showCatastrophic" name="showCatastrophic"
					value="true"> True <input type="radio"
					id="showCatastrophic" name="showCatastrophic" value="false"
					checked="checked"> False <br>

				</td>
			</tr>


			<tr>
				<td width="50%" bgcolor="#C4FCB8">Plan Level: <select
					id="planLevel" name="planLevel"></select>
				</td>
				<td width="50%" bgcolor="#C4FCB8">EHB Covered: <input
					type="radio" id="ehbCovered" name="ehbCovered" value="true">
					True <input type="radio" id="ehbCovered" name="ehbCovered"
					value="false" checked="checked"> False


				</td>
			</tr>

			<tr>
				<td width="50%" bgcolor="#C4FCB8">Tenant: <select id="tenant"
					name="tenant">
						<option value="">Select</option>
						<option value="GINS">GetInsured</option>
						<option value="BBAY">BenefitBay</option>
				</select> <input type="hidden" id="providers" name="providers"
					value="<%=providers %>">

				</td>

				<td width="50%" bgcolor="#C4FCB8">Special Enrollment: <input
					type="radio" id="specialEnrol" name="specialEnrol" value="true">True
					<input type="radio" id="specialEnrol" name="specialEnrol"
					value="false" checked="checked">False
				</td>

			</tr>

			<tr>
				<td width="50%" bgcolor="#C4FCB8">Market Type: <select
					id="market" name="market">
						<option value="">Select</option>
						<option value="SHOP">SHOP</option>
						<option value="INDIVIDUAL">INDIVIDUAL</option>
				</select>
				</td>

				<td width="50%" bgcolor="#C4FCB8">Exchange type: <input
					type="radio" id="exType" name="exType" value="ON" checked="checked">
					ON <input type="radio" id="exType" name="exType" value="OFF">
					OFF
				</td>
			</tr>

			<tr>

				<td colspan="2" bgcolor="#C4FCB8"><input type="checkbox"
					id="mem1" name="mem1" onclick="enableMember();">
					<div id="memone">
						<b>Primary</b> Id: <input type="text" name="id" id="id">
						Age: <input type="text" id="age" name="age"> Relation: <select
							id="relation" name="relation">
							<option value="">Select</option>
							<option value="primary">Primary</option>
							<option value="spouse">Spouse</option>
							<option value="child">Child</option>
						</select> Tobacco: <input type="radio" id="tobacco" name="tobacco"
							value="y"> Yes <input type="radio" id="tobacco"
							name="tobacco" value="n" checked="checked">No
					</div></td>

			</tr>

			<tr>
				<td colspan="2" bgcolor="#C4FCB8"><input type="checkbox"
					id="mem2" name="mem2" onclick="enableMember();">
					<div id="memtwo">

						<b>Spouse</b> Id: <input type="text" name="id2" id="id2">
						Age: <input type="text" id="age2" name="age2"> Relation: <select
							id="relation2" name="relation2">
							<option value="">Select</option>
							<option value="primary">Primary</option>
							<option value="spouse">Spouse</option>
							<option value="child">Child</option>
						</select> Tobacco: <input type="radio" id="tobacco2" name="tobacco2"
							value="y"> Yes <input type="radio" id="tobacco2"
							name="tobacco2" value="n" checked="checked">No
					</div></td>
			</tr>

			<tr>
				<td colspan="2" bgcolor="#C4FCB8"><input type="checkbox"
					id="mem3" name="mem3" onclick="enableMember();">
					<div id="memthree">

						<b>Child</b> Id: <input type="text" name="id3" id="id3">
						Age: <input type="text" id="age3" name="age3"> Relation: <select
							id="relation3" name="relation3">
							<option value="">Select</option>
							<option value="primary">Primary</option>
							<option value="spouse">Spouse</option>
							<option value="child">Child</option>
						</select> Tobacco: <input type="radio" id="tobacco3" name="tobacco3"
							value="y"> Yes <input type="radio" id="tobacco3"
							name="tobacco3" value="n" checked="checked">No
					</div></td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#C4FCB8"><input type="checkbox"
					id="mem4" name="mem4" onclick="enableMember();">
					<div id="memfour">

						<b>Child</b> Id: <input type="text" name="id4" id="id4">
						Age: <input type="text" id="age4" name="age4"> Relation: <select
							id="relation4" name="relation4">
							<option value="">Select</option>
							<option value="primary">Primary</option>
							<option value="spouse">Spouse</option>
							<option value="child">Child</option>
						</select> Tobacco: <input type="radio" id="tobacco4" name="tobacco4"
							value="y"> Yes <input type="radio" id="tobacco4"
							name="tobacco4" value="n" checked="checked">No
					</div></td>
			</tr>




			<tr>
				<td colspan="2" bgcolor="#C4FCB8" align="center"><input
					type="button" value="Populate" name="Populate" onclick="getValue()"></td>
				<td></td>
			</tr>


			<tr>
				<td colspan="2" bgcolor="#C4FCB8"><textarea rows="10"
						name="inputJSON" cols="150" style="background-color: #BDFFFF"
						id="inputJSON">
						<c:out value="${inputJSON}"></c:out>
					</textarea></td>
				<td></td>

			</tr>
			<tr>
				<td colspan="2" bgcolor="#C4FCB8">
					<p align="left">
						<font face="Arial" size="2">Protocol: <input type="radio"
							id="protocol" name="protocol" value="http" checked="checked">
							HTTP <input type="radio" id="protocol" name="protocol"
							value="https"> HTTPS
						</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font face="Arial" size="2">URL:
							<input type="text" name="targetURL" size="20"
							style="background-color: #BDFFFF" value="<%=hostName%>"
							id="targetURL"> <input type="button" value="Post"
							name="Post"
							onclick="submitData(document.getElementById('inputJSON').value);">
							<input type="reset" value="Reset" name="B2">
						</font>
					</p>
				</td>
				<td></td>
			<tr>
			<tr>
				<td width="50%" bgcolor="#999999"><font face="Arial" size="2"><b>Duration</b>:
						<output id="duration" name="duration"> </output></td>
				<td width="50%" bgcolor="#999999"><b>Size</b>: <output
						id="count" name="count"> </output> | <output id="kb" name="kb">
					</output></td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#999999"><b>Raw Response:</b></td>
			</tr>
			<tr>
				<td align="left" bgcolor="#CCCCCC" colspan="2"><textarea
						cols="150" rows="20" name="outputRaw"
						style="background-color: #BDFFFF" id="outputRaw">
								<c:out value="${outputRaw}"></c:out>
							</textarea></td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#CCCCCC"><b>JSON Response:</b><input
					type="button" value="Collapse" name="Collapse" id="collapse-btn"
					onclick="doCollapse()"> <input type="button" value="Expand"
					name="Expand" id="expand-btn" onclick="doExpand()"></td>
				<td></td>

			</tr>
			<tr>
				<td colspan="2" bgcolor="#CCCCCC"><div id="json"></div></td>
			<tr>
			<tr>
				<td  bgcolor="#CCCCCC">SQLs:</td>
			</tr>
			
			<tr>
				<td colspan="2"><b>Health Query:</b>
				
				<div id="healthQuery">
				
				SELECT p.id AS plan_id,
					  p.name    AS plan_name,
					  plan_level,
					  SUM(pre)       AS total_premium,
					  p.network_type AS network_type,
					  i.name         AS issuer_name,
					  phealth_id,
					  parent_id,
					  ehb_covered,
					  i.id AS issuer_id,
					  listagg(memberid, ',') within GROUP (
					ORDER BY memberid) AS memberlist ,
					  listagg(pre, ',') within GROUP (
					ORDER BY memberid) AS memberprelist,
					  listagg(rname, ',') within GROUP (
					ORDER BY memberid) AS memberrelist,
					  sbc_ucm_id,
					  p.brochure AS brochure,
					  benefits_url,
					  net.network_url   AS network_url,
					  i.company_logo    AS issuer_logo,
					  p.brochure_ucm_id AS brochure_ucm,
					  max_coins_spdrug,
					  max_chr_cpy,
					  pmrycare_no_vsit,
					  pmrycare_after_copay,
					  p.formularly_id,
					  p.tp_id,
					  p.is_puf,
					  rate_option,
					  p.hsa,
					  net.network_key,
					  net.has_provider_data,
					  p.issuer_plan_number
					FROM plan p,
					  issuers i,
					  network net,
					  (
					  (SELECT phealth.id       AS phealth_id,
					    phealth.parent_plan_id AS parent_id,
					    phealth.plan_id,
					    prate.rate                           AS pre,
					    phealth.plan_level                   AS plan_level,
					    phealth.ehb_covered                  AS ehb_covered,
					    11                                   AS memberid,
					    prarea.rating_area                   AS rname,
					    1                                    AS memberctr,
					    phealth.sbc_ucm_id                   AS sbc_ucm_id,
					    phealth.benefits_url                 AS benefits_url,
					    phealth.max_coinsurance_for_sp_drug  AS max_coins_spdrug,
					    phealth.max_chrging_inpatient_cpy    AS max_chr_cpy,
					    phealth.pmry_care_cs_aftr_no_visits  AS pmrycare_no_vsit,
					    phealth.pmry_care_dedtcoins_aftr_cpy AS pmrycare_after_copay,
					    prate.rate_option                    AS rate_option
					  FROM plan_health phealth,
					    pm_service_area psarea,
					    pm_plan_rate prate,
					    plan p2,
					    pm_rating_area prarea,
					    pm_zip_county_rating_area pzcrarea
					  WHERE p2.id                 = phealth.plan_id
					  AND phealth.parent_plan_id  = 0
					  AND p2.service_area_id      = psarea.service_area_id
					  AND psarea.zip              = ?
					  AND prarea.id               = prate.rating_area_id
					  AND p2.id                   = prate.plan_id
					  AND p2.is_deleted           = 'N'
					  AND prate.is_deleted        = 'N'
					  AND psarea.is_deleted       = 'N'
					  AND pzcrarea.rating_area_id = prate.rating_area_id
					  AND (prate.tobacco          = UPPER('n')
					  OR prate.tobacco           IS NULL)
					  AND ( (?                  >= prate.min_age
					  AND ?                     <= prate.max_age)
					  OR (?                   >= prate.min_age
					  AND ?                   <= prate.max_age) )
					  AND prate.rate              > 0
					  AND (pzcrarea.zip           = ?
					  OR pzcrarea.zip            IS NULL)
					  AND TO_DATE ('2014-08-21', 'YYYY-MM-DD') BETWEEN prate.effective_start_date AND prate.effective_end_date
					  AND phealth.plan_level   = ?
					  AND phealth.ehb_covered  = ?
					  AND psarea.fips          = ?
					  AND pzcrarea.county_fips = ?
					  )
					) temp
					WHERE p.id = temp.plan_id
					AND net.id = p.provider_network_id
					AND TO_DATE ('?, 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date
					AND p.status                             IN ('CERTIFIED', 'RECERTIFIED')
					AND p.issuer_verification_status          ='VERIFIED'
					AND p.enrollment_avail                    ='AVAILABLE'
					AND TO_DATE (?, 'YYYY-MM-DD') >= p.enrollment_avail_effdate
					AND p.market                              = 'SHOP'
					AND p.issuer_id                           = i.id
					AND p.exchange_type                       = 'ON'
					AND p.available_for                      IN ('ADULTONLY', 'ADULTANDCHILD')
					GROUP BY p.id,
					  p.name,
					  net.network_url,
					  phealth_id,
					  parent_id,
					  plan_level,
					  i.name,
					  i.company_logo,
					  p.network_type,
					  ehb_covered,
					  i.id,
					  sbc_ucm_id,
					  p.brochure,
					  p.brochure_ucm_id,
					  benefits_url,
					  max_coins_spdrug,
					  max_chr_cpy,
					  pmrycare_no_vsit,
					  pmrycare_after_copay,
					  p.formularly_id,
					  p.tp_id,
					  p.is_puf,
					  rate_option,
					  p.hsa,
					  net.network_key,
					  net.has_provider_data,
					  p.issuer_plan_number
					HAVING SUM(memberctr) = 2
						</div>		
				</td>
				
			</tr>
			<tr><td colspan="2"><b>Dental Query:</b>
			<div id="dentalQuery">
			SELECT p.id AS plan_id,
						  p.name    AS plan_name,
						  plan_level,
						  SUM(pre)       AS total_premium,
						  p.network_type AS network_type,
						  i.name         AS issuer_name,
						  pdental_id,
						  net.network_url AS network_url,
						  i.company_logo  AS issuer_logo,
						  listagg(memberid, ',') within GROUP (
						ORDER BY memberid) AS memberlist ,
						  listagg(pre, ',') within GROUP (
						ORDER BY memberid) AS memberprelist,
						  listagg(rname, ',') within GROUP (
						ORDER BY memberid)  AS memberrelist,
						  p.brochure_ucm_id AS brochure_ucm,
						  p.brochure        AS brochure,
						  p.is_puf,
						  rate_option,
						  p.formularly_id,
						  p.hsa,
						  GVER,
						  i.id AS issuer_id
						FROM plan p,
						  issuers i,
						  network net,
						  (
						  (SELECT pdental.id AS pdental_id,
						    pdental.plan_id,
						    prate.rate                           AS pre,
						    pdental.plan_level                   AS plan_level,
						    11                                   AS memberid,
						    prarea.rating_area                   AS rname,
						    1                                    AS memberctr,
						    prate.rate_option                    AS rate_option,
						    pdental.GUARANTEED_VS_ESTIMATED_RATE AS GVER
						  FROM plan_dental pdental,
						    pm_service_area psarea,
						    pm_rating_area prarea,
						    pm_plan_rate prate,
						    plan p2,
						    pm_zip_county_rating_area pzcrarea
						  WHERE p2.id                 = pdental.plan_id
						  AND p2.service_area_id      = psarea.service_area_id
						  AND psarea.zip              = ?
						  AND p2.id                   = prate.plan_id
						  AND p2.is_deleted           = 'N'
						  AND prate.is_deleted        = 'N'
						  AND psarea.is_deleted       = 'N'
						  AND prarea.id               = prate.rating_area_id
						  AND pzcrarea.rating_area_id = prate.rating_area_id
						  AND (pzcrarea.zip           = ?
						  OR pzcrarea.zip            IS NULL)
						  AND (prate.tobacco          = UPPER('n')
						  OR prate.tobacco           IS NULL)
						  AND ( (?                  >= prate.min_age
						  AND ?                     <= prate.max_age)
						  OR (?                   >= prate.min_age
						  AND ?                   <= prate.max_age) )
						  AND prate.rate              > 0
						  AND TO_DATE ('2014-08-22', 'YYYY-MM-DD') BETWEEN prate.effective_start_date AND prate.effective_end_date
						  AND pdental.plan_level   = 'HIGH'
						  AND psarea.fips          = '35015'
						  AND pzcrarea.county_fips = '35015'
						  ) ) temp
						WHERE p.id = temp.plan_id
						AND net.id = p.provider_network_id
						AND TO_DATE (?, 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date
						AND p.status                             IN ('CERTIFIED', 'RECERTIFIED')
						AND p.issuer_verification_status          ='VERIFIED'
						AND p.enrollment_avail                    ='AVAILABLE'
						AND TO_DATE (?, 'YYYY-MM-DD') >= p.enrollment_avail_effdate
						AND p.market                              = ?
						AND p.issuer_id                           = i.id
						AND p.exchange_type                       = ?
						AND p.available_for                      IN ('ADULTONLY', 'ADULTANDCHILD')
						GROUP BY p.id,
						  p.name,
						  net.network_url,
						  pdental_id,
						  plan_level,
						  i.name,
						  i.company_logo,
						  p.network_type,
						  p.brochure,
						  p.brochure_ucm_id,
						  p.is_puf,
						  rate_option,
						  p.formularly_id,
						  p.hsa,
						  GVER,
						  i.id
						HAVING SUM(memberctr) = 1
			
			
			</div>
			
			</td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#CCCCCC">Notes:
					http://jsbin.com/bacet/4/edit</td>
				<td></td>
			</tr>
		</table>
		<input id="whereClause" name="whereClause" type="hidden">
	</form>

</body>


</html>