<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@include file="datasource.jsp" %>

<head>
	<title>Plan Docs Admin</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<script type="text/javascript">

	$(function() {
		$("#date1").datepicker().attr('readOnly', 'true');
	});
	
	/* $("input").on("keypress keyup paste", function(e) {
	    this.value = this.value.replace("/(\!|@|\#|\$|\%|\^|\&|\*|\(|\)|\+|\=)/g", "");
	}); */
	
	function validateDate(dateGiven) {
		
		var today=new Date();
		var dob_mm = dateGiven[0];  
		var dob_dd = dateGiven[1];  
		var dob_yy = dateGiven[2]; 
		
		var birthDate=new Date();
		birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
	
		if ((today.getFullYear() - 2) > birthDate.getFullYear()) {
			// alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
		}
	
		if ((dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear())) {
			// alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
		}
	
		if(today.getTime() < birthDate.getTime()){
			//alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
		}
	}
	
	function setWhereClause(type) {
		
		var whereClause = '';
		var filterStatement = '';
		var topTenSelectClause = '';
		var topTenWhereClause = '';
		
		switch (type) {
			case 'B1' : 
			<%if(isPostgresDB) {%>
				whereClause = 'last_updated between (now()-interval \'1 day\') and now()';
			<%} else {%>
				whereClause = 'last_updated between (sysdate-1) and sysdate';
			<%}%>
				filterStatement = ' in last 24 hours';
				break;
			case 'B2' : 
			<%if(isPostgresDB) {%>
				whereClause = 'last_updated between (now()-interval \'12 hour\') and now()';
			<%} else {%>
				whereClause = 'last_updated between (sysdate-1/2) and sysdate';
			<%}%>
				filterStatement = ' in last 12 hours';
				break;
			case 'B3' : 
			<%if(isPostgresDB) {%>
				whereClause = 'last_updated between (now()-interval \'1 hour\') and now()';
			<%} else {%>
				whereClause = 'last_updated between (sysdate-1/24) and sysdate';
			<%}%>
				filterStatement = ' in last 1 hour';
				break;
			case 'B4' : 
			    //Step 1: Get List Box value, if it is not empty
				var fieldName = document.getElementById('field_name').value;
				var fieldValue = document.getElementById('field_value').value;
				
				if (fieldValue && fieldValue.length > 0) {
				<%if(isPostgresDB) {%>
					whereClause = " lower(CAST ("+fieldName + " as TEXT)) like lower('%" + fieldValue + "%')";
				<%} else {%>
					whereClause = " lower("+fieldName + ") like lower('%" + fieldValue + "%')";
				<%}%>
					filterStatement = ' with ' + fieldName + " = " + fieldValue;
				}
				
				//Step 2: Get the Status Value
				var statuses = document.getElementById('status');
				var status = statuses.options[statuses.selectedIndex].value;
				
				if(status == 'COMPLETED' || status == 'FAILED')
				{
					if (whereClause) {
						whereClause = whereClause  + " and status = '" + status + "'";
					}
					else {
						whereClause = whereClause  + " status = '" + status + "'";
					}
				}
				
				if (status == 'S') {
					
					filterStatement = filterStatement+ ' which were successful';
				}
				
				if (status == 'F') {
				
					filterStatement = filterStatement+ ' which have failed';
				}
				
				//Step 3: Get the Dates
				var date1 = document.getElementById('date1').value;
				
					if (date1) {
						if (whereClause) {
							whereClause = whereClause+"and to_char(last_updated  ,'mm/dd/yyyy' )='"+date1+"'" ;
							filterStatement = filterStatement + 'start date ' +  date1 ;
						}
						else {
							whereClause = whereClause+" to_char(last_updated  ,'mm/dd/yyyy' )='"+date1+"'" ;
							filterStatement = filterStatement + 'last update date' +  date1 ;
						}
					}
					
					if (!whereClause) {
					<%if(isPostgresDB) {%>
						whereClause = "last_updated <= now()";
					<%} else {%>
						whereClause = "last_updated <= sysdate";
					<%}%>
					}
				break;
			case 'B5' : 
				topTenSelectClause=' Select * from ( ';
				<%if(isPostgresDB) {%>
				topTenWhereClause=' ) allrec limit 10 ';
				<%} else {%>
				topTenWhereClause=' ) where rownum <=10 order by  rownum';
				<%}%>
				filterStatement = 'for latest records';
				break;
		}
		document.getElementById('whereClause').value = whereClause;
		document.getElementById('filterStatement').value = filterStatement;
		document.getElementById('topTenSelectClause').value = topTenSelectClause;
		document.getElementById('topTenWhereClause').value = topTenWhereClause;
		//alert('whereClause==>'+whereClause);
	}
</script>    
    
</head>

<form method="POST" action="getPlanDocsTopPage">
<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
				<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
				<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
				<input type="submit" value="Show Latest" name="B5" title="View top 10 requests" onclick="setWhereClause('B5');">
			</td>
			<td bgcolor="#66FFCC">
				<p>
					&nbsp;Field:
					<select size="1" id="field_name" name="field_name">
						<option value="PLAN_ID" selected="">Plan ID</option>
						<option value="HIOS_PRODUCT_ID" >HIOS Product ID</option>
						<option value="ISSUER_PLAN_NUMBER">Issuer Plan Number</option>
						<option value="DOCUMENT_TYPE">Document Type</option>
						<option value="DOC_NAME_NEW">New Document Name</option>
						<option value="DOC_NAME_OLD">Old Document Name</option>
						<!-- <option value="ERROR_MSG">ERROR_MSG</option> -->						
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
					Status:
					<select size="1" id="status" name="status">
					<option value="A" selected="">All</option>
					<option value="COMPLETED" >Completed</option>
					<option value="FAILED">Failed</option>
				</select>
				Last Updated Date:
				<input type="text" id="date1" name="date1" size="10" maxlength="10">
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
	<input id="topTenSelectClause" name="topTenSelectClause" type="hidden">
	<input id="topTenWhereClause" name="topTenWhereClause" type="hidden">
</form>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		if(isPostgresDB) {
			whereClause = "last_updated >= now()- interval '2 day'";
		} else {
		whereClause = "last_updated >= sysdate-2";
	}
	}
	String topTenSelectClause = request.getParameter("topTenSelectClause");
	if( topTenSelectClause == null || topTenSelectClause == ""){
		topTenSelectClause="";
		
	}
	
	String topTenWhereClause = request.getParameter("topTenWhereClause");
	if( topTenWhereClause == null || topTenWhereClause == ""){
		topTenWhereClause="";
		
	}
%>

<sql:query dataSource="${jspDataSource}" var="documentResults">
<%=topTenSelectClause %> SELECT ID,
CASE
WHEN (DOCUMENT_TYPE='A') THEN 'N/A'
ELSE PLAN_ID
END AS PLAN_ID,
CASE
WHEN (DOCUMENT_TYPE='A') THEN 'N/A'
ELSE HIOS_PRODUCT_ID
END AS HIOS_PRODUCT_ID,
CASE
WHEN (DOCUMENT_TYPE='A') THEN 'N/A'
ELSE ISSUER_PLAN_NUMBER
END AS ISSUER_PLAN_NUMBER,
<%if(isPostgresDB) {%>
'<a href="getPlanDocsBottomLeft?attachment='||UCM_ID_NEW||chr(38)||'type='||substr(doc_name_new,position('.' in doc_name_new)+1,length(doc_name_new)) ||'" target="oldDocs_1">New('||DOC_NAME_NEW||')</a>' AS NEW_DOC,
<%} else {%>
'<a href="getPlanDocsBottomLeft?attachment='||UCM_ID_NEW||chr(38)||'type='||substr(doc_name_new,INSTR(doc_name_new,'.')+1,length(doc_name_new)) ||'" target="oldDocs_1">New('||DOC_NAME_NEW||')</a>' AS NEW_DOC,
<%}%>
UCM_ID_NEW,
CASE
WHEN (DOCUMENT_TYPE='A') THEN 'N/A'
ELSE 
<%if(isPostgresDB) {%>
'<a href="getPlanDocsBottomRight?attachment='||UCM_ID_OLD||chr(38)||'type='||substr(doc_name_new,position('.' in doc_name_new)+1,length(doc_name_new)) ||'" target="newDocs_1">Old('||DOC_NAME_OLD||')</a>'
<%} else {%>
'<a href="getPlanDocsBottomRight?attachment='||UCM_ID_OLD||chr(38)||'type='||substr(doc_name_new,INSTR(doc_name_new,'.')+1,length(doc_name_new)) ||'" target="newDocs_1">Old('||DOC_NAME_OLD||')</a>'
<%}%>
END AS OLD_DOC,
STATUS, ERROR_MSG, 
CASE
WHEN (UPPER(DOCUMENT_TYPE)='B') THEN 'Brochure'
WHEN (UPPER(DOCUMENT_TYPE)='S') THEN 'SBC'
WHEN (UPPER(DOCUMENT_TYPE)='A') THEN 'Ancillary'
WHEN (UPPER(DOCUMENT_TYPE)='E') THEN 'EOC'
END AS DOC_TYPE,
TO_CHAR(LAST_UPDATED,'DD-MON-YY HH24:MI:SS') AS LAST_UPDATED_DATE
FROM SERFF_PLAN_DOCUMENTS_JOB
WHERE <%=whereClause%>
ORDER BY ID DESC <%=topTenWhereClause %>
</sql:query>

Requests Received by SERFF Batch
	<% 
		if(request.getParameter("filterStatement") != null){
			out.println(request.getParameter("filterStatement"));
		}
	%>
<br>

<display:table name="${documentResults.rows}" requestURI="/admin/getPlanDocsTopPage" id="table1" style="white-space: pre-wrap;" export="true" pagesize="10">
	<display:column property="ID" title="ID" sortable="false" />
	<display:column property="PLAN_ID" title="Plan ID" sortable="false" />
	<display:column property="HIOS_PRODUCT_ID" title="HIOS Product ID" sortable="false" />
	<display:column property="ISSUER_PLAN_NUMBER" title="Issuer Plan Number" sortable="false" />
	<display:column property="NEW_DOC" title="New Document" sortable="false" />
	<display:column property="UCM_ID_NEW" title="New Document ID" sortable="false" />
	<display:column property="OLD_DOC" title="Old Document" sortable="false" />
	<display:column property="STATUS" title="Status" sortable="false" />
	<display:column property="ERROR_MSG" title="Error Msg" sortable="false" />
	<display:column property="DOC_TYPE" title="Doc Type" sortable="false" />
	<display:column property="LAST_UPDATED_DATE" title="Last Updated" sortable="false" />

	<display:setProperty name="export.xml.filename" value="serffPlanDocs.xml"/>
	<display:setProperty name="export.csv.filename" value="serffPlanDocs.csv"/>
	<display:setProperty name="export.excel.filename" value="serffPlanDocs.xls"/>
</display:table>
