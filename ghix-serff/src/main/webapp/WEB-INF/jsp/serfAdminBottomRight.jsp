<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ page import = "java.io.OutputStream" %>
<%@ page import="com.getinsured.hix.platform.ecm.*"%>
<%@ page import="oracle.sql.CLOB"%>
<%@include file="datasource.jsp" %>

<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">

</head>

<body>
Content of Request / Response, Attachments Content
<br>
<%
ContentManagementService ecmService = (ContentManagementService) ctx.getBean("ecmService");

Map<String, String[]> parameters = request.getParameterMap();
try{

Enumeration parameterList = request.getParameterNames();
  while( parameterList.hasMoreElements() )
  {
    String sName = parameterList.nextElement().toString();
    if("attachment".equalsIgnoreCase(sName))
    {
	    out.println( "Attachment : "+ sName + " = " + request.getParameter( sName ) + "<br>" );
	    byte[] attachment = ecmService.getContentDataById(request.getParameter( sName ));
	    String xmlDoc = new String(attachment);
	    if("xml".equalsIgnoreCase(request.getParameter("type")))
	    {%>
	    <textarea name="spec" rows="10" cols="72"><c:out value="<%=xmlDoc%>"/></textarea>
	    <%
	    }
	    else
	    {
	    
	    response.reset();
	    out.clear();
	    response.setHeader("Content-Disposition", ("inline"));
	    OutputStream o = response.getOutputStream();
	    o.write(attachment); 
	    o.flush(); 
	    o.close(); 
	    }
    }else
    if("request".equalsIgnoreCase(sName))
        {
        out.println("Request for ID = " + request.getParameter( sName ) + "<br>" );
        %>
        <sql:transaction dataSource="${jspDataSource}">
	<sql:query var="specsql" >
	select request_xml from serff_plan_mgmt where serff_operation='TRANSFER_PLAN' and serff_req_id = <%=request.getParameter("request")%>
	</sql:query>
	<c:set value="${specsql.rows[0].request_xml}" var="spec" scope="request"/>
	<%
					String spec = null;
					if (isPostgresDB) {
						spec = request.getAttribute("spec").toString();
					}
					else {
	// ugly hack, not my fault.
	oracle.sql.CLOB clob=(oracle.sql.CLOB)request.getAttribute("spec");
	long size=clob.length();
						spec = clob.getSubString(1, (int) size);
					}
					request.setAttribute("requestXML", spec);
	%>
	</sql:transaction>
				<textarea name="spec" rows="10" cols="72"><c:out value="${requestXML}"/></textarea>
        <%
        
    }else
    if("response".equalsIgnoreCase(sName))
    {
        out.println( "Response for ID = " + request.getParameter( sName ) + "<br>" );
        %>
         <sql:transaction dataSource="${jspDataSource}">
		<sql:query var="specsql" >
		select response_xml from serff_plan_mgmt where serff_operation='TRANSFER_PLAN' and serff_req_id = <%=request.getParameter("response")%>
		</sql:query>
		<c:set value="${specsql.rows[0].response_xml}" var="spec" scope="request"/>
		<%
					String spec = null;
					if (isPostgresDB) {
						spec = request.getAttribute("spec").toString();
					}
					else {
		// ugly hack, not my fault.
		oracle.sql.CLOB clob=(oracle.sql.CLOB)request.getAttribute("spec");
		long size=clob.length();
						spec = clob.getSubString(1, (int) size);
					}
					request.setAttribute("responseXML", spec);
		%>
		</sql:transaction>
				<textarea name="spec" rows="10" cols="72"><c:out value="${responseXML}"/></textarea>
        <%
        
    }else
    if("pmresponse".equalsIgnoreCase(sName))
    {
        out.println( "Plan Management Response for ID = " + request.getParameter( sName ) + "<br>" );
        %>
         <sql:transaction dataSource="${jspDataSource}">
		<sql:query var="specsql" >
		select pm_response_xml from serff_plan_mgmt where serff_operation='TRANSFER_PLAN' and serff_req_id = <%=request.getParameter("pmresponse")%>
		</sql:query>
		<c:set value="${specsql.rows[0].pm_response_xml}" var="spec" scope="request"/>
		<%
					String spec = null;
					if (isPostgresDB) {
						spec = request.getAttribute("spec").toString();
					}
					else {
		// ugly hack, not my fault.
		oracle.sql.CLOB clob=(oracle.sql.CLOB)request.getAttribute("spec");
		long size=clob.length();
						spec = clob.getSubString(1, (int) size);
					}
					request.setAttribute("pmResponseXML", spec);
		%>
		</sql:transaction>
				<textarea name="spec" rows="10" cols="72"><c:out value="${pmResponseXML}"/></textarea>
        <%
        
    }
    else
    {
    //out.println( "Not handled "+sName + " = " + request.getParameter( sName ) + "<br>" );
    }
  }

}catch(Exception ex)
{
}finally
{
}
%>

</body>

</html>
