<html>
<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>
<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8">

<head>
<title>Issuer Certification Status</title>

<script type="text/javascript">

	function setWhereClause(type){
		
		var whereClause = '';
		var filterStatement = '';
		
		switch (type) {
			case 'B1' : 
				var fieldName = document.getElementById('field_name').value;
				var fieldValue = document.getElementById('field_value').value;
				
				if(fieldValue != null && fieldValue.length > 0)
				{
				whereClause = " lower("+fieldName + ") like lower('%" + fieldValue + "%')";
				filterStatement = ' with ' + fieldName + " = " + fieldValue;
				}
				if(!whereClause){
					whereClause = "hios_issuer_id is not null";
				}
				
			
			break;
		}
		
		document.getElementById('whereClause').value = whereClause;
		document.getElementById('filterStatement').value = filterStatement;
	}
	
</script>

</head>

<body>


<form method="POST" action="issuerStatus">
<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			
			<td bgcolor="#66FFCC">
				<p>
					&nbsp;Field:
					<select size="1" id="field_name" name="field_name">
						
						<option value="SHORT_NAME">Issuer Name</option>
						<option value="HIOS_ISSUER_ID">Hios Issuer Id</option>
						<option value="CERTIFICATION_STATUS">Certification Status</option>
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
					<input type="submit" value="Search" name="B1" title="Filter requests based on field value" onclick="setWhereClause('B1');">
			
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
	
</form>
<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "hios_issuer_id is not null";
		
	}

%>
<sql:query dataSource="${jspDataSource}" var="result1">
select HIOS_ISSUER_ID,FEDERAL_EIN,MARKETING_NAME,SHORT_NAME,CERTIFICATION_STATUS,ISSUER_ACCREDITATION,LICENSE_NUMBER,LICENSE_STATUS, STATE,SHOP_SITE_URL,COMPANY_LOGO
from issuers where  <%=whereClause%>
</sql:query> 
<b>Issuer Status information.</b>
<br>

<display:table name="${result1.rows}"  requestURI="issuerStatus" id="table1" export="true" pagesize="100" style="width:80%">
	<display:column property="hios_issuer_id" title="Hios Issuer Id " sortable="true"/>
	<display:column property="FEDERAL_EIN" title="Fedreal EIN" sortable="true"/>
	<display:column property="MARKETING_NAME" title="Marketing Name " sortable="true"/>
	<display:column property="SHORT_NAME" title="Short Name" sortable="true"/>
	<display:column property="certification_status" title="Certification Status" sortable="true"/>
	<display:column property="ISSUER_ACCREDITATION" title="Issuer Accreditation" sortable="true"/>
	<display:column property="LICENSE_NUMBER" title="License Number" sortable="true"/>
	<display:column property="LICENSE_STATUS" title="License Status" sortable="true"/>
	<display:column property="STATE" title="State" sortable="true"/>
	<display:column property="SHOP_SITE_URL" title="Shop site Url" sortable="true"/>
	<display:column property="COMPANY_LOGO" title="Company Logo" sortable="true"/>
	<display:column property="state_of_domicile" title="State of Domicile" sortable="true"/>
	
	<display:setProperty name="export.xml.filename" value="issuerStatus.xml"/>
	<display:setProperty name="export.csv.filename" value="issuerStatus.csv"/>
	<display:setProperty name="export.excel.filename" value="issuerStatus.xls"/>
</display:table>

 </body> 
 </html>


