<%
session = request.getSession(false);
if(null == session)
{
	/* RequestDispatcher rd = request.getRequestDispatcher("index.jsp");     
	rd.forward(request, response); */
	response.sendRedirect("index.jsp");
}
else
{
	String loggedin = (String)session.getAttribute( "loggedin");
	if(!"true".equals(loggedin))
	{		
		session.invalidate();	
		response.sendRedirect("../index.jsp");
	}
}
%>