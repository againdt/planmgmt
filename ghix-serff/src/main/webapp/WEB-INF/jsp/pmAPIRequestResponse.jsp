<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<!-- JQuery Start -->
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/multiselect/jquery.multiselect.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/multiselect/jquery.multiselect.filter.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/multiselect/style.css'/>" />

<link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>

<script type="text/javascript" src="<c:url value='/resources/js//multiselect/jquery.multiselect.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/js//multiselect/jquery.multiselect.filter.js'/>"></script>
<!-- JQuery End -->

<link type="text/css" rel="stylesheet" href="../resources/css/json_tool.css">
<script type="text/javascript" charset="utf-8" src="../resources/js/json_tool.js"></script>
<script type="text/javascript" charset="utf-8">

$(function() {
	$("#planMgmtApi").multiselect({
		multiple: false,
		selectedList: 1
	}).multiselectfilter();
});

document.onreadystatechange = function(){ 
	if(document.readyState=="complete")
	{ 
		//Set Host URL.
		if(window.location.protocol == 'https:'){
			document.getElementById('targetURL').value = 'https://' + window.location.hostname;
		}else if(window.location.protocol == 'http:'){
			if(window.location.hostname == 'localhost'){
				document.getElementById('targetURL').value = window.location.origin;
			}else{
				document.getElementById('targetURL').value = 'http://' + window.location.hostname;
			}
		}
	} 
} 
	
var xmlHttp;
	
	function getValidStates() {
		//Create xmlHTTP object.
		createXmlHTTPObject();
		var url = "getValidStates";
		xmlHttp.onreadystatechange = populateStateList;
		xmlHttp.open("GET", url, true);
		xmlHttp.send(null);			
     }

	function createXmlHTTPObject() {
		if (typeof XMLHttpRequest != "undefined") {
			xmlHttp = new XMLHttpRequest();
		} else if (window.ActiveXObject) {
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		if (xmlHttp == null) {
			alert("Browser does not support XMLHTTP Request")
			return;
		}
	}

	function getSelectedText(elementId) {
		var elt = document.getElementById(elementId);

		if (elt.selectedIndex == -1)
			return null;

		return elt.options[elt.selectedIndex].text;
	}

	function getJSONRequestText() {
		document.getElementById("inputJSON").value = "";
		var selectedApiURL = document.getElementById("planMgmtApi").value;
		//admin/getIssuerD2CInfo
		if(selectedApiURL == 'NA') {
			alert("This API doesn't need any input. Please use 'Post Request' button to call API.");
			return;
		}
		//Create xmlHTTP object.
		createXmlHTTPObject();
			var state = document.getElementById("state").value;
			var url = "../"
				+ document.getElementById("planMgmtApi").value;
			var firstParamSuffix="?";
			var genericParamSuffix="&";
			var isFirstParam=false;
			//Add parameters.
			var e = document.getElementById("state");
			var state = e.options[e.selectedIndex].text;
			if(state != 'Any'){
				if(isFirstParam == false){
					isFirstParam=true;
					url += firstParamSuffix + "state=" + e.options[e.selectedIndex].value;
				}else{
					isFirstParam=true;
					url += firstParamSuffix + "state=" + e.options[e.selectedIndex].value;
				}
			}
			
			e = document.getElementById("exchangeType");
			var exchangeType = e.options[e.selectedIndex].text;
			if(exchangeType != 'Any'){
				if(isFirstParam == false){
					isFirstParam=true;
					url += firstParamSuffix + "exchangeType=" + e.options[e.selectedIndex].value;
				}else{
					url += genericParamSuffix + "exchangeType=" + e.options[e.selectedIndex].value;
				}
			}
			
			e = document.getElementById("planType");
			var planType = e.options[e.selectedIndex].text;
			
			if ((document.getElementById("planMgmtApi").value == 'admin/getBenefitsAndCostJSONRequest')
					&& (planType == 'Any' || planType == 'STM' || planType == 'AME')){
				alert("Please select HEALTH or DENTAL plan type for 'getBenefitsAndCost' API.");
				return;
			}
			
			if (planType != 'Any') {
				
				if ((document.getElementById("planMgmtApi").value == 'admin/getBenefitCostDataJSONRequest')
						&& (planType == 'STM' || planType == 'AME')){
					alert("Please select HEALTH or DENTAL plan type for 'getBenefitCostData' API.");
					return;
				}
				
				if(isFirstParam == false){
					isFirstParam=true;
					url += firstParamSuffix + "planType=" + e.options[e.selectedIndex].value;
				}else{
					url += genericParamSuffix + "planType=" + e.options[e.selectedIndex].value;
				}
			}
			
			e = document.getElementById("planLevel");
			var planLevel = e.options[e.selectedIndex].text;
			if(planLevel != 'Any'){
				if(isFirstParam == false){
					isFirstParam=true;
					url += firstParamSuffix + "planLevel=" + e.options[e.selectedIndex].value;
				}else{
					url += genericParamSuffix + "planLevel=" + e.options[e.selectedIndex].value;
				}
			}
			
			e = document.getElementById("planYear");
			var planYear = e.options[e.selectedIndex].text;
			if(planYear != 'Any'){
				if(isFirstParam == false){
					isFirstParam=true;
					url += firstParamSuffix + "planYear=" + e.options[e.selectedIndex].value;
				}else{
					url += genericParamSuffix + "planYear=" + e.options[e.selectedIndex].value;
				}
			}
			
			
			e = document.getElementById("market");
			var market = e.options[e.selectedIndex].text;
			if(market != 'Any'){
				//SHOP is mandatory for getRefPlan API.
				if(market == 'Individual' &&  (document.getElementById("planMgmtApi").value == 'admin/getRefplan')){
					alert("Please select market as 'SHOP' for 'getRefplan' API.");
					return;
				}
				
				if(isFirstParam == false){
					isFirstParam=true;
					url += firstParamSuffix + "market=" + e.options[e.selectedIndex].value;
				}else{
					url += genericParamSuffix + "market=" + e.options[e.selectedIndex].value;
				}
			}else if(market == 'Any'){
				//For getPlanRef API market type must be SHOP.
				var refPlanApi = document.getElementById("planMgmtApi").value;
				if(refPlanApi == 'admin/getRefplan'){
					if(isFirstParam == false){
						isFirstParam=true;
						url += firstParamSuffix + "market=SHOP";
					}else{
						url += genericParamSuffix + "market=SHOP";
					}
				}
			}

			e = document.getElementById("networkType");
			var networkType = e.options[e.selectedIndex].text;
			if(networkType != 'Any'){
				if(isFirstParam == false){
					isFirstParam=true;
					url += firstParamSuffix + "networkType=" + e.options[e.selectedIndex].value;
				}else{
					url += genericParamSuffix + "networkType=" + e.options[e.selectedIndex].value;
				}
			}
			
			e = document.getElementById("healthSavingsAccount");
			var healthSavingsAccount = e.options[e.selectedIndex].text;
			if(healthSavingsAccount != 'Any'){
				if(isFirstParam == false){
					isFirstParam=true;
					url += firstParamSuffix + "healthSavingsAccount=" + e.options[e.selectedIndex].value;
				}else{
					url += genericParamSuffix + "healthSavingsAccount=" + e.options[e.selectedIndex].value;
				}
			}
		
			e = document.getElementById("availableFor");
			var availableFor = e.options[e.selectedIndex].text;
			if(availableFor != 'Any'){
				if(isFirstParam == false){
					isFirstParam=true;
					url += firstParamSuffix + "availableFor=" + e.options[e.selectedIndex].value;
				}else{
					url += genericParamSuffix + "availableFor=" + e.options[e.selectedIndex].value;
				}
			}
			//alert("URL created : " + url);
			//Set JSON text.
			document.getElementById("inputJSON").value = "Fetching valid plan. Please wait.";
			xmlHttp.onreadystatechange = displayJSONRequest;
			xmlHttp.open("GET", url, true);
			xmlHttp.send(null);

	}
	
	
	function postPMAPIRequest(){
		
		//Create xmlHTTP object.
		createXmlHTTPObject();
		
		var url = "postPMAPIRequest";
		var payLoad = document.getElementById("inputJSON").value;
		var targetURL = document.getElementById("targetURL").value;
		var apiName = getSelectedText("planMgmtApi");
				
		if (null == targetURL || targetURL.length == 0) {
			alert("Please enter correct URL.");
			return;
		} else if (null == apiName || apiName.length == 0) {
			alert("Please select correct API to call.");
        return;
    } else if (null != payLoad && payLoad == "Fetching valid plan. Please wait.") {
        alert("Can not post JSON request. Please try again.");
			return;
		} else if (null == payLoad || payLoad.length == 0) {
			var selectedApiURL = document.getElementById("planMgmtApi").value;
			if(selectedApiURL != 'NA'){
				alert("Please check JSON request. Can not post empty JSON request.");
				return;
			}else{
				document.getElementById("inputJSON").value = " ";
				payLoad = document.getElementById("inputJSON").value;
			}			
		} else {
			url += "?host=" + targetURL +"&apiName=" + apiName +"&payLoad=" + payLoad;
			xmlHttp.onreadystatechange = processAPIResponse;
			xmlHttp.open("GET", url, true);
			xmlHttp.setRequestHeader("Content-type", "application/json")
			xmlHttp.send(null);
		}
		
	}
	
	function displayJSONRequest() {
		if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
			if(null == xmlHttp.responseText || xmlHttp.responseText.length == 0){
				document.getElementById("inputJSON").value = "No plans found for selected request."
			}else{
				document.getElementById("inputJSON").value = xmlHttp.responseText;
			}
		}
	}
	
	function processAPIResponse() {
		if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
			//alert(xmlHttp.responseText);
			var json = JSON.parse(xmlHttp.responseText);
			document.getElementById("duration").value = json.duration;
			document.getElementById("size").value = json.size;
			document.getElementById("apiURL").value = json.url;
			document.getElementById("rawJson").value = json.jsonResponse;
			document.getElementById("method").value = json.method;
		}
	}
		
	function populateStateList() {
		if (xmlHttp.readyState == 4 || xmlHttp.readyState == "complete") {
			var json = JSON.parse(xmlHttp.responseText);
			var select = document.getElementById("state");
			var defaultOption = document.createElement('option');
			defaultOption.value = "";
			defaultOption.text = "Any";	
			select.add(defaultOption, 0);
			var index = 1;
			for(var propName in json) {
			    var value = json[propName];
			    var option = document.createElement('option');
			    option.value = propName;
			    option.text = value+"["+propName+"]";			 
			    select.add(option, index);
			    index++;
			}
		}
	}
	
	function displayValidFields(){
		displayAllFields();
		var apiSelected = document.getElementById("planMgmtApi").value;
		//Hide fields as per API selected.
		
		if (apiSelected == "NA") {
			notAvailable();
			document.getElementById('Generate').disabled=true;
			document.getElementById("inputJSON").value = "This API doesn't need any input. Please use 'Post Request' button to call API.";
		}
		else {
			document.getElementById('Generate').disabled=false;
		}
		
		if(apiSelected == "admin/getEmployerPlanRateJSONRequest"){
			displayGetemployerPlanRateFields();
		}
		
		if(apiSelected == "admin/getCheckIndvHealthPlanAvailabilityJSONRequest"){
			displayGetCheckIndvHealthPlanAvailabilityFields();
		}
		
		if(apiSelected == "admin/getDentalPlansJSONRequest"){
			displayGetDentalPlanFields();
		}
		
		if(apiSelected == "admin/getLowestDentalPremiumJSONRequest"){
			displayGetLowestDentalPremiumFields();
		}
		
		if(apiSelected == "admin/getEmployerMinMaxRateJSONRequest"){
			displayGetEmployerMinMaxRateFields();
		}
		
		if(apiSelected == "admin/getStmplanJSONRequest"){
			displaySTMPlanFields();
		}
		
		if(apiSelected == "admin/getFindLowestPremiumPlanRateJSONRequest"){
			displayFindLowestPremiumPlanRateFields();
		}
		
		if(apiSelected == "admin/getHouseholdJSONRequest"){
			displayHouseholdFields();
		}
		
		if(apiSelected == "admin/getEligiblityJSONRequest"){
			displayEligiblityFields();
		}
		
		if(apiSelected == "admin/getMemberLevelEmployeeRateJSONRequest"){
			displayMemberLevelEmployeeRateFields();
		}
		
		if(apiSelected == "admin/getBenchmarkPlanRequestXML" || apiSelected == "admin/getBenchmarkRateJSONRequest" ){
			displayBenchmarkPlanRequestFields();
		}
		
		if(apiSelected == "admin/getBenefitsAndCostJSONRequest"){
			displayBenefitsAndCostFields();
		}
		
		if(apiSelected == "admin/checkMetalTierAvailabilityJSONRequest"){
			displayCheckMetalTierAvailableFields();
		}
		
		if(apiSelected == "admin/getlowestamepremiumJSONRequest"
				|| apiSelected == "admin/getameplansJSONRequest" || apiSelected == "admin/getAllAMEplansJSONRequest" ){
			displayAncillaryPlanFields();
		}
		
		if(apiSelected == "admin/getVisionPlansJSONRequest"){
			displayAncillaryPlanFields();
		}
		
		if(apiSelected == "admin/getMedicaidPlansJSONRequest"){
			displayAncillaryPlanFields();
		}
	}
	
	function displayAllFields(){
		//Reset All fields to visible.
		document.getElementById("stateDiv").style.display = 'inline';
		document.getElementById("exchangeTypeDiv").style.display = "inline";
		document.getElementById("planTypeDiv").style.display = "inline";
		document.getElementById("planLevelDiv").style.display = "inline";
		document.getElementById("marketDiv").style.display = "inline";
		document.getElementById("networkTypeDiv").style.display = "inline";
		document.getElementById("healthSavingsAccountDiv").style.display = "inline";
		document.getElementById("availableForDiv").style.display = "inline";
		document.getElementById("planYearDiv").style.display = "inline";
	}
	
	function notAvailable(){
		document.getElementById("stateDiv").style.display = 'none';
		document.getElementById("exchangeTypeDiv").style.display = 'none';
		document.getElementById("planTypeDiv").style.display = 'none';
		document.getElementById("planLevelDiv").style.display = 'none';
		document.getElementById("marketDiv").style.display = 'none';
		document.getElementById("networkTypeDiv").style.display = 'none';
		document.getElementById("healthSavingsAccountDiv").style.display = 'none';
		document.getElementById("availableForDiv").style.display = 'none';
		document.getElementById("planYearDiv").style.display = 'none';
	}
	
	function displayMemberLevelEmployeeRateFields(){
		document.getElementById("planTypeDiv").style.display = 'none'; 
		document.getElementById("marketDiv").style.display = 'none';
	}
	
	function displayEligiblityFields(){
		document.getElementById("planTypeDiv").style.display = 'none'; 
	}
	
	function displayHouseholdFields(){
		document.getElementById("planTypeDiv").style.display = 'none'; 
	}
	
	
	function displayFindLowestPremiumPlanRateFields(){
		document.getElementById("planTypeDiv").style.display = 'none'; 
	}
	
	function displaySTMPlanFields(){
		document.getElementById("planTypeDiv").style.display = 'none'; 
	}
	
	function displayGetemployerPlanRateFields(){
		document.getElementById("availableForDiv").style.display = 'none'; 
		document.getElementById("planTypeDiv").style.display = 'none';
	}
	
	function displayGetCheckIndvHealthPlanAvailabilityFields(){
		document.getElementById("planTypeDiv").style.display = 'none'; 
		document.getElementById("marketDiv").style.display = 'none';
	}
	
	function displayGetDentalPlanFields(){
		document.getElementById("planTypeDiv").style.display = 'none'; 
	}
	
	function displayGetLowestDentalPremiumFields(){
		document.getElementById("planTypeDiv").style.display = 'none'; 
	}
	
	function displayGetEmployerMinMaxRateFields(){
		document.getElementById("planTypeDiv").style.display = 'none'; 
		document.getElementById("marketDiv").style.display = 'none'; 
	}
	
	function displayBenchmarkPlanRequestFields(){
		document.getElementById("planTypeDiv").style.display = 'none'; 
		document.getElementById("marketDiv").style.display = 'none'; 
		document.getElementById("planLevelDiv").style.display = 'none'; 
	}
	
	function displayBenefitsAndCostFields(){
		document.getElementById("exchangeTypeDiv").style.display = 'none';
		document.getElementById("planLevelDiv").style.display = 'none';
		document.getElementById("marketDiv").style.display = 'none';
		document.getElementById("networkTypeDiv").style.display = 'none';
		document.getElementById("healthSavingsAccountDiv").style.display = 'none';
		document.getElementById("availableForDiv").style.display = 'none';
	}
	
	function displayCheckMetalTierAvailableFields(){
		document.getElementById("exchangeTypeDiv").style.display = 'none';
		document.getElementById("planTypeDiv").style.display = 'none';
		document.getElementById("planLevelDiv").style.display = 'none';
		document.getElementById("marketDiv").style.display = 'none';
		document.getElementById("networkTypeDiv").style.display = 'none';
		document.getElementById("healthSavingsAccountDiv").style.display = 'none';
		document.getElementById("availableForDiv").style.display = 'none';
	}
	
	function displayAncillaryPlanFields(){
		document.getElementById("exchangeTypeDiv").style.display = 'none';
		document.getElementById("planTypeDiv").style.display = 'none';
		document.getElementById("planLevelDiv").style.display = 'none';
		document.getElementById("marketDiv").style.display = 'none';
		document.getElementById("networkTypeDiv").style.display = 'none';
		document.getElementById("healthSavingsAccountDiv").style.display = 'none';
		document.getElementById("availableForDiv").style.display = 'none';
		document.getElementById("planYearDiv").style.display = 'none';
	}
	
</script>

<style>
#panel,#flip {
	padding: 5px;
	text-align: left;
	background-color: #e5eecc;
	border: solid 1px #c3c3c3;
}

#panel {
	padding: 5px;
	display: none;
}
</style>


<title>Plan Management API Tester</title>
</head>

<body onload="getValidStates();">
<table border="0" cellpadding="5" cellspacing="0" width="100%">
  <tr>
    <td width="100%" valign="top" align="left" colspan="4">

 <b><font face="Arial" size="4">
Plan Management API Tester</font></b>
    </td>
  </tr>
  
  <tr>
    <td width="100%" bgcolor="#D8FDD0" valign="top" align="left" colspan="4">

		<form method="POST" action="--WEBBOT-SELF--">
        	<label for="planMgmtApi"><%=request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() %>/ghix-planmgmt/</label>
        	<select onchange="displayValidFields()" id="planMgmtApi" name="planMgmtApi" style="width:420px">
        		<optgroup label="Crosssell-API">
		          	<option value="admin/getDentalPlansJSONRequest">crosssell/getdentalplans</option>
					<option value="admin/getLowestDentalPremiumJSONRequest">crosssell/getlowestdentalpremium</option>
					<option value="admin/getlowestamepremiumJSONRequest">crosssell/getlowestamepremium</option>
					<option value="admin/getameplansJSONRequest">crosssell/getameplans</option>
				</optgroup>
				<optgroup label="Issuer-API">
					<option value="admin/getIssuerInfobyIdJSONRequest">issuerInfo/byId</option>
					<option value="admin/getIssuerInfoByZipcodeJSONRequest">issuerInfo/byzipcode</option>s
					<option value="admin/getDisclaimerInfoJSONRequest">issuerInfo/disclaimerInfo</option>
					<option value="NA">issuerInfo/getIssuerD2CInfo</option>
					<option value="admin/getDisclaimerInfoJSONRequest">issuerInfo/paymentInfoByID</option>
				</optgroup>
				<optgroup label="Plan-API">
					<option value="admin/getCheckPlanAvailabilityForEmployeesJSONRequest">plan/checkPlanAvailabilityForEmployees</option>
					<option value="admin/getCheckIndvHealthPlanAvailabilityJSONRequest">plan/checkindvhealthplanavailability</option>
					<option value="admin/getPlanIssuerInfoJSONRequest">plan/getPlanIssuerInfo</option>
					<option value="admin/getBenefitCostDataJSONRequest">plan/getbenefitcostdata</option>
					<option value="admin/getPlansDetailsJSONRequest">plan/getdetails</option>
					<option value="admin/getPlanInfoJSONRequest">plan/getinfo</option>
					<option value="admin/getPlanInfoByZipJSONRequest">plan/getplaninfobyzip</option>
					<option value="admin/getPlansByIssuerJSONRequest">plan/getplansbyissuer</option>
					<option value="admin/getPlansForUniversalCartJSONRequest">plan/getPlansForUniversalCart</option>
					<option value="admin/getPlansDataJSONRequest">plan/getplansdata</option>
					<option value="admin/getRefplanJSONRequest">plan/getrefplan</option>
					<option value="admin/getProviderNetworkJSONRequest">plan/getprovidernetwork</option>
					<option value="NA">plan/updateEnrollmentAvaility</option>
					<option value="admin/checkMetalTierAvailabilityJSONRequest">plan/checkMetalTierAvailability</option>
					<option value="NA">plan/updatestatus</option>
				</optgroup>
				<optgroup label="Rate Benefit-API">
					<option value="admin/getFindLowestPremiumPlanRateJSONRequest">planratebenefit/findLowestPremiumPlanRate</option>
					<option value="admin/getEmployerMinMaxRateJSONRequest">planratebenefit/getemployerminmaxrate</option>
					<option value="admin/getEmployerPlanRateJSONRequest">planratebenefit/getemployerplanrate</option>
					<option value="admin/getHouseholdJSONRequest">planratebenefit/household</option>
					<option value="admin/getMemberLevelEmployeeRateJSONRequest">planratebenefit/memberlevelemployeerate</option>
					<option value="admin/getMemberLevelPremiumJSONRequest">planratebenefit/memberlevelpremium</option>
					<option value="admin/getBenchmarkRateJSONRequest">planratebenefit/rate/benchmark</option>
					<option value="admin/getBenefitsAndCostByPlanIdJSONRequest">planratebenefit/getBenefitsAndCostByPlanId</option>
					<option value="admin/getBenefitsAndCostJSONRequest">planratebenefit/getBenefitsAndCost</option>
				</optgroup>
				<optgroup label="Ancillary-API">
					<option value="admin/getStmplanJSONRequest">stmplanratebenefit/stmplan</option>
					<option value="admin/getAllAMEplansJSONRequest">ameplan/getameplans</option>
					<option value="admin/getVisionPlansJSONRequest">visionplan/getvisionplans</option>
				</optgroup>
				<optgroup label="Other-API">
					<option value="admin/getHouseholdReqJSONRequest">premiumRestService/houseHoldReq</option>
					<option value="admin/getEligiblityJSONRequest">teaserPlanRestService/teaserPlan/eligiblityRequest</option>
					<option value="admin/getBenchmarkPlanRequestXML">planmgmt/benchMarkPlan</option>
					<option value="admin/getMedicaidPlansJSONRequest">medicaidplan/getmedicaidplans</option>
				</optgroup>
        	</select><br><br>
        	
            <font face="Arial" size="2">
        		Note: All fields are not applicable for all APIs. For more information, please refer Help Page or Request object.
        	</font><br>
         	<div id="stateDiv" style="display:inline;"><label id="stateText">Select State :</label><select size="1" id="state" name="state"></select></div>
          	     	        	     

            <div id="exchangeTypeDiv" style="display:inline;"><label for="exchangeType" id="exchangeTypeText">Exchange :</label><select size="1" name="exchangeType" id="exchangeType">
	          <option value="Any" selected>Any</option>
	          <option value="ON">On Exchange</option>
	          <option value="OFF">Off Exchange</option>	          
	        </select></div> 
	        
	        <div id="planTypeDiv" style="display:none;"><label for="planType" id="planTypeText">Plan Type :</label><select size="1" name="planType" id="planType">
	          <option selected value="Any">Any</option>
	          <option value="HEALTH">Health</option>
	          <option value="DENTAL">Dental</option>	          
	          <option value="STM">STM</option>
	          <option value="AME">AME</option>
	        </select></div> 
	        
	        <div id="planLevelDiv" style="display:inline;"><label for="planLevel" id="planLevelText">Plan Level :</label><select size="1" name="planLevel" id="planLevel">
	          <option value="Any">Any</option>
	          <option value="PLATINUM">Platinum</option>
	          <option value="CATASTROPHIC">Catastrophic</option>
	          <option value="GOLD">Gold</option>
	          <option value="SILVER">Silver</option>
	          <option value="BRONZE">Bronze</option>
	        </select></div><br>
	     	        
	        <div id="marketDiv" style="display:inline;"><label for="market" id="marketText">Market :</label><select size="1" name="market" id="market">
	          <option value="Any" selected>Any</option>
	          <option value="INDIVIDUAL">Individual</option>
	          <option value="SHOP">Shop</option>
	        </select></div> 
	        
	        <div id="networkTypeDiv" style="display:inline;"><label for="networkType" id="networkTypeText">Network Type :</label><select size="1" name="networkType" id="networkType">
	          <option value="ANY" selected>Any</option>
	          <option value="EPO">EPO</option>
	          <option value="POS">POS</option>
	          <option value="HMO">HMO</option>
	          <option value="PPO">PPO</option>
	          <option value="INDEMNITY">Indemnity</option>
	        </select></div> 
	        
	        <div id="healthSavingsAccountDiv" style="display:inline;"><label for="healthSavingsAccount" id="healthSavingsAccountText">HSA :</label><select size="1" name="healthSavingsAccount" id="healthSavingsAccount">
	          <option value="ANY">Any</option>
	          <option value="Yes">Yes</option>
	          <option value="No">No</option>
	        </select></div>
	         
	        <div id="availableForDiv" style="display:inline;"><label for="availableFor" id="availableForText">Available For :</label><select size="1" name="availableFor" id="availableFor">
	          <option value="ANY" selected>Any</option>
	          <option value="ADULTONLY">Adult Only</option>
	          <option value="CHILDONLY">Child Only</option>
	          <option value="ADULTANDCHILD">Both</option>
	          </select></div>
	          
			<div id="planYearDiv" style="display:inline;"><label for="planYear" id="planYearText">Plan Year :</label><select size="1" name="planYear" id="planYear">
	          <option value="ANY" selected>Any</option>
	          <option value="2014">2014</option>
	          <option value="2015">2015</option>	          
        	 </select></div><br>
        	 <br><input type="button" value="Generate Test Request" id="Generate" name="Generate" onclick="getJSONRequestText()"> </font>
      </form>
    </td>
  </tr>
  
  <tr>
    <td width="100%" bgcolor="#E6FFFF" colspan="4">
      <form method="POST" action="--WEBBOT-SELF--">      
        <font face="Arial" size="2">
        <label for="inputJSON"><b>Request: </b>Modify Request object as required</label><br>
        <textarea rows="5" id="inputJSON" name="inputJSON" cols="90" ></textarea>
        <br>
        <label for="targetURL">Target Host:&nbsp;</label><input type="text" name="targetURL" id="targetURL" size="50" value="" >
        <input type="button" value="Post Request" name="Post" id="postRequest" onclick="postPMAPIRequest()"></font>
      </form>
    </td>
  </tr>
  <tr>
    <td bgcolor="#C0C0C0">
    <table><tr><td>
    <font face="Arial" size="2"><b><label for="duration">Duration</label></b>:<input id="duration" type="text" readonly="readonly" size=15/>
      Milliseconds</font> </td>
    <td><font face="Arial" size="2">
      <b>Size</b>:<input id="size" type="text"  readonly="readonly" size=10/> Bytes</font> </td>
    <td><font face="Arial" size="2"><b>URL</b>:<input id="apiURL" type="text"  readonly="readonly" size=50/></a></font> </td>
    <td><font face="Arial" size="2"><b>Method</b>:
      <input id="method" size ="4" type="text"/>&nbsp;</font> 
      </td></tr></table>
      </td>
  </tr>
  </table>
  
  	<!-- Data in JSON Format-->
  
  <div id="header"> 
		<b>Response</b>
		<textarea id="rawJson" rows="1" cols="20"></textarea>
	</div>
	<div id="actionBar">
		<input id ="btnProcess" type="button" value="process" onclick="Process()">
		<input id="btnToggle" type="button" value="hide" onclick="toggleHeader()">
		<input id="btnClear" type="button" value="clear" onclick="clearJSON()">
		<div class="uiLabelLeft">tab</div>
		<select id="TabSize" onchange="TabSizeChanged()">
			<option value="1">1</option>
			<option value="2" selected="true">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			<option value="6">6</option>
		</select>
		&nbsp;
		<input id="QuoteKeys" type="checkbox" onclick="QuoteKeysClicked()" checked="true">
		<div class="uiLabelRight">""</div>
		&nbsp;
		<input id="CollapsibleView" type="checkbox" onclick="CollapsibleViewClicked()" checked="true">
		<div class="uiLabelRight">+/-</div>
		&nbsp;
		<input id="btnSelect" type="button" value="select all" onclick="SelectAllClicked()">
		<input id="btnExpand" type="button" value="expand" onclick="ExpandAllClicked()">
		<div class="uiLabelCenter">or</div>
		<input id="btnCollapse" type="button" value="collapse" onclick="CollapseAllClicked()">
		<div class="uiLabelLeft">to level:</div>
		<input id="btnCollapse2" type="button" value="2" onclick="CollapseLevel(3)">
		<input id="btnCollapse3" type="button" value="3" onclick="CollapseLevel(4)">
		<input id="btnCollapse4" type="button" value="4" onclick="CollapseLevel(5)">
		<input id="btnCollapse5" type="button" value="5" onclick="CollapseLevel(6)">
		<input id="btnCollapse5" type="button" value="6" onclick="CollapseLevel(7)">
		<input id="btnCollapse5" type="button" value="7" onclick="CollapseLevel(8)">
		<input id="btnCollapse5" type="button" value="8" onclick="CollapseLevel(9)">
	</div>
	<div id="Canvas">
		<pre class="CodeContainer"></pre>
	</div>
	
	<font face="Arial" size="1">&nbsp; <a href="http://Help.com">
	<script language="javascript">init();</script>Help</a></font>

</body>

</html>
