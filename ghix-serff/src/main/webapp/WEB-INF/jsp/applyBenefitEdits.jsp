<!DOCTYPE html>
<%@page import="java.util.HashMap"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="session.jsp" %>

<html lang="en">
<head>
<link rel="stylesheet"  type="text/css" href="../resources/css/bootstrap.css"/>
<link rel="stylesheet"  type="text/css" href="../resources/css/ghixcustom.css"/>
<script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script>
<%-- <script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../resources/js/bootstrap-filestyle.js"></script> --%>



<style>
	ul.unstyled li{
		padding:10px;
	}
</style>

</head>

<body>


<div class="row-fluid" id="main">
	<div class="container">
		<div class="row-fluid">
			<div class="gutter10">
				<div class="row-fluid">
					<h1 id="">Re Apply Plan Benefit</h1>
				</div><!-- end of .row-fluid -->
				
				<div class="row-fluid">
					<div id="sidebar" class="span3">
						<div class="header"><span>Re-apply Plan Benefit Edits</span></div>
						<ul class="unstyled">
							<li><b>Step 1: </b>Please select Plan Year</li>
							<li><b>Step 2: </b>Please select Plan eligible for Re apply benefit</li>
							
							<li class="gutter10"><b>Click Apply: </b>Proceed to Apply</li>
						</ul>	
					</div><!-- end of .span3 -->
					
					<div class="span9" id="rightpanel">
						<div class="header" style="height:40px"></div>
						<form:form id="frmPlanEdits" name="frmPlanEdits" action="${pageContext.request.contextPath}/admin/applyBenefitEdits" method="POST">
							<df:csrfToken/>
				            <div class="control-group">
				                <label for="applicableYear" class="control-label"><b>Step 1:</b> Plan Year*&nbsp;&nbsp;</label>
								<select id="applicableYear" name="applicableYear"  onchange="getValidPlans();">
									<option value="" selected="selected" >Select Plan Year</option>
									<c:forEach var="benefitYear" items="${applicableYear}">
										<c:if test="${benefitYear!= null}"> 
											<option value="${benefitYear}">${benefitYear}</option>
										</c:if> 
									</c:forEach>
								</select>
							</div>
							
							<div class="control-group">
				                <label for="planHiosID" class="control-label"><b>Step 2:</b> Plan HIOS ID*&nbsp;&nbsp;</label>
								<select  id="planHiosID" name = "planHiosID" disabled="disabled"> </select>
							</div>
							
				            <div class="row-fluid gutter10-t">
								<a href="javascript:doSubmit('frmPlanEdits', '${pageContext.request.contextPath}/admin/applyBenefitEdits');" class="btn btn-primary pull-left" type="submit" id="save">Reapply Edits</a>
							</div>
							
							<c:if test="${applyBenefitsMessage != null}">
								<br />
								<div align="left" style="font-size: 12pt;">
									<c:out value="${applyBenefitsMessage}" />
								</div>
	            			</c:if> 
						</form:form>
					</div><!-- end of .span9 -->
				</div><!-- end of .row-fluid -->
			</div> <!-- end of .gutter10 -->
		<!--row-fluid--> 
		</div>
	<!--container--> 
	</div>
<!--main-->
</div>
<script type="text/javascript">


	function doSubmit(formName, url) {
		if(isValidateForm() && formName && url){
			document.forms[formName].method = "post";
			document.forms[formName].action = url;
			document.forms[formName].submit();
		}
		
	}

	function isValidateForm() {
		
		var flag = true;
		var loForm = document.forms["frmPlanEdits"];
		
		if (loForm) {
			
			var laFields = new Array();
			var li = 0;
			
			var nodeValue = loForm["applicableYear"].value;
			if (!nodeValue || !loForm["applicableYear"].value) {
				laFields[li++] = "Please select Plan Year.";
			}
			
			nodeValue = loForm["planHiosID"].value;
			
			if (!nodeValue || nodeValue==-1) {
				laFields[li++] = "Please select valid plan Hios Id.";
			}
			
			
			if (laFields.length > 0) {
			    
				if(laFields.length > 1) {
					
					for (var fi = 0; fi < laFields.length; fi++) {
						laFields[fi] = "- " + laFields[fi];
					}
				}
		       	alert(laFields.join('\n'));
		       	flag = false;
		    }
		}
		return flag;
	}
	
	
	function getValidPlans(){	
		$('#planHiosID').empty();
		$.ajax({
			type :"POST",
			url: "<c:url value='/admin/getValidPlans'/>",
			data:{"planYearSelected": $("#applicableYear").val()},
			success: function(options){		
				plandata(options) ;
	       	}
		});
	}
	
	function plandata(options){
		var ddOptions = "<option value='-1' >Select Plan</option>";
	  
	        for ( var i = 0, len = options.length; i < len; ++i) {
	        	ddOptions += "<option value='" + options[i] + "'>"  + options[i] + "</option>";
	    }
	      
	        $('#planHiosID').attr("disabled",false).html(ddOptions);
	}
	
	
</script>



</body>
</html>