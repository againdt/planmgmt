<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>Plan Brochure Bulk Upload</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="https://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="https://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	
	<script type="text/javascript">
	$(function() {
		var fieldName = "<%=request.getParameter("field_name")%>";
	    if (fieldName && fieldName != "null") {
	    	$("#field_name").val(fieldName);
		}

		var fieldValue = "<%=request.getParameter("field_value")%>";
	    if (fieldValue && fieldValue != "null") {
	    	$("#field_value").val(fieldValue);
		}

		var status = "<%=request.getParameter("status")%>";
	    if (status && status != "null") {
	    	$("#status").val(status);
		}

		var startDate = "<%=request.getParameter("startDate")%>";
	    if (startDate && startDate != "null") {
	    	$("#startDate").val(startDate);
		}

		var endDate = "<%=request.getParameter("endDate")%>";
	    if (endDate && endDate != "null") {
	    	$("#endDate").val(endDate);
		}
			
		$("#startDate").datepicker({
			changeMonth: true,
			changeYear: true,
			onSelect: function(selected) {
				$("#endDate").datepicker("option", "minDate", selected)
			}
		}).attr("readOnly", "true");
		
		$("#endDate").datepicker({
			changeMonth: true,
			changeYear: true,
			onSelect: function(selected) {
				$("#startDate").datepicker("option", "maxDate", selected)
			}
		}).attr("readOnly", "true");
			});
	
		function setWhereClause(type){
			
			var whereClause = '';
			var filterStatement = '';
			var topTenSelectClause='';
			var topTenWhereClause='';
			
			switch (type) {
				case 'B1' : 
					<%if(isPostgresDB) {%>
					whereClause = 'start_time between (now()-interval \'1 day\') and now()';
					<%} else {%>
					whereClause = 'start_time between (sysdate-1) and sysdate';
					<%}%>
					filterStatement = ' in last 24 hours';
					break;
				case 'B2' : 
					<%if(isPostgresDB) {%>
					whereClause = 'start_time between (now()-interval \'12 hour\') and now()';
					<%} else {%>
					whereClause = 'start_time between (sysdate-1/2) and sysdate';
					<%}%>
					filterStatement = ' in last 12 hours';
					break;
				case 'B3' : 
					<%if(isPostgresDB) {%>
					whereClause = 'start_time between (now()-interval \'1 hour\') and now()';
					<%} else {%>
					whereClause = 'start_time between (sysdate-1/24) and sysdate';
					<%}%>
					filterStatement = ' in last 1 hour';
					break;
				case 'B4' : 
						var fieldName = document.getElementById('field_name').value;
						var fieldValue = document.getElementById('field_value').value;
						if(fieldValue != null && fieldValue.length > 0)
						{
							<%if(isPostgresDB) {%>
							whereClause = " lower(CAST ("+fieldName + " as TEXT)) like lower('%" + fieldValue + "%')";
							<%} else {%>
						whereClause = " lower("+fieldName + ") like lower('%" + fieldValue + "%')";
							<%}%>
						filterStatement = ' with ' + fieldName + " = " + fieldValue;
						}
						
						//Step 2: Get the Status Value
						var statuses = document.getElementById('status');
						var status = statuses.options[statuses.selectedIndex].value;
						if(status == 'S' || status == 'F')
						{
							if(whereClause){
								whereClause = whereClause  + " and request_status = '" + status + "'";
							}else{
								whereClause = whereClause  + " request_status = '" + status + "'";
							}
							
						}
						if(status == 'S'){
							
							filterStatement = filterStatement+ ' which were successful';
						}
						if(status == 'F'){
						
							filterStatement = filterStatement+ ' which have failed';
						}
						
						//Step 3: Get the Dates
						var startDate = document.getElementById('startDate').value;
						var endDate = document.getElementById('endDate').value;
						
						if(startDate && endDate)
						{	
							
							if( whereClause){
								whereClause = whereClause+"and start_time between to_timestamp('"+startDate+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"+endDate+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
								filterStatement = filterStatement + 'start date ' +  startDate + 'end date ' +  endDate;
								
							}else{
								whereClause = whereClause+" start_time between to_timestamp('"+startDate+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"+endDate+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
								filterStatement = filterStatement + 'start date ' +  startDate + 'end date ' +  endDate;
								
							}
						
						}else{
							if(startDate&& !endDate  ){
								if( whereClause){
								
								whereClause = whereClause+"and to_char(start_time  ,'mm/dd/yyyy' )='"+startDate+"'" ;
								filterStatement = filterStatement + 'start date ' +  startDate + 'end date ' +  endDate;
								}else{
									whereClause = whereClause+" to_char(start_time  ,'mm/dd/yyyy' )='"+startDate+"'" ;
									filterStatement = filterStatement + 'start date ' +  startDate + 'end date ' +  endDate;
								}
							}else{
								
								 if(!startDate && endDate ){
									 if( whereClause){
											whereClause = whereClause+"and to_char(end_time  ,'mm/dd/yyyy' )='"+endDate+"'" ;
											filterStatement = filterStatement + 'start date ' +  startDate + 'end date ' +  endDate;
									 }else{
											whereClause = whereClause+" to_char(end_time  ,'mm/dd/yyyy' )='"+endDate+"'" ;
											filterStatement = filterStatement + 'start date ' +  startDate + 'end date ' +  endDate;
										 }
									}
								}
							
							}
						
							if (!whereClause) {
								<%if(isPostgresDB) {%>
								whereClause = "start_time between (start_time) and now()";
								<%} else {%>
								whereClause = "start_time between (start_time) and sysdate";
								<%}%>
							}
					break;
				
				case 'B5' :
					topTenSelectClause=" Select * from ( ";
					<%if(isPostgresDB) {%>
						whereClause = "start_time between (start_time) and now()";
						topTenWhereClause=" ) allrec limit 10";
					<%} else {%>
						whereClause = "start_time between (start_time) and sysdate";
					topTenWhereClause=" ) where rownum in (1,2,3,4,5,6,7,8,9,10) order by rownum";
					<%}%>
					filterStatement = "for latest records";
					break;
					
			}
			document.getElementById('whereClause').value = whereClause; // escape(whereClause);
			document.getElementById('filterStatement').value = filterStatement;
			document.getElementById('topTenSelectClause').value = topTenSelectClause;
			document.getElementById('topTenWhereClause').value = topTenWhereClause;
		}
	</script>    
</head>

<form method="POST" action="${pageContext.request.contextPath}/admin/uploadPlanBrochureStatusTop">
<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
				<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
				<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
				<input type="submit" value="Show Latest" name="B5" title="View top 10 requests" onclick="setWhereClause('B5');">
			</td>
			<td bgcolor="#66FFCC">
				<p>
					&nbsp;Field:
					<select size="1" id="field_name" name="field_name">
						<option value="serff_req_id" selected="">Req id</option>
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
					Status:
					<select size="1" id="status" name="status">
					<option value="A" selected="">All</option>
					<option value="S" >Success</option>
					<option value="F">Failure</option>
				</select>
				Date Start:
				<input type="text" id="startDate" name="startDate" size="10" maxlength="10"> End:
				<input type="text" id="endDate" name="endDate" size="10" maxlength="10">
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
				<button type="reset" value="Reset">Reset</button>
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
	<input id="topTenSelectClause" name="topTenSelectClause" type="hidden">
	<input id="topTenWhereClause" name="topTenWhereClause" type="hidden">
</form>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		if(isPostgresDB) {
			whereClause = "start_time >= now()- interval '2 day'";
		} else {
		whereClause = "start_time >= sysdate-2";
		}
	}
	String topTenSelectClause = request.getParameter("topTenSelectClause");
	if( topTenSelectClause == null || topTenSelectClause == ""){
		topTenSelectClause="";
	}
	
	String topTenWhereClause = request.getParameter("topTenWhereClause");
	if( topTenWhereClause == null || topTenWhereClause == ""){
		topTenWhereClause="";
	}
%>

<sql:query dataSource="${jspDataSource}" var="result1">
<%=topTenSelectClause %>select serff_req_id,serff_operation,process_ip, 
to_char(start_time,'DD-MON-YY HH24:MI:SS') as startTime,
to_char(end_time,'DD-MON-YY HH24:MI:SS') as endTime,
CASE
WHEN (pm_response_xml is NULL) THEN 'Response'
ELSE '<a href="getSTMPlansStatusBottom?serffReqId='||serff_req_id||'" target="bottom">Response</a>'
END  as pm_response_xml, 
CASE
WHEN (request_status='F') THEN 'Failed'
WHEN (request_status='S') THEN 'Success'
WHEN (request_status='P') THEN 'Uploaded'
ELSE 'Unknown'
END as request_status, 
remarks, 
CASE
WHEN (request_state='B') THEN 'Begin'
WHEN (request_state='E') THEN 'Ended'
WHEN (request_state='P') THEN 'Uploaded'
ELSE 'Waiting'
END as  request_state,

request_state_desc
from serff_plan_mgmt 
where (serff_operation='TRANSFER_PLAN_BROCHURE') and <%=whereClause%>
order by serff_req_id desc <%=topTenWhereClause %>
</sql:query>

Requests Received by SERFF 
	<%
		if (request.getParameter("filterStatement") != null) {
			out.println(request.getParameter("filterStatement"));
		}
	%>
<br>

<display:table name="${result1.rows}" requestURI="/admin/uploadPlanBrochureStatusTop" id="table1" style="white-space: pre-wrap;" export="true" pagesize="10">
	<display:column property="serff_req_id" title="ID" sortable="false" />
	<display:column property="serff_operation" title="Operation" sortable="false" />
	<display:column property="pm_response_xml" title="Response" sortable="false" />
	<display:column property="process_ip" title="Process IP" sortable="false" />
	<display:column property="startTime" title="Start" sortable="false" />
	<display:column property="endTime" title="End" sortable="false" />
	<display:column property="request_status" title="Status" sortable="false" />
	<display:column property="remarks" title="Remarks" sortable="false" />
	<display:column property="request_state" title="State" sortable="false" />
	<display:column property="request_state_desc" title="State Desc" sortable="false" />

	<display:setProperty name="export.xml.filename" value="planBrochureStatus.xml"/>
	<display:setProperty name="export.csv.filename" value="planBrochureStatus.csv"/>
	<display:setProperty name="export.excel.filename" value="planBrochureStatus.xls"/>
</display:table>
