<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="session.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Upload PDFs</title>
	</head>
<body>
	<p align="center">Upload Brochure/SBC PDFs</p>
	<p align="center">&nbsp;</p>
	<div align="center">
		<center>
			<table border="0" cellpadding="0" cellspacing="0" width="80%">
				<c:if test="${uploadFileCount == 0}">
					<tr><td align="center" style="font-size: 12pt;">
						Please upload Brochure/SBC/EOC PDFs manually at FTP server to start this process.
					</td></tr>
				</c:if>
				<tr>
					<td align="center" style="font-size: 14pt;">
						<p align="center" style="font-size: 14pt;">
							This process uploads files to ECM and maps to respective plans in database.
							<br/>
							Please use the file name as XXXXXXXXXXXXXX_YYYY_[brochure/sbc/eoc]_filename.pdf
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<form action="${pageContext.request.contextPath}/admin/uploadBrochure" onsubmit="document.getElementById('submit').disabled=true;" method="GET">
							<p align="center">
								<input type="submit" id="submit" name="submit" value="Upload Brochure / SBC PDFs" style="font-size: 12pt; color: #0066CC">
							</p>
						</form>
					</td>
				</tr>
				<c:if test="${uploadBrochureMessage != null}">
					<tr>
						<td align="center" style="font-size: 12pt;">
							<p align="center" style="font-size: 12pt;">
								<c:out value="${uploadBrochureMessage}" />
							</p>
						</td>
					</tr>
				</c:if>
				<tr>
					<td align="center" style="font-size: 14pt;">
						<a href="${pageContext.request.contextPath}/admin/getPlanDocs" >Click here&nbsp;</a>to check Plans PDFs Status
					</td>
				</tr>
			</table>
		</center>
	</div>
</body>
</html>
