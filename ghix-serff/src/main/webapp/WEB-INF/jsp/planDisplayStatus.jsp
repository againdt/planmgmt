<html>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<head>
	<title>Plan Display Status</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	
	<script type="text/javascript">
		window.onload = function() {
			var planType = "<%=request.getParameter("planType")%>";
		    if (planType && planType != "null") {
		    	document.getElementById('planType').value = planType;
			}
		    
			var applicableYear = "<%=request.getParameter("applicableYear")%>";
		    if (applicableYear && applicableYear != "null") {
		    	document.getElementById('applicableYear').value = applicableYear;
			}
		    
			var benefitName = "<%=request.getParameter("benefitName")%>";
		    if (benefitName && benefitName != "null") {
		    	document.getElementById('benefitName').value = benefitName;
			}
		    
			var hiosIssuerId = "<%=request.getParameter("hiosIssuerId")%>";
		    if (hiosIssuerId && hiosIssuerId != "null") {
		    	document.getElementById('hiosIssuerId').value = hiosIssuerId;
			}
		};

		function setWhereClause(type) {
	
			var whereClause = "";
			var topTenSelectClause = "";
			var topTenWhereClause = "";
	
			switch (type) {
			case 'B4':
				//Step 1: Get List Box value, if it is not empty
				var planType = document.getElementById('planType').value;
				var applicableYear = document.getElementById('applicableYear').value;
				var benefitName = document.getElementById('benefitName').value;
				var hiosIssuerId = document.getElementById('hiosIssuerId').value;

				if (planType) {
					
					if (planType == "DENTAL") {
						whereClause += "SELECT \n" +
							"ISSUER_PLAN_NUMBER, APPLICABLE_YEAR, PLAN_DENTAL_BENEFIT.NAME, 'N/A' PMRY_CARE_CS_AFTR_NO_VISITS, 'N/A' PMRY_CARE_DEDTCOINS_AFTR_CPY, \n" +
							"NETWORK_T1_COINSURANCE_VAL, NETWORK_T1_COINSURANCE_ATTR, NETWORK_T1_COPAY_VAL, NETWORK_T1_COPAY_ATTR, NETWORK_T1_TILE_DISPLAY, NETWORK_T1_DISPLAY, \n" +
							"NETWORK_T2_COINSURANCE_VAL, NETWORK_T2_COINSURANCE_ATTR, NETWORK_T2_COPAY_VAL, NETWORK_T2_COPAY_ATTR, NETWORK_T2_DISPLAY, \n" +
							"OUTNETWORK_COINSURANCE_VAL, OUTNETWORK_COINSURANCE_ATTR, OUTNETWORK_COPAY_VAL, OUTNETWORK_COPAY_ATTR, OUTNETWORK_DISPLAY \n" +
							"FROM \n" +
							"PLAN_DENTAL_BENEFIT INNER JOIN PLAN_DENTAL ON PLAN_DENTAL_BENEFIT.PLAN_DENTAL_ID = PLAN_DENTAL.ID \n" +
							"INNER JOIN PLAN ON PLAN_DENTAL.PLAN_ID = PLAN.ID \n" +
							"WHERE \n" +
							"IS_DELETED = 'N'";

						if (benefitName) {
							whereClause += " AND PLAN_DENTAL_BENEFIT.NAME = '"+ benefitName +"' ";
						}
					}
					else {
						whereClause += "SELECT \n" +
							"ISSUER_PLAN_NUMBER, APPLICABLE_YEAR, PLAN_HEALTH_BENEFIT.NAME, PMRY_CARE_CS_AFTR_NO_VISITS, PMRY_CARE_DEDTCOINS_AFTR_CPY, \n" +
							"NETWORK_T1_COINSURANCE_VAL, NETWORK_T1_COINSURANCE_ATTR, NETWORK_T1_COPAY_VAL, NETWORK_T1_COPAY_ATTR, NETWORK_T1_TILE_DISPLAY, NETWORK_T1_DISPLAY, \n" +
							"NETWORK_T2_COINSURANCE_VAL, NETWORK_T2_COINSURANCE_ATTR, NETWORK_T2_COPAY_VAL, NETWORK_T2_COPAY_ATTR, NETWORK_T2_DISPLAY, \n" +
							"OUTNETWORK_COINSURANCE_VAL, OUTNETWORK_COINSURANCE_ATTR, OUTNETWORK_COPAY_VAL, OUTNETWORK_COPAY_ATTR, OUTNETWORK_DISPLAY \n" +
							"FROM \n" +
							"PLAN_HEALTH_BENEFIT INNER JOIN PLAN_HEALTH ON PLAN_HEALTH_BENEFIT.PLAN_HEALTH_ID = PLAN_HEALTH.ID \n" +
							"INNER JOIN PLAN ON PLAN_HEALTH.PLAN_ID = PLAN.ID \n" +
							"WHERE \n" +
							"IS_DELETED = 'N'";

						if (benefitName) {
							whereClause += " AND PLAN_HEALTH_BENEFIT.NAME = '"+ benefitName +"' ";
						}
					}

					if (applicableYear) {
						whereClause += " AND APPLICABLE_YEAR = '"+ applicableYear +"' ";
					}

					if (hiosIssuerId) {
						whereClause += " AND ISSUER_PLAN_NUMBER LIKE '"+ hiosIssuerId + "%' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID = '"+ hiosIssuerId.substring(0, 5) + "') ";
					}
					else {
						whereClause = "";
						alert("Issuer Plan Number must be required.");
						break;
					}

					if (planType == "DENTAL") {
						whereClause += "\n ORDER BY PLAN_DENTAL_BENEFIT.NAME DESC";
					}
					else {
						whereClause += "\n ORDER BY PLAN_HEALTH_BENEFIT.NAME DESC";
					}
				}
				else {
					whereClause = "";
					alert("Plan Type must be required.");
				}
				break;
			case 'B5':
				topTenSelectClause=" SELECT * FROM ( ";
				if(isPostgresDB) {
					topTenWhereClause=" ) allRec limit 10";
				} else {
				topTenWhereClause=" ) WHERE ROWNUM IN (1,2,3,4,5,6,7,8,9,10) ORDER BY ROWNUM";
				}
				break;
			}
			document.getElementById('whereClause').value = whereClause;
			document.getElementById('filterStatement').value = filterStatement;
			document.getElementById('topTenSelectClause').value = topTenSelectClause;
			document.getElementById('topTenWhereClause').value = topTenWhereClause;
		}
	</script>
</head>

<body>
<b>Search Plan Display Mapping [HIX-24105 & HIX-50129]</b>
	<form method="POST" action="${pageContext.request.contextPath}/admin/planDisplayStatus">
	<df:csrfToken/>
		<table border="1" cellspacing="0" style="border-color:#C0C0C0">
			<tr>
				<td bgcolor="#66FFCC">
					<p>
						<label for="hiosIssuerId" class="control-label">&nbsp;<b>Issuer Plan Number or HIOS ID:*</b></label>
						<input type="text" id="hiosIssuerId" name="hiosIssuerId" size="16" maxlength="16" />
						
						<label for="benefitName" class="control-label">&nbsp;<b>Benefit Name:</b></label>
						<input type="text" id="benefitName" name="benefitName" size="25" maxlength="400" />
					</p>
				</td>
			</tr>
			<tr>
				<td bgcolor="#66FFCC">
					<p>
						<label for="applicableYear" class="control-label">&nbsp;<b>Applicable Year:</b></label>
						<select id="applicableYear" name="applicableYear">
							<option value="2014" selected="selected">2014</option>
							<option value="2015">2015</option>
						</select>
						
						<label for="planType" class="control-label">&nbsp;<b>Plan Type*:</b></label>
						<select id="planType" name="planType">
							<option value="HEALTH" selected="selected">HEALTH</option>
							<option value="DENTAL">DENTAL</option>
						</select>
						
						<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
						<button type="reset" value="Reset">Reset</button>
					</p>
				</td>
			</tr>
		</table>
		<input id="whereClause" name="whereClause" type="hidden" />		
		<input id="topTenSelectClause" name="topTenSelectClause" type="hidden" />
		<input id="topTenWhereClause" name="topTenWhereClause" type="hidden" />
	</form>
		
	<br>
	
	<%
	String whereClause = request.getParameter("whereClause");
	// Not displaying any data on first time page loading.
	if (whereClause != null && whereClause.trim().length() > 0) {
	%>
	<sql:query dataSource="${jspDataSource}" var="planDisplayResults">
		<%=whereClause%>
	</sql:query>
	<display:table name="${planDisplayResults.rows}"  requestURI="planDisplayStatus" id="table1" export="true"  style="width:10%">
	
		<display:column property="ISSUER_PLAN_NUMBER" title="Issuer Plan Number" style="white-space: nowrap" sortable="true"/>
		<display:column property="APPLICABLE_YEAR" title="Applicable Year" style="white-space: nowrap" sortable="true"/>
		<display:column property="NAME" title="Benefit Name" style="white-space: nowrap" sortable="true"/>
		<display:column property="PMRY_CARE_CS_AFTR_NO_VISITS" title="Primary Care Cost After Visits [V]" style="white-space: nowrap" sortable="true"/>
		<display:column property="PMRY_CARE_DEDTCOINS_AFTR_CPY" title="Primary Care Deductible or Coin Copays [S]" style="white-space: nowrap" sortable="true"/>
		
		<display:column property="NETWORK_T1_COINSURANCE_VAL" title="Coinsurance In Network T1 Value" style="white-space: nowrap" sortable="true"/>
		<display:column property="NETWORK_T1_COINSURANCE_ATTR" title="Coinsurance In Network T1 Attr" style="white-space: nowrap" sortable="true"/>
		<display:column property="NETWORK_T1_COPAY_VAL" title="Copay In Network T1 Value" style="white-space: nowrap" sortable="true"/>
		<display:column property="NETWORK_T1_COPAY_ATTR" title="Copay In Network T1 Attr" style="white-space: nowrap" sortable="true"/>
		<display:column property="NETWORK_T1_TILE_DISPLAY" title="Network T1 Tile Display" style="white-space: nowrap" sortable="true"/>
		<display:column property="NETWORK_T1_DISPLAY" title="Network T1 Display" style="white-space: nowrap" sortable="true"/>
		
		<display:column property="NETWORK_T2_COINSURANCE_VAL" title="Coinsurance In Network T2 Value" style="white-space: nowrap" sortable="true"/>
		<display:column property="NETWORK_T2_COINSURANCE_ATTR" title="Coinsurance In Network T2 Attr" style="white-space: nowrap" sortable="true"/>
		<display:column property="NETWORK_T2_COPAY_VAL" title="Copay In Network T2 Value" style="white-space: nowrap" sortable="true"/>
		<display:column property="NETWORK_T2_COPAY_ATTR" title="Copay In Network T2 Attr" style="white-space: nowrap" sortable="true"/>
		<display:column property="NETWORK_T2_DISPLAY" title="Network T2 Display" style="white-space: nowrap" sortable="true"/>
		
		<display:column property="OUTNETWORK_COINSURANCE_VAL" title="Coinsurance Out of Network Value" style="white-space: nowrap" sortable="true"/>
		<display:column property="OUTNETWORK_COINSURANCE_ATTR" title="Coinsurance Out of Network Attr" style="white-space: nowrap" sortable="true"/>
		<display:column property="OUTNETWORK_COPAY_VAL" title="Copay Out of Network Value" style="white-space: nowrap" sortable="true"/>
		<display:column property="OUTNETWORK_COPAY_ATTR" title="Copay Out of Network Attr" style="white-space: nowrap" sortable="true"/>
		<display:column property="OUTNETWORK_DISPLAY" title="Out of Network Display" style="white-space: nowrap" sortable="true"/>
		
		<display:setProperty name="export.xml.filename" value="planDisplayStatus.xml"/>
		<display:setProperty name="export.csv.filename" value="planDisplayStatus.csv"/>
		<display:setProperty name="export.excel.filename" value="planDisplayStatus.xls"/>
	</display:table>
	<%
	}
	%>
</body> 
</html>
