<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="session.jsp" %>


<head>
<link rel="stylesheet"  type="text/css" href="../resources/css/bootstrap.css"/>
<link rel="stylesheet"  type="text/css" href="../resources/css/ghixcustom.css"/>

<script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../resources/js/bootstrap-filestyle.js"></script>
<script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script>

<script type="text/javascript">

	$(document).ready(function() {
		$(":file").filestyle({'buttonText': 'Browse ...'});
	});
	
	function doSubmit(formName, url) {
		
		if (isValidateForm() && formName && url) {
			document.forms[formName].method = "post";
			document.forms[formName].action = url;
			document.forms[formName].submit();
		}
	}
	
	function isValidateForm() {
		
		var flag = true;
		var loForm = document.forms["frmUploadPlans"];
		
		if (loForm) {
			
			var laFields = new Array();
			var li = 0;
		
			var fileUploadNode = loForm["fileUploadExcel"];
			
			if (fileUploadNode.files.length == 0) {
				laFields[li++] = "Please select an Plan docs excel file to upload.";
			}
			else {
				var fileName = fileUploadNode.value;
				var ext = fileName.split('.').pop();
				
				var XLSX = "xlsx" ;
				var XLS ="xls";
				
				if(ext!=XLSX && ext!=XLS) {
					laFields[li++] = "File type must be Excel.";
				}
			}
	
			if (laFields.length > 0) {
			    
				if(laFields.length > 1) {
					
					for (var fi = 0; fi < laFields.length; fi++) {
						laFields[fi] = "- " + laFields[fi];
					}
				}
		       	alert(laFields.join('\n'));
		       	flag = false;
		    }
		}
		return flag;
	}
</script>

<style>
	ul.unstyled li{
		padding:10px;
	}
</style>
</head>

<body>
<div class="row-fluid" id="main">
	<div class="container">
		<div class="row-fluid">
			<div class="gutter10">
				<div class="row-fluid">
					<h1 id="">Upload Plans Docs (Excel File) to FTP Server</h1>
				</div><!-- end of .row-fluid -->
				
				<div class="row-fluid">
					<div id="sidebar" class="span3">
						<div class="header"><span>Steps</span></div>
						<ul class="unstyled">
							<li><b>Step 1: </b>Choose file to upload</li>
							<li><b>Step 2: </b>Provide file description (optional)</li>
							<li class="gutter10"><b>Click Save: </b>Proceed to clicking Upload after required Excel files have been successfully uploaded.</li>
						</ul>
					</div><!-- end of .span3 -->
					
					<div class="span9" id="rightpanel">
						<div class="header" style="height:40px"></div>
						<form:form id="frmUploadPlans" name="frmUploadPlans" action="${pageContext.request.contextPath}/admin/uploadSTMExcel" modelAttribute="UploadExcel" enctype="multipart/form-data" method="POST">
							<df:csrfToken/>
							
							<input type="hidden" id="planType" name ="planType" value="PLAN_DOCS">
							<input type="hidden" id="returnPage" name ="returnPage" value="uploadPlanDocs">
							
							<div class="control-group">
								<label class="control-label" for="fileUploadExcel"><b>Step 1:</b> Choose Excel File* </label>
								<div class="controls">
									<input type="file" class="filestyle"  multiple="multiple" id="fileUploadExcel" name="fileUploadExcel" data-classButton="btn">
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label" for="description"><b>Step 2:</b> File Description </label>
								<div class="controls">
									<textarea rows="2" cols="70" id="description" name ="description"></textarea>
								</div>
							</div>
							
				            <div class="row-fluid gutter10-t">
								<div class="span2">
				            		<a href="javascript:document.forms['frmUploadPlans'].reset()" class="btn pull-left" id="cancel">Cancel</a>
				            	</div>
								<div class="span2 margin10-l">
				            		<a href="javascript:doSubmit('frmUploadPlans', '${pageContext.request.contextPath}/admin/uploadSTMExcel');" class="btn btn-primary pull-left" type="submit" id="save">Upload</a>
								</div>
							</div>
						</form:form>
					</div><!-- end of .span9 -->
					<c:if test="${uploadSTMExcelMessage != null}">
						<div align="left" style="font-size: 12pt; color: green; font-weight: bold;">
							<c:out value="${uploadSTMExcelMessage}" />
						</div>
					</c:if>
				</div><!-- end of .row-fluid -->
			</div> <!-- end of .gutter10 -->
		<!--row-fluid--> 
		</div>
	<!--container--> 
	</div>
<!--main-->
</div>
</body>
</html>
