<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.net.InetAddress"%>
<%@page import="com.getinsured.hix.platform.util.DateUtil"%>
<%@page import="java.io.OutputStream"%>
<%@page import="com.serff.admin.DownloadSOAPUIProject"%>
<%@page import="com.serff.admin.PostSerffData"%>
<%@page import="com.serff.util.SerffConstants"%>
<%@include file="datasource.jsp"%>
<html>
<head>
	<title>POST SERFF Data</title>
	
	<script type="text/javascript">
	
		function validate(){
			
			var id = document.getElementById('id').value;
			
			if(!id || isNaN(id)){
				alert('Invalid Id');
				return false;
			}
			
			var port = document.getElementById('port').value;
			
			if(!port || isNaN(port)){
				alert('Invalid Port');
				return false;
			}
		}
		
		function validateReqId() {
			
			var id = document.getElementById('requestIdDownload').value;
			
			if(!id || isNaN(id)){
				alert('Invalid Id');
				return false;
			}
		}
	
	</script>
	
</head>

<body bgcolor="#000000">

<h4>Download SOAP UI Project</h4>

<p>This options lets us download SOAP UI project with attachments for a request Id</p>

<form method="POST" action="postSerffData" onsubmit="return(validateReqId());">
<df:csrfToken/>
	SERFF Request ID: <input type="text" id="requestIdDownload" name="requestIdDownload" size="20" maxlength="10"> 
  	<input type="submit" value="Download">
  	<input type="hidden" id="type" name="type" value="download"> 
</form>  	

<h4>POST SERFF Data</h4>

<p>This option helps us to POST the data from one environment to another for testing/debugging</p>

<form method="POST" action="postSerffData" onsubmit="return(validate());">
<df:csrfToken/>
  <p>
  	SERFF Request ID: <input type="text" id="id" name="id" size="10" maxlength="10"> 
  	Target Host: <input type="text" id="host" name="host" size="20" maxlength="100">
  	Port No: <input type="text" id="port" name="port" size="10" maxlength="6">
  	<input type="button" value="Post">
  	<!-- input type="submit" value="Post" -->
  	<!-- Disabled temporarily for security-->
  </p>
</form>
Note: Post Button was disabled temporarily for security
</body>

<%! private static PostSerffData postSerffData = null; %>
<%! private static DownloadSOAPUIProject downloadSOAPUIProject = null; %>

<% 

String requestId = request.getParameter("id");
String requestIdDownload = request.getParameter("requestIdDownload");
String type = request.getParameter("type");
String postResponse = "";
String error = "";

if(type != null && type.equalsIgnoreCase("download") && requestIdDownload != null && requestIdDownload.trim().length() > 0){
	
	downloadSOAPUIProject = (DownloadSOAPUIProject)ctx.getBean("downloadSOAPUIProject");
	byte[] file = downloadSOAPUIProject.downloadSerffProject(new Long(requestIdDownload));
	
	
	if(file != null){
		response.setContentType("application/octet-stream");
		String currentDate = DateUtil.dateToString(new Date(), "MMddyyyy")+"_";
		
		//Creating fileName - SERFF_SOAPUI_ipaddress_id_DDMMMYYYY_HHMM.zip
		InetAddress processIp = InetAddress.getLocalHost();
		SimpleDateFormat format = new SimpleDateFormat("ddMMMyyyy_HHmm");
		String fileName = "SERFF_SOAPUI_"+processIp.getHostAddress()+"_"+requestIdDownload+"_"+format.format(new Date())+".zip";
		response.setHeader("Content-Disposition", ("attachment; filename=" + fileName.replaceAll(SerffConstants.UNSECURE_CHARS_REGEX, "").trim()));
		
	    OutputStream o = response.getOutputStream();
	    o.write(file); 
	    o.flush(); 
	    o.close(); 
	    error = "Successfully retrieved SOAP UI project for this request";
	}
	else{
		error = "Invalid Id or Exception Occured<br>";
	}
}
else if(requestId != null && requestId.trim().length() > 0) {
	String host = request.getParameter("host");
	String port = request.getParameter("port");
	postSerffData = (PostSerffData)ctx.getBean("postSerffData");
	postResponse = postSerffData.postData(new Long(requestId), host, port);
}


%>
<body>
Response<br>

<textarea rows="15" cols="100" draggable="false" style="resize: none;" readonly="readonly">
	<%=postResponse%>
</textarea>

<b><br><%=error%></b>

</body>


</html>