<html>
<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>
<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8">

<head>
<title>Plan Name Check</title>
</head>

<body>

<sql:query dataSource="${jspDataSource}" var="result1">
select Plan_name , len1 from(
select name as Plan_name ,LENGTH(TRIM(TRANSLATE(name , 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+-.0123456789 %$,_.:;=?()!/&''', ' '))) as len1
from plan) res1
where len1>1
order by len1 desc

</sql:query> 
<b>Junk Characters in Plan Name</b>
<br>
<br>
Note: If there are any junk characters, please fix them.
<br>
<br>

<display:table name="${result1.rows}"  requestURI="planNameCheck" id="table1" export="true"  style="width:50%" defaultorder="descending" >
	<display:column property="Plan_name" title="Plan Name"  />
	<display:column property="len1" title="Length"  />
	<display:setProperty name="export.xml.filename" value="JunkData.xml"/>
	<display:setProperty name="export.csv.filename" value="JunkData.csv"/>
	<display:setProperty name="export.excel.filename" value="JunkData.xls"/>
	
</display:table>

 </body> 
 </html>


