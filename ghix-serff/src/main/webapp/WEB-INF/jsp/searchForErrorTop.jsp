<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp"%>

<sql:query dataSource="${jspDataSource}" var="result1">
select distinct MODULE_NAME from GI_ERROR_CODE 
</sql:query>

<html>
<head>
	<title>Search for Error Messages</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<body>
	<%
		String whereClause = "id is not null";
		String andClause = "";

		if (request.getParameter("check") != null
				&& request.getParameter("check").equalsIgnoreCase("Y")) {
			andClause += " MODULE NAME =" + request.getParameter("check");

		}
	%>

	<form method="POST" action="searchForErrorBottom" >
	<df:csrfToken/>
		<b>Select Module Names</b> <br>
		<table class="table" border="1" cellspacing="0" width="30%">
			<thead>
				<tr class="graydrkbg">
					<th scope="col">Module Names</th>
					<th scope="col">Select</th>
				</tr>

			</thead>
			<tbody class="msg">
				<c:forEach var="test" items="${result1.rows}">
					<tr>
						<td>${test.module_name}</td>
						<td><input type="checkBox" value="${test.module_name}"
							name="checkbox" checked="checked"></td>
					</tr>
				</c:forEach>


			</tbody>
		</table>
		<br> <input type="submit" value="Search">
	</form>
</body>
</html>