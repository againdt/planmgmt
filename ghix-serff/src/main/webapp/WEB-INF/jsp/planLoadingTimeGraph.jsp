<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ include file="datasource.jsp" %>
<%@ page import = "java.util.*" %>
<%@ page import = "java.text.*" %>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Plan loading time graph</title>

<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>"/>
<link rel="stylesheet" href="<c:url value='/resources/css/jquery-ui.css'/>"/>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<!-- <script type="text/javascript" src="../resources/js/jsapi.js"></script> -->

<script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="../resources/js/jquery-ui.js"></script>
<%
	String dbType = prop.getProperty("database.type");

	String graphTitle = null;
	String whereClause = request.getParameter("whereClause");
	
	if (whereClause == null || whereClause.trim().length() == 0) {
		if(isPostgresDB) {
			whereClause = " AND START_TIME >= (now() - interval '1 day')" +
					" AND END_TIME <= now()" +
					" ORDER BY SERFF_REQ_ID DESC";
		} else {
		whereClause = " AND START_TIME >= (SYSDATE - 1)" +
				" AND END_TIME <= SYSDATE" +
				" ORDER BY SERFF_REQ_ID DESC";
		}
		graphTitle = "Plan loading time graph for last 24 Hours";
	}
	else if (null != request.getParameter("startDate") && null != request.getParameter("endDate")) {
		
		if (request.getParameter("startDate").equals(request.getParameter("endDate"))) {
			graphTitle = "Plan loading time graph for " + request.getParameter("startDate");
		}
		else {
			graphTitle = "Plan loading time graph between " + request.getParameter("startDate") + " to " + request.getParameter("endDate");
		}
	}
	
	String timeFields;
	if("POSTGRESQL".equalsIgnoreCase(dbType)) {
		timeFields = "SUBSTR((END_TIME - START_TIME), 0, POSITION('.' in (END_TIME - START_TIME)) - 1) AS DIFF, " +
		"Extract(hours from ((CAST(END_TIME AS DATE) - CAST(START_TIME AS DATE)))) * 60 + Extract(mins from ((CAST(END_TIME AS DATE) - CAST(START_TIME AS DATE)))) AS DURATION_IN_MIN";
		//"((CAST(END_TIME AS DATE) - CAST(START_TIME AS DATE))) AS DURATION_IN_MIN";
	} else {
		timeFields = "SUBSTR((END_TIME - START_TIME), 0, INSTR((END_TIME - START_TIME), '.') - 1) AS DIFF, " +
				"TRUNC((CAST(END_TIME AS DATE) - CAST(START_TIME AS DATE)) * 86400/60) AS DURATION_IN_MIN";
	}
	String selectQuery = "SELECT SERFF_REQ_ID, ISSUERS.HIOS_ISSUER_ID, ISSUERS.NAME, TO_CHAR(START_TIME, 'mm/dd/yyyy') AS START_TIME, " +
			timeFields + ", PLAN_STATS " +
			"FROM SERFF_PLAN_MGMT LEFT JOIN ISSUERS ON SERFF_PLAN_MGMT.ISSUER_ID = ISSUERS.ID " +
			"WHERE REQUEST_STATUS = 'S' AND SERFF_OPERATION = 'TRANSFER_PLAN' " + whereClause;
%>

<sql:query dataSource="jdbc/ghixDS" var="timeGraphResults">
	<%=selectQuery%>
</sql:query>

<script type="text/javascript">
	
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
	
	function drawChart() {
        var planLoadingTimeData = google.visualization.arrayToDataTable([
			['Request ID', 'Duration'],
			<c:forEach var="row" items="${timeGraphResults.rows}">
				['<c:out value="${row.SERFF_REQ_ID}"/>',<c:out value="${row.DURATION_IN_MIN}"/>],
			</c:forEach>
     	]);
		
		var options = {
			title: '<%=graphTitle%>',
			hAxis: {title: 'SERFF Request ID', titleTextStyle: {color: 'red'}},
			vAxis: {title: 'Duration in minute', titleTextStyle: {color: 'red'}}
		};
		
		var chart = new google.visualization.ColumnChart(document.getElementById('divTimeGraph'));
		chart.draw(planLoadingTimeData, options);
	}
	
	$(function() {
		$( "#startDate" ).datepicker(
		{
			changeMonth: true,
			changeYear: true,
			onSelect: function(selected) {
				$("#endDate").datepicker("option","minDate", selected)
			}
		}
		).attr( "readOnly" , "true" );
		
		$( "#endDate" ).datepicker(
		{
			changeMonth: true,
			changeYear: true,
			onSelect: function(selected) {
				$("#startDate").datepicker("option","maxDate", selected)
			}
		}
		).attr( "readOnly" , "true" );
	});
	
	window.onload = function() {
		var startDate = "<%=request.getParameter("startDate")%>";
	    if (startDate && startDate != "null") {
	    	$("#startDate").val(startDate);
		}
	    
		var endDate = "<%=request.getParameter("endDate")%>";
	    if (endDate && endDate != "null") {
	    	$("#endDate").val(endDate);
		}
	};
	
	function setWhereClause(type) {
		var whereClause = "";
		
		switch (type) {
			case "B4" : 
				// Validate the dates
			    // validateDates();
			    // Step 1: Get List Box value, if it is not empty
				var startDate = document.getElementById("startDate").value;
				var endDate = document.getElementById("endDate").value;
				var errorMsg = "";
				
				if (startDate) {
					whereClause += " AND START_TIME >= TO_TIMESTAMP( '"+ startDate + " 00:00:00' , 'mm/dd/yyyy HH24:MI:SS')";
				}
				else {
					errorMsg = "- Start Date must be required.";
				}
				
				if (endDate) {
					whereClause += " AND END_TIME <= TO_TIMESTAMP( '"+ endDate + " 23:59:59' , 'mm/dd/yyyy HH24:MI:SS')";
				}
				else {
					
					if (errorMsg) {
						errorMsg += "\n";
					}
					errorMsg += "- End Date must be required.";
				}
				
				if (errorMsg) {
					whereClause = "";
					alert(errorMsg);
					break;
				}
				else {
					" ORDER BY SERFF_REQ_ID DESC";
				}
				break;
		}
		document.getElementById("whereClause").value = whereClause;
		
		if (whereClause) {
			document.forms["frmTimeGraph"].action = "${pageContext.request.contextPath}/admin/planLoadingTimeGraph";
			document.forms["frmTimeGraph"].submit();
		}
	}
</script>
</head>

<body>
<form id="frmTimeGraph" name="frmTimeGraph" action="${pageContext.request.contextPath}/admin/planLoadingTimeGraph" method="post">
	<df:csrfToken/>
	<input id="whereClause" name="whereClause" type="hidden" />
	<b>Plan loading time graph [HIX-53952]</b>
	
	<table border="1" cellspacing="0" style="border-color:#C0C0C0">
		<tr>
			<td bgcolor="#66FFCC">
				<p>
					<label for="startDate" class="control-label">&nbsp;<b>Start Date*:</b></label>
					<input type="text" id="startDate" name="startDate" size="10" maxlength="10" />
					
					<label for="endDate" class="control-label">&nbsp;<b>End Date*:</b></label>
					<input type="text" id="endDate" name="endDate" size="10" maxlength="10" />
					
					<input type="button" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
					<button type="reset" value="Reset">Reset</button>
				</p>
			</td>
		</tr>
	</table>
	<div id="divTimeGraph" style="width: 900px; height: 500px;"></div>
</form>

<display:table name="${timeGraphResults.rows}" id="timeGraphTable"
	requestURI="planLoadingTimeGraph" defaultsort="1" pagesize="50"
	style="white-space: pre-wrap;width: 100%;" export="true">
	<display:column property="SERFF_REQ_ID" title="SERFF Request ID" sortable="true" />
	<display:column property="HIOS_ISSUER_ID" title="HIOS Issuer ID" sortable="true" />
	<display:column property="NAME" title="Name" sortable="true" />
	<display:column property="START_TIME" title="Start Time" sortable="true" />
	<display:column property="DIFF" title="Difference" sortable="true" />
	<display:column property="DURATION_IN_MIN" title="Duration in minute" sortable="true" />
	<%-- <display:column property="REQUEST_STATUS" title="Status" sortable="true" /> --%>
	<display:column property="PLAN_STATS" title="Plan Count Stats" sortable="true" />
	
	<display:setProperty name="export.xml.filename" value="zipWithoutPlans.xml" />
	<display:setProperty name="export.csv.filename" value="zipWithoutPlans.csv" />
	<display:setProperty name="export.excel.filename" value="zipWithoutPlans.xls" />
</display:table>

<br />
<div id="selectQuery">
	<b>Query:=</b>&nbsp;<%=selectQuery%>
</div>
</body>
</html>