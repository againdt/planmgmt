<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="datasource.jsp"%>

<html lang="en">
<head>
<link rel="stylesheet" type="text/css" href="../resources/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="../resources/css/ghixcustom.css"/>
<link rel="stylesheet" type="text/css" href="../resources/css/displaytag_serff.css" charset="utf-8" />

<script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../resources/js/bootstrap-filestyle.js"></script>
<script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script>

<script type="text/javascript">

	function doSubmit(formName, url) {
		document.forms[formName].method = "POST";
		document.forms[formName].action = url;
		document.forms[formName].submit();
	}
	
	function isValidateIssuerOrPlanId () {
		
		var reprocessID = document.getElementById("reprocessID").value;
		var errorMsg = "";
		
		if (reprocessID) {
			
			if (reprocessID.length < 5) {
				errorMsg = "Invalid length of Issuer Plan Number Or HIOS Issuer Id !!";
			}
			else if (reprocessID.length == 5 && isNaN(reprocessID)) {
				errorMsg = "Invalid HIOS Issuer Id !!";
			}
			else if (reprocessID.length == 14) {
				
				var patterPlanId = new RegExp("(^[0-9]{5})([A-Za-z]{2})([0-9]{7})$");
				
				if (!patterPlanId.test(reprocessID)) {
					errorMsg = "Invalid Issuer Plan Number !!";
				}
			}
			else if (reprocessID.length > 5) {
				errorMsg = "Invalid length of Issuer Plan Number Or HIOS Issuer Id !!";
			}
		}
		else {
			errorMsg = "Issuer Plan Number Or HIOS Issuer Id is required !!";
		}
		
		if (errorMsg) {
			alert(errorMsg);
		}
		else {
			doSubmit('frmUploadPlans', '${pageContext.request.contextPath}/admin/reprocessDisplayData');
		}
	}
</script>

<style>
	ul.unstyled li{
		padding:10px;
	}
</style>
</head>
<sql:query dataSource="${jspDataSource}" var="result1">
		select i.HIOS_ISSUER_ID, i.ID , i.SHORT_NAME, p.applicable_year, count (*) as count from plan_health_benefit phb 
 		 Join plan_health ph ON ph.ID = phb.plan_health_id
 		 Join plan p ON p.ID = ph.plan_id
 		 join issuers i on i.id=p.ISSUER_ID
  		 where phb.network_t1_display is null and phb.network_t1_tile_display is null and p.is_deleted = 'N' and p.applicable_year in (2016, 2015, 2014) 
  		Group by i.HIOS_ISSUER_ID, i.ID, i.SHORT_NAME, p.applicable_year ORDER BY p.applicable_year DESC, i.HIOS_ISSUER_ID ASC
</sql:query>

<body>
<div class="row-fluid" id="main">
	<div class="container">
		<div class="row-fluid">
			<div class="gutter10">

				<div class="row-fluid">
					<h1 id="">Process Plan Health Benefits Display value</h1>
				</div><!-- end of .row-fluid -->
	
				<div class="row-fluid">
					
					<!-- Note: http://jira.getinsured.com/browse/HIX-25244 / HIX-29432 -->
					<br>This is one time activity .Below is the list of Issuers with null display values.
					<br>Enter Issuer HIOS ID, Select year and click on Process button to update display benefit texts for the issuer. 
					<br>This process might take some time .Please wait for process to complete before selecting another issuer.
					<br><br>
					
					<div class="span9" id="rightpanel">
						<form:form id="frmUploadPlans" name="frmUploadPlans" action="javascript: void(0);" modelAttribute="uploadPlans" enctype="multipart/form-data" method="POST">
						<df:csrfToken/>
							<div class="header" style="height:5px"></div>
							<div class="control-group">
				                <label for="carrierSelect" class="control-label"><b>List of issuer with blank benefit display data </b></label>
								<select style="width:600px" id="carrierSelect" name="carrierSelect" multiple="multiple">
									<!-- <option value="0" selected="selected">Select Hios Id</option> -->
									
										<c:forEach var="test" items="${result1.rows}">
											<c:if test="${test.HIOS_ISSUER_ID != null}"> 
												<option value="${test.ID}">[${test.HIOS_ISSUER_ID}] ${test.SHORT_NAME } [${test.APPLICABLE_YEAR} ][Record Count:${test.count}] </option>
											</c:if> 
										</c:forEach>
								</select>
							</div>
							
				            <%-- <div class="gutter10">
					            <c:choose>
						            <c:when test="${result1.rowCount > 0}"> 
						            	 <a href="javascript: doSubmit('frmUploadPlans', '${pageContext.request.contextPath}/admin/processDisplayData');" class="btn btn-primary offset1" type="submit" id="save">Process</a>
						            	 <input type ="hidden" id ='list' name =list value="">
						           	</c:when>
						           	<c:otherwise>
						           		<h3>No record remaining to be processed!</h3>
						          	</c:otherwise>
								</c:choose>
				            </div> --%>
				            <div class="header" style="height:5px"></div>
				            
				            <div class="control-group">
				                <label for="reprocessID" class="control-label"><b>Process Plan Benefits using Issuer Plan Number Or HIOS Issuer Id:</b></label>
								<input type="text" name="reprocessID" id="reprocessID" />
							</div>
				            
				            <div class="control-group">
				                <label for="applicableYear" class="control-label"><b>Applicable Year:</b></label>
								<select id="applicableYear" name="applicableYear">
									<!-- <option value="0" selected="selected">Select Applicable Year</option> -->
									<option value="2017">2017</option>
									<option value="2016">2016</option>
									<option value="2015">2015</option>
									<option value="2014">2014</option>
									<!-- <option value="2017">2017</option> -->
								</select>
							</div>
							
							<div class="gutter10">
								<a href="javascript: isValidateIssuerOrPlanId();" class="btn btn-primary offset1" type="submit" id="reProcess">Process</a>
				            </div>
				            <div class="header" style="height:5px"></div>
						</form:form>
					</div><!-- end of .span9 -->
				</div><!-- end of .row-fluid -->
			</div> <!-- end of .gutter10 -->
		<!--row-fluid--> 
		</div>
	<!--container--> 
	</div>
<!--main-->
</div>
</body>
</html>