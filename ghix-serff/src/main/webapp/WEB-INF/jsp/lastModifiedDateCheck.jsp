<html>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>Templates Last Updated Date</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	
	<script type="text/javascript">
	
		function doSubmit(formName, url) {
			
			if (formName && url) {
				document.forms[formName].method = "post";
				document.forms[formName].action = url;
				document.forms[formName].submit();
			}
		}
		
		function setWhereClause(type){
			
			var whereClause = '';
			
			switch (type) {
		
				case 'B4' : 
					var planId = document.getElementById('planId').value;
					var applicableYear = document.getElementById('applicableYear').value;
				
					if(planId != null && planId.length > 0 &&applicableYear!= null && applicableYear.length>0 )
					{
					whereClause = "plan_id ='"+planId+"'and applicable_year= '"+applicableYear+"'";
				
					}else{
						alert("Please enter plan id and applicable year both");
					}
					
					if(!whereClause){
						whereClause = "plan_id is not null";
					}
					break;
				}
			document.getElementById('whereClause').value = whereClause;
		}
	</script>
</head>


<form method="POST" id="frmlastModifiedDate" name="frmlastModifiedDate" action="allTemplatesLastModifiedDate">
<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			
			<td bgcolor="#66FFCC">
				<p>
			
					&nbsp;Issuer Plan Number:
					<input type="text" id="planId" name="planId" size="20" maxlength="14">
					&nbsp;Applicable Year:
					<input type="text" id="applicableYear" name="applicableYear" size="10" maxlength="4">
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">

</form>
<body>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "plan_id is not null";
	}
	
	
%>


<sql:query dataSource="${jspDataSource}" var="result1">
select plan_id , 
to_char(admin_last_updated , 'DD-MON-YYYY:hh:mi:ss') as admin_last_updated ,
to_char(network_last_updated , 'DD-MON-YYYY:hh:mi:ss') as network_last_updated,
to_char(service_area_last_updated , 'DD-MON-YYYY:hh:mi:ss') as service_area_last_updated,
to_char(plan_last_updated , 'DD-MON-YYYY:hh:mi:ss') as plan_last_updated,
to_char(rates_last_updated , 'DD-MON-YYYY:hh:mi:ss') as rates_last_updated,
to_char(pres_drug_last_updated , 'DD-MON-YYYY:hh:mi:ss') as pres_drug_last_updated,
to_char(business_rules_last_updated , 'DD-MON-YYYY:hh:mi:ss') as business_rules_last_updated,
to_char(urac_last_updated , 'DD-MON-YYYY:hh:mi:ss') as urac_last_updated,
to_char(ncqa_last_updated , 'DD-MON-YYYY:hh:mi:ss') as ncqa_last_updated,
to_char(urrt_last_updated ,'DD-MON-YYYY:hh:mi:ss') as urrt_last_updated,
to_char(admin_lmd,'DD-MON-YYYY:hh:mi:ss') as admin_lmd,
to_char(network_lmd,'DD-MON-YYYY:hh:mi:ss') as network_lmd,
to_char(Svcarea_Lmd,'DD-MON-YYYY:hh:mi:ss') as Svcarea_Lmd,
to_char(plan_lmd,'DD-MON-YYYY:hh:mi:ss') as plan_lmd,
to_char(rates_lmd,'DD-MON-YYYY:hh:mi:ss') as rates_lmd,
to_char(Pres_Drug_Lmd,'DD-MON-YYYY:hh:mi:ss') as Pres_Drug_Lmd,
 applicable_year from Serff_Templates  where <%=whereClause %>
</sql:query> 
<b>Templates Last Modified Date.</b>

<display:table name="${result1.rows}"  requestURI="allTemplatesLastModifiedDate" id="table1" export="true" pagesize="100" style="width:80%" defaultorder="descending" >
	<display:column property="plan_id" title="Plan Id" sortable="true" />
	<display:column property="admin_last_updated" title="Admin Last Update Date" sortable="true" />
	<display:column property="network_last_updated" title="Network Last Update Date" sortable="true" />
	<display:column property="service_area_last_updated" title="Service Area Last Update Date" sortable="true" />
	<display:column property="plan_last_updated" title="Plan Last Update Date" sortable="true" />
	<display:column property="rates_last_updated" title="Rates Last Update Date" sortable="true" />
	<display:column property="pres_drug_last_updated" title="Drug Last Update Date" sortable="true" />
	<display:column property="business_rules_last_updated" title="Business Rule Last Update Date" sortable="true" />
	<display:column property="urac_last_updated" title="URAC Last Update Date" sortable="true" />
	<display:column property="ncqa_last_updated" title="NCQA Last Update Date" sortable="true" />
	<display:column property="urrt_last_updated" title="URRT Last Update Date" sortable="true" />
	<%--<display:column property="admin_lmd" title="Admin Last Modified Date" sortable="true" />
	<display:column property="network_lmd" title="Network Last Modified Date" sortable="true" />
	<display:column property="Svcarea_Lmd" title="Service Area Last Modified Date" sortable="true" />
	<display:column property="plan_lmd" title="Plan Last Modified Date" sortable="true" />
	<display:column property="rates_lmd" title="Rates Last Modified Date" sortable="true" />
	<display:column property="Pres_Drug_Lmd" title="P&D Last Modified Date" sortable="true" /> --%>
	<display:column property="applicable_year" title="Applicable Year" sortable="true" />
	
	<display:setProperty name="export.xml.filename" value="TemplatesLastModifiedDate.xml"/>
	<display:setProperty name="export.csv.filename" value="TemplatesLastModifiedDate.csv"/>
	<display:setProperty name="export.excel.filename" value="TemplatesLastModifiedDate.xls"/>
</display:table>
<br>
Note: Query to check records in DB <br>
select plan_id , 
to_char(admin_last_updated , 'DD-MON-YYYY:hh:mi:ss') as admin_last_updated ,
to_char(network_last_updated , 'DD-MON-YYYY:hh:mi:ss') as network_last_updated,
to_char(service_area_last_updated , 'DD-MON-YYYY:hh:mi:ss') as service_area_last_updated,
to_char(plan_last_updated , 'DD-MON-YYYY:hh:mi:ss') as plan_last_updated,
to_char(rates_last_updated , 'DD-MON-YYYY:hh:mi:ss') as rates_last_updated,
to_char(pres_drug_last_updated , 'DD-MON-YYYY:hh:mi:ss') as pres_drug_last_updated,
to_char(business_rules_last_updated , 'DD-MON-YYYY:hh:mi:ss') as business_rules_last_updated,
to_char(urac_last_updated , 'DD-MON-YYYY:hh:mi:ss') as urac_last_updated,
to_char(ncqa_last_updated , 'DD-MON-YYYY:hh:mi:ss') as ncqa_last_updated,
to_char(urrt_last_updated ,'DD-MON-YYYY:hh:mi:ss') as urrt_last_updated,
to_char(admin_lmd,'DD-MON-YYYY:hh:mi:ss') as admin_lmd,
to_char(network_lmd,'DD-MON-YYYY:hh:mi:ss') as network_lmd,
to_char(Svcarea_Lmd,'DD-MON-YYYY:hh:mi:ss') as Svcarea_Lmd,
to_char(plan_lmd,'DD-MON-YYYY:hh:mi:ss') as plan_lmd,
to_char(rates_lmd,'DD-MON-YYYY:hh:mi:ss') as rates_lmd,
to_char(Pres_Drug_Lmd,'DD-MON-YYYY:hh:mi:ss') as Pres_Drug_Lmd,
 applicable_year from Serff_Templates where 
plan_id ='?' and applicable_year ='?';
</body> 
 
</html>


