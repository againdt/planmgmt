<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ page import="com.getinsured.hix.model.serff.SerffPlanMgmtBatch"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>SERFF Ancillary for AME/STM Excel Admin</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	
	<script type="text/javascript">
		$(function() {
			$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
		});
		
		$(function() {
			$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
		});
		
		function validateDates() {
		
			var dateStart = document.getElementById('date1').value.split('/');
			
			if(dateStart != "" && dateStart.length > 0) {
				validateDate(dateStart);
			}
		
			var dateEnd = document.getElementById('date2').value.split('/');
			
			if(dateEnd !=  "" && dateEnd.length > 0) {
				validateDate(dateEnd);
			}
		}
		
		function validateDate(dateGiven) {
			
			var today=new Date();
		
			var dob_mm = dateGiven[0];  
			var dob_dd = dateGiven[1];  
			var dob_yy = dateGiven[2]; 
			
			var birthDate=new Date();
			birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
		
			if( (today.getFullYear() - 2) >  birthDate.getFullYear() ) {
				return false; 
			}
		
			if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) {
				return false; 
			}
		
			if(today.getTime() < birthDate.getTime()){
				return false; 
			}
		}
		
		function setWhereClause(type){
			
			var whereClause = '';
			var filterStatement = '';
			var topTenSelectClause='';
			var topTenWhereClause='';
			
			switch (type) {
				case 'B1' : 
					if(isPostgresDB) {
						whereClause = 'FTP_START_TIME BETWEEN (now()-interval \'1 day\') and now()';
					} else {
					whereClause = 'FTP_START_TIME BETWEEN (SYSDATE-1) AND SYSDATE';
					}
					filterStatement = ' in last 24 hours';
					break;
				case 'B2' : 
					if(isPostgresDB) {
						whereClause = 'FTP_START_TIME BETWEEN (now()-interval \'12 hour\') and now()';
					} else {
					whereClause = 'FTP_START_TIME BETWEEN (SYSDATE-1/2) AND SYSDATE';
					}
					filterStatement = ' in last 12 hours';
					break;
				case 'B3' : 
					if(isPostgresDB) {
						whereClause = 'FTP_START_TIME BETWEEN (now()-interval \'1 hour\') and now()';
					} else {
					whereClause = 'FTP_START_TIME BETWEEN (SYSDATE-1/24) AND SYSDATE';
					}
					filterStatement = ' in last 1 hour';
					break;
				case 'B4' : 
					//Validate the dates
					validateDates();
					
				    // Step 1: Get List Box value, if it is not empty
					var fieldName = document.getElementById('field_name').value;
					var fieldValue = document.getElementById('field_value').value;
					
					if (fieldValue != null && fieldValue.length > 0) {
						
						if (fieldName == "SPMB.ID" || fieldName == "SERFF_REQ_ID") {
							whereClause = " " + fieldName + " = " + fieldValue + "";
						}
						else {
							if(isPostgresDB) {
								whereClause = " LOWER(CAST ("+fieldName + " as TEXT)) LIKE LOWER('%" + fieldValue + "%')";
							} else {
							whereClause = " LOWER("+fieldName + ") LIKE LOWER('%" + fieldValue + "%')";
						}
						}
						// whereClause = " LOWER("+fieldName + ") LIKE LOWER('%" + fieldValue + "%')";
						filterStatement = ' WITH ' + fieldName + " = " + fieldValue;
					}
					
					//Step 2: Get the Status Value
					var statuses = document.getElementById('status');
					var status = statuses.options[statuses.selectedIndex].value;
					
					if (status == 'S' || status == 'F') {
						
						if (whereClause) {
							whereClause = whereClause  + " AND REQUEST_STATUS = '" + status + "'";
						}
						else{
							whereClause = whereClause  + " REQUEST_STATUS = '" + status + "'";
						}
					}
					
					if (status == 'S') {
						filterStatement = filterStatement+ ' which were successful';
					}
					
					if (status == 'F') {
						filterStatement = filterStatement+ ' which have failed';
					}
					
					//Step 3: Get the Dates
					var date1 = document.getElementById('date1').value;
					var date2 = document.getElementById('date2').value;
					
					if(date1 && date2) {	
						
						if (whereClause) {
							whereClause = whereClause+"and FTP_START_TIME BETWEEN TO_TIMESTAMP('"+date1+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') AND TO_TIMESTAMP('"+date2+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
						}
						else {
							whereClause = whereClause+" FTP_START_TIME BETWEEN TO_TIMESTAMP('"+date1+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') AND TO_TIMESTAMP('"+date2+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
						}
					
					}else{
						if(date1&& !date2  ){
							if( whereClause){
							
							whereClause = whereClause+"and to_char(FTP_START_TIME  ,'mm/dd/yyyy' )='"+date1+"'" ;
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							}else{
								whereClause = whereClause+" to_char(FTP_START_TIME  ,'mm/dd/yyyy' )='"+date1+"'" ;
								filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							}
						}else{
							
							 if(!date1 && date2 ){
								 if( whereClause){
										whereClause = whereClause+"and to_char(end_time  ,'mm/dd/yyyy' )='"+date2+"'" ;
										filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
								 }else{
										whereClause = whereClause+" to_char(end_time  ,'mm/dd/yyyy' )='"+date2+"'" ;
										filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
									 }
								}
							}
						
						}
					
						if (!whereClause) {
							// whereClause = "FTP_START_TIME <= SYSDATE";
							if(isPostgresDB) {
								whereClause = "FTP_START_TIME between (FTP_START_TIME) and now()";
							} else {
							whereClause = "FTP_START_TIME between (FTP_START_TIME) and SYSDATE";
						}
						}
					
					break;
				
				case 'B5' :
					topTenSelectClause=" Select * from ( ";
					if(isPostgresDB) {
						whereClause = "FTP_START_TIME between (FTP_START_TIME) and now()";
						topTenWhereClause=" ) allrec limit 10 ";
					} else {
						whereClause = "FTP_START_TIME between (FTP_START_TIME) and SYSDATE";
					topTenWhereClause=" ) where rownum in (1,2,3,4,5,6,7,8,9,10) order by rownum";
					}
					filterStatement = "for latest records";
					
					/* whereClause = "ftp_start_time <= SYSDATE";
					topTenSelectClause=" Select * from ( ";
					topTenWhereClause=" ) where rownum in (1,2,3,4,5,6,7,8,9,10) order by rownum";
					filterStatement = "for latest records"; */
					break;
					
			}
			
			document.getElementById('whereClause').value = whereClause; // escape(whereClause);
			document.getElementById('filterStatement').value = filterStatement;
			document.getElementById('topTenSelectClause').value = topTenSelectClause;
			document.getElementById('topTenWhereClause').value = topTenWhereClause;
			//alert('whereClause==>'+whereClause);
			/* alert('topTenSelectClause==>'+topTenSelectClause);
			alert('topTenWhereClause==>'+topTenWhereClause); */
		}
	</script>    
</head>

<form method="POST" action="${pageContext.request.contextPath}/admin/getSTMPlansStatusTop" enctype="multipart/form-data">
<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
				<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
				<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
				<input type="submit" value="Show Latest" name="B5" title="View top 10 requests" onclick="setWhereClause('B5');">
			</td>
			<td bgcolor="#66FFCC">
				<p>
					&nbsp;Field:
					<select size="1" id="field_name" name="field_name">
						<option value="SPMB.ID" selected>Batch id</option>
						<option value="SERFF_REQ_ID">Serff Req id</option>
						<!-- <option value="SPMB.ISSUER_ID">HIOS Issuer ID</option> -->
						<option value="OPERATION_TYPE">Operation</option>
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
					Status:
					<select size="1" id="status" name="status">
					<option value="A" selected="">All</option>
					<option value="S" >Success</option>
					<option value="F">Failure</option>
				</select>
				Date Start:
				<input type="text" id="date1" name="date1" size="10" maxlength="10"> End:
				<input type="text" id="date2" name="date2" size="10" maxlength="10">
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
	<input id="topTenSelectClause" name="topTenSelectClause" type="hidden">
	<input id="topTenWhereClause" name="topTenWhereClause" type="hidden">
</form>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		if(isPostgresDB) {
			whereClause = "FTP_START_TIME >= now()- interval '2 day'";
		} else {
		whereClause = "FTP_START_TIME >= SYSDATE-2";
		}
	}
	String topTenSelectClause = request.getParameter("topTenSelectClause");
	if( topTenSelectClause == null || topTenSelectClause == ""){
		topTenSelectClause="";
	}
	
	String topTenWhereClause = request.getParameter("topTenWhereClause");
	if( topTenWhereClause == null || topTenWhereClause == ""){
		topTenWhereClause="";
	}
%>

<sql:query dataSource="${jspDataSource}" var="result1">
<%=topTenSelectClause %> SELECT SPMB.ID BATCH_ID, SPMB.ISSUER_ID, ISSUER.SHORT_NAME,
CASE
WHEN (SPM.REQUEST_STATUS='F') THEN 'Failed'
WHEN (SPM.REQUEST_STATUS='S') THEN 'Success'
WHEN (SPM.REQUEST_STATUS='P') THEN 'In-Progress'
ELSE 'Unknown'
END as REQUEST_STATUS, 
CASE
WHEN (REQUEST_STATE='B') THEN 'Begin'
WHEN (REQUEST_STATE='E') THEN 'Ended'
WHEN (REQUEST_STATE='P') THEN 'In-Progress'
ELSE 'Waiting'
END as REQUEST_STATE, 
SPMB.FTP_STATUS,
TO_CHAR(SPMB.FTP_START_TIME, 'DD-MON-YY HH24:MI:SS') AS FTP_START_TIME,
TO_CHAR(SPMB.FTP_END_TIME, 'DD-MON-YY HH24:MI:SS') AS FTP_END_TIME,
SPMB.BATCH_STATUS,
TO_CHAR(SPMB.BATCH_START_TIME, 'DD-MON-YY HH24:MI:SS') AS BATCH_START_TIME,
TO_CHAR(SPMB.BATCH_END_TIME, 'DD-MON-YY HH24:MI:SS') AS BATCH_END_TIME,
SPM.SERFF_REQ_ID, REMARKS, REQUEST_STATE_DESC, SPMB.OPERATION_TYPE,
CASE
WHEN (PM_RESPONSE_XML IS NULL) THEN 'Response'
ELSE '<a href="getSTMPlansStatusBottom?serffReqId='||SERFF_REQ_ID||'" target="bottom">Response</a>'
END  AS PM_RESPONSE_XML,
CASE
WHEN (SERFF_REQ_ID IS NULL) THEN ''
ELSE '<a href="${pageContext.request.contextPath}/admin/downloadSTMExcel?serffReqId='||SERFF_REQ_ID||CHR(38)||'type=download">Download</a>'
END AS DOWNLOAD_OPTION,
SPM.ATTACHMENTS_LIST, SPM.PROCESS_IP, SPMB.IS_DELETED, U.EMAIL
FROM SERFF_PLAN_MGMT_BATCH SPMB LEFT JOIN SERFF_PLAN_MGMT SPM ON SPMB.SERFF_PLAN_MGMT_ID = SPM.SERFF_REQ_ID
INNER JOIN ISSUERS ISSUER ON SPMB.ISSUER_ID = ISSUER.HIOS_ISSUER_ID
LEFT JOIN USERS U ON SPMB.USER_ID = U.ID
WHERE OPERATION_TYPE IN ('STM', 'STM_HCC', 'STM_AREA_FACTOR', 'AME_FAMILY',
'PLAN_DOCS', 'PLAN_MEDICAID', 'VISION', 'AME_GENDER', 'AME_AGE', 'LIFE')
AND <%=whereClause%>
ORDER BY SPMB.ID DESC <%=topTenWhereClause %> --%>
</sql:query>

Requests Received by SERFF 
	<%
		if (request.getParameter("filterStatement") != null) {
			out.println(request.getParameter("filterStatement"));
		}
	%>
<br>

<display:table name="${result1.rows}" requestURI="getSTMPlansStatusTop" id="table1" style="white-space: pre-wrap;" export="true" pagesize="10">
	<display:column property="BATCH_ID" title="Batch ID" sortable="false" />
	<display:column property="ISSUER_ID" title="HIOS Issuer ID" sortable="false" />
	<display:column property="SHORT_NAME" title="Carrier" sortable="false" />
	<display:column property="REQUEST_STATUS" title="Status" sortable="false" />
	<display:column style="background-color:#A9E2F3" property="FTP_STATUS" title="FTP Status" sortable="false" />
	<display:column style="background-color:#CEF6D8" property="BATCH_STATUS" title="Batch Status" sortable="false" />
	<display:column property="OPERATION_TYPE" title="Operation" sortable="false" />
	<display:column property="PM_RESPONSE_XML" title="Response" sortable="false" />
	<display:column property="ATTACHMENTS_LIST" title="File Name" sortable="false" />
	<display:column property="SERFF_REQ_ID" title="SERFF Plan Mgmt ID" sortable="false" />
	<display:column property="DOWNLOAD_OPTION" title="Download" sortable="false" />
	<display:column property="REQUEST_STATE" title="State" sortable="false" />
	<display:column property="REQUEST_STATE_DESC" title="State Desc" sortable="false" />
	<display:column property="PROCESS_IP" title="Process IP" sortable="false" />
	<display:column style="background-color:#A9E2F3" property="FTP_START_TIME" title="FTP Start Time" sortable="false" />
	<display:column style="background-color:#A9E2F3" property="FTP_END_TIME" title="FTP End Time" sortable="false" />	
	<display:column style="background-color:#CEF6D8" property="BATCH_START_TIME" title="Batch Start Time" sortable="false" />
	<display:column style="background-color:#CEF6D8" property="BATCH_END_TIME" title="Batch End Time" sortable="false" />
	<%-- <display:column property="REMARKS" title="Remarks" sortable="false" /> --%>
	<display:column property="EMAIL" title="User Email" sortable="false" />
	<display:column property="IS_DELETED" title="IS Deleted" sortable="false" />

	<display:setProperty name="export.xml.filename" value="serffAdmin.xml"/>
	<display:setProperty name="export.csv.filename" value="serffAdmin.csv"/>
	<display:setProperty name="export.excel.filename" value="serffAdmin.xls"/>
</display:table>
