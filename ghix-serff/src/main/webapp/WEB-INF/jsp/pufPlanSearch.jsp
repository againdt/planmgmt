<html>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp"%>

<head>
	<title>Search PUF Plans</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

	<script type="text/javascript">
		window.onload = function() {
			var zip = "<%=request.getParameter("zip")%>";
		    if (zip && zip != "null") {
		    	document.getElementById('zip').value = zip;
			}
		    
			var countyFips = "<%=request.getParameter("countyFips")%>";
		    if (countyFips && countyFips != "null") {
		    	document.getElementById('countyFips').value = countyFips;
			}
		}
		
		function setWhereClause(type) {
	
			var whereClause = '';
			// var filterStatement = '';
			var topTenSelectClause = '';
			var topTenWhereClause = '';
	
			switch (type) {
			case 'B1':
				if(isPostgresDB) {
					whereClause = ' AND CREATION_TIMESTAMP BETWEEN (now()-interval \'1 day\') and now()';
				} else {
				whereClause = ' AND CREATION_TIMESTAMP BETWEEN (SYSDATE-1) AND SYSDATE';
				}
				// filterStatement = ' in last 24 hours';
				break;
			case 'B2':
				if(isPostgresDB) {
					whereClause = ' AND CREATION_TIMESTAMP BETWEEN (now()-interval \'12 hour\') and now()';
				} else {
				whereClause = ' AND CREATION_TIMESTAMP BETWEEN (SYSDATE-1/2) AND SYSDATE';
				}
				// filterStatement = ' in last 12 hours';
				break;
			case 'B3':
				if(isPostgresDB) {
					whereClause = ' AND CREATION_TIMESTAMP BETWEEN (now()-interval \'1 hour\') and now()';
				} else {
				whereClause = ' AND CREATION_TIMESTAMP BETWEEN (SYSDATE-1/24) AND SYSDATE';
				}
				// filterStatement = ' in last 1 hour';
				break;
			case 'B4':
				//Step 1: Get List Box value, if it is not empty
				var zip = document.getElementById('zip').value;
				var countyFips = document.getElementById('countyFips').value;
	
				if (zip) {
					whereClause = " AND (PZAREA.ZIP = "+ zip +" OR PZAREA.ZIP IS NULL)";
				}
	
				if (countyFips) {
					whereClause = " AND PZAREA.COUNTY_FIPS='"+ countyFips +"'";
				}
	
				break;
			case 'B5':
				// whereClause = "start_time between (start_time) and sysdate";
				topTenSelectClause=" SELECT * FROM ( ";
				if(isPostgresDB) {
					topTenWhereClause=" )  allrec limit 10";
				} else {
				topTenWhereClause=" ) WHERE ROWNUM IN (1,2,3,4,5,6,7,8,9,10) ORDER BY ROWNUM";
				}
				// filterStatement = "for latest records";
				// topTenSelectClause = ' Select * from ( ';
				// topTenWhereClause = ' ) where rownum <=10 order by  rownum';
				// filterStatement = 'for latest records';
				break;
			}
			document.getElementById('whereClause').value = whereClause;
			document.getElementById('filterStatement').value = filterStatement;
			document.getElementById('topTenSelectClause').value = topTenSelectClause;
			document.getElementById('topTenWhereClause').value = topTenWhereClause;
		}
	</script>
</head>
<body>
	<form method="POST" action="${pageContext.request.contextPath}/admin/pufPlanSearch">
	<df:csrfToken/>
		<table border="1" style="border-color:#C0C0C0">
			<tbody><tr>
				<!-- <td bgcolor="#C0C0C0">
					<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
					<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
					<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
					<input type="submit" value="Show Latest" name="B5" title="View top 10 requests" onclick="setWhereClause('B5');">
				</td> -->
				<td bgcolor="#66FFCC">
					<p>
						&nbsp;ZIP:
						<input type="text" id="zip" name="zip" size="5" maxlength="5"> County Fips:
						<input type="text" id="countyFips" name="countyFips" size="5" maxlength="5">
						<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
					</p>
				</td>
			</tr></tbody>
		</table>
		<input id="whereClause" name="whereClause" type="hidden" />
		<!-- <input id="filterStatement" name="filterStatement" type="hidden" /> -->
		<input id="topTenSelectClause" name="topTenSelectClause" type="hidden" />
		<input id="topTenWhereClause" name="topTenWhereClause" type="hidden" />
	</form>
	
	<%
		String whereClause = request.getParameter("whereClause");
		if (whereClause == null || whereClause == "") {
			whereClause = " AND (PZAREA.ZIP IS NULL)";
		}

		/* String topTenSelectClause = request.getParameter("topTenSelectClause");
		if (topTenSelectClause == null || topTenSelectClause == "") {
			topTenSelectClause = "";
		}

		String topTenWhereClause = request.getParameter("topTenWhereClause");
		if (topTenWhereClause == null || topTenWhereClause == "") {
			topTenWhereClause = "";
		} */
	%>
	
	<sql:query dataSource="${jspDataSource}" var="pufResults">
		SELECT DISTINCT(PUF.PLAN_ID_STANDARD_COMPONENT) AS PLAN_ID_STANDARD_COMPONENT, PUF.STATE, PUF.RATING_AREA,
		PUF.METAL_LEVEL, PUF.ISSUER_NAME, PUF.PLAN_MARKETING_NAME, PUF.PLAN_TYPE
		FROM PUF_QHP PUF, PM_RATING_AREA PRA, PM_ZIP_COUNTY_RATING_AREA PZAREA
		WHERE PRA.STATE = PUF.STATE
		AND PRA.RATING_AREA = PUF.RATING_AREA
		AND PZAREA.RATING_AREA_ID = PRA.ID
		<%=whereClause%>
		ORDER BY PLAN_ID_STANDARD_COMPONENT
	</sql:query>
	
	<b>Search PUF Plans:</b><br />
	
	<display:table name="${pufResults.rows}" requestURI="pufPlanSearch" id="pufPlanSearch" export="true" style="width:100%">
		<display:column property="PLAN_ID_STANDARD_COMPONENT" title="Plan ID" sortable="true" />
		<display:column property="STATE" title="State" sortable="true" />
		<display:column property="RATING_AREA" title="Rating Area" sortable="true" />
		<display:column property="METAL_LEVEL" title="Metal Level" sortable="true" />
		<display:column property="ISSUER_NAME" title="Issuer Name" sortable="true" />
		<display:column property="PLAN_MARKETING_NAME" title="Marketing Name" sortable="true" />
		<display:column property="PLAN_TYPE" title="Plan Type" sortable="true" />

		<display:setProperty name="export.xml.filename" value="pufPlans.xml" />
		<display:setProperty name="export.csv.filename" value="pufPlans.csv" />
		<display:setProperty name="export.excel.filename" value="pufPlans.xls" />
	</display:table>
	
	<b>Query:</b><br />
	SELECT DISTINCT(PUF.PLAN_ID_STANDARD_COMPONENT) AS PLAN_ID_STANDARD_COMPONENT, PUF.STATE, PUF.RATING_AREA, PUF.METAL_LEVEL, PUF.ISSUER_NAME, PUF.PLAN_MARKETING_NAME, PUF.PLAN_TYPE
	FROM PUF_QHP PUF, PM_RATING_AREA PRA, PM_ZIP_COUNTY_RATING_AREA PZAREA
	WHERE PRA.STATE = PUF.STATE AND PRA.RATING_AREA = PUF.RATING_AREA AND PZAREA.RATING_AREA_ID = PRA.ID <%=whereClause%>
	ORDER BY PLAN_ID_STANDARD_COMPONENT
	<br /><br />
</body>
</html>
