<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="session.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Bulk Upload Issuer LOGO</title>
		
		<script type="text/javascript" charset="utf-8">
		
		</script>
	</head>
<body>
	<p align="center">Issuer Logo Bulk Upload</p>
	<p align="center">&nbsp;</p>
	<div align="center">
		<center>
			<form action="${pageContext.request.contextPath}/admin/processIssuerLogoBulkUpdate" onsubmit="document.getElementById('submit').disabled=true;" method="POST">
				<table border="0" cellpadding="0" cellspacing="0" width="50%">
					<tr>
						<td align="center" style="font-size: 14pt;">
								<select id = "issuerLogo" name ="issuerLogo" onchange="document.getElementById('folderName').value=this.options[this.selectedIndex].text">>
									<option value ="" selected = selected>Select Folder</option>
									<c:if test="${folderList != null}">
										<c:forEach var ="issuerItem" items= "${folderList}">
											<option value ="${ issuerItem}">${ issuerItem}</option>
										</c:forEach>
									</c:if>
								</select>
								<input type ="hidden" id ="folderName" name ="folderName" />
						</td>
					</tr>
					<tr>
						<td>
							<df:csrfToken/>
							<p align="center">
								<input type="submit" id="submit" name="submit" value="Process" style="font-size: 12pt; color: #0066CC">
							</p>							
						</td>
					</tr>
					<c:if test="${msgbulkupdate != null}">
						<tr>
							<td align="center" style="font-size: 14pt;">
								<c:out value="${msgbulkupdate}" />
							</td>
						</tr>
					</c:if>
				</table>
			</form>
		</center>
	</div>
	<div style="font-size: 12pt;">
		Notes:
		<br> Please upload folder to process with prefix LOGO_
		<br> Please upload one excel file per folder with HIOS Issuer Id and logo file mapping along with physical logo files:
		<br>
		<b>Clicking above button will </b>
		<br>1. Reads file from FTP Location from specific folder
		<br>2. Upload file to ECM and get ECM Id
		<br>3. Update Issuer LOGO with file ECM Id
		<br>4. Display success / error message	
		<br>
	</div>
	
</body>
</html>
