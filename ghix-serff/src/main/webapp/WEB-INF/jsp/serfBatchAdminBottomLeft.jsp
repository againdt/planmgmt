<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="datasource.jsp" %>

<head>
	<base target="batchmain_1">
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
</head>

File Names
 <% 
String req_Id=request.getParameter("fileid");
%>
<%
if(null != req_Id ){
	
%>
Files for ID: <%=req_Id %>
<table class="table" style="white-space: pre-wrap;width: 100%;" border =1>
                    <thead>
	                    <tr class="graydrkbg">                                                                          
	                        <th scope="col">Files</th>
	                    </tr>
</thead>
<tbody class="msg">
		<c:choose>
			<c:when test="${FILE_NAME != null}">
				<c:forEach var="file" items="${FILE_NAME}">
				<tr><td><a href="getBatchBottomRight?folderName=${FOLDER_NAME}&fileName=${file}&type=xml">${file}</a></td></tr>
				</c:forEach>
			</c:when>
		</c:choose>
</tbody>
</table>

<% } %>