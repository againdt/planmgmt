<%@ page import="com.getinsured.hix.model.Plan"%>

<%@ include file="datasource.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<html>
<head>
	<title>Quotit Services Status</title>
	<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />

	<script type="text/javascript">

		window.onload = function() {
			var parentTrackingID = "<%=request.getParameter("parentTrackingID")%>";
		    if (parentTrackingID && parentTrackingID != "null") {
		    	document.getElementById('parentTrackingID').value = parentTrackingID;
			}

			var externalRequestID = "<%=request.getParameter("externalRequestID")%>";
		    if (externalRequestID && externalRequestID != "null") {
		    	document.getElementById('externalRequestID').value = externalRequestID;
			}

			var jobStatus = "<%=request.getParameter("jobStatus")%>";
		    if (jobStatus && jobStatus != "null") {
		    	document.getElementById('jobStatus').value = jobStatus;
			}
		};

		function setWhereClause(type) {
			var whereClause = "";
			var filterStatement = "";

			switch (type) {
				case "B4" : 
					// Validate the dates
				    // validateDates();
				    // Step 1: Get List Box value, if it is not empty
					var parentTrackingID = document.getElementById("parentTrackingID").value;
					var externalRequestID = document.getElementById("externalRequestID").value;
					var jobStatus = document.getElementById("jobStatus").value;

					if (parentTrackingID) {
						whereClause += " AND ID LIKE '"+ parentTrackingID + "%' ";
					}

					if (externalRequestID) {
						whereClause += " AND EXTERNAL_REQ_ID LIKE '"+ externalRequestID + "%' ";
					}

					if (jobStatus) {
						whereClause += " AND JOB_STATUS LIKE '"+ jobStatus + "%' ";
					}
					break;
			}
			document.getElementById("whereClause").value = whereClause; // escape(whereClause);
			document.getElementById("filterStatement").value = filterStatement;
		}
	</script>
</head>

<body>
	<b>Quotit Services Status [HIX-66514]</b>
	<form method="POST" id="frmQuotitServicesStatus" name="frmQuotitServicesStatus" action="${pageContext.request.contextPath}/admin/loadQuotitServicesStatus">
		<df:csrfToken/>
		<table border="1" cellspacing="0" style="border-color:#C0C0C0">
			<tbody><tr>
				<td bgcolor="#66FFCC">
					<p>
						<label for="parentTrackingID" class="control-label">&nbsp;<b>Parent Tracking ID:</b></label>
						<input type="text" id="parentTrackingID" name="parentTrackingID" size="10" maxlength="10" />

						<label for="externalRequestID" class="control-label">&nbsp;<b>External Request ID:</b></label>
						<input type="text" id="externalRequestID" name="externalRequestID" size="10" maxlength="10" />

						<label for="jobStatus" class="control-label">&nbsp;<b>Job Status:</b></label>
						<select id="jobStatus" name="jobStatus">
							<option value="" selected="selected">Select</option>
							<option value="COMPLETE">Complete</option>
							<option value="PENDING">Pending</option>
						</select>
					</p>
				</td>
				<td bgcolor="#66FFCC">
					<p>
						<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
						<button type="reset" value="Reset">Reset</button>
					</p>
				</td>
			</tr>
		</tbody></table>
		<input id="whereClause" name="whereClause" type="hidden">
		<input id="filterStatement" name="filterStatement" type="hidden">
	</form>
<%
	String whereClause = request.getParameter("whereClause");

	if (whereClause == null || 0 == whereClause.trim().length()) {
		whereClause = "";
	}
%>
	<sql:query dataSource="${jspDataSource}" var="quotitServiceResults">
		SELECT ID, '<a href="loadQuotitRequestResponse?parentTrackingID='||ID||'">'||ID||'</a>' AS QID,
			EXTERNAL_REQ_ID, END_POINT, START_TIME, JOB_STATUS, REQUEST_PARAMS FROM (
			(SELECT ID, EXTERNAL_REQ_ID, END_POINT, START_TIME, 'COMPLETE' AS JOB_STATUS, REQUEST_PARAMS FROM EXT_WS_CALL_LOG E1
			WHERE E1.METHOD_NAME = 'GetCarriersPlansBenefits-Carriers'
			AND EXISTS (SELECT * FROM EXT_WS_CALL_LOG E2 WHERE E2.BATCH_STATE='END' AND E2.PARENT_REQ_ID=E1.ID))
			UNION
			(SELECT ID, EXTERNAL_REQ_ID, END_POINT, START_TIME, 'PENDING' AS JOB_STATUS, REQUEST_PARAMS FROM EXT_WS_CALL_LOG E1
			WHERE E1.METHOD_NAME = 'GetCarriersPlansBenefits-Carriers'
			AND NOT EXISTS (SELECT * FROM EXT_WS_CALL_LOG E2 WHERE E2.BATCH_STATE='END' AND E2.PARENT_REQ_ID=E1.ID))
		) allRec WHERE JOB_STATUS IS NOT NULL <%=whereClause%> ORDER BY ID DESC
	</sql:query>

	<display:table name="${quotitServiceResults.rows}" id="quotitServiceResultsTable" requestURI="loadQuotitServicesStatus" pagesize="10"
		style="white-space: pre-wrap;width: 100%;" export="true">
		<display:column property="QID" title="Quotit ID" sortable="true" />
		<display:column property="EXTERNAL_REQ_ID" title="External Request ID" sortable="true" />
		<display:column property="END_POINT" title="End Point" sortable="true" />
		<display:column property="START_TIME" title="Start Time" sortable="true" />
		<display:column property="JOB_STATUS" title="Job Status" sortable="true" />
		<display:column property="REQUEST_PARAMS" title="Request Parameters" sortable="true" />

		<display:setProperty name="export.xml.filename" value="quotitServiceResults.xml" />
		<display:setProperty name="export.csv.filename" value="quotitServiceResults.csv" />
		<display:setProperty name="export.excel.filename" value="quotitServiceResults.xls" />
	</display:table>
</body>
</html>