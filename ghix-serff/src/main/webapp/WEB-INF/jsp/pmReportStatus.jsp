<html>
<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>PM Report Status</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	<script type="text/javascript">
		
		$(function() {
			$( "#createdDate" ).datepicker().attr( 'readOnly' , 'true' );
		});
			
		function setWhereClause(type){
			
			var whereClause = '';
			var filterStatement = '';
			var isFiltered = false;
			switch (type) {	
				case 'B' :		   
					//Step 1: Get the Status Value
					var status = document.getElementById('status').value;			
					if(status)
					{		
							whereClause = whereClause  + " and pmreport.status = '" + status + "'";	
						    isFiltered = true;
							if(!filterStatement){
								   filterStatement = " Report Status = " + status ;
							   }else{
								   filterStatement = filterStatement + " , Report Status = " + status ;
							 } 
					}
					
					//Step 2: Get the Status Value
					var planids = document.getElementById('planid').value;
					if(planids)
					{		
						   if(isNaN(planids)){
							  alert("Enter valid plan id.");
						   }else{
							   whereClause = whereClause  + " and pmreport.plan_ids like '%" + planids + "%'";		
							   isFiltered = true;
							   if(!filterStatement){
								   filterStatement = " Plan id  " + planids ;
							   }else{
								   filterStatement = filterStatement + " , Plan id = " + planids ;
							  } 
							  
						   }					
					}
					
					//Step 3: Get the hiosid Value
					var hiosid = document.getElementById('hiosid').value;
					if(hiosid)
					{		
						  if(isNaN(hiosid)){
							  alert("Enter valid hiosid.");
						   }else{
							   whereClause = whereClause  + " and hios_issuer_id =" + hiosid;
							   isFiltered = true;
							   if(!filterStatement){
								   filterStatement = " Hios Id =  " + hiosid ;
							   }else{
								   filterStatement = filterStatement + " , Hios Id = " + hiosid ;
							  } 
						   }					
					}
					
					//Step 4: Get the Report Type Value
					var reportType = document.getElementById('reportType').value;	
					if(reportType)
					{		
							whereClause = whereClause  + " and pmreport.report_type = '" + reportType + "'";
							isFiltered = true;
							if(!filterStatement){
								   filterStatement = " Report Type =  " + reportType ;
							   }else{
								   filterStatement = filterStatement + " , Report Type = " + reportType ;
						    } 
					}
					
					//Step 5: Get the exception Value
					var exception = document.getElementById('exception').value;	
					if(exception)
					{		
						whereClause = whereClause  + " and pmreport.report_exception " + exception;
						isFiltered = true;
						if(!filterStatement){
							   filterStatement = " Exception " + exception ;
						   }else{
							   filterStatement = filterStatement + " , Exception " + exception ;
					    } 
					}
					
					//Step 6: Get the created date.
					var createDate = document.getElementById('createdDate').value;	
					if(createDate)
					{		
						whereClause = whereClause+" and to_char(pmreport.creation_timestamp  ,'mm/dd/yyyy' )='"+createDate+"'" ;
						isFiltered = true;
						if(!filterStatement){
							   filterStatement = " Created On =  " + createDate ;
						   }else{
							   filterStatement = filterStatement + " , Created On =  " + createDate ;
					    }
					}			
				
					if(isFiltered){
						filterStatement = " Searching with " + filterStatement ;
					}else{
						filterStatement = " Searching with dafault filter." ;
					}
					//alert(filterStatement);
			}
			
			document.getElementById('whereClause').value = whereClause;
			document.getElementById('filterStatement').value = filterStatement;
		    //alert('whereClause==>'+whereClause);
		}
	</script>
</head>
<body>
<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = " and pmreport.id is not null ";
	}	
%>
<sql:query dataSource="${jspDataSource}" var="result1">
select pmreport.id id,pmreport.plan_ids plan_ids,issuer.hios_issuer_id issuer_id,pmreport.status status, 
	decode(pmreport.report_type, 'S', 'SDP', 'Q', 'QHP') report_type,
	case
	when pmreport.ecm_doc_id is null THEN ''
	else '<a href="/exchange-hapi/admin/serff/filedownloadbyid?documentId='||pmreport.ecm_doc_id||'">Download</a>'
	end as ecm_doc_id
,pmreport.ecm_doc_name ecm_doc_name,pmreport.report_exception report_exception,pmreport.is_deleted is_deleted,usr.first_name||' '||usr.last_name created_by,
to_char(pmreport.creation_timestamp,'DD-MON-YY HH24:MI:SS') as creation_timestamp, to_char(pmreport.last_update_timestamp,'DD-MON-YY HH24:MI:SS') as last_update_timestamp,pmreport.deleted_by
deleted_by from pm_plan_comp_report pmreport, issuers issuer, users usr where issuer.id=pmreport.id and usr.id=pmreport.created_by <%=whereClause%>
order by pmreport.id desc
</sql:query> 
<b>PM Plan Compare Report Status</b>
 	
<br>
<form method="POST" action="pmReportStatus">
<df:csrfToken/>
		<table>
			<tr>
				<td>
				Date: <input type="text" id="createdDate" name="createdDate" size="14" maxlength="14"> 
				
				Plan Id: <input type="text" id="planid" name="planid" size="14" maxlength="14"> 
				
				Issuer HIOS Id: <input type="text" id="hiosid" name="hiosid" size="14" maxlength="14">
				</br>
				Exception: <select size="1" id="exception" name="exception" style="width:120px">
						<option value="" selected=""></option>
						<option value="is null" >Null</option>
						<option value="is not null" >Not Null</option>
					</select>
					
					Report Type: <select size="1" id="reportType" name="reportType" style="width:120px">
						<option value="" selected=""></option>
						<option value="Q" >QHP</option>
						<option value="S" >SADP</option>
					</select>
					
					Status: <select size="1" id="status" name="status" style="width:120px">
				        <option value="" selected=""></option>
						<option value="COMPLETE">Completed</option>
						<option value="FAILED">Failed</option>
						<option value="IN_PROGRESS">In Progress</option>
				</select>
				<input type="submit" value="Search" name="B" title="Filter requests based on field value" onclick="setWhereClause('B');">
				</td>							
			</tr>
			
		</table>		
		<input id="whereClause" name="whereClause" type="hidden"/>
		<input id="filterStatement" name="filterStatement" type="hidden">
	</form>
<% 
		if(request.getParameter("filterStatement") != null){
			out.println("<b>" + request.getParameter("filterStatement") + "</b><br><br>");
		}
%>	
<display:table name="${result1.rows}"  requestURI="pmReportStatus" id="table1" export="true" pagesize="500" style="width:80%" defaultorder="descending" defaultsort="4">
	<display:column property="id" title="Id" sortable="true" />
	<display:column property="plan_ids" title="Plan" sortable="true"/>
	<display:column property="issuer_id" title="Issuer ID" sortable="true"/>
	<display:column property="status" title="Status" sortable="true"  />
	<display:column property="report_type" title="Report Type" sortable="true"/>
	<display:column property="ecm_doc_id" title="Download Report" sortable="false"/>
	<display:column property="ecm_doc_name" title="Report Name" sortable="true"/>	
	<display:column property="report_exception" title="Exception" sortable="true"/>	
	<display:column property="created_by" title="Created By" sortable="true"/>	
	<display:column property="creation_timestamp" title="Created On" sortable="true"/>	
	<display:column property="last_update_timestamp" title="Updated On" sortable="true"/>	
</display:table>
</body> 
</html>