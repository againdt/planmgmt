<%@ page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ page import="com.serff.util.SerffConstants"%>
<%@ include file="datasource.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<html>
<head>
	<title>Display Zip Codes without plans</title>
	<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>" />
</head>

<body>
	<b>Display Zip Codes without plans [HIX-51464]</b>

<%
	String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
	String whereClause = "";
	
	if (!SerffConstants.PHIX_PROFILE.equalsIgnoreCase(stateCode)) {
		whereClause = "AND STATE = '"+ stateCode +"' ";
	}
%>

	<sql:query dataSource="${jspDataSource}" var="zipResults">
		SELECT ZIPCODE, COUNTY_FIPS, COUNTY, CITY 
		FROM ZIPCODES WHERE ZIPCODE NOT IN ( 
		SELECT DISTINCT ZIP FROM PM_ZIP_PLANS)
		<%=whereClause%>
		ORDER BY ZIPCODE,COUNTY_FIPS
	</sql:query>

	<display:table name="${zipResults.rows}" id="zipResultsTable"
		requestURI="getZipWithoutPlans" defaultsort="1" pagesize="100"
		style="white-space: pre-wrap;width: 100%;" export="true">
		<display:column property="ZIPCODE" title="Zip Code" sortable="true" />
		<display:column property="COUNTY_FIPS" title="Fips" sortable="true" />
		<display:column property="COUNTY" title="County" sortable="true" />
		<display:column property="CITY" title="City" sortable="true" />
		<display:setProperty name="export.xml.filename" value="zipWithoutPlans.xml" />
		<display:setProperty name="export.csv.filename" value="zipWithoutPlans.csv" />
		<display:setProperty name="export.excel.filename" value="zipWithoutPlans.xls" />
	</display:table>
</body>
</html>