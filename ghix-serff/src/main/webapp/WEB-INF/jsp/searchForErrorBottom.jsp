<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="datasource.jsp" %>

<html>

<head>
	<title>Search for plans</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<%
String [] array=request.getParameterValues("checkbox");
String whereClause =null;
if(array!=null){
	StringBuilder createWhere= new StringBuilder("module_name in (") ;
	for(int i=0 ;i<array.length;i++){
		String value = array[i];
		createWhere.append("'"+value +"',");
		
	}
	 whereClause =createWhere.substring(0,createWhere.length()-1);

	whereClause=whereClause+ ")";
}else{
	whereClause="code_id is not null";
}


%>

<sql:query dataSource="${jspDataSource}" var="result1">
select CODE_ID,ERROR_CODE, MODULE_NAME, MESSAGE ,to_char(INSERT_DATE, 'DD-Mon-YYYY HH24:MI:SS') as date1
from GI_ERROR_CODE where <%=whereClause %>
</sql:query>

<display:table name="${result1.rows}" requestURI="/admin/searchForErrorBottom" id="table1" style="white-space: pre-wrap;" export="true" pagesize="50">
	<display:column property="CODE_ID" title="Code ID" sortable="true" />
	<display:column property="ERROR_CODE" title="Error Code" sortable="true" />
	<display:column property="MODULE_NAME" title="Moduel Name" sortable="true" />
	<display:column property="MESSAGE" title="Error Messgae" sortable="true" />
	<display:column property="date1" title="Date" sortable="true" />
	<display:setProperty name="export.xml.filename" value="ErrorPlans.xml"/>
	<display:setProperty name="export.csv.filename" value="ErrorPlans.csv"/>
	<display:setProperty name="export.excel.filename" value="ErrorPlans.xls"/>
</display:table> 
</html>