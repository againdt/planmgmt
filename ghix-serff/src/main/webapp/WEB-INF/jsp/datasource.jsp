<%@ page import = "java.util.*" %>
<%@page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%
//Session Checking Begin -------------------------------------------------
session = request.getSession(false);
if(null == session)
{
	response.sendRedirect("index.jsp");
}
else
{
	String loggedin = (String)session.getAttribute( "loggedin");
	if(!"true".equals(loggedin))
	{		
		session.invalidate();	
		response.sendRedirect("../index.jsp");
	}
}
//Session Checking End -------------------------------------------------
%>

<%!
	private static ApplicationContext ctx = null;
	private static Properties prop = null;
	private static String dbType = null;
	private static boolean isPostgresDB = false;
%>

<%
	if (ctx == null) {
		ctx = org.springframework.web.servlet.support.RequestContextUtils.getWebApplicationContext(request, application);
	}
	if (prop == null) {
		prop = (Properties) ctx.getBean("configProp");
		dbType = prop.getProperty("database.type");
		isPostgresDB = "POSTGRESQL".equalsIgnoreCase(dbType);
	}
%>
<sql:setDataSource var="jspDataSource" dataSource="jdbc/ghixDS"/>
