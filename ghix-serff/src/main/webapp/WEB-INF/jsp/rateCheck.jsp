<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Check Plan Rate</title>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="/resources/demos/style.css" />
<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
<script>
	function validateInput() {
	   var minRate = document.getElementById("minRate").value;
	   var maxRate = document.getElementById("maxRate").value;
	   if(isNaN(minRate)){
		   alert("Please enter valid 'Min Rate' value.");
		   document.getElementById("minRate").value = "";
		   return false;
	   }else if(isNaN(maxRate)){
		   alert("Please enter valid 'Max Rate' value.");
		   document.getElementById("maxRate").value = "";
		   return false;
	   }
	}
</script>
</head>
<body>
<%
    String minRateParam = request.getParameter("minRate");
    String maxRateParam = request.getParameter("maxRate");
    int minRate = 1;
    int maxRate = 5000;
    if(null != minRateParam && minRateParam.length() > 0){
    	minRate = Integer.parseInt(request.getParameter("minRate"));
    }

    if(null != maxRateParam && maxRateParam.length() > 0){
    	maxRate = Integer.parseInt(request.getParameter("maxRate"));
    }
%>

<form method="POST" action="searchPlanRate">       
<table>
	<tr>
    	<td width="100%"><font size="4">
    	<b>Check Plans Rate for below min rate and above max rate: </b>
    	<br>It checks only valid Plan Rates
    	</font></td>
    </tr>
    <tr>
    	<td width="100%"><font size="4"></font></td>
    </tr>
	<tr>
		<td>
		Min Rate: <input type="text" id="minRate" name="minRate" size="5" maxlength="5" value='1'> 
		Max Rate: <input type="text" id="maxRate" name="maxRate" size="5" maxlength="5" value='5000'> 
		<input type="submit" value="Search" name="rateSearch" title="Filter requests based on field value" onclick="validateInput();">
		</td>							
	</tr>
</table>		
</form>

</body>

<%

if( (minRate > 0) || (maxRate > 0)){
	%>
	<sql:query dataSource="${jspDataSource}" var="result1">
	select distinct p.ISSUER_PLAN_NUMBER as issuerPlanNumber, p.name as planName,i.HIOS_ISSUER_ID as issuerId,i.name as issuerName, ppr.rate as planRate, p.available_for as availableFor,p.insurance_type as planType  ,
	ppr.MIN_AGE,ppr.MAX_AGE,ppr.RATING_AREA_ID, ppr.TOBACCO,  ppr.EFFECTIVE_START_DATE, ppr.EFFECTIVE_END_DATE, 
	to_char(ppr.EFFECTIVE_START_DATE,'mm/dd/yyyy' ) as SD,
	to_char(ppr.EFFECTIVE_END_DATE,'mm/dd/yyyy' ) as ED
	from plan p, issuers i, pm_plan_Rate ppr 
	where p.issuer_id = i.id 
	and ppr.plan_id = p.id 
	and p.STATUS = 'CERTIFIED' 
	and p.ISSUER_VERIFICATION_STATUS = 'VERIFIED' 
	and p.IS_DELETED='N' 
	and i.CERTIFICATION_STATUS='CERTIFIED' 
	and ppr.is_deleted= 'N' 	
	<% 
      if((minRate > 0) && (0 == maxRate)){
    %>	  
    	  and (ppr.rate <= <%=minRate%>)
    <% 	   
      }else if((maxRate > 0) && (0 == minRate)){
    %>	  
    	  and (ppr.rate >= <%=maxRate%>)
    <%  
      }else if((minRate > 0) && (maxRate > 0)){
    %>
		 and (ppr.rate <= <%=minRate%> or ppr.rate >= <%=maxRate%>)
    <%	  
      }
	%>
	order by ppr.rate
	</sql:query> 
	<%
}

%>
		
<display:table name="${result1.rows}"  requestURI="planRateCheck" id="table1" export="true"  style="width:100%" defaultorder="descending" pagesize="20">
<display:column property="issuerPlanNumber" title="Issuer Plan Number"  />
<display:column property="planName" title="Plan Name"  />
<display:column property="issuerId" title="Issuer ID"  />
<display:column property="issuerName" title="Issuer"  />
<display:column property="planRate" title="Plan Rate"  />
<display:column property="availableFor" title="Available For"  />
<display:column property="planType" title="Plan Type"  />

<display:column property="MIN_AGE" title="Min Age"  />
<display:column property="MAX_AGE" title="Max Age"  />
<display:column property="SD" title="Effective Start"  />
<display:column property="ED" title="Effective End"  />

<display:setProperty name="export.xml.filename" value="PlanRateData.xml"/>
<display:setProperty name="export.csv.filename" value="PlanRateData.csv"/>
<display:setProperty name="export.excel.filename" value="PlanRateData.xls"/>
</display:table>

</html>