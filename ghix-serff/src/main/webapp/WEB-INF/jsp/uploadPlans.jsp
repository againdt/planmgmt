<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="session.jsp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Upload Plans to SERFF</title>
	</head>
<body>
	<p align="center">Upload Plans to SERFF</p>
	<p align="center">&nbsp;</p>
	<div align="center">
		<center>
			<table border="0" cellpadding="0" cellspacing="0" width="50%">
				<tr>
					<td align="center" style="font-size: 14pt;">
						Batch Job is running every 5 minutes to load the files to SERFF. Click on this button if you want to initiate without waiting for Batch.
					</td>
				</tr>
				<tr>
					<td>
						<form action="${pageContext.request.contextPath}/admin/executeSerffBatch" onsubmit="document.getElementById('submit').disabled=true;" method="POST">
						<df:csrfToken/>
							<p align="center">
								<input type="submit" id="submit" name="submit" value="Upload Plans to SERFF" style="font-size: 12pt; color: #0066CC">
							</p>
						</form>
						<p align="center">&nbsp;</p>
					</td>
				</tr>
				<c:if test="${executeBatchMessage != null}">
					<tr>
						<td align="center" style="font-size: 14pt;">
							<c:out value="${executeBatchMessage}" />
						</td>
					</tr>
				</c:if>
			</table>
		</center>
	</div>
</body>
</html>
