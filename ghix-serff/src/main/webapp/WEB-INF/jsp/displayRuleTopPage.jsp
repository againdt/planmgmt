<%@page import="org.springframework.util.StringUtils"%>
<%@page import="org.apache.poi.util.StringUtil"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Plan Display Rules</title>
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<link rel="stylesheet" href="/resources/demos/style.css" />
<link rel="stylesheet" href="../resources/css/displaytag_serff.css"
	type="text/css" charset="utf-8" />
<script>
	function validateInput() {
		resetPage();
		var hiosIssuerId = document.getElementById("hiosIssuerIdInput").value;
		if (isNaN(hiosIssuerId) || hiosIssuerId.length != 5) {
			alert("Please provide valid 'HIOS Issuer Id'");
			document.getElementById("hiosIssuerIdInput").value = "";
			return false;
		} else {
			document.getElementById("hiosIssuerId").value = hiosIssuerId;
			return true;
		}
	}
	
	function setValues(ruleId, hiosIssuerId, tileDisplayTemplate, standardDisplayTemplate, primaryCareT1ForNumCopay, primaryCareT1ForNumVisits, primaryCareT1ForCopayAndVisits, copayValue,
			copayAttribute, coinsuranceValue, coinsuranceAttribute){
		parent.main_1.document.saveDisplayRuleForm.ruleId.value=ruleId;
		parent.main_1.document.saveDisplayRuleForm.hiosIssuerId.value=hiosIssuerId;
		parent.main_1.document.saveDisplayRuleForm.tileDisplayTemplate.value=tileDisplayTemplate;
		parent.main_1.document.saveDisplayRuleForm.standardDisplayTemplate.value=standardDisplayTemplate;
		parent.main_1.document.saveDisplayRuleForm.primaryCareT1ForNumCopay.value=primaryCareT1ForNumCopay;
		parent.main_1.document.saveDisplayRuleForm.primaryCareT1ForNumVisits.value=primaryCareT1ForNumVisits;
		parent.main_1.document.saveDisplayRuleForm.primaryCareT1ForCopayAndVisits.value=primaryCareT1ForCopayAndVisits;
		parent.main_1.document.saveDisplayRuleForm.copayValue.value=copayValue;
		parent.main_1.document.saveDisplayRuleForm.copayAttribute.value=copayAttribute;
		parent.main_1.document.saveDisplayRuleForm.coinsuranceValue.value=coinsuranceValue;
		parent.main_1.document.saveDisplayRuleForm.coinsuranceAttribute.value=coinsuranceAttribute;
		parent.main_1.document.getElementById("saveRuleButton").disabled = false;
		return false;
	}
	
	function cancelCopy() {
		parent.topframe.document.forms.displayRuleTopPageForm.action = "getDisplayRuleTopPage";
		parent.topframe.document.forms.displayRuleTopPageForm.hiosIssuerId.value = null;
		parent.topframe.document.forms.displayRuleTopPageForm.submit();
		return true;
	}

	function resetPage() {
		parent.main_1.document.saveDisplayRuleForm.reset();
		parent.main_1.document.saveDisplayRuleForm.ruleId.value="";
		parent.main_1.document.saveDisplayRuleForm.hiosIssuerId.value="";
		parent.main_1.document.getElementById("saveRuleButton").disabled = true;
	}
	
	function copyRules() {
		resetPage();
		if(document.getElementById("copyFromIssuerId").value == "" ) {
			alert("Please select a 'HIOS Issuer Id' to copy rules from.");
			return false;
		} else {
			parent.topframe.document.forms.displayRuleTopPageForm.submit();
			return true;
		}
	}
</script>
</head>
	<c:if test="${showCopyForm != 'true'}">
		<form method="POST" action="getDisplayRuleTopPage" id="displayRuleTopPageForm" >
			<table width="100%">
				<tr>
					<td>
						<table width="100%">
							<tr>
								<td width="100%">
									<font size="4"> <b>Get Plan Display rules for issuer</b></font>
								</td>
							</tr>
							<tr height="10px">
								<td width="100%">&nbsp;</td>
							</tr>
							<tr>
								<td>Hios Issuer Id: <input type="text" id="hiosIssuerIdInput"
									name="hiosIssuerIdInput" size="5" maxlength="5"> <input
									type="submit" value="Load Display Rules" name="getDisplayRules"
									title="Get Plan Display rules for issuer."
									onclick="return validateInput();">
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<input id="hiosIssuerId" name="hiosIssuerId" type="hidden"/>
		</form>
		<display:table name="${planDisplayRule}" requestURI="getDisplayRuleTopPage" id="dispRule" list="dispRule" export="true" style="width:100%" defaultorder="descending" >
			<display:column property="hiosIssuerId" title="Hios Issuer Id" />
			<display:column property="copayValue" title="Copay Value" escapeXml="true" sortable="true"/>
			<display:column property="copayAttribute" title="Copay Atribute" sortable="true" escapeXml="true"/>
			<display:column property="coinsuranceValue" title="Coinsurance Value" sortable="true" escapeXml="true"/>
			<display:column property="coinsuranceAttribute" title="Coinsurance Attribute" sortable="true" escapeXml="true"/>
			<display:column property="tileDisplayTemplate" title="Tile Display"  escapeXml="true"/>
			<display:column property="standardDisplayTemplate" title="Standard Display"  escapeXml="true"/>
			<display:column property="primaryCareT1ForNumCopay" title="Primary Care Tier1 Display For Number Of Copay"  escapeXml="true"/>
			<display:column property="primaryCareT1ForNumVisits" title="Primary Care Tier1 Display For Number Of Visits"  escapeXml="true"/>
			<display:column property="primaryCareT1ForCopayAndVisits" title="Primary Care Tier1 Display For Copay And Visits"  escapeXml="true"/>
			<display:column property="creationTimestamp" title="Creation Timestamp" />
			<display:column>
      			<a href="#" onclick="return setValues('${dispRule.id}','${dispRule.hiosIssuerId}','${dispRule.tileDisplayTemplate}',
      			'${dispRule.standardDisplayTemplate}','${dispRule.primaryCareT1ForNumCopay}','${dispRule.primaryCareT1ForNumVisits}',
      			'${dispRule.primaryCareT1ForCopayAndVisits}','${dispRule.copayValue}','${dispRule.copayAttribute}',
      			'${dispRule.coinsuranceValue}','${dispRule.coinsuranceAttribute}');">Edit</a>
			</display:column>
			<display:setProperty name="export.xml.filename" value="DisplayRuleData.xml" />
			<display:setProperty name="export.csv.filename" value="DisplayRuleData.csv" />
			<display:setProperty name="export.excel.filename" value="DisplayRuleData.xls" />
		</display:table>                        
	</c:if>
	<c:if test="${showCopyForm == 'true'}">
		<form method="POST" action="copyDisplayRules" id="displayRuleTopPageForm" >
			<table width="100%">
				<tr height="50px">
					<td  colspan="2">&nbsp;</td>
				</tr>
				<tr width="100%">
					<td colspan="2">
						<font face="Arial" size="4">Issuer specific display rules are not present for the Issuer ${hiosIssuerId}</font>
					</td>
				</tr>
				<tr>
					<td  colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td width="40%" nowrap><font face="Arial" size="4">Copy the display rules from the selected HIOS ID Issuer:</font></td>
					<td>
						<select size="1"  id="copyFromIssuerId" name="copyFromIssuerId">
							<option value="">Select</option>
							<c:forEach var="issuerId" items="${issuerIdsHavingActiveRules}">
								<c:if test="${issuerId == '00000'}">
									<option value="${issuerId}" SELECTED>${issuerId}</option>
								</c:if>
								<c:if test="${issuerId != '00000'}">
									<option value="${issuerId}">${issuerId}</option>
								</c:if>
							</c:forEach>
						</select>
					</td>
				</tr>
				<tr height="30px">
					<td  colspan="2"><font face="Arial" size="4" color="red">${ErrorMessage}</font></td>
				</tr>
				<tr>
					<td width="40%" style="text-align:right"><input type="submit" value="Cancel" onclick="return cancelCopy();" align="center"/></td>
					<td><input type="submit" value="Save" onclick="return copyRules();"/></td>
				</tr>
			</table>
			<input id="hiosIssuerId" name="hiosIssuerId" type="hidden" value="${hiosIssuerId}"/>
		</form>
	</c:if>
</html>