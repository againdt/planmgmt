<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="session.jsp" %>

<html lang="en">
<head>
<link rel="stylesheet"  type="text/css" href="../resources/css/bootstrap.css"/>
<link rel="stylesheet"  type="text/css" href="../resources/css/ghixcustom.css"/>

<script type="text/javascript" src="../resources/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../resources/js/bootstrap-filestyle.js"></script>
<script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script>

<script type="text/javascript">

	$(document).ready(function() {
		$(":file").filestyle({'buttonText': 'Browse ...'});
	});
	
	function doSubmit(formName, url) {
		
		if (isValidateForm() && formName && url) {
			document.forms[formName].method = "post";
			document.forms[formName].action = url;
			document.forms[formName].submit();
		}
	}
	
	function isValidateForm() {
		
		var flag = true;
		var loForm = document.forms["frmUploadDocument"];
		
		if (loForm) {
			
			var laFields = new Array();
			var li = 0;
			var fileUploadNode = loForm["fileUploadDocument"];
			
			if (fileUploadNode.files.length == 0) {
				laFields[li++] = "Please select an Ancillary Document (AME/STM/HCC PDF) file to upload.";
			}
			else if (!fileUploadNode.files[0].type || fileUploadNode.files[0].type != 'application/pdf') {
				laFields[li++] = "File type must be PDF.";
			}
			
			if (laFields.length > 0) {
			    
				if(laFields.length > 1) {
					
					for (var fi = 0; fi < laFields.length; fi++) {
						laFields[fi] = "- " + laFields[fi];
					}
				}
		       	alert(laFields.join('\n'));
		       	flag = false;
		    }
		}
		return flag;
	}
</script>

<style>
	ul.unstyled li{
		padding:10px;
	}
</style>
</head>

<body>
<div class="row-fluid" id="main">
	<div class="container">
		<div class="row-fluid">
			<div class="gutter10">
				<div class="row-fluid">
					<h1 id="">Upload AME/STM/HCC Document (PDF File) to ECM Server HIX-47913</h1>
				</div><!-- end of .row-fluid -->
				
				<div class="row-fluid">
					<div id="sidebar" class="span3">
						<div class="header"><span>Steps</span></div>
						<ul class="unstyled">
							<li><b>Step 1: </b>Choose Document to upload</li>
							<li class="gutter10"><b>Click Save: </b>Proceed to clicking Upload after required Document file have been selected.</li>
						</ul>
					</div><!-- end of .span3 -->
					
					<div class="span9" id="rightpanel">
						<div class="header" style="height:40px"></div>
						<form:form id="frmUploadDocument" name="frmUploadDocument" action="${pageContext.request.contextPath}/admin/uploadAncillaryDocs" modelAttribute="UploadAncillaryDocs" enctype="multipart/form-data" method="POST">
							<df:csrfToken/>
							<div class="control-group">
								<label class="control-label" for="fileUploadDocument"><b>Step 1:</b> Choose Document File* </label>
								<div class="controls">
									<input type="file" class="filestyle" id="fileUploadDocument" name="fileUploadDocument" data-classButton="btn">
								</div>
							</div>
							
				            <div class="row-fluid gutter10-t">
								<div class="span2">
				            		<a href="javascript:document.forms['frmUploadDocument'].reset()" class="btn pull-left" id="cancel">Cancel</a>
				            	</div>
								<div class="span2 margin10-l">
				            		<a href="javascript:doSubmit('frmUploadDocument', '${pageContext.request.contextPath}/admin/uploadAncillaryDocs');" class="btn btn-primary pull-left" type="submit" id="save">Upload</a>
								</div>
							</div>
						</form:form>
					</div><!-- end of .span9 -->
	            	<c:if test="${uploadDocumentErrorMessage != null}">
						<div align="left" style="font-size: 12pt; color: red; font-weight: bold;">
							<c:out value="${uploadDocumentErrorMessage}" />
						</div>
	            	</c:if>
	            	<c:if test="${uploadDocumentMessage != null}">
						<div align="left" style="font-size: 12pt; color: green; font-weight: bold;">
							<c:out value="${uploadDocumentMessage}" />
						</div>
	            	</c:if>
				</div><!-- end of .row-fluid -->
			</div> <!-- end of .gutter10 -->
		<!--row-fluid--> 
		</div>
	<!--container--> 
	</div>
<!--main-->
</div>
</body>
</html>