<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@include file="datasource.jsp" %>

<head>
	<title>SERFF Admin</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<sql:query dataSource="${jspDataSource}" var="result1">
select USER_TYPE,NAME,GI_LANGUAGE, GI_NOTICE_TYPE,
Case
When (External_Id Is Null) Then ''
Else '<a href="getEcmTemplateBottomPage?ecmId='||External_Id||'" target="test2">'||External_Id||'</a>'
END  as ECM_ID,
TEMPLATE_LOCATION,EMAIL_SUBJECT,EMAIL_FROM,EMAIL_TO,EMAIL_CLASS,
NOTICE_COMMUNICATION_METHOD, 
Case
When CAST(Editable as Integer)= 1 Then 'YES'
Else 'NO'
END  as EDITABLE ,
to_char(EFFECTIVE_dATE,'DD/MON/YYYY') as EFECTIVE_START,
to_char(TERMINATION_DATE,'DD/MON/YYYY') as EFECTIVE_END,
to_char(CREATION_TIMESTAMP,'DD/MON/YYYY') as CREATED,
To_Char(Last_Update_Timestamp,'DD/MON/YYYY') As Updated
From Notice_Types
order by user_type
</sql:query>

<br>
<br>
<b>ECM Email Templates Status </b>

<display:table name="${result1.rows}" requestURI="/admin/eCMTeplatesTopPage" id="table1" style="white-space: pre-wrap;" export="true" >
	<display:column property="USER_TYPE" title="User Type" sortable="false" />
	<display:column property="NAME" title="Name" sortable="false"  />
	<display:column property="GI_LANGUAGE" title="Language" sortable="false"  />
	<display:column property="GI_NOTICE_TYPE" title="Notification Type" sortable="false"  />
	<display:column property="ECM_ID" title="ECM Doc Id" sortable="false" />
	<display:column property="TEMPLATE_LOCATION" title="Template Location" sortable="false"  />
	<display:column property="EMAIL_SUBJECT" title="Email Subject" sortable="false"  />
	<display:column property="EMAIL_FROM" title="Email From" sortable="false" />
	<display:column property="EMAIL_TO" title="Email To" sortable="false"  />
	<display:column property="EMAIL_CLASS" title="Email Class" sortable="false"  />
	<display:column property="NOTICE_COMMUNICATION_METHOD" title="Notice Communication Method" sortable="false"  />
	<display:column property="EDITABLE" title="Is Template Editable" sortable="false"  />
	<display:column property="EFECTIVE_START" title="Start Time" sortable="false"  />
	<display:column property="EFECTIVE_END" title="End Time" sortable="false"  />
	<display:column property="CREATED" title="Created On" sortable="false"  />
	<display:column property="UPDATED" title="Updated On" sortable="false"  />
	
</display:table>

Query:
select USER_TYPE,NAME,GI_LANGUAGE, GI_NOTICE_TYPE,
Case
When (External_Id Is Null) Then ''
Else '<a href="getEcmTemplateBottomPage?ecmId='||External_Id||'" target="test2">'||External_Id||'</a>'
END  as ECM_ID,
TEMPLATE_LOCATION,EMAIL_SUBJECT,EMAIL_FROM,EMAIL_TO,EMAIL_CLASS,
NOTICE_COMMUNICATION_METHOD, 
Case
When Editable= 1 Then 'YES'
Else 'NO'
END  as EDITABLE ,
to_char(EFFECTIVE_dATE,'DD/MON/YYYY') as EFECTIVE_START,
to_char(TERMINATION_DATE,'DD/MON/YYYY') as EFECTIVE_END,
to_char(CREATION_TIMESTAMP,'DD/MON/YYYY') as CREATED,
To_Char(Last_Update_Timestamp,'DD/MON/YYYY') As Updated
From Notice_Types
order by user_type;

