<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page contentType="text/html" import="java.util.*"%>
<%@ include file="datasource.jsp"%>



<html>
<script type="text/javascript"
	src="../resources/js/jquery-1.10.2.min.js"></script>
<link rel="stylesheet"
	href="https://rawgithub.com/yesmeck/jquery-jsonview/master/dist/jquery.jsonview.css" />
<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script type="text/javascript"
	src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript"
	src="https://rawgithub.com/yesmeck/jquery-jsonview/master/dist/jquery.jsonview.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

<script>
	
	$( "#effDate" ).datepicker();
	
</script>

<script type="text/javascript">

	function getValue(val) {

		var zipCode = null;
		var date = null;
		var fips = null;
		var exType = null;
		zipCode = $("#zipCode").val();
		date = $("#effDate").val();
		fips = $("#countyFip").val();
		exType = $("#exType").val();

		var str = "{\"zip\":\"" + zipCode + "\"," + "\"effectiveDate\":\""
				+ date + "\"," + "\"countyFIPS\":\"" + fips + "\","
				+ "\"exchangeType\":\"" + exType + "\"}";
		$("textarea[name='inputJSON']").val(str);

	}
	
	

	function submitData(val) {
		
		var json = "";
		var start = new Date();
		var url = "";
		var defaultUrl = "ghix-planmgmt/plan/checkindvhealthplanavailability";
		var targetUrl = $('#targetURL').val();
		//var jsonview = "";
		if (targetUrl != "") {
			url = $('#protocol').val() + "://" + targetUrl + "/" + defaultUrl;
		} else {
			alert("Please enter valid url, e.g. localhost:8080 ");
		}
		//alert(url);
		$.ajax({
			type : "POST",
			contentType : "application/json; charset=utf-8",
			url : url,
			data : val,
			header: { 'Access-Control-Allow-Origin': '*'},
		    crossDomain: true,
			success : function(responseText) {
				$("textarea[name='outputRaw']").val(responseText);
				var end = new Date();
				var duration = end - start;
				$('output[name="duration"]').val(duration + " Milli Seconds");
				if(responseText == false){
					$('#json').JSONView("false");
				}else{
					$('#json').JSONView("true");
				}
				

			},
			error : function(responseText) {
				//alert(responseText);
				$("textarea[name='outputRaw']").val(
						"Error occurred while getting plan info");
				var end = new Date();
				var duration = end - start;
				$('output[name="duration"]').val(duration + " Milli Seconds");
				$('#json').JSONView(responseText);

			}

		});

	}
	
	
	function getCountyFips(){
		var whereClause = "";
		var zipCode = $('#zipCode').val();
		whereClause = "zip="+zipCode;
		document.getElementById('whereClause').value = whereClause;
		//alert(whereClause);
		$('#load').submit();
				
	}
</script>

<% 
      String hostName = request.getHeader("host");
	
%>
<%
	String whereClause = request.getParameter("whereClause");
	 if( whereClause == null || whereClause == ""){
		whereClause = "zip=0";
		
	} 
	 
	String zip= request.getParameter("zipCode");
	if(zip != null)
	request.setAttribute("zipCode",zip);
	 
	
%>

<sql:query dataSource="${jspDataSource}" var="result1">
		select distinct(fips) from pm_service_area where  <%=whereClause %>
</sql:query>


<body>

	<form method="POST" id="load">
		<table border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td width="100%" bgcolor="#FFCC00"><font face="Arial" size="2"><b>API:
							Get Individual Health Plan Availability</b></font></td>
			</tr>
			<tr>
				<td width="100%" bgcolor="#FFCC00"><font face="Arial" size="2"><b>Path:</b>
						<a
						href="http://localhost:8080/ghix-planmgmt/plan/admin/checkindvhealthplanavailability">/ghix-planmgmt/plan/admin/checkindvhealthplanavailability</a>
						&nbsp;</font></td>
			</tr>
			<tr>
				<td width="100%" bgcolor="#C4FCB8">
					<p>
						<font face="Arial" size="2">Zip Code: <input type="number"
							id="zipCode" name="zipCode" value="${zipCode}" maxlength="5">
							<%-- <input id="hdnzipCode" name="hdnzipCode" type="hidden"	value="${zipCode}">  --%>
							<input type="button" value="loadFips"
							name="loadFips" onclick="getCountyFips()"> County Fips: <select
							id="countyFip" name="countyFip">
								<option value="">Select</option>
								<c:forEach var="servicearea" items="${result1.rows}">
									<option value="${servicearea.fips}">${servicearea.fips}
									</option>
								</c:forEach>

						</select> Effective date: <input type="date" class="datepicker"
							id="effDate" name="effDate" title="dd/MM/yyyy" /> <%-- <input type="text" id="countyFips" name="countyFips" value="${countyFips}"> <br> --%>
							Exchange type <input type="radio" id="exType" name="exType"
							value="ON"> ON <input type="radio" id="exType"
							name="exType" value="OFF"> OFF <input type="button"
							value="Populate" name="Populate" onclick="getValue()">
						</font>
					</p> <textarea rows="5" name="inputJSON" cols="40"
						style="background-color: #BDFFFF" id="inputJSON">
						<c:out value="${inputJSON}"></c:out>
					</textarea>

				</td>

			</tr>
			<tr>
				<td width="100%" bgcolor="#C4FCB8">
					<p align="left">
						<font face="Arial" size="2">Protocol: <input type="radio"
							id="protocol" name="protocol" value="http"> HTTP <input
							type="radio" id="protocol" name="protocol" value="https">
							HTTPS
						</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font face="Arial" size="2">URL:
							<input type="text" name="targetURL" size="20"
							style="background-color: #BDFFFF" value="<%=hostName%>"
							id="targetURL"> <input type="button" value="Post"
							name="Post"
							onclick="submitData(document.getElementById('inputJSON').value);">
							<input type="reset" value="Reset" name="B2">
						</font>
					</p>
				</td>
			<tr>
			<tr>
				<td width="100%" bgcolor="#999999"><font face="Arial" size="2"><b>Duration</b>:
						<output id="duration" name="duration"> </output></td>
			</tr>
			<tr>
				<td width="100%" bgcolor="#CCCCCC"><font face="Arial" size="2">Raw
						Response:</font>
					<p>
						<font face="Arial" size="2"> <!-- <textarea rows="5" name="outputRaw" cols="40" style="background-color: #CCCCCC"></textarea></font></td> -->
							<textarea rows="5" name="outputRaw" cols="50"
								style="background-color: #BDFFFF" id="outputRaw">
								<c:out value="${outputRaw}"></c:out>
							</textarea>
			</tr>

			<tr>
				<td width="100%" bgcolor="#CCCCCC">SQLs: <br> SELECT
					COUNT(DISTINCT p.id )FROM plan p, pm_service_area psarea,
					plan_health phealth, pm_zip_county_rating_area pzcrarea,
					pm_plan_rate prate WHERE p.service_area_id = psarea.service_area_id
					AND p.id = phealth.plan_id AND pzcrarea.rating_area_id =
					prate.rating_area_id AND p.id = prate.plan_id AND p.insurance_type
					= ? AND psarea.zip = ? AND (pzcrarea.zip = ? OR pzcrarea.zip IS
					NULL) AND psarea.fips = ? AND pzcrarea.county_fips = ? AND
					p.is_deleted = 'N' AND psarea.is_deleted = 'N' AND prate.is_deleted
					= 'N' AND TO_DATE (?, 'YYYY-MM-DD') BETWEEN p.start_date AND
					p.end_date AND p.status IN ('CERTIFIED', 'RECERTIFIED') AND
					p.issuer_verification_status = 'VERIFIED' AND ( (
					p.enrollment_avail ='AVAILABLE' AND TO_DATE (?, 'YYYY-MM-DD') >=
					p.enrollment_avail_effdate AND ( TO_DATE (?, 'YYYY-MM-DD') <
					p.future_erl_avail_effdate OR p.future_erl_avail_effdate IS NULL) )
					OR ( p.future_enrollment_avail ='AVAILABLE' AND TO_DATE (?,
					'YYYY-MM-DD') >= p.future_erl_avail_effdate) ) AND p.market =
					'INDIVIDUAL' AND p.exchange_type = ? AND phealth.parent_plan_id = 0
					GROUP BY (pzcrarea.rating_area_id)<br>

				</td>
			</tr>
			<tr>
				<td width="100%" bgcolor="#CCCCCC">Notes:
					http://jsbin.com/bacet/4/edit</td>
			</tr>
		</table>
		<input id="whereClause" name="whereClause" type="hidden">
	</form>

</body>


</html>