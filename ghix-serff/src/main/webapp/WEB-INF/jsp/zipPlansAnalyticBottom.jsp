<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@include file="datasource.jsp" %>

<head>
	<base target="batchmain_1">
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
</head>
<%
	String zipCode = request.getParameter("zipCode");
	String fips = request.getParameter("fips");

	if (null != zipCode && null != fips) {
		
		String whereClause = "WHERE ZIP = '"+ zipCode +"'  AND FIPS = '"+ fips +"'";
%>

<sql:query dataSource="${jspDataSource}" var="zipPlanResults">
SELECT ZIP, FIPS, ISSUER_PLAN_NUMBER, APPLICABLE_YEAR, NAME, INSURANCE_TYPE, PLAN_LEVEL, CREATION_TIMESTAMP
FROM PM_ZIP_PLANS
<%=whereClause%>
ORDER BY ISSUER_PLAN_NUMBER, APPLICABLE_YEAR
</sql:query>

<b>Zip Plans Analytic Data for ZIP[<%=zipCode%>] and FIPs[<%=fips%>]</b>
<br />
<display:table name="${zipPlanResults.rows}" requestURI="getZipPlansAnalyticBottom" id="zipPlanTable" defaultsort="1" style="white-space: pre-wrap;" export="true" pagesize="1000">
	<display:column property="ZIP" title="Zip Code" sortable="false" />
	<display:column property="FIPS" title="Fips" sortable="false" />
	<display:column property="ISSUER_PLAN_NUMBER" title="Issuer Plan Number" sortable="false" />
	<display:column property="APPLICABLE_YEAR" title="Applicable Year" sortable="false" />
	<display:column property="NAME" title="Name" sortable="false" />
	<display:column property="INSURANCE_TYPE" title="Insurance Type" sortable="false" />
	<display:column property="PLAN_LEVEL" title="Plan(Metal) Type" sortable="false" />
	<display:column property="CREATION_TIMESTAMP" title="Creation Time" sortable="false" />

	<display:setProperty name="export.xml.filename" value="zipPlansAnalyticData.xml"/>
	<display:setProperty name="export.csv.filename" value="zipPlansAnalyticData.csv"/>
	<display:setProperty name="export.excel.filename" value="zipPlansAnalyticData.xls"/>
</display:table>
<%
	}
%>