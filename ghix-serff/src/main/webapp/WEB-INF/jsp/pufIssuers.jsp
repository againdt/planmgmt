<html>
<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>PUF Issuers</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<body>

<sql:query dataSource="${jspDataSource}" var="result1">
select STATE, ISSUER_NAME , count(*) as total
from puf_qhp
group by state, issuer_name
order by total desc,state, issuer_name
</sql:query> 

<br>

<sql:query dataSource="${jspDataSource}" var="result2">
select STATE, ISSUER_NAME , count(distinct(plan_id_standard_component)) as total
from puf_qhp
group by state, issuer_name
order by total desc,state, issuer_name
</sql:query> 

<table>
<tr><td>
<b>Issuer/State Total Records</b>
<display:table name="${result1.rows}"  requestURI="pufIssuers" id="table1" export="true"  style="width:100%">
	<display:column property="STATE" title="State" sortable="true"/>
	<display:column property="ISSUER_NAME" title="Issuer Name" sortable="true"/>
	<display:column property="total" title="Total" sortable="true"/>
	
	<display:setProperty name="export.xml.filename" value="pufIssuersTotal.xml"/>
	<display:setProperty name="export.csv.filename" value="pufIssuersTotal.csv"/>
	<display:setProperty name="export.excel.filename" value="pufIssuersTotal.xls"/>
</display:table>
</td><td>
<b>Issuer/State Unique Plans</b>
<display:table name="${result2.rows}"  requestURI="pufIssuers" id="table2" export="true" style="width:100%">
	<display:column property="STATE" title="State" sortable="true"/>
	<display:column property="ISSUER_NAME" title="Issuer Name" sortable="true"/>
	<display:column property="total" title="Total" sortable="true"/>
	
	<display:setProperty name="export.xml.filename" value="pufIssuersUnique.xml"/>
	<display:setProperty name="export.csv.filename" value="pufIssuersUnique.csv"/>
	<display:setProperty name="export.excel.filename" value="pufIssuersUnique.xls"/>
</display:table>
</td></tr>
</table>

 </body> 
 </html>
