<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>SERFF Batch Admin</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	
	<script type="text/javascript">
		function submitBatchBottomRight() {
			doSubmit('frmBatchTopPage', '${pageContext.request.contextPath}/admin/getBatchBottomRight');
		}
		
		function doSubmit(formName, url) {
			
			if (formName && url) {
				document.forms[formName].method = "post";
				document.forms[formName].action = url;
				document.forms[formName].submit();
			}
		}
		
		$(function() {
			$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
			
			});
		
		$(function() {
			$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
			});
		
		function validateDates(){
		
			var dateStart = document.getElementById('date1').value.split('/');
			
			if(dateStart != "" && dateStart.length > 0)
			{
				
			var boolean1 = validateDate(dateStart);
			}
		
			var dateEnd = document.getElementById('date2').value.split('/');
			
			if(dateEnd !=  "" && dateEnd.length > 0)
		        {
				var boolean2 = validateDate(dateEnd);
			}
		}
		
		function validateDate(dateGiven){
			
			var today=new Date();
		
			var dob_mm = dateGiven[0];  
			var dob_dd = dateGiven[1];  
			var dob_yy = dateGiven[2]; 
			
			var birthDate=new Date();
			birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
		
			if( (today.getFullYear() - 2) >  birthDate.getFullYear() ) {
			//alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
			}
		
			if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) {
		//	alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
			}
		
			if(today.getTime() < birthDate.getTime()){
		//	alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
			}
			
		}
		
		function setWhereClause(type){
			
			var whereClause = '';
			var filterStatement = '';
			var topTenSelectClause='';
			var topTenWhereClause='';
			switch (type) {
				case 'B1' : 
					<%if(isPostgresDB) {%>
						whereClause = 'ftp_start_time between (now()-interval \'1 day\') and now()';
					<%} else {%>
					whereClause = 'ftp_start_time between (sysdate-1) and sysdate';
					<%}%>
					filterStatement = ' in last 24 hours';
					break;
				case 'B2' : 
					<%if(isPostgresDB) {%>
						whereClause = 'ftp_start_time between (now()-interval \'12 hour\') and now()';
					<%} else {%>
					whereClause = 'ftp_start_time between (sysdate-1/2) and sysdate';
					<%}%>
					filterStatement = ' in last 12 hours';
					break;
				case 'B3' : 
					<%if(isPostgresDB) {%>
						whereClause = 'ftp_start_time between (now()-interval \'1 hour\') and now()';
					<%} else {%>
					whereClause = 'ftp_start_time between (sysdate-1/24) and sysdate';
					<%}%>
					filterStatement = ' in last 1 hour';
					break;
				case 'B4' : 
				
					
					//Validate the dates
				        validateDates();
				         
				         //Step 1: Get List Box value, if it is not empty
					var fieldName = document.getElementById('field_name').value;
					var fieldValue = document.getElementById('field_value').value;
					//alert(fieldValue);
					if(fieldValue != null && fieldValue.length > 0)
					{
					<%if(isPostgresDB) {%>
						whereClause = " lower(CAST ("+fieldName + " as TEXT)) like lower('%" + fieldValue + "%')";
					<%} else {%>
					whereClause = " lower("+fieldName + ") like lower('%" + fieldValue + "%')";
					<%}%>
					filterStatement = ' with ' + fieldName + " = " + fieldValue;
					}
					
					//Step 2: Get the Status Value
					var statuses = document.getElementById('status');
					var status = statuses.options[statuses.selectedIndex].value;
					if(status == 'S' || status == 'F')
					{
						if(whereClause){
							whereClause = whereClause  + " and request_status = '" + status + "'";
						}else{
							whereClause = whereClause  + " request_status = '" + status + "'";
						}
					
						
					}
					if(status == 'S'){
						
						filterStatement = filterStatement+ ' which were successful';
					}
					if(status == 'F'){
					
						filterStatement = filterStatement+ ' which have failed';
					}
					
					//Step 3: Get the Dates
					var date1 = document.getElementById('date1').value;
					var date2 = document.getElementById('date2').value;
					
					if(date1 && date2)
					{	
						
						if( whereClause){
							whereClause = whereClause+"and ftp_start_time between to_timestamp('"+date1+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and  to_timestamp('"+date2+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							
						}else{
							whereClause = whereClause+" ftp_start_time between to_timestamp('"+date1+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and  to_timestamp('"+date2+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							
						}
					
					}else{
						if(date1&& !date2  ){
							if( whereClause){
							
							whereClause = whereClause+"and to_char(ftp_start_time  ,'mm/dd/yyyy' )='"+date1+"'" ;
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							
							}else{
								whereClause = whereClause+" to_char(ftp_start_time  ,'mm/dd/yyyy' )='"+date1+"'" ;
								filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							}
						}else{
							
							 if(!date1 && date2 ){
								 if( whereClause){
										whereClause = whereClause+"and to_char(ftp_end_time  ,'mm/dd/yyyy' )='"+date2+"'" ;
										filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
								 }else{
										whereClause = whereClause+" to_char(ftp_end_time  ,'mm/dd/yyyy' )='"+date2+"'" ;
										filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
									 }
						
								}
							}
						
						}if(!whereClause){
						<%if(isPostgresDB) {%>
							whereClause = "ftp_start_time <= now()";
						<%} else {%>
							whereClause = "ftp_start_time <= sysdate";
						<%}%>
						}
					
					break;
				case 'B5' : 
					topTenSelectClause=' Select * from ( ';
					<%if(isPostgresDB) {%>
					topTenWhereClause=' ) allrec limit 10 ';
					<%} else {%>
					topTenWhereClause=' ) where rownum <=10 order by  rownum';
					<%}%>
					filterStatement = 'for latest records';
					break;
			}
			
			document.getElementById('whereClause').value = whereClause;
			document.getElementById('filterStatement').value = filterStatement;
			document.getElementById('topTenSelectClause').value = topTenSelectClause;
			document.getElementById('topTenWhereClause').value = topTenWhereClause;
		//	alert('whereClause==>'+whereClause);
		}
		
		function getBatchTopPage(id, isCancel) {
		
			if (!id) {
				return;
			}
			var batchStatus = "";
			var statusNode = document.getElementById(id + "Status");
			
			if (statusNode && statusNode.value) {
				batchStatus = statusNode.value;
			}
			
			var isValidStatus = false;
			if (!batchStatus || batchStatus == "FAILED") {
				isValidStatus = true;
			}
			
			if (isValidStatus && isCancel) {
				window.location = "${pageContext.request.contextPath}/admin/getBatchTopPage?cancelid="+ id +"";
			}
			else if (isValidStatus && !isCancel) {
				window.location = "${pageContext.request.contextPath}/admin/getBatchTopPage?reprocessId="+ id +"";
			}
		}
	</script>
</head>

<form method="POST" id="frmBatchTopPage" name="frmBatchTopPage" action="getBatchTopPage">
<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
				<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
				<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
				<input type="submit" value="Show Latest" name="B4" title="View top 10 requests" onclick="setWhereClause('B5');">
			</td>
			<td bgcolor="#66FFCC">
				<p>
					Field:
					<select size="1" id="field_name" name="field_name">
						<option value="ftp_status" selected="">FTP Status</option>
						<option value="batch_status" >Batch Status</option>
						<option value="is_deleted">IS Deleted</option>
						<option value="Career">Carrier</option>
						<option value="state">State</option>
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
					Status:
					<select size="1" id="status" name="status">
					<option value="A" selected="">All</option>
					<option value="S" >Success</option>
					<option value="F">Failure</option>
				</select>
				Date Start:
				<input type="text" id="date1" name="date1" size="10" maxlength="10"> End:
				<input type="text" id="date2" name="date2" size="10" maxlength="10">
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
	<input id="topTenSelectClause" name="topTenSelectClause" type="hidden">
	<input id="topTenWhereClause" name="topTenWhereClause" type="hidden">

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		if(isPostgresDB) {
			whereClause = "ftp_start_time >= now()- interval '2 day'";
		} else {
		whereClause = "ftp_start_time >= sysdate-2";
	}
	}
	
	String topTenSelectClause = request.getParameter("topTenSelectClause");
	if( topTenSelectClause == null || topTenSelectClause == ""){
		topTenSelectClause="";
		
	}
	
	String topTenWhereClause = request.getParameter("topTenWhereClause");
	if( topTenWhereClause == null || topTenWhereClause == ""){
		topTenWhereClause="";
		
	}
%>

<sql:query dataSource="${jspDataSource}" var="result1">
<%=topTenSelectClause %> select spmb.id,spmb.user_id,u.email,spmb.ftp_status,
to_char(spmb.ftp_start_time ,'DD-MON-YY HH24:MI:SS') as ftpStart_time,
to_char(spmb.ftp_end_time,'DD-MON-YY HH24:MI:SS') as ftpEnd_time,
CASE
WHEN spmb.batch_status='WAITING' THEN 'Cancel'
WHEN spmb.batch_status='IN_PROGRESS' THEN 'Cancel'
WHEN spmb.batch_status='COMPLETED' THEN 'Cancel'
WHEN spmb.is_deleted='Y' THEN 'Cancel'
ELSE '<a href="javascript:getBatchTopPage('||spmb.id||', true)" id="'||spmb.id||'Cancel">Cancel</a>'
END as cancel,
CASE
WHEN spmb.is_deleted='Y' THEN 'Process'
WHEN spmb.batch_status='WAITING' THEN 'Process'
WHEN spmb.batch_status='IN_PROGRESS' THEN 'Process'
WHEN spmb.batch_status='COMPLETED' THEN 'Process'
WHEN spmb.batch_status='FAILED' THEN '<a href="javascript:getBatchTopPage('||spmb.id||', false)" id="'||spmb.id||'Reprocess">Reprocess</a>'
ELSE '<a href="javascript:getBatchTopPage('||spmb.id||', false)" id="'||spmb.id||'Reprocess">Process</a>'
END as reprocess,
'<a href="${pageContext.request.contextPath}/admin/getBatchBottomLeft?fileid='||spmb.id||'" target="batchcontents_1">Files</a>' as files,
'<input type="hidden" id="'||spmb.id||'Status" value="'||spmb.batch_status||'">'||spmb.batch_status||'' as batchStatus,
to_char(spmb.batch_Start_time,'DD-MON-YY HH24:MI:SS') as batchStart_time,
to_char(spmb.batch_end_time,'DD-MON-YY HH24:MI:SS') as batchEnd_time,
spmb.is_deleted,
spmb.serff_plan_mgmt_id,spmb.issuer_id,spmb.career,spmb.state,spmb.exchange_type as exchange_type,
spm.serff_req_id,issuer.short_name,spm.serff_track_num,spm.state_track_num,spm.serff_operation,spm.process_ip,spm.plan_stats ,
to_char(spm.start_time,'DD-MON-YY HH24:MI:SS') as startDate,
to_char(spm.end_time,'DD-MON-YY HH24:MI:SS') as endDate,

CASE
WHEN spm.serff_req_id is null THEN 'Request'
ELSE '<a href="getBottomRight?request='||spm.serff_req_id||'" target="batchmain_1">Request</a>' 
END as request_xml,
CASE
WHEN spm.serff_req_id is null THEN 'Response'
ELSE '<a href="getBottomRight?response='||spm.serff_req_id||'" target="batchmain_1">Response</a>'
END  as response_xml, 

CASE
WHEN (spm.pm_response_xml is NULL) THEN ''
WHEN (spm.pm_response_xml like '%<errMsg></errMsg>%') THEN '<a href="getBottomRight?pmresponse='||spm.serff_req_id||'" target="batchmain_1">Pass</a>' 
ELSE '<a href="getBottomRight?pmresponse='||spm.serff_req_id||'" target="batchmain_1">Warning</a>' 
END as pm_response_xml, 

CASE
WHEN spm.serff_req_id is null THEN 'Attachments'
ELSE '<a href="getBottomLeft?attachments='||spm.serff_req_id||'" target="batchcontents_1">Attachments</a>' 
END as list1, 

CASE
WHEN (spm.request_status='F') THEN 'Failed'
WHEN (spm.request_status='S') THEN 'Success'
WHEN (spm.request_status='P') THEN 'In-Progress'
ELSE 'Unknown'
END as request_status, 
spm.remarks, spm.attachments_list,
CASE
WHEN (spm.request_state='B') THEN 'Begin'
WHEN (spm.request_state='E') THEN 'Ended'
WHEN (spm.request_state='P') THEN 'In-Progress'
ELSE 'Waiting'
END as request_state, 
spm.request_state_desc

from serff_plan_mgmt spm
right join serff_plan_mgmt_batch spmb on  spm.serff_req_id = spmb.serff_plan_mgmt_id
left join users u on spmb.user_id = u.id
left join issuers issuer on spmb.issuer_id = issuer.hios_issuer_id
where (spmb.operation_type='PRESCRIPTION_DRUG' or spmb.operation_type='PLAN' or spmb.operation_type='DRUGS_XML' or spmb.operation_type='DRUGS_JSON') and <%=whereClause%>
order by id desc <%=topTenWhereClause %>
</sql:query>

Requests Received by SERFF Batch
	<% 
		if(request.getParameter("filterStatement") != null){
			out.println(request.getParameter("filterStatement"));
		}
	%>
<br>

<display:table name="${result1.rows}" requestURI="getBatchTopPage" id="table1" style="white-space: pre-wrap;" export="true" pagesize="10">
	<display:column property="id" title="Batch ID" sortable="false" />
	<display:column property="issuer_id" title="HIOS Issuer ID" sortable="false" />
	<display:column property="short_name" title="Carrier" sortable="false" />
	<display:column property="state" title="State" sortable="false" />
	<display:column property="exchange_type" title="Exchange Type" sortable="false" />
	<display:column property="request_status" title="Request Status" sortable="false" />
	<display:column style="background-color:#A9E2F3" property="files" title="Files in FTP" sortable="false" />
	<display:column style="background-color:#A9E2F3" property="ftp_status" title="FTP Status" sortable="false" />
	<display:column style="background-color:#CEF6D8" property="batchStatus" title="Batch Status" sortable="false" />
	<display:column style="background-color:#CEF6D8" property="cancel" title="Cancel Batch" sortable="false" />
	<display:column style="background-color:#CEF6D8" property="reprocess" title="Reprocess Batch" sortable="false" />
	<display:column property="is_deleted" title="IS Deleted" sortable="false" />
	<display:column property="serff_plan_mgmt_id" title="Serff Plan Mgmt ID" sortable="false" />
	<display:column style="background-color:#A9E2F3" property="list1" title="Attachments" sortable="false" />
	<display:column property="request_xml" title="Request" sortable="false" />
	<display:column property="response_xml" title="Response" sortable="false" />
	<display:column property="pm_response_xml" title="PM Response" sortable="false" />
	<display:column property="plan_stats" title="Plan Count Stats" sortable="false" />
	<display:column property="serff_operation" title="SERFF Operation" sortable="false" />
	<display:column property="serff_track_num" title="SERFF Track No" sortable="false" />
	<display:column property="state_track_num" title="State Track No" sortable="false" />
	<display:column property="process_ip" title="Process IP" sortable="false" />
	<display:column property="remarks" title="Remarks" sortable="false" />
	<display:column property="request_state" title="Job Status" sortable="false" />
	<display:column property="request_state_desc" title="Job Status Description" sortable="false" />
	<display:column property="email" title="User Email" sortable="false" />
	<display:column style="background-color:#A9E2F3" property="ftpStart_time" title="FTP Start Time" sortable="false" />
	<display:column style="background-color:#A9E2F3" property="ftpEnd_time" title="FTP End Time" sortable="false" />	
	<display:column style="background-color:#CEF6D8" property="batchStart_time" title="Batch Start Time" sortable="false" />
	<display:column style="background-color:#CEF6D8" property="batchEnd_time" title="Batch End Time" sortable="false" />
	<display:column property="startDate" title="Request Start Date" sortable="false" />
	<display:column property="endDate" title="Request End Date" sortable="false" />
	
	<display:setProperty name="export.xml.filename" value="serffBatchAdmin.xml"/>
	<display:setProperty name="export.csv.filename" value="serffBatchAdmin.csv"/>
	<display:setProperty name="export.excel.filename" value="serffBatchAdmin.xls"/>
</display:table>
</form>