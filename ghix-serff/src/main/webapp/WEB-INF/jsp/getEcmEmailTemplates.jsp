<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp"%>

<sql:query dataSource="${jspDataSource}" var="result1">
select name as templateName, '<a href="${pageContext.request.contextPath}/admin/emailTemplate/Download?templateLocation='||template_location||'">Download</a>' as downloadLink,
to_char(effective_date,'DD-MON-YY') as effectiveDate,
to_char(termination_date,'DD-MON-YY') as terminationDate,
to_char(last_update_timestamp,'DD-MON-YY HH24:mi:SS') as lastUpdatedTimestamp from notice_types where template_location in ('notificationTemplate/IssuerAccountActivationEmail.html','notificationTemplate/IssuerCertificationStatusChange.html',
'notificationTemplate/IssuerFinancialInformationChange.html','notificationTemplate/IssuerPlanCertification.html','notificationTemplate/IssuerPlanCertification.html',
'notificationTemplate/IssuerPlanEnrollmentAvailabilityChange.html','notificationTemplate/IssuerPlanDeCertification.html')
</sql:query>

<html>
<head>
	<title>Search for Error Messages</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>
<frame name="trgFrame" hidden="true">
<body>
	</br></br>
	<b>ECM Email Templates </b>
	</br></br>
	This page downloads the requested email templates from ECM. If those needs to be downloaded from configuration then please manually copy ghix-config.jar in /lib folder.
	</br></br>
	    <c:if test="${downloadMsgTemplate != null}">
			<div align="left" style="font-size: 12pt; color: red; font-weight: bold;">
				<c:out value="${downloadMsgTemplate}" />
			</div>
	    </c:if>
		<display:table name="${result1.rows}"  requestURI="ecmEmailTemplates" id="table1" export="false" pagesize="500" style="width:80%" defaultorder="descending" >
			<display:column property="templateName" title="Template" sortable="true" />
			<display:column property="downloadLink" title="Download" sortable="true"/>
			<display:column property="effectiveDate" title="Effective Date" sortable="true"/>
			<display:column property="terminationDate" title="Termination Date" sortable="true"/>
			<display:column property="lastUpdatedTimestamp" title="Last Updated" sortable="true"/>
		</display:table>
</body>
</html>