<html>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>Duplicate Records</title>
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	<link rel="stylesheet" href="/resources/demos/style.css" />
</head>

<body>
<sql:query dataSource="${jspDataSource}" var="result1">
select * from ( select plan_id,applicable_year, count(*) as rec_count from serff_templates group by plan_id, applicable_year) res1 where rec_count >1
</sql:query> 

<b>Duplicate Records from serff_templates table</b>
<br>
Note: If there are any duplicate plans, we need to set is_delete='Y' in plan table manually.
<br><br><br>
<display:table name="${result1.rows}"  requestURI="duplicateRec" id="table1" export="true" pagesize="500" style="width:80%" defaultorder="descending" >
	<display:column property="plan_id" title="plan Id " sortable="true" />
	<display:column property="rec_count" title="record count" sortable="true" />
	<display:column property="applicable_year" title="Plan year" sortable="true" />
	<display:setProperty name="export.xml.filename" value="DuplicatePlans.xml"/>
	<display:setProperty name="export.csv.filename" value="DuplicatePlans.csv"/>
	<display:setProperty name="export.excel.filename" value="DuplicatePlans.xls"/>
</display:table>
<br>
Note: Query to check records in DB <br>
select * from ( select plan_id,applicable_year, count(*) as rec_count from serff_templates group by plan_id, applicable_year) where rec_count >1
</body> 
</html>
