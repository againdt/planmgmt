<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<html>
<head>
	<title>Search Cross Walk Templates Data</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<%
String givenPlanID = request.getParameter("selectPlanID");
//Default display all plans
String currentPlanIDWhereClause = "current_hios_plan_id like '%'";
if(givenPlanID!=null){
	currentPlanIDWhereClause = "current_hios_plan_id ='"+givenPlanID+"'";	
	}

%>

<body>
<b>Search plans for given Pland ID<b>
	<form method="POST" action="searchCrossWalkData">
	<df:csrfToken/>		
		<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0"> Current Plan ID:
				<input type ="text" id="selectPlanID" name="selectPlanID" multiple="multiple">
				 <input type ="hidden" id ='list' name =list value="">
				<input type="submit" value="Search">
			</td>
			</tr>
		</table>
</body>


<sql:query dataSource="${jspDataSource}" var="result1">
select hios_issuer_id,applicable_year,current_hios_plan_id,crosswalk_hios_plan_id,crosswalk_reason,
county_name,county_code,crosswalk_tier,zip_list,crosswalk_level,
to_char(creation_timestamp,'DD-MON-YY HH24:MI:SS') as createdTime,
to_char(last_update_timestamp,'DD-MON-YY HH24:MI:SS') as updatedTime
from pm_crosswalk where is_deleted='N'
and <%=currentPlanIDWhereClause %>
order by current_hios_plan_id,county_code
</sql:query>


<display:table name="${result1.rows}" requestURI="/admin/searchCrossWalkData" id="table1" style="white-space: pre-wrap;" export="true" pagesize="50">
	<display:column property="hios_issuer_id" title="HIOS Issuer Id" sortable="true" />
	<display:column property="applicable_year" title="Year" sortable="true" />
	<display:column property="current_hios_plan_id" title="2014 Plan ID" sortable="true" />
	<display:column property="crosswalk_hios_plan_id" title="Mapped 2015 Plan ID" sortable="true" />
	<display:column property="crosswalk_reason" title="Cross Walk Reason" sortable="true" />
	<display:column property="county_name" title="County" sortable="true" />
	<display:column property="county_code" title="FIPS" sortable="true" />
	<display:column property="crosswalk_tier" title="Cross Walk Tier" sortable="true" />
	<display:column property="zip_list" title="Zip List" sortable="true" />
	<display:column property="crosswalk_level" title="Cross Walk Level" sortable="true" />
	
	<display:column property="createdTime" title="Created Time" sortable="true" />
	<display:column property="updatedTime" title="Updated Time" sortable="true" />
	
	<display:setProperty name="export.xml.filename" value="crosswalkPlans.xml"/>
	<display:setProperty name="export.csv.filename" value="crosswalkPlans.csv"/>
	<display:setProperty name="export.excel.filename" value="crosswalkPlans.xls"/>
	
</display:table> 
<br>
Note: Query to check records in DB <br>
select hios_issuer_id,applicable_year,current_hios_plan_id,crosswalk_hios_plan_id,crosswalk_reason,
county_name,county_code,crosswalk_tier,zip_list,crosswalk_level,
to_char(creation_timestamp,'DD-MON-YY HH24:MI:SS') as createdTime,
to_char(last_update_timestamp,'DD-MON-YY HH24:MI:SS') as updatedTime
from pm_crosswalk where is_deleted='N'
and current_hios_plan_id = '?'
order by current_hios_plan_id,county_code
<br>
</html>