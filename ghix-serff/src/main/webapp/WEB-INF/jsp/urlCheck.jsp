<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>
<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>URL Check</title>
</head>
<body>
<b>This page is to Check URLs</b>
<br>
<sql:query dataSource="${jspDataSource}" var="result1">
	select HIOS_ISSUER_ID,NAME,CERTIFICATION_STATUS,
	ENROLLMENT_URL,SITE_URL,COMPANY_SITE_URL,INDV_SITE_URL,SHOP_SITE_URL,PAYMENT_URL ,PRODUCER_URL,APPLICATION_URL
	from issuers
	where certification_status = 'CERTIFIED'
</sql:query> 
<br>
<b>Issuers All URLs</b>
<br>
<display:table name="${result1.rows}"  requestURI="urlCheck" id="table1" export="true" style="width:50%" defaultorder="descending" pagesize="10">
	<display:column property="HIOS_ISSUER_ID" title="Hios Issuer Id"  />
	<display:column property="NAME" title="Name"  />
	<display:column property="CERTIFICATION_STATUS" title="Certification Status"  />
	<display:column property="SITE_URL" title="Site URL"  />
	<display:column property="COMPANY_SITE_URL" title="Company Site URL"  />
	<display:column property="INDV_SITE_URL" title="Individual Site URL"  />
	<display:column property="SHOP_SITE_URL" title="Shop Site URL"  />
	<display:column property="PAYMENT_URL" title="Payment URL"  />
	<display:column property="PRODUCER_URL" title="Producer URL"  />
	<display:column property="APPLICATION_URL" title="Application URL"  />
	
	<display:setProperty name="export.xml.filename" value="IssuerURLs.xml"/>
	<display:setProperty name="export.csv.filename" value="IssuerURLs.csv"/>
        <display:setProperty name="export.excel.filename" value="IssuerURLs.xls"/>

</display:table>
-----------------------------------------------------------------
<br>All following URLs must be prefixed with HTTP.
<br>If there is no data means, it is good.
<br>
<sql:query dataSource="${jspDataSource}" var="result2">
	select distinct BENEFITS_URL
	from plan_health
	where BENEFITS_URL  not like '%http%'
</sql:query> 

<br><br>
<b>Health Benefit URLs</b>
<br>
<display:table name="${result2.rows}"  requestURI="urlCheck" id="table1"   style="width:50%" defaultorder="descending" >
	<display:column property="BENEFITS_URL" title="Benefit Url"  />
</display:table>



<sql:query dataSource="${jspDataSource}" var="result3">
	select distinct ENROLLMENT_URL
	from plan_health
	where ENROLLMENT_URL not like '%http%'
</sql:query> 

<br><br>
<b>Health Enrollment URLs</b>
<br>
<display:table name="${result3.rows}"  requestURI="urlCheck" id="table1"   style="width:50%" defaultorder="descending" >
	<display:column property="ENROLLMENT_URL" title="Enrollment Url"  />
</display:table>


<sql:query dataSource="${jspDataSource}" var="result4">
	select distinct BENEFITS_URL
	from plan_dental
	where BENEFITS_URL not like '%http%'
</sql:query> 

<br><br>
<b>Dental Benefit URLs</b>
<br>
<display:table name="${result4.rows}"  requestURI="urlCheck" id="table1"   style="width:50%" defaultorder="descending" >
	<display:column property="BENEFITS_URL" title="Benefit Url"  />
</display:table>

<sql:query dataSource="${jspDataSource}" var="result5">
	select distinct ENROLLMENT_URL
	from plan_dental
	where ENROLLMENT_URL not like '%http%' 
</sql:query> 

<br><br>
<b>Dental Enrollment URLs</b>
<br>
<display:table name="${result5.rows}"  requestURI="urlCheck" id="table1"   style="width:50%" defaultorder="descending" >
	<display:column property="ENROLLMENT_URL" title="Enrollment Url"  />
</display:table>

<br><br>
Note: If http or https is missing, manually update database. Or Issuer need to send data in HIOS Templates
<br>JIRA: HIX-52935

<br><br>
select HIOS_ISSUER_ID,NAME,CERTIFICATION_STATUS,
ENROLLMENT_URL,SITE_URL,COMPANY_SITE_URL,INDV_SITE_URL,SHOP_SITE_URL,PAYMENT_URL ,PRODUCER_URL,APPLICATION_URL
from issuers
where certification_status = 'CERTIFIED';

<br><br>
select distinct BENEFITS_URL
from plan_health 
where BENEFITS_URL not like '%http%';

<br><br>
select distinct ENROLLMENT_URL
from plan_health 
where ENROLLMENT_URL not like '%http%';

<br><br>
select distinct BENEFITS_URL
from plan_dental
where BENEFITS_URL not like '%http%';

<br><br>
select distinct ENROLLMENT_URL
from plan_dental 
where ENROLLMENT_URL not like '%http%';

</body>
</html>