<html>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>Duplicate Records</title>
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	<link rel="stylesheet" href="/resources/demos/style.css" />
</head>

<body>
<sql:query dataSource="${jspDataSource}" var="result1">

Select Sd.Serff_Doc_Id , Sd.Document_Name , Sd.Document_Size , Sd.Ecm_Doc_Id ,
to_char(Sd.Creation_Timestamp,'DD-MON-YY HH24:mi:SS') as createdTime
From Serff_Document Sd ,  Serff_Plan_Mgmt  Spm Where  Spm.Serff_Operation ='TRANSFER_CROSSWALK' And Sd.Serff_Req_Id = Spm.Serff_Req_Id
order by Sd.Serff_Doc_Id desc

</sql:query> 

<b>Cross Walk Templates</b>
<br>
Note: For every load, we mark existing records as delete and load news ones.
Cross Walk Template is used to map 2014 Plan IDs with 2015 Plan IDs.
This information is used for renewals.
<br>
<display:table name="${result1.rows}"  requestURI="crossWalkUploadAdmin" id="table1" export="true" pagesize="10" style="width:80%" defaultorder="descending" >
	<display:column property="Sd.Serff_Doc_Id" title="SERFF DOC ID" sortable="true" />
	<display:column property="Sd.Document_Name" title="Document Name" sortable="true" />
	<display:column property="Sd.Document_Size" title="Document Size" sortable="true" />
	<display:column property="Sd.Ecm_Doc_Id" title="ECM Doc ID" sortable="true" />
	<display:column property="createdTime" title="Created Time" sortable="true" />
	<display:column  title="Download" href="${pageContext.request.contextPath}/admin/crosswalk/DownloadExcel" paramId="ecmId" paramProperty="ECM_DOC_ID">Download</display:column>

		
	<display:setProperty name="export.xml.filename" value="crossWalkDoc.xml"/>
	<display:setProperty name="export.csv.filename" value="crossWalkDoc.csv"/>
	<display:setProperty name="export.excel.filename" value="crossWalkDoc.xls"/>
</display:table>
<br>
Note: Query to check records in DB <br>
Select Sd.Serff_Doc_Id , Sd.Document_Name , Sd.Document_Size , Sd.Ecm_Doc_Id ,
to_char(Sd.Creation_Timestamp,'DD-MON-YY HH24:MI:SS') as createdTime
From Serff_Document Sd ,  Serff_Plan_Mgmt  Spm Where  Spm.Serff_Operation ='TRANSFER_CROSSWALK' And Sd.Serff_Req_Id = Spm.Serff_Req_Id
order by Sd.Serff_Doc_Id desc

</body> 
</html>
