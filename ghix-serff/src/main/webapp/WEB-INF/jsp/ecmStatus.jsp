<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="session.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>DB and ECM Status</title>
	</head>
<body>
	</br></br></br></br></br>
	<div align="center">
	
		<table border="0" cellpadding="10" cellspacing="0" width="80%">
			<tr>
				<td align="left" style="font-size: 14pt;"><b>DB Status</b></td>
				<td align="left" style="font-size: 14pt;"><b>ECM Status </b></td>
			</tr>					
			<tr>
				<td align="left" style="font-size: 14pt;">Database Response: <c:out value="${dbResultText}"/></td>
				<td align="left" style="font-size: 14pt;">ECM Server Response:<c:out value="${ecmServer}"/></td>
			</tr>	

			<tr>
				<td></td>
				<td>
					<c:if test="${issuerId != null}"> 
							<img class="resize-img" src="<c:url value="/admin/serff/logo/${issuerId}"/>"/>
				    </c:if>						
				</td>
			</tr>					
			<tr>
				<td><textarea rows="5" cols="40" name="dbStatusText"><c:out value="${dbMessage}"/></textarea></td>
				<td><textarea rows="5" cols="40" name="ecmStatusText"><c:out value="${ecmMessage}"/></textarea></td>
			</tr>			
			<tr>
				<td>
					<form action="${pageContext.request.contextPath}/admin/checkDBStatus" onsubmit="document.getElementById('submit1').disabled=true;" method="POST">
					<df:csrfToken/>
						<input type="submit" id="submit1" name="submit1" value="Check DB Status" style="font-size: 12pt; color: #0066CC">
					</form>

				</td>
				<td>
					<form action="${pageContext.request.contextPath}/admin/checkECMStatus" onsubmit="document.getElementById('submit2').disabled=true;" method="POST">
						<input type="submit" id="submit2" name="submit2" value="Check ECM Status" style="font-size: 12pt; color: #0066CC">
					</form>
				</td>
			</tr>	
			<tr>
			<td align="left" style="font-size: 11pt;">Note: SELECT 'System Time : ' ||SYSTIMESTAMP || ' IP Adress : '|| SYS_CONTEXT('USERENV','IP_ADDRESS') || ' Host Name : '|| SYS_CONTEXT('USERENV','SERVER_HOST') as result  FROM  DUAL </td>
			<td align="left" style="font-size: 11pt;">Note: select id,company_logo,name from issuers where company_logo is not null and rownum=1; Get logo id and fetch logo from ECM.</td>
			</tr>
		</table>
	
	</div>
</body>
</html>