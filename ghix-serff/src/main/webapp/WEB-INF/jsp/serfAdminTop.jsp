<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@include file="datasource.jsp" %>

<head>
	<title>SERFF Admin</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>

	<script type="text/javascript">
		$(function() {
			$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
			
			});
		
		$(function() {
			$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
			});
		
		function validateDates(){
		
			var dateStart = document.getElementById('date1').value.split('/');
			
			if(dateStart != "" && dateStart.length > 0)
			{
				
			var boolean1 = validateDate(dateStart);
			}
		
			var dateEnd = document.getElementById('date2').value.split('/');
			
			if(dateEnd !=  "" && dateEnd.length > 0)
		        {
				var boolean2 = validateDate(dateEnd);
			}
		}
		
		function validateDate(dateGiven){
			
			var today=new Date();
		
			var dob_mm = dateGiven[0];  
			var dob_dd = dateGiven[1];  
			var dob_yy = dateGiven[2]; 
			
			var birthDate=new Date();
			birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
		
			if( (today.getFullYear() - 2) >  birthDate.getFullYear() ) {
			//alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
			}
		
			if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) {
		//	alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
			}
		
			if(today.getTime() < birthDate.getTime()){
		//	alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
			}
			
		}
		
		function setWhereClause(type){
			
			var whereClause = '';
			var filterStatement = '';
			var topTenSelectClause='';
			var topTenWhereClause='';
			switch (type) {
				case 'B1' : 
					if(<%=isPostgresDB%>) {
						whereClause = 'start_time between (now()-interval \'1 day\') and now()';
					} else {
					whereClause = 'start_time between (sysdate-1) and sysdate';
					}
					filterStatement = ' in last 24 hours';
					break;
				case 'B2' : 
					if(<%=isPostgresDB%>) {
						whereClause = 'start_time between (now()-interval \'12 hour\') and now()';
					} else {
					whereClause = 'start_time between (sysdate-1/2) and sysdate';
					}
					filterStatement = ' in last 12 hours';
					break;
				case 'B3' : 
					if(<%=isPostgresDB%>) {
						whereClause = 'start_time between (now()-interval \'1 hour\') and now()';
					} else {
					whereClause = 'start_time between (sysdate-1/24) and sysdate';
					}
					filterStatement = ' in last 1 hour';
					break;
				case 'B4' : 
				
					
					//Validate the dates
				        validateDates();
				         
				         //Step 1: Get List Box value, if it is not empty
					var fieldName = document.getElementById('field_name').value;
					var fieldValue = document.getElementById('field_value').value;
					if(fieldValue != null && fieldValue.length > 0)
					{
						if(<%=isPostgresDB%>) {
							whereClause = " lower(CAST ("+fieldName + " as TEXT)) like lower('%" + fieldValue + "%')";
						} else {
					whereClause = " lower("+fieldName + ") like lower('%" + fieldValue + "%')";
						}
					filterStatement = ' with ' + fieldName + " = " + fieldValue;
					}
					
					//Step 2: Get the Status Value
					var statuses = document.getElementById('status');
					var status = statuses.options[statuses.selectedIndex].value;
					if(status == 'S' || status == 'F')
					{
						if(whereClause){
							whereClause = whereClause  + " and request_status = '" + status + "'";
						}else{
							whereClause = whereClause  + " request_status = '" + status + "'";
						}
					
						
					}
					if(status == 'S'){
						
						filterStatement = filterStatement+ ' which were successful';
					}
					if(status == 'F'){
					
						filterStatement = filterStatement+ ' which have failed';
					}
					
					//Step 3: Get the Dates
					var date1 = document.getElementById('date1').value;
					var date2 = document.getElementById('date2').value;
					
					if(date1 && date2)
					{	
						
						if( whereClause){
							whereClause = whereClause+"and start_time between to_timestamp('"+date1+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"+date2+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							
						}else{
							whereClause = whereClause+" start_time between to_timestamp('"+date1+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"+date2+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							
						}
					
					}else{
						if(date1&& !date2  ){
							if( whereClause){
							
							whereClause = whereClause+"and to_char(start_time  ,'mm/dd/yyyy' )='"+date1+"'" ;
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							}else{
								whereClause = whereClause+" to_char(start_time  ,'mm/dd/yyyy' )='"+date1+"'" ;
								filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							}
						}else{
							
							 if(!date1 && date2 ){
								 if( whereClause){
										whereClause = whereClause+"and to_char(end_time  ,'mm/dd/yyyy' )='"+date2+"'" ;
										filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
								 }else{
										whereClause = whereClause+" to_char(end_time  ,'mm/dd/yyyy' )='"+date2+"'" ;
										filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
									 }
						
								}
							}
						
						}if(!whereClause){

							if(<%=isPostgresDB%>) {
								whereClause = "start_time <= now()";
							} else {
							whereClause = "start_time <= sysdate";
						}
						}
					
					break;
				
				case 'B5' : 
					topTenSelectClause=' Select * from ( ';
					if(<%=isPostgresDB%>) {
						topTenWhereClause=' ) allRec limit 10 ';
					} else {
					topTenWhereClause=' ) where rownum <=10 order by  rownum';
					}
					filterStatement = 'for latest records';
					break;
					
			}
			
			document.getElementById('whereClause').value = whereClause;
			document.getElementById('filterStatement').value = filterStatement;
			document.getElementById('topTenSelectClause').value = topTenSelectClause;
			document.getElementById('topTenWhereClause').value = topTenWhereClause;
			//alert('whereClause==>'+whereClause);
			//alert('topTenSelectClause==>'+topTenSelectClause);
			//alert('topTenWhereClause==>'+topTenWhereClause);
		}
	</script>    
</head>

<form method="POST" action="getTopPage">
<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
				<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
				<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
				<input type="submit" value="Show Latest" name="B5" title="View top 10 requests" onclick="setWhereClause('B5');">
			</td>
			<td bgcolor="#66FFCC">
				<p>
					&nbsp;Field:
					<select size="1" id="field_name" name="field_name">
						<option value="serff_req_id" selected="">Serff Req id</option>
						<option value="issuer_id" >Issuer id</option>
						<option value="issuer_name">Issuer Name</option>
						<option value="hios_product_id">HIOS Prod ID</option>
						<option value="plan_id">Plan ID</option>
						<option value="plan_name">Plan Name</option>
						<option value="serff_track_num">SERFF Track No</option>
						<option value="state_track_num">State Track No</option>
						<option value="serff_operation">Operation</option>
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
					Status:
					<select size="1" id="status" name="status">
					<option value="A" selected="">All</option>
					<option value="S" >Success</option>
					<option value="F">Failure</option>
				</select>
				Date Start:
				<input type="text" id="date1" name="date1" size="10" maxlength="10"> End:
				<input type="text" id="date2" name="date2" size="10" maxlength="10">
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
	<input id="topTenSelectClause" name="topTenSelectClause" type="hidden">
	<input id="topTenWhereClause" name="topTenWhereClause" type="hidden">
</form>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		if(isPostgresDB) {
			whereClause = "start_time >= now()- interval '2 day'";
		} else {
		whereClause = "start_time >= sysdate-2";
		}		
	}
	String topTenSelectClause = request.getParameter("topTenSelectClause");
	if( topTenSelectClause == null || topTenSelectClause == ""){
		topTenSelectClause="";
		
	}
	
	String topTenWhereClause = request.getParameter("topTenWhereClause");
	if( topTenWhereClause == null || topTenWhereClause == ""){
		topTenWhereClause="";
		
	}
%>

<sql:query dataSource="${jspDataSource}" var="result1">
<%=topTenSelectClause %>select serff_req_id,issuer_id,issuer_name,hios_product_id,plan_id, plan_name,serff_track_num,state_track_num,serff_operation,process_ip, 
to_char(start_time,'DD-MON-YY HH24:MI:SS') as start1,
to_char(end_time,'DD-MON-YY HH24:MI:SS') as end1,
'<a href="getBottomRight?request='||serff_req_id||'" target="main_1">Request</a>' as request_xml, 
'<a href="getBottomRight?response='||serff_req_id||'" target="main_1">Response</a>' as response_xml, 
'<a href="getBottomRight?pmresponse='||serff_req_id||'" target="main_1">PM Response</a>' as pm_response_xml, 
'<a href="getBottomLeft?attachments='||serff_req_id||'" target="contents_1">Attachments</a>' as list1, 
CASE
WHEN (serff_req_id is NULL) THEN ''
ELSE '<a href="${pageContext.request.contextPath}/admin/downloadSOAPProjectFile?id='||serff_req_id||'&requestIdDownload='||serff_req_id||'&type=download">Download</a>'
END as download_option,
CASE
WHEN (request_status='F') THEN 'Failed'
WHEN (request_status='S') THEN 'Success'
WHEN (request_status='P') THEN 'In-Progress'
ELSE 'Unknown'
END as request_status, 
remarks, attachments_list,
CASE
WHEN (request_state='B') THEN 'Begin'
WHEN (request_state='E') THEN 'Ended'
WHEN (request_state='P') THEN 'In-Progress'
ELSE 'Waiting'
END as request_state,
request_state_desc ,
plan_stats
from serff_plan_mgmt 
where (serff_operation='TRANSFER_PLAN' or serff_operation='TRANSFER_PRESCRIPTION_DRUG' or serff_operation='TRANSFER_DRUGS_JSON' or serff_operation='TRANSFER_DRUGS_XML') 
and <%=whereClause%>
order by serff_req_id desc <%=topTenWhereClause %>
</sql:query>

Requests Received by SERFF 
	<% 
	//decode (request_status,'F','Failed','S','Success','P','In-Progress','Unknown') request_status
	//decode (request_state, 'B','Begin','E','Ended','P','In-Progress','W','Waiting') request_state
		if(request.getParameter("filterStatement") != null){
			out.println(request.getParameter("filterStatement"));
		}
	%>
<br>
<display:table name="${result1.rows}" requestURI="/admin/getTopPage" id="table1" style="white-space: pre-wrap;" export="true" pagesize="10">
	<display:column property="serff_req_id" title="ID" sortable="false" />
	<display:column property="issuer_id" title="Issuer ID" sortable="false"  />
	<display:column property="issuer_name" title="Issuer Name" sortable="false"  />
	<display:column property="hios_product_id" title="HIOS" sortable="false"  />
	<display:column property="plan_id" title="Plan ID" sortable="false" />
	<display:column property="plan_name" title="Plan Name" sortable="false" />
	<display:column property="serff_track_num" title="SERFF Track No" sortable="false" />
	<display:column property="state_track_num" title="State Track No" sortable="false" />
	<display:column property="serff_operation" title="Operation" sortable="false" />
	<display:column property="request_xml" title="Request" sortable="false" />
	<display:column property="response_xml" title="Response" sortable="false" />
	<display:column property="pm_response_xml" title="PM Response" sortable="false" />
	<display:column property="plan_stats" title="Plan Count Stats" sortable="false" />
	<display:column property="process_ip" title="Process IP" sortable="false" />
	<display:column property="start1" title="Start" sortable="false" />
	<display:column property="end1" title="End" sortable="false" />
	<display:column property="list1" title="Attachments" sortable="false" />
	<display:column property="download_option" title="Download" sortable="false" />
	<display:column property="request_status" title="Status" sortable="false" />
	<display:column property="remarks" title="Remarks" sortable="false" />
	<display:column property="request_state" title="State" sortable="false" />
	<display:column property="request_state_desc" title="State Desc" sortable="false" />
	<display:setProperty name="export.xml.filename" value="serffAdmin.xml"/>
	<display:setProperty name="export.csv.filename" value="serffAdmin.csv"/>
	<display:setProperty name="export.excel.filename" value="serffAdmin.xls"/>
</display:table>
