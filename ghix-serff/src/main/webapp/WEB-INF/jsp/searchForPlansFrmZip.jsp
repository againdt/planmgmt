<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<html>
<head>
	<title>Search for plans</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<%

String [] array = request.getParameterValues("selectzip");
String zipcode="00501";
String whereClause =null;
String zipcodeWhereClause = null;
if(array!=null){
	StringBuilder createWhere= new StringBuilder("zip in (") ;
	
	StringBuilder createzipcodeWhereClause= new StringBuilder("zipcode in (") ;
	
	for(int i=0 ;i<array.length;i++){
		String value = array[i];
		createWhere.append("'"+value +"',");
		
	}
	
	for(int j=0 ;j<array.length;j++){
		String value = array[j];
		createzipcodeWhereClause.append("'"+value +"',");
		
	}
	 whereClause =createWhere.substring(0,createWhere.length()-1);
	 zipcodeWhereClause= createzipcodeWhereClause.substring(0,createzipcodeWhereClause.length()-1);

	whereClause=whereClause+ ")";
	zipcodeWhereClause =zipcodeWhereClause+")";
}else{
	whereClause="zip ='" +zipcode + "'";
	zipcodeWhereClause ="zipcode = '"+zipcode+ "'";
}

%>

<body>
<b>Search plans for given Zip code<b>
	<form method="POST" action="getPlanFromZip">
	<df:csrfToken/>		
		<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0"> Zip code:
				<input type ="text" id="selectzip" name="selectzip" multiple="multiple">
				 <input type ="hidden" id ='list' name =list value="">
				<input type="submit" value="Search">
			</td>
			</tr>
		</table>
</body>


<sql:query dataSource="${jspDataSource}" var="result1">

 Select Distinct P.Issuer_Plan_Number ,P.Name , P.Issuer_Id , I.Hios_Issuer_Id , I.Short_Name ,
 P.Status ,P.Enrollment_Avail ,P.Issuer_Verification_Status
 From  Plan P, Pm_Plan_Rate Ppr  , Issuers I, Pm_Service_Area psa 
 Where I.Id = P.Issuer_Id And 
P.Id=Ppr.Plan_Id And psa.Service_Area_Id=P.Service_Area_Id and
Ppr.Rating_Area_Id In (Select Rating_Area_Id From Pm_Zip_County_Rating_Area Where <%=whereClause%> Or Lower(County) In
(Select lower(County) From Zipcodes Where <%=zipcodeWhereClause %>) ) 
And Psa.<%=whereClause%> 
and psa.is_deleted='N' and p.is_deleted='N' and ppr.is_deleted='N'


</sql:query>


<display:table name="${result1.rows}" requestURI="/admin/getPlanFromZip" id="table1" style="white-space: pre-wrap;" export="true" pagesize="50">
	<display:column property="hios_issuer_id" title="HIOS Issuer Id" sortable="true" />
	<display:column property="short_name" title="Issuer Name" sortable="true" />
	<display:column property="issuer_plan_number" title="Plan Number" sortable="true" />
	<display:column property="name" title="Plan Name" sortable="true" />
	<display:column property="issuer_id" title="Issuer Id" sortable="true" />
	<display:column property="Status" title="Plan Status" sortable="true" />
	<display:column property="Enrollment_Avail" title="Plan Enrollment" sortable="true" />
	<display:column property="Issuer_Verification_Status" title="Issuer Verification Status" sortable="true" />
	<display:setProperty name="export.xml.filename" value="ZipPlans.xml"/>
	<display:setProperty name="export.csv.filename" value="ZipPlans.csv"/>
	<display:setProperty name="export.excel.filename" value="ZipPlans.xls"/>
	
</display:table> 
<br>
Note: Query to check records in DB <br>
select distinct p.issuer_plan_number ,p.name , p.issuer_id , i.hios_issuer_id , i.short_name from  plan p , pm_plan_rate ppr  , issuers i where i.id = p.issuer_id and p.id=ppr.plan_id and
ppr.rating_area_id in (select rating_area_id from pm_zip_county_rating_area where zip = '?' or lower(county) in(select lower(county) from zipcodes where zipcode=?)) and 
p.service_area_id in (select service_area_id from pm_service_area where zip = '?');
<br>
</html>