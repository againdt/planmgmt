<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>SERFF Admin</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	
	<script type="text/javascript">
		$(function() {
			$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
			
			});
		
		$(function() {
			$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
			});
		
		function validateDates(){
		
			var dateStart = document.getElementById('date1').value.split('/');
			
			if(dateStart != "" && dateStart.length > 0)
			{
				
			var boolean1 = validateDate(dateStart);
			}
		
			var dateEnd = document.getElementById('date2').value.split('/');
			
			if(dateEnd !=  "" && dateEnd.length > 0)
		        {
				var boolean2 = validateDate(dateEnd);
			}
		}
		
		function validateDate(dateGiven){
			
			var today=new Date();
		
			var dob_mm = dateGiven[0];  
			var dob_dd = dateGiven[1];  
			var dob_yy = dateGiven[2]; 
			
			var birthDate=new Date();
			birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
		
			if( (today.getFullYear() - 2) >  birthDate.getFullYear() ) {
			//alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
			}
		
			if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) {
		//	alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
			}
		
			if(today.getTime() < birthDate.getTime()){
		//	alert('Please enter a valid date in MM/DD/YYYY format');
			return false; 
			}
			
		}
		
		function setWhereClause(type){
			
			var whereClause = '';
			var filterStatement = '';
			var topTenSelectClause='';
			var topTenWhereClause='';
			
			switch (type) {
				case 'B1' : 
					if(isPostgresDB) {
						whereClause = 'where creation_timestamp between (now()-interval \'1 day\') and now()';
					} else {
						whereClause = 'where creation_timestamp between (sysdate-1) and sysdate';
					}
					filterStatement = ' in last 24 hours';
					
					break;
				case 'B2' : 
					if(isPostgresDB) {
						whereClause = 'where creation_timestamp between (now()-interval \'12 hour\') and now()';
					} else {
						whereClause = 'where creation_timestamp between (sysdate-1/2) and sysdate';
					}
					filterStatement = ' in last 12 hours';
					break;
				case 'B3' : 
					if(isPostgresDB) {
						whereClause = 'where creation_timestamp between (now()-interval \'1 hour\') and now()';
					} else {
						whereClause = 'where creation_timestamp between (sysdate-1/24) and sysdate';
					}
					filterStatement = ' in last 1 hour';
					break;
				case 'B4' : 
				         //Step 1: Get List Box value, if it is not empty
					var fieldName = document.getElementById('field_name').value;
					var fieldValue = document.getElementById('field_value').value;
					
					if(fieldValue != null && fieldValue.length > 0)
					{
						if(isPostgresDB) {
							whereClause = "where lower(CAST ("+fieldName + " as TEXT)) like lower('%" + fieldValue + "%')";
						} else {
							whereClause = "where lower("+fieldName + ") like lower('%" + fieldValue + "%')";
						}
					filterStatement = ' with ' + fieldName + " = " + fieldValue;
					}
					
					
					//Step 3: Get the Dates
					var date1 = document.getElementById('date1').value;
					var date2 = document.getElementById('date2').value;
					
					if(date1 && date2)
					{	
						
						if( whereClause){
							whereClause = whereClause+"and creation_timestamp between to_timestamp('"+date1+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"+date2+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							
						}else{
							whereClause = "where creation_timestamp between to_timestamp('"+date1+" 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"+date2+" 23:59:59','mm/dd/yyyy HH24:MI:SS')";
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							
						}
					
					}else{
						if(date1&& !date2  ){
							if( whereClause){
							
							whereClause = whereClause+"and to_char(creation_timestamp  ,'mm/dd/yyyy' )='"+date1+"'" ;
							filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							}else{
								whereClause = "where to_char(creation_timestamp  ,'mm/dd/yyyy' )='"+date1+"'" ;
								filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
							}
						}else{
							
							 if(!date1 && date2 ){
								 if( whereClause){
										whereClause = whereClause+"and to_char(creation_timestamp  ,'mm/dd/yyyy' )='"+date2+"'" ;
										filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
								 }else{
										whereClause = "where to_char(creation_timestamp  ,'mm/dd/yyyy' )='"+date2+"'" ;
										filterStatement = filterStatement + 'start date ' +  date1 + 'end date ' +  date2;
									 }
						
								}
							}
						
						}if(!whereClause){
							if(isPostgresDB) {
								whereClause = "where creation_timestamp <= now()";
							} else {
								whereClause = "where creation_timestamp <= sysdate";
							}
						}
					
					break;
				case 'B5' : 
					if(isPostgresDB) {
						topTenWhereClause=' ) allrec limit 10';
				} else {
					topTenWhereClause=' ) where rownum <=10 order by  rownum';
					}
					topTenSelectClause=' Select * from ( ';
					filterStatement = 'for latest records';
					break;
				
			}
			
			document.getElementById('whereClause').value = whereClause;
			document.getElementById('filterStatement').value = filterStatement;
			document.getElementById('topTenSelectClause').value = topTenSelectClause;
			document.getElementById('topTenWhereClause').value = topTenWhereClause;
			//alert('whereClause==>'+whereClause);
			/* alert('topTenSelectClause==>'+topTenSelectClause);
			alert('topTenWhereClause==>'+topTenWhereClause); */
		}
	</script>    
</head>

<form method="POST" action="pufAdmin">
<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="24 Hours" name="B1" title="View requests made in last 24 hours" onclick="setWhereClause('B1');">
				<input type="submit" value="12 Hours" name="B2" title="View requests made in last 12 hours" onclick="setWhereClause('B2');">
				<input type="submit" value="1 Hour" name="B3" title="View requests made in last 1 hour" onclick="setWhereClause('B3');">
				<input type="submit" value="Show Latest" name="B5" title="View top 10 requests" onclick="setWhereClause('B5');">
			</td>
			<td bgcolor="#66FFCC">
				<p>
					&nbsp;Field:
					<select size="1" id="field_name" name="field_name">
					<!-- ID,STATE,ISSUER_NAME,PLAN_ID_STANDARD_COMPONENT,IS_DELETE,JOB_STATUS,FTP -->
						<option value="ID" selected="">Id</option>
						<option value="STATE" >State</option>
						<option value="ISSUER_NAME">Issuer Name</option>
						<option value="PLAN_ID_STANDARD_COMPONENT">Plan ID Standard Component </option>
						<option value="IS_DELETE">Is deleted</option>
						<option value="JOB_STATUS">Job Status</option>
						<option value="FTP_STATUS">FTP Status</option>
							<option value="SERFF_ERROR_CODE">Serff Error Code</option>
						
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
					
				Date Start:
				<input type="text" id="date1" name="date1" size="10" maxlength="10"> End:
				<input type="text" id="date2" name="date2" size="10" maxlength="10">
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
	<input id="topTenSelectClause" name="topTenSelectClause" type="hidden">
	<input id="topTenWhereClause" name="topTenWhereClause" type="hidden">
</form>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		if(isPostgresDB) {
			whereClause = "";
		} else {
			whereClause = "where rownum<=100";
		}
		
	}
	String topTenSelectClause = request.getParameter("topTenSelectClause");
	if( topTenSelectClause == null || topTenSelectClause == ""){
		topTenSelectClause="";
		
	}
	
	String topTenWhereClause = request.getParameter("topTenWhereClause");
	if( topTenWhereClause == null || topTenWhereClause == ""){
		topTenWhereClause="";
		
	}
%>

<sql:query dataSource="${jspDataSource}" var="result1">
<%=topTenSelectClause %>select ID,STATE,ISSUER_NAME,PLAN_ID_STANDARD_COMPONENT,IS_DELETE,JOB_STATUS,FTP_STATUS,ERROR_CODE,ERROR_MESSAGE,DURATION,SERFF_ERROR_CODE,SERFF_ERROR_MSG,CREATION_TIMESTAMP,LAST_UPDATED_BY,LAST_UPDATE_TIMESTAMP,DELETED_BY,DELETED_TIMESTAMP
from puf_qhp
 <%=whereClause%>
order by id desc <%=topTenWhereClause %>
</sql:query>

Requests Received by SERFF 
	<% 
		if(request.getParameter("filterStatement") != null){
			out.println(request.getParameter("filterStatement"));
		}
	%>
<br>


<display:table name="${result1.rows}" requestURI="/admin/pufAdmin" id="table1" style="white-space: pre-wrap;" export="true" pagesize="10">
	<display:column property="ID" title="ID" sortable="false" />
	<display:column property="STATE" title="State" sortable="false"  />
	<display:column property="ISSUER_NAME" title="Issuer Name" sortable="false"  />	
	<display:column property="PLAN_ID_STANDARD_COMPONENT" title="Plan ID" sortable="false" />
	<display:column property="IS_DELETE" title="Is Deleted" sortable="false" />
	<display:column property="JOB_STATUS" title="Job Status" sortable="false" />
	<display:column property="FTP_STATUS" title="FTP Status" sortable="false" />
	<display:column property="ERROR_CODE" title="Error Code" sortable="false" />
	<display:column property="ERROR_MESSAGE" title="Error Message" sortable="false" />
	<display:column property="DURATION" title="Duration" sortable="false" />
	<display:column property="SERFF_ERROR_CODE" title="SERFF_ERROR_CODE" sortable="false" />
	<display:column property="SERFF_ERROR_MSG" title="SERFF_ERROR_MSG" sortable="false" />

	<display:column property="ID" title="Download HIOS Templates" sortable="false" />
	<display:column property="ID" title="Error Log" sortable="false" />

	<display:setProperty name="export.xml.filename" value="serffAdmin.xml"/>
	<display:setProperty name="export.csv.filename" value="serffAdmin.csv"/>
	<display:setProperty name="export.excel.filename" value="serffAdmin.xls"/>
</display:table>


