<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp"%>

<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Get Disclaimer Information</title>
<script type="text/javascript" src="../resources/js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery.min.js"></script>
<% 
      String hostName = request.getHeader("host");
%>
<script>
	function populateJson(){		
		$("#inputJSON").val('{"id":'+ $("#planId").val()+',"exchangeType":"'+ $("input[name=exchangeType]:checked").val() + '"}');	
	}
	
	function checkLength(val) {
        var countMe = val;
        var escapedStr = encodeURI(countMe);
        if (escapedStr.indexOf("%") != -1) {
            var count = escapedStr.split("%").length - 1;
            if (count == 0) count++;  //perverse case; can't happen with real UTF-8
            var tmp = escapedStr.length - (count * 3);
            count = count + tmp;
        } else {
            count = escapedStr.length;
        }
        
        $('output[name="count"]').val(count + " bytes");       
        if (count > 0 ){
        	var kb = count/1024 + "KB";
        	 $('output[name="kb"]').val(kb + " KB");
		}
	}
	
	function submitData(val) {
		var start = new Date();
		var url = "";
		var defaultUrl = "ghix-planmgmt/issuerInfo/disclaimerInfo";
		var targetUrl = $('#targetURL').val();
		
		if (targetUrl != "") {
			url =  $("input[name=protocol]:checked").val() + "://" + targetUrl + "/" + defaultUrl;
		} else {
			alert("Please enter valid url, e.g. localhost:8080 ");
		}
		$.ajax({
			type : "POST",
			// dataType: "text/html",
			contentType : "application/json; charset=utf-8",
			url : url,
			data : val,
			crossDomain: true,
			success : function(responseText) {
				$("textarea[name='outputRaw']").val(responseText.disclaimerInfo);
				var end = new Date();
				var duration = end - start;
				$('output[name="duration"]').val(duration + " Milli Seconds");
				checkLength(responseText.disclaimerInfo);
			},
			error : function(responseText) {
				$("textarea[name='outputRaw']").val("Error occurred while getting plan info");
				var end = new Date();
				var duration = end - start;
				$('output[name="duration"]').val(duration + " Milli Seconds");
			}
		});
	}
	
	function doCollapse(){
		$('#collapse-btn').on('click', function() {
		    $('#json').JSONView('collapse');
		});
	}
	
	function doExpand(){		
		 $('#expand-btn').on('click', function() {
			$('#json').JSONView('expand');
		});
	}
</script>
</head>

<body>

<sql:query dataSource="${jspDataSource}" var="result">
	select id,issuer_plan_number,name from plan  where is_deleted = 'N' and end_date > SYSDATE and ROWNUM <= 10
</sql:query>

<table border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td width="100%" bgcolor="#FFCC00"><font face="Arial" size="2"><b>API: Get
      Disclaimer Info</b></font></td>
  </tr>
  <tr>
    <td width="100%" bgcolor="#FFCC00"><font face="Arial" size="2"><b>Path:</b> <a href="../../../../ghix-planmgmt/issuerInfo/disclaimerInfo">/ghix-planmgmt/issuerInfo/disclaimerInfo</a>
      &nbsp;</font></td>
  </tr>
  <tr>
    <td width="100%" bgcolor="#C4FCB8">
      <form method="POST" action="">        
        <p>
        <font face="Arial" size="2">Plan ID:         	
        	<select id="planId" name="planId">
				<option value="">Select</option>
				<c:forEach var="plan" items="${result.rows}">
					<option value="${plan.id}">${plan.id} - ${plan.issuer_plan_number} - ${plan.name}</option>
				</c:forEach>
			</select> 
			On Exchange <input type="radio" id="exchangeType" name="exchangeType" value="ON" checked>
        	Off Exchange <input type="radio" id="exchangeType" name="exchangeType" value="OFF"></font></p>
        	<p align="right"><font face="Arial" size="2"> (First 10 valid plans populated) 
        	<input type="button" value="Populate" name="Populate" onclick="populateJson();"> &nbsp;</font></p>
      </form>
    </td>
  </tr>
 
  <tr>
    <td width="100%" bgcolor="#C4FCB8">
      <form method="POST" action="--WEBBOT-SELF--">        
        <p><font face="Arial" size="2"><textarea rows="2" id="inputJSON" name="inputJSON" cols="40" style="background-color: #00FFFF">{"id":12,"exchangeType":"ON"}</textarea></font></p>
      </form>
    </td>
  </tr>
   <tr>
		<td width="100%" bgcolor="#C4FCB8">
			<p align="left">
				<font face="Arial" size="2">Protocol:
					<input type="radio" id="protocol" name="protocol" value="http" checked="checked"> HTTP
					<input type="radio" id="protocol" name="protocol" value="https"> HTTPS
				</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<font face="Arial" size="2">URL: 
					<input type="text" name="targetURL" size="20" style="background-color: #BDFFFF" value="<%=hostName%>" id="targetURL"> 
					<input type="button" value="Post" name="Post" onclick="submitData(document.getElementById('inputJSON').value);">
					<input type="reset" value="Reset" name="B2"></font>
			</p>
		</td>
	<tr>
  <tr>
    <td width="100%" bgcolor="#999999"><font face="Arial" size="2"><b>Duration</b>: 200 MilliSeconds&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <b>Size</b>: 10 bytes | .001 KB</font> </td>
  </tr>
  <tr>
    <td width="100%" bgcolor="#CCCCCC"><font face="Arial" size="2">Raw Response:</font>
      <p><font face="Arial" size="2"><textarea rows="20" name="outputRaw" cols="70" style="background-color: #BDFFFF"></textarea></font></td>
  </tr> 
  	<tr>
		<td width="100%" bgcolor="#CCCCCC">SQLs: <br>
			select ON_EXCHANGE_DISCLAIMER, OFF_EXCHANGE_DISCLAIMER from issuers  where  id=?		
		</td>
	</tr>
			
</table>
</body>
</html>
