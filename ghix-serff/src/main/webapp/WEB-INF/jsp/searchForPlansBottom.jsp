<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ include file="datasource.jsp" %>

<html>
<head>
	<title>Search for plans</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<%
	String whereClause = "id is not null";
	String andClause = "";
	String searchCriteria = "";

	if(request.getParameter("condition1")!= null && request.getParameter("condition1").equalsIgnoreCase("Y")){
		if(isPostgresDB) {
			andClause += " AND now() between START_DATE and END_DATE";
		} else {
		andClause += " AND SYSDATE between START_DATE and END_DATE";
		}
		searchCriteria += "Plan is currently effective, ";
	}
	if(request.getParameter("condition2")!= null && request.getParameter("condition2").equalsIgnoreCase("Y")){
		andClause += " AND STATUS IN ('CERTIFIED', 'RECERTIFIED')";
		searchCriteria += "Plan is certified, ";
	}
	if(request.getParameter("condition3")!= null && request.getParameter("condition3").equalsIgnoreCase("Y")){
		andClause += " AND ENROLLMENT_AVAIL ='AVAILABLE'";
		searchCriteria += "Enrollment is available, ";
	}
	if(request.getParameter("condition4")!= null && request.getParameter("condition4").equalsIgnoreCase("Y")){
		if(isPostgresDB) {
			andClause += " AND now() >= ENROLLMENT_AVAIL_EFFDATE";
		} else {
		andClause += " AND SYSDATE >= ENROLLMENT_AVAIL_EFFDATE";
		}
		searchCriteria += "Enrollment has started, ";
	}
	if(request.getParameter("condition5")!= null && request.getParameter("condition5").equalsIgnoreCase("Y")){
		andClause += " AND ISSUER_VERIFICATION_STATUS ='VERIFIED'";
		searchCriteria += "Issuer is verified, ";
	}
	if(request.getParameter("condition6")!= null && request.getParameter("condition6").equalsIgnoreCase("Y")){
		andClause += " AND IS_DELETED='N'";
		searchCriteria += "Plan is not deleted ";
	}
	
	if(andClause.equals("")){
		if(isPostgresDB) {
			andClause = "AND now() between START_DATE and END_DATE AND STATUS IN ('CERTIFIED', 'RECERTIFIED') AND ENROLLMENT_AVAIL ='AVAILABLE' AND SYSDATE >= ENROLLMENT_AVAIL_EFFDATE AND ISSUER_VERIFICATION_STATUS ='VERIFIED' AND IS_DELETED='N'";
		} else {
		andClause = "AND SYSDATE between START_DATE and END_DATE AND STATUS IN ('CERTIFIED', 'RECERTIFIED') AND ENROLLMENT_AVAIL ='AVAILABLE' AND SYSDATE >= ENROLLMENT_AVAIL_EFFDATE AND ISSUER_VERIFICATION_STATUS ='VERIFIED' AND IS_DELETED='N'";
		}
		searchCriteria = "All Plans";
	}
%>

<sql:query dataSource="${jspDataSource}" var="result1">
select INSURANCE_TYPE,ISSUER_PLAN_NUMBER,MARKET,NAME,STATUS,ISSUER_ID,BROCHURE,HSA,ENROLLMENT_AVAIL,ENROLLMENT_AVAIL_EFFDATE,ISSUER_VERIFICATION_STATUS,VERIFIED,HIOSPRODUCT_ID,STATE,IS_DELETED 
from plan 
where <%=whereClause%>
<%=andClause%>
</sql:query>

<h3>Selected Search Criteria: <span style="color:green"><%=searchCriteria%></span></h3>

<display:table name="${result1.rows}" requestURI="/admin/searchForPlansBottom" id="table1" style="white-space: pre-wrap;" export="true" pagesize="50">
	<display:column property="INSURANCE_TYPE" title="Insurance Type" sortable="true" />
	<display:column property="ISSUER_PLAN_NUMBER" title="Issuer Plan Number" sortable="true" />
	<display:column property="MARKET" title="Market Type" sortable="true" />
	<display:column property="NAME" title="Plan Name" sortable="true" />
	<display:column property="STATUS" title="Plan Status" sortable="true" />
	<display:column property="ISSUER_ID" title="Issuer Id" sortable="true" />
	<display:column property="BROCHURE" title="Brochure" sortable="true" />
	<display:column property="HSA" title="HSA" sortable="true" />
	<display:column property="ENROLLMENT_AVAIL" title="Is enrollment Available ?" sortable="true" />
	<display:column property="ENROLLMENT_AVAIL_EFFDATE" title="Enrollment effective Date" sortable="true" />
	<display:column property="ISSUER_VERIFICATION_STATUS" title="Issuer verification status" sortable="true" />
	<display:column property="VERIFIED" title="Is Plan Verified ?" sortable="true" />
	<display:column property="HIOSPRODUCT_ID" title="HIOS Product Id" sortable="true" />
	<display:column property="STATE" title="Plan State" sortable="true" />
	<display:column property="IS_DELETED" title="Is Plan Deleted" sortable="true" />
	
	<display:setProperty name="export.xml.filename" value="planSerach.xml"/>
	<display:setProperty name="export.csv.filename" value="planSerach.csv"/>
	<display:setProperty name="export.excel.filename" value="planSerach.xls"/>
</display:table> 
</html>