<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page contentType="text/html" import="java.util.*"%>
<%@ include file="datasource.jsp"%>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<title>New Page 1</title>

<script type="text/javascript"
	src="../resources/js/jquery-1.10.2.min.js"></script>
<link rel="stylesheet"
	href="https://rawgithub.com/yesmeck/jquery-jsonview/master/dist/jquery.jsonview.css" />
<script type="text/javascript"
	src="http://code.jquery.com/jquery.min.js"></script>
<script type="text/javascript"
	src="https://rawgithub.com/yesmeck/jquery-jsonview/master/dist/jquery.jsonview.js"></script>
	
</head>
<script type="text/javascript">

$(document).ready(function() {
	$("#memone").children().attr("disabled","disabled");
	$("#memtwo").children().attr("disabled","disabled");
	$("#memthree").children().attr("disabled","disabled");
	$("#memfour").children().attr("disabled","disabled");
	$("#planOne").children().attr("disabled","disabled");
	$("#planTwo").children().attr("disabled","disabled");
	$("#planThree").children().attr("disabled","disabled");
	
});
function enablePlan(){
	var memCheck1 = $("#plan1:checked").val();
	var memCheck2 = $("#plan2:checked").val();
	var memCheck3 = $("#plan3:checked").val();
	
	if(memCheck1=='on'){
		$("#planOne").children().removeAttr("disabled");
	}else{
		$("#planOne").children().attr("disabled","disabled");
	}
	
	if(memCheck2=='on'){
		$("#planTwo").children().removeAttr("disabled");
	}else{
		$("#planTwo").children().attr("disabled","disabled");
	}
	if(memCheck3=='on'){
		$("#planThree").children().removeAttr("disabled");
	}else{
		$("#planThree").children().attr("disabled","disabled");
	}
}
function enableMember(){
	var memCheck1 = $("#mem1:checked").val();
	var memCheck2 = $("#mem2:checked").val();
	var memCheck3 = $("#mem3:checked").val();
	var memCheck4 = $("#mem4:checked").val();
	//alert(memCheck1);
	if(memCheck1=='on'){
		$("#memone").children().removeAttr("disabled");
	}else{
		$("#memone").children().attr("disabled","disabled");
	}
	
	if(memCheck2=='on'){
		$("#memtwo").children().removeAttr("disabled");
	}else{
		$("#memtwo").children().attr("disabled","disabled");
	}
	if(memCheck3=='on'){
		$("#memthree").children().removeAttr("disabled");
	}else{
		$("#memthree").children().attr("disabled","disabled");
	}
	if(memCheck4=='on'){
		$("#memfour").children().removeAttr("disabled");
	}else{
		$("#memfour").children().attr("disabled","disabled");
	}
}
	function getValue(val) {

		var id = null;
		var zip = null;
		var countycode = null;
		var age = null;
		var tobacco = null;
		var relation = null;
		var mem1 = null;
		var mem2 = null;
		var mem3 = null;
		var mem4 = null;
		var plan1 = null;
		var plan2 = null;
		var plan3 = null;
		
		zip = $("#zipCode").val();
		countycode = $("#countyCode").val();
				
		var memCheck1 = $("#mem1:checked").val();
		var memCheck2 = $("#mem2:checked").val();
		var memCheck3 = $("#mem3:checked").val();
		var memCheck4 = $("#mem4:checked").val();
		
		var planCheck1 = $("#plan1:checked").val();
		var planCheck2 = $("#plan2:checked").val();
		var planCheck3 = $("#plan3:checked").val();
		
		if(planCheck1=='on'){
			id = $("#planId1").val();
			tiername = $("#tierName1").val();
			sumEmployeesPremium = $("#sumEmployeesPremium1").val();
			plan1 = "{\"planId\":\"" + id + "\"," 
					+ "\"tierName\":\"" + tiername + "\"," 
					+ "\"sumEmployeesPremium\":\"" + sumEmployeesPremium + "\"}"; 
			
		}
		
		if(planCheck2=='on'){
			id = $("#planId2").val();
			tiername = $("#tierName2").val();
			sumEmployeesPremium = $("#sumEmployeesPremium2").val();
			plan2 = ",{\"planId\":\"" + id + "\"," 
					+ "\"tierName\":\"" + tiername + "\"," 
					+ "\"sumEmployeesPremium\":\"" + sumEmployeesPremium + "\"}"; 
			
		}
		
		if(planCheck3=='on'){
			id = $("#planId3").val();
			tiername = $("#tierName3").val();
			sumEmployeesPremium = $("#sumEmployeesPremium3").val();
			plan3 = ",{\"planId\":\"" + id + "\"," 
					+ "\"tierName\":\"" + tiername + "\"," 
					+ "\"sumEmployeesPremium\":\"" + sumEmployeesPremium + "\"}"; 
			
		}
		
		if(memCheck1=='on'){
			id = $("#id").val();
			age = $("#age").val();
			tobacco = $("#tobacco:checked").val();
			relation = $("#relation").val();
			mem1 = "{\"id\":\"" + id + "\"," 
					+ "\"zip\":\"" + zip + "\"," 
					+ "\"countycode\":\"" + countycode + "\"," 
					+ "\"age\":\"" + age + "\"," 
					+ "\"tobacco\":\"" + tobacco + "\"," 
					+ "\"relation\":\"" + relation + "\"}";
			
		}
		
		if(memCheck2=='on'){
			id2 = $("#id").val();
			age2 = $("#age").val();
			tobacco2 = $("#tobacco:checked").val();
			relation2 = $("#relation2").val();
			mem2 = ",{\"id\":\"" + id2 + "\"," 
					+ "\"zip\":\"" + zip + "\"," 
					+ "\"countycode\":\"" + countycode + "\"," 
					+ "\"age\":\"" + age2 + "\"," 
					+ "\"tobacco\":\"" + tobacco2 + "\"," 
					+ "\"relation\":\"" + relation2 + "\"}";
		}
		
		if(memCheck3=='on'){
			id3 = $("#id").val();
			age3 = $("#age").val();
			tobacco3 = $("#tobacco:checked").val();
			relation3 = $("#relation3").val();
			mem3 = ",{\"id\":\"" + id3 + "\"," 
				+ "\"zip\":\"" + zip + "\"," 
				+ "\"countycode\":\"" + countycode + "\"," 
				+ "\"age\":\"" + age3 + "\"," 
				+ "\"tobacco\":\"" + tobacco3 + "\"," 
				+ "\"relation\":\"" + relation3 + "\"}";
		}
		
		if(memCheck4=='on'){
			id4 = $("#id").val();
			age4 = $("#age").val();
			tobacco4 = $("#tobacco:checked").val();
			relation4 = $("#relation4").val();
			mem4 = ",{\"id\":\"" + id4 + "\"," 
				+ "\"zip\":\"" + zip + "\"," 
				+ "\"countycode\":\"" + countycode + "\"," 
				+ "\"age\":\"" + age4 + "\"," 
				+ "\"tobacco\":\"" + tobacco4 + "\"," 
				+ "\"relation\":\"" + relation4 + "\"}";
		}
		
		var insType = null;
		var effectiveDate = null;
		effectiveDate = $("#effDate").val();
		insType = $("#insType").val();
		zipCode = $("#zipCode").val();
		fips = $("#countyCode").val();
		ehbCovered = $("#ehbCovered").val();
		
		var str1 =  "{\"effectiveDate\":\"" + effectiveDate + "\"," 
		+ "\"insType\":\"" + insType + "\"," 
		+ "\"empPrimaryZip\":\"" + zipCode + "\","
		+ "\"empCountyCode\":\"" + fips + "\","
		+ "\"ehbCovered\":\"" + ehbCovered + "\",";
		
		var strPlan = "\"planDTOList\":[" ;
		if(plan1 != null){
			strPlan=strPlan+plan1;
		}
		if(plan2 != null){
			strPlan=strPlan+plan2;
		}
		if(plan3 != null){
			strPlan=strPlan+plan3;
		}
		
		var str2 = "\"censusList\":[[[" ;
		if(mem1 != null){
			str2=str2+mem1;
		}
		if(mem2 != null){
			str2=str2+mem2;
		}
		if(mem3 != null){
			str2=str2+mem3;
		}
		if(mem4 != null){
			str2=str2+mem4;
		}
		str1=str1+strPlan+"],"+str2+"]]]}";
		$("textarea[name='inputJSON']").val(str1);

	}
	
	function doCollapse(){
		alert(1);
		$('#collapse-btn').on('click', function() {
		    $('#json').JSONView('collapse');
		  });
		
		
	}
	
	function doExpand(){
		alert(2);
		 $('#expand-btn').on('click', function() {
			    $('#json').JSONView('expand');
			  });
	}
	
	function getCountyFips(){
		var whereClause = "";
		var zipCode = $('#zipCode').val();
		whereClause = "zip="+zipCode;
		document.getElementById('whereClause').value = whereClause;
		//alert(whereClause);
		if(zipCode != null && zipCode != ""){
		$('#load').submit();
		}
				
	}
	
	function checkLength(val) {
	     var countMe = val;
	     var escapedStr = encodeURI(countMe);
	     if (escapedStr.indexOf("%") != -1) {
	         var count = escapedStr.split("%").length - 1;
	         if (count == 0) count++;  //perverse case; can't happen with real UTF-8
	         var tmp = escapedStr.length - (count * 3);
	         count = count + tmp;
	     } else {
	         count = escapedStr.length;
	     }
	     
	     $('output[name="count"]').val(count + " bytes");       
	     if (count > 0 ){
	      var kb = count/1024;
	      $('output[name="kb"]').val(kb + " KB");
	  }
	 }
	 
	 function submitData(val) {
	  var start = new Date();
	  var url = "";
	  var defaultUrl = "/ghix-planmgmt/planratebenefit/getemployerplanrate";
	  var targetUrl = $('#targetURL').val();
	  
	  if (targetUrl != "") {
	   url =  $("input[name=protocol]:checked").val() + "://" + targetUrl  + defaultUrl;
	  } else {
	   alert("Please enter valid url, e.g. localhost:8080 ");
	  }
	  $.ajax({
	   type : "POST",
	   // dataType: "text/html",
	   contentType : "application/json; charset=utf-8",
	   url : url,
	   data : val,
	   crossDomain: true,
	   header: { 'Access-Control-Allow-Origin': '*'},
	   success : function(responseText) {
	    $("textarea[name='outputRaw']").val(responseText);
	    //$("textarea[name='outputJson']").val(responseText);
	   // $('#json').JSONView(responseText);
	    var end = new Date();
	    var duration = end - start;
	    $('output[name="duration"]').val(duration + " Milli Seconds");
	    checkLength(responseText);
	   },
	   error : function(responseText) {
	    $("textarea[name='outputRaw']").val("Error occurred while getting plan rate");
	    var end = new Date();
	    var duration = end - start;
	    $('output[name="duration"]').val(duration + " Milli Seconds");
	   }
	  });
	 }
</script>
<% 
      String hostName = request.getHeader("host");
	
%>
<%
	String whereClause = request.getParameter("whereClause");
	 if( whereClause == null || whereClause == ""){
		whereClause = "zip='00000'";
		
	} 
	 
	String zip= request.getParameter("zipCode");
	if(zip != null){
		//whereClause = zip;
		request.setAttribute("zipCode",zip);
	}
	 
	
%>
<sql:query dataSource="${jspDataSource}" var="result1">
		select distinct(fips) from pm_service_area where  <%=whereClause %>
</sql:query>	
<body>


      <form method="POST"  id="load">
        <!--webbot bot="SaveResults" U-File="fpweb:///_private/form_results.txt"
        S-Format="TEXT/CSV" S-Label-Fields="TRUE" -->
        
        <table >
			<tr>
				<td width="50%" bgcolor="#FFCC00" align="left"><font
					face="Arial" size="2"><b>API: Employee Plan Rate</b></font></td>

				<td width="50%" bgcolor="#FFCC00" align="left"><font
					face="Arial" size="2"><b>Path:</b> <a
									href="../../../../ghix-planmgmt/planratebenefit/getEmployerPlanRate">/ghix-planmgmt/planratebenefit/getEmployerPlanRate</a>
						&nbsp;</font></td>
			</tr>

			<tr>
				<td width="50%" bgcolor="#C4FCB8">Insurance Type: 
					<select
							id="insType" name="insType">
							<option value="">Select</option>
							<option value="health">health</option>
			</select>
				</td>
				<td width="50%" bgcolor="#C4FCB8">Effective date: <input
					type="date" class="datepicker" id="effDate" name="effDate" />

				</td>
			</tr>

			<tr>
				<td bgcolor="#C4FCB8">Employer
									Quoting Zip Code: <input type="number"
					id="zipCode" name="zipCode" value="${zipCode}" maxlength="5">
					<input type="button" value="loadFips" name="loadFips"
					onclick="getCountyFips()">
				</td>

				<td bgcolor="#C4FCB8">County: <select id="countyCode"
					name="countyCode">
						<option value="">Select</option>
						<c:forEach var="servicearea" items="${result1.rows}">
							<option value="${servicearea.fips}">${servicearea.fips}
							</option>
						</c:forEach>

				</select>
				</td>
			</tr>
			<tr>
			<td bgcolor="#C4FCB8">
			EHB Covvered
			<select
							id="ehbCovered" name="ehbCovered">
							<option value="">Select</option>
							<option value="PARTIAL">PARTIAL</option>
							<option value="COMPLETE">COMPLETE</option>
			</select>
			</td>
			<td bgcolor="#C4FCB8">
			</td>				
			</tr>
			<tr>

				<td colspan="2" bgcolor="#C4FCB8"><input type="checkbox"
					id="mem1" name="mem1" onclick="enableMember();">
					<div id="memone">
						<b>Primary</b> Id: <input type="text" name="id" id="id">
						Age: <input type="text" id="age" name="age"> Relation: <select
							id="relation" name="relation">
							<option value="">Select</option>
							<option value="member">Member</option>
							<option value="spouse">Spouse</option>
							<option value="child">Child</option>
						</select> Tobacco: <input type="radio" id="tobacco" name="tobacco"
							value="y"> Yes <input type="radio" id="tobacco"
							name="tobacco" value="n" checked="checked">No
					</div></td>

			</tr>

			<tr>
				<td colspan="2" bgcolor="#C4FCB8"><input type="checkbox"
					id="mem2" name="mem2" onclick="enableMember();">
					<div id="memtwo">

						<b>Spouse</b> Id: <input type="text" name="id2" id="id2">
						Age: <input type="text" id="age2" name="age2"> Relation: <select
							id="relation2" name="relation2">
							<option value="">Select</option>
							<option value="member">Member</option>
							<option value="spouse">Spouse</option>
							<option value="child">Child</option>
						</select> Tobacco: <input type="radio" id="tobacco2" name="tobacco2"
							value="y"> Yes <input type="radio" id="tobacco2"
							name="tobacco2" value="n" checked="checked">No
					</div></td>
			</tr>

			<tr>
				<td colspan="2" bgcolor="#C4FCB8"><input type="checkbox"
					id="mem3" name="mem3" onclick="enableMember();">
					<div id="memthree">

						<b>Child</b> Id: <input type="text" name="id3" id="id3">
						Age: <input type="text" id="age3" name="age3"> Relation: <select
							id="relation3" name="relation3">
							<option value="">Select</option>
							<option value="member">Member</option>
							<option value="spouse">Spouse</option>
							<option value="child">Child</option>
						</select> Tobacco: <input type="radio" id="tobacco3" name="tobacco3"
							value="y"> Yes <input type="radio" id="tobacco3"
							name="tobacco3" value="n" checked="checked">No
					</div></td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#C4FCB8"><input type="checkbox"
					id="mem4" name="mem4" onclick="enableMember();">
					<div id="memfour">

						<b>Child</b> Id: <input type="text" name="id4" id="id4">
						Age: <input type="text" id="age4" name="age4"> Relation: <select
							id="relation4" name="relation4">
							<option value="">Select</option>
							<option value="member">Member</option>
							<option value="spouse">Spouse</option>
							<option value="child">Child</option>
						</select> Tobacco: <input type="radio" id="tobacco4" name="tobacco4"
							value="YES"> Yes <input type="radio" id="tobacco4"
							name="tobacco4" value="NO" checked="checked">No
					</div></td>
			</tr>
			
			<tr>
			<td colspan="2" bgcolor="#C4FCB8">
			<input type="checkbox"
					id="plan1" name="plan1" onclick="enablePlan();">
					<b>Plan 1</b>
					<div id="planOne">
					 Id: <input type="text" name="planId1" id="planId1">
					 Tier Name: 
					 <select
							id="tierName1" name="tierName1">
					 <option value="">Select</option>
							<option value="BRONZE">BRONZE</option>
							<option value="GOLD">GOLD</option>
							<option value="PLATINUM">PLATINUM</option>
							<option value="SILVER">SILVER</option>
					 </select>		
					 Sum Employees Premium: <input type="text" id="sumEmployeesPremium1" name="sumEmployeesPremium1">
					</div>
			</td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#C4FCB8"><input type="checkbox"
					id="plan2" name="plan2" onclick="enablePlan();"> <b>Plan 2</b>
					<div id="planTwo">
						Id: <input type="text" name="planId2" id="planId2"> Tier Name:
						<select
							id="tierName2" name="tierName2">
					 <option value="">Select</option>
							<option value="BRONZE">BRONZE</option>
							<option value="GOLD">GOLD</option>
							<option value="PLATINUM">PLATINUM</option>
							<option value="SILVER">SILVER</option>
					 </select>		 Sum
						Employees Premium: <input type="text" id="sumEmployeesPremium2"
							name="sumEmployeesPremium2">
					</div></td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#C4FCB8"><input type="checkbox"
					id="plan3" name="plan3" onclick="enablePlan();"> <b>Plan 3</b>
					<div id="planThree">
						Id: <input type="text" name="planId3" id="planId3"> Tier Name:
						<select
							id="tierName3" name="tierName3">
					 <option value="">Select</option>
							<option value="BRONZE">BRONZE</option>
							<option value="GOLD">GOLD</option>
							<option value="PLATINUM">PLATINUM</option>
							<option value="SILVER">SILVER</option>
					 </select>		 Sum
						Employees Premium: <input type="text" id="sumEmployeesPremium3"
							name="sumEmployeesPremium3">
					</div></td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#C4FCB8" align="center"><input
					type="button" value="Populate" name="Populate" onclick="getValue()"></td>
				<td></td>
			</tr>


			<tr>
				<td colspan="2" bgcolor="#C4FCB8"><textarea rows="10"
						name="inputJSON" cols="150" style="background-color: #BDFFFF"
						id="inputJSON">
						<c:out value="${inputJSON}"></c:out>
					</textarea></td>
				<td></td>

			</tr>
			<tr>
				<td colspan="2" bgcolor="#C4FCB8">
					<p align="left">
						<font face="Arial" size="2">Protocol: <input type="radio"
							id="protocol" name="protocol" value="http" checked="checked">
							HTTP <input type="radio" id="protocol" name="protocol"
							value="https"> HTTPS
						</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font face="Arial" size="2">URL:
							<input type="text" name="targetURL" size="20"
							style="background-color: #BDFFFF" value="<%=hostName%>"
							id="targetURL"> <input type="button" value="Post"
							name="Post"
							onclick="submitData(document.getElementById('inputJSON').value);">
							<input type="reset" value="Reset" name="B2">
						</font>
					</p>
				</td>
				<td></td>
			<tr>
			<tr>
				<td width="50%" bgcolor="#999999"><font face="Arial" size="2"><b>Duration</b>:
						<output id="duration" name="duration"> </output></td>
				<td width="50%" bgcolor="#999999"><b>Size</b>: <output
						id="count" name="count"> </output> | <output id="kb" name="kb">
					</output></td>
			</tr>
			<tr>
				<td colspan="2" bgcolor="#999999"><b>Raw Response:</b></td>
			</tr>
			<tr>
				<td align="left" bgcolor="#CCCCCC" colspan="2"><textarea
						cols="150" rows="20" name="outputRaw"
						style="background-color: #BDFFFF" id="outputRaw">
								<c:out value="${outputRaw}"></c:out>
							</textarea></td>
			</tr>
			<%-- <tr>
				<td colspan="2" bgcolor="#CCCCCC"><b>JSON Response:</b>
				<textarea
						cols="150" rows="20" name="outputJson"
						style="background-color: #BDFFFF" id="outputJson">
								<c:out value="${outputRaw}"></c:out>
							</textarea>
				<br><input
					type="button" value="Collapse" name="Collapse" id="collapse-btn"
					onclick="doCollapse()"> <input type="button" value="Expand"
					name="Expand" id="expand-btn" onclick="doExpand()"></td>
				<td></td>

			</tr>
			<tr>
				<td colspan="2" bgcolor="#CCCCCC"><div id="json"><c:out value="${outputRaw}"></c:out></div></td>
			<tr> --%>
			<tr>
				<td  bgcolor="#CCCCCC">SQLs:</td>
			</tr>
			
			<tr>
				<td colspan="2"><b>Health Query:</b>
				
				<div id="healthQuery">
				SELECT p.id    AS plan_id,
  SUM(pre)     AS total_premium,
  SUM(indvpre) AS indv_premium
FROM plan p,
  (
  (SELECT phealth.plan_id,
    prate.rate AS pre,
    1          AS memberctr,
    prate.rate AS indvpre
  FROM plan_health phealth,
    pm_service_area psarea,
    pm_plan_rate prate,
    plan p2,
    pm_zip_county_rating_area pzcrarea
  WHERE p2.id                 = phealth.plan_id
  AND phealth.parent_plan_id  = 0
  AND p2.service_area_id      = psarea.service_area_id
  AND psarea.zip              = '60164'
  AND p2.id                   = prate.plan_id
  AND p2.is_deleted           = 'N'
  AND prate.is_deleted        = 'N'
  AND psarea.is_deleted       = 'N'
  AND pzcrarea.rating_area_id = prate.rating_area_id
  AND (prate.tobacco          = 'Y'
  OR prate.tobacco           IS NULL)
  AND ( (41                  >= prate.min_age
  AND 41                     <= prate.max_age)
  OR (1005                   >= prate.min_age
  AND 1005                   <= prate.max_age) )
  AND TO_DATE ('2014-01-01', 'YYYY-MM-DD') BETWEEN prate.effective_start_date AND prate.effective_end_date
  AND (pzcrarea.zip        = '60164'
  OR pzcrarea.zip         IS NULL)
  AND psarea.fips          = '17031'
  AND pzcrarea.county_fips = '17031'
  )
UNION ALL
  (SELECT phealth.plan_id,
    prate.rate AS pre,
    1          AS memberctr,
    0          AS indvpre
  FROM plan_health phealth,
    pm_service_area psarea,
    pm_plan_rate prate,
    plan p2,
    pm_zip_county_rating_area pzcrarea
  WHERE p2.id                 = phealth.plan_id
  AND phealth.parent_plan_id  = 0
  AND p2.service_area_id      = psarea.service_area_id
  AND psarea.zip              = '60164'
  AND p2.id                   = prate.plan_id
  AND p2.is_deleted           = 'N'
  AND prate.is_deleted        = 'N'
  AND psarea.is_deleted       = 'N'
  AND pzcrarea.rating_area_id = prate.rating_area_id
  AND (prate.tobacco          = 'Y'
  OR prate.tobacco           IS NULL)
  AND ( (31                  >= prate.min_age
  AND 31                     <= prate.max_age)
  OR (1005                   >= prate.min_age
  AND 1005                   <= prate.max_age) )
  AND TO_DATE ('2014-01-01', 'YYYY-MM-DD') BETWEEN prate.effective_start_date AND prate.effective_end_date
  AND (pzcrarea.zip        = '60164'
  OR pzcrarea.zip         IS NULL)
  AND psarea.fips          = '17031'
  AND pzcrarea.county_fips = '17031'
  )
UNION ALL
  (SELECT phealth.plan_id,
    prate.rate AS pre,
    1          AS memberctr,
    0          AS indvpre
  FROM plan_health phealth,
    pm_service_area psarea,
    pm_plan_rate prate,
    plan p2,
    pm_zip_county_rating_area pzcrarea
  WHERE p2.id                 = phealth.plan_id
  AND phealth.parent_plan_id  = 0
  AND p2.service_area_id      = psarea.service_area_id
  AND psarea.zip              = '60164'
  AND p2.id                   = prate.plan_id
  AND p2.is_deleted           = 'N'
  AND prate.is_deleted        = 'N'
  AND psarea.is_deleted       = 'N'
  AND pzcrarea.rating_area_id = prate.rating_area_id
  AND (prate.tobacco          = 'Y'
  OR prate.tobacco           IS NULL)
  AND ( (23                  >= prate.min_age
  AND 23                     <= prate.max_age)
  OR (1005                   >= prate.min_age
  AND 1005                   <= prate.max_age) )
  AND TO_DATE ('2014-01-01', 'YYYY-MM-DD') BETWEEN prate.effective_start_date AND prate.effective_end_date
  AND (pzcrarea.zip        = '60164'
  OR pzcrarea.zip         IS NULL)
  AND psarea.fips          = '06013'
  AND pzcrarea.county_fips = '06013'
  ) ) temp
WHERE p.id           = temp.plan_id
AND p.id             = 329463
AND p.available_for IN ('ADULTONLY', 'ADULTANDCHILD')
GROUP BY p.id
HAVING SUM(memberctr) = 3</div>		
				</td>
				
			</tr>
			
			<tr>
				<td colspan="2" bgcolor="#CCCCCC">Notes:
					http://jsbin.com/bacet/4/edit</td>
				<td></td>
			</tr>
		</table>
        
        
<input id="whereClause" name="whereClause" type="hidden">
</form>
</body>

</html>
