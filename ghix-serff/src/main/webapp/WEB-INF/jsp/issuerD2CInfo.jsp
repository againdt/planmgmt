<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@include file="datasource.jsp" %>
<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8">
<head>
<title>Issuer D2C Info</title>
<script type="text/javascript">


function doSubmit(formName, url) {
	
	if (formName && url) {
		document.forms[formName].method = "post";
		document.forms[formName].action = url;
		document.forms[formName].submit();
	}
}



function setWhereClause(type){
	
	var whereClause = '';
	
	switch (type) {

		case 'B4' : 
			var fieldName = document.getElementById('field_name').value;
			var fieldValue = document.getElementById('field_value').value;
		//	alert(fieldValue);
			if(fieldValue != null && fieldValue.length > 0)
			{
			whereClause = " lower( idc."+fieldName + ") like lower('%" + fieldValue + "%')";
			filterStatement = ' with ' + fieldName + " = " + fieldValue;
			}
			
			if(!whereClause){
				whereClause = "i.id is not null and idc.d2cflag ='Y'";
			}
			break;
		}
	
	document.getElementById('whereClause').value = whereClause;

//	alert('whereClause==>'+whereClause);
}


</script>
</head>

<form method="POST" id="frmIssuerD2c" name="frmIssuerD2c" action="issuerD2CInfo">
<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			
			<td bgcolor="#66FFCC">
				<p>
			
					&nbsp;Field:
					<select size="1" id="field_name" name="field_name">
						<option value="hios_issuer_id" selected="">Hios Issuer Id</option>
						<option value="applicable_year" >Applicable Year</option>
					</select>
					<input type="text" id="field_value" name="field_value" size="10" maxlength="100">
					
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
	<input id="filterStatement" name="filterStatement" type="hidden">
	

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "i.id is not null and idc.d2cflag ='Y'";
	}
	
	
%>

<sql:query dataSource="${jspDataSource}" var="result1">

select idc.hios_issuer_id , idc.applicable_year ,idc.d2cflag  , to_char(idc.creation_timestamp ,'DD-MON-YY HH24:MI:SS') as creation_timestamp from issuer_d2c idc , issuers i where i.hios_issuer_id = idc.hios_issuer_id 
and <%=whereClause %> 
</sql:query>
Request received by IssuerD2C

	<% 
		if(request.getParameter("filterStatement") != null){
			out.println(request.getParameter("filterStatement"));
		}
	%>
<br>

<display:table name="${result1.rows}" requestURI="issuerD2CInfo" id="table1" style="white-space: pre-wrap;width:40%" export="true" pagesize="20"   defaultsort="1">
	<display:column property="hios_issuer_id" title="HIOS Issuer Id" sortable="true" sortName ='hios_issuer_id'/>
	<display:column property="applicable_year" title="Applicable Year" sortable="true" sortName ='applicable_year' />
	<display:column property="d2cflag" title="D2C Flag" sortable="true" />
	<display:column property="creation_timestamp" title="Creation time" sortable="true" />
	<display:setProperty name="export.xml.filename" value="IssuerD2CInfo.xml"/>
	<display:setProperty name="export.csv.filename" value="IssuerD2CInfo.csv"/>
	<display:setProperty name="export.excel.filename" value="IssuerD2CInfo.xls"/>
</display:table>
</form>

<br>
Note: Query to check records in DB <br>
select idc.hios_issuer_id , idc.applicable_year ,idc.d2cflag , to_char(idc.creation_timestamp ,'DD-MON-YY HH24:MI:SS') as creation_timestamp from issuer_d2c idc , issuers i where i.hios_issuer_id = idc.hios_issuer_id 
and i.hios_issuer_id ='?'
</body> 
 
</html>
