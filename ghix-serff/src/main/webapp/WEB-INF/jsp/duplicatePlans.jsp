<html>
<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>
<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8">

<sql:query dataSource="${jspDataSource}" var="result1">
select planYear,total from(
select planYear, count(planYear) as total
from(
select  issuer_plan_number||'('||applicable_year ||')' planYear
from plan
where IS_DELETED='N'
) res1 group by planYear
) res2 where total>1
order by total desc
</sql:query> 

<head>
<title>Duplicate Plans from plan table</title>
</head>

<body>

<b>Duplicate Plans from plan table</b>
<br>
Note: If there are any duplicate plans, we need to set is_delete='Y' in plan table manually.
DBA, PM need to make decision on what plans to delete
<br><br><br>
<display:table name="${result1.rows}"  requestURI="duplicatePlans" id="table1" export="true" pagesize="500" style="width:30%" defaultorder="descending" >
	<display:column property="planYear" title="Plan ID(Year)" sortable="true" />
	<display:column property="total" title="Total" sortable="true"/>
	<display:setProperty name="export.xml.filename" value="DuplicatePlans.xml"/>
	<display:setProperty name="export.csv.filename" value="DuplicatePlans.csv"/>
	<display:setProperty name="export.excel.filename" value="DuplicatePlans.xls"/>
</display:table>

 </body> 
 </html>