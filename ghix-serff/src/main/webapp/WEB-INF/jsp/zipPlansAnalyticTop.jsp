<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ page import="com.getinsured.hix.model.Plan"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>Zip Plans Analytic</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
	
	<script type="text/javascript">
	
		window.onload = function() {
			var zipCode = "<%=request.getParameter("zipCode")%>";
		    if (zipCode && zipCode != "null") {
		    	document.getElementById('zipCode').value = zipCode;
			}
		    
			var fips = "<%=request.getParameter("fips")%>";
		    if (fips && fips != "null") {
		    	document.getElementById('fips').value = fips;
			}
		    
			var hiosIssuerId = "<%=request.getParameter("hiosIssuerId")%>";
		    if (hiosIssuerId && hiosIssuerId != "null") {
		    	document.getElementById('hiosIssuerId').value = hiosIssuerId;
			}
		    
			var applicableYear = "<%=request.getParameter("applicableYear")%>";
		    if (applicableYear && applicableYear != "null") {
		    	document.getElementById('applicableYear').value = applicableYear;
			}
		    
			var planLevel = "<%=request.getParameter("planLevel")%>";
		    if (planLevel && planLevel != "null") {
		    	document.getElementById('planLevel').value = planLevel;
			}
		    
			var insuranceType = "<%=request.getParameter("insuranceType")%>";
		    if (insuranceType && insuranceType != "null") {
		    	document.getElementById('insuranceType').value = insuranceType;
			}
		};
	
		function setWhereClause(type) {
			var whereClause = "";
			
			switch (type) {
				case "B4" : 
					// Validate the dates
				    // validateDates();
				    // Step 1: Get List Box value, if it is not empty
					var zipCode = document.getElementById("zipCode").value;
					var fips = document.getElementById("fips").value;
					var hiosIssuerId = document.getElementById("hiosIssuerId").value;
					var applicableYear = document.getElementById("applicableYear").value;
					var planLevel = document.getElementById("planLevel").value;
					var insuranceType = document.getElementById("insuranceType").value;
					
					if (zipCode) {
						whereClause += " AND ZIP = '"+ zipCode + "' ";
					}
					
					if (fips) {
						whereClause += " AND FIPS = '"+ fips + "' ";
					}
					
					if (hiosIssuerId) {
						whereClause += " AND ISSUER_PLAN_NUMBER LIKE '"+ hiosIssuerId + "%' ";
					}
					
					if (applicableYear) {
						whereClause += " AND APPLICABLE_YEAR = '"+ applicableYear +"' ";
					}
					
					if (planLevel) {
						whereClause += " AND PLAN_LEVEL = '"+ planLevel +"' ";
					}
					
					if (insuranceType) {
						whereClause += " AND INSURANCE_TYPE = '"+ insuranceType +"' ";
					}
					break;
			}
			document.getElementById("whereClause").value = whereClause; // escape(whereClause);
		}
	</script>    
</head>

<%
    pageContext.setAttribute("planLevelByOptions", Plan.PlanLevel.values());
	pageContext.setAttribute("insuranceTypeOptions", Plan.PlanInsuranceType.values());
%>
<b>Counts zip & fips / View plans [HIX-51464]</b>
<form method="POST" action="${pageContext.request.contextPath}/admin/getZipPlansAnalyticTop">
<df:csrfToken/>
	<table border="1" cellspacing="0" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#66FFCC">
				<p>
					<label for="zipCode" class="control-label">&nbsp;<b>ZIP Code:</b></label>
					<input type="text" id="zipCode" name="zipCode" size="5" maxlength="5" />
					
					<label for="fips" class="control-label">&nbsp;<b>FIPs:</b></label>
					<input type="text" id="fips" name="fips" size="5" maxlength="5" />
					
					<label for="hiosIssuerId" class="control-label">&nbsp;<b>Issuer Plan Number or HIOS ID:</b></label>
					<input type="text" id="hiosIssuerId" name="hiosIssuerId" size="16" maxlength="16" />
					
					<label for="applicableYear" class="control-label">&nbsp;<b>Applicable Year:</b></label>
					<select id="applicableYear" name="applicableYear">
						<option value="" selected="selected">Select</option>
						<option value="2014">2014</option>
						<option value="2015">2015</option>
					</select>
				</p>
			</td>
		</tr>
		<tr>
			<td bgcolor="#66FFCC">
				<p>
					<label for="planLevel" class="control-label">&nbsp;<b>Metal Level:</b></label>
					<select id="planLevel" name="planLevel">
						<option value="" selected="selected">Select</option>
						<c:forEach var="planLevelByOption" items="${planLevelByOptions}">
							<option value="${planLevelByOption}">${planLevelByOption}</option>
						</c:forEach>
					</select>
					
					<label for="insuranceTypeOptions" class="control-label">&nbsp;<b>Plan Type:</b></label>
					<select id="insuranceType" name="insuranceType">
						<option value="" selected="selected">Select</option>
						<c:forEach var="insuranceTypeOption" items="${insuranceTypeOptions}">
							<option value="${insuranceTypeOption}">${insuranceTypeOption}</option>
						</c:forEach>
					</select>
					
					<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause('B4');">
					<button type="reset" value="Reset">Reset</button>
				</p>
			</td>
		</tr>
	</tbody></table>
	<input id="whereClause" name="whereClause" type="hidden">
</form>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "";
	}
%>

<sql:query dataSource="${jspDataSource}" var="zipPlanResults">
SELECT ZIP, FIPS, COUNTY, TOTAL2014, TOTAL2015,
'<a href="getZipPlansAnalyticBottom?zipCode='||ZIP||'&fips='||FIPS||'" target="bottom">SHOW</a>' AS SHOW,
(TOTAL2015 - TOTAL2014) AS DIFF
FROM (
	SELECT ZIP, FIPS, COUNTY,
	SUM(CASE WHEN APPLICABLE_YEAR = 2014 THEN 1 ELSE 0 END) AS TOTAL2014,
	SUM(CASE WHEN APPLICABLE_YEAR = 2015 THEN 1 ELSE 0 END) AS TOTAL2015,
	ZIP||','||FIPS||',PARAMS' AS SHOW
	FROM PM_ZIP_PLANS, ZIPCODES
	WHERE ZIP = ZIPCODE AND STATE_FIPS || COUNTY_FIPS = FIPS
	<%=whereClause%>
	GROUP BY ZIP, FIPS, COUNTY
) allrec ORDER BY DIFF
</sql:query>
<br>

<display:table name="${zipPlanResults.rows}" requestURI="getZipPlansAnalyticTop" id="zipPlanTable" style="white-space: pre-wrap;" export="true" pagesize="50">
	<display:column property="ZIP" title="ZIP" sortable="true" />
	<display:column property="FIPS" title="FIPs" sortable="true" />
	<display:column property="COUNTY" title="County" sortable="true" />
	<display:column property="TOTAL2014" title="Total 2014 Plans" sortable="true" />
	<display:column property="TOTAL2015" title="Total 2015 Plans" sortable="true" />
	<display:column property="DIFF" title="Difference" sortable="true" />
	<display:column property="SHOW" title="Show" sortable="true" />

	<display:setProperty name="export.xml.filename" value="zipPlansAnalyticCounts.xml"/>
	<display:setProperty name="export.csv.filename" value="zipPlansAnalyticCounts.csv"/>
	<display:setProperty name="export.excel.filename" value="zipPlansAnalyticCounts.xls"/>
</display:table>
