<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="session.jsp" %>
<%@ page import="com.serff.util.SerffConstants"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Upload Ancillary (AME,STM,HCC,VISION,LIFE) Plans to Database</title>
		
		<script type="text/javascript" charset="utf-8">
		function showPlanSql(planType) {
						
			var e = document.getElementById("planType");
			var planType = e.options[e.selectedIndex].value;
			var lineString = "------------------------------------------------------------------------------------------------------";
		    var sql1 = "";
		    var sql2 = "";
		    var sql3 = "";
		    var planTypeSQLString = "";
			
			if (planType == 'AME_FAMILY' || planType == 'AME_AGE') {
				sql1 = "<br>1. SELECT * FROM PLAN_AME WHERE PLAN_ID IN (SELECT ID FROM PLAN WHERE APPLICABLE_YEAR=0 AND IS_DELETED = 'N' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID='99969')) ORDER BY ID DESC; <br>";
				sql2 = "<br>2. SELECT * FROM PLAN_AME_BENEFITS WHERE PLAN_AME_ID IN (SELECT ID FROM PLAN_AME WHERE PLAN_ID IN (SELECT ID FROM PLAN WHERE APPLICABLE_YEAR=0 AND IS_DELETED = 'N' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID='99969'))) ORDER BY ID DESC; <br>";
				sql3 = "<br>3. SELECT * FROM PM_AME_PREMIUM_FACTOR WHERE IS_DELETED = 'N' AND ISSUER_HIOS_ID IN (SELECT HIOS_ISSUER_ID FROM ISSUERS WHERE HIOS_ISSUER_ID='99998') ORDER BY ID DESC; <br>";
				planTypeSQLString = "<br>AME Plan SQLs : <br>";
			}
			else if (planType == 'STM'  || planType == 'HCC') {
				sql1 = "<br>1. SELECT * FROM PLAN_STM WHERE PLAN_ID IN (SELECT ID FROM PLAN WHERE APPLICABLE_YEAR=0 AND IS_DELETED = 'N' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID='00001')) ORDER BY ID DESC; <br>";
				sql2 = "<br>2. SELECT * FROM PLAN_STM_BENEFITS WHERE PLAN_STM_ID IN (SELECT ID FROM PLAN_STM WHERE PLAN_ID IN (SELECT ID FROM PLAN WHERE APPLICABLE_YEAR=0 AND IS_DELETED = 'N' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID='00001'))) ORDER BY ID DESC; <br>";
				sql3 = "<br>3. SELECT * FROM PLAN_STM_COST WHERE PLAN_STM_ID IN (SELECT ID FROM PLAN_STM WHERE PLAN_ID IN (SELECT ID FROM PLAN WHERE APPLICABLE_YEAR=0 AND IS_DELETED = 'N' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID='00001'))) ORDER BY ID DESC; <br>";
				planTypeSQLString = "<br>STM Plan SQLs : <br>";
			}
			else if (planType == 'VISION') {
				sql1 = "<br>1. SELECT * FROM PLAN_VISION WHERE PLAN_ID IN (SELECT ID FROM PLAN WHERE APPLICABLE_YEAR=0 AND IS_DELETED = 'N' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID='00090')) ORDER BY ID DESC; <br>";
				sql2 = "<br>2. SELECT * FROM PLAN_VISION_BENEFITS WHERE PLAN_VISION_ID IN (SELECT ID FROM PLAN_VISION WHERE PLAN_ID IN (SELECT ID FROM PLAN WHERE APPLICABLE_YEAR=0 AND IS_DELETED = 'N' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID='00090'))) ORDER BY ID DESC; <br>";
				sql3 = "<br>3. SELECT * FROM PM_PLAN_RATE WHERE PLAN_ID IN (SELECT ID FROM PLAN WHERE APPLICABLE_YEAR=0 AND IS_DELETED = 'N' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID='00090')) ORDER BY ID DESC; <br>";
				planTypeSQLString = "<br>VISION Plan SQLs : <br>";
			}
			else if(planType == 'LIFE') {
				sql1 = "<br>1. SELECT * FROM PLAN_LIFE WHERE PLAN_ID IN (SELECT ID FROM PLAN WHERE APPLICABLE_YEAR=0 AND IS_DELETED = 'N' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID='00090')) ORDER BY ID DESC; <br>";
				sql2 = "<br>3. SELECT * FROM PM_PLAN_RATE WHERE PLAN_ID IN (SELECT ID FROM PLAN WHERE APPLICABLE_YEAR=0 AND IS_DELETED = 'N' AND ISSUER_ID IN (SELECT ID FROM ISSUERS WHERE HIOS_ISSUER_ID='L0001')) ORDER BY ID DESC; <br>";
				planTypeSQLString = "<br>LIFE Plan SQLs : <br>";
			}
			else if (planType == 'AREA_FACTOR' ){
				sql1 = "<br>1. SELECT * FROM PM_STM_HCC_AREA_FACTOR WHERE CARRIER_HIOS_ID ='00004' AND IS_DELETED ='N' BY ID DESC; <br>";
				planTypeSQLString = "<br>Area Factor SQL : <br>";
			}
			document.getElementById("planSQL").innerHTML = "<br>" + lineString + lineString + planTypeSQLString + lineString + lineString + sql1 + sql2 + sql3 + lineString + lineString
		}
		</script>
	</head>
<body>
	<p align="center">Upload Ancillary (AME,STM,HCC,VISION,LIFE) Plans to Database/ECM</p>
	<p align="center">&nbsp;</p>
	<div align="center">
		<center>
			<form action="${pageContext.request.contextPath}/admin/executeSerffSTMBatch" onsubmit="document.getElementById('submit').disabled=true;" method="POST">
				<table border="0" cellpadding="0" cellspacing="0" width="50%">
					<tr>
						<td align="center" style="font-size: 14pt;">
							<select onchange="showPlanSql()" id="planType" name="planType" >
								<option value="">Select Plan Type</option>
								<option value="<%=SerffConstants.PLAN_TYPE.AME_FAMILY.name()%>">AME [Family Rates]</option>
								<option value="<%=SerffConstants.PLAN_TYPE.AME_AGE.name()%>">AME [Age Band Rates]</option>
								<option value="<%=SerffConstants.PLAN_TYPE.AME_GENDER.name()%>">AME [Age-Gender based Rates]</option>
								<option value="<%=SerffConstants.PLAN_TYPE.STM.name()%>">STM</option>
								<option value="<%=SerffConstants.PLAN_TYPE.HCC.name()%>">STM for HCC With Rates</option>
								<option value="<%=SerffConstants.PLAN_TYPE.AREA_FACTOR.name()%>">STM HCC Area Factor</option>
								<option value="<%=SerffConstants.PLAN_TYPE.VISION.name()%>">VISION</option>
								<option value="<%=SerffConstants.PLAN_TYPE.LIFE.name()%>">LIFE</option>
							</select>
						</td>
					</tr>
					
					<tr>
						<td>
							<df:csrfToken/>
							<p align="center">
								<input type="submit" id="submit" name="submit" value="Upload Ancillary (AME,STM,HCC,VISION,LIFE) Plans to Database/ECM" style="font-size: 12pt; color: #0066CC">
							</p>
						</td>
					</tr>
					<c:if test="${executeBatchMessage != null}">
						<tr>
							<td align="center" style="font-size: 14pt;">
								<c:out value="${executeBatchMessage}" />
							</td>
						</tr>
					</c:if>
				</table>
			</form>
		</center>
	</div>
	<div style="font-size: 12pt;">
		Notes:
		Clicking above button will 
		<br>1. Reads file from FTP Location
		<br>2. Upload file to ECM for future reference
		<br>3. Parse data and load to Database
		<br>4. Display success / error message	
		<br>
	</div>
	<div id="planSQL">
		
	</div>
</body>
</html>
