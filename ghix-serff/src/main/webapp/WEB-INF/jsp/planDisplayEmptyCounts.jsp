<html>
<%@page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ include file="datasource.jsp" %>

<head>
	<title>Empty Plan Display Counts</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
	<link rel="stylesheet" href="/resources/demos/style.css" />
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
</head>

<body>

<sql:query dataSource="${jspDataSource}" var="result1">
SELECT I.HIOS_ISSUER_ID AS hiosId, P.APPLICABLE_YEAR,
  I.ID AS id,
  I.SHORT_NAME AS name,
  COUNT (*) AS count
FROM PLAN_HEALTH_BENEFIT PHB
JOIN PLAN_HEALTH PH
ON PH.ID = PHB.PLAN_HEALTH_ID
JOIN PLAN P
ON P.ID = PH.PLAN_ID
JOIN ISSUERS I
ON I.ID                          =P.ISSUER_ID
WHERE PHB.NETWORK_T1_DISPLAY    IS NULL
AND PHB.NETWORK_T2_DISPLAY      IS NULL
AND PHB.OUTNETWORK_DISPLAY      IS NULL
AND PHB.NETWORK_T1_TILE_DISPLAY IS NULL
AND P.IS_DELETED                 = 'N'
GROUP BY I.HIOS_ISSUER_ID,
  I.ID,P.APPLICABLE_YEAR,
  I.SHORT_NAME
ORDER BY COUNT DESC,
  I.HIOS_ISSUER_ID ASC
</sql:query> 

<b>Plan Display Benefits Count where all are null</b>
<br>
<display:table name="${result1.rows}"  requestURI="planDisplayStatus" id="table1" export="true"  style="width:10%">
	
	<display:column property="id" title="ID" style="white-space: nowrap" sortable="true"/>
	<display:column property="hiosId" title="Hios ID" style="white-space: nowrap" sortable="true"/>
	<display:column property="name" title="Issuer Name" style="white-space: nowrap" sortable="true"/>
	<display:column property="APPLICABLE_YEAR" title="Applicable Year" style="white-space: nowrap" sortable="true"/>
	<display:column property="count" title="Record Count" style="white-space: nowrap" sortable="true"/>
	
	<display:setProperty name="export.xml.filename" value="planDisplayStatus.xml"/>
	<display:setProperty name="export.csv.filename" value="planDisplayStatus.csv"/>
	<display:setProperty name="export.excel.filename" value="planDisplayStatus.xls"/>
</display:table>
<br>
<br>
Query:
SELECT I.HIOS_ISSUER_ID hiosId, P.APPLICABLE_YEAR,
  I.ID id,
  I.SHORT_NAME name,
  COUNT (*) AS count
FROM PLAN_HEALTH_BENEFIT PHB
JOIN PLAN_HEALTH PH
ON PH.ID = PHB.PLAN_HEALTH_ID
JOIN PLAN P
ON P.ID = PH.PLAN_ID
JOIN ISSUERS I
ON I.ID                          =P.ISSUER_ID
WHERE PHB.NETWORK_T1_DISPLAY    IS NULL
AND PHB.NETWORK_T2_DISPLAY      IS NULL
AND PHB.OUTNETWORK_DISPLAY      IS NULL
AND PHB.NETWORK_T1_TILE_DISPLAY IS NULL
AND P.IS_DELETED                 = 'N'
GROUP BY I.HIOS_ISSUER_ID,
  I.ID,P.APPLICABLE_YEAR,
  I.SHORT_NAME
ORDER BY COUNT DESC,
  I.HIOS_ISSUER_ID ASC

</body> 
</html>


