<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page import="com.serff.util.PlanManagmentReportsRecord"%>
<%@include file="datasource.jsp"%>

<%@ page import="com.serff.admin.PlanManagementRepots"%>

<%! private static PlanManagementRepots planManagementRepots = null; %>

<%
	String dateFilter = request.getParameter("dateFilter");
	if(dateFilter == null){
		dateFilter = "365";
	}

	planManagementRepots = ctx.getBean("planManagementRepots", PlanManagementRepots.class);
	List<PlanManagmentReportsRecord> records = planManagementRepots.getDatewiseStats(dateFilter);
	pageContext.setAttribute("records", records);
%>

<html>
<head>
	<link rel="stylesheet" href="../resources/css/displaytag_serff.css" type="text/css" charset="utf-8" />
	
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <!-- <script type="text/javascript" src="../resources/js/jsapi.js"></script> -->
    
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Creation Date', 'Start Date','End Date', 'Updation Date','Certified Date','Enrollment Effective Date'],
          	<c:forEach var="row" items="${records}">
				['<c:out value="${row.date}"/>',<c:out value="${row.createdCount}"/>,<c:out value="${row.startDateCount}"/>,
				 <c:out value="${row.endDateCount}"/>,<c:out value="${row.updationCount}"/>,
				 <c:out value="${row.certifiedDateCount}"/>,<c:out value="${row.enrollmentEffectiveDateCount}"/>],
			</c:forEach>
        ]);

        var options = {
          title: 'Date-wise statistics',
          curveType: "function",
          width: 1200, height: 400,
          vAxis: {maxValue: 10}
        };

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
      
      function setDateFilter(dateFilter) {
    	  document.getElementById('dateFilter').value = dateFilter;
      }
      
    </script>
  </head>
  <body>
  	
  	<form method="POST" action="dateTables">
	<h3>Choose Date Filter</h3>
	<df:csrfToken/>
	<table border="1" style="border-color:#C0C0C0">
		<tbody><tr>
			<td bgcolor="#C0C0C0">
				<input type="submit" value="30 days" name="B1" title="View statistics for last 30 days" onclick="setDateFilter('30');">
				<input type="submit" value="60 days" name="B2" title="View statistics for last 60 days" onclick="setDateFilter('60');">
				<input type="submit" value="90 days" name="B3" title="View statistics for last 90 days" onclick="setDateFilter('90');">
				
				<input type="submit" value="180 days" name="B4" title="View statistics for last 180 days" onclick="setDateFilter('180');">
				<input type="submit" value="365 days" name="B5" title="View statistics for last 365 days" onclick="setDateFilter('365');">
			</td>
		</tr>
		</tbody>
	</table>
	<h4>Current Filter : <%=dateFilter%> days</h4>
	<input id="dateFilter" name="dateFilter" type="hidden">
	</form>			
    <div id="chart_div" style="height: 500px;"></div>
    <h3>Table showing counts for respective dates</h3>
    <display:table name="${records}"  requestURI="dateTables" id="table1" export="true" pagesize="500" defaultsort="1" defaultorder="ascending">
		<display:column property="date" title="Date" sortable="true" style="width:100px"/>
		
		<display:column property="createdCount" title="Created Date" sortable="true" />
		<display:column property="startDateCount" title="Start Date" sortable="true" />
		<display:column property="endDateCount" title="End Date" sortable="true" />
		
		<display:column property="updationCount" title="Updated" sortable="true" />
		<display:column property="certifiedDateCount" title="Certified Date" sortable="true" />
		<display:column property="enrollmentEffectiveDateCount" title="Enrollment Effective Date" sortable="true" />
		
		<display:setProperty name="export.xml.filename" value="dateTable.xml"/>
		<display:setProperty name="export.csv.filename" value="dateTable.csv"/>
		<display:setProperty name="export.excel.filename" value="dateTable.xls"/>
	</display:table>
  </body>
</html>