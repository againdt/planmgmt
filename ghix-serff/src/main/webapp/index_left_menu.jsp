<!DOCTYPE html>
<%@ page import="com.serff.util.SerffConstants" %>
<%@ page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil" %>
<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum" %>

<%!
	boolean isPHIXProfile = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE).equals(SerffConstants.PHIX_PROFILE);
	
	boolean isCAProfile = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase("CA");
%>

<html lang="en">
<%@ include file="/WEB-INF/jsp/datasource.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<base target="main">
	<link rel="stylesheet" href="resources/css/reset.css" type="text/css" charset="utf-8">
	<link rel="stylesheet" href="resources/css/core.css" type="text/css" charset="utf-8">
	<link rel="stylesheet" href="resources/css/accordion.core.css" type="text/css" charset="utf-8">
	
	<style type="text/css">
		.loading {
			display: none;
		}
		
	    .accordion {
	        border: 1px solid #ccc;
	        width:  100%;
	    }
	    
        .accordion li h3 a {
            background:             #666;
            background:             #666 -webkit-gradient(linear, left top, left bottom, from(#999), to(#666)) no-repeat;
            background:             #666 -moz-linear-gradient(top,  #999,  #666) no-repeat;
            border-bottom:          1px solid #333;
            border-top:             1px solid #ccc;
            color:                  #fff;
            display:                block;
            font-style:             normal;
            margin:                 0;
            padding:                5px 10px;
            text-shadow:            0 -1px 2px #333, #ccc 0 1px 2px;
        }
        
        .accordion li.active h3 a {
            background:             #369;
            background:             #369 -webkit-gradient(linear, left top, left bottom, from(#69c), to(#369)) no-repeat;
            background:             #369 -moz-linear-gradient(top,  #69c,  #369) no-repeat;
            border-bottom:          1px solid #036;
            border-top:             1px solid #9cf;
            text-shadow:            0 -1px 2px #036, #9cf 0 1px 2px;
        }
        
        .accordion li.locked h3 a {
            background:             #963;
            background:             #963 -webkit-gradient(linear, left top, left bottom, from(#c96), to(#963)) no-repeat;
            background:             #963 -moz-linear-gradient(top,  #c96,  #963) no-repeat;
            border-bottom:          1px solid #630;
            border-top:             1px solid #fc9;
            text-shadow:            0 -1px 2px #630, #fc9 0 1px 2px;
        }
        
		.accordion li h3 {
		    margin:         0;
		    padding:        0;
		}
		
		.accordion .panel {
		    padding:        10px;
		}
	</style>
</head>
<body bgcolor="#FFFFFF">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td width="90%"><a href="index_right_content.jsp" >SERFF Admin</a></td>
    <td> <a href="logout.jsp" target="_top"><img src="./resources/img/exit.png" width="20"  alt="Logout"></a></td>
  </tr>
</table>
    
    <br><a href="admin/getMain" >View Incoming Data</a>
	
	<div id="page">
		<ul id="example4" class="accordion">
			<li>
				<h3>Admin Links</h3>
				<div class="panel loading">
					<a href="admin/getMain" >View Incoming Data</a>
					<br><a href="admin/ecmStatus" target="main">DB/ECM Check</a>
					<br><a href="admin/loggeradmin" target="main">Log4j Admin</a>
					<br><a href="admin/allTemplatesLastModifiedDate" target ="main">Templates Date Check</a>
					<br><a href="admin/displayUnixConsole" target="main">SERFF Unix Console</a>
					<br><a href="admin/postSerffData" >Post/Download Data</a>
				</div>
			</li>

			<li>
				<h3>Configurations</h3>
				<div class="panel loading">
					<a href="admin/sysinfo" target="main">System Info</a>
					<br><a href="admin/serffInfo" target="main">SERFF Info</a>
					<br><a href="admin/serffConfigurations" target="main">SERFF Data</a>
					<br><a href="admin/pmConfigurations" target="main">Plan Mgmt Data</a>
					<% if (isPHIXProfile) { %>
					<br><a href="admin/ecmEmailTemplates" target="main">PM Email Templates</a>
					<br><a href="admin/allECMEmailemplates" target ="main">All Email Templates</a>
					<% } %>
				</div>
			</li>
			<li>
				<h3>Upload SERFF Plans/Docs</h3>
				<div class="panel loading">
			<% if (!isCAProfile) { %>
					<a href="admin/loadUploadPlanDetails" target="main">Upload Plans</a><br>
			<% } %>
					<a href="admin/getBatchMain">Uploaded Plan Status</a><br>
					<a href="admin/getUploadPlans">Upload Plan Util</a><br>
			<% if (!isCAProfile) { %>
					<a href="admin/getUploadBrochureCount">Upload PDF Util for SERFF</a>
					<br><a href="admin/getPlanDocs">Uploaded PDF Status</a>
					<br><a href="admin/uploadPlanDocsExcel">Upload Plan Docs Excel</a>
					<br><a href="admin/uploadPlanDocsExcelStatus">Upload Plan Docs Status</a>
					<br><a href="admin/uploadMain?uploadType=medicaid" target="main">Upload Medicaid</a>
					<br><a href="admin/uploadMedicaidPlansStatus">Upload Medicaid Plans Status</a>
					<!-- <br><a href="admin/uploadPressDrugAndPlan" target="main">Upload Presscription & Drug</a> -->
			<% } %>
				</div>
			</li>
			<% if (isPHIXProfile) { %>
			<li>
				<h3>Plan Mgmt Reports</h3>
				<div class="panel loading">
					<a href="admin/searchForPlans" target="main">Search for Plans</a>
					<br><a href="admin/pieCharts" target="main">Pie Charts</a>
					<br><a href="admin/dateTables" target="main">Date Tables</a>
					<br><a href="admin/allIssuersStatus" target="main">Issuers Plans Status</a>
					<!-- <br><a href="admin/getReqCount" target="main">SERFF Traffic</a> 
					<br><a href="admin/planLoadingTimeGraph" target="main">Plan loading time graph</a>-->
				</div>
			</li>

			<li>
				<h3>Plan Mgmt APIs</h3>
				<div class="panel loading">
					<a href="admin/getPlanMgmtAPI" target="main">Plan Management API</a>
					<br><a href="admin/getPlanMgmtAPIHelp" target="main">Plan Management API Help</a>
				</div>
			</li>

			<li>
				<h3>Plan Validations</h3>
				<div class="panel loading">
					<a href="admin/errorMsg">Error Msg</a>
					<br><a href="admin/duplicatePlans">Duplicate Plans</a>
					<br><a href="admin/rateRange">Rate Range Existing</a>
					<br><a href="admin/searchPlanRate">Rate Range Check</a>
					<br><a href="admin/planNameCheck">Check Plan Names</a>
					<br><a href="admin/issuerStatus">Issuer Status</a>					
					<br><a href="admin/urlCheck">Issuer URLs</a>										
					<br><a href="admin/duplicateRec">Duplicate Records</a>
					<br><a href="admin/getPlanFromZip">Get plans from Zip</a>
					<br><a href="admin/getIssuerFromZip">Get Issuer from Zip</a>
					<!-- <br><a href="admin/PlanMgmtViewData">Plan Mgmt Data (TBD)</a> -->										
					<br><a href="admin/zipRatingArea">Zip Rating Area</a>
				</div>
			</li>
			<% } %>

			<li>
				<h3>Plan Display Benefits</h3>
				<div class="panel loading">
					 <!--<a href="admin/planDisplayEmptyCounts" target="main">Empty Display Counts</a>
					<br><a href="admin/planDisplayStatus" target="main">Search Plan Display Data</a>
					<br><a href="admin/processT1DisplayUtil" target="main">Process T1 Display Data</a>-->
					<a href="admin/getEditDisplayRule"  target="main">Edit Plan Display Rules</a>
					<br><a href="admin/processDisplayUtil" target="main">Process Health Display Data</a>
					<br><a href="admin/processDentalDisplayUtil" target="main">Process Dental Display Data</a>  
					<% if (isPHIXProfile) { %>
					<br><a href="admin/reapplyplanBenefits">Re-apply Plan Benefit Edits</a>
					<% } %>
				</div>
			</li>
			<!-- <li>
				<h3>Utilities</h3>
				<div class="panel loading">
					<a href="admin/migrateIssuerLogoUI">Migrate Issuer Logo to CDN</a>
				</div>
			</li> -->
			<% if (isPHIXProfile) { %>

			<li>
				<h3>Zip - Plans Analytics</h3>
				<div class="panel loading">
					<a href="admin/getZipPlansAnalytic" target="main">Zip & Fips Counts</a>
					<br><b>Display Zips with</b>
					<br><a href="admin/getZipWith2ndLowestSilverPlans" target="main"> - 2nd lowest silver plans</a>
					<br><a href="admin/getZipWithNativeAmericanPlans" target="main"> - native american plans</a>
					<br><b>Display Zips without</b>
					<br><a href="admin/getZipWithoutPlans" target="main"> - plans</a>
					<!-- <br><a href="javascript:void(0);" target="main"> - 2nd lowest silver plans</a>
					<br><a href="javascript:void(0);" target="main"> - native american plans</a> -->
				</div>
			</li>

			<li>
				<h3>Medicare Bulk Upload</h3>
				<div class="panel loading">
					<a href="admin/updateIssuerLogo">Update Issuer Logo</a>
					<br><a href="admin/issuerlogoBulkUpdateStatus">Update Issuer Logo Status</a>
					<br><a href="admin/updatePlanBrochure">Update Plan Brochure</a>
					<br><a href="admin/uploadPlanBrochureStatus">Update Plan Brochure Status</a>
				</div>
			</li>
			<% } %>

			<% if (isCAProfile) { %>
			<li>
				<h3>Cross Walk Templates</h3>
				<div class="panel loading">
					<a href="admin/searchCrossWalkData" target="main">Cross Walk Search</a>
					<br><a href="admin/crossWalkUploadAdmin" target="main">Cross Walk Files</a>
				</div>
			</li>
			<% } %>

			<% if (isPHIXProfile) { %>
			<li>
				<h3>Ancillary (Phix)</h3>
				<div class="panel loading">
					<!-- 	<a href="admin/uploadSTMExcelForm" target="_self">Upload Ancillary</a> -->
					<a href="admin/uploadMain?uploadType=ancillary" target="main">Upload Ancillary</a>
					<br><a href="admin/getUploadSTMPlans" target="main">Upload Ancillary Util</a>
					<br><a href="admin/getSTMPlansStatus" target="main">Uploaded Ancillary Status</a>
					<br><a href="admin/loadUploadAncillaryDocs" target="main">Upload PDF Util for Ancillary</a>
					<br><a href="admin/getPlanDocs" target="main">Uploaded PDF Status</a>
					<br><a href="admin/loadInvokeQuotitService">Invoke Quotit Service</a>
					<br><a href="admin/getZipCodeInfoQuotitService" target="main">Quotit ZipCode Info</a>
					<br><a href="admin/loadQuotitServicesStatus">Quotit Services Status</a>
				</div>
			</li>

			<li>
				<h3>Custom Plans  (Phix)</h3>
				<div class="panel loading">
					<!-- <a href="admin/aetnaPlanStatus" target="main">Aetna Plan Status</a>
					<br><a href="admin/uhcPlanStatus" target="main">UHC Plan Status</a><br> -->
					<a href="admin/issuerD2CInfo" target="main">Issuer D2C Information</a>
					<br><a href="admin/issuerD2C" target="main">Issuer Brand Names</a>
				</div>
			</li>

			<li>
				<h3>PUF  (Phix)</h3>
				<div class="panel loading">
					<b>Controlling UI</b>
					<br><a href="admin/loadPUFExcel" target="main">Upload Medicare PUF Excel</a>
					<br><a href="admin/loadPUFStatus" target="main">Medicare PUF Status</a>
					<br><a href="admin/pufAdmin" target="main">PUF Admin</a>
					<br><a href="admin/pufAllPlansStatus" target="main">PUF Plan Status</a>

					<br><b>Reports</b>
					<br><a href="admin/pufIssuers" target="main">PUF Issuers</a>
					<br><a href="admin/pufPlanStatus" target="main">PUF Plans Status</a>
					<br><a href="admin/pufPlanSearch" target="main">Search PUF Plans</a>
				</div>
			</li>
			<!-- <li>
				<h3>Migrate Edit Plan</h3>
				<div class="panel loading">
					<a href="admin/migratePlan" target="main">Search Edit Plans</a>
				</div>
			</li> -->
			
			<% } %>
        </ul>
	</div>
		<script type="text/javascript" src="resources/js/jquery-1.4.2.min.js" charset="utf-8"></script>
		<script type="text/javascript" src="resources/js/jquery.accordion.2.0.js" charset="utf-8"></script>
		<script type="text/javascript">
			$('#example4').accordion({
				canToggle: true,
				canOpenMultiple: true
			});
			$(".loading").removeClass("loading");
		</script>
    
    
    </body>
</html>