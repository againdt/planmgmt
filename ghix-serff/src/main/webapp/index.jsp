<%@ page language="java" contentType="text/html; charset=ISO-8859-1"	pageEncoding="ISO-8859-1"%>
<%@ page import = "java.util.*" %>
<%@ page import = "javax.servlet.RequestDispatcher" %>
<%@ page import = "com.serff.util.SerffConstants" %>
<%@page import="com.getinsured.hix.platform.util.GhixPlatformConstants" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>


<%
	String given=(String)session.getAttribute("total");
	String passKey=(String)request.getParameter("passKey");
	String errorMessage="";
	
	String adminPassword = GhixPlatformConstants.SERFF_CSR_PASSKEY;
	
	if(null == adminPassword || adminPassword.isEmpty()) {
		adminPassword = SerffConstants.SERFF_CSR_PASSKEY_VALUE;
	}
	
	//System.out.println(adminPassword);
	if(null != given && adminPassword != null && passKey != null &&  passKey.equals(adminPassword)) {
		//out.println("success");
		session = request.getSession(false);
		
		if(null == session) {
			session = request.getSession(true);
		}
		session.setAttribute( "loggedin", "true" );
		//-------------------------------------------------------------
		
		RequestDispatcher rd = request.getRequestDispatcher("mainmenu.jsp");     
		rd.forward(request, response);
	}
	else if(null != passKey) {
		errorMessage="<font size='3' color='red'>Error!</font>";	
	}
%>
<!-- ============================ Validation Ends =============================== -->

<%
	String display = null;
	int total= 0;
	Random rn = new Random();
	int i = rn.nextInt() % 10;
	int j = rn.nextInt() % 9;
	int prefix = rn.nextInt() % 1000;
	int suffix = rn.nextInt() % 99;
	int middle = rn.nextInt() % 999;

	if (i < 0) {
		i = i * -1;
	}

	if (j < 0) {
		j = j * -1;
	}

	if (prefix < 0) {
		prefix = prefix * -1;
	}

	if (suffix < 0) {
		suffix = suffix * -1;
	}

	if (middle < 0) {
		middle = middle * -1;
	}

	display = "" + prefix + "<font color='red'>" + i + "</font>"
			+ middle + "<font color='red'>" + j + "</font>" + j
			+ suffix;
	System.out.println(display);
	total = i + j;
	System.out.println(total);
	session.setAttribute("total", total + "");
%>



<html>
  <head>
    <title>SERFF Login Page</title>
  </head>

  <body bgcolor=white>
</br></br></br></br></br></br></br></br></br>
</br></br></br></br></br>
  <table border="0" cellpadding="10" align=center>
    <tr>
      <td align=center>
	<form name="login" method="post" autocomplete="off">
	<df:csrfToken/>
	<%= display %><input type="text" name="answered" size="2">
	    <strong>Secure Key</strong>
	    <input type="password" size="15" name="passKey" autocomplete="off">        
	    <input type="submit" value="Submit">
	</form>        
	
	<%= errorMessage%>
	
	
      </td>
     
    </tr>
  </table>

  </body>
</html> 