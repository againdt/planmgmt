<%@ page import="com.serff.util.SerffConstants" %>
<%@ page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil" %>
<%@ page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum" %>

<html>

<head>
	<meta http-equiv="Content-Language" content="en-us">
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
	<title>Admin Links</title>
	<script type="text/javascript" src="resources/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			document.getElementById("currentDate").innerHTML = "<b>" + new Date().toLocaleString() + "</b>";
		});
	</script>
</head>

<body>

<%!
	boolean isPHIXProfile = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase(SerffConstants.PHIX_PROFILE);
	boolean isCAProfile = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE).equalsIgnoreCase("CA");
%>

	<table border="0" cellpadding="2" cellspacing="0" width="100%">
		<tr>
			<td width="80%">
				<h2 style="color: rgb(0, 0, 0); font-family: Times New Roman; font-style: normal; font-variant: normal; letter-spacing: normal; line-height: normal; orphans: auto; text-indent: 0px; text-transform: none; white-space: normal; widows: auto; word-spacing: 0px; -webkit-text-stroke-width: 0px" align="center">
					<font size="4">SERFF Admin</font>
				</h2>
			</td>
			<td width="20%">
				<font size="4"><p id="currentDate" align="right" /></font>
			</td>
		</tr>
	</table>

	<table border="0" cellpadding="5" cellspacing="0" width="100%">
		<tr><td width="25%" valign="top">
			<!-- Widget 1 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #DC4546">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#DC4546"> 
							<p align="center"><b><font color="#FFFFFF">Admin Links</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
								<a href="admin/getMain" target="_self">View Incoming Data</a>
								<br><a href="admin/ecmStatus" target="_self">DB/ECM Check</a>
								<br><a href="admin/loggeradmin" target="_self">Log4j Admin</a>
								<br><a href="admin/allTemplatesLastModifiedDate" target="_self">Templates Date Check</a>
								<br><a href="admin/displayUnixConsole" target="_self">SERFF Unix Console</a>
								<br><a href="admin/postSerffData" target="_self">Post/Download Data</a>
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			<!-- Widget 1 End -->
			<br>
			<!-- Widget 4 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #DC4546">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#DC4546" valign="top"> 
							<p align="center"><b><font color="#FFFFFF">Configurations</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
								<a href="admin/sysinfo" target="_self">System Info</a>
								<br><a href="admin/serffInfo" target="_self">SERFF Info</a>
								<br><a href="admin/serffConfigurations" target="_self">SERFF Data</a>
								<br><a href="admin/pmConfigurations" target="_self">Plan Mgmt Data</a>
								<% if (isPHIXProfile) { %>
								<br><a href="admin/ecmEmailTemplates" target="_self">PM Email Templates</a>
								<br><a href="admin/allECMEmailemplates" target="_self">All Email Templates</a>
								<% } %>
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			<!-- Widget 4 End -->
			<br>
			<% if (!isCAProfile) { %>
			<!-- Widget 3 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #DC4546">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#DC4546" valign="top"> 
							<p align="center"><b><font color="#FFFFFF">Upload SERFF Plans/Docs</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
								<a href="admin/loadUploadPlanDetails" target="_self">Upload Plans</a>
								<br><a href="admin/getBatchMain" target="_self">Uploaded Plan Status</a>
								<br><a href="admin/getUploadPlans" target="_self">Upload Plan Util</a>
								<br><a href="admin/getUploadBrochureCount" target="_self">Upload PDF Util for SERFF</a>
								<br><a href="admin/getPlanDocs" target="_self">Uploaded PDF Status</a>
								<br><a href="admin/uploadPlanDocsExcel">Upload Plan Docs Excel</a>
								<br><a href="admin/uploadPlanDocsExcelStatus">Upload Plan Docs Status</a>
								<br><a href="admin/uploadMain?uploadType=medicaid" target="main">Upload Medicaid</a>
								<br><a href="admin/uploadMedicaidPlansStatus">Upload Medicaid Plans Status</a>
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			<% } %>
			<!-- Widget 3 End -->
	    </td>
		<% if (isPHIXProfile) { %>
	    <td width="25%" valign="top">
			<!-- Widget 5 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #4AAD4A">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#4AAD4A" valign="top"> 
							<p align="center"><b><font color="#FFFFFF">Plan Mgmt Reports</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
								<a href="admin/searchForPlans" target="_self">Search for Plans</a>
								<br><a href="admin/pieCharts" target="_self">Pie Charts</a>
								<br><a href="admin/dateTables" target="_self">Date Tables</a>
								<br><a href="admin/allIssuersStatus" target="_self">Issuers Plans Status</a>
								<!-- <br><a href="admin/getReqCount" target="_self">SERFF Traffic</a> 
								<br><a href="admin/planLoadingTimeGraph" target="_self">Plan loading time graph</a>-->
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			<!-- Widget 5 End -->
			<br>
			<!-- Widget 6 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #4AAD4A">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#4AAD4A" valign="top"> 
							<p align="center"><b><font color="#FFFFFF">Plan Mgmt APIs</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
								<a href="admin/getPlanMgmtAPI" target="_self">Plan Management API</a>
								<br><a href="admin/getPlanMgmtAPIHelp" target="_self">Plan Management API Help</a>
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			<!-- Widget 6 End -->
			<br>
			<!-- Widget 7 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #4AAD4A">
				<tr><td width="100%">
				<table border="0" cellpadding="2" cellspacing="0" width="100%">
					<tr><td width="100%" bgcolor="#4AAD4A" valign="top"> 
						<p align="center"><b><font color="#FFFFFF">Plan Validations</font></b>
					</td></tr>
					<tr><td width="100%">
						<font size="4">
							<a href="admin/errorMsg" target="_self">Error Msg</a>
							<br><a href="admin/duplicatePlans" target="_self">Duplicate Plans</a>
							<br><a href="admin/rateRange" target="_self">Rate Range Existing </a>
							<br><a href="admin/searchPlanRate" target="_self">Rate Range Check</a>
							<br><a href="admin/planNameCheck" target="_self">Check Plan Names</a>
							<br><a href="admin/issuerStatus" target="_self">Issuer Status</a>
							<br><a href="admin/urlCheck">Issuer URLs</a>																					
							<br><a href="admin/duplicateRec" target="_self">Duplicate Records</a>
							<br><a href="admin/getPlanFromZip" target="_self">Get plans from Zip</a>
							<br><a href="admin/getIssuerFromZip" target="_self">Get Issuer from Zip</a>
							<!-- <br><a href="admin/PlanMgmtViewData" target="_self">Plan Mgmt Data (TBD)</a> -->							
							<br><a href="admin/zipRatingArea">Zip Rating Area</a>
						</font>
					</td></tr>
				</table>
				</td></tr>
			</table>
			<!-- Widget 7 End -->    
		</td>
		<% } %>
		<td width="25%" valign="top">
			<!-- Widget 40 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #CF6700">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#CF6700" valign="top"> 
							<p align="center"><b><font color="#FFFFFF">Plan Display Benefits</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
							   <!--  <a href="admin/planDisplayEmptyCounts" target="_self">Empty Display Counts</a>
								<br><a href="admin/planDisplayStatus" target="_self">Search Plan Display Data</a>
								<br><a href="admin/processT1DisplayUtil" target="_self">Process T1 Display Data</a>
								 -->
								<a href="admin/getEditDisplayRule" target="_self">Edit Plan Display Rules</a>
								<br><a href="admin/processDisplayUtil" target="_self">Process Health Display Data</a>
								<br><a href="admin/processDentalDisplayUtil" target="_self">Process Dental Display Data</a>
								<% if (isPHIXProfile) { %>
								<br><a href="admin/reapplyplanBenefits">Re-apply Plan Benefit Edits</a>
								<% } %>
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			<br>
			<!-- <table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #CF6700">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#CF6700" valign="top"> 
							<p align="center"><b><font color="#FFFFFF">Utilities</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
								<a href="admin/migrateIssuerLogoUI">Migrate Issuer Logo to CDN</a>
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			<br> -->
			<% if (isPHIXProfile) { %>
			<!-- Widget 40 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #CF6700">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#CF6700" valign="top"> 
							<p align="center"><b><font color="#FFFFFF">Zip - Plans Analytics</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
								<a href="admin/getZipPlansAnalytic" target="_self">Zip & Fips Counts</a>
								<br><font size="3"><b>Display Zips with</b></font>
								<br><a href="admin/getZipWith2ndLowestSilverPlans" target="_self"> - 2nd lowest silver plans</a>
								<br><a href="admin/getZipWithNativeAmericanPlans" target="_self"> - native american plans</a>
								<br><font size="3"><b>Display Zips without</b></font> 
								<br><a href="admin/getZipWithoutPlans" target="_self"> - plans</a>
								<!-- <br><a href="javascript:void(0);" target="_self"> - 2nd lowest silver plans</a>
								<br><a href="javascript:void(0);" target="_self"> - native american plans</a> -->
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #CF6700">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#D4AF37" valign="top"> 
							<p align="center"><b><font color="#FFFFFF">Medicare Bulk Upload</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
								<a href="admin/updateIssuerLogo">Update Issuer Logo</a>
								<br><a href="admin/issuerlogoBulkUpdateStatus">Update Issuer Logo Status</a>
								<br><a href="admin/updatePlanBrochure">Update Plan Brochure</a>
								<br><a href="admin/uploadPlanBrochureStatus">Update Plan Brochure Status</a>
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			
			<!-- Widget 40 End -->
			<% } %>
			<% if (isCAProfile) { %>
			<br>
			<!-- Widget 400 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #D4AF37">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#D4AF37" valign="top"> 
							<p align="center"><b><font color="#FFFFFF">Cross Walk Templates (CA)</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
								<a href="admin/searchCrossWalkData">Cross Walk Search</a>
								<br><a href="admin/crossWalkUploadAdmin">Cross Walk Files</a>
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			<!-- Widget 400 End -->
			<% } %>
		</td>
		<% if (isPHIXProfile) { %>
		<td width="25%" valign="top">
			<!-- Widget 2 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #3F8ABF">
				<tr><td width="100%">
				<table border="0" cellpadding="2" cellspacing="0" width="100%">
					<tr><td width="100%" bgcolor="#3F8ABF"> 
						<p align="center"><b><font color="#FFFFFF">Ancillary (Phix)</font></b>
					</td></tr>
					<tr><td width="100%">
						<font size="4">
						<!-- 	<a href="admin/uploadSTMExcelForm" target="_self">Upload Ancillary</a> -->
							<a href="admin/uploadMain?uploadType=ancillary" target="main">Upload Ancillary</a>
							<br><a href="admin/getUploadSTMPlans" target="_self">Upload Ancillary Util</a>
							<br><a href="admin/getSTMPlansStatus" target="_self">Uploaded Ancillary Status</a>
							<br><a href="admin/loadUploadAncillaryDocs" target="_self">Upload PDF Util for Ancillary</a>
							<br><a href="admin/getPlanDocs" target="_self">Uploaded PDF Status</a>
							<br><a href="admin/loadInvokeQuotitService">Invoke Quotit Service</a>
							<br><a href="admin/getZipCodeInfoQuotitService" target="main">Quotit ZipCode Info</a>
							<br><a href="admin/loadQuotitServicesStatus">Quotit Services Status</a>
						</font>
					</td></tr>
				</table>
				</td></tr>
			</table>
			<!-- Widget 2 End -->
			<br>
			<!-- Widget 41 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #3F8ABF">
				<tr><td width="100%">
				<table border="0" cellpadding="2" cellspacing="0" width="100%">
					<tr><td width="100%" bgcolor="#3F8ABF" valign="top"> 
						<p align="center"><b>
						<font color="#FFFFFF">Custom Plans (Phix)</font></b>
					</td></tr>
					<tr><td width="100%">
						<font size="4">
							<!-- <a href="admin/aetnaPlanStatus" target="_self">Aetna Plan Status</a>
							<br><a href="admin/uhcPlanStatus" target="_self">UHC Plan Status</a><br>  -->
							<a href="admin/issuerD2CInfo" target="_self">Issuer D2C Information</a>
							<br><a href="admin/issuerD2C" target="_self">Issuer Brand Names</a>
						</font>
					</td></tr>
				</table>
				</td></tr>
			</table>
			<!-- Widget 41 End -->
			<br>
			<!-- Widget 42 Begin -->
			<table border="1" cellpadding="0" cellspacing="0" width="100%" style="border: 1 solid #3F8ABF">
				<tr><td width="100%">
					<table border="0" cellpadding="2" cellspacing="0" width="100%">
						<tr><td width="100%" bgcolor="#3F8ABF" valign="top">
							<p align="center"><b><font color="#FFFFFF">PUF (Phix)</font></b>
						</td></tr>
						<tr><td width="100%">
							<font size="4">
								<a href="admin/loadPUFExcel" target="main">Upload Medicare PUF Excel</a>
								<br><a href="admin/loadPUFStatus" target="main">Medicare PUF Status</a>
								<br><a href="admin/pufAdmin" target="_self">PUF Admin</a>	
								<br><a href="admin/pufAllPlansStatus" target="_self">PUF Plan Status</a>
	
								<br><font size="3"><b>Reports</b></font>
								<br><a href="admin/pufIssuers" target="_self">PUF Issuers</a>
								<br><a href="admin/pufPlanStatus" target="_self">PUF Plans Status</a>
								<br><a href="admin/pufPlanSearch" target="_self">Search PUF Plans</a>
							</font>
						</td></tr>
					</table>
				</td></tr>
			</table>
			<!-- Widget 42 End -->
		</td>
		<% } %>
		</tr>
	</table>
</body>
</html>
