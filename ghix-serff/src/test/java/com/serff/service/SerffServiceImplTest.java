/**
 * 
 */
package com.serff.service;

import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.DefaultConfigurationBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.model.serff.SerffDocument;

/**
 * @author talreja_n
 *
 */
public class SerffServiceImplTest{
	
	@Autowired SerffService serffService;
	
	
	//@Test
	public void saveDocument() {
		SerffDocument doc = new SerffDocument();
		doc = serffService.saveSerffDocument(doc);
	}
	
	//@Test
	public void testConfig() throws ConfigurationException {
		DefaultConfigurationBuilder xmlConfig = new DefaultConfigurationBuilder("E:/Project/ghix-git/ghix/ghix-setup/conf/appconf/ghix-configuration.xml");
		CombinedConfiguration combinedConfig = xmlConfig.getConfiguration(true);
		System.out.println(combinedConfig.getString("planManagement.SHOPQuoting.ConsiderMemberPMPM"));
		System.out.println(combinedConfig.getList("planManagement.HealthCosts.CostNames"));
	}
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws ConfigurationException {
		DefaultConfigurationBuilder xmlConfig = new DefaultConfigurationBuilder("E:/Project/ghix-git/ghix/ghix-setup/conf/appconf/ghix-configuration.xml");
		CombinedConfiguration combinedConfig = xmlConfig.getConfiguration(true); 
		//System.out.println(combinedConfig.getString("planManagement.SHOPQuoting.ConsiderMemberPMPM"));
		//List<String> costs = combinedConfig.getList("planManagement.HealthCosts.CostNames");
		//List<String> constNames = combinedConfig.getList("planManagement.HealthCosts.ConstNames");
		/*Map<String, String> healthCosts = new HashMap<String, String>();
		for(int i=0; i<costs.size(); i++){
			healthCosts.put(costs.get(i), constNames.get(i));
		}
		System.out.println(healthCosts);*/
	}

}
