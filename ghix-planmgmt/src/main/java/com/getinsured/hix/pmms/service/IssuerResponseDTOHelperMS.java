/* 
@author Santanu 
@Version 1.0
@Date 10 Feb 2017
*/

package com.getinsured.hix.pmms.service;

import com.getinsured.hix.dto.planmgmt.microservice.IssuerDTO;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;

public class IssuerResponseDTOHelperMS {

	// setObjectToDTO method set Object to IssuerDTO DTO
	public IssuerDTO setObjectToDTO(Issuer issuer, boolean minimizeIssuerData){
		IssuerDTO issuerDTO = new IssuerDTO();
		issuerDTO.setId(issuer.getId());
		issuerDTO.setName(issuer.getName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getName());
		issuerDTO.setHiosIssuerId(issuer.getHiosIssuerId() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getHiosIssuerId());
		issuerDTO.setCompanyLogo(issuer.getLogoURL() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLogoURL());
		issuerDTO.setCarrierApplicationUrl(issuer.getApplicationUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getApplicationUrl());
		issuerDTO.setProducerPortalUname(issuer.getProducerUserName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getProducerUserName());
		issuerDTO.setProducerPortalPwd(issuer.getProducerPassword() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getProducerPassword());
		issuerDTO.setProducerPortalUrl(issuer.getProducerUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getProducerUrl());
		issuerDTO.setPaymentUrl(issuer.getPaymentUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getPaymentUrl());
		if(issuer.getIssuerBrandName() != null){
			issuerDTO.setParentBrandId(issuer.getIssuerBrandName().getId());
			issuerDTO.setParentBrandName(issuer.getIssuerBrandName().getBrandName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIssuerBrandName().getBrandName());
			//issuerDTO.setIsDeleted(issuer.getIssuerBrandName().getIsDeleted() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIssuerBrandName().getIsDeleted());
			//issuerDTO.setBrandUrl(issuer.getIssuerBrandName().getBrandUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIssuerBrandName().getBrandUrl());
		}else{
			issuerDTO.setParentBrandId(PlanMgmtConstants.ZERO);
			issuerDTO.setParentBrandName(PlanMgmtConstants.EMPTY_STRING);
			//issuerDTO.setIsDeleted(PlanMgmtConstants.EMPTY_STRING);
			//issuerDTO.setBrandUrl(PlanMgmtConstants.EMPTY_STRING);					
		}
		issuerDTO.setFederalEmployeeId(issuer.getFederalEin() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getFederalEin());
		issuerDTO.setNaicCompanyCode(issuer.getNaicCompanyCode() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getNaicCompanyCode());
		issuerDTO.setNaicGroupCode(issuer.getNaicGroupCode() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getNaicGroupCode());
		issuerDTO.setShortName(issuer.getShortName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShortName());
		issuerDTO.setStreetAddress1(issuer.getAddressLine1() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAddressLine1());
		issuerDTO.setStreetAddress2(issuer.getAddressLine2() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAddressLine2());
		issuerDTO.setCity(issuer.getCity() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCity());
		issuerDTO.setState(issuer.getState() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getState());
		issuerDTO.setZip(issuer.getZip() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getZip());
		
		issuerDTO.setEffectiveStartDate(issuer.getEffectiveStartDate());
		issuerDTO.setEffectiveEndDate(issuer.getEffectiveEndDate());
		issuerDTO.setCreationTimestamp(issuer.getCreationTimestamp());
		issuerDTO.setLastUpdateTimestamp(issuer.getLastUpdateTimestamp());

		if(!minimizeIssuerData){
			issuerDTO.setCompanyLegalName(issuer.getCompanyLegalName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyLegalName());
			issuerDTO.setStatus(issuer.getCertificationStatus() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCertificationStatus());
			
			//issuerDTO.setAccreditingEntity(issuer.getAccreditingEntity() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAccreditingEntity());
			//issuerDTO.setLicenseNumber(issuer.getLicenseNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLicenseNumber());
			//issuerDTO.setLicenseStatus(issuer.getLicenseStatus() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLicenseStatus());
			
			//issuerDTO.setPhoneNumber(issuer.getPhoneNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getPhoneNumber());
			//issuerDTO.setEmailAddress(issuer.getEmailAddress() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getEmailAddress());
			//issuerDTO.setEnrollmentUrl(issuer.getEnrollmentUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getEnrollmentUrl());
			//issuerDTO.setContactPerson(issuer.getContactPerson() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getContactPerson());
			//issuerDTO.setInitialPayment(issuer.getInitialPayment() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getInitialPayment());
			//issuerDTO.setRecurringPayment(issuer.getRecurringPayment() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getRecurringPayment());
			//issuerDTO.setSiteUrl(issuer.getSiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getSiteUrl());
			issuerDTO.setIssuerState(issuer.getStateOfDomicile() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getStateOfDomicile());
			issuerDTO.setCompanyAddress1(issuer.getCompanyAddressLine1() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyAddressLine1());
			issuerDTO.setCompanyAddress2(issuer.getCompanyAddressLine2() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyAddressLine2());
			issuerDTO.setCompanyCity(issuer.getCompanyCity() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyCity());
			issuerDTO.setCompanyState(issuer.getCompanyState() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyState());
			issuerDTO.setCompanyZip(issuer.getCompanyZip() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyZip());
			//issuerDTO.setCompanySiteUrl(issuer.getCompanySiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanySiteUrl());

			if (null != issuer.getNationalProducerNumber()) {
				issuerDTO.setNationalProducerNum(issuer.getNationalProducerNumber());
			}
			issuerDTO.setIndvMktCustServicePhoneNumber(issuer.getIndvCustServicePhone() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIndvCustServicePhone());
			issuerDTO.setIndvMktCustServicePhoneNumberExt(issuer.getIndvCustServicePhoneExt() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIndvCustServicePhoneExt());
			issuerDTO.setIndvMktCustServiceTollFreePhoneNumber(issuer.getIndvCustServiceTollFree() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIndvCustServiceTollFree());
			issuerDTO.setIndvMktCustServiceTTY(issuer.getIndvCustServiceTTY() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIndvCustServiceTTY());
			issuerDTO.setIndvMktCustWebsiteUrl(issuer.getIndvSiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIndvSiteUrl());
			issuerDTO.setIndvMktAppStatusDeptPhoneNumber(issuer.getAppStatusDeptPhoneNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAppStatusDeptPhoneNumber());
			issuerDTO.setIndvMktBrokerEmailId(issuer.getBrokerEmailId() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerEmailId());
			issuerDTO.setIndvMktBrokerName(issuer.getBrokerContactName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerContactName());
			issuerDTO.setIndvMktBrokerPhoneNumber(issuer.getBrokerPhoneNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerPhoneNumber());
			issuerDTO.setIndvMktBrokerPhoneNumberExt(issuer.getBrokerPhoneNumberExt() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerPhoneNumberExt());
			issuerDTO.setIndvMktBrokerReportingPortalPwd(issuer.getAgencyRepoPortalPassword() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAgencyRepoPortalPassword());
			issuerDTO.setIndvMktBrokerReportingPortalUname(issuer.getAgencyRepoPortalUserName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAgencyRepoPortalUserName());
			issuerDTO.setIndvMktAgencyRepoPortalURL(issuer.getAgencyRepoPortalUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAgencyRepoPortalUrl());
			
			issuerDTO.setIndvMktPcdPhoneNumber(issuer.getPcdPhoneNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getPcdPhoneNumber());
			issuerDTO.setIndvMktPcdPhoneNumberExt(issuer.getPcdPhoneNumberExt() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getPcdPhoneNumberExt());
			issuerDTO.setIndvMktPcdEmail(issuer.getPcdEmailId() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getPcdEmailId());
			issuerDTO.setIndvMktPcdFAX(issuer.getPcdFaxNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getPcdFaxNumber());

			if(issuer.getLocation() != null){
				issuerDTO.setIndvMktPcdStreetAddress1(issuer.getLocation().getAddress1() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLocation().getAddress1());
				issuerDTO.setIndvMktPcdStreetAddress2(issuer.getLocation().getAddress2() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLocation().getAddress2());
				issuerDTO.setIndvMktPcdZip(issuer.getLocation().getZip() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLocation().getZip());
				issuerDTO.setIndvMktPcdState(issuer.getLocation().getState() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLocation().getState());
			}else{
				issuerDTO.setIndvMktPcdStreetAddress1(PlanMgmtConstants.EMPTY_STRING);
				issuerDTO.setIndvMktPcdStreetAddress2(PlanMgmtConstants.EMPTY_STRING);
				issuerDTO.setIndvMktPcdZip(PlanMgmtConstants.EMPTY_STRING);
				issuerDTO.setIndvMktPcdState(PlanMgmtConstants.EMPTY_STRING);
			}
			
			issuerDTO.setIndvMktCommContactName(issuer.getCommissionsContactName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCommissionsContactName());
			issuerDTO.setIndvMktCommEmail(issuer.getCommissionsEmailId() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCommissionsEmailId());
			issuerDTO.setIndvMktCommPhoneNumber(issuer.getCommissionsPhoneNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCommissionsPhoneNumber());
			issuerDTO.setIndvMktCommPhoneNumberExt(issuer.getCommissionsPhoneNumberExt() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCommissionsPhoneNumberExt());
			issuerDTO.setIndvMktCommPortalUrl(issuer.getCommissionPortalUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCommissionPortalUrl());
			issuerDTO.setIndvMktCommPortalUname(issuer.getCommissionPortalUserName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCommissionPortalUserName());
			issuerDTO.setIndvMktCommPortalPwd(issuer.getCommissionPortalPassword() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCommissionPortalPassword());
			
			issuerDTO.setShopMktCustServicePhoneNumber(issuer.getShopCustServicePhone() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShopCustServicePhone());
			issuerDTO.setShopMktCustServicePhoneNumberExt(issuer.getShopCustServicePhoneExt() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShopCustServicePhoneExt());
			issuerDTO.setShopMktCustServiceTollFreePhoneNumber(issuer.getShopCustServiceTollFree() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShopCustServiceTollFree());
			issuerDTO.setShopMktCustServiceTTY(issuer.getShopCustServiceTTY() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShopCustServiceTTY());
			issuerDTO.setShopMktCustWebsiteUrl(issuer.getShopSiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShopSiteUrl());
			//issuerDTO.setLastUpdatedBy(issuer.getLastUpdatedBy() == null ? PlanMgmtConstants.ZERO : issuer.getLastUpdatedBy());
			//issuerDTO.setCertificationDoc(issuer.getCertificationDoc() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCertificationDoc());
			//issuerDTO.setTxn820Version(issuer.getTxn820Version() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getTxn820Version());
			//issuerDTO.setTxn834Version(issuer.getTxn834Version() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getTxn834Version());
			//issuerDTO.setMarketingName(issuer.getMarketingName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getMarketingName());
			//issuerDTO.setNationalProducerNumber(issuer.getNationalProducerNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getNationalProducerNumber());
			issuerDTO.setAgentFirstName(issuer.getAgentFirstName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAgentFirstName());
			issuerDTO.setAgentLastName(issuer.getAgentLastName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAgentLastName());
			issuerDTO.setAgentPhoneNumber(issuer.getBrokerPhone() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerPhone());
			issuerDTO.setAgentEmail(issuer.getBrokerEmail() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerEmail());
			issuerDTO.setAgentFaxNumber(issuer.getBrokerFax() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerFax());
			
			issuerDTO.setIsLoginInRequired(issuer.getLoginRequired() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLoginRequired());
			issuerDTO.setOnExchangeDisclaimer(issuer.getOnExchangeDisclaimer() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getOnExchangeDisclaimer());
			issuerDTO.setOffExchangeDisclaimer(issuer.getOffExchangeDisclaimer() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getOffExchangeDisclaimer());
			issuerDTO.setBrokerId(issuer.getBrokerId() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerId());
			issuerDTO.setWritingAgent(issuer.getWritingAgent() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getWritingAgent());
			issuerDTO.setCustomerWebsiteUrl(issuer.getCompanySiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanySiteUrl());
			issuerDTO.setWebsiteUrl(issuer.getSiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getSiteUrl());
			
			//issuerDTO.setBrokerPhone(issuer.getBrokerPhone() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerPhone());
			//issuerDTO.setBrokerFax(issuer.getBrokerFax() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerFax());
			//issuerDTO.setBrokerEmail(issuer.getBrokerEmail() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerEmail());
			//issuerDTO.setProducerUrl(issuer.getProducerUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getProducerUrl());
			//issuerDTO.setD2C(issuer.getD2C() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getD2C());
			//issuerDTO.setOrgChart(issuer.getOrgChart());
			//issuerDTO.setCertifiedOn(issuer.getCertifiedOn());
			//issuerDTO.setDecertifiedOn(issuer.getDecertifiedOn());
		}
		
		return  issuerDTO;
	}
	
	
}
