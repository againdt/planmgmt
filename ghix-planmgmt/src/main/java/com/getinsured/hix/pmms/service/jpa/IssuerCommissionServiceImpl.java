/**
 * IssuerCommissionServiceImpl.java
 * @author santanu
 * @version 1.0
 * @since Jan 2, 2017 
 * It's an implementation file. Issuer Commission related interfaces are described here. 
 */

package com.getinsured.hix.pmms.service.jpa;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerCommissionDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerCommissionRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerCommissionResponseDTO;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerCommission;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.repository.IIssuerCommissionRepository;
import com.getinsured.hix.pmms.service.IssuerCommissionService;
import com.getinsured.hix.pmms.service.validation.IssuerCommissionValidatior;

@Service("IssuerCommissionService")
public class IssuerCommissionServiceImpl implements IssuerCommissionService {

	@Autowired
	private IssuerService issuerService;
	@Autowired
	private IIssuerCommissionRepository iIssuerCommissionRepository;
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	@Autowired
	private IssuerCommissionValidatior issuerCommissionValidatior;

	private static final String DATE_FORMAT = "MM/dd/yyyy";
	private static final Logger LOGGER = LoggerFactory
			.getLogger(IssuerCommissionServiceImpl.class);
	
	

	/* ##### implementation of update issuer commission in bulk ##### */
	@Override
	public GhixResponseDTO updateIssuerCommissionInBulk(
			IssuerCommissionRequestDTO issuerCommissionRequestDTO) {

		GhixResponseDTO issuerCommissionResponseDTO = issuerCommissionValidatior.validateUpldateIssuerCommissionInBulk(issuerCommissionRequestDTO);
		
		if(!GhixResponseDTO.STATUS.FAILED.name().equals(issuerCommissionResponseDTO.getStatus()))
		{
			
			Issuer issuerObj = null;
			IssuerCommission issuerCommissionObj = null;
			String insuranceType = issuerCommissionRequestDTO
					.getInsuranceType();
			String scheduleStartDate = issuerCommissionRequestDTO
					.getStartDate();
			String scheduleEndDate = issuerCommissionRequestDTO.getEndDate();
			String firstYearComm = issuerCommissionRequestDTO
					.getFirstYearCommissionAmount();
			String firstYearCommFormat = issuerCommissionRequestDTO
					.getFirstYearCommissionFormat();
			String secondYearComm = issuerCommissionRequestDTO
					.getSecondYearCommissionAmout();
			String secondYearCommFormat = issuerCommissionRequestDTO
					.getSecondYearCommissionFormat();

			try {

				for (Integer issuerId : issuerCommissionRequestDTO
						.getIssuerIdList()) {
					issuerObj = issuerService.getIssuerbyId(issuerId);

					issuerCommissionObj = new IssuerCommission();

					// Delete records where existing start dates are greater
					// than new start date
					int ctr = deleteFutureRecords(issuerObj.getId(),
							insuranceType, scheduleStartDate);
					int ctr2 = resetScheduleDate(issuerObj.getId(),
							insuranceType, scheduleStartDate);

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug(ctr + " records deleted. ======= " + ctr2
								+ " records update.");
					}

					// Insert new set of data in DB
					if (firstYearCommFormat
							.equalsIgnoreCase(PlanMgmtConstants.DOLLAR_SIGN)) {
						issuerCommissionObj.setCommissionDollarFirstYear(Double
								.parseDouble(firstYearComm));
					} else if (firstYearCommFormat
							.equalsIgnoreCase(PlanMgmtConstants.PERCENTAGE_SIGN)) {
						issuerCommissionObj
								.setCommissionPercentageFirstYear(Double
										.parseDouble(firstYearComm));
					}

					if (secondYearCommFormat
							.equalsIgnoreCase(PlanMgmtConstants.DOLLAR_SIGN)) {
						issuerCommissionObj
								.setCommissionDollarSecondYear(Double
										.parseDouble(secondYearComm));
					} else if (secondYearCommFormat
							.equalsIgnoreCase(PlanMgmtConstants.PERCENTAGE_SIGN)) {
						issuerCommissionObj
								.setCommissionPercentageSecondYear(Double
										.parseDouble(secondYearComm));
					}

					issuerCommissionObj.setScheduleStartDate(DateUtil
							.StringToDate(scheduleStartDate, DATE_FORMAT));
					if (StringUtils.isNotBlank(scheduleEndDate)) {
						issuerCommissionObj.setScheduleEndDate(DateUtil
								.StringToDate(scheduleEndDate, DATE_FORMAT));
					}
					issuerCommissionObj.setFrequency(issuerCommissionRequestDTO
							.getFrequency());
					issuerCommissionObj.setIssuer(issuerObj);
					issuerCommissionObj.setInsuranceType(insuranceType);
					issuerCommissionObj
							.setCreatedBy(Integer
									.parseInt(issuerCommissionRequestDTO
											.getCreateBy()));
					issuerCommissionObj
							.setLastUpdatedBy(Integer
									.parseInt(issuerCommissionRequestDTO
											.getCreateBy()));

					iIssuerCommissionRepository.save(issuerCommissionObj); // save
																			// object

				}

				issuerCommissionResponseDTO
						.setStatus(GhixResponseDTO.STATUS.SUCCESS.name());
				issuerCommissionResponseDTO.endResponse();
				
			} catch (Exception ex) {
				giExceptionHandler.recordFatalException(Component.PLANMGMT,
						null, "Failed to execute updateCommissionInfoInBulk ", ex);
				issuerCommissionResponseDTO
						.setStatus(GhixResponseDTO.STATUS.FAILED.name());
				issuerCommissionResponseDTO
						.setErrorCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				issuerCommissionResponseDTO
						.setErrorMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}
			
		}
	
		return issuerCommissionResponseDTO;
	}

	
	
	// Reset schedule end date of existing records with -1 day interval if input
	// scheduleStartDate in between date range
	private int resetScheduleDate(Integer issuerId, String insuranceType,
			String scheduleStartDate) {
		int ctr = 0;
		try {
			IssuerCommission issuerCommissionObj = iIssuerCommissionRepository
					.getRecordsBetweenDateRange(issuerId, insuranceType,
							DateUtil.StringToDate(scheduleStartDate,
									DATE_FORMAT));

			if (null != issuerCommissionObj) {
				// reset schedule end date with schedule start date -1 day
				// interval
				Calendar calendar = TSCalendar.getInstance();
				calendar.setTime(DateUtil.StringToDate(scheduleStartDate,
						DATE_FORMAT));
				calendar.add(Calendar.DATE, -1);
				issuerCommissionObj.setScheduleEndDate(calendar.getTime());
				iIssuerCommissionRepository.save(issuerCommissionObj);
				ctr++;
			}
		} catch (Exception ex) {
			LOGGER.error("Error inside resetScheduleDate() ", ex);
		}

		return ctr;
	}
	

	// Override existing records if input scheduleStartDate is less than/equal
	// existing scheduleStartDate
	private int deleteFutureRecords(Integer issuerId, String insuranceType,
			String scheduleStartDate) {
		int ctr = 0;
		try {
			ctr = iIssuerCommissionRepository.deleteFutureRecords(issuerId,
					insuranceType,
					DateUtil.StringToDate(scheduleStartDate, DATE_FORMAT));
		} catch (Exception ex) {
			LOGGER.error("Error inside deleteFutureRecords() ", ex);
		}

		return ctr;
	}

	
	/* ##### implementation of get issuer commission by HIOS Issuer Id  ##### */
	@Override
	public IssuerCommissionResponseDTO getIssuerCommissionByHiosId(String hiosIssuerId){
		
		IssuerCommissionResponseDTO issuerCommissionResponseDTO = issuerCommissionValidatior.validateGetIssuerCommissionByHiosId(hiosIssuerId);

		if(StringUtils.isBlank(issuerCommissionResponseDTO.getErrorMsg()))
		{
			try
			{
				Issuer issuerObj = issuerService.getIssuerByHIOSId(hiosIssuerId);
				if(null != issuerObj){

					issuerCommissionResponseDTO.setHiosIssuerId(hiosIssuerId);
					issuerCommissionResponseDTO.setIssuerName(issuerObj.getName());
					issuerCommissionResponseDTO.setCompanyLogo(issuerObj.getLogoURL());

					 // fetch commission data from DB against issuer id
					 List<IssuerCommission> issuerCommissionObjList =  iIssuerCommissionRepository.getByIssuerId(issuerObj.getId());
					
					 if(!issuerCommissionObjList.isEmpty() && issuerCommissionObjList.size() > 0)
					 {
						 IssuerCommissionDTO issuerCommissionDTO = null;
						 for(IssuerCommission issuerCommissionObj : issuerCommissionObjList)
						 {
							 issuerCommissionDTO = new IssuerCommissionDTO();
							 issuerCommissionDTO.setLastUpdateDate(DateUtil.dateToString(issuerCommissionObj.getLastUpdateTimestamp(), DATE_FORMAT));
							 issuerCommissionDTO.setInsuranceType(issuerCommissionObj.getInsuranceType());
							 issuerCommissionDTO.setStartDate(DateUtil.dateToString(issuerCommissionObj .getScheduleStartDate(), DATE_FORMAT));
							 	
							 if(null != issuerCommissionObj.getScheduleEndDate()){
							 	 issuerCommissionDTO.setEndDate(DateUtil.dateToString(issuerCommissionObj.getScheduleEndDate(), DATE_FORMAT));
							}else{
								 issuerCommissionDTO.setEndDate(PlanMgmtConstants.EMPTY_STRING);
							}
							 	
							if(null != issuerCommissionObj.getCommissionDollarFirstYear()){
								issuerCommissionDTO.setFirstYearCommissionAmount(issuerCommissionObj.getCommissionDollarFirstYear() + PlanMgmtConstants.DOLLAR_SIGN);
							}else if(null != issuerCommissionObj.getCommissionPercentageFirstYear()){
								issuerCommissionDTO.setFirstYearCommissionAmount(issuerCommissionObj.getCommissionPercentageFirstYear() + PlanMgmtConstants.PERCENTAGE_SIGN);
							}
							 	
							if(null != issuerCommissionObj.getCommissionDollarSecondYear()){
								issuerCommissionDTO.setSecondYearCommissionAmout(issuerCommissionObj.getCommissionDollarSecondYear() + PlanMgmtConstants.DOLLAR_SIGN);
							}else if(null != issuerCommissionObj.getCommissionPercentageSecondYear()){
								issuerCommissionDTO.setSecondYearCommissionAmout(issuerCommissionObj.getCommissionPercentageSecondYear() + PlanMgmtConstants.PERCENTAGE_SIGN);
							}
							 	
							issuerCommissionDTO.setFrequency(issuerCommissionObj.getFrequency().toString());

							issuerCommissionResponseDTO.setIssuerCommissionDTO(issuerCommissionDTO);
							issuerCommissionResponseDTO.setStatus(GhixResponseDTO.STATUS.SUCCESS.name());
						 
					 }	 
				}
					 else
					 {
						 issuerCommissionResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
						 issuerCommissionResponseDTO.setErrorMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
					 }
					
				}
				
			}
			catch(Exception ex){
				giExceptionHandler.recordFatalException(Component.PLANMGMT,
						null, "Failed to execute getIssuerCommissionByHiosId ", ex);
				issuerCommissionResponseDTO
						.setStatus(GhixResponseDTO.STATUS.FAILED.name());
				issuerCommissionResponseDTO
						.setErrorCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				issuerCommissionResponseDTO
						.setErrorMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}
		}
		
		issuerCommissionResponseDTO.endResponse();
		 
		return issuerCommissionResponseDTO;
	}
	
	
	/* ##### implementation of issuer commission. request parameters are issuer id, insurance type, effective date  ##### */
	@Override
	public IssuerCommission getIssuerCommission(Integer issuerId, String insuranceType, String commEffectiveDate){
		IssuerCommission issuerCommissionObj = null;
		
		try{
				issuerCommissionObj = iIssuerCommissionRepository
				.getRecordsBetweenDateRange(issuerId, insuranceType, DateUtil.StringToDate(commEffectiveDate, DATE_FORMAT));
		}
		catch(Exception ex)
		{
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute getIssuerCommission ", ex);
			LOGGER.error("Failed to execute getIssuerCommission. Exception: ", ex);
		}
		
		return issuerCommissionObj;
	}
	
}
