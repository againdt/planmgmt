package com.getinsured.hix.pmms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Issuer;

@Repository("iIssuerRepository")
public interface IIssuerRepository extends JpaRepository<Issuer, Integer>, RevisionRepository<Issuer, Integer, Integer> {

	@Query("SELECT i FROM Issuer i WHERE i.id = :id")
	Issuer findById(@Param("id") Integer id);

	@Query("SELECT COUNT(issuer.id) FROM Issuer issuer WHERE issuer.hiosIssuerId in (:hiosIssuerIdList)")
	Integer getCountOfHiosIssuerID(@Param("hiosIssuerIdList") List<String> hiosIssuerIdList);

	@Query("SELECT COUNT(issuer.id) FROM Issuer issuer WHERE issuer.id in (:issuerIdList)")
	Integer getCountOfIssuerID(@Param("issuerIdList") List<Integer> issuerIdList);

	@Query("SELECT i FROM Issuer i WHERE i.hiosIssuerId = :id")
	Issuer findByHiosId(@Param("id") String id);

	@Modifying
    @Transactional
	@Query("UPDATE Issuer issuer SET issuer.issuerBrandName.id = :brandNameId, issuer.lastUpdatedBy = :userId WHERE issuer.hiosIssuerId IN (:hiosIssuerIdList)")
	Integer bulkUpdateIssuerBrandId(@Param("brandNameId") Integer brandNameId, @Param("userId") Integer userId, @Param("hiosIssuerIdList") List<String> hiosIssuerIdList);

	@Query("SELECT issuer.hiosIssuerId, issuer.shortName, issuer.name from Issuer as issuer WHERE issuer.hiosIssuerId NOT LIKE 'QT_%' AND issuer.hiosIssuerId NOT LIKE 'PF_%' AND issuer.hiosIssuerId NOT LIKE 'DRX_%' ORDER BY issuer.hiosIssuerId")
	List<Object[]> getIssuerDropDownList();

	@Query("SELECT issuer.hiosIssuerId, issuer.shortName, issuer.name from Issuer as issuer "
			+ "WHERE ((issuer.stateOfDomicile=:stateCode) OR (issuer.stateOfDomicile is NULL AND issuer.state =:stateCode) OR "
			+ "(issuer.stateOfDomicile is NULL and issuer.state is NULL )) AND "
			+ "issuer.hiosIssuerId NOT LIKE 'QT_%' AND issuer.hiosIssuerId NOT LIKE 'PF_%' AND issuer.hiosIssuerId NOT LIKE 'DRX_%' "
			+ "ORDER BY issuer.hiosIssuerId")
	List<Object[]> getIssuerDropDownListByState(@Param("stateCode") String stateCode);

	@Query("SELECT issuer.id, issuer.name, issuer.stateOfDomicile, issuer.hiosIssuerId FROM Issuer issuer WHERE issuer.id IN "
			+ "( select distinct p.issuer from Plan p where p.isDeleted='N' AND p.insuranceType = :insuranceType AND p.applicableYear = :applicableYear AND ('ALL' = :selectedState OR p.state = :selectedState) ) "
			+ " order by UPPER(issuer.name)")
	List<Object[]> getAllIssuerNames(@Param("insuranceType") String insuranceType, @Param("selectedState") String selectedState, @Param("applicableYear") int applicableYear);

	@Query("SELECT issuer.id, issuer.name, issuer.stateOfDomicile, issuer.hiosIssuerId from Issuer as issuer WHERE issuer.id IN (SELECT distinct plan.issuer from Plan plan where plan.isDeleted='N' AND plan.insuranceType in('MC_ADV','MC_SUP','MC_RX')) AND (issuer.hiosIssuerId LIKE 'QT_%' OR issuer.hiosIssuerId like 'PF_%') ORDER BY UPPER(issuer.name)")
	List<Object[]> getMedicareIssuerDropDownList();	
}
