package com.getinsured.hix.pmms.service;

import javax.persistence.EntityManager;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepSearchResponseDTO;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;

public interface IssuerRepresentativeService {
	IssuerRepSearchResponseDTO getIssuerReps(IssuerRepSearchRequestDTO issuerRepSearchRequest);
	IssuerRepSearchResponseDTO getIssuerReps(String issuerHiosId);
	IssuerRepSearchResponseDTO getIssuerRepDetails(String issuerHiosId, String issuerRepId);
	IssuerRepSearchResponseDTO getIssuerRepByEmail(IssuerRepDTO issuerRepDTO);
	GhixResponseDTO createIssuerRep(IssuerRepDTO issuerRepDTO);
	GhixResponseDTO updateIssuerRep(String id, IssuerRepDTO issuerRepDTO);
	String isEmailAssignedToIssuerRep(String userEmailId);
	IssuerRepresentative saveIssuerRepInformation(IssuerRepDTO issuerRepDTO, IssuerRepresentative representativeObj, Issuer issuer, EntityManager em, boolean isNew) throws Exception;
	IssuerRepresentative getRepresentativeByEmail(String userEmailId);
	GhixResponseDTO assignIssuerRepMapping(IssuerRepDTO issuerRepDTO);
	GhixResponseDTO removeIssuerRepMapping(String issuerHiosId, Integer issuerRepId);
}
