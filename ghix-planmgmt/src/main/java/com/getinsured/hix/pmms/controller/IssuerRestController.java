package com.getinsured.hix.pmms.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Produces;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.microservice.DropDownListResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerEnrollmentFlowRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerEnrollmentResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerHistoryRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerHistoryResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerLightDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerSearchResponseDTO;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.service.IssuerService;

/**
 * Controller class is used to implement Microservice REST APIs for Issuer. 
 * @since Jan 08, 2017
 */
@Controller("pmmsIssuerRestController")
@RequestMapping(value = "/issuers")
public class IssuerRestController {
	private static final Logger LOGGER = Logger.getLogger(IssuerRestController.class);
	@Autowired
	private IssuerService pmmsIssuerService;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler; 
	
	public IssuerRestController() {
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	// for testing purpose
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to GHIX-PlanmgmtMS module, invoke IssuerController()");
		return "Welcome to GHIX-PlanmgmtMS module, invoke IssuerController";
	}
	
	@RequestMapping(value = "/history", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody IssuerHistoryResponseDTO getIssuerHistory(@RequestBody IssuerHistoryRequestDTO issuerRequest) {
			return pmmsIssuerService.getIssuerHistory(issuerRequest);
	}

	@RequestMapping(value = "/certificationHistory", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody IssuerHistoryResponseDTO getIssuerCertificationHistory(@RequestBody IssuerHistoryRequestDTO issuerRequest) {
		return pmmsIssuerService.getIssuerCertificationHistory(issuerRequest);
	}
			
/*	@RequestMapping(value = "/{hiosId}/payment", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody IssuerPaymentResponseDTO getIssuerPaymentInfo(@PathVariable("hiosId") String hiosId) {
		return pmmsIssuerService.getIssuerPaymentInfo(hiosId);
	}
*/
	@RequestMapping(value = "/{hiosId}/enrollmentInfo", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody IssuerEnrollmentResponseDTO getIssuerEnrollmentInfo(@PathVariable("hiosId") String hiosId) {
		return pmmsIssuerService.getIssuerEnrollmentInfo(hiosId);
	}

	/**
	 * Method is used to execute updateIssuerEnrollmentFlow API.
	 */
	@RequestMapping(value = "/updateIssuerEnrollmentFlow", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO updateIssuerEnrollmentFlow(@RequestBody IssuerEnrollmentFlowRequestDTO issuerEnrollmentRequest) {

		LOGGER.info("Begin execution of updateIssuerEnrollmentFlow API.");
		try {
			return pmmsIssuerService.updateIssuerEnrollmentFlowData(issuerEnrollmentRequest);
		}
		finally {
			LOGGER.info("End execution of updateIssuerEnrollmentFlow API.");
		}
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO createIssuer(@RequestBody IssuerDTO issuerDTO) {
		return pmmsIssuerService.createIssuer(issuerDTO);
	}
	
	
	@RequestMapping(value = "/{issuerId}", method = RequestMethod.PUT)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO updateIssuer(@PathVariable("issuerId") String issuerId, @RequestBody IssuerDTO issuerDTO) {
		return pmmsIssuerService.updateIssuer(issuerId, issuerDTO);
	}
	
	
	/* ##### Controller to get issuer info by HIOS Id ##### */
	@RequestMapping(value = "/hiosId/{hiosId}", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody IssuerResponseDTO getIssuerByHiosId(@PathVariable("hiosId") String hiosId) {
		return pmmsIssuerService.getIssuerByHiosId(hiosId);
	}

	
	/* ##### Controller to get list of issuers as per search criteria ##### */
	@RequestMapping(value = "/", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody IssuerSearchResponseDTO getIssuerList(@RequestBody IssuerSearchRequestDTO  issuerSearchRequestDTO) {
		return pmmsIssuerService.getIssuerList(issuerSearchRequestDTO);
	}

	@RequestMapping(value = "/getIssuerDropDownList", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody DropDownListResponseDTO getIssuerDropDownList() {
		return pmmsIssuerService.getIssuerDropDownList(null);
	}

	@RequestMapping(value = "/getIssuerDropDownListByState/{stateCode}", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody DropDownListResponseDTO getIssuerDropDownListByState(@PathVariable("stateCode") String stateCode) {
		return pmmsIssuerService.getIssuerDropDownList(stateCode);
	}
	
	 @RequestMapping(value = "/getIssuerListForDropDown", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public IssuerSearchResponseDTO getIssuerListForDropDown(@RequestBody IssuerSearchRequestDTO request) {
		IssuerSearchResponseDTO responseDTO = new IssuerSearchResponseDTO();
		List<IssuerLightDTO> issuerNames = new ArrayList<IssuerLightDTO>();
		responseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				
		try {
			  issuerNames = pmmsIssuerService.getIssuerListForDropDown(request);	
		} 
		catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "IssuerRestController - Failed to execute getIssuerListForDropDown.", ex);
			LOGGER.error("IssuerRestController - Failed to execute getIssuerListForDropDown:", ex);
			responseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_DROP_DOWN_LIST.getCode());
			responseDTO.setErrorMsg(Arrays.toString(ex.getStackTrace()));
		} 
		finally{
			responseDTO.setIssuerLightDTOList(issuerNames);
		}
       return responseDTO;
	}

	@RequestMapping(value = "/getMedicareIssuerListForDropDown", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody
	public IssuerSearchResponseDTO getMedicareIssuerListForDropDown() {

		IssuerSearchResponseDTO responseDTO = new IssuerSearchResponseDTO();
		List<IssuerLightDTO> medicareIssuerList = new ArrayList<IssuerLightDTO>();

		try {
			medicareIssuerList = pmmsIssuerService.getMedicareIssuerListForDropDown();

			if (!CollectionUtils.isEmpty(medicareIssuerList)) {
				responseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
			else {
				responseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				responseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_DROP_DOWN_LIST.getCode());
				responseDTO.setErrorMsg("Medicare Issuer does not exist in database.");
			}
		}
		catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null,
					"IssuerRestController - Failed to execute getMedicareIssuerListForDropDown.", ex);
			LOGGER.error("IssuerRestController - Failed to execute getMedicareIssuerListForDropDown:", ex);
			responseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			responseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_DROP_DOWN_LIST.getCode());
			responseDTO.setErrorMsg(Arrays.toString(ex.getStackTrace()));
		}
		finally {
			responseDTO.setIssuerLightDTOList(medicareIssuerList);
		}
		return responseDTO;
	}

	 @RequestMapping(value = "/getIssuerListForIssuerRep/{issuerRepId}", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public IssuerSearchResponseDTO getIssuerListForIssuerRep(@PathVariable("issuerRepId") String issuerRepId) {
		IssuerSearchResponseDTO responseDTO = new IssuerSearchResponseDTO();
		List<IssuerLightDTO> issuerNames = null;
				
		try {
			  issuerNames = pmmsIssuerService.getIssuerListForIssuerRep(issuerRepId);	
		} 
		catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "IssuerRestController - Failed to execute getIssuerListForIssuerRep.", ex);
			LOGGER.error("IssuerRestController - Failed to execute getIssuerListForIssuerRep:", ex);
			responseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_DROP_DOWN_LIST.getCode());
			responseDTO.setErrorMsg(Arrays.toString(ex.getStackTrace()));
			responseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
		} 
		finally{
			if(null != issuerNames) {
				responseDTO.setIssuerLightDTOList(issuerNames);
				responseDTO.setTotalNumOfRecords(issuerNames.size());
				responseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
			} else {
				responseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				responseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				responseDTO.setErrorMsg("Invalid Parameter to get Issuer List for Issuer RepId");
			}
		}
      return responseDTO;
	}
	 
	 @RequestMapping(value = "/getIssuerListForUser/{issuerRepUserId}", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public IssuerSearchResponseDTO getIssuerListForUser(@PathVariable("issuerRepUserId") String issuerRepUserId) {
		IssuerSearchResponseDTO responseDTO = new IssuerSearchResponseDTO();
		List<IssuerLightDTO> issuerNames = null;
				
		try {
			  issuerNames = pmmsIssuerService.getIssuerListForUser(issuerRepUserId);	
		} 
		catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "IssuerRestController - Failed to execute getIssuerListForUser.", ex);
			LOGGER.error("IssuerRestController - Failed to execute getIssuerListForUser:", ex);
			responseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUERS_LIST.getCode());
			responseDTO.setErrorMsg(Arrays.toString(ex.getStackTrace()));
			responseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
		} 
		finally{
			if(null != issuerNames) {
				responseDTO.setIssuerLightDTOList(issuerNames);
				responseDTO.setTotalNumOfRecords(issuerNames.size());
				responseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
			} else {
				responseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				responseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				responseDTO.setErrorMsg("Invalid Parameter to get Issuer List for User");
			}
		}
      return responseDTO;
	} 
	 
	@RequestMapping(value = "/getIssuerForIssuerRep/{issuerRepId}", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody IssuerResponseDTO getIssuerByIssuerRepId(@PathVariable("issuerRepId") String issuerRepId) {
		return pmmsIssuerService.getIssuerByIssuerRepId(issuerRepId);
	}

}
