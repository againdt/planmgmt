package com.getinsured.hix.pmms.service;

import java.util.List;

import com.getinsured.hix.dto.planmgmt.microservice.DropDownListResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerEnrollmentFlowRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerEnrollmentResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerHistoryRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerHistoryResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerLightDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerSearchResponseDTO;

/**
 * Interface is implemented in IssuerServiceImpl class.
 * @since Jan 10, 2017
 */
public interface IssuerService {
	IssuerHistoryResponseDTO getIssuerHistory(IssuerHistoryRequestDTO issuerHistoryRequestDTO);
	
	IssuerHistoryResponseDTO getIssuerCertificationHistory(IssuerHistoryRequestDTO issuerHistoryRequestDTO);
	
	IssuerEnrollmentResponseDTO getIssuerEnrollmentInfo(String hiosId);
	
	GhixResponseDTO updateIssuerEnrollmentFlowData(IssuerEnrollmentFlowRequestDTO issuerEnrollmentFlowRequest);
	
	GhixResponseDTO createIssuer(IssuerDTO issuerDTO);
	
	/* ##### interface of get issuer data by HIOS Id  ##### */
	IssuerResponseDTO getIssuerByHiosId(String hiosId); 
	
	/* ##### interface of get issuer list as per search criteria  ##### */
	IssuerSearchResponseDTO getIssuerList(IssuerSearchRequestDTO issuerSearchRequestDTO);

	GhixResponseDTO updateIssuer(String issuerId, IssuerDTO issuerDTO);

	DropDownListResponseDTO getIssuerDropDownList(String stateCode);
	
	List<IssuerLightDTO> getIssuerListForDropDown(IssuerSearchRequestDTO issuerSearchRequestDTO);

	List<IssuerLightDTO> getMedicareIssuerListForDropDown();

	List<IssuerLightDTO> getIssuerListForIssuerRep(String issuerRepId);
	
	IssuerResponseDTO getIssuerByIssuerRepId(String issuerRepId); 
	
	List<IssuerLightDTO> getIssuerListForUser(String issuerRepUserId);

	IssuerSearchResponseDTO getMedicareIssuerList(IssuerSearchRequestDTO issuerSearchRequestDTO);
}
