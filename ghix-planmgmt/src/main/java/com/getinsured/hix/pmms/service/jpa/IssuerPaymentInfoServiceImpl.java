package com.getinsured.hix.pmms.service.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerPaymentInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerPaymentResponseDTO;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerPaymentInformation;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes.ErrorCode;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.repository.IIssuerPaymentInfoRepo;
import com.getinsured.hix.pmms.service.IssuerPaymentInfoService;
import com.getinsured.hix.pmms.service.validation.IssuerAPIRequestValidator;
import com.getinsured.hix.pmms.service.validation.PlanMgmtErrorVO;
import com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil;

/**
 * Class is used to provide service to IssuerPaymentInfo Service APIs [Plan Management-Microservice]
 * @since Jan 10, 2017
 */
@Service("pmmsIssuerPaymentInfoService")
public class IssuerPaymentInfoServiceImpl  implements IssuerPaymentInfoService {

	private static final Logger LOGGER = Logger.getLogger(IssuerPaymentInfoServiceImpl.class);

	@Autowired private com.getinsured.hix.planmgmt.service.IssuerService issuerService;
	@Autowired private IIssuerPaymentInfoRepo issuerPaymentInfoRepo;
	@Autowired private GIExceptionHandler giExceptionHandler;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private IssuerAPIRequestValidator issuerAPIRequestValidator;

	@PersistenceUnit private EntityManagerFactory emf;

	@Override
	public GhixResponseDTO createIssuerPayment(IssuerPaymentInfoDTO issuerPaymentInfoDTO) {
		GhixResponseDTO ghixResponseDTO = new GhixResponseDTO();
		if(issuerAPIRequestValidator.validateIssuerPaymentInfo(issuerPaymentInfoDTO, ghixResponseDTO)) {
			List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
			String hiosId = issuerPaymentInfoDTO.getHiosId();
			Issuer issuerObj = issuerService.getIssuerByHIOSId(hiosId);
			if(null != issuerObj) {
				try {
					IssuerPaymentInformation issuerPaymentInformation = issuerPaymentInfoRepo.findByIssuerId(issuerObj.getId());
                    if(issuerPaymentInformation != null){
        				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Cannot create new record as record already exist for HIOS ID: " + hiosId);
                    } else {
                    	issuerPaymentInformation = new IssuerPaymentInformation();
                    	if(StringUtils.isNumeric(issuerPaymentInfoDTO.getCreateBy())) {
                    		issuerPaymentInformation.setCreatedBy(Integer.valueOf(issuerPaymentInfoDTO.getCreateBy()));
                    	} else {
                    		issuerPaymentInformation.setCreatedBy(Integer.valueOf(issuerPaymentInfoDTO.getLastUpdatedBy()));
                    	}

						issuerPaymentInformation = saveIssuerPaymentInformation(issuerPaymentInfoDTO, issuerPaymentInformation, issuerObj);
						ghixResponseDTO.setId(issuerPaymentInformation.getId());
                    }
				} catch(Exception ex) {
					LOGGER.info("Exception occurred in saving Issuer payment info: " + ex.getMessage());
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred in saving Issuer payment info.");
					giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.ISSUER_CERTIFICATION_STATUS_EXCEPTION.code), "Exception occurred in updateIssuerPayment service: ",ex);
				}
			}  else {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("saveIssuerPayment: Failed to get Issuer for HIOS ID: " + hiosId);
				}
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Issuer HIOS_ID", hiosId, "Failed to get Issuer record.");
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ghixResponseDTO);
		}
		return 	ghixResponseDTO;
	}
	
	@Override
	public GhixResponseDTO updateIssuerPayment(Integer id, IssuerPaymentInfoDTO issuerPaymentInfoDTO) {
		GhixResponseDTO ghixResponseDTO = new GhixResponseDTO();
		if(issuerAPIRequestValidator.validateIssuerPaymentInfo(issuerPaymentInfoDTO, ghixResponseDTO)) {
			List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
			try {
				IssuerPaymentInformation issuerPaymentInformation = issuerPaymentInfoRepo.findById(id);
                if(issuerPaymentInformation == null){
    				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Issuer Payment Record does not exist for ID: " + id);
                } else if(!issuerPaymentInfoDTO.getHiosId().equals(issuerPaymentInformation.getIssuer().getHiosIssuerId())) {
    				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Issuer Payment Record does not belong to provided HIOS ID: " + issuerPaymentInfoDTO.getHiosId());
                } else {
                	saveIssuerPaymentInformation(issuerPaymentInfoDTO, issuerPaymentInformation, issuerPaymentInformation.getIssuer());
                	ghixResponseDTO.setId(id);
                }
			} catch(Exception ex) {
				LOGGER.info("Exception occurred in saving Issuer payment info: " + ex.getMessage());
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred in saving Issuer payment info.");
				giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.ISSUER_CERTIFICATION_STATUS_EXCEPTION.code), "Exception occurred in updateIssuerPayment service: ",ex);
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ghixResponseDTO);
		}
		return 	ghixResponseDTO;
	}
	
	private IssuerPaymentInformation saveIssuerPaymentInformation(IssuerPaymentInfoDTO issuerPaymentInfoDTO, IssuerPaymentInformation issuerPaymentInformation, Issuer issuerObj) {
        String encryptedPassword = "";
		String encryptedSecurituKey = "";
		if(StringUtils.isNotBlank(issuerPaymentInfoDTO.getPassword())){
			encryptedPassword = ghixJasyptEncrytorUtil.encryptStringByJasypt(issuerPaymentInfoDTO.getPassword());
		}
	
		if(StringUtils.isNotBlank(issuerPaymentInfoDTO.getPasswordSecuredKey())){
			encryptedSecurituKey = ghixJasyptEncrytorUtil.encryptStringByJasypt(issuerPaymentInfoDTO.getPasswordSecuredKey());
		}
		issuerPaymentInformation.setIssuer(issuerObj);
		issuerPaymentInformation.setPassword(encryptedPassword);
		issuerPaymentInformation.setIssuerAuthURL(issuerPaymentInfoDTO.getIssuerAuthUrl());
		issuerPaymentInformation.setKeyStoreFileLocation(issuerPaymentInfoDTO.getKeystoreFileLocation());
		issuerPaymentInformation.setPasswordSecuredKey(encryptedSecurituKey);
		issuerPaymentInformation.setPrivateKeyName(issuerPaymentInfoDTO.getPrivateKeyName());
		issuerPaymentInformation.setSecurityAddress(issuerPaymentInfoDTO.getSecurityAddress());
		issuerPaymentInformation.setSecurityDnsName(issuerPaymentInfoDTO.getSecurityDnsName());
		issuerPaymentInformation.setSecurityKeyInfo(issuerPaymentInfoDTO.getSecurityKeyInfo());
		issuerPaymentInformation.setSecurityCertName(issuerPaymentInfoDTO.getSecurityCertName());
		issuerPaymentInformation.setLastUpdatedBy(Integer.valueOf(issuerPaymentInfoDTO.getLastUpdatedBy()));
		return issuerPaymentInfoRepo.save(issuerPaymentInformation);
	}


	@Override
	public IssuerPaymentResponseDTO getIssuerPaymentInfo(String hiosId) {
		IssuerPaymentResponseDTO issuerPaymentResponseDTO = new IssuerPaymentResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		if(issuerAPIRequestValidator.validateIssuerHiosId(hiosId, errorVOList, issuerPaymentResponseDTO)) {
			Issuer issuer = issuerService.getIssuerByHIOSId(hiosId);
			if(null != issuer) {
				try {
					IssuerPaymentInformation issuerPaymentInformation = issuerPaymentInfoRepo.findByIssuerId(issuer.getId());
					if(null != issuerPaymentInformation) {
						IssuerPaymentInfoDTO issuerPaymentInfoDTO = new IssuerPaymentInfoDTO();
						issuerPaymentResponseDTO.setIssuerName(issuer.getName());
						issuerPaymentResponseDTO.setCompanyLogo(issuer.getLogoURL());
						issuerPaymentInfoDTO.setId(issuerPaymentInformation.getId());
						issuerPaymentInfoDTO.setHiosId(hiosId);
						issuerPaymentInfoDTO.setIssuerAuthUrl(issuerPaymentInformation.getIssuerAuthURL());
						issuerPaymentInfoDTO.setKeystoreFileLocation(issuerPaymentInformation.getKeyStoreFileLocation());
						issuerPaymentInfoDTO.setPassword(ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerPaymentInformation.getPassword()));
						issuerPaymentInfoDTO.setPasswordSecuredKey(ghixJasyptEncrytorUtil.decryptStringByJasypt(issuerPaymentInformation.getPasswordSecuredKey()));
						issuerPaymentInfoDTO.setPrivateKeyName(issuerPaymentInformation.getPrivateKeyName());
						issuerPaymentInfoDTO.setSecurityAddress(issuerPaymentInformation.getSecurityAddress());
						issuerPaymentInfoDTO.setSecurityDnsName(issuerPaymentInformation.getSecurityDnsName());
						issuerPaymentInfoDTO.setSecurityKeyInfo(issuerPaymentInformation.getSecurityKeyInfo());
						issuerPaymentInfoDTO.setSecurityCertName(issuerPaymentInformation.getSecurityCertName());
						issuerPaymentResponseDTO.setIssuerPaymentInfoDTO(issuerPaymentInfoDTO);
					} else {
						LOGGER.info("getIssuerPaymentInfo: Payment information not found.");
					}
				} catch(Exception ex) {
					LOGGER.error("Exception occurred while getIssuerPaymentInfo: ", ex);
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting Payment Info for Issuer having HIOS ID: " + hiosId);
					giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.code), "Exception occurred in getIssuerPaymentInfo service: ",ex);
				}
			} else {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("getIssuerPaymentInfo: Failed to get Issuer for HIOS ID: " + hiosId);
				}
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Issuer HIOS_ID", hiosId, "Failed to get Issuer record.");
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), issuerPaymentResponseDTO);
		}
		return issuerPaymentResponseDTO;
	}

}
