package com.getinsured.hix.pmms.service;

import java.util.List;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameResponseDTO;
import com.getinsured.hix.model.IssuerBrandName;

/**
 * Interface is implemented in IssuerBrandNameServiceImpl class.
 * @since Jan 17, 2017
 */
public interface IssuerBrandNameService {

	IssuerBrandNameResponseDTO getIssuerBrandNameList();
	GhixResponseDTO addIssuerBrandNameInDB(IssuerBrandNameDTO issuerBrandNameDTO);
	IssuerBrandName getIssuerBrandNameById(Integer issuerBrandNameId);
	GhixResponseDTO bulkUpdateIssuerBrandId(IssuerBrandNameRequestDTO brandNameRequestDTO);
	List<IssuerBrandName> getIssuerBrandNameByIsDeleted();
	boolean isDuplicateExist(IssuerBrandNameDTO brandNameDTO);
}
