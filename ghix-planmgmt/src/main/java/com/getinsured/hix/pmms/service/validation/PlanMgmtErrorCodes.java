package com.getinsured.hix.pmms.service.validation;

@Deprecated
public final class PlanMgmtErrorCodes {

	public static final int ECODE_DEFAULT = 1001;
	public static final String EMSG_DEFAULT = "Internal Server Error.";

	public static final int ECODE_UPDATE_ISSUER_ENROLLMENT_FLOW_VALIDATION = 1003;

	public static final String MODULE_NAME= "PM";
	public static final String FAILURE = "FAILURE";
	public static final String SUCCESS = "Success";
	// Enum to set the error code for Exception handling
	public enum ErrorCode {

		ISSUER_CERTIFICATION_STATUS_EXCEPTION(20124), 
		VIEW_ISSUER_HISTORY_EXCEPTION(20126), 
		MANAGE_REPRESENTATIVE_EXCEPTION(20155), 
		VIEW_ISSUER_ENROLLMENT_INFO_EXCEPTION(20336),
		UPDATE_ISSUER_ENROLLMENT_FLOW_EXCEPTION(20338)
		;

		public final int code;

		private ErrorCode(int cd) {
			code = cd;
		}

		public int getCode() {
			return code;
		}
	}
}
