package com.getinsured.hix.pmms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.getinsured.hix.model.IssuerBrandName;

/**
 * Interface is used to get data from PM_ISSUER_BRAND_NAME table.
 * @since Jan 17, 2017
 */

public interface IIssuerBrandNameRepository extends JpaRepository<IssuerBrandName, Integer> {

	@Query("SELECT issuerBrandName FROM IssuerBrandName issuerBrandName WHERE issuerBrandName.isDeleted = 'N' ORDER BY UPPER(issuerBrandName.brandName)")
	List<IssuerBrandName> getIssuerBrandNameList();

	@Query("SELECT COUNT(issuerBrandName) FROM IssuerBrandName issuerBrandName WHERE UPPER(issuerBrandName.brandName) = UPPER(:brandName) AND issuerBrandName.isDeleted = 'N'")
	Integer countIssuerBrandName(@Param("brandName") String brandName);

	@Query("SELECT COUNT(issuerBrandName) FROM IssuerBrandName issuerBrandName WHERE issuerBrandName.brandId = :brandId AND issuerBrandName.isDeleted = 'N'")
	Integer countIssuerBrandID(@Param("brandId") String brandId);

	@Query("SELECT issuerBrandName.id FROM IssuerBrandName issuerBrandName WHERE issuerBrandName.brandId = :brandId AND issuerBrandName.isDeleted = 'N'")
	Integer getIssuerBrandNameID(@Param("brandId") String brandId);

	IssuerBrandName findByIdAndIsDeleted(int id, String isDeleted);
	
	List<IssuerBrandName> findByIsDeletedOrderByIdAsc(@Param("isDeleted") String isDeleted);

}
