/**
 * IssuerCommissionController.java
 * @author santanu
 * @version 1.0
 * @since Jan 2, 2017 
 * This REST controller would be use to handle Issuer Commission related request and response
 */

package com.getinsured.hix.pmms.controller;

import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerCommissionRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerCommissionResponseDTO;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.pmms.service.IssuerCommissionService;

@Controller
@RequestMapping(value = PlanMgmtConstants.ISSUER_COMMISSION_ENDPOINT)
public class IssuerCommissionController {

	@Autowired
	private IssuerCommissionService issuerCommissionService;

	/* ##### Controller to update issuer commission info ##### */
	@RequestMapping(value = "/updateIssuerCommission", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public GhixResponseDTO updateIssuerCommission(
			@RequestBody IssuerCommissionRequestDTO issuerCommissionRequestDTO) {
		GhixResponseDTO issuerCommissionResponseDTO = issuerCommissionService
				.updateIssuerCommissionInBulk(issuerCommissionRequestDTO);

		return issuerCommissionResponseDTO;

	}
	
	
	/* ##### Controller to get issuer commission info by HIOS Id ##### */
	@RequestMapping(value = "/hiosId/{hiosIssuerId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody
	public IssuerCommissionResponseDTO getIssuerCommission(
			@PathVariable("hiosIssuerId") String hiosIssuerId) {
		IssuerCommissionResponseDTO issuerCommissionResponseDTO = issuerCommissionService
				.getIssuerCommissionByHiosId(hiosIssuerId);

		return issuerCommissionResponseDTO;

	}

}
