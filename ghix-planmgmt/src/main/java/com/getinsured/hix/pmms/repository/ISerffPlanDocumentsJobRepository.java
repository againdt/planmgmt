package com.getinsured.hix.pmms.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.PlanDocumentsJob;

/**
 * This class is repository for PlanDocumentsJob [SERFF_PLAN_DOCUMENTS_JOB] entity.
 * @since May 25, 2017
 */
public interface ISerffPlanDocumentsJobRepository extends JpaRepository<PlanDocumentsJob, Integer> {

}
