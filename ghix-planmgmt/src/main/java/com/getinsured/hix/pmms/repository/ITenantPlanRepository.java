package com.getinsured.hix.pmms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.TenantPlan;

@Repository("pmmsITenantPlanRepository")
public interface ITenantPlanRepository extends JpaRepository<TenantPlan, Integer> {

	@Query("SELECT tp FROM TenantPlan tp WHERE tp.tenant.id= :tenantId and tp.plan.id= :planid")
	TenantPlan getTenantPlanByTenantIdAndPlanId(@Param("tenantId") Long tenantId,@Param("planid") int planid);
	
	@Query("SELECT tp.plan.id,  COUNT(tp) FROM TenantPlan tp WHERE tp.plan.id in (:planIds) group by tp.plan.id")
	List<Object[]> getPlanCountByPlanIds(@Param("planIds") List<Integer> planIds);
	
	List<TenantPlan> findByPlan_Id(int planId);
	
}
