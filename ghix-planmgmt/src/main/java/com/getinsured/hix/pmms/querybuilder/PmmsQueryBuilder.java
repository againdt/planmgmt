package com.getinsured.hix.pmms.querybuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.pmms.service.IssuerServiceHelper.SortOrder;

public class PmmsQueryBuilder {

	private static final Logger LOGGER = LoggerFactory.getLogger(PmmsQueryBuilder.class);
	
	public static String buildIssuerSearchQuery(String DB_TYPE, String tenantCode, String d2cFlowTypeId, String sortBy, String sortOrder, Integer startRecord, Integer pageSize ){
		StringBuilder sqlQuery = new StringBuilder(2056);
		
		try{

			if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
				sqlQuery.append(" SELECT temp3.* FROM ( SELECT temp2.* FROM ( ");
				sqlQuery.append(" SELECT temp.id as idTemp, temp.id as id, temp.name, temp.HIOS_ISSUER_ID, to_char(temp.LAST_UPDATE_TIMESTAMP, 'YYYY-MM-DD HH:MI:SS.MS'), temp.CERTIFICATION_STATUS, ");
			}
			else {
				sqlQuery.append(" SELECT temp3.* FROM ( SELECT ROWNUM AS rn , temp2.* FROM ( ");
				sqlQuery.append(" SELECT temp.id as id, temp.name, temp.HIOS_ISSUER_ID, temp.LAST_UPDATE_TIMESTAMP, temp.CERTIFICATION_STATUS, ");
			}
			
			// if DB is POSTGRES then use string_agg() instead of listagg()
			if(DB_TYPE != null && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)){
				sqlQuery.append(" string_agg(temp.tenantName, ',' ORDER BY temp.tenantName) AS tenantName , ");
			}else{
				sqlQuery.append(" listagg(temp.tenantName, ', ') within GROUP (ORDER BY temp.tenantName) AS tenantName, ");
			}
			
			sqlQuery.append(" temp.BRAND_NAME_ID AS brandNameId,  temp.BRAND_NAME AS brandName, temp.D2C_FLOW_TYPE AS d2cFlowType FROM ( ");
			sqlQuery.append(" SELECT distinct t.name as tenantName, i.id, i.name, i.HIOS_ISSUER_ID, i.LAST_UPDATE_TIMESTAMP, i.CERTIFICATION_STATUS, i.BRAND_NAME_ID, pib.BRAND_NAME, id2c.D2C_FLOW_TYPE ");
			sqlQuery.append(" FROM issuers i ");
			sqlQuery.append(" LEFT JOIN ISSUER_D2C id2c 	ON i.HIOS_ISSUER_ID = id2c.HIOS_ISSUER_ID ");

			if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
				sqlQuery.append(" AND CAST(id2c.APPLICABLE_YEAR as TEXT)  =:d2cFlowYear ");
			}
			else {
				sqlQuery.append(" AND id2c.APPLICABLE_YEAR  =:d2cFlowYear ");
			}
			sqlQuery.append(" LEFT JOIN plan p 				ON i.id = p.issuer_id AND p.IS_DELETED ='N' ");
			sqlQuery.append(" LEFT JOIN PM_TENANT_PLAN tp 	ON p.id = tp.plan_id ");
			sqlQuery.append(" LEFT JOIN PM_ISSUER_BRAND_NAME pib 	ON i.BRAND_NAME_ID = pib.id AND pib.IS_DELETED ='N' ");
			
			if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL) && !tenantCode.equalsIgnoreCase(PlanMgmtConstants.NONE)){
				sqlQuery.append(" INNER JOIN tenant t 			ON tp.TENANT_ID = t.id AND t.IS_ACTIVE = 'Y'");
				sqlQuery.append(" AND UPPER(t.code) = UPPER(:tenantCode) " );
			} else{
				sqlQuery.append(" LEFT JOIN tenant t 			ON tp.TENANT_ID = t.id AND t.IS_ACTIVE = 'Y'");
			}
			
			sqlQuery.append(" WHERE (i.HIOS_ISSUER_ID NOT LIKE 'QT%') " );
			sqlQuery.append(" AND (i.HIOS_ISSUER_ID NOT LIKE 'PF%') " );
			sqlQuery.append(" AND (i.HIOS_ISSUER_ID NOT LIKE 'DRX_%') " );
			
			sqlQuery.append(" AND ('ALL' = :");
			sqlQuery.append(PlanMgmtConstants.STATUS);
			sqlQuery.append(" OR UPPER(i.CERTIFICATION_STATUS) = UPPER(:");
			sqlQuery.append(PlanMgmtConstants.STATUS);
			sqlQuery.append("))");
			
			sqlQuery.append(" AND ('ALL' = :");
			sqlQuery.append(PlanMgmtConstants.HIOS_ISSUER_ID);
			sqlQuery.append(" OR UPPER(i.HIOS_ISSUER_ID) LIKE '%' || UPPER(:");
			sqlQuery.append(PlanMgmtConstants.HIOS_ISSUER_ID);
			sqlQuery.append(") || '%') " );
			
			sqlQuery.append(" AND ('ALL' = :");
			sqlQuery.append(PlanMgmtConstants.NAME);
			sqlQuery.append(" OR UPPER(i.NAME) LIKE '%' || UPPER(:");
			sqlQuery.append(PlanMgmtConstants.NAME);
			sqlQuery.append(") || '%') " );
			
			if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
				sqlQuery.append(" AND (CAST('0' as TEXT) = :");
				sqlQuery.append(PlanMgmtConstants.BRAND_NAME_ID);
				sqlQuery.append(" OR CAST(i.BRAND_NAME_ID as TEXT) = :");
				sqlQuery.append(PlanMgmtConstants.BRAND_NAME_ID);
			}
			else {
				sqlQuery.append(" AND ('0' = :");
				sqlQuery.append(PlanMgmtConstants.BRAND_NAME_ID);
				sqlQuery.append(" OR i.BRAND_NAME_ID = :");
				sqlQuery.append(PlanMgmtConstants.BRAND_NAME_ID);
			}
			
			if(PlanMgmtConstants.NO_D2C.equalsIgnoreCase(d2cFlowTypeId)) {
				sqlQuery.append(") AND (id2c.D2C_FLOW_TYPE is null");
				sqlQuery.append(" ) order by t.name) temp " );
			} else {
				sqlQuery.append(" ) AND ('ALL' = :");
				sqlQuery.append(PlanMgmtConstants.D2C_FLOW_TYPE_ID);
				sqlQuery.append(" OR UPPER(id2c.D2C_FLOW_TYPE) = UPPER(:");
				sqlQuery.append(PlanMgmtConstants.D2C_FLOW_TYPE_ID);
				sqlQuery.append(" )) order by t.name) temp " );
			}
			
			if(tenantCode.equalsIgnoreCase(PlanMgmtConstants.NONE)){
				sqlQuery.append("WHERE temp.tenantname is null");
			}
			sqlQuery.append(" group by temp.id, temp.name, temp.HIOS_ISSUER_ID, temp.LAST_UPDATE_TIMESTAMP, temp.CERTIFICATION_STATUS, temp.BRAND_NAME_ID, temp.BRAND_NAME, temp.D2C_FLOW_TYPE " );
			
			if ((sortBy != null) && !sortBy.toString().trim().isEmpty()) {
				if (sortBy.equalsIgnoreCase("name")) {
					sqlQuery.append(" order by UPPER(temp.name) " + SortOrder.valueOf(sortOrder));
			
				} else if (sortBy.equalsIgnoreCase("hiosIssuerId")) {
					sqlQuery.append(" order by temp.HIOS_ISSUER_ID " + SortOrder.valueOf(sortOrder));
				
				} else if (sortBy.equalsIgnoreCase("lastUpdateTimestamp")) {
					sqlQuery.append(" order by temp.LAST_UPDATE_TIMESTAMP " + SortOrder.valueOf(sortOrder));
			
				} else if (sortBy.equalsIgnoreCase("certificationStatus")) {
					sqlQuery.append(" order by UPPER(temp.CERTIFICATION_STATUS) " + SortOrder.valueOf(sortOrder));
			
				} else if (sortBy.equalsIgnoreCase("brandNameId")) {
					sqlQuery.append(" order by temp.BRAND_NAME_ID " + SortOrder.valueOf(sortOrder));
			
				} else if (sortBy.equalsIgnoreCase("d2cFlowTypeId")) {
					sqlQuery.append(" order by temp.D2C_FLOW_TYPE " + SortOrder.valueOf(sortOrder));
				} else {
					LOGGER.warn("Issuer search sorting ignored as sorting not supported for column ".intern() + sortBy);
				}
			
			} else {
				// default sort by created date
				sqlQuery.append(" order by temp.LAST_UPDATE_TIMESTAMP " + SortOrder.DESC);
			}
			
			if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
				sqlQuery.append(" ) temp2 ) temp3 ");
				sqlQuery.append(" offset " + startRecord);
				sqlQuery.append(" limit " + pageSize);
			}
			else {
				sqlQuery.append(" ) temp2 where rownum <= " + (startRecord + pageSize) + " ) temp3 ");
				sqlQuery.append(" WHERE rn > " + startRecord);
			}
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Build query " + sqlQuery.toString());
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occoured inside buildIssuerSearchQuery()", ex);
		}
		return sqlQuery.toString();
	}

	public static String buildIssuerSearchCountQuery(String DB_TYPE, String tenantCode, String d2cFlowTypeId) {

		StringBuilder queryForCount = new StringBuilder(2056);

		try {
			queryForCount.append(" SELECT count(*) FROM (SELECT i.id "); 
			queryForCount.append(" FROM issuers i LEFT JOIN ISSUER_D2C id2c ON i.HIOS_ISSUER_ID = id2c.HIOS_ISSUER_ID");

			if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
				queryForCount.append(" AND CAST(id2c.APPLICABLE_YEAR as TEXT) =:d2cFlowYear ");
			}
			else {
				queryForCount.append(" AND id2c.APPLICABLE_YEAR =:d2cFlowYear ");
			}
			queryForCount.append(" LEFT JOIN plan p ON i.id = p.issuer_id AND p.IS_DELETED ='N' ");
			queryForCount.append(" LEFT JOIN PM_TENANT_PLAN tp 	ON p.id = tp.plan_id ");
			queryForCount.append(" LEFT JOIN PM_ISSUER_BRAND_NAME pib ON pib.id = i.BRAND_NAME_ID AND pib.IS_DELETED ='N' ");

			if (!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL) && !tenantCode.equalsIgnoreCase(PlanMgmtConstants.NONE)) {
				queryForCount.append(" INNER JOIN tenant t ON tp.TENANT_ID = t.id AND t.IS_ACTIVE = 'Y'");
				queryForCount.append(" AND UPPER(t.code) = UPPER(:tenantCode) ");
			}
			else {
				queryForCount.append(" LEFT JOIN tenant t ON tp.TENANT_ID = t.id AND t.IS_ACTIVE = 'Y'");
			}
			queryForCount.append(" WHERE (i.HIOS_ISSUER_ID NOT LIKE 'QT_%') ");
			queryForCount.append(" AND (i.HIOS_ISSUER_ID NOT LIKE 'PF_%') ");
			queryForCount.append(" AND (i.HIOS_ISSUER_ID NOT LIKE 'DRX_%') ");

			queryForCount.append(" AND ('ALL' = :");
			queryForCount.append(PlanMgmtConstants.STATUS);
			queryForCount.append(" OR UPPER(i.CERTIFICATION_STATUS) = UPPER(:");
			queryForCount.append(PlanMgmtConstants.STATUS);
			queryForCount.append("))");

			queryForCount.append(" AND ('ALL' = :");
			queryForCount.append(PlanMgmtConstants.HIOS_ISSUER_ID);
			queryForCount.append(" OR UPPER(i.HIOS_ISSUER_ID) LIKE '%' || UPPER(:");
			queryForCount.append(PlanMgmtConstants.HIOS_ISSUER_ID);
			queryForCount.append(") || '%')");

			queryForCount.append(" AND ('ALL' = :");
			queryForCount.append(PlanMgmtConstants.NAME);
			queryForCount.append(" OR UPPER(i.NAME) LIKE '%' || UPPER(:");
			queryForCount.append(PlanMgmtConstants.NAME);
			queryForCount.append(") || '%')");

			if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
				queryForCount.append(" AND (CAST('0' as TEXT) = :");
				queryForCount.append(PlanMgmtConstants.BRAND_NAME_ID);
				queryForCount.append(" OR CAST(i.BRAND_NAME_ID as TEXT) = :");
				queryForCount.append(PlanMgmtConstants.BRAND_NAME_ID);
				queryForCount.append(")");
			}
			else {
				queryForCount.append(" AND ('0' = :");
				queryForCount.append(PlanMgmtConstants.BRAND_NAME_ID);
				queryForCount.append(" OR i.BRAND_NAME_ID = :");
				queryForCount.append(PlanMgmtConstants.BRAND_NAME_ID);
				queryForCount.append(")");
			}

			if (tenantCode.equalsIgnoreCase(PlanMgmtConstants.NONE)) {
				queryForCount.append(" AND t.name is null ");
			}

			if (PlanMgmtConstants.NO_D2C.equalsIgnoreCase(d2cFlowTypeId)) {
				queryForCount.append(" AND (id2c.D2C_FLOW_TYPE is null");
				queryForCount.append(")");
			}
			else {
				queryForCount.append(" AND ('ALL' = :");
				queryForCount.append(PlanMgmtConstants.D2C_FLOW_TYPE_ID);
				queryForCount.append(" OR UPPER(id2c.D2C_FLOW_TYPE) = UPPER(:");
				queryForCount.append(PlanMgmtConstants.D2C_FLOW_TYPE_ID);
				queryForCount.append("))");
			}
			queryForCount.append(" GROUP BY i.id, i.name, i.HIOS_ISSUER_ID, i.LAST_UPDATE_TIMESTAMP, i.CERTIFICATION_STATUS, i.BRAND_NAME_ID, pib.BRAND_NAME, id2c.D2C_FLOW_TYPE) t ");

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Build count query " + queryForCount.toString());
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occoured inside buildIssuerSearchQuery()", ex);
		}
		return queryForCount.toString();
	}
	
	
	public static String buildMedicareIssuerSearchQuery(String dbType, String sortBy, String sortOrder, Integer startRecord, Integer pageSize ){
		StringBuilder sqlQuery = new StringBuilder(2056);
		
		try {
			if (null != dbType && dbType.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
				sqlQuery.append("SELECT temp2.* FROM (SELECT temp.* FROM (SELECT i.ID as tempId, i.ID as id, i.STATE_OF_DOMICILE, i.NAME, i.HIOS_ISSUER_ID, i.LAST_UPDATE_TIMESTAMP from issuers i where ((i.HIOS_ISSUER_ID LIKE 'QT%') OR (i.HIOS_ISSUER_ID LIKE 'PF%'))");
			}
			else {
				sqlQuery.append("SELECT temp2.* FROM (SELECT ROWNUM AS rn , temp.* FROM (SELECT i.ID, i.STATE_OF_DOMICILE, i.NAME, i.HIOS_ISSUER_ID, i.LAST_UPDATE_TIMESTAMP from issuers i where ((i.HIOS_ISSUER_ID LIKE 'QT%') OR (i.HIOS_ISSUER_ID LIKE 'PF%'))");
			}
			sqlQuery.append(" AND ('ALL' = :");
			sqlQuery.append(PlanMgmtConstants.STATES);
			sqlQuery.append(" OR UPPER(i.STATE_OF_DOMICILE) LIKE UPPER(:");
			sqlQuery.append(PlanMgmtConstants.STATES);
			sqlQuery.append("))");

			sqlQuery.append(" AND ('ALL' = :");
			sqlQuery.append(PlanMgmtConstants.HIOS_ISSUER_ID);
			sqlQuery.append(" OR UPPER(i.HIOS_ISSUER_ID) LIKE UPPER(:");
			sqlQuery.append(PlanMgmtConstants.HIOS_ISSUER_ID);
			sqlQuery.append(") || '%') " );

			sqlQuery.append(" AND ('ALL' = :");
			sqlQuery.append(PlanMgmtConstants.NAME);
			sqlQuery.append(" OR UPPER(i.NAME) LIKE UPPER(:");
			sqlQuery.append(PlanMgmtConstants.NAME);
			sqlQuery.append(") || '%')");

			if ((sortBy != null) && !sortBy.toString().trim().isEmpty()) {
				if (sortBy.equalsIgnoreCase("state")) {
					sqlQuery.append(" order by i.STATE_OF_DOMICILE " + SortOrder.valueOf(sortOrder));

				} else if (sortBy.equalsIgnoreCase("name")) {
					sqlQuery.append(" order by i.NAME " + SortOrder.valueOf(sortOrder));

				} else if (sortBy.equalsIgnoreCase("hiosIssuerId")) {
					sqlQuery.append(" order by i.HIOS_ISSUER_ID " + SortOrder.valueOf(sortOrder));

				} else if (sortBy.equalsIgnoreCase("lastUpdateTimestamp")) {
					sqlQuery.append(" order by i.LAST_UPDATE_TIMESTAMP " + SortOrder.valueOf(sortOrder));
				} else {
					LOGGER.warn("Sorting ignored for Medicare issuer search as sorting not supported for column ".intern() + sortBy);
				}
				sqlQuery.append(") temp");
			} else {
				// default sort by last updated
				sqlQuery.append(" order by i.LAST_UPDATE_TIMESTAMP " + SortOrder.valueOf(sortOrder)+ ") temp");
			}

			if (null != dbType && dbType.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
				sqlQuery.append(" ) temp2 ");
				sqlQuery.append(" offset " + startRecord);
				sqlQuery.append(" limit " + pageSize);
			}
			else {
				sqlQuery.append(" where rownum <= " + (startRecord + pageSize) + " ) temp2 ");
				sqlQuery.append(" WHERE rn > " + startRecord);
			}

			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Build query " + sqlQuery.toString());
			}
			
		} catch (Exception ex) {
			LOGGER.error("Error occoured inside buildMedicareIssuerSearchQuery()", ex);
		}
		return sqlQuery.toString();
	}
	
	public static String buildMedicareIssuerSearchCountQuery(String dbType) {

		StringBuilder sqlQuery = new StringBuilder(2056);

		try {
			sqlQuery.append("SELECT count(*) FROM ( SELECT i.ID, i.STATE_OF_DOMICILE, i.NAME, i.HIOS_ISSUER_ID, i.LAST_UPDATE_TIMESTAMP from issuers i where ((i.HIOS_ISSUER_ID LIKE 'QT%') OR (i.HIOS_ISSUER_ID LIKE 'PF%'))");
			sqlQuery.append(" AND ('ALL' = :");
			sqlQuery.append(PlanMgmtConstants.STATES);
			sqlQuery.append(" OR UPPER(i.STATE_OF_DOMICILE) = UPPER(:");
			sqlQuery.append(PlanMgmtConstants.STATES);
			sqlQuery.append("))");

			sqlQuery.append(" AND ('ALL' = :");
			sqlQuery.append(PlanMgmtConstants.HIOS_ISSUER_ID);
			sqlQuery.append(" OR UPPER(i.HIOS_ISSUER_ID) LIKE UPPER(:");
			sqlQuery.append(PlanMgmtConstants.HIOS_ISSUER_ID);
			sqlQuery.append(") || '%') " );

			sqlQuery.append(" AND ('ALL' = :");
			sqlQuery.append(PlanMgmtConstants.NAME);
			sqlQuery.append(" OR UPPER(i.NAME) LIKE UPPER(:");
			sqlQuery.append(PlanMgmtConstants.NAME);
			sqlQuery.append(") || '%')");
			sqlQuery.append(") issuerData ");

			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Build count query " + sqlQuery.toString());
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occoured inside buildMedicareIssuerSearchCountQuery()", ex);
		}
		return sqlQuery.toString();
	}

}
