package com.getinsured.hix.pmms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.IssuerPaymentInformation;

public interface IIssuerPaymentInfoRepo extends JpaRepository<IssuerPaymentInformation , Integer> {
    IssuerPaymentInformation findByIssuerId(Integer issuerId);
	@Query("SELECT i FROM IssuerPaymentInformation i WHERE i.id = :id")
	IssuerPaymentInformation findById(@Param("id") Integer id);
}
