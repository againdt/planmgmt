package com.getinsured.hix.pmms.controller;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerPaymentInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerPaymentResponseDTO;
import com.getinsured.hix.pmms.service.IssuerPaymentInfoService;

/**
 * Controller class is used to implement Microservice REST APIs for IssuerPaymentInfo. 
 * @since Jan 08, 2017
 */
@Controller("pmmsIssuerPaymentInfoRestController")
@RequestMapping(value = "/issuerpayments")
public class IssuerPaymentInfoRestController {
	private static final Logger LOGGER = Logger.getLogger(IssuerPaymentInfoRestController.class);
	
	@Autowired
	private IssuerPaymentInfoService pmmsIssuerPaymentInfoService;
	public IssuerPaymentInfoRestController() {
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	// for testing purpose
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to GHIX-PlanmgmtMS module, invoke IssuerPaymentInfoRestController()");
		return "Welcome to GHIX-PlanmgmtMS module, invoke IssuerPaymentInfoRestController";
	}



	@RequestMapping(value = "", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO createIssuerPayment(@RequestBody IssuerPaymentInfoDTO issuerPaymentInfoDTO) {
		return pmmsIssuerPaymentInfoService.createIssuerPayment(issuerPaymentInfoDTO);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO updateIssuerPayment(@PathVariable("id") Integer id, @RequestBody IssuerPaymentInfoDTO issuerPaymentInfoDTO) {
		return pmmsIssuerPaymentInfoService.updateIssuerPayment(id, issuerPaymentInfoDTO);
	}
	
	@RequestMapping(value = "/hiosId/{hiosId}", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody IssuerPaymentResponseDTO getIssuerPaymentInfo(@PathVariable("hiosId") String hiosId) {
		return pmmsIssuerPaymentInfoService.getIssuerPaymentInfo(hiosId);
	}
	
}
