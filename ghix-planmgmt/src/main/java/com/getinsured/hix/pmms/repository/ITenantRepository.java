package com.getinsured.hix.pmms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.Tenant;

@Repository("pmmsITenantRepository")
public interface ITenantRepository extends JpaRepository<Tenant, Integer> {

	Tenant getTenantByCode(@Param("code") String code);
}
