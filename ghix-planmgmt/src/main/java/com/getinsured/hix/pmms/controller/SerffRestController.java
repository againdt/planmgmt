package com.getinsured.hix.pmms.controller;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffBatchSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffBatchSearchResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffPlanMgmtBatchDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffPlanMgmtBatchResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferAttachmentResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferSearchResponseDTO;
import com.getinsured.hix.pmms.service.SerffTransferService;

/**
 * Controller class is used to implement Microservice REST APIs for SERFF. 
 * @since Mar 15, 2017
 */
@Controller("pmmsSerffRestController")
@RequestMapping(value = "/serfftransfer")
public class SerffRestController {
	private static final Logger LOGGER = Logger.getLogger(SerffRestController.class);
	@Autowired
	private SerffTransferService pmmsSerffTransferService;
	
	public SerffRestController() {
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	// for testing purpose
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to GHIX-PlanmgmtMS module, invoke SerffRestController()");
		return "Welcome to GHIX-PlanmgmtMS module, invoke SerffRestController";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody SerffTransferSearchResponseDTO getSerffTransferHistory(@RequestBody SerffTransferSearchRequestDTO serffTransferSearchRequestDTO) {
		return pmmsSerffTransferService.getSerffTransferList(serffTransferSearchRequestDTO);
	}

	@RequestMapping(value = "/batchDataList", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody SerffBatchSearchResponseDTO getSerffBatchDataList(@RequestBody SerffBatchSearchRequestDTO serffBatchSearchRequestDTO) {
		return pmmsSerffTransferService.getSerffBatchSearchResponse(serffBatchSearchRequestDTO);
	}

	@RequestMapping(value = "/{serffReqId}/attachments", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody SerffTransferAttachmentResponseDTO getSerffTransferAttachments(@PathVariable("serffReqId") Integer serffReqId) {
		return pmmsSerffTransferService.getSerffTransferAttachments(serffReqId);
	}

	@RequestMapping(value = "/createBatchRecord", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody SerffPlanMgmtBatchResponseDTO createBatchRecord(@RequestBody SerffPlanMgmtBatchDTO serffPlanMgmtBatchDTO) {
		return pmmsSerffTransferService.createBatchRecord(serffPlanMgmtBatchDTO);
	}

	@RequestMapping(value = "/updateBatchRecord", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO updateBatchRecord(@RequestBody SerffPlanMgmtBatchDTO serffPlanMgmtBatchDTO) {
		return pmmsSerffTransferService.updateBatchRecord(serffPlanMgmtBatchDTO);
	}

	@RequestMapping(value = "/createPlanDocumentRecord", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO createPlanDocumentRecord(@RequestBody PlanDocumentsJobDTO planDocumentsJobDTO) {
		return pmmsSerffTransferService.createPlanDocumentRecord(planDocumentsJobDTO);
	}
	
	@RequestMapping(value = "/getPlanDocumentList", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody PlanDocumentsJobResponseDTO getPlanDocumentList(@RequestBody PlanDocumentsJobRequestDTO planDocumentsJobRequestDTO) {
		return pmmsSerffTransferService.getPlanDocumentList(planDocumentsJobRequestDTO);
	}
}
