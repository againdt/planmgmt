package com.getinsured.hix.pmms.service;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerPaymentInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerPaymentResponseDTO;

public interface IssuerPaymentInfoService {
	GhixResponseDTO createIssuerPayment(IssuerPaymentInfoDTO issuerPaymentInfoDTO);
	GhixResponseDTO updateIssuerPayment(Integer id, IssuerPaymentInfoDTO issuerPaymentInfoDTO);
	IssuerPaymentResponseDTO getIssuerPaymentInfo(String hiosId);
}
