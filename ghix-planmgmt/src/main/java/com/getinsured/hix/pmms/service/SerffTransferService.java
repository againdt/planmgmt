package com.getinsured.hix.pmms.service;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffBatchSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffBatchSearchResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffPlanMgmtBatchDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffPlanMgmtBatchResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferAttachmentResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferSearchResponseDTO;

/**
 * Interface is implemented in SerffTransferServiceImpl class.
 * @since Mar 15, 2017
 */
public interface SerffTransferService {

	SerffTransferSearchResponseDTO getSerffTransferList(SerffTransferSearchRequestDTO serffTransferSearchRequestDTO);

	SerffTransferAttachmentResponseDTO getSerffTransferAttachments(Integer serffReqId);

	SerffBatchSearchResponseDTO getSerffBatchSearchResponse(SerffBatchSearchRequestDTO requestDTO);

	SerffPlanMgmtBatchResponseDTO createBatchRecord(SerffPlanMgmtBatchDTO serffPlanMgmtBatchDTO);

	GhixResponseDTO updateBatchRecord(SerffPlanMgmtBatchDTO serffPlanMgmtBatchDTO);

	GhixResponseDTO createPlanDocumentRecord(PlanDocumentsJobDTO planDocumentsJobDTO);

	PlanDocumentsJobResponseDTO getPlanDocumentList(PlanDocumentsJobRequestDTO requestDTO);
}
