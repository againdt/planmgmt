package com.getinsured.hix.pmms.service.validation;

/**
 * Class is used to store validation error info.
 * @since Jan 10, 2017
 */
public class PlanMgmtErrorVO {

	private String fieldName;
	private String fieldValue;
	private String errorMessage;

	public PlanMgmtErrorVO(){
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
}
