package com.getinsured.hix.pmms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.IssuerD2c;

public interface IIssuerD2CRepository extends JpaRepository<IssuerD2c, Integer>, RevisionRepository<IssuerD2c, Integer, Integer> {
    @Query("SELECT i from IssuerD2c i where i.hiosIssuerId = :hiosIssuerId order by applicableYear desc")
    List<IssuerD2c> findByIssuerHiosId(@Param("hiosIssuerId") String hiosIssuerId);
    
    @Query("SELECT i from IssuerD2c i where i.hiosIssuerId = :hiosIssuerId and applicableYear = :plaYear")
    List<IssuerD2c> findByIssuerHiosIdAndYear(@Param("hiosIssuerId") String hiosIssuerId, @Param("plaYear") Integer plaYear);
}
