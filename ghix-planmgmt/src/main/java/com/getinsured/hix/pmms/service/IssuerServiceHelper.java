package com.getinsured.hix.pmms.service;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerLightDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerSearchResponseDTO;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.querybuilder.PmmsQueryBuilder;


@Service
@Qualifier("IssuerServiceHelper")
public class IssuerServiceHelper {

	@Value(PlanMgmtConstants.DATABASE_TYPE_CONFIG)
	private String DB_TYPE;
	
	public enum SortOrder {
		ASC, DESC
	}
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerServiceHelper.class);
	public static final String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	
	@PersistenceUnit
	private EntityManagerFactory emf;
	@Autowired private GIExceptionHandler giExceptionHandler;

	public IssuerSearchResponseDTO getIssuerList(Integer pageSize, Integer startRecord, String sortBy, String sortOrder, String issuerName, String status, 
			String tenantCode, String hiosIssuerId, String brandNameId, String d2cFlowTypeId, String d2cFlowYear) {		
		
		EntityManager entityManager = null;
		IssuerSearchResponseDTO issuerSearchResponseDTO = new IssuerSearchResponseDTO();
		
		try {
			entityManager = emf.createEntityManager();
			String sqlQuery = PmmsQueryBuilder.buildIssuerSearchQuery(DB_TYPE, tenantCode, d2cFlowTypeId, sortBy, sortOrder, startRecord, pageSize);
			
			Query query = entityManager.createNativeQuery(sqlQuery);
			query.setParameter(PlanMgmtConstants.STATUS, status);
			query.setParameter(PlanMgmtConstants.HIOS_ISSUER_ID, hiosIssuerId);
			query.setParameter(PlanMgmtConstants.NAME, issuerName);
			query.setParameter(PlanMgmtConstants.BRAND_NAME_ID, brandNameId);
			
			if (!PlanMgmtConstants.ALL.equalsIgnoreCase(tenantCode)
					&& !PlanMgmtConstants.NONE.equalsIgnoreCase(tenantCode)) {
				query.setParameter(PlanMgmtConstants.TENANT_CODE, tenantCode);
			}
			
			if (!PlanMgmtConstants.NO_D2C.equalsIgnoreCase(d2cFlowTypeId)) {
				query.setParameter(PlanMgmtConstants.D2C_FLOW_TYPE_ID, d2cFlowTypeId);
			}
			query.setParameter(PlanMgmtConstants.D2C_FLOW_YEAR, d2cFlowYear);
			
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
			// iterating thru representative records
			
			IssuerLightDTO issuerLightDTO = null;
			while (rsIterator.hasNext()) {
				issuerLightDTO = new IssuerLightDTO();
				Object[] objArray = (Object[]) rsIterator.next();
			
				issuerLightDTO.setIssuerId(Integer.parseInt(objArray[1].toString()));
				issuerLightDTO.setIssuerName((objArray[2] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[2].toString());
				issuerLightDTO.setHiosId((objArray[3] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[3].toString());

				if (null != objArray[4]) {
					issuerLightDTO.setLastUpdatedTimestamp(DateUtil.dateToString(DateUtil.StringToDate(objArray[4].toString(), PlanMgmtConstants.TIMESTAMP_PATTERN1), PlanMgmtConstants.TIMESTAMP_PATTERN3));
				}
				else {
					issuerLightDTO.setLastUpdatedTimestamp(PlanMgmtConstants.EMPTY_STRING);
				}
				issuerLightDTO.setCertificationStatus((objArray[5] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[5].toString());
				if(objArray[6] != null){
					String[] strTemp = objArray[6].toString().split(",");
					if(strTemp.length > 3){
						issuerLightDTO.setShortTenantList(strTemp[0] + ", " + strTemp[1] + ", " + strTemp[2] + " <a href='#' class='more' id='" +objArray[3].toString() +"' >...more</a>");
						issuerLightDTO.setLongTenantList(objArray[6].toString());
					}
					else{
						issuerLightDTO.setShortTenantList(objArray[6].toString());
						issuerLightDTO.setLongTenantList("");
					}
				} else{
					issuerLightDTO.setShortTenantList("");
				}
				 
				if(null != objArray[7] && null != objArray[8]){
					issuerLightDTO.setId(Integer.parseInt(objArray[7].toString()));
					issuerLightDTO.setBrandName(objArray[8].toString());
				}
				if(objArray[9] != null) {
					issuerLightDTO.setD2cFlowType(objArray[9].toString());
				} else {
					issuerLightDTO.setD2cFlowType(PlanMgmtConstants.NO_D2C_ENABLED);
				}
				
				issuerSearchResponseDTO.setIssuerLightDTO(issuerLightDTO);
			}

			if (!CollectionUtils.isEmpty(issuerSearchResponseDTO.getIssuerLightDTOList())) {

				query = entityManager.createNativeQuery(PmmsQueryBuilder.buildIssuerSearchCountQuery(DB_TYPE, tenantCode, d2cFlowTypeId));
				query.setParameter(PlanMgmtConstants.STATUS, status);
				query.setParameter(PlanMgmtConstants.HIOS_ISSUER_ID, hiosIssuerId);
				query.setParameter(PlanMgmtConstants.NAME, issuerName);
				query.setParameter(PlanMgmtConstants.BRAND_NAME_ID, brandNameId);
				
				if (!PlanMgmtConstants.ALL.equalsIgnoreCase(tenantCode)
						&& !PlanMgmtConstants.NONE.equalsIgnoreCase(tenantCode)) {
					query.setParameter(PlanMgmtConstants.TENANT_CODE, tenantCode);		
				}

				if (!PlanMgmtConstants.NO_D2C.equalsIgnoreCase(d2cFlowTypeId)) {
					query.setParameter(PlanMgmtConstants.D2C_FLOW_TYPE_ID, d2cFlowTypeId);
				}
				query.setParameter(PlanMgmtConstants.D2C_FLOW_YEAR, d2cFlowYear);

				Object objCount = query.getSingleResult();
				if (null != objCount) {
					issuerSearchResponseDTO.setTotalNumOfRecords(Integer.parseInt(objCount.toString()));
				}
				issuerSearchResponseDTO.setStatus(GhixResponseDTO.STATUS.SUCCESS.name());
			}
			else {
				issuerSearchResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
				issuerSearchResponseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.RECORD_NOT_FOUND.code);
				issuerSearchResponseDTO.setErrorMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
			}
		}
		catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to get Issuer List from Database", ex);
			issuerSearchResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
			issuerSearchResponseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
			issuerSearchResponseDTO.setErrorMsg(PlanMgmtConstants.ERROR_TECHNICAL);
		}
		finally {

			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
				entityManager = null;
			}
		}
		return issuerSearchResponseDTO;
	}
	
	public IssuerSearchResponseDTO getMedicareIssuerList(Integer pageSize, Integer startRecord, String sortBy, String sortOrder, String issuerName, String state, 
			String hiosIssuerId) {		
		
		EntityManager entityManager = null;
		IssuerSearchResponseDTO issuerSearchResponseDTO = new IssuerSearchResponseDTO();
		
		try {
			entityManager = emf.createEntityManager();
			String sqlQuery = PmmsQueryBuilder.buildMedicareIssuerSearchQuery(DB_TYPE, sortBy, sortOrder, startRecord, pageSize);
			
			Query query = entityManager.createNativeQuery(sqlQuery);
			query.setParameter(PlanMgmtConstants.STATES, state);
			query.setParameter(PlanMgmtConstants.HIOS_ISSUER_ID, hiosIssuerId);
			query.setParameter(PlanMgmtConstants.NAME, issuerName);
			
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
			
			IssuerLightDTO issuerLightDTO = null;
			while (rsIterator.hasNext()) {
				issuerLightDTO = new IssuerLightDTO();
				Object[] objArray = (Object[]) rsIterator.next();
			
				issuerLightDTO.setIssuerId(Integer.parseInt(objArray[1].toString()));
				issuerLightDTO.setStateOfDomicile((objArray[2] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[2].toString());
				issuerLightDTO.setIssuerName((objArray[3] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[3].toString());
				issuerLightDTO.setHiosId((objArray[4] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[4].toString());

				if (null != objArray[5]) {
					issuerLightDTO.setLastUpdatedTimestamp(DateUtil.dateToString(DateUtil.StringToDate(objArray[5].toString(), PlanMgmtConstants.TIMESTAMP_PATTERN1), PlanMgmtConstants.TIMESTAMP_PATTERN3));
				}
				else {
					issuerLightDTO.setLastUpdatedTimestamp(PlanMgmtConstants.EMPTY_STRING);
				}
				
				issuerSearchResponseDTO.setIssuerLightDTO(issuerLightDTO);
			}

			if (!CollectionUtils.isEmpty(issuerSearchResponseDTO.getIssuerLightDTOList())) {

				query = entityManager.createNativeQuery(PmmsQueryBuilder.buildMedicareIssuerSearchCountQuery(DB_TYPE));
				query.setParameter(PlanMgmtConstants.STATES, state);
				query.setParameter(PlanMgmtConstants.HIOS_ISSUER_ID, hiosIssuerId);
				query.setParameter(PlanMgmtConstants.NAME, issuerName);
				
				Object objCount = query.getSingleResult();
				if (null != objCount) {
					issuerSearchResponseDTO.setTotalNumOfRecords(Integer.parseInt(objCount.toString()));
				}
				issuerSearchResponseDTO.setStatus(GhixResponseDTO.STATUS.SUCCESS.name());
			}
			else {
				issuerSearchResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
				issuerSearchResponseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.RECORD_NOT_FOUND.code);
				issuerSearchResponseDTO.setErrorMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
			}
		}
		catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to get Medicare Issuer List from Database", ex);
			issuerSearchResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
			issuerSearchResponseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
			issuerSearchResponseDTO.setErrorMsg(PlanMgmtConstants.ERROR_TECHNICAL);
		}
		finally {

			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
				entityManager = null;
			}
		}
		return issuerSearchResponseDTO;
	}
}
