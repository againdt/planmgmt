package com.getinsured.hix.pmms.controller;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepSearchResponseDTO;
import com.getinsured.hix.pmms.service.IssuerRepresentativeService;

/**
 * Controller class is used to implement Microservice REST APIs for IssuerRepresentative. 
 * @since Jan 08, 2017
 */
@Controller("pmmsIssuerRepresentativeRestController")
@RequestMapping(value = "/issuerreps")
public class IssuerRepresentativeRestController {
	private static final Logger LOGGER = Logger
			.getLogger(IssuerRepresentativeRestController.class);
	@Autowired
	private IssuerRepresentativeService pmmsIssuerRepresentativeService;
	public IssuerRepresentativeRestController() {
	}

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	// for testing purpose
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to GHIX-PlanmgmtMS module, invoke IssuerRepresentativeRestController()");
		return "Welcome to GHIX-PlanmgmtMS module, invoke IssuerRepresentativeRestController";
	}

	@RequestMapping(value = "", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO createIssuerRep(@RequestBody IssuerRepDTO issuerRepDTO) {
		return pmmsIssuerRepresentativeService.createIssuerRep(issuerRepDTO);
	}

	@RequestMapping(value = "/{hiosId}/{id}", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody IssuerRepSearchResponseDTO getIssuerRep(@PathVariable("hiosId") String issuerHiosId, @PathVariable("id") String issuerRepId) {
		return pmmsIssuerRepresentativeService.getIssuerRepDetails(issuerHiosId,issuerRepId);
	}
	
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO updateIssuerRep(@PathVariable("id") String issuerRepId, @RequestBody IssuerRepDTO issuerRepDTO) {
		return pmmsIssuerRepresentativeService.updateIssuerRep(issuerRepId, issuerRepDTO);
	}

	@RequestMapping(value = "/email", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody IssuerRepSearchResponseDTO getIssuerRepByEmailId(@RequestBody IssuerRepDTO issuerRepDTO) {
		return pmmsIssuerRepresentativeService.getIssuerRepByEmail(issuerRepDTO);
	}

	@RequestMapping(value = "/hiosId/{hiosId}", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody IssuerRepSearchResponseDTO getIssuerRepsByHiosId(@PathVariable("hiosId") String issuerHiosId) {
		return pmmsIssuerRepresentativeService.getIssuerReps(issuerHiosId);
	}

	/**
	 * Method is used to get Issuer Representative List from database.
	 */
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody IssuerRepSearchResponseDTO getIssuerRepsList(@RequestBody IssuerRepSearchRequestDTO issuerRepSearchRequest) {
		return pmmsIssuerRepresentativeService.getIssuerReps(issuerRepSearchRequest);
	}

	/**
	 * Method is used to assign Issuer Representative mapping with issuer.
	 */
	@RequestMapping(value = "/assignMapping", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO assignIssuerRepMapping(@RequestBody IssuerRepDTO issuerRepDTO) {
		return pmmsIssuerRepresentativeService.assignIssuerRepMapping(issuerRepDTO);
	}

	/**
	 * Method is used to remove mapping of Issuer Representative with issuer.
	 */
	@RequestMapping(value = "removeMapping/{hiosId}/{id}", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO removeIssuerRepMapping(@PathVariable("hiosId") String issuerHiosId,
			@PathVariable("id") Integer issuerRepId) {
		return pmmsIssuerRepresentativeService.removeIssuerRepMapping(issuerHiosId, issuerRepId);
	}
}
