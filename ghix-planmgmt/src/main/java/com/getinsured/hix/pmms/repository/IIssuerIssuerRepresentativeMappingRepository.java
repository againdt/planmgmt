package com.getinsured.hix.pmms.repository;

import java.util.List;

//import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.jpa.repository.Query;
//import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.IssuerIssuerRepresentativeMapping;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IIssuerIssuerRepresentativeMappingRepository extends JpaRepository<IssuerIssuerRepresentativeMapping, Integer>{

	@Query("FROM IssuerIssuerRepresentativeMapping irm WHERE irm.issuer.id=:issuerId AND irm.issuerRepresentative.id=:issuerRepresentativeId")
	IssuerIssuerRepresentativeMapping findByIssuerRepId(@Param("issuerId") Integer issuerId, @Param("issuerRepresentativeId") Integer issuerRepresentativeId);
	
	@Query("FROM IssuerIssuerRepresentativeMapping irm WHERE irm.issuer.id=:issuerId")
	List<IssuerIssuerRepresentativeMapping> findByIssuerId(@Param("issuerId") Integer issuerId);
	
	@Query("FROM IssuerIssuerRepresentativeMapping irm WHERE irm.issuer.id=:issuerId AND irm.primaryContact='YES' ")
	IssuerIssuerRepresentativeMapping getPrimaryContactByIssuerId(@Param("issuerId") Integer issuerId);
	
}
