package com.getinsured.hix.pmms.controller;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameResponseDTO;
import com.getinsured.hix.pmms.service.IssuerBrandNameService;

/**
 * Controller class is used to implement Microservice REST APIs for Issuer Brand Names.
 * 
 * @since Jan 08, 2017
 */
@Controller("pmmsIssuerBrandNamesController")
@RequestMapping(value = "/issuerbrandnames")
public class IssuerBrandNamesController {

	private static final Logger LOGGER = Logger.getLogger(IssuerBrandNamesController.class);

	@Autowired private IssuerBrandNameService pmmsIssuerBrandNameService;

	public IssuerBrandNamesController() {
	}

	/**
	 * For testing purpose
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		LOGGER.info("Welcome to GHIX-PlanmgmtMS module, invoke IssuerBrandNamesController()");
		return "Welcome to GHIX-PlanmgmtMS module, invoke IssuerBrandNamesController";
	}

	/**
	 * Method is used to get Issuer Brand Name List from database.
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Produces("application/json")
	public @ResponseBody IssuerBrandNameResponseDTO getIssuerBrandNames() {
		LOGGER.debug("Begin execution of Get IssuerBrandNames API.");

		try {
			return pmmsIssuerBrandNameService.getIssuerBrandNameList();
		}
		finally {
			LOGGER.debug("End execution of Get IssuerBrandNames API.");
		}
	}

	/**
	 * Method is used to create Issuer Brand Name record in database.
	 */
	@RequestMapping(value = "", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO createIssuerBrandName(@RequestBody IssuerBrandNameDTO issuerBrandNameDTO) {
		LOGGER.debug("Begin execution of createIssuerBrandName API.");

		try {
			return pmmsIssuerBrandNameService.addIssuerBrandNameInDB(issuerBrandNameDTO);
		}
		finally {
			LOGGER.debug("End execution of createIssuerBrandName API.");
		}
	}

	/**
	 * Method is used to bulk update Brand-Id in Issuer Table.
	 */
	@RequestMapping(value = "/updateIssuerBrandName", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody GhixResponseDTO bulkUpdateIssuerBrandId(@RequestBody IssuerBrandNameRequestDTO brandNameRequestDTO) {
		LOGGER.debug("Begin execution of bulkUpdateIssuerBrandId API.");

		try {
			return pmmsIssuerBrandNameService.bulkUpdateIssuerBrandId(brandNameRequestDTO);
		}
		finally {
			LOGGER.debug("End execution of bulkUpdateIssuerBrandId API.");
		}
	}
	
	
	/**
	 * Method is used to check for duplicate BrandId or BrandName
	 */
	@RequestMapping(value = "/checkDuplicate", method = RequestMethod.POST)
	public @ResponseBody boolean checkDuplicateBrandNameOrId(@RequestBody IssuerBrandNameDTO issuerBrandNameDTO) {
		LOGGER.debug("Begin execution of checkDuplicateBrandNameOrId API.");
		try {
			
			return pmmsIssuerBrandNameService.isDuplicateExist(issuerBrandNameDTO);
		}
		finally {
			LOGGER.debug("End execution of checkDuplicateBrandNameOrId API.");
		}
	}
}
