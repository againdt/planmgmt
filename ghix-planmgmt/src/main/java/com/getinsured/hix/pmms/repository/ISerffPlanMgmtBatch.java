package com.getinsured.hix.pmms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;

/**
 * Interface is used to get data from SERFF_PLAN_MGMT_BATCH table.
 * @since May 12, 2017
 */
@Repository
public interface ISerffPlanMgmtBatch extends JpaRepository<SerffPlanMgmtBatch, Long> {

}
