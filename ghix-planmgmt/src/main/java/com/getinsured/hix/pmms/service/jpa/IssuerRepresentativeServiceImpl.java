package com.getinsured.hix.pmms.service.jpa;

import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.setErrorMessage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepSearchResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerIssuerRepresentativeMapping;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes.ErrorCode;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.repository.IIssuerIssuerRepresentativeMappingRepository;
import com.getinsured.hix.pmms.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.pmms.service.IssuerRepresentativeService;
import com.getinsured.hix.pmms.service.validation.IssuerAPIRequestValidator;
import com.getinsured.hix.pmms.service.validation.PlanMgmtErrorVO;
import com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil;

/**
 * Class is used to provide service to IssuerRepresentative Service APIs [Plan Management-Microservice]
 * @since Jan 10, 2017
 */
@Service("pmmsIssuerRepresentativeService")
public class IssuerRepresentativeServiceImpl  implements IssuerRepresentativeService {

	private static final Logger LOGGER = Logger.getLogger(IssuerRepresentativeServiceImpl.class);

	@Autowired private com.getinsured.hix.planmgmt.service.IssuerService issuerService;
	@Autowired private GIExceptionHandler giExceptionHandler;
	@Autowired private IssuerAPIRequestValidator issuerAPIRequestValidator;
	@Autowired private IIssuerRepresentativeRepository iIssuerRepresentativeRepository;
	@Autowired private IIssuerIssuerRepresentativeMappingRepository representativeMappingRepository;
	@Autowired private UserService userService;

	@PersistenceUnit private EntityManagerFactory emf;

	private static final String ASC = "ASC";
	private static final String DESC = "DESC";
	private static final String EMPTY_STRING = "";
	private static final String TRUE_STRING = "true";
	//private static final String NULL_STRING = "NULL";
	private static final String FALSE_STRING = "false";

	@Value(PlanMgmtConstants.DATABASE_TYPE_CONFIG)
	private String DB_TYPE;

	@Override
	public IssuerRepSearchResponseDTO getIssuerReps(IssuerRepSearchRequestDTO issuerRepSearchRequest) {
		IssuerRepSearchResponseDTO issuerRepSearchResponseDTO = new IssuerRepSearchResponseDTO();
		if(issuerAPIRequestValidator.validateIssuerRepSearchRequest(issuerRepSearchRequest, issuerRepSearchResponseDTO)) {
			List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
			String queryStr = getIssuerRepSearchQuery(issuerRepSearchRequest);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Final build query " + queryStr);
			}
			IssuerRepDTO issuerRepDTO = issuerRepSearchRequest.getIssuerRepDTO();
			Issuer issuer = issuerService.getIssuerByHIOSId(issuerRepDTO.getHiosId());
			EntityManager entityManager = null;

			if (null == issuer) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("getIssuerReps: Failed to get Issuer for HIOS ID: " + issuerRepDTO.getHiosId());
				}
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Issuer HIOS_ID", issuerRepDTO.getHiosId(), "Failed to get Issuer record.");
			}
			else {
				try {
					issuerRepSearchResponseDTO.setIssuerName(issuer.getName());
					issuerRepSearchResponseDTO.setCompanyLogo(issuer.getLogoURL());

					entityManager = emf.createEntityManager();
					Query query = entityManager.createNativeQuery(queryStr);
					query.setParameter("issuerId", issuer.getId());
					List<?> rsList = query.getResultList();
					if(null != rsList) {
						Iterator<?> rsIterator = rsList.iterator();
						int count = 0;
						SimpleDateFormat sdf = new SimpleDateFormat(GhixConstants.DISPLAY_DATE_FORMAT);

						while (rsIterator.hasNext()) {
							IssuerRepDTO issuerRep = new IssuerRepDTO();
							Object[] objArray = (Object[]) rsIterator.next();
							issuerRep.setId(objArray[1].toString());
							issuerRep.setFirstName((objArray[2] != null) ? objArray[2].toString() : "");
							issuerRep.setLastName((objArray[3] != null) ? objArray[3].toString() : "");
							issuerRep.setIssuerRepTitle((objArray[4] != null) ? objArray[4].toString() : "");
							issuerRep.setLastUpdateDate((objArray[5] != null) ? sdf.format(DateUtil.StringToDate(objArray[5].toString(), "yyyy-MM-dd")) : "");
							issuerRep.setConfirmed((objArray[6] != null) ? objArray[6].toString() : "0");
							issuerRep.setLastUpdatedBy((objArray[7] != null) ? objArray[7].toString() : "");
							issuerRep.setHiosId(issuerRepDTO.getHiosId());
							issuerRep.setUserId((objArray[8] != null) ? objArray[8].toString() : "0");
							issuerRep.setPrimaryContact((objArray[9] != null) ? objArray[9].toString() : "");
							//issuerRep.setIssuerId((objArray[10] != null) ? objArray[10].toString() : "")
							//issuerRep.setPhone((objArray[9] != null) ? objArray[9].toString() : "");
							//issuerRep.setEmail(email);
							//issuerRepData.put("updated", (objArray[5] != null) ? sdf.format(DateUtil.StringToDate((objArray[5].toString()), "yyyy-MM-dd")) : "");
							issuerRepSearchResponseDTO.setIssuerRepDTO(issuerRep);
							count++;
						}
						issuerRepSearchResponseDTO.setTotalNumOfRecords(Integer.toString(count));
						issuerRepSearchResponseDTO.setCompanyLogo(issuer.getLogoURL());
					}
				} catch(Exception ex) {
					LOGGER.error("Exception occurred in getIssuerRepresentatives: ", ex);
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting representative list for Issuer having HIOS ID: " + issuerRepDTO.getHiosId() );
					giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.MANAGE_REPRESENTATIVE_EXCEPTION.code), "Exception occurred in getIssuerReps service: ",ex);
				}
				finally
				{
					if(entityManager != null && entityManager.isOpen())
					{
						try {
							entityManager.close();
						}
						catch(IllegalStateException ex)
						{
							LOGGER.error("EntityManager is managed by application server", ex);
						}
					}
				}
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.MANAGE_REPRESENTATIVE_EXCEPTION.getCode(), issuerRepSearchResponseDTO);
		}
		return issuerRepSearchResponseDTO;
	}

	private String getIssuerRepSearchQuery(IssuerRepSearchRequestDTO issuerRepSearchRequest) {
		String sortColumn = issuerRepSearchRequest.getSortBy();
		String pageNum = issuerRepSearchRequest.getPageNumber();
		int startRecord = 0, endRecords = 1;
		if (pageNum != null) {
			try
			{
				endRecords = Integer.parseInt(pageNum) * GhixConstants.PAGE_SIZE;
				startRecord = (Integer.parseInt(pageNum) - 1) * GhixConstants.PAGE_SIZE;
			}
			catch(NumberFormatException nfe)
			{
				endRecords *= GhixConstants.PAGE_SIZE;
			}
		} else {
			endRecords = endRecords * GhixConstants.PAGE_SIZE;
		}

		String sortOrder = (issuerRepSearchRequest.getChangeOrder() == null) ? ASC : ((issuerRepSearchRequest.getSortOrder().equalsIgnoreCase(ASC) ? ASC : DESC));
		sortOrder = (sortOrder.equals(EMPTY_STRING)) ? ASC : sortOrder;
		
		String changeOrder = (issuerRepSearchRequest.getChangeOrder() == null) ? FALSE_STRING : ((issuerRepSearchRequest.getChangeOrder().equalsIgnoreCase(FALSE_STRING) ? FALSE_STRING : TRUE_STRING));
		sortOrder = (changeOrder.equals(TRUE_STRING)) ? ((sortOrder.equals(ASC)) ? DESC : ASC) : sortOrder;

		StringBuilder buildquery = new StringBuilder();

		if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
			buildquery.append("SELECT t.* FROM (SELECT t.* FROM (SELECT ir.id as tempId, ir.id as id, ir.first_name,ir.last_name, ir.title, ir.last_update_timestamp, u.confirmed, (select FIRST_NAME ||' '|| LAST_NAME from users where id=ir.updatedBy) AS updatedBy, ir.user_id, iir.primary_contact, ir.issuer_id ");
		}
		else {
			buildquery = new StringBuilder("SELECT t.* FROM (SELECT ROWNUM AS rn , t.* FROM (SELECT ir.id, ir.first_name,ir.last_name, ir.title, ir.last_update_timestamp, u.confirmed, (select FIRST_NAME ||' '|| LAST_NAME from users where id=ir.updatedBy) AS updatedBy, ir.user_id, iir.primary_contact, ir.issuer_id ");
		}
		buildquery.append("FROM pm_issuer_issuerrep iir INNER JOIN issuer_representative ir on iir.issuer_rep_id = ir.id LEFT JOIN users u ON ir.user_id = u.id WHERE (iir.issuer_id=:issuerId) ");
		IssuerRepDTO issuerRepDTO = issuerRepSearchRequest.getIssuerRepDTO();
		// when search for particular representative on the basis of name
		String appendClause = null;
		String endWith = null;
		if (!StringUtils.isBlank(issuerRepDTO.getFirstName())) {
			buildquery.append(" AND ( UPPER(ir.first_name) LIKE '%" + issuerRepDTO.getFirstName().trim().toUpperCase() + "%' ");
			appendClause = " OR ";
			endWith = ")";
		} else {
			appendClause = " AND (";
			endWith = "";
		}
		if (!StringUtils.isBlank(issuerRepDTO.getLastName())) {
			buildquery.append(appendClause);
			buildquery.append(" UPPER(ir.last_name) LIKE '%" + issuerRepDTO.getLastName().trim().toUpperCase() + "%') ");
		} else {
			buildquery.append(endWith);
		}
			// when search for particular representative on the basis of title
		if (!StringUtils.isBlank(issuerRepDTO.getIssuerRepTitle())) {
			buildquery.append(" AND UPPER(ir.title) LIKE '%" + issuerRepDTO.getIssuerRepTitle().trim().toUpperCase() + "%'");
		}
		
		if (StringUtils.isNotBlank(issuerRepDTO.getConfirmed())) {

			if ("1".equals(issuerRepDTO.getConfirmed())) {
				buildquery.append(" AND ( u.confirmed = '");
				buildquery.append(issuerRepDTO.getConfirmed());
				buildquery.append("' ) ");
			}
			else {
				buildquery.append(" AND ( u.confirmed = '");
				buildquery.append(issuerRepDTO.getConfirmed());
				buildquery.append("' or u.confirmed is null ) ");
			}
		}
		
		if (StringUtils.isNotBlank(sortColumn)) {
			if(sortColumn.equals("firstName")) {
				buildquery.append(" ORDER BY ir.first_name ");
			} else if (sortColumn.equals("title")) {
				buildquery.append(" ORDER BY ir.title ");
			} else if (sortColumn.equals("updated")) {
				buildquery.append(" ORDER BY ir.last_update_timestamp ");
			} else if (sortColumn.equals("userRecord.confirmed")) {
				buildquery.append(" ORDER BY u.confirmed ");
			} else if (sortColumn.equals("updatedBy.firstName")) {
				buildquery.append(" ORDER BY updatedBy ");
			} else {
				LOGGER.warn("Sorting ignored for issuer rep search as sorting not supported for column ".intern() + sortColumn);
			}
		} else {
			buildquery.append(" ORDER BY ir.last_update_timestamp ");
		}
		if (StringUtils.isNotBlank(sortOrder) && (sortOrder.equalsIgnoreCase(SortOrder.DESC.toString()))) {
			if (StringUtils.isNotBlank(sortColumn)) {
				if (sortColumn.equals("userRecord.confirmed")) {
					buildquery.append(" DESC NULLS last ");
				} else {
					buildquery.append(" DESC ");
				}
			} else {
				buildquery.append(" DESC ");
			}

		} else {
			if (StringUtils.isNotBlank(sortColumn)) {
				if (sortColumn.equals("userRecord.confirmed")) {
					buildquery.append(" ASC NULLS first ");
				} else {
					buildquery.append(" ASC ");
				}
			} else {
				buildquery.append(" ASC ");
			}
		}

		// Page number would be -1 when there is no pagination applied in API.
		if ("-1".equals(pageNum)) {
			buildquery.append(" ) t ) t ");
		}
		else {

			if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
				buildquery.append(" ) t ) t ");
				buildquery.append(" offset " + startRecord);
				buildquery.append(" limit " + GhixConstants.PAGE_SIZE);
			}
			else {
				buildquery.append(" ) t where rownum <= " + endRecords + " ) t ");
				buildquery.append(" WHERE rn > " + startRecord);
			}
		}
		return buildquery.toString();
	}

	@Override
	public IssuerRepSearchResponseDTO getIssuerRepDetails(String issuerHiosId, String issuerRepId) {
		IssuerRepSearchResponseDTO issuerRepSearchResponseDTO = new IssuerRepSearchResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		
		if(StringUtils.isNotBlank(issuerRepId) && StringUtils.isNumeric(issuerRepId) 
				 && StringUtils.isNotBlank(issuerHiosId) && StringUtils.isNumeric(issuerHiosId) ) {
			try {
				Integer repId = Integer.parseInt(issuerRepId);
				if (repId != null) {
					IssuerRepresentative representativeObj = iIssuerRepresentativeRepository.findById(repId);
					if(null != representativeObj) {
						IssuerRepDTO issuerRepDTO = new IssuerRepDTO();
						//issuerRepDTO.setCreateBy(representativeObj.);
						issuerRepDTO.setEmail(representativeObj.getEmail());
						issuerRepDTO.setFirstName(representativeObj.getFirstName());
						issuerRepDTO.setLastName(representativeObj.getLastName());
						issuerRepDTO.setId(Integer.toString(representativeObj.getId()));
						issuerRepDTO.setIssuerRepTitle(representativeObj.getTitle());
						if(null != representativeObj.getUpdatedBy()) {
							issuerRepDTO.setLastUpdatedBy(Integer.toString(representativeObj.getUpdatedBy().getId()));
						}
						issuerRepDTO.setPhone(representativeObj.getPhone());
						if(null != representativeObj.getUserRecord()) {
							issuerRepDTO.setUserId(Integer.toString(representativeObj.getUserRecord().getId()));
						}

						Issuer issuer = issuerService.getIssuerByHIOSId(issuerHiosId);
						if (null != issuer) {
							IssuerIssuerRepresentativeMapping issuerRepresentativeMapping = representativeMappingRepository.findByIssuerRepId(issuer.getId(), representativeObj.getId());
							if (null != issuerRepresentativeMapping) {
								issuerRepDTO.setHiosId(issuer.getHiosIssuerId());
								issuerRepSearchResponseDTO.setIssuerName(issuer.getName());
								issuerRepSearchResponseDTO.setCompanyLogo(issuer.getLogoURL());
								if(null != issuerRepresentativeMapping.getPrimaryContact()) {
									issuerRepDTO.setPrimaryContact(issuerRepresentativeMapping.getPrimaryContact().toString());
								}
							}
						}
						issuerRepSearchResponseDTO.setIssuerRepDTO(issuerRepDTO);
					} else {
						if(LOGGER.isInfoEnabled()) {
						LOGGER.info("getIssuerRepDetails: Issuer Representative record not found." + repId + " Issuer Hios Id :" + issuerHiosId);
						}
						PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Failed to get Issuer representative details.");
					}
				}
			} catch (Exception ex) {
				LOGGER.error("Exception occurred while getting Issuer Representative infor for ID: " + issuerRepId, ex);
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting Issuer Representative Info.");
				giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.REPRESENTATIVE_DETAILS_EXCEPTION.code), "Exception occurred in getIssuerRepDetails service: ",ex);
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.REPRESENTATIVE_DETAILS_EXCEPTION.getCode(), issuerRepSearchResponseDTO);
		} else {
			setErrorMessage(errorVOList, "Issuer Representative ID", null, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), issuerRepSearchResponseDTO);
		}
		return issuerRepSearchResponseDTO;
	}


	@Override
	public GhixResponseDTO createIssuerRep(IssuerRepDTO issuerRepDTO) {
		GhixResponseDTO ghixResponseDTO = new GhixResponseDTO();
		boolean duplicateIssuerRep = false;

		if(issuerAPIRequestValidator.validateIssuerRepresentativeInfo(issuerRepDTO, ghixResponseDTO)) {
			List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
			String hiosId = issuerRepDTO.getHiosId();
			Issuer issuerObj = issuerService.getIssuerByHIOSId(hiosId);
			if(null != issuerObj) {
				IssuerRepresentative issuerRepresentativeObj = null;
				EntityManager em = emf.createEntityManager();
				if(null != em && em.isOpen()) {
					try {
						issuerRepresentativeObj = iIssuerRepresentativeRepository.findByEmail(issuerRepDTO.getEmail());
	                    if(issuerRepresentativeObj == null){
	                    	issuerRepresentativeObj = new IssuerRepresentative();
	    					if(em.isOpen()) {
		        				em.getTransaction().begin();
		                    	issuerRepresentativeObj = saveIssuerRepInformation(issuerRepDTO, issuerRepresentativeObj, issuerObj, em, true);
		                    	em.getTransaction().commit();
		                    	ghixResponseDTO.setId(issuerRepresentativeObj.getId());
	    					}
	                    } else {
	                    	duplicateIssuerRep = true;
	        				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Cannot create new record as record already exist for email: " + issuerRepDTO.getEmail());
	                    }
					} catch(Exception ex) {
						if(em.isOpen() && em.getTransaction().isActive()) {
	                    	em.getTransaction().rollback();
						}
						LOGGER.error("Exception occurred in saving Issuer Representative details: " + ex.getMessage());
						PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred in saving Issuer Representative details.");
						giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.SAVE_REPRESENTATIVE_EXCEPTION.code), "Exception occurred in createIssuerRep service: ",ex);
					} finally {
						if(em.isOpen()) {
							em.close();
							em = null;
						}
					}
				} else {
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "DB error while creating issuer representative record.");
				}
			}  else {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("saveIssuerRepresentative: Failed to get Issuer for HIOS ID: " + hiosId);
				}
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Issuer HIOS_ID", hiosId, "Failed to get Issuer record.");
			}

			if (duplicateIssuerRep) {
				PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.DUPLICATE_EMAIL_ADDRESS_EXCEPTION.getCode(), ghixResponseDTO);
			}
			else {
				PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.SAVE_REPRESENTATIVE_EXCEPTION.getCode(), ghixResponseDTO);
			}
		}
		return 	ghixResponseDTO;
	}

	@Override
	public GhixResponseDTO updateIssuerRep(String issuerRepId, IssuerRepDTO issuerRepDTO) {
		GhixResponseDTO ghixResponseDTO = new GhixResponseDTO();
		if(issuerAPIRequestValidator.validateIssuerRepresentativeInfo(issuerRepDTO, ghixResponseDTO)) {
			List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
			if(null == issuerRepId) {
				setErrorMessage(errorVOList, "Issuer Representative ID", null, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
			} else {
				EntityManager em = emf.createEntityManager();
				if(null != em && em.isOpen()) {
					try {
						IssuerRepresentative representativeObj = iIssuerRepresentativeRepository.findById(Integer.parseInt(issuerRepId));
		                if(representativeObj == null){
		    				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Issuer Representative Record does not exist for ID: " + issuerRepId);
		                } else if(!issuerRepDTO.getHiosId().equals(representativeObj.getIssuer().getHiosIssuerId())) {
		    				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Issuer Representative Record does not belong to provided HIOS ID: " + issuerRepDTO.getHiosId());
		                } else if(!issuerRepDTO.getEmail().equals(representativeObj.getEmail())) {
		    				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Email ID of Issuer Representative could not be changed : " + issuerRepDTO.getEmail());
		                } else {
	    					if(em.isOpen()) {
		        				em.getTransaction().begin();
		        				saveIssuerRepInformation(issuerRepDTO, representativeObj, representativeObj.getIssuer(), em, false);
		        				em.getTransaction().commit();
		        				ghixResponseDTO.setId(representativeObj.getId());
	    					}
		                }
					} catch(Exception ex) {
						if(em.isOpen() && em.getTransaction().isActive()) {
	                    	em.getTransaction().rollback();
						}
						LOGGER.error("Exception occurred in saving Issuer Representative info: " + ex.getMessage());
						PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred in saving Issuer Representative info.");
						giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.ISSUER_CERTIFICATION_STATUS_EXCEPTION.code), "Exception occurred in Updating Issuer Representative service: ",ex);
					} finally {
						if(em.isOpen()) {
							em.close();
							em = null;
						}
					}
				} else {
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "DB error while updating issuer representative record.");
				}
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), ghixResponseDTO);
		}
		return 	ghixResponseDTO;
	}

	@Override
	public IssuerRepresentative saveIssuerRepInformation(IssuerRepDTO issuerRepDTO, IssuerRepresentative representativeObj, Issuer issuer, EntityManager em, boolean isNew) throws Exception {
		IssuerRepresentative newIssuerRepresentative = null;
		if(null != em && em.isOpen()) {
			if(isNew) {
				representativeObj.setIssuer(issuer);
				representativeObj.setUserRecord(null); 
				representativeObj.setLocation(null);
				representativeObj.setEmail(issuerRepDTO.getEmail());
			}
			AccountUser accountUserObj = userService.findById(Integer.parseInt(issuerRepDTO.getLastUpdatedBy()));
			representativeObj.setUpdatedBy(accountUserObj);
			representativeObj.setFirstName(issuerRepDTO.getFirstName());
			representativeObj.setLastName(issuerRepDTO.getLastName());
			representativeObj.setTitle(issuerRepDTO.getIssuerRepTitle());
			representativeObj.setPhone(issuerRepDTO.getPhone());
			newIssuerRepresentative = em.merge(representativeObj);

			IssuerIssuerRepresentativeMapping issuerRepresentativeMapping = representativeMappingRepository.findByIssuerRepId(issuer.getId(), newIssuerRepresentative.getId());
			List<IssuerIssuerRepresentativeMapping> mappings = representativeMappingRepository.findByIssuerId(issuer.getId());

			if (null == issuerRepresentativeMapping) {
				issuerRepresentativeMapping = new IssuerIssuerRepresentativeMapping();
				issuerRepresentativeMapping.setIssuer(issuer);
				issuerRepresentativeMapping.setIssuerRepresentative(newIssuerRepresentative);
				issuerRepresentativeMapping.setCreatedBy(Integer.parseInt(issuerRepDTO.getLastUpdatedBy()));
				issuerRepresentativeMapping.setLastUpdateBy(Integer.parseInt(issuerRepDTO.getLastUpdatedBy()));
				if(mappings.isEmpty()) {
					issuerRepresentativeMapping.setPrimaryContact(IssuerIssuerRepresentativeMapping.PrimaryContact.YES);
				}
				else {
					issuerRepresentativeMapping.setPrimaryContact(IssuerIssuerRepresentativeMapping.PrimaryContact.NO);
				}
				em.merge(issuerRepresentativeMapping);
			}
			else if (!mappings.isEmpty()) {
				issuerRepresentativeMapping.setLastUpdateBy(Integer.parseInt(issuerRepDTO.getLastUpdatedBy()));

				if (IssuerIssuerRepresentativeMapping.PrimaryContact.YES.toString().equalsIgnoreCase(issuerRepDTO.getPrimaryContact())) {
					issuerRepresentativeMapping.setPrimaryContact(IssuerIssuerRepresentativeMapping.PrimaryContact.YES);

					for (IssuerIssuerRepresentativeMapping iirm : mappings) {

						if (null != iirm && newIssuerRepresentative.getId() != iirm.getIssuerRepresentative().getId()
								&& iirm.getPrimaryContact().equals(IssuerIssuerRepresentativeMapping.PrimaryContact.YES)) {
							iirm.setPrimaryContact(IssuerIssuerRepresentativeMapping.PrimaryContact.NO);
							iirm.setCreatedBy(Integer.parseInt(issuerRepDTO.getLastUpdatedBy()));
							iirm.setLastUpdateBy(Integer.parseInt(issuerRepDTO.getLastUpdatedBy()));
							em.merge(iirm);
						}
					}
				}
				else {
					issuerRepresentativeMapping.setPrimaryContact(IssuerIssuerRepresentativeMapping.PrimaryContact.NO);
				}
				em.merge(issuerRepresentativeMapping);
			}
		}
		return newIssuerRepresentative;
	}


	@Override
	public IssuerRepSearchResponseDTO getIssuerRepByEmail(IssuerRepDTO issuerRepDTO) {
		IssuerRepSearchResponseDTO issuerRepSearchResponseDTO = new IssuerRepSearchResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		if(null != issuerRepDTO && StringUtils.isNotBlank(issuerRepDTO.getEmail())) {
			String issuerRepEmailId = issuerRepDTO.getEmail();
			try {
				IssuerRepresentative representativeObj = iIssuerRepresentativeRepository.findByEmail(issuerRepEmailId);
				if(null != representativeObj) {
					IssuerRepDTO issuerRepDTOObj = new IssuerRepDTO();
					//issuerRepDTO.setCreateBy(representativeObj.);
					issuerRepDTOObj.setEmail(representativeObj.getEmail());
					issuerRepDTOObj.setFirstName(representativeObj.getFirstName());
					issuerRepDTOObj.setLastName(representativeObj.getLastName());

					Issuer issuer = representativeObj.getIssuer();
					if (null != issuer) {
						issuerRepDTOObj.setHiosId(issuer.getHiosIssuerId());
						issuerRepSearchResponseDTO.setIssuerName(issuer.getName());
						issuerRepSearchResponseDTO.setCompanyLogo(issuer.getLogoURL());

						IssuerIssuerRepresentativeMapping issuerRepresentativeMapping = representativeMappingRepository.findByIssuerRepId(issuer.getId(), representativeObj.getId());
						if (null != issuerRepresentativeMapping && null != issuerRepresentativeMapping.getPrimaryContact()) {
							issuerRepDTO.setPrimaryContact(issuerRepresentativeMapping.getPrimaryContact().toString());
						}
					}
					issuerRepDTOObj.setId(Integer.toString(representativeObj.getId()));
					issuerRepDTOObj.setIssuerRepTitle(representativeObj.getTitle());
					if(null != representativeObj.getUpdatedBy()) {
						issuerRepDTOObj.setLastUpdatedBy(Integer.toString(representativeObj.getUpdatedBy().getId()));
					}
					issuerRepDTOObj.setPhone(representativeObj.getPhone());
					if(null != representativeObj.getUserRecord()) {
						issuerRepDTOObj.setUserId(Integer.toString(representativeObj.getUserRecord().getId()));
					}
					issuerRepSearchResponseDTO.setIssuerRepDTO(issuerRepDTOObj);
					issuerRepSearchResponseDTO.setTotalNumOfRecords("1");
				} else {
					LOGGER.info("getIssuerRepDetails: Issuer Representative record not found. Email ID: " + issuerRepEmailId);
					issuerRepSearchResponseDTO.setTotalNumOfRecords("0");
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Failed to get Issuer representative details for email ID - " + issuerRepEmailId);
				}
			} catch (Exception ex) {
				LOGGER.error("Exception occurred while getting Issuer Representative infor for Email ID: " + issuerRepEmailId, ex);
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting Issuer Representative Info for email id: " + issuerRepEmailId);
				giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.REPRESENTATIVE_DETAILS_EXCEPTION.code), "Exception occurred in getIssuerRepByEmail service: ",ex);
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.REPRESENTATIVE_DETAILS_EXCEPTION.getCode(), issuerRepSearchResponseDTO);
		} else {
			setErrorMessage(errorVOList, "Issuer Representative Email ID", null, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), issuerRepSearchResponseDTO);
		}
		return issuerRepSearchResponseDTO;
	}


	@Override
	public IssuerRepSearchResponseDTO getIssuerReps(String issuerHiosId) {
		IssuerRepSearchRequestDTO issuerRepSearchRequest = new IssuerRepSearchRequestDTO();
		IssuerRepDTO issuerRepDTO = new IssuerRepDTO();
		issuerRepDTO.setHiosId(issuerHiosId);
		issuerRepSearchRequest.setIssuerRepDTO(issuerRepDTO);
		return getIssuerReps(issuerRepSearchRequest);
	}
	
	@Override
	public String isEmailAssignedToIssuerRep(String userEmailId) {

		IssuerRepresentative issuerRepresentative = iIssuerRepresentativeRepository.findByEmail(userEmailId);
		if (issuerRepresentative != null) {
			return PlanMgmtConstants.DATA_FOUND + ";" + issuerRepresentative.getFirstName() + ";" + issuerRepresentative.getLastName() + ";"
					+ issuerRepresentative.getTitle() + ";" + issuerRepresentative.getPhone();
		} else {
			AccountUser useObj = userService.findByEmail(userEmailId);
			if (useObj != null) {
				return PlanMgmtConstants.ASSOCIATED_WITH_DIFF_ROLE;
			} else {
				return PlanMgmtConstants.NO_DATA_FOUND;
			}
		}
	}

	@Override
	public IssuerRepresentative getRepresentativeByEmail(String userEmailId) {
		return iIssuerRepresentativeRepository.findByEmail(userEmailId);
	}

	/**
	 * Method is used to assign Issuer Representative mapping with issuer.
	 */
	@Override
	@Transactional
	public GhixResponseDTO assignIssuerRepMapping(IssuerRepDTO issuerRepDTO) {

		GhixResponseDTO ghixResponseDTO = new GhixResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		try {
			boolean hasValidValue = true;

			if (issuerAPIRequestValidator.validateIssuerRepForMapping(errorVOList, issuerRepDTO, ghixResponseDTO)) {

				Issuer issuer = issuerService.getIssuerByHIOSId(issuerRepDTO.getHiosId());
				IssuerRepresentative representativeObj = null;

				if (null == issuer) {
					hasValidValue = false;
					setErrorMessage(errorVOList, "Issuer HIOS ID", issuerRepDTO.getHiosId(), PlanMgmtValidatorUtil.EMSG_NOT_EXIST_IN_DB);
				}
				else {
					representativeObj = iIssuerRepresentativeRepository.findById(Integer.valueOf(issuerRepDTO.getId()));
					if (null == representativeObj) {
						hasValidValue = false;
						setErrorMessage(errorVOList, "Issuer Rep ID", issuerRepDTO.getId(), PlanMgmtValidatorUtil.EMSG_NOT_EXIST_IN_DB);
					}
				}

				if (hasValidValue) {
					List<IssuerIssuerRepresentativeMapping> issuerMappingRepList = representativeMappingRepository.findByIssuerId(issuer.getId());
					IssuerIssuerRepresentativeMapping issuerRepresentativeMapping = new IssuerIssuerRepresentativeMapping();
					issuerRepresentativeMapping.setIssuer(issuer);
					issuerRepresentativeMapping.setIssuerRepresentative(representativeObj);
					issuerRepresentativeMapping.setCreatedBy(Integer.parseInt(issuerRepDTO.getCreateBy()));
					issuerRepresentativeMapping.setLastUpdateBy(Integer.parseInt(issuerRepDTO.getLastUpdatedBy()));
	
					if (issuerMappingRepList.isEmpty()) {
						issuerRepresentativeMapping.setPrimaryContact(IssuerIssuerRepresentativeMapping.PrimaryContact.YES);
					}
					else {
						issuerRepresentativeMapping.setPrimaryContact(IssuerIssuerRepresentativeMapping.PrimaryContact.NO);
					}
					issuerRepresentativeMapping = representativeMappingRepository.save(issuerRepresentativeMapping);
					ghixResponseDTO.setId(issuerRepresentativeMapping.getId());
				}
			}
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred in assigning Issuer Representative Mapping.");
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.REPRESENTATIVE_DETAILS_EXCEPTION.code), "Exception occurred in assigning Issuer Representative Mapping: ",ex);
		}
		finally {
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.REPRESENTATIVE_DETAILS_EXCEPTION.getCode(), ghixResponseDTO);
		}
		return ghixResponseDTO;
	}

	/**
	 * Method is used to remove mapping of Issuer Representative with issuer.
	 */
	@Override
	@Transactional
	public GhixResponseDTO removeIssuerRepMapping(String issuerHiosId, Integer issuerRepId) {

		GhixResponseDTO ghixResponseDTO = new GhixResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		try {
			boolean hasValidValue = true;

			if (StringUtils.isBlank(issuerHiosId)) {
				setErrorMessage(errorVOList, "Issuer HIOS ID", null, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
				hasValidValue = false;
			}

			if (null == issuerRepId || 0 >= issuerRepId) {
				setErrorMessage(errorVOList, "Issuer Representative ID", null, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
				hasValidValue = false;
			}

			if (hasValidValue) {
				Issuer issuer = issuerService.getIssuerByHIOSId(issuerHiosId);

				if (null != issuer) {

					IssuerIssuerRepresentativeMapping iMapping = representativeMappingRepository.findByIssuerRepId(issuer.getId(), issuerRepId);

					if (null != iMapping) {
						ghixResponseDTO.setId(iMapping.getId());
						representativeMappingRepository.delete(iMapping);
					}
					else {
						setErrorMessage(errorVOList, "Issuer Representative Mapping object", null, PlanMgmtValidatorUtil.EMSG_NOT_EXIST_IN_DB);
					}
				}
				else {
					setErrorMessage(errorVOList, "Issuer HIOS ID", issuerHiosId, PlanMgmtValidatorUtil.EMSG_NOT_EXIST_IN_DB);
				}
			}
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred in removing Issuer Representative Mapping.");
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.REPRESENTATIVE_DETAILS_EXCEPTION.code), "Exception occurred in removing Issuer Representative Mapping: ",ex);
		}
		finally {
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.REPRESENTATIVE_DETAILS_EXCEPTION.getCode(), ghixResponseDTO);
		}
		return ghixResponseDTO;
	}
}
