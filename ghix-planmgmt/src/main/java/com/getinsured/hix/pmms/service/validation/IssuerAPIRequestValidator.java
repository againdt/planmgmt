package com.getinsured.hix.pmms.service.validation;

import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_CAPITAL_FORMAT;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_EMPTY;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_EMPTY_LIST;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_INVALID_VALUE;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_REQUEST;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_TEXT_LENGTH_NOT_MATCHED;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.PATTERN_ISSUER_BRAND_ID;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.PATTERN_ISSUER_BRAND_NAME;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.YES_CHAR;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.isValidPattern;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.setErrorMessage;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.setStatusResponse;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateAgainstAllowedValue;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateBlankData;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateEmailAddress;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateHiosIssuerId;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateHiosIssuerIdInDB;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateMaxStringLength;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateMinMaxStringLength;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateNumeric;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateURL;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerEnrollmentFlowRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerHistoryRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerPaymentInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.UploadFileDTO;
import com.getinsured.hix.model.Issuer.certification_status;
import com.getinsured.hix.model.IssuerD2c;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes.ErrorCode;
import com.getinsured.hix.pmms.repository.IIssuerBrandNameRepository;
import com.getinsured.hix.pmms.repository.IIssuerRepository;

/**
 * Class is used to validate Issuer API Requests.
 * @since January 4, 2017
 */
@Component
public final class IssuerAPIRequestValidator {

	private static final Logger LOGGER = Logger.getLogger(IssuerAPIRequestValidator.class);
	private static final String D2C_FLAG_VALUES = "y,n";
	private static String[] CERTIFICATION_STATUS_VALS = 
			{
				certification_status.CERTIFIED.name(), 
				certification_status.DECERTIFIED.name(), 
				certification_status.PENDING.name(), 
				certification_status.REGISTERED.name()
			}; 
	@Autowired private IIssuerRepository iIssuerRepository;
	@Autowired private IIssuerBrandNameRepository iIssuerBrandNameRepository;

	public IssuerAPIRequestValidator() {
	}

	/**
	 * Method is used to validate Issuer Enrollment Flow Request DTO before persistence.
	 * 
	 * @param enrollmentFlowDTO - IssuerEnrollmentFlowRequestDTO instance
	 * @param errorVOList - PlanMgmtErrorVO List instance
	 * @return IssuerEnrollmentResponseDTO
	 */
	public GhixResponseDTO validateEnrollmentFlowAPI(IssuerEnrollmentFlowRequestDTO enrollmentFlowDTO, List<PlanMgmtErrorVO> errorVOList) {

		LOGGER.debug("validateEnrollmentFlowAPI() Start");
		GhixResponseDTO ghixResponse = new GhixResponseDTO();

		try {

			if (null == enrollmentFlowDTO) {
				setErrorMessage(errorVOList, "IssuerEnrollmentFlowRequestDTO", null, EMSG_REQUEST);
				return ghixResponse;
			}

			if (CollectionUtils.isEmpty(enrollmentFlowDTO.getHiosIssuerIdList())) {
				setErrorMessage(errorVOList, "HIOS Issuer ID", null, EMSG_EMPTY_LIST);
			}
			boolean hasNotValidHIOSList = false;

			for (String hiosIssuerId : enrollmentFlowDTO.getHiosIssuerIdList()) {

				if (!validateHiosIssuerIdForAPI(hiosIssuerId, errorVOList, "HIOS Issuer ID")
						&& !hasNotValidHIOSList) {
					hasNotValidHIOSList = true;
				}
			}

			if (!hasNotValidHIOSList) {
				validateHiosIssuerIdInDB(enrollmentFlowDTO.getHiosIssuerIdList(), errorVOList, "HIOS Issuer ID", iIssuerRepository);
			}

			validateBlankData(enrollmentFlowDTO.getIsD2cEnabled(), errorVOList, "D2C Flag");
			validateAgainstAllowedValue(enrollmentFlowDTO.getIsD2cEnabled(), errorVOList, "D2C Flag", D2C_FLAG_VALUES);
			if (YES_CHAR.equalsIgnoreCase(enrollmentFlowDTO.getIsD2cEnabled())) {
				validateD2CFlowType(enrollmentFlowDTO.getD2cFlowType(), errorVOList, "D2C Flow Type");
			}
			validateNumeric(enrollmentFlowDTO.getPlanYear(), errorVOList, "Plan Year");
			validateNumeric(enrollmentFlowDTO.getCreateBy(), errorVOList, "Created By");
			validateNumeric(enrollmentFlowDTO.getLastUpdatedBy(), errorVOList, "Last Updated By");
		}
		catch (Exception ex) {
			setErrorMessage(errorVOList, null, null, ex.getMessage());
		}
		finally {

			if (!CollectionUtils.isEmpty(errorVOList)) {
				setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponse);
			}
			LOGGER.debug("validateEnrollmentFlowAPI() End");
		}
		return ghixResponse;
	}

	/**
	 * Method is used to validate Issuer Brand Name DTO before persistence.
	 * 
	 * @param brandNameDTO - IssuerBrandNameDTO instance
	 * @param errorVOList - PlanMgmtErrorVO List instance
	 * @return GhixResponseDTO
	 */
	public GhixResponseDTO validateIssuerBrandNameDTO(IssuerBrandNameDTO brandNameDTO, List<PlanMgmtErrorVO> errorVOList) {

		LOGGER.debug("validateIssuerBrandNameDTO() Start");
		GhixResponseDTO ghixResponse = new GhixResponseDTO();

		try {

			if (null == brandNameDTO) {
				setErrorMessage(errorVOList, "IssuerBrandNameDTO", null, EMSG_REQUEST);
				return ghixResponse;
			}

			if (validateMinMaxStringLength(brandNameDTO.getBrandId(), errorVOList, "Brand ID is required with max length of 8 chars", 1, 8)) {
				validateIssuerBrandID(brandNameDTO.getBrandId(), errorVOList, "Brand ID");
			}

			if (validateBlankData(brandNameDTO.getBrandName(), errorVOList, "Brand Name")) {
				validateIssuerBrandName(brandNameDTO.getBrandName(), errorVOList, "Brand Name");
			}

			if (validateURL(brandNameDTO.getBrandUrl(), errorVOList, "Brand URL")) {
				validateMaxStringLength(brandNameDTO.getBrandUrl(), errorVOList, "Brand URL, Max allowed 400 chars", 400);
			}
			validateNumeric(brandNameDTO.getCreateBy(), errorVOList, "Created By");
		}
		catch (Exception ex) {
			setErrorMessage(errorVOList, null, null, ex.getMessage());
		}
		finally {

			if (!CollectionUtils.isEmpty(errorVOList)) {
				setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponse);
			}
			LOGGER.debug("validateIssuerBrandNameDTO() End");
		}
		return ghixResponse;
	}

	/**
	 * Method is used to validate Issuer Enrollment Flow Request DTO before persistence.
	 */
	public boolean validateIssuerBrandNameRequestDTO(IssuerBrandNameRequestDTO brandNameRequestDTO, GhixResponseDTO ghixResponse, List<PlanMgmtErrorVO> errorVOList) {

		LOGGER.debug("validateIssuerBrandNameRequestDTO() Start");
		boolean valid = false;

		try {

			if (null == brandNameRequestDTO) {
				setErrorMessage(errorVOList, "IssuerBrandNameRequestDTO", null, EMSG_REQUEST);
				return valid;
			}

			if (CollectionUtils.isEmpty(brandNameRequestDTO.getHiosIssuerIdList())) {
				setErrorMessage(errorVOList, "HIOS Issuer ID", null, EMSG_EMPTY_LIST);
			}
			boolean hasNotValidHIOSList = false;

			for (String hiosIssuerId : brandNameRequestDTO.getHiosIssuerIdList()) {

				if (!validateHiosIssuerIdForAPI(hiosIssuerId, errorVOList, "HIOS Issuer ID")
						&& !hasNotValidHIOSList) {
					hasNotValidHIOSList = true;
				}
			}

			if (!hasNotValidHIOSList) {
				validateHiosIssuerIdInDB(brandNameRequestDTO.getHiosIssuerIdList(), errorVOList, "HIOS Issuer ID", iIssuerRepository);
			}

			if (null == brandNameRequestDTO.getId()) {
				setErrorMessage(errorVOList, "Internal ID", null, EMSG_EMPTY);
			}
			else if (0 > brandNameRequestDTO.getId()) {
				setErrorMessage(errorVOList, "Internal ID", brandNameRequestDTO.getId().toString(), EMSG_INVALID_VALUE);
			}
			validateNumeric(brandNameRequestDTO.getLastUpdatedBy(), errorVOList, "Last Updated By");
		}
		catch (Exception ex) {
			setErrorMessage(errorVOList, null, null, ex.getMessage());
		}
		finally {

			if (!CollectionUtils.isEmpty(errorVOList)) {
				setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponse);
			}
			else {
				valid = true;
			}
			LOGGER.debug("validateIssuerBrandNameRequestDTO() End with Valid Status: " + valid);
		}
		return valid;
	}

	/**
	 * Method is used to validate Issuer Brand ID.
	 */
	private boolean validateIssuerBrandID(String fieldValue, List<PlanMgmtErrorVO> errorVOList, String fieldName) {

		boolean valid = false;

		if (StringUtils.isNotBlank(fieldValue) && !isValidPattern(PATTERN_ISSUER_BRAND_ID, fieldValue)) {
			setErrorMessage(errorVOList, fieldName, fieldValue, EMSG_CAPITAL_FORMAT);
		}
		else {

			Integer countBrandID = iIssuerBrandNameRepository.countIssuerBrandID(fieldValue);
			if (null != countBrandID && 0 < countBrandID) {
				setErrorMessage(errorVOList, fieldName, fieldValue, "Duplicate Issuer Brand ID in database.");
			}
			else {
				valid = true;
			}
		}
		return valid;
	}

	/**
	 * Method is used to validate Issuer Brand Name.
	 */
	private boolean validateIssuerBrandName(String fieldValue, List<PlanMgmtErrorVO> errorVOList, String fieldName) {

		boolean valid = false;

		if (StringUtils.isNotBlank(fieldValue) && !isValidPattern(PATTERN_ISSUER_BRAND_NAME, fieldValue)) {
			setErrorMessage(errorVOList, fieldName, fieldValue, EMSG_INVALID_VALUE);
		}
		else {

			Integer countBrandName = iIssuerBrandNameRepository.countIssuerBrandName(fieldValue);
			if (null != countBrandName && 0 < countBrandName) {
				setErrorMessage(errorVOList, fieldName, fieldValue, "Duplicate Issuer Brand Name in database.");
			}
			else {
				valid = true;
			}
		}
		return valid;
	}

	public boolean validateIssuerHistoryRequest(IssuerHistoryRequestDTO issuerHistoryRequestDTO, GhixResponseDTO responseDTO) {
		boolean valid = false;
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		if(null == issuerHistoryRequestDTO) {
			setErrorMessage(errorVOList, null, null, EMSG_REQUEST);
		} else {
			valid = validateHiosIssuerIdForAPI(issuerHistoryRequestDTO.getHiosId(), errorVOList, "HIOS Issuer ID");
		}
		if(!valid) {
			setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), responseDTO);
		}
		return valid;
	}

	public boolean validateIssuerHiosId(String hiodId, List<PlanMgmtErrorVO> errorVOList, GhixResponseDTO responseDTO) {
		boolean valid = false;
		valid = validateHiosIssuerIdForAPI(hiodId, errorVOList, "HIOS Issuer ID");
		if(!valid) {
			setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), responseDTO);
		}
		return valid;
	}

	public boolean validateIssuerRepSearchRequest(IssuerRepSearchRequestDTO issuerRepSearchRequestDTO, GhixResponseDTO responseDTO) {
		boolean valid = false;
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		if(null == issuerRepSearchRequestDTO) {
			setErrorMessage(errorVOList, null, null, EMSG_REQUEST);
		} else {
			IssuerRepDTO issuerRepDTO = issuerRepSearchRequestDTO.getIssuerRepDTO();
			if(null == issuerRepDTO) {
				setErrorMessage(errorVOList, "issuerRepDTO", null, EMSG_REQUEST);
			} else {
				valid = validateHiosIssuerIdForAPI(issuerRepDTO.getHiosId(), errorVOList, "HIOS Issuer ID");
			}
		}
		if(!valid) {
			setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), responseDTO);
		}
		return valid;
	}

	public boolean validateIssuerPaymentInfo(IssuerPaymentInfoDTO issuerPaymentInfoDTO, GhixResponseDTO responseDTO) {
		boolean valid = false;
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		if(null == issuerPaymentInfoDTO) {
			setErrorMessage(errorVOList, null, null, EMSG_REQUEST);
		} else {
			valid = validateHiosIssuerIdForAPI(issuerPaymentInfoDTO.getHiosId(), errorVOList, "HIOS Issuer ID");
			valid = valid && (validateMaxStringLength(issuerPaymentInfoDTO.getKeystoreFileLocation(), errorVOList, "Keystore File Location, Max allowed 1020 chars", 1020)); 
			valid = valid && (validateMaxStringLength(issuerPaymentInfoDTO.getPrivateKeyName(), errorVOList, "Private Key Name, Max allowed 1020 chars", 1020)); 
			valid = valid && (validateMaxStringLength(issuerPaymentInfoDTO.getPassword(), errorVOList, "Password, Max allowed 100 chars", 100)); 
			valid = valid && (validateMaxStringLength(issuerPaymentInfoDTO.getPasswordSecuredKey(), errorVOList, "Password Secured Key, Max allowed 100 chars", 100)); 
			valid = valid && (validateMaxStringLength(issuerPaymentInfoDTO.getSecurityDnsName(), errorVOList, "Security DNS Name, Max allowed 1020 chars", 1020)); 
			valid = valid && (validateMaxStringLength(issuerPaymentInfoDTO.getSecurityAddress(), errorVOList, "Security Address, Max allowed 1020 chars", 1020)); 
			valid = valid && (validateMaxStringLength(issuerPaymentInfoDTO.getSecurityKeyInfo(), errorVOList, "Security Key Info, Max allowed 1020 chars", 1020)); 
			valid = valid && (validateMaxStringLength(issuerPaymentInfoDTO.getIssuerAuthUrl(), errorVOList, "Issuer Auth URL, Max allowed 1020 chars", 1020)); 
			valid = valid && (validateMaxStringLength(issuerPaymentInfoDTO.getSecurityCertName(), errorVOList, "Security Certificate Name, Max allowed 255 chars", 255)); 
			//valid = valid && (validateNumeric(issuerPaymentInfoDTO.getCreateBy(), errorVOList, "Created By"));
			valid = valid && (validateNumeric(issuerPaymentInfoDTO.getLastUpdatedBy(), errorVOList, "Last Updated By"));
		}
		if(!valid) {
			setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), responseDTO);
		}
		return valid;
	}

	/**
	 * Method is used to validate HIOS Issuer Id Value.
	 */
	private boolean validateHiosIssuerIdForAPI(String hiosIssuerId, List<PlanMgmtErrorVO> errorVOList, String fieldName) {
		boolean validHiosIssuerId = true;

		if (StringUtils.isBlank(hiosIssuerId)) {
			setErrorMessage(errorVOList, fieldName, hiosIssuerId, EMSG_EMPTY);
			validHiosIssuerId = false;
		}
		else if (!validateHiosIssuerId(hiosIssuerId)) {
			setErrorMessage(errorVOList, fieldName, hiosIssuerId, EMSG_TEXT_LENGTH_NOT_MATCHED);
			validHiosIssuerId = false;
		}
		return validHiosIssuerId;
	}

	/**
	 * Method is used to validate D2C Flow Type.
	 */
	private void validateD2CFlowType(String d2cFlowType, List<PlanMgmtErrorVO> errorVOList, String fieldName) {

		if (StringUtils.isBlank(d2cFlowType)) {
			setErrorMessage(errorVOList, fieldName, d2cFlowType, EMSG_EMPTY);
		}
		else {

			try {
				IssuerD2c.D2CFLOWTYPE.valueOf(d2cFlowType);
			}
			catch (IllegalArgumentException ex) {
				LOGGER.error(ex.getMessage());
				setErrorMessage(errorVOList, fieldName, d2cFlowType, EMSG_INVALID_VALUE);
			}
		}
	}

	public boolean validateIssuerRepresentativeInfo(IssuerRepDTO issuerRepDTO, GhixResponseDTO ghixResponseDTO) {
		boolean valid = false;
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		if(null == issuerRepDTO) {
			setErrorMessage(errorVOList, null, null, EMSG_REQUEST);
		} else {
			valid = validateHiosIssuerIdForAPI(issuerRepDTO.getHiosId(), errorVOList, "HIOS Issuer ID");
			valid = valid && (validateMinMaxStringLength(issuerRepDTO.getFirstName(), errorVOList, "First Name is required with max length of 50 chars", 1, 50));
			valid = valid && (validateMinMaxStringLength(issuerRepDTO.getLastName(), errorVOList, "Last Name is required with max length of 50 chars", 1, 50)); 
			valid = valid && (validateMaxStringLength(issuerRepDTO.getIssuerRepTitle(), errorVOList, "Title could have max length of 50 chars", 50)); 
			valid = valid && (validateMinMaxStringLength(issuerRepDTO.getPhone(), errorVOList, "Phone number should consist of 10 digits", 10, 10)); 
			valid = valid && (validateNumeric(issuerRepDTO.getPhone(), errorVOList, "Phone number should consist of 10 digits")); 
			valid = valid && (validateEmailAddress(issuerRepDTO.getEmail(), errorVOList, "Email ID is not valid")); 
			valid = valid && (validateNumeric(issuerRepDTO.getLastUpdatedBy(), errorVOList, "Last Updated By"));
		}
		if(!valid) {
			setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponseDTO);
		}
		return valid;
	}

	public boolean validateIssuerRepForMapping(List<PlanMgmtErrorVO> errorVOList, IssuerRepDTO issuerRepDTO, GhixResponseDTO ghixResponseDTO) {
		boolean valid = false;

		if (null == issuerRepDTO) {
			setErrorMessage(errorVOList, null, null, EMSG_REQUEST);
		}
		else {
			valid = validateHiosIssuerIdForAPI(issuerRepDTO.getHiosId(), errorVOList, "HIOS Issuer ID");
			valid = valid && (validateNumeric(issuerRepDTO.getId(), errorVOList, "ID"));
			valid = valid && (validateNumeric(issuerRepDTO.getCreateBy(), errorVOList, "Created By"));
			valid = valid && (validateNumeric(issuerRepDTO.getLastUpdatedBy(), errorVOList, "Last Updated By"));
		}

		if (!valid) {
			setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponseDTO);
		}
		return valid;
	}

	public boolean validateNewIssuerInfo(IssuerDTO issuerDTO, GhixResponseDTO ghixResponseDTO) {
		boolean valid = false;
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		if(null == issuerDTO || null == issuerDTO.getPrimaryContact() ) {
			setErrorMessage(errorVOList, null, null, EMSG_REQUEST);
		} else {
			valid = validateIssuerDetailsInfo(issuerDTO, errorVOList);
			valid = valid && (validateMinMaxStringLength(issuerDTO.getPrimaryContact().getFirstName(), errorVOList, "Primary Contact First Name is required with max length of 50 chars", 1, 50));
			valid = valid && (validateMinMaxStringLength(issuerDTO.getPrimaryContact().getLastName(), errorVOList, "Primary Contact Last Name is required with max length of 50 chars", 1, 50));
			valid = valid && (validateMaxStringLength(issuerDTO.getPrimaryContact().getIssuerRepTitle(), errorVOList, "Primary Contact Title could have max length of 15 chars", 15));
			valid = valid && (validateMinMaxStringLength(issuerDTO.getPrimaryContact().getPhone(), errorVOList, "Primary Contact Phone number should consist of 10 digits", 10, 10)); 
			valid = valid && (validateNumeric(issuerDTO.getPrimaryContact().getPhone(), errorVOList, "Primary Contact Phone number should consist of 10 digits")); 
			valid = valid && (validateEmailAddress(issuerDTO.getPrimaryContact().getEmail(), errorVOList, "Primary Contact Email ID is not valid")); 
			valid = valid && (validateNumeric(issuerDTO.getLastUpdatedBy(), errorVOList, "Last Updated By"));
		}
		if(!valid) {
			setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponseDTO);
		}
		return valid;
	}

	public boolean validateIssuerInfoForUpdate(String issuerId, IssuerDTO issuerDTO, GhixResponseDTO ghixResponseDTO) {
		boolean valid = false;
		boolean flagSet = false;
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		if(null == issuerId || !StringUtils.isNumeric(issuerId)) {
			setErrorMessage(errorVOList, "Issuer Id", issuerId, "Issuer ID is not valid.");
		} else if(null == issuerDTO) {
			setErrorMessage(errorVOList, null, null, EMSG_REQUEST);
		} else {
			valid = validateHiosIssuerIdForAPI(issuerDTO.getHiosIssuerId(), errorVOList, "HIOS Issuer ID");
			valid = valid && (validateNumeric(issuerDTO.getLastUpdatedBy(), errorVOList, "Last Updated By"));

			if(issuerDTO.isUpdateCertStatus()) {
				if(null != issuerDTO.getCertificationDoc()) {
					valid = valid && (validateMinMaxStringLength(issuerDTO.getCertificationDoc(), errorVOList, "Certification document path length should be less than 400 char.", 1, 400));
				}
				valid = valid && (validateMinMaxStringLength(issuerDTO.getCertificationStatus(), errorVOList, "Invalid certification status.", 1, 20));
				if(!ArrayUtils.contains(CERTIFICATION_STATUS_VALS, issuerDTO.getCertificationStatus())) {
					valid = false;
					setErrorMessage(errorVOList, "CertificationStatus", issuerDTO.getCertificationStatus(), "Invalid certification status.");
				}
				if(null != issuerDTO.getCommentId() && issuerDTO.getCommentId() < 1) {
					valid = false;
					setErrorMessage(errorVOList, "Comment ID", String.valueOf(issuerDTO.getCommentId()), "Invalid comment id.");
				}
				flagSet = true;
				//COMMENT_ID NUMBER              
			} 
			if (issuerDTO.isUpdateIndvMktProfile()) {
				valid = valid && validateIssuerIndvMarketProfileInfoForUpdate(issuerDTO, errorVOList);
				flagSet = true;
			} 
			if (issuerDTO.isUpdateCompMktProfile()) {
				valid = valid && validateIssuerCompanyProfileInfoForUpdate(issuerDTO, errorVOList);
				flagSet = true;
			} 
			if (issuerDTO.isUpdateIssuerDetails()) {
				valid = valid && validateIssuerDetailsInfo(issuerDTO, errorVOList);
				flagSet = true;
			} 
			if(!flagSet)
			{
				valid = false;
				setErrorMessage(errorVOList, null, null, "No flag set to select group of information to update.");
			}
		}
		if(!valid) {
			setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponseDTO);
		}
		return valid;
	}


	private boolean validateIssuerIndvMarketProfileInfoForUpdate(IssuerDTO issuerDTO, List<PlanMgmtErrorVO> errorVOList) {
		boolean valid = true;
		//indvMktCustServicePhoneNumber;			INDV_CUST_SERVICE_PHONE                VARCHAR2(1024)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktCustServicePhoneNumber()) || StringUtils.isNotBlank(issuerDTO.getIndvMktCustServicePhoneNumberExt())) {
			boolean validPhone = validateMinMaxStringLength(issuerDTO.getIndvMktCustServicePhoneNumber(), errorVOList, "Customer Service Phone should consist of 10 digits", 10, 10); 
			if(validPhone) {
				validPhone = validateNumeric(issuerDTO.getIndvMktCustServicePhoneNumber(), errorVOList, "Customer Service Phone number should consist of 10 digits");
			}
			valid = valid && validPhone;
			
			//indvMktCustServicePhoneNumberExt;		INDV_CUST_SERVICE_PHONE_EXT            VARCHAR2(1024)   
			if(StringUtils.isNotBlank(issuerDTO.getIndvMktCustServicePhoneNumberExt())){
				validPhone = validateMaxStringLength(issuerDTO.getIndvMktCustServicePhoneNumberExt(), errorVOList, "Customer Service Phone extension could have max length of 3 digits", 3);
				if(validPhone) {
					validPhone = validateNumeric(issuerDTO.getIndvMktCustServicePhoneNumberExt(), errorVOList, "Customer Service Phone number extension should consist of max 3 digits");
				}
				valid = valid && validPhone;
			}	
		}
		//indvMktCustServiceTollFreePhoneNumber;		INDV_CUST_SERVICE_TOLL_FREE            VARCHAR2(1024)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktCustServiceTollFreePhoneNumber())) {
			boolean validPhone = validateMinMaxStringLength(issuerDTO.getIndvMktCustServiceTollFreePhoneNumber(), errorVOList, "Customer Service Toll Free Phone should consist of 10 digits", 10, 10); 
			if(validPhone) {
				validPhone = validateNumeric(issuerDTO.getIndvMktCustServiceTollFreePhoneNumber(), errorVOList, "Customer Service Toll Free Phone number should consist of 10 digits");
			}
			valid = valid && validPhone;
		}
		//indvMktCustServiceTTY;				INDV_CUST_SERVICE_TTY                  VARCHAR2(1024)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktCustServiceTTY())) {
			boolean validPhone = validateMinMaxStringLength(issuerDTO.getIndvMktCustServiceTTY(), errorVOList, "Customer Service TTY should consist of 10 digits", 10, 10); 
			if(validPhone) {
				validPhone = validateNumeric(issuerDTO.getIndvMktCustServiceTTY(), errorVOList, "Customer Service TTY number should consist of 10 digits");
			}
			valid = valid && validPhone;
		}
		//indvMktCustWebsiteUrl;				INDV_SITE_URL                          VARCHAR2(1000)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktCustWebsiteUrl())) {
			boolean validUrl = validateMaxStringLength(issuerDTO.getIndvMktCustWebsiteUrl(), errorVOList, "Customer Website Address could have max length of 1000 chars", 1000);
			if(validUrl) {
				validUrl = validateURL(issuerDTO.getIndvMktCustWebsiteUrl(), errorVOList, "Customer Website Address is not a valid URL");
			} 
			valid = valid && validUrl;
		}
		//indvMktAppStatusDeptPhoneNumber;			APP_STATUS_DEPT_PHONE_NUMBER           VARCHAR2(25) 
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktAppStatusDeptPhoneNumber())) {
			boolean validPhone = validateMinMaxStringLength(issuerDTO.getIndvMktAppStatusDeptPhoneNumber(), errorVOList, "Application Status Department Phone Number should consist of 10 digits", 10, 10); 
			if(validPhone) {
				validPhone = validateNumeric(issuerDTO.getIndvMktAppStatusDeptPhoneNumber(), errorVOList, "Application Status Department Phone Number should consist of 10 digits");
			}
			valid = valid && validPhone;
		}
		//indvMktCommContactName;				COMMISSIONS_CONTACT_NAME               VARCHAR2(400)
		valid = valid && (validateMaxStringLength(issuerDTO.getIndvMktCommContactName(), errorVOList, "Contact Name could have max length of 100 chars", 100));
		
		//indvMktCommPhoneNumber;				COMMISSIONS_PHONE_NUMBER               VARCHAR2(25)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktCommPhoneNumber()) || StringUtils.isNotBlank(issuerDTO.getIndvMktCommPhoneNumberExt())) {
			boolean validPhone = validateMinMaxStringLength(issuerDTO.getIndvMktCommPhoneNumber(), errorVOList, "Commissions Phone should consist of 10 digits", 10, 10); 
			if(validPhone) {
				validPhone = validateNumeric(issuerDTO.getIndvMktCommPhoneNumber(), errorVOList, "Commissions Phone number should consist of 10 digits");
			}
			valid = valid && validPhone;
			
			if(StringUtils.isNotBlank(issuerDTO.getIndvMktCommPhoneNumberExt())){
				//indvMktCommPhoneNumberExt;			COMMISSIONS_PHONE_NUMBER_EXT           VARCHAR2(15)     
				validPhone = validateMaxStringLength(issuerDTO.getIndvMktCommPhoneNumberExt(), errorVOList, "Commissions Phone number extension could have max length of 11 digits", 11);
				if(validPhone) {
					validPhone = validateNumeric(issuerDTO.getIndvMktCommPhoneNumberExt(), errorVOList, "Commissions Phone number extension should consist of max 11 digits");
				}
				valid = valid && validPhone;
			}	
		}
		//indvMktCommEmail;				COMMISSIONS_EMAIL_ID                   VARCHAR2(100)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktCommEmail())) {
			boolean validEmail = validateMaxStringLength(issuerDTO.getIndvMktCommEmail(), errorVOList, "Commission Email ID could have max length of 100 chars", 100);
			if(validEmail) {
				validEmail = validateEmailAddress(issuerDTO.getIndvMktCommEmail(), errorVOList, "Commission Email ID is not valid");
			} 
			valid = valid && validEmail;
		}
		//indvMktCommPortalUrl;				COMMISSION_PORTAL_URL                  VARCHAR2(1000)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktCommPortalUrl()) || StringUtils.isNotBlank(issuerDTO.getIndvMktCommPortalPwd()) 
				|| StringUtils.isNotBlank(issuerDTO.getIndvMktCommPortalUname())) {
			boolean validUrl = validateMinMaxStringLength(issuerDTO.getIndvMktCommPortalUrl(), errorVOList, "Commission Portal URL could have max length of 200 chars", 1, 200);
			if(validUrl) {
				validUrl = validateURL(issuerDTO.getIndvMktCommPortalUrl(), errorVOList, "Commission Portal URL is not valid");
			} 
			valid = valid && validUrl;
			//indvMktCommPortalUname;				COMMISSION_PORTAL_USERNAME             VARCHAR2(200)
			valid = valid && (validateMinMaxStringLength(issuerDTO.getIndvMktCommPortalUname(), errorVOList, "Commission Portal User Name must be specified with max length of 100 chars", 1, 100));
			//indvMktCommPortalPwd;				COMMISSION_PORTAL_PASSWORD             VARCHAR2(500)
			valid = valid && (validateMinMaxStringLength(issuerDTO.getIndvMktCommPortalPwd(), errorVOList, "Commission Portal Password must be specified with max length of 100 chars", 1, 100));
		}
		//indvMktPcdPhoneNumber;				PCD_PHONE_NUMBER                       VARCHAR2(25) 
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktPcdPhoneNumber()) || StringUtils.isNotBlank(issuerDTO.getIndvMktPcdPhoneNumberExt())) {
			boolean validPhone = validateMinMaxStringLength(issuerDTO.getIndvMktPcdPhoneNumber(), errorVOList, "Policy Cancellation Department Phone number should consist of 10 digits", 10, 10); 
			if(validPhone) {
				validPhone = validateNumeric(issuerDTO.getIndvMktPcdPhoneNumber(), errorVOList, "Policy Cancellation Department Commissions Phone number should consist of 10 digits");
			}
			valid = valid && validPhone;
			if(StringUtils.isNotBlank(issuerDTO.getIndvMktPcdPhoneNumberExt())){
				//indvMktPcdPhoneNumberExt;			PCD_PHONE_NUMBER_EXT                   VARCHAR2(15)     
				validPhone = validateMaxStringLength(issuerDTO.getIndvMktPcdPhoneNumberExt(), errorVOList, "Policy Cancellation Department Phone number extension could have max length of 11 digits", 11);
				if(validPhone) {
					validPhone = validateNumeric(issuerDTO.getIndvMktPcdPhoneNumberExt(), errorVOList, "Policy Cancellation Department Phone number extension should consist of max 11 digits");
				}
				valid = valid && validPhone;
			}	
		}
		//indvMktPcdFAX;					PCD_FAX_NUMBER                         VARCHAR2(25)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktPcdFAX())) {
			boolean validPhone = validateMinMaxStringLength(issuerDTO.getIndvMktPcdFAX(), errorVOList, "Policy Cancellation Department FAX should consist of 10 digits", 10, 10); 
			if(validPhone) {
				validPhone = validateNumeric(issuerDTO.getIndvMktPcdFAX(), errorVOList, "Policy Cancellation Department FAX should consist of 10 digits");
			}
			valid = valid && validPhone;
		}
		//indvMktPcdEmail;					PCD_EMAIL_ID                           VARCHAR2(100)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktPcdEmail())) {
			boolean validEmail = validateMaxStringLength(issuerDTO.getIndvMktPcdEmail(), errorVOList, "Policy Cancellation Department Email ID could have max length of 50 chars", 50);
			if(validEmail) {
				validEmail = validateEmailAddress(issuerDTO.getIndvMktPcdEmail(), errorVOList, "Policy Cancellation Department Email ID is not valid");
			} 
			valid = valid && validEmail;
		}
		//indvMktPcdLocation;				PCD_LOCATION_ID                        NUMBER
		
		//indvMktBrokerName;			BROKER_CONTACT_NAME                    VARCHAR2(400)	[100] 
		valid = valid && (validateMaxStringLength(issuerDTO.getIndvMktBrokerName(), errorVOList, "Broker Contact Name could have max length of 100 chars", 100));
		
		//indvMktBrokerPhoneNumber;		BROKER_PHONE_NUMBER                    VARCHAR2(25)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktBrokerPhoneNumber()) || StringUtils.isNotBlank(issuerDTO.getIndvMktBrokerPhoneNumberExt()) ) {
			boolean validPhone = validateMinMaxStringLength(issuerDTO.getIndvMktBrokerPhoneNumber(), errorVOList, "Broker Phone number should consist of 10 digits", 10, 10); 
			if(validPhone) {
				validPhone = validateNumeric(issuerDTO.getIndvMktBrokerPhoneNumber(), errorVOList, "Broker Phone number should consist of 10 digits");
			}
			valid = valid && validPhone;
			if(StringUtils.isNotBlank(issuerDTO.getIndvMktBrokerPhoneNumberExt())){
				//indvMktBrokerPhoneNumberExt;		BROKER_PHONE_NUMBER_EXT                VARCHAR2(15)    
				validPhone = validateMaxStringLength(issuerDTO.getIndvMktBrokerPhoneNumberExt(), errorVOList, "Broker Phone number extension could have max length of 11 digits", 11);
				if(validPhone) {
					validPhone = validateNumeric(issuerDTO.getIndvMktBrokerPhoneNumberExt(), errorVOList, "Broker Phone number extension should consist of max 11 digits");
				}
				valid = valid && validPhone;
			}	
		}
		//indvMktBrokerEmailId;			BROKER_EMAIL_ID                        VARCHAR2(100) 
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktBrokerEmailId())) {
			boolean validEmail = validateMaxStringLength(issuerDTO.getIndvMktBrokerEmailId(), errorVOList, "Broker Email ID could have max length of 100 chars", 100);
			if(validEmail) {
				validEmail = validateEmailAddress(issuerDTO.getIndvMktBrokerEmailId(), errorVOList, "Broker Email ID is not valid");
			} 
			valid = valid && validEmail;
		}
		//indvMktAgencyRepoPortalURL;		AGENCY_REPO_PORTAL_URL                 VARCHAR2(1000)
		if(StringUtils.isNotBlank(issuerDTO.getIndvMktAgencyRepoPortalURL()) || StringUtils.isNotBlank(issuerDTO.getIndvMktBrokerReportingPortalPwd())
				|| StringUtils.isNotBlank(issuerDTO.getIndvMktBrokerReportingPortalUname())) {
			boolean validUrl = validateMinMaxStringLength(issuerDTO.getIndvMktAgencyRepoPortalURL(), errorVOList, "Agency Reporting Portal URL could have max length of 200 chars", 1, 200);
			if(validUrl) {
				validUrl = validateURL(issuerDTO.getIndvMktAgencyRepoPortalURL(), errorVOList, "Agency Reporting Portal URL is not valid");
			} 
			valid = valid && validUrl;
			//indvMktBrokerReportingPortalUname;	AGENCY_REPO_PORTAL_USERNAME            VARCHAR2(200)
			valid = valid && (validateMinMaxStringLength(issuerDTO.getIndvMktBrokerReportingPortalUname(), errorVOList, "Agency Reporting Portal User Name must be specified with max length of 100 chars", 1, 100));
			//indvMktBrokerReportingPortalPwd;		AGENCY_REPO_PORTAL_PASSWORD            VARCHAR2(500)
			valid = valid && (validateMinMaxStringLength(issuerDTO.getIndvMktBrokerReportingPortalPwd(), errorVOList, "Agency Reporting Portal Password must be specified with max length of 100 chars", 1, 100));
		}
		return valid;
	}
	
	private boolean validateIssuerDetailsInfo(IssuerDTO issuerDTO, List<PlanMgmtErrorVO> errorVOList) {
		boolean valid = true;
		//hiosIssuerId;		HIOS_ISSUER_ID                         VARCHAR2(10)
		valid = validateHiosIssuerIdForAPI(issuerDTO.getHiosIssuerId(), errorVOList, "HIOS Issuer ID");
		//name;			NAME                                   VARCHAR2(400)
		valid = valid && (validateMinMaxStringLength(issuerDTO.getName(), errorVOList, "Issuer Name is required with max length of 50 chars", 1, 50));
		//shortName;		SHORT_NAME                             VARCHAR2(100)
		valid = valid && (validateMaxStringLength(issuerDTO.getShortName(), errorVOList, "Short Name could have max length of 25 chars", 25));
		//naicCompanyCode;		NAIC_COMPANY_CODE                      VARCHAR2(25)
		if(StringUtils.isNotBlank(issuerDTO.getNaicCompanyCode())) {
			valid = valid && (validateMaxStringLength(issuerDTO.getNaicCompanyCode(), errorVOList, "NAIC Company Code could have max length of 10", 10));
			valid = valid && (validateNumeric(issuerDTO.getNaicCompanyCode(), errorVOList, "NAIC Company Code should have numeric value"));
		}
		//naicGroupCode;		NAIC_GROUP_CODE                        VARCHAR2(25)
		if(StringUtils.isNotBlank(issuerDTO.getNaicGroupCode())) {
			valid = valid && (validateMaxStringLength(issuerDTO.getNaicGroupCode(), errorVOList, "NAIC Group Code could have max length of 10", 10));
			valid = valid && (validateNumeric(issuerDTO.getNaicGroupCode(), errorVOList, "NAIC Group Code should have numeric value"));
		}
		//federalEmployeeId;	FEDERAL_EIN                            VARCHAR2(15)
		if(StringUtils.isNotBlank(issuerDTO.getFederalEmployeeId())) {
			valid = valid && (validateMinMaxStringLength(issuerDTO.getFederalEmployeeId(), errorVOList, "Federal Employer ID should have length of 9", 9, 9));
			valid = valid && (validateNumeric(issuerDTO.getFederalEmployeeId(), errorVOList, "Federal Employer ID should have numeric value"));
		}
		//streetAddress1;		ADDRESS_LINE1                          VARCHAR2(800)
		valid = valid && (validateMinMaxStringLength(issuerDTO.getStreetAddress1(), errorVOList, "Street Address 1 is required with max length of 25 chars", 1, 25));
		//streetAddress2;		ADDRESS_LINE2                          VARCHAR2(200)
		valid = valid && (validateMaxStringLength(issuerDTO.getStreetAddress2(), errorVOList, "Street Address 2 could have max length of 25 chars", 25));
		//city; 			CITY                                   VARCHAR2(120)
		valid = valid && (validateMinMaxStringLength(issuerDTO.getCity(), errorVOList, "City is required with max lengthof 30 chars", 1, 30));
		//state;			STATE                                  VARCHAR2(8)
		valid = valid && (validateMinMaxStringLength(issuerDTO.getState(), errorVOList, "State code is required with length of 2 chars", 2, 2));
		//zip;			ZIP                                    VARCHAR2(20)
		valid = valid && (validateMinMaxStringLength(issuerDTO.getZip(), errorVOList, "Zip code is required with length of 5 chars", 5, 5));
		valid = valid && (validateNumeric(issuerDTO.getZip(), errorVOList, "Zip Code should have numeric value"));
		return valid;
	}
	
	private boolean validateIssuerCompanyProfileInfoForUpdate(IssuerDTO issuerDTO, List<PlanMgmtErrorVO> errorVOList) {
		boolean valid = true;
		//companyLegalName;	COMPANY_LEGAL_NAME                     VARCHAR2(400)
		valid = valid && (validateMaxStringLength(issuerDTO.getCompanyLegalName(), errorVOList, "Company Legal Name could have max length of 400 chars", 400));
		//companyLogo;		COMPANY_LOGO                           VARCHAR2(400)
		valid = valid && (validateMaxStringLength(issuerDTO.getCompanyLogo(), errorVOList, "Company Logo path could have max length of 400 chars", 400));
		//issuerState;		STATE_OF_DOMICILE                      VARCHAR2(2)    
		valid = valid && (validateMaxStringLength(issuerDTO.getIssuerState(), errorVOList, "Issuer State code could have max length of 2 chars", 2));
		//brokerId;		BROKER_ID                              VARCHAR2(20 CHAR)    
		valid = valid && (validateMaxStringLength(issuerDTO.getBrokerId(), errorVOList, "Agency of Record/Broker ID could have max length of 20 chars", 20));
		//writingAgent;		WRITING_AGENT                          VARCHAR2(20 CHAR)    
		valid = valid && (validateMaxStringLength(issuerDTO.getWritingAgent(), errorVOList, "Writing Agent could have max length of 20 chars", 20));
		//nationalProducerNum;	NATIONAL_PRODUCER_NUMBER               VARCHAR2(25)    
		valid = valid && (validateMaxStringLength(issuerDTO.getNationalProducerNum(), errorVOList, "National Producer Number could have max length of 25 chars", 25));
		//agentFirstName;		AGENT_FIRST_NAME                       VARCHAR2(20 CHAR)    
		valid = valid && (validateMaxStringLength(issuerDTO.getAgentFirstName(), errorVOList, "Agent/Broker First Name could have max length of 20 chars", 20));
		//agentLastName;		AGENT_LAST_NAME                        VARCHAR2(20 CHAR)    
		valid = valid && (validateMaxStringLength(issuerDTO.getAgentLastName(), errorVOList, "Agent/Broker Last Name could have max length of 20 chars", 20));
		//agentPhoneNumber;	BROKER_PHONE                           VARCHAR2(20 CHAR)
		if(null != issuerDTO.getAgentPhoneNumber()) {
			boolean validPhone = validateMinMaxStringLength(issuerDTO.getAgentPhoneNumber(), errorVOList, "Agent/Broker Phonenumber should consist of 10 digits", 10, 10); 
			if(validPhone) {
				validPhone = validateNumeric(issuerDTO.getAgentPhoneNumber(), errorVOList, "Agent/Broker Phone number should consist of 10 digits");
			}
			valid = valid && validPhone;
		}
		//agentFaxNumber;		BROKER_FAX                             VARCHAR2(20 CHAR) 
		valid = valid && (validateMaxStringLength(issuerDTO.getAgentFaxNumber(), errorVOList, "Agent/Broker Fax could have max length of 20 chars", 20));
		//agentEmail;		BROKER_EMAIL                           VARCHAR2(50 CHAR)
		if(null != issuerDTO.getAgentEmail()) {
			boolean validEmail = validateMaxStringLength(issuerDTO.getAgentEmail(), errorVOList, "Agent/Broker Email ID could have max length of 50 chars", 50);
			if(validEmail) {
				validEmail = validateEmailAddress(issuerDTO.getAgentEmail(), errorVOList, "Agent/Broker Email ID is not valid");
			} 
			valid = valid && validEmail;
		}

		// validate issuer logo
		if(null != issuerDTO.getUploadCompanyLogo()) {
			boolean validLogo = validateLogo(issuerDTO.getUploadCompanyLogo(), errorVOList, "Issuer Logo");
			valid = valid && validLogo;
		}
		
		//paymentUrl;		PAYMENT_URL                            VARCHAR2(1020 CHAR)
		if(null != issuerDTO.getPaymentUrl()) {
			boolean validUrl = validateMaxStringLength(issuerDTO.getPaymentUrl(), errorVOList, "Payment URL could have max length of 1000 chars", 1000);
			if(validUrl) {
				validUrl = validateURL(issuerDTO.getPaymentUrl(), errorVOList, "Payment URL is not valid");
			} 
			valid = valid && validUrl;
		}
		//producerPortalUrl;	PRODUCER_URL                           VARCHAR2(1000)
		if(null != issuerDTO.getProducerPortalUrl() || null != issuerDTO.getProducerPortalPwd() || null != issuerDTO.getProducerPortalUname()) {
			boolean validUrl = validateMinMaxStringLength(issuerDTO.getProducerPortalUrl(), errorVOList, "Producer Portal URL could have max length of 1000 chars", 1, 1000);
			if(validUrl) {
				validUrl = validateURL(issuerDTO.getProducerPortalUrl(), errorVOList, "Producer Portal URL is not valid");
			} 
			valid = valid && validUrl;
			//producerPortalUname;	PRODUCER_UNAME                         VARCHAR2(1020)
			valid = valid && (validateMinMaxStringLength(issuerDTO.getProducerPortalUname(), errorVOList, "Producer Portal User Name must be specified with max length of 200 chars", 1, 200));
			//producerPortalPwd;	PRODUCER_PASSWORD                      VARCHAR2(4000)
			valid = valid && (validateMinMaxStringLength(issuerDTO.getProducerPortalPwd(), errorVOList, "Producer Portal Password must be specified with max length of 4000 chars", 1, 4000));
		}
		//carrierApplicationUrl;	APPLICATION_URL                        VARCHAR2(1000)
		if(null != issuerDTO.getCarrierApplicationUrl()) {
			boolean validUrl = validateMaxStringLength(issuerDTO.getCarrierApplicationUrl(), errorVOList, "Carrier Application URL could have max length of 1000 chars", 1000);
			if(validUrl) {
				validUrl = validateURL(issuerDTO.getCarrierApplicationUrl(), errorVOList, "Carrier Application URL is not valid");
			} 
			valid = valid && validUrl;
		}				
		//isLoginInRequired;	LOGIN_REQUIRED                         VARCHAR2(3)
		if(null != issuerDTO.getIsLoginInRequired()) {
			valid = valid && (validateAgainstAllowedValue(issuerDTO.getIsLoginInRequired(), errorVOList, "IsLoginInRequired", "YES,NO"));
		}
		//onExchangeDisclaimer;	ON_EXCHANGE_DISCLAIMER                 VARCHAR2(4000)    
		valid = valid && (validateMaxStringLength(issuerDTO.getOnExchangeDisclaimer(), errorVOList, "ON Exchange Disclaimers could have max length of 2000 chars", 2000));
		//offExchangeDisclaimer;	OFF_EXCHANGE_DISCLAIMER                VARCHAR2(4000)
		valid = valid && (validateMaxStringLength(issuerDTO.getOffExchangeDisclaimer(), errorVOList, "OFF Exchange Disclaimers could have max length of 2000 chars", 2000));
		//companyAddress1;		COMPANY_ADDRESS_LINE1                  VARCHAR2(200)    
		valid = valid && (validateMaxStringLength(issuerDTO.getCompanyAddress1(), errorVOList, "Company - Address line 1 could have max length of 200 chars", 200));
		//companyAddress2;		COMPANY_ADDRESS_LINE2                  VARCHAR2(200)           
		valid = valid && (validateMaxStringLength(issuerDTO.getCompanyAddress2(), errorVOList, "Company - Address line 2 could have max length of 200 chars", 200));
		//companyCity;		COMPANY_CITY                           VARCHAR2(30)
		valid = valid && (validateMaxStringLength(issuerDTO.getCompanyCity(), errorVOList, "Company City could have max length of 30 chars", 30));
		//companyState;		COMPANY_STATE                          VARCHAR2(2)
		valid = valid && (validateMaxStringLength(issuerDTO.getCompanyState(), errorVOList, "Company State Code could have max length of 2 chars", 2));
		//companyZip;		COMPANY_ZIP                            VARCHAR2(9)  
		if(null != issuerDTO.getCompanyZip()) {
			boolean validZip = validateMinMaxStringLength(issuerDTO.getCompanyZip(), errorVOList, "Company Zip code is required with length of 5 chars", 5, 5); 
			if(validZip) {
				validZip = validateNumeric(issuerDTO.getCompanyZip(), errorVOList, "Company Zip Code should have numeric value");
			}
			valid = valid && validZip; 
		}
		//websiteUrl;		SITE_URL                               VARCHAR2(1000)
		if(null != issuerDTO.getWebsiteUrl()) {
			boolean validUrl = validateMaxStringLength(issuerDTO.getWebsiteUrl(), errorVOList, "Issuer Website Address could have max length of 1000 chars", 1000);
			if(validUrl) {
				validUrl = validateURL(issuerDTO.getWebsiteUrl(), errorVOList, "Issuer Website Address is not a valid URL");
			} 
			valid = valid && validUrl;
		}				
		//customerWebsiteUrl;	COMPANY_SITE_URL                       VARCHAR2(1000)
		if(null != issuerDTO.getCustomerWebsiteUrl()) {
			boolean validUrl = validateMaxStringLength(issuerDTO.getCustomerWebsiteUrl(), errorVOList, "Customer Website Address could have max length of 1000 chars", 1000);
			if(validUrl) {
				validUrl = validateURL(issuerDTO.getCustomerWebsiteUrl(), errorVOList, "Customer Website Address is not a valid URL");
			} 
			valid = valid && validUrl;
		}
		return valid;
	}
	
	
	public static boolean validateLogo(UploadFileDTO uploadCompanyLogo, List<PlanMgmtErrorVO> errorVOList, String fieldName) {
		boolean valid = false;

		if (null == uploadCompanyLogo 
				|| null == uploadCompanyLogo.getFileSize()
				|| null == uploadCompanyLogo.getFileContent()
				|| null == uploadCompanyLogo.getOriginalFileName()) {
			setErrorMessage(errorVOList, fieldName, null, EMSG_INVALID_VALUE);
		}
		else{
			valid = true;
		}
		return valid;
	}
	
}
