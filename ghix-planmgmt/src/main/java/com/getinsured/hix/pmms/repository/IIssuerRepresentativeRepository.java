package com.getinsured.hix.pmms.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.IssuerRepresentative;

@Repository("iIssuerRepresentativeRepository")
public interface IIssuerRepresentativeRepository extends JpaRepository<IssuerRepresentative, Integer> {
	IssuerRepresentative findById(Integer repId);

	IssuerRepresentative findByIdAndIssuerId(Integer repId, Integer issuerId);

	@Query("SELECT ir FROM IssuerRepresentative ir WHERE LOWER(ir.email) like LOWER(:email)")
	IssuerRepresentative findByEmail(@Param("email") String email);

	@Query("SELECT ir FROM IssuerRepresentative ir WHERE userRecord.id = (:userId)")
	IssuerRepresentative findByUserId(@Param("userId") Integer userId);
}
