package com.getinsured.hix.pmms.service.jpa;

import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_EMPTY;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_INVALID_VALUE;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateState;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.microservice.DropDownListResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.EnrollmentFlowDTO;
import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerEnrollmentFlowRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerEnrollmentResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerHistoryDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerHistoryRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerHistoryResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerLightDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerRepSearchResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerSearchResponseDTO;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Issuer.certification_status;
import com.getinsured.hix.model.IssuerBrandName;
import com.getinsured.hix.model.IssuerD2c;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.planmgmt.util.IssuerLogoUtil;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes.ErrorCode;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;
import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.repository.IIssuerD2CRepository;
import com.getinsured.hix.pmms.repository.IIssuerRepository;
import com.getinsured.hix.pmms.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.pmms.service.IssuerBrandNameService;
import com.getinsured.hix.pmms.service.IssuerHistoryServiceHelper;
import com.getinsured.hix.pmms.service.IssuerRepresentativeService;
import com.getinsured.hix.pmms.service.IssuerResponseDTOHelperMS;
import com.getinsured.hix.pmms.service.IssuerService;
import com.getinsured.hix.pmms.service.IssuerServiceHelper;
import com.getinsured.hix.pmms.service.validation.IssuerAPIRequestValidator;
import com.getinsured.hix.pmms.service.validation.PlanMgmtErrorVO;
import com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil;

/**
 * Class is used to provide service to Issuer Service APIs [Plan Management-Microservice]
 * @since Jan 10, 2017
 */
@Service("pmmsIssuerService")
public class IssuerServiceImpl  implements IssuerService {

	private static final Logger LOGGER = Logger.getLogger(IssuerServiceImpl.class);
	private static final String TIMESTAMP_PATTERN = "MMM dd, yyyy";
	
	

	@Autowired private ObjectFactory<DisplayAuditService> objectFactory;
	@Autowired 
	@Qualifier("issuerHistoryServiceHelper")
	private HistoryRendererService issuerHistoryRendererImpl;
	@Autowired private com.getinsured.hix.planmgmt.service.IssuerService issuerService;
	@Autowired private IIssuerD2CRepository iIssuerD2CRepository;
	@Autowired private IIssuerRepository iIssuerRepository;
	@Autowired private IIssuerRepresentativeRepository iIssuerRepresentativeRepository;
	@Autowired private GIExceptionHandler giExceptionHandler;
	@Autowired private IssuerAPIRequestValidator issuerAPIRequestValidator;
	@Autowired private IssuerRepresentativeService pmmsIssuerRepresentativeService;
	@Autowired private IssuerBrandNameService pmmsIssuerBrandNameService;
	@Autowired private IssuerServiceHelper issuerServiceHelper;
	@Autowired private IssuerLogoUtil issuerLogoUtil; 
	@Autowired private PlanMgmtService planMgmtService;

	@PersistenceUnit private EntityManagerFactory emf;
	String CERT_STATUS_CERTIFIED = certification_status.CERTIFIED.toString();

	public IssuerHistoryResponseDTO getIssuerHistory(IssuerHistoryRequestDTO issuerHistoryRequestDTO) {
		IssuerHistoryResponseDTO issuerHistoryResponseDTO = new IssuerHistoryResponseDTO();
		if(issuerAPIRequestValidator.validateIssuerHistoryRequest(issuerHistoryRequestDTO, issuerHistoryResponseDTO)) {
			Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
			List<String> compareCols = new ArrayList<String>();
			List<Integer> displayCols = new ArrayList<Integer>();
			List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

			setDataAndCompareColsForIssuerHistory(requiredFieldsMap, compareCols, displayCols);

			try {
				List<Map<String, Object>> data = null;
				Issuer issuer = issuerService.getIssuerByHIOSId(issuerHistoryRequestDTO.getHiosId());
				DisplayAuditService service = objectFactory.getObject();
				if(null == issuer) {
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("Failed to get Issuer for HIOS ID: " + issuerHistoryRequestDTO.getHiosId());
					}
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Issuer HIOS_ID", issuerHistoryRequestDTO.getHiosId(), "Failed to get Issuer record.");
				} else if(null == service) {
					LOGGER.info("Internal error getting DisplayAuditService object");
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Internal error getting service object");
				} else {
					issuerHistoryResponseDTO.setHiosIssuerId(issuer.getHiosIssuerId());
					issuerHistoryResponseDTO.setIssuerName(issuer.getName());
					issuerHistoryResponseDTO.setCompanyLogo(issuer.getLogoURL());
					List<Map<String, String>> issuerData = service.findRevisions(iIssuerRepository, IssuerHistoryServiceHelper.MODEL_NAME,
					requiredFieldsMap, issuer.getId());
					data = issuerHistoryRendererImpl.processData(issuerData, compareCols, displayCols);
				}

				if (null != data) {

					IssuerHistoryDTO issuerHistoryDTO = null;
					SimpleDateFormat displayFormat = new SimpleDateFormat(TIMESTAMP_PATTERN);

					for (Map<String, Object> historyRec : data) {

						issuerHistoryDTO = new IssuerHistoryDTO();
						issuerHistoryDTO.setLastUpdatedOn(displayFormat.format(historyRec.get(IssuerHistoryServiceHelper.UPDATED_COL)));
						issuerHistoryDTO.setFieldUpdated((String)historyRec.get(IssuerHistoryServiceHelper.DISPLAY_FIELD_COL));
						issuerHistoryDTO.setNewValue((String)historyRec.get(IssuerHistoryServiceHelper.DISPLAY_VAL_COL));
						issuerHistoryDTO.setLastUpdatedBy((String)historyRec.get(IssuerHistoryServiceHelper.UPDATEDBY_COL));
						issuerHistoryDTO.setCommentId((String)historyRec.get(IssuerHistoryServiceHelper.COMMENT_COL));
						//issuerHistoryDTO.setDocumentId((String)historyRec.get(IssuerHistoryServiceHelper.CERTIFICATION_STATUS_FILE_COL));
						// issuerHistoryDTO.setCompanyLogo(companyLogo);
						issuerHistoryResponseDTO.setIssuerHistoryDTO(issuerHistoryDTO);
					}
				} else {
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("History Data not found for Issuer : " + issuerHistoryRequestDTO.getHiosId());
					}
				}
			} catch(Exception ex) {
				LOGGER.error("Exception occurred in getIssuerHistory service: " + ex.getMessage());
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred in getIssuerHistory service: ");
				giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.code), "Exception occurred in getIssuerHistory service: ",ex);
			}
		
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), issuerHistoryResponseDTO);
		}
		return issuerHistoryResponseDTO;
	}
	
	private void setDataAndCompareColsForIssuerHistory(Map<String, String> requiredFieldsMap, List<String> compareCols, List<Integer> displayCols) {
		if(null != requiredFieldsMap) {
			requiredFieldsMap.put(IssuerHistoryServiceHelper.CERTIFICATION_STATUS_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.CREATED_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.UPDATED_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.UPDATEDBY_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.NAME_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.FEDERAL_EMPLOYER_ID_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.NAIC_COMPANY_CODE_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.NAIC_GROUP_CODE_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.HIOS_ISSUER_ID_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.ADDRESS_LINE1_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.ADDRESS_LINE2_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.CITY_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.STATE_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.ZIP_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.ISSUER_SHORTNAME_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.COMMENT_COL, "");
		}
		
		if(null != compareCols) {
			compareCols.add(IssuerHistoryServiceHelper.CERTIFICATION_STATUS_COL);
			compareCols.add(IssuerHistoryServiceHelper.NAME_COL);
			compareCols.add(IssuerHistoryServiceHelper.FEDERAL_EMPLOYER_ID_COL);
			compareCols.add(IssuerHistoryServiceHelper.NAIC_COMPANY_CODE_COL);
			compareCols.add(IssuerHistoryServiceHelper.NAIC_GROUP_CODE_COL);
			compareCols.add(IssuerHistoryServiceHelper.HIOS_ISSUER_ID_COL);
			compareCols.add(IssuerHistoryServiceHelper.ADDRESS_LINE1_COL);
			compareCols.add(IssuerHistoryServiceHelper.ADDRESS_LINE2_COL);
			compareCols.add(IssuerHistoryServiceHelper.CITY_COL);
			compareCols.add(IssuerHistoryServiceHelper.STATE_COL);
			compareCols.add(IssuerHistoryServiceHelper.ZIP_COL);
			compareCols.add(IssuerHistoryServiceHelper.ISSUER_SHORTNAME_COL);
		}
		
		if(null != displayCols) {
			displayCols.add(IssuerHistoryServiceHelper.CERTIFICATION_STATUS);
			displayCols.add(IssuerHistoryServiceHelper.CREATED);
			displayCols.add(IssuerHistoryServiceHelper.UPDATED);
			displayCols.add(IssuerHistoryServiceHelper.UPDATEDBY);
			displayCols.add(IssuerHistoryServiceHelper.NAME);
			displayCols.add(IssuerHistoryServiceHelper.USER_NAME);
			displayCols.add(IssuerHistoryServiceHelper.CERTIFICATION_STATUS_VAL);
			displayCols.add(IssuerHistoryServiceHelper.NAME_VAL);
			displayCols.add(IssuerHistoryServiceHelper.DISPLAY_VAL);
			displayCols.add(IssuerHistoryServiceHelper.FEDERAL_EMPLOYER_ID_VAL);
			displayCols.add(IssuerHistoryServiceHelper.NAIC_COMPANY_CODE_VAL);
			displayCols.add(IssuerHistoryServiceHelper.NAIC_GROUP_CODE_VAL);
			displayCols.add(IssuerHistoryServiceHelper.HIOS_ISSUER_ID_VAL);
			displayCols.add(IssuerHistoryServiceHelper.ADDRESS_LINE1_VAL);
			displayCols.add(IssuerHistoryServiceHelper.ADDRESS_LINE2_VAL);
			displayCols.add(IssuerHistoryServiceHelper.CITY_VAL);
			displayCols.add(IssuerHistoryServiceHelper.STATE_VAL);
			displayCols.add(IssuerHistoryServiceHelper.ZIP_VAL);
			displayCols.add(IssuerHistoryServiceHelper.COMMENT);
			displayCols.add(IssuerHistoryServiceHelper.ISSUER_SHORTNAME_VAL);
		}
	}

	public IssuerHistoryResponseDTO getIssuerCertificationHistory(IssuerHistoryRequestDTO issuerHistoryRequestDTO) {
		IssuerHistoryResponseDTO issuerHistoryResponseDTO = new IssuerHistoryResponseDTO();
		if(issuerAPIRequestValidator.validateIssuerHistoryRequest(issuerHistoryRequestDTO, issuerHistoryResponseDTO)) {
			Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
	
			List<String> compareCols = new ArrayList<String>();
			List<Integer> displayCols = new ArrayList<Integer>();
			List<Map<String, Object>> data = null;
			List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
			
			setDataAndCompareColsForIssuerCertificationHistory(requiredFieldsMap, compareCols, displayCols);
			
			try {
				DisplayAuditService service = objectFactory.getObject();
				Issuer issuer = issuerService.getIssuerByHIOSId(issuerHistoryRequestDTO.getHiosId());
				if(null == issuer) {
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("getIssuerCertificationHistory: Failed to get Issuer for HIOS ID: " + issuerHistoryRequestDTO.getHiosId());
					}
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Issuer HIOS_ID", issuerHistoryRequestDTO.getHiosId(), "Failed to get Issuer record.");
				} else if(null == service) {
					LOGGER.info("getIssuerCertificationHistory: Internal error getting DisplayAuditService object");
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Internal error getting service object");
				} else {
					issuerHistoryResponseDTO.setHiosIssuerId(issuer.getHiosIssuerId());
					issuerHistoryResponseDTO.setIssuerName(issuer.getName());
					issuerHistoryResponseDTO.setCompanyLogo(issuer.getLogoURL());
					List<Map<String, String>> issuerData = service.findRevisions(iIssuerRepository, IssuerHistoryServiceHelper.MODEL_NAME,
					requiredFieldsMap, issuer.getId());
					data = issuerHistoryRendererImpl.processData(issuerData, compareCols, displayCols);
				}
				if(null != data) {
					SimpleDateFormat displayFormat = new SimpleDateFormat(TIMESTAMP_PATTERN);
					for (Map<String, Object> historyRec : data) {
						IssuerHistoryDTO issuerHistoryDTO = new IssuerHistoryDTO();
						issuerHistoryDTO.setLastUpdatedOn(displayFormat.format(historyRec.get(IssuerHistoryServiceHelper.UPDATED_COL)));
						issuerHistoryDTO.setFieldUpdated((String)historyRec.get(IssuerHistoryServiceHelper.NAME_COL));
						issuerHistoryDTO.setNewValue((String)historyRec.get(IssuerHistoryServiceHelper.CERTIFICATION_STATUS_COL));
						issuerHistoryDTO.setLastUpdatedBy((String)historyRec.get(IssuerHistoryServiceHelper.UPDATEDBY_COL));
						issuerHistoryDTO.setCommentId((String)historyRec.get(IssuerHistoryServiceHelper.COMMENT_COL));
						issuerHistoryDTO.setDocumentId((String)historyRec.get(IssuerHistoryServiceHelper.CERTIFICATION_STATUS_FILE_COL));
						// issuerHistoryDTO.setCompanyLogo(companyLogo);
						issuerHistoryResponseDTO.setIssuerHistoryDTO(issuerHistoryDTO);
					}
				} else {
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("Certification History Data not found for Issuer : " + issuerHistoryRequestDTO.getHiosId());
					}
				}
			} catch(Exception ex) {
				LOGGER.info("Exception occurred in getting Issuer Certification History service: " + ex.getMessage());
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred in getIssuerCertificationHistory : ");
				giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.ISSUER_CERTIFICATION_STATUS_EXCEPTION.code), "Exception occurred in getIssuerCertificationHistory service: ",ex);
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.ISSUER_CERTIFICATION_STATUS_EXCEPTION.getCode(), issuerHistoryResponseDTO);
		}
		
		return issuerHistoryResponseDTO;
	}

	private void setDataAndCompareColsForIssuerCertificationHistory(Map<String, String> requiredFieldsMap, List<String> compareCols, List<Integer> displayCols) {
		if(null != requiredFieldsMap) {
			requiredFieldsMap.put(IssuerHistoryServiceHelper.CERTIFICATION_STATUS_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.CREATED_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.UPDATED_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.NAME_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.COMMENT_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.CERTIFICATION_STATUS_FILE_COL, "");
			requiredFieldsMap.put(IssuerHistoryServiceHelper.UPDATEDBY_COL, "");
		}
		
		if(null != compareCols) {
			compareCols.add(IssuerHistoryServiceHelper.CERTIFICATION_STATUS_COL);
		}
		
		if(null != displayCols) {
			displayCols.add(IssuerHistoryServiceHelper.CERTIFICATION_STATUS);
			displayCols.add(IssuerHistoryServiceHelper.CREATED);
			displayCols.add(IssuerHistoryServiceHelper.UPDATED);
			displayCols.add(IssuerHistoryServiceHelper.NAME);
			displayCols.add(IssuerHistoryServiceHelper.COMMENT);
			displayCols.add(IssuerHistoryServiceHelper.CERTIFICATION_STATUS_FILE);
			displayCols.add(IssuerHistoryServiceHelper.UPDATEDBY);
		}
	}

	@Override
	public IssuerEnrollmentResponseDTO getIssuerEnrollmentInfo(String hiosId) {
		IssuerEnrollmentResponseDTO issuerEnrollmentResponseDTO = new IssuerEnrollmentResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		if(issuerAPIRequestValidator.validateIssuerHiosId(hiosId, errorVOList, issuerEnrollmentResponseDTO)) {
			try {
				Issuer issuer = issuerService.getIssuerByHIOSId(hiosId);
				issuerEnrollmentResponseDTO.setHiosIssuerId(hiosId);
				issuerEnrollmentResponseDTO.setIssuerName(issuer.getName());
				issuerEnrollmentResponseDTO.setCompanyLogo(issuer.getLogoURL());

		        List<IssuerD2c> issuerD2cList = iIssuerD2CRepository.findByIssuerHiosId(hiosId);
				if (CollectionUtils.isNotEmpty(issuerD2cList)) {

		        	for (IssuerD2c issuerD2c : issuerD2cList) {
			            EnrollmentFlowDTO enrollmentFlowDTO = new EnrollmentFlowDTO();
			            enrollmentFlowDTO.setPlanYear((issuerD2c.getApplicableYear() != null) ? issuerD2c.getApplicableYear().toString() : null);
			            enrollmentFlowDTO.setD2cFlowType((issuerD2c.getD2cFlowType() != null) ? issuerD2c.getD2cFlowType().toString() : null);
			            enrollmentFlowDTO.setIsD2cEnabled(issuerD2c.getD2cFlag());
			            issuerEnrollmentResponseDTO.setEnrollmentFlowDTO(enrollmentFlowDTO);
					}
		        } else {
					LOGGER.info("getIssuerEnrollmentInfo: Enrollment information not found.");
		        }
			} catch(Exception ex) {
				LOGGER.error("Exception occurred while getIssuerEnrollmentInfo: ", ex);
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting Enrollment Info for Issuer having HIOS ID: " + hiosId);
				giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.VIEW_ISSUER_ENROLLMENT_INFO_EXCEPTION.code), "Exception occurred in getIssuerEnrollmentInfo service: ",ex);
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.VIEW_ISSUER_HISTORY_EXCEPTION.getCode(), issuerEnrollmentResponseDTO);
		} 
		
		return issuerEnrollmentResponseDTO;
	}

	/**
	 * Method is used to update Issuer Enrollment Flow in IssuerD2C table.
	 */
	@Override
	public GhixResponseDTO updateIssuerEnrollmentFlowData(IssuerEnrollmentFlowRequestDTO issuerEnrollmentFlowRequest) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("updateIssuerEnrollmentFlowData() Start");
		}
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		GhixResponseDTO ghixResponse = issuerAPIRequestValidator.validateEnrollmentFlowAPI(issuerEnrollmentFlowRequest, errorVOList);

		try {

			if (GhixResponseDTO.STATUS.FAILED.name().equals(ghixResponse.getStatus())) {
				return ghixResponse;
			}

			for (String hiosIssuerId : issuerEnrollmentFlowRequest.getHiosIssuerIdList()) {
				updateIssuerEnrollmentFlowData(hiosIssuerId, issuerEnrollmentFlowRequest, ghixResponse, errorVOList);
			}
			ghixResponse.setStatus(GhixResponseDTO.STATUS.SUCCESS.name());
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while updating Issuer Enrollment Flow in IssuerD2C table");
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.VIEW_ISSUER_ENROLLMENT_INFO_EXCEPTION.code),
							"Exception occurred in updateIssuerEnrollmentFlowData() service: ", ex);
		}
		finally {
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.VIEW_ISSUER_ENROLLMENT_INFO_EXCEPTION.getCode(), ghixResponse);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("updateIssuerEnrollmentFlowData() End");
			}
		}
		return ghixResponse;
	}

	/**
	 * Method is used to update Issuer Enrollment Flow in IssuerD2C table.
	 */
	private void updateIssuerEnrollmentFlowData(String hiosIssuerId, IssuerEnrollmentFlowRequestDTO issuerEnrollmentFlowRequest,
			GhixResponseDTO ghixResponse, List<PlanMgmtErrorVO> errorVOList) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("updateIssuerEnrollmentFlowData() Start");
		}

		try {
			List<IssuerD2c> issuerD2cList = iIssuerD2CRepository.findByIssuerHiosIdAndYear(hiosIssuerId, Integer.valueOf(issuerEnrollmentFlowRequest.getPlanYear()));
			// if record exists for hios issuer id and applicable year combination then update existing records else insert new record
			IssuerD2c newIssuerD2c = new IssuerD2c();

			if (issuerD2cList.size() > 0) {
				// update existing record
				newIssuerD2c.setId(issuerD2cList.get(0).getId());
				newIssuerD2c.setCreatedBy(issuerD2cList.get(0).getCreatedBy());
				newIssuerD2c.setCreationTimestamp(issuerD2cList.get(0).getCreationTimestamp());
				newIssuerD2c.setLastUpdatedBy(Integer.valueOf(issuerEnrollmentFlowRequest.getLastUpdatedBy()));
			}
			else {
				// create new record
				newIssuerD2c.setCreatedBy(Integer.valueOf(issuerEnrollmentFlowRequest.getCreateBy()));
			}
			newIssuerD2c.setApplicableYear(Integer.valueOf(issuerEnrollmentFlowRequest.getPlanYear()));
			newIssuerD2c.setHiosIssuerId(hiosIssuerId);
			newIssuerD2c.setD2cFlag(issuerEnrollmentFlowRequest.getIsD2cEnabled());

			if ("Y".equalsIgnoreCase(issuerEnrollmentFlowRequest.getIsD2cEnabled())) {
				newIssuerD2c.setD2cFlowType(IssuerD2c.D2CFLOWTYPE.valueOf(issuerEnrollmentFlowRequest.getD2cFlowType()));
			}
			else {
				newIssuerD2c.setD2cFlowType(null);
			}
			iIssuerD2CRepository.save(newIssuerD2c);
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while updating Issuer Enrollment Flow in IssuerD2C table for HIOS Issuer ID" + hiosIssuerId);
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.VIEW_ISSUER_ENROLLMENT_INFO_EXCEPTION.code),
							"Exception occurred in updateIssuerEnrollmentFlowData() service: ", ex);
		}
		finally {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("updateIssuerEnrollmentFlowData() End");
			}
		}
	}

	

	@Override
	public GhixResponseDTO createIssuer(IssuerDTO issuerDTO) {
		GhixResponseDTO ghixResponseDTO = new GhixResponseDTO();
		if(issuerAPIRequestValidator.validateNewIssuerInfo(issuerDTO, ghixResponseDTO)) {
			List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
			String hiosId = issuerDTO.getHiosIssuerId();
			Issuer issuerObj = issuerService.getIssuerByHIOSId(hiosId);
			if(null == issuerObj) {
				EntityManager em = emf.createEntityManager();
				if(null != em && em.isOpen()) {
					try {
						em.getTransaction().begin();
						IssuerRepresentative representativeObj = createNewIssuer(issuerDTO, errorVOList, em);
	                	if(null != representativeObj) {
	    					if(null != em.getTransaction() && em.getTransaction().isActive()) {
	    						em.getTransaction().commit();
	    					}
	                		ghixResponseDTO.setId(representativeObj.getId());
	                	} else {
	    					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Failed to create Issuer record.");
	    					if(null != em.getTransaction() && em.getTransaction().isActive()) {
	    						em.getTransaction().rollback();
	    					}
	                	}
					} catch(Exception ex) {
						if(null != em.getTransaction() && em.getTransaction().isActive()) {
							em.getTransaction().rollback();
						}
						LOGGER.info("Exception occurred in saving Issuer info: " + ex.getMessage());
						PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred in saving Issuer info.");
						giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.NEW_ISSUER_CREATION_EXCEPTION.code), "Exception occurred in updateIssuerPayment service: ",ex);
					} finally {
						if(em.isOpen()) {
							em.close();
							em = null;
						}
					}
				}
				PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.NEW_ISSUER_CREATION_EXCEPTION.getCode(), ghixResponseDTO);
			}
			else {
				LOGGER.error("saveIssuer: Issuer record already exist for HIOS ID: " + hiosId);
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Issuer HIOS ID", hiosId, "Issuer record already exist.");
				PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.DUPLICATE_HIOS_EXCEPTION.getCode(), ghixResponseDTO);
			}
		}
		return 	ghixResponseDTO;
	}

	private IssuerRepresentative createNewIssuer(IssuerDTO issuerDTO, List<PlanMgmtErrorVO> errorVOList, EntityManager em) {
		Issuer issuerObj = null;
		IssuerRepresentative representativeObj = null;
		String isEmailAlreadyExists = pmmsIssuerRepresentativeService.isEmailAssignedToIssuerRep(issuerDTO.getPrimaryContact().getEmail());
		if (isEmailAlreadyExists.equalsIgnoreCase(PlanMgmtConstants.ASSOCIATED_WITH_DIFF_ROLE)) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Email ID", issuerDTO.getPrimaryContact().getEmail(), "User already exist with a different Role.");
		} else {
			if(null != em && em.isOpen()) {
				try {
					issuerObj = new Issuer();
					setIssuerDetails(issuerDTO, issuerObj, true);
					issuerObj = em.merge(issuerObj);
					if(null != issuerObj) {
						issuerDTO.getPrimaryContact().setLastUpdatedBy(issuerDTO.getLastUpdatedBy());
						if (isEmailAlreadyExists.equalsIgnoreCase(PlanMgmtConstants.NO_DATA_FOUND)) {
							representativeObj = pmmsIssuerRepresentativeService.saveIssuerRepInformation(issuerDTO.getPrimaryContact(), new IssuerRepresentative(), issuerObj, em, true);
						} else {
							IssuerRepSearchResponseDTO issuerRepSearchResponseDTO = pmmsIssuerRepresentativeService.getIssuerRepByEmail(issuerDTO.getPrimaryContact());
							if(issuerRepSearchResponseDTO == null || CollectionUtils.isEmpty(issuerRepSearchResponseDTO.getIssuerRepDTOList())) {
								PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Failed to get existing issuer representative record.");
								issuerObj = null;
							} else {
								IssuerRepDTO issuerRepDTO = issuerRepSearchResponseDTO.getIssuerRepDTOList().get(0);
								issuerRepDTO.setFirstName(issuerDTO.getPrimaryContact().getFirstName());
								issuerRepDTO.setLastName(issuerDTO.getPrimaryContact().getLastName());
								issuerRepDTO.setIssuerRepTitle(issuerDTO.getPrimaryContact().getIssuerRepTitle());
								issuerRepDTO.setPhone(issuerDTO.getPrimaryContact().getPhone());
								issuerRepDTO.setLastUpdatedBy(issuerDTO.getLastUpdatedBy());
								representativeObj = pmmsIssuerRepresentativeService.getRepresentativeByEmail(issuerDTO.getPrimaryContact().getEmail());
								representativeObj = pmmsIssuerRepresentativeService.saveIssuerRepInformation(issuerRepDTO, representativeObj, issuerObj, em, false);
							}
						}
						
						if(null == representativeObj) {
							PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Failed to create issuer primary contact record.");
							issuerObj = null;
						}else {
							representativeObj.setIssuer(issuerObj);
						}
					} else {
						PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Failed to create new issuer record.");
					}
				} catch(Exception ex) {
					LOGGER.info("Exception occurred in Creating Issuer record: " + ex.getMessage());
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occured while creating new issuer record.");
					giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.NEW_ISSUER_CREATION_EXCEPTION.code), "Exception occurred in Creating Issuer details: ",ex);
					issuerObj = null;
				}
			} else {
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "DB error while setting up issuer representative record.");
			}
		}
		return representativeObj;
	}
	
	private void setIssuerDetails(IssuerDTO issuerDTO, Issuer issuerObj, boolean isNew) {
		if(isNew) {
			IssuerBrandName issuerBrandName = null;
			if (null != issuerDTO.getParentBrandId() && 0 < issuerDTO.getParentBrandId()) {
				issuerBrandName = pmmsIssuerBrandNameService.getIssuerBrandNameById(issuerDTO.getParentBrandId());
				issuerObj.setIssuerBrandName(issuerBrandName);
			}
			issuerObj.setCertificationStatus(CERT_STATUS_CERTIFIED);
			issuerObj.setSendEnrollmentEndDateFlag(PlanMgmtConstants.TRUE_STRING);
			issuerObj.setLastUpdatedBy(Integer.valueOf(issuerDTO.getLastUpdatedBy()));
			issuerObj.setEmailAddress(issuerDTO.getPrimaryContact().getEmail());
		}
		issuerObj.setName(issuerDTO.getName());
		issuerObj.setShortName(issuerDTO.getShortName());
		issuerObj.setNaicCompanyCode(issuerDTO.getNaicCompanyCode());
		issuerObj.setNaicGroupCode(issuerDTO.getNaicGroupCode());
		issuerObj.setFederalEin(issuerDTO.getFederalEmployeeId());
		issuerObj.setHiosIssuerId(issuerDTO.getHiosIssuerId());
		issuerObj.setAddressLine1(issuerDTO.getStreetAddress1());
		issuerObj.setAddressLine2(issuerDTO.getStreetAddress2());
		issuerObj.setCity(issuerDTO.getCity());
		issuerObj.setState(issuerDTO.getState());
		issuerObj.setZip(issuerDTO.getZip());
		//issuerObj.setCertifiedOn(issuerDTO.get);
		//issuerObj.setCreationTimestamp(issuerDTO.get);
		//issuerObj.setLastUpdateTimestamp(issuerDTO.get);
	}
	
	
	/* ##### implementation of get issuer data by HIOS Id  ##### */
	@Override
	public IssuerResponseDTO getIssuerByHiosId(String hiosId){
		IssuerResponseDTO issuerResponseDTO = new IssuerResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		
		if(issuerAPIRequestValidator.validateIssuerHiosId(hiosId, errorVOList, issuerResponseDTO)) {
			try
			{
				Issuer issuerObj = iIssuerRepository.findByHiosId(hiosId);
				if(null != issuerObj){
					IssuerDTO issuerDTO = null;
					IssuerResponseDTOHelperMS issuerResponseDTOHelper = new IssuerResponseDTOHelperMS();
					issuerDTO = issuerResponseDTOHelper.setObjectToDTO(issuerObj, false);
					issuerResponseDTO.setIssuerDTO(issuerDTO);
					issuerResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
					
				}else{
					issuerResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					issuerResponseDTO.setErrorMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
				}
			}
			catch(Exception ex)
			{
				giExceptionHandler.recordFatalException(Component.PLANMGMT,
						null, "Failed to execute getIssuerByHiosId ", ex);
				issuerResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				issuerResponseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				issuerResponseDTO.setErrorMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}
			
		}
		
		issuerResponseDTO.endResponse();
		return issuerResponseDTO;
	}
	
	
	/* ##### implementation of get issuer list as per search criteria   ##### */
	@Override
	public IssuerSearchResponseDTO getIssuerList(IssuerSearchRequestDTO issuerSearchRequestDTO) {

		IssuerSearchResponseDTO issuerSearchResponseDTO = new IssuerSearchResponseDTO();
		issuerSearchResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
		
		try {
			String status = PlanMgmtConstants.ALL; 
			String issuerName = PlanMgmtConstants.ALL;
			String hiosIssuerId = PlanMgmtConstants.ALL; 
			String tenantCode = PlanMgmtConstants.ALL;
			String brandNameId = PlanMgmtConstants.ZERO_STR;
			String d2cFlowTypeId = PlanMgmtConstants.ALL;
			String d2cFlowYear = PlanMgmtConstants.EMPTY_STRING;
			String sortBy = PlanMgmtConstants.EMPTY_STRING;
			String sortOrder = PlanMgmtConstants.ASC;
			Integer pageSize = GhixConstants.PAGE_SIZE;
			Integer startRecord = 0;
			
			if(StringUtils.isNotBlank(issuerSearchRequestDTO.getIssuerName())){
				issuerName = issuerSearchRequestDTO.getIssuerName().trim();
			}
			if(StringUtils.isNotBlank(issuerSearchRequestDTO.getStatus())){
				status = issuerSearchRequestDTO.getStatus().trim();
			}
			if(StringUtils.isNotBlank(issuerSearchRequestDTO.getHiosId())){
				hiosIssuerId = issuerSearchRequestDTO.getHiosId().trim();
			}
			if(StringUtils.isNotBlank(issuerSearchRequestDTO.getTenantCode())){
				tenantCode = issuerSearchRequestDTO.getTenantCode().trim();
			}
			if(StringUtils.isNotBlank(issuerSearchRequestDTO.getBrandNameId())){
				brandNameId = issuerSearchRequestDTO.getBrandNameId().trim();
			}
			if(StringUtils.isNotBlank(issuerSearchRequestDTO.getD2cFlowTypeId())){
				d2cFlowTypeId = issuerSearchRequestDTO.getD2cFlowTypeId().trim();
			}
			if(StringUtils.isNotBlank(issuerSearchRequestDTO.getD2cFlowYear())){
				d2cFlowYear = issuerSearchRequestDTO.getD2cFlowYear().trim();
			}else{
				d2cFlowYear = String.valueOf(PlanMgmtUtil.getCurrentYear());  // current year as default year
			}
			
			if(StringUtils.isNotBlank(issuerSearchRequestDTO.getPageNumber())){
				startRecord = (Integer.parseInt(issuerSearchRequestDTO.getPageNumber()) - 1) * GhixConstants.PAGE_SIZE;
				//currentPage = Integer.parseInt(issuerSearchRequestDTO.getPageNumber());
			}
			
			if(StringUtils.isNotBlank(issuerSearchRequestDTO.getSortBy())){
				sortBy = issuerSearchRequestDTO.getSortBy().toString().trim();;
			}	
			
			if (StringUtils.isNotBlank(issuerSearchRequestDTO.getSortOrder())
					&& issuerSearchRequestDTO.getSortOrder().trim().equalsIgnoreCase(PlanMgmtConstants.ASC)) {
				sortOrder = PlanMgmtConstants.ASC;
			}
			else if (StringUtils.isNotBlank(issuerSearchRequestDTO.getSortOrder())
					&& issuerSearchRequestDTO.getSortOrder().trim().equalsIgnoreCase(PlanMgmtConstants.DESC)) {
				sortOrder = PlanMgmtConstants.DESC;
			}
			issuerSearchResponseDTO = issuerServiceHelper.getIssuerList(pageSize, startRecord, sortBy, sortOrder, 
					issuerName, status, tenantCode, hiosIssuerId, brandNameId, d2cFlowTypeId, d2cFlowYear);
		}
		catch(Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute getIssuerList ", ex);
			issuerSearchResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
			issuerSearchResponseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
			issuerSearchResponseDTO.setErrorMsg(PlanMgmtConstants.ERROR_TECHNICAL);
		}
		issuerSearchResponseDTO.endResponse();
		return issuerSearchResponseDTO;
	}

	@Override
	public GhixResponseDTO updateIssuer(String issuerId, IssuerDTO issuerDTO) {
		GhixResponseDTO ghixResponseDTO = new GhixResponseDTO();
		if(issuerAPIRequestValidator.validateIssuerInfoForUpdate(issuerId, issuerDTO, ghixResponseDTO)) {
			int errorCode = ErrorCode.SAVE_ISSUER_EXCEPTION.getCode();
			List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
			String hiosId = issuerDTO.getHiosIssuerId();
			Issuer issuerObj = issuerService.getIssuerbyId(Integer.parseInt(issuerId));
			if(null != issuerObj && issuerObj.getHiosIssuerId().equals(hiosId)) {
				try {
					if(issuerDTO.isUpdateCertStatus()) {
						errorCode = ErrorCode.UPDATE_ISSUER_STATUS_EXCEPTION.getCode();
						updateIssuerCertStatus(issuerObj, issuerDTO, errorVOList);
					} 
					if (issuerDTO.isUpdateIndvMktProfile() && errorVOList.isEmpty()) {
						errorCode = ErrorCode.SAVE_MARKET_PROFILE_EXCEPTION.getCode();
						updateIssuerIndvMktProfile(issuerObj, issuerDTO, errorVOList);
					} 
					if (issuerDTO.isUpdateCompMktProfile() && errorVOList.isEmpty()) {
						errorCode = ErrorCode.SAVE_COMPANY_PROFILE_EXCEPTION.getCode();
						updateIssuerCompMktProfile(issuerObj, issuerDTO, errorVOList);
					} 
					if (issuerDTO.isUpdateIssuerDetails() && errorVOList.isEmpty()) {
						updateIssuerDetails(issuerObj, issuerDTO, errorVOList);
					}
					if(errorVOList.isEmpty()) {
						issuerObj.setLastUpdatedBy(Integer.valueOf(issuerDTO.getLastUpdatedBy()));
						if(StringUtils.isBlank(issuerObj.getFederalEin())){
							issuerObj.setFederalEin(null);
						}
						if(StringUtils.isBlank(issuerObj.getAddressLine2())){
							issuerObj.setAddressLine2(null);
						}
							
						iIssuerRepository.save(issuerObj);
						// apply to parent brand name
						applyToParentBrand(issuerDTO, issuerObj);
						
						ghixResponseDTO.setId(issuerObj.getId());
					}
				} catch(Exception ex) {
					LOGGER.info("Exception occurred in updating Issuer info: " + ex.getMessage());
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred in updating Issuer info.");
					giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(errorCode), "Exception occurred in updateIssuer service: ",ex);
				}
			}else{
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "No matching record found for id=" + issuerId + " and hios-id=" + hiosId);
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, errorCode, ghixResponseDTO);
		}
		return ghixResponseDTO;
	}

	private void updateIssuerDetails(Issuer issuerObj, IssuerDTO issuerDTO, List<PlanMgmtErrorVO> errorVOList) {
		IssuerBrandName issuerBrandName = null;
		if(issuerDTO.getParentBrandId() != null){
			issuerBrandName = pmmsIssuerBrandNameService.getIssuerBrandNameById(issuerDTO.getParentBrandId());
			if(null == issuerBrandName) {
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "ParentBrandId",  issuerDTO.getParentBrandId().toString(), "Parent Brand Name record not found." );
				return;
			}	
		}else {
			issuerBrandName = new IssuerBrandName();
		}
		issuerObj.setIssuerBrandName(issuerBrandName);
		issuerObj.setName(issuerDTO.getName());
		issuerObj.setShortName(issuerDTO.getShortName());
		issuerObj.setNaicCompanyCode(issuerDTO.getNaicCompanyCode());
		issuerObj.setNaicGroupCode(issuerDTO.getNaicGroupCode());
		issuerObj.setFederalEin(issuerDTO.getFederalEmployeeId());
		issuerObj.setAddressLine1(issuerDTO.getStreetAddress1());
		issuerObj.setAddressLine2(issuerDTO.getStreetAddress2());
		issuerObj.setCity(issuerDTO.getCity());
		issuerObj.setState(issuerDTO.getState());
		issuerObj.setZip(issuerDTO.getZip());
	}

	private void updateIssuerCompMktProfile(Issuer issuerObj, IssuerDTO issuerDTO, List<PlanMgmtErrorVO> errorVOList) {
		if(null != issuerDTO.getOnExchangeDisclaimer() && !(issuerDTO.getOnExchangeDisclaimer().isEmpty())){
    		String onFilteredDisclaimerText = getDisclaimerTextWithoutSpecialChar(issuerDTO.getOnExchangeDisclaimer());
    		issuerObj.setOnExchangeDisclaimer(onFilteredDisclaimerText);
		}else{
			issuerObj.setOnExchangeDisclaimer(null);
		}
		
		if(null != issuerDTO.getOffExchangeDisclaimer() && !(issuerDTO.getOffExchangeDisclaimer().isEmpty())){
    		String offfilteredDisclaimerText = getDisclaimerTextWithoutSpecialChar(issuerDTO.getOffExchangeDisclaimer());
    		issuerObj.setOffExchangeDisclaimer(offfilteredDisclaimerText);
		}else{
			issuerObj.setOffExchangeDisclaimer(null);
		}
		
		issuerObj.setCompanyLegalName(issuerDTO.getCompanyLegalName());
		issuerObj.setStateOfDomicile(issuerDTO.getIssuerState());
		issuerObj.setCompanyAddressLine1(issuerDTO.getCompanyAddress1());
		issuerObj.setCompanyAddressLine2(issuerDTO.getCompanyAddress2());
		issuerObj.setCompanyCity(issuerDTO.getCompanyCity());
		issuerObj.setCompanyState(issuerDTO.getCompanyState());
		issuerObj.setCompanyZip(issuerDTO.getCompanyZip());
		issuerObj.setProducerUrl(issuerDTO.getProducerPortalUrl());
		issuerObj.setProducerUserName(issuerDTO.getProducerPortalUname());
		issuerObj.setLoginRequired(issuerDTO.getIsLoginInRequired());
		
		if (StringUtils.isNotBlank(issuerDTO.getProducerPortalPwd())) {
			// get encrypted password in request
			issuerObj.setProducerPassword(issuerDTO.getProducerPortalPwd());
		}
		else {
			issuerObj.setProducerPassword(null);
		}
		if(StringUtils.isNotBlank(issuerDTO.getCarrierApplicationUrl())) {
			issuerObj.setApplicationUrl(issuerDTO.getCarrierApplicationUrl().replace("+", "%2B"));
		}else{
			issuerObj.setApplicationUrl(null);
		}
	
		issuerObj.setSiteUrl(issuerDTO.getWebsiteUrl());
		issuerObj.setCompanySiteUrl(issuerDTO.getCustomerWebsiteUrl());
		issuerObj.setNationalProducerNumber(issuerDTO.getNationalProducerNum());
		issuerObj.setAgentFirstName(issuerDTO.getAgentFirstName());
		issuerObj.setAgentLastName(issuerDTO.getAgentLastName());
		issuerObj.setPaymentUrl(issuerDTO.getPaymentUrl());
		issuerObj.setWritingAgent(issuerDTO.getWritingAgent());
		issuerObj.setBrokerId(issuerDTO.getBrokerId());
		issuerObj.setBrokerPhone(issuerDTO.getAgentPhoneNumber());
		issuerObj.setBrokerFax(issuerDTO.getAgentFaxNumber());
		issuerObj.setBrokerEmail(issuerDTO.getAgentEmail());
		//set issuer logo
		if(null !=issuerDTO.getUploadCompanyLogo()) {
			issuerLogoUtil.uploadLogoAndSetDetails(issuerDTO.getUploadCompanyLogo(), issuerObj);
		}
	}

	private void updateIssuerIndvMktProfile(Issuer issuerObj, IssuerDTO issuerDTO, List<PlanMgmtErrorVO> errorVOList) {
		issuerObj.setIndvCustServicePhone(issuerDTO.getIndvMktCustServicePhoneNumber());
		issuerObj.setIndvCustServicePhoneExt(issuerDTO.getIndvMktCustServicePhoneNumberExt());
		issuerObj.setIndvCustServiceTollFree(issuerDTO.getIndvMktCustServiceTollFreePhoneNumber());
		issuerObj.setIndvCustServiceTTY(issuerDTO.getIndvMktCustServiceTTY());
		issuerObj.setIndvSiteUrl(issuerDTO.getIndvMktCustWebsiteUrl());
		issuerObj.setAppStatusDeptPhoneNumber(issuerDTO.getIndvMktAppStatusDeptPhoneNumber());
		issuerObj.setCommissionsContactName(issuerDTO.getIndvMktCommContactName());
		issuerObj.setCommissionsPhoneNumber(issuerDTO.getIndvMktCommPhoneNumber());
		issuerObj.setCommissionsPhoneNumberExt(issuerDTO.getIndvMktCommPhoneNumberExt());
		issuerObj.setCommissionsEmailId(issuerDTO.getIndvMktCommEmail());
		issuerObj.setPcdPhoneNumber(issuerDTO.getIndvMktPcdPhoneNumber());
		issuerObj.setPcdPhoneNumberExt(issuerDTO.getIndvMktPcdPhoneNumberExt());
		issuerObj.setPcdFaxNumber(issuerDTO.getIndvMktPcdFAX());
		issuerObj.setPcdEmailId(issuerDTO.getIndvMktPcdEmail());
		
		Location location = null;
		if(issuerObj.getLocation() != null){
			location = issuerObj.getLocation();
		}else if(issuerDTO.getIndvMktPcdStreetAddress1() != null || issuerDTO.getIndvMktPcdStreetAddress2() != null || issuerDTO.getIndvMktPcdState() != null || issuerDTO.getIndvMktPcdZip() != null) {
			location =  new Location();
		}
		if(issuerDTO.getIndvMktPcdStreetAddress1() != null){
			location.setAddress1(issuerDTO.getIndvMktPcdStreetAddress1());
		}
		if(issuerDTO.getIndvMktPcdStreetAddress2() != null){
			location.setAddress2(issuerDTO.getIndvMktPcdStreetAddress2());
		}
		if(issuerDTO.getIndvMktPcdState() != null){
			location.setState(issuerDTO.getIndvMktPcdState());
		}
		if(issuerDTO.getIndvMktPcdZip() != null){
			location.setZip(issuerDTO.getIndvMktPcdZip());
		}
		
		issuerObj.setLocation(location);
		issuerObj.setBrokerContactName(issuerDTO.getIndvMktBrokerName());
		issuerObj.setBrokerEmailId(issuerDTO.getIndvMktBrokerEmailId());
		issuerObj.setBrokerPhoneNumber(issuerDTO.getIndvMktBrokerPhoneNumber());
		issuerObj.setBrokerPhoneNumberExt(issuerDTO.getIndvMktBrokerPhoneNumberExt());
		issuerObj.setCommissionPortalUrl(issuerDTO.getIndvMktCommPortalUrl());
		issuerObj.setCommissionPortalUserName(issuerDTO.getIndvMktCommPortalUname());
		issuerObj.setCommissionPortalPassword(issuerDTO.getIndvMktCommPortalPwd());
		issuerObj.setAgencyRepoPortalUrl(issuerDTO.getIndvMktAgencyRepoPortalURL());
		issuerObj.setAgencyRepoPortalUserName(issuerDTO.getIndvMktBrokerReportingPortalUname());
		issuerObj.setAgencyRepoPortalPassword(issuerDTO.getIndvMktBrokerReportingPortalPwd());
	}

	private void updateIssuerCertStatus(Issuer issuerObj, IssuerDTO issuerDTO, List<PlanMgmtErrorVO> errorVOList) {
		String issuerCertifyNewStatus = issuerDTO.getCertificationStatus();
		issuerObj.setCommentId(issuerDTO.getCommentId());
		issuerObj.setCertificationStatus(issuerDTO.getCertificationStatus());
		issuerObj.setCertificationDoc(issuerDTO.getCertificationDoc());
		issuerObj.setDecertifiedOn(null);
		// when issuer certification status is updated to Certified
		if (issuerCertifyNewStatus.equalsIgnoreCase(certification_status.CERTIFIED.toString())) {
			issuerObj.setCertifiedOn(new TSDate());
			issuerObj.setEffectiveStartDate(new TSDate());
			issuerObj.setEffectiveEndDate(null);
		} else if (issuerCertifyNewStatus.equalsIgnoreCase(certification_status.DECERTIFIED.toString())) {
			// when issuer certification status is updated to Decertified
			issuerObj.setDecertifiedOn(new TSDate());
			issuerObj.setEffectiveEndDate(new TSDate());
			planMgmtService.decertifyPlans(issuerObj.getId());
		}
	}
	
	private String getDisclaimerTextWithoutSpecialChar(String disclaimerText){
		StringBuilder filteredDisclaimerText = new StringBuilder();
		
		String [] inputStringStringArray = disclaimerText.split(" ");
		for(String string: inputStringStringArray){
			if(string.startsWith("&") || string.endsWith(";")){
				string="";
			}
			if(string.contains("&") || string.contains(";")){
				string="";
			}
			if(filteredDisclaimerText.toString().isEmpty()){
				filteredDisclaimerText.append(string);
			}else{
				filteredDisclaimerText.append(" ");
				filteredDisclaimerText.append(string);
			}
		}
		return filteredDisclaimerText.toString();
	}

	/**
	 * Method is used to get Issuer Drop Down List data.
	 * Excluded Quotit, PUF & DRX issuers.
	 */
	@Override
	public DropDownListResponseDTO getIssuerDropDownList(String stateCode) {

		LOGGER.debug("getIssuerDropDownList Begin");
		DropDownListResponseDTO dropDownData = new DropDownListResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		try {

			List<Object[]> issuerDataList = null;

			if (StringUtils.isBlank(stateCode)) {
				issuerDataList = iIssuerRepository.getIssuerDropDownList();
			}
			else {

				if (validateState(stateCode.trim().toUpperCase())) {
					issuerDataList = iIssuerRepository.getIssuerDropDownListByState(stateCode.trim().toUpperCase());
				}
				else {
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "State Code", stateCode, EMSG_INVALID_VALUE);
					return dropDownData;
				}
			}

			if (CollectionUtils.isEmpty(issuerDataList)) {
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Issuer List", null, EMSG_EMPTY);
				return dropDownData;
			}

			for (Object[] issuerData : issuerDataList) {

				if (null != issuerData[0]) {

					String value = null;
					if (null != issuerData[1]) {
						value = issuerData[1].toString();
					}
					else {
						value = "";
					}
					dropDownData.setData(issuerData[0].toString(), value.trim());
				}
			}
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while geting Issuer Drop Down Data List ");
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.GET_ISSUERS_LIST.code),
							"Exception occurred while geting Issuer Drop Down Data List: ", ex);
		}
		finally {
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.GET_ISSUERS_LIST.getCode(), dropDownData);
			LOGGER.debug("getIssuerDropDownList end");
		}
		return dropDownData;
	}
	
	/*@Override
	public IssuerSearchResponseDTO getIssuerListForDropDown(IssuerSearchRequestDTO issuerSearchRequestDTO) {

		LOGGER.debug("getIssuerDropDownList Begin");
		DropDownListResponseDTO dropDownData = new DropDownListResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		try {

			List<Object[]> issuerDataList = null;

			if (StringUtils.isBlank(stateCode)) {
				issuerDataList = iIssuerRepository.getIssuerDropDownList();
			}
			else {

				if (validateState(stateCode.trim().toUpperCase())) {
					issuerDataList = iIssuerRepository.getIssuerDropDownListByState(stateCode.trim().toUpperCase());
				}
				else {
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "State Code", stateCode, EMSG_INVALID_VALUE);
					return dropDownData;
				}
			}

			if (CollectionUtils.isEmpty(issuerDataList)) {
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Issuer List", null, EMSG_EMPTY);
				return dropDownData;
			}

			for (Object[] issuerData : issuerDataList) {

				if (null != issuerData[0]) {

					String value = null;
					if (null != issuerData[1]) {
						value = issuerData[1].toString();
					}
					else {
						value = "";
					}
					dropDownData.setData(issuerData[0].toString(), value.trim());
				}
			}
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while geting Issuer Drop Down Data List ");
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.GET_ISSUERS_LIST.code),
							"Exception occurred while geting Issuer Drop Down Data List: ", ex);
		}
		finally {
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.GET_ISSUERS_LIST.getCode(), dropDownData);
			LOGGER.debug("getIssuerDropDownList end");
		}
		return dropDownData;
	}
	*/
	
	@Override
	public List<IssuerLightDTO> getIssuerListForDropDown(IssuerSearchRequestDTO request) {
		
	
			String selectedState = request.getState();
			if(StringUtils.isEmpty(selectedState) || PlanMgmtConstants.NA.equalsIgnoreCase(selectedState)){
				selectedState = PlanMgmtConstants.ALL;				
			}
				
			
			int applicableYear = TSCalendar.getInstance().get(Calendar.YEAR);  // default value current year
			if(request.getApplicabelYear() >= 0){
				applicableYear = request.getApplicabelYear();
			}
			
			String insuranceType = "HEALTH";
			if(!StringUtils.isBlank(request.getInsuranceType())){
				insuranceType = request.getInsuranceType();				
			}
			
		 	List<Object[]> issuerNamesList = iIssuerRepository.getAllIssuerNames(insuranceType, selectedState, applicableYear);
		 	return getIssuerLightDTOListForDropDown(issuerNamesList);
	}

	/**
	 * Method is used to get IssuerLightDTO-List for Drop-Down.
	 */
	private List<IssuerLightDTO> getIssuerLightDTOListForDropDown(List<Object[]> issuerNamesList) {

		List<IssuerLightDTO> issuerNames = new ArrayList<IssuerLightDTO>();
		IssuerLightDTO issuerName = null;

		if (!CollectionUtils.isEmpty(issuerNamesList)) {

			for (Object[] objectArray : issuerNamesList) {

				issuerName = new IssuerLightDTO();

				if (objectArray[0] != null) {
					issuerName.setId(Integer.parseInt(objectArray[0].toString()));
				}

				if (objectArray[1] != null) {
					issuerName.setIssuerName(objectArray[1].toString());
				}

				if (objectArray[2] != null) {
					issuerName.setStateOfDomicile(objectArray[2].toString());
				}

				if (objectArray[3] != null) {
					issuerName.setHiosId(objectArray[3].toString());
				}
				issuerNames.add(issuerName);
			}
		}
		return issuerNames;
	}

	/**
	 * Method is used to get Medicare Issuer List from Database for Drop-Down.
	 */
	@Override
	public List<IssuerLightDTO> getMedicareIssuerListForDropDown() {
		List<Object[]> medicareIssuerList = iIssuerRepository.getMedicareIssuerDropDownList();
		return getIssuerLightDTOListForDropDown(medicareIssuerList);
	}
	
	
	
	@Transactional(readOnly = true)
	private void applyToParentBrand(IssuerDTO issuerDTO, Issuer issuerObj){
		EntityManager entityManager = null;
		
		try
		{
			// if requested issuer tie up with IssuerBrandName, 
			// then update data of other issuers of same IssuerBrandName depends on *ApplyToParentBand() flag
			if(issuerObj.getIssuerBrandName()!=null && issuerObj.getIssuerBrandName().getId() > PlanMgmtConstants.ZERO)
			{
				StringBuilder applyToParentBrandQuery = new StringBuilder(1028);
				boolean updateToParentBrand = false;
				applyToParentBrandQuery.append("UPDATE issuers SET ");
				
				// ------------- update company profile start here ----------------------------//
				
				if(issuerDTO.isBrokerIdApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" broker_id = '" + issuerObj.getBrokerId() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isWritingAgentApplyToParentBand() == true ){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" writing_agent = '" + issuerObj.getWritingAgent() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isNationalProducerNumApplyToParentBand() == true ){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" national_producer_number = '" + issuerObj.getNationalProducerNumber() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isAgentFirstNameApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" agent_first_name = '" + issuerObj.getAgentFirstName() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isAgentLastNameApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" agent_last_name = '" + issuerObj.getAgentLastName() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isAgentPhoneNumberApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" broker_phone = '" + issuerObj.getBrokerPhone() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isAgentFaxNumberApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" broker_fax = '" + issuerObj.getBrokerFax() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isAgentEmailApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" broker_email = '" + issuerObj.getBrokerEmail() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isPaymentUrlApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" payment_url = '" + issuerObj.getPaymentUrl() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isProducerPortalUrlApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					if(StringUtils.isNotBlank(issuerObj.getProducerUrl())){
						applyToParentBrandQuery.append(" producer_url = '" + issuerObj.getProducerUrl() + "'");
					}else{
						applyToParentBrandQuery.append(" producer_url = NULL");
					}
					updateToParentBrand = true;
				}
				if(issuerDTO.isProducerPortalUnameApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					if(StringUtils.isNotBlank(issuerObj.getProducerUserName())){
						applyToParentBrandQuery.append(" producer_uname = '" + issuerObj.getProducerUserName() + "'");
					}else{
						applyToParentBrandQuery.append(" producer_uname = NULL");
					}
					updateToParentBrand = true;
				}
				if(issuerDTO.isProducerPortalPwdApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					if(issuerObj.getProducerPassword() != null){
						applyToParentBrandQuery.append(" producer_password = '" + issuerObj.getProducerPassword() + "'");
					}else{
						applyToParentBrandQuery.append(" producer_password = NULL");
					}
					
					updateToParentBrand = true;
				}
				if(issuerDTO.isCarrierApplicationUrlApplyToParentBand() == true ){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" application_url = '" + issuerObj.getApplicationUrl() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isLoginInRequiredApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" login_required = '" + issuerObj.getLoginRequired() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isOnExchangeDisclaimerApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" ON_EXCHANGE_DISCLAIMER = '" + issuerObj.getOnExchangeDisclaimer() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isOffExchangeDisclaimerApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" OFF_EXCHANGE_DISCLAIMER = '" + issuerObj.getOffExchangeDisclaimer() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isCompanyAddress1ApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" company_address_line1 = '" + issuerObj.getCompanyAddressLine1() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isCompanyAddress2ApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" company_address_line2 = '" + issuerObj.getCompanyAddressLine2() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isCompanyCityApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" company_city = '" + issuerObj.getCompanyCity() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isCompanyStateApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" company_state = '" + issuerObj.getCompanyState() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isCompanyZipApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" company_zip = '" + issuerObj.getCompanyZip() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isWebsiteUrlApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" site_url = '" + issuerObj.getSiteUrl() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isCustomerWebsiteUrlApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" company_site_url = '" + issuerObj.getCompanySiteUrl() + "'");
					updateToParentBrand = true;
				}
				
				// ------------- update company profile end here ----------------------------//
				
				// ------------- update indv market profile start here ----------------------------//
				
				if(issuerDTO.isIndvMktCustServicePhoneNumberApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" indv_cust_service_phone = '" + issuerObj.getIndvCustServicePhone() + "'");
					applyToParentBrandQuery.append(", indv_cust_service_phone_ext = '" + issuerObj.getIndvCustServicePhoneExt() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktCustServiceTollFreePhoneNumberApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" indv_cust_service_toll_free = '" + issuerObj.getIndvCustServiceTollFree() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktCustServiceTTYApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" indv_cust_service_tty = '" + issuerObj.getIndvCustServiceTTY() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktCustWebsiteUrlApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" indv_site_url = '" + issuerObj.getIndvSiteUrl() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktAppStatusDeptPhoneNumberApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" APP_STATUS_DEPT_PHONE_NUMBER = '" + issuerObj.getAppStatusDeptPhoneNumber() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktCommContactNameApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" COMMISSIONS_CONTACT_NAME = '" + issuerObj.getCommissionsContactName() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktCommPhoneNumberApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" COMMISSIONS_PHONE_NUMBER = '" + issuerObj.getCommissionsPhoneNumber() + "'");
					applyToParentBrandQuery.append(", COMMISSIONS_PHONE_NUMBER_EXT = '" + issuerObj.getCommissionsPhoneNumberExt() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktCommEmailApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" COMMISSIONS_EMAIL_ID = '" + issuerObj.getCommissionsEmailId() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktCommPortalUrlApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" COMMISSION_PORTAL_URL = '" + issuerObj.getCommissionPortalUrl() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktCommPortalUnameApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" COMMISSION_PORTAL_USERNAME = '" + issuerObj.getCommissionPortalUserName() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktCommPortalPwdApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" COMMISSION_PORTAL_PASSWORD = '" + issuerObj.getCommissionPortalPassword() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktPcdPhoneNumberApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" PCD_PHONE_NUMBER = '" + issuerObj.getPcdPhoneNumber() + "'");
					applyToParentBrandQuery.append(", PCD_PHONE_NUMBER_EXT = '" + issuerObj.getPcdPhoneNumberExt() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktPcdFAXApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" PCD_FAX_NUMBER = '" + issuerObj.getPcdFaxNumber() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktPcdEmailApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" PCD_EMAIL_ID = '" + issuerObj.getPcdEmailId() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktPcdStreetAddress1ApplyToParentBand() == true || issuerDTO.isIndvMktPcdStreetAddress2ApplyToParentBand() == true || 
						issuerDTO.isIndvMktPcdStateApplyToParentBand() == true || issuerDTO.isIndvMktPcdZipApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" PCD_LOCATION_ID = '" + issuerObj.getLocation().getId() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktBrokerNameApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" BROKER_CONTACT_NAME = '" + issuerObj.getBrokerContactName() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktBrokerPhoneNumberApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" BROKER_PHONE_NUMBER = '" + issuerObj.getBrokerPhoneNumber() + "'");
					applyToParentBrandQuery.append(", BROKER_PHONE_NUMBER_EXT = '" + issuerObj.getBrokerPhoneNumberExt() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktBrokerEmailIdApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" BROKER_EMAIL_ID = '" + issuerObj.getBrokerEmailId() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktAgencyRepoPortalURLApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" AGENCY_REPO_PORTAL_URL = '" + issuerObj.getAgencyRepoPortalUrl() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktBrokerReportingPortalUnameApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" AGENCY_REPO_PORTAL_USERNAME = '" + issuerObj.getAgencyRepoPortalUserName() + "'");
					updateToParentBrand = true;
				}
				if(issuerDTO.isIndvMktBrokerReportingPortalPwdApplyToParentBand() == true){
					if(updateToParentBrand) 
					 applyToParentBrandQuery.append(",");
					applyToParentBrandQuery.append(" AGENCY_REPO_PORTAL_PASSWORD = '" + issuerObj.getAgencyRepoPortalPassword() + "'");
					updateToParentBrand = true;
				}
					
				applyToParentBrandQuery.append(" WHERE brand_name_id = " + issuerObj.getIssuerBrandName().getId());
				applyToParentBrandQuery.append(" AND hios_issuer_id != " + issuerObj.getHiosIssuerId());
					
				// if updateToParentBrand true, then execute query 
				if(updateToParentBrand){
					//LOGGER.info("APPLY SQL = " + applyToParentBrandQuery.toString());
					entityManager = emf.createEntityManager();
					if(!entityManager.getTransaction().isActive()){
						entityManager.getTransaction().begin();
					}
					
					Query query = entityManager.createNativeQuery(applyToParentBrandQuery.toString());
					int queryResult = query.executeUpdate();
					entityManager.getTransaction().commit();
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("# of records update : " + queryResult);
					}
				}	
				
			}
			
		}
		catch(Exception ex)
		{
			LOGGER.error(" Exception occurs inside applyToParentBrand() ", ex); 
			if(entityManager.getTransaction().isActive()){
				entityManager.getTransaction().rollback();
			}
		}
		finally
		{
			if(entityManager != null && entityManager.isOpen())
			{
				try {
					entityManager.close();
				}
				catch(IllegalStateException ex)
				{
					LOGGER.error("EntityManager is managed by application server", ex);
					
				}
			}
		}
		
	}
	
	/* ##### implementation of get Medicare issuer list as per search criteria   ##### */
	@Override
	public IssuerSearchResponseDTO getMedicareIssuerList(IssuerSearchRequestDTO request) {

		IssuerSearchResponseDTO issuerSearchResponseDTO = new IssuerSearchResponseDTO();
		issuerSearchResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
		
		try {
			
			String sortBy = PlanMgmtConstants.EMPTY_STRING;
			String sortOrder = PlanMgmtConstants.ASC;
			Integer pageSize = GhixConstants.PAGE_SIZE;
			Integer startRecord = 0;
			
			String issuerName = PlanMgmtConstants.ALL;			
			String state = PlanMgmtConstants.ALL;			
			String hiosIssuerId = PlanMgmtConstants.ALL;			
			
			if(StringUtils.isNotBlank(request.getSortBy())){
				sortBy = request.getSortBy().trim();;
			}	
			
			if(StringUtils.isNotBlank(request.getIssuerName())){
				issuerName = request.getIssuerName().trim();
			}
			if(StringUtils.isNotBlank(request.getState())){
				state = request.getState().trim();
			}
			if(StringUtils.isNotBlank(request.getHiosId())){
				hiosIssuerId = request.getHiosId().trim();
			}
			
			if(StringUtils.isNotBlank(request.getPageNumber())){
				startRecord = (Integer.parseInt(request.getPageNumber()) - 1) * GhixConstants.PAGE_SIZE;
			}
			
			if (StringUtils.isNotBlank(request.getSortOrder())
					&& request.getSortOrder().trim().equalsIgnoreCase(PlanMgmtConstants.ASC)) {
				sortOrder = PlanMgmtConstants.ASC;
			}
			else if (StringUtils.isNotBlank(request.getSortOrder())
					&& request.getSortOrder().trim().equalsIgnoreCase(PlanMgmtConstants.DESC)) {
				sortOrder = PlanMgmtConstants.DESC;
			}
			issuerSearchResponseDTO = issuerServiceHelper.getMedicareIssuerList(pageSize, startRecord, sortBy, sortOrder, 
					issuerName, state, hiosIssuerId);
		}
		catch(Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute getMedicareIssuerList ", ex);
			issuerSearchResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
			issuerSearchResponseDTO.setErrorCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
			issuerSearchResponseDTO.setErrorMsg(PlanMgmtConstants.ERROR_TECHNICAL);
		}
		issuerSearchResponseDTO.endResponse();
		return issuerSearchResponseDTO;
	}


	@Override
	public List<IssuerLightDTO> getIssuerListForIssuerRep(String issuerRepId) {
		if(StringUtils.isNoneBlank(issuerRepId)) {
			String buildquery = "select i.id,i.name,i.hios_issuer_id,i.state_of_domicile from pm_issuer_issuerrep pm_rp inner join issuers i on pm_rp.issuer_id = i.id where pm_rp.issuer_rep_id = :id";
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("build query for getIssuerListForIssuerRep : " + buildquery);
			}
			return getIssuerList(buildquery, issuerRepId);
		}
		return null;
	}
	
	@Override
	public List<IssuerLightDTO> getIssuerListForUser(String issuerRepUserId) {
		if(StringUtils.isNoneBlank(issuerRepUserId)) {
			String buildquery = "select i.id,i.name,i.hios_issuer_id,i.state_of_domicile from pm_issuer_issuerrep pm_rp, issuers i where (pm_rp.hios_issuer_id = i.hios_issuer_id or pm_rp.issuer_id = i.id) and pm_rp.issuer_rep_id = (select id from issuer_representative where user_id =:id)";

			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("build query for getIssuerListForUser : " + buildquery);
			}
			return getIssuerList(buildquery, issuerRepUserId);
		}
		return null;
	}
	
	private List<IssuerLightDTO> getIssuerList(String query, String id) {
		List<IssuerLightDTO> issuersList = null;
		EntityManager entityManager = null;
		try {
			entityManager = emf.createEntityManager();
			Query buildquery = entityManager.createNativeQuery(query);
			buildquery.setParameter("id", id);
			List<?> issuerList = buildquery.getResultList();
			if (!CollectionUtils.isEmpty(issuerList)) {
				Iterator<?> rsIterator = issuerList.iterator();
				issuersList = new ArrayList<IssuerLightDTO>();
				IssuerLightDTO issuerDTO = null;
	
				while (rsIterator.hasNext()) {
					Object[] objArray = (Object[]) rsIterator.next();
					if (objArray[PlanMgmtConstants.ONE] != null) {
						issuerDTO = new IssuerLightDTO();
						issuerDTO.setIssuerId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
						issuerDTO.setIssuerName(objArray[PlanMgmtConstants.ONE].toString());
						issuerDTO.setHiosId(objArray[PlanMgmtConstants.TWO].toString());
						issuerDTO.setStateOfDomicile((objArray[PlanMgmtConstants.THREE] != null ? objArray[PlanMgmtConstants.THREE].toString() : PlanMgmtConstants.EMPTY_STRING));
						issuersList.add(issuerDTO);
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured @ getIssuersForIssuerRepresentative: ", ex);
		}
		finally
		{
			if(entityManager != null && entityManager.isOpen())
			{
				try {
					entityManager.close();
				}
				catch(IllegalStateException ex)
				{
					LOGGER.error("EntityManager is managed by application server", ex);
				}
			}
		}
		return issuersList;
	}

	@Override
	public IssuerResponseDTO getIssuerByIssuerRepId(String issuerRepId) {
		String hiosIssuerId = null;
		if(StringUtils.isNumeric(issuerRepId)) {
			IssuerRepresentative issuerRep = iIssuerRepresentativeRepository.findByUserId(Integer.valueOf(issuerRepId));
			if(null != issuerRep) {
				hiosIssuerId = issuerRep.getIssuer().getHiosIssuerId();
			}
		}
		return getIssuerByHiosId(hiosIssuerId);
	}
}
