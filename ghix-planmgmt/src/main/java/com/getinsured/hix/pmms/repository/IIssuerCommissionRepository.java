package com.getinsured.hix.pmms.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.IssuerCommission;

@Repository("iIssuerCommissionRepository")
public interface IIssuerCommissionRepository extends
		JpaRepository<IssuerCommission, Integer>,
		RevisionRepository<IssuerCommission, Integer, Integer> {

	@Modifying
	@Transactional
	@Query("DELETE FROM IssuerCommission ic WHERE ic.issuer.id = :issuerId AND ic.insuranceType = :insuranceType AND :scheduleStartDate <= ic.scheduleStartDate")
	int deleteFutureRecords(@Param("issuerId") Integer issuerId,
			@Param("insuranceType") String insuranceType,
			@Param("scheduleStartDate") Date scheduleStartDate);

	@Query("SELECT ic FROM IssuerCommission ic WHERE ic.issuer.id = :issuerId AND UPPER(ic.insuranceType) = UPPER(:insuranceType) AND ( (:scheduleStartDate >= ic.scheduleStartDate AND ic.scheduleEndDate IS NULL) OR (:scheduleStartDate BETWEEN ic.scheduleStartDate AND ic.scheduleEndDate) ) ORDER BY ic.lastUpdateTimestamp DESC ")
	IssuerCommission getRecordsBetweenDateRange(
			@Param("issuerId") Integer issuerId,
			@Param("insuranceType") String insuranceType,
			@Param("scheduleStartDate") Date scheduleStartDate);
	
	@Query("SELECT ic from IssuerCommission ic WHERE ic.issuer.id = :issuerId ORDER BY ic.lastUpdateTimestamp DESC ")
	List<IssuerCommission> getByIssuerId(@Param("issuerId") Integer issuerId);
	
}
