package com.getinsured.hix.pmms.service.validation;

import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.COMMA;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_EMPTY;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_INVALID_VALUE;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_REQUEST;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_TEXT_LENGTH_NOT_MATCHED;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.setErrorMessage;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.setStatusResponse;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateAgainstAllowedValue;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateBlankData;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateHiosIssuerId;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateNumeric;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.validateState;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffBatchSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffPlanMgmtBatchDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferSearchRequestDTO;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes.ErrorCode;
import com.getinsured.hix.pmms.repository.IIssuerRepository;

@Component
public final class SerffAPIRequestValidator {

	private static final Logger LOGGER = Logger.getLogger(SerffAPIRequestValidator.class);
	@Autowired private IIssuerRepository iIssuerRepository;

	public SerffAPIRequestValidator() {
	}

	public boolean validateSerffTransferSearchRequest(SerffTransferSearchRequestDTO serffTransferSearchRequestDTO, GhixResponseDTO responseDTO) {
		boolean valid = false;
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		if(null == serffTransferSearchRequestDTO) {
			setErrorMessage(errorVOList, null, null, EMSG_REQUEST);
		} else {
			valid = true;
			if(StringUtils.isNotBlank(serffTransferSearchRequestDTO.getPageNumber())) {
				valid = valid && validateNumeric(serffTransferSearchRequestDTO.getPageNumber(), errorVOList, "Page number should be a numeric value greater than 0");
			}
			if(StringUtils.isNotBlank(serffTransferSearchRequestDTO.getHiosIssuerID())) {
				valid = valid && validateHiosIssuerIdForAPI(serffTransferSearchRequestDTO.getHiosIssuerID(), errorVOList, "HIOS Issuer ID: ");
			}
			if(StringUtils.isNotBlank(serffTransferSearchRequestDTO.getStatus())) {
				valid = valid && (validateAgainstAllowedValue(serffTransferSearchRequestDTO.getStatus(), errorVOList, "Status Filter (P/F/S)", "P,F,S"));
			}
			if (StringUtils.isNotBlank(serffTransferSearchRequestDTO.getStartDate())) {
				SimpleDateFormat inSDF = new SimpleDateFormat("MM/dd/yyyy");

				try {
					inSDF.setLenient(false);
					inSDF.parse(serffTransferSearchRequestDTO.getStartDate());
				}
				catch (ParseException e) {
					valid = false;
					setErrorMessage(errorVOList, "Date Filter:", serffTransferSearchRequestDTO.getStartDate(), "Invalid date, expected date in format MM/DD/YYYY");
				}
			}
		}
		if(!valid) {
			setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), responseDTO);
		}
		return valid;
	}

	public boolean validateSerffBatchSearchRequest(List<PlanMgmtErrorVO> errorVOList, SerffBatchSearchRequestDTO requestDTO, GhixResponseDTO responseDTO) {

		LOGGER.debug("validateSerffBatchSearchRequest() Start");
		boolean valid = false;

		try {
			if (null == requestDTO) {
				setErrorMessage(errorVOList, null, null, EMSG_REQUEST);
			}
			else {

				if (StringUtils.isNotBlank(requestDTO.getPageNumber())) {
					validateNumeric(requestDTO.getPageNumber(), errorVOList, "Page number should be a numeric value greater than 0");
				}

				if (StringUtils.isNotBlank(requestDTO.getHiosIssuerID())) {
					validateHiosIssuerIdForAPI(requestDTO.getHiosIssuerID(), errorVOList, "HIOS Issuer ID");
				}

				if (StringUtils.isNotBlank(requestDTO.getStatus())) {
					validateBatchStatus(requestDTO.getStatus(), errorVOList, "Batch Status");
				}

				if (StringUtils.isNotBlank(requestDTO.getUploadDate())) {
					SimpleDateFormat inSDF = new SimpleDateFormat("MM/dd/yyyy");
	
					try {
						inSDF.setLenient(false);
						inSDF.parse(requestDTO.getUploadDate());
					}
					catch (ParseException e) {
						setErrorMessage(errorVOList, "Date Filter:", requestDTO.getUploadDate(), "Invalid date, expected date in format MM/DD/YYYY");
					}
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Exception occurred in validateRequestToUpdateSerffBatchRecord: ", ex);
			setErrorMessage(errorVOList, null, null, ex.getMessage());
		}
		finally {

			if (!CollectionUtils.isEmpty(errorVOList)) {
				setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), responseDTO);
			}
			else {
				valid = true;
			}
			LOGGER.debug("validateSerffBatchSearchRequest() End with valid: " + valid);
		}
		return valid;
	}

	/**
	 * Method is used to validate HIOS Issuer Id Value.
	 */
	private boolean validateHiosIssuerIdForAPI(String hiosIssuerId, List<PlanMgmtErrorVO> errorVOList, String fieldName) {
		boolean validHiosIssuerId = true;

		if (StringUtils.isBlank(hiosIssuerId)) {
			setErrorMessage(errorVOList, fieldName, hiosIssuerId, EMSG_EMPTY);
			validHiosIssuerId = false;
		}
		else if (!validateHiosIssuerId(hiosIssuerId)) {
			setErrorMessage(errorVOList, fieldName, hiosIssuerId, EMSG_TEXT_LENGTH_NOT_MATCHED);
			validHiosIssuerId = false;
		}
		return validHiosIssuerId;
	}

	/**
	 * Method is used to validate SerffPlanMgmtBatchDTO to create SERFF Batch Record.
	 */
	public boolean validateRequestToCreateSerffBatchRecord(SerffPlanMgmtBatchDTO batchDTO, GhixResponseDTO ghixResponse,
			List<PlanMgmtErrorVO> errorVOList) {

		LOGGER.debug("validateRequestToCreateSerffBatchRecord() Start");
		boolean valid = false;

		try {
			if (null == batchDTO) {
				setErrorMessage(errorVOList, "SerffPlanMgmtBatchDTO", null, EMSG_REQUEST);
				return valid;
			}

			if (validateBlankData(batchDTO.getIssuerId(), errorVOList, "Issuer ID")) {
				validateHiosIssuerId(batchDTO.getIssuerId(), errorVOList, "Issuer ID", iIssuerRepository);
			}
			validateBlankData(batchDTO.getCareer(), errorVOList, "Carrier name");

			if (StringUtils.isNotBlank(batchDTO.getState()) && !validateState(batchDTO.getState())) {
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "State", batchDTO.getState(), EMSG_INVALID_VALUE);
			}
			validateExchangeType(batchDTO.getExchangeType(), errorVOList, "Exchange Type");

			if (validateBlankData(batchDTO.getDefaultTenant(), errorVOList, "Tenant")) {

				for (String tenantID : batchDTO.getDefaultTenant().split(COMMA)) {
					validateNumeric(tenantID, errorVOList, "Tenant");
				}
			}
			validateBlankData(batchDTO.getOperationType(), errorVOList, "Operation Type");

			if (0 >= batchDTO.getUserId()) {
				setErrorMessage(errorVOList, "User ID", batchDTO.getUserId() + StringUtils.EMPTY, EMSG_INVALID_VALUE);
			}
		}
		catch (Exception ex) {
			LOGGER.error("Exception occurred in validateRequestToCreateSerffBatchRecord: ", ex);
			setErrorMessage(errorVOList, null, null, ex.getMessage());
		}
		finally {

			if (!CollectionUtils.isEmpty(errorVOList)) {
				setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponse);
			}
			else {
				valid = true;
			}
			LOGGER.debug("validateRequestToCreateSerffBatchRecord() End");
		}
		return valid;
	}

	/**
	 * Method is used to validate SerffPlanMgmtBatchDTO to update SERFF Batch Record.
	 */
	public boolean validateRequestToUpdateSerffBatchRecord(SerffPlanMgmtBatchDTO batchDTO, GhixResponseDTO ghixResponse,
			List<PlanMgmtErrorVO> errorVOList) {

		LOGGER.debug("validateRequestToUpdateSerffBatchRecord() Start");
		boolean valid = false;

		try {
			if (null == batchDTO) {
				setErrorMessage(errorVOList, "SerffPlanMgmtBatchDTO", null, EMSG_REQUEST);
				return valid;
			}

			if (0 >= batchDTO.getId()) {
				setErrorMessage(errorVOList, "ID", batchDTO.getId() + StringUtils.EMPTY, EMSG_INVALID_VALUE);
			}
			validateFtpStatus(batchDTO.getFtpStatus(), errorVOList, "FTP Status");
			validateBatchStatus(batchDTO.getBatchStatus(), errorVOList, "Batch Status");
		}
		catch (Exception ex) {
			LOGGER.error("Exception occurred in validateRequestToUpdateSerffBatchRecord: ", ex);
			setErrorMessage(errorVOList, null, null, ex.getMessage());
		}
		finally {

			if (!CollectionUtils.isEmpty(errorVOList)) {
				setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponse);
			}
			else {
				valid = true;
			}
			LOGGER.debug("validateRequestToUpdateSerffBatchRecord() End");
		}
		return valid;
	}

	/**
	 * Method is used to validate Exchange Type.
	 */
	private void validateExchangeType(String exchangeType, List<PlanMgmtErrorVO> errorVOList, String fieldName) {

		if (StringUtils.isBlank(exchangeType)) {
			setErrorMessage(errorVOList, fieldName, exchangeType, EMSG_EMPTY);
		}
		else {

			try {
				SerffPlanMgmtBatch.EXCHANGE_TYPE.valueOf(exchangeType);
			}
			catch (IllegalArgumentException ex) {
				LOGGER.error(ex.getMessage());
				setErrorMessage(errorVOList, fieldName, exchangeType, EMSG_INVALID_VALUE);
			}
		}
	}

	/**
	 * Method is used to validate Ftp Status.
	 */
	private void validateFtpStatus(String ftpStatus, List<PlanMgmtErrorVO> errorVOList, String fieldName) {

		if (StringUtils.isBlank(ftpStatus)) {
			setErrorMessage(errorVOList, fieldName, ftpStatus, EMSG_EMPTY);
		}
		else {

			try {
				SerffPlanMgmtBatch.FTP_STATUS.valueOf(ftpStatus);
			}
			catch (IllegalArgumentException ex) {
				LOGGER.error(ex.getMessage());
				setErrorMessage(errorVOList, fieldName, ftpStatus, EMSG_INVALID_VALUE);
			}
		}
	}

	/**
	 * Method is used to validate Batch Status.
	 */
	private void validateBatchStatus(String batchStatus, List<PlanMgmtErrorVO> errorVOList, String fieldName) {

		if (StringUtils.isBlank(batchStatus)) {
			setErrorMessage(errorVOList, fieldName, batchStatus, EMSG_EMPTY);
		}
		else {

			try {
				SerffPlanMgmtBatch.BATCH_STATUS.valueOf(batchStatus);
			}
			catch (IllegalArgumentException ex) {
				LOGGER.error(ex.getMessage());
				setErrorMessage(errorVOList, fieldName, batchStatus, EMSG_INVALID_VALUE);
			}
		}
	}

	/**
	 * Method is used to validate PlanDocumentsJobDTO to create Plan Documents Record.
	 */
	public boolean validateRequestToCreatePlanDocumentsRecord(PlanDocumentsJobDTO planDocumentsJobDTO, GhixResponseDTO ghixResponse,
			List<PlanMgmtErrorVO> errorVOList) {

		LOGGER.debug("validateRequestToCreatePlanDocumentsRecord() Start");
		boolean valid = false;

		try {
			if (null == planDocumentsJobDTO) {
				setErrorMessage(errorVOList, "PlanDocumentsJobDTO", null, EMSG_REQUEST);
				return valid;
			}

			if (StringUtils.isNotBlank(planDocumentsJobDTO.getDocumentType())
					&& 1 < planDocumentsJobDTO.getDocumentType().trim().length()) {
				setErrorMessage(errorVOList, "Document Type", planDocumentsJobDTO.getDocumentType(), EMSG_INVALID_VALUE);
			}
			validateBlankData(planDocumentsJobDTO.getPlanId(), errorVOList, "Plan Id");
			validateBlankData(planDocumentsJobDTO.getHiosProductId(), errorVOList, "HIOS Product ID");
			validateBlankData(planDocumentsJobDTO.getIssuerPlanNumber(), errorVOList, "Issuer Plan Number");
			validateFtpStatus(planDocumentsJobDTO.getFtpStatus(), errorVOList, "FTP Status");
		}
		catch (Exception ex) {
			LOGGER.error("Exception occurred in validateRequestToUpdateSerffBatchRecord: ", ex);
			setErrorMessage(errorVOList, null, null, ex.getMessage());
		}
		finally {

			if (!CollectionUtils.isEmpty(errorVOList)) {
				setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponse);
			}
			else {
				valid = true;
			}
			LOGGER.debug("validateRequestToCreatePlanDocumentsRecord() End");
		}
		return valid;
	}

	public boolean validatePlanDocumentsJobRequest(PlanDocumentsJobRequestDTO planDocumentsJobRequestDTO, GhixResponseDTO ghixResponse) {

		boolean valid = false;
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		try {
			if (null == planDocumentsJobRequestDTO) {
				setErrorMessage(errorVOList, null, null, EMSG_REQUEST);
			}
			else {

				if (StringUtils.isNotBlank(planDocumentsJobRequestDTO.getPageNumber())) {
					validateNumeric(planDocumentsJobRequestDTO.getPageNumber(), errorVOList,
							"Page number should be a numeric value greater than 0");
				}

				if (StringUtils.isNotBlank(planDocumentsJobRequestDTO.getLastUpdatedDate())) {
					SimpleDateFormat inSDF = new SimpleDateFormat("MM/dd/yyyy");

					try {
						inSDF.setLenient(false);
						inSDF.parse(planDocumentsJobRequestDTO.getLastUpdatedDate());
					}
					catch (ParseException e) {
						setErrorMessage(errorVOList, "Date Filter: ", planDocumentsJobRequestDTO.getLastUpdatedDate(), "Invalid date, expected date in format MM/DD/YYYY");
					}
				}

				if (StringUtils.isNotBlank(planDocumentsJobRequestDTO.getStatus())) {
					validateFtpStatus(planDocumentsJobRequestDTO.getStatus(), errorVOList, "FTP Status (IN_PROGRESS, COMPLETED, FAILED): ");
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Exception occurred in validatePlanDocumentsJobRequest: ", ex);
			setErrorMessage(errorVOList, null, null, ex.getMessage());
		}
		finally {

			if (!CollectionUtils.isEmpty(errorVOList)) {
				setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), ghixResponse);
			}
			else {
				valid = true;
			}
		}
		return valid;
	}
}
