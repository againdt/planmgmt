package com.getinsured.hix.pmms.service.jpa;

import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.EMSG_NOT_EXIST_IN_DB;
import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.setErrorMessage;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerBrandNameResponseDTO;
import com.getinsured.hix.model.IssuerBrandName;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes.ErrorCode;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.repository.IIssuerBrandNameRepository;
import com.getinsured.hix.pmms.repository.IIssuerRepository;
import com.getinsured.hix.pmms.service.IssuerBrandNameService;
import com.getinsured.hix.pmms.service.validation.IssuerAPIRequestValidator;
import com.getinsured.hix.pmms.service.validation.PlanMgmtErrorVO;
import com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil;

/**
 * Class is used to provide service to Issuer Brand Name APIs [Plan Management-Microservice]
 * @since Jan 17, 2017
 */
@Service("pmmsIssuerBrandNameService")
public class IssuerBrandNameServiceImpl implements IssuerBrandNameService {

	private static final Logger LOGGER = Logger.getLogger(IssuerBrandNameServiceImpl.class);

	@Autowired private GIExceptionHandler giExceptionHandler;
	@Autowired private IIssuerBrandNameRepository iIssuerBrandNameRepository;
	@Autowired private IssuerAPIRequestValidator issuerAPIRequestValidator;
	@Autowired private IIssuerRepository iIssuerRepository;

	/**
	 * Method is used to get Issuer Brand Name List from database.
	 */
	@Override
	public IssuerBrandNameResponseDTO getIssuerBrandNameList() {

		LOGGER.debug("getIssuerBrandNameList() Start");

		IssuerBrandNameResponseDTO brandNameResponseDTO = new IssuerBrandNameResponseDTO();

		try {
			List<IssuerBrandName> issuerBrandNameListFromDB = iIssuerBrandNameRepository.getIssuerBrandNameList();

			if (CollectionUtils.isEmpty(issuerBrandNameListFromDB)) {
				brandNameResponseDTO.setErrorMsg("There is no record available in Issuer Brand Name table.");
				brandNameResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
				LOGGER.error(brandNameResponseDTO.getErrorMsg());
				return brandNameResponseDTO;
			}
			IssuerBrandNameDTO brandNameDTO = null;

			for (IssuerBrandName brandNameModel : issuerBrandNameListFromDB) {
				brandNameDTO = new IssuerBrandNameDTO();
				brandNameDTO.setId(brandNameModel.getId());
				brandNameDTO.setBrandId(brandNameModel.getBrandId());
				brandNameDTO.setBrandName(brandNameModel.getBrandName());
				brandNameDTO.setBrandUrl(StringUtils.isNotBlank(brandNameModel.getBrandUrl()) ? brandNameModel.getBrandUrl() : null);
				brandNameDTO.setCreateBy(null != brandNameModel.getCreatedBy() ? brandNameModel.getCreatedBy().toString() : null);
				brandNameDTO.setLastUpdatedBy(null != brandNameModel.getLastUpdatedBy() ? brandNameModel.getLastUpdatedBy().toString() : null);
				brandNameResponseDTO.setIssuerBrandNameDTO(brandNameDTO);
			}
			brandNameResponseDTO.setStatus(GhixResponseDTO.STATUS.SUCCESS.name());
		}
		catch(Exception ex) {
			brandNameResponseDTO.setErrorMsg("Exception occurred while fetching record from IssuerBrandName table.");
			brandNameResponseDTO.setStatus(GhixResponseDTO.STATUS.FAILED.name());
			LOGGER.error(brandNameResponseDTO.getErrorMsg());
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.GET_BRAND_NAME_LIST_EXCEPTION.code),
					"Exception occurred in getIssuerBrandNameList() service: ", ex);
		}
		finally {
			LOGGER.debug("getIssuerBrandNameList() End");
		}
		return brandNameResponseDTO;
	}

	/**
	 * Method is used to save record in IssuerBrandName table.
	 */
	@Override
	public GhixResponseDTO addIssuerBrandNameInDB(IssuerBrandNameDTO brandNameDTO) {

		LOGGER.debug("addIssuerBrandNameInDB() Start");
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		GhixResponseDTO ghixResponse = issuerAPIRequestValidator.validateIssuerBrandNameDTO(brandNameDTO, errorVOList);

		try {

			if (GhixResponseDTO.STATUS.FAILED.name().equals(ghixResponse.getStatus())) {
				return ghixResponse;
			}

			if (null != brandNameDTO) {
				IssuerBrandName newIssuerBrandName = new IssuerBrandName();
				newIssuerBrandName.setBrandName(brandNameDTO.getBrandName());
				newIssuerBrandName.setIsDeleted(PlanMgmtConstants.N);
				newIssuerBrandName.setBrandId(brandNameDTO.getBrandId());
				newIssuerBrandName.setBrandUrl(StringUtils.isNotBlank(brandNameDTO.getBrandUrl()) ? brandNameDTO.getBrandUrl() : null);
				newIssuerBrandName.setCreatedBy(Integer.valueOf(brandNameDTO.getCreateBy()));
				iIssuerBrandNameRepository.save(newIssuerBrandName);
			}
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while inserting record in IssuerBrandName table");
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.ADD_NEW_ISSUER_BRAND_NAME_EXCEPTION.code),
					"Exception occurred in addIssuerBrandNameInDB() service: ", ex);
		}
		finally {
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.ADD_NEW_ISSUER_BRAND_NAME_EXCEPTION.getCode(), ghixResponse);
			LOGGER.debug("addIssuerBrandNameInDB() End");
		}
		return ghixResponse;
	}

	@Override
	public IssuerBrandName getIssuerBrandNameById(Integer issuerBrandNameId){
		if (iIssuerBrandNameRepository.exists(issuerBrandNameId)) {
			return iIssuerBrandNameRepository.findOne(issuerBrandNameId);
		}
		return null;
	}

	/**
	 * Method is used to bulk update Brand-Id in Issuer Table.
	 */
	@Override
	public GhixResponseDTO bulkUpdateIssuerBrandId(IssuerBrandNameRequestDTO brandNameRequestDTO) {

		LOGGER.debug("bulkUpdateIssuerBrandId() Start");
		GhixResponseDTO ghixResponse = new GhixResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		try {
			if (!issuerAPIRequestValidator.validateIssuerBrandNameRequestDTO(brandNameRequestDTO, ghixResponse, errorVOList)) {
				return ghixResponse;
			}

			IssuerBrandName existingBrandName = iIssuerBrandNameRepository.findByIdAndIsDeleted(brandNameRequestDTO.getId(), PlanMgmtConstants.N);
			if (null != existingBrandName) {
				Integer totalUpdatedBrandId = iIssuerRepository.bulkUpdateIssuerBrandId(existingBrandName.getId(),
								Integer.valueOf(brandNameRequestDTO.getLastUpdatedBy()), brandNameRequestDTO.getHiosIssuerIdList());

				if (LOGGER.isDebugEnabled() && totalUpdatedBrandId > 0) {
					LOGGER.debug("Total updated Brand ID in Issuer table is " + totalUpdatedBrandId);
				}
			}
			else {
				setErrorMessage(errorVOList, "Internal ID", brandNameRequestDTO.getId().toString(), EMSG_NOT_EXIST_IN_DB);
			}
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while updating Brand ID in Issuer table.");
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.BULK_UPDATE_ISSUER_BRAND_NAME_EXCEPTION.code),
					"Exception occurred in bulkUpdateIssuerBrandId() service: ", ex);
		}
		finally {
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.BULK_UPDATE_ISSUER_BRAND_NAME_EXCEPTION.getCode(), ghixResponse);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("bulkUpdateIssuerBrandId() End with status: " + ghixResponse.getStatus());
			}
		}
		return ghixResponse;
	}
	
	@Override
	public List<IssuerBrandName> getIssuerBrandNameByIsDeleted(){
		
			return iIssuerBrandNameRepository.findByIsDeletedOrderByIdAsc("N");
		
	}
	
	@Override
	public boolean isDuplicateExist(IssuerBrandNameDTO brandNameDTO) {
		Integer existingBrandNameCount = null;
		Integer existingBrandIdCount = null;
		boolean hasDuplicate = false;
		
		if(StringUtils.isNotBlank(brandNameDTO.getBrandId())) {
			existingBrandNameCount = iIssuerBrandNameRepository.countIssuerBrandID(brandNameDTO.getBrandId());
		}
		if(StringUtils.isNotBlank(brandNameDTO.getBrandName())) {
			existingBrandIdCount = iIssuerBrandNameRepository.countIssuerBrandName(brandNameDTO.getBrandName());
		}
		
		if ((null != existingBrandNameCount &&  existingBrandNameCount > 0)||
				(null != existingBrandIdCount &&  existingBrandIdCount > 0)){
			hasDuplicate = true;
		}else {
			hasDuplicate = false;
		}
		
		return hasDuplicate;
	}
}
