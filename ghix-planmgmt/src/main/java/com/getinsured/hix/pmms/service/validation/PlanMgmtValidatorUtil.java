package com.getinsured.hix.pmms.service.validation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.pmms.repository.IIssuerRepository;
import com.serff.template.admin.niem.usps.states.USStateCodeSimpleType;

/**
 * This class is going to have all generic validations required for Plan Management APIs.
 * @since January 4, 2017
 */
public final class PlanMgmtValidatorUtil {

	private static final Logger LOGGER = Logger.getLogger(PlanMgmtValidatorUtil.class);

	public static final String OPTION_VALUES = "yes,no,y,n";
	public static final String SPACE = " ";
	public static final String COMMA = ",";
	public static final String DOLLAR = "\\$";
	public static final String PERCENTAGE = "%";
	public static final String DOLLARSIGN = "$";
	public static final String NEW_LINE = "\n";
	public static final String OPEN_BRACKET = "[";
	public static final String CLOSING_BRACKET = "]";
	public static final String HYPHEN = " - ";
	public static final String YES = "yes";
	public static final String NO = "no";
	public static final String YES_CHAR = "Y";
	public static final String NO_CHAR = "N";
	public static final String NOT_AVAILABLE = "NA";

	//HIOS issued Id for an issuer
	public static final int LEN_HIOS_ISSUER_ID = 5;
	public static final int LEN_MIN_MEDICARE_HIOS_ISSUER_ID = 5;
	public static final int LEN_MAX_MEDICARE_HIOS_ISSUER_ID = 10;
	public static final int LEN_ISSUER_STATE = 2;
	public static final int LEN_PLAN_YEAR = 4;
	public static final int LEN_MEDICAID_PLAN_ID = 16;
	public static final int LEN_STM_PLAN_ID = 7;
	public static final int LEN_AME_PLAN_ID = 7;
	public static final int LEN_LIFE_PLAN_ID = 7;
	public static final int LEN_FORMULARY_ID = 6;
	public static final int LEN_ZIP_CODE_1 = 5;
	public static final int LEN_ZIP_CODE_2 = 9;
	//Health Plan Id
	public static final int LEN_HP_ID = 10;
	//A 6-digit number that is the issuer's company identifier provided by the National Association of Insurance Commissioners (NAIC)
	public static final int LEN_NAIC_CODE = 6;
	public static final int LEN_PHONE_1 = 10;
	public static final int LEN_PHONE_2 = 11;
	public static final int LEN_CHILD_ONLY_PLAN_ID = 14;
	public static final int LEN_HIOS_PLAN_ID = 14;
	public static final int LEN_PLAN_VARIANCE = 2;
	public static final int LEN_HIOS_PRODUCT_ID = 10;
	public static final int LET_STANDARD_COMPONENT_ID = 14;
	public static final int LET_STANDARD_COMPONENT_ID_WITH_VARIANT = 17;
	public static final int LET_NETWORK_ID = 6;
	public static final int LET_SERVICE_AREA_ID = 6;
	public static final int LET_APPLICATION_NUMBER = 9;
	public static final int LET_TIN = 9;
	public static final int LEN_HPID = 10;

	public static final String PATTERN_URL = "^(https?://)?([a-zA-Z0-9_\\-])+([\\.][a-zA-Z0-9_/\\-]+)*\\.([A-Za-z0-9/]{1,5})([:][0-9]+[/]?)?([\\S ]+)?";
	public static final String PATTERN_ECM = "^(?i:workspace)(:)(\\/\\/)(?i:SpacesStore)(\\/)([A-Za-z0-9\\-])+(;)([0-9\\.]{1,5})$";
	public static final String PATTERN_EMAIL = "^([A-Za-z0-9_\\-\\.])+\\@([A-Za-z0-9_\\-\\.])+\\.([A-Za-z]{2,4})";
	public static final String PATTERN_TIN = "(^[0-9]{2})([\\-]{1})([0-9]{7})$";
	public static final String PATTERN_PLAN_ID = "(^[0-9]{5})([A-Za-z]{2})([0-9]{7})$";
	public static final String PATTERN_HIOS_PLAN_NUM = "(^[A-Za-z0-9]{5})([A-Za-z]{2})([0-9]{7})$";
	public static final String PATTERN_STANDARD_COMPONENT_ID = "(^[0-9]{5})([A-Za-z]{2})([0-9]{7})$";
	public static final String PATTERN_STANDARD_COMPONENT_ID_WITH_VARIANT = "(^[0-9]{5})([A-Za-z]{2})([0-9]{7})([\\-]{1})([0]{1}[0-6]{1})+$";
	public static final String PATTERN_VARIANT_COMPONENT_ID = "(^[0]{1}[0-6]{1}$)";
	public static final String PATTERN_FORMULARY_ID = "(^[A-Za-z]{2}[Ff])([0-9]{3})$";
	public static final String PATTERN_NETWORK_ID = "(^[A-Za-z]{2}[Nn])([0-9]{3})$";
	public static final String PATTERN_SERVICE_AREA_ID = "(^[A-Za-z]{2}[Ss])([0-9]{3})$";
	public static final String PATTERN_PM_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String PATTERN_ISSUER_BRAND_NAME = "^[a-zA-Z -]+$";
	public static final String PATTERN_ISSUER_BRAND_ID = "^[A-Z]+$";
	public static final String PATTERN_MEDICARE_HIOS_ISSUER_ID = "^[A-Z|a-z].*$";

	public static final String MARKET_COVERAGE_TYPE_SHOP = "SHOP";
	public static final String MARKET_COVERAGE_TYPE_INDIVIDUAL = "INDIVIDUAL";

	public static final DateTimeFormatter PLAN_DATE_FORMATTER = DateTimeFormat.forPattern("MM/dd/yyyy").withLocale(Locale.US);
	public static final DateTimeFormatter RATE_DATE_FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd").withLocale(Locale.US);

	public static final String EMSG_REQUEST = "Request DTO should not be empty";
	public static final String EMSG_EMPTY = "Value should not be empty";
	public static final String EMSG_INVALID_VALUE = "Invalid value";
	public static final String EMSG_CAPITAL_FORMAT = "Accepts only letter in CAPITAL format";
	public static final String EMSG_NOT_EXIST_IN_DB = "Value dose not exist in database";
	public static final String EMSG_EMPTY_LIST = " List should not be empty";
	public static final String EMSG_TEXT_LENGTH_NOT_MATCHED = "Text length is not matched with length allowed";
	public static final String EMSG_TEXT_LENGTH_EXCEEDS = "Text length is greater than maximum length allowed";
	public static final String EMSG_TEXT_LENGTH_SHORT = "Text length is too short for minimum length allowed";
	public static final int DATE_START_AND_END_INVALID = 99;
	public static final int DATE_START_AND_END_EQUAL = 0;
	public static final int DATE_START_GREATER_THAN_END = 1;
	public static final int DATE_START_LESS_THAN_END = -1;
	public static final String EMSG_FUTURE_DATE = "Should be future date";
	public static final String EMSG_GREATER_DATE = "Should be higher than other";
	

	public static void setErrorMessage(List<PlanMgmtErrorVO> errorVOList, String fieldName, String fieldValue, String errorMessage) {

		if (null == errorVOList) {
			return;
		}
		PlanMgmtErrorVO errorVO = new PlanMgmtErrorVO();
		errorVO.setFieldName(fieldName);
		errorVO.setFieldValue(fieldValue);
		errorVO.setErrorMessage(errorMessage);
		errorVOList.add(errorVO);
	}

	private static void formatErrorMessage(PlanMgmtErrorVO errorVO, int errorCounter, StringBuffer errorMessages) {
		errorMessages.append(errorCounter);
		errorMessages.append(") ");
		errorMessages.append(errorVO.getErrorMessage());
		errorMessages.append(HYPHEN);
		errorMessages.append(errorVO.getFieldName());

		if (StringUtils.isNotBlank(errorVO.getFieldValue())) {
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(errorVO.getFieldValue());
			errorMessages.append(CLOSING_BRACKET);
		}
		errorMessages.append(COMMA);
		errorMessages.append(SPACE);
	}

	public static void setStatusResponse(List<PlanMgmtErrorVO> errorVOList, Integer errorCode, GhixResponseDTO ghixResponse) {

		LOGGER.debug("setErrorResponse() Start");
		if (null != ghixResponse) {
			if (CollectionUtils.isEmpty(errorVOList)) {
				ghixResponse.setStatus(GhixResponseDTO.STATUS.SUCCESS.name());
			} else {
		
		//		ghixResponse.setId(0); TODO
				ghixResponse.setErrorCode(errorCode);
		//		ghixResponse.setExecutionDuration(0); TODO
				ghixResponse.setStatus(GhixResponseDTO.STATUS.FAILED.name());
		
				int errorCounter = 0;
				StringBuffer errorMessages = new StringBuffer();
		
				for (PlanMgmtErrorVO errorVO : errorVOList) {
					formatErrorMessage(errorVO, ++errorCounter, errorMessages);
				}
				ghixResponse.setErrorMsg(errorMessages.replace(errorMessages.lastIndexOf(COMMA), errorMessages.length(), StringUtils.EMPTY).toString());
				LOGGER.error(errorMessages.toString());
			}
		}
		LOGGER.debug("setErrorResponse() End");
	}

	/**
	 * Method is used to validate Blank Data from Value.
	 */
	public static boolean validateBlankData(String fieldValue, List<PlanMgmtErrorVO> errorVOList, String fieldName) {
		boolean valid = false;
		if (StringUtils.isBlank(fieldValue)) {
			setErrorMessage(errorVOList, fieldName, fieldValue, EMSG_EMPTY);
		} else {
			valid = true;
		}
		return valid;
	}

	/**
	 * Method is used to validate URL from Value.
	 */
	public static boolean validateURL(String fieldValue, List<PlanMgmtErrorVO> errorVOList, String fieldName) {

		boolean valid = false;

		if (StringUtils.isNotBlank(fieldValue) && !isValidPattern(PATTERN_URL, fieldValue)) {
			setErrorMessage(errorVOList, fieldName, fieldValue, EMSG_INVALID_VALUE);
		}
		else {
			valid = true;
		}
		return valid;
	}

	/**
	 * Method is used to validate numeric value.
	 */
	public static boolean validateNumeric(String fieldValue, List<PlanMgmtErrorVO> errorVOList, String fieldName) {
		boolean valid = false;
		if (!StringUtils.isNumeric(fieldValue)) {
			setErrorMessage(errorVOList, fieldName, fieldValue, EMSG_INVALID_VALUE);
		} else {
			valid = true;
		}
		return valid;
	}

	/**
	 * Method is used to validate String length range for a required field.
	 */
	public static boolean validateEmailAddress(String emailId, List<PlanMgmtErrorVO> errorVOList, String fieldName) {
		boolean valid = false;
			if (!isValidPattern(PATTERN_PM_EMAIL, emailId)) {
				setErrorMessage(errorVOList, fieldName, emailId, EMSG_INVALID_VALUE);
			} else {
				valid = true;
			}
		return valid;
	}

	
	/**
	 * Method is used to validate String length range for a required field.
	 */
	public static boolean validateMinMaxStringLength(String fieldValue, List<PlanMgmtErrorVO> errorVOList, String fieldName, int minLength, int maxLength) {
		boolean valid = false;
		if (StringUtils.isNotBlank(fieldValue)) {
			if (fieldValue.length() > maxLength) {
				setErrorMessage(errorVOList, fieldName, fieldValue, EMSG_TEXT_LENGTH_EXCEEDS);
			} else if (fieldValue.length() < minLength) {
				setErrorMessage(errorVOList, fieldName, fieldValue, EMSG_TEXT_LENGTH_SHORT);
			} else {
				valid = true;
			}
		} else {
			setErrorMessage(errorVOList, fieldName, fieldValue, EMSG_EMPTY);
		}
		return valid;
	}

	/**
	 * Method is used to validate max String length.
	 */
	public static boolean validateMaxStringLength(String fieldValue, List<PlanMgmtErrorVO> errorVOList, String fieldName, int maxLength) {
		boolean valid = false;
		if (StringUtils.isNotBlank(fieldValue)) {
			valid = validateMinMaxStringLength(fieldValue, errorVOList, fieldName, 0, maxLength) ;
		} else {
			valid = true;
		}
		return valid;
	}

	/**
	 * Method is used to validate given value with passed argument pattern.
	 * @param pattern
	 * @return True for Valid otherwise False.
	 */
	public static boolean isValidPattern(final String pattern, final String givenValue) {

		boolean isValid = false;

		if (StringUtils.isNotEmpty(pattern)
				&& StringUtils.isNotEmpty(givenValue)) {

			// Assigning the regular expression
			return givenValue.matches(pattern);
		}
		return isValid;
	}

	/**
	 * Method is used to validate length of field.
	 * @param length
	 * @param value
	 * @return True for Valid URL otherwise False.
	 */
	public static boolean isValidLength(final int length, final String value) {
		return length == StringUtils.length(value);
	}
	
	/**
	 * Validate length is within specified range.
	 * @param minLength
	 * @param maxLength
	 * @param value
	 * @return
	 */
	public static boolean isValidLength(final int minLength, final int maxLength, final String value) {
		return (StringUtils.length(value) >= minLength && StringUtils.length(value) <= maxLength);
	}

	/**
	 * Validate the date
	 * @param day
	 * @param month
	 * @param year
	 * @return
	 */
	public static boolean isValidDate(int day, int month, int year) {

		GregorianCalendar gc = new GregorianCalendar();
		gc.setLenient(false);
		gc.set(GregorianCalendar.YEAR, year);
		gc.set(GregorianCalendar.MONTH, month);
		gc.set(GregorianCalendar.DATE, day);

		try {
			gc.getTime();
		} catch (Exception e) {
			LOGGER.debug("Invalid Date: " + e.getMessage(), e);
			return false;
		}
		return true;
	}

	/**
	  * This method validates the Standard Component Id.
	  * @param issuerPlanNumber String Format.
	  */
	public static boolean isValidateStandardComponentId(String issuerPlanNumber) {

		boolean flag = false;

		if (StringUtils.isNotBlank(issuerPlanNumber) && isValidLength(LEN_HIOS_PLAN_ID, issuerPlanNumber)
				&& isValidPattern(PATTERN_PLAN_ID, issuerPlanNumber)) {
			flag = true;
		}
		return flag;
	}

	/**
	  * This method validates the Issuer Plan Number of HEALTH/DENTAL/AME/STM.
	  * @param issuerPlanNumber String Format.
	  */
	public static boolean isValidHIOSPlanNumber(String issuerPlanNumber) {

		boolean flag = false;

		if (StringUtils.isNotBlank(issuerPlanNumber) && isValidLength(LEN_HIOS_PLAN_ID, issuerPlanNumber)
				&& isValidPattern(PATTERN_HIOS_PLAN_NUM, issuerPlanNumber)) {
			flag = true;
		}
		return flag;
	}

	public static boolean validateState(String stateCode) {
		
		boolean flag = false;
		
		try {
			USStateCodeSimpleType.valueOf(stateCode);
			flag = true;
		}
		catch (Exception ex) {
			LOGGER.debug("Invalid State: " + ex.getMessage(), ex);
		}
		return flag;
	}

	public static boolean validateHiosIssuerId(String hiosIssuerId) {
		
		boolean flag = false;

		if (StringUtils.isNotBlank(hiosIssuerId)) {
			if(StringUtils.isNumeric(hiosIssuerId)) {
				flag = isValidLength(LEN_HIOS_ISSUER_ID, hiosIssuerId);
			}else if(hiosIssuerId.matches(PATTERN_MEDICARE_HIOS_ISSUER_ID)) {
				flag = isValidLength(LEN_MIN_MEDICARE_HIOS_ISSUER_ID, LEN_MAX_MEDICARE_HIOS_ISSUER_ID, hiosIssuerId);
			}
		}
		return flag;
	}

	public static void validateHiosIssuerId(String hiosIssuerId, List<PlanMgmtErrorVO> errorVOList, String fieldName,
			IIssuerRepository iIssuerRepository) {

		if (!validateHiosIssuerId(hiosIssuerId)) {
			setErrorMessage(errorVOList, fieldName, hiosIssuerId, EMSG_INVALID_VALUE);
		}
		else {
			List<String> hiosIssuerIdList = new ArrayList<String>();
			hiosIssuerIdList.add(hiosIssuerId);
			validateHiosIssuerIdInDB(hiosIssuerIdList, errorVOList, fieldName, iIssuerRepository);
		}
	}

	/**
	 * Method is used to validate HIOS Issuer Id Value in database.
	 */
	public static void validateHiosIssuerIdInDB(List<String> hiosIssuerIdList, List<PlanMgmtErrorVO> errorVOList, String fieldName,
			IIssuerRepository iIssuerRepository) {

		try {
			int count = iIssuerRepository.getCountOfHiosIssuerID(hiosIssuerIdList);

			if (count != hiosIssuerIdList.size()) {
				setErrorMessage(errorVOList, fieldName, null, EMSG_NOT_EXIST_IN_DB);
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
			setErrorMessage(errorVOList, fieldName, null, EMSG_NOT_EXIST_IN_DB);
		}
	}

	/**
	 * Method is used to get Integer value from String.
	 */
	public static Integer getIntegerValue(String value) {
		
		Integer intValue = null;
		
		if (validateNumeric(value.trim())) {
			intValue = Double.valueOf(value.trim()).intValue();
		}
		return intValue;
	}

	/**
	 * Method is used to get Integer Percentage value from String.
	 */
	public static Integer getPercentageValue(String value) {
		
		Integer intValue = null;
		
		if (StringUtils.isNotBlank(value) && validateNumeric(value.trim().replace(PERCENTAGE, StringUtils.EMPTY))) {
			intValue = Double.valueOf(value.trim().replace(PERCENTAGE, StringUtils.EMPTY)).intValue();
		}
		return intValue;
	}

	/**
	 * Method is used to get Integer Percentage value from String.
	 */
	public static Double getPercentageValueInDouble(String value) {
		
		Double doubleValue = null;
		
		if (StringUtils.isNotBlank(value) && validateNumeric(value.trim().replace(PERCENTAGE, StringUtils.EMPTY))) {
			doubleValue = Double.valueOf(value.trim().replace(PERCENTAGE, StringUtils.EMPTY));
		}
		return doubleValue;
	}

	/**
	 * Method is used to get Integer Dollar value from String.
	 */
	public static Integer getDollarValue(String value) {
		
		Integer intValue = null;
		
		if (StringUtils.isNotBlank(value) && validateNumeric(value.trim().replace(DOLLARSIGN, StringUtils.EMPTY))) {
			intValue = Double.valueOf(value.trim().replace(DOLLARSIGN, StringUtils.EMPTY)).intValue();
		}
		return intValue;
	}

	/**
	 * Method is used to get Integer Dollar value from String.
	 */
	public static Double getDollarValueInDouble(String value) {
		
		Double doubleValue = null;
		
		if (StringUtils.isNotBlank(value) && validateNumeric(value.trim().replace(DOLLARSIGN, StringUtils.EMPTY))) {
			doubleValue = Double.valueOf(value.trim().replace(DOLLARSIGN, StringUtils.EMPTY));
		}
		return doubleValue;
	}

	/**
	 * Method is used to get Float value from String.
	 */
	public static Float getFloatValue(String value) {
		
		Float fltValue = null;
		
		if (StringUtils.isNotBlank(value) && validateNumeric(value.trim().replace(DOLLARSIGN, StringUtils.EMPTY))) {
			fltValue = Float.valueOf(value.trim().replace(DOLLARSIGN, StringUtils.EMPTY));
		}
		return fltValue;
	}

	/**
	 * Method is used to verified that Value is numeric or not.
	 */
	public static boolean validateNumeric(String value) {
		
		try {
			LOGGER.debug("Numeric is " + Double.valueOf(value));
			return true;
		}
		catch (Exception ex) {
			LOGGER.debug("Invalid Decimal value: " + ex.getMessage(), ex);
		}
		return false;
	}

	/**
	 * Method is used to verified that Amount is valid or not.
	 */
	public static boolean validateAmount(String amount) {

		boolean status = true;
		
		try {

			BigDecimal value = new BigDecimal(amount.replaceAll(DOLLAR, StringUtils.EMPTY).replaceAll(COMMA, StringUtils.EMPTY));

			if (-1 == value.signum()) {
				status = false;
			}
		}
		catch (Exception ex) {
			status = false;
			LOGGER.debug("Invalid Amount value: " + ex.getMessage(), ex);
		}
		return status;
	}

	/**
	 * Method is used to verified that Whole Amount is valid or not.
	 */
	public static boolean validateWholeAmount(String amount) {

		boolean status = true;

		try {

			BigDecimal value = new BigDecimal(amount.replaceAll(DOLLAR, StringUtils.EMPTY).replaceAll(COMMA, StringUtils.EMPTY));

			if (-1 == value.signum() || !(0 == value.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO))) {
				status = false;
			}
		}
		catch (Exception ex) {
			status = false;
			LOGGER.debug("Invalid Amount value: " + ex.getMessage(), ex);
		}
		return status;
	}

	/**
	 * Method is used to compare Effective Date and Expiration Date.
	 * @param date, Date
	 * @return boolean value
	 */
	public static boolean validateDate(String date, DateTimeFormatter dtFormatter) {

		try {
			dtFormatter.parseLocalDate(date);
			return true;
		}
		catch (Exception e) {
			LOGGER.debug("Validating Date [" + date + "] Error: " + e.getMessage(), e);
		}
		return false;
	}

	/**
	 * Method is used to validate Allowed value from Value withouth Null check.
	 */
	public static boolean validateAgainstAllowedValue(String fieldValue, List<PlanMgmtErrorVO> errorVOList,
			String fieldName, String allowedValue) {
		boolean valid = true;
		if (StringUtils.isNotBlank(fieldValue) && StringUtils.isNotBlank(allowedValue)) {

			List<String> list = Arrays.asList(allowedValue.toLowerCase().split(COMMA));

			if (!list.contains(fieldValue.toLowerCase())) {
				setErrorMessage(errorVOList, fieldName, fieldValue, EMSG_INVALID_VALUE);
				valid = false;
			}
		}
		return valid;
	}

	
	/**
	 * Method is used to compare Start Date and End Date.
	 */
	public static int compareDates(String startDateText, String endDateText, DateTimeFormatter dateFormat) {

		LOGGER.debug("Begin compareDates with Start Date: " + startDateText + " & End Date: " + endDateText);
		int compareValue = DATE_START_AND_END_INVALID;

		try {

			if (StringUtils.isBlank(startDateText)
				|| StringUtils.isBlank(endDateText)) {
				return compareValue;
			}
			LocalDate startDate = dateFormat.parseDateTime(startDateText).toLocalDate();
			LocalDate endDate = dateFormat.parseDateTime(endDateText).toLocalDate();
			compareValue = startDate.compareTo(endDate);
		}
		finally {
			LOGGER.debug("End compareDates() with compareValue: " + compareValue);
		}
		return compareValue;
	}
	
	/**
	 * Method is used to plan's insurance type.
	 */
	public static boolean validateInsuranceType(String value){
		try {
			PlanInsuranceType[] planInsuranceTypeList = Plan.PlanInsuranceType.values();
			for(PlanInsuranceType planInsuranceType: planInsuranceTypeList){
				if(planInsuranceType.toString().equalsIgnoreCase(value)){
					return true;
				}	
			}
		}
		catch (Exception ex) {
			LOGGER.debug("Error occurs @validateInsuranceType(): " + ex.getMessage(), ex);
		}
		return false;	
	}
	
	
	/**
	 * Method is used to validate Issuer Id in database.
	 */
	public static void validateIssuerIdInDB(List<Integer> IssuerIdList, List<PlanMgmtErrorVO> errorVOList, String fieldName,
			IIssuerRepository iIssuerRepository) {

		try {
			int count = iIssuerRepository.getCountOfIssuerID(IssuerIdList);

			if (count != IssuerIdList.size()) {
				setErrorMessage(errorVOList, fieldName, null, EMSG_NOT_EXIST_IN_DB);
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurs @validateIssuerIdInDB(): " + ex.getMessage(), ex);
			setErrorMessage(errorVOList, fieldName, null, EMSG_NOT_EXIST_IN_DB);
		}
	}
	
	/**
	 * Method is used to validate User Id in database.
	 */
	public static void validateUserId(String fieldValue, List<PlanMgmtErrorVO> errorVOList, String fieldName, UserService userService){
		try
		{
			if(validateNumeric(fieldValue, errorVOList,fieldName)){
				AccountUser accountUser = userService.findById(Integer.parseInt(fieldValue));
				if(null == accountUser){
					setErrorMessage(errorVOList, fieldName, null, EMSG_NOT_EXIST_IN_DB);
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurs @validateUserId(): " + ex.getMessage(), ex);
			setErrorMessage(errorVOList, fieldName, null, EMSG_NOT_EXIST_IN_DB);
		}
	}
	
	
	public static boolean isDateLessThanCurrentDate(Date dateVal){
		try
		{
			Date today = new TSDate();
			if(today.compareTo(dateVal) > 0 && today.compareTo(dateVal) == 0){
				return true;
			}
		}
		catch(Exception ex){
			LOGGER.error("Error occurs @isFutureDate(): " + ex.getMessage(), ex);
		}
		return false;
	}
}
