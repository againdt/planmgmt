package com.getinsured.hix.pmms.service.validation;

import static com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil.setStatusResponse;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerCommissionRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerCommissionResponseDTO;
import com.getinsured.hix.model.IssuerCommission;
import com.getinsured.hix.model.IssuerCommission.Frequency;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes.ErrorCode;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.pmms.repository.IIssuerRepository;


@Component
public final class IssuerCommissionValidatior {

	private static final Logger LOGGER = Logger.getLogger(IssuerCommissionValidatior.class);
	private static final String DATE_FORMAT = "MM/dd/yyyy";
	private static final String COMMISSION_AMOUNT_FORMAT = "([0-9]*)(\\.{1})?([0-9]+)$" ;
	
	@Autowired
	private UserService userService;
	@Autowired
	IIssuerRepository iIssuerRepository;

	public GhixResponseDTO validateUpldateIssuerCommissionInBulk(
			IssuerCommissionRequestDTO issuerCommissionRequestDTO) {
		
		GhixResponseDTO issuerCommissionResponseDTO = new GhixResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		
		try{
			if(null == issuerCommissionRequestDTO){
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "issuerCommissionRequestDTO", null, PlanMgmtValidatorUtil.EMSG_REQUEST);
				return issuerCommissionResponseDTO;
			}else{
				
				validateIssuerList(issuerCommissionRequestDTO, "Issuer Ids", errorVOList);
				validateInsuranceType(issuerCommissionRequestDTO.getInsuranceType(), "Insurance Type", errorVOList);
				validateStartDate(issuerCommissionRequestDTO.getStartDate(), "Start Date", errorVOList);
				validateEndDate(issuerCommissionRequestDTO.getEndDate(), "End Date", errorVOList, issuerCommissionRequestDTO.getStartDate());
				validateCommissionAmount(issuerCommissionRequestDTO.getFirstYearCommissionAmount(), "First Year Commission Amount", errorVOList);
				validateCommissionFormat(issuerCommissionRequestDTO.getFirstYearCommissionFormat(), "First Year Commission Format", errorVOList);
				validateCommissionAmount(issuerCommissionRequestDTO.getSecondYearCommissionAmout(), "Second Year Commission Amount", errorVOList);
				validateCommissionFormat(issuerCommissionRequestDTO.getSecondYearCommissionFormat(), "Second Year Commission Format", errorVOList);
				validateFrequency(issuerCommissionRequestDTO.getFrequency(), "Frequency", errorVOList);
				PlanMgmtValidatorUtil.validateUserId(issuerCommissionRequestDTO.getCreateBy(), errorVOList, "Created By", userService);
			}
		}
		catch(Exception ex)
		{
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, ex.getMessage());
		}
		finally
		{
			if (!CollectionUtils.isEmpty(errorVOList)) {
				setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), issuerCommissionResponseDTO);
			}
		}
		
		return issuerCommissionResponseDTO;
	}

	
	public IssuerCommissionResponseDTO validateGetIssuerCommissionByHiosId(
			String hiosIssuerId) {
		
		IssuerCommissionResponseDTO issuerCommissionResponseDTO = new IssuerCommissionResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		
		try{
			if(StringUtils.isBlank(hiosIssuerId)){
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "hiosIssuerId", null, PlanMgmtValidatorUtil.EMSG_EMPTY);
				return issuerCommissionResponseDTO;
			}else{
								
				if(!PlanMgmtValidatorUtil.validateHiosIssuerId(hiosIssuerId)){
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "hiosId", null, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
				}else{
					List<String> hiosIssuerIdList = new ArrayList<String>();
					hiosIssuerIdList.add(hiosIssuerId);
					PlanMgmtValidatorUtil.validateHiosIssuerIdInDB(hiosIssuerIdList, errorVOList, "HIOS Issuer ID", iIssuerRepository);
				}
			}
		}
		catch(Exception ex)
		{
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, ex.getMessage());
		}
		finally
		{
			if (!CollectionUtils.isEmpty(errorVOList)) {
				setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), issuerCommissionResponseDTO);
			}
		}
		
		return issuerCommissionResponseDTO;
	}

	
	private void validateIssuerList(IssuerCommissionRequestDTO issuerCommissionRequestDTO, String fieldName, List<PlanMgmtErrorVO> errorVOList) throws SQLException {
		if (null == issuerCommissionRequestDTO.getIssuerIdList())
		{
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, null, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
		else if (issuerCommissionRequestDTO.getIssuerIdList().size() == PlanMgmtConstants.ZERO)
		{
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, null, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
		else
		{
			PlanMgmtValidatorUtil.validateIssuerIdInDB(issuerCommissionRequestDTO.getIssuerIdList(), errorVOList, fieldName, iIssuerRepository);
		}
		
	}
	
	private void validateInsuranceType(String fieldValue, String fieldName, List<PlanMgmtErrorVO> errorVOList) {
		if(StringUtils.isBlank(fieldValue))
		{
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
		else if(!PlanMgmtValidatorUtil.validateInsuranceType(fieldValue))
		{
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
	}
	
	private void validateStartDate(String fieldValue, String fieldName, List<PlanMgmtErrorVO> errorVOList) {
		if(StringUtils.isBlank(fieldValue)) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
		else if(StringUtils.isNotBlank(fieldValue) && !DateUtil.isValidDate(fieldValue, DATE_FORMAT)){
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
		else
		{
			Date startDate = DateUtil.StringToDate(fieldValue, DATE_FORMAT);
			
			if(PlanMgmtValidatorUtil.isDateLessThanCurrentDate(startDate)){
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_FUTURE_DATE);
			}
		}
	}
	
	private void validateEndDate(String fieldValue, String fieldName, List<PlanMgmtErrorVO> errorVOList, String compareValue) {

		if (StringUtils.isNotBlank(fieldValue)) {

			if (!DateUtil.isValidDate(fieldValue, DATE_FORMAT)) {
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
			}
			else if (DateUtil.isValidDate(compareValue, DATE_FORMAT)) {
				Date endDate = DateUtil.StringToDate(fieldValue, DATE_FORMAT);
				Date startDate = DateUtil.StringToDate(compareValue, DATE_FORMAT);
				
				if (startDate.compareTo(endDate) >= 0) {
					PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_GREATER_DATE);
				}
			}
			else {
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_GREATER_DATE);
			}
		}
	}
	
	
	private void validateCommissionFormat(String fieldValue, String fieldName, List<PlanMgmtErrorVO> errorVOList) {
		if(StringUtils.isBlank(fieldValue))
		{
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
		else if(!fieldValue.equalsIgnoreCase(PlanMgmtConstants.DOLLAR_SIGN) && !fieldValue.equalsIgnoreCase(PlanMgmtConstants.PERCENTAGE_SIGN))
		{
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
	}
	
	private void validateFrequency(String fieldValue, String fieldName, List<PlanMgmtErrorVO> errorVOList) {
		if(StringUtils.isBlank(fieldValue))
		{
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
		else if(!validateFrequencyValue(fieldValue)){
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
	}
	
	private void validateCommissionAmount(String fieldValue, String fieldName, List<PlanMgmtErrorVO> errorVOList) {
		if(StringUtils.isBlank(fieldValue))
		{
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
		}
		else if(PlanMgmtValidatorUtil.validateMaxStringLength(fieldValue, errorVOList, fieldName, PlanMgmtConstants.THREE))
		{
			if(!PlanMgmtValidatorUtil.isValidPattern(COMMISSION_AMOUNT_FORMAT, fieldValue)){
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, fieldName, fieldValue, PlanMgmtValidatorUtil.EMSG_INVALID_VALUE);
			}
		}
	}
	
	
	private boolean validateFrequencyValue(String frequencyValue){
		try {
			Frequency[] frequencyList = IssuerCommission.Frequency.values();
			for(Frequency frequency: frequencyList){
				if(frequency.toString().equalsIgnoreCase(frequencyValue)){
					return true;
				}	
			}
		}
		catch (Exception ex) {
			LOGGER.debug("Error occurs @validateFrequencyValue(): " + ex.getMessage(), ex);
		}
		return false;	
	}	
	
}
