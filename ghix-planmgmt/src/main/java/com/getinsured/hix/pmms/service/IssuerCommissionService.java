/**
 * IssuerCommissionService.java
 * @author santanu
 * @version 1.0
 * @since Jan 2, 2017 
 * Issuer Commission related interfaces are defined here 
 */

package com.getinsured.hix.pmms.service;


import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerCommissionRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerCommissionResponseDTO;
import com.getinsured.hix.model.IssuerCommission;

public interface IssuerCommissionService {

	/* ##### interface of update issuer commission in bulk  ##### */
	GhixResponseDTO updateIssuerCommissionInBulk(IssuerCommissionRequestDTO issuerCommissionRequestDTO); 
	
	/* ##### interface of get issuer commission by HIOS Id  ##### */
	IssuerCommissionResponseDTO getIssuerCommissionByHiosId(String hiosIssuerId); 
	
	/* ##### interface of get issuer commission. request parameters are issuer id, insurance type, effective date ##### */
	IssuerCommission getIssuerCommission(Integer issuerId, String insuranceType, String commEffectiveDate);
}
