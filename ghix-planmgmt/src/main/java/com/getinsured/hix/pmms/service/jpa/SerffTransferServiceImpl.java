package com.getinsured.hix.pmms.service.jpa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.planmgmt.microservice.GhixResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.PlanDocumentsJobResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffBatchInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffBatchSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffBatchSearchResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffPlanMgmtBatchDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffPlanMgmtBatchResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferAttachmentInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferAttachmentResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferSearchResponseDTO;
import com.getinsured.hix.model.PlanDocumentsJob;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch.FTP_STATUS;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes.ErrorCode;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.repository.ISerffPlanDocumentsJobRepository;
import com.getinsured.hix.pmms.repository.ISerffPlanMgmtBatch;
import com.getinsured.hix.pmms.service.SerffTransferService;
import com.getinsured.hix.pmms.service.validation.PlanMgmtErrorVO;
import com.getinsured.hix.pmms.service.validation.PlanMgmtValidatorUtil;
import com.getinsured.hix.pmms.service.validation.SerffAPIRequestValidator;

/**
 * Class is used to provide service to Serff Transfer Service APIs [Plan Management-Microservice]
 * @since Mar 15, 2017
 */
@Service("pmmsSerffTransferService")
public class SerffTransferServiceImpl  implements SerffTransferService {

	private static final Logger LOGGER = Logger.getLogger(SerffTransferServiceImpl.class);

	@Autowired private GIExceptionHandler giExceptionHandler;
	@Autowired private SerffAPIRequestValidator serffAPIRequestValidator;
	@Autowired private ISerffPlanMgmtBatch iSerffPlanMgmtBatch;
	@Autowired private ISerffPlanDocumentsJobRepository iSerffPlanDocumentsJobRepository;

	@PersistenceUnit private EntityManagerFactory emf;
	
	public SerffTransferSearchResponseDTO getSerffTransferList(SerffTransferSearchRequestDTO serffTransferSearchRequestDTO) {
		SerffTransferSearchResponseDTO serffTransferSearchResponseDTO = new SerffTransferSearchResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		
		if(serffAPIRequestValidator.validateSerffTransferSearchRequest(serffTransferSearchRequestDTO, serffTransferSearchResponseDTO)) {
			EntityManager em = null;
	    	Query countQuery = null;
	    	Query query = null;
			int pageNumber = 1, startRecord = 0, totalRecCount = 0; 
	    	String countQueryStr = getSerffTransferSearchQuery(serffTransferSearchRequestDTO, true);
	    	String queryStr = getSerffTransferSearchQuery(serffTransferSearchRequestDTO, false);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Plan Load Status Data Query: " + queryStr);
			}
			if (StringUtils.isNotBlank(serffTransferSearchRequestDTO.getPageNumber())) {
				pageNumber = PlanMgmtValidatorUtil.getIntegerValue(serffTransferSearchRequestDTO.getPageNumber());
				if(pageNumber <= 0) {
					LOGGER.warn("Invalid page number passed, resetting it to 1 : " + pageNumber);
					pageNumber = 1;
				}
				startRecord = (pageNumber - 1) * GhixConstants.PAGE_SIZE;
			}
			try {
				String startFilterDate = null;
	
				em = emf.createEntityManager();
				countQuery = em.createQuery(countQueryStr);
				query = em.createQuery(queryStr);
	
				if (StringUtils.isNotBlank(serffTransferSearchRequestDTO.getStartDate())) {
					startFilterDate = DateUtil.dateToString(DateUtil.StringToDate(serffTransferSearchRequestDTO.getStartDate(), "MM/dd/yyyy"), "MM/dd/yyyy");
					countQuery.setParameter("startFilterDate", startFilterDate);
					query.setParameter("startFilterDate", startFilterDate);
				}
				query.setFirstResult(startRecord);
				query.setMaxResults(GhixConstants.PAGE_SIZE);
			   	Object objCount = countQuery.getSingleResult();
				if (objCount != null) {
					totalRecCount = Integer.parseInt(objCount.toString());
				}
				serffTransferSearchResponseDTO.setTotalRecordCount(totalRecCount);
				serffTransferSearchResponseDTO.setPageSize(GhixConstants.PAGE_SIZE);

				List<?> rsList = query.getResultList();
				if(null != rsList) {
					Iterator<?> rsIterator = rsList.iterator();
					int count = 0;
					while (rsIterator.hasNext()) {
						serffTransferSearchResponseDTO.setSerffTransferInfo(getSerffTransferInfoDTO((Object[]) rsIterator.next()));
						count++;
					}
					serffTransferSearchResponseDTO.setCurrentRecordCount(count);
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Found " + count + " records for serff transfer");
					}				}
			}
			catch (Exception ex) {
				LOGGER.error("Exception occurred in getSerffTransferList: ", ex);
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting serff transfer list " );
				giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.SERFF_TRANSFER_LIST_EXCEPTION.code), "Exception occurred while getting serff transfer list: ",ex);
			} finally {
				countQuery = null;
				query = null;
				if (em != null && em.isOpen()) {
					em.clear();
					em.close();
				}
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.SERFF_TRANSFER_LIST_EXCEPTION.getCode(), serffTransferSearchResponseDTO);
		}
		return serffTransferSearchResponseDTO;
	}
	
	private SerffTransferInfoDTO getSerffTransferInfoDTO(Object[] objArray) {
		SerffTransferInfoDTO serffTransferInfoDTO = null;
		if(null != objArray && objArray.length == 11) {
			serffTransferInfoDTO = new SerffTransferInfoDTO();
			if(null != objArray[0] && objArray[0] instanceof Long) {
				serffTransferInfoDTO.setSerffReqID((Long) objArray[0]);
			}
			if(null != objArray[1] && objArray[1] instanceof String) {
				serffTransferInfoDTO.setHiosIssuerID((String) objArray[1]);
			}
			if(null != objArray[2] && objArray[2] instanceof String) {
				serffTransferInfoDTO.setPlanNumber((String) objArray[2]);
			}
			if(null != objArray[3] && objArray[3] instanceof String) {
				serffTransferInfoDTO.setSerffTrackNumber((String) objArray[3]);
			}
			if(null != objArray[4] && objArray[4] instanceof String) {
				serffTransferInfoDTO.setStateTrackNumber((String) objArray[4]);
			}
			if(null != objArray[5] && objArray[5] instanceof String) {
				serffTransferInfoDTO.setSerffRequest((String) objArray[5]);
			}
			if(null != objArray[6] && objArray[6] instanceof String) {
				serffTransferInfoDTO.setSerffResponse((String) objArray[6]);
			}
			if(null != objArray[7] && objArray[7] instanceof Date) {
				serffTransferInfoDTO.setStartDate((Date) objArray[7]);
			}
			if(null != objArray[8] && objArray[8] instanceof Date) {
				serffTransferInfoDTO.setEndDate((Date) objArray[8]);
			}
			if(null != objArray[9] && objArray[9] instanceof SerffPlanMgmt.REQUEST_STATUS) {
				serffTransferInfoDTO.setStatus(objArray[9].toString());
			}
			if(null != objArray[10] && objArray[10] instanceof String) {
				serffTransferInfoDTO.setStatusDescription((String) objArray[10]);
			}
		}
		return serffTransferInfoDTO;
	}
	
	private String getSerffTransferSearchQuery(SerffTransferSearchRequestDTO serffTransferSearchRequestDTO, boolean forRecCount) {
		String fieldList = null;
		
		if(forRecCount) {
			fieldList = "COUNT(*)";
		} else {
			fieldList = "serffReqId, SUBSTRING(planId,1,5), planId, serffTrackNum, stateTrackNum, requestXml, responseXml, startTime, endTime, requestStatus, remarks";
		}
		
		StringBuffer queryStr = new StringBuffer("SELECT ")
			.append(fieldList)
			.append(" FROM SerffPlanMgmt ")
			.append("WHERE operation = '").append(GhixConstants.TRANSFER_PLAN).append("' ");

		if (StringUtils.isNotBlank(serffTransferSearchRequestDTO.getStatus())) {
			queryStr.append(" AND requestStatus = ('");
			queryStr.append(serffTransferSearchRequestDTO.getStatus().trim());
			queryStr.append("') ");
		}

		if (StringUtils.isNotBlank(serffTransferSearchRequestDTO.getHiosIssuerID())) {
			queryStr.append(" AND planId LIKE '");
			queryStr.append(serffTransferSearchRequestDTO.getHiosIssuerID().trim());
			queryStr.append("%' ");
		}

		if (StringUtils.isNotBlank(serffTransferSearchRequestDTO.getStartDate())) {
			queryStr.append(" AND TO_DATE(TO_CHAR(startTime, 'MM/DD/YYYY'), 'MM/DD/YYYY') = TO_DATE(:startFilterDate, 'MM/DD/YYYY') ");
		}

		if(!forRecCount) {
			queryStr.append(" ORDER BY serffReqId DESC");
		}
		return queryStr.toString(); 
	}
	
	public SerffTransferAttachmentResponseDTO getSerffTransferAttachments(Integer serffReqId) {
		SerffTransferAttachmentResponseDTO serffTransferAttachmentResponseDTO = new SerffTransferAttachmentResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		
		if(null !=serffReqId) {
			EntityManager em = null;
	    	Query query = null;
	    	String queryStr = getSerffTransferAttachmentsQuery(serffReqId);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Serff transfer attachments Data Query: " + queryStr);
			}
			try {
				em = emf.createEntityManager();
				query = em.createQuery(queryStr);
	
				List<?> rsList = query.getResultList();
				if(null != rsList) {
					Iterator<?> rsIterator = rsList.iterator();
					int count = 0;
					while (rsIterator.hasNext()) {
						serffTransferAttachmentResponseDTO.setSerffTransferAttachmentInfo(getSerffTransferAttachmentInfoDTO((Object[]) rsIterator.next()));
						count++;
					}
					serffTransferAttachmentResponseDTO.setTotalNumOfRecords(count);
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Found " + count + " records for serff transfer Attachments");
					}				
				}
			}
			catch (Exception ex) {
				LOGGER.error("Exception occurred in getSerffTransferAttachmentList: ", ex);
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting serff transfer attachments list " );
				giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.SERFF_TRANSFER_ATTACHMENT_LIST_EXCEPTION.code), "Exception occurred while getting serff transfer attachments list: ",ex);
			} finally {
				if (em != null && em.isOpen()) {
					em.clear();
					em.close();
				}
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.SERFF_TRANSFER_ATTACHMENT_LIST_EXCEPTION.getCode(), serffTransferAttachmentResponseDTO);
		} else {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "Request Parameter", null, "Invalid request parameter" );
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.INVALID_REQUEST_PARAMETER.getCode(), serffTransferAttachmentResponseDTO);
		}
		return serffTransferAttachmentResponseDTO;
	}

	private SerffTransferAttachmentInfoDTO getSerffTransferAttachmentInfoDTO(Object[] objArray) {
		SerffTransferAttachmentInfoDTO serffTransferAttachmentInfoDTO = null;
		if(null != objArray && objArray.length == 6) {
			serffTransferAttachmentInfoDTO = new SerffTransferAttachmentInfoDTO();
			if(null != objArray[0] && objArray[0] instanceof Long) {
				serffTransferAttachmentInfoDTO.setAttachmentID((Long) objArray[0]); //serffDocId
			}
			if(null != objArray[1] && objArray[1] instanceof String) {
				serffTransferAttachmentInfoDTO.setDocumentId((String) objArray[1]); //ecmDocId
				if(serffTransferAttachmentInfoDTO.getDocumentId().startsWith("SF::PLAN::")) {
					serffTransferAttachmentInfoDTO.setDownloadable(true); 	
				} else {
					serffTransferAttachmentInfoDTO.setDownloadable(false);
				}
			}
			if(null != objArray[2] && objArray[2] instanceof String) {
				serffTransferAttachmentInfoDTO.setDocumentName((String) objArray[2]); //docName
			}
			if(null != objArray[3] && objArray[3] instanceof SerffDocument.DOC_TYPE) {
				serffTransferAttachmentInfoDTO.setDocumentType(objArray[3].toString()); //docType
			}
			if(null != objArray[4] && objArray[4] instanceof Long) {
				serffTransferAttachmentInfoDTO.setDocumentSize((Long) objArray[4]); //docSize
			}
			if(null != objArray[5] && objArray[5] instanceof Date) {
				serffTransferAttachmentInfoDTO.setCreatedOn((Date) objArray[5]); //createdOn
			}
		}
		return serffTransferAttachmentInfoDTO;
	}

	private String getSerffTransferAttachmentsQuery(Integer serffReqId) {
		StringBuffer queryStr = new StringBuffer("SELECT serffDocId, ecmDocId, docName, docType, docSize, ")
				.append("createdOn FROM SerffDocument WHERE serffReqId = ")
				.append(serffReqId);
		return queryStr.toString(); 
	}

	/**
	 * Method is used to get Plan Load Status Data List from SerffPlanMgmtBatch & SerffPlanMgmt tables.
	 */
	@Override
	public SerffBatchSearchResponseDTO getSerffBatchSearchResponse(SerffBatchSearchRequestDTO requestDTO) {

		LOGGER.debug("getSerffBatchSearchResponse start");
		SerffBatchSearchResponseDTO responseDTO = new SerffBatchSearchResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();
		EntityManager em = null;

		try {

			if (!serffAPIRequestValidator.validateSerffBatchSearchRequest(errorVOList, requestDTO, responseDTO)) {
				return responseDTO;
			}

			int pageNumber = 1, startRecord = 0;

			if (StringUtils.isNotBlank(requestDTO.getPageNumber())) {
				pageNumber = PlanMgmtValidatorUtil.getIntegerValue(requestDTO.getPageNumber());

				if (pageNumber <= 0) {
					LOGGER.warn("Invalid page number passed, resetting it to 1 : " + pageNumber);
					pageNumber = 1;
				}
				startRecord = (pageNumber - 1) * GhixConstants.PAGE_SIZE;
			}

			em = emf.createEntityManager();

			// Record count query
			responseDTO.setTotalRecordCount(getRecordCountForSerffBatchData(em, requestDTO, errorVOList));
			responseDTO.setPageSize(GhixConstants.PAGE_SIZE);

			// Actual record query
			List<SerffBatchInfoDTO> serffBatchInfoDTOList = getSerffBatchDataList(em, requestDTO, errorVOList, startRecord);

			if (!CollectionUtils.isEmpty(serffBatchInfoDTOList)) {
				responseDTO.setCurrentRecordCount(serffBatchInfoDTOList.size());
			}
			else {
				LOGGER.warn("SERFF Batch Info Data List is empty.");
			}
			responseDTO.setSerffBatchInfoDTOList(serffBatchInfoDTOList);
		}
		catch (Exception ex) {
			LOGGER.error("Exception occurred in getSerffBatchSearchResponse: ", ex);

			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting serff batch search response " );
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.SERFF_TRANSFER_LIST_EXCEPTION.code),
					"Exception occurred while getting serff batch search response : ", ex);
		}
		finally {

			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.SERFF_TRANSFER_LIST_EXCEPTION.getCode(), responseDTO);
			LOGGER.debug("getSerffBatchSearchResponse End");
		}
		return responseDTO;
	}

	/**
	 * Method is used to get Total Record Count for SERFF Batch Data.
	 */
	private int getRecordCountForSerffBatchData(EntityManager em, SerffBatchSearchRequestDTO requestDTO, List<PlanMgmtErrorVO> errorVOList) {

		LOGGER.debug("getRecordCountForSerffBatchData Start");
		int resultCount = 0;

		try {
			StringBuffer queryForCount = new StringBuffer("SELECT COUNT(serffBatch) "
					+ "FROM SerffPlanMgmtBatch AS serffBatch LEFT JOIN serffBatch.serffReqId AS serffPlanMgmt "
					+ "WHERE serffBatch.operationType IN('");

			queryForCount.append(SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.name());
			queryForCount.append("','");
			queryForCount.append(SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.name());
			queryForCount.append("') ");

			queryForCount.append(getPlanLoadStatusClause(requestDTO.getHiosIssuerID(), requestDTO.getStatus(), requestDTO.getUploadDate()));
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Plan Load Status Data Count Query: " + queryForCount);
			}

			Query query = em.createQuery(queryForCount.toString());

			if (StringUtils.isNotBlank(requestDTO.getUploadDate())) {
				query.setParameter("uploadedDate", DateUtil.dateToString(getSelectedDate(requestDTO.getUploadDate()), "MM/dd/yyyy"));
			}

		   	Object objCount = query.getSingleResult();
			if (objCount != null) {
				resultCount = Integer.parseInt(objCount.toString());
			}
			LOGGER.info("Total Result Count for SERFF batch data list: " + resultCount);
		}
		catch (Exception ex) {
			LOGGER.error("Exception occurred in getRecordCountForSerffBatchData: ", ex);

			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting record count for serff batch data list " );
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.SERFF_TRANSFER_LIST_EXCEPTION.code),
					"Exception occurred while getting record count for serff batch data list : ", ex);
		}
		finally {
			LOGGER.debug("getRecordCountForSerffBatchData End");
		}
		return resultCount;
	}

	/**
	 * Method is used to get SERFF Batch Data List.
	 */
	private List<SerffBatchInfoDTO> getSerffBatchDataList(EntityManager em, SerffBatchSearchRequestDTO requestDTO,
			List<PlanMgmtErrorVO> errorVOList, int startRecord) {

		LOGGER.debug("getSerffBatchDataList Start");
		List<SerffBatchInfoDTO> serffBatchInfoDTOList = null;

		try {
			StringBuffer queryStr = new StringBuffer("SELECT new com.getinsured.hix.dto.planmgmt.microservice.SerffBatchInfoDTO(serffPlanMgmt.serffReqId, "
					+ "serffBatch.career, serffBatch.issuerId, serffBatch.ftpStartTime, serffBatch.ftpEndTime, serffBatch.batchStartTime, serffBatch.batchEndTime, "
					+ "serffBatch.ftpStatus, serffBatch.batchStatus, serffBatch.state, serffBatch.exchangeType, serffBatch.defaultTenant, "
					+ "serffBatch.operationType, serffBatch.isDeleted, serffPlanMgmt.remarks, serffPlanMgmt.startTime, serffPlanMgmt.endTime, ");

			if (requestDTO.getIncludeRequestXml()) {
				queryStr.append("serffPlanMgmt.requestXml, serffPlanMgmt.responseXml, ");
			}
			else {
				queryStr.append("serffPlanMgmt.responseXml, ");
			}
			queryStr.append("serffPlanMgmt.pmResponseXml, serffPlanMgmt.requestStatus, serffPlanMgmt.requestState, serffPlanMgmt.requestStateDesc) "
					+ "FROM SerffPlanMgmtBatch AS serffBatch LEFT JOIN serffBatch.serffReqId AS serffPlanMgmt "
					+ "WHERE serffBatch.operationType IN('");
			queryStr.append(SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.name());
			queryStr.append("','");
			queryStr.append(SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.name());
			queryStr.append("') ");

			queryStr.append(getPlanLoadStatusClause(requestDTO.getHiosIssuerID(), requestDTO.getStatus(), requestDTO.getUploadDate()));
			queryStr.append("ORDER BY serffBatch.id DESC");
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Plan Load Status Data Query: " + queryStr);
			}

			Query query = em.createQuery(queryStr.toString());

			if (StringUtils.isNotBlank(requestDTO.getUploadDate())) {
				query.setParameter("uploadedDate", DateUtil.dateToString(getSelectedDate(requestDTO.getUploadDate()), "MM/dd/yyyy"));
			}
			query.setFirstResult(startRecord);
			query.setMaxResults(GhixConstants.PAGE_SIZE);
			serffBatchInfoDTOList = query.getResultList();
		}
		catch (Exception ex) {
			LOGGER.error("Exception occurred in getSerffBatchDataList: ", ex);

			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting serff batch data list " );
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.SERFF_TRANSFER_LIST_EXCEPTION.code),
					"Exception occurred while getting serff batch data list : ", ex);
		}
		finally {
			LOGGER.debug("getSerffBatchDataList End");
		}
		return serffBatchInfoDTOList;
	}

	/**
	 * Method is used to add a clause for Plan Load Status Data.
	 */
	private String getPlanLoadStatusClause(String selectedIssuer, String selectedStatus, String selectedDate) {

		StringBuffer clause = new StringBuffer();

		if (StringUtils.isNotBlank(selectedStatus)) {

			try {

				SerffPlanMgmtBatch.BATCH_STATUS.valueOf(selectedStatus.trim());
				clause.append(" AND serffBatch.batchStatus = ('");
				clause.append(selectedStatus.trim());
				clause.append("') ");
			}
			catch (IllegalArgumentException ex) {
				LOGGER.warn("Illegal Request status filter.");
			}
		}

		if (StringUtils.isNotBlank(selectedIssuer)) {
			clause.append(" AND serffBatch.issuerId = '");
			clause.append(selectedIssuer.trim());
			clause.append("' ");
		}

		if (StringUtils.isNotBlank(selectedDate)) {

			SimpleDateFormat inSDF = new SimpleDateFormat("MM/dd/yyyy");

			try {
				inSDF.parse(selectedDate);
				clause.append(" AND TO_DATE(TO_CHAR(serffBatch.ftpEndTime, 'MM/DD/YYYY'), 'MM/DD/YYYY') = TO_DATE(:uploadedDate, 'MM/DD/YYYY') ");
			}
			catch (ParseException e) {
				LOGGER.warn("Illegal Date filter.");
			}
		}
		return clause.toString();
	}

	/**
	 * Method is used to convert String value to Date form.
	 */
	private Date getSelectedDate(String selectedDate) {

		Date planDate = null;

		if (StringUtils.isNotBlank(selectedDate)) {
			planDate = DateUtil.StringToDate(selectedDate, "MM/dd/yyyy");
		}
		return planDate;
	}

	/**
	 * Method is used to create SERFF Batch Record.
	 */
	@Override
	public SerffPlanMgmtBatchResponseDTO createBatchRecord(SerffPlanMgmtBatchDTO batchDTO) {

		LOGGER.debug("createBatchRecord Begin");
		SerffPlanMgmtBatchResponseDTO responseBatchDTO = new SerffPlanMgmtBatchResponseDTO();
		SerffPlanMgmtBatch serffPlanMgmtBatch = null;
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		if (!serffAPIRequestValidator.validateRequestToCreateSerffBatchRecord(batchDTO, responseBatchDTO, errorVOList)) {
			return responseBatchDTO;
		}

		try {
			serffPlanMgmtBatch = new SerffPlanMgmtBatch();
			serffPlanMgmtBatch.setIsDeleted(SerffPlanMgmtBatch.IS_DELETED.N);
			serffPlanMgmtBatch.setFtpStartTime(new Date());
			serffPlanMgmtBatch.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.IN_PROGRESS);
			serffPlanMgmtBatch.setIssuerId(batchDTO.getIssuerId());
			serffPlanMgmtBatch.setCareer(batchDTO.getCareer());

			if (StringUtils.isNotBlank(batchDTO.getState())) {
			serffPlanMgmtBatch.setState(batchDTO.getState());
			}
			serffPlanMgmtBatch.setExchangeType(batchDTO.getExchangeType());
			serffPlanMgmtBatch.setDefaultTenant(batchDTO.getDefaultTenant());
			serffPlanMgmtBatch.setOperationType(batchDTO.getOperationType());

			if (0 < batchDTO.getUserId()) {
				serffPlanMgmtBatch.setUserId(batchDTO.getUserId());
			}
			serffPlanMgmtBatch = iSerffPlanMgmtBatch.save(serffPlanMgmtBatch);

			if (null != serffPlanMgmtBatch) {
				// set data in SERFF Plan Mgmt Batch DTO from SerffPlanMgmtBatch Model class.
				responseBatchDTO.setSerffPlanMgmtBatchDTO(setDataInSerffPlanMgmtBatchDTO(serffPlanMgmtBatch));
				responseBatchDTO.setStatus(GhixResponseDTO.STATUS.SUCCESS.name());
			}
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while creating SERFF batch record ");
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.SERFF_BATCH_RECORD_CREATE_EXCEPTION.code),
							"Exception occurred while creating SERFF batch record: ", ex);
		}
		finally {
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.SERFF_BATCH_RECORD_CREATE_EXCEPTION.getCode(), responseBatchDTO);
			LOGGER.debug("createBatchRecord Begin");
		}
		return responseBatchDTO;
	}

	/**
	 * Method is used to update SERFF Batch Record.
	 */
	@Override
	public GhixResponseDTO updateBatchRecord(SerffPlanMgmtBatchDTO batchDTO) {

		LOGGER.debug("createBatchRecord Begin");
		GhixResponseDTO responseBatchDTO = new GhixResponseDTO();
		SerffPlanMgmtBatch serffPlanMgmtBatch = null;
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		if (!serffAPIRequestValidator.validateRequestToUpdateSerffBatchRecord(batchDTO, responseBatchDTO, errorVOList)) {
			return responseBatchDTO;
		}

		try {
			serffPlanMgmtBatch = iSerffPlanMgmtBatch.findOne(batchDTO.getId());

			if (null == serffPlanMgmtBatch) {
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, "SERFF Batch ID", String.valueOf(batchDTO.getId()),
						"Value dose not exist in database ");
				return responseBatchDTO;
			}
			serffPlanMgmtBatch.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.valueOf(batchDTO.getFtpStatus()));

			if (null != batchDTO.getFtpEndTime()) {
				serffPlanMgmtBatch.setFtpEndTime(batchDTO.getFtpEndTime());
			}
			serffPlanMgmtBatch.setBatchStatus(SerffPlanMgmtBatch.BATCH_STATUS.valueOf(batchDTO.getBatchStatus()));
			serffPlanMgmtBatch = iSerffPlanMgmtBatch.save(serffPlanMgmtBatch);

			if (null != serffPlanMgmtBatch) {
				responseBatchDTO.setStatus(GhixResponseDTO.STATUS.SUCCESS.name());
			}
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while updating SERFF batch record ");
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.SERFF_BATCH_RECORD_UPDATE_EXCEPTION.code),
				"Exception occurred while updating SERFF batch record: ", ex);
		}
		finally {
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.SERFF_BATCH_RECORD_UPDATE_EXCEPTION.getCode(), responseBatchDTO);
			LOGGER.debug("createBatchRecord Begin");
		}
		return responseBatchDTO;
	}

	/**
	 * Method is used to set data in SerffPlanMgmtBatchDTO from SerffPlanMgmtBatch Model class.
	 */
	private SerffPlanMgmtBatchDTO setDataInSerffPlanMgmtBatchDTO(SerffPlanMgmtBatch serffPlanMgmtBatch) {

		SerffPlanMgmtBatchDTO responseDTO = null;

		if (null == serffPlanMgmtBatch) {
			return responseDTO;
		}
		responseDTO = new SerffPlanMgmtBatchDTO();
		responseDTO.setId(serffPlanMgmtBatch.getId());
		responseDTO.setUserId(serffPlanMgmtBatch.getUserId());

		if (null != serffPlanMgmtBatch.getSerffReqId()) {
			responseDTO.setSerffReqId(serffPlanMgmtBatch.getSerffReqId().getSerffReqId());
		}
		responseDTO.setFtpStartTime(serffPlanMgmtBatch.getFtpStartTime());
		responseDTO.setFtpEndTime(serffPlanMgmtBatch.getFtpEndTime());

		if (null != serffPlanMgmtBatch.getBatchStatus()) {
			responseDTO.setBatchStatus(serffPlanMgmtBatch.getBatchStatus().toString());
		}
		responseDTO.setBatchStartTime(serffPlanMgmtBatch.getBatchStartTime());
		responseDTO.setBatchEndTime(serffPlanMgmtBatch.getBatchEndTime());

		if (null != serffPlanMgmtBatch.getIsDeleted()) {
			responseDTO.setIsDeleted(serffPlanMgmtBatch.getIsDeleted().toString());
		}

		if (null != serffPlanMgmtBatch.getFtpStatus()) {
			responseDTO.setFtpStatus(serffPlanMgmtBatch.getFtpStatus().toString());
		}
		responseDTO.setIssuerId(serffPlanMgmtBatch.getIssuerId());
		responseDTO.setCareer(serffPlanMgmtBatch.getCareer());
		responseDTO.setState(serffPlanMgmtBatch.getState());
		responseDTO.setExchangeType(serffPlanMgmtBatch.getExchangeType());
		responseDTO.setDefaultTenant(serffPlanMgmtBatch.getDefaultTenant());
		responseDTO.setOperationType(serffPlanMgmtBatch.getOperationType());
		return responseDTO;
	}

	/**
	 * Method is used to create SERFF Plan Document Record in SERFF_PLAN_DOCUMENTS_JOB table.
	 */
	@Override
	public GhixResponseDTO createPlanDocumentRecord(PlanDocumentsJobDTO planDocumentsJobDTO) {

		LOGGER.debug("createPlanDocumentRecord Begin");
		GhixResponseDTO responseDTO = new GhixResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		if (!serffAPIRequestValidator.validateRequestToCreatePlanDocumentsRecord(planDocumentsJobDTO, responseDTO, errorVOList)) {
			return responseDTO;
		}

		try {
			PlanDocumentsJob planDocumentJob = new PlanDocumentsJob();
			planDocumentJob.setDocumentNewName(planDocumentsJobDTO.getDocumentNewName());
			planDocumentJob.setDocumentOldName(planDocumentsJobDTO.getDocumentOldName());

			if (null != planDocumentsJobDTO.getDocumentType()) {
				planDocumentJob.setDocumentType(planDocumentsJobDTO.getDocumentType().trim().toUpperCase());
			}
			planDocumentJob.setErrorMessage(planDocumentsJobDTO.getErrorMessage());
			planDocumentJob.setHiosProductId(planDocumentsJobDTO.getHiosProductId());
			planDocumentJob.setIssuerPlanNumber(planDocumentsJobDTO.getIssuerPlanNumber());
			planDocumentJob.setLastUpdated(new Date());
			planDocumentJob.setPlanId(planDocumentsJobDTO.getPlanId());
			planDocumentJob.setUcmNewId(planDocumentsJobDTO.getUcmNewId());
			planDocumentJob.setUcmOldId(planDocumentsJobDTO.getUcmOldId());
			planDocumentJob.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.valueOf(planDocumentsJobDTO.getFtpStatus()));
			planDocumentJob = iSerffPlanDocumentsJobRepository.save(planDocumentJob);

			if (null != planDocumentJob) {
				responseDTO.setId(planDocumentJob.getId());
				responseDTO.setStatus(GhixResponseDTO.STATUS.SUCCESS.name());
			}
		}
		catch (Exception ex) {
			PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while creating Plan Document record ");
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.PLAN_DOCUMENT_RECORD_CREATE_EXCEPTION.code),
				"Exception occurred while creating Plan Document record: ", ex);
		}
		finally {
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.PLAN_DOCUMENT_RECORD_CREATE_EXCEPTION.getCode(), responseDTO);
			LOGGER.debug("createPlanDocumentRecord End");
		}
		return responseDTO;
	}

	/**
	 * Method is used to get Plan Document List from SERFF_PLAN_DOCUMENTS_JOB table.
	 */
	@Override
	public PlanDocumentsJobResponseDTO getPlanDocumentList(PlanDocumentsJobRequestDTO requestDTO) {

		LOGGER.debug("getPlanDocumentList Start");
		PlanDocumentsJobResponseDTO responseDTO = new PlanDocumentsJobResponseDTO();
		List<PlanMgmtErrorVO> errorVOList = new ArrayList<PlanMgmtErrorVO>();

		if (serffAPIRequestValidator.validatePlanDocumentsJobRequest(requestDTO, responseDTO)) {
			EntityManager em = null;
	    	Query countQuery = null;
	    	Query query = null;
			int pageNumber = 1, startRecord = 0, totalRecCount = 0; 
			String countQueryStr = getPlanDocumentsJobQuery(requestDTO, true);
			String queryStr = getPlanDocumentsJobQuery(requestDTO, false);

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Plan Documents Status Data Query: " + queryStr);
			}

			if (StringUtils.isNotBlank(requestDTO.getPageNumber())) {
				pageNumber = PlanMgmtValidatorUtil.getIntegerValue(requestDTO.getPageNumber());

				if (pageNumber <= 0) {
					LOGGER.warn("Invalid page number passed, resetting it to 1 : " + pageNumber);
					pageNumber = 1;
				}
				startRecord = (pageNumber - 1) * GhixConstants.PAGE_SIZE;
			}

			try {
				String lastUpdatedDate = null;
	
				em = emf.createEntityManager();
				countQuery = em.createQuery(countQueryStr);
				query = em.createQuery(queryStr);
	
				if (StringUtils.isNotBlank(requestDTO.getLastUpdatedDate())) {
					lastUpdatedDate = DateUtil.dateToString(DateUtil.StringToDate(requestDTO.getLastUpdatedDate(), "MM/dd/yyyy"), "MM/dd/yyyy");
					countQuery.setParameter("lastUpdatedDate", lastUpdatedDate);
					query.setParameter("lastUpdatedDate", lastUpdatedDate);
				}
				query.setFirstResult(startRecord);
				query.setMaxResults(GhixConstants.PAGE_SIZE);
			   	Object objCount = countQuery.getSingleResult();

				if (objCount != null) {
					totalRecCount = Integer.parseInt(objCount.toString());
				}
				responseDTO.setTotalRecordCount(totalRecCount);
				responseDTO.setPageSize(GhixConstants.PAGE_SIZE);

				List<?> rsList = query.getResultList();

				if (null != rsList) {

					Iterator<?> rsIterator = rsList.iterator();
					int count = 0;

					while (rsIterator.hasNext()) {
						responseDTO.setPlanDocumentsJob(getPlanDocumentsJobDTO((Object[]) rsIterator.next()));
						count++;
					}
					responseDTO.setCurrentRecordCount(count);

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Found " + count + " records for Plan Document List");
					}
				}
			}
			catch (Exception ex) {
				LOGGER.error("Exception occurred in getPlanDocumentList: ", ex);
				PlanMgmtValidatorUtil.setErrorMessage(errorVOList, null, null, "Exception occurred while getting Plan Document list " );
				giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(ErrorCode.PLAN_DOCUMENT_LIST_EXCEPTION.code),
						"Exception occurred while getting Plan Document list: ", ex);
			}
			finally {
				countQuery = null;
				query = null;

				if (em != null && em.isOpen()) {
					em.clear();
					em.close();
				}
			}
			PlanMgmtValidatorUtil.setStatusResponse(errorVOList, ErrorCode.PLAN_DOCUMENT_LIST_EXCEPTION.getCode(), responseDTO);
		}
		LOGGER.debug("getPlanDocumentList End");
		return responseDTO;
	}

	private String getPlanDocumentsJobQuery(PlanDocumentsJobRequestDTO requestDTO, boolean forRecCount) {

		String fieldList = null;

		if (forRecCount) {
			fieldList = "COUNT(*)";
		}
		else {
			fieldList = "id, planId, hiosProductId, issuerPlanNumber, ucmNewId, ucmOldId, documentNewName, documentOldName, ftpStatus, errorMessage, documentType, lastUpdated ";
		}

		StringBuffer queryStr = new StringBuffer("SELECT ")
			.append(fieldList)
			.append(" FROM PlanDocumentsJob ");

		if (StringUtils.isNotBlank(requestDTO.getStatus()) || StringUtils.isNotBlank(requestDTO.getIssuerPlanNumber())
				|| StringUtils.isNotBlank(requestDTO.getLastUpdatedDate())) {

			boolean isFirstClause = true;
			queryStr.append(" WHERE ");

			if (StringUtils.isNotBlank(requestDTO.getStatus())) {
				isFirstClause = false;
				queryStr.append(" ftpStatus = ('");
				queryStr.append(requestDTO.getStatus().trim());
				queryStr.append("') ");
			}

			if (StringUtils.isNotBlank(requestDTO.getIssuerPlanNumber())) {

				if (isFirstClause) {
					isFirstClause = false;
				}
				else {
					queryStr.append(" AND ");
				}
				queryStr.append(" issuerPlanNumber LIKE '");
				queryStr.append(requestDTO.getIssuerPlanNumber().trim());
				queryStr.append("%' ");
			}

			if (StringUtils.isNotBlank(requestDTO.getLastUpdatedDate())) {

				if (!isFirstClause) {
					queryStr.append(" AND ");
				}
				queryStr.append(" TO_DATE(TO_CHAR(lastUpdated, 'MM/DD/YYYY'), 'MM/DD/YYYY') = TO_DATE(:lastUpdatedDate, 'MM/DD/YYYY') ");
			}
		}

		if (!forRecCount) {
			queryStr.append(" ORDER BY id DESC");
		}
		return queryStr.toString(); 
	}

	private PlanDocumentsJobDTO getPlanDocumentsJobDTO(Object[] objArray) {

		PlanDocumentsJobDTO planDocumentsJobDTO = null;

		if (null != objArray && objArray.length == 12) {

			planDocumentsJobDTO = new PlanDocumentsJobDTO();

			if (null != objArray[0] && objArray[0] instanceof Integer) {
				planDocumentsJobDTO.setId((Integer) objArray[0]);
			}

			if (null != objArray[1] && objArray[1] instanceof String) {
				planDocumentsJobDTO.setPlanId((String) objArray[1]);
			}

			if (null != objArray[2] && objArray[2] instanceof String) {
				planDocumentsJobDTO.setHiosProductId((String) objArray[2]);
			}

			if (null != objArray[3] && objArray[3] instanceof String) {
				planDocumentsJobDTO.setIssuerPlanNumber((String) objArray[3]);
			}

			if (null != objArray[4] && objArray[4] instanceof String) {
				planDocumentsJobDTO.setUcmNewId((String) objArray[4]);
			}

			if (null != objArray[5] && objArray[5] instanceof String) {
				planDocumentsJobDTO.setUcmOldId((String) objArray[5]);
			}

			if (null != objArray[6] && objArray[6] instanceof String) {
				planDocumentsJobDTO.setDocumentNewName(objArray[6].toString());
			}

			if (null != objArray[7] && objArray[7] instanceof String) {
				planDocumentsJobDTO.setDocumentOldName((String) objArray[7]);
			}

			if (null != objArray[8] && objArray[8] instanceof FTP_STATUS) {
				planDocumentsJobDTO.setFtpStatus(objArray[8].toString());
			}

			if (null != objArray[9] && objArray[9] instanceof String) {
				planDocumentsJobDTO.setErrorMessage((String) objArray[9]);
			}

			if (null != objArray[10] && objArray[10] instanceof String) {
				planDocumentsJobDTO.setDocumentType((String) objArray[10]);
			}

			if (null != objArray[11] && objArray[11] instanceof Date) {
				planDocumentsJobDTO.setLastUpdated((Date) objArray[11]);
			}
		}
		return planDocumentsJobDTO;
	}
}
