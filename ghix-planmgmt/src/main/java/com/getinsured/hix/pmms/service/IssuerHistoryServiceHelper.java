package com.getinsured.hix.pmms.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;

@Service
@Qualifier("issuerHistoryServiceHelper")
public class IssuerHistoryServiceHelper implements HistoryRendererService {
	@Value("#{configProp['appUrl']}") 
	private String appUrl;
//private String appUrl = GhixConstants.APP_URL;	
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerHistoryServiceHelper.class);
	
	public static final String TIMESTAMP_PATTERN1 = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String TIMESTAMP_PATTERN2 = "yyyy-MM-dd";
	
	public static final String REPOSITORY_NAME = "iIssuerRepository";
	public static final String MODEL_NAME = "com.getinsured.hix.model.Issuer";
	
	private static final String COMMENT_TEXT_PRE = "<a onclick=\"getComment('";
	private static final String COMMENT_TEXT_POST = "');\" href='#modalBox' data-toggle='modal'>Comment</a>";
	
	private static final String DOWNLOAD_FILE_PRETEXT1 = "<a href='#' onClick=\"showdetail('";//"<a href='";
	private static final String DOWNLOAD_FILE_PRETEXT2 = "download/document?documentId=";
	private static final String DOWNLOAD_FILE_POSTTEXT_CERTI = "&docType=certSuppDoc');>Download File</a>";
	
	public static final int CERTIFICATION_STATUS = 1;
	public static final int CREATED = 2;
	public static final int UPDATED = 3;
	public static final int NAME = 4;
	
	/* The below keys are for issuer history display columns*/
	public static final int CERTIFICATION_STATUS_VAL = 5;
	public static final int NAME_VAL = 6;
	public static final int DISPLAY_VAL = 7;
	public static final int USER_NAME = 8;
	public static final int FEDERAL_EMPLOYER_ID_VAL = 9;
	public static final int NAIC_COMPANY_CODE_VAL = 10;
	public static final int NAIC_GROUP_CODE_VAL = 11;
	public static final int HIOS_ISSUER_ID_VAL = 12;
	public static final int ISSUER_ACCREDITATION_VAL = 13;
	public static final int ACCREDITING_ENTITY_VAL = 14;
	public static final int LICENSE_NUMBER_VAL = 15;
	public static final int LICENSE_STATUS_VAL = 16;
	public static final int ADDRESS_LINE1_VAL = 17;
	public static final int ADDRESS_LINE2_VAL = 18;
	public static final int CITY_VAL = 19;
	public static final int STATE_VAL = 20;
	public static final int ZIP_VAL = 21;
	public static final int PHONE_VAL = 22;
	public static final int EMAIL_VAL = 23;
	public static final int ENROLLMENT_URL_VAL = 24;
	public static final int CONTACT_PERSON_VAL = 25;
	public static final int INITIAL_PAYMENT_VAL = 26;
	public static final int RECURRING_PAYMENT_VAL = 27;
	public static final int SITE_URL_VAL = 28;
	public static final int COMMENT = 29;
	public static final int COMMENT_VAL = 30;
	public static final int CERTIFICATION_STATUS_FILE = 31;
	public static final int UPDATEDBY =32;
	public static final int ISSUER_SHORTNAME_VAL =33;
	
	
	public static final String CERTIFICATION_STATUS_COL = "certificationStatus";
	public static final String CREATED_COL = "creationTimestamp";
	public static final String UPDATED_COL = "lastUpdateTimestamp";
	public static final String UPDATEDBY_COL = "lastUpdatedBy";
	public static final String NAME_COL = "name";
	
	/* The below keys are for issuer history display columns*/
	public static final String DISPLAY_VAL_COL = "displayVal";
	public static final String DISPLAY_FIELD_COL = "displayField";
	public static final String USER_NAME_COL = "userName";
	public static final String FEDERAL_EMPLOYER_ID_COL = "federalEin";
	public static final String NAIC_COMPANY_CODE_COL = "naicCompanyCode";
	public static final String NAIC_GROUP_CODE_COL = "naicGroupCode";
	public static final String HIOS_ISSUER_ID_COL = "hiosIssuerId";
	public static final String ISSUER_ACCREDITATION_COL = "issuerAccreditation";
	public static final String ACCREDITING_ENTITY_COL = "accreditingEntity";
	public static final String LICENSE_NUMBER_COL = "licenseNumber";
	public static final String LICENSE_STATUS_COL = "licenseStatus";
	public static final String ADDRESS_LINE1_COL = "addressLine1";
	public static final String ADDRESS_LINE2_COL = "addressLine2";
	public static final String CITY_COL = "city";
	public static final String STATE_COL = "state";
	public static final String ZIP_COL = "zip";
	public static final String PHONE_COL = "phoneNumber";
	public static final String EMAIL_COL = "emailAddress";
	public static final String ENROLLMENT_URL_COL = "enrollmentUrl";
	public static final String CONTACT_PERSON_COL = "contactPerson";
	public static final String INITIAL_PAYMENT_COL = "initialPayment";
	public static final String RECURRING_PAYMENT_COL = "recurringPayment";
	public static final String SITE_URL_COL = "siteUrl";
	public static final String COMMENT_COL = "commentId";
	public static final String CERTIFICATION_STATUS_FILE_COL = "certificationDoc";
	public static final String ISSUER_SHORTNAME_COL = "shortName";
	public static final String CERTIFICATION_STATUS_ASSTRING = "Cerification Status";
	public static final String NAME_ASSTRING = "Name";
	public static final String  FEDERAL_EMPLOYER_ID = "Federal Employer ID";
	public static final String  NAIC_COMPANY_CODE = "NAIC Company Code";
	public static final String  NAIC_GROUP_CODE = "NAIC Group Code";
	public static final String  HIOS_ISSUER_ID = "HIOS Issuer Id";
	public static final String  ISSUER_ACCREDITATION = "Issuer Accreditation";
	public static final String  LICENSE_NUMBER = "License Number";
	public static final String  LICENSE_STATUS = "License Status";
	public static final String  ADDRESS_LINE1 = "Address Line1";
	public static final String ADDRESS_LINE2 = "Address Line2";
	public static final String CITY = "City";
	public static final String STATE = "State";
	public static final String ZIP = "Zip"; 
	public static final String PHONE = "Phone";
	public static final String EMAIL = "Email";
	public static final String ENROLLMENT_URL = "Enrollment Url";
	public static final String CONTACT_PERSON = "Contact Person";
	public static final String INITIAL_PAYMENT = "Initial Payment";
	public static final String RECURRING_PAYMENT = "Recurring Payment";
	public static final String SITE_URL = "Site Url";
	public static final String ISSUER_SHORTNAME = "Issuer Short Name";
	private static final String CERTIFIED = "Certified";
	private static final String DE_CERTIFIED = "De-certified";
	//private static final String NON_CERTIFIED = "Non-certified";
	private static final String PENDING = "Pending";
	//private static final String RE_CERTIFIED = "Re-certified";
	private static final String REGISTERED = "Registered";
	
	
	@Autowired private UserService userService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	/**
	 * Populates the List<Map<String, Object>> which can be used to display the corresponding data as History pages in UI
	 * @param data
	 * 			List<Map<String, String>> input of the entire revision history data.
	 * @param compareColumns
	 * 			List<String> of column names based on which the filteration of @param data should be done.
	 * @param columnsToDisplay
	 * 			List<Integer> of column ids specified in corresponding HistoryService, which will indicate newly prepared map should contain what all keys.
	 * @return
	 * 			List<Map<String, Object>> Filtered data based on @param data, @param compareColumns and @param columnsToDisplay.
	 */
	public List<Map<String, Object>> processData(List<Map<String, String>> data, List<String> compareColumns, List<Integer> columnsToDisplay) 
	{
		if(null == data) {
			return null;
		}
		
		List<Map<String, String>> orderedData = new ArrayList<Map<String,String>>(data);
		Collections.reverse(orderedData);
		int size = orderedData.size();
		Map<String, String> firstElement = null;
		Map<String, String> secondElement = null;
		List<Map<String, Object>> processedData = new ArrayList<Map<String,Object>>();
		if(size > 0){
			{
				firstElement = orderedData.get(0);
			}
			for (int i=0; i<size-1;i++)
			{
				firstElement = orderedData.get(i);
				secondElement = orderedData.get(i+1);
				for (String keyColumn : compareColumns)
				{
					boolean valueChanged;
					if(keyColumn.equals(CERTIFICATION_STATUS_COL)){
						valueChanged = isCertificationEqual(firstElement,secondElement,keyColumn);
					}else {
						valueChanged = isEqual(firstElement,secondElement,keyColumn);
					}
					if(!valueChanged)
					{
						Map<String, Object> processedMap = formMap(firstElement, secondElement , keyColumn, columnsToDisplay);
						processedData.add(processedMap);
					}
				}
			}
			if(size == 1)
			{
				secondElement = firstElement;
			}
			for (String keyColumn : compareColumns)
			{
				Map<String, Object> processedMap = formMap(secondElement, secondElement , keyColumn, columnsToDisplay);
				processedData.add(processedMap);
			}
		}
		return processedData;
	}
	
	
	
	
	/**
	 * Compares values of maps firstElement and secondElement for key keyColumn.
	 */
	private boolean isEqual(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) 
	{
		return firstElement.get(keyColumn).equals(secondElement.get(keyColumn));
	}
	/**
	 * Compares CertificationStatus of maps firstElement and secondElement for key keyColumn.
	 * 
	 * @param firstElement of type
	 * 			Map<String, String>
	 * @param secondElement of type
	 * 			Map<String, String>
	 * @param keyColumn of type	String	
	 * 
	 * @return boolean
	 */
	private boolean isCertificationEqual(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) {
		boolean flag = true;
		try{
			if(null != firstElement.get(keyColumn) && null != secondElement.get(keyColumn)) {
				if(!firstElement.get(keyColumn).equals(secondElement.get(keyColumn))){
					flag = false;
				}
			}
			if(null != firstElement.get(CERTIFICATION_STATUS_FILE_COL) && null != secondElement.get(CERTIFICATION_STATUS_FILE_COL)) {
				if(!firstElement.get(CERTIFICATION_STATUS_FILE_COL).equals(secondElement.get(CERTIFICATION_STATUS_FILE_COL))){
					flag = false;
				}
			}
		}catch (Exception e) {
			LOGGER.error("Exception Occured in isCertificationEqual() ", e);
		}
		return flag;
	}
	/**
	 * Forms map for given key.
	 * The newly formed map contains key-value pairs based on the list provided as columnsToDisplay.
	 */
	private Map<String,Object> formMap(Map<String,String> firstMap, Map<String,String> secondMap, String key, List<Integer> columnsToDisplay)
	{
		Map<String, Object> values = new HashMap<String, Object>();
		String fileUrl= null;
		String status = null;
		SimpleDateFormat sdf = new SimpleDateFormat();
		SimpleDateFormat sdf1 = new SimpleDateFormat();
		
		for (int colId : columnsToDisplay)
		{
			switch (colId) 
			{
			case CERTIFICATION_STATUS : 
				status = getStatus(firstMap.get(CERTIFICATION_STATUS_COL));
				values.put(CERTIFICATION_STATUS_COL, status);
				break;
			
			case NAME : 
				values.put(NAME_COL,  firstMap.get(NAME_COL));
				break;
				
			case CREATED : 
				String dt = firstMap.get(CREATED_COL);
				sdf.applyPattern(TIMESTAMP_PATTERN1);
				try 
				{
					values.put(CREATED_COL, sdf.parse(dt));
				}
				catch (ParseException e) 
				{
					LOGGER.error("Unable to parse Updated Date "+ dt , e);
				}
				break;
				
			case UPDATED:
				String effDate = null;
				effDate = firstMap.get(UPDATED_COL);
				
				sdf1.applyPattern(TIMESTAMP_PATTERN2);
				try 
				{
					values.put(UPDATED_COL, sdf1.parse(effDate));
				}
				catch (ParseException e) 
				{
					LOGGER.error("Unable to parse Effective Date "+ effDate, e);
				}
				break;
			case CERTIFICATION_STATUS_VAL : 
			case FEDERAL_EMPLOYER_ID_VAL :
			case NAIC_COMPANY_CODE_VAL :
			case NAIC_GROUP_CODE_VAL :
			case HIOS_ISSUER_ID_VAL :
			case ISSUER_ACCREDITATION_VAL :
			case ACCREDITING_ENTITY_VAL :
			case LICENSE_NUMBER_VAL :
			case LICENSE_STATUS_VAL :
			case ADDRESS_LINE1_VAL :
			case ADDRESS_LINE2_VAL :
			case CITY_VAL :
			case STATE_VAL :
			case ZIP_VAL :
			case PHONE_VAL :
			case EMAIL_VAL :
			case ENROLLMENT_URL_VAL :
			case CONTACT_PERSON_VAL :
			case INITIAL_PAYMENT_VAL :
			case RECURRING_PAYMENT_VAL :
			case SITE_URL_VAL :
			case NAME_VAL :
			case ISSUER_SHORTNAME_VAL : 
				String  value = getValue(firstMap, secondMap, key, colId);
				if (value != null)
				{
					values.put(DISPLAY_VAL_COL, value);
					
				}
				break;
			case DISPLAY_VAL:
				String displayVal = getDisplayVal(key);
				values.put(DISPLAY_FIELD_COL, displayVal);
				break; 
			case USER_NAME:
				 String loggedInUser = null;
				 try{
					 // TODO: set appropriate value here.
					 loggedInUser=userService.getLoggedInUser()!=null? userService.getLoggedInUser().getFirstName() + " " + userService.getLoggedInUser().getLastName() :"";
				 }
				 catch(Exception e){
					 LOGGER.error("Invalid User...!", e);
				 }
				 values.put(USER_NAME_COL, loggedInUser);
				break;
			case COMMENT :
				String commentId = firstMap.get(COMMENT_COL);
				if(commentId != null && commentId.length()>0)
				{
					values.put(COMMENT_COL, COMMENT_TEXT_PRE+commentId+COMMENT_TEXT_POST);
				}
				else
				{
					values.put(COMMENT_COL, "");
				}
				break;
			case CERTIFICATION_STATUS_FILE : 
				String statusFile = null;
				statusFile = firstMap.get(CERTIFICATION_STATUS_FILE_COL);
				if(statusFile != null && statusFile.length()>0)
				{
					fileUrl = DOWNLOAD_FILE_PRETEXT1 + appUrl + DOWNLOAD_FILE_PRETEXT2 + ghixJasyptEncrytorUtil.encryptStringByJasypt(statusFile) + "');\" " + DOWNLOAD_FILE_POSTTEXT_CERTI;					
					values.put(CERTIFICATION_STATUS_FILE_COL, fileUrl);
				}
				else
				{
					values.put(CERTIFICATION_STATUS_FILE_COL, fileUrl);
				}
				break;	
			case UPDATEDBY:
				try {
					// TODO: set appropriate value here.
					AccountUser user = userService.findById(Integer.parseInt(firstMap.get(UPDATEDBY_COL)));
					values.put(UPDATEDBY_COL, user.getFirstName() + " " + user.getLastName());
				}
				catch (Exception e) {
					values.put(UPDATEDBY_COL, "");
				}
				break;					
			}
		   
		  }
					
		return values;
	}
	/**
	 * Returns Issuer certification Status 
	 * @param status of type String
	 * @return of type String
	 */
	private String getStatus(String status) {
		String sts = null;
		if (Issuer.certification_status.CERTIFIED.toString().equals(status))
		{
			sts = CERTIFIED;
		}
		else if (Issuer.certification_status.DECERTIFIED.toString().equals(status))
		{
			sts = DE_CERTIFIED; 
		}
		else if (Issuer.certification_status.PENDING.toString().equals(status))
		{
			sts = PENDING;
		}
		else if (Issuer.certification_status.REGISTERED.toString().equals(status))
		{
			sts = REGISTERED; 
		}
		return sts;
	}
	
	/**
	 * Returns display value if input key matches.
	 * @param key of type String
	 * @return of type String
	 */
	private String getDisplayVal(String key) 
	{
		String displayVal = null;
		if (key.equals(CERTIFICATION_STATUS_COL))
		{
			displayVal = CERTIFICATION_STATUS_ASSTRING;
		}
		else if (key.equals(NAME_COL))
		{
			displayVal = NAME_ASSTRING;
		}
		else if (key.equals(FEDERAL_EMPLOYER_ID_COL)){
			displayVal = FEDERAL_EMPLOYER_ID;
		}
		else if (key.equals(NAIC_COMPANY_CODE_COL)){
			displayVal = NAIC_COMPANY_CODE;
		}
		else if (key.equals(NAIC_GROUP_CODE_COL)){
			displayVal = NAIC_GROUP_CODE;
		}
		else if (key.equals(HIOS_ISSUER_ID_COL)){
			displayVal = HIOS_ISSUER_ID;
		}
		else if (key.equals(ISSUER_ACCREDITATION_COL)){
			displayVal = ISSUER_ACCREDITATION;
		}
		else if (key.equals(ACCREDITING_ENTITY_COL)){
			displayVal = ISSUER_ACCREDITATION;
		}
		else if (key.equals(LICENSE_NUMBER_COL)){
			displayVal = LICENSE_NUMBER;
		}
		else if (key.equals(LICENSE_STATUS_COL)){
			displayVal = LICENSE_STATUS;
		}
		else if (key.equals(ADDRESS_LINE1_COL)){
			displayVal = ADDRESS_LINE1;
		}
		else if (key.equals(ADDRESS_LINE2_COL)){
			displayVal = ADDRESS_LINE2;
		}
		else if (key.equals(CITY_COL)){
			displayVal = CITY;
		}
		else if (key.equals(STATE_COL)){
			displayVal = STATE;
		}
		else if (key.equals(ZIP_COL)){
			displayVal = ZIP;
		}
		else if (key.equals(PHONE_COL)){
			displayVal = PHONE;
		}
		else if (key.equals(EMAIL_COL)){
			displayVal = EMAIL;
		}
		else if (key.equals(ENROLLMENT_URL_COL)){
			displayVal = ENROLLMENT_URL;
		}
		else if (key.equals(CONTACT_PERSON_COL)){
			displayVal = CONTACT_PERSON;
		}
		else if (key.equals(INITIAL_PAYMENT_COL)){
			displayVal = INITIAL_PAYMENT;
		}
		else if (key.equals(RECURRING_PAYMENT_COL)){
			displayVal = RECURRING_PAYMENT;
		}
		else if (key.equals(SITE_URL_COL)){
			displayVal = SITE_URL;
		}
		else if (key.equals(ISSUER_SHORTNAME_VAL)) {
			displayVal = ISSUER_SHORTNAME;
		}
		return displayVal;
	}
	
	/**
	 * Returns a String value from firstMap of input key
	 * @param firstMap of type Map<String,String>
	 * @param secondMap of type Map<String,String>
	 * @param key of type String
	 * @param colId of type int
	 * @return of type String 
	 */
	private String getValue(Map<String,String> firstMap, Map<String,String> secondMap, String key, int colId) 
	{
		String value = null;
		switch (colId) 
		{
		case CERTIFICATION_STATUS_VAL : 
			if (CERTIFICATION_STATUS_COL.equals(key))
			{
				String status = firstMap.get(CERTIFICATION_STATUS_COL);
				value = getStatus(status);
			}
			break;
		case FEDERAL_EMPLOYER_ID_VAL :
			if (FEDERAL_EMPLOYER_ID_COL.equals(key))
			{
				value = firstMap.get(FEDERAL_EMPLOYER_ID_COL);
			}
			break;
		case NAIC_COMPANY_CODE_VAL :
			if (NAIC_COMPANY_CODE_COL.equals(key))
			{
				value = firstMap.get(NAIC_COMPANY_CODE_COL);
			}
			break;
		case NAIC_GROUP_CODE_VAL :
			if (NAIC_GROUP_CODE_COL.equals(key))
			{
				value = firstMap.get(NAIC_GROUP_CODE_COL);
			}
			break;
		case HIOS_ISSUER_ID_VAL :
			if (HIOS_ISSUER_ID_COL.equals(key))
			{
				value = firstMap.get(HIOS_ISSUER_ID_COL);
			}
			break;
		case ISSUER_ACCREDITATION_VAL :
			if (ISSUER_ACCREDITATION_COL.equals(key))
			{
				value = firstMap.get(ISSUER_ACCREDITATION_COL);
			}
			break;
		case ACCREDITING_ENTITY_VAL :
			if (ACCREDITING_ENTITY_COL.equals(key))
			{
				value = firstMap.get(ACCREDITING_ENTITY_COL);
			}
			break;
		case LICENSE_NUMBER_VAL :
			if (LICENSE_NUMBER_COL.equals(key))
			{
				value = firstMap.get(LICENSE_NUMBER_COL);
			}
			break;
		case LICENSE_STATUS_VAL :
			if (LICENSE_STATUS_COL.equals(key))
			{
				value = firstMap.get(LICENSE_STATUS_COL);
			}
			break;
		case ADDRESS_LINE1_VAL :
			if (ADDRESS_LINE1_COL.equals(key))
			{
				value = firstMap.get(ADDRESS_LINE1_COL);
			}
			break;
		case ADDRESS_LINE2_VAL :
			if (ADDRESS_LINE2_COL.equals(key))
			{
				value = firstMap.get(ADDRESS_LINE2_COL);
			}
			break;
		case CITY_VAL :
			if (CITY_COL.equals(key))
			{
				value = firstMap.get(CITY_COL);
			}
			break;
		case STATE_VAL :
			if (STATE_COL.equals(key))
			{
				value = firstMap.get(STATE_COL);
			}
			break;
		case ZIP_VAL :
			if (ZIP_COL.equals(key))
			{
				value = firstMap.get(ZIP_COL);
			}
			break;
		case PHONE_VAL :
			if (PHONE_COL.equals(key))
			{
				value = firstMap.get(PHONE_COL);
			}
			break;
		case EMAIL_VAL :
			if (EMAIL_COL.equals(key))
			{
				value = firstMap.get(EMAIL_COL);
			}
			break;
		case ENROLLMENT_URL_VAL :
			if (ENROLLMENT_URL_COL.equals(key))
			{
				value = firstMap.get(ENROLLMENT_URL_COL);
			}
			break;
		case CONTACT_PERSON_VAL :
			if (CONTACT_PERSON_COL.equals(key))
			{
				value = firstMap.get(CONTACT_PERSON_COL);
			}
			break;
		case INITIAL_PAYMENT_VAL :
			if (INITIAL_PAYMENT_COL.equals(key))
			{
				value = firstMap.get(INITIAL_PAYMENT_COL);
			}
			break;
		case RECURRING_PAYMENT_VAL :
			if (RECURRING_PAYMENT_COL.equals(key))
			{
				value = firstMap.get(RECURRING_PAYMENT_COL);
			}
			break;
		case SITE_URL_VAL :
			if (SITE_URL_COL.equals(key))
			{
				value = firstMap.get(SITE_URL_COL);
			}
			break;
		case NAME_VAL :
			if (NAME_COL.equals(key))
			{
				value = firstMap.get(NAME_COL);
			}
			break;
		case ISSUER_SHORTNAME_VAL :
			if (ISSUER_SHORTNAME_COL.equals(key))
			{
				value = firstMap.get(ISSUER_SHORTNAME_COL);
			}
			break;
		
		}
		
		return value;
	}
	
	
}
