//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.12 at 02:20:37 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.ihc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HpaDentalQuote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HpaDentalQuote">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AnnualMax" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplyUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BriefDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Brochure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CompanyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Deductible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Disclaimer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FindDentist" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HasOrthoCare" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Highlights" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InitialCost" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="IsSelected" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="LogoFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MonthlyCost" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="OrthoCareOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OrthoFee" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="OrthoUrlParameter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanBulletPoint" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanChoice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanDetails" type="{http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental}ArrayOfHpaDentalPlanDetail" minOccurs="0"/>
 *         &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PlanLength" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanLengthDisplay" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanOption" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HpaDentalQuote", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", propOrder = {
    "annualMax",
    "applyUrl",
    "briefDescription",
    "brochure",
    "companyName",
    "deductible",
    "disclaimer",
    "findDentist",
    "hasOrthoCare",
    "highlights",
    "initialCost",
    "isSelected",
    "logoFile",
    "monthlyCost",
    "orthoCareOption",
    "orthoFee",
    "orthoUrlParameter",
    "planBulletPoint",
    "planChoice",
    "planDetails",
    "planID",
    "planLength",
    "planLengthDisplay",
    "planName",
    "planOption",
    "productId"
})
public class HpaDentalQuote {

    @XmlElementRef(name = "AnnualMax", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> annualMax;
    @XmlElementRef(name = "ApplyUrl", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> applyUrl;
    @XmlElementRef(name = "BriefDescription", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> briefDescription;
    @XmlElementRef(name = "Brochure", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> brochure;
    @XmlElementRef(name = "CompanyName", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> companyName;
    @XmlElementRef(name = "Deductible", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> deductible;
    @XmlElementRef(name = "Disclaimer", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> disclaimer;
    @XmlElementRef(name = "FindDentist", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> findDentist;
    @XmlElement(name = "HasOrthoCare")
    protected Boolean hasOrthoCare;
    @XmlElementRef(name = "Highlights", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> highlights;
    @XmlElement(name = "InitialCost")
    protected Double initialCost;
    @XmlElement(name = "IsSelected")
    protected Boolean isSelected;
    @XmlElementRef(name = "LogoFile", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> logoFile;
    @XmlElement(name = "MonthlyCost")
    protected Double monthlyCost;
    @XmlElementRef(name = "OrthoCareOption", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orthoCareOption;
    @XmlElement(name = "OrthoFee")
    protected Double orthoFee;
    @XmlElementRef(name = "OrthoUrlParameter", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> orthoUrlParameter;
    @XmlElementRef(name = "PlanBulletPoint", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planBulletPoint;
    @XmlElementRef(name = "PlanChoice", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planChoice;
    @XmlElementRef(name = "PlanDetails", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfHpaDentalPlanDetail> planDetails;
    @XmlElement(name = "PlanID")
    protected Integer planID;
    @XmlElementRef(name = "PlanLength", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planLength;
    @XmlElementRef(name = "PlanLengthDisplay", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planLengthDisplay;
    @XmlElementRef(name = "PlanName", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planName;
    @XmlElementRef(name = "PlanOption", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planOption;
    @XmlElementRef(name = "ProductId", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.HpaDental", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productId;

    /**
     * Gets the value of the annualMax property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getAnnualMax() {
        return annualMax;
    }

    /**
     * Sets the value of the annualMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setAnnualMax(JAXBElement<String> value) {
        this.annualMax = value;
    }

    /**
     * Gets the value of the applyUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApplyUrl() {
        return applyUrl;
    }

    /**
     * Sets the value of the applyUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApplyUrl(JAXBElement<String> value) {
        this.applyUrl = value;
    }

    /**
     * Gets the value of the briefDescription property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBriefDescription() {
        return briefDescription;
    }

    /**
     * Sets the value of the briefDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBriefDescription(JAXBElement<String> value) {
        this.briefDescription = value;
    }

    /**
     * Gets the value of the brochure property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getBrochure() {
        return brochure;
    }

    /**
     * Sets the value of the brochure property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setBrochure(JAXBElement<String> value) {
        this.brochure = value;
    }

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCompanyName(JAXBElement<String> value) {
        this.companyName = value;
    }

    /**
     * Gets the value of the deductible property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDeductible() {
        return deductible;
    }

    /**
     * Sets the value of the deductible property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDeductible(JAXBElement<String> value) {
        this.deductible = value;
    }

    /**
     * Gets the value of the disclaimer property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDisclaimer() {
        return disclaimer;
    }

    /**
     * Sets the value of the disclaimer property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDisclaimer(JAXBElement<String> value) {
        this.disclaimer = value;
    }

    /**
     * Gets the value of the findDentist property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFindDentist() {
        return findDentist;
    }

    /**
     * Sets the value of the findDentist property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFindDentist(JAXBElement<String> value) {
        this.findDentist = value;
    }

    /**
     * Gets the value of the hasOrthoCare property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasOrthoCare() {
        return hasOrthoCare;
    }

    /**
     * Sets the value of the hasOrthoCare property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasOrthoCare(Boolean value) {
        this.hasOrthoCare = value;
    }

    /**
     * Gets the value of the highlights property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getHighlights() {
        return highlights;
    }

    /**
     * Sets the value of the highlights property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setHighlights(JAXBElement<String> value) {
        this.highlights = value;
    }

    /**
     * Gets the value of the initialCost property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getInitialCost() {
        return initialCost;
    }

    /**
     * Sets the value of the initialCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setInitialCost(Double value) {
        this.initialCost = value;
    }

    /**
     * Gets the value of the isSelected property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsSelected() {
        return isSelected;
    }

    /**
     * Sets the value of the isSelected property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsSelected(Boolean value) {
        this.isSelected = value;
    }

    /**
     * Gets the value of the logoFile property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLogoFile() {
        return logoFile;
    }

    /**
     * Sets the value of the logoFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLogoFile(JAXBElement<String> value) {
        this.logoFile = value;
    }

    /**
     * Gets the value of the monthlyCost property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getMonthlyCost() {
        return monthlyCost;
    }

    /**
     * Sets the value of the monthlyCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setMonthlyCost(Double value) {
        this.monthlyCost = value;
    }

    /**
     * Gets the value of the orthoCareOption property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrthoCareOption() {
        return orthoCareOption;
    }

    /**
     * Sets the value of the orthoCareOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrthoCareOption(JAXBElement<String> value) {
        this.orthoCareOption = value;
    }

    /**
     * Gets the value of the orthoFee property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getOrthoFee() {
        return orthoFee;
    }

    /**
     * Sets the value of the orthoFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setOrthoFee(Double value) {
        this.orthoFee = value;
    }

    /**
     * Gets the value of the orthoUrlParameter property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getOrthoUrlParameter() {
        return orthoUrlParameter;
    }

    /**
     * Sets the value of the orthoUrlParameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setOrthoUrlParameter(JAXBElement<String> value) {
        this.orthoUrlParameter = value;
    }

    /**
     * Gets the value of the planBulletPoint property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanBulletPoint() {
        return planBulletPoint;
    }

    /**
     * Sets the value of the planBulletPoint property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanBulletPoint(JAXBElement<String> value) {
        this.planBulletPoint = value;
    }

    /**
     * Gets the value of the planChoice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanChoice() {
        return planChoice;
    }

    /**
     * Sets the value of the planChoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanChoice(JAXBElement<String> value) {
        this.planChoice = value;
    }

    /**
     * Gets the value of the planDetails property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfHpaDentalPlanDetail }{@code >}
     *     
     */
    public JAXBElement<ArrayOfHpaDentalPlanDetail> getPlanDetails() {
        return planDetails;
    }

    /**
     * Sets the value of the planDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfHpaDentalPlanDetail }{@code >}
     *     
     */
    public void setPlanDetails(JAXBElement<ArrayOfHpaDentalPlanDetail> value) {
        this.planDetails = value;
    }

    /**
     * Gets the value of the planID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPlanID() {
        return planID;
    }

    /**
     * Sets the value of the planID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPlanID(Integer value) {
        this.planID = value;
    }

    /**
     * Gets the value of the planLength property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanLength() {
        return planLength;
    }

    /**
     * Sets the value of the planLength property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanLength(JAXBElement<String> value) {
        this.planLength = value;
    }

    /**
     * Gets the value of the planLengthDisplay property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanLengthDisplay() {
        return planLengthDisplay;
    }

    /**
     * Sets the value of the planLengthDisplay property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanLengthDisplay(JAXBElement<String> value) {
        this.planLengthDisplay = value;
    }

    /**
     * Gets the value of the planName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanName() {
        return planName;
    }

    /**
     * Sets the value of the planName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanName(JAXBElement<String> value) {
        this.planName = value;
    }

    /**
     * Gets the value of the planOption property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanOption() {
        return planOption;
    }

    /**
     * Sets the value of the planOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanOption(JAXBElement<String> value) {
        this.planOption = value;
    }

    /**
     * Gets the value of the productId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProductId() {
        return productId;
    }

    /**
     * Sets the value of the productId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProductId(JAXBElement<String> value) {
        this.productId = value;
    }

}
