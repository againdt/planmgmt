package com.getinsured.hix.planmgmt.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.NetworkTransparency;
import com.getinsured.hix.model.PlanRateBenefit;

import com.getinsured.hix.planmgmt.repository.INetworkTransparencyRepository;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;

@Service("networkTansparencyRatingService")
@Repository
@Transactional
public class NetworkTansparencyRatingServiceImpl implements NetworkTansparencyRatingService{
	
	@Autowired 
	INetworkTransparencyRepository iNetworkTransparencyRepository;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	private static final Logger LOGGER = Logger.getLogger(NetworkTansparencyRatingServiceImpl.class);
	
	@Override
	public Map<String, String> getNetworkTansparencyRatingByHIOSPlanIdList(List<String> HIOSPlanIdList, String countyFips, Integer applicableYear){
		Map<String, String> networkTransparencyMap = new HashMap<String, String> ();
		List<NetworkTransparency> networkTransparencyList = null;

		try{
			// pull network transparency records from database
			networkTransparencyList = iNetworkTransparencyRepository.findNetworkTansparencyRatingByHIOSPlanIdList(HIOSPlanIdList, countyFips, applicableYear);
			
			for(NetworkTransparency networkTransparency : networkTransparencyList){
				// set network transparency rating for each HIOS plan id
				networkTransparencyMap.put(networkTransparency.getIssuerPlanNumber(), networkTransparency.getRating());
			}
			
		}catch(Exception ex){
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getNetworkTansparencyRatingByHIOSPlanIdList ",ex);
			LOGGER.error("Exception occured in getNetworkTansparencyRatingByHIOSPlanIdList: ", ex);
		}
		
		return networkTransparencyMap;
	}
	
	@Override
	public List<PlanRateBenefit> setTansparencyRatingService(List<PlanRateBenefit> planRateBenefitList, List<String> HIOSPlanIdList, String countyFips, Integer applicableYear){
		try{
			Map<String, String> networkTransparencyList = this.getNetworkTansparencyRatingByHIOSPlanIdList(HIOSPlanIdList, countyFips, applicableYear);
			for(PlanRateBenefit planRateBenefit : planRateBenefitList){
				if( null != networkTransparencyList.get(planRateBenefit.getHiosPlanNumber().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FOURTEEN))){
					planRateBenefit.setNetworkTransparencyRating(networkTransparencyList.get(planRateBenefit.getHiosPlanNumber().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FOURTEEN)));
				}				
			}
		}catch(Exception ex){
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in setTansparencyRatingService ",ex);
				LOGGER.error("Exception occured in setTansparencyRatingService: ", ex);
		}
		
		return  planRateBenefitList;
	}

}
