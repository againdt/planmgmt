package com.getinsured.hix.planmgmt.repository;
/**
 * Repository class for PlanBenefitEdit
 */
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanBenefitEdit;
	

public interface IPlanBenefitEditRepository extends JpaRepository<PlanBenefitEdit, Integer> {
	
	@Query("SELECT pbe FROM PlanBenefitEdit pbe WHERE pbe.applicableYear=:applicableYear and pbe.planHiosId=:planHiosId")
	List<PlanBenefitEdit> getBenefitsEditedByPlanHiosId(@Param("applicableYear") String applicableYear, @Param("planHiosId") String planHiosId);
	
	List <PlanBenefitEdit> findByPlanHiosIdAndBenefitNameAndApplicableYear(String hiosId , String benefitName , String applicableYear);

	@Query("SELECT COUNT(pbe.id) FROM PlanBenefitEdit pbe, Plan p WHERE p.isDeleted='N' AND p.id <> pbe.plan.id AND p.issuerPlanNumber = pbe.planHiosId AND pbe.planHiosId = :issuerPlanNumber AND pbe.applicableYear = p.applicableYear AND pbe.applicableYear = :applicableYear")
	Long getPlanCountForIssuerPlanNumberAndApplicableYear(@Param("issuerPlanNumber") String issuerPlanNumber, @Param("applicableYear") String applicableYear);
	
}
