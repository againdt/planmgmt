/**
 * Medicare plan rate Service interface.
 * 
 * @author santanu
 * @version 1.0
 * @since Feb 2, 2015
 * 
 */
package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.MedicarePlan;
import com.getinsured.hix.dto.planmgmt.Member;

public interface MedicarePlanRateBenefitService {
	
	List<MedicarePlan> getMedicarePlanRateBenefits(List<Member> memberList, String effectiveDate, String insuranceType, String tenant, String zipCode, String countyName, String planYear);
	
//	Map<String, Map<String, Map<String, String>>> getMedicarePlanBenefits(String medicarePlanIdStr);
	
	List<Map<String, String>> getCountyByZip(String zipCode);
	
	List<MedicarePlan> getPUFPlanRateBenefits(List<Member> memberList, String effectiveDate, String insuranceType, String tenant, String zipCode, String countyName, String planYear);
	
	Map<String, Map<String, String>> getMedicarePlanBenefits(int planMedicareIdStr);
}