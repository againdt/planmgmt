package com.getinsured.hix.planmgmt.service;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.IssuerQualityRating;
import com.getinsured.hix.model.IssuerQualityRating.IssuerQualityRatingField;
import com.getinsured.hix.model.IssuerQualityRating.IssuerQualityRatingsXMLTags;
import com.getinsured.hix.model.IssuerQualityRating.IssuerQualityRatingsXMLTagsForCA;
import com.getinsured.hix.planmgmt.repository.IIssuerQualityRatingRepository;


/**
 * 
 * @version 1.0
 * @since Mar 25, 2013 
 *
 */
@Service("issuerQualityRatingService")
@Repository
@Transactional
public class IssuerQualityRatingServiceImpl implements IssuerQualityRatingService{

	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerQualityRatingServiceImpl.class);
	public static final String TIMESTAMP_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	
	
	@Autowired
	private IIssuerQualityRatingRepository qualityRatingRepository;  
	
	/**
	 * 	This method will return a Map of all the values of the ISSUER_QUALITY_RATINGS table to
	 * the caller
	 */
	

	@Override
	public Map<String, Map<String, String>> getIssuerQualityRatingByHIOSPlanIdList(List<String> HIOSPlanIdList, String effectiveDate, String stateCode) {
		Map<String, Map<String, String>> issuerQualityRatingMap = new LinkedHashMap<String, Map<String,String>>();
		Map<String, String> toPlanSelectionFieldsMap = null;
		List<IssuerQualityRating> issuerQualityRating = qualityRatingRepository.findRatingByHIOSPlanIdList(HIOSPlanIdList, effectiveDate);
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("stateCode : " + stateCode);
		}
		
		if(!(issuerQualityRating.isEmpty())){
			String hiosPlanId = null;
			String overallQualityRating = null;
			String sourceOfRating = null;
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String effectiveRatingDate = null;
			IssuerQualityRatingField attrib = null;
			String allRatingsCSVString  = null;
			List<String> ratings = null;
			String fieldKey = null;
			String fieldValue = null;
			
			for (IssuerQualityRating issuerQualityRating2 : issuerQualityRating) {
				toPlanSelectionFieldsMap = new LinkedHashMap<String, String>();
				//Retrieve the main fields of the table and put inside the Map
				hiosPlanId =  issuerQualityRating2.getPlanHiosId();
		        overallQualityRating = issuerQualityRating2.getQualityRating();
		        sourceOfRating = issuerQualityRating2.getQualitySource();
		        effectiveRatingDate = dateFormat.format(issuerQualityRating2.getEffectiveDate());
		        attrib = IssuerQualityRatingField.QualityRating;
		        toPlanSelectionFieldsMap.put(attrib.toString(), overallQualityRating);
		        attrib = IssuerQualityRatingField.QualitySource;
		        toPlanSelectionFieldsMap.put(attrib.toString(), sourceOfRating);
		        attrib = IssuerQualityRatingField.EffectiveDate;
		        toPlanSelectionFieldsMap.put(attrib.toString(), effectiveRatingDate);				
				
				//Retrieve the CSV values from 'quality_rating_details' field and put inside a Map
				allRatingsCSVString = issuerQualityRating2.getQualityRatingDetails();
				ratings = new ArrayList<String>(Arrays.asList(allRatingsCSVString.split(",")));
				
				int i=0;
				// ref HIX-83521, For CA state we use different template for issuer quality rating  
				if("CA".equalsIgnoreCase(stateCode) || "MN".equalsIgnoreCase(stateCode)){
					for (IssuerQualityRatingsXMLTagsForCA tag :IssuerQualityRatingsXMLTagsForCA.values()){
					    fieldKey = tag.toString();
					    if(i < ratings.size()){
					    	fieldValue = ratings.get(i);
					    	toPlanSelectionFieldsMap.put(fieldKey, fieldValue);
					    }
					    i++;
					}
				}else{
					for (IssuerQualityRatingsXMLTags tag :IssuerQualityRatingsXMLTags.values()){
					    fieldKey = tag.toString();
					    if(i < ratings.size()){
					    	fieldValue = ratings.get(i);
					    	toPlanSelectionFieldsMap.put(fieldKey, fieldValue);
					    }
					    i++;
					}
				}
				issuerQualityRatingMap.put(hiosPlanId, toPlanSelectionFieldsMap);
			}
		}
		return issuerQualityRatingMap;
	}	
	
		
}
