package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanDentalBenefit;

public interface IPlanDentalBenefitRepository extends JpaRepository<PlanDentalBenefit, Integer> {
	
	@Query("SELECT pb.networkLimitation, pb.networkLimitationAttribute, pb.networkExceptions, " +
			"pb.networkT1CopayVal, pb.networkT1CopayAttr, pb.networkT1CoinsurVal, pb.networkT1CoinsurAttr, " +
			"pb.networkT2CopayVal, pb.networkT2CopayAttr, pb.networkT2CoinsurVal, pb.networkT2CoinsurAttr, " +
			"pb.outOfNetworkCopayVal, pb.outOfNetworkCopayAttr, pb.outOfNetworkCoinsurVal, pb.outOfNetworkCoinsurAttr, pb.name, " +
			"pb.networkT1display, pb.networkT2display, pb.outOfNetworkDisplay, pb.networkT1TileDisplay, " +
			"pb.subjectToInNetworkDuductible, pb.subjectToOutNetworkDeductible, pb.isCovered, pb.explanation, pb.limitExcepDisplay "+
			"from PlanDentalBenefit pb WHERE (pb.plan.id = :planId) and (TO_DATE (:effectiveDate, 'YYYY-MM-DD') BETWEEN pb.effStartDate AND pb.effEndDate)")
	List<Object[]> getPlanDentalBenefit(@Param("planId") Integer planId, @Param("effectiveDate") String effectiveDate);
	
	@Query("SELECT planDentalBenefit from PlanDentalBenefit planDentalBenefit where planDentalBenefit.plan.plan.id IN (:planId )")
	List<PlanDentalBenefit> getPlanDentalBenefitByPlanId(@Param("planId") List<Integer> planId);
	
	@Query("SELECT phb from PlanDentalBenefit phb WHERE phb.plan.id = :planDentalId")
	List<PlanDentalBenefit> getBenefitByPlanDentalId(@Param("planDentalId") Integer planDentalId);

}


