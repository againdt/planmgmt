package com.getinsured.hix.planmgmt.controller;

import static org.apache.log4j.Logger.getLogger;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.PrescriptionSearchByHIOSIdRequestDTO;
import com.getinsured.hix.dto.planmgmt.PrescriptionSearchByHIOSIdResponseDTO;
import com.getinsured.hix.planmgmt.service.PrescriptionSearchService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;

@Controller
@RequestMapping(value = "/prescription")
public class PrescriptionRestController {

	private static final Logger LOGGER = getLogger(PrescriptionRestController.class);

	@Autowired
	private GIExceptionHandler giExceptionHandler; 
	@Autowired
	private PrescriptionSearchService prescriptionSearchService;
	
	
	@RequestMapping(value = "/search/byHiosPlanId", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public PrescriptionSearchByHIOSIdResponseDTO searchByHiosPlanId(@RequestBody PrescriptionSearchByHIOSIdRequestDTO prescriptionSearchByHIOSIdRequestDTO) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside searchByHiosPlanId() ============");
		}
		PrescriptionSearchByHIOSIdResponseDTO prescriptionSearchByHIOSIdResponseDTO = new PrescriptionSearchByHIOSIdResponseDTO();
		
		try{
			if(null == prescriptionSearchByHIOSIdRequestDTO.getHiosPlanIdList() 
			    || null == prescriptionSearchByHIOSIdRequestDTO.getApplicableYear()
			    || null == prescriptionSearchByHIOSIdRequestDTO.getRxCodeList()
			    || prescriptionSearchByHIOSIdRequestDTO.getRxCodeList().size() == PlanMgmtConstants.ZERO
				|| prescriptionSearchByHIOSIdRequestDTO.getHiosPlanIdList().size() == PlanMgmtConstants.ZERO ){
				prescriptionSearchByHIOSIdResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				prescriptionSearchByHIOSIdResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				prescriptionSearchByHIOSIdResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				prescriptionSearchByHIOSIdResponseDTO.endResponse();
			}else{
				List<String> hiosPlanIdList = prescriptionSearchByHIOSIdRequestDTO.getHiosPlanIdList();
				Integer applicableYear  = prescriptionSearchByHIOSIdRequestDTO.getApplicableYear();
				List<String> rxCodeList = prescriptionSearchByHIOSIdRequestDTO.getRxCodeList();
			    
				prescriptionSearchByHIOSIdResponseDTO.setPrescriptionList(prescriptionSearchService.prescriptionSearchByHiosPlanId(hiosPlanIdList, applicableYear, rxCodeList));
				prescriptionSearchByHIOSIdResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				prescriptionSearchByHIOSIdResponseDTO.endResponse();
			}
		}
		catch(Exception ex)
		{
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute PrescriptionRestController.searchByHiosPlanId().", ex);
			LOGGER.error("Failed to execute PlanRestController.getSbcScenario(). Exception:", ex);					
			prescriptionSearchByHIOSIdResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			prescriptionSearchByHIOSIdResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_SBC_SCENARIO.getCode());
			prescriptionSearchByHIOSIdResponseDTO.setErrMsg(Arrays.toString(ex.getStackTrace()));
		}
				
		return prescriptionSearchByHIOSIdResponseDTO;
	}
}
