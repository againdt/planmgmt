//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.03.03 at 04:50:02 PM MST 
//


package com.getinsured.hix.planmgmt.phix.ws.uhc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetNonWebQuoteResult" type="{http://www.goldenrulehealthws.com/IWebQuoteService/Latest}WebQuoteReturn" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getNonWebQuoteResult"
})
@XmlRootElement(name = "GetNonWebQuoteResponse")
public class GetNonWebQuoteResponse {

    @XmlElement(name = "GetNonWebQuoteResult")
    protected WebQuoteReturn getNonWebQuoteResult;

    /**
     * Gets the value of the getNonWebQuoteResult property.
     * 
     * @return
     *     possible object is
     *     {@link WebQuoteReturn }
     *     
     */
    public WebQuoteReturn getGetNonWebQuoteResult() {
        return getNonWebQuoteResult;
    }

    /**
     * Sets the value of the getNonWebQuoteResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebQuoteReturn }
     *     
     */
    public void setGetNonWebQuoteResult(WebQuoteReturn value) {
        this.getNonWebQuoteResult = value;
    }

}
