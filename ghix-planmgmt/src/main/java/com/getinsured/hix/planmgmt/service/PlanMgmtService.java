package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.FormularyDrugListResponseDTO;
import com.getinsured.hix.dto.planmgmt.ListOfStringArrayResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanBulkUpdateRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanComponentHistoryDTO;
import com.getinsured.hix.dto.planmgmt.PlanCrossWalkResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanDocumentRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanDocumentResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanListResponse;
import com.getinsured.hix.dto.planmgmt.PlanRateListResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.planmgmt.PlanSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanSearchResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanTenantRequestDTO;
import com.getinsured.hix.dto.planmgmt.SbcScenarioDTO;
import com.getinsured.hix.dto.planmgmt.ServiceAreaDetailsDTO;
import com.getinsured.hix.dto.shop.EmployeeCoverageDTO;
import com.getinsured.hix.dto.shop.employer.eps.PlanDTO;
import com.getinsured.hix.model.BenchmarkPremiumReq;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.TenantPlan;


/**
 * Any "field" you declare in an interface will be a static final field
 *
 */
public interface PlanMgmtService {

	enum UPDATE_PLAN_FIELD {
		NON_COMMISSION_FLAG, IS_DELETED, PLAN_STATUS;
	}
    long getZipCountForCounty(String county);
    
    long getCountForZipAndCountyCode(String zip, String countyCode);
    
    long getZipCountForCountyAndZip(String county, String zip);
    
    long getZipCountForZip(String zip);
    
    void saveBenchmarkPremiumReq(BenchmarkPremiumReq benchmarkPremiumReq);
    
    List<PlanDTO> getReferenceHealthPlans (String empPrimaryZip, String ehbCovered, String insuranceType, String effectiveDate,  String empCountyCode );
    
    Plan getPlanData(String planId);
    
    Plan getPlanIssuerData(String planId);
           
    String getRegionName(Integer parseInt, String insuranceType);    

    boolean checkIndvHealthPlanAvailability(String zip, String countyFips, String effectiveDate, String exchangeType, String csrValue, String tenantCode); 
    
    List getPlansByZipAndIssuerName(String zip, String countyCode, String insuranceType, String issuerName, String planLevel, String tenantCode);

    String updatePlanStatus();
    
	String updateEnrollmentAvailability();
	
	Float getCSRMultiplier(String csrVariation, String planLevel, Integer applicableYear);
	
	String getProviderURL(String providerNetworkId);
	
	List<EmployeeCoverageDTO> checkPlanAvailabilityForEmployees(List<EmployeeCoverageDTO> employeeCoverageDTOList,String planYear,String effectiveDate);
	
	List<PlanResponse> getPlanDataBenefitAndcost(List<Integer> planIdsList, String effectiveDate, String insuranceType);
	
	List<String> getCostNames();
	
	List<PlanResponse> getPlanResponseListForUniversalCart (List<Integer> planIdList);
	
	String getSeparateDrugDeductibleForPlanSTMCost(Integer planStmId);
	
	List<PlanResponse> getPlanInfoMap(List<Integer> planIds);
	
	List<Map<String, String>> getpProviderNetwork(String PlanId, List<Map<String, List<String>>> providerList);
	
	List<PlanResponse> getPlanInfoByZip(String zip, String countyFIPS, String insuranceType, String exchangeType, String effectiveDate, String tenantCode, String medicarePlanDataSource);
	
	Map<String, String> getMetalTierAvailability(String empPrimaryZip, String ehbCovered, String insuranceType, String effectiveDate,  String empCountyCode );
	
	Map<String, Map<String, String>> getPlansMapData(List<PlanResponse> plansList);
	
	PlanCrossWalkResponseDTO getCrossWalkPlanId(String actualHiosPlanNumber, String zip, String countyCode, String effectiveDate, String isEligibleForCatastrophic);
	
	boolean reapplyBenefitEditsToPlan(Integer planId, StringBuilder uiMessage);
	
	String getPediatricPlanInfo(String hiosPlanNumber, Integer planYear);
	
	Plan getPlanInfoByHIOSPlanId(String HIOSPlanId, String effectiveDate);

	//Adding for web
	List<String> getDistinctPlanYearsByInsuranceType(String insuranceType);
	List<String> getDistinctPlanYearsByInsuranceTypeAndIssuerId(String insuranceType, int issuerId);


	com.getinsured.hix.dto.planmgmt.PlanDTO getPlanDetailsById(int planId);

	GHIXResponse updatePlanDetailsById(PlanRequestDTO planReqDTO);

	PlanSearchResponseDTO getPlans(PlanSearchRequestDTO request);

	ServiceAreaDetailsDTO getServiceAreaDetailsByPlanId(int planId);

	void addTenants(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO);
	
	void addTenantsForSelectedPlans(PlanTenantRequestDTO request);

	Long getPlanCountByHiosIssuerId(String hiosIssuerId);
	
	ListOfStringArrayResponseDTO getPlanBenefitsAndCostData(int planId);
	
	PlanComponentHistoryDTO getPlanRateHistoryByRoleAndPlanId(String roleType, int planId);

	PlanRateListResponseDTO getPlanRateList(int planId, String effectiveStartDate, String effectiveEndDate);

	PlanDocumentResponseDTO updatePlanDocuments(PlanDocumentRequestDTO requestDTO);
	
	List<Object[]> getPlanIdsByHiosIds(List<String> hiosPlanIds, String coverageStartDate, String insuranceType);

	Map<String, SbcScenarioDTO> getSbcScenarioIdsByHiosPlanIds(List<String> hiosPlanIdList, Integer applicableYear);

	List<Object[]> getPlanIdsAndNetowrkNamesByHiosIds(List<String> hiosPlanIdList, String coverageStartDate, String insuranceType);
	void removeTenants(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO);
    void removeTenantsForSelectedPlans(PlanTenantRequestDTO request);

    boolean modifyMedicarePlanStatus(PlanBulkUpdateRequestDTO requestDTO);

	boolean updatePlanStatus(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO);
	
	boolean softDeletePlans(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO);
	
	boolean updateNonCommissionFlagForPlans(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO);
	
	int bulkEnableSelectedOnExchangePlansForOffExchange(PlanSearchRequestDTO request);

	PlanSearchResponseDTO bulkUpdatePlanStatusForSelectedPlans(PlanSearchRequestDTO request);
	
	List<TenantPlan> getPlanTenants(int planId);
	
	void decertifyPlans(int issuerId);

	FormularyDrugListResponseDTO getFormularyDrugList(int applicableYear, String issuerHiosId, String formularyId);

	void getPlanInfoDataByPlanIds(PlanRequest request, PlanListResponse response);
}
