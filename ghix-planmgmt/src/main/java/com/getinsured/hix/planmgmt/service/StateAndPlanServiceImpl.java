package com.getinsured.hix.planmgmt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.StatePlan;
import com.getinsured.hix.planmgmt.repository.IStatePlanRepository;

@Service("stateAndPlanService")
@Repository
public class StateAndPlanServiceImpl implements StateAndPlanService{
	
	@Autowired IStatePlanRepository statePlanRepository;
	
	@Override
	public int getZipCodeForState(String state){
		
		return statePlanRepository.getZipCodeForState(state);
		
	}
	
	@Override
	public StatePlan getStatePlanForState(String state){
		
		return statePlanRepository.getStatePlanForState(state);
		
	}

	@Override
	public List<StatePlan> getStatePlan(){
		return statePlanRepository.getStatePlan();
	}
}
