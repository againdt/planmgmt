//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.12 at 02:20:37 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.ihc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BundleQuoteResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BundleQuoteResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BundleGroup" type="{http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models}BundleGroup" minOccurs="0"/>
 *         &lt;element name="QuoteRequest" type="{http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models}BundleGroupQuoteRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BundleQuoteResponse", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", propOrder = {
    "bundleGroup",
    "quoteRequest"
})
public class BundleQuoteResponse {

    @XmlElementRef(name = "BundleGroup", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<BundleGroup> bundleGroup;
    @XmlElementRef(name = "QuoteRequest", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<BundleGroupQuoteRequest> quoteRequest;

    /**
     * Gets the value of the bundleGroup property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BundleGroup }{@code >}
     *     
     */
    public JAXBElement<BundleGroup> getBundleGroup() {
        return bundleGroup;
    }

    /**
     * Sets the value of the bundleGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BundleGroup }{@code >}
     *     
     */
    public void setBundleGroup(JAXBElement<BundleGroup> value) {
        this.bundleGroup = value;
    }

    /**
     * Gets the value of the quoteRequest property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BundleGroupQuoteRequest }{@code >}
     *     
     */
    public JAXBElement<BundleGroupQuoteRequest> getQuoteRequest() {
        return quoteRequest;
    }

    /**
     * Sets the value of the quoteRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BundleGroupQuoteRequest }{@code >}
     *     
     */
    public void setQuoteRequest(JAXBElement<BundleGroupQuoteRequest> value) {
        this.quoteRequest = value;
    }

}
