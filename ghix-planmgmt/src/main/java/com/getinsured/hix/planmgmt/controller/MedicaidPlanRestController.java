/* 
* MedicaidPlanRestController.java
 * @author santanu
 * @version 1.0
 * @since Jan 14, 2015 
 * This REST controller would be use to handle Medicaid plan request and response
 */

package com.getinsured.hix.planmgmt.controller;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Produces;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.WebServiceIOException;

import com.getinsured.hix.dto.planmgmt.MedicaidPlan;
import com.getinsured.hix.dto.planmgmt.MedicaidPlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.MedicaidPlanResponseDTO;
import com.getinsured.hix.planmgmt.service.MedicaidPlanBenefitService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;


//@Api(value="Medicaid Plan Information", description ="Returning Medicaid Plan information depends on provided information")
@Controller
@RequestMapping(value = "/medicaidplan")
public class MedicaidPlanRestController {

	@Autowired
	private MedicaidPlanBenefitService medicaidPlanBenefitService;
	
	private static final Logger LOGGER = Logger.getLogger(MedicaidPlanRestController.class);
	
	/* test controller */	
//	@RequestMapping(value = "/welcome", method = RequestMethod.GET) // for testing purpose
//	@ResponseBody public String welcome()
//	{
//		LOGGER.info("Welcome to GHIX-Planmgmt module, invoke MedicaidPlanRestController()");		
//		return "Welcome to GHIX-Planmgmt module, invoke MedicaidPlanRestController";
//	}
	
	/* return Medicaid plans */
//	@ApiOperation(value="getmedicaidplans", notes="This operation retrieves Medicaid plans based on census information and effective date.")
//	@ApiResponse(value="return list of Medicaid plans for provided census information and effective date")
	@RequestMapping(value = "/getmedicaidplans", method = RequestMethod.POST) // for testing purpose
	@Produces("application/json")
	@ResponseBody public MedicaidPlanResponseDTO getMedicaidPlans(@RequestBody MedicaidPlanRequestDTO medicaidPlanRequestDTO )
	{
		LOGGER.info("inside getMedicaidPlans()");	
		if(LOGGER.isDebugEnabled()) {
			StringBuilder buildStr = new StringBuilder();
			buildStr.append("Request data ===>");	
			buildStr.append(", HouseholdCaseId : ");
			buildStr.append(medicaidPlanRequestDTO.getHouseholdCaseId());
			buildStr.append(" Zip : ");
			buildStr.append(medicaidPlanRequestDTO.getZipCode());
			buildStr.append(" County code : ");
			buildStr.append(medicaidPlanRequestDTO.getCountyCode());
			buildStr.append(" Effective date : ");
			buildStr.append(medicaidPlanRequestDTO.getEffectiveDate());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));
		}
		
		MedicaidPlanResponseDTO medicaidPlanResponseDTO = new MedicaidPlanResponseDTO();
		
		if(null == medicaidPlanRequestDTO.getEffectiveDate() || medicaidPlanRequestDTO.getEffectiveDate().trim().isEmpty()				
				|| null == medicaidPlanRequestDTO.getZipCode() || medicaidPlanRequestDTO.getZipCode().trim().isEmpty()
				|| null == medicaidPlanRequestDTO.getCountyCode() || medicaidPlanRequestDTO.getCountyCode().trim().isEmpty()
				|| null == medicaidPlanRequestDTO.getHouseholdCaseId()  		
				) {
				medicaidPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				medicaidPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				medicaidPlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			
			} else {
				try{
					List<MedicaidPlan> medicaidPlanList = medicaidPlanBenefitService.getMedicaidPlans(medicaidPlanRequestDTO.getZipCode(), medicaidPlanRequestDTO.getCountyCode(), medicaidPlanRequestDTO.getEffectiveDate());
					medicaidPlanResponseDTO.setMedicaidPlanList(medicaidPlanList);
					medicaidPlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
					medicaidPlanResponseDTO.setHouseholdCaseId(medicaidPlanRequestDTO.getHouseholdCaseId());
				}catch (WebServiceIOException webServiceIOException) {
					LOGGER.error("Exception occurrecd while processing request. Exception:",webServiceIOException);	
					medicaidPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					medicaidPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_MEDICAID_PLANS.code);
					medicaidPlanResponseDTO.setErrMsg(Arrays.toString(webServiceIOException.getStackTrace()));					
				}
				catch(Exception ex) {
					LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);				
					medicaidPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					medicaidPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_MEDICAID_PLANS.code);
					medicaidPlanResponseDTO.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}	
			}
		return medicaidPlanResponseDTO;
		
	}	
	
}
