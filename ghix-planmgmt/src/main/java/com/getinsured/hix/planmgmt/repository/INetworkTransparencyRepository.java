package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.NetworkTransparency;

public interface INetworkTransparencyRepository extends JpaRepository<NetworkTransparency, Integer>{

	@Query("SELECT ntr FROM NetworkTransparency ntr WHERE ntr.issuerPlanNumber IN (:HIOSIdList) AND ntr.countyFips = :countyFips AND ntr.applicableYear =:applicableYear AND ntr.isDeleted = 'N' ")	
	List<NetworkTransparency> findNetworkTansparencyRatingByHIOSPlanIdList(@Param("HIOSIdList") List<String> HIOSPlanIdList, @Param("countyFips") String countyFips, @Param("applicableYear") Integer applicableYear);
}
