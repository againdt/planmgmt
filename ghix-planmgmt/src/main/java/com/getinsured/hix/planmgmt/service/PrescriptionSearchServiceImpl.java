/**
 * 
 */
package com.getinsured.hix.planmgmt.service;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.PrescriptionDTO;
import com.getinsured.hix.dto.planmgmt.PrescriptionSearchDTO;
import com.getinsured.hix.dto.prescription.PrescriptionSearchRequest;
import com.getinsured.hix.planmgmt.repository.IFormularyRepository;
import com.getinsured.hix.planmgmt.repository.IPMCostShareRepository;
import com.getinsured.hix.planmgmt.repository.IPlanRepository;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.timeshift.TimeShifterUtil;

/**
 * This class contains implementation of prescriptionSearch
 * @author shengole_s
 *
 */
@Service("prescriptionSearchService")
public class PrescriptionSearchServiceImpl implements PrescriptionSearchService{

	private static final Logger LOGGER = Logger.getLogger(PrescriptionSearchServiceImpl.class);
	private List<Map<String, String>> drugCostShareInfoList = null;
	private List<PrescriptionSearchDTO> prescriptionSearchDTOList = new ArrayList<PrescriptionSearchDTO>();
	@PersistenceUnit 
	private EntityManagerFactory emf;
	@Autowired
	private IPlanRepository iPlanRepository;
	@Autowired
	private IFormularyRepository iFormularyRepository;
	@Autowired
	private IPMCostShareRepository iPMCostShareRepository;
	@Autowired
	private GIExceptionHandler giExceptionHandler; 
	
	private static final String ISSUERID = "issuerId";
	private static final String FORMULARLYID = "formularlyId";
	private static final String FORMULARYID = "formularyId";
	private static final String NO_CHARGE = "No Charge";
	private static final String NO_CHARGE_AFTER_DEDUCTIBLE = "No Charge after deductible";
	private static final String NOT_APPLICABLE = "Not Applicable";
	private static final String COINS_AFTER_DEDUCTIBLE = "Coinsurance after deductible";
	private static final String COPAY_AFTER_DEDUCTIBLE = "Copay after deductible";
	private static final String COPAY_BEFORE_DEDUCTIBLE = "Copay before deductible";
	private static final String COPAY_WITH_DEDUCTIBLE = "Copay with deductible";
	private static final String DOT_ZERO = ".0";
	private static final String DOT_ZERO_ZERO = ".00";
	
	private static final int COPAY_NO_CHARGE_INDEX = 0; 
	private static final int COPAY_NO_CHARGE_AFTER_DEDUCTIBLE_INDEX = 1; 
	private static final int COPAY_DOLLAR_AMOUNT_INDEX = 2; 
	private static final int COPAY_AFTER_DEDUCTIBLE_INDEX = 3; 
	private static final int COPAY_BEFORE_DEDUCTIBLE_INDEX = 4; 
	private static final int COPAY_WITH_DEDUCTIBLE_INDEX = 5; 
	private static final int COPAY_NOT_APPLICABLE_INDEX = 6; 

	private static final int COINS_NO_CHARGE_INDEX = 0; 
	private static final int COINS_NO_CHARGE_AFTER_DEDUCTIBLE_INDEX = 1; 
	private static final int COINS_PERCENTAGE_INDEX = 2; 
	private static final int COINS_AFTER_DEDUCTIBLE_INDEX = 3; 
	private static final int COINS_NOT_APPLICABLE_INDEX = 4;
	
	private static final String DOLLAR_AMOUNT_COPAY = "$%2$s Copay";
	private static final String DOLLAR_AMOUNT_COPAY_AFTER_DEDUCTIBLE = "$%2$s Copay after deductible";
	private static final String DOLLAR_AMOUNT_COPAY_BEFORE_DEDUCTIBLE = "$%2$s Copay before deductible";
	private static final String DOLLAR_AMOUNT_COPAY_WITH_DEDUCTIBLE = "$%2$s Copay with deductible";
	private static final String DOLLAR_AMOUNT_COPAY_AFTER_DEDUCTIBLE_PERCENT_COINS = "$%2$s Copay after deductible, %1$s%% Coinsurance";
	private static final String DOLLAR_AMOUNT_COPAY_BEFORE_DEDUCTIBLE_PERCENT_COINS = "$%2$s Copay before deductible, %1$s%% Coinsurance";
	private static final String DOLLAR_AMOUNT_COPAY_WITH_DEDUCTIBLE_PERCENT_COINS = "$%2$s Copay with deductible, %1$s%% Coinsurance";
	private static final String DOLLAR_AMOUNT_COPAY_AFTER_DEDUCT_PERCENT_COINS_AFTER_DEDUCT = "$%2$s Copay after deductible, %1$s%% Coinsurance after deductible";
	private static final String DOLLAR_AMOUNT_COPAY_BEFORE_DEDUCT_PERCENT_COINS_AFTER_DEDUCT = "$%2$s Copay before deductible, %1$s%% Coinsurance after deductible";
	private static final String DOLLAR_AMOUNT_COPAY_WITH_DEDUCT_PERCENT_COINS_AFTER_DEDUCT = "$%2$s Copay with deductible, %1$s%% Coinsurance after deductible";
	private static final String DOLLAR_AMOUNT_COPAY_PERCENT_COINS = "$%2$s Copay, %1$s%% Coinsurance";
	private static final String DOLLAR_AMOUNT_COPAY_PERCENT_COINS_AFTER_DEDUCT = "$%2$s Copay, %1$s%% Coinsurance after deductible";
	private static final String PERCENT_COINSURANCE = "%1$s%% Coinsurance";
	private static final String PERCENT_COINS_AFTER_DEDUCT = "%1$s%% Coinsurance after deductible";
	private static final String PERCENT_COINS_AFTER_DEDUCT_DOLLAR_AMOUNT_COPAY_AFTER_DEDUCT = "%1$s%% Coinsurance after deductible, $%2$s Copay after deductible";
	private static final String PERCENT_COINS_DOLLAR_AMOUNT_COPAY_AFTER_DEDUCTIBLE = "%1$s%% Coinsurance, $%2$s Copay after deductible";
	private static final String NOT_COVERED = "Drug administered under plan medical benefit, not as a drug benefit";
	
	
	private static final String[][] COPAY_COINS_DISPLAY_RULES = 
		{	{DOLLAR_AMOUNT_COPAY, PERCENT_COINS_AFTER_DEDUCT, PERCENT_COINSURANCE, PERCENT_COINS_AFTER_DEDUCT, DOLLAR_AMOUNT_COPAY},
			{DOLLAR_AMOUNT_COPAY_AFTER_DEDUCTIBLE, DOLLAR_AMOUNT_COPAY_AFTER_DEDUCTIBLE, PERCENT_COINS_DOLLAR_AMOUNT_COPAY_AFTER_DEDUCTIBLE, PERCENT_COINS_AFTER_DEDUCT_DOLLAR_AMOUNT_COPAY_AFTER_DEDUCT, DOLLAR_AMOUNT_COPAY_AFTER_DEDUCTIBLE},
			{DOLLAR_AMOUNT_COPAY, DOLLAR_AMOUNT_COPAY_PERCENT_COINS_AFTER_DEDUCT, DOLLAR_AMOUNT_COPAY_PERCENT_COINS, DOLLAR_AMOUNT_COPAY_PERCENT_COINS_AFTER_DEDUCT, DOLLAR_AMOUNT_COPAY},
			{DOLLAR_AMOUNT_COPAY_AFTER_DEDUCTIBLE, DOLLAR_AMOUNT_COPAY_AFTER_DEDUCT_PERCENT_COINS_AFTER_DEDUCT, DOLLAR_AMOUNT_COPAY_AFTER_DEDUCTIBLE_PERCENT_COINS, DOLLAR_AMOUNT_COPAY_AFTER_DEDUCT_PERCENT_COINS_AFTER_DEDUCT, DOLLAR_AMOUNT_COPAY_AFTER_DEDUCTIBLE},
			{DOLLAR_AMOUNT_COPAY_BEFORE_DEDUCTIBLE, DOLLAR_AMOUNT_COPAY_BEFORE_DEDUCT_PERCENT_COINS_AFTER_DEDUCT, DOLLAR_AMOUNT_COPAY_BEFORE_DEDUCTIBLE_PERCENT_COINS, DOLLAR_AMOUNT_COPAY_BEFORE_DEDUCT_PERCENT_COINS_AFTER_DEDUCT, DOLLAR_AMOUNT_COPAY_BEFORE_DEDUCTIBLE},
			{DOLLAR_AMOUNT_COPAY_WITH_DEDUCTIBLE, DOLLAR_AMOUNT_COPAY_WITH_DEDUCT_PERCENT_COINS_AFTER_DEDUCT, DOLLAR_AMOUNT_COPAY_WITH_DEDUCTIBLE_PERCENT_COINS, DOLLAR_AMOUNT_COPAY_WITH_DEDUCT_PERCENT_COINS_AFTER_DEDUCT, DOLLAR_AMOUNT_COPAY_WITH_DEDUCTIBLE},
			{PERCENT_COINSURANCE, PERCENT_COINS_AFTER_DEDUCT, PERCENT_COINSURANCE, PERCENT_COINS_AFTER_DEDUCT, NOT_COVERED}
		};
	
			
	@Override
	public List<PrescriptionSearchDTO> getPrescriptionSearchDrugInformation(String tempFormularyId, Integer issuerId, List<PrescriptionSearchRequest> prescriptionSearchRequstList) {
		try{
			if (!(tempFormularyId.isEmpty())) {
				Integer formularlyId = Integer.parseInt(tempFormularyId);
				List<Map<String, String>> drugInfoDBList = null;
				drugInfoDBList = getDrugNameFromFormularyId(formularlyId, issuerId, prescriptionSearchRequstList);
				
				if(null != drugInfoDBList && !(drugInfoDBList.isEmpty())){
					String drugTierIds = getStringFromListOfMap(drugInfoDBList);
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("formularlyId :::: drugTierIds: " + SecurityUtil.sanitizeForLogging(String.valueOf(formularlyId)) +" :::: "+ SecurityUtil.sanitizeForLogging(drugTierIds));
					}

					if(null != drugTierIds && !(drugTierIds.isEmpty())){
						List<Map<String,Map<String,Map<String,String>>>> drugRxCodeWithCostShareMapList = null;
						drugRxCodeWithCostShareMapList = getDrugCostShareInfo(formularlyId, drugTierIds, drugInfoDBList);
						if(null != drugRxCodeWithCostShareMapList && !(drugRxCodeWithCostShareMapList.isEmpty())){
							drugCostShareInfoList = new ArrayList<Map<String, String>>();
							drugCostShareInfoList = getDrugInfoUsingCostShare(drugRxCodeWithCostShareMapList, prescriptionSearchRequstList);
							if(LOGGER.isDebugEnabled()) {
								LOGGER.debug("drugCostShareInfoList: " + drugCostShareInfoList);
							}	
						}

					}
				}
				prescriptionSearchDTOList = getPrescriptionSearchResponseforFormularyId(drugInfoDBList, drugCostShareInfoList, prescriptionSearchRequstList);
			} else {
				prescriptionSearchDTOList = getPrescriptionSearchResponseforNonFormularyId(prescriptionSearchRequstList);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getPrescriptionSearchDrugInformation: ", ex);
		}
		return prescriptionSearchDTOList;
	}

	/**
	 * create comma separated list using list of object
	 * @param drugsNamesList
	 * @return
	 */
	/*private String getStringFromList(List<PrescriptionSearchRequest> prescriptionSearchRequstList) {
		StringBuilder rxCodeBuilder = new StringBuilder();
		try {
			for(PrescriptionSearchRequest prescriptionSearchRequest : prescriptionSearchRequstList){
				if(null != prescriptionSearchRequest.getDrugRxCode() && !(prescriptionSearchRequest.getDrugRxCode().isEmpty())){
					if(rxCodeBuilder.toString().isEmpty()){
						rxCodeBuilder.append(prescriptionSearchRequest.getDrugRxCode());
					}else{
						rxCodeBuilder.append(PlanMgmtConstants.COMMA);
						rxCodeBuilder.append(prescriptionSearchRequest.getDrugRxCode());
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception in getStringFromList: ", e);
		}
		return rxCodeBuilder.toString();
	}
	*/
	/**
	 * Returns DrugName and DrugTier by using below Data
	 * @param formularlyId
	 * @param issuerId
	 * @param drugsNamesList
	 * @return List of Map with drug information
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Map<String, String>> getDrugNameFromFormularyId(int formularlyId, int issuerId, List<PrescriptionSearchRequest> prescriptionSearchRequstList) {
		Map<String, String> drugName = null;
		List<Map<String, String>> drugNameListFromFormularlyId = new ArrayList<Map<String, String>>();
		List<Map<String, String>> returnDurgNameListFromFormularlyId = new ArrayList<Map<String, String>>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			StringBuilder buildquery = new StringBuilder();
			StringBuilder questionMarkBuilder = new StringBuilder();
			buildquery.append("SELECT DISTINCT(pd.rxcui),pdt.drug_tier_level FROM formulary F,pm_drug_list PDL,pm_drug_tier PDT,pm_drug PD WHERE f.drug_list_id=PDL.id AND PDT.drug_list_id=PDL.id ");
			buildquery.append("AND PD.drug_tier_id = PDT.id AND PDL.is_deleted = 'N' AND F.issuer_id= (:" + ISSUERID + ") AND f.ID= (:" + FORMULARLYID + ") ");

			if(null != prescriptionSearchRequstList && !(prescriptionSearchRequstList.isEmpty())){
				int index = 1;
				for(PrescriptionSearchRequest prescriptionSearchRequest : prescriptionSearchRequstList){
					if(null != prescriptionSearchRequest.getDrugRxCode() && !(prescriptionSearchRequest.getDrugRxCode().isEmpty())){
						if(null != questionMarkBuilder && !(questionMarkBuilder.toString().isEmpty())){
							questionMarkBuilder.append(PlanMgmtConstants.COMA_QUESTION_MARK);
							questionMarkBuilder.append(String.valueOf(index++));
						}else{
							questionMarkBuilder.append(PlanMgmtConstants.QUESTION_MARK);
							questionMarkBuilder.append(String.valueOf(index++));
						}
					}
				}
			}

			if (StringUtils.isNotBlank(questionMarkBuilder.toString())) {
				buildquery.append(" AND PD.rxcui IN (");
			buildquery.append(questionMarkBuilder);
				buildquery.append(") ");
			}

			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("QueryForGettingdrugName: " + buildquery);
			}
			Query query = entityManager.createNativeQuery(buildquery.toString());
			query.setParameter(ISSUERID, issuerId);
			query.setParameter(FORMULARLYID, formularlyId);
			
			if(null != prescriptionSearchRequstList && !(prescriptionSearchRequstList.isEmpty())){
				int index = 1;
				for(PrescriptionSearchRequest prescriptionSearchRequest : prescriptionSearchRequstList){
					if(null != prescriptionSearchRequest.getDrugRxCode() && !(prescriptionSearchRequest.getDrugRxCode().isEmpty())){
						query.setParameter(index, prescriptionSearchRequest.getDrugRxCode());
						index++;
					}
				}
			}			
			
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
			
			while (rsIterator.hasNext()) {
				drugName = new HashMap<String, String>();
				Object[] objArray = (Object[]) rsIterator.next();
				drugName.put(PlanMgmtConstants.DRUG_RX_CODE, objArray[0].toString());
				drugName.put(PlanMgmtConstants.DRUG_TIER, null != objArray[1] ? String.valueOf(objArray[1]) : PlanMgmtConstants.EMPTY_STRING);
				drugNameListFromFormularlyId.add(drugName);
			}
			
			if(null != prescriptionSearchRequstList && !(prescriptionSearchRequstList.isEmpty())){
				for(PrescriptionSearchRequest prescriptionSearchRequest : prescriptionSearchRequstList){
					if(null != prescriptionSearchRequest.getDrugRxCode() && !(prescriptionSearchRequest.getDrugRxCode().isEmpty())){
						drugName = new HashMap<String, String>();
						for(Map<String,String> drugMapFromDB : drugNameListFromFormularlyId){
							if(null != drugMapFromDB.get(PlanMgmtConstants.DRUG_RX_CODE) && prescriptionSearchRequest.getDrugRxCode().equalsIgnoreCase(drugMapFromDB.get(PlanMgmtConstants.DRUG_RX_CODE))){
								//drugName.put(PlanMgmtConstants.DRUG_RX_CODE, drugMapFromDB.get(PlanMgmtConstants.DRUG_RX_CODE));
								drugName.put(PlanMgmtConstants.DRUG_RX_CODE, prescriptionSearchRequest.getDrugRxCode());
								drugName.put(PlanMgmtConstants.DRUG_TIER, drugMapFromDB.get(PlanMgmtConstants.DRUG_TIER));
								drugName.put(PlanMgmtConstants.IS_SUPPORT_PLAN, PlanMgmtConstants.Y);
								break;
							}else{
								drugName.put(PlanMgmtConstants.DRUG_RX_CODE, prescriptionSearchRequest.getDrugRxCode());
								drugName.put(PlanMgmtConstants.DRUG_TIER, PlanMgmtConstants.EMPTY_STRING);
								drugName.put(PlanMgmtConstants.IS_SUPPORT_PLAN, PlanMgmtConstants.N);
							}
						}
						if(!(drugName.isEmpty())){
							returnDurgNameListFromFormularlyId.add(drugName);
						}
					}
				}
			}
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("returnDurgNameListFromFormularlyId: " + returnDurgNameListFromFormularlyId);
			}	
		}catch(Exception ex){
			LOGGER.error("Exception occured in getDrugNameFromFormularlyId: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return returnDurgNameListFromFormularlyId;
	}
	
	/**
	 * create comma separated string using list map of Strings
	 * @param drugInfoDBList
	 * @return comma separated drug tier id
	 */
	private String getStringFromListOfMap(List<Map<String,String>>drugInfoDBList){
		StringBuilder drugTierIds = new StringBuilder();
		for(Map<String,String> drugInfoMap : drugInfoDBList){
			if(null != drugInfoMap.get(PlanMgmtConstants.DRUG_TIER) && !(drugInfoMap.get(PlanMgmtConstants.DRUG_TIER).isEmpty())){
				if(drugTierIds.toString().isEmpty()){
					drugTierIds.append(drugInfoMap.get(PlanMgmtConstants.DRUG_TIER));
				}else{
					drugTierIds.append(PlanMgmtConstants.COMMA);
					drugTierIds.append(drugInfoMap.get(PlanMgmtConstants.DRUG_TIER));
				}
			}
		}
		return drugTierIds.toString();
	}
	
	/**
	 * Returns List Map of cost share Information by using below data
	 * @param formularyId
	 * @param drugTierLevel
	 * @return list of Map of Map cost share information
	 */
	@Override
	@Transactional(readOnly = true)
	public List<Map<String,Map<String,Map<String,String>>>> getDrugCostShareInfo(Integer formularyId, String drugTierIds, List<Map<String,String>> drugInfoDBList) {
		List<Map<String,Map<String,Map<String,String>>>> drugRxCodewithCostShareList = null;
		Map<String, String> costShareMapInfo = null;
		Map<String, Map<String, String>> drugPrescPeriodMap = null;
		List<Map<String, Map<String, String>>> finalCostShareList = new ArrayList<Map<String, Map<String, String>>>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			StringBuilder buildquery = new StringBuilder();
			String[] drugIdStrArr =  drugTierIds.replace("'", "").split(",");
			StringBuilder questionMarkBuilder = new StringBuilder();
			
			buildquery.append("select distinct(formulary_id), drug_tier_level, cost_share_type, copayment_amount, coinsurance_percent, network_cost_type, drug_prescription_period from cost_share where formulary_id= ");
			buildquery.append("(:" + FORMULARYID + ")");
			buildquery.append(" and drug_tier_level IN (");
			if(!(StringUtils.isEmpty(drugTierIds))){
				int index = 1;
				for(String drugId : drugIdStrArr ){
					if(!(StringUtils.isEmpty(drugId))){
						if(null != questionMarkBuilder && !(questionMarkBuilder.toString().isEmpty())){
							questionMarkBuilder.append(PlanMgmtConstants.COMA_QUESTION_MARK);
							questionMarkBuilder.append(String.valueOf(index++));
						}else{
							questionMarkBuilder.append(PlanMgmtConstants.QUESTION_MARK);
							questionMarkBuilder.append(String.valueOf(index++));
						}
					}
				}
			}
			buildquery.append(questionMarkBuilder);
			buildquery.append(") and network_cost_type = 'In Network' and drug_prescription_period = 1 AND is_deleted = 'N'");
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("QueryForGettingCostShareInfo: "+ buildquery);
			}
			Query query = entityManager.createNativeQuery(buildquery.toString());			
			query.setParameter(FORMULARYID, formularyId);

			if (!(StringUtils.isEmpty(drugTierIds))) {

				int index = 1;
				for (String drugId : drugIdStrArr) {

					if (StringUtils.isNumeric(drugId)) {
						query.setParameter(index, Integer.valueOf(drugId));
						index++;
					}
				}
			}

			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
	
			while (rsIterator.hasNext()) {
				costShareMapInfo = new HashMap<String, String>();
				drugPrescPeriodMap = new HashMap<String, Map<String, String>>();
	
				Object[] objArray = (Object[]) rsIterator.next();
				costShareMapInfo.put("formulary_id", objArray[PlanMgmtConstants.ZERO].toString());
				costShareMapInfo.put(PlanMgmtConstants.DRUG_TIER, objArray[PlanMgmtConstants.ONE].toString());
				costShareMapInfo.put(PlanMgmtConstants.COST_SHARE_TYPE, objArray[PlanMgmtConstants.TWO].toString());
				costShareMapInfo.put(PlanMgmtConstants.COPAYMENT_AMOUNT, objArray[PlanMgmtConstants.THREE].toString());
				costShareMapInfo.put(PlanMgmtConstants.COINSURANCE_PERCENT, objArray[PlanMgmtConstants.FOUR].toString());
				costShareMapInfo.put("network_cost_type", objArray[PlanMgmtConstants.FIVE].toString());
				costShareMapInfo.put("drug_prescription_period", objArray[PlanMgmtConstants.SIX].toString());
				if (objArray[PlanMgmtConstants.FIVE] != null) {
					drugPrescPeriodMap.put(objArray[PlanMgmtConstants.ONE].toString(), costShareMapInfo);
				}
				finalCostShareList.add(drugPrescPeriodMap);
			}
			
			if(null != finalCostShareList && !(finalCostShareList.isEmpty())){
				drugRxCodewithCostShareList = new ArrayList<Map<String,Map<String,Map<String,String>>>>();
				Map<String,Map<String,Map<String,String>>> tempDurgRxCodewithCostShareMap = null;
				Map<String, Map<String, String>> drugCostShareMap = null;
				
				for(Map<String,String> drugInfoMap : drugInfoDBList){
					for(Map<String,Map<String,String>> costShareInfoMap: finalCostShareList){
						Map<String,String> costInfoMap = costShareInfoMap.get(drugInfoMap.get(PlanMgmtConstants.DRUG_TIER));
						if(null !=  costInfoMap && !(costInfoMap.isEmpty())){
							tempDurgRxCodewithCostShareMap = new HashMap<String,Map<String,Map<String,String>>>();
							drugCostShareMap = new HashMap<String, Map<String, String>>();
							drugCostShareMap.put(PlanMgmtConstants.FOR_PRESC_PERIOD_1, costInfoMap);
							tempDurgRxCodewithCostShareMap.put(drugInfoMap.get(PlanMgmtConstants.DRUG_RX_CODE), drugCostShareMap);
							drugRxCodewithCostShareList.add(tempDurgRxCodewithCostShareMap);
						}
					}
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getDrugCostShareInfo: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return drugRxCodewithCostShareList;
	}
	
	/**
	 * Returns Map of amount by using below data and depending on
	 * Cost_Sharing_Type from cost share table
	 * @param costShareInfoList
	 * @param drugNameList
	 * @param drugPrescPeriod
	 * @return Map of drug information with cost share information
	 */
	private List<Map<String, String>> getDrugInfoUsingCostShare(List<Map<String,Map<String,Map<String,String>>>> costShareInfoList, List<PrescriptionSearchRequest> prescriptionSearchRequstList) {
		Map<String, String> costShareInfoMap = null;
		List<Map<String, String>> listOfAmountInfoMap = new ArrayList<Map<String, String>>();
		Integer keyGeneratorId = PlanMgmtConstants.ONE;
		try{
			List<Map<String, String>> listOfGreaterOfCopayorCoins = new ArrayList<Map<String,String>>();
			for(PrescriptionSearchRequest prescriptionSearchRequest : prescriptionSearchRequstList){
				if(null != prescriptionSearchRequest.getDrugRxCode() && !(prescriptionSearchRequest.getDrugRxCode().isEmpty())){
					for(Map<String,Map<String,Map<String,String>>> drugRxCodeCostShareInfoMap : costShareInfoList){
						Map<String, Map<String, String>> costShareMap = drugRxCodeCostShareInfoMap.get(prescriptionSearchRequest.getDrugRxCode());
						if(null != costShareMap && !(costShareMap.isEmpty())){
							costShareInfoMap = costShareMap.get(PlanMgmtConstants.FOR_PRESC_PERIOD_1);
							if (costShareInfoMap != null && costShareInfoMap.get(PlanMgmtConstants.COST_SHARE_TYPE) != null && !(costShareInfoMap.get(PlanMgmtConstants.COST_SHARE_TYPE).isEmpty())) {
								Map<String, String> copayAndCoinsuranceAmountMap = getValuesOfCopayAndCoinsuranceAmount(costShareInfoMap.get(PlanMgmtConstants.COST_SHARE_TYPE), costShareInfoMap.get(PlanMgmtConstants.COPAYMENT_AMOUNT), costShareInfoMap.get(PlanMgmtConstants.COINSURANCE_PERCENT), prescriptionSearchRequest, keyGeneratorId);
								listOfGreaterOfCopayorCoins.add(copayAndCoinsuranceAmountMap);
							}
						}
					}
					keyGeneratorId++;
				}
			}
			if(null != costShareInfoMap && null != costShareInfoMap.get(PlanMgmtConstants.COST_SHARE_TYPE))  {
				listOfAmountInfoMap = getAmountInfoMap(listOfGreaterOfCopayorCoins, costShareInfoMap.get(PlanMgmtConstants.COST_SHARE_TYPE));
			}
	
		}catch(Exception ex){
			LOGGER.error("Exception occured in getDrugInfoUsingCostShare: ", ex);
		}
		return listOfAmountInfoMap;
	}

	/**
	 * Returns Map of amount depending on copayment and coinsurance values
	 * @param copaymentAmount
	 * @param coinsurancePercent
	 * @param drugNameList
	 * @return map of drug price using fair price and copay amount and coinsurance amount 
	 */
	private Map<String, String> getValuesOfCopayAndCoinsuranceAmount(String costShareType, String copaymentAmount, String coinsurancePercent, PrescriptionSearchRequest filteredprescriptionSearchRequest, Integer keyGeneratorId){
		Map<String, String> greaterofcopayorcoins = null;
		String drugPriceFromAPI = PlanMgmtConstants.EMPTY_STRING;
		try{
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("checking for costShareType: " + SecurityUtil.sanitizeForLogging(costShareType));
			}
			if(null != filteredprescriptionSearchRequest.getDrugRxCode() && !(filteredprescriptionSearchRequest.getDrugRxCode().isEmpty())){
				switch(costShareType){
					/* Checking For COPAYMENT */
					case PlanMgmtConstants.COPAYMENT:
						if (copaymentAmount != null && !copaymentAmount.equalsIgnoreCase(PlanMgmtConstants.STR_NULL)) {
							Double tempcopaymentAmount = Double.parseDouble(copaymentAmount);
							if (tempcopaymentAmount != 0) {
								greaterofcopayorcoins = new HashMap<String, String>();
								String rxCodeKey = filteredprescriptionSearchRequest.getDrugRxCode() + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
								greaterofcopayorcoins.put(PlanMgmtConstants.RX_CODE_KEY, rxCodeKey);
								greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_RX_CODE, filteredprescriptionSearchRequest.getDrugRxCode());
								greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_PRICE, filteredprescriptionSearchRequest.getFairCost());
								greaterofcopayorcoins.put(PlanMgmtConstants.COPAYMENT_AMOUNT, String.valueOf(tempcopaymentAmount));
							} else {
								greaterofcopayorcoins = new HashMap<String, String>();
								String rxCodeKey = filteredprescriptionSearchRequest.getDrugRxCode() + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
								greaterofcopayorcoins.put(PlanMgmtConstants.RX_CODE_KEY, rxCodeKey);
								greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_RX_CODE, filteredprescriptionSearchRequest.getDrugRxCode());
								greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_PRICE, filteredprescriptionSearchRequest.getFairCost());
								greaterofcopayorcoins.put(PlanMgmtConstants.COPAYMENT_AMOUNT, PlanMgmtConstants.ZERO_STR);
							}
						}
					break;
					/* Checking For COINSURANCE */
					case PlanMgmtConstants.COINSURANCE_STR:
						if (coinsurancePercent != null && !coinsurancePercent.equalsIgnoreCase(PlanMgmtConstants.STR_NULL)) {
							Double intCoinsurancePercentage = Double.parseDouble(coinsurancePercent);
							if (intCoinsurancePercentage != 0) {
								greaterofcopayorcoins = new HashMap<String, String>();
								if (filteredprescriptionSearchRequest.getFairCost() != null && !(filteredprescriptionSearchRequest.getFairCost().isEmpty())
										&& !PlanMgmtConstants.STR_NULL.equalsIgnoreCase(filteredprescriptionSearchRequest.getFairCost().trim())) {
									//Float fairPrice = Float.parseFloat(filteredprescriptionSearchRequest.getFairPrice());
									Double fairPrice = Double.parseDouble(filteredprescriptionSearchRequest.getFairCost());
									if (fairPrice != 0 && intCoinsurancePercentage != 0) {
										String stringFairPrice = calculatePrice(fairPrice, intCoinsurancePercentage);
										String rxCodeKey = filteredprescriptionSearchRequest.getDrugRxCode() + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
										greaterofcopayorcoins.put(PlanMgmtConstants.RX_CODE_KEY, rxCodeKey);
										greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_RX_CODE, filteredprescriptionSearchRequest.getDrugRxCode());
										greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_AMOUNT, stringFairPrice);
										greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_PERCENT, String.valueOf(intCoinsurancePercentage));
										greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_PRICE, filteredprescriptionSearchRequest.getFairCost());
									}
								}else{
									String rxCodeKey = filteredprescriptionSearchRequest.getDrugRxCode() + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
									greaterofcopayorcoins.put(PlanMgmtConstants.RX_CODE_KEY, rxCodeKey);
									greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_RX_CODE, filteredprescriptionSearchRequest.getDrugRxCode());
									greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_AMOUNT, PlanMgmtConstants.EMPTY_STRING);
									greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_PERCENT, String.valueOf(intCoinsurancePercentage));
									greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_PRICE, filteredprescriptionSearchRequest.getFairCost());
								}
							} else {
								greaterofcopayorcoins = new HashMap<String, String>();
								String rxCodeKey = filteredprescriptionSearchRequest.getDrugRxCode() + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
								greaterofcopayorcoins.put(PlanMgmtConstants.RX_CODE_KEY, rxCodeKey);
								greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_PERCENT, coinsurancePercent);
								greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_RX_CODE, filteredprescriptionSearchRequest.getDrugRxCode());
								greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_AMOUNT, PlanMgmtConstants.ZERO_STR);
								greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_PRICE, drugPriceFromAPI);
							}
						}
						/* Checking For COPAY COINSURANCE */
					case PlanMgmtConstants.COPAY_COINSURANCE:
						if (copaymentAmount != null && coinsurancePercent != null && !copaymentAmount.equalsIgnoreCase(PlanMgmtConstants.STR_NULL) && !coinsurancePercent.equalsIgnoreCase(PlanMgmtConstants.STR_NULL) ) {
							Double intCoinsurancePercentage = Double.parseDouble(coinsurancePercent);
							Double tempcopaymentAmount = Double.parseDouble(copaymentAmount);
							
							if (tempcopaymentAmount != 0 && intCoinsurancePercentage == 0) { // if copay amount is non zero but co-insurance is zero then send copay amount
								greaterofcopayorcoins = new HashMap<String, String>();
								String rxCodeKey = filteredprescriptionSearchRequest.getDrugRxCode() + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
								greaterofcopayorcoins.put(PlanMgmtConstants.RX_CODE_KEY, rxCodeKey);
								greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_RX_CODE, filteredprescriptionSearchRequest.getDrugRxCode());
								greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_PRICE, filteredprescriptionSearchRequest.getFairCost());
								greaterofcopayorcoins.put(PlanMgmtConstants.COPAYMENT_AMOUNT, String.valueOf(tempcopaymentAmount));
							}else if (tempcopaymentAmount == 0  && intCoinsurancePercentage != 0) { // if copay amount is zero but co-insurance is non zero then send co-insurance amount
									greaterofcopayorcoins = new HashMap<String, String>();
										if (filteredprescriptionSearchRequest.getFairCost() != null && !(filteredprescriptionSearchRequest.getFairCost().isEmpty()) 
												&& !PlanMgmtConstants.STR_NULL.equalsIgnoreCase(filteredprescriptionSearchRequest.getFairCost().trim())) {
											//Float fairPrice = Float.parseFloat(filteredprescriptionSearchRequest.getFairPrice());
											Double fairPrice = Double.parseDouble(filteredprescriptionSearchRequest.getFairCost());
											if (fairPrice != 0 && intCoinsurancePercentage != 0) {
												String stringFairPrice = calculatePrice(fairPrice, intCoinsurancePercentage);
												String rxCodeKey = filteredprescriptionSearchRequest.getDrugRxCode() + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
												greaterofcopayorcoins.put(PlanMgmtConstants.RX_CODE_KEY, rxCodeKey);
												greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_RX_CODE, filteredprescriptionSearchRequest.getDrugRxCode());
												greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_AMOUNT, stringFairPrice);
												greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_PERCENT, String.valueOf(intCoinsurancePercentage));
												greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_PRICE, filteredprescriptionSearchRequest.getFairCost());
											}
										}else{
											String rxCodeKey = filteredprescriptionSearchRequest.getDrugRxCode() + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
											greaterofcopayorcoins.put(PlanMgmtConstants.RX_CODE_KEY, rxCodeKey);
											greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_RX_CODE, filteredprescriptionSearchRequest.getDrugRxCode());
											greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_AMOUNT, PlanMgmtConstants.EMPTY_STRING);
											greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_PERCENT, String.valueOf(intCoinsurancePercentage));
											greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_PRICE, filteredprescriptionSearchRequest.getFairCost());
										}
									
								}else if (tempcopaymentAmount >= 0  && intCoinsurancePercentage >= 0) { // if copay amount and co-insurance are zero or greater than zero then send both amount
									greaterofcopayorcoins = new HashMap<String, String>();
									String rxCodeKey = filteredprescriptionSearchRequest.getDrugRxCode() + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
									greaterofcopayorcoins.put(PlanMgmtConstants.RX_CODE_KEY, rxCodeKey);
									greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_RX_CODE, filteredprescriptionSearchRequest.getDrugRxCode());
									greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_PRICE, filteredprescriptionSearchRequest.getFairCost());
									greaterofcopayorcoins.put(PlanMgmtConstants.COPAYMENT_AMOUNT, String.valueOf(tempcopaymentAmount));
									
									if (filteredprescriptionSearchRequest.getFairCost() != null && !(filteredprescriptionSearchRequest.getFairCost().isEmpty())
											&& !PlanMgmtConstants.STR_NULL.equalsIgnoreCase(filteredprescriptionSearchRequest.getFairCost().trim())) {
										Double fairPrice = Double.parseDouble(filteredprescriptionSearchRequest.getFairCost());
										if (fairPrice != 0 && intCoinsurancePercentage != 0) {
											String stringFairPrice = calculatePrice(fairPrice, intCoinsurancePercentage);
											greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_AMOUNT, stringFairPrice);
											greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_PERCENT, String.valueOf(intCoinsurancePercentage));
										}
									}else{
										greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_AMOUNT, PlanMgmtConstants.EMPTY_STRING);
										greaterofcopayorcoins.put(PlanMgmtConstants.COINSURANCE_PERCENT, String.valueOf(intCoinsurancePercentage));
									}
								}
							}
							else {
								greaterofcopayorcoins = new HashMap<String, String>();
								String rxCodeKey = filteredprescriptionSearchRequest.getDrugRxCode() + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
								greaterofcopayorcoins.put(PlanMgmtConstants.RX_CODE_KEY, rxCodeKey);
								greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_RX_CODE, filteredprescriptionSearchRequest.getDrugRxCode());
								greaterofcopayorcoins.put(PlanMgmtConstants.DRUG_PRICE, filteredprescriptionSearchRequest.getFairCost());
								greaterofcopayorcoins.put(PlanMgmtConstants.COPAYMENT_AMOUNT, PlanMgmtConstants.ZERO_STR);
							}
				
					break;
					default:
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug("No Implementation Provided");
						}	
					break;
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getValuesOfCopayAndCoinsuranceAmount: ", ex);
		}
		return greaterofcopayorcoins;
	}
	
	/**
	 * Retuns fair price of drug depending on percentage
	 * (coinsurance_percentage)
	 * @param fairPrice
	 * @param percentage
	 * @return fair price value calculated from coinsurance percentage
	 */
	private String calculatePrice(Double fairPrice, Double percentage) {
		Double doubleFairPrice = 0.0D;
		try{
			doubleFairPrice = (percentage / PlanMgmtConstants.HUNDRED) * fairPrice;
		}catch(Exception ex){
			LOGGER.error("Exception occured in calculatePrice: ", ex);
		}
		return String.valueOf(doubleFairPrice.floatValue());
	}
	
	/**
	 * @param listOfGreaterOfCopayorCoins
	 * @param costShareType
	 * @return Map with Amount information of the drug
	 */
	private List<Map<String,String>> getAmountInfoMap(List<Map<String, String>> listOfGreaterOfCopayorCoins, String costShareType){
		List<Map<String,String>> listOfAmountInfoMap = new ArrayList<Map<String,String>>();
		Map<String,String> amountInfoMap = null;
		try{
			for(Map<String,String> greaterofcopayorcoins : listOfGreaterOfCopayorCoins){
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("greaterofcopayorcoins " + greaterofcopayorcoins  );
				}	
				amountInfoMap = new HashMap<String,String>();
				switch(costShareType){
					/* Checking For COPAYMENT */
					case PlanMgmtConstants.COPAYMENT:
						if (greaterofcopayorcoins != null && greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT) != null) {
							amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
							amountInfoMap.put(PlanMgmtConstants.COPAYMENT_AMOUNT, greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT));
							//amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
							//amountInfoMap.put(PlanMgmtConstants.DRUG_RX_CODE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_RX_CODE));
							//amountInfoMap.put(PlanMgmtConstants.DRUG_PRICE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_PRICE));

						} else {
							/*if(null != greaterofcopayorcoins){
								amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
							}*/
							amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
							amountInfoMap.put(PlanMgmtConstants.COPAYMENT_AMOUNT, PlanMgmtConstants.ZERO_STR);
							//amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
							//amountInfoMap.put(PlanMgmtConstants.DRUG_RX_CODE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_RX_CODE));
							//amountInfoMap.put(PlanMgmtConstants.DRUG_PRICE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_PRICE));
						}
					break;
					/* Checking For COINSURANCE */
					case PlanMgmtConstants.COINSURANCE_STR:
						if (greaterofcopayorcoins != null && (null != greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT) || null != greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_PERCENT))) {
							amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
							amountInfoMap.put(PlanMgmtConstants.COINSURANCE_AMOUNT, greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT));
							amountInfoMap.put(PlanMgmtConstants.COINSURANCE_PERCENTAGE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_PERCENT));
							//amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
							//amountInfoMap.put(PlanMgmtConstants.DRUG_RX_CODE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_RX_CODE));
							//amountInfoMap.put(PlanMgmtConstants.DRUG_PRICE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_PRICE));
						} else {
							/*if(null != greaterofcopayorcoins){
								amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
							}*/
							amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
							amountInfoMap.put(PlanMgmtConstants.COINSURANCE_AMOUNT, PlanMgmtConstants.ZERO_STR);
							amountInfoMap.put(PlanMgmtConstants.COINSURANCE_PERCENTAGE_KEY, PlanMgmtConstants.ZERO_STR);
							//amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
							//amountInfoMap.put(PlanMgmtConstants.DRUG_RX_CODE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_RX_CODE));
							//amountInfoMap.put(PlanMgmtConstants.DRUG_PRICE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_PRICE));
						}
					break;
					/* Checking For COPAY COINSURANCE */
					case PlanMgmtConstants.COPAY_COINSURANCE:
						
						if (greaterofcopayorcoins != null &&  null != greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT) && null == greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT) 
							&& Double.parseDouble(greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT)) > 0 ){ // when only copay exists
							amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
							amountInfoMap.put(PlanMgmtConstants.COPAYMENT_AMOUNT, greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT));
						}else if (greaterofcopayorcoins != null &&  null == greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT) && null != greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT) 
							&&  new Double(greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_PERCENT)).intValue() > 0  ){ // when only co-insurance exists
							amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
							amountInfoMap.put(PlanMgmtConstants.COINSURANCE_AMOUNT, greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT));
							amountInfoMap.put(PlanMgmtConstants.COINSURANCE_PERCENTAGE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_PERCENT));
						}else if (greaterofcopayorcoins != null &&  null != greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT) && null != greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT) 
								&& Double.parseDouble(greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT)) >= 0 && new Double(greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_PERCENT)).intValue() >= 0  ){ // when copay and co-insurance both exists
							amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
							amountInfoMap.put(PlanMgmtConstants.COPAYMENT_AMOUNT, greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT));
							amountInfoMap.put(PlanMgmtConstants.COINSURANCE_AMOUNT, greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT));
							amountInfoMap.put(PlanMgmtConstants.COINSURANCE_PERCENTAGE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_PERCENT));
						} 
						else {
							amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
							amountInfoMap.put(PlanMgmtConstants.COPAYMENT_AMOUNT, PlanMgmtConstants.ZERO_STR);
							amountInfoMap.put(PlanMgmtConstants.COINSURANCE_AMOUNT, PlanMgmtConstants.ZERO_STR);
							amountInfoMap.put(PlanMgmtConstants.COINSURANCE_PERCENTAGE_KEY, PlanMgmtConstants.ZERO_STR);
						}
					break;
					/* Checking For Greater of Copayment or Coinsurance  */
					case PlanMgmtConstants.GRATER_OF_COPAYMENT_OR_COINSURANCE:
						if (greaterofcopayorcoins != null && greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT) != null
								&& greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT) != null) {
							Float copaymentamount = Float.parseFloat(greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT));
							Float coinsuranceamount = Float.parseFloat(greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT));
							if (copaymentamount > coinsuranceamount) {
								/*if(null != greaterofcopayorcoins){
									amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
								}*/
								amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
								amountInfoMap.put(PlanMgmtConstants.COPAYMENT_AMOUNT, String.valueOf(copaymentamount));
								//amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
								//amountInfoMap.put(PlanMgmtConstants.DRUG_RX_CODE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_RX_CODE));
								//amountInfoMap.put(PlanMgmtConstants.DRUG_PRICE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_PRICE));
							} else {
								/*if(null != greaterofcopayorcoins){
									amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
								}*/
								amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
								amountInfoMap.put(PlanMgmtConstants.COINSURANCE_AMOUNT, String.valueOf(coinsuranceamount));
								//amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
								//amountInfoMap.put(PlanMgmtConstants.DRUG_RX_CODE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_RX_CODE));
								//amountInfoMap.put(PlanMgmtConstants.DRUG_PRICE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_PRICE));
							}
						}
					break;
					/* Checking For Lesser of Copayment or Coinsurance */
					case PlanMgmtConstants.LESSER_OF_COPAYMENT_OR_COINSURANCE:
						if (greaterofcopayorcoins != null && greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT) != null
								&& greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT) != null) {
							Float copaymentamount = Float.parseFloat(greaterofcopayorcoins.get(PlanMgmtConstants.COPAYMENT_AMOUNT));
							Float coinsuranceamount = Float.parseFloat(greaterofcopayorcoins.get(PlanMgmtConstants.COINSURANCE_AMOUNT));
							if (copaymentamount < coinsuranceamount) {
								/*if(null != greaterofcopayorcoins){
									amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
								}*/
								amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
								amountInfoMap.put(PlanMgmtConstants.COPAYMENT_AMOUNT, String.valueOf(copaymentamount));
								//amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
								//amountInfoMap.put(PlanMgmtConstants.DRUG_RX_CODE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_RX_CODE));
								//amountInfoMap.put(PlanMgmtConstants.DRUG_PRICE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_PRICE));
							} else {
								/*if(null != greaterofcopayorcoins){
									amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
								}*/
								amountInfoMap = setAmountInfoMap(greaterofcopayorcoins);
								amountInfoMap.put(PlanMgmtConstants.COINSURANCE_AMOUNT, String.valueOf(coinsuranceamount));
								//amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
								//amountInfoMap.put(PlanMgmtConstants.DRUG_RX_CODE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_RX_CODE));
								//amountInfoMap.put(PlanMgmtConstants.DRUG_PRICE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_PRICE));
							}
						}
					break;
					default:
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug("No Implementation Provided");
						}	
					break;
				}
				listOfAmountInfoMap.add(amountInfoMap);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getAmountInfoMap: ", ex);
		}
		return listOfAmountInfoMap;
		
	}
	
	/**
	 * @param greaterofcopayorcoins
	 * @return return amountInfoMaps common fields
	 */
	private Map<String, String> setAmountInfoMap(Map<String,String> greaterofcopayorcoins){
		Map<String, String> amountInfoMap = new HashMap<String,String>();
		if(null != greaterofcopayorcoins){
			amountInfoMap.put(PlanMgmtConstants.RX_CODE_KEY, greaterofcopayorcoins.get(PlanMgmtConstants.RX_CODE_KEY));
			amountInfoMap.put(PlanMgmtConstants.DRUG_RX_CODE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_RX_CODE));
			amountInfoMap.put(PlanMgmtConstants.DRUG_PRICE, greaterofcopayorcoins.get(PlanMgmtConstants.DRUG_PRICE));
		}
		return amountInfoMap;
	}
	
	/**
	 * @param drugInfoDBMap
	 * @param drugInfoFromAPI
	 * @return list of PrescriptionSearchDTO
	 */
	private List<PrescriptionSearchDTO> getPrescriptionSearchResponseforFormularyId(List<Map<String,String>> drugInfoDBList, List<Map<String,String>> drugCostShareInfoList, List<PrescriptionSearchRequest> prescriptionSearchRequstList){
		List<PrescriptionSearchDTO> responseDTOList = new ArrayList<PrescriptionSearchDTO>();
		List<Map<String,Map<String,String>>> resultantMapList = new ArrayList<Map<String,Map<String,String>>>();
		Integer keyGeneratorId = PlanMgmtConstants.ONE;
		String rxCodeKey = null;
		try{
			if((null != drugInfoDBList && !(drugInfoDBList.isEmpty())) && (null != drugCostShareInfoList && !(this.drugCostShareInfoList.isEmpty()))){
				resultantMapList = getMergedMap(drugInfoDBList, drugCostShareInfoList);
			}
			
			for(PrescriptionSearchRequest prescriptionSearchRequest : prescriptionSearchRequstList){
				PrescriptionSearchDTO prescriptionSearchDTO = new PrescriptionSearchDTO();
				prescriptionSearchDTO.setRxCuiCode(prescriptionSearchRequest.getDrugRxCode());
				prescriptionSearchDTO.setDrugName(prescriptionSearchRequest.getDrugName());
				prescriptionSearchDTO.setDrugNameForTooltip(prescriptionSearchRequest.getDrugNameForTooltip());
				prescriptionSearchDTO.setFairCost(prescriptionSearchRequest.getFairCost());
				
				if((null != drugInfoDBList && !(drugInfoDBList.isEmpty())) && (null != drugCostShareInfoList && !(drugCostShareInfoList.isEmpty()))){
					if(null != prescriptionSearchRequest.getDrugRxCode() && !(prescriptionSearchRequest.getDrugRxCode().isEmpty())){
						rxCodeKey = prescriptionSearchRequest.getDrugRxCode() +PlanMgmtConstants.UNDERSCORE_STRING+ String.valueOf(keyGeneratorId);
						keyGeneratorId++;
					}else{
						rxCodeKey = prescriptionSearchRequest.getDrugRxCode();
					}
					for(Map<String,Map<String,String>> resultantDrugDataMap : resultantMapList){
						//Map<String,String> resultantDrugData = resultantDrugDataMap.get(prescriptionSearchRequest.getDrugRxCode());
						if(null != rxCodeKey){
							Map<String,String> resultantDrugData = resultantDrugDataMap.get(rxCodeKey);
							if(null != resultantDrugData && !(resultantDrugData.isEmpty())){
								String drugTier = resultantDrugData.get(PlanMgmtConstants.DRUG_TIER);
								String isSupportPlan = null;
								if(null != prescriptionSearchRequest.getDrugRxCode() && !(prescriptionSearchRequest.getDrugRxCode().isEmpty())){
									isSupportPlan = resultantDrugData.get(PlanMgmtConstants.IS_SUPPORT_PLAN);
								}else{
									isSupportPlan = PlanMgmtConstants.U;
								}
								
								if(null != drugTier && null != isSupportPlan && isSupportPlan.equalsIgnoreCase(PlanMgmtConstants.Y)){
									prescriptionSearchDTO.setDrugTier(drugTier);
									prescriptionSearchDTO.setIsSupportPlan(isSupportPlan);
									String copayAmount = resultantDrugData.get(PlanMgmtConstants.COPAYMENT_AMOUNT);
									prescriptionSearchDTO.setCopayAmount(null != copayAmount ? copayAmount : PlanMgmtConstants.EMPTY_STRING);
									
									if(null != prescriptionSearchRequest.getFairCost() && !(prescriptionSearchRequest.getFairCost().isEmpty())){
										String coinsuranceAmount = resultantDrugData.get(PlanMgmtConstants.COINSURANCE_AMOUNT);
										prescriptionSearchDTO.setCoinsuranceAmount(null != coinsuranceAmount ? coinsuranceAmount : PlanMgmtConstants.EMPTY_STRING);
									}else{
										String coinsurancePercentage = resultantDrugData.get(PlanMgmtConstants.COINSURANCE_PERCENTAGE_KEY);
										prescriptionSearchDTO.setCoinsurancePercentage(null != coinsurancePercentage ? coinsurancePercentage : PlanMgmtConstants.EMPTY_STRING);
										prescriptionSearchDTO.setFairCost(PlanMgmtConstants.EMPTY_STRING);
									}
								}else{
									prescriptionSearchDTO.setIsSupportPlan(isSupportPlan);
									if(null == prescriptionSearchRequest.getFairCost() && prescriptionSearchDTO.getIsSupportPlan().equals(PlanMgmtConstants.N)){
										prescriptionSearchDTO.setFairCost(PlanMgmtConstants.EMPTY_STRING);
									}
								}
							}else{
								prescriptionSearchDTO.setIsSupportPlan(PlanMgmtConstants.U);
							}
						}else{
							prescriptionSearchDTO.setIsSupportPlan(PlanMgmtConstants.U);
							if(null == prescriptionSearchRequest.getFairCost() && prescriptionSearchDTO.getIsSupportPlan().equals(PlanMgmtConstants.U)){
								prescriptionSearchDTO.setFairCost(PlanMgmtConstants.EMPTY_STRING);
							}
						}
					}
				}else{
					if(null != prescriptionSearchRequest.getDrugRxCode() && !(prescriptionSearchRequest.getDrugRxCode().isEmpty())){
						prescriptionSearchDTO.setIsSupportPlan(PlanMgmtConstants.N);
					}else{
						prescriptionSearchDTO.setIsSupportPlan(PlanMgmtConstants.U);
					}
				}
				responseDTOList.add(prescriptionSearchDTO);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in generatePrescriptionSearchResponse: ", ex);
		}
		return responseDTOList;
	}
	
	
	/**
	 * @param drugInfoDBList
	 * @param drugInfoFromAPI
	 * @return single list Map<String, String> with drug data
	 */
	private List<Map<String,Map<String,String>>> getMergedMap(List<Map<String,String>> drugInfoDBList, List<Map<String,String>> drugCostShareInfoList){
		List<Map<String,Map<String,String>>> resultantMapList = new ArrayList<Map<String,Map<String,String>>>();
		Map<String,Map<String,String>> resultantMap = new HashMap<String,Map<String,String>>();
		Map<String,String> resultant = null;
		Integer keyGeneratorId = PlanMgmtConstants.ONE;
		try{
			for(Map<String,String> drugInfoMap : drugInfoDBList){
				resultant = new HashMap<String,String>();
				resultant.put(PlanMgmtConstants.IS_SUPPORT_PLAN, drugInfoMap.get(PlanMgmtConstants.IS_SUPPORT_PLAN));
				resultant.put(PlanMgmtConstants.DRUG_TIER, drugInfoMap.get(PlanMgmtConstants.DRUG_TIER));
				String rxCodeKey = drugInfoMap.get(PlanMgmtConstants.DRUG_RX_CODE) + PlanMgmtConstants.UNDERSCORE_STRING + String.valueOf(keyGeneratorId);
				resultantMap.put(rxCodeKey, resultant);
				keyGeneratorId++;
				resultantMapList.add(resultantMap);
			}

			for(Map<String,Map<String,String>> resultantMapInfo : resultantMapList){
				for(Map<String,String> drugCostShareInfoMap : drugCostShareInfoList){
					Map<String,String> tempDurgMap = resultantMapInfo.get(drugCostShareInfoMap.get(PlanMgmtConstants.RX_CODE_KEY));
					if(null != tempDurgMap){
						tempDurgMap.put(PlanMgmtConstants.COPAYMENT_AMOUNT, drugCostShareInfoMap.get(PlanMgmtConstants.COPAYMENT_AMOUNT));
						tempDurgMap.put(PlanMgmtConstants.COINSURANCE_AMOUNT, drugCostShareInfoMap.get(PlanMgmtConstants.COINSURANCE_AMOUNT));
						tempDurgMap.put(PlanMgmtConstants.COINSURANCE_PERCENTAGE_KEY, drugCostShareInfoMap.get(PlanMgmtConstants.COINSURANCE_PERCENTAGE_KEY));
					}
				}
			}
			
		}catch(Exception ex){
			LOGGER.error("Exception occured in getMergedMap: ", ex);
		}
		return resultantMapList;
	}
	
	private List<PrescriptionSearchDTO> getPrescriptionSearchResponseforNonFormularyId(List<PrescriptionSearchRequest> prescriptionSearchRequstList){
		List<PrescriptionSearchDTO> prescriptionSearchDTOs = new ArrayList<PrescriptionSearchDTO>();
		try{
			for(PrescriptionSearchRequest prescriptionSearchRequest : prescriptionSearchRequstList){
				PrescriptionSearchDTO prescriptionSearchDTO = new PrescriptionSearchDTO();
				prescriptionSearchDTO.setRxCuiCode(prescriptionSearchRequest.getDrugRxCode());
				prescriptionSearchDTO.setDrugName(prescriptionSearchRequest.getDrugName());
				prescriptionSearchDTO.setDrugNameForTooltip(prescriptionSearchRequest.getDrugNameForTooltip());
				if(null != prescriptionSearchRequest.getFairCost()){
					prescriptionSearchDTO.setFairCost(prescriptionSearchRequest.getFairCost());
				}else{
					prescriptionSearchDTO.setFairCost(PlanMgmtConstants.EMPTY_STRING);
				}
				prescriptionSearchDTO.setIsSupportPlan(PlanMgmtConstants.U);
				/* As the drug is not supporting hence not sending drugTier, copayAmout and coinsuranceAmount
				prescriptionSearchDTO.setDrugTier(PlanMgmtConstants.EMPTY_STRING);
				if(null == prescriptionSearchRequest.getFairPrice()){
					prescriptionSearchDTO.setCoinsurancePercentage(PlanMgmtConstants.EMPTY_STRING);
				}else{
					prescriptionSearchDTO.setCoinsuranceAmount(PlanMgmtConstants.EMPTY_STRING);
				}
				prescriptionSearchDTO.setCopayAmount(PlanMgmtConstants.EMPTY_STRING);*/
				prescriptionSearchDTOs.add(prescriptionSearchDTO);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getPrescriptionSearchResponseforNonFormularyId: ", ex);
		}
		return prescriptionSearchDTOs;
	}
	/**
	 * Returns list of durgs information after getting data from GoodRx API
	 * @deprecated
	 * @param drugsNames
	 * @return
	 */
	/*@Deprecated
	private List<Map<String, Map<String, String>>> getDurgInfoFromAPI(List<String> drugsNames) {
		List<Map<String, Map<String, String>>> allDrugInfo = new ArrayList<Map<String, Map<String, String>>>();
		Map<String, String> drugMap = new HashMap<String, String>();
		Map<String, Map<String, String>> drugInfoWithRxAPI = new HashMap<String, Map<String, String>>();
		try{
			for (String drugName : drugsNames) {
				if (drugName != null) {
					Map<String, String> urlVariables = new HashMap<String, String>();
					urlVariables.put("name_val", drugName);
					urlVariables.put("api_key_val", "d6de803087");
					urlVariables.put("sig_val", "78ek4jSfSkQ876vzW2mZBRDJxvOjtN537DJAsm8YdwQ=");
					String url = "http://api.goodrx.com/fair-price?name={name_val}&api_key={api_key_val}&sig={sig_val}";
	
					GoodRxAPIResponse goodRxAPIResponse = restTemplate.getForObject(url, GoodRxAPIResponse.class, urlVariables);
					GoodRxAPIResponseData dataMap = goodRxAPIResponse.getData();
	
					drugMap.put("mobile_url", dataMap.getMobile_url());
					drugMap.put("form", dataMap.getForm());
					drugMap.put("url", dataMap.getUrl());
					drugMap.put("dosage", dataMap.getDosage());
					drugMap.put("price", String.valueOf(dataMap.getPrice()));
					drugMap.put("quantity", String.valueOf(dataMap.getQuantity()));
					drugMap.put("display", dataMap.getDisplay());
					drugMap.put("manufacturer", dataMap.getManufacturer());
					drugMap.put("brand", String.valueOf(dataMap.getBrand()));
					drugMap.put("generic", String.valueOf(dataMap.getGeneric()));
	
					drugInfoWithRxAPI.put(drugName.toUpperCase(), drugMap);
	
					allDrugInfo.add(drugInfoWithRxAPI);
					LOGGER.debug("checkDataOfRestCall: " + dataMap.getDisplay() + " ::::::::: " + dataMap.getDosage() + " ::::::::: " + dataMap.getPrice());
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getDurgInfoFromAPI: " , ex);
		}
	
		return allDrugInfo;
	}*/
	
	/**
	 * Returns Amount with respect to durg
	 * @deprecated
	 * @param drugMapInfo
	 * @param drugNameList
	 * @return
	 */
	/*@Deprecated
	private List<Map<String, String>> getAmountFromAPI(List<Map<String, Map<String, String>>> drugMapInfo, List<String> drugNameList) {
		List<Map<String, String>> drugAmountList = new ArrayList<Map<String, String>>();
		Map<String, String> drugAmount = new HashMap<String, String>();
		Map<String, String> drugInfoMap = new HashMap<String, String>();

		for (Map<String, Map<String, String>> drugInfoMapWithDrug : drugMapInfo) {

			for (String drugName : drugNameList) {
				drugInfoMap = drugInfoMapWithDrug.get(drugName.toUpperCase());

				 Replace lipitor by drugName.toUpperCase() 
					if (drugInfoMap != null && drugInfoMap.get("display") != null && String.valueOf(drugInfoMap.get("display")).toUpperCase().contains(drugName.toUpperCase()) && !(drugAmount.containsKey(drugName.toUpperCase()))) {
							drugAmount.put(drugName.toUpperCase(), drugInfoMap.get("price"));
							drugAmountList.add(drugAmount);
							LOGGER.debug("Drug Price from getAmountFromAPI: " + drugInfoMap.get("price"));
					}
			}
		}
		
		return drugAmountList;
	}*/
	
	
	/**
	 * @param hiosPlanIdList
	 * @param applicableYear
	 * @param drugCodeList
	 * @return PrescriptionSearchByHIOSIdResponseDTO 
	 */
	 @Override
	 @Transactional(readOnly = true)
	 public List<PrescriptionDTO> prescriptionSearchByHiosPlanId(List<String> hiosPlanIdList, Integer applicableYear, List<String> rxCodeList){
		 List<PrescriptionDTO> prescriptionDTOList = new ArrayList<PrescriptionDTO>();
		 
		 try
		 {
			 String hiosPlanId = null;
			 Integer formularyId = PlanMgmtConstants.ZERO;
			 Integer issuerId = null; 
			 PrescriptionDTO prescriptionDTO = null; 
			 Map<String, String> supportedHiosPlanMap = null;
			 
			 List<Object[]> drugCostShareObjList  = null;
			 List<Map<String, String>> supportedPlanList = null; 
			 List<Object[]> drugObjectList = null;
			 Integer drugTierLevel = null;
			 Map<Integer, String> costShareDisplayTextMap = null;
			 List<Integer> costShareIdList = new ArrayList<Integer>();
			 List<Integer> issuerIdList =  new ArrayList<Integer>();
			 List<Integer> formularyIdList =  new ArrayList<Integer>();
			 long q1 = TimeShifterUtil.currentTimeMillis();
			 long startTime = TimeShifterUtil.currentTimeMillis();
			 
			 // --------- pull data from DB in single shot starts here ----------
			 // fetch formulary id and plan benefits by hiosPlanIdList and applicableYear from plan table
			 //HIX-106111
			 List<Object[]> planObjectList = iPlanRepository.getByHiosPlanIds(hiosPlanIdList, applicableYear, PlanMgmtConstants.HEALTH);
			 long q2 = TimeShifterUtil.currentTimeMillis();
			 if(LOGGER.isInfoEnabled()) {
				 LOGGER.info("time taken for executing iPlanRepository.getByHiosPlanIds() ==> " + (q2-q1));
			 }
			 
			 // if planObjectList is not null and empty
			 if(planObjectList != null && planObjectList.size() > PlanMgmtConstants.ZERO)
			 {
				 // prepare issuer id list and formulary id list
				 for(Object[] planObject : planObjectList)
				 {
					 if(!issuerIdList.contains((Integer) planObject[PlanMgmtConstants.TWO]))
					 {
						 issuerIdList.add((Integer) planObject[PlanMgmtConstants.TWO]);
					 }
					 if(!formularyIdList.contains(Integer.parseInt((String) planObject[PlanMgmtConstants.ONE])))
					 {
						 formularyIdList.add(Integer.parseInt((String) planObject[PlanMgmtConstants.ONE]));
					 } 
				 }	 
			 			
				 // fetch drug tier by rxCodeList, issuerIdList and formularyIdList
				 q1 = TimeShifterUtil.currentTimeMillis();
				 drugObjectList = iFormularyRepository.searchByRxCode(rxCodeList, issuerIdList, formularyIdList);
				 if(LOGGER.isInfoEnabled()) {
					 q2 = TimeShifterUtil.currentTimeMillis();
					 LOGGER.info("time taken for executing iFormularyRepository.searchByRxCode2() ==> " + (q2-q1));
				 }
				 
				 // if drugObjectList is not null and empty
				 if(drugObjectList != null && drugObjectList.size() > PlanMgmtConstants.ZERO)
				 {
					 for(Object[] drugObj : drugObjectList)
					 {
						 //drugTierLevelList.add((Integer) drugObj[PlanMgmtConstants.ONE]);
						 //drugFormularyList.add((Integer) drugObj[PlanMgmtConstants.THREE]);
						 costShareIdList.add((Integer) drugObj[PlanMgmtConstants.FOUR]);
					 }
					 q1 = TimeShifterUtil.currentTimeMillis();
					 //drugCostShareObjList = iPMCostShareRepository.getCostShareByDrugTier(drugFormularyList, drugTierLevelList);
					 drugCostShareObjList = iPMCostShareRepository.getCostShareByIdList(costShareIdList);
					 if(LOGGER.isInfoEnabled()) {
						 q2 = TimeShifterUtil.currentTimeMillis();
						 LOGGER.info("time taken for executing iFormularyRepository.getCostShareByDrugTier() ==> " + (q2-q1));
					 }
					 
					 if(drugCostShareObjList != null && !drugCostShareObjList.isEmpty()) {
						 costShareDisplayTextMap = new HashMap<Integer, String>();
						 createCostShareDisplayTextMap(drugCostShareObjList, costShareDisplayTextMap);
					 }
				 }	 
			 }
			 
			 // itarate requested rxCodeList
			 for(String rxCode : rxCodeList)
				 {
					 prescriptionDTO = new PrescriptionDTO();
					 supportedPlanList = new ArrayList<Map<String, String>>();
					 prescriptionDTO.setRxCode(rxCode);
					 
					 // if planObjectList is not null and empty
					 if(planObjectList != null && planObjectList.size() > PlanMgmtConstants.ZERO)
					 {
						 for(Object[] planObject : planObjectList)
						 {
							 hiosPlanId = String.valueOf(planObject[PlanMgmtConstants.ZERO]);
							 formularyId = Integer.parseInt((String) planObject[PlanMgmtConstants.ONE]);
							 issuerId =  (Integer) planObject[PlanMgmtConstants.TWO];
							 
							 // if drugObjectList is not null and empty
							 if(drugObjectList != null && drugObjectList.size() > PlanMgmtConstants.ZERO)
							 {
								 for(Object[] drugObj : drugObjectList)
								 {
									 // if requested rxCode, issuer id, formulary id and benefit name are matched with pulled data
									 if(rxCode.equals(drugObj[PlanMgmtConstants.ZERO]) 
							            && issuerId.equals(drugObj[PlanMgmtConstants.TWO]) 
							            && formularyId.equals(drugObj[PlanMgmtConstants.THREE])) {
										String displayVal = null;
										drugTierLevel = (Integer) drugObj[PlanMgmtConstants.ONE];
										if(costShareDisplayTextMap != null){
										 	displayVal = costShareDisplayTextMap.get(drugObj[PlanMgmtConstants.FOUR]);
										}
										if(null == displayVal) {
											displayVal = "UNKNOWN";
										}
										supportedHiosPlanMap = new  HashMap<String, String>();
									 	supportedHiosPlanMap.put("hiosPlanId", hiosPlanId);
										supportedHiosPlanMap.put("drugTier", String.valueOf(drugTierLevel));
										supportedHiosPlanMap.put("displayVal", displayVal);
										if(drugObj[PlanMgmtConstants.FIVE] != null) {
											supportedHiosPlanMap.put("authRequired", (String) drugObj[PlanMgmtConstants.FIVE]);
										}
										if(drugObj[PlanMgmtConstants.SIX] != null) {
											supportedHiosPlanMap.put("stepTherapyRequired", (String) drugObj[PlanMgmtConstants.SIX]);
										}
										supportedPlanList.add(supportedHiosPlanMap);
									}
								 }
								 if(LOGGER.isInfoEnabled()) {
									 long endTime = TimeShifterUtil.currentTimeMillis();
									 LOGGER.info("time taken for executing prescriptionSearchByHiosPlanId ==> " + (endTime-startTime));
								 }
							 }
						 }
					 }
					 
					 prescriptionDTO.setSupportedPlanList(supportedPlanList);
					 prescriptionDTOList.add(prescriptionDTO);
				 }	 

		 }
		 catch(Exception ex)
		 {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute PrescriptionRestController.searchByHiosPlanId().", ex);
			LOGGER.error("Failed to execute PlanRestController.prescriptionSearchByHiosPlanId(). Exception:", ex);	
		 }
		 
		 return prescriptionDTOList;
	 }


	private void createCostShareDisplayTextMap(List<Object[]> drugCostShareObjList,
			Map<Integer, String> costShareDisplayTextMap) {
		if(null != drugCostShareObjList && null != costShareDisplayTextMap) {
			for (Object[] costShare : drugCostShareObjList) {
				String copayAmount, coinsAmount;
				String copayAttr = null, coinsAttr = null;
				copayAmount = stripDecimalZero(costShare[2].toString());
				if(null != costShare[5]) {
					copayAttr = (String) costShare[5];
				}
				coinsAmount = stripDecimalZero(costShare[3].toString());
				if(null != costShare[6]) {
					coinsAttr = (String) costShare[6];
				}
				costShareDisplayTextMap.put((Integer)costShare[0], getCopayCoinsuranceDisplayString(copayAmount, copayAttr, coinsAmount, coinsAttr));
			}
		}
	}
	
	private String getCopayCoinsuranceDisplayString(String copayAmount, String copayAttr, String coinsAmount, String coinsAttr) {
		StringBuilder displayText = new StringBuilder();
		int copayIndex = getCopayRuleIndex(copayAttr);
		int coinsIndex = getCoinsRuleIndex(coinsAttr);
		String ruleString = COPAY_COINS_DISPLAY_RULES[copayIndex][coinsIndex];
		Formatter formatter = new Formatter(displayText, Locale.US);
		formatter.format(ruleString, coinsAmount, copayAmount);
		formatter.close();
		return displayText.toString();
	}
	
	private int getCopayRuleIndex(String copayAttr) {
		int copayIndex = COPAY_DOLLAR_AMOUNT_INDEX; 
		if(StringUtils.isNotBlank(copayAttr)) {
			switch(copayAttr) {
				case NO_CHARGE:
					copayIndex = COPAY_NO_CHARGE_INDEX;
					break;
				case NO_CHARGE_AFTER_DEDUCTIBLE:
					copayIndex = COPAY_NO_CHARGE_AFTER_DEDUCTIBLE_INDEX;
					break;
				case COPAY_AFTER_DEDUCTIBLE:
					copayIndex = COPAY_AFTER_DEDUCTIBLE_INDEX;
					break;
				case COPAY_BEFORE_DEDUCTIBLE:
					copayIndex = COPAY_BEFORE_DEDUCTIBLE_INDEX;
					break;
				case COPAY_WITH_DEDUCTIBLE:
					copayIndex = COPAY_WITH_DEDUCTIBLE_INDEX;
					break;
				case NOT_APPLICABLE:
					copayIndex = COPAY_NOT_APPLICABLE_INDEX;
					break;
			}
		}
		return copayIndex;
	}

	private int getCoinsRuleIndex(String coinsAttr) {
		int coinsIndex = COINS_PERCENTAGE_INDEX; 
		if(StringUtils.isNotBlank(coinsAttr)) {
			switch(coinsAttr) {
				case NO_CHARGE:
					coinsIndex = COINS_NO_CHARGE_INDEX;
					break;
				case NO_CHARGE_AFTER_DEDUCTIBLE:
					coinsIndex = COINS_NO_CHARGE_AFTER_DEDUCTIBLE_INDEX;
					break;
				case COINS_AFTER_DEDUCTIBLE:
					coinsIndex = COINS_AFTER_DEDUCTIBLE_INDEX;
					break;
				case NOT_APPLICABLE:
					coinsIndex = COINS_NOT_APPLICABLE_INDEX;
					break;
			}
		}
		return coinsIndex;
	}

	private String stripDecimalZero(String value) {
		String returnVal = null;
		if(StringUtils.isNotBlank(value)) {
			returnVal = value.trim();
			if(returnVal.endsWith(DOT_ZERO_ZERO)) {
				returnVal = returnVal.replace(DOT_ZERO_ZERO, StringUtils.EMPTY);
			} else if(returnVal.endsWith(DOT_ZERO)) {
				returnVal = returnVal.replace(DOT_ZERO, StringUtils.EMPTY);
			}
		} else {
			returnVal = value;
		}
		return returnVal;
	}
}
