//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.12 at 02:20:37 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.ihc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Quote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Quote">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenericBenefits" type="{http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models}ArrayOfGenericBenefit" minOccurs="0"/>
 *         &lt;element name="IsTopSeller" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Premium" type="{http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models}Premium" minOccurs="0"/>
 *         &lt;element name="QuoteID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="StandardBenefits" type="{http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models}StandardBenefits" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Quote", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", propOrder = {
    "genericBenefits",
    "isTopSeller",
    "premium",
    "quoteID",
    "standardBenefits"
})
public class Quote {

    @XmlElementRef(name = "GenericBenefits", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGenericBenefit> genericBenefits;
    @XmlElement(name = "IsTopSeller")
    protected Boolean isTopSeller;
    @XmlElementRef(name = "Premium", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<Premium> premium;
    @XmlElement(name = "QuoteID")
    protected Integer quoteID;
    @XmlElementRef(name = "StandardBenefits", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<StandardBenefits> standardBenefits;

    /**
     * Gets the value of the genericBenefits property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGenericBenefit }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGenericBenefit> getGenericBenefits() {
        return genericBenefits;
    }

    /**
     * Sets the value of the genericBenefits property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGenericBenefit }{@code >}
     *     
     */
    public void setGenericBenefits(JAXBElement<ArrayOfGenericBenefit> value) {
        this.genericBenefits = value;
    }

    /**
     * Gets the value of the isTopSeller property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTopSeller() {
        return isTopSeller;
    }

    /**
     * Sets the value of the isTopSeller property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTopSeller(Boolean value) {
        this.isTopSeller = value;
    }

    /**
     * Gets the value of the premium property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Premium }{@code >}
     *     
     */
    public JAXBElement<Premium> getPremium() {
        return premium;
    }

    /**
     * Sets the value of the premium property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Premium }{@code >}
     *     
     */
    public void setPremium(JAXBElement<Premium> value) {
        this.premium = value;
    }

    /**
     * Gets the value of the quoteID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getQuoteID() {
        return quoteID;
    }

    /**
     * Sets the value of the quoteID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setQuoteID(Integer value) {
        this.quoteID = value;
    }

    /**
     * Gets the value of the standardBenefits property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StandardBenefits }{@code >}
     *     
     */
    public JAXBElement<StandardBenefits> getStandardBenefits() {
        return standardBenefits;
    }

    /**
     * Sets the value of the standardBenefits property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StandardBenefits }{@code >}
     *     
     */
    public void setStandardBenefits(JAXBElement<StandardBenefits> value) {
        this.standardBenefits = value;
    }

}
