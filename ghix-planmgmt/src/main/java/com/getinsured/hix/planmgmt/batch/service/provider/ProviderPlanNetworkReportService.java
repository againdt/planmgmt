/**
 * 
 */
package com.getinsured.hix.planmgmt.batch.service.provider;

/**
 * @author shengole_s
 *
 */
public interface ProviderPlanNetworkReportService {
	String generatePlanNetworkReport();
}
