//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.12 at 02:20:37 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.ihc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Product complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Product">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CarrierLogo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CarrierName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FindDoctorUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsThirdParty" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Plans" type="{http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models}ArrayOfPlan" minOccurs="0"/>
 *         &lt;element name="ProductCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductFees" type="{http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models}ProductFees" minOccurs="0"/>
 *         &lt;element name="ProductID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Product", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", propOrder = {
    "carrierLogo",
    "carrierName",
    "findDoctorUrl",
    "isThirdParty",
    "name",
    "plans",
    "productCode",
    "productFees",
    "productID"
})
public class Product {

    @XmlElementRef(name = "CarrierLogo", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> carrierLogo;
    @XmlElementRef(name = "CarrierName", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> carrierName;
    @XmlElementRef(name = "FindDoctorUrl", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> findDoctorUrl;
    @XmlElement(name = "IsThirdParty")
    protected Boolean isThirdParty;
    @XmlElementRef(name = "Name", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> name;
    @XmlElementRef(name = "Plans", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfPlan> plans;
    @XmlElementRef(name = "ProductCode", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<String> productCode;
    @XmlElementRef(name = "ProductFees", namespace = "http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models", type = JAXBElement.class, required = false)
    protected JAXBElement<ProductFees> productFees;
    @XmlElement(name = "ProductID")
    protected Integer productID;

    /**
     * Gets the value of the carrierLogo property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCarrierLogo() {
        return carrierLogo;
    }

    /**
     * Sets the value of the carrierLogo property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCarrierLogo(JAXBElement<String> value) {
        this.carrierLogo = value;
    }

    /**
     * Gets the value of the carrierName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCarrierName() {
        return carrierName;
    }

    /**
     * Sets the value of the carrierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCarrierName(JAXBElement<String> value) {
        this.carrierName = value;
    }

    /**
     * Gets the value of the findDoctorUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFindDoctorUrl() {
        return findDoctorUrl;
    }

    /**
     * Sets the value of the findDoctorUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFindDoctorUrl(JAXBElement<String> value) {
        this.findDoctorUrl = value;
    }

    /**
     * Gets the value of the isThirdParty property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsThirdParty() {
        return isThirdParty;
    }

    /**
     * Sets the value of the isThirdParty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsThirdParty(Boolean value) {
        this.isThirdParty = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setName(JAXBElement<String> value) {
        this.name = value;
    }

    /**
     * Gets the value of the plans property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPlan }{@code >}
     *     
     */
    public JAXBElement<ArrayOfPlan> getPlans() {
        return plans;
    }

    /**
     * Sets the value of the plans property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfPlan }{@code >}
     *     
     */
    public void setPlans(JAXBElement<ArrayOfPlan> value) {
        this.plans = value;
    }

    /**
     * Gets the value of the productCode property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getProductCode() {
        return productCode;
    }

    /**
     * Sets the value of the productCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setProductCode(JAXBElement<String> value) {
        this.productCode = value;
    }

    /**
     * Gets the value of the productFees property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ProductFees }{@code >}
     *     
     */
    public JAXBElement<ProductFees> getProductFees() {
        return productFees;
    }

    /**
     * Sets the value of the productFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ProductFees }{@code >}
     *     
     */
    public void setProductFees(JAXBElement<ProductFees> value) {
        this.productFees = value;
    }

    /**
     * Gets the value of the productID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProductID() {
        return productID;
    }

    /**
     * Sets the value of the productID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProductID(Integer value) {
        this.productID = value;
    }

}
