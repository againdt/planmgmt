package com.getinsured.hix.planmgmt.model;

import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;

@Component
public class GhixConfiguration {

	public String EXCHANGE_TYPE;

//	@Value("#{combinedConfig.getString('global.ExchangeType')}")
	public void setExchangeType(String exchangeType)
	{
		EXCHANGE_TYPE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
	}
}
