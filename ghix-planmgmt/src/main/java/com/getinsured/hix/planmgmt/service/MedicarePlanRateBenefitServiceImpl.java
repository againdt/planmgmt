/**
 * Medicare plan rate Service implementation.
 * 
 * @author santanu
 * @version 1.0
 * @since Feb 2, 2015
 * 
 */
package com.getinsured.hix.planmgmt.service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.getinsured.hix.dto.planmgmt.MedicarePlan;
import com.getinsured.hix.dto.planmgmt.Member;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.phix.ws.quotit.ArrayOfGetIfpQuoteRequestMember;
import com.getinsured.hix.planmgmt.phix.ws.quotit.BenchmarkPlan;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteRequestIfpRateFactor;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteRequestMember;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteRequestPlanVisibility;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteRequestPreference;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteResponseQuote;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteResponseQuoteCarrierRate;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteResponseQuoteCarrierRatePlanRate;
import com.getinsured.hix.planmgmt.phix.ws.quotit.IfpPlanEligibility;
import com.getinsured.hix.planmgmt.phix.ws.quotit.InsuranceType;
import com.getinsured.hix.planmgmt.phix.ws.quotit.ObjectFactory;
import com.getinsured.hix.planmgmt.phix.ws.quotit.RelationshipType;
import com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpQuote;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.SecurityUtil;


@Service("MedicarePlanRateBenefitService")
@Repository
@DependsOn("dynamicPropertiesUtil")
public class MedicarePlanRateBenefitServiceImpl implements MedicarePlanRateBenefitService{
	private static final Logger LOGGER = Logger.getLogger(MedicarePlanRateBenefitServiceImpl.class);
	
	@Autowired
	private WebServiceTemplate webServiceTemplate;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@PersistenceUnit private EntityManagerFactory emf;
	
	@Value("#{configProp['appUrl']}")
	private StringBuilder appUrl;
	
	public static final String DASH = "-";	
	private static final String DOCUMENT_URL = "download/document?documentId=";	
	private static String REMOTE_ACCESS_KEY;
	private static final String DEFAULT_REMOTE_ACCESS_KEY = "CE7C9822-EA67-4C92-B6BB-4B6D01023652";
	private static String WEBSITE_ACCESS_KEY;
	private static final String DEFAULT_WEBSITE_ACCESS_KEY = "CE7C9822-EA67-4C92-B6BB-4B6D01023652";
	private static String BROKER_ID;
	private static final String DEFAULT_BROKER_ID = "23460451";
	private static String QUOTING_WSDL_URL;
	private static final String DEFAULT_QUOTING_WSDL_URL = "https://ws.quotit.net/ActWS/ACA/v2/ACA.svc";
	private static String QUOTING_ENDPOINT_URL;
	private static final String DEFAULT_QUOTING_ENDPOINT_URL = "http://www.quotit.com/Services/ActWS/ACA/2/IACA/GetIfpQuote";
							
	private static final String EFFECTIVEDATE = "effectiveDate";
	private static final String INSURANCETYPE = "insuranceType";
	private static final String ZIPCODE = "zipCode";
	private static final String COUNTYNAME = "countyName";
	//private static final String QUOTITPLANIDS = "quotitPlanIdStr";
	//private static final String BENEFITCATEGORIES = "benefitCategories";
	//private static final String MEDICAREPLANIDSTR = "medicarePlanIdStr";
	private static final String PLANMEDICAREIDSTR = "planMedicareIdStr";
	private static final String PLANYEAR = "planYear";
	private static final String TENANT = "tenant";
	
	
	@Value("#{configProp['serff.medicareBatch.remoteAccessKey']}")
	public void setRemoteAccessKey(String remoteAccessKey) {
		REMOTE_ACCESS_KEY = remoteAccessKey;
	}
	
	public String getRemoteAccessKey(){
		if(StringUtils.isBlank(REMOTE_ACCESS_KEY)){
			REMOTE_ACCESS_KEY = DEFAULT_REMOTE_ACCESS_KEY;
		}		
		return REMOTE_ACCESS_KEY.trim();
	}
	
	@Value("#{configProp['serff.medicareBatch.websiteAccessKey']}")
	public void setWebsiteAccessKey(String websiteAccessKey) {
		WEBSITE_ACCESS_KEY = websiteAccessKey;
	}
	
	public String getWebsiteAccessKey(){
		if(StringUtils.isBlank(WEBSITE_ACCESS_KEY)){
			WEBSITE_ACCESS_KEY = DEFAULT_WEBSITE_ACCESS_KEY;
		}		
		return WEBSITE_ACCESS_KEY.trim();
	}
	
	@Value("#{configProp['serff.medicareBatch.brokerId']}")
	public void setBrokerId(String brokerId) {
		BROKER_ID = brokerId;
	}
	
	public String getBrokerId(){
		if(StringUtils.isBlank(BROKER_ID)){
			BROKER_ID = DEFAULT_BROKER_ID;
		}		
		return BROKER_ID.trim();
	}
	
	@Value("#{configProp['serff.medicareBatch.endPoint']}")
	public void setQuotingWSDLUrl(String wdslUrl) {
		QUOTING_WSDL_URL = wdslUrl;
	}
	
	public String getQuointgWSDLUrl(){
		if(StringUtils.isBlank(QUOTING_WSDL_URL)){
			QUOTING_WSDL_URL = DEFAULT_QUOTING_WSDL_URL;
		}		
		return QUOTING_WSDL_URL.trim();
	}
	
	@Value("#{configProp['planmgmt.medicareQuoting.endPoint']}")
	public void setQuotingEndPointUrl(String quotingEndPointUrl) {
		QUOTING_ENDPOINT_URL = quotingEndPointUrl;
	}
	
	public String getQuotingEndPointUrl(){
		if(StringUtils.isBlank(QUOTING_ENDPOINT_URL)){
			QUOTING_ENDPOINT_URL = DEFAULT_QUOTING_ENDPOINT_URL;
		}		
		return QUOTING_ENDPOINT_URL.trim();
	}
	
	
	@Transactional(readOnly = true)
	public List<MedicarePlan> getMedicarePlanRateBenefits(List<Member> memberList, String effectiveDate, String insuranceType, String tenant, String zipCode, String countyName, String planYear){
		
	    XMLGregorianCalendar formattedEffectiveDate = null;
		try{
			String newEffectiveDate = null;
			if(effectiveDate != null && effectiveDate.length() == 10){
				newEffectiveDate = effectiveDate.substring(6,10)+"-"+effectiveDate.substring(0,2)+"-"+effectiveDate.substring(3,5);			 
			}
			formattedEffectiveDate = getXMLGregorianCalendar("yyyy-MM-dd", newEffectiveDate);			
		}catch(Exception e){
			
		}
		
		LOGGER.info("Preparing QUOTIT SOAP Request..."); 		
		
		ObjectFactory objectFactory = new ObjectFactory();
		com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.ObjectFactory ofaca = new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.ObjectFactory();
		
		GetIfpQuoteRequestPreference pref = objectFactory.createGetIfpQuoteRequestPreference();
		pref.setInsuranceTypes(objectFactory.createArrayOfInsuranceType());
		
		switch(insuranceType.toUpperCase()) {
			case "MC_ADV":
				pref.getInsuranceTypes().getInsuranceType().add(InsuranceType.MEDICARE_ADVANTAGE);
				break;
			case "MC_SUP":
				pref.getInsuranceTypes().getInsuranceType().add(InsuranceType.MEDIGAP);
				break;
			case "MC_RX":
				pref.getInsuranceTypes().getInsuranceType().add(InsuranceType.RX);
				break;
		}
	
		pref.setPlanVisibility(GetIfpQuoteRequestPlanVisibility.ON_ANY);
		pref.getQuoteFormat().add("PlanMembersDetail");
		pref.setPlanEligibility(IfpPlanEligibility.FULL_ELIGIBILITY_ONLY);
		pref.setBenchmarkPlan(BenchmarkPlan.DESIGNATE);
		pref.getQuoteFormat().add("PlanMembersDetail");
				
		
		GetIfpQuoteRequestIfpRateFactor factor = objectFactory.createGetIfpQuoteRequestIfpRateFactor();
		factor.setEffectiveDate(objectFactory.createGetIfpQuoteRequestIfpRateFactorEffectiveDate(formattedEffectiveDate));
		factor.setZipCode(zipCode);
		factor.setCountyName(objectFactory.createGetIfpQuoteRequestIfpRateFactorCountyName(countyName));
		factor.setMembers(setMemberData(memberList));
	
		
		GetIfpQuoteRequest req =  new GetIfpQuoteRequest();
		req.setRemoteAccessKey(getRemoteAccessKey());
		req.setWebsiteAccessKey(getWebsiteAccessKey());
		req.setBrokerId(objectFactory.createQuoteRequestBaseBrokerId(Integer.parseInt(getBrokerId())));
		req.setIfpRateFactors(objectFactory.createGetIfpQuoteRequestIfpRateFactors(factor));
		req.setPreferences(pref);
		
		JAXBElement<GetIfpQuoteRequest> getIfpQuoteReq = ofaca.createGetIfpQuoteQuoteRequest(req); 
		GetIfpQuote request = new GetIfpQuote();
		request.setQuoteRequest(getIfpQuoteReq);
		
		// prepare request data	end
		//LOGGER.info("Request prepartion done.");
		
		List<MedicarePlan> medicarePlanList = new ArrayList<MedicarePlan>();
		String quotitPlanIdStr = PlanMgmtConstants.EMPTY_STRING;
		String medicarePlanIdStr = PlanMgmtConstants.EMPTY_STRING;
		StringBuilder queryBuilder = new StringBuilder();
		//StringBuilder logoUrl = new StringBuilder();
		String brochureUrl = null;
		String newPlanYear = null;
		Map<String, BigDecimal> quotitPlans = new HashMap<String, BigDecimal>();
		
		com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpQuoteResponse response = null;	
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			// set WSDL url
			webServiceTemplate.setDefaultUri(getQuointgWSDLUrl());
			LOGGER.info("Calling Quotit WSDL... Submiting request...");
			// send request			
			response =  (com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpQuoteResponse) webServiceTemplate.marshalSendAndReceive(request,  new SoapActionCallback(getQuotingEndPointUrl()));
			
			if(null != response && null != response.getGetIfpQuoteResult()
					&& null != response.getGetIfpQuoteResult().getValue().getIfpQuote()) {
				LOGGER.info("Get response from QUOTIT web service. Parsing SOAP response...");
				GetIfpQuoteResponseQuote quote = response.getGetIfpQuoteResult().getValue().getIfpQuote();
				
				if (null != quote && null != quote.getCarriers()
						&& null != quote.getCarriers().getValue()
						&& !CollectionUtils.isEmpty(quote.getCarriers().getValue().getGetIfpQuoteResponseQuoteCarrierRate())) {

					for (Iterator<GetIfpQuoteResponseQuoteCarrierRate> iterator = 
							quote.getCarriers().getValue().getGetIfpQuoteResponseQuoteCarrierRate().iterator(); 
							iterator.hasNext();) {
						
						GetIfpQuoteResponseQuoteCarrierRate carrierRate = (GetIfpQuoteResponseQuoteCarrierRate) iterator.next();
						
						if (null == carrierRate || null == carrierRate.getPlanRates()
								|| null == carrierRate.getPlanRates().getValue()
								|| CollectionUtils.isEmpty(carrierRate.getPlanRates().getValue().getGetIfpQuoteResponseQuoteCarrierRatePlanRate())) {
							continue;
						}
						
						for (Iterator<GetIfpQuoteResponseQuoteCarrierRatePlanRate> iterator2 = 
								carrierRate.getPlanRates().getValue().getGetIfpQuoteResponseQuoteCarrierRatePlanRate().iterator(); 
								iterator2.hasNext();) {
							GetIfpQuoteResponseQuoteCarrierRatePlanRate rate = 
									(GetIfpQuoteResponseQuoteCarrierRatePlanRate) iterator2.next();
							
							if (null == rate) {
								continue;
							}
							quotitPlans.put(rate.getPlanId(), rate.getRate());
							quotitPlanIdStr += (quotitPlanIdStr.length() == 0) ? "'" + rate.getPlanId() + "'" : ", '" + rate.getPlanId() + "'";
						}
					}
				}
								
				queryBuilder.append(" SELECT pm.id as medicare_plan_id, p.name as plan_name, p.issuer_id, i.name as issuer_name, p.brochure, p.network_type, i.logo_url, pm.cms_is_snp, pm.is_medicaid, p.issuer_plan_number, p.id, p.insurance_type, p.applicable_year, p.hiosproduct_id");
				queryBuilder.append(" FROM plan p, issuers i, plan_medicare pm, pm_tenant_plan tp, tenant t ");
				queryBuilder.append(" WHERE ");
				queryBuilder.append(" i.id = p.issuer_id AND p.id = pm.plan_id ");
				queryBuilder.append(" AND p.status IN('CERTIFIED', 'RECERTIFIED') ");
				queryBuilder.append(" AND p.insurance_type = :");
				queryBuilder.append(INSURANCETYPE);
				if(null == planYear || planYear.equalsIgnoreCase("")) {
					newPlanYear =  effectiveDate.substring(6,10);					
				} else {
					newPlanYear = planYear;
				}
				queryBuilder.append(" AND p.applicable_year = CAST(:");
				queryBuilder.append(PLANYEAR);
				queryBuilder.append(" AS INTEGER) AND p.issuer_verification_status = 'VERIFIED' ");
				queryBuilder.append(" AND p.is_deleted = 'N' ");
				queryBuilder.append(" AND pm.data_source = 'QUOTIT'");

				if (StringUtils.isNotBlank(quotitPlanIdStr)) {
					queryBuilder.append(" AND p.issuer_plan_number IN (" + quotitPlanIdStr + ")");
				}
				else {
					LOGGER.warn("Get empty list of Plan ID, so could not get Medicare Plan Rate Benefits.");
					return medicarePlanList;
				}
				queryBuilder.append(" AND p.id = tp.plan_id");
				queryBuilder.append(" AND tp.tenant_id = t.id");
				queryBuilder.append(" AND t.code = UPPER(:");
				queryBuilder.append(TENANT);
				queryBuilder.append(") ");

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Medicare SQL========"+ queryBuilder.toString());
				}
				
				
				Query query = entityManager.createNativeQuery(queryBuilder.toString());
				query.setParameter(INSURANCETYPE, insuranceType.toUpperCase());
//				query.setParameter(QUOTITPLANIDS, quotitPlanIdStr.toString());
				query.setParameter(PLANYEAR, newPlanYear);
				query.setParameter(TENANT, tenant);

				List rsList = query.getResultList();
				Iterator rsIterator = rsList.iterator();
				Iterator rsIterator1 = rsList.iterator();
				Object[] rsObjectArray = null;
				
				while(rsIterator.hasNext()){
					rsObjectArray = (Object[]) rsIterator.next();
					String logoURL = null;

					MedicarePlan medicarePlan = new MedicarePlan();
					medicarePlan.setId(Integer.parseInt(rsObjectArray[PlanMgmtConstants.TEN].toString()));
					medicarePlan.setName((String.valueOf(rsObjectArray[PlanMgmtConstants.ONE])));
					medicarePlan.setIssuerId(Integer.parseInt(rsObjectArray[PlanMgmtConstants.TWO].toString()));
					medicarePlan.setIssuerName((String.valueOf(rsObjectArray[PlanMgmtConstants.THREE])));														
					medicarePlan.setNetworkType((String.valueOf(rsObjectArray[PlanMgmtConstants.FIVE])));
					if(null != rsObjectArray[PlanMgmtConstants.SIX]) {
						logoURL = rsObjectArray[PlanMgmtConstants.SIX].toString().trim();
					}
					//medicarePlan.setIssuerLogo(rsObjectArray[PlanMgmtConstants.SIX] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.SIX]).trim()));
					medicarePlan.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, null, 
							ghixJasyptEncrytorUtil.encryptStringByJasypt(rsObjectArray[PlanMgmtConstants.TWO].toString()), appUrl.toString()));
					medicarePlan.setBenefitsLink("");
					medicarePlan.setIsSNP((String.valueOf(rsObjectArray[PlanMgmtConstants.SEVEN])));
					medicarePlan.setIsMedicaid((String.valueOf(rsObjectArray[PlanMgmtConstants.EIGHT])));					
					
					brochureUrl = rsObjectArray[PlanMgmtConstants.FOUR] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.FOUR]).trim());
					
					if (StringUtils.isNotBlank(brochureUrl)) {
						
						if (brochureUrl.matches(PlanMgmtConstants.ECM_REGEX)) {
							brochureUrl = appUrl.append(DOCUMENT_URL).append(ghixJasyptEncrytorUtil.encryptStringByJasypt(brochureUrl)).toString();				
						}
						else if (!brochureUrl.matches(PlanMgmtConstants.URL_REGEX)) {
							brochureUrl = PlanMgmtConstants.HTTP_PROTOCOL + brochureUrl;				
						}
					}
					
					medicarePlan.setBrochureUrl(brochureUrl);
					medicarePlan.setPremium(quotitPlans.get(rsObjectArray[PlanMgmtConstants.NINE]).floatValue());
					medicarePlan.setInsuranceType((String.valueOf(rsObjectArray[PlanMgmtConstants.ELEVEN])));
					medicarePlan.setPlanYear((String.valueOf(rsObjectArray[PlanMgmtConstants.TWELVE])));
					medicarePlan.setPlanHiosId((String.valueOf(rsObjectArray[PlanMgmtConstants.THIRTEEN])));
					
					//LOGGER.info("Medicare Plan =========== Id =" + medicarePlan.getId() + "Name= " +medicarePlan.getName());
					medicarePlanList.add(medicarePlan);
					
					medicarePlanIdStr += (medicarePlanIdStr.length() == 0) ? "'" + rsObjectArray[PlanMgmtConstants.ZERO].toString() + "'" : ", '" + rsObjectArray[PlanMgmtConstants.ZERO].toString() + "'";					
				}

				if (quotitPlanIdStr.length() > PlanMgmtConstants.ZERO) {
					Map<String, Map<String, Map<String, String>>> benefitData = new HashMap<String, Map<String, Map<String, String>>>();
					
					benefitData = getMedicarePlanBenefits(medicarePlanIdStr); // fetch Quotit plan benefits from database
										
					while(rsIterator1.hasNext()){
						rsObjectArray = (Object[]) rsIterator1.next();
						
						for (MedicarePlan medicarePlan : medicarePlanList) {
							if(medicarePlan.getId() == Integer.parseInt(rsObjectArray[PlanMgmtConstants.TEN].toString())){
								medicarePlan.setBenefits(benefitData.get(String.valueOf(rsObjectArray[PlanMgmtConstants.ZERO].toString())));
								break;
							}													
						}
					}
					
				}
								
			}
			else{
				LOGGER.info("Response not found from QUOTIT web service");
			}	
			
		}catch(WebServiceIOException wsIOE){
			LOGGER.error(wsIOE);
		}catch(SoapFaultClientException sfce){
			LOGGER.error(sfce.getMessage() + PlanMgmtConstants.TRIPLE_DASH_STRING + sfce.toString());
		}catch(Exception e) {
			LOGGER.error("Error occurred while fetching plans from QUOTIT: ",e);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return medicarePlanList;		
	}
		
	private XMLGregorianCalendar getXMLGregorianCalendar(String formatStr, String dateValue) throws Exception {
		final DateFormat format = new SimpleDateFormat(formatStr);
		GregorianCalendar gregory = new GregorianCalendar();
		gregory.setTime(format.parse(dateValue));
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);			
	}
	
	
	private ArrayOfGetIfpQuoteRequestMember setMemberData(List<Member> memberList){
		ObjectFactory objectFactory = new ObjectFactory();
		ArrayOfGetIfpQuoteRequestMember members = objectFactory.createArrayOfGetIfpQuoteRequestMember();
		
		
		XMLGregorianCalendar memberDOB = null;
		for (Member member : memberList) {			
			GetIfpQuoteRequestMember mem = objectFactory.createGetIfpQuoteRequestMember();
			mem.setMemberId(Integer.toString(member.getId()));
						
			switch(member.getRelation().toLowerCase()){
				case "self" :
					mem.getMemberType().add("Subscriber");
					mem.setRelationshipType(RelationshipType.SELF);					
					break;
				case "spouse" : 
					mem.getMemberType().add("Dependent");
					mem.setRelationshipType(RelationshipType.SPOUSE);					
					break;
				case "child" : 					
					mem.getMemberType().add("Dependent");	
					mem.setRelationshipType(RelationshipType.CHILD);
					break;
			}
			
			
			try{
				String newDob = null;
				if(member.getDob() != null && member.getDob().length() == 10){
					newDob = member.getDob().substring(6,10)+"-"+member.getDob().substring(0,2)+"-"+member.getDob().substring(3,5);			 
				}
				memberDOB = getXMLGregorianCalendar("yyyy-MM-dd", newDob);								
			}catch(Exception e){
			   LOGGER.error("Unable to parse member DOB as " + SecurityUtil.sanitizeForLogging( member.getDob()) );	
			}
			mem.setDateOfBirth(memberDOB);
			mem.getGender().add(member.getGender());
			mem.setIsSmoker((member.getTobacco().equalsIgnoreCase("Y")) ?  objectFactory.createGetIfpQuoteRequestMemberIsSmoker(true) :  objectFactory.createGetIfpQuoteRequestMemberIsSmoker(false));
			mem.setLivesInHousehold(true);				
			members.getGetIfpQuoteRequestMember().add(mem);			
		}
		
		return members;
	}
	
	@Transactional(readOnly = true)
	private Map<String, Map<String, Map<String, String>>> getMedicarePlanBenefits(String medicarePlanIdStr){

		Map<String, Map<String, Map<String, String>>> benefitData = new HashMap<String, Map<String, Map<String, String>>>();
		if (StringUtils.isBlank(medicarePlanIdStr)) {
			LOGGER.warn("Get empty list of Plan ID, so could not get Medicare Plan Benefits data.");
			return benefitData;
		}

		StringBuilder buildQuery = new StringBuilder();
		String benefitCategories = "'OVERALL_PLAN_RATING', 'DEDUCTIBLE', 'DRUG_DEDUCTIBLE', 'MAX_ANNUAL_COPAY', 'OFFICE_VISITS_PRIMARY', 'MONTHLY_PREMIUM', 'RX_COVERED'";
		buildQuery.append("SELECT pmb.plan_medicare_id, pmb.name, pmb.category, pmb.category_name, pmb.full_value || pmb.FULL_VALUE_EXT, pmb.tiny_value " );
		buildQuery.append("FROM " );
		buildQuery.append("plan_medicare pm, plan_mc_benefit pmb ");
		buildQuery.append("WHERE " );
		buildQuery.append("pm.id = pmb.plan_medicare_id ");
		buildQuery.append("AND category IN (" + benefitCategories + ")");
			buildQuery.append(" AND pm.id IN (" + medicarePlanIdStr +") ");

		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT buildquery :: " + buildQuery);
		}
		
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildQuery.toString());
//			query.setParameter(BENEFITCATEGORIES, benefitCategories);
//			query.setParameter(MEDICAREPLANIDSTR, medicarePlanIdStr);
			
			Iterator rsIterator = query.getResultList().iterator();
			
			while (rsIterator.hasNext()) {
				Object[] benefitFileds = (Object[]) rsIterator.next();
				Map<String, String> benefitAttrib = new HashMap<String, String>();
				
				String medicarePlanId = benefitFileds[PlanMgmtConstants.ZERO].toString();
				String benefitName = (benefitFileds[PlanMgmtConstants.TWO] == null ? "" : benefitFileds[PlanMgmtConstants.TWO].toString());
				
				benefitAttrib.put(PlanMgmtConstants.LONG_VALUE, (benefitFileds[PlanMgmtConstants.FOUR] == null) ? "" : benefitFileds[PlanMgmtConstants.FOUR].toString());
				benefitAttrib.put(PlanMgmtConstants.TINY_VALUE, (benefitFileds[PlanMgmtConstants.FIVE] == null) ? "" : benefitFileds[PlanMgmtConstants.FIVE].toString());
												
				if (benefitData.get(medicarePlanId) == null) {
					benefitData.put(medicarePlanId, new HashMap<String, Map<String, String>>());
				}
				
				benefitData.get(medicarePlanId).put(benefitName, benefitAttrib);	
								
			}
		}catch(Exception ex){
			LOGGER.error("Exception Occured in getStmPlanCosts_test", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return benefitData;
	}
	
	
	@Transactional(readOnly = true)
	public List<Map<String, String>> getCountyByZip(String zipCode){	
		//Map<String, Map<String, String>> zipCountyFullList = new HashMap<String,Map<String, String>>();
		List<Map<String, String>> zipCountyFullList = new ArrayList<Map<String, String>>();
		StringBuilder buildQuery = new StringBuilder();
		String medicareSource = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.MEDICARE_PLANS_SOURCE);
		if(null != medicareSource) {			
			if(PlanMgmtConstants.QUOTIT.equalsIgnoreCase(medicareSource)) {
				buildQuery.append(" SELECT zipcode, county, concat(state_fips,county_fips), STATE ");
				buildQuery.append(" FROM ZIPCODE_MEDICARE   ");
				buildQuery.append(" WHERE zipcode = (:" + ZIPCODE + ")");	
			} else if(PlanMgmtConstants.PUF.equalsIgnoreCase(medicareSource)) {
				buildQuery.append(" SELECT zipcode, county, concat(state_fips,county_fips), STATE ");
				buildQuery.append(" FROM ZIPCODES   ");
				buildQuery.append(" WHERE zipcode = (:" + ZIPCODE + ")");
			}
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Query to fetch county code and county name : " + buildQuery);
		}
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildQuery.toString());
			query.setParameter(ZIPCODE, zipCode);
			
			Iterator rsIterator = query.getResultList().iterator();
			
			while (rsIterator.hasNext()) {
				Object[] countyMedicares = (Object[]) rsIterator.next();			
				Map<String, String> zipCountyList = new HashMap<String, String>();
				zipCountyList.put("countyName", String.valueOf(countyMedicares[PlanMgmtConstants.ONE]));
				zipCountyList.put("countyCode", String.valueOf(countyMedicares[PlanMgmtConstants.TWO]));
				zipCountyList.put("state", String.valueOf(countyMedicares[PlanMgmtConstants.THREE]));				
				zipCountyFullList.add(zipCountyList);
			}
			
		}catch(Exception e){
			LOGGER.error("Error fetching county code and county name ", e);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return zipCountyFullList;
	}
	
	@Transactional(readOnly = true)	
	public List<MedicarePlan> getPUFPlanRateBenefits(List<Member> memberList, String effectiveDate, String insuranceType, String tenant, String zipCode, String countyName, String planYear){
		List<MedicarePlan> medicarePlanList = new ArrayList<MedicarePlan>();
		//StringBuilder logoUrl = new StringBuilder();
		String brochureUrl = null;
		//String issuerLogo = null;
		StringBuilder buildQuery = new StringBuilder(1048);
		String medicarePlanIdStr = PlanMgmtConstants.EMPTY_STRING;
		
		String newPlanYear = null;
		Object[] rsObjectArray = null;
		EntityManager entityManager = null;
		
		try {		
			entityManager = emf.createEntityManager();
			buildQuery.append("SELECT pm.id AS medicare_plan_id,p.name AS plan_name,p.issuer_id,i.name AS issuer_name,p.brochure,p.network_type,i.logo_url,pm.cms_is_snp,pm.is_medicaid,");
			buildQuery.append("p.issuer_plan_number,p.id,p.insurance_type,p.applicable_year,p.hiosproduct_id");
			buildQuery.append(" FROM plan p,issuers i,plan_medicare pm,pm_service_area psarea, pm_tenant_plan tp, tenant t ");
			buildQuery.append(" WHERE i.id = p.issuer_id AND p.id = pm.plan_id AND p.status IN('CERTIFIED', 'RECERTIFIED')");
			buildQuery.append(" AND TO_DATE ((:" + EFFECTIVEDATE + "), 'MM/DD/YYYY') BETWEEN p.start_date and p.end_date ");
			buildQuery.append(" AND p.issuer_verification_status = 'VERIFIED'");
			buildQuery.append(" AND p.INSURANCE_TYPE=(:" + INSURANCETYPE +")");
			if(null == planYear || planYear.equalsIgnoreCase("")) {
				newPlanYear = effectiveDate.substring(6,10);
			} else {
				newPlanYear = planYear;				
			}
			buildQuery.append(" AND p.applicable_year = :");
			buildQuery.append(PLANYEAR);
			buildQuery.append(" AND p.SERVICE_AREA_ID = psarea.SERVICE_AREA_ID");
			buildQuery.append(" AND psarea.ZIP = (:" + ZIPCODE + ")");
			buildQuery.append(" AND psarea.COUNTY = (:" + COUNTYNAME + ")");
			buildQuery.append(" AND p.is_deleted = 'N' AND psarea.is_deleted = 'N'");
			buildQuery.append(" AND pm.DATA_SOURCE = 'PUF'");
			buildQuery.append(" AND p.id = tp.plan_id");
			buildQuery.append(" AND tp.tenant_id = t.id");
			buildQuery.append(" AND t.code = UPPER(:");
			buildQuery.append(TENANT);
			buildQuery.append(") ");
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL === " + buildQuery.toString());
			}
			Query query = entityManager.createNativeQuery(buildQuery.toString());
			query.setParameter(EFFECTIVEDATE, effectiveDate);
			query.setParameter(INSURANCETYPE, insuranceType);			
			query.setParameter(ZIPCODE, zipCode);
			query.setParameter(COUNTYNAME, countyName);
			query.setParameter(PLANYEAR, newPlanYear);
			query.setParameter(TENANT, tenant);
			
			Iterator rsIterator = query.getResultList().iterator();
			while(rsIterator.hasNext()){
				String logoURL = null;

				rsObjectArray = (Object[]) rsIterator.next();
				
				MedicarePlan medicarePlan = new MedicarePlan();
				medicarePlan.setId(Integer.parseInt(rsObjectArray[PlanMgmtConstants.TEN].toString()));
				medicarePlan.setName((String.valueOf(rsObjectArray[PlanMgmtConstants.ONE])));
				medicarePlan.setIssuerId(Integer.parseInt(rsObjectArray[PlanMgmtConstants.TWO].toString()));
				medicarePlan.setIssuerName((String.valueOf(rsObjectArray[PlanMgmtConstants.THREE])));														
				medicarePlan.setNetworkType((String.valueOf(rsObjectArray[PlanMgmtConstants.FIVE])));				
				medicarePlan.setBenefitsLink("");
				medicarePlan.setIsSNP((String.valueOf(rsObjectArray[PlanMgmtConstants.SEVEN])));
				medicarePlan.setIsMedicaid((String.valueOf(rsObjectArray[PlanMgmtConstants.EIGHT])));
				if(null != rsObjectArray[PlanMgmtConstants.SIX]) {
					logoURL = rsObjectArray[PlanMgmtConstants.SIX].toString().trim();
				}
				
				medicarePlan.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, null, 
						ghixJasyptEncrytorUtil.encryptStringByJasypt(rsObjectArray[PlanMgmtConstants.TWO].toString()), appUrl.toString()));
				/*issuerLogo = rsObjectArray[PlanMgmtConstants.SIX] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.SIX]).trim());
				if (StringUtils.isNotBlank(issuerLogo)) {
					
					if (issuerLogo.matches(PlanMgmtConstants.ECM_REGEX)) {
						issuerLogo = appUrl.append(DOCUMENT_URL).append(ghixJasyptEncrytorUtil.encryptStringByJasypt(issuerLogo)).toString();				
					}
					else if (!issuerLogo.matches(PlanMgmtConstants.URL_REGEX)) {
						issuerLogo = PlanMgmtConstants.HTTP_PROTOCOL + issuerLogo;				
					}
				}*/
				
				brochureUrl = rsObjectArray[PlanMgmtConstants.FOUR] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.FOUR]).trim());
				
				if (StringUtils.isNotBlank(brochureUrl)) {
					
					if (brochureUrl.matches(PlanMgmtConstants.ECM_REGEX)) {
						brochureUrl = appUrl.append(DOCUMENT_URL).append(ghixJasyptEncrytorUtil.encryptStringByJasypt(brochureUrl)).toString();				
					}
					else if (!brochureUrl.matches(PlanMgmtConstants.URL_REGEX)) {
						brochureUrl = PlanMgmtConstants.HTTP_PROTOCOL + brochureUrl;				
					}
				}
				
				//medicarePlan.setIssuerLogo(issuerLogo);
				medicarePlan.setBrochureUrl(brochureUrl);
				medicarePlan.setInsuranceType((String.valueOf(rsObjectArray[PlanMgmtConstants.ELEVEN])));
				medicarePlan.setPlanYear((String.valueOf(rsObjectArray[PlanMgmtConstants.TWELVE])));
				medicarePlan.setPlanHiosId((String.valueOf(rsObjectArray[PlanMgmtConstants.THIRTEEN])));
				
				
				medicarePlanIdStr += (medicarePlanIdStr.length() == 0) ? "'" + rsObjectArray[PlanMgmtConstants.ZERO].toString() + "'" : ", '" + rsObjectArray[PlanMgmtConstants.ZERO].toString() + "'";
				
				Map<String, Map<String, Map<String, String>>> benefitData = getMedicarePlanBenefits(medicarePlanIdStr); // fetch Medicare plan benefits from database		
				Map<String, Map<String, String>> benefits = benefitData.get(String.valueOf(rsObjectArray[PlanMgmtConstants.ZERO].toString()));
				Map<String, String> benefitKey = benefits.get(PlanMgmtConstants.MONTHLY_PREMIUM_VAL);				
				String premium = benefitKey.get(PlanMgmtConstants.TINY_VALUE);			
				if(null != premium && premium.contains("$")) {
					medicarePlan.setPremium(Float.parseFloat(premium.replace("$", "").trim()));
				}else {
					medicarePlan.setPremium(Float.parseFloat(premium));
				}
				medicarePlan.setBenefits(benefitData.get(String.valueOf(rsObjectArray[PlanMgmtConstants.ZERO].toString())));

				medicarePlanList.add(medicarePlan);
			}

			
		}catch(Exception e) {
			LOGGER.error("Error while fetching medicare PUF plans ", e);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return medicarePlanList;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Map<String, Map<String, String>> getMedicarePlanBenefits(int planMedicareIdStr) {
		Map<String, Map<String, String>> benefitMap = new HashMap<String, Map<String, String>>();
				
		StringBuilder buildquery = new StringBuilder();
		buildquery.append("SELECT PLAN_MEDICARE_ID, CATEGORY, FULL_VALUE || FULL_VALUE_EXT, TINY_VALUE ");
		buildquery.append("FROM PLAN_MC_BENEFIT WHERE PLAN_MEDICARE_ID IN (:");
		buildquery.append(PLANMEDICAREIDSTR);
		buildquery.append(") ORDER BY PLAN_MEDICARE_ID");
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("INPUT buildquery :: " + buildquery);		
		}	
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildquery.toString());
			query.setParameter(PLANMEDICAREIDSTR, planMedicareIdStr);
			Iterator rsIterator = query.getResultList().iterator();
			
			while (rsIterator.hasNext()) {
				Object[] benefitFileds = (Object[]) rsIterator.next();
				Map<String, String> benefitAttrib = new HashMap<String, String>();
				
				String planStmId = benefitFileds[PlanMgmtConstants.ZERO].toString();
				String benefitName = (benefitFileds[PlanMgmtConstants.ONE] == null ? "" : benefitFileds[PlanMgmtConstants.ONE].toString());
				
				benefitAttrib.put(PlanMgmtConstants.LONG_VALUE, benefitFileds[PlanMgmtConstants.TWO] == null ? "" : benefitFileds[PlanMgmtConstants.TWO].toString());
				benefitAttrib.put(PlanMgmtConstants.TINY_VALUE, benefitFileds[PlanMgmtConstants.THREE] == null ? "" : benefitFileds[PlanMgmtConstants.THREE].toString());				
				benefitMap.put(benefitName, benefitAttrib);
				
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getMedicarePlanBenefits", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
				
		return benefitMap;
	}	

}
