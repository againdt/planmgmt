package com.getinsured.hix.planmgmt.service;

import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingResponseDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;

import org.apache.log4j.Logger;

public class PlanDataDTOHelper {

	private static final Logger LOGGER = Logger.getLogger(PlanDataDTOHelper.class);
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	public void setLifeSpanEnrollmentQuotingResponseDTO(LifeSpanEnrollmentQuotingResponseDTO lifeSpanEnrollmentQuotingResponseDTO, Plan planObj, String insuranceType, String considerEHBPortion, String appUrl, PlanMgmtService planMgmtService){
		try{
		
			lifeSpanEnrollmentQuotingResponseDTO.setPlanName(planObj.getName());
			lifeSpanEnrollmentQuotingResponseDTO.setPlanYear(planObj.getApplicableYear());
			//lifeSpanEnrollmentQuotingResponseDTO.setCsrAmount((float)0.00);
			
			// SET CSR AMOUNT
			lifeSpanEnrollmentQuotingResponseDTO.setCsrAmount(getCsrAmount(insuranceType, planObj, planMgmtService));
			
			// IF INSURANCE TYPE IS HEALTH 
			if(PlanMgmtConstants.HEALTH.equalsIgnoreCase(insuranceType)){
				lifeSpanEnrollmentQuotingResponseDTO.setCostSharing(planObj.getPlanHealth().getCostSharing());
				lifeSpanEnrollmentQuotingResponseDTO.setPlanLevel(planObj.getPlanHealth().getPlanLevel());
	
				// SET EHB AMOUNT
				if(PlanMgmtConstants.Y.equalsIgnoreCase(considerEHBPortion)){						
					// set default value as 1 for EHB percentage and 0 for state mandate EHB percentage
					lifeSpanEnrollmentQuotingResponseDTO.setEhbPercentage(getEhbPercentage(planObj));
					lifeSpanEnrollmentQuotingResponseDTO.setStateEhbMandatePercentage(getStateEhbMandatePercentage());		
				}
			}		
			else if(PlanMgmtConstants.DENTAL.equalsIgnoreCase(insuranceType)){ 		 // IF INSURANCE TYPE IS DENTAL
				lifeSpanEnrollmentQuotingResponseDTO.setCostSharing(planObj.getPlanDental().getCostSharing());
				lifeSpanEnrollmentQuotingResponseDTO.setPlanLevel(planObj.getPlanDental().getPlanLevel());
				
				// SET PEDIATRIC DENTAL COMPONENT AMOUNT
				if(PlanMgmtConstants.Y.equalsIgnoreCase(considerEHBPortion) && planObj.getPlanDental().getEhbApptForPediatricDental() != null){
					lifeSpanEnrollmentQuotingResponseDTO.setPediatricDentalComponent(getPediatricDentalComponent(planObj));
				}	
			}
			
			lifeSpanEnrollmentQuotingResponseDTO.setHiosPlanId(planObj.getIssuerPlanNumber());
			lifeSpanEnrollmentQuotingResponseDTO.setIssuerName(planObj.getIssuer().getName());
			lifeSpanEnrollmentQuotingResponseDTO.setIssuerId(planObj.getIssuer().getId());
			lifeSpanEnrollmentQuotingResponseDTO.setIssuerTaxId((planObj.getIssuer().getFederalEin() != null) ? planObj.getIssuer().getFederalEin().replace("-", PlanMgmtConstants.EMPTY_STRING) : PlanMgmtConstants.EMPTY_STRING);
			lifeSpanEnrollmentQuotingResponseDTO.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(planObj.getIssuer().getLogoURL(), planObj.getIssuer().getHiosIssuerId(), null, appUrl));
			lifeSpanEnrollmentQuotingResponseDTO.setHiosIssuerId(planObj.getIssuer().getHiosIssuerId());
			lifeSpanEnrollmentQuotingResponseDTO.setPlanDecertificationDate((planObj.getDeCertificationEffDate() != null) ? String.valueOf(planObj.getDeCertificationEffDate()) : PlanMgmtConstants.EMPTY_STRING);
		}catch(Exception ex){
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occurs at setLifeSpanEnrollmentQuotingResponseDTO() ",ex);
			LOGGER.error("Exception occurs at setLifeSpanEnrollmentQuotingResponseDTO() : ", ex);	
		}
	}
	
	
	private float getCsrAmount(String insuranceType, Plan planObj, PlanMgmtService planMgmtService){
		float csrAmount = (float)0.00;

		if(PlanMgmtConstants.HEALTH.equalsIgnoreCase(insuranceType)){		// IF INSURANCE TYPE IS HEALTH
			
			// IF PLAN'S APPLICABLE YEAR >= 2015 THEN PULL CSR AMOUNT FORM CSR_MULTIPLIER TABLE 
			// ELSE CONSIDER PLAN_HEALTH.CSR_ADV_PAY_AMOUNT AS CSR AMOUNT
			if(planObj.getApplicableYear() >= PlanMgmtConstants.TWO_ZERO_ONE_FIVE){
				csrAmount = planMgmtService.getCSRMultiplier(planObj.getPlanHealth().getCostSharing(), planObj.getPlanHealth().getPlanLevel(), planObj.getApplicableYear());
			}else{
				if(planObj.getPlanHealth().getCsrAdvPayAmt() != null && planObj.getPlanHealth().getCsrAdvPayAmt() > 0 ){
					csrAmount = planObj.getPlanHealth().getCsrAdvPayAmt();
				}
			}
		}else if(PlanMgmtConstants.DENTAL.equalsIgnoreCase(insuranceType)){		// IF INSURANCE TYPE IS DENTAL
			
			// IF PLAN'S APPLICABLE YEAR >= 2015 THEN PULL CSR AMOUNT FORM CSR_MULTIPLIER TABLE 
			if(planObj.getApplicableYear() >= PlanMgmtConstants.TWO_ZERO_ONE_FIVE){
				csrAmount = planMgmtService.getCSRMultiplier(planObj.getPlanDental().getCostSharing(), planObj.getPlanDental().getPlanLevel(), planObj.getApplicableYear());
			}
		}
		
		return csrAmount;
	}
	
	
	// SET EHB PERCENTAGE
	private String getEhbPercentage(Plan planObj){
		String ehbPercentage = PlanMgmtConstants.ONE_STR;		// DEFAULT VALUE IS 1

		// HIX-65077, fetch ehb portion from plan table.
		if(null != planObj.getEhbPremiumFraction()){
			ehbPercentage = planObj.getEhbPremiumFraction().toString();
		}
		
		return ehbPercentage;
	}
	
	
	// SET STATE MANDATE EHB PERCENTAGE
	private String getStateEhbMandatePercentage(){
		return PlanMgmtConstants.STRZERO;		// DEFAULT VALUE IS 0
	}
	
	
	// SET PEDIATRIC DENTAL COMPONENT
	private String getPediatricDentalComponent(Plan planObj){
		return planObj.getPlanDental().getEhbApptForPediatricDental().replace("$", PlanMgmtConstants.EMPTY_STRING);
	}
	
}
