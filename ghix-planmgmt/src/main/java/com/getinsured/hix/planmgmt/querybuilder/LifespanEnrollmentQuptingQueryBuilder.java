package com.getinsured.hix.planmgmt.querybuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.service.QuotingBusinessLogic;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.lookup.service.LookupService;

@Component
public class LifespanEnrollmentQuptingQueryBuilder {

	@Autowired 
	private LookupService lookupService;
	
	@Autowired
	private QuotingBusinessLogic quotingBusinessLogic; 
	
	public StringBuilder buildQuery(ArrayList<String> paramList, List<Map<String, String>> processedHouseholdData, String configuredDB, String planId){
		
		
		StringBuilder buildQuery = new StringBuilder(2048);
		String familyTier = quotingBusinessLogic.getFamilyTier(processedHouseholdData); // compute family tier
		String familyTierLookupCode = lookupService.getLookupValueCode(PlanMgmtConstants.FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
		String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedHouseholdData, Plan.PlanInsuranceType.HEALTH.toString());
		String stateName = quotingBusinessLogic.getApplicantStateCode(processedHouseholdData);	
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		String applicableYear = null;
		
		buildQuery.append("SELECT sum(pre) AS total_premium, ");
		if(PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB) || StringUtils.isEmpty(configuredDB)) {
			buildQuery.append("LISTAGG(memberid, ',') within GROUP (order by memberid) AS memberlist, ");
			buildQuery.append("LISTAGG(pre, ',') within GROUP (order by memberid) AS memberprelist, ");
		} else if(PlanMgmtConstants.DB_POSTGRESQL.equalsIgnoreCase(configuredDB)) {
			buildQuery.append("STRING_AGG(CAST (memberid AS VARCHAR), ',' order by memberid) AS memberlist, ");
			buildQuery.append("STRING_AGG(to_char(CAST (pre AS DOUBLE PRECISION), '9999D99'), ',' order by memberid) AS memberprelist, ");
		}

		buildQuery.append("rateOption, ratingArea ");
		buildQuery.append("FROM plan p, ( ");

		for (int i = 0; i < processedHouseholdData.size(); i++) {
			applicableYear = processedHouseholdData.get(i).get(PlanMgmtConstants.MEMBER_COVERAGE_DATE).substring(0,4);
			buildQuery.append("( SELECT prate.rate AS pre, 1 as memberctr, p2.id AS plan_id, ");
			buildQuery.append(processedHouseholdData.get(i).get(PlanMgmtConstants.ID));
		    buildQuery.append(" AS memberid, prate.rate_option AS rateOption, prarea.rating_area AS ratingArea ");
			buildQuery.append("FROM ");
			buildQuery.append("pm_plan_rate prate, plan p2, pm_rating_area prarea ");
			buildQuery.append("WHERE ");
			buildQuery.append("	p2.id = " + planId);
			buildQuery.append(" AND p2.id = prate.plan_id ");
			buildQuery.append("AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') between p2.start_date and p2.end_date ");
			paramList.add(processedHouseholdData.get(i).get(PlanMgmtConstants.MEMBER_COVERAGE_DATE));
			buildQuery.append("AND p2.is_deleted = 'N' ");
			buildQuery.append("AND prarea.id = prate.rating_area_id ");
			buildQuery.append("AND prate.rating_area_id = ");

			// -------- changes for HIX-100092 starts --------
			buildQuery.append(quotingBusinessLogic.buildSearchRatingAreaLogic(stateCode, paramList, processedHouseholdData, i, applicableYear, configuredDB));
			// -------- changes for HIX-100092 ends --------
			
			// in private exchange rate could by age band or family tier
			buildQuery.append(quotingBusinessLogic.buildRateLogic("prate", stateName, houseHoldType, familyTierLookupCode, processedHouseholdData.get(i).get(PlanMgmtConstants.AGE), processedHouseholdData.get(i).get(PlanMgmtConstants.TOBACCO), processedHouseholdData.get(i).get(PlanMgmtConstants.MEMBER_COVERAGE_DATE), paramList));
		//	buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic("p2", householdData.get(i).get(PlanMgmtConstants.MEMBER_COVERAGE_DATE), PlanMgmtConstants.YES, paramList));
			
			buildQuery.append(" ) ");
			
			if (i + 1 != processedHouseholdData.size()) {
				buildQuery.append(" UNION ALL ");
			}
		}

		buildQuery.append(") temp WHERE p.id = temp.plan_id ");
		buildQuery.append("GROUP BY p.id, rateOption, ratingArea HAVING sum(memberctr) = " + processedHouseholdData.size());

		return buildQuery;
	}
	
}
