package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.PlanComponentHistoryDTO;

public interface PlanHistoryService {

	// define interface of get plan certification history
	List<Map<String, Object>> getPlanCertificationHistory(Integer planId);
	
	// define interface of get plan history
	List<Map<String, Object>> getPlanHistory(Integer planId);

	List<Map<String, Object>> getIssuerPlanHistory(Integer planId);

	PlanComponentHistoryDTO getPlanBenefitsHistoryByRoleAndPlanId(String roleType, int planId);

	PlanComponentHistoryDTO getPlanEnrollmentHistoryByPlanId(int planId);
}
