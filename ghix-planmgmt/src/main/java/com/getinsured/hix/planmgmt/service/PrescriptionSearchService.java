/**
 * 
 */
package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.PrescriptionDTO;
import com.getinsured.hix.dto.planmgmt.PrescriptionSearchDTO;
import com.getinsured.hix.dto.prescription.PrescriptionSearchRequest;

/**
 * This Service used for getting drug information
 * depend on rxCode, durgName, fairCost
 * and returning copayAmount and coinsuranceAmount
 * @author shengole_s
 *
 */
public interface PrescriptionSearchService {
	
	
	/**
	 * Based on provided drugNameList and drugRxCodeList it will
	 * find drug information
	 * @param drugsNamesList
	 * @param drugsNamesList_Rxcode
	 * @param formularyId
	 * @return list of PrescriptionSearchDTO
	 */
	List<PrescriptionSearchDTO> getPrescriptionSearchDrugInformation(String formularyId, Integer issuerId, List<PrescriptionSearchRequest> prescriptionSearchRequstList);
	
	/**
	 * @param formularlyId
	 * @param issuer_id
	 * @param prescriptionSearchRequstList
	 * @return list of maps with drug name and formularyId
	 */
	List<Map<String, String>> getDrugNameFromFormularyId(int formularlyId, int issuer_id, List<PrescriptionSearchRequest> prescriptionSearchRequstList);
	
	/**
	 * @param formularyId
	 * @param drugTierIds
	 * @param drugInfoDBList
	 * @return list of maps with drugTierId, period of specific drug with key and map of cost share information
	 */
	List<Map<String,Map<String,Map<String,String>>>> getDrugCostShareInfo(Integer formularyId, String drugTierIds, List<Map<String,String>> drugInfoDBList);
	
	
	/**
	 * @param hiosPlanIdList
	 * @param applicableYear
	 * @param drugCodeList
	 * @return PrescriptionSearchByHIOSIdResponseDTO 
	 */
	 List<PrescriptionDTO> prescriptionSearchByHiosPlanId(List<String> hiosPlanIdList, Integer applicableYear, List<String> rxCodeList);
}
