package com.getinsured.hix.planmgmt.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.BenchmarkPremiumReq;

public interface IBenchmarkPremiumReqRepository  extends JpaRepository<BenchmarkPremiumReq, Integer> {

}
