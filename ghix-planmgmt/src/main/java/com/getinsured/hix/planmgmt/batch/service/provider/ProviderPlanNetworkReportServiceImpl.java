/**
 * 
 */
package com.getinsured.hix.planmgmt.batch.service.provider;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.PlanNetworkReport;
import com.getinsured.hix.planmgmt.batch.repository.IProviderPlanNetworkReportRepository;
import com.getinsured.hix.planmgmt.querybuilder.ProviderReportQueryBuilder;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * @author shengole_s
 *
 */
@Service("providerPlanNetworkReportService")
@Transactional

public class ProviderPlanNetworkReportServiceImpl implements ProviderPlanNetworkReportService{
	private static final Logger LOGGER = Logger.getLogger(ProviderPlanNetworkReportServiceImpl.class);
	
	@Autowired 
	private ContentManagementService ecmService;
	
	@Autowired
	private IProviderPlanNetworkReportRepository iProviderPlanNetworkReportRepository;
	
	@Override
	@Transactional
	public String generatePlanNetworkReport() {
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("------- Inside generatePlanNetworkReport -------");
		}
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		String reportGenerationResponse = PlanMgmtConstants.FAILURE;
		String planNetworkMappingDocumentId = null;
		String planNetworkMappingVendorDocumentId = null;
		try{
			String getproviderPlanNetworkReportSQL = ProviderReportQueryBuilder.getproviderPlanNetworkReportSQL();
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("getproviderPlanNetworkReportSQL: " + getproviderPlanNetworkReportSQL);
			}
			
			Context initialContext = new InitialContext();
		    DataSource datasource = (DataSource)initialContext.lookup(PlanMgmtConstants.DATASRC);
		    if (datasource != null) {
		        conn = datasource.getConnection();
		        stmt = conn.prepareStatement(getproviderPlanNetworkReportSQL);
		        stmt.setFetchSize(100);
		        rs = stmt.executeQuery();
				List<Map<String,String>> reportMapsList = getDataFromResultSet(rs);
		        if(null != reportMapsList){
					planNetworkMappingDocumentId = generateExcelFromMapsList(reportMapsList);
					planNetworkMappingVendorDocumentId = generateVendorExcelFromMapsList(reportMapsList);
				}
				
				if(null != planNetworkMappingDocumentId || null != planNetworkMappingVendorDocumentId){
					reportGenerationResponse = saveReportFileInfoInTable(planNetworkMappingDocumentId, planNetworkMappingVendorDocumentId);
				}
		    }
			
		}catch(Exception ex){
			LOGGER.error("Exception occured in generatePlanNetworkReport: ", ex);
		}finally{
			PlanMgmtUtil.closeResultSet(rs);
        	PlanMgmtUtil.closePreparedStatement(stmt);
        	PlanMgmtUtil.closeDBConnection(conn);	    
		}
		return reportGenerationResponse;
	}
	
	/**
	 * returning list of maps for generating excel
	 * @param resultSet
	 * @return list of maps of report data
	 */
	private List<Map<String,String>> getDataFromResultSet(ResultSet resultSet){
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("------- Getting data for report -------");
		}
		List<Map<String,String>> reportDataMapsList = new ArrayList<Map<String,String>>();
		Map<String,String> reportDataMap = null;
		try{
			if(null != resultSet){
				while(resultSet.next()){
					reportDataMap = new HashMap<String,String>();
					reportDataMap.put(PlanMgmtConstants.ID, checkNullUtil(resultSet.getString(PlanMgmtConstants.ID_CAPS)));
					reportDataMap.put(PlanMgmtConstants.PLAN_NAME, checkNullUtil(resultSet.getString(PlanMgmtConstants.NAME)));
					reportDataMap.put(PlanMgmtConstants.ISSUER_NAME, checkNullUtil(resultSet.getString(PlanMgmtConstants.CARRIER_NAME)));
					reportDataMap.put(PlanMgmtConstants.NETWORK_TYPE, checkNullUtil(resultSet.getString(PlanMgmtConstants.NETWORK_TYPE)));
					reportDataMap.put(PlanMgmtConstants.STATES, checkNullUtil(resultSet.getString(PlanMgmtConstants.STATE_CAPS)));
					reportDataMap.put(PlanMgmtConstants.STATUS, checkNullUtil(resultSet.getString(PlanMgmtConstants.STATUS_CAPS)));
					reportDataMap.put(PlanMgmtConstants.ISSUER_VERIFICATION, checkNullUtil(resultSet.getString(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS)));
					reportDataMap.put(PlanMgmtConstants.EXCHANGE_TYPE, checkNullUtil(resultSet.getString(PlanMgmtConstants.EXCHANGE_TYPE_CAPS)));
					reportDataMap.put(PlanMgmtConstants.NETWORK_NAME, checkNullUtil(resultSet.getString(PlanMgmtConstants.NETWORK_NAME)));
					reportDataMap.put(PlanMgmtConstants.NETWORK_ID, checkNullUtil(resultSet.getString(PlanMgmtConstants.NETWORK_ID)));
					reportDataMap.put(PlanMgmtConstants.NETWORK_URL, checkNullUtil(resultSet.getString(PlanMgmtConstants.NETWORK_URL)));
					reportDataMap.put(PlanMgmtConstants.ISSUERPLANNUMBER_MAP, checkNullUtil(resultSet.getString(PlanMgmtConstants.ISSUER_PLAN_NUMBER)));
					reportDataMap.put(PlanMgmtConstants.PLAN_LEVEL, checkNullUtil(resultSet.getString(PlanMgmtConstants.PLANLEVEL)));
					reportDataMap.put(PlanMgmtConstants.HIOS_ISSUER_ID, checkNullUtil(resultSet.getString(PlanMgmtConstants.HIOS_ISSUER_ID)));
					reportDataMap.put(PlanMgmtConstants.T2_COPAY, checkNullUtil(resultSet.getString(PlanMgmtConstants.T2_COPAY)));
					reportDataMap.put(PlanMgmtConstants.T2_COINS, checkNullUtil(resultSet.getString(PlanMgmtConstants.T2_COINS)));
					reportDataMap.put(PlanMgmtConstants.TIER_2_PRESENT, checkNullUtil(resultSet.getString(PlanMgmtConstants.TIER_2_PRESENT)));
					reportDataMap.put(PlanMgmtConstants.APPLICABLE_YEAR, checkNullUtil(resultSet.getString(PlanMgmtConstants.APPLICABLE_YEAR)));
					reportDataMap.put(PlanMgmtConstants.NETWORK_KEY, checkNullUtil(resultSet.getString(PlanMgmtConstants.NETWORK_KEY)));
					reportDataMap.put(PlanMgmtConstants.PRODUCT_ID, checkNullUtil(resultSet.getString(PlanMgmtConstants.PRODUCT_ID)));
					reportDataMap.put(PlanMgmtConstants.PRODUCT_NAME, checkNullUtil(resultSet.getString(PlanMgmtConstants.PRODUCT_NAME)));
					reportDataMapsList.add(reportDataMap);
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getDataFromReportIterator: ", ex);
		}
		return reportDataMapsList;
	}
	
	/**
	 * check value passed to function is null or not
	 * @param checkNullString
	 * @return emptyString if null else return string
	 */
	private String checkNullUtil(Object checkNullString){
		String returnString = PlanMgmtConstants.EMPTY_STRING;
		try{
			returnString = (null== checkNullString ? PlanMgmtConstants.EMPTY_STRING : checkNullString).toString();
		}catch(Exception ex){
			LOGGER.error("Exception occured in checkNullUtil: ", ex);
		}
		return returnString;
	}
	
	/**
	 * Generating and uploading excel file from list of maps
	 * @param reportMapsList
	 * @return string response success if uploaded successfully
	 */
	private String generateExcelFromMapsList(List<Map<String,String>> reportMapsList){
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("------- Generation and uploading excel file for default User -------");
		}
		String documentId = null;
		try{
			String[] excelHeaderArray = {"ID","NAME","CARRIER_NAME","NETWORK_TYPE","STATE","STATUS","ISSUER_VERIFICATION_STATUS","EXCHANGE_TYPE",
						"NETWORK_NAME","NETWORK_ID","NETWORK_URL","ISSUER_PLAN_NUMBER","PLAN_LEVEL","HIOS_ISSUER_ID","T2_COPAY","T2_COINS","TIER_2_PRESENT","APPLICABLE_YEAR", "PRODUCT_CODE", "PRODUCT_NAME", "NETWORK KEY"};
			
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet worksheet = workbook.createSheet("Plan Data Report");
			HSSFRow headerRow = worksheet.createRow(PlanMgmtConstants.ZERO);
			int header_index = PlanMgmtConstants.ZERO;
			
			/*	Adding header in the excel	*/
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("------- Adding headers to excel file -------");
			}
			for(String excelHeader : excelHeaderArray){
				HSSFCell headerCell = headerRow.createCell(header_index);
				header_index++;
				headerCell.setCellValue(excelHeader);
				
				/* Font style for header */
				HSSFCellStyle cellStyle = setPlanNetworkCellStyle(workbook, "HEADER", PlanMgmtConstants.DEFAULT_USER);
				headerCell.setCellStyle(cellStyle);
			}
			
			/*	AutoSize the column width	*/
			for(int colNum = PlanMgmtConstants.ZERO; colNum<headerRow.getLastCellNum();colNum++){
				workbook.getSheet("Plan Data Report").autoSizeColumn(colNum);
			}
			
			if(null != reportMapsList){
				String addRowResponse = addDataToExcelRows(workbook, worksheet, reportMapsList, PlanMgmtConstants.DEFAULT_USER);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("addRowResponse : " + addRowResponse);
				}
				
				if(StringUtils.isNotEmpty(addRowResponse) && addRowResponse.equalsIgnoreCase(PlanMgmtConstants.SUCCESS)){
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("----- Uploading Document in to ECM -----");
					}
					documentId = uploadPlanNetworkReportIntoECM("PlanNetworkMapping_Folder", workbook, PlanMgmtConstants.DEFAULT_USER);
				}
			}
			
		}catch(Exception ex){
			LOGGER.error("Exception occured in generateExcelFromMapsList: ", ex);
		}
		return documentId;
	}
	
	
	/**
	 * Generating and uploading vendor excel file from list of maps
	 * @param reportMapsList
	 * @return string response success if uploaded successfully
	 */
	private String generateVendorExcelFromMapsList(List<Map<String,String>> reportMapsList){
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("------- Generation and uploading excel file for vendor user -------");
		}
		String documentId = null;
		try{
			String[] excelHeaderArray = {"ISSUER PLAN NUMBER","NAME","CARRIER NAME","NETWORK TYPE","STATE","STATUS","ISSUER VERIFICATION STATUS","EXCHANGE TYPE",
						"NETWORK NAME","NETWORK ID","NETWORK URL","PLAN LEVEL","HIOS ISSUER ID","NETWORK KEY","TIER 2 PRESENT","YEAR","ID"};
			
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet worksheet = workbook.createSheet("Plan Data Report");
			HSSFRow headerRow = worksheet.createRow(PlanMgmtConstants.ZERO);
			int header_index = PlanMgmtConstants.ZERO;
			
			/*	Adding header in the excel	*/
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("------- Adding headers to excel file -------");
			}
			for(String excelHeader : excelHeaderArray){
				HSSFCell headerCell = headerRow.createCell(header_index);
				header_index++;
				headerCell.setCellValue(excelHeader);
				
				/* Font style for header */
				HSSFCellStyle cellStyle = setPlanNetworkCellStyle(workbook, "HEADER", PlanMgmtConstants.VENDOR_USER);
				headerCell.setCellStyle(cellStyle);
			}
			
			/*	AutoSize the column width	*/
			for(int colNum = PlanMgmtConstants.ZERO; colNum<headerRow.getLastCellNum();colNum++){
				workbook.getSheet("Plan Data Report").autoSizeColumn(colNum);
			}
			
			if(null != reportMapsList){
				String addRowResponse = addDataToExcelRows(workbook, worksheet, reportMapsList, PlanMgmtConstants.VENDOR_USER);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("addRowResponse : " + addRowResponse);
				}
				
				if(StringUtils.isNotEmpty(addRowResponse) && addRowResponse.equalsIgnoreCase(PlanMgmtConstants.SUCCESS)){
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("----- Uploading Document in to ECM -----");
					}
					documentId = uploadPlanNetworkReportIntoECM("PlanNetworkMapping_Folder", workbook, PlanMgmtConstants.VENDOR_USER);
				}
			}
			
		}catch(Exception ex){
			LOGGER.error("Exception occured in generateExcelFromMapsList: ", ex);
		}
		return documentId;
	}
	
	/**
	 * Get key for row in excel file
	 * @param generateForUser
	 * @return
	 */
	private String[] getKeysForExcel(String generateForUser){
		if(StringUtils.isNotEmpty(generateForUser) && generateForUser.equalsIgnoreCase(PlanMgmtConstants.DEFAULT_USER)){
			String[] defaultArrayOfKeys = {PlanMgmtConstants.ID,PlanMgmtConstants.PLAN_NAME,PlanMgmtConstants.ISSUER_NAME,PlanMgmtConstants.NETWORK_TYPE,PlanMgmtConstants.STATES,PlanMgmtConstants.STATUS,PlanMgmtConstants.ISSUER_VERIFICATION,
					PlanMgmtConstants.EXCHANGE_TYPE,PlanMgmtConstants.NETWORK_NAME,PlanMgmtConstants.NETWORK_ID,PlanMgmtConstants.NETWORK_URL,PlanMgmtConstants.ISSUERPLANNUMBER_MAP,
					PlanMgmtConstants.PLAN_LEVEL,PlanMgmtConstants.HIOS_ISSUER_ID,PlanMgmtConstants.T2_COPAY,PlanMgmtConstants.T2_COINS,PlanMgmtConstants.TIER_2_PRESENT,PlanMgmtConstants.APPLICABLE_YEAR, 
					PlanMgmtConstants.PRODUCT_ID, PlanMgmtConstants.PRODUCT_NAME, PlanMgmtConstants.NETWORK_KEY};
			return defaultArrayOfKeys;
		}else{
			String[] vendorArrayOfKeys = {PlanMgmtConstants.ISSUERPLANNUMBER_MAP,PlanMgmtConstants.PLAN_NAME,PlanMgmtConstants.ISSUER_NAME,PlanMgmtConstants.NETWORK_TYPE,PlanMgmtConstants.STATES,PlanMgmtConstants.STATUS,PlanMgmtConstants.ISSUER_VERIFICATION,
					PlanMgmtConstants.EXCHANGE_TYPE,PlanMgmtConstants.NETWORK_NAME,PlanMgmtConstants.NETWORK_ID,PlanMgmtConstants.NETWORK_URL,
					PlanMgmtConstants.PLAN_LEVEL,PlanMgmtConstants.HIOS_ISSUER_ID,PlanMgmtConstants.NETWORK_KEY,PlanMgmtConstants.TIER_2_PRESENT,PlanMgmtConstants.APPLICABLE_YEAR,PlanMgmtConstants.ID};
			return vendorArrayOfKeys;
		}
	}
	
	/**
	 * Add data to rows in excel
	 * @param temp_workbook
	 * @param temp_worksheet
	 * @param temp_reportMapsList
	 * @return string response return success/failure
	 */
	private String addDataToExcelRows(HSSFWorkbook temp_workbook, HSSFSheet temp_worksheet, List<Map<String,String>> temp_reportMapsList, String generateForUser){
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("------- Adding rows to report file -------");
		}
		String[] arrayOfKeys = getKeysForExcel(generateForUser);

		HSSFSheet worksheet = temp_worksheet;
		HSSFWorkbook workbook = temp_workbook;
		List<Map<String,String>> reportMapsList = temp_reportMapsList;
		int rowCount = PlanMgmtConstants.ONE;
		int cellCount = PlanMgmtConstants.ZERO;
		String returnResponse = PlanMgmtConstants.SUCCESS;
		Cell cell = null;
		try{
			HSSFCellStyle cellStyle = setPlanNetworkCellStyle(workbook, "DATA", generateForUser);
			if(null != reportMapsList){
				for(Map<String,String> reportMap : reportMapsList){
					Row row = worksheet.createRow(rowCount++);
					for(cellCount = PlanMgmtConstants.ZERO; cellCount < worksheet.getRow(PlanMgmtConstants.ZERO).getLastCellNum(); cellCount++){
						if(StringUtils.isNotEmpty(generateForUser) && generateForUser.equalsIgnoreCase(PlanMgmtConstants.DEFAULT_USER)){
							//cellCount == 13 ==> HIOS_ISSUER_ID
							//cellCount == 17 ==> APPLICABLE_YEAR
							//cellCount == 18 ==> PRODUCT ID
							if(cellCount == 13 || cellCount == 17 || cellCount == 18){
								cell = row.createCell(cellCount, Cell.CELL_TYPE_NUMERIC);
								cell.setCellStyle(cellStyle);
							}else{
								cell = row.createCell(cellCount, Cell.CELL_TYPE_STRING);
								cell.setCellStyle(cellStyle);
							}
						}else{
							//cellCount == 12 ==> HIOS_ISSUER_ID
							//cellCount == 15 ==> APPLICABLE_YEAR
							if(cellCount == 12 || cellCount == 15){
								cell = row.createCell(cellCount, Cell.CELL_TYPE_NUMERIC);
								cell.setCellStyle(cellStyle);
							}else{
								cell = row.createCell(cellCount, Cell.CELL_TYPE_STRING);
								cell.setCellStyle(cellStyle);
							}
						}
						
						if(null != cell && StringUtils.isNotEmpty(generateForUser) && generateForUser.equalsIgnoreCase(PlanMgmtConstants.DEFAULT_USER)){
							//cellCount == 13 ==> HIOS_ISSUER_ID
							//cellCount == 17 ==> APPLICABLE_YEAR
							//cellCount == 18 ==> PRODUCT ID
					        if(cellCount == 13 || cellCount == 17 || cellCount == 18){
					        	cell.setCellType(Cell.CELL_TYPE_NUMERIC);
					        	if(null != reportMap.get(arrayOfKeys[cellCount])){
					        		cell.setCellValue(reportMap.get(arrayOfKeys[cellCount]));
					        	}
					        }else if(cellCount != 13 && cellCount != 17 && cellCount !=18){
					        	cell.setCellValue(reportMap.get(arrayOfKeys[cellCount]));
					        }
						}else{
							//cellCount == 12 ==> HIOS_ISSUER_ID
							//cellCount == 15 ==> APPLICABLE_YEAR
					        if(cellCount == 12 || cellCount == 15){
					        	cell.setCellType(Cell.CELL_TYPE_NUMERIC);
					        	if(null != reportMap.get(arrayOfKeys[cellCount])){
					        		cell.setCellValue(Integer.parseInt(reportMap.get(arrayOfKeys[cellCount])));
					        	}
					        }else if(cellCount != 12 && cellCount != 15){
					        	cell.setCellValue(reportMap.get(arrayOfKeys[cellCount]));
					        }
						}
					}
				}
				returnResponse = PlanMgmtConstants.SUCCESS;
			}
		}catch(Exception ex){
			returnResponse = PlanMgmtConstants.FAILURE;
			LOGGER.error("Exception occure in addDataToExcelRows: ", ex);
		}
		return returnResponse;
	}
	
	/**
	 * generating cellStyle for excel file
	 * @param workbook
	 * @param applyFor
	 * @return cellStyle for excel
	 */
	private HSSFCellStyle setPlanNetworkCellStyle(HSSFWorkbook workbook, String applyFor, String generateForUser){
		HSSFCellStyle cellStyle= null; 
		try{
			cellStyle = workbook.createCellStyle();
			if(StringUtils.isNotEmpty(applyFor)){
				if(applyFor.equalsIgnoreCase("HEADER") && StringUtils.isNotEmpty(generateForUser) && generateForUser.equalsIgnoreCase(PlanMgmtConstants.DEFAULT_USER)){				
					cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
				}else{
					cellStyle.setFillForegroundColor(HSSFColor.WHITE.index);
				}
				cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				
				/* Font style for header */
				HSSFFont hSSFFont = workbook.createFont();
		        if(StringUtils.isNotEmpty(generateForUser) && generateForUser.equalsIgnoreCase(PlanMgmtConstants.DEFAULT_USER)){
		        	hSSFFont.setFontName(HSSFFont.FONT_ARIAL);
		        	hSSFFont.setFontHeightInPoints((short) 8);
		        }else{
		        	hSSFFont.setFontName("Calibri");
		        	hSSFFont.setFontHeightInPoints((short) 12);
		        }
		        
		        if(applyFor.equalsIgnoreCase("HEADER")){
		        	hSSFFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		        }
		        hSSFFont.setColor(HSSFColor.GREY_80_PERCENT.index);
		        cellStyle.setFont(hSSFFont);
		        cellStyle.setBorderRight((short)1);
		        cellStyle.setBorderBottom((short)1);
		        cellStyle.setRightBorderColor(HSSFColor.BLUE_GREY.index);
		        cellStyle.setBottomBorderColor(HSSFColor.BLUE_GREY.index);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in setPlanNetworkCellStyle : ", ex);
		}
		return cellStyle;
	}
	
	/**
	 * Uploading the document into ECM
	 * @param fileToUpload
	 * @param folderName
	 * @param workbook
	 * @return doucmentId
	 */
	private String uploadPlanNetworkReportIntoECM(String folderName, HSSFWorkbook workbook, String uploadForUser){
		String returnString = PlanMgmtConstants.FAILURE;
		StringBuilder fileNameBuffer = null;
		try{
			if(StringUtils.isNotEmpty(folderName) && StringUtils.isNotEmpty(uploadForUser)){
				if(uploadForUser.equalsIgnoreCase(PlanMgmtConstants.DEFAULT_USER)){
					fileNameBuffer = new StringBuilder("PlanNetWorkMapping");
				}else{
					fileNameBuffer = new StringBuilder("VendorPlanNetWorkMapping");
				}
				fileNameBuffer.append(new SimpleDateFormat("MMddyyyy-HHmm").format(TSCalendar.getInstance().getTime()));
		        fileNameBuffer.append(PlanMgmtConstants.FILE_EXTENSION);
		        
				String filePath = GhixConstants.UPLOAD_PATH+PlanMgmtConstants.DOUBLE_SLASH+fileNameBuffer;
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("FilePath of generated File: " + filePath);
				}
		        
			    FileOutputStream fileOut = new FileOutputStream(filePath);
			    workbook.write(fileOut);
			    fileOut.flush();
			    fileOut.close();
			    
			    File file = new File(filePath);
				InputStream inputStream = new FileInputStream(file);
				byte [] bytes = IOUtils.toByteArray(inputStream);
				
				String modifiedFileName = FilenameUtils.getBaseName(file.getName()) + GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT + FilenameUtils.getExtension(file.getName());
				
				String documentId = ecmService.createContent(GhixConstants.ISSUER_DOC_PATH + PlanMgmtConstants.SINGLE_SLASH + folderName, modifiedFileName, bytes,
						ECMConstants.PlanMgmt.DOC_CATEGORY, ECMConstants.PlanMgmt.DOC_SUB_CATEGORY, null );
				returnString = file.getName() + PlanMgmtConstants.COMMA_STRING + documentId;
	
				if(file.exists()){
					inputStream.close();
					file.delete();
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception Occured in uploadPlanNetworkReportIntoECM : ", ex);
			return returnString;
		}
		return returnString;
	}
	
	/**
	 * save file information into table
	 * @param planNetworkMappingDocumentInfo
	 * @return string response success/failure
	 */
	private String saveReportFileInfoInTable(String planNetworkMappingDocumentInfo, String planNetworkMappingVendorDocumentInfo){
		String saveDocumentInfoResponse = PlanMgmtConstants.FAILURE;
		try{
			if(StringUtils.isNotBlank(planNetworkMappingDocumentInfo) && StringUtils.isNotBlank(planNetworkMappingVendorDocumentInfo)){
				String[] documentInfoArray = planNetworkMappingDocumentInfo.split(PlanMgmtConstants.COMMA);
				String[] vendorDocumentInfoArray = planNetworkMappingVendorDocumentInfo.split(PlanMgmtConstants.COMMA);
				if(null != documentInfoArray && null != vendorDocumentInfoArray){
					if((null != documentInfoArray[PlanMgmtConstants.ZERO] && !((PlanMgmtConstants.FAILURE).equalsIgnoreCase(documentInfoArray[PlanMgmtConstants.ZERO]))) &&
							(null != vendorDocumentInfoArray[PlanMgmtConstants.ZERO] && !((PlanMgmtConstants.FAILURE).equalsIgnoreCase(vendorDocumentInfoArray[PlanMgmtConstants.ZERO])))){
						PlanNetworkReport planNetworkReport = new PlanNetworkReport();
						planNetworkReport.setEcmDocumentName(documentInfoArray[PlanMgmtConstants.ZERO]);
						planNetworkReport.setEcmDocumentId(documentInfoArray[PlanMgmtConstants.ONE]);
						
						planNetworkReport.setVendorDocumentName(vendorDocumentInfoArray[PlanMgmtConstants.ZERO]);
						planNetworkReport.setVendorDocumentId(vendorDocumentInfoArray[PlanMgmtConstants.ONE]);
	
						iProviderPlanNetworkReportRepository.save(planNetworkReport);
						saveDocumentInfoResponse = PlanMgmtConstants.SUCCESS;
					}else{
						saveDocumentInfoResponse = PlanMgmtConstants.FAILURE;
					}
					if(LOGGER.isInfoEnabled()){
						LOGGER.info("----- Report Generated -----");
					}
				}
			}
		}catch(Exception ex){		
			LOGGER.error("Exception occured in saveReportFileInfoInTable: ", ex);
		}
		return saveDocumentInfoResponse;
	}
}
