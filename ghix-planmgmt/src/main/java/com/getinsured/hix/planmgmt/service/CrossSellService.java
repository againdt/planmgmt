package com.getinsured.hix.planmgmt.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.CrossSellPlan;

public interface CrossSellService {
	Float getLowestDentalPlanPremium(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenant);
		
	List<CrossSellPlan> getDentalPlans(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenant);
	
	public List<CrossSellPlan> getAMEPlans(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenant);
	
	BigDecimal getLowestAMEPlanPremium(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenant);
	
	Float getLowestVisionPlanPremium(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenant);
	
	public List<CrossSellPlan> getVisionPlans(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenant);
	
}
