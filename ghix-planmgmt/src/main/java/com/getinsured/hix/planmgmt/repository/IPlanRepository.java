package com.getinsured.hix.planmgmt.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Plan;

@Repository("iPlanRepository")
public interface IPlanRepository extends JpaRepository<Plan, Integer>, RevisionRepository<Plan, Integer, Integer> {	

	@Query("SELECT new Plan(plan.id, plan.name, " +
			"plan.issuer.id, plan.issuer.name, plan.issuer.federalEin, " +
			"plan.planHealth.csrAdvPayAmt, plan.planHealth.planLevel, " +
			"plan.issuerPlanNumber, plan.insuranceType, plan.market) from Plan as plan " +
			"where plan.id=:planId")
	Plan getPlanHealthIssuerData(@Param("planId") int planId);
	
	@Query("SELECT new Plan(plan.id, plan.name, " +
			"plan.issuer.id, plan.issuer.name, plan.issuer.federalEin, " +
			"plan.planDental.planLevel, " +
			"plan.issuerPlanNumber, plan.insuranceType, plan.market) from Plan as plan " +
			"where plan.id=:planId")
	Plan getPlanDentalIssuerData(@Param("planId") int planId);
	
    @Query("SELECT distinct(p.insuranceType) FROM Plan p where p.id IN (:planIdList) AND isDeleted='N'")
    String getInsuranceTypeForPlanList(@Param("planIdList") List<Integer> planIdList);
    
    @Query("SELECT id,name,hiosProductId,insuranceType FROM Plan p WHERE p.id=:planId AND isDeleted='N'")
    List<Object[]> getPlanInfo(@Param("planId") Integer planId);
    
    @Modifying
	@Transactional
	@Query("UPDATE Plan p SET p.enrollmentAvail = p.futureEnrollmentAvail, p.futureEnrollmentAvail = NULL WHERE p.enrollmentAvailEffDate = to_date(to_char(CURRENT_DATE)) AND p.futureEnrollmentAvail IS NOT NULL")
	void updateEnrollmentAvaility();
    
    @Query("SELECT p FROM Plan p WHERE p.issuerPlanNumber=:hiosPlanNumber AND p.applicableYear = :applicableYear AND p.isDeleted='N'")
    List<Plan> getPlanByHIOSID(@Param("hiosPlanNumber") String hiosPlanNumber, @Param("applicableYear") int applicableYear);    
    
    @Query("SELECT distinct(p.applicableYear) FROM Plan p WHERE p.insuranceType=:insuranceType AND p.isDeleted='N' ORDER BY p.applicableYear DESC")
    List<String> getDistinctPlanYearsByInsuranceType(@Param("insuranceType") String insuranceType);
    
    @Query("SELECT distinct(p.applicableYear) FROM Plan p WHERE p.insuranceType=:insuranceType AND  p.issuer.id=:issuerId AND p.isDeleted='N' ORDER BY p.applicableYear DESC")
    List<String> getDistinctPlanYearsByInsuranceTypeAndIssuerId(@Param("insuranceType") String insuranceType, @Param("issuerId") int issuerId);

	@Query("SELECT count(p.id) FROM Plan p where p.issuer.hiosIssuerId=:hiosIssuerId AND isDeleted='N'")
	Long getPlanCountByHiosIssuerId(@Param("hiosIssuerId") String hiosIssuerId);

	@Query("SELECT COUNT(p.id) FROM Plan p WHERE (LOWER(p.issuerPlanNumber) LIKE LOWER(:planId) || '%') AND p.isDeleted='N' AND p.applicableYear=:applicableYear")
	Long getPlanCountByPlanIdAndApplicableYear(@Param("planId") String planId, @Param("applicableYear") Integer applicableYear);

	List<Plan> findByIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYear(@Param("isDeleted") String isDeleted,
			@Param("issuerPlanNumber") String issuerPlanNumber, @Param("applicableYear") Integer applicableYear);

//    @RestResource(path="getPlanYears")
//    @Query("SELECT  p FROM Plan p WHERE p.insuranceType=:insuranceType AND p.isDeleted='N' ORDER BY p.applicableYear DESC")
//    List<PlanLight> getPlanYearsByInsuranceType(@Param("insuranceType") String insuranceType);
//    
//    @RestResource(path="some")
//    List<Plan> findDistinctApplicableYearByInsuranceTypeAndIsDeleted(@Param("insuranceType") String insuranceType, @Param("isDeleted") String isDeleted);
//  
	
	@Query("SELECT id, issuerPlanNumber FROM Plan p WHERE p.issuerPlanNumber IN(:hiosPlanIds) AND p.isDeleted='N' AND p.applicableYear=:applicableYear AND p.insuranceType=:insuranceType")
	List<Object[]> getPlanIdsByHiosPlanIds(@Param("hiosPlanIds") List<String> hiosPlanIdList, @Param("applicableYear") Integer applicableYear, @Param("insuranceType") String insuranceType);
	
	@Query("SELECT p.id, p.issuerPlanNumber, p.network.name FROM Plan p WHERE p.issuerPlanNumber IN(:hiosPlanIds) AND p.isDeleted='N' AND p.applicableYear=:applicableYear AND p.insuranceType=:insuranceType")
	List<Object[]> getPlanIdsAndNetworkNamesByHiosPlanIds(@Param("hiosPlanIds") List<String> hiosPlanIdList, @Param("applicableYear") Integer applicableYear, @Param("insuranceType") String insuranceType);
	
	@Query("SELECT p.issuerPlanNumber, p.formularlyId, p.issuer.id, phb.name, phb.networkT1display " +
			"FROM " +
			"Plan p, PlanHealthBenefit phb, PlanHealth ph " +
			"WHERE " +
			"ph.plan.id = p.id and phb.plan.id = ph.id AND  p.issuerPlanNumber IN(:hiosPlanIds) AND p.isDeleted='N' " +
			"AND p.applicableYear=:applicableYear AND p.insuranceType=:insuranceType AND p.formularlyId IS NOT NULL " +
			"AND phb.name IN ('GENERIC', 'PREFERRED_BRAND', 'NON_PREFERRED_BRAND', 'SPECIALTY_DRUGS') ")
	List<Object[]> getFormularyIdAndBenefitsByHiosPlanIds(@Param("hiosPlanIds") List<String> hiosPlanIdList, @Param("applicableYear") Integer applicableYear, @Param("insuranceType") String insuranceType);

	@Query("SELECT issuerPlanNumber, formularlyId, issuer.id FROM Plan p WHERE p.issuerPlanNumber IN(:hiosPlanIds) AND p.isDeleted='N' "
			+ "AND p.applicableYear=:applicableYear AND p.insuranceType=:insuranceType AND p.formularlyId IS NOT NULL ")
	List<Object[]> getByHiosPlanIds(@Param("hiosPlanIds") List<String> hiosPlanIdList, @Param("applicableYear") Integer applicableYear, @Param("insuranceType") String insuranceType);
	
	@Modifying
	@Transactional
	@Query("UPDATE Plan plan SET plan.issuerVerificationStatus =:newIssuerVerificationStatus, plan.status =:newCertificationStatus, "
			+ " plan.lastUpdatedBy =:lastUpdatedBy, plan.lastUpdateTimestamp = CURRENT_TIMESTAMP() "
			+ " WHERE ( isDeleted='N' AND plan.insuranceType IN ('MC_ADV','MC_SUP','MC_RX')) "
			+ " AND ('ALL'  =:filterIssuerPlanNumber 	OR 	UPPER(plan.issuerPlanNumber) LIKE UPPER(:filterIssuerPlanNumber) || '%') "
			+ " AND ('ALL' =:filterState 				OR 	plan.state =:filterState) "
			+ " AND ('ALL' =:filterStatus 				OR 	plan.status =:filterStatus) "
			+ " AND (0 =:filterIssuerId 				OR 	plan.issuer.id =:filterIssuerId)"
			+ " AND (0L =:filterTenantId    		    OR  plan.id IN (SELECT DISTINCT (tp.plan.id) FROM TenantPlan tp WHERE tp.tenant.id =:filterTenantId))")
	int modifyMedicarePlanStatusInBulk(@Param("newCertificationStatus") String newCertificationStatus,
			@Param("newIssuerVerificationStatus") String newIssuerVerificationStatus,
			@Param("lastUpdatedBy") Integer lastUpdatedBy,
			@Param("filterIssuerPlanNumber") String filterIssuerPlanNumber, @Param("filterState") String filterState,
			@Param("filterStatus") String filterStatus, @Param("filterIssuerId") int filterIssuerId,
			@Param("filterTenantId") Long filterTenantId);

	@Modifying
	@Transactional
	@Query("UPDATE Plan plan SET plan.issuerVerificationStatus =:newIssuerVerificationStatus, plan.status =:newCertificationStatus, "
			+ " plan.lastUpdatedBy =:lastUpdatedBy, plan.lastUpdateTimestamp = CURRENT_TIMESTAMP() "
			+ " WHERE isDeleted='N' AND plan.insuranceType IN ('MC_ADV','MC_SUP','MC_RX') AND plan.id IN (:planIdList)")
	int modifyMedicarePlansForSelectedPlans(@Param("newCertificationStatus") String newCertificationStatus,
			@Param("newIssuerVerificationStatus") String newIssuerVerificationStatus,
			@Param("lastUpdatedBy") Integer lastUpdatedBy, @Param("planIdList") List<Integer> planIdList);
	
	@Modifying
    @Transactional
    @Query("UPDATE Plan p SET p.issuerVerificationStatus =:issuerVerificationStatus, p.status =:status, "
    		+ " p.enrollmentAvail =:enrollmentavail, p.lastUpdatedBy =:lastUpdatedBy, p.lastUpdateTimestamp = CURRENT_TIMESTAMP() "
    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:state 				OR 	p.state =:state) "
    		+ " AND ('ALL' 	=:planName 				OR 	UPPER(p.name) LIKE  '%' || UPPER(:planName) || '%') "
    		+ " AND ('ALL'  =:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "    		
    		+ " AND ('ALL' 	=:exchangeType    		OR  p.exchangeType =:exchangeType	)"
    		+ " AND ('ALL' 	=:nonCommissionFlag 	OR  p.nonCommissionFlag =:nonCommissionFlag)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"
    		+ " AND (0L		=:tenantId    			OR  p.id 		IN (SELECT DISTINCT (tp.plan.id) 	FROM TenantPlan tp 	WHERE tp.tenant.id 	=:tenantId	) ) "
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (ph.plan.id) 	FROM PlanHealth ph 	WHERE ph.planLevel  =:planLevel	) )"
    		+ " AND (0 		=:brandNameId 			OR 	p.issuer.id IN (SELECT DISTINCT (i.id) 			FROM Issuer i 		WHERE (i.issuerBrandName.id =:brandNameId)) )"
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
			+ " AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) "
			+ " AND (substring(p.issuerPlanNumber,15,2) IN (:selectedCsrVariation))"
			+ " AND ('ALL' =:enableForOffExchange 	OR 	p.enableForOffExchFlow =:enableForOffExchange)")
    int bulkUpdateQHPPlans(@Param("status") String status, @Param("issuerVerificationStatus") String issuerVerificationStatus, @Param("enrollmentavail") String enrollmentavail,
    		@Param("lastUpdatedBy") int lastUpdatedBy, @Param("insuranceType") String insuranceType, 
    		@Param("state") String state, @Param("issuerPlanNumber") String issuerPlanNumber,     		
    		@Param("planMarket") String planMarket, @Param("filterStatus") String filterStatus, @Param("exchangeType") String exchangeType,
    		@Param("nonCommissionFlag") String  nonCommissionFlag, @Param("planYear") int applicableYear, @Param("tenantId") Long tenantId, 
    		@Param("planLevel") String planLevel, @Param("brandNameId") int brandNameId, @Param("issuerId") int issuerId, @Param("selectedCsrVariation") List<String> selectedCsrVariation,
    		@Param("enableForOffExchange") String enableForOffExchange, @Param("planName") String planName);
	
	@Modifying
    @Transactional
    @Query("UPDATE Plan p SET p.issuerVerificationStatus =:issuerVerificationStatus, p.status =:status, "
    		+ " p.enrollmentAvail =:enrollmentavail, p.lastUpdatedBy =:lastUpdatedBy, p.lastUpdateTimestamp = CURRENT_TIMESTAMP() "
    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "    		
    		+ " AND ('ALL' 	=:state 				OR 	p.state =:state) "
    		+ " AND ('ALL' 	=:planName 				OR 	UPPER(p.name) LIKE  '%' || UPPER(:planName) || '%') "
    		+ " AND ('ALL'  =:issuerPlanNumber 		OR 	UPPER(p.issuerPlanNumber) LIKE UPPER(:issuerPlanNumber) || '%'		) "
    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "		
    		+ " AND ('ALL' 	=:exchangeType    		OR  p.exchangeType =:exchangeType	)"
    		+ " AND ('ALL' 	=:verified    			OR  p.issuerVerificationStatus =:verified	)"
    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"
    		+ " AND (0L		=:tenantId    			OR  p.id 		IN (SELECT DISTINCT (tp.plan.id) 	FROM TenantPlan tp 	WHERE tp.tenant.id 	=:tenantId	) ) "
			+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (pd.plan.id) 	FROM PlanDental pd 	WHERE pd.planLevel  =:planLevel	) )"
    		+ " AND (0 		=:brandNameId 			OR 	p.issuer.id IN (SELECT DISTINCT (i.id) 			FROM Issuer i 		WHERE (i.issuerBrandName.id =:brandNameId)) )"
    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
			+ " AND (p.issuer.id IN (select   id from Issuer i  where  i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) ")
    int bulkUpdateQDPPlans(@Param("status") String status, @Param("issuerVerificationStatus") String issuerVerificationStatus, @Param("enrollmentavail") String enrollmentavail,
    		@Param("lastUpdatedBy") int lastUpdatedBy, @Param("insuranceType") String insuranceType, 
    		@Param("state") String state, @Param("issuerPlanNumber") String issuerPlanNumber,     		
    		@Param("planMarket") String planMarket, @Param("filterStatus") String filterStatus, @Param("exchangeType") String exchangeType,
    		@Param("planYear") int applicableYear, @Param("tenantId") Long tenantId, @Param("verified") String verified, @Param("planLevel") String planLevel,
    		@Param("brandNameId") int brandNameId, @Param("issuerId") int issuerId, @Param("planName") String planName);
	
 
	@Query("SELECT p FROM Plan p WHERE p.id in (:planIds)")
    List<Plan> getPlanListByPlanIds(@Param("planIds") List<Integer> planIdList);  
	
	   @Query("SELECT COUNT(p.id) FROM Plan p"
	    		+ " WHERE ( p.isDeleted='N' AND p.insuranceType =:insuranceType)"
	    		+ " AND (substring(p.issuerPlanNumber,15,2) IN (:selectedCsrVariation))"
	    		+ " AND ('ALL' =:state 					OR 	p.state =:state)"
	    		+ " AND ('ALL' 	=:planName 				OR 	UPPER(p.name) LIKE  '%' || UPPER(:planName) || '%') "
	    		+ " AND ('ALL' =:issuerPlanNumber 		OR	UPPER(p.issuerPlanNumber) LIKE UPPER(:issuerPlanNumber) || '%') "
	    		+ " AND ('ALL' =:planMarket    			OR  p.market =:planMarket)"
	    		+ " AND ('ALL' =:filterStatus 			OR 	p.status =:filterStatus)"    		
	    		+ " AND ('ALL' =:exchangeType  			OR  p.exchangeType =:exchangeType)"
	    		+ " AND ('ALL' =:nonCommissionFlag 		OR  p.nonCommissionFlag =:nonCommissionFlag)"
	    		+ " AND (0 =:planYear    				OR  p.applicableYear =:planYear )"
	    		+ " AND (0L	=:tenantId    				OR  p.id IN (SELECT DISTINCT (tp.plan.id) FROM TenantPlan tp WHERE tp.tenant.id =:tenantId)) "
				+ " AND ('ALL' =:planLevel     			OR  p.id IN (SELECT DISTINCT (ph.plan.id) FROM PlanHealth ph WHERE ph.planLevel =:planLevel))"
	    		+ " AND (0 =:brandNameId 				OR 	p.issuer.id IN (SELECT DISTINCT (i.id) FROM Issuer i WHERE (i.issuerBrandName.id =:brandNameId)))"
	    		+ " AND (0 =:issuerId 					OR 	p.issuer.id =:issuerId )"
	    		+ " AND ('ALL' =:enableForOffExchange 	OR 	p.enableForOffExchFlow =:enableForOffExchange)"
	    		)
	 Long checkCsrVariationsOfPlans( @Param("insuranceType") String insuranceType, @Param("selectedCsrVariation") List<String> selectedCsrVariation, @Param("state") String state, 
	    		@Param("issuerPlanNumber") String issuerPlanNumber, @Param("planMarket") String planMarket, @Param("filterStatus") String filterStatus, @Param("exchangeType") String exchangeType,
	    		@Param("nonCommissionFlag") String  nonCommissionFlag, @Param("planYear") int applicableYear, @Param("tenantId") long tenantId,
	    		@Param("planLevel") String planLevel, @Param("brandNameId") int brandNameId, @Param("issuerId") int issuerId,
	    		@Param("enableForOffExchange") String enableForOffExchange, @Param("planName") String planName);
	   
	    
		@Modifying
	    @Transactional
	    @Query("UPDATE Plan p SET p.enableForOffExchFlow = :csrOffExchangeFlag, p.lastUpdatedBy =:lastUpdatedBy, p.lastUpdateTimestamp = CURRENT_TIMESTAMP() "
	    		+ " WHERE ( isDeleted='N' AND p.insuranceType =:insuranceType) "
	    		+ " AND (substring(p.issuerPlanNumber,15,2) = '01')"
	    		+ " AND ('ALL' 	=:state 				OR 	p.state =:state) "
	    		+ " AND ('ALL' 	=:planName 				OR 	UPPER(p.name) LIKE  '%' || UPPER(:planName) || '%') "
	    		+ " AND ('ALL'  =:issuerPlanNumber 		OR	UPPER(p.issuerPlanNumber) LIKE UPPER(:issuerPlanNumber) || '%'		) "
	    		+ " AND ('ALL' 	=:planMarket    		OR  p.market =:planMarket			) "
	    		+ " AND ('ALL' 	=:filterStatus 			OR 	p.status =:filterStatus			) "    		
	    		+ " AND ('ALL' 	=:exchangeType    		OR  p.exchangeType =:exchangeType	)"
	    		+ " AND ('ALL' 	=:nonCommissionFlag 	OR  p.nonCommissionFlag =:nonCommissionFlag)"
	    		+ " AND (0 		=:planYear    			OR  p.applicableYear =:planYear )"
	    		+ " AND (0L		=:tenantId    			OR  p.id 		IN (SELECT DISTINCT (tp.plan.id) 	FROM TenantPlan tp 	WHERE tp.tenant.id 	=:tenantId	) ) "
				+ " AND ('ALL' 	=:planLevel     		OR  p.id 		IN (SELECT DISTINCT (ph.plan.id) 	FROM PlanHealth ph 	WHERE ph.planLevel  =:planLevel	) )"
	    		+ " AND (0 		=:brandNameId 			OR 	p.issuer.id IN (SELECT DISTINCT (i.id) 			FROM Issuer i 		WHERE (i.issuerBrandName.id =:brandNameId)) )"
	    		+ " AND (0 		=:issuerId 				OR 	p.issuer.id =:issuerId )"
	    		+ " AND ('ALL' =:enableForOffExchange 	OR 	p.enableForOffExchFlow =:enableForOffExchange)")
	    int bulkEnableOnExchangePlansForOffExchange(@Param("csrOffExchangeFlag") String csrOffExchangeFlag, @Param("lastUpdatedBy") int lastUpdatedBy, @Param("insuranceType") String insuranceType, 
	    		@Param("state") String state, @Param("issuerPlanNumber") String issuerPlanNumber,     		
	    		@Param("planMarket") String planMarket, @Param("filterStatus") String filterStatus, @Param("exchangeType") String exchangeType,
	    		@Param("nonCommissionFlag") String  nonCommissionFlag, @Param("planYear") int applicableYear, @Param("tenantId") Long tenantId, 
	    		@Param("planLevel") String planLevel, @Param("brandNameId") int brandNameId, @Param("issuerId") int issuerId,
	    		@Param("enableForOffExchange") String enableForOffExchange, @Param("planName") String planName);
		
		
		@Modifying
	    @Transactional
	    @Query("UPDATE Plan as p SET status='DECERTIFIED' WHERE p.issuer.id=:issuerId AND isDeleted='N'")
	    void decertifyPlans(@Param("issuerId") int issuerId);

	@Query("select new com.getinsured.hix.dto.planmgmt.PlanResponse(plan.id, plan.issuer.id, plan.issuer.hiosIssuerId, plan.issuerPlanNumber, plan.name, "
			+ " plan.issuer.name, plan.status, plan.insuranceType, plan.planHealth.planLevel, plan.applicableYear, plan.issuer.federalEin, "
			+ " plan.planHealth.costSharing, plan.ehbPremiumFraction, '', plan.issuer.logoURL) from Plan as plan "
			+ " where plan.id in (:planIds) and plan.insuranceType = 'HEALTH' and isDeleted='N' ")
	List<com.getinsured.hix.dto.planmgmt.PlanResponse> getHealthPlansByIdIn(@Param("planIds") Set<Integer> idList);

	@Query("select new com.getinsured.hix.dto.planmgmt.PlanResponse(plan.id, plan.issuer.id, plan.issuer.hiosIssuerId, plan.issuerPlanNumber, plan.name, "
			+ " plan.issuer.name, plan.status, plan.insuranceType, plan.planDental.planLevel, plan.applicableYear, plan.issuer.federalEin, "
			+ " plan.planDental.costSharing, plan.ehbPremiumFraction, plan.planDental.ehbApptForPediatricDental, plan.issuer.logoURL) from Plan as plan "
			+ " where plan.id in (:planIds) and plan.insuranceType = 'DENTAL' and isDeleted='N' ")
	List<com.getinsured.hix.dto.planmgmt.PlanResponse> getDentalPlansByIdIn(@Param("planIds") Set<Integer> idList);
}
