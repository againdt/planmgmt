package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.StatePlanAvailability;

public interface IStatePlanAvailabilityRepository extends JpaRepository<StatePlanAvailability, Integer>, RevisionRepository<StatePlanAvailability, Integer, Integer>{
	
	@Query("SELECT sa FROM StatePlanAvailability sa WHERE sa.state= :state")
	StatePlanAvailability getStatePlanAvailabilityForZipAndState(@Param("state") String state);
	
	@Query("SELECT sa FROM StatePlanAvailability sa order by state")
	List<StatePlanAvailability> getStatePlanAvailability();
	
}
