package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Region;

public interface IRegionRepository extends JpaRepository<Region, Integer> {
	
	@Query("SELECT rg FROM Region rg WHERE (rg.regionName=:regionName) order by rg.county, rg.zip")
    List<Region> findByRegionName(@Param("regionName") String regionName);
    
	@Query("SELECT rg FROM Region rg WHERE (rg.county=:county) order by rg.regionName, rg.zip")
    List<Region> findByCounty(@Param("county") String county);
	
    Region findByZip(String zip);
    
    @Query("SELECT count(rg.id) FROM Region rg WHERE UPPER(rg.county)=UPPER(:county)")
    long getZipCountForCounty(@Param("county") String county);
    
    @Query("SELECT count(rg.id) FROM Region rg WHERE UPPER(rg.county)=UPPER(:county) and rg.zip=:zip")
    long getZipCountForCountyAndZip(@Param("county") String county, @Param("zip") String zip);
    
    @Query("SELECT count(rg.id) FROM Region rg WHERE rg.zip=:zip")
    long getZipCountForZip(@Param("zip") String zip);
    
    @Query("SELECT count(rg.id) FROM Region rg WHERE rg.zip=:zip AND rg.countyFips=:countyCode")
    long getCountForZipAndCountyCode(@Param("zip") String zip, @Param("countyCode") String countyCode);
}
