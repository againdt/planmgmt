//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.14 at 11:56:49 AM PST 
//


package com.getinsured.hix.planmgmt.phix.ws.quotit;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IfpPlanEligibility.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IfpPlanEligibility">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FullEligibilityOnly"/>
 *     &lt;enumeration value="ShortPlansOK"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IfpPlanEligibility")
@XmlEnum
public enum IfpPlanEligibility {

    @XmlEnumValue("FullEligibilityOnly")
    FULL_ELIGIBILITY_ONLY("FullEligibilityOnly"),
    @XmlEnumValue("ShortPlansOK")
    SHORT_PLANS_OK("ShortPlansOK");
    private final String value;

    IfpPlanEligibility(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IfpPlanEligibility fromValue(String v) {
        for (IfpPlanEligibility c: IfpPlanEligibility.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
