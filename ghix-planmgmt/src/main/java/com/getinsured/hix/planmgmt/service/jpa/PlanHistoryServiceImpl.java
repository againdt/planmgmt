package com.getinsured.hix.planmgmt.service.jpa;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
//import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.PlanComponentHistoryDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.planmgmt.history.jpa.PlanChildHistoryRendererHelper;
import com.getinsured.hix.planmgmt.history.jpa.PlanHistoryRendererHelper;
import com.getinsured.hix.planmgmt.repository.IPlanDentalRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthRepository;
//import com.getinsured.hix.planmgmt.repository.IPlanDentalRepository;
//import com.getinsured.hix.planmgmt.repository.IPlanHealthRepository;
import com.getinsured.hix.planmgmt.repository.IPlanRepository;
import com.getinsured.hix.planmgmt.service.PlanHistoryService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;
import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
//import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
//import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;


@Service("planHistoryService")
@Repository
@Transactional
public class PlanHistoryServiceImpl implements PlanHistoryService, ApplicationContextAware {
	
	@Autowired
	private ObjectFactory<DisplayAuditService> objectFactory;
	@Autowired
	private GIExceptionHandler giExceptionHandler; 
	@Autowired
	//@Qualifier("planHistoryRendererImpl")
	private PlanHistoryRendererHelper planHistoryRendererHelper;
	@Autowired
	private HistoryRendererService planChildHistoryRendererHelper;
	@Autowired
	private IPlanRepository iPlanRepository;
	@Autowired
	private IPlanHealthRepository iPlanHealthRepository;
	@Autowired
	private IPlanDentalRepository iPlanDentalRepository;
	@PersistenceUnit
	private EntityManagerFactory emf;
	private ApplicationContext appContext;
	//@Autowired 
	//private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	private static final Logger LOGGER = Logger.getLogger(PlanHistoryServiceImpl.class);
	
	// implementation of interface getPlanCertificationHistory
	public List<Map<String, Object>> getPlanCertificationHistory(Integer planId){
		List<Map<String, Object>> historyData = null;
		
		try{
			Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
			
			requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, PlanMgmtConstants.DATE_STRING);
		 	requiredFieldsMap.put(PlanMgmtConstants.STATUS_FILE_COL, PlanMgmtConstants.FILE_STRING);
		 	requiredFieldsMap.put(PlanMgmtConstants.UPDATED_BY_COL, PlanMgmtConstants.USER_STRING);
		 	requiredFieldsMap.put(PlanMgmtConstants.STATUS_COL, "STATUS");
		 	requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");
	
			List<String> compareCols = new ArrayList<String>();
			compareCols.add(PlanMgmtConstants.STATUS_COL);
			
			List<Integer> displayCols = new ArrayList<Integer>();
	
			displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		 	displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		 	displayCols.add(PlanMgmtConstants.STATUS_INT);
		 	displayCols.add(PlanMgmtConstants.STATUS_FILE_INT);
		 	displayCols.add(PlanMgmtConstants.COMMENT_INT);
			
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planData = service.findRevisions(iPlanRepository, PlanMgmtConstants.PLAN_MODEL_NAME, requiredFieldsMap, planId);
			List<Map<String, String>> combinedPlanData = new ArrayList<Map<String, String>>(); 
			combinedPlanData.addAll(planData);
			//HIX-108751: Add latest plan data, processData will remove duplicates if any
			Plan plan = iPlanRepository.findOne(planId);
			if(null != plan) {
				Map<String, String> planCertStatus = new HashMap<String, String>();
				planCertStatus.put(PlanMgmtConstants.UPDATED_DATE_COL, DateUtil.dateToString(plan.getLastUpdateTimestamp(), PlanMgmtConstants.TIMESTAMP_PATTERN1));
				planCertStatus.put(PlanMgmtConstants.STATUS_FILE_COL, plan.getSupportFile());
				planCertStatus.put(PlanMgmtConstants.UPDATED_BY_COL, "" + plan.getLastUpdatedBy());
				planCertStatus.put(PlanMgmtConstants.STATUS_COL, plan.getStatus());
				planCertStatus.put(PlanMgmtConstants.COMMENT_COL, "");
				combinedPlanData.add(planCertStatus);
			}
			historyData = planHistoryRendererHelper.processHistoryData(combinedPlanData, compareCols, displayCols);			
		}
		catch(Exception ex){
			LOGGER.error("Error inside getPlanCertificationHistory() ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Error inside getPlanCertificationHistory()", ex);
		}
		
		return historyData;
	}
	
	
	// implementation of interface getPlanHistory
	public List<Map<String, Object>> getPlanHistory(Integer planId){
		List<Map<String, Object>> historyData = null;
		
		try{					
			
			Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
			
			requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, PlanMgmtConstants.DATE_STRING);
		 	requiredFieldsMap.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, "Enroll");
		 	requiredFieldsMap.put(PlanMgmtConstants.STATUS_COL, "STATUS");
		 	requiredFieldsMap.put(PlanMgmtConstants.UPDATED_BY_COL, PlanMgmtConstants.USER_STRING);
		 	requiredFieldsMap.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, PlanMgmtConstants.EFF_DATE);
		 	requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");
		 	requiredFieldsMap.put(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL, "");
		 	
			List<String> compareCols = new ArrayList<String>();		
			compareCols.add(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
		 	compareCols.add(PlanMgmtConstants.STATUS_COL);
		 	compareCols.add(PlanMgmtConstants.EFFECTIVE_DATE_COL);
		 	compareCols.add(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL);
		 	
			List<Integer> displayCols = new ArrayList<Integer>();
			displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
			displayCols.add(PlanMgmtConstants.USER_NAME_INT);
			displayCols.add(PlanMgmtConstants.FN_ENROLLMENT_AVAIL_VAL_INT);
			displayCols.add(PlanMgmtConstants.FN_STATUS_VAL_INT);
			displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_INT);
			displayCols.add(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_VAL_INT);
			displayCols.add(PlanMgmtConstants.COMMENT_INT);
			displayCols.add(PlanMgmtConstants.FN_EFFECTIVE_START_DATE_VAL_INT);
			
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planData = service.findRevisions(iPlanRepository, PlanMgmtConstants.PLAN_MODEL_NAME, requiredFieldsMap, planId);
			List<Map<String, String>> combinedPlanData = new ArrayList<Map<String, String>>(); 
			combinedPlanData.addAll(planData);
			//HIX-108751: Add latest plan data, processData will remove duplicates if any
			Plan plan = iPlanRepository.findOne(planId);
			if(null != plan) {
				Map<String, String> planCertStatus = new HashMap<String, String>();
				String effectiveDate = null;
				if(null != plan.getEnrollmentAvailEffDate()) {
					effectiveDate = DateUtil.dateToString(plan.getEnrollmentAvailEffDate(), PlanMgmtConstants.TIMESTAMP_PATTERN1); 
				} else {
					effectiveDate = "";
				}
				planCertStatus.put(PlanMgmtConstants.UPDATED_DATE_COL, DateUtil.dateToString(plan.getLastUpdateTimestamp(), PlanMgmtConstants.TIMESTAMP_PATTERN1));
				planCertStatus.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, plan.getEnrollmentAvail());
				planCertStatus.put(PlanMgmtConstants.STATUS_COL,plan.getStatus());
				planCertStatus.put(PlanMgmtConstants.UPDATED_BY_COL, "" + plan.getLastUpdatedBy());
				planCertStatus.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, effectiveDate);
				planCertStatus.put(PlanMgmtConstants.COMMENT_COL, "");
				planCertStatus.put(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL, plan.getIssuerVerificationStatus());
				combinedPlanData.add(planCertStatus);
			}
			historyData = planHistoryRendererHelper.processHistoryData(combinedPlanData, compareCols, displayCols);			
		}
		catch(Exception ex){
			LOGGER.error("Error inside getPlanHistory() ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Error inside getPlanHistory()", ex);
		}
		
		return historyData;
	}
	
	
	@Override
	public PlanComponentHistoryDTO getPlanBenefitsHistoryByRoleAndPlanId(String roleType, int planId) {
		PlanComponentHistoryDTO planBenefitsHistoryDTO = new PlanComponentHistoryDTO();
		int errorCode = 0;
		String errorMsg = null;
		if(planId > 0 && StringUtils.isNotBlank(roleType)) {
			List<Map<String, Object>> planBrochureData = getPlanBrochureHistory(planId);
			Plan plan = iPlanRepository.findOne(planId);
			List<Map<String, Object>> planData = new ArrayList<Map<String, Object>>();
			String planInsuranceType = plan.getInsuranceType();
			if (planInsuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())) {
				//Integer planHealthId = iPlanHealthRepository.getPlanHealthId(planId);
				planData = getHealthPlanBenefitAndSbcBrochure(plan.getPlanHealth().getId(), planId, roleType);
			} else if (planInsuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
				//Integer planDentalId = iPlanDentalRepository.getPlanDentalId(planId);
				planData = getDentalPlanBenefitsHistory(plan.getPlanDental().getId(), planId, roleType);
			}
			planBenefitsHistoryDTO.setHistoryList( mergeLists(planData, planBrochureData));
			planBenefitsHistoryDTO.setHiosIssuerId(plan.getIssuer().getHiosIssuerId());
			planBenefitsHistoryDTO.setInsuranceType(plan.getInsuranceType());
			planBenefitsHistoryDTO.setIssuerId(plan.getIssuer().getId());
			planBenefitsHistoryDTO.setIssuerLogoUrl(plan.getIssuer().getLogoURL());
			planBenefitsHistoryDTO.setIssuerName(plan.getIssuer().getName());
			planBenefitsHistoryDTO.setIssuerPlanNumber(plan.getIssuerPlanNumber());
			planBenefitsHistoryDTO.setPlanId(plan.getId());
			planBenefitsHistoryDTO.setPlanName(plan.getName());
			planBenefitsHistoryDTO.setPlanStatus(plan.getStatus());
			planBenefitsHistoryDTO.setPlanYear(plan.getApplicableYear());
			planBenefitsHistoryDTO.setPlanStartDate(plan.getStartDate());
			planBenefitsHistoryDTO.setPlanEndDate(plan.getEndDate());
			planBenefitsHistoryDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
			return planBenefitsHistoryDTO;

		} else {
			errorCode = PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode();
			errorMsg = PlanMgmtConstants.INVALID_REQUEST;
		}
		planBenefitsHistoryDTO.setErrMsg(errorMsg);
		planBenefitsHistoryDTO.setErrCode(errorCode);
		planBenefitsHistoryDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
		return planBenefitsHistoryDTO;
	}

	/**
	 * Method is used to get Plan Enrollment History Data By Plan-ID.
	 */
	@Override
	public PlanComponentHistoryDTO getPlanEnrollmentHistoryByPlanId(int planId) {

		PlanComponentHistoryDTO planEnrollmentHistoryDTO = new PlanComponentHistoryDTO();

		if (planId <= 0) {
			planEnrollmentHistoryDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			planEnrollmentHistoryDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planEnrollmentHistoryDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			return planEnrollmentHistoryDTO;
		}

		Plan plan = iPlanRepository.findOne(planId);
		// Get Plan Enrollment History Data
		planEnrollmentHistoryDTO.setHistoryList(getPlanEnrollmentHistory(planId, plan));
		planEnrollmentHistoryDTO.setHiosIssuerId(plan.getIssuer().getHiosIssuerId());
		planEnrollmentHistoryDTO.setInsuranceType(plan.getInsuranceType());
		planEnrollmentHistoryDTO.setIssuerId(plan.getIssuer().getId());
		planEnrollmentHistoryDTO.setIssuerLogoUrl(plan.getIssuer().getLogoURL());
		planEnrollmentHistoryDTO.setIssuerName(plan.getIssuer().getName());
		planEnrollmentHistoryDTO.setIssuerPlanNumber(plan.getIssuerPlanNumber());
		planEnrollmentHistoryDTO.setPlanId(plan.getId());
		planEnrollmentHistoryDTO.setPlanName(plan.getName());
		planEnrollmentHistoryDTO.setPlanStatus(plan.getStatus());
		planEnrollmentHistoryDTO.setPlanYear(plan.getApplicableYear());
		planEnrollmentHistoryDTO.setPlanStartDate(plan.getStartDate());
		planEnrollmentHistoryDTO.setPlanEndDate(plan.getEndDate());
		planEnrollmentHistoryDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
		return planEnrollmentHistoryDTO;
	}

	private List<Map<String, Object>> getPlanEnrollmentHistory(Integer planId, Plan plan) {

		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, PlanMgmtConstants.DATE_STRING);
	 	requiredFieldsMap.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, PlanMgmtConstants.FILE_STRING);
	 	requiredFieldsMap.put(PlanMgmtConstants.UPDATED_BY_COL, PlanMgmtConstants.USER_STRING);
	 	requiredFieldsMap.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, PlanMgmtConstants.EFF_DATE);
	 	requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");

	 	List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
	 	displayCols.add(PlanMgmtConstants.USER_NAME_INT);
	 	displayCols.add(PlanMgmtConstants.EFFECTIVE_DATE_INT);
	 	displayCols.add(PlanMgmtConstants.ENROLLMENT_AVAIL_INT);
	 	displayCols.add(PlanMgmtConstants.COMMENT_INT);

		List<Map<String, Object>> data = null;
		DisplayAuditService service = objectFactory.getObject();
		service.setApplicationContext(appContext);
		List<Map<String, String>> planData = service.findRevisions(iPlanRepository, PlanMgmtConstants.PLAN_MODEL_NAME, requiredFieldsMap, planId);
		List<Map<String, String>> combinedPlanData = new ArrayList<Map<String, String>>(); 
		combinedPlanData.addAll(planData);
		//HIX-108751: Add latest plan data, processData will remove duplicates if any
		if(null != plan) {
			Map<String, String> planCertStatus = new HashMap<String, String>();
			String effectiveDate = null;
			if(null != plan.getEnrollmentAvailEffDate()) {
				effectiveDate = DateUtil.dateToString(plan.getEnrollmentAvailEffDate(), PlanMgmtConstants.TIMESTAMP_PATTERN1); 
			} else {
				effectiveDate = "";
			}
			planCertStatus.put(PlanMgmtConstants.UPDATED_DATE_COL, DateUtil.dateToString(plan.getLastUpdateTimestamp(), PlanMgmtConstants.TIMESTAMP_PATTERN1));
			planCertStatus.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, plan.getEnrollmentAvail());
			planCertStatus.put(PlanMgmtConstants.UPDATED_BY_COL, "" + plan.getLastUpdatedBy());
			planCertStatus.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, effectiveDate);
			planCertStatus.put(PlanMgmtConstants.COMMENT_COL, "");
			combinedPlanData.add(planCertStatus);
		}
		data = planHistoryRendererHelper.processHistoryData(combinedPlanData, compareCols, displayCols);			
		return data;
	}

	private List<Map<String, Object>> getPlanBrochureHistory(Integer planId) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		/*requiredFieldsMap.put(UPDATED_DATE_COL, PlanMgmtConstants.DATE_STRING);*/
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, PlanMgmtConstants.DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.BROCHURE_COL, "Brochure");
		requiredFieldsMap.put(PlanMgmtConstants.BROCHURE_UCM_ID_CAL, "Brochure");
		/*requiredFieldsMap.put(UPDATED_BY_COL, PlanMgmtConstants.USER_STRING);*/
		requiredFieldsMap.put(PlanMgmtConstants.UPDATED_BY_COL, PlanMgmtConstants.USER_STRING);

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(PlanMgmtConstants.BROCHURE_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.FN_BROCHURE_VAL_INT);
		displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		List<Map<String, Object>> data = null;
		DisplayAuditService service = objectFactory.getObject();
		service.setApplicationContext(appContext);
		//List<Map<String, String>> planHealthData = service.findRevisions(REPOSITORY_NAME, MODEL_NAME, requiredFieldsMap, planId);
		List<Map<String, String>> planHealthData = service.findRevisions(iPlanRepository, PlanMgmtConstants.PLAN_MODEL_NAME, requiredFieldsMap, planId);
		data = planHistoryRendererHelper.processHistoryData(planHealthData, compareCols, displayCols);
		return data;
	}
	
	private List<Map<String, Object>> getHealthPlanBenefitAndSbcBrochure(Integer planHealthId, Integer planId, String role) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		try {
			/*requiredFieldsMap.put(UPDATE_DATE_COL, PlanMgmtConstants.DATE_STRING);
			requiredFieldsMap.put(USER_NAME_ALIAS, PlanMgmtConstants.USER_STRING);*/
			requiredFieldsMap.put(PlanMgmtConstants.UPDATE_DATE_COL, PlanMgmtConstants.DATE_STRING);
			requiredFieldsMap.put(PlanMgmtConstants.USER_NAME_ALIAS, PlanMgmtConstants.USER_STRING);
			requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");
			requiredFieldsMap.put(PlanMgmtConstants.BENEFITFILE_COL, PlanMgmtConstants.BENEFIT_FILE_CAPTION);
			requiredFieldsMap.put(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_COL, "");
			requiredFieldsMap.put(PlanMgmtConstants.PLAN_SBC_COL, PlanMgmtConstants.PLAN_SBC_FILE);

			List<String> compareCols = new ArrayList<String>();

			/*compareCols.add(PLAN_SBC_COL);
			compareCols.add(BENEFITFILE_COL);*/
			compareCols.add(PlanMgmtConstants.PLAN_SBC_COL);
	 	 	compareCols.add(PlanMgmtConstants.BENEFITFILE_COL);
			compareCols.add(planId.toString());
			compareCols.add(role);

			List<Integer> displayCols = new ArrayList<Integer>();

			displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
			displayCols.add(PlanMgmtConstants.USER_NAME_INT);
			displayCols.add(PlanMgmtConstants.FN_BENEFITFILE_VAL_INT);
			displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_RATEHISTORY_INT);
			displayCols.add(PlanMgmtConstants.COMMENT_INT);
			displayCols.add(PlanMgmtConstants.EFFECTIVE_DATE_COMBINED_INT);
			displayCols.add(PlanMgmtConstants.PLAN_SBC_VAL_INT);
			List<Map<String, Object>> data = null;
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planHealthData = service.findRevisions(iPlanHealthRepository, PlanChildHistoryRendererHelper.MODEL_NAME,
					requiredFieldsMap, planHealthId);
			data = planChildHistoryRendererHelper.processData(planHealthData, compareCols, displayCols);
			return data;
		} catch (Exception e) {
			LOGGER.error("Error while loading plan health history : " , e);
		}
		return null;
	}	
	
	private List<Map<String, Object>> getDentalPlanBenefitsHistory(Integer dentalId, Integer planId, String role) {
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		/*requiredFieldsMap.put(UPDATE_DATE_COL, PlanMgmtConstants.DATE_STRING);*/
		requiredFieldsMap.put(PlanMgmtConstants.UPDATE_DATE_COL, PlanMgmtConstants.DATE_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_COL, "Benefit date");
		/*requiredFieldsMap.put(USER_NAME_ALIAS, PlanMgmtConstants.USER_STRING);*/
		requiredFieldsMap.put(PlanMgmtConstants.USER_NAME_ALIAS, PlanMgmtConstants.USER_STRING);
		requiredFieldsMap.put(PlanMgmtConstants.BENEFITFILE_COL, PlanMgmtConstants.BENEFIT_FILE_CAPTION);
		requiredFieldsMap.put(PlanMgmtConstants.COMMENT_COL, "");
		requiredFieldsMap.put(PlanMgmtConstants.PLAN_SBC_COL, PlanMgmtConstants.PLAN_SBC_FILE);

		List<String> compareCols = new ArrayList<String>();
		/*compareCols.add(BENEFITFILE_COL);*/
		compareCols.add(PlanMgmtConstants.BENEFITFILE_COL);
		compareCols.add(PlanMgmtConstants.PLAN_SBC_COL);
		compareCols.add(planId.toString());
		compareCols.add(role);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
		displayCols.add(PlanMgmtConstants.USER_NAME_INT);
		displayCols.add(PlanMgmtConstants.FN_BENEFITFILE_VAL_INT);
		displayCols.add(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_INT);
		displayCols.add(PlanMgmtConstants.FN_DISPLAYVAL_RATEHISTORY_INT);
		displayCols.add(PlanMgmtConstants.COMMENT_INT);
		displayCols.add(PlanMgmtConstants.PLAN_SBC_VAL_INT);
		List<Map<String, Object>> data = null;
		try {
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planDentalData = service.findRevisions(iPlanDentalRepository,
					PlanChildHistoryRendererHelper.DENTAL_MODEL_NAME, requiredFieldsMap, dentalId);
			data = planChildHistoryRendererHelper.processData(planDentalData, compareCols, displayCols);
		} catch (Exception e) {
			LOGGER.error("Error while loading Dental plan benefits history : " ,e);
		}
		return data;
	}
	
	private List<Map<String, Object>> mergeLists(List<Map<String, Object>> firstList, List<Map<String, Object>> secondList) {
		List<Map<String, Object>> mergedList = new ArrayList<Map<String, Object>>();
		int i = 0;
		int j = 0;
		Map<String, Object> firstMap = null;
		Map<String, Object> secondMap = null;
		Object firstVal = null;
		Object secondVal = null;
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(PlanMgmtConstants.TIMESTAMP_PATTERN1);
		if (firstList != null && secondList != null) {
			while (i < firstList.size() && j < secondList.size()) {
				firstMap = firstList.get(i);
				secondMap = secondList.get(j);
				firstVal = firstMap.get(PlanMgmtConstants.UPDATED_DATE_COL);
				secondVal = secondMap.get(PlanMgmtConstants.UPDATED_DATE_COL);
				if (((Date) firstVal).after(((Date) secondVal))) {
					mergedList.add(firstMap);
					i++;
				} else {
					mergedList.add(secondMap);
					j++;
				}
			}
		}
		if (firstList != null) {
			while (i < firstList.size()) {
				mergedList.add(firstList.get(i));
				i++;
			}
		}
		if (secondList != null) {
			while (j < secondList.size()) {
				mergedList.add(secondList.get(j));
				j++;
			}
		}
		return mergedList;
	}


	@Override
	public List<Map<String, Object>> getIssuerPlanHistory(Integer planId) {
		List<Map<String, Object>> historyData = null;
		
		try{					
			
			Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
			
			requiredFieldsMap.put(PlanMgmtConstants.UPDATED_DATE_COL, PlanMgmtConstants.DATE_STRING);
			requiredFieldsMap.put(PlanMgmtConstants.STATUS_COL, "STATUS");
			requiredFieldsMap.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, "ENROLMENT");
			requiredFieldsMap.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, PlanMgmtConstants.EFF_DATE);
			requiredFieldsMap.put(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL, "");

			List<String> compareCols = new ArrayList<String>();		
			compareCols.add(PlanMgmtConstants.STATUS_COL);
			compareCols.add(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
			compareCols.add(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL);
		 	
			List<Integer> displayCols = new ArrayList<Integer>();
			displayCols.add(PlanMgmtConstants.UPDATE_DATE_INT);
			displayCols.add(PlanMgmtConstants.STATUS_INT);
			displayCols.add(PlanMgmtConstants.ENROLLMENT_AVAIL_INT);
			displayCols.add(PlanMgmtConstants.EFFECTIVE_DATE_INT);
			displayCols.add(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_VAL_INT);

			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(appContext);
			List<Map<String, String>> planData = service.findRevisions(iPlanRepository, PlanMgmtConstants.PLAN_MODEL_NAME, requiredFieldsMap, planId);
			List<Map<String, String>> combinedPlanData = new ArrayList<Map<String, String>>(); 
			combinedPlanData.addAll(planData);
			//HIX-108751: Add latest plan data, processData will remove duplicates if any
			Plan plan = iPlanRepository.findOne(planId);
			if(null != plan) {
				Map<String, String> planCertStatus = new HashMap<String, String>();
				String effectiveDate = null;
				if(null != plan.getEnrollmentAvailEffDate()) {
					effectiveDate = DateUtil.dateToString(plan.getEnrollmentAvailEffDate(), PlanMgmtConstants.TIMESTAMP_PATTERN1); 
				} else {
					effectiveDate = "";
				}
				planCertStatus.put(PlanMgmtConstants.UPDATED_DATE_COL, DateUtil.dateToString(plan.getLastUpdateTimestamp(), PlanMgmtConstants.TIMESTAMP_PATTERN1));
				planCertStatus.put(PlanMgmtConstants.STATUS_COL,plan.getStatus());
				planCertStatus.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, plan.getEnrollmentAvail());
				planCertStatus.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, effectiveDate);
				planCertStatus.put(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL, plan.getIssuerVerificationStatus());
				combinedPlanData.add(planCertStatus);
			}
			historyData = planHistoryRendererHelper.processHistoryData(combinedPlanData, compareCols, displayCols);			
		}
		catch(Exception ex){
			LOGGER.error("Error inside getPlanHistory() ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Error inside getPlanHistory()", ex);
		}
		
		return historyData;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.appContext=applicationContext;
		
	}

}
