//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.03.03 at 04:50:02 PM MST 
//


package com.getinsured.hix.planmgmt.phix.ws.uhc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuoteRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuoteRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ExtensionData" type="{http://www.goldenrulehealthws.com/IWebQuoteService/Latest}ExtensionDataObject" minOccurs="0"/>
 *         &lt;element name="Coverage" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="County" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryInsured" type="{http://www.goldenrulehealthws.com/IWebQuoteService/Latest}PrimaryInsuredType" minOccurs="0"/>
 *         &lt;element name="SpouseInsured" type="{http://www.goldenrulehealthws.com/IWebQuoteService/Latest}SpouseInsuredType" minOccurs="0"/>
 *         &lt;element name="Children" type="{http://www.goldenrulehealthws.com/IWebQuoteService/Latest}ArrayOfChild" minOccurs="0"/>
 *         &lt;element name="PaymentMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BrokerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Arrangement" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuoteDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Generation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PlanFilter" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="QuotePlanFilter" type="{http://www.goldenrulehealthws.com/IWebQuoteService/Latest}ArrayOfString" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuoteRequestType", propOrder = {
    "extensionData",
    "coverage",
    "state",
    "zipCode",
    "county",
    "primaryInsured",
    "spouseInsured",
    "children",
    "paymentMode",
    "effectiveDate",
    "brokerID",
    "arrangement",
    "quoteDate",
    "generation",
    "planFilter",
    "quotePlanFilter"
})
public class QuoteRequestType {

    @XmlElement(name = "ExtensionData")
    protected ExtensionDataObject extensionData;
    @XmlElement(name = "Coverage", required = true, type = Short.class, nillable = true)
    @XmlSchemaType(name = "unsignedByte")
    protected Short coverage;
    @XmlElement(name = "State")
    protected String state;
    @XmlElement(name = "ZipCode")
    protected String zipCode;
    @XmlElement(name = "County")
    protected String county;
    @XmlElement(name = "PrimaryInsured")
    protected PrimaryInsuredType primaryInsured;
    @XmlElement(name = "SpouseInsured")
    protected SpouseInsuredType spouseInsured;
    @XmlElement(name = "Children")
    protected ArrayOfChild children;
    @XmlElement(name = "PaymentMode")
    protected String paymentMode;
    @XmlElement(name = "EffectiveDate")
    protected String effectiveDate;
    @XmlElement(name = "BrokerID")
    protected String brokerID;
    @XmlElement(name = "Arrangement")
    protected String arrangement;
    @XmlElement(name = "QuoteDate")
    protected String quoteDate;
    @XmlElement(name = "Generation")
    protected String generation;
    @XmlElement(name = "PlanFilter")
    protected String planFilter;
    @XmlElement(name = "QuotePlanFilter")
    protected ArrayOfString quotePlanFilter;

    /**
     * Gets the value of the extensionData property.
     * 
     * @return
     *     possible object is
     *     {@link ExtensionDataObject }
     *     
     */
    public ExtensionDataObject getExtensionData() {
        return extensionData;
    }

    /**
     * Sets the value of the extensionData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtensionDataObject }
     *     
     */
    public void setExtensionData(ExtensionDataObject value) {
        this.extensionData = value;
    }

    /**
     * Gets the value of the coverage property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getCoverage() {
        return coverage;
    }

    /**
     * Sets the value of the coverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setCoverage(Short value) {
        this.coverage = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the zipCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     * Sets the value of the zipCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZipCode(String value) {
        this.zipCode = value;
    }

    /**
     * Gets the value of the county property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCounty() {
        return county;
    }

    /**
     * Sets the value of the county property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCounty(String value) {
        this.county = value;
    }

    /**
     * Gets the value of the primaryInsured property.
     * 
     * @return
     *     possible object is
     *     {@link PrimaryInsuredType }
     *     
     */
    public PrimaryInsuredType getPrimaryInsured() {
        return primaryInsured;
    }

    /**
     * Sets the value of the primaryInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrimaryInsuredType }
     *     
     */
    public void setPrimaryInsured(PrimaryInsuredType value) {
        this.primaryInsured = value;
    }

    /**
     * Gets the value of the spouseInsured property.
     * 
     * @return
     *     possible object is
     *     {@link SpouseInsuredType }
     *     
     */
    public SpouseInsuredType getSpouseInsured() {
        return spouseInsured;
    }

    /**
     * Sets the value of the spouseInsured property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpouseInsuredType }
     *     
     */
    public void setSpouseInsured(SpouseInsuredType value) {
        this.spouseInsured = value;
    }

    /**
     * Gets the value of the children property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfChild }
     *     
     */
    public ArrayOfChild getChildren() {
        return children;
    }

    /**
     * Sets the value of the children property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfChild }
     *     
     */
    public void setChildren(ArrayOfChild value) {
        this.children = value;
    }

    /**
     * Gets the value of the paymentMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * Sets the value of the paymentMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMode(String value) {
        this.paymentMode = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEffectiveDate(String value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the brokerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrokerID() {
        return brokerID;
    }

    /**
     * Sets the value of the brokerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrokerID(String value) {
        this.brokerID = value;
    }

    /**
     * Gets the value of the arrangement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArrangement() {
        return arrangement;
    }

    /**
     * Sets the value of the arrangement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArrangement(String value) {
        this.arrangement = value;
    }

    /**
     * Gets the value of the quoteDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuoteDate() {
        return quoteDate;
    }

    /**
     * Sets the value of the quoteDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuoteDate(String value) {
        this.quoteDate = value;
    }

    /**
     * Gets the value of the generation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneration() {
        return generation;
    }

    /**
     * Sets the value of the generation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneration(String value) {
        this.generation = value;
    }

    /**
     * Gets the value of the planFilter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanFilter() {
        return planFilter;
    }

    /**
     * Sets the value of the planFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanFilter(String value) {
        this.planFilter = value;
    }

    /**
     * Gets the value of the quotePlanFilter property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getQuotePlanFilter() {
        return quotePlanFilter;
    }

    /**
     * Sets the value of the quotePlanFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setQuotePlanFilter(ArrayOfString value) {
        this.quotePlanFilter = value;
    }

}
