package com.getinsured.hix.planmgmt.util;

public class PlanMgmtParamConstants {

	public static final String PARAM_STATE_CODE = "stateCode";
	public static final String PARAM_INSURANCE_TYPE = "insuranceType";
	public static final String PARAM_TENANT_CODE = "tenantCode";
	public static final String PARAM_APPLICABLE_YEAR = "applicableYear";
	public static final String PARAM_TODAY_DATE = "todayDate";
	public static final String PARAM_CSR_VARIATION = "csrVariation";
	public static final String PARAM_PLAN_LEVEL = "planLevel";
	public static final String PARAM_PLAN_YEAR = "planYear";
	public static final String PARAM_PLAN_NETWORK_ID = "planNetworkId";
	public static final String PARAM_ZIP_CODE = "zipCode";
	public static final String PARAM_COUNTY_CODE = "countyCode";
	public static final String PARAM_PLAN_ID = "planId";
	public static final String PARAM_ISSUER_NAME = "issuerName";
	public static final String PARAM_EHB_COVERED = "ehbCovered";
	public static final String PARAM_EXCHANGE_TYPE = "exchangeType";
	public static final String PARAM_IS_DELETED = "isDeleted";
	public static final String PARAM_STATE_LIST = "stateList";
	public static final String PARAM_USER_ID = "userId";
	public static final String PARAM_ISSUER_LIST = "issuerList";
	public static final String PARAM_EFFECTIVE_DATE = "effectiveDate";
	public static final String PARAM_AGE = "age";
	public static final String PARAM_TOBACCO = "tobacco"; 
	public static final String PARAM_GENDER = "gender";
	public static final String PARAM_DATA_SOURCE = "dataSource";
	public static final String PARAM_PLAN_ID_LIST = "planIdList"; 
	public static final String PARAM_HIOS_PLAN_ID_LIST = "hiosPlanIdList"; 
}
