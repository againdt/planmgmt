//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.14 at 11:56:49 AM PST 
//


package com.getinsured.hix.planmgmt.phix.ws.quotit;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TierLevel" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="OneMonthOutNetworkRetailOfferedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="TierType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ThreeMonthInNetworkMailOfferedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ThreeMonthOutNetworkMailOfferedIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="CostSharingTypeVOs" type="{}ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO" minOccurs="0"/>
 *         &lt;element name="FormularyDrugs" type="{}ArrayOfFormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier.FormularyDrug" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FormularyLookup.Response.Carrier.Plan.PlanFormulary.FormularyTier", propOrder = {
    "tierLevel",
    "oneMonthOutNetworkRetailOfferedIndicator",
    "tierType",
    "threeMonthInNetworkMailOfferedIndicator",
    "threeMonthOutNetworkMailOfferedIndicator",
    "costSharingTypeVOs",
    "formularyDrugs"
})
public class FormularyLookupResponseCarrierPlanPlanFormularyFormularyTier {

    @XmlElement(name = "TierLevel")
    protected Integer tierLevel;
    @XmlElement(name = "OneMonthOutNetworkRetailOfferedIndicator")
    protected boolean oneMonthOutNetworkRetailOfferedIndicator;
    @XmlElementRef(name = "TierType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> tierType;
    @XmlElement(name = "ThreeMonthInNetworkMailOfferedIndicator")
    protected boolean threeMonthInNetworkMailOfferedIndicator;
    @XmlElement(name = "ThreeMonthOutNetworkMailOfferedIndicator")
    protected boolean threeMonthOutNetworkMailOfferedIndicator;
    @XmlElementRef(name = "CostSharingTypeVOs", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> costSharingTypeVOs;
    @XmlElementRef(name = "FormularyDrugs", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> formularyDrugs;

    /**
     * Gets the value of the tierLevel property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTierLevel() {
        return tierLevel;
    }

    /**
     * Sets the value of the tierLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTierLevel(Integer value) {
        this.tierLevel = value;
    }

    /**
     * Gets the value of the oneMonthOutNetworkRetailOfferedIndicator property.
     * 
     */
    public boolean isOneMonthOutNetworkRetailOfferedIndicator() {
        return oneMonthOutNetworkRetailOfferedIndicator;
    }

    /**
     * Sets the value of the oneMonthOutNetworkRetailOfferedIndicator property.
     * 
     */
    public void setOneMonthOutNetworkRetailOfferedIndicator(boolean value) {
        this.oneMonthOutNetworkRetailOfferedIndicator = value;
    }

    /**
     * Gets the value of the tierType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTierType() {
        return tierType;
    }

    /**
     * Sets the value of the tierType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTierType(JAXBElement<String> value) {
        this.tierType = value;
    }

    /**
     * Gets the value of the threeMonthInNetworkMailOfferedIndicator property.
     * 
     */
    public boolean isThreeMonthInNetworkMailOfferedIndicator() {
        return threeMonthInNetworkMailOfferedIndicator;
    }

    /**
     * Sets the value of the threeMonthInNetworkMailOfferedIndicator property.
     * 
     */
    public void setThreeMonthInNetworkMailOfferedIndicator(boolean value) {
        this.threeMonthInNetworkMailOfferedIndicator = value;
    }

    /**
     * Gets the value of the threeMonthOutNetworkMailOfferedIndicator property.
     * 
     */
    public boolean isThreeMonthOutNetworkMailOfferedIndicator() {
        return threeMonthOutNetworkMailOfferedIndicator;
    }

    /**
     * Sets the value of the threeMonthOutNetworkMailOfferedIndicator property.
     * 
     */
    public void setThreeMonthOutNetworkMailOfferedIndicator(boolean value) {
        this.threeMonthOutNetworkMailOfferedIndicator = value;
    }

    /**
     * Gets the value of the costSharingTypeVOs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> getCostSharingTypeVOs() {
        return costSharingTypeVOs;
    }

    /**
     * Sets the value of the costSharingTypeVOs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO }{@code >}
     *     
     */
    public void setCostSharingTypeVOs(JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO> value) {
        this.costSharingTypeVOs = value;
    }

    /**
     * Gets the value of the formularyDrugs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> getFormularyDrugs() {
        return formularyDrugs;
    }

    /**
     * Sets the value of the formularyDrugs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug }{@code >}
     *     
     */
    public void setFormularyDrugs(JAXBElement<ArrayOfFormularyLookupResponseCarrierPlanPlanFormularyFormularyTierFormularyDrug> value) {
        this.formularyDrugs = value;
    }

}
