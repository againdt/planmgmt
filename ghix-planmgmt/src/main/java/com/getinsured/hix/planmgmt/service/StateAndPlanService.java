package com.getinsured.hix.planmgmt.service;

import java.util.List;

import com.getinsured.hix.model.StatePlan;

public interface StateAndPlanService {
	
	int getZipCodeForState(String state);
	
	StatePlan getStatePlanForState(String state);
	
	List<StatePlan> getStatePlan();

}
