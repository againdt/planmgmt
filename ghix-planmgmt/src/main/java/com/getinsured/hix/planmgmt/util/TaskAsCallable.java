package com.getinsured.hix.planmgmt.util;

/*import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;*/
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

//import org.apache.log4j.Logger;


import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.planmgmt.service.PlanCostAndBenefitsService;

public class TaskAsCallable implements Callable<List<PlanRateBenefit>>{

	private List<Integer> planIdList ;
	private  Map<Integer, String> planIdAndHiosIdMap;
	private String insuranceType;
	private String coverageStartDate;
	private PlanCostAndBenefitsService planCostAndBenefitsService;
				   
	//private static final Logger LOGGER = Logger.getLogger(TaskAsCallable.class);
     
    public TaskAsCallable(List<Integer> planIdList, Map<Integer, String> planIdAndHiosIdMap, String insuranceType, String  coverageStartDate, PlanCostAndBenefitsService planCostAndBenefitsService) {
    	this.planIdList = planIdList;
    	this.insuranceType = insuranceType;
    	this.coverageStartDate = coverageStartDate;
    	this.planIdAndHiosIdMap = planIdAndHiosIdMap;
    	this.planCostAndBenefitsService = planCostAndBenefitsService;
    }

    
	@Override
	public List<PlanRateBenefit> call() throws Exception {
		//DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		//Date date = new TSDate();
		//LOGGER.info("here : for plan id "+ this.planIdList.toString() + " Time starts " + dateFormat.format(date) );

		List<PlanRateBenefit> PlanRateBenefitList = this.planCostAndBenefitsService.getIndividualPlanBenefitsAndCost( this.planIdList, this.planIdAndHiosIdMap, this.insuranceType, this.coverageStartDate);
		
		//Date date2 = new TSDate();
		//LOGGER.info("here : for plan id "+ this.planIdList.toString()  + " Time ends " + dateFormat.format(date2) + " size " + PlanRateBenefitList.size()  );
			
		return PlanRateBenefitList;
	}
	  
}
