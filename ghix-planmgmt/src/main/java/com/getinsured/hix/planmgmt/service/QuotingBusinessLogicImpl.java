package com.getinsured.hix.planmgmt.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.ServiceArea;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.querybuilder.QuotingBusinessLogicQueryBuilder;
import com.getinsured.hix.planmgmt.repository.IAMEPremiumFactorRepository;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;


@Service("QuotingBusinessLogic")
@Repository
@Transactional
@DependsOn("dynamicPropertiesUtil")
public class QuotingBusinessLogicImpl implements QuotingBusinessLogic {
	
	@Autowired
	private LookupService lookupService;
	@Autowired
	private ZipCodeService zipCodeService;	
	@Autowired
	private IAMEPremiumFactorRepository iAMEPremiumFactorRepository;
	
	private static final Logger LOGGER = Logger.getLogger(QuotingBusinessLogicImpl.class);
	
	private static HashMap<String, String> familyTierLookupCodeMap =  new HashMap<String, String>();
	
	private static HashMap<String, String> relatioshipIdMap =  new HashMap<String, String>();
	
	private QuotingBusinessLogicImpl(){
		 relatioshipIdMap.put("01", PlanMgmtConstants.SPOUSE);
		 relatioshipIdMap.put("03", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("04", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("05", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("06", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("07", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("08", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("09", PlanMgmtConstants.CHILD);
		 relatioshipIdMap.put("10", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("11", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("12", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("13", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("14", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("15", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("16", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("17", PlanMgmtConstants.CHILD);
		 relatioshipIdMap.put("18", PlanMgmtConstants.SELF);
		 relatioshipIdMap.put("19", PlanMgmtConstants.CHILD);
		 relatioshipIdMap.put("23", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("24", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("25", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("26", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("31", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("38", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("53", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("60", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("D2", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("G8", PlanMgmtConstants.DEPENDENT);
		 relatioshipIdMap.put("G9", PlanMgmtConstants.DEPENDENT);
	}
	
	// process household family data
	@Override
	public List<Map<String, String>> processMemberData(List<Map<String, String>> memberList, String insuranceType)  {
		return processMemberData(memberList, insuranceType, false)  ;
	}

	@Override
	public List<Map<String, String>> processMemberData(List<Map<String, String>> memberList, String insuranceType, boolean keepChildOnly)  {

		List<Map<String, String>> processedMemberData = new ArrayList<Map<String, String>>();
		String considerChildAge = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_CONSIDERCHILDAGE);
		// HIX-65794, use Children age limit configuration value for dental quoting
		if(PlanMgmtConstants.DENTAL.equalsIgnoreCase(insuranceType)){
			considerChildAge = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_CONSIDER_CHILD_AGE_FOR_DENTAL);
		}
		
		String considerMaxChilds = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_CONSIDERMAXCHILDREN);
		
		int smokingAge =  Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_SMOKINGAGE));
		int cMaxAge = 0;
		int childCount = 0;
		int considerMaxChildrens = 0;
		// HIX-63755 : max child quoting limit for dental is to be considered
		if(PlanMgmtConstants.DENTAL.equalsIgnoreCase(insuranceType)){
			considerMaxChilds = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_CONSIDERMAXCHILDREN_QDP);
		}
		
		// Before process member list sort census data on basis of member's age. So we can consider elder children 
		Collections.sort(memberList, new CompareAge());
		cMaxAge = Integer.parseInt(considerChildAge);
		considerMaxChildrens = Integer.parseInt(considerMaxChilds);
				
		for (Map<String, String> member : memberList) {
			if (member.get(PlanMgmtConstants.RELATION) != null) {
				//if member age  < smoking age (i.e 18), set tobacco uses as NO. Because member <19 can't consume tobacco legally 				
				if(Integer.parseInt(member.get(PlanMgmtConstants.AGE)) < smokingAge ){
					if(member.get(PlanMgmtConstants.TOBACCO) != null && member.get(PlanMgmtConstants.TOBACCO).equalsIgnoreCase(PlanMgmtConstants.YES)){
						member.put(PlanMgmtConstants.TOBACCO, PlanMgmtConstants.NO);
					}
					if(member.get(PlanMgmtConstants.TOBACCO) != null && member.get(PlanMgmtConstants.TOBACCO).equalsIgnoreCase(PlanMgmtConstants.Y)){
						member.put(PlanMgmtConstants.TOBACCO, PlanMgmtConstants.N);
					}
				}
				//HIX-63755 : For idaho max child quoting limit does not apply so if 0 then all children should be quoted 
				if(considerMaxChildrens != PlanMgmtConstants.ZERO){
					if (member.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.CHILD)) {
						if (Integer.parseInt(member.get(PlanMgmtConstants.AGE)) < cMaxAge && childCount < considerMaxChildrens) {
							processedMemberData.add(member);
							childCount++;
						} else if (!keepChildOnly && Integer.parseInt(member.get(PlanMgmtConstants.AGE)) >= cMaxAge) {
							processedMemberData.add(member);
						}
					} else {
						if(!keepChildOnly) {
							processedMemberData.add(member);
						}
					}
				}else{
					if(!keepChildOnly || (member.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.CHILD) && Integer.parseInt(member.get(PlanMgmtConstants.AGE)) < cMaxAge)) {
						processedMemberData.add(member);
					}
				}
			}
			
			if(member.get(PlanMgmtConstants.GENDER) != null && !(member.get(PlanMgmtConstants.GENDER).isEmpty()) && member.get(PlanMgmtConstants.GENDER).equalsIgnoreCase(PlanMgmtConstants.MALE)){
				member.put(PlanMgmtConstants.GENDER, PlanMgmtConstants.M);
			}else if(member.get(PlanMgmtConstants.GENDER) != null && !(member.get(PlanMgmtConstants.GENDER).isEmpty()) && member.get(PlanMgmtConstants.GENDER).equalsIgnoreCase(PlanMgmtConstants.FEMALE)){
				member.put(PlanMgmtConstants.GENDER, PlanMgmtConstants.F);
			}
		}
		
		if(LOGGER.isDebugEnabled()){
		LOGGER.debug("processedMemberData size == " + processedMemberData.size());
		}
		
		return processedMemberData;
	}
	
	// compute house hold type
	@Override
	public String getHouseHoldType(List<Map<String, String>> processedMemberData, String insuranceType){
		String houseHoldType = "";
		int childCount = 0;
		int adultCount = 0;
		int totalMemCount = processedMemberData.size();
		int considerChildAge = PlanMgmtConstants.TWENTYONE; // default child age is 21
		 // for dental quoting pull consider child age value from app_config table
		if(PlanMgmtConstants.DENTAL.equalsIgnoreCase(insuranceType)){
			considerChildAge =  Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_AGE_FOR_DENTAL_CHILD_ONLY_PLANS));
		}
		
		for (int i = 0; i < totalMemCount; i++) {
			int memberAge = Integer.valueOf(processedMemberData.get(i).get(PlanMgmtConstants.AGE));
			 if(memberAge < considerChildAge){
				 childCount++;
			 }else if(memberAge >= considerChildAge){
				 adultCount++; 
			 }
		}
		if(childCount == totalMemCount){
			houseHoldType = PlanMgmtConstants.CHILD_ONLY;
		}else if(adultCount == totalMemCount){
			houseHoldType = PlanMgmtConstants.ADULT_ONLY;
		}else{
			houseHoldType = PlanMgmtConstants.ADULT_AND_CHILD;
		}
		return houseHoldType;
	}
	
	
	// compute family tier
	@Override
	public String getFamilyTier(List<Map<String, String>> memberList) {
			Boolean isSelfQuoted = false;
			Boolean isSpouseQuoted = false;
			Boolean isDependentQuoted = false;

			for (int i = 0; i < memberList.size(); i++) {
				if (memberList.get(i).get(PlanMgmtConstants.RELATION) != null) {
					if (memberList.get(i).get(PlanMgmtConstants.RELATION).equalsIgnoreCase("member") || memberList.get(i).get(PlanMgmtConstants.RELATION).equalsIgnoreCase("self")) {
						isSelfQuoted = true;
					}
					if (memberList.get(i).get(PlanMgmtConstants.RELATION).equalsIgnoreCase("spouse")) {
						isSpouseQuoted = true;
					}
					if (memberList.get(i).get(PlanMgmtConstants.RELATION).equalsIgnoreCase("child") || memberList.get(i).get(PlanMgmtConstants.RELATION).equalsIgnoreCase("dependent")) {
						isDependentQuoted = true;
					}
				}
			}

			if (isSelfQuoted && memberList.size() == 1) { // when only self  quoted
				return PlanMgmtConstants.FAMILY_TIER_PRIMARY;
			} else if (isSelfQuoted && isSpouseQuoted && memberList.size() == 2) { // when couple quoted
				return PlanMgmtConstants.FAMILY_TIER_COUPLE;
			} else if (isSelfQuoted && isSpouseQuoted && isDependentQuoted && memberList.size() == PlanMgmtConstants.THREE) { // when couple +  1 dependent quoted
				return PlanMgmtConstants.FAMILY_TIER_COUPLE_ONE_DEPENDENT;
			} else if (isSelfQuoted && isSpouseQuoted && isDependentQuoted && memberList.size() == PlanMgmtConstants.FOUR) { // when couple +  2 dependents quoted
				return PlanMgmtConstants.FAMILY_TIER_COUPLE_TWO_DEPENDENT;
			} else if (isSelfQuoted && isSpouseQuoted && isDependentQuoted && memberList.size() > PlanMgmtConstants.FOUR) { // when couple and more than 2 dependents quoted
				return PlanMgmtConstants.FAMILY_TIER_COUPLE_MANY_DEPENDENT;
			} else if (!isSpouseQuoted && isSelfQuoted && isDependentQuoted && memberList.size() == PlanMgmtConstants.TWO) { // when self and 1 dependent (not spouse) quoted
				return PlanMgmtConstants.FAMILY_TIER_PRIMARY_ONE_DEPENDENT;
			} else if (!isSpouseQuoted && isSelfQuoted && isDependentQuoted && memberList.size() == PlanMgmtConstants.THREE) { // when self and 2 dependents (not spouse) quoted
				return PlanMgmtConstants.FAMILY_TIER_PRIMARY_TWO_DEPENDENT;
			} else if (!isSpouseQuoted && isSelfQuoted && isDependentQuoted && memberList.size() > PlanMgmtConstants.THREE) { // when self and more than 2 dependent (not spouse) quoted
				return PlanMgmtConstants.FAMILY_TIER_PRIMARY_MANY_DEPENDENT;
			} else {
				return PlanMgmtConstants.FAMILY_TIER_PRIMARY;
			}

		}
		
	
	@Override
	public String getApplicantStateCode(List<Map<String, String>> houseHoldData) {		 
		String stateCode = PlanMgmtConstants.EMPTY_STRING;
		if(!houseHoldData.isEmpty() ){
			String applicantZip = houseHoldData.get(0).get(PlanMgmtConstants.ZIP);
			String applicantCounty = houseHoldData.get(0).get(PlanMgmtConstants.COUNTYCODE);				
			// pull data from database					
			ZipCode zipCode = zipCodeService.getZipcodeByZipAndCountyCode(applicantZip, applicantCounty);
			if(null != zipCode){
				stateCode = zipCode.getState().toUpperCase();
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Zip code - "+ SecurityUtil.sanitizeForLogging(applicantZip) + ",  applicant's county -" + SecurityUtil.sanitizeForLogging(applicantCounty) + " belongs to state "+ stateCode);						
				}
			}	
		}
		return stateCode;
	}
		
		
		
	// build household availability logic
	@Override
	public String buildHouseHoldAvailabilityLogic(String tableName, String houseHoldType ){
		StringBuilder buildquery = new StringBuilder();
		
		if(houseHoldType.equalsIgnoreCase(PlanMgmtConstants.CHILD_ONLY)){
			buildquery.append(" AND ");
			buildquery.append( tableName.trim() );
			buildquery.append(".available_for IN ('");
			buildquery.append( PlanMgmtConstants.CHILD_ONLY);
			buildquery.append( "', '");
			buildquery.append(PlanMgmtConstants.ADULT_AND_CHILD );
			buildquery.append( "') ");
		}else {
			buildquery.append(" AND ");
			buildquery.append( tableName.trim() );
			buildquery.append(".available_for IN ('");
			buildquery.append( PlanMgmtConstants.ADULT_ONLY);
			buildquery.append( "', '");
			buildquery.append(PlanMgmtConstants.ADULT_AND_CHILD );
			buildquery.append( "') ");			
		}
		return buildquery.toString();
	}
	
	
	// build enrollment availability logic
	@Override
	public String buildEnrollmentAvailabilityLogic(String inputTableName, String effectiveDate, String isSpecialEnrollment, ArrayList<String> paramList){
		StringBuilder buildquery = new StringBuilder();
		String tableName = inputTableName.trim();
		if(isSpecialEnrollment.equalsIgnoreCase(PlanMgmtConstants.YES)){			
			buildquery.append(" AND ( ( ");
			buildquery.append(tableName);
			buildquery.append(".enrollment_avail IN('");
			buildquery.append( Plan.EnrollmentAvail.AVAILABLE.toString());
			buildquery.append("','");
			buildquery.append(Plan.EnrollmentAvail.DEPENDENTSONLY.toString());
			buildquery.append("') AND TO_DATE ( :param_" + paramList.size());
			paramList.add( effectiveDate);
			buildquery.append(" , 'YYYY-MM-DD') >= ");
			buildquery.append(tableName);
			buildquery.append(".enrollment_avail_effdate  AND ( TO_DATE ( :param_" + paramList.size());
			paramList.add(effectiveDate);
			buildquery.append(" , 'YYYY-MM-DD') < ");
			buildquery.append(tableName);
			buildquery.append(".future_erl_avail_effdate OR  ");
			buildquery.append(tableName);
			buildquery.append(".future_erl_avail_effdate is null) ) ");
			buildquery.append(" OR ");	
			buildquery.append(" ( ");
			buildquery.append(tableName);
			buildquery.append(".future_enrollment_avail IN('");
			buildquery.append(Plan.EnrollmentAvail.AVAILABLE.toString());
			buildquery.append( "','");
			buildquery.append(Plan.EnrollmentAvail.DEPENDENTSONLY.toString());
			buildquery.append("') AND TO_DATE (  :param_" + paramList.size());
			paramList.add(effectiveDate);
			buildquery.append(" , 'YYYY-MM-DD') >= ");
			buildquery.append(tableName);
			buildquery.append(".future_erl_avail_effdate) )");			
		}else{
			buildquery.append(" AND ( ( ");
			buildquery.append(tableName);
			buildquery.append(".enrollment_avail='");
			buildquery.append(Plan.EnrollmentAvail.AVAILABLE.toString());
			buildquery.append("' AND TO_DATE (  :param_" + paramList.size());
			paramList.add(effectiveDate);
			buildquery.append(" , 'YYYY-MM-DD') >= ");
			buildquery.append(tableName);
			buildquery.append(".enrollment_avail_effdate  AND ( TO_DATE (  :param_" + paramList.size());
			paramList.add(effectiveDate);
			buildquery.append(", 'YYYY-MM-DD') < ");
			buildquery.append(tableName);
			buildquery.append(".future_erl_avail_effdate OR  ");
			buildquery.append(tableName);
			buildquery.append(".future_erl_avail_effdate is null) ) ");
			buildquery.append(" OR ");
			buildquery.append(" ( ");
			buildquery.append(tableName);
			buildquery.append(".future_enrollment_avail = '");
			buildquery.append(Plan.EnrollmentAvail.AVAILABLE.toString());
			buildquery.append("' AND TO_DATE (  :param_" + paramList.size());
			paramList.add(effectiveDate);
			buildquery.append( " , 'YYYY-MM-DD') >= ");
			buildquery.append(tableName);
			buildquery.append(".future_erl_avail_effdate) ) ");
		}
		return buildquery.toString();
	}

	// build enrollment availability logic
	@Override
	public String buildEnrollmentAvailabilityLogic(String aliasOfPlanTable, String effectiveDate, String isSpecialEnrollment,
			String applicantZip, String applicantCounty, List<String> currentHiosIdList, List<String> paramList) {

		final boolean hasFutureEnrollment = true;
		StringBuilder buildquery = new StringBuilder();
		LOGGER.info("Has Special Enrollment: " + isSpecialEnrollment);

		buildquery.append(" AND ( ( ( ");

		buildquery.append(buildEnrollmentAvailableLogic(!hasFutureEnrollment, aliasOfPlanTable));

		buildquery.append(buildEnrollmentEffectiveDateLogic(!hasFutureEnrollment, aliasOfPlanTable, effectiveDate, paramList));

		buildquery.append(" ) OR ( ");

		buildquery.append(buildEnrollmentAvailableLogic(hasFutureEnrollment, aliasOfPlanTable));

		buildquery.append(buildEnrollmentEffectiveDateLogic(hasFutureEnrollment, aliasOfPlanTable, effectiveDate, paramList));

		buildquery.append(" ) ) OR ( ( ");

		buildquery.append(buildEnrollmentDependentLogic(!hasFutureEnrollment, aliasOfPlanTable, applicantZip, applicantCounty, currentHiosIdList));

		buildquery.append(buildEnrollmentEffectiveDateLogic(!hasFutureEnrollment, aliasOfPlanTable, effectiveDate, paramList));

		buildquery.append(" ) OR ( ");

		buildquery.append(buildEnrollmentDependentLogic(hasFutureEnrollment, aliasOfPlanTable, applicantZip, applicantCounty, currentHiosIdList));

		buildquery.append(buildEnrollmentEffectiveDateLogic(hasFutureEnrollment, aliasOfPlanTable, effectiveDate, paramList));

		buildquery.append(" ) ) ) ");

		return buildquery.toString();
	}

	private String buildEnrollmentAvailableLogic(boolean hasFutureEnrollment, String aliasOfPlanTable) {

		StringBuilder buildquery = new StringBuilder();

		if (!hasFutureEnrollment) {
			buildquery.append(aliasOfPlanTable);
			buildquery.append(".enrollment_avail='");
			buildquery.append(Plan.EnrollmentAvail.AVAILABLE.toString());
			buildquery.append("' ");
		}
		else {
			buildquery.append(aliasOfPlanTable);
			buildquery.append(".future_enrollment_avail = '");
			buildquery.append(Plan.EnrollmentAvail.AVAILABLE.toString());
			buildquery.append("' ");
		}
		return buildquery.toString();
	}

	private String buildEnrollmentDependentLogic(boolean hasFutureEnrollment, String aliasOfPlanTable, String applicantZip, String applicantCounty,
			List<String> currentHiosIdList) {

		StringBuilder buildquery = new StringBuilder();
		final String DELIMITER_CONST = "%' OR " + aliasOfPlanTable + ".issuer_plan_number LIKE '";
		final String PREFIX_CONST = aliasOfPlanTable + ".issuer_plan_number LIKE '";
		final String SUFFIX_CONST = "%'";

		if (!hasFutureEnrollment) {
			buildquery.append(" ( ");

			if (!CollectionUtils.isEmpty(currentHiosIdList)) {
				buildquery.append(aliasOfPlanTable);
				buildquery.append(".enrollment_avail = '");
				buildquery.append(Plan.EnrollmentAvail.DEPENDENTSONLY.toString());
				buildquery.append("' AND (");

				// EXAMPLE: (p.issuer_plan_number LIKE '11111%' OR p.issuer_plan_number LIKE '22222%' OR p.issuer_plan_number LIKE '33333%')
				buildquery.append(currentHiosIdList.stream().collect(Collectors.joining(DELIMITER_CONST, PREFIX_CONST, SUFFIX_CONST)));
				buildquery.append(")) OR (");
			}
			buildquery.append(aliasOfPlanTable);
			buildquery.append(".enrollment_avail = '");
			buildquery.append(Plan.EnrollmentAvail.DEPENDENTSONLY.toString());
			buildquery.append("' AND ");
			buildquery.append(aliasOfPlanTable);
			buildquery.append(".service_area_id IN (");
			buildquery.append("SELECT service_area_id FROM pm_service_area WHERE zip = '");
			buildquery.append(applicantZip);
			buildquery.append("' AND fips = '");
			buildquery.append(applicantCounty);
			buildquery.append("' AND enrollment_avail_status = '");
			buildquery.append(ServiceArea.EnrollmentAvailStatus.AVAILABLE.toString());
			buildquery.append("' ) ) ");
		}
		else {
			buildquery.append(" ( ");

			if (!CollectionUtils.isEmpty(currentHiosIdList)) {
				buildquery.append(aliasOfPlanTable);
				buildquery.append(".future_enrollment_avail = '");
				buildquery.append(Plan.EnrollmentAvail.DEPENDENTSONLY.toString());
				buildquery.append("' AND (");

				// EXAMPLE: (p.issuer_plan_number LIKE '11111%' OR p.issuer_plan_number LIKE '22222%' OR p.issuer_plan_number LIKE '33333%')
				buildquery.append(currentHiosIdList.stream().collect(Collectors.joining(DELIMITER_CONST, PREFIX_CONST, SUFFIX_CONST)));
				buildquery.append(")) OR (");
			}
			buildquery.append(aliasOfPlanTable);
			buildquery.append(".future_enrollment_avail = '");
			buildquery.append(Plan.EnrollmentAvail.DEPENDENTSONLY.toString());
			buildquery.append("' AND ");
			buildquery.append(aliasOfPlanTable);
			buildquery.append(".service_area_id IN (");
			buildquery.append("SELECT service_area_id FROM pm_service_area WHERE zip = '");
			buildquery.append(applicantZip);
			buildquery.append("' AND fips = '");
			buildquery.append(applicantCounty);
			buildquery.append("' AND enrollment_avail_status = '");
			buildquery.append(ServiceArea.EnrollmentAvailStatus.AVAILABLE.toString());
			buildquery.append("' ) ) ");
		}
		return buildquery.toString();
	}

	private String buildEnrollmentEffectiveDateLogic(boolean hasFutureEnrollment, String aliasOfPlanTable,
			String effectiveDate, List<String> paramList) {

		StringBuilder buildquery = new StringBuilder();

		if (!hasFutureEnrollment) {
			buildquery.append(" AND TO_DATE (  :param_" + paramList.size());
			paramList.add(effectiveDate);
			buildquery.append(" , 'YYYY-MM-DD') >= ");
			buildquery.append(aliasOfPlanTable);
			buildquery.append(".enrollment_avail_effdate  AND ( TO_DATE (  :param_" + paramList.size());
			paramList.add(effectiveDate);
			buildquery.append(", 'YYYY-MM-DD') < ");
			buildquery.append(aliasOfPlanTable);
			buildquery.append(".future_erl_avail_effdate OR  ");
			buildquery.append(aliasOfPlanTable);
			buildquery.append(".future_erl_avail_effdate is null) ");
		}
		else {
			buildquery.append(" AND TO_DATE (  :param_" + paramList.size());
			paramList.add(effectiveDate);
			buildquery.append( " , 'YYYY-MM-DD') >= ");
			buildquery.append(aliasOfPlanTable);
			buildquery.append(".future_erl_avail_effdate ");
		}
		return buildquery.toString();
	}

	// build rate computation logic
	@Override
	public String buildRateLogic(String inputTableName, String stateName, String houseHoldType, String familyTierLookupCode, String memberAge, String memberTobaccoUsage, String effectiveDate,
			ArrayList<String> paramList){
		// if quote for NY state and household type are child only i.e all family members are children
		// then quote household by age band.
		// for other state quote on basis of age band or family tier
		StringBuilder buildquery = new StringBuilder(512);
		String tableName = inputTableName.trim();
		if(!stateName.equals(PlanMgmtConstants.EMPTY_STRING) && stateName.equalsIgnoreCase("NY") && houseHoldType.equalsIgnoreCase(PlanMgmtConstants.CHILD_ONLY)){
			buildquery.append(" AND ( CAST (:param_" + paramList.size());
			paramList.add(memberAge);
			buildquery.append(" AS INTEGER) >= ");
			buildquery.append(tableName);
			buildquery.append(".min_age AND CAST (:param_" + paramList.size());
			paramList.add(memberAge);
			buildquery.append(" AS INTEGER) <= ");
			buildquery.append(tableName);
			buildquery.append(".max_age) ");
		}else{			
			// on private exchange rate could be on basis of age band or family tier			
			buildquery.append("AND ( ( CAST (:param_" + paramList.size());
			paramList.add(memberAge);
			buildquery.append(" AS INTEGER) >= ");
			buildquery.append(tableName);
			buildquery.append(".min_age AND CAST (:param_" + paramList.size());
			paramList.add(memberAge);
			buildquery.append(" AS INTEGER) <= ");
			buildquery.append(tableName);
			buildquery.append(".max_age) ");
			if(null != familyTierLookupCode) {
				buildquery.append(" OR (CAST (");
				buildquery.append(familyTierLookupCode);
				buildquery.append(" AS INTEGER) >= ");
				buildquery.append(tableName);
				buildquery.append(".min_age AND CAST (");
				buildquery.append(familyTierLookupCode);
				buildquery.append(" AS INTEGER) <= ");
				buildquery.append(tableName);
				buildquery.append(".max_age) ");
			}
			buildquery.append(") ");				
		}
		
		// consider tobacco usages to compute plan rate 
		if(memberTobaccoUsage != null && !memberTobaccoUsage.equals(PlanMgmtConstants.EMPTY_STRING)){
			buildquery.append(" AND (");
			buildquery.append(tableName);
			buildquery.append(".tobacco = UPPER(  :param_" + paramList.size());
			paramList.add(memberTobaccoUsage);
			buildquery.append(") OR ");
			buildquery.append(tableName);
			buildquery.append(".tobacco is NULL) ");
		} else {
			buildquery.append(" AND (");
			buildquery.append(tableName);
			buildquery.append(".tobacco = UPPER(  :param_" + paramList.size());
			paramList.add("N");
			buildquery.append(") OR ");
			buildquery.append(tableName);
			buildquery.append(".tobacco is NULL) ");
		}
		
		buildquery.append(" AND TO_DATE ( :param_" + paramList.size() + ", 'YYYY-MM-DD') BETWEEN ");
		paramList.add(effectiveDate);
		buildquery.append(tableName);
		buildquery.append(".effective_start_date AND ");
		buildquery.append(tableName);
		buildquery.append(".effective_end_date ");
		buildquery.append("AND ");
		buildquery.append(tableName);
		buildquery.append(".rate > 0 ");
		buildquery.append("AND ");
		buildquery.append(tableName);
		buildquery.append(".is_deleted='N' ");		
		
		return buildquery.toString();
	}	
		
	
	// build rate computation logic for dental quoting
		@Override
		public String buildRateLogicForDentalQuoting(String rateTable, String issuerTable, String stateName, String houseHoldType, String familyTierLookupCode, String memberAge, String memberTobaccoUsage, String effectiveDate, String exchangeFlow, boolean doQuoteIssuerIHC, 
				String IHCfamilyTierLookupCode, ArrayList<String> paramList){
			// if quote for NY state and household type are child only i.e all family members are children
			// then quote household by age band.
			// for other state quote on basis of age band or family tier
			StringBuilder buildquery = new StringBuilder();
			
			if(!stateName.equals(PlanMgmtConstants.EMPTY_STRING) && stateName.equalsIgnoreCase("NY") && houseHoldType.equalsIgnoreCase(PlanMgmtConstants.CHILD_ONLY)){
				buildquery.append(" AND ( CAST (:param_" + paramList.size());
				paramList.add(memberAge);
				buildquery.append(" AS INTEGER) >= ");
				buildquery.append(rateTable);
				buildquery.append(".min_age AND CAST (:param_" + paramList.size());
				paramList.add(memberAge);
				buildquery.append(" AS INTEGER) <= ");
				buildquery.append(rateTable);
				buildquery.append(".max_age) ");
			}else{			
				// on private exchange rate could be on basis of age band or family tier
				if(exchangeFlow.equalsIgnoreCase(Plan.EXCHANGE_TYPE.OFF.toString())){	
					buildquery.append(" AND ( ( CAST (:param_" + paramList.size());
					paramList.add(memberAge);
					buildquery.append(" AS INTEGER) >= ");
					buildquery.append(rateTable);
					buildquery.append(".min_age AND CAST (:param_" + paramList.size());
					paramList.add(memberAge);
					buildquery.append(" AS INTEGER) <= ");
					buildquery.append(rateTable);
					buildquery.append(".max_age) ");
					buildquery.append(" OR (  CAST (:param_" + paramList.size());
					paramList.add(familyTierLookupCode);
					buildquery.append(" AS INTEGER) >= prate.min_age AND CAST (:param_" + paramList.size());
					paramList.add(familyTierLookupCode);
					buildquery.append(" AS INTEGER) <= prate.max_age AND ");
					buildquery.append(issuerTable);
					buildquery.append(".name NOT LIKE '%IHC%' ) ");
										
					// if household is applicable for issuer IHC quoting, then use IHC's own family tier look up code
					if(doQuoteIssuerIHC){ 
						buildquery.append(" OR ( CAST (:param_" + paramList.size());
						paramList.add(IHCfamilyTierLookupCode);
						buildquery.append(" AS INTEGER) >= prate.min_age AND CAST (:param_" + paramList.size());
						paramList.add(IHCfamilyTierLookupCode);
						buildquery.append(" AS INTEGER) <= prate.max_age AND ");
						buildquery.append(issuerTable);
						buildquery.append(".name LIKE '%IHC%' ) ) ");
						//buildquery.append(" OR (" + familyTierLookupCode + " >= prate.min_age AND " + familyTierLookupCode + " <= prate.max_age AND i2.name NOT LIKE '%IHC%' ) ) ");
					}else{
						buildquery.append(" ) ");
					}					
					
				}else{ // when exchange flow is ON, use age band for quoting  
					buildquery.append("AND ( CAST (:param_" + paramList.size());
					paramList.add(memberAge);
					buildquery.append(" AS INTEGER) >= ");
					buildquery.append(rateTable);
					buildquery.append(".min_age AND CAST (:param_" + paramList.size());
					paramList.add(memberAge);
					buildquery.append(" AS INTEGER) <= ");
					buildquery.append(rateTable);
					buildquery.append(".max_age ");
					buildquery.append(") ");
				}				
			}			
			
			// consider tobacco usages to compute plan rate 
			if(memberTobaccoUsage != null && !memberTobaccoUsage.equals(PlanMgmtConstants.EMPTY_STRING)){
				buildquery.append(" AND (");
				buildquery.append(rateTable);
				buildquery.append(".tobacco = UPPER( :param_" + paramList.size());
				paramList.add(memberTobaccoUsage);
				buildquery.append(") OR ");
				buildquery.append(rateTable);
				buildquery.append(".tobacco is NULL) ");
			}
			
			buildquery.append(" AND TO_DATE ( :param_" + paramList.size() + ", 'YYYY-MM-DD') BETWEEN ");
			paramList.add(effectiveDate);
			buildquery.append(rateTable);
			buildquery.append(".effective_start_date AND ");
			buildquery.append(rateTable);
			buildquery.append(".effective_end_date ");
			buildquery.append("AND ");
			buildquery.append(rateTable);
			buildquery.append(".rate > 0 ");
			buildquery.append("AND ");
			buildquery.append(rateTable);
			buildquery.append(".is_deleted='N' ");		
			
			return buildquery.toString();
		}	
			
	
	
	// build cost sharing logic
	@Override
	public String buildCostSharingLogic(String inputTableName, String csrValue) {
		StringBuilder buildquery = new StringBuilder();
		String tableName = inputTableName.trim();
		
		if (csrValue.equals("") || csrValue.equalsIgnoreCase("CS1")) {
			// pick all base plan of all metal tier
			buildquery.append(" AND ");
			buildquery.append(tableName);
			buildquery.append(".parent_plan_id = 0 "); 
		} else if (csrValue.equalsIgnoreCase("CS2") || csrValue.equalsIgnoreCase("CS3")) {
			 // pick all respective cost sharing plan of all metal tier
			buildquery.append(" AND ");
			buildquery.append(tableName);
			buildquery.append(".cost_sharing = UPPER('");
			buildquery.append(csrValue);
			buildquery.append("')");
		} else if (csrValue.equalsIgnoreCase("CS4") || csrValue.equalsIgnoreCase("CS5") || csrValue.equalsIgnoreCase("CS6")) {
			// pick base plan of other metal tiers except silver + respective cost sharing plan
			buildquery.append(" AND ((");
			buildquery.append(tableName);
			buildquery.append(".parent_plan_id = 0 AND ");
			buildquery.append(tableName);
			buildquery.append(".plan_level !='");
			buildquery.append(Plan.PlanLevel.SILVER.toString());
			buildquery.append("')  OR ");
			buildquery.append(tableName);
			buildquery.append(".cost_sharing=UPPER('");
			buildquery.append(csrValue);
			buildquery.append("'))"); 
		}
	
		return buildquery.toString();
	}
		
	
	@Override
	public String getIHCFamilyTierLookupCode(List<Map<String, String>> memberList){
		String iHCFamilyTierLookupCode  = "";
		switch(memberList.size()){
			case 1: iHCFamilyTierLookupCode = PlanMgmtConstants.IHC_INDV_CODE;
			break;
			case 2: iHCFamilyTierLookupCode = PlanMgmtConstants.IHC_INDV_PULS_ONE_CODE;
			break;
			case 3: iHCFamilyTierLookupCode = PlanMgmtConstants.IHC_INDV_PULS_TWO_CODE;
			break;
			case 4: iHCFamilyTierLookupCode = PlanMgmtConstants.IHC_INDV_PULS_THREE_CODE;
			break;
			case 5: iHCFamilyTierLookupCode = PlanMgmtConstants.IHC_INDV_PULS_FOUR_CODE;
			break;
			case 6: iHCFamilyTierLookupCode = PlanMgmtConstants.IHC_INDV_PULS_FIVE_CODE;
			break;
			case 7: iHCFamilyTierLookupCode = PlanMgmtConstants.IHC_INDV_PULS_SIX_CODE;
			break;
			case 8: iHCFamilyTierLookupCode = PlanMgmtConstants.IHC_INDV_PULS_SEVEN_CODE;
			break;
			default: iHCFamilyTierLookupCode = PlanMgmtConstants.IHC_INDV_PULS_SEVEN_CODE;
			break;
		}
		
		return iHCFamilyTierLookupCode;
	}
	
	@Override
	public boolean isAgeAllowForIHCQuoting(int age){
		if(age < PlanMgmtConstants.EIGHTEEN){
			return false;
		}else if(age > PlanMgmtConstants.SIXTY_FOUR){
			return false;
		}else{
			return true;
		}
	}

	@Override
	public int getApplicantAge(List<Map<String, String>> modifiedMemberList){
		int applicantAge = 0;
		for(Map<String, String> applicantData: modifiedMemberList)
		{
			if(applicantData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.SELF) || applicantData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.MEMBER)){
				applicantAge = Integer.parseInt(applicantData.get(PlanMgmtConstants.AGE));
				break;
			}
		}
		return applicantAge;
	}
	
	// get family tier look up code
	@Override
	public String getFamilyTierLookupCode(String familyTier){
		String familyTierLookUpCode = PlanMgmtConstants.EMPTY_STRING;
		
		// if family tier look up code exists in class properties, then use that else pull from database
		try {			
			if(familyTierLookupCodeMap.containsKey(familyTier)){
				familyTierLookUpCode = familyTierLookupCodeMap.get(familyTier);
				if(familyTierLookUpCode != null){
					return familyTierLookUpCode;
				}
			}		
			
			// pull data from database
			familyTierLookUpCode = lookupService.getLookupValueCode(PlanMgmtConstants.FAMILY_RATE_LOOKUP_NAME, familyTier);
			familyTierLookupCodeMap.put(familyTier, familyTierLookUpCode);
			
		} catch (Exception e) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME
					+ PlanMgmtErrorCodes.ErrorCode.FAMILY_TIER_LOOKIUP_CODE_EXCEPTION,
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}	

		return familyTierLookUpCode;
	}
	
	/* 
	 * used to compute the rate depends on the rateOption and hiosIssuerId
	 * return householdPremium
	 * 
	 */
	@Override
	public Float calculatePremiumForAMEPlan(String rateOption, String hiosIssuerId, Float householdPremium, int memberdataSize, String stateName, DecimalFormat df) {
		Float temp_householdPremium = householdPremium;
		// if rate option is Family, then premium would be on family level, don't consider each quoted member's premium
		// if rate option is Age band, then premium would be for each member and aggregate for whole household
		if ("F".equalsIgnoreCase(rateOption)) {  // family quoting 
			temp_householdPremium = (temp_householdPremium / memberdataSize);
		}else if ("A".equalsIgnoreCase(rateOption)) {  // age band quoting
			// ref HIX-54376, get ame carrier premium factor from "pm_ame_premium_factor" table
			Double premiumFactor = iAMEPremiumFactorRepository.getMonthlyFactor(hiosIssuerId, stateName);
			// if ame premium factor not null for respective issuer and state code then multiply premium with premium factor
			if(premiumFactor != null){
				temp_householdPremium = (temp_householdPremium * Float.parseFloat(premiumFactor.toString()));						
				temp_householdPremium = Float.parseFloat(df.format(temp_householdPremium));
			}					
		}
		return temp_householdPremium;
	}
	
	
	@Override
	public String buildPlanCertificationLogic(String inputTableName, boolean issuerVerifiedFlag){
		return QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic(inputTableName, issuerVerifiedFlag);
	}
	
	@Override
	public String buildPlanCertificationLogicJPA(String inputTableName, boolean issuerVerifiedFlag){
		return QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA(inputTableName, issuerVerifiedFlag);
	}
	
	@Override
	public void buildEnrollmentAvailabilityLogic(StringBuilder parentBuilder, String inputTableName,String isSpecialEnrollment){
		QuotingBusinessLogicQueryBuilder.buildEnrollmentAvailabilityLogic(parentBuilder, inputTableName, isSpecialEnrollment);
	}
	
	@Override
	public String getApplicantZipCode(List<Map<String, String>> memberList){
		String applicantZip = PlanMgmtConstants.EMPTY_STRING;
		for (Map<String, String> member : memberList) {					
			if (member.get(PlanMgmtConstants.RELATION) != null) {	
				if(member.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.SELF)){							
					applicantZip = member.get(PlanMgmtConstants.ZIP);	
					break;
				}	
			}	
		}
		return applicantZip;	
	}
	

	// HIX-100131 :: Enhance Re-quoting and SLSP API to support relationship Id.
	// Convert requested Relationship Id to Relation String
	@Override
	public List<Map<String, String>> convertRelationshipIdToString(List<Map<String, String>> memberList)  {
		List<Map<String, String>> processedMemberList = new ArrayList<Map<String, String>>();
		//LOGGER.info(" Before  " + memberList.toString());
		try
		{
			for (Map<String, String> member : memberList) {
				if (member.get(PlanMgmtConstants.RELATION) != null) {
					// if requested relationship code exists in relatioshipIdMap then pull corresponding relationship string and set value for "relation" key
					// otherwise keep as it is
					if(relatioshipIdMap.containsKey(member.get(PlanMgmtConstants.RELATION).trim())){
						member.put(PlanMgmtConstants.RELATION, relatioshipIdMap.get(member.get(PlanMgmtConstants.RELATION).trim()));
					}
				}
				
				processedMemberList.add(member);
			}
		}
		catch(Exception ex){
			LOGGER.error("Error inside setRelationshipForMemberData(). Exce: ", ex);
		}
		
		//LOGGER.info(" After  " + processedMemberList.toString());

		return processedMemberList;
	}
	
	
	// HIX-100092 :: prepare search rating are query depends on state exchange
		@Override
		public String buildSearchRatingAreaLogic(String stateCode, ArrayList<String> paramList, List<Map<String, String>> processedMemberData, int ctr, String applicableYear, String configuredDB){
			StringBuilder buildquery = new StringBuilder(1024);
			
			// if state exchange is ID, then apply applicable year filter on 'pm_zip_county_rating_area' table  
			if(PlanMgmtConstants.STATE_CODE_ID.equalsIgnoreCase(stateCode))
			{
				buildquery.append(" (SELECT rAreaId ");
				buildquery.append("FROM ");
				buildquery.append("(SELECT prarea2.id AS rAreaId, row_number() OVER (ORDER by pzcrarea.applicable_year DESC) AS seqnum ");
				buildquery.append("FROM ");
				buildquery.append("pm_zip_county_rating_area pzcrarea ");
				buildquery.append("LEFT JOIN pm_rating_area prarea2 ");
				buildquery.append("ON pzcrarea.rating_area_id = prarea2.id ");
				buildquery.append("WHERE ");

				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("(pzcrarea.zip = to_number(trim(:param_" + paramList.size() + ")) OR pzcrarea.zip IS NULL) ");
					paramList.add(processedMemberData.get(ctr).get(PlanMgmtConstants.ZIP));
				}else{
					buildquery.append("(pzcrarea.zip = :param_" + paramList.size() + " OR pzcrarea.zip IS NULL) ");
					paramList.add(processedMemberData.get(ctr).get(PlanMgmtConstants.ZIP));
				}
				
				if (!StringUtils.isBlank(processedMemberData.get(ctr).get(PlanMgmtConstants.COUNTYCODE))) {
					buildquery.append("AND pzcrarea.county_fips = :param_" + paramList.size());
					paramList.add(processedMemberData.get(ctr).get(PlanMgmtConstants.COUNTYCODE) );
				}	

				buildquery.append(" AND pzcrarea.applicable_year <= :param_" + paramList.size());
				paramList.add(applicableYear);
				
				buildquery.append(") rowline WHERE seqnum=1 ) ");
			}
			else
			{
				buildquery.append(" (SELECT prarea2.id ");
				buildquery.append("FROM ");
				buildquery.append("pm_zip_county_rating_area pzcrarea ");
				buildquery.append("LEFT JOIN pm_rating_area prarea2 ");
				buildquery.append("ON pzcrarea.rating_area_id = prarea2.id ");
				buildquery.append("WHERE ");

				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("(pzcrarea.zip = to_number(trim(:param_" + paramList.size() + ")) OR pzcrarea.zip IS NULL) ");
					paramList.add(processedMemberData.get(ctr).get(PlanMgmtConstants.ZIP));
				}else{
					buildquery.append("(pzcrarea.zip = :param_" + paramList.size() + " OR pzcrarea.zip IS NULL) ");
					paramList.add(processedMemberData.get(ctr).get(PlanMgmtConstants.ZIP));
				}
				
				if (!StringUtils.isBlank(processedMemberData.get(ctr).get(PlanMgmtConstants.COUNTYCODE))) {
					buildquery.append("AND pzcrarea.county_fips = :param_" + paramList.size());
					paramList.add(processedMemberData.get(ctr).get(PlanMgmtConstants.COUNTYCODE) );
				}	
				
				buildquery.append(") ");
			}
			
			return buildquery.toString();
		}
		
		
}
