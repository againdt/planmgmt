package com.getinsured.hix.planmgmt.service;

import java.util.List;

import com.getinsured.hix.dto.planmgmt.LifePlanDTO;
import com.getinsured.hix.dto.planmgmt.Member;

public interface LifePlanRateBenefitService {

	List<LifePlanDTO> getPlans(String effectiveDate, Member memberInfo, String stateCode, String tenantCode);		
}
