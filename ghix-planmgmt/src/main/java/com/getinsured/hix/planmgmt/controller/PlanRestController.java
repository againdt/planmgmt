/**
 * PlanRestController.java
 * @author santanu de
 * @version 1.0
 * @since April 16, 2013 
 */
package com.getinsured.hix.planmgmt.controller;


import static org.apache.log4j.Logger.getLogger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Produces;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.FormularyDrugListResponseDTO;
import com.getinsured.hix.dto.planmgmt.ListOfStringArrayResponseDTO;
import com.getinsured.hix.dto.planmgmt.PediatricPlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.PediatricPlanResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanBenefitAndCostRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanBulkUpdateRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanComponentHistoryDTO;
import com.getinsured.hix.dto.planmgmt.PlanCrossWalkRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanCrossWalkResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanDocumentRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanDocumentResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanListResponse;
import com.getinsured.hix.dto.planmgmt.PlanRateListResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.planmgmt.PlanSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanSearchResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanTenantLightDTO;
import com.getinsured.hix.dto.planmgmt.PlanTenantRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanTenantResponseDTO;
import com.getinsured.hix.dto.planmgmt.RatingAreaRequestDTO;
import com.getinsured.hix.dto.planmgmt.RatingAreaResponseDTO;
import com.getinsured.hix.dto.planmgmt.SbcScenarioDTO;
import com.getinsured.hix.dto.planmgmt.SbcScenarioRequestDTO;
import com.getinsured.hix.dto.planmgmt.SbcScenarioResponseDTO;
import com.getinsured.hix.dto.planmgmt.ServiceAreaDetailsDTO;
import com.getinsured.hix.dto.shop.EmployeeCoverageDTO;
import com.getinsured.hix.dto.shop.EmployerCoverageDTO;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.dto.shop.ShopResponse.ResponseType;
import com.getinsured.hix.dto.shop.employer.eps.EmployerQuotingDTO;
import com.getinsured.hix.dto.shop.employer.eps.PlanDTO;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerCommission;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.TenantPlan;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.repository.IFormularyRepository;
import com.getinsured.hix.planmgmt.repository.IPlanRepository;
import com.getinsured.hix.planmgmt.service.AMEPlanBenefitService;
import com.getinsured.hix.planmgmt.service.IssuerQualityRatingService;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.NetworkTansparencyRatingService;
import com.getinsured.hix.planmgmt.service.PlanCostAndBenefitsService;
import com.getinsured.hix.planmgmt.service.PlanHistoryService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.planmgmt.service.PlanRateBenefitService;
import com.getinsured.hix.planmgmt.service.PlanSbcScenarioService;
import com.getinsured.hix.planmgmt.service.ProviderService;
import com.getinsured.hix.planmgmt.service.RatingAreaService;
import com.getinsured.hix.planmgmt.service.STMPlanRateBenefitService;
import com.getinsured.hix.planmgmt.service.VisionPlanRateBenefitService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.service.IssuerCommissionService;


//@Api(value="Plan Information Operation", description ="Returning Specific Plan Information")
@Controller
@DependsOn("dynamicPropertiesUtil")
@RequestMapping(value = "/plan")
public class PlanRestController {	
	private static final Logger LOGGER = getLogger(PlanRestController.class);
	@Autowired 
	private PlanMgmtService planMgmtService;
	@Autowired 
	private PlanRateBenefitService planRateBenefitService;
	@Autowired 
	private STMPlanRateBenefitService stmPlanRateBenefitService;
	@Autowired
	private AMEPlanBenefitService amePlanBenefitService;
	@Autowired
	private IFormularyRepository formularyRepository;
	@Autowired 
	private PlanCostAndBenefitsService planCostAndBenefitsService;
	@Autowired 
	private ProviderService providerService;
	@Autowired
	private IssuerQualityRatingService issuerQualityRatingService;
	@Autowired
	VisionPlanRateBenefitService visionPlanRateBenefitService;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private GIExceptionHandler giExceptionHandler; 
	@Autowired
	private RatingAreaService ratingAreaService;
	@Autowired
	private NetworkTansparencyRatingService networkTansparencyRatingService;
	@Autowired
	private IssuerService issuerService;
	@Autowired
	private PlanSbcScenarioService planSbcScenarioService;
	@Autowired
	private PlanHistoryService planHistoryService;
	@Autowired
	private IssuerCommissionService issuerCommissionService;
	@Autowired
	private IPlanRepository iPlanRepository;	

	
	@Value("#{configProp['appUrl']}") 
	private String appUrl;

	private static final String DOCUMENT_URL= "download/document?documentId="; // document download url
	
//	@ApiOperation(value="welcome", notes="Welcome to Plan Information API")
//	@ApiResponse(value="Welcome to Plan rest controller, invoke PlanRestController")
//	@RequestMapping(value = "/welcome", method = RequestMethod.GET) // for testing purpose
//	@ResponseBody public String welcome()
//	{
//		if(LOGGER.isInfoEnabled()) {
//			LOGGER.info("Welcome to GHIX-Planmgmt module, invoke PlanRestController");
//		}
//		return "Welcome to GHIX-Planmgmt module, invoke PlanRestController";
//	}
		
//	@ApiOperation(value="getrefplan", notes="This API takes emp zip, county, insuranceType, effectiveDate and ehbCovered e.g. PARTIAL as input and provides Plan, Issuer Plan number, tier name e.g. GOLD and sum of employees premium")
//	@ApiResponse(value="Returning ShopResonse with PlanDTO list")
	@RequestMapping(value = "/getrefplan", method = RequestMethod.POST)
	@ResponseBody public String getReferencePlans(@RequestBody EmployerQuotingDTO employerQuotingDTO)throws  GIException
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getRefPlans() ============ ");
		}
		if(LOGGER.isDebugEnabled() ) {
			if(null != employerQuotingDTO) {
				StringBuilder buildStr = new StringBuilder();
				buildStr.append("Request data ===>");	
				buildStr.append(" Insurance type : ");
				buildStr.append(employerQuotingDTO.getInsType());
				buildStr.append(" Effective date : ");
				buildStr.append(employerQuotingDTO.getEffectiveDate());
				buildStr.append(" empPrimaryZip : ");
				buildStr.append(employerQuotingDTO.getEmpPrimaryZip());
				buildStr.append(" empCountyCode : ");
				buildStr.append(employerQuotingDTO.getEmpCountyCode());				
				buildStr.append(" ehbCovered : ");
				buildStr.append(employerQuotingDTO.getEhbCovered());
				LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));
			} else {
				LOGGER.debug("getReferencePlans: Input param is null");
			}
		}
		
		ShopResponse shopResponse = new ShopResponse();	
		// if request data elements are null throw error
		if(null == employerQuotingDTO 
				|| null == employerQuotingDTO.getEffectiveDate()
				|| 0 == employerQuotingDTO.getEffectiveDate().trim().length()
				|| null == employerQuotingDTO.getEmpPrimaryZip()
				|| 0 == employerQuotingDTO.getEmpPrimaryZip().trim().length()
				|| null == employerQuotingDTO.getInsType()	
				|| 0 == employerQuotingDTO.getInsType().trim().length()	
				|| null == employerQuotingDTO.getEmpCountyCode()
				|| 0 == employerQuotingDTO.getEmpCountyCode().trim().length()
				) {
			shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			shopResponse.setErrMsg("Invalid request parameters.");
		}else{			
				String empPrimaryZip = employerQuotingDTO.getEmpPrimaryZip().trim();
				String empCountyCode = employerQuotingDTO.getEmpCountyCode().trim();
				String ehbCovered = (employerQuotingDTO.getEhbCovered() != null) ? employerQuotingDTO.getEhbCovered().trim() : "";
				String insuranceType = employerQuotingDTO.getInsType().trim();
				String effectiveDate = employerQuotingDTO.getEffectiveDate().trim();			
							
				try{
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Looking for SHOP plans for zip " + SecurityUtil.sanitizeForLogging(empPrimaryZip) + ", county code " + SecurityUtil.sanitizeForLogging(empCountyCode));
					}
					List<PlanDTO> planDTOList = planMgmtService.getReferenceHealthPlans(empPrimaryZip, ehbCovered, insuranceType, effectiveDate, empCountyCode);
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug(planDTOList.size() + " ref plans found");
					}
					employerQuotingDTO.setPlanDTOList(planDTOList);
					shopResponse.addResponseData(EmployerQuotingDTO.class.getName(), employerQuotingDTO);
				}catch(Exception ex) {
					LOGGER.error("Fail to fetch plans. Exception:",ex);					
					shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_REFERENCE_PLANS.getCode());
					shopResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}			
				
		}

		return shopResponse.sendResponse(ResponseType.XML);		
	}
	
//	@ApiOperation(value="checkPlanAvailabilityForEmployees", notes="This operation checks availability of plans for employees")
//	@ApiResponse(value="Returning ShopResonse with EmployeeCoverageDTO list")
	@RequestMapping(value = "/checkPlanAvailabilityForEmployees", method = RequestMethod.POST)
	@ResponseBody public String checkPlanAvailabilityForEmployees(@RequestBody List<EmployerCoverageDTO> employerCoverageDTOList)throws  GIException
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside checkPlanAvailabilityForEmployees() ============ ");
		}
		
		ShopResponse shopResponse = new ShopResponse();	
		try{
		// if request data elements are null throw error
		if(null == employerCoverageDTOList ) {
			shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			shopResponse.setErrCode(PlanMgmtConstants.STATUS_FAILURE_CODE);
			shopResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}else{
			List<EmployeeCoverageDTO> employeeCoverageDTOList ;
			
			for(EmployerCoverageDTO employerCoverageDTO : employerCoverageDTOList){
				employeeCoverageDTOList = employerCoverageDTO.getEmployeeCoverage();
				String planYear = employerCoverageDTO.getCoverageStartDate().split("-")[2];
				employerCoverageDTO.setEmployeeCoverage(planMgmtService.checkPlanAvailabilityForEmployees(employeeCoverageDTOList,planYear,employerCoverageDTO.getCoverageStartDate()));
			}
		}
		shopResponse.addResponseData(ShopResponse.class.getName(), employerCoverageDTOList);
		}catch(Exception ex) {
			LOGGER.error("Fail to check PlanAvailability For Employees. Exception:", ex);					
			shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.CHECK_PLAN_AVAILABILITY_FOR_EMPLOYEES.getCode());
			shopResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
		}	
		 return shopResponse.sendResponse(ResponseType.XML);
	}
	
//	@ApiOperation(value="getinfo", notes="This operation retrieves details of a specific plan based on plan level, insurance type, market type, exchange Type, plan id(s), effective date, zip, issuer name etc. This API takes Plan ID as input and provides plan information - for example given id=3 plan info will have - plan benefits, issuer details")
//	@ApiResponse(value="Returning PlanResonse with Plan Information, Specific Benefits Information and Speicific Cost Information")	
	@RequestMapping(value = "/getinfo", method = RequestMethod.POST)
	@ResponseBody public String getPlanInfo(@RequestBody PlanRequest planRequest)throws  GIException	
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getPlanInfo() ============ ");
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Request data :: plan Id : " + SecurityUtil.sanitizeForLogging(planRequest.getPlanId()) );
		}	
		
		PlanResponse planResponse = new PlanResponse();
		// if request data elements are null throw error
		if(null == planRequest ||
				!org.apache.commons.lang3.StringUtils.isNumeric(planRequest.getPlanId())) {
			planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}else{
				String planId = planRequest.getPlanId();
				String marketType = (planRequest.getMarketType() == null) ? Plan.PlanMarket.SHOP.toString() : planRequest.getMarketType() ; // default value is SHOP
				String considerEHBPortion = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CONSIDER_EHB_PORTION);
				boolean minimizePlanData = planRequest.isMinimizePlanData();
				// get tenant code from request. if tenant code does not exists in request then consider GINS as default teant code
				String tenantCode = (planRequest.getTenantCode() != null )? planRequest.getTenantCode() : PlanMgmtConstants.TENANT_GINS;
				// HIX-101472 - commission effective date is optional in request parameter 
				String commEffectiveDate = (planRequest.getCommEffectiveDate() != null) ? planRequest.getCommEffectiveDate() : PlanMgmtConstants.EMPTY_STRING;
				
				try{
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("Fetching plan data....");
					}
					Plan planData = planMgmtService.getPlanData(planId);				

					if(null != planData){
						planResponse.setPlanId(planData.getId());
						planResponse.setPlanName(planData.getName());					
						planResponse.setIssuerId(planData.getIssuer().getId());
						planResponse.setIssuerName(planData.getIssuer().getName());
						planResponse.setIssuerLegalName((planData.getIssuer().getCompanyLegalName() != null) ? planData.getIssuer().getCompanyLegalName() : PlanMgmtConstants.EMPTY_STRING);
						//added for widget display on member portal
						planResponse.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(planData.getIssuer().getLogoURL(), planData.getIssuer().getHiosIssuerId(), null, appUrl));
						planResponse.setHiosIssuerId(planData.getIssuerPlanNumber().substring(0, 5));
						
						if(marketType.equalsIgnoreCase(Plan.PlanMarket.SHOP.toString())){						
							planResponse.setIssuerSite(planData.getIssuer().getShopSiteUrl());
							planResponse.setIssuerPhone(planData.getIssuer().getShopCustServicePhone());
						}
						if(marketType.equalsIgnoreCase(Plan.PlanMarket.INDIVIDUAL.toString())){						
							planResponse.setIssuerSite(planData.getIssuer().getIndvSiteUrl());
							planResponse.setIssuerPhone(planData.getIssuer().getIndvCustServicePhone());
						}					
						
						planResponse.setPaymentUrl(planData.getIssuer().getPaymentUrl());
						planResponse.setApplicationUrl(issuerService.getApplicationUrl(planData.getIssuer(), tenantCode));
						planResponse.setProducerUserName(planData.getIssuer().getProducerUserName());
						planResponse.setProducerPassword(planData.getIssuer().getProducerPassword());
						
						planResponse.setIssuerPlanNumber(planData.getIssuerPlanNumber());	
						planResponse.setInsuranceType(planData.getInsuranceType());//As per HIX-20024
						planResponse.setTaxIdNumber((planData.getIssuer().getFederalEin() != null) ? planData.getIssuer().getFederalEin().replace("-", "") : "");
						
						if(planData.getBrochure() != null) {
							String brochureUrl = planData.getBrochure();
							if (brochureUrl.matches(PlanMgmtConstants.ECM_REGEX)) {
							      brochureUrl = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(brochureUrl);    
							     }
							     else if (!brochureUrl.matches(PlanMgmtConstants.URL_REGEX)) {
							      brochureUrl = PlanMgmtConstants.HTTP_PROTOCOL + brochureUrl;    
							     }
							planResponse.setBrochure(brochureUrl);
						}
						else{
							planResponse.setBrochure(PlanMgmtConstants.EMPTY_STRING);
						}
						//planResponse.setCsrMultiplier(planMgmtService.getCSRMultiplier(planData.getPlanHealth().getCostSharing(), planData.getPlanHealth().getPlanLevel(), planData.getApplicableYear()));
						
						/*	code changes for HIX-35798	*/
						if(planData.getApplicableYear() < PlanMgmtConstants.TWO_ZERO_ONE_FIVE){
							// CSR amount is applicable only for QHP
							if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString()) && planData.getPlanHealth().getCsrAdvPayAmt() != null && planData.getPlanHealth().getCsrAdvPayAmt() > 0 ){
							    planResponse.setCsrAmount(planData.getPlanHealth().getCsrAdvPayAmt());
							}
							else{
								 planResponse.setCsrAmount((float) 0.00);
							}
						}else if(planData.getApplicableYear() >= PlanMgmtConstants.TWO_ZERO_ONE_FIVE){
							if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
								planResponse.setCsrAmount(planMgmtService.getCSRMultiplier(planData.getPlanHealth().getCostSharing(), planData.getPlanHealth().getPlanLevel(), planData.getApplicableYear()));	
							} else if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
								planResponse.setCsrAmount(planMgmtService.getCSRMultiplier(planData.getPlanDental().getCostSharing(), planData.getPlanDental().getPlanLevel(), planData.getApplicableYear()));
							}	
						}
						
						
						if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
							planResponse.setPlanLevel(planData.getPlanHealth().getPlanLevel());
							planResponse.setCostSharing(planData.getPlanHealth().getCostSharing());
						}
						if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
							planResponse.setPlanLevel((planData.getPlanDental().getPlanLevel() == null) ? "" : planData.getPlanDental().getPlanLevel());
							planResponse.setCostSharing(planData.getPlanDental().getCostSharing());
						}
						
						String planRegionName = "";
						if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString()) || planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
							planRegionName = planMgmtService.getRegionName(planData.getId(),planData.getInsuranceType());											
						}
						planResponse.setPlanRegionName(planRegionName);
						planResponse.setPlanMarket(planData.getMarket());
						planResponse.setNetworkType(planData.getNetworkType());
						planResponse.setExchangeType(planData.getExchangeType());
						planResponse.setPlanYear(planData.getApplicableYear());
						planResponse.setNonCommissionFlag(planData.getNonCommissionFlag() == null ? "N" : planData.getNonCommissionFlag());
						
						// HIX-101472 ::Enhance GetInfo API to include additional commission fields for Enrollment module
					    // if commEffectiveDate exists in request then pull issuer commission
						if(StringUtils.isNotBlank(commEffectiveDate)){
							IssuerCommission issuerCommissionObj = issuerCommissionService.getIssuerCommission(planData.getIssuer().getId(), planData.getInsuranceType(), commEffectiveDate);
							if(null != issuerCommissionObj){
								planResponse.setFirstYearCommDollarAmt(issuerCommissionObj.getCommissionDollarFirstYear());
								planResponse.setFirstYearCommPercentageAmt(issuerCommissionObj.getCommissionPercentageFirstYear());
								planResponse.setSecondYearCommDollarAmt(issuerCommissionObj.getCommissionDollarSecondYear());
								planResponse.setSecondYearCommPercentageAmt(issuerCommissionObj.getCommissionPercentageSecondYear());
								planResponse.setFrequency(issuerCommissionObj.getFrequency());
							}
						}
						// End of HIX-101472
						
						String deductibleCost = "NA";
						String deductibleCostFamily = "NA";
						String maxOopCost = "NA";
						String maxOopCostFamily = "NA";
						
						// pull plan benefit and cost data for HEALTH plan
						if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
							String effectiveDate = planData.getStartDate().toString();					
							
							// ref HIX-85509 send max coinsurance for specialty drugs 
							planResponse.setMaxCoinsForSpecialtyDrugs((planData.getPlanHealth().getMaxCoinsuranceForSPDrug() == null) ? PlanMgmtConstants.EMPTY_STRING : String.valueOf(planData.getPlanHealth().getMaxCoinsuranceForSPDrug()));
							
							// HIX-76237, pull plan benefit and cost data when minimizePlanData is false
							if(!minimizePlanData){
								List<Integer> planHealthIdList = new ArrayList<Integer>();
								planHealthIdList.add(planData.getPlanHealth().getId());
								
								Map<Integer, Map<String, Map<String, String>>> planBenefitData = planCostAndBenefitsService.getOptimizedHealthPlanBenefits(planHealthIdList, effectiveDate);
								Map<Integer, Map<String, Map<String, String>>> planCostsData  = planCostAndBenefitsService.getOptimizedHealthPlanCosts(planHealthIdList);
								Map<String, Map<String, String>> planBenefits = planBenefitData.get(planData.getPlanHealth().getId());
								Map<String, Map<String, String>> planCosts = planCostsData.get(planData.getPlanHealth().getId());
								
								if(null != planBenefits.get(PlanMgmtConstants.PRIMARY_VISIT)){
									planResponse.setOfficeVisit(planBenefits.get(PlanMgmtConstants.PRIMARY_VISIT).get("netWkT1Disp"));
									planResponse.setOfficeVisitCost(planBenefits.get(PlanMgmtConstants.PRIMARY_VISIT).get("tier1CopayVal"));
								}
								if(null != planBenefits.get(PlanMgmtConstants.GENERIC)){
									planResponse.setGenericMedications(planBenefits.get(PlanMgmtConstants.GENERIC).get("netWkT1Disp"));
									planResponse.setGenericDrugs(planBenefits.get(PlanMgmtConstants.GENERIC).get("tier1CopayVal"));
								}
								
								// if DEDUCTIBLE_MEDICAL benefit does not exists look for DEDUCTIBLE_INTG_MED_DRUG						
								if(null != planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL)){  
									deductibleCost = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL).get("inNetworkInd")) ;
									deductibleCostFamily = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL).get("inNetworkFly")) ;
								}else if(null != planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG)){
									deductibleCost = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG).get("inNetworkInd")) ;
									deductibleCostFamily = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG).get("inNetworkFly")) ;
								}
								
								// if MAX_OOP_MEDICAL benefit does not exists look for MAX_OOP_INTG_MED_DRUG
								if(null != planCosts.get(PlanMgmtConstants.MAX_OOP_MEDICAL)){
									maxOopCost = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.MAX_OOP_MEDICAL).get("inNetworkInd")) ;
									maxOopCostFamily = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.MAX_OOP_MEDICAL).get("inNetworkFly")) ;
								}else if(null != planCosts.get(PlanMgmtConstants.MAX_OOP_INTG_MED_DRUG)){
									maxOopCost = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.MAX_OOP_INTG_MED_DRUG).get("inNetworkInd")) ;
									maxOopCostFamily = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.MAX_OOP_INTG_MED_DRUG).get("inNetworkFly")) ;
								}
								
								planResponse.setDeductible(deductibleCost);
								planResponse.setDeductibleFamily(deductibleCostFamily);
								planResponse.setOopMax(maxOopCost);		
								planResponse.setOopMaxFamily(maxOopCostFamily);
							}	
							
						}else if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
							// pull plan benefit and cost data for DENTAL plan						
							// HIX-76237, pull plan benefit and cost data when minimizePlanData is false
							if(!minimizePlanData){
								int tmpPlanId = planData.getPlanDental().getId();
								ArrayList<Integer> planDentalIdList = new ArrayList<Integer>();
								planDentalIdList.add(tmpPlanId);
								
								Map<Integer, Map<String, Map<String, String>>> planCostsData  = planCostAndBenefitsService.getOptimizedDentalPlanCosts(planDentalIdList, false);
								Map<String, Map<String, String>> planCosts = planCostsData.get(planData.getPlanDental().getId());
								
								planResponse.setOfficeVisit("NA");
								planResponse.setGenericMedications("NA");
								
								// if DEDUCTIBLE_MEDICAL benefit does not exists look for DEDUCTIBLE_INTG_MED_DRUG						
								if(null != planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL)){  
									deductibleCost = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL).get("inNetworkInd")) ;
									deductibleCostFamily = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL).get("inNetworkFly")) ;
								}else if(null != planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG)){
									deductibleCost = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG).get("inNetworkInd")) ;
									deductibleCostFamily = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG).get("inNetworkFly") );
								}						
							
								// if MAX_OOP_MEDICAL benefit does not exists look for MAX_OOP_INTG_MED_DRUG
								if(null != planCosts.get(PlanMgmtConstants.MAX_OOP_MEDICAL)){
									maxOopCost = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.MAX_OOP_MEDICAL).get("inNetworkInd")) ;
									maxOopCostFamily = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.MAX_OOP_MEDICAL).get("inNetworkFly")) ;
								}else if(null != planCosts.get(PlanMgmtConstants.MAX_OOP_INTG_MED_DRUG)){
									maxOopCost = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.MAX_OOP_INTG_MED_DRUG).get("inNetworkInd")) ;
									maxOopCostFamily = PlanMgmtUtil.removeDecimal(planCosts.get(PlanMgmtConstants.MAX_OOP_INTG_MED_DRUG).get("inNetworkFly")) ;
								}
								
								planResponse.setDeductible(deductibleCost);
								planResponse.setDeductibleFamily(deductibleCostFamily);
								planResponse.setOopMax(maxOopCost);	
								planResponse.setOopMaxFamily(maxOopCostFamily);
							}	
							
							// If consider EHB is ON, then send Pediatric Dental component in response. Ref HIX-39220 
							// send Pediatric Dental component if exists
							if(considerEHBPortion.equalsIgnoreCase("Y") && planData.getPlanDental().getEhbApptForPediatricDental() != null){
								planResponse.setPediatricDentalComponent(planData.getPlanDental().getEhbApptForPediatricDental().replace("$", ""));
							}	
							
						}else if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.STM.toString())){										
							planResponse.setCoinsurance(null == planData.getPlanStm().getCoinsurance() ? PlanMgmtConstants.ZERO_STR : planData.getPlanStm().getCoinsurance().toString());
							planResponse.setOopMax(planData.getPlanStm().getOopMaxValue().toString());		
							planResponse.setOopMaxFamily(planData.getPlanStm().getOopMaxFmly().toString());
							
							// pull plan benefit and cost data for STM plan
							// HIX-76237, pull plan benefit and cost data when minimizePlanData is false
							if(!minimizePlanData){
								Map<String, Map<String, Map<String, String>>> planBenefits = stmPlanRateBenefitService.getStmPlanBenefits(Integer.toString(planData.getPlanStm().getId())); // fetch stm plan benefits from db 
								Map<String, Map<String, Map<String, String>>> planCosts = stmPlanRateBenefitService.getStmPlanCosts(Integer.toString(planData.getPlanStm().getId()));  // fetch stm plan cost from db
								String stmPlanId = String.valueOf(planData.getPlanStm().getId());														
													
								if(null != planBenefits.get(stmPlanId)){	
									planResponse.setOfficeVisit(planBenefits.get(stmPlanId).get(PlanMgmtConstants.OFFICE_VISIT).get("netWkT1Disp"));
									planResponse.setOfficeVisitCost(planBenefits.get(stmPlanId).get(PlanMgmtConstants.OFFICE_VISIT).get("tier1CopayVal"));						
									planResponse.setGenericMedications(planBenefits.get(stmPlanId).get(PlanMgmtConstants.GENERIC).get("netWkT1Disp"));
									planResponse.setGenericDrugs(planBenefits.get(stmPlanId).get(PlanMgmtConstants.GENERIC).get("tier1CopayVal"));
								}
								
								if(null != planCosts.get(stmPlanId)){							
									deductibleCost = planCosts.get(stmPlanId).get(PlanMgmtConstants.DEDUCTIBLE).get("IN_NETWORK_IND");
									deductibleCostFamily = planCosts.get(stmPlanId).get(PlanMgmtConstants.DEDUCTIBLE).get("IN_NETWORK_FLY");
								}
								
								planResponse.setDeductible(deductibleCost);
								planResponse.setDeductibleFamily(deductibleCostFamily);
							}	
							
						} else if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.AME.toString())){
							// pull plan benefit and cost data for AME plan
							if(null != planData.getPlanAme().getDeductibleValue() && null != planData.getPlanAme().getDeductibleAttribute()) {
								planResponse.setDeductible(planData.getPlanAme().getDeductibleValue().toString());
								planResponse.setDeductibleAttrib(planData.getPlanAme().getDeductibleAttribute().toString());		
							}
							if(null != planData.getPlanAme().getBenefitMaxValue() && null != planData.getPlanAme().getBenefitMaxAttribute()) {
								planResponse.setMaxBenefitVal(planData.getPlanAme().getBenefitMaxValue().toString());	
								planResponse.setMaxBenefitAttrib(planData.getPlanAme().getBenefitMaxAttribute().toString());
							}			
							if(null != planData.getPlanAme().getExclusionURL()) {
								String exclusionsAndLimitations = planData.getPlanAme().getExclusionURL();							
							     if (exclusionsAndLimitations.matches(PlanMgmtConstants.ECM_REGEX)) {
							      exclusionsAndLimitations = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(exclusionsAndLimitations);    
							     } else if (!exclusionsAndLimitations.matches(PlanMgmtConstants.URL_REGEX)) {
							      exclusionsAndLimitations = PlanMgmtConstants.HTTP_PROTOCOL + exclusionsAndLimitations;    
							     }	
								planResponse.setLmitationAndExclusions(exclusionsAndLimitations);
							}
							
							// HIX-76237, pull plan benefit and cost data when minimizePlanData is false
							if(!minimizePlanData){
								Map<String, Map<String, String>> ameBenefits = amePlanBenefitService.getAmePlanBenefits(planData.getPlanAme().getId());
								if(null != ameBenefits && !ameBenefits.isEmpty()) {
									planResponse.setPlanBenefits(ameBenefits);		
								}
							}	
							
						}else if(Plan.PlanInsuranceType.LIFE.toString().equalsIgnoreCase(planData.getInsuranceType()) ){ // HIX-84828 send life plan attributes
							planResponse.setPolicyTerm(String.valueOf(planData.getPlanLife().getPlanDuration()));
							planResponse.setBenefitUrl(planData.getPlanLife().getBenefitURL());
						}
						
						
						planResponse.setHsa((null != planData.getHsa()) ? planData.getHsa().toString() : planData.getHsa());					
							
						
						// If consider EHB is ON, then compute and send EHB percentage in response. Ref HIX-39220 
						if(considerEHBPortion.equalsIgnoreCase("Y") && PlanMgmtConstants.HEALTH.equalsIgnoreCase(planData.getInsuranceType())){						
							// set default value as 1 for EHB percentage and 0 for state mandate EHB percentage
							planResponse.setEhbPercentage(PlanMgmtConstants.ONE_STR);
							planResponse.setStateEhbMandatePercentage(PlanMgmtConstants.STRZERO);		
							
							// HIX-65077, fetch ehb portion from plan table.
							if(null != planData.getEhbPremiumFraction()){
								planResponse.setEhbPercentage(planData.getEhbPremiumFraction().toString());
							}
							// we are not using URRT template no more.
							/*else if(null != planData.getUnifiedPlanRateChangeId() && StringUtils.isNotBlank(planData.getUnifiedPlanRateChangeId().toString())){
								List<Object[]> URRTObjectInfo =	iURRTPremiumClaimRepository.getEhbPercentage(planData.getUnifiedPlanRateChangeId());
								if(URRTObjectInfo != null){
									planResponse.setEhbPercentage(URRTObjectInfo.get(PlanMgmtConstants.ZERO)[PlanMgmtConstants.ZERO].toString());
									planResponse.setStateEhbMandatePercentage(URRTObjectInfo.get(PlanMgmtConstants.ZERO)[PlanMgmtConstants.ONE].toString());
								}
							}*/
						}
						
						planResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						planResponse.endResponse();
					}else{
						planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLAN_INFO.getCode());
						planResponse.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
					}
					
				}catch(Exception ex) {
					giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch plan info.",ex);
					LOGGER.error("Fail to fetch plans. Exception:", ex);					
					planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLAN_INFO.getCode());
					planResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}				
										
		}
		
		return planResponse.toString();
	}

	/**
	 * REST-API is used to get Plan info by Plan IDs.
	 */
	@RequestMapping(value = "/getInfoByPlanIds", method = RequestMethod.POST)
	@ResponseBody
	public PlanListResponse getPlanInfoByPlanIds(@RequestBody PlanRequest request) throws GIException {

		LOGGER.info("Begin execution of getPlanInfoByPlanIds API.");
		PlanListResponse response = new PlanListResponse();
		response.startResponse();

		try {
			planMgmtService.getPlanInfoDataByPlanIds(request, response);
		}
		finally {

			if (HttpStatus.OK.equals(response.getStatusCode())) {
				LOGGER.info(response.getMessage());
			}
			else {
				LOGGER.error(response.getMessage());
			}
			response.endResponse();
			LOGGER.info("End execution of getPlanInfoByPlanIds API.");
		}
		return response;
	}

//	@ApiOperation(value="getdetails", notes="This API takes Plan Id and effective date and returns Plan Information, Issuer Information, Plan Benefits and Plan Costs")
//	@ApiResponse(value="Returning PlanResonse with Plan Information, Benefits Information and Cost Information")
	@RequestMapping(value = "/getdetails", method = RequestMethod.POST)
	@Produces("application/json;charset=UTF-8")
	@ResponseBody public String getPlanDetails(@RequestBody PlanRequest planRequest)throws  GIException	
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getPlanDetails() ============ ");
		}
		PlanResponse planResponse = new PlanResponse();
		// if request data elements are null throw error
		if(null == planRequest || StringUtils.isBlank(planRequest.getPlanId()) || !StringUtils.isNumeric(planRequest.getPlanId())) 
		{
			planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}else{
			try{
				if(!StringUtils.isBlank(planRequest.getEffectiveDate())){
					SimpleDateFormat sdf = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT);
					sdf.setLenient(false);
	    			sdf.parse(planRequest.getEffectiveDate().trim());
				}
    			planResponse = getPlanDetailsHelper(planRequest);
			}catch(Exception ex){
				planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_EFFECTIVE_DATE.code);
				planResponse.setErrMsg(PlanMgmtConstants.INVALID_EFFECTIVE_DATE_FORMAT);
			}
		}
		
		return planResponse.toString();
	}
	
	
	public PlanResponse getPlanDetailsHelper(PlanRequest planRequest)throws  GIException{
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Request data :: plan Id : " + SecurityUtil.sanitizeForLogging(planRequest.getPlanId()) + ", Effective date :" +  SecurityUtil.sanitizeForLogging(planRequest.getEffectiveDate()));
		}
		PlanResponse planResponse = new PlanResponse();

		String planId = planRequest.getPlanId().trim();
		String effectiveDate;
		String considerEHBPortion = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CONSIDER_EHB_PORTION);
		String computeIssuerQualityRating = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.COMPUTEISSUERQUALITYRATING);
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		
		try{
			long time = System.currentTimeMillis();

			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Fetching plan data....");
			}
			Plan planData = planMgmtService.getPlanData(planId);
			
			if(null != planData){
				//convert date to String
				if(StringUtils.isBlank(planRequest.getEffectiveDate())){
					if(null != planData.getStartDate()) {
						effectiveDate = DateUtil.dateToString(planData.getStartDate(), "yyyy-MM-dd");
					} else {
						effectiveDate = DateUtil.dateToString(new TSDate(), "yyyy-MM-dd");
					}
				}
				else{
					effectiveDate = planRequest.getEffectiveDate().trim();
				}
				planResponse.setPlanId(planData.getId());
				planResponse.setPlanName(planData.getName());					
				planResponse.setIssuerId(planData.getIssuer().getId());
				planResponse.setIssuerName(planData.getIssuer().getName());					
				planResponse.setIssuerPlanNumber(planData.getIssuerPlanNumber());
				planResponse.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(planData.getIssuer().getLogoURL(), planData.getIssuer().getHiosIssuerId(), null, appUrl));
				String marketType = (planData.getMarket() == null) ? PlanMgmtConstants.EMPTY_STRING : planData.getMarket();
	
				if(marketType.equalsIgnoreCase(Plan.PlanMarket.SHOP.toString())){						
					planResponse.setIssuerSite(planData.getIssuer().getShopSiteUrl());
					planResponse.setIssuerPhone(planData.getIssuer().getShopCustServicePhone());
				}
				if(marketType.equalsIgnoreCase(Plan.PlanMarket.INDIVIDUAL.toString())){						
					planResponse.setIssuerSite(planData.getIssuer().getIndvSiteUrl());
					planResponse.setIssuerPhone(planData.getIssuer().getIndvCustServicePhone());
				}
				
				
				if(planData.getBrochure() != null) {
					String brochureUrl = planData.getBrochure();
					if (brochureUrl.matches(PlanMgmtConstants.ECM_REGEX)) {
						brochureUrl = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(brochureUrl);    
					}
					else if (!brochureUrl.matches(PlanMgmtConstants.URL_REGEX)) {
						brochureUrl = PlanMgmtConstants.HTTP_PROTOCOL + brochureUrl;    
					}
					planResponse.setBrochure(brochureUrl);
				}else{
					planResponse.setBrochure(PlanMgmtConstants.EMPTY_STRING);
				}
	
				// send formulary URL
				if(planData.getFormularlyId() != null){
					String formularyUrl = getFormularyURL(Integer.parseInt(planData.getFormularlyId()));
					planResponse.setFormularyUrl(formularyUrl);					
				}else{
					planResponse.setFormularyUrl(PlanMgmtConstants.EMPTY_STRING);	
				}
				
				String providerUrl = (planData.getProviderNetworkId() == null || planData.getProviderNetworkId().toString().isEmpty() ) ? PlanMgmtConstants.EMPTY_STRING : planMgmtService.getProviderURL(String.valueOf(planData.getProviderNetworkId()));
				/* Adding Regular expression for adding HTTP for HIX-44407 */
				if (providerUrl != null && !providerUrl.equals("") && !providerUrl.matches(PlanMgmtConstants.URL_REGEX)) {
					providerUrl = PlanMgmtConstants.HTTP_PROTOCOL + providerUrl;				
				}
				planResponse.setProviderUrl(providerUrl);	
				planResponse.setPlanYear(planData.getApplicableYear());
				
				List<String> costList = planRateBenefitService.getCostNames();
				// if insurance type is HEALTH then pull SBC url and CSR information
				if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
					String sbcDocURL = "";
					String benefitURL = "";
					if (planData.getPlanHealth().getSbcUcmId() != null) { 
						sbcDocURL = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(planData.getPlanHealth().getSbcUcmId());
					}	
					if (planData.getPlanHealth().getBenefitsUrl() != null) {	
						benefitURL = (isURLValid(planData.getPlanHealth().getBenefitsUrl())) ? planData.getPlanHealth().getBenefitsUrl() :  appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(planData.getPlanHealth().getBenefitsUrl()) ;					
					}
	
					String returnedString = PlanMgmtUtil.returnOneString(sbcDocURL, benefitURL);
					planResponse.setSbcDocUrl(returnedString);
					planResponse.setCostSharing(planData.getPlanHealth().getCostSharing());
					String tier2util = planData.getPlanHealth().getTier2Util();
					planResponse.setTier2util(tier2util != null ? tier2util : PlanMgmtConstants.EMPTY_STRING);
					
					// compute issuer quality rating where computeIssuerQualityRating flag value is YES
					if (computeIssuerQualityRating.equalsIgnoreCase(PlanMgmtConstants.YES)) {
						List<String> hiosPlaIdList = new ArrayList<String>();
						hiosPlaIdList.add(planData.getIssuerPlanNumber());
						Map<String, Map<String, String>> issuerQualityRating = new LinkedHashMap<String, Map<String, String>>();
						issuerQualityRating = issuerQualityRatingService.getIssuerQualityRatingByHIOSPlanIdList(hiosPlaIdList, effectiveDate, stateCode);
						planResponse.setIssuerQualityRating(issuerQualityRating.get(planData.getIssuerPlanNumber()));
					}
					
					
					// HIX-87498 : compute Network Transparency Rating  
					//List<String> hiosPlanIdListForNetworkTransparencyRating =  new ArrayList<String>();
					//hiosPlanIdListForNetworkTransparencyRating.add(planData.getIssuerPlanNumber().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FOURTEEN));
					
					// don't call network transparency rating service  in plan details API. Because caller doesn't have county code in request
					//networkTansparencyRatingService.getNetworkTansparencyRatingByHIOSPlanIdList(hiosPlanIdListForNetworkTransparencyRating, countyFips, planData.getApplicableYear());
					
					
					planResponse.setPlanLevel(planData.getPlanHealth().getPlanLevel());
					ArrayList<Integer> planHealthIdList = new ArrayList<Integer>();
					int tempPlanId = planData.getPlanHealth().getId();
					if( tempPlanId> 0){	
						planHealthIdList.add(tempPlanId);
						// pull health plan benefit and cost
						Map<Integer, Map<String, Map<String, String>>> planBenefitData = planCostAndBenefitsService.getOptimizedHealthPlanBenefits(planHealthIdList, effectiveDate);
						Map<Integer, Map<String, Map<String, String>>> planCostsData  = planCostAndBenefitsService.getOptimizedHealthPlanCosts(planHealthIdList);
						
						planResponse.setPlanBenefits(planBenefitData.get(planData.getPlanHealth().getId()));
						Map<String, Map<String, String>> planCosts = planCostsData.get(planData.getPlanHealth().getId());					
						Map<String, Map<String,String>> planCostMap = new HashMap<String, Map<String,String>>();
						Map<String, Map<String,String>> optionalPlanCostMap = new HashMap<String, Map<String,String>>();
						Set<String> costNameSet = planCosts.keySet();
						
						for (String costName: costNameSet){
							if(costList.contains(costName)){
								planCostMap.put(costName, planCosts.get(costName));
							}else{
								optionalPlanCostMap.put(costName, planCosts.get(costName));
							}
						}
						planResponse.setPlanCosts(planCostMap);
						planResponse.setOptionalDeductible(optionalPlanCostMap);
					}
					
					// ref HIX-85509 send max coinsurance for specialty drugs 
					 planResponse.setMaxCoinsForSpecialtyDrugs((planData.getPlanHealth().getMaxCoinsuranceForSPDrug() == null) ? PlanMgmtConstants.EMPTY_STRING : String.valueOf(planData.getPlanHealth().getMaxCoinsuranceForSPDrug()));
					
					 SbcScenarioDTO sbcScenarioDTO = null; 
					 planResponse.setSbcScenarioDTO(planSbcScenarioService.setSbcScenario(planData.getPlanHealth().getPlanSbcScenario(),sbcScenarioDTO));
					 
				}
	
				// if insurance type is DENTAL then pull SBC url 
				if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
					String sbcDocURL = "";
					String benefitURL = "";
					if (planData.getPlanDental().getSbcUcmId() != null) { 
						sbcDocURL = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(planData.getPlanDental().getSbcUcmId());
					}
	
					if (planData.getPlanDental().getBenefitsUrl() != null) {	
						benefitURL = (isURLValid(planData.getPlanDental().getBenefitsUrl())) ? planData.getPlanDental().getBenefitsUrl() : appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(planData.getPlanDental().getBenefitsUrl()) ;					
					}
	
					String returnedString = PlanMgmtUtil.returnOneString(sbcDocURL, benefitURL);
					planResponse.setSbcDocUrl(returnedString);
					String tier2util = planData.getPlanDental().getTier2Util();
					planResponse.setTier2util(tier2util != null ? tier2util : PlanMgmtConstants.EMPTY_STRING);
					planResponse.setCostSharing(planData.getPlanDental().getCostSharing());
	
					planResponse.setPlanLevel((planData.getPlanDental().getPlanLevel() == null) ? "" : planData.getPlanDental().getPlanLevel());
					if(planData.getPlanDental().getId() > 0){
						ArrayList<Integer> planDentalIdList = new ArrayList<Integer>();
						planDentalIdList.add(planData.getPlanDental().getId());
						// pull dental plan benefit and cost
						Map<Integer, Map<String, Map<String, String>>> planBenefitData = planCostAndBenefitsService.getOptimizedDentalPlanBenefits(planDentalIdList, effectiveDate);
						Map<Integer, Map<String, Map<String, String>>> planCostsData  = planCostAndBenefitsService.getOptimizedDentalPlanCosts(planDentalIdList, false);
						
						Map<String, Map<String, String>> planCosts = planCostsData.get(planData.getPlanDental().getId());
						planResponse.setPlanBenefits(planBenefitData.get(planData.getPlanDental().getId()));
						Map<String, Map<String,String>> planCostMap = new HashMap<String, Map<String,String>>();
						Map<String, Map<String,String>> optionalPlanCostMap = new HashMap<String, Map<String,String>>();
						Set<String> costNameSet = planCosts.keySet();
						
						for (String costName: costNameSet){
							if(costList.contains(costName)){
								planCostMap.put(costName, planCosts.get(costName));
							}else{
								optionalPlanCostMap.put(costName, planCosts.get(costName));
							}
						}
						planResponse.setPlanCosts(planCostMap);
						planResponse.setOptionalDeductible(optionalPlanCostMap);
					}	
					planResponse.setGuaranteedVsEstimatedRate(planData.getPlanDental().getGuaranteedVsEstimatedRate() != null ? planData.getPlanDental().getGuaranteedVsEstimatedRate() : "" );
					
					// If consider EHB is ON, then send Pediatric Dental component in response. Ref HIX-39220 
					// send Pediatric Dental component if exists
					if(considerEHBPortion.equalsIgnoreCase("Y") && planData.getPlanDental().getEhbApptForPediatricDental() != null){
						planResponse.setPediatricDentalComponent(planData.getPlanDental().getEhbApptForPediatricDental().replace("$",""));
					}
				}		
				
				// when insurance type is STM
				if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.STM.toString())){
					planResponse.setCoinsurance(planData.getPlanStm().getCoinsurance() == null ? PlanMgmtConstants.ZERO_STR : planData.getPlanStm().getCoinsurance().toString());
					planResponse.setPolicyLength(planData.getPlanStm().getMaxDuration() == null ? PlanMgmtConstants.ZERO_STR : planData.getPlanStm().getMaxDuration().toString());
					planResponse.setOopMax(planData.getPlanStm().getOopMaxValue() == null ? PlanMgmtConstants.ZERO_STR : planData.getPlanStm().getOopMaxValue().toString());
					planResponse.setPolicyLengthUnit(planData.getPlanStm().getDurationUnit() == null ? PlanMgmtConstants.EMPTY_STRING : planData.getPlanStm().getDurationUnit());
					planResponse.setPolicyLimit(planData.getPlanStm().getPolicyMax() == null ? PlanMgmtConstants.ZERO_STR : planData.getPlanStm().getPolicyMax().toString());
					if(planData.getPlanStm().getOutOfNetworkCoverage() != null && planData.getPlanStm().getOutOfNetworkCoverage().equalsIgnoreCase(PlanMgmtConstants.Y)){
						planResponse.setOutOfNetwkCoverage(PlanMgmtConstants.YES);
					}else if(planData.getPlanStm().getOutOfNetworkCoverage() != null && planData.getPlanStm().getOutOfNetworkCoverage().equalsIgnoreCase(PlanMgmtConstants.N)){
						planResponse.setOutOfNetwkCoverage(PlanMgmtConstants.NO);
					}
	
					if(planData.getPlanStm().getOutOfCountryCoverage() != null && planData.getPlanStm().getOutOfCountryCoverage().equalsIgnoreCase(PlanMgmtConstants.Y)){
						planResponse.setOutOfCountyCoverage(PlanMgmtConstants.YES);
					}else if(planData.getPlanStm().getOutOfCountryCoverage() != null && planData.getPlanStm().getOutOfCountryCoverage().equalsIgnoreCase(PlanMgmtConstants.N)){
						planResponse.setOutOfCountyCoverage(PlanMgmtConstants.NO);
					}
	
					String separateDrugDeductible = planMgmtService.getSeparateDrugDeductibleForPlanSTMCost(planData.getPlanStm().getId());
					planResponse.setSeparateDrugDeductible(separateDrugDeductible == null ? PlanMgmtConstants.EMPTY_STRING : separateDrugDeductible);
					planResponse.setOopMaxAttr(planData.getPlanStm().getOopMaxAttr() == null ? PlanMgmtConstants.EMPTY_STRING : planData.getPlanStm().getOopMaxAttr());
					planResponse.setOopMaxFamily(planData.getPlanStm().getOopMaxFmly() == null ? PlanMgmtConstants.EMPTY_STRING : planData.getPlanStm().getOopMaxFmly());
	
					Map<String, Map<String, Map<String, String>>> planBenefits = stmPlanRateBenefitService.getStmPlanBenefits(Integer.toString(planData.getPlanStm().getId())); // fetch stm plan benefits from db 
					Map<String, Map<String, Map<String, String>>> stmPlanCosts = stmPlanRateBenefitService.getStmPlanCosts(Integer.toString(planData.getPlanStm().getId()));  // fetch stm plan cost from db
					String stmPlanId = String.valueOf(planData.getPlanStm().getId());														
					planResponse.setPlanBenefits(planBenefits.get(stmPlanId));
	
					Map<String, Map<String,String>> optionalPlanCostMap = new HashMap<String, Map<String,String>>();
					planResponse.setPlanCosts(stmPlanCosts.get(stmPlanId));
					planResponse.setOptionalDeductible(optionalPlanCostMap);
				}
				
				// when insurance type is AME
				if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.AME.toString())){
					if(null != planData.getPlanAme().getDeductibleValue() && null != planData.getPlanAme().getDeductibleAttribute()) {
						planResponse.setDeductible(planData.getPlanAme().getDeductibleValue().toString());
						planResponse.setDeductibleAttrib(planData.getPlanAme().getDeductibleAttribute().toString());		
					}
					if(null != planData.getPlanAme().getBenefitMaxValue() && null != planData.getPlanAme().getBenefitMaxAttribute()) {
						planResponse.setMaxBenefitVal(planData.getPlanAme().getBenefitMaxValue().toString());	
						planResponse.setMaxBenefitAttrib(planData.getPlanAme().getBenefitMaxAttribute().toString());
					}			
					if(null != planData.getPlanAme().getExclusionURL()) {
						String exclusionsAndLimitations = planData.getPlanAme().getExclusionURL();							
						if (exclusionsAndLimitations.matches(PlanMgmtConstants.ECM_REGEX)) {
							exclusionsAndLimitations = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(exclusionsAndLimitations);    
						} else if (!exclusionsAndLimitations.matches(PlanMgmtConstants.URL_REGEX)) {
							exclusionsAndLimitations = PlanMgmtConstants.HTTP_PROTOCOL + exclusionsAndLimitations;    
						}	
						planResponse.setLmitationAndExclusions(exclusionsAndLimitations);
					}
	
					Map<String, Map<String, String>> ameBenefits = amePlanBenefitService.getAmePlanBenefits(planData.getPlanAme().getId());
					if(null != ameBenefits && !ameBenefits.isEmpty()) {
						planResponse.setPlanBenefits(ameBenefits);					
	
					}
	
				}			
				
				// when insurance type is VISION
				if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.VISION.toString())){
					planResponse.setPremiumPayment(planData.getPlanVision().getPremiumPayment());				
					planResponse.setOutOfNetwkAvailability(planData.getPlanVision().getOutOfNetAvailability());
					planResponse.setOutOfNetwkCoverage(planData.getPlanVision().getOutOfNetCoverage());
					planResponse.setProviderUrl((planData.getPlanVision().getProviderNetURL() !=null) ? (!planData.getPlanVision().getProviderNetURL().matches(PlanMgmtConstants.URL_REGEX)) ? PlanMgmtConstants.HTTP_PROTOCOL + planData.getPlanVision().getProviderNetURL() : planData.getPlanVision().getProviderNetURL() : PlanMgmtConstants.EMPTY_STRING);
					Map<String, Map<String, Map<String, String>>> visionBenefits = visionPlanRateBenefitService.getVisionPlanBenefits(String.valueOf(planData.getPlanVision().getId()), PlanMgmtConstants.EMPTY_STRING);
					planResponse.setPlanBenefits(visionBenefits.get(String.valueOf(planData.getPlanVision().getId())));				
				}						
	
				// HIX-84829 send life plan attributes
				if(Plan.PlanInsuranceType.LIFE.toString().equalsIgnoreCase(planData.getInsuranceType()) ){ 
					planResponse.setPolicyTerm(String.valueOf(planData.getPlanLife().getPlanDuration()));
					planResponse.setBenefitUrl(planData.getPlanLife().getBenefitURL());
				}
				 
				planResponse.setPlanMarket(planData.getMarket());
				planResponse.setNetworkType(planData.getNetworkType());				
				planResponse.setInsuranceType(planData.getInsuranceType());
				planResponse.setExchangeType(planData.getExchangeType());
				planResponse.setHsa((planData.getHsa() != null) ? planData.getHsa().toString() : PlanMgmtConstants.EMPTY_STRING);
				String eocDocURL = null;
				if(null != planData.getEocDocId()) {
					if (planData.getEocDocId().matches(PlanMgmtConstants.ECM_REGEX)) {
						eocDocURL = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(planData.getEocDocId().toString());    
					} else {
						eocDocURL = planData.getEocDocId();    
					}
				} else {
					eocDocURL = PlanMgmtConstants.EMPTY_STRING;
				}
				
				planResponse.setEocDocUrl(eocDocURL); 
				
				// HIX-57015 :: Update '/getdetails' API to return ehbPercentage
				// If consider EHB is ON, then compute and send EHB percentage in response. Ref HIX-39220 
				if(considerEHBPortion.equalsIgnoreCase("Y") && PlanMgmtConstants.HEALTH.equalsIgnoreCase(planData.getInsuranceType())){						
					// set default value as 1 for EHB percentage and 0 for state mandate EHB percentage
					planResponse.setEhbPercentage(PlanMgmtConstants.ONE_STR);
					planResponse.setStateEhbMandatePercentage(PlanMgmtConstants.STRZERO);
					
					// HIX-65077, fetch ehb portion from plan table. no more use of URRT template
					if(planData.getEhbPremiumFraction() != null){
						planResponse.setEhbPercentage(planData.getEhbPremiumFraction().toString());
					}							
				}
	
				planResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				planResponse.endResponse();
				
			}else{
				planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLAN_DETAILS.getCode());
				planResponse.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
			}
			LOGGER.warn("Received getPlanDetailsHelper(), response for Plan ID " + planRequest.getPlanId() + " in  " + (System.currentTimeMillis() - time) + " ms");
		}
		catch(Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch plan details.",ex);
			LOGGER.error("Fail to fetch plans. Exception:", ex);					
			planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
			planResponse.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
		}				

		return planResponse;
	}
	
//	@ApiOperation(value="getPlanIssuerInfo", notes="This API takes Plan ID as input and provide plan and issuer information")
//	@ApiResponse(value="Returning PlanResonse with necessary Plan Information and Issuer Information")
	@RequestMapping(value = "/getPlanIssuerInfo", method = RequestMethod.POST)
	@ResponseBody public String getPlanIssuerInfo(@RequestBody PlanRequest planRequest)throws  GIException	
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getPlanIssuerInfo() ============ ");
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Request data :: plan Id : " + SecurityUtil.sanitizeForLogging(planRequest.getPlanId()) );
		}
		
		PlanResponse planResponse = new PlanResponse();
		// if request data elements are null throw error
		if(null == planRequest || null == planRequest.getPlanId()) {
			planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}else{
				String planId = planRequest.getPlanId();				
				try{
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("Fetching plan data....");
					}
					Plan planData = planMgmtService.getPlanIssuerData(planId);		
					
					if(null != planData){
						planResponse.setPlanId(planData.getId());
						planResponse.setPlanName(planData.getName());					
						planResponse.setIssuerId(planData.getIssuer().getId());
						planResponse.setIssuerName(planData.getIssuer().getName());
						planResponse.setIssuerPlanNumber(planData.getIssuerPlanNumber());						
						planResponse.setTaxIdNumber((planData.getIssuer().getFederalEin() != null) ? planData.getIssuer().getFederalEin().replace("-", "") : "");
						// CSR amount is applicable only for QHP
						if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString()) && planData.getPlanHealth().getCsrAdvPayAmt() != null){
						    planResponse.setCsrAmount(planData.getPlanHealth().getCsrAdvPayAmt());
						}
						else{
							 planResponse.setCsrAmount((float) 0.00);
						}
						if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
							planResponse.setPlanLevel(planData.getPlanHealth().getPlanLevel());
						}
						if(planData.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
							planResponse.setPlanLevel((planData.getPlanDental().getPlanLevel() == null) ? "" : planData.getPlanDental().getPlanLevel());
						}
						
						String planRegionName = planMgmtService.getRegionName(planData.getId(),planData.getInsuranceType());
						planResponse.setPlanRegionName(planRegionName);					
						planResponse.setPlanMarket(planData.getMarket());
						planResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						
					}else{
						planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLAN_ISSUER_INFO.getCode());
						planResponse.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
					}
					
					planResponse.endResponse();
					
				}catch(Exception ex) {
					LOGGER.error("Fail to fetch plans. Exception:" , ex);					
					planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLAN_ISSUER_INFO.getCode());
					planResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}				
				
		}
		
		return planResponse.toString();
	}
	
//	@ApiOperation(value="checkindvhealthplanavailability", notes="This API takes Zip, County, Effective Date and Exchange Type as input and returns true or false. If the plan is available it will return true else false")
//	@ApiResponse(value="Returning boolean information after checking availability of individual Health Plan")
	// check health plan availability on certain zip code, county code, effective date and exchange type combination
	@RequestMapping(value = "/checkindvhealthplanavailability", method = RequestMethod.POST)
	@ResponseBody public boolean checkIndvHealthPlanAvailability(@RequestBody PlanRequest planRequest)throws  GIException	
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside checkIndvHealthPlanAvailability() ============ ");
		}
		return checkIndvHealthPlanAvailabilityHelper(planRequest);
	}
	
	/**
	 * Non rest version
	 * @param planRequest
	 * @return
	 * @throws GIException
	 */
	// check health plan availability on certain zip code, county code, effective date and exchange type combination
	public boolean checkIndvHealthPlanAvailabilityHelper( PlanRequest planRequest)throws  GIException	
	{
		if(LOGGER.isDebugEnabled()) {
			if(null != planRequest) {
				StringBuilder requestparam  = new StringBuilder();  
				requestparam.append("REQUEST ==> zipCode: " + SecurityUtil.sanitizeForLogging(planRequest.getZip()));
				requestparam.append(", countyCode:");
				requestparam.append(SecurityUtil.sanitizeForLogging(planRequest.getCountyFIPS()));
				requestparam.append(", effective date:");
				requestparam.append(SecurityUtil.sanitizeForLogging(planRequest.getEffectiveDate()));
				requestparam.append(", exchangeType:");
				requestparam.append(SecurityUtil.sanitizeForLogging(planRequest.getExchangeType()));
				LOGGER.debug(requestparam.toString());
			} else {
				LOGGER.debug("checkIndvHealthPlanAvailabilityHelper: Input param is null");
			}
		}
		
		boolean isAvailable = false;
		// if request data elements are null then throw error
		if(null == planRequest 
			|| null == planRequest.getZip() 
			|| null == planRequest.getCountyFIPS() 
			|| null == planRequest.getEffectiveDate() 
			|| null == planRequest.getExchangeType()) {						
			LOGGER.error(PlanMgmtConstants.INVALID_REQUEST);
		}else{
				String zip = planRequest.getZip().trim();
				String countyFips = planRequest.getCountyFIPS().trim();
				String effectiveDate = planRequest.getEffectiveDate().trim();
				String exchangeType = planRequest.getExchangeType().trim();
				String csrValue = (planRequest.getCsrValue() == null || planRequest.getCsrValue().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.CS1 : planRequest.getCsrValue().trim();  // default value is CS1
				String tenantCode = (planRequest.getTenantCode() == null || planRequest.getTenantCode().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.TENANT_GINS : planRequest.getTenantCode().trim() ; // default value is GINS
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug(" csrValue =" + SecurityUtil.sanitizeForLogging(csrValue) + ", tenantCode = "  + SecurityUtil.sanitizeForLogging(tenantCode));
				}
				try{
					isAvailable = planMgmtService.checkIndvHealthPlanAvailability(zip, countyFips, effectiveDate, exchangeType, csrValue, tenantCode);			
									
				}catch(Exception ex) {
					LOGGER.error("Fail to fetch plans. Exception:", ex);
				}		
			
		}
		
		return isAvailable;
	}
	
//	@ApiOperation(value="getplansbyissuer", notes="This operation retrieves plans for a specific issuer")
//	@ApiResponse(value="Returning PlanResoponse with list plan present for that issuer")
	@RequestMapping(value = "/getplansbyissuer", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody
	PlanResponse getPlansByIssuer(@RequestBody PlanRequest planRequest) {
	PlanResponse planResponse = new PlanResponse();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Beginning to process getPlansByIssuer().");
		}
		
		if(null == planRequest || null == planRequest.getZip() || planRequest.getZip().equals(PlanMgmtConstants.EMPTY_STRING) || null == planRequest.getIssuerName() || planRequest.getIssuerName().equals(PlanMgmtConstants.EMPTY_STRING)) {
			planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}else{
			String zipCode = planRequest.getZip().trim();
			String countyCode = (planRequest.getCountyFIPS() == null) ? "" : planRequest.getCountyFIPS().trim();
			String issuerName = planRequest.getIssuerName().trim();
			String planLevel = (planRequest.getPlanLevel() == null ) ? "" : planRequest.getPlanLevel().trim();
			String insuranceType = (planRequest.getInsuranceType() == null || planRequest.getInsuranceType().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? Plan.PlanInsuranceType.HEALTH.toString() : planRequest.getInsuranceType().trim(); // HEALTH is default insurance type
			String tenantCode = (planRequest.getTenantCode() == null || planRequest.getTenantCode().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.TENANT_GINS : planRequest.getTenantCode().trim() ; // default value is GINS
			
			if(LOGGER.isDebugEnabled()) {
				StringBuilder requestparam  = new StringBuilder();  
				requestparam.append("REQUEST ==> zipCode: " + SecurityUtil.sanitizeForLogging(zipCode));
				requestparam.append(", countyCode:");
				requestparam.append(SecurityUtil.sanitizeForLogging(countyCode));
				requestparam.append(", issuerName:");
				requestparam.append(SecurityUtil.sanitizeForLogging(issuerName));
				requestparam.append(", planLevel:");
				requestparam.append(SecurityUtil.sanitizeForLogging(planLevel));
				requestparam.append(", insuranceType:");
				requestparam.append(SecurityUtil.sanitizeForLogging(insuranceType));
				requestparam.append(", tenantCode: ");
				requestparam.append(SecurityUtil.sanitizeForLogging(tenantCode));
				LOGGER.debug(requestparam.toString());
			}
			
			// this API is applicable only for Health and Dental plans
			if(Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(insuranceType) || Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(insuranceType) ){
				try {
					List planList = planMgmtService.getPlansByZipAndIssuerName(zipCode, countyCode, insuranceType, issuerName, planLevel, tenantCode);
					planResponse.setPlanList(planList);
					planResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				} catch (Exception e) {
					LOGGER.error("Exception occured while getting byzipcode",e);
					planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLANS_BY_ZIP_AND_ISSUERNAME.getCode());
					planResponse.setErrMsg(e.getStackTrace().toString());
					planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}else{
				planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				planResponse.setErrMsg("Invalid request parameters.");
			}
				
		}	

		return planResponse;
	}
//	
//	@ApiOperation(value="updateEnrollmentAvaility", notes="This API does not required any input and returns true/false based on enrollment availability status change. ")
//	@ApiResponse(value="Returning String ture/false After updting enrollment availability of all plans")
	@RequestMapping(value = "/updateEnrollmentAvaility", method=RequestMethod.POST)
	@ResponseBody public String updateEnrollmentAvaility() {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside Controllers method updateEnrollmentAvaility() ============ ");
		}
		return planMgmtService.updateEnrollmentAvailability();
	}
	
//	@ApiOperation(value="getbenefitcostdata", notes="This API takes Effective Date, Insurance Type and Plan Id's and returns Plan Details, Issuer Details, Plan Benefits and Costs")
//	@ApiResponse(value="Returning list of PlanResonse with benefit information and cost inforation of provided planIds")
	@RequestMapping(value = "/getbenefitcostdata", method = RequestMethod.POST)
	@ResponseBody public String getBenefitCostData(@RequestBody PlanRequest planRequest)throws  GIException	
	{
		PlanResponse planResponse = new PlanResponse();
		List<PlanResponse> planResponselist = new ArrayList<PlanResponse>();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getBenefitCostData() ============ ");
		}
		try{
			if(null == planRequest || null == planRequest.getPlanIds() || null == planRequest.getInsuranceType() || null == planRequest.getEffectiveDate()) {
				planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			}else{
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("getBenefitCostData for : " + SecurityUtil.sanitizeForLogging(planRequest.getPlanIds().toString()) +" :::: "+ SecurityUtil.sanitizeForLogging(planRequest.getInsuranceType()));
				}
				if(planRequest.getInsuranceType().equalsIgnoreCase(PlanMgmtConstants.HEALTH)){
					planResponselist =  planMgmtService.getPlanDataBenefitAndcost(planRequest.getPlanIds(), planRequest.getEffectiveDate(), PlanMgmtConstants.HEALTH);
				}else if(planRequest.getInsuranceType().equalsIgnoreCase(PlanMgmtConstants.DENTAL)){
					planResponselist =  planMgmtService.getPlanDataBenefitAndcost(planRequest.getPlanIds(), planRequest.getEffectiveDate(), PlanMgmtConstants.DENTAL);
				}else{
					planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
					planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception Occured in getBenefitCostData:", ex);
		}
		return planResponselist.toString();
	}
	
//	@ApiOperation(value="updatestatus", notes="This operation updates status of all plans. This API does not require any input and it returns success when the plan status changes")
//	@ApiResponse(value="Returning String success after updating status of plans to DECERTIFIED")
    @RequestMapping(value="/updatestatus",method = RequestMethod.POST)
    @ResponseBody public String updatePlanstatus() {
        
    	// call planmgmt service to set plan status as DECERTIFIED
    	//List<Map<String, String>> listOfPlans = planMgmtService.updatePlanStatus();
    	return  planMgmtService.updatePlanStatus();
    }
    
//	@ApiOperation(value="getplansdata", notes="This API takes Plan Id's and returns Issuer Info, Plan Info, Plan Benefits")
//	@ApiResponse(value="Returning list of PlanResponse with specific benefit and Cost information of provided planIds")
    @RequestMapping(value = "/getplansdata", method = RequestMethod.POST)
	@ResponseBody public String getPlanMetaData(@RequestBody PlanRequest planRequest)throws  GIException	
	{
		PlanResponse planResponse = new PlanResponse();
		List<PlanResponse> planResponselist = new ArrayList<PlanResponse>();	
	
		try{
			if(null == planRequest || null == planRequest.getPlanIds() || planRequest.getPlanIds().isEmpty()) {
				planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				planResponse.setErrMsg("Invalid request parameters.");
				planResponselist.add(planResponse);
			}else{				
				planResponselist = getPlanMetaDataHelper(planRequest);
			}
		}catch(Exception ex){
			LOGGER.error("Exception Occured in getPlanMetaData:", ex);
		}
		return planResponselist.toString();
	}
    
    public List<PlanResponse> getPlanMetaDataHelper(PlanRequest planRequest)throws  GIException	{		
		List<PlanResponse> planResponselist = new ArrayList<PlanResponse>();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getPlanMetaData() ============ ");
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("getPlanMetaData for : " + SecurityUtil.sanitizeForLogging(planRequest.getPlanIds().toString()));
		}
	
		try{
			if(null == planRequest || null == planRequest.getPlanIds() || planRequest.getPlanIds().isEmpty()) {
				return planResponselist;
			}else{				
				planResponselist = planMgmtService.getPlanInfoMap(planRequest.getPlanIds());
			}
		}catch(Exception ex){
			LOGGER.error("Exception Occured in getBenefitCostData:", ex);
		}
		
		return planResponselist;
	}
    
//	@ApiOperation(value="getprovidernetwork", notes="This operation retrieves plan provider network information")
//	@ApiResponse(value="Returning PlanResponse with list of providers")
    @RequestMapping(value = "/getprovidernetwork", method = RequestMethod.POST)
	@ResponseBody public PlanResponse getpProviderNetwork(@RequestBody PlanRequest planRequest)throws  GIException	
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getpProviderNetwork() ============ ");
		}
			
		PlanResponse planResponse = new PlanResponse();
		// if request data elements are null throw error
		if(null == planRequest 
			|| null == planRequest.getPlanId()
			|| planRequest.getPlanId().isEmpty()
			|| Integer.parseInt(planRequest.getPlanId()) < 1
			// HIX-57901 do getProviderList optional
			) {
			planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}else{
			   List<Map<String, List<String>>> providers = (List<Map<String, List<String>>>)planRequest.getProviderList();
			   
			// HIX-57901 if strenuss id passed in request, in that case pull network keys for each strenuss id
				List<String> strenussIdList = (List<String>) planRequest.getStrenussIdList(); 
				if(strenussIdList != null && !strenussIdList.isEmpty()){
					providers = providerService.getNetworkListByStrenussId(strenussIdList);
				}
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Request data :: plan Id : " + SecurityUtil.sanitizeForLogging(planRequest.getPlanId()) + " >> Provider Data: " + SecurityUtil.sanitizeForLogging(providers.toString()));
				}
				try{
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("Fetching plan and provider data....");
					}
					List<Map<String, String>> planProviderNetworkData = planMgmtService.getpProviderNetwork(planRequest.getPlanId(), providers );					
					planResponse.setProviders(planProviderNetworkData);					
					
				}catch(Exception ex) {
					LOGGER.error("Fail to fetch plans. Exception:", ex);					
					planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLAN_INFO.getCode());
					planResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}	
		}
		
		return planResponse;
	}
    
    /**
     * HIX-50844
     * @author Shubhendu Singh (mailto: shubhendu.singh@getinsured.com)
     * Created on: Sept 29, 2014 
     * Returns List of PlanResponse objects with information about plan name, 
     * issuer name, issuer id, issuer logo required to be displayed on Universal Cart Page
     * @param planRequest
     * @return 
     * @throws GIException
     */
//	@ApiOperation(value="getPlansForUniversalCart", notes="This API takes Plan Id's as input and returns Plan Info, Issuer Info, Plan Benefits and Plan Costs")
//	@ApiResponse(value="Returning list of PlanResponse for universal cart")
    @RequestMapping(value = "/getPlansForUniversalCart", method = RequestMethod.POST)
   	@ResponseBody public List<PlanResponse> getPlansForUniversalCart(@RequestBody PlanRequest planRequest)throws  GIException	
   	{
    	return planMgmtService.getPlanResponseListForUniversalCart(planRequest.getPlanIds());
   	}
    
//	@ApiOperation(value="getplaninfobyzip", notes="This API takes zip, county, insurance type e.g. (Health, Dental, AME, STM) and exchange type and returns Issuer Id and Plans List and Plans details.")
//	@ApiResponse(value="Returning list of PlanResponse having Plan information for that zip code")
    @RequestMapping(value = "/getplaninfobyzip", method = RequestMethod.POST)
   	@ResponseBody public String getPlanInfoByZip(@RequestBody PlanRequest planRequest) throws  GIException{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getPlanInfoByZip() of Controller ============ ");
		}
   		List<PlanResponse> plansList = new ArrayList<PlanResponse>();
   		PlanResponse planResponse = new PlanResponse();
   		Map<String,Map<String, String>> plansMap = new HashMap<String, Map<String, String>>();
   		// if request data elements are null throw error
   		try{
   			if(null == planRequest || (null == planRequest.getZip() && null == planRequest.getCountyFIPS())) {
   				planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
   				planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
   				planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
   				plansList.add(planResponse);
   			}else if(planRequest.getZip().trim().isEmpty() && planRequest.getCountyFIPS().trim().isEmpty()) {
   				planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
   				planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
   				planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
   				plansList.add(planResponse);
   			} else if((null == planRequest.getInsuranceType() || null == planRequest.getExchangeType()) || (planRequest.getInsuranceType().trim().isEmpty() || planRequest.getExchangeType().trim().isEmpty())){
   				planResponse.setStatus(GhixConstants.EXCEPTION_IO_ERROR_DESC);
   				planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
   				planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
   				plansList.add(planResponse);
   			}else{
   				String tenantCode = (planRequest.getTenantCode() == null || planRequest.getTenantCode().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.ALL : planRequest.getTenantCode().trim() ; // default value is ALL to pull all records
   				// HIX-98514 : Get data source for Medicare plans. Default data source is DRX.
   				String medicarePlanDataSource = (planRequest.getMedicarePlanDataSource() == null || planRequest.getMedicarePlanDataSource().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.DEFAULT_MEDICARE_PLAN_DATA_SOURCE : planRequest.getMedicarePlanDataSource().trim().toUpperCase() ;
   				
   				plansList = planMgmtService.getPlanInfoByZip(planRequest.getZip(), planRequest.getCountyFIPS(), planRequest.getInsuranceType(), planRequest.getExchangeType(), planRequest.getEffectiveDate(), tenantCode, medicarePlanDataSource);
   				if(!(plansList.isEmpty())){
   					plansMap = planMgmtService.getPlansMapData(plansList);
   				}
   				planResponse.setPlans(plansMap);
   				planResponse.setPlanList(plansList);
   			}
   		}catch(Exception ex){
   			LOGGER.error("Exception Occured in getPlanInfoByZip @ Controller", ex);
   		}
   		
   		return planResponse.toString();
   	}

    private String getFormularyURL(Integer formularlyId){
		String formularyURL = formularyRepository.findFormularyUrlById(formularlyId);
		/* Adding Regular expression for adding HTTP  */
		if (!formularyURL.equals("") && !formularyURL.matches(PlanMgmtConstants.URL_REGEX)) {
			formularyURL = "http://" + formularyURL;			
		}
		return formularyURL;
	}
    
   
    

	private boolean isURLValid(String checkURL) {
		try {
			if (checkURL.matches("(?i).*https.*") || checkURL.matches("(?i).*http.*") || checkURL.matches("(?i).*www.*") || checkURL.matches("(?i).*ftp.*")) {
				return true;
			} 
			return false;
		} catch (RuntimeException e) {
			LOGGER.error("Runtime exception occurred while url validation", e);
			return false;
		}
	}
	
//	@ApiOperation(value="checkMetalTierAvailability", notes="check plans available for Metail Tier")
//	@ApiResponse(value="Returning ShopResposne with EmployerQuotingDTO having information of metal tier availability")
	@RequestMapping(value = "/checkMetalTierAvailability", method = RequestMethod.POST)
	@ResponseBody public String getMetalTierAvailability(@RequestBody EmployerQuotingDTO employerQuotingDTO)throws  GIException
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getMetalTierAvailability() ============ ");
		}
		if(LOGGER.isDebugEnabled()) {
			if(null != employerQuotingDTO) {
				StringBuilder buildStr = new StringBuilder();
				buildStr.append("Request data ===>");	
				buildStr.append(" Insurance type : ");
				buildStr.append(employerQuotingDTO.getInsType());
				buildStr.append(" Effective date : ");
				buildStr.append(employerQuotingDTO.getEffectiveDate());
				buildStr.append(" empPrimaryZip : ");
				buildStr.append(employerQuotingDTO.getEmpPrimaryZip());
				buildStr.append(" empCountyCode : ");
				buildStr.append(employerQuotingDTO.getEmpCountyCode());				
				buildStr.append(" ehbCovered : ");
				buildStr.append(employerQuotingDTO.getEhbCovered());
				LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));
			} else {
				LOGGER.debug("getMetalTierAvailability: Input param is null");
			}
		}
		ShopResponse shopResponse = new ShopResponse();	
		// if request data elements are null throw error
		if(null == employerQuotingDTO 
				|| null == employerQuotingDTO.getEffectiveDate()
				|| 0 == employerQuotingDTO.getEffectiveDate().trim().length()
				|| null == employerQuotingDTO.getEmpPrimaryZip()
				|| 0 == employerQuotingDTO.getEmpPrimaryZip().trim().length()
				|| null == employerQuotingDTO.getInsType()	
				|| 0 == employerQuotingDTO.getInsType().trim().length()	
				|| null == employerQuotingDTO.getEmpCountyCode()
				|| 0 == employerQuotingDTO.getEmpCountyCode().trim().length()
				) {
			shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			shopResponse.setErrMsg("Invalid request parameters.");
		}else{			
				String empPrimaryZip = employerQuotingDTO.getEmpPrimaryZip().trim();
				String empCountyCode = employerQuotingDTO.getEmpCountyCode().trim();
				String ehbCovered = (employerQuotingDTO.getEhbCovered() != null) ? employerQuotingDTO.getEhbCovered() : "";
				String insuranceType = employerQuotingDTO.getInsType().trim();
				String effectiveDate = employerQuotingDTO.getEffectiveDate().trim();			
				Map<String, String> metalTierAvailMap = null;
				try{
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Looking for SHOP plans for zip " + SecurityUtil.sanitizeForLogging(empPrimaryZip) + ", county code " + SecurityUtil.sanitizeForLogging(empCountyCode));
					}
					metalTierAvailMap = planMgmtService.getMetalTierAvailability(empPrimaryZip, ehbCovered, insuranceType, effectiveDate, empCountyCode);
					employerQuotingDTO.setMetalTierAvailability(metalTierAvailMap);
					shopResponse.addResponseData(EmployerQuotingDTO.class.getName(), employerQuotingDTO);
				}catch(Exception ex) {
					LOGGER.error("Fail to fetch plans. Exception:",ex);					
					shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_METAL_TIER_AVAILABILITY.getCode());
					shopResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}			
		}
		return shopResponse.sendResponse(ResponseType.XML);		
	}
	
	
	// This API is to return cross walk HIOS plan id
	@RequestMapping(value = "/getcrosswalkhiosplanid", method = RequestMethod.POST)
	@ResponseBody public String getCrosswalkHiosPlanId(@RequestBody PlanCrossWalkRequestDTO planCrossWalkRequest)throws  GIException{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getCrosswalkHiosPlanId() ============ ");
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Request data :: plan Id : " + SecurityUtil.sanitizeForLogging(planCrossWalkRequest.getHiosPlanNumber()) +
					", zip: " + SecurityUtil.sanitizeForLogging(planCrossWalkRequest.getZip()) + ", countyCode: " + SecurityUtil.sanitizeForLogging(planCrossWalkRequest.getCountyCode()) 
					+ ", effective date: " + SecurityUtil.sanitizeForLogging(planCrossWalkRequest.getEffectiveDate()) +
					", eligible for catastropic: " + SecurityUtil.sanitizeForLogging(planCrossWalkRequest.getIsEligibleForCatastrophic()) );		
		}
		
		PlanCrossWalkResponseDTO planCrossWalkResponse = new PlanCrossWalkResponseDTO();
		// if request data elements are null then throw error
		if(null == planCrossWalkRequest 
			|| null == planCrossWalkRequest.getHiosPlanNumber()
			|| null == planCrossWalkRequest.getZip()
			|| null == planCrossWalkRequest.getCountyCode()
			|| null == planCrossWalkRequest.getEffectiveDate()
			|| null == planCrossWalkRequest.getIsEligibleForCatastrophic()
			//|| 
			) {
			planCrossWalkResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planCrossWalkResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planCrossWalkResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}/*else if((PlanMgmtConstants.Y.equalsIgnoreCase(planCrossWalkRequest.getIsEligibleForCatastrophic()) || PlanMgmtConstants.N.equalsIgnoreCase(planCrossWalkRequest.getIsEligibleForCatastrophic()))){
			planCrossWalkResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planCrossWalkResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planCrossWalkResponse.setErrMsg(PlanMgmtConstants.INVALIDREQUEST);
		}*/
		else{
			long time = System.currentTimeMillis();
			try{
				planCrossWalkResponse = planMgmtService.getCrossWalkPlanId(planCrossWalkRequest.getHiosPlanNumber(), planCrossWalkRequest.getZip(), planCrossWalkRequest.getCountyCode(),planCrossWalkRequest.getEffectiveDate(), planCrossWalkRequest.getIsEligibleForCatastrophic());
			}catch(Exception ex) {
				LOGGER.error("Fail to fetch plans. Exception:",ex);					
				planCrossWalkResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planCrossWalkResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.CROSS_WALK_PLAN.getCode());
				planCrossWalkResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
			}
			LOGGER.warn("Received getcrosswalkhiosplanid-API, response for HIOS Plan ID " + planCrossWalkRequest.getHiosPlanNumber() + " in  " + (System.currentTimeMillis() - time) + " ms");
		}
		return planCrossWalkResponse.toString();
	}
	
	
//	@ApiOperation(value="reapplyPlanBenefitEdits", notes="This API reapply benefits that were edited for previous version of plan and return response based on processing status. ")
//	@ApiResponse(value="Returns PlanResponse containing status after reapplying benefit edits to existing active plan")
	@RequestMapping(value = "/reapplyPlanBenefitEdits", method=RequestMethod.POST)
	@ResponseBody public String reapplyPlanBenefitEdits(@RequestBody PlanRequest planRequest) {
		boolean status = false;
   		PlanResponse planResponse = new PlanResponse();
		StringBuilder message = new StringBuilder();
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside Controllers method reapplyPlanBenefitEdits() ============ ");
		}
		if(null == planRequest || null == planRequest.getPlanId()) {
			LOGGER.error("Invalid input param received for reapplying plan benefits");					
			planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			message.append(PlanMgmtConstants.ERROR_RE_APPLY_BENEFITS);
		}else{
			planResponse.setErrCode(PlanMgmtConstants.STATUS_FAILURE_CODE);
			String planId = planRequest.getPlanId();				
			try{
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Reapplying plan benefits for plan ...." + SecurityUtil.sanitizeForLogging(planId));
				}
				status = planMgmtService.reapplyBenefitEditsToPlan(Integer.parseInt(planId), message);
			}catch(Exception ex) {
				message.append(PlanMgmtConstants.ERROR_RE_APPLY_BENEFITS);
				LOGGER.error("Exception occured:" , ex);
			}
		}
		planResponse.setErrMsg(message.toString());
		if(status) {
			planResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			planResponse.setErrCode(0);
		} else {
			planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return planResponse.toString();
	}

	
	
//	@ApiOperation(value="", notes="get pediatric plan information")
//	@ApiResponse(value="Returning plan's pediatric information ")
	@RequestMapping(value = "/getpediatricplaninfo", method = RequestMethod.POST)
	@ResponseBody public PediatricPlanResponseDTO getPediatricPlanInfo(@RequestBody PediatricPlanRequestDTO pediatricPlanRequestDTO)throws  GIException
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getPediatricPlanInfo() ============ ");
		}
		if(LOGGER.isDebugEnabled()) {
			if(null != pediatricPlanRequestDTO) {
				StringBuilder buildStr = new StringBuilder();
				buildStr.append("Request data ===>");	
				buildStr.append(" HIOS Plan Id : ");
				buildStr.append(pediatricPlanRequestDTO.getHiosPlanId());
				buildStr.append(" Plan year : ");
				buildStr.append(pediatricPlanRequestDTO.getPlanYear());
				LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));
			} else {
				LOGGER.debug("pediatricPlanRequestDTO: Input param is null");
			}
		}
		
		PediatricPlanResponseDTO pediatricPlanResponseDTO = new PediatricPlanResponseDTO();	
		// if request data elements are null throw error
		if(null == pediatricPlanResponseDTO 
				||StringUtils.isBlank(pediatricPlanRequestDTO.getHiosPlanId())
				||StringUtils.isBlank(String.valueOf(pediatricPlanRequestDTO.getPlanYear()))				
				) {
			pediatricPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			pediatricPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			pediatricPlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}else{			
				String hiosPlanNumber = pediatricPlanRequestDTO.getHiosPlanId().trim();
				Integer planYear = pediatricPlanRequestDTO.getPlanYear();
				try{
					String plaAvailFor = planMgmtService.getPediatricPlanInfo(hiosPlanNumber, planYear);
					 
					// if records not found in plan table for corresponding hios plan number and plan year
					if(plaAvailFor.equalsIgnoreCase( PlanMgmtConstants.DATA_NOT_AVAILABLE)){
						pediatricPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
						pediatricPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PEDIATRIC_PLAN_INFO.getCode());
						pediatricPlanResponseDTO.setErrMsg(PlanMgmtConstants.DATA_NOT_AVAILABLE);
					}else{
						switch(plaAvailFor.toUpperCase()){
							case "CHILDONLY" :
								pediatricPlanResponseDTO.setChildOnly(true);
								pediatricPlanResponseDTO.setAdultOnly(false);
								pediatricPlanResponseDTO.setAdultAndChildOnly(false);
								break;
							case "ADULTONLY" :
								pediatricPlanResponseDTO.setChildOnly(false);
								pediatricPlanResponseDTO.setAdultOnly(true);
								pediatricPlanResponseDTO.setAdultAndChildOnly(false);
								break;
							case "ADULTANDCHILD" :
								pediatricPlanResponseDTO.setChildOnly(false);
								pediatricPlanResponseDTO.setAdultOnly(false);
								pediatricPlanResponseDTO.setAdultAndChildOnly(true);
								break;	
							default:
								pediatricPlanResponseDTO.setChildOnly(false);
								pediatricPlanResponseDTO.setAdultOnly(false);
								pediatricPlanResponseDTO.setAdultAndChildOnly(false);
								break;
						}
					}						
					pediatricPlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
					pediatricPlanResponseDTO.endResponse();					
				}catch(Exception ex) {
					LOGGER.error("Fail to get pediatric plan info. Exception:",ex);					
					pediatricPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					pediatricPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PEDIATRIC_PLAN_INFO.getCode());
					pediatricPlanResponseDTO.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}			
		}
		
		return pediatricPlanResponseDTO;		
	}

	
//	@ApiOperation(value="getPlanInfoByHiosPlanId", notes="This API takes HIOS Plan ID and effective date as input and provides limited plan information like plan id, plan name, insurance type")
//	@ApiResponse(value="Returning PlanResonse with limited plan information ")	
	@RequestMapping(value = "/getPlanInfoByHiosPlanId", method = RequestMethod.POST)
	@ResponseBody public String getPlanInfoByHiosPlanId(@RequestBody PlanRequest planRequest)throws  GIException	
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getPlanInfoByHiosPlanId() ============ ");
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Request data :: HIOS plan Id : " + SecurityUtil.sanitizeForLogging(planRequest.getHiosPlanId()) + " Effective start date : " + SecurityUtil.sanitizeForLogging(planRequest.getEffectiveDate()));
		}	
		long time = System.currentTimeMillis();
		
		PlanResponse planResponse = new PlanResponse();
		// if request data elements are null throw error
		if(null == planRequest 
			|| StringUtils.isEmpty(planRequest.getHiosPlanId())
			|| StringUtils.isEmpty(planRequest.getEffectiveDate()) ) {
			planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}else{
				String hiosPlanId = planRequest.getHiosPlanId().trim();
				String effectiveDate = planRequest.getEffectiveDate().trim();
				 
				try{

					Plan planData = planMgmtService.getPlanInfoByHIOSPlanId(hiosPlanId, effectiveDate);	
														      
					if(null == planData){
						planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
						planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLAN_BY_HIOS_ID.getCode());
						planResponse.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
					}else{
						Issuer issuerData = planData.getIssuer();

						if (null != issuerData) {
							planResponse.setIssuerId(issuerData.getId());
							planResponse.setIssuerName(issuerData.getName());
						}
						planResponse.setPlanId(planData.getId());
						planResponse.setPlanName(planData.getName());					
						planResponse.setInsuranceType(planData.getInsuranceType());
						planResponse.setPlanYear(planData.getApplicableYear());
						// set plan level
						if(Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(planData.getInsuranceType())){
							planResponse.setPlanLevel(planData.getPlanHealth().getPlanLevel());
						}
						if(Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(planData.getInsuranceType())){
							planResponse.setPlanLevel(planData.getPlanDental().getPlanLevel());
						}
					
						planResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
						planResponse.endResponse();
					}	
					
				}catch(Exception ex) {
					giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch plan info.",ex);
					LOGGER.error("Fail to fetch plans. Exception:", ex);					
					planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLAN_BY_HIOS_ID.getCode());
					planResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}				
		}
		LOGGER.warn("Received getPlanInfoByHiosPlanId-API, response for HIOS Plan ID " + planRequest.getHiosPlanId() + " in  " + (System.currentTimeMillis() - time) + " ms");
		return planResponse.toString();
	}
	
	
	
//	@ApiOperation(value="getRatingArea", notes="This API takes state code and county FIPS as input and returns rating area")
//	@ApiResponse(value="Returning rating area ")	
	@RequestMapping(value = "/getRatingArea", method = RequestMethod.POST)
	@ResponseBody public RatingAreaResponseDTO getRatingArea(@RequestBody RatingAreaRequestDTO ratingAreaRequestDTO)throws  GIException	
	{
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("=========  Inside getRatingArea() ============ ");
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Request data :: State Code : " + SecurityUtil.sanitizeForLogging(ratingAreaRequestDTO.getStateCode()) + " County FIPS : " + SecurityUtil.sanitizeForLogging(ratingAreaRequestDTO.getCountyFips()));
		}
		
		RatingAreaResponseDTO ratingAreaResponseDTO = new RatingAreaResponseDTO();
		
		if(null == ratingAreaRequestDTO 
				|| StringUtils.isEmpty(ratingAreaRequestDTO.getStateCode())
				|| StringUtils.isEmpty(ratingAreaRequestDTO.getCountyFips())) {
				ratingAreaResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				ratingAreaResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				ratingAreaResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			}else{
				String stateCode = ratingAreaRequestDTO.getStateCode().trim();
				String countyFips = ratingAreaRequestDTO.getCountyFips().trim();
				String ratingArea = PlanMgmtConstants.EMPTY_STRING;
				
				try{
					ratingArea = ratingAreaService.getRatingAreaByStateCodeAndCountyFips(stateCode, countyFips);
					if(!PlanMgmtConstants.EMPTY_STRING.equalsIgnoreCase(ratingArea)){
						ratingAreaResponseDTO.setRatingArea(ratingArea);
						ratingAreaResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
						ratingAreaResponseDTO.endResponse();
					}else{
						ratingAreaResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
						ratingAreaResponseDTO.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
						ratingAreaResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_RATING_AREA.getCode());
						ratingAreaResponseDTO.endResponse();
					}
				}catch(Exception ex) {
					giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch rating area name.",ex);
					LOGGER.error("Failed to fetch rating area name. Exception:", ex);					
					ratingAreaResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					ratingAreaResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_RATING_AREA.getCode());
					ratingAreaResponseDTO.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}		
					
			}
		return ratingAreaResponseDTO;
	}	
	
	
	//web

	 @RequestMapping(value = "/getDistinctPlanYearsByInsuranceType/{insuranceType}", method = RequestMethod.GET)
	 @ResponseBody
	 public List<String> getDistinctPlanYearsByInsuranceType(@PathVariable("insuranceType") String insuranceType )throws  GIException {	
	 
		 return planMgmtService.getDistinctPlanYearsByInsuranceType(insuranceType);

	    
	  }
	 
	 @RequestMapping(value = "/getDistinctPlanYearsByInsuranceTypeAndIssuerId/{insuranceType}/{issuerId}", method = RequestMethod.GET)
	 @ResponseBody
	 public List<String> getDistinctPlanYearsByInsuranceTypeAndIssuerId(@PathVariable("insuranceType") String insuranceType,
			                                                            @PathVariable("issuerId") int issuerId)throws  GIException {	
	 
		 return planMgmtService.getDistinctPlanYearsByInsuranceTypeAndIssuerId(insuranceType, issuerId);

	    
	  }
	 
	 @RequestMapping(value = "/getPlans", method = RequestMethod.POST)
	 @ResponseBody
	 public PlanSearchResponseDTO getPlans(@RequestBody PlanSearchRequestDTO request)throws  GIException {	
		 PlanSearchResponseDTO planSearchResponseDTO = new PlanSearchResponseDTO();
		 try{
			 planSearchResponseDTO =  planMgmtService.getPlans(request);
		 }
	     catch(Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "PlanRestController.getPlans: Error occured when executing getPlans",ex);
			LOGGER.error("PlanRestController.getPlans: Error occured when executing getPlans", ex);					
			
		 }	
	     return planSearchResponseDTO;	 
	 }
	
	 @RequestMapping(value = "/{planId}", method = RequestMethod.GET)
	 @Produces("application/json")
	 @ResponseBody public com.getinsured.hix.dto.planmgmt.PlanDTO getPlanById(@PathVariable("planId") int planId) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside getPlanById() ============ " + planId);
		}
		com.getinsured.hix.dto.planmgmt.PlanDTO planDto = null;
		boolean success = false;
		int errorCode = 0;
		String errorMsg = null;
		try {
			if (0 < planId) {
				planDto = planMgmtService.getPlanDetailsById(planId);
				if (null != planDto) {
					success = true;
				} else {
					errorCode = PlanMgmtErrorCodes.ErrorCode.GET_PLAN_INFO.getCode();
					errorMsg = "Failed to fetch plan details.";
				}
			} else {
				errorCode = PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode();
				errorMsg = PlanMgmtConstants.INVALID_REQUEST;
			}
		} catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch Plan details.", ex);
			LOGGER.error("Failed to fetch plan details. Exception:", ex);
			errorCode = PlanMgmtErrorCodes.ErrorCode.GET_PLAN_INFO.getCode();
			errorMsg = Arrays.toString(ex.getStackTrace());
		} finally {
			if (null == planDto) {
				planDto = new com.getinsured.hix.dto.planmgmt.PlanDTO();
			}
			if (success) {
				planDto.setStatus(GhixConstants.RESPONSE_SUCCESS);
			} else {
				planDto.setStatus(GhixConstants.RESPONSE_FAILURE);
				planDto.setErrCode(errorCode);
				planDto.setErrMsg(errorMsg);
			}
		}

		return planDto;
	}
	 
	 
	// HIX-101333 :: QHP Certification screen - move the repository access to PM service
	@RequestMapping(value = "/getPlanCertificationHistory/{planId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody public List<Map<String, Object>> getPlanCertificationHistory(@PathVariable("planId") Integer planId ) throws  GIException {	
		return planHistoryService.getPlanCertificationHistory(planId);
	}
	
	
	// HIX-101447 :: Load QHP history using PlanMgmt API
	@RequestMapping(value = "/getPlanHistory/{planId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody public List<Map<String, Object>> getPlanHistory(@PathVariable("planId") Integer planId ) throws  GIException {	
		return planHistoryService.getPlanHistory(planId);
	}
	 
	@RequestMapping(value = "/getIssuerPlanHistory/{planId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody public List<Map<String, Object>> getIssuerPlanHistory(@PathVariable("planId") Integer planId ) throws  GIException {	
		return planHistoryService.getIssuerPlanHistory(planId);
	}
	 
	@RequestMapping(value = "/updatePlanDetails", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public GHIXResponse updatePlanName(@RequestBody PlanRequestDTO planReqDTO)throws  GIException	
	{
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside updatePlanName() ============ ");
		}
		return planMgmtService.updatePlanDetailsById(planReqDTO);
	}
	
	@RequestMapping(value = "/getServiceAreaDetails/{planId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody public ServiceAreaDetailsDTO getServiceAreaDetails(@PathVariable("planId") int planId) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside getServiceAreaDetails() ============ ");
		}
		return planMgmtService.getServiceAreaDetailsByPlanId(planId);
	}
	
	@RequestMapping(value = "/getBenefitsHistory/{roleType}/{planId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody public PlanComponentHistoryDTO getPlanBenefitsHistory(@PathVariable("roleType") String roleType, @PathVariable("planId") int planId) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside getBenefitsHistory() ============ " + roleType + " : Plan-" + planId);
		}
		return planHistoryService.getPlanBenefitsHistoryByRoleAndPlanId(roleType, planId);
	}
	
	@RequestMapping(value = "/getPlanCountByHiosIssuerId/{hiosIssuerId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody public Long getPlanCountByHiosIssuerId(@PathVariable("hiosIssuerId") String hiosIssuerId) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside getPlanCountByHiosIssuerId() ============ HIOS Issuer Id: " + hiosIssuerId);
		}
		return planMgmtService.getPlanCountByHiosIssuerId(hiosIssuerId);
	}

	@RequestMapping(value = "/getBenefitsAndCostData/{planId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody public ListOfStringArrayResponseDTO getBenefitsAndCostData(@PathVariable("planId") int planId) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside getBenefitsAndCostData() ============ : Plan-" + planId);
		}
		return planMgmtService.getPlanBenefitsAndCostData(planId);
	}


	 @RequestMapping(value = "/addTenants", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public GHIXResponse addTenants(@RequestBody PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO) {
		
        GHIXResponse ghixResponse = new GHIXResponse();
		
		try {
			
			planMgmtService.addTenants(planBulkUpdateRequestDTO);
	        ghixResponse.setStatus(PlanMgmtConstants.SUCCESS);
				
		} catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "PlanRestController - Failed to execute addTenants.", ex);
			LOGGER.error("PlanRestController - Failed to execute addTenants:", ex);
			ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.ADD_TENANTS.getCode());
			ghixResponse.setErrMsg("Failed to execute addTenants.");
			ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
		} 
        return ghixResponse;
	}
	 
	 @RequestMapping(value = "/addTenantsForSelectedPlans", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public GHIXResponse addTenantsForSelectedPlans(@RequestBody PlanTenantRequestDTO request) {
		
        GHIXResponse ghixResponse = new GHIXResponse();
        ghixResponse.setStatus(PlanMgmtConstants.SUCCESS);
		
		try {
			
			planMgmtService.addTenantsForSelectedPlans(request);
				
		} catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "PlanRestController - Failed to execute addTenantsForSelectedPlans.", ex);
			LOGGER.error("PlanRestController - Failed to execute addTenantsForSelectedPlans:", ex);
			ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.ADD_TENANTS_FOR_SELECTED_PLANS.getCode());
			ghixResponse.setErrMsg("Failed to execute addTenantsForSelectedPlans");
			ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
		} 
        return ghixResponse;
	}

	@RequestMapping(value = "/getPlanRateHistory/{roleType}/{planId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody public PlanComponentHistoryDTO getPlanRateHistory(@PathVariable("roleType") String roleType, @PathVariable("planId") int planId) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside getPlanRateHistory() ============ " + roleType + " : Plan-" + planId);
		}
		return planMgmtService.getPlanRateHistoryByRoleAndPlanId(roleType, planId);
	}
	
	@RequestMapping(value = "/getPlanRateList/{planId}/{effectiveStartDate}/{effectiveEndDate}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody public PlanRateListResponseDTO getPlanRateList(@PathVariable("planId") int planId, @PathVariable("effectiveStartDate") String effectiveStartDate, @PathVariable("effectiveEndDate") String effectiveEndDate) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside getPlanRateList() ============ ");
		}
		return planMgmtService.getPlanRateList(planId, effectiveStartDate, effectiveEndDate);
	}

	@RequestMapping(value = "/updatePlanDocuments", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public PlanDocumentResponseDTO updatePlanDocuments(@RequestBody PlanDocumentRequestDTO requestDTO) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside getPlanCountByHiosIssuerId() ============");
		}
		return planMgmtService.updatePlanDocuments(requestDTO);
	}
	
	
	@RequestMapping(value = "/getSbcScenario", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public SbcScenarioResponseDTO getSbcScenario(@RequestBody SbcScenarioRequestDTO sbcScenarioRequestDTO){
		SbcScenarioResponseDTO sbcScenarioResponse = new SbcScenarioResponseDTO();
		
		try{
			if(null == sbcScenarioRequestDTO.getHiosPlanIdList() 
			    || null == sbcScenarioRequestDTO.getApplicableYear()
				|| sbcScenarioRequestDTO.getHiosPlanIdList().size() == PlanMgmtConstants.ZERO ){
				sbcScenarioResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				sbcScenarioResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				sbcScenarioResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				sbcScenarioResponse.endResponse();
			}else{
				List<String> hiosPlanIdList = sbcScenarioRequestDTO.getHiosPlanIdList();
				Integer applicableYear  = sbcScenarioRequestDTO.getApplicableYear();
			
				sbcScenarioResponse.setSbcScenario(planMgmtService.getSbcScenarioIdsByHiosPlanIds(hiosPlanIdList, applicableYear));
				sbcScenarioResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				sbcScenarioResponse.endResponse();
			}
		}
		catch(Exception ex)
		{
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute PlanRestController.getSbcScenario().", ex);
			LOGGER.error("Failed to execute PlanRestController.getSbcScenario(). Exception:", ex);					
			sbcScenarioResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			sbcScenarioResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_SBC_SCENARIO.getCode());
			sbcScenarioResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
		}
		
		return  sbcScenarioResponse;
	}

	//HIX-101994 :: Publish API to save plan benefits and costs
	@RequestMapping(value = "/updateBenefitAndCost", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public GHIXResponse updateBenefitAndCost(@RequestBody PlanBenefitAndCostRequestDTO planBenefitAndCostRequestDTO) {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside updateBenefitAndCost() ============");
		}
					
		return planCostAndBenefitsService.updatePlanBenefitAndCost(planBenefitAndCostRequestDTO);
	}

	@RequestMapping(value = "/getEnrollmentHistory/{planId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody
	public PlanComponentHistoryDTO getPlanEnrollmentHistory(@PathVariable("planId") int planId) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("=========  Inside getPlanEnrollmentHistory() ============  Plan -" + planId);
		}
		return planHistoryService.getPlanEnrollmentHistoryByPlanId(planId);
	}
	
	@RequestMapping(value = "/removeTenants", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public GHIXResponse removeTenants(@RequestBody PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO) {
		
      GHIXResponse ghixResponse = new GHIXResponse();
      ghixResponse.setStatus(PlanMgmtConstants.SUCCESS);
		
		try {
			
			planMgmtService.removeTenants(planBulkUpdateRequestDTO);
				
		} catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "PlanRestController - Failed to execute removeTenants.", ex);
			LOGGER.error("PlanRestController - Failed to execute removeTenants:", ex);
			ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.REMOVE_TENANTS.getCode());
			ghixResponse.setErrMsg("Failed to execute removeTenants");
			ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
		} 
      return ghixResponse;
	}
	 
	 @RequestMapping(value = "/removeTenantsForSelectedPlans", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public GHIXResponse removeTenantsForSelectedPlans(@RequestBody PlanTenantRequestDTO request) {
		
      GHIXResponse ghixResponse = new GHIXResponse();
		
		try {
			planMgmtService.removeTenantsForSelectedPlans(request);
		      ghixResponse.setStatus(PlanMgmtConstants.SUCCESS);
		} catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "PlanRestController - Failed to execute removeTenantsForSelectedPlans.", ex);
			LOGGER.error("PlanRestController - Failed to execute removeTenantsForSelectedPlans:", ex);
			ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.REMOVE_TENANTS_FOR_SELECTED_PLANS.getCode());
			ghixResponse.setErrMsg(" Failed to execute removeTenantsForSelectedPlan");
			ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
		} 
      return ghixResponse;
	}

	@RequestMapping(value = "/modifyMedicarePlanStatus", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public GHIXResponse modifyMedicarePlanStatus(@RequestBody PlanBulkUpdateRequestDTO requestDTO) {

		GHIXResponse ghixResponse = new GHIXResponse();
		boolean statusFlag = false;

		try {
			statusFlag = planMgmtService.modifyMedicarePlanStatus(requestDTO);

			if (statusFlag) {
				ghixResponse.setStatus(PlanMgmtConstants.SUCCESS);
			}
			else {
				ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
				ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_PLAN_UPDATE.getCode());
				ghixResponse.setErrMsg("Error occurred while modifying Medicare Plan Status in Bulk.");
			}
		}
		catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Error occurred while modifying Medicare Plan Status in Bulk: .", ex);
			LOGGER.error("Error occurred while modifying Medicare Plan Status in Bulk: ", ex);
			ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_PLAN_UPDATE.getCode());
			ghixResponse.setErrMsg("Error occurred while modifying Medicare Plan Status in Bulk.");
			ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
		}
		return ghixResponse;
	}
	
	@RequestMapping(value = "/updatePlanStatus", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public GHIXResponse updatePlanStatus(@RequestBody PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO ) {

		GHIXResponse ghixResponse = new GHIXResponse();
		boolean statusFlag = false;

		try {
			statusFlag = planMgmtService.updatePlanStatus(planBulkUpdateRequestDTO);

			if (statusFlag) {
				ghixResponse.setStatus(PlanMgmtConstants.SUCCESS);
			}
			else {
				ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
				ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_PLAN_UPDATE.getCode());
				ghixResponse.setErrMsg("Error occurred while updating Plan Status in Bulk.");
			}
		}
		catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Error occurred while updating Plan Status in Bulk: .", ex);
			LOGGER.error("Error occurred while updating Plan Status in Bulk: ", ex);
			ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_PLAN_UPDATE.getCode());
			ghixResponse.setErrMsg("Error occurred while updating Plan Status in Bulk.");
			ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
		}
		return ghixResponse;
	}

	/**
	 * Method is used to bulk update Non-Commission Flag for Plans.
	 */
	@RequestMapping(value = "/bulkUpdateNonCommissionFlag", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public GHIXResponse bulkUpdateNonCommissionFlag(@RequestBody PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO ) {

		GHIXResponse ghixResponse = new GHIXResponse();
		boolean statusFlag = false;
		final String ERROR_MESSAGE = "Error occurred while updating Plans in Bulk. ";

		try {
			statusFlag = planMgmtService.updateNonCommissionFlagForPlans(planBulkUpdateRequestDTO);

			if (statusFlag) {
				ghixResponse.setStatus(PlanMgmtConstants.SUCCESS);
			}
			else {
				ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
				ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_PLAN_UPDATE.getCode());
				ghixResponse.setErrMsg(ERROR_MESSAGE);
			}
		}
		catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, ERROR_MESSAGE, ex);
			LOGGER.error(ERROR_MESSAGE, ex);
			ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_PLAN_UPDATE.getCode());
			ghixResponse.setErrMsg(ERROR_MESSAGE);
			ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
		}
		return ghixResponse;
	}

	@RequestMapping(value = "/bulkSoftdelete", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public GHIXResponse softDeletePlans(@RequestBody PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO ) {

		GHIXResponse ghixResponse = new GHIXResponse();
		boolean statusFlag = false;

		try {
			statusFlag = planMgmtService.softDeletePlans(planBulkUpdateRequestDTO);

			if (statusFlag) {
				ghixResponse.setStatus(PlanMgmtConstants.SUCCESS);
			}
			else {
				ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
				ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_PLAN_UPDATE.getCode());
				ghixResponse.setErrMsg("Error occurred while softdeleting Plans in Bulk.");
			}
		}
		catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Error occurred while softdeleting Plans in Bulk: .", ex);
			LOGGER.error("Error occurred while softdeleting Plans in Bulk: ", ex);
			ghixResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_PLAN_UPDATE.getCode());
			ghixResponse.setErrMsg("Error occurred while softdeleting Plans in Bulk");
			ghixResponse.setStatus(PlanMgmtConstants.FAILURE);
		}
		return ghixResponse;
	}
	
	 @RequestMapping(value = "/bulkUpdatePlanStatus", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public PlanSearchResponseDTO bulkUpdatePlanStatus(@RequestBody PlanSearchRequestDTO request) {
		
	  PlanSearchResponseDTO  response = new PlanSearchResponseDTO();
      response.setStatus(PlanMgmtConstants.SUCCESS);
		
		try {
			
			if(null == request) {
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				response.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				response.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				return response;
				
			}	
			
			int planYear = Integer.parseInt(request.getPlanYear());
			int issuerBrandNameId = Integer.parseInt(request.getIssuerBrandNameId());
			int updatedCount = 0;
	
			String insuranceType = request.getInsuranceType();
			
			if (!StringUtils.isBlank(insuranceType) && insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
			    updatedCount = iPlanRepository.bulkUpdateQHPPlans(request.getPlanStatusForUpdate(), request.getIssuerVerificationStatusForUpdate(), request.getEnrollmentAvailForUpdate(), request.getLastUpdatedBy(), 			
					                                              insuranceType, request.getState(), request.getIssuerPlanNumber(), request.getMarket(), request.getStatus(),
					                                              request.getExchangeType(), request.getNonCommissionFlag(), planYear, request.getTenantId(), request.getSelectedPlanLevel(), 
					                                              issuerBrandNameId, request.getIssuerId(), request.getSelectedCsrVariationList(), request.getEnableForOffExchangeFlow(),
					                                              request.getSelectedPlanName());
			}
			else if(!StringUtils.isBlank(insuranceType) && insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
				updatedCount = iPlanRepository.bulkUpdateQDPPlans(request.getPlanStatusForUpdate(), request.getIssuerVerificationStatusForUpdate(), request.getEnrollmentAvailForUpdate(),
						                                          request.getLastUpdatedBy(), insuranceType, request.getState(), request.getIssuerPlanNumber(), request.getMarket(), request.getStatus(),
						                                          request.getExchangeType(), planYear, request.getTenantId(), request.getIssuerVerificationStatus(), request.getSelectedPlanLevel(), issuerBrandNameId, request.getIssuerId(), request.getSelectedPlanName());
			}
			response.setUpdatedCount(updatedCount);
				
		} catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "PlanRestController - Failed to execute bulkUpdatePlanStatus.", ex);
			LOGGER.error("PlanRestController - Failed to execute bulkUpdatePlanStatus:", ex);
			response.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_UPDATE_PLAN_STATUS.getCode());
			response.setErrMsg(Arrays.toString(ex.getStackTrace()));
			response.setStatus(PlanMgmtConstants.FAILURE);
		} 
        return response;
	 }
	 
	 @RequestMapping(value = "/getPlanCountForCsrVariations", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public PlanSearchResponseDTO getPlanCountForCsrVariations(@RequestBody PlanSearchRequestDTO request) {
		
	  PlanSearchResponseDTO  response = new PlanSearchResponseDTO();
      response.setStatus(PlanMgmtConstants.SUCCESS);
		
		try {
			
			if(null == request) {
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				response.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				response.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				response.setCsrPlanCount(0L);
				return response;	
			}
			
			int planYear = Integer.parseInt(request.getPlanYear());
			int issuerBrandNameId = Integer.parseInt(request.getIssuerBrandNameId());
			Long csrPlanCount = iPlanRepository.checkCsrVariationsOfPlans(request.getInsuranceType(), request.getSelectedCsrVariationList(),
					                request.getState(), request.getIssuerPlanNumber(), request.getMarket(), request.getStatus(),
					                request.getExchangeType(), request.getNonCommissionFlag(), planYear,
					                request.getTenantId(), request.getSelectedPlanLevel(), issuerBrandNameId,
					                request.getIssuerId(), request.getEnableForOffExchangeFlow(), request.getSelectedPlanName());
			response.setCsrPlanCount(csrPlanCount);
				
		} catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "PlanRestController - Failed to execute getPlanCountForCsrVariations.", ex);
			LOGGER.error("PlanRestController - Failed to execute getPlanCountForCsrVariations:", ex);
			response.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLAN_COUNT_FOR_CSR_VARIATIONS.getCode());
			response.setErrMsg(Arrays.toString(ex.getStackTrace()));
			response.setStatus(PlanMgmtConstants.FAILURE);
			response.setCsrPlanCount(0L);
		} 
      return response;
	}
	 
	 
	 @RequestMapping(value = "/bulkEnableOnExchangePlansForOffExchange", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public PlanSearchResponseDTO bulkEnableOnExchangePlansForOffExchange(@RequestBody PlanSearchRequestDTO request) {
		
	  PlanSearchResponseDTO  response = new PlanSearchResponseDTO();
      response.setStatus(PlanMgmtConstants.SUCCESS);
		
		try {
			
			if(null == request) {
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				response.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				response.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				return response;
				
			}
			
			int planYear = Integer.parseInt(request.getPlanYear());
			int issuerBrandNameId = Integer.parseInt(request.getIssuerBrandNameId());
			
			int updatedCount = iPlanRepository.bulkEnableOnExchangePlansForOffExchange(request.getCsrOffExchangeFlag(), request.getLastUpdatedBy(), 
					                                   request.getInsuranceType(), request.getState(), request.getIssuerPlanNumber(),
					                                   request.getMarket(), request.getStatus(), request.getExchangeType(),
					                                   request.getNonCommissionFlag(), planYear, request.getTenantId(), 
					                                   request.getSelectedPlanLevel(), issuerBrandNameId, request.getIssuerId(),
					                                   request.getEnableForOffExchangeFlow(), request.getSelectedPlanName());
			response.setUpdatedCount(updatedCount);
				
		} catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "PlanRestController - Failed to execute bulkEnableOnExchangePlansForOffExchange.", ex);
			LOGGER.error("PlanRestController - Failed to execute bulkEnableOnExchangePlansForOffExchange:", ex);
			response.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_ENABLE_ON_EXCHANGE_PLANS_FOR_OFF_EXCHANGE.getCode());
			response.setErrMsg(Arrays.toString(ex.getStackTrace()));
			response.setStatus(PlanMgmtConstants.FAILURE);
			response.setCsrPlanCount(0L);
		} 
        return response;
	 }
	 
	 
	 @RequestMapping(value = "/bulkEnableSelectedOnExchangePlansForOffExchange", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public PlanSearchResponseDTO bulkEnableSelectedOnExchangePlansForOffExchange(@RequestBody PlanSearchRequestDTO request) {
		
	  PlanSearchResponseDTO  response = new PlanSearchResponseDTO();
      response.setStatus(PlanMgmtConstants.SUCCESS);
		
		try {
			if(null == request || request.getSelectedPlanIds() == null) {
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				response.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				response.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				return response;
				
			}
			int updatedCount = planMgmtService.bulkEnableSelectedOnExchangePlansForOffExchange(request);
			
			response.setUpdatedCount(updatedCount);
				
		} catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "PlanRestController - Failed to execute bulkEnableOnExchangePlansForOffExchangeForSelectedPlans.", ex);
			LOGGER.error("PlanRestController - Failed to execute bulkEnableOnExchangePlansForOffExchangeForSelectedPlans:", ex);
			response.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_ENABLE_ON_EXCHANGE_PLANS_FOR_OFF_EXCHANGE.getCode());
			response.setErrMsg(Arrays.toString(ex.getStackTrace()));
			response.setStatus(PlanMgmtConstants.FAILURE);
		} 
        return response;
	 }
	 
	 @RequestMapping(value = "/bulkUpdatePlanStatusForSelectedPlans", method = RequestMethod.POST)
	 @Produces("application/json")
	 @ResponseBody public PlanSearchResponseDTO bulkUpdatePlanStatusForSelectedPlans(@RequestBody PlanSearchRequestDTO request) {
		
	  PlanSearchResponseDTO  response = new PlanSearchResponseDTO();
      response.setStatus(PlanMgmtConstants.SUCCESS);
		
		try {
			if(null == request || request.getSelectedPlanIds() == null || StringUtils.isBlank(request.getPlanStatusForUpdate()) || 
					                                                      StringUtils.isBlank(request.getIssuerVerificationStatusForUpdate()) ||
					                                                      StringUtils.isBlank(request.getEnrollmentAvailForUpdate())) {
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				response.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				response.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				return response;
				
			}
			response = planMgmtService.bulkUpdatePlanStatusForSelectedPlans(request);
			
			
				
		} catch (Exception ex) {
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "PlanRestController - Failed to execute bulkUpdatePlanStatusForSelectedPlans.", ex);
			LOGGER.error("PlanRestController - Failed to execute bulkUpdatePlanStatusForSelectedPlans:", ex);
			response.setErrCode(PlanMgmtErrorCodes.ErrorCode.BULK_UPDATE_PLAN_STATUS_FOR_SELECTED_PLANS.getCode());
			response.setErrMsg(Arrays.toString(ex.getStackTrace()));
			response.setStatus(PlanMgmtConstants.FAILURE);
		} 
        return response;
	 }
	 
	 /**
		 * Method is used to retrieve all Tenants for respective plan.
		 */
		@RequestMapping(value = "/getTenants/{planId}", method = RequestMethod.GET)
		@Produces("application/json")
		@ResponseBody
		public PlanTenantResponseDTO getTenants(@PathVariable("planId") String planId) {

			PlanTenantResponseDTO response = new PlanTenantResponseDTO();
			response.setStatus(PlanMgmtConstants.SUCCESS);
			try {
				if (!StringUtils.isNumeric(planId)) {
					LOGGER.error("Invalid Plan ID.");
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					response.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
					response.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
					return response;
				}

				Plan planData = planMgmtService.getPlanData(planId);
				response.setPlanId(planData.getId());
				response.setPlanName(planData.getName());
				if (planData.getIssuer() != null) {
					response.setIssuerId(planData.getIssuer().getId());
					response.setHiosIssuerId(planData.getIssuer().getHiosIssuerId());
				}

				List<TenantPlan> tenantList = planMgmtService.getPlanTenants(Integer.parseInt(planId));
				
				if(LOGGER.isDebugEnabled()){
					if (!CollectionUtils.isEmpty(tenantList)) {
						LOGGER.debug("planTenantsList.size: " + tenantList.size());
					} else {
						LOGGER.debug("No Tenants are available for selected plan ID: " + planId);
					}
				}
				List<PlanTenantLightDTO> tenantPlanList = new ArrayList<>();
				for (TenantPlan tPlan : tenantList) {
					PlanTenantLightDTO planTenant = new PlanTenantLightDTO();
					planTenant.setId(tPlan.getId());
					planTenant.setTenantCreatedDate(tPlan.getCreatedDate());
					if (tPlan.getTenant() != null) {
						planTenant.setTenantCode(tPlan.getTenant().getCode());
					}

					tenantPlanList.add(planTenant);
				}
				response.setTenantPlanList(tenantPlanList);
			} catch (Exception ex) {
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null,
						"PlanRestController - Failed to execute getTenants.", ex);
				LOGGER.error("PlanRestController - Failed to execute getTenants:", ex);
				response.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_TENANT.getCode());
				response.setErrMsg(Arrays.toString(ex.getStackTrace()));
				response.setStatus(PlanMgmtConstants.FAILURE);

			}

			return response;
		}	
	
		@RequestMapping(value = "/getFormularyDrugList/{applicableYear}/{issuerHiosId}/{formularyId}", method = RequestMethod.GET)
		@Produces("application/json")
		@ResponseBody public FormularyDrugListResponseDTO getFormularyDrugList(@PathVariable("applicableYear") int applicableYear, @PathVariable("issuerHiosId") String issuerHiosId, @PathVariable("formularyId") String formularyId) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("=========  Inside getFormularyDrugList() ============ ");
			}
			return planMgmtService.getFormularyDrugList(applicableYear, issuerHiosId, formularyId);
		}

}
