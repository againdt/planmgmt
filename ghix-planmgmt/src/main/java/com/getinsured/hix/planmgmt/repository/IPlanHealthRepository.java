package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.PlanHealth;

@Repository("iPlanHealthRepository")
public interface IPlanHealthRepository extends JpaRepository<PlanHealth, Integer>, RevisionRepository<PlanHealth, Integer, Integer> {

	@Query("SELECT planHealth.id,planHealth.plan.id FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId) and planHealth.costSharing = :costSharing ")
    Object[] getPlanHealthIdByParentId(@Param("parentPlanId") String parentPlanId,@Param("costSharing") String costSharing);
	
	@Query("SELECT planHealth.acturialValueCertificate, planHealth.benefitFile, planHealth.costSharing FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId)")
    List<Object[]> getPlanHealthDetails(@Param("parentPlanId") String parentPlanId);
    
    @Query("SELECT planHealth.plan.id FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId) and planHealth.costSharing = :costSharing ")
    List<Integer> getCostSharingPlanIds(@Param("parentPlanId") String parentPlanId,@Param("costSharing") String costSharing);

    @Query("SELECT planHealth.plan.id FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId) ")
    List<Integer> getChildPlanIds(@Param("parentPlanId") String parentPlanId);
    
    @Query("SELECT planHealth.id FROM PlanHealth planHealth WHERE planHealth.plan.id = :planId ")
    Integer getPlanHealthId(@Param("planId") Integer planId);
    
    @Query("SELECT planHealth.planSbcScenarioId, p.issuerPlanNumber FROM PlanHealth planHealth, Plan p WHERE p.id = planHealth.plan.id AND p.issuerPlanNumber IN(:hiosPlanIdList) AND p.isDeleted='N' AND p.applicableYear =:applicableYear) ")
    List<Object[]> getSbcScenarioIdsByHiosPlanIds(@Param("hiosPlanIdList") List<String> hiosPlanIdList, @Param("applicableYear") Integer applicableYear);
    
}


