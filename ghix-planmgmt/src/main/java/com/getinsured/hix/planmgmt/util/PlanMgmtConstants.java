package com.getinsured.hix.planmgmt.util;

import java.text.SimpleDateFormat;

import com.getinsured.hix.platform.util.GhixConstants;

public final class PlanMgmtConstants {

	private PlanMgmtConstants() {
	}

	// Integer variable Declaration
	public static final int MINUS_ONE = -1;
	public static final int ZERO = 0;
	public static final int ONE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	public static final int FOUR = 4;
	public static final int FIVE = 5;
	public static final int SIX = 6;
	public static final int SEVEN = 7;
	public static final int EIGHT = 8;
	public static final int NINE = 9;
	public static final int TEN = 10;
	public static final int ELEVEN = 11;
	public static final int TWELVE = 12;
	public static final int THIRTEEN = 13;
	public static final int FOURTEEN = 14;
	public static final int FIFTEEN = 15;
	public static final int SIXTEEN = 16;
	public static final int SEVENTEEN = 17;
	public static final int EIGHTEEN = 18;
	public static final int NINTEEN = 19;
	public static final int TWENTY = 20;
	public static final int TWENTYONE = 21;
	public static final int TWENTYTWO = 22;
	public static final int TWENTYTHREE = 23;
	public static final int TWENTYFOUR = 24;
	public static final int TWENTYFIVE = 25;
	public static final int TWENTYSIX = 26;
	public static final int TWENTYSEVEN = 27;
	public static final int TWENTYEIGHT = 28;
	public static final int TWENTYNINE = 29;
	public static final int THIRTY = 30;
	public static final int THIRTYONE = 31;
	public static final int THIRTYTWO = 32;
	public static final int THIRTYTHREE = 33;
	public static final int THIRTYFOUR = 34;
	public static final int THIRTYFIVE = 35;
	public static final int THIRTYSIX = 36;
	public static final int THIRTYSEVEN = 37;
	public static final int THIRTYEIGHT = 38;
	public static final int THIRTYNINE = 39;
	public static final int FOURTY = 40;
	public static final int FOURTYONE = 41;
	public static final int FOURTYSIX = 46;
	public static final int SIXTY_FOUR = 64;
	public static final int TOTALCSVCOLUMN = 24;
	public static final int LISTING_NUMBER = 5;
	public static final int END_DATE_SPAN = 12;
	public static final int DECREMENT_SPAN_FOR_END_DATE = -1;
	public static final int COLUMN1 = 0;
	public static final int COLUMN2 = 1;
	public static final int COLUMN3 = 2;
	public static final int RATING_AREA_START = 1;
	public static final int RATING_AREA_END = 19;
	public static final int VALID_ZIP_CODE_LENGTH = 5;
	public static final int TIME_OUT = 1800;
	public static final int HUNDRED = 100;
	public static final int TWOHUNDRED = 200;
	public static final int THOUSAND = 1000;
	public static final int TWENTYFIVEHUNDRED = 2500;
	public static final String ONE_EIGHTY_FIVE_DAYS = "185";
	public static final String THIRTY_DAYS = "30";
	//Boolean variable Declaration

	/*	String Variable Declaration	*/
	public static final String DOLLAR_SIGN = "$";
	public static final String PERCENT_SIGN = "%";
	public static final String STR_ZERO = "0";
	public static final String STR_ONE = "1";
	public static final String STR_THIRTY = "30";
	public static final String STR_THREE_FIVETIMES_ZERO = "300000";
	public static final String EMPTY_STRING = "";
	public static final String SINGLE_DASH_STRING = "-";
	public static final String FORWARD_SLASH_STRING = "/";
	public static final String TRIPLE_DASH_STRING = " --- ";
	public static final String IHC = "IHC";
	public static final String COMMA = ",";
	//Boolean variable Declaration
	public static final String INVALID_REQUEST = "Invalid request parameters";
	public static final String INVALID_EFFECTIVE_DATE_FORMAT = "Invalid effective date format";
	
	/*	Adding constants for STM	*/
	public static final int PLAN_ID_STM_1 = 1000001;
	public static final int PLAN_ID_STM_2 = 331042;
	public static final int PLAN_ID_STM_3 = 331045;
	public static final int PLAN_ID_STM_4 =  331046;
	public static final int PLAN_ID_STM_5 = 331047;
	public static final int PLAN_ID_STM_6 = 331052;
	
	public static final float FIFTY_POINT_THIRTY = 50.30F;
	public static final float FOURTY_POINT_ZERO_ZERO = 40.00F;
	public static final float THIRTYNINE_POINT_FOURTY = 39.40F;
	public static final float FOURTYNINE_POINT_THIRTY = 49.30F;
	public static final float FIFTYFOUR_POINT_ZERO = 54.00F;
	public static final float SIXTYFOUR_POINT_ZERO = 64.00F;
	public static final int NINE_NINE_NINE = 999;
	public static final int NINE_NINE_ZERO = 990;
	
	public static final String PLAN_IDS = "PLANID";
	public static final String PLAN_STM_IDS = "PLANSTMID";
	public static final String PLAN_STM_INFO = "PLANINFO";
	
	public static final String ANY_TIER = "Any Tier";
	public static final String CS = "CS";
	public static final String ONE_STR = "1";
	public static final String URL_REGEX = "^(http|https)://.*$";
	public static final String ECM_REGEX = "^(?i:workspace)(:)(\\/\\/)(?i:SpacesStore)(\\/)([A-Za-z0-9\\-])+(;)([0-9\\.]{1,5})$";
	
	public static final String OPENING_BRACKET = "(";
	public static final String CLOSING_BRACKET = ")";
	public static final String SINGLE_QUOTE = "'";
	public static final String SEMICOLON_SEPERATOR = ";";
		
	public static final String PRIMARY_VISIT = "PRIMARY_VISIT";
	public static final String OFFICE_VISIT = "OFFICE_VISIT";
	public static final String GENERIC = "GENERIC";
	public static final String DEDUCTIBLE_MEDICAL = "DEDUCTIBLE_MEDICAL";
	public static final String DEDUCTIBLE_INTG_MED_DRUG = "DEDUCTIBLE_INTG_MED_DRUG";
	public static final String MAX_OOP_MEDICAL = "MAX_OOP_MEDICAL";
	public static final String MAX_OOP_INTG_MED_DRUG = "MAX_OOP_INTG_MED_DRUG";
	public static final String DEDUCTIBLE = "DEDUCTIBLE";
	public static final String STRZERO = "0";
	public static final String STRHUNDRED = "100";
	public static final String NO = "No";
	public static final String Y = "Y";
	public static final String N = "N";
	public static final String YES = "Yes";
	public static final String PLAN_STM_ID = "PLAN_STM_ID";
	public static final String NA = "NA";
	
	public static final String NETWORK_T1_DISPLAY = "netWkT1Disp";
	public static final String TIER1_COPAY_VALUE = "tier1CopayVal";
	public static final String IN_NETWORK_IND = "inNetworkInd";
	public static final String IN_NETWORK_FLY = "inNetworkFly";
	public static final String EHB_APPT_FOR_PEDIATRIC_DENTAL = "ehb_appt_for_pediatric_dental";
	public static final String EHB_APPT_FOR_PEDIATRIC_DENTAL_VALUE = "ehb_appt_for_pediatric_dental_value";
	public static final String PLAN_COST_MAP = "PlanCostMap";
	public static final String OPTIONAL_COST_MAP = "OptionalPlanCostMap";
	
	public static final String OOP_MAX_VAL = "OOP_MAX_VAL";
	public static final String OOP_MAX_FAMILY = "OOP_MAX_FAMILY";
	public static final String COINSURANCE = "COINSURANCE";
	public static final String MAX_DURATION = "MAX_DURATION";
	public static final String DURATION_UNIT = "DURATION_UNIT";
	public static final String POLICY_MAX = "POLICY_MAX";
	public static final String OUT_OF_NETWORK_COVERAGE = "OUT_OF_NETWORK_COVERAGE";
	public static final String OUT_OF_COUNTRY_COVERAGE = "OUT_OF_COUNTRY_COVERAGE";
	public static final String SEPARATE_DEDT_PRESCPT_DRUG = "SEPARATE_DEDT_PRESCPT_DRUG";
	
	public static final String ON = "ON";
	public static final String OFF = "OFF";
	public static final String TENANT_GINS = "GINS";
	
	public static final String RELATION = "relation";
	public static final String ZIP = "zip";
	public static final String AGE = "age";
	public static final String TOBACCO = "tobacco";
	public static final String COUNTYCODE = "countycode";
	public static final String ID = "id";
	public static final String CHILD = "child";
	public static final String SELF = "self";
	public static final String MEMBER = "member";
	
	public static final String FAMILY_LOOKUP_NAME = "SERFF_FAMILY_OPTION";
	public static final String FAMILY_RATE_LOOKUP_NAME = "SERFF_FAMILY_OPTION";
	public static final String CHILD_ONLY = "CHILDONLY";
	public static final String ADULT_ONLY = "ADULTONLY";
	public static final String ADULT_AND_CHILD = "ADULTANDCHILD";
	public static final String FAMILY_TIER_PRIMARY_ONE_DEPENDENT = "Primary Subscriber and One Dependent";
	public static final String FAMILY_TIER_PRIMARY_TWO_DEPENDENT = "Primary Subscriber and Two Dependents";
	public static final String FAMILY_TIER_PRIMARY_MANY_DEPENDENT = "Primary Subscriber and Three or More Dependents";
	public static final String FAMILY_TIER_COUPLE_ONE_DEPENDENT = "Couple and One Dependent";
	public static final String FAMILY_TIER_COUPLE_TWO_DEPENDENT = "Couple and Two Dependents";
	public static final String FAMILY_TIER_COUPLE_MANY_DEPENDENT = "Couple and Three or More Dependents";
	public static final String FAMILY_TIER_PRIMARY = "Individual Rate";
	public static final String FAMILY_TIER_COUPLE = "Couple";
	
	public static final String DOWNLOAD_DOCUMENT_URL = "download/document?documentId="; // document download url
		
	// other PlanMgmtConstants merging for Hix-44588
	
	/*	Common String variables		*/
	public static final String MODULE_NAME= "PM";
	public static final String PHONE1 = "phone1";
	public static final String PHONE2 = "phone2";
	public static final String PHONE3 = "phone3";
	public static final String PHONE4 = "phone4";
	public static final String PHONE5 = "phone5";
	public static final String PHONE6 = "phone6";
	public static final String PHONE7 = "phone7";
	public static final String PHONE8 = "phone8";
	public static final String PHONE9 = "phone9";
	public static final String DUPLICATE_HIOS = "duplicateHios";
	public static final String ZERO_STR = "0";
	public static final String TWO_STR = "2";
	public static final String THREE_STR = "3";
	public static final String FOUR_STR = "4";
	public static final String FIVE_STR = "5";
	public static final String CONTENT_TYPE = "text/html";
	public static final String STATE_CODE_CA = "CA";
	public static final String UPLOADPATH = GhixConstants.UPLOAD_PATH;
	public static final String UNUSED = "unused";
	public static final String COMMA_STRING = ",";
	public static final String UNDERSCORE_STRING = "_";
	public static final String DASH_SIGN = " - ";
	public static final String COMPANY_LOGO = "companyLogo";
	public static final String ISSUER_ID_DASH = "issuer_Id";
	public static final String LOGO_IMAGE = "logo_image";
	public static final String IS_PAYMENT_GATEWAY_STRING = "financial.IS_PAYMENT_GATEWAY";
	public static final String MANAGE_FINANCIAL_INFORMATION_PERMISSION_STRING = "hasPermission(#model, 'MANAGE_FINANCIAL_INFORMATION')";
	public static final String MANAGE_ISSUER_PERMISSION_STRING = "hasPermission(#model, 'MANAGE_ISSUER')";
	public static final String PARTNER_ID = "partnerId";
	public static final String MANAGE_REPRESENTATIVE_REDIRECT_URL = "redirect:/admin/issuer/representative/manage/";
	public static final String COMPANY_PROFILE_REDIRECT_URL = "redirect:/admin/issuer/company/profile/";
	public static final String FAILURE = "FAILURE";
	public static final String RAW_TYPE = "rawtypes";
	
	/*	Generate Plan Report	*/
	public static final String QHP_GENERATE_PALN_REPORT_MESSAGE = "Your Report is being generated for the selected plans. Please look under the 'Reports' tab to find your 'QHP Rate and Benefit Report'";
	public static final String SADP_GENERATE_PALN_REPORT_MESSAGE = "Your Report is being generated for the selected plans. Please look under the 'Reports' tab to find your 'SADP Rate and Benefit Report'";
	public static final String ERROR_WHILE_ADDING_IN_QUEUE = "Error While adding you for Report Generation";

	public static final String INPUT_BUILD_QUERY_MSG = "INPUT buildquery :: ";
	
	/*	Constants For Issuer Report Controller	*/
	public static final String ISSUER = "Issuer";
	public static final Integer YEAR_DEFAULT_SPAN = 5;
	public static final Integer MONTHLY_DEFAULT_SPAN = 12;
	public static final Integer QUARTERLY_DEFAULT_SPAN = 4;
	public static final String MONTHLY_PERIOD = "Monthly";
	public static final String QUARTERLY_PERIOD = "Quarterly";
	public static final String YEARLY_PERIOD = "Yearly";
	public static final String DENTAL = "DENTAL";
	public static final String HEALTH = "HEALTH";
	public static final String INDIVIDUAL = "INDIVIDUAL";
	public static final String CURRENT_DATE = "CURRENT_DATE";
	public static final String REQUIRED_PREVIOUS_DATE = "REQUIRED_PREVIOUS_DATE";
	public static final String REQUIRED_DATE_FORMAT = "yyyy-MM-dd";
	public static final String REQUIRED_DATE_FORMAT_FOR_REPORT = "MM/dd/yyyy";
	public static final Integer YEARLY_DEC = -5;
	public static final Integer Q1_START = 1;
	public static final Integer Q1_END = 3;
	public static final Integer Q2_START = 4;
	public static final Integer Q2_END = 6;
	public static final Integer Q3_START = 7;
	public static final Integer Q3_END = 9;
	public static final Integer Q4_START = 10;
	public static final Integer Q4_END = 12;
	public static final Integer PAGE_SIZE = 30;
	public static final Integer ONE_EIGHT_FOUR = 189;
	public static final int IN_CLAUSE_ITEM_COUNT = 500;
	public static final String PLAN_NUMBER_FOR_QHP = "planNumberForQHP";
	public static final String MARKET_NUMBER_FOR_QHP = "marketForQHP";
	public static final String REGION_NAME_FOR_QHP = "rNameForQHP";
	public static final String PLATINUM_CHECK_FOR_QHP = "platinumCheckQHP";
	public static final String GOLD_CHECK_FOR_QHP = "goldCheckQHP";
	public static final String SILVER_CHECK_FOR_QHP = "silverCheckQHP";
	public static final String BRONZE_CHECK_FOR_QHP = "bronzeCheckQHP";
	public static final String CATASTROPHIC_CHECK_FOR_QHP = "catastrophicCheckQHP";
	public static final String PERIOD_ID_FOR_QHP = "periodIDForQHP";
	public static final String PLAN_LEVEL_FOR_QHP = "planLevelForQHP";
	public static final String RESET_QUERYSTRING = "r";
	public static final String PLAN_ID = "planId";
	public static final String MARKET_ID = "marketID";
	public static final String REGION_NAME = "rName";
	public static final String PERIOD_ID = "periodID";
	public static final String PLAN_LEVEL = "planLevel";
	public static final String ALL_PLAN_LEVELS_FOR_QHP = "platinum,gold,silver,bronze,catastrophic";
	public static final String ALL_PLAN_LEVELS_FOR_SADP = "high,low";
	public static final String ALL_MARKET_TYPES = "INDIVIDUAL,SHOP";
	public static final String PLAN_LIST = "planList";
	public static final String REGION_LIST = "regionList";
	public static final String PLAN_NAME_FOR_SADP = "planNameForSADP";
	public static final String MARKET_NUMBER_FOR_SADP = "marketForSADP";
	public static final String REGION_NAME_FOR_SADP = "rNameForSADP";
	public static final String LOW_CHECK_FOR_SADP = "lowCheckForSADP";
	public static final String HIGH_CHECK_FOR_SADP = "highCheckForSADP";
	public static final String PERIOD_ID_FOR_SADP = "periodIDForSADP";
	public static final String PLAN_LEVEL_FOR_SADP = "planLevelForSADP";
	public static final String TIME_PERIOD = "timePeriod";
	public static final String PLAN_MARKET = "planMarket";
	public static final String ENROLLMENT = "enrollment";
	public static final String LIST_PAGE_DATA= "listPageData";
	public static final String LIST_PAGE_SIZE= "pageSize";
	public static final String HIGH_CHECK= "highCheck";
	public static final String LOW_CHECK= "lowCheck";
	public static final String PLATINUM_CHECK = "platinumCheck";
	public static final String GOLD_CHECK = "goldCheck";
	public static final String SILVER_CHECK = "silverCheck";
	public static final String BRONZE_CHECK = "bronzeCheck";
	public static final String CATASTROPHIC_CHECK = "catastrophicCheck";

	/*	Constants For QHPAdminController	*/
	public static final String PAGE_TITLE = "page_title";
	public static final String IS_EDIT = "isEdit";
	public static final String PLAN_OBJ = "plan";
	public static final String PLAN_CLASS = "Plan";
	public static final String NETWORK = "network";
	public static final String COMMENTS = "comments";
	public static final String DATA = "data";
	public static final String SORT_ORDER = "sortOrder";
	public static final String SORT_BY = "sortBy";
	public static final String STATUS = "status";
	public static final String VERIFIED = "verified";
	public static final String LEVEL_VERIFIED = "Verified";
	public static final String MARKET = "market";
	public static final String RATE_FILE = "rateFile";
	public static final String BENEFIT_FILE = "benefitFile";
	public static final String PLAN_NUMBER = "planNumber";
	public static final String ISSUER_NAME = "issuerName";
	public static final String PLAN_NAME = "planName";
	public static final String ISSUERNAME = "issuername";
	public static final String ISSUER_VERIFICATION = "issuerVerificationStatus";
	public static final String STATES = "state";
	public static final String TRUE_STRING = "true";
	public static final String FALSE_STRING = "false";
	public static final String PAGE_SIZE_VAR = "pageSize";
	public static final String DISPLAYEDITBUTTON = "displayEditButton";
	public static final String DISPLAYPROVIDERNETWORK = "displayProviderNetwork";
	public static final String DISPLAYACTION = "displayAction";
	public static final String BENEFIT_FILE_NAME = "Benefit_";
	public static final String CERTIFICATION_DOC_FOLDER = "CertificationDocument";
	public static final String EX_TYPE = "exType";
	public static final String TENANT = "tenant";
	public static final String SUCCESS = "Success";
	public static final Integer PDF_TABLE_SIZE = 100;
	public static final Integer PDF_PAGE_SIZE = 2;

	/*	Constants For Admin Report Controller	*/
	public static final String ISSUER_ID_FOR_QHP = "issuerIdForQHP";
	public static final String ISSUER_ID_FOR_SADP = "issuerIdForSADP";
	public static final String ISSUER_ID = "issuerId";
	public static final String ISSUER_LIST = "issuersList";
	
	public static final String PAYMENT_METHODS_OBJ = "paymentMethodsObj";
	public static final String LIST_EXCHG_PARTNERS_OBJ = "listExchgPartnersObj";
	public static final String ACCOUNT_USER = "AccountUser";
	public static final String STATUS_STRING = "status";
	public static final String DUPLICATE_ISSUER_ERR = "duplicateIssuerErr";	
	public static final String DUPLICATE_EMAIL_ERR = "repDuplicateEmail";
	public static final String ISSUER_OBJ = "issuerObj";
	public static final String STATUS_HISTORY = "statusHistory";
	public static final String STATE_LIST = "statelist";
	public static final String REDIRECT_URL = "redirectUrl";	
	public static final String DEFAULT_SORT = "defaultSort";
	public static final String RESULT_SIZE = "resultSize";
	public static final String REDIRECT_TO = "redirectTo";
	public static final String ISSUER_DETAILS_NOT_SUBMITTED = "issuerDetailsNotSubmitted";
	public static final String ADMIN_LOGIN_PATH = "redirect:/account/user/login?module=admin";
	public static final String MANAGE_ISSUER_PATH = "redirect:/admin/manageissuer";
	public static final String ISSUERQUALITYRATING = "IssuerQualityRating;";
	public static final String QUALITYRATIING_FOLDER = "qualityRating";
	public static final String REQUIRED_DATE_FORMAT_MMDDYYYY = "MM/dd/yyyy";
	public static final String REP_ID = "repId";		
	public static final String UPDATED = "updated";
	public static final String ISSUER_REP_OBJ = "issuerRepObj";
	public static final String INVALID_HIOS_ISSUER_ID = "HIOS Issuer ID does not match with Issuer name";
	public static final String INVALID_EFFECTIVE_START_DATE = "Effective Start State has to be a future date";
	public static final String CORRECT_DATA_FLAG = "0";
	public static final String INCORRECT_DATA_FLAG = "1";
	public static final String GENERATEACTIVATIONLINKINFO = "generateactivationlinkInfo";
	public static final String MULTIPLE_ISSUERS_FOUND = "multiple";
	public static final String AUTHORITYFILE = "authorityFile";
	public static final String MARKETINGFILE = "marketingFile";
	public static final String DISCLOSURESFILE = "disclosuresFile";
	public static final String FINANCDISCLOSURESFILE = "financeDisclosuresFile";
	public static final String ACCREDITATIONFILES = "accreditationFile";
	public static final String ORGANIZATIONFILES = "organizationFiles";
	public static final String SUCCESSFULLYUPLOADED = "successFullyUploaded";
	public static final String DOCUMENT_DOWNLOAD_URL_START = "<a href='";
	public static final String DOCUMENT_DOWNLOAD_ACC_DOCID = "/hix/admin/document/filedownload?documentId=";
	public static final String DOCUMENT_DOWNLOAD_ACC_FLNAME = "&fileName=";
	public static final String DOCUMENT_DOWNLOAD_CLOSING_HREF = "' target=\"_blank\" >";
	public static final String DOCUMENT_DOWNLOAD_URL_END = "</a>";
	public static final String DOUBLE_SLASH = "//";

	public static final String SHOW_OTHER_NAVIGATION = "showOtherNavigation";
	public static final String ALLOW_REVIEW_AND_SUBMIT = "allowReviewAndSubmit";
	public static final String STATECODEONEXCHANGE = "stateCodeOnExchange";
	public static final String STAECODEFORCA = "CA";
	public static final String IS_PAYMENT_GATEWAY_FLAG = "isPaymentGateway";
	public static final String ROUTING_NUMBER = "routing_number";
	public static final String INVALID_ROUTING_NUMBER = "invalid_routing_number";
	public static final String PAYMENT_METHOD_INFO_OBJ = "paymentMethodsObjInfo";	
	public static final String EXCHANGE_TYPE = "exchangeType";
	public static final String STATE_CODE = "STATE_CODE";
	public static final String IS_EMAIL_ACTIVATION = "IS_EMAIL_ACTIVATION";
	public static final String UPLOADED_FILE_NAME = "uploadedFileName";
	public static final String UPLOADED_FILE_LINK = "uploadedFileLink";
	
	public static final String AUTHORITY_DOCUMENT_INFO_MAP = "authorityDocumentInfoMap";
	public static final String ACCREDITATION_DOC_INFO_MAP = "accreditationDocumentInfoMap";
	public static final String ORG_DATA_DOC_INFO_MAP = "orgDataDocumentInfoMap";
	public static final String MARKETING_DOC_INFO_MAP = "marketingDocumentInfoMap";
	public static final String FIN_CLOSURES_DOC_INFO_MAP = "finclosuresDocumentInfoMap";
	public static final String CLAIM_PAYPOL_DOC_INFO_MAP = "claimpaypolDocumentInfoMap";
	public static final String RATING_PRACTICES_DOC_INFO_MAP = "ratingPracticesDocumentInfoMap";
	public static final String COST_SHARING_PAY_DOC_INFO_MAP = "costSharingPayDocumentInfoMap";
	
	public static final String ADD_SUPP_DOC_INFO_MAP = "addSupportDocumentInfoMap";
	
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	
	public static final long FUNTIONAL_IDENTIFIER_VAL = 4000000001l;

	public static final String TRANSACTION_TYPE_LIST = "transactionTypeList";
	public static final String ISA_05_VALUES = "isa05Values";
	public static final String ISA_07_VALUES = "isa07Values";
	public static final String GS08_VALUES = "gs08Values";
	public static final String EDI_MAP_NAME = "ediMapNameValues";
	public static final String EDI_VALIDATION_STANDARD = "ediValidationStandardValues";
	public static final String EDI_APF = "ediApfValues";
	public static final String EDI_ROLES = "ediRoles";

	public static final String GS08_834 = "834";
	public static final String GS08_820 = "820";
	public static final String GS08_999 = "999";
	public static final String GS08_TA1 = "TA1";

	public static final String KEY_ISA05 = "ISA05";
	public static final String KEY_ST01 = "ST01";
	public static final String KEY_ISA07 = "ISA07";
	public static final String KEY_EDI_MAP_NAME = "EDI_MAP_NAME";
	public static final String KEY_EDI_VALIDATION_STANDARD = "EDI_VALIDATION_STANDARD";
	public static final String KEY_EDI_APF = "EDI_APF";
	public static final String KEY_ISA_05_DEFAULT_VALUE = "isa05DefaultValue";
	public static final String KEY_ISA_07_DEFAULT_VALUE = "isa07DefaultValue";
	public static final String KEY_ISA_06_DEFAULT_VALUE = "isa06DefaultValue";
	public static final String KEY_ISA_08_DEFAULT_VALUE = "isa08DefaultValue";
	public static final String KEY_GS_02_DEFAULT_VALUE = "gs02DefaultValue";
	public static final String KEY_GS_03_DEFAULT_VALUE = "gs03DefaultValue";
	public static final String PAYMENT_ID = "paymentId";
	public static final String UPDATE = "update";
	public static final String EDIT = "edit";
	public static final String CURRENT_PAYMENT_ID = "curent_payment_id";
	public static final String FLOW = "flow";
	public static final String IS_ISSUER_CREATED = "isIssuerCreated";
	//for dynamic configuration
	public static final String PLANMGMT_HEALTHBENEFITS = "planManagement.Health";
	public static final String PLANMGMT_DENTALBENEFITS = "planManagement.Dental";
	public static final String PLANMGMT_DUPLICATE_KEY = "2";
	public static final Long  FILE_SIZE = 512000L;
	
	/*	Common Integer Variables	*/
	public static final int BYTE_ARRAY_FILE_SIZE = 5120;
	
	public static final int ONE_FOUR_SIX = 146;
	public static final int TWO_FIVE_FIVE = 255;
	public static final int SEVEN_NINE = 79;
	
	public static final int TWO_ONE_NINE = 219;
	public static final int TWO_TWO_NINE = 229;
	public static final int TWO_FOUR_ONE = 241;
	public static final int ONE_NINE_SEVEN = 197;
	public static final int TWO_ONE_SEVEN = 217;
	public static final int ONE_ONE_ZERO = 110;
	public static final int ONE_NINE_TWO = 192;
	public static final int TWO_THREE_THREE = 233;
	public static final int TWO_FIVE_THREE = 253;
	
	/*	CONSTANTS FOR PLAN COMPARE REPORT	*/
	public static final String PLAN_BENEFIT_FILE = "Plan Benefit";
	public static final String IN_NETWORK = "In Network";
	public static final String OUT_NETWORK = "Out Network";
	public static final String COMBINED_IN_OUT_NETWORK = "Combined In/Out Network";
	public static final String DEDUCTIBLES = "Deductibles";
	public static final String INDIVIDUAL_FILE = "Individual";
	public static final String FAMILY = "Family";
	public static final String RATING_AREA = "Rating Area";
	public static final String NON_TOBACCO = "Non-tobacco";
	public static final String MARKET_TYPE = "Market Type";
	public static final String PLAN_NAME_FILE = "Plan Name";
	public static final String PLAN_ID_FILE = "Plan ID";
	public static final String PLAN_TYPE_FILE = "Plan Type";
	public static final String MARKET_LEVEL_FILE = "Metal Level";
	public static final String SMOKE_STATUS = "Smoke Status";
	public static final String AGE_FILE = "Age";
	public static final String PLANID_MAP = "PLANID";
	public static final String PLANMARKET_MAP = "PLANMARKET";
	public static final String PLANNAME_MAP = "PLANNAME";
	public static final String ISSUERPLANNUMBER_MAP = "ISSUERPLANNUMBER";
	public static final String NETWORKTYPE_MAP = "NETWORKTYPE";
	public static final String PLANLEVEL_MAP = "PLANLEVEL";
	public static final String BENEFITS_FILE = "Benefits";
	public static final String COPAY_FILE = "Copay";
	public static final String COINSURANCE_FILE = "Coinsurance";
	public static final String LIMITATION_FILE = "Limitation";
	public static final String LIMITATION_ATTRIB_FILE = "Limitation Attribute";
	public static final String QHP_FILE = "QHP_";
	public static final String PLAN_REPORT_FILE = "_Plan_Report_";
	public static final String SADP_FILE = "SADP_";
	public static final String SINGLE_SLASH = "/";
	public static final String DASH_FILE = "-";
	public static final String COSTSHARINGREGEX = "^([^A-Z]*?[A-Z][A-Z]*?)+.?";
	public static final String BENEFITREGEX = "^([^A-Z]*?[A-Z][A-Z]*?)+.?";
	public static final String DATEFORMATFORFILENAME = "yyyyMMdd_HHmmss";
	public static final String FILE_EXTENSION = ".xls";
	public static final String DOLLWER_SIGN = "$";
	public static final String PERCENTAGE_SIGN = "%";
	public static final String PLUS_SIGN = "+";
	public static final String AGE_RANGE_START = "0-20";
	public static final int RATING_AREA_RECORD_LIMIT_HEALTH = 190;
	public static final int RATING_AREA_RECORD_LIMIT_DENTAL = 52;
	public static final int MAX_AGE = 65;
	public static final int HEALTH_ROW_LIMIT = 80;
	public static final int DENTAL_ROW_LIMIT = 15;
	public static final int TWO_ZERO_ONE_FIVE = 2015;
	
	public static final String PLAN_HEALTH_IDS_LIST = "PlanHealthIdsList";
	public static final String EFFECTIVE_DATE = "EffectiveDate";
	public static final String NOTAVAILABLE = "not available";
	public static final int STATUS_FAILURE_CODE = 1;
	
	public static final String INNETWORK = "inNetWork";
	public static final String OUTNETWORK = "outNetWork";
	public static final String UNKNOWN_AFFILIATION = "Unknown Affiliation";
	public static final String NETWORK_STATUS = "networkStatus"; 
	public static final String PROVIDER_ID = "providerId";
	
	public static final String LOWEST_PREMIUM = "LowestPremium";
	public static final String HIGHEST_PREMIUM = "HighestPremium";
	public static final String PLANID = "PLAN_ID";
	public static final String PLANHEALTHID = "PLAN_HEALTH_ID";
	public static final String PLANNAME = "PLAN NAME";
	public static final String ISSUERID = "ISSUER_ID";
	public static final String TOTALPREMIUM = "TOTAL_PREMIUM";
	public static final String PLANLEVEL = "PLAN_LEVEL";
	public static final String INAME = "ISSUER_NAME";
	public static final String INTERNAL_PLAN_ID = "PID";
	
	public static final String OOP_MAX_IND = "OOP_MAX_IND";
	public static final String OOP_MAX_FLY = "OOP_MAX_FLY";
	public static final String DEDUCTIBLE_IND = "DEDUCTIBLE_IND";
	public static final String DEDUCTIBLE_FLY = "DEDUCTIBLE_FLY";
	public static final String MONTHLY_PREMIUM = "MONTHLY PREMIUM";
	public static final String REQUIRED_FORMAT_YYYYMMDD = "YYYY-MM-dd";
	public static final String GILPP = "GILPP";
	public static final String GILGP = "GILGP";
	public static final String GILSP = "GILSP";
	public static final String GILBP = "GILBP";
	public static final String GIHPP = "GIHPP";
	public static final String GIHGP = "GIHGP";
	public static final String GIHSP = "GIHSP";
	public static final String GIHBP = "GIHBP";
	
	public static final String PLAN_NAME_KEY = "Plan Name";
	public static final String MONTHLY_PREMIUM_KEY = "Monthly Premium";
	public static final String OOP_MAX_IND_KEY = "Oop_Max_Ind";
	public static final String OOP_MAX_FLY_KEY = "Oop_Max_Fly";
	public static final String DEDUCTIBLE_IND_KEY = "Deductible_Ind";
	public static final String DEDUCTIBLE_FLY_KEY = "Deductible_Fly";
	public static final String GENERIC_KEY = "Generic";
	public static final String PRIMARY_VISIT_KEY = "Primary_Visit";
	public static final Double ZERO_PREMIUM = 0.0D;
	
	public static final String NETWORK_LIMIT = "netwkLimit";
	public static final int THOUSAND_NINE_EIGHTY_NINE = 1989;
	public static final int THOUSAND_NINE_EIGHTY = 1980;
	
	public static final String HTTP_PROTOCOL = "http://";
	public static final String RATINGS_FOLDER = "ratings";
	public static final String PLAN_MGMT_REST_URL = "Plan Management REST URLs";
	public static final String PLAN_MGMT_MAIL_TEMPLATE_CONF = "Plan management mail template configuration";
	public static final String AME = "AME";	
	public static final String MAX_BENEFIT = "MAX_BENEFIT";	
	public static final String DISPLAY_VAL = "displayVal";
	
	public static final String APPILICANT_STATE_CODE = "APPILICANT_STATE_CODE";
	
	public static final String IHC_INDV_CODE = "1008";
	public static final String IHC_INDV_PULS_ONE_CODE = "1001";
	public static final String IHC_INDV_PULS_TWO_CODE = "1002";
	public static final String IHC_INDV_PULS_THREE_CODE = "1003";
	public static final String IHC_INDV_PULS_FOUR_CODE = "1004";
	public static final String IHC_INDV_PULS_FIVE_CODE = "1005";
	public static final String IHC_INDV_PULS_SIX_CODE = "1006";
	public static final String IHC_INDV_PULS_SEVEN_CODE = "1007";
	
	public static final String NETWORK_LIMIT_ATTRIBUTE = "netwkLimitAttrib";
	public static final String NETWORK_EXEC = "netwkExce";
	public static final String TIER1_COPAY_ATTRIBUTE = "tier1CopayAttrib";
	public static final String TIER1_COINSVAL = "tier1CoinsVal";
	public static final String TIER2_COINSVAL = "tier2CoinsVal";
	public static final String OUTNETWORK_COINSVAL = "outNetWkCoinsVal";
	public static final String OUTNETWORK_COINS_ATTRIBUTE = "outNetWkCoinsAttrib";
	public static final String TIER1_COINS_ATTRIBUTE = "tier1CoinsAttrib";
	public static final String TIER2_COINS_ATTRIBUTE = "tier2CoinsAttrib";
	public static final String TIER2_COPAY_VALUE = "tier2CopayVal";
	public static final String OUTNETWORK_COPAY_VALUE = "outNetWkCopayVal";
	public static final String OUTNETWORK_COPAY_ATTRIBUTE = "outNetWkCopayAttrib";
	public static final String TIER2_COPAY_ATTRIBUTE = "tier2CopayAttrib";
	public static final String SUBTONETDEDUCT = "subToNetDeduct";
	public static final String SUBTONONNETDEDUCT = "subToNonNetDeduct";
	public static final String ISCOVERED = "isCovered";
	public static final String EXPLANATION = "explanation";
	public static final String NETWORK_T2_DISPLAY = "netWkT2Disp";
	public static final String OUTNETWORK_DISPLAY = "outNetWkDisp";
	public static final String LIMITEXCEPDISPLAY = "limitExcepDisp";
	public static final String NETWORKTILEDISPLAY = "netWkT1TileDisp";
	public static final String MEMBERLIST = "memberList";
	public static final String PLANRATESERVICE_CONFIGURE_ERROR = "PlanRateBenefitService is improperly configured.";
	public static final String EFFECTIVEDATE = "effectiveDate";
	public static final String QUESTION_MARK = "?";
	public static final String COMA_QUESTION_MARK = ",?";
	public static final String APPLICANT = "applicant";
	public static final String PLAN_STATUS_CERTIFIED = com.getinsured.hix.model.Plan.PlanStatus.CERTIFIED.toString();
	public static final String PLAN_ISSUER_VERIFIED = com.getinsured.hix.model.Plan.IssuerVerificationStatus.VERIFIED.toString();
	public static final String PLAN_ENROLLMENT_AVAILABLE = com.getinsured.hix.model.Plan.EnrollmentAvail.AVAILABLE.toString();
	public static final String PLAN_MARKET_INDIVIDUAL = com.getinsured.hix.model.Plan.PlanMarket.INDIVIDUAL.toString();
	public static final String PLAN_EXCHANGE_TYPE_ON = "ON";
	public static final String PLAN_EXCHANGE_TYPE_OFF = "OFF";
	
	public static final String COPAY_VALUE = "copayVal";
	public static final String COPAY_ATTRIBUTE = "copayAttrib";
	public static final String BENEFIT_DETAILS = "benefitDetails";
	public static final String LONG_VALUE = "longValue";
	public static final String SHORT_VALUE = "shortValue";
	public static final String TINY_VALUE = "tinyValue";
	
	public static final String DRUG_NAME = "drugName";
	public static final String DRUG_TIER = "drugTier";
	public static final String IS_SUPPORT_PLAN = "isSupportPlan";
	public static final String DRUG_RX_CODE = "drugRxCode";
	public static final String COINSURANCE_AMOUNT = "coinsurance_amount";
	public static final String COPAYMENT_AMOUNT = "copayment_amount";
	public static final String COINSURANCE_PERCENT = "coinsurance_percent";
	public static final String COST_SHARE_TYPE = "cost_share_type";
	public static final String DRUG_PRICE = "drugPrice";
	public static final String FOR_PRESC_PERIOD_1 = "FOR_PRESC_PERIOD_1";
	public static final String COPAYMENT = "Copayment";
	public static final String COINSURANCE_STR = "Coinsurance";
	public static final String COPAY_COINSURANCE = "CoPay CoInsurance";
	public static final String GRATER_OF_COPAYMENT_OR_COINSURANCE = "Greater of Copayment or Coinsurance";
	public static final String LESSER_OF_COPAYMENT_OR_COINSURANCE = "Lesser of Copayment or Coinsurance";
	public static final String VISION = "VISION" ;
	public static final String GENDER = "gender";
	public static final String MALE = "Male";
	public static final String FEMALE = "Female";
	public static final String M = "M";
	public static final String F = "F";
	public static final String SPOUSE = "spouse";
	
	public static final String COINSURANCE_ONLY_CASE = "Coinsurance Only";
	public static final String COPAYMENT_ONLY_CASE = "Copayment Only";
	public static final String GRATER_OFCOPAYMENT_OR_COINSURANCE_CASE = "Greater of Copayment or Coinsurance";
	public static final String LESSER_OFCOPAYMENT_OR_COINSURANCE_CASE = "Lesser of Copayment or Coinsurance";
	public static final String COINSURANCE_PERCENTAGE_KEY = "coinsurancePercentage";
	public static final String U = "U";
	public static final String RX_CODE_KEY = "rxCodeKey";
	public static final String FAMILY_RATE_OPTION = "F";
	public static final String AGE_RATE_OPTION = "A";
	
	public static final String HIOS_PLAN_NUMBER = "hiosPlanNumber";
	public static final String EFFECTIVE_YEAR = "effectiveYear";
	
	public static final String DOCUMENT_URL = "download/document?documentId="; // document download url
	
	public static final String DEDUCTIBLE_VAL = "DEDUCTIBLE_VAL";
	public static final String MAX_BENEFIT_VAL = "MAX_BENEFIT_VAL";
	public static final String PLAN_AME_ID = "PLAN_AME_ID";
	public static final String NETWORK_TYPE = "NETWORK_TYPE";
	public static final String PLAN_BROCHURE = "PLAN_BROCHURE";
	public static final String DEDUCTIBLE_ATTR = "DEDUCTIBLE_ATTR";
	public static final String BENEFIT_MAX_ATTR = "BENEFIT_MAX_ATTR";
	public static final String EXCLUSIONS_URL = "EXCLUSIONS_URL";
	public static final String QUOTIT = "QUOTIT";
	public static final String PUF = "PUF";
	public static final String MONTHLY_PREMIUM_VAL = "MONTHLY_PREMIUM";
	public static final String DATASRC = "java:comp/env/jdbc/ghixDS";
	public static final int FETCHSIZE = 400;	
	
	public static final String CS1 = "CS1";
	public static final String NETWORK_NAME = "NETWORK_NAME";
	public static final String NETWORK_ID = "NETWORK_ID";
	public static final String NETWORK_URL = "NETWORK_URL";
	public static final String HIOS_ISSUER_ID = "HIOS_ISSUER_ID";
	public static final String T2_COPAY = "T2_COPAY";
	public static final String T2_COINS = "T2_COINS";
	public static final String TIER_2_PRESENT = "TIER_2_PRESENT";
	public static final String APPLICABLE_YEAR = "APPLICABLE_YEAR";
	public static final String ALL = "ALL";
	public static final String LOGO_BY_HIOSID_URL_PATH = "/download/logo/";
	public static final String LOGO_BY_ID_URL_PATH = "/download/logobyid/";
	public static final String EXCHANGE_USER = "exchange@ghix.com";
	
	public static final String ERROR_RE_APPLY_BENEFITS = "Error: Benefits cannot be updated! ";
	public static final String ERROR_RE_APPLY_COSTS = "Error: Deductibles / MOOP cannot be updated! ";
	public static final String BENEFIT_NOT_LOADED = "No benefits available for the plan. ";
	public static final String INVALID_PLAN_TYPE = "Invalid plan type to re-apply benefits.";
	public static final String NO_BENEFIT_EDITED = "No benefit edits found for plan ";
	public static final String NO_VALID_PLAN_FOUND = "No valid benefit edits found.";		
	public static final String BENEFIT_EDIT_UI_MESSAGE = "Warning! Following benefit edits could not be reapplied because the benefit value has changed ";
	public static final String PROCESS_COMPLETED_SUCCESS = "Benefit update successfully done!";
	public static final String PROCESS_COMPLETED_WITH_PARTIAL_SUCCESS = "Benefit update completed with some warnings!";
	public static final String COST_REAPPLY_COMPLETED_SUCCESS = "Deductibles / MOOP update successfully done!";
	public static final String COST_REAPPLY_COMPLETED_WITH_PARTIAL_SUCCESS = "Deductibles / MOOP update completed with some warnings!";
	public static enum NetworkType {
		IN_NETWORK_TIER_1, IN_NETWORK_TIER_2, OUT_OF_NETWORK;
	}

	public static final String NETWORK_KEY = "NETWORK_KEY";
	public static final String YEAR = "YEAR";
	public static final String DEFAULT_USER = "DEFAULT_USER";
	public static final String VENDOR_USER = "VENDOR_USER";
	public static final String INVALID_USER_ID = "Invalid user id";
	public static final String DATA_NOT_AVAILABLE = "Data not available";
	public static final String BBAY = "BBAY";
	public static final double DOUBLE_ZERO = 0.0; 
	
	public static final String PRODUCT_ID = "PRODUCT_ID";
	public static final String PRODUCT_NAME = "PRODUCT_NAME";
	public static final String ISSUER_PLAN_NUMBER = "ISSUER_PLAN_NUMBER";
	public static final String EXCHANGE_TYPE_CAPS = "EXCHANGE_TYPE";
	public static final String ISSUER_VERIFICATION_STATUS = "ISSUER_VERIFICATION_STATUS";
	public static final String STATUS_CAPS = "STATUS";
	public static final String STATE_CAPS = "STATE";
	public static final String CARRIER_NAME = "CARRIER_NAME";
	public static final String NAME = "NAME";
	public static final String ID_CAPS = "ID";
	public static final String RECORD_NOT_FOUND = "Record Not Found";
	public static final String INVALID_ISSUER_LIST = "Invalid Issuer List";
	
//  AHBX plan attribute constants STARTS
	public static final String AHBX_PLAN_ID = "planId";
	public static final String AHBX_ISSUER_ID = "issuerId";
	public static final String AHBX_PLAN_EFFCT_END_DATE = "planEffectiveEndDate";
	public static final String AHBX_PLAN_EFFCT_START_DATE = "planEffectiveStartDate";
	public static final String AHBX_PLAN_LEVEL = "planLevel";
	public static final String AHBX_PLAN_MARKET = "planMarket";
	public static final String AHBX_PLAN_NAME = "planName";
	public static final String AHBX_PLAN_NETWORK_TYPE = "planNetworkType";
	public static final String AHBX_PLAN_RECORD_INDICATOR = "planRecordIndicator";
	public static final String AHBX_PLAN_TYPE = "planType";
	public static final String AHBX_PLAN_HIOS_ID = "hiosPlanId";
	public static final String AHBX_PLAN_STATUS = "planInfoStatus";	
	public static final String AHBX_PLAN_YEAR = "planYear";	
	public static final SimpleDateFormat AHBX_DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
	public static final String DB_ORACLE ="ORACLE";
	public static final String DB_POSTGRESQL ="POSTGRESQL";
	public static final String MEMBER_COVERAGE_DATE = "memberCoverageDate";
	public static final String DAYS_ACTIVE = "daysActive";
	
	public static final String CONFIG_DB_TYPE = "#{configProp['database.type']}";
	public static final String CONFIG_APP_URL = "#{configProp['appUrl']}";
	
	public static final String ERROR_TECHNICAL = "Failed to process the request due to technical error";
	public static final String PLAN_NOT_FOUND = "Requested plan id does not exists";
	public static final String BOTH = "BOTH";
	public static final String STR_NULL = "null";
	public static final String ISSUER_COMMISSION_ENDPOINT = "/issuercommissions";
	
	public static final String DATA_FOUND = "DataFound";
	public static final String ASSOCIATED_WITH_DIFF_ROLE = "AssociatedWithDiffRole";
	public static final String NO_DATA_FOUND = "NoDataFound";
	public static final String DEFAULT_MEDICARE_PLAN_DATA_SOURCE = "DRX";
	
	public static final String DATABASE_TYPE_CONFIG = "#{configProp['database.type']}";
	public static final String POSTGRES = "POSTGRESQL"; 
	public static final String NONE = "NONE";
	public static final String ANY = "ANY";
	public static final String BRAND_NAME_ID = "brandNameId";
	public static final String NO_D2C = "noD2c";
	public static final String NO_D2C_ENABLED = "No D2C Enabled";
	public static final String D2C_FLOW_TYPE_ID = "d2cFlowTypeId";
	public static final String TENANT_CODE = "tenantCode";
	public static final String D2C_FLOW_YEAR = "d2cFlowYear";
	public static final String ASC = "ASC";
	public static final String DESC = "DESC";
	public static final String DEPENDENT = "dependent";
	public static final String STATE_CODE_ID = "ID";
	public static final String STATE_CODE_WA = "WA";
	public static final String STATE_CODE_MN = "MN";
	
	public static final String UPDATED_BY_COL = "lastUpdatedBy";
	public static final String UPDATED_DATE_COL = "lastUpdateTimestamp";
	public static final String STATUS_COL = "status";
	public static final String STATUS_FILE_COL = "supportFile";
	public static final String COMMENT_COL = "commentId";
	public static final String DATE_STRING = "Date";
	public static final String FILE_STRING = "FILE";
	public static final String USER_STRING = "User";
	public static final String UPPER_STRING = "UPPER";
	public static final String PLAN_SBC_FILE = "Plan SBC";

	public static final int ENROLLMENT_AVAIL_INT = 1;
	public static final int UPDATE_DATE_INT = 2;
	public static final int EFFECTIVE_DATE_INT = 3;
	public static final int USER_NAME_INT = 4;
	public static final int COMMENT_INT = 5;
	public static final int ISSUERPLANNUMBER_INT = 6;
	public static final int STATUS_INT = 7;
	public static final int STATUS_FILE_INT = 8;
	public static final int FN_ENROLLMENT_AVAIL_VAL_INT = 9;
	public static final int FN_STATUS_VAL_INT = 10;
	public static final int FN_DISPLAYVAL_INT = 11;
	public static final int PROVIDER_NETWORK_NAME_INT = 12;
	public static final int PROVIDER_NETWORK_TYPE_INT = 13;
	public static final int PROVIDER_NETWORK_ID_INT = 14;
	public static final int FN_BROCHURE_VAL_INT = 15;
	public static final int START_DATE_INT = 16;
	public static final int FN_EFFECTIVE_START_DATE_VAL_INT= 17;
	public static final int BROCHURE_UCM_ID_VAL_INT = 18;
	public static final int ISSUER_VERIFICATION_STATUS_VAL_INT = 19;
	
	public static final int RATEFILE_INT = 1;
	public static final int RATE_EFFECTIVE_DATE_INT = 3;
	public static final int BENEFITFILE_INT = 6;
	public static final int FN_RATEFILE_VAL_INT = 7;
	public static final int FN_BENEFITFILE_VAL_INT = 8;
	public static final int FN_DISPLAYVAL_RATEHISTORY_INT = 9;
	public static final int BENEFIT_EFFECTIVE_DATE_INT = 10;
	public static final int EFFECTIVE_DATE_COMBINED_INT = 11;
	public static final int PLAN_SBC_VAL_INT = 12;
	
	public static final String DISPLAYVAL_COL = "displayVal";
	public static final String DISPLAYFIELD_COL = "displayField";

	public static final String PLAN_REPOSITORY_NAME = "iPlanRepository";
	public static final String PLAN_MODEL_NAME = "com.getinsured.hix.model.Plan";
	public static final String PLAN_HEALTH_REPOSITORY_NAME = "iPlanHealthRepository";
	public static final String PLAN_HEALTH_MODEL_NAME = "com.getinsured.hix.model.PlanHealth";
	public static final String PLAN_DENTAL_REPOSITORY_NAME = "iPlanDentalRepository";
	public static final String PLAN_DENTAL_MODEL_NAME = "com.getinsured.hix.model.PlanDental";
	
	public static final String EFFECTIVE_DATE_COL = "enrollmentAvailEffDate";
	public static final String ENROLLMENT_AVAIL_COL = "enrollmentAvail";
	public static final String ISSUER_VERIFICATION_STATUS_COL = "issuerVerificationStatus";
	/*public static final String REPOSITORY_NAME = "iPlanRepository";
	public static final String MODEL_NAME = "com.getinsured.hix.model.Plan";*/
	public static final String START_DATE_COL = "startDate";
	public static final String BENEFITFILE_COL = "benefitFile";
	public static final String PLAN_SBC_COL = "sbcUcmId";
	public static final String RATEFILE_COL = "rateFile";
	public static final String RATE_EFFECTIVE_DATE_COL = "rateEffectiveDate";
	public static final String UPDATE_DATE_COL = "lastUpdateTimestamp";
	public static final String USER_NAME_ALIAS = "lastUpdatedBy";
	public static final String EFF_DATE = "EFF DATE";
	public static final String USER_NAME_COL = "userName";
	public static final String COMMENT_TEXT_PRE = "<a onclick=\"getComment('";
	public static final String COMMENT_TEXT_POST = "');\" href='#modalBox' data-toggle='modal'>Comment</a>";
	public static final String TIMESTAMP_PATTERN1 = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String TIMESTAMP_PATTERN2 = "yyyy-MM-dd";
	public static final String TIMESTAMP_PATTERN3 = "MMM dd, yyyy";
	public static final String DOWNLOAD_FILE_PRETEXT1 = "<a href='";
	public static final String DOWNLOAD_FILE_PRETEXT2 = "download/document?documentId=";
	public static final String DOWNLOAD_FILE_PRETEXT3 = "'>Download File</a>";
	public static final String DOWNLOAD_FILE_POSTTEXT_CERTI = "&docType=certSuppDoc'>Download File</a>";
	public static final String DOWNLOAD_FILE_POSTTEXT_BROCHURE = "' target='_blank'>Download File</a>";
	
	public static final String DOWNLOAD_FILE_POSTTEXT_BENEFITS = "'>Download File</a>";
	public static final String CREATE_AND_DOWNLOAD_BENEFIT_FILE1 = "<a href='/hix/admin/planmgmt/dowloadbenefits/";
	public static final String CREATE_AND_DOWNLOAD_BENEFIT_FILE2 =	"'>Download File</a>";
	public static final String CREATE_AND_DOWNLOAD_BENEFIT_FILE3 = "<a href='/hix/issuer/planmgmt/dowloadbenefits/";
	public static final String CREATE_AND_DOWNLOAD_RATE_FILE1 = "<a href='/hix/issuer/qhp/dowloadqhprates/";
	public static final String CREATE_AND_DOWNLOAD_RATE_FILE2 =	"'>Download File</a>";
	public static final String BROCHURE_COL = "brochure";
	public static final String BROCHURE_UCM_ID_CAL = "brochureUCmId";
	public static final String DE_CERTIFIED = "De-certified";
	public static final String NON_CERTIFIED = "Non-certified";
	public static final String INCOMPLETE = "Incomplete";
	public static final String RE_CERTIFIED = "Re-certified";
	public static final String PENDING = "Pending";
	public static final String REGISTERED = "Registered";
	public static final String LOADED = "Loaded";
	public static final String AVAILABLE = "Available";
	public static final String NOT_AVAILABLE = "Not Available";
	public static final String DEPENDENTS_ONLY = "Dependents Only";
	public static final String PLAN_STATUS = "Plan Status";
	public static final String ENROLLMENT_AVAILABILITY_COL = "Enrollment Availability";
	public static final String PLAN_BROUCHURE = "Plan Brochure";
	public static final String EFFECTIVE_DATE_COLUMN = "Effective Date";
	public static final String CERTIFIED = "Certified";
	public static final String BENEFIT_EFFECTIVE_DATE_COL = "benefitEffectiveDate";
	public static final String EFFECTIVE_DATE_COMB_COL = "effectiveDateCombined";
	public static final String ADMIN_ROLE = "ADMIN";
	public static final String ISSUER_ROLE = "ISSUER";
	public static final String PLAN_BENEFITS = "Plan Benefits";
	public static final String PLAN_RATES = "Plan Rates";
	public static final String PLAN_SBC = "Plan SBC";
	
	
	public static final String BENEFIT_FILE_CAPTION = "Benefit File";
	
	public static final String N_A = "N/A";
	public static final String ELEMENT_DISP_COST_DATA = "display_cost_data.";
	public static final String ELEMENT_DISP_BENEFIT_DATA = "display_benefit_data.";
	public static final String PIPE_SIGN = "|";
	public static final String COST_SHARING_TYPE_IND = "IND";
	public static final String COST_SHARING_TYPE_FLY = "FLY";
	
	public static final String CSR_VARIATION_01 = "01";
	public static final String YES_CAPS = "YES";
	public static final String NO_CAPS = "NO";
	public static final Long COMPANY_LOGO_FILE_SIZE = 512000L;
	public static final String CDN_ISSUER_LOGO_PATH = "issuers/logo/";
	
}
