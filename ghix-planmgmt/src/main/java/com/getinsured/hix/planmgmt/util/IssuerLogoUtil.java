package com.getinsured.hix.planmgmt.util;

import java.net.URL;
import java.util.Arrays;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.planmgmt.microservice.UploadFileDTO;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.ecm.MimeConfig;
import com.getinsured.hix.platform.external.cloud.service.ExternalCloudService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

@Component
@DependsOn("dynamicPropertiesUtil")
public class IssuerLogoUtil {

	private static final Logger LOGGER = Logger.getLogger(PlanMgmtUtil.class);
	private static String CDN_URL = null;
	
	@Autowired 
	private ExternalCloudService externalCloudService;
	
	
	/**
	 * Method is used upload Company LOGO at CDN server and set LOGO Contents(BLOG) and CDN URL in Issuer table.
	 */
	public void uploadLogoAndSetDetails(UploadFileDTO uploadFileDTO, Issuer issuerObj) {

		LOGGER.info("Execution of uploadLogoAndSetDetails() start");
		boolean failedToSetLogo = false;
		Long companyLogoSize = 0L;

		try {
			if (null == uploadFileDTO 
					|| null == uploadFileDTO.getFileSize()
					|| null == uploadFileDTO.getFileContent()
					|| null == uploadFileDTO.getOriginalFileName()) {
				LOGGER.info("Uploaded file attributes are missing");
				return;
			}

			if (null == issuerObj) {
				LOGGER.error("Issuer Object is empty.");
				return;
			}
			
			final String COMPANY_LOGO_MIME_TYPE = "image/gif,image/jpeg,image/png";
			String contentType = uploadFileDTO.getContentType().toLowerCase();
			String actualMimetype = MimeConfig.getMimeConfig().validateIncomingContent(uploadFileDTO.getFileContent(), uploadFileDTO.getOriginalFileName());
			
			//If actual Mime type and the file extension does not match OR actual mime type is not in allowed mime type list
			if(!contentType.equals(actualMimetype) || !Arrays.asList(COMPANY_LOGO_MIME_TYPE.split(",")).contains(actualMimetype)) {
				String errorMsg = "Invalid Company Logo file type: "+actualMimetype+" ,Allowed logo file types : " + COMPANY_LOGO_MIME_TYPE;
				LOGGER.error(errorMsg);
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.SAVE_COMPANY_PROFILE_EXCEPTION.getCode(), null, errorMsg, Severity.HIGH);
			}
			
			String companyLogoSizeStr = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.COMPANYLOGOFILESIZE);
			if (StringUtils.isNotBlank(companyLogoSizeStr)) {
				companyLogoSize = Long.parseLong(companyLogoSizeStr);
			}
			else {
				companyLogoSize = PlanMgmtConstants.COMPANY_LOGO_FILE_SIZE;
			}

			if (uploadFileDTO.getFileSize() < companyLogoSize) {

				// get CDN url from app_config table
				CDN_URL = DynamicPropertiesUtil.getPropertyValue("global.CdnUrl");

				// if CDN is enabled then push issuer logo to CDN
				if (StringUtils.isNotBlank(CDN_URL)) {
					URL docUrl = externalCloudService.upload(PlanMgmtConstants.CDN_ISSUER_LOGO_PATH, issuerObj.getId() + "_"+ issuerObj.getHiosIssuerId() + uploadFileDTO.getOriginalFileName(), uploadFileDTO.getFileContent());
					
					// set CDN url in issuer.LOGO_URL column
					if (null != docUrl) {
						issuerObj.setLogoURL(docUrl.toString());

						if (LOGGER.isDebugEnabled()) {
							LOGGER.debug("CDN Logo URL: " + docUrl.toString());
						}
					}
					else {
						LOGGER.warn("Failed to upload Company Logo to CDN.");
					}
				}
				else {
					LOGGER.debug("Skipping to upload logo to CDN as CDN is not enable.");
				}
				
				//set file content in issuer.LOGO column
				issuerObj.setLogo(uploadFileDTO.getFileContent());
			}
			else {
				failedToSetLogo = true;
			}
		}
		catch (Exception ex) {
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.SAVE_COMPANY_PROFILE_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH);
		}
		finally {

			if (failedToSetLogo) {
				String errorMessage = "Company logo size("+ uploadFileDTO.getFileSize() +") is exceeding maximum size " + companyLogoSize;
				LOGGER.error(errorMessage);
				throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.EXCEEDING_LOGO_SIZE_EXCEPTION.getCode(), null, errorMessage, Severity.HIGH);
			}
			
			LOGGER.info("Execution of uploadLogoAndSetDetails() end");
		}
		
	}

}
