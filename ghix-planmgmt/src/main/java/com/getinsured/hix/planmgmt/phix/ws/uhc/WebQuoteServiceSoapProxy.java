package com.getinsured.hix.planmgmt.phix.ws.uhc;

public class WebQuoteServiceSoapProxy implements com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteServiceSoap {
  private String _endpoint = null;
  private com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteServiceSoap webQuoteServiceSoap = null;
  
  public WebQuoteServiceSoapProxy() {
    _initWebQuoteServiceSoapProxy();
  }
  
  public WebQuoteServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initWebQuoteServiceSoapProxy();
  }
  
  private void _initWebQuoteServiceSoapProxy() {
    try {
      webQuoteServiceSoap = (new com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteServiceLocator()).getWebQuoteServiceSoap();
      if (webQuoteServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)webQuoteServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)webQuoteServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (webQuoteServiceSoap != null)
      ((javax.xml.rpc.Stub)webQuoteServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteServiceSoap getWebQuoteServiceSoap() {
    if (webQuoteServiceSoap == null)
      _initWebQuoteServiceSoapProxy();
    return webQuoteServiceSoap;
  }
  
  public com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteReturn getQuote(com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteRequest value) throws java.rmi.RemoteException{
    if (webQuoteServiceSoap == null)
      _initWebQuoteServiceSoapProxy();
    return webQuoteServiceSoap.getQuote(value);
  }
  
  
}