//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.14 at 11:56:49 AM PST 
//


package com.getinsured.hix.planmgmt.phix.ws.quotit;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomProduct.Plan complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomProduct.Plan">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PlanId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PlanName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BusinessRules" type="{}CustomProduct.Plan.BusinessRule" minOccurs="0"/>
 *         &lt;element name="AreaLookupStrategy" type="{}ArrayOfCustomProduct.Plan.LookupSequence" minOccurs="0"/>
 *         &lt;element name="RawRates" type="{}ArrayOfCustomProduct.Plan.RawRate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomProduct.Plan", propOrder = {
    "planId",
    "planName",
    "businessRules",
    "areaLookupStrategy",
    "rawRates"
})
public class CustomProductPlan {

    @XmlElement(name = "PlanId", required = true, nillable = true)
    protected String planId;
    @XmlElement(name = "PlanName", required = true, nillable = true)
    protected String planName;
    @XmlElementRef(name = "BusinessRules", type = JAXBElement.class, required = false)
    protected JAXBElement<CustomProductPlanBusinessRule> businessRules;
    @XmlElementRef(name = "AreaLookupStrategy", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomProductPlanLookupSequence> areaLookupStrategy;
    @XmlElementRef(name = "RawRates", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfCustomProductPlanRawRate> rawRates;

    /**
     * Gets the value of the planId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanId() {
        return planId;
    }

    /**
     * Sets the value of the planId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanId(String value) {
        this.planId = value;
    }

    /**
     * Gets the value of the planName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanName() {
        return planName;
    }

    /**
     * Sets the value of the planName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanName(String value) {
        this.planName = value;
    }

    /**
     * Gets the value of the businessRules property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link CustomProductPlanBusinessRule }{@code >}
     *     
     */
    public JAXBElement<CustomProductPlanBusinessRule> getBusinessRules() {
        return businessRules;
    }

    /**
     * Sets the value of the businessRules property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link CustomProductPlanBusinessRule }{@code >}
     *     
     */
    public void setBusinessRules(JAXBElement<CustomProductPlanBusinessRule> value) {
        this.businessRules = value;
    }

    /**
     * Gets the value of the areaLookupStrategy property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanLookupSequence }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomProductPlanLookupSequence> getAreaLookupStrategy() {
        return areaLookupStrategy;
    }

    /**
     * Sets the value of the areaLookupStrategy property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanLookupSequence }{@code >}
     *     
     */
    public void setAreaLookupStrategy(JAXBElement<ArrayOfCustomProductPlanLookupSequence> value) {
        this.areaLookupStrategy = value;
    }

    /**
     * Gets the value of the rawRates property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanRawRate }{@code >}
     *     
     */
    public JAXBElement<ArrayOfCustomProductPlanRawRate> getRawRates() {
        return rawRates;
    }

    /**
     * Sets the value of the rawRates property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfCustomProductPlanRawRate }{@code >}
     *     
     */
    public void setRawRates(JAXBElement<ArrayOfCustomProductPlanRawRate> value) {
        this.rawRates = value;
    }

}
