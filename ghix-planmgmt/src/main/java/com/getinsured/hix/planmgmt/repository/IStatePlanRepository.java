package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.StatePlan;

public interface IStatePlanRepository extends JpaRepository<StatePlan, Integer>, RevisionRepository<StatePlan, Integer, Integer> {

	@Query("SELECT statePlan.zipCode  FROM StatePlan statePlan WHERE (LOWER(statePlan.state) LIKE '%' || lower(:state))")
	int getZipCodeForState(@Param("state") String state);
	
	@Query("SELECT statePlan FROM StatePlan statePlan WHERE (LOWER(statePlan.state) LIKE '%' || lower(:state))")
	StatePlan getStatePlanForState(@Param("state") String state);
	
	@Query("SELECT statePlan FROM StatePlan statePlan order by state")
	List<StatePlan> getStatePlan();

}
