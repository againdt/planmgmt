package com.getinsured.hix.planmgmt.service;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.SbcScenarioDTO;
import com.getinsured.hix.model.PlanSbcScenario;
import com.getinsured.hix.planmgmt.repository.ISbcSecnarioRepository;


@Service("planSbcScenarioService")
@Repository
@Transactional
public class PlanSbcScenarioServiceImpl implements PlanSbcScenarioService {
	
	@Autowired
	ISbcSecnarioRepository iSbcSecnarioRepository;
	
	@Override
	public SbcScenarioDTO setSbcScenario(PlanSbcScenario planSbcScenario, SbcScenarioDTO sbcScenarioDTO){
		 sbcScenarioDTO = new SbcScenarioDTO();
		
		// set baby attributes start
		 if(planSbcScenario.getBabyCoinsurance() != null)
			 sbcScenarioDTO.setBabyCoinsurance(planSbcScenario.getBabyCoinsurance());
		 
		 if(planSbcScenario.getBabyCopayment() != null)
			 sbcScenarioDTO.setBabyCopay(planSbcScenario.getBabyCopayment());
		 
		 if(planSbcScenario.getBabyDeductible() != null)
			 sbcScenarioDTO.setBabyDeductible(planSbcScenario.getBabyDeductible());
		 
		 if(planSbcScenario.getBabyLimit() != null)
			 sbcScenarioDTO.setBabyLimit(planSbcScenario.getBabyLimit());
		// set baby attributes end
		 
		// set Diabetes attributes start 
		 if(planSbcScenario.getDiabetesCoinsurance() != null)
			 sbcScenarioDTO.setDiabetesCoinsurance(planSbcScenario.getDiabetesCoinsurance());
		 
		 if(planSbcScenario.getDiabetesCopay() != null)
			 sbcScenarioDTO.setDiabetesCopay(planSbcScenario.getDiabetesCopay());
		 
		 if(planSbcScenario.getDiabetesDeductible() != null)
			 sbcScenarioDTO.setDiabetesDeductible(planSbcScenario.getDiabetesDeductible());
		 
		 if(planSbcScenario.getDiabetesLimit() != null)
			 sbcScenarioDTO.setDiabetesLimit(planSbcScenario.getDiabetesLimit());
		 // set Diabetes attributes end 
		 
		 // set Fracture attributes start 
		 if(planSbcScenario.getFractureCoinsurance() != null)
			 sbcScenarioDTO.setFractureCoinsurance(planSbcScenario.getFractureCoinsurance());
		 
		 if(planSbcScenario.getFractureCopay() != null)
			 sbcScenarioDTO.setFractureCopay(planSbcScenario.getFractureCopay());
		 
		 if(planSbcScenario.getFractureDeductible() != null)
			 sbcScenarioDTO.setFractureDeductible(planSbcScenario.getFractureDeductible());
		 
		 if(planSbcScenario.getFractureLimit() != null)
			 sbcScenarioDTO.setFractureLimit(planSbcScenario.getFractureLimit());
		 // set Fracture attributes end 
		 
		 return sbcScenarioDTO;
	}
	
	
	@Override
	public List<PlanSbcScenario> getSbcScenarioIdList(List<Integer> sbcScenarioIdList){
		 List<PlanSbcScenario> planSbcScenarioList = new ArrayList<PlanSbcScenario>();
		 planSbcScenarioList = iSbcSecnarioRepository.getSbcSecnarioByIdList(sbcScenarioIdList);
		 
		 return planSbcScenarioList;
	}
	
}
