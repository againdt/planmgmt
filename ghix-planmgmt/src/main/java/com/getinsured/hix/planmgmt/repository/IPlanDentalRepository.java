package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.PlanDental;

@Repository("iPlanDentalRepository")
public interface IPlanDentalRepository extends JpaRepository<PlanDental, Integer>, RevisionRepository<PlanDental, Integer, Integer> {
	@Query("SELECT planDental.id,planDental.plan.id FROM PlanDental planDental WHERE LOWER(planDental.parentPlanId) = LOWER(:parentPlanId) and planDental.costSharing = :costSharing ")
    Object[] getPlanDentalIdByParentId(@Param("parentPlanId") String parentPlanId,@Param("costSharing") String costSharing);
	
	@Query("SELECT planDental.acturialValueCertificate, planDental.benefitFile, planDental.costSharing FROM PlanDental planDental WHERE LOWER(planDental.parentPlanId) = LOWER(:parentPlanId)")
    List<Object[]> getPlanDentalDetails(@Param("parentPlanId") String parentPlanId);
    
    @Query("SELECT planDental.plan.id FROM PlanDental planDental WHERE LOWER(planDental.parentPlanId) = LOWER(:parentPlanId) and planDental.costSharing = :costSharing ")
    List<Integer> getCostSharingPlanIds(@Param("parentPlanId") String parentPlanId,@Param("costSharing") String costSharing);

    @Query("SELECT planDental.plan.id FROM PlanDental planDental WHERE planDental.parentPlanId = :parentPlanId ")
    List<Integer> getChildPlanIds(@Param("parentPlanId") Integer parentPlanId);
    
    @Query("SELECT planDental.id FROM PlanDental planDental WHERE planDental.plan.id = :planId ")
    Integer getPlanDentalId(@Param("planId") Integer planId);
}