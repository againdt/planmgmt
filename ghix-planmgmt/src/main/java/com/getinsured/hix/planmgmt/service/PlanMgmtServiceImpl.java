package com.getinsured.hix.planmgmt.service;

import static com.getinsured.hix.dto.planmgmt.PlanDocumentRequestDTO.POS_APPLICABLE_YEAR;
import static com.getinsured.hix.dto.planmgmt.PlanDocumentRequestDTO.POS_BROCHURE_ECM;
import static com.getinsured.hix.dto.planmgmt.PlanDocumentRequestDTO.POS_BROCHURE_FILE;
import static com.getinsured.hix.dto.planmgmt.PlanDocumentRequestDTO.POS_PLAN_ISSUER_NUM;
import static com.getinsured.hix.dto.planmgmt.PlanDocumentRequestDTO.POS_SBC_ECM;
import static com.getinsured.hix.dto.planmgmt.PlanDocumentRequestDTO.POS_SBC_FILE;

import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Comparator;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.planmgmt.FormularyDrugItemDTO;
import com.getinsured.hix.dto.planmgmt.FormularyDrugListResponseDTO;
import com.getinsured.hix.dto.planmgmt.FormularyPlanDTO;
import com.getinsured.hix.dto.planmgmt.ListOfStringArrayResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanBulkUpdateRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanComponentHistoryDTO;
import com.getinsured.hix.dto.planmgmt.PlanCrossWalkResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanDocumentRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanDocumentResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanLightDTO;
import com.getinsured.hix.dto.planmgmt.PlanListResponse;
import com.getinsured.hix.dto.planmgmt.PlanRateDTO;
import com.getinsured.hix.dto.planmgmt.PlanRateListResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.PlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanResponse;
import com.getinsured.hix.dto.planmgmt.PlanResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.PlanSearchResponseDTO;
import com.getinsured.hix.dto.planmgmt.PlanTenantDTO;
import com.getinsured.hix.dto.planmgmt.PlanTenantRequestDTO;
import com.getinsured.hix.dto.planmgmt.SbcScenarioDTO;
import com.getinsured.hix.dto.planmgmt.ServiceAreaDetailsDTO;
import com.getinsured.hix.dto.planmgmt.ZipCounty;
import com.getinsured.hix.dto.shop.EmployeeCoverageDTO;
import com.getinsured.hix.dto.shop.employer.eps.PlanDTO;
import com.getinsured.hix.model.BenchmarkPremiumReq;
import com.getinsured.hix.model.Formulary;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerServiceArea;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.model.PlanBenefitEdit;
import com.getinsured.hix.model.PlanCostEdit;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanDentalCost;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.PlanHealthCost;
import com.getinsured.hix.model.PlanSbcScenario;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.TenantPlan;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.mapper.PlanHealthMapper;
import com.getinsured.hix.planmgmt.querybuilder.PlanMgmtQueryBuilder;
import com.getinsured.hix.planmgmt.repository.IBenchmarkPremiumReqRepository;
import com.getinsured.hix.planmgmt.repository.IFormularyRepository;
import com.getinsured.hix.planmgmt.repository.IIssuerServiceAreaRepository;
import com.getinsured.hix.planmgmt.repository.IPlanBenefitEditRepository;
import com.getinsured.hix.planmgmt.repository.IPlanCostEditRepository;
import com.getinsured.hix.planmgmt.repository.IPlanDentalBenefitRepository;
import com.getinsured.hix.planmgmt.repository.IPlanDentalCostRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthBenefitRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthCostRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthRepository;
import com.getinsured.hix.planmgmt.repository.IPlanRepository;
import com.getinsured.hix.planmgmt.repository.IPlanSTMCostRepository;
import com.getinsured.hix.planmgmt.repository.IRegionRepository;
import com.getinsured.hix.planmgmt.repository.PMIssuerRepository;
import com.getinsured.hix.planmgmt.service.PlanRateBenefitServiceImpl.CostNames;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.planmgmt.util.PlanMgmtParamConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.repository.ITenantPlanRepository;
import com.getinsured.hix.pmms.repository.ITenantRepository;

@Service("planmgmtService")
@Repository
@Transactional
@DependsOn("dynamicPropertiesUtil")
public class PlanMgmtServiceImpl implements PlanMgmtService {
	
	@Value("#{configProp['appUrl']}")
	private String appUrl;

	@Value(PlanMgmtConstants.CONFIG_DB_TYPE)
	private String configuredDB;
	
	private static final Logger LOGGER = Logger.getLogger(PlanMgmtServiceImpl.class);
	private static final String ZIPCODE = "zipCode";
	private static final String COUNTYCODE = "countyCode";
	private static final SimpleDateFormat DATE_FORMAT_FOR_EFFECTIVE_DATE = new SimpleDateFormat("yyyy-MM-dd");
	private static final String PLANID = "planId";
	
	
	@Autowired
	private IRegionRepository iRegionRepository;
	@Autowired
	private IBenchmarkPremiumReqRepository iBenchmarkPremiumReqRepository;
	@Autowired
	private IPlanRepository iPlanRepository;	
	@PersistenceUnit private EntityManagerFactory emf;
	@Autowired 
	private IPlanSTMCostRepository iPlanSTMCostRepository;
	@Autowired
	private STMPlanRateBenefitService stmPlanRateBenefitService;
	@Autowired
	private QuotingBusinessLogic quotingBusinessLogic; 
	@Autowired
	private PlanCostAndBenefitsService planCostAndBenefitsService;
	@Autowired 
	private ProviderService providerService;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired 
	private IPlanBenefitEditRepository iPlanBenefitEditRepository;	
	@Autowired 
	private IPlanCostEditRepository iPlanCostEditRepository;	
	@Autowired 
	private IPlanHealthBenefitRepository iPlanHealthBenefitRepository;
	@Autowired 
	private IPlanHealthCostRepository iPlanHealthCostRepository;
	@Autowired 
	private IPlanDentalBenefitRepository iPlanDentalBenefitRepository;
	@Autowired 
	private IPlanDentalCostRepository iPlanDentalCostRepository;
	@Autowired 
	private IFormularyRepository iFormularyRepository;
	@Autowired 
	private IIssuerServiceAreaRepository pmIssuerServiceAreaRepository;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	@Autowired
	private SyncPlanWithAHBXService syncPlanWithAHBXService;
	@Autowired
	private ZipCodeService zipCodeService;
	@Autowired 
	private ITenantRepository pmmsITenantRepository;
	@Autowired 
	private ITenantPlanRepository pmmsITenantPlanRepository;
	@Autowired
	private IPlanHealthRepository iPlanHealthRepository;	
	@Autowired
	private PlanSbcScenarioService planSbcScenarioService;
	@Autowired
	private PMIssuerRepository  issuerRepository;

	
	@Override
	public long getZipCountForCounty(String county) {
		return iRegionRepository.getZipCountForCounty(county);
	}

	@Override
	public long getZipCountForCountyAndZip(String county, String zip) {
		return iRegionRepository.getZipCountForCountyAndZip(county, zip);
	}

	@Override
	public long getZipCountForZip(String zip) {
		return iRegionRepository.getZipCountForZip(zip);
	}

	@Override
	public long getCountForZipAndCountyCode(String zip, String countyCode) {
		return iRegionRepository.getCountForZipAndCountyCode(zip, countyCode);
	}

	@Override
	public void saveBenchmarkPremiumReq(BenchmarkPremiumReq benchmarkPremiumReq) {
		iBenchmarkPremiumReqRepository.save(benchmarkPremiumReq);
	}

	private String providerURLMapQuery;


	// ref plan selection method
	@Override
	@Transactional(readOnly = true)
	public List<PlanDTO> getReferenceHealthPlans (String empPrimaryZip, String ehbCovered, String insuranceType, String effectiveDate, String empCountyCode) {		
		List<PlanDTO> planDTOList = new ArrayList<PlanDTO>(); 
		EntityManager entityManager = null;
		
		try{
			HashMap<String, Object> paramList = new HashMap<String, Object>();
			paramList.put("param_effectiveDate_0", effectiveDate);
			paramList.put("param_effectiveDate_1", effectiveDate);
			paramList.put("param_effectiveDate_2", effectiveDate);
			paramList.put("param_effectiveDate_3", effectiveDate);
			paramList.put(PlanMgmtParamConstants.PARAM_COUNTY_CODE, empCountyCode);
			paramList.put(PlanMgmtParamConstants.PARAM_ZIP_CODE, empPrimaryZip);
			
			String buildQuery = null;
			entityManager = emf.createEntityManager();
			
			if (StringUtils.isEmpty(ehbCovered)) {
				buildQuery = PlanMgmtQueryBuilder.getReferenceHealthPlanWithoutEhbBuilderQuery(configuredDB);
			} else {
				paramList.put(PlanMgmtParamConstants.PARAM_EHB_COVERED, ehbCovered);
				buildQuery = PlanMgmtQueryBuilder.getReferenceHealthPlanWithEhbBuilderQuery(configuredDB);
			}
	
			Query query = entityManager.createQuery(buildQuery.toString());
			query = PlanMgmtUtil.setQueryParameters(query, paramList);
	
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL " + buildQuery.toString());
			}
			
			paramList = null;
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();		
			PlanDTO planDTO = null;
			Object[] objArray = null;
			
			while (rsIterator.hasNext()) {
				objArray = (Object[]) rsIterator.next();
				planDTO = new PlanDTO();	
				planDTO.setPlanId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
				planDTO.setTierName(objArray[PlanMgmtConstants.ONE].toString());				
				planDTO.setIssuerPlanNumber(objArray[PlanMgmtConstants.TWO].toString());
				planDTOList.add(planDTO);
			}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getReferenceHealthPlans ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return planDTOList;
	}

	@Override
	public Plan getPlanData(String planId) {
		Plan planData = null;

		if (org.apache.commons.lang3.StringUtils.isNumeric(planId)) {
			planData = iPlanRepository.findOne(Integer.parseInt(planId));
		}
		return planData;
	}

	@Override
	public Plan getPlanIssuerData(String planId) {
		Plan planData = null;
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("planId: " + SecurityUtil.sanitizeForLogging( planId));
		}

		if (StringUtils.isNotBlank(planId) && StringUtils.isNumeric(planId)) {
			planData = iPlanRepository.getPlanHealthIssuerData(Integer.parseInt(planId));

			if (null == planData) {
				planData = iPlanRepository.getPlanDentalIssuerData(Integer.parseInt(planId));
			}
		} else {
			LOGGER.error("Invalid Plan Id !!");
		}

		return planData;
	}
	

	@Override
	@Transactional(readOnly = true)
	public String getRegionName(Integer planId, String insuranceType) {
		StringBuilder regionNames = new StringBuilder(512);
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			String queryBuilder = PlanMgmtQueryBuilder.getRegionNameQueryBuilder(Plan.PlanInsuranceType.HEALTH);
			Query query = entityManager.createQuery(queryBuilder);
			query.setParameter(PLANID, planId);
	
			List<?> rsList = query.getResultList();
			if (rsList != null && (!(rsList.isEmpty()))) {
	
				Iterator<?> rsIterator = rsList.iterator();
				while (rsIterator.hasNext()) {
					regionNames.append(((String) rsIterator.next()));
					regionNames.append(",");
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getRegionName", ex);
		}
		finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		return regionNames.toString();
	}


	public String returnOneString(String sbcDocURL, String benefitURL) { // if sbc doc and benefit url both exists then first preference goes to sbc doc 
		String returnString = "";
		if (!(sbcDocURL.isEmpty()) && !(benefitURL.isEmpty())) {
			returnString = sbcDocURL;
		} else if ((sbcDocURL.isEmpty()) && !(benefitURL.isEmpty())) {
			returnString = benefitURL;
		} else if (!(sbcDocURL.isEmpty()) && (benefitURL.isEmpty())) {
			returnString = sbcDocURL;
		} else {
			returnString = "";
		}
		return returnString;
	}

	@Override
	@Transactional(readOnly = true)
	public boolean checkIndvHealthPlanAvailability(String zip, String countyFips, String effectiveDate, String exchangeType, String csrValue, String tenantCode) {
		String buildquery = null;
		boolean booleanResponse = false;
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			HashMap<String, Object> paramList = new HashMap<String, Object>();
			paramList.put(PlanMgmtParamConstants.PARAM_ZIP_CODE, zip);
			paramList.put(PlanMgmtParamConstants.PARAM_COUNTY_CODE, countyFips);
			paramList.put("param_effectiveDate_0", effectiveDate);
			paramList.put("param_effectiveDate_1", effectiveDate);
			paramList.put("param_effectiveDate_2", effectiveDate);
			paramList.put("param_effectiveDate_3", effectiveDate);
			//paramList.put(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE, exchangeType);
			paramList.put(PlanMgmtParamConstants.PARAM_TENANT_CODE, tenantCode.toUpperCase());
	
			buildquery = PlanMgmtQueryBuilder.getIndvHealthPlanAvailabilityQuery(csrValue, exchangeType, configuredDB);
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug(" SQL === " + buildquery.toString());
			}
			Query query = entityManager.createQuery(buildquery);
			query = PlanMgmtUtil.setQueryParameters(query, paramList);
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
			Long count = null;
			
			while (rsIterator.hasNext()) {
				count = (Long) rsIterator.next();
			}
	
			if(null != count){
			   if(count.intValue() > 0){
			     booleanResponse = true;
			   }
			}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in checkIndvHealthPlanAvailability", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}	
		return booleanResponse;
	}

	@Override
	@Transactional(readOnly = true)
	public List<?> getPlansByZipAndIssuerName(String zipCode, String countyCode, String insuranceType, String issuerName, String planLevel, String tenantCode) {
		List<String> planList = new ArrayList<String>();
		String queryStr = null;
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			// if insurance type is HEALTH, pull health plans
			if (insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())) {
				queryStr = PlanMgmtQueryBuilder.getPlansByZipAndIssuerName(countyCode, insuranceType, issuerName, planLevel, Plan.PlanInsuranceType.HEALTH, configuredDB);
			} else if (insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())) {
				queryStr = PlanMgmtQueryBuilder.getPlansByZipAndIssuerName(countyCode, insuranceType, issuerName, planLevel, Plan.PlanInsuranceType.DENTAL, configuredDB);
			}
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL == " + queryStr);
			}
	
			Query query = entityManager.createQuery(queryStr);
			query.setParameter(PlanMgmtParamConstants.PARAM_ZIP_CODE, zipCode);
			if (!StringUtils.isEmpty(countyCode)) {
				query.setParameter(PlanMgmtParamConstants.PARAM_COUNTY_CODE, countyCode);
			}
			if (!StringUtils.isEmpty(issuerName)) {
				query.setParameter(PlanMgmtParamConstants.PARAM_ISSUER_NAME, issuerName);
			}
			if (!StringUtils.isEmpty(planLevel)) {
				query.setParameter(PlanMgmtParamConstants.PARAM_PLAN_LEVEL, planLevel);
			}
			
			query.setParameter(PlanMgmtParamConstants.PARAM_TENANT_CODE, tenantCode.toUpperCase());
	
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
	
			while (rsIterator.hasNext()) {
				String planName = rsIterator.next().toString();
				planList.add(planName);
			}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getPlansByZipAndIssuerName", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		return planList;
	}
	
	
	private List<Plan> getListOfPlansSetForDecertification(EntityManager entityManager){
		List<Plan> planObjList = null;
		try{
			DateFormat formater = new SimpleDateFormat("MM-dd-YYYY");
			String todaysDate = formater.format(new TSDate());
			String buildQuery = PlanMgmtQueryBuilder.updatePlanStatusQuery();
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Batch job runs on "+ todaysDate + ". Build query :-" + buildQuery);
			}
			
			Query query = entityManager.createQuery(buildQuery);
			query.setParameter(PlanMgmtParamConstants.PARAM_TODAY_DATE, todaysDate);
			planObjList = query.getResultList();
		}	
		catch(Exception ex){
			LOGGER.error("Exception occured in getListOfPlansSetForDecertification: ", ex);
		}	
		
		return planObjList;
	}
	
	
	@Override
	@Transactional
	public String updatePlanStatus() {
		EntityManager entityManager = null;
		List<Map<String, String>> listOfPlans = new ArrayList<Map<String, String>>();
				
		try{
			entityManager = emf.createEntityManager();
			entityManager.getTransaction().begin();

			List<Plan> planObjList  = getListOfPlansSetForDecertification(entityManager); 
			
			if(planObjList!= null && (planObjList.size() > PlanMgmtConstants.ZERO) ){
				Plan planObj = null;
				String insuranceType = PlanMgmtConstants.EMPTY_STRING;
				String planlevel = PlanMgmtConstants.EMPTY_STRING;
				Map<String, String> requestData = null;
				
				for (int i = 0; i < planObjList.size(); i++) {
					planObj = planObjList.get(i);
					
					insuranceType = planObj.getInsuranceType();
					if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
						planlevel = planObj.getPlanHealth().getPlanLevel();
					}else if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
						planlevel = planObj.getPlanDental().getPlanLevel();
					}
					requestData = new HashMap<String, String>();
					requestData.put(PlanMgmtConstants.AHBX_PLAN_ID, String.valueOf(planObj.getId()));
					requestData.put(PlanMgmtConstants.AHBX_ISSUER_ID, planObj.getIssuerPlanNumber().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FIVE));
					requestData.put(PlanMgmtConstants.AHBX_PLAN_LEVEL, planlevel);
					requestData.put(PlanMgmtConstants.AHBX_PLAN_MARKET, planObj.getMarket());
					requestData.put(PlanMgmtConstants.AHBX_PLAN_NAME, planObj.getName());
					requestData.put(PlanMgmtConstants.AHBX_PLAN_NETWORK_TYPE, planObj.getNetworkType());
					requestData.put(PlanMgmtConstants.AHBX_PLAN_RECORD_INDICATOR, (planObj.getLastUpdateTimestamp().equals(planObj.getCreationTimestamp())) ? "N" : "U");
					requestData.put(PlanMgmtConstants.AHBX_PLAN_TYPE, insuranceType);
					requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_START_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(planObj.getStartDate()));
					requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_END_DATE, PlanMgmtConstants.AHBX_DATE_FORMAT.format(planObj.getEndDate()));
					requestData.put(PlanMgmtConstants.AHBX_PLAN_HIOS_ID, planObj.getIssuerPlanNumber());
					requestData.put(PlanMgmtConstants.AHBX_PLAN_STATUS, Plan.PlanStatus.DECERTIFIED.toString());
					requestData.put(PlanMgmtConstants.AHBX_PLAN_YEAR, Integer.toString(planObj.getApplicableYear()));
					listOfPlans.add(requestData);
					
					// set plan status as DECERTIFIED 
					planObj.setStatus(Plan.PlanStatus.DECERTIFIED.toString());
					planObj.setDeCertificationEffDate(null); // nullify de-certification date
					entityManager.merge(planObj);
					//iPlanRepository.save(planObj);
					
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Decertification of plan id " + planObj.getId() +" done successfully");
					}
					
					planObj = null;
				}
				
				if(entityManager !=null && entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().commit();
					LOGGER.debug("Transaction to decertify plans completed successfully");
				}
				
				// HIX-84059 : call syncPlanWithAHBX() to sync plan with AHBX
				syncPlanWithAHBXService.syncPlanWithAHBX(listOfPlans);
				
			}else{
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Plans not found to decertify.");
				}
			}
						
			return "success";
		}catch(Exception ex){
			LOGGER.error("Exception occured in updatePlanStatus: ", ex);
			return "failure";
		} finally {
			 // close the entity manager
			if(entityManager !=null && entityManager.isOpen()){
				if(entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}
				
	}
	
	
	private  List<Plan> getListOfPlansSetForEnrollmentAvailability(EntityManager entityManager){
		List<Plan> planObjList = null;
		
		try {
			DateFormat fmt = new SimpleDateFormat("MM-dd-YYYY");
			String todaysDate = fmt.format(new TSDate());
			String queryStr = PlanMgmtQueryBuilder.getUpdateEnrollmentAvailabilityQuery();

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Batch job runs on " + todaysDate + ". Build query :-" + queryStr);
			}
			
			Query query = entityManager.createQuery(queryStr);
			query.setParameter(PlanMgmtParamConstants.PARAM_TODAY_DATE, todaysDate);
			planObjList = query.getResultList();			
		} catch (Exception e) {
			LOGGER.error("Exception occured in getListOfPlansSetForEnrollmentAvailability: ", e);
		}
		
		return planObjList;
	}
		
	@Override
	@Transactional
	public String updateEnrollmentAvailability() {
		EntityManager entityManager = null;
		try {
			entityManager = emf.createEntityManager();
			entityManager.getTransaction().begin();
			List<Plan> planObjList = getListOfPlansSetForEnrollmentAvailability(entityManager);
			
			if(planObjList!= null && planObjList.size() > PlanMgmtConstants.ZERO ){
				Plan plan = null;
				
				for (int i = 0; i < planObjList.size(); i++) {
					plan = planObjList.get(i);
						
					if(plan != null && StringUtils.isNotBlank(plan.getFutureEnrollmentAvail())){
						plan.setEnrollmentAvailEffDate(plan.getFutureErlAvailEffDate());
						plan.setEnrollmentAvail(plan.getFutureEnrollmentAvail());
						plan.setFutureErlAvailEffDate(null);
						plan.setFutureEnrollmentAvail(null);
						entityManager.merge(plan);
						//iPlanRepository.save(plan);
						if(LOGGER.isDebugEnabled()) {
							LOGGER.debug("Set enrollment availability of plan id  " + plan.getId() +" successfully");
						}
					}
					plan = null;
				}
				if(entityManager !=null && entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().commit();
					LOGGER.debug("Transaction to update Enrollment Availability for plans completed successfully");
				}
			}else{
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Plans not found to set enrollment availability.");
				}
			}
			return "true";
		} catch (Exception e) {
			LOGGER.error("Exception occured in updateEnrollmentAvailability: ",e);
			return "false";
		} finally{
			// close the entity manager
			if (entityManager != null && entityManager.isOpen()) {
				if(entityManager.getTransaction().isActive()) {
					entityManager.getTransaction().rollback();
				}
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Float getCSRMultiplier(String inputCsrVariation, String planLevel, Integer applicableYear) {
		Float csrMultiplier = (float) 0.00;
		String csrVariation = inputCsrVariation;
		EntityManager entityManager = null;

		try{
			entityManager = emf.createEntityManager();
			if(StringUtils.isNotBlank(csrVariation)){
				csrVariation = csrVariation.replace(PlanMgmtConstants.CS, "");
			}else{
				csrVariation = PlanMgmtConstants.ONE_STR;
			}
			
			String buildquery = PlanMgmtQueryBuilder.getCSRMUltiplierQuery();
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("build query for multiplier:- " +buildquery);
			}
			
			Query query = entityManager.createNativeQuery(buildquery);
			query.setParameter(PlanMgmtParamConstants.PARAM_CSR_VARIATION, "0" + csrVariation);
			// if cost sharing variation CS1, then consider plan level as "Any tier"
			if (csrVariation.equals(PlanMgmtConstants.ONE_STR)) {
				query.setParameter(PlanMgmtParamConstants.PARAM_PLAN_LEVEL, PlanMgmtConstants.ANY_TIER.toUpperCase());
			} else {
				query.setParameter(PlanMgmtParamConstants.PARAM_PLAN_LEVEL, planLevel);
			}
			query.setParameter(PlanMgmtParamConstants.PARAM_PLAN_YEAR, applicableYear);

			Iterator<?> rsIterator = query.getResultList().iterator();
			while (rsIterator.hasNext()) {
				String multiplierField =  rsIterator.next().toString();
				if (multiplierField != null) {
					csrMultiplier = Float.parseFloat(multiplierField.toString());
				}
			}
			
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getCSRMultiplier", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return csrMultiplier;
	}

	@Override
	@Transactional(readOnly = true)
	public String getProviderURL(String providerNetworkId) {
		String providerUrl = PlanMgmtConstants.EMPTY_STRING;
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			StringBuilder buildquery  = new StringBuilder();
			buildquery.append("SELECT NETWORK_ID, NETWORK_URL, APPLICABLE_YEAR ");
			buildquery.append("FROM ");
			buildquery.append("NETWORK ");
			buildquery.append("WHERE ");
			buildquery.append("ID = CAST(? AS NUMERIC)");
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("***ProviderURL_Query:-***" +buildquery);
			}
			
			Query query = entityManager.createNativeQuery(buildquery.toString());
			query.setParameter(PlanMgmtConstants.ONE, providerNetworkId);
			Iterator<?> rsIterator = query.getResultList().iterator();
			
			while (rsIterator.hasNext()) {
				Object[] tempObject = (Object[])(rsIterator.next());
				if(tempObject[PlanMgmtConstants.ONE] != null ){
					providerUrl = tempObject[PlanMgmtConstants.ONE].toString();
				}
			}			
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getProviderURL", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return providerUrl;
	}
	
	@Override
	@Transactional
	public List<EmployeeCoverageDTO> checkPlanAvailabilityForEmployees(List<EmployeeCoverageDTO> employeeCoverageDTOList,String planYear,String effectiveDate){
		EntityManager entityManager = null;
		try{
			entityManager = emf.createEntityManager();
			String queryBuilder = PlanMgmtQueryBuilder.getPlanAvailabilityForEmployeeQuery(configuredDB);
			Query query = entityManager.createNativeQuery(queryBuilder.toString());
			
			for(EmployeeCoverageDTO employeeCoverageDTO : employeeCoverageDTOList){
				if(StringUtils.isNotBlank(employeeCoverageDTO.getHealthHiosPlanNumber())){
					employeeCoverageDTO.setHealthAvailability(PlanMgmtConstants.NOTAVAILABLE);//"not available");
				}
				if(StringUtils.isNotBlank(employeeCoverageDTO.getDentalHiosPlanNumber())){
					employeeCoverageDTO.setDentalAvailability(PlanMgmtConstants.NOTAVAILABLE);//"not available");
				}
								
				if(StringUtils.isNotEmpty(employeeCoverageDTO.getZip()) && StringUtils.isNotEmpty(employeeCoverageDTO.getCountyCode()) && StringUtils.isNotEmpty(effectiveDate.trim()) && StringUtils.isNotEmpty(planYear)){
					query.setParameter(PlanMgmtConstants.ONE, (StringUtils.isNotEmpty(employeeCoverageDTO.getHealthHiosPlanNumber()) ? employeeCoverageDTO.getHealthHiosPlanNumber().trim() : PlanMgmtConstants.EMPTY_STRING));
					query.setParameter(PlanMgmtConstants.TWO, (StringUtils.isNotEmpty(employeeCoverageDTO.getDentalHiosPlanNumber()) ? employeeCoverageDTO.getDentalHiosPlanNumber().trim() : PlanMgmtConstants.EMPTY_STRING));
					query.setParameter(PlanMgmtConstants.THREE, employeeCoverageDTO.getZip().trim());
					query.setParameter(PlanMgmtConstants.FOUR, employeeCoverageDTO.getCountyCode().trim());
					query.setParameter(PlanMgmtConstants.FIVE, Integer.parseInt(planYear));
					query.setParameter(PlanMgmtConstants.SIX, effectiveDate);
					query.setParameter(PlanMgmtConstants.SEVEN, effectiveDate);
					query.setParameter(PlanMgmtConstants.EIGHT, effectiveDate);
					query.setParameter(PlanMgmtConstants.NINE, effectiveDate);
					query.setParameter(PlanMgmtConstants.TEN, effectiveDate);
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("queryBuilder SQL = " + queryBuilder.toString());
					}
					
					Iterator<?> rsIterator = query.getResultList().iterator();
					while(rsIterator.hasNext()){
						Object[] tempObject = (Object[])(rsIterator.next());
						
						if(StringUtils.isNotBlank(employeeCoverageDTO.getHealthHiosPlanNumber()) && StringUtils.isNotBlank(tempObject[PlanMgmtConstants.ONE].toString()) && tempObject[PlanMgmtConstants.ONE].toString().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
							employeeCoverageDTO.setHealthAvailability(PlanMgmtConstants.AVAILABLE.toString());
						}
						if(StringUtils.isNotBlank(employeeCoverageDTO.getDentalHiosPlanNumber()) && StringUtils.isNotBlank(tempObject[PlanMgmtConstants.ONE].toString()) && tempObject[PlanMgmtConstants.ONE].toString().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
							employeeCoverageDTO.setDentalAvailability(PlanMgmtConstants.AVAILABLE.toString());
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured in checkPlanAvailabilityForEmployees", e);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		return employeeCoverageDTOList;
	}
	
		
	@Transactional(readOnly = true)
	public List<PlanResponse> getPlanDataBenefitAndcost(List<Integer> planIdsList, String effectiveDate, String insuranceType){
		List<Integer> healthOrDentalIdsList = new ArrayList<Integer>();
		List<Integer> providerNetworkIds = new ArrayList<Integer>();
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Plan Benefit Aand Cost Call for Plan Ids:"+planIdsList);
		}
		
		Map<Integer, Map<String, Map<String, String>>> benefitData = new HashMap<Integer, Map<String, Map<String, String>>>();
		Map<Integer, Map<String, Map<String, String>>> planCosts = new HashMap<Integer, Map<String, Map<String, String>>>();
		List<PlanResponse> planResponsesList = new ArrayList<PlanResponse>();
		EntityManager entityManager = null;
		Map<Integer, PlanResponse> planInfoResponseMap = new HashMap<Integer, PlanResponse>();
		
		try{
			entityManager = emf.createEntityManager();
			String buildquery = null;
			HashMap<String, Object> paramList = new HashMap<String, Object>();
			paramList.put("param_effectiveDate_0", effectiveDate);
			paramList.put("param_effectiveDate_1", effectiveDate);
			paramList.put("param_effectiveDate_2", effectiveDate);
			paramList.put("param_effectiveDate_3", effectiveDate);
			paramList.put("param_planIdInputList", planIdsList);

			if (insuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())) {
				buildquery = PlanMgmtQueryBuilder.getPlanBenefitQueryForHealthInsurance();

			} else if (insuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
				buildquery = PlanMgmtQueryBuilder.getPlanDataBenefitQueryForDentalInsurance();
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("***Plan Data Benefit And Cost****" + buildquery);
			}
			
			Query query = entityManager.createQuery(buildquery);
			query = PlanMgmtUtil.setQueryParameters(query, paramList);
			paramList = null;
			Iterator<?> rsIterator = query.getResultList().iterator();
			Object[] tempObject = null;
			while (rsIterator.hasNext()) {
				String logoURL = null;
				tempObject = (Object[]) (rsIterator.next());
				PlanResponse planResponse = new PlanResponse();
				if(tempObject[PlanMgmtConstants.ZERO] == null) {
					planResponse.setPlanId(PlanMgmtConstants.ZERO);
				}else{
					planResponse.setPlanId(Integer.parseInt(tempObject[PlanMgmtConstants.ZERO].toString()));
				}	
				if(tempObject[PlanMgmtConstants.THREE] == null){
					planResponse.setPlanName(PlanMgmtConstants.EMPTY_STRING);
				}else{
					planResponse.setPlanName(tempObject[PlanMgmtConstants.THREE].toString());
				}
				if(tempObject[PlanMgmtConstants.FOUR] == null){
					planResponse.setInsuranceType(PlanMgmtConstants.EMPTY_STRING);
				}else{
					planResponse.setInsuranceType(tempObject[PlanMgmtConstants.FOUR].toString());
				}
				if(tempObject[PlanMgmtConstants.FIVE] == null){
					planResponse.setNetworkType(PlanMgmtConstants.EMPTY_STRING);
				}else{
					planResponse.setNetworkType(tempObject[PlanMgmtConstants.FIVE].toString());
				}
				providerNetworkIds.add((Integer) tempObject[PlanMgmtConstants.SIX]);
				if(tempObject[PlanMgmtConstants.SEVEN] == null){
					planResponse.setIssuerPlanNumber(PlanMgmtConstants.EMPTY_STRING);
				}else{
					planResponse.setIssuerPlanNumber(tempObject[PlanMgmtConstants.SEVEN].toString());
				}
				if(tempObject[PlanMgmtConstants.NINE] == null){
					planResponse.setPlanMarket(PlanMgmtConstants.EMPTY_STRING);
				}else{
					planResponse.setPlanMarket(tempObject[PlanMgmtConstants.NINE].toString());
				}	
				if(tempObject[PlanMgmtConstants.EIGHT] == null){
					planResponse.setIssuerId(PlanMgmtConstants.ZERO);
				}else{
					planResponse.setIssuerId(Integer.parseInt(tempObject[PlanMgmtConstants.EIGHT].toString()));
				}	
				if(tempObject[PlanMgmtConstants.TEN] == null){
					planResponse.setIssuerName(PlanMgmtConstants.EMPTY_STRING);
				}else{
					planResponse.setIssuerName(tempObject[PlanMgmtConstants.TEN].toString());
				}
//				planResponse.setIssuerLogo((tempObject[PlanMgmtConstants.ELEVEN] != null) ? appUrl + DOCUMENT_URL
//						+ ghixJasyptEncrytorUtil.encryptStringByJasypt(tempObject[PlanMgmtConstants.ELEVEN].toString()) : "");
				if(null != tempObject[PlanMgmtConstants.ELEVEN]) {
					logoURL = tempObject[PlanMgmtConstants.ELEVEN].toString();
				}
				if(tempObject[PlanMgmtConstants.TWELVE] == null){
					planResponse.setHiosIssuerId(PlanMgmtConstants.EMPTY_STRING);
				}else{
					planResponse.setHiosIssuerId(tempObject[PlanMgmtConstants.TWELVE].toString());
				}
				planResponse.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, planResponse.getHiosIssuerId(), null, appUrl));
				if(tempObject[PlanMgmtConstants.THIRTEEN] == null){
					planResponse.setHsa( PlanMgmtConstants.EMPTY_STRING);
				}else{
					planResponse.setHsa(tempObject[PlanMgmtConstants.THIRTEEN].toString());
				}
				healthOrDentalIdsList.add((Integer) tempObject[PlanMgmtConstants.ONE]);
				planInfoResponseMap.put((Integer)tempObject[PlanMgmtConstants.ONE], planResponse);
			}
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Plan Infor Response received with "+planInfoResponseMap.size()+" responses");
				LOGGER.debug("Prepared Plan ID List"+healthOrDentalIdsList.size());
			}
			tempObject = null;
			if (healthOrDentalIdsList.size() > PlanMgmtConstants.ZERO) {
				if (insuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())) {
					benefitData = planCostAndBenefitsService.getOptimizedHealthPlanBenefits(healthOrDentalIdsList, effectiveDate);
					planCosts = planCostAndBenefitsService.getOptimizedHealthPlanCosts(healthOrDentalIdsList);
				} else if (insuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
					benefitData = planCostAndBenefitsService.getOptimizedDentalPlanBenefits(healthOrDentalIdsList, effectiveDate);
					planCosts = planCostAndBenefitsService.getOptimizedDentalPlanCosts(healthOrDentalIdsList, false);
				}
				Map<Integer, String> providerUrlMap = getProviderURLMap(providerNetworkIds, planIdsList);
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("providerUrlMap: " + providerUrlMap);
				}

				List<String> costList = getCostNames();
				for (Integer planHealthId : healthOrDentalIdsList) {
						PlanResponse planResponse = planInfoResponseMap.get(planHealthId);
						if(planResponse != null){
							planResponse.setPlanBenefits(benefitData.get(planHealthId));
							Map<String, Map<String, String>> costMap = planCosts.get(planHealthId);
							Set<String> costNameSet = costMap.keySet();
							Map<String, Map<String, String>> planCostMap = new HashMap<String, Map<String, String>>();
							Map<String, Map<String, String>> optionalPlanCostMap = new HashMap<String, Map<String, String>>();

							for (String costName : costNameSet) {
								if (costList.contains(costName)) {
									planCostMap.put(costName, costMap.get(costName));
								} else {
									optionalPlanCostMap.put(costName, costMap.get(costName));
								}
							}
							planResponse.setPlanCosts(planCostMap);
							planResponse.setOptionalDeductible(optionalPlanCostMap);
							planResponsesList.add(planResponse);
						}else{
							LOGGER.warn("No plan response received for plan Id:"+planHealthId);
						}
				}
				
				String providerUrl = null;
				for (PlanResponse planResponse : planResponsesList) {
					providerUrl = providerUrlMap.get(planResponse.getPlanId());
					if(providerUrl != null){
						planResponse.setProviderUrl(providerUrl);
					}
				}
			}
			
		}catch(Exception ex){
			LOGGER.error("Exception Occured in getPlanHealthIds: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return planResponsesList;
	}
	
	@Transactional(readOnly = true)
	private Map<Integer, String> getProviderURLMap(List<Integer> providerNetworkIds, List<Integer> planIdList) {
		Map<Integer, String> providerUrlMap = new HashMap<Integer, String>();
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Provider Network Id:"+providerNetworkIds);
			LOGGER.debug("Plan Ids:"+planIdList);
		}	
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			if (this.providerURLMapQuery == null) {
				this.providerURLMapQuery = "SELECT P.id, N.id, N.networkID, N.networkURL, N.applicableYear " + " FROM Network N, Plan P "
						+ "WHERE P.providerNetworkId = N.id " 
						+ "AND N.id IN (:param_providerNetIdInputList) "
						+ "AND P.id IN (:param_planIdList) ORDER BY P.id ASC";
			}
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("ProviderURL_Query:- " + providerURLMapQuery);
			}
			
			Query query = entityManager.createQuery(providerURLMapQuery);
			query.setParameter("param_providerNetIdInputList", providerNetworkIds);
			query.setParameter("param_planIdList", planIdList);
			Iterator<?> rsIterator = query.getResultList().iterator();
			Object[] tempObject = null;
			while (rsIterator.hasNext()) {
				tempObject = (Object[]) (rsIterator.next());
				providerUrlMap.put((Integer)tempObject[PlanMgmtConstants.ZERO],
						(tempObject[PlanMgmtConstants.THREE].toString() == null ? PlanMgmtConstants.EMPTY_STRING
								: tempObject[PlanMgmtConstants.THREE].toString()));
			}

		} catch (Exception ex) {
			LOGGER.error("Exceptin Occured in getCSRMultiplier", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		return providerUrlMap;
	}
	
	@Override
	public List<String> getCostNames(){
		List<String> deductibles = new ArrayList<String>();
		deductibles.add(CostNames.MAX_OOP_MEDICAL.toString());
		deductibles.add(CostNames.MAX_OOP_DRUG.toString());
		deductibles.add(CostNames.MAX_OOP_INTG_MED_DRUG.toString());
		deductibles.add(CostNames.DEDUCTIBLE_MEDICAL.toString());
		deductibles.add(CostNames.DEDUCTIBLE_DRUG.toString());
		deductibles.add(CostNames.DEDUCTIBLE_INTG_MED_DRUG.toString());
		deductibles.add(CostNames.DEDUCTIBLE_BRAND_NAME_DRUG.toString());
		return deductibles;
	}
	
	@Override
	public String getSeparateDrugDeductibleForPlanSTMCost(Integer planStmId){
		String SeparateDrugDeductible = null;
		try{
			SeparateDrugDeductible = iPlanSTMCostRepository.getSeparateDrugDeductibleForPlanSTMCost(planStmId);
		}catch(Exception ex){
			LOGGER.error("Exception in getSeparateDrugDeductibleForPlanSTMCost ", ex);
		}
		return SeparateDrugDeductible == null ? PlanMgmtConstants.EMPTY_STRING : SeparateDrugDeductible;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<PlanResponse> getPlanInfoMap(List<Integer> planIdsList){
		List<PlanResponse> planResponsesList = new ArrayList<PlanResponse>();		
		StringBuilder healthPlanIdBuilder = new StringBuilder();
		StringBuilder dentalPlanIdBuilder = new StringBuilder();
		StringBuilder stmPlanIdBuilder = new StringBuilder();		
		StringBuilder planIds = new StringBuilder();
		String considerEHBPortion = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CONSIDER_EHB_PORTION);
		EntityManager entityManager = null;
		
		try{			
			entityManager = emf.createEntityManager();
			StringBuilder buildquery  = new StringBuilder();			
			for(Integer planId : planIdsList ){
				planIds.append((planIds.length() == PlanMgmtConstants.ZERO ) ? PlanMgmtConstants.QUESTION_MARK : PlanMgmtConstants.COMA_QUESTION_MARK);
			}
			buildquery.append("SELECT P.ID, P.NAME, P.MARKET, P.INSURANCE_TYPE, P.NETWORK_TYPE, P.PROVIDER_NETWORK_ID, P.ISSUER_ID, I.NAME AS issuerName, I.PHONE_NUMBER, ");
			buildquery.append("I.INDV_SITE_URL, I.INDV_CUST_SERVICE_TOLL_FREE, I.SHOP_SITE_URL, I.SHOP_CUST_SERVICE_TOLL_FREE, I.FEDERAL_EIN, I.HIOS_ISSUER_ID, ");
			buildquery.append("I.PAYMENT_URL, I.APPLICATION_URL, P.EXCHANGE_TYPE, P.HSA, I.LOGO_URL, P.ehb_premium_fraction ");
			buildquery.append("FROM PLAN P, ISSUERS I WHERE P.ID IN (");			
			buildquery.append(planIds);
			buildquery.append(") AND P.ISSUER_ID = I.ID ORDER BY P.ID ASC");
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("PlanInfoMap:- " +buildquery);
			}
			
			Query query = entityManager.createNativeQuery(buildquery.toString());
			int itemIndex = 1;
    		for(Integer planId : planIdsList){
    			query.setParameter(itemIndex, planId);
    			itemIndex++;
    		}
			Iterator<?> rsIterator = query.getResultList().iterator();
			while (rsIterator.hasNext()) {
				String logoURL = null;
				Object[] tempObject = (Object[])(rsIterator.next());
				PlanResponse planResponse = new PlanResponse();
				planResponse.setPlanId(tempObject[PlanMgmtConstants.ZERO] == null ? PlanMgmtConstants.ZERO : Integer.parseInt(tempObject[PlanMgmtConstants.ZERO].toString()));
				planResponse.setPlanName(tempObject[PlanMgmtConstants.ONE] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.ONE].toString());
				planResponse.setInsuranceType(tempObject[PlanMgmtConstants.THREE] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.THREE].toString());
				planResponse.setNetworkType(tempObject[PlanMgmtConstants.FOUR] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.FOUR].toString());
				
				planResponse.setIssuerId(tempObject[PlanMgmtConstants.SIX] == null ? PlanMgmtConstants.ZERO : Integer.parseInt(tempObject[PlanMgmtConstants.SIX].toString()));
				planResponse.setIssuerName(tempObject[PlanMgmtConstants.SEVEN] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.SEVEN].toString());

				if(tempObject[PlanMgmtConstants.TWO] != null && tempObject[PlanMgmtConstants.TWO].toString().equalsIgnoreCase(Plan.PlanMarket.INDIVIDUAL.toString())){
					planResponse.setIssuerSite(tempObject[PlanMgmtConstants.NINE] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.NINE].toString());
					planResponse.setIssuerPhone(tempObject[PlanMgmtConstants.TEN] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.TEN].toString());
				}else{
					planResponse.setIssuerSite(PlanMgmtConstants.EMPTY_STRING);
					planResponse.setIssuerPhone(PlanMgmtConstants.EMPTY_STRING);					
				}
				
				planResponse.setPlanMarket(tempObject[PlanMgmtConstants.TWO] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.TWO].toString());
				
				if(tempObject[PlanMgmtConstants.TWO] != null && tempObject[PlanMgmtConstants.TWO].toString().equalsIgnoreCase(Plan.PlanMarket.SHOP.toString())){
					planResponse.setIssuerSite(tempObject[PlanMgmtConstants.ELEVEN] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.ELEVEN].toString());
					planResponse.setIssuerPhone(tempObject[PlanMgmtConstants.TWELVE] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.TWELVE].toString());
				}else{
					planResponse.setIssuerSite(PlanMgmtConstants.EMPTY_STRING);
					planResponse.setIssuerPhone(PlanMgmtConstants.EMPTY_STRING);					
				}
				
				if(tempObject[PlanMgmtConstants.THREE] != null && tempObject[PlanMgmtConstants.THREE].toString().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
					if(!(healthPlanIdBuilder.toString().isEmpty())){
						healthPlanIdBuilder.append(PlanMgmtConstants.COMMA + tempObject[PlanMgmtConstants.ZERO]);
					}else{
						healthPlanIdBuilder.append(tempObject[PlanMgmtConstants.ZERO]);
					}
				}else if(tempObject[PlanMgmtConstants.THREE] != null && tempObject[PlanMgmtConstants.THREE].toString().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
					if(!(dentalPlanIdBuilder.toString().isEmpty())){
						dentalPlanIdBuilder.append(PlanMgmtConstants.COMMA + tempObject[PlanMgmtConstants.ZERO]);
					}else{
						dentalPlanIdBuilder.append(tempObject[PlanMgmtConstants.ZERO]);
					}
				}else if(tempObject[PlanMgmtConstants.THREE] != null && tempObject[PlanMgmtConstants.THREE].toString().equalsIgnoreCase(Plan.PlanInsuranceType.STM.toString())){
					
					if(!(stmPlanIdBuilder.toString().isEmpty())){
						stmPlanIdBuilder.append(PlanMgmtConstants.COMMA + tempObject[PlanMgmtConstants.ZERO]);
					}else{
						stmPlanIdBuilder.append(tempObject[PlanMgmtConstants.ZERO]);
					}
				}
				
				planResponse.setTaxIdNumber(tempObject[PlanMgmtConstants.THIRTEEN] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.THIRTEEN].toString());
				planResponse.setHiosIssuerId(tempObject[PlanMgmtConstants.FOURTEEN] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.FOURTEEN].toString());
				planResponse.setPaymentUrl(tempObject[PlanMgmtConstants.FIFTEEN] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.FIFTEEN].toString());
				planResponse.setApplicationUrl(tempObject[PlanMgmtConstants.SIXTEEN] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.SIXTEEN].toString());
				planResponse.setProviderUrl((tempObject[PlanMgmtConstants.FIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : getProviderURL(tempObject[PlanMgmtConstants.FIVE].toString()));
				planResponse.setExchangeType(tempObject[PlanMgmtConstants.SEVENTEEN] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.SEVENTEEN].toString());
				planResponse.setHsa(tempObject[PlanMgmtConstants.EIGHTEEN] == null ? PlanMgmtConstants.EMPTY_STRING : tempObject[PlanMgmtConstants.EIGHTEEN].toString());
				//planResponse.setIssuerLogo(tempObject[PlanMgmtConstants.NINTEEN] == null ? PlanMgmtConstants.EMPTY_STRING : appUrl+tempObject[PlanMgmtConstants.NINTEEN]);
				if(null != tempObject[PlanMgmtConstants.NINTEEN]) {
					logoURL = tempObject[PlanMgmtConstants.NINTEEN].toString();
				}

				planResponse.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, planResponse.getHiosIssuerId(), null, appUrl));
				
				if(planResponse.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString()) || planResponse.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
					String planRegionName = getRegionName(planResponse.getPlanId(), planResponse.getInsuranceType());
					planResponse.setPlanRegionName(planRegionName);
				}else{
					planResponse.setPlanRegionName(PlanMgmtConstants.EMPTY_STRING);
				}
				
				// If consider EHB is ON, then compute and send EHB percentage in response. Ref HIX-39220 
				if(considerEHBPortion.equalsIgnoreCase(PlanMgmtConstants.Y)){						
					// set default value as 1 for EHB percentage and 0 for state mandate EHB percentage
					planResponse.setEhbPercentage(PlanMgmtConstants.ONE_STR);
					planResponse.setStateEhbMandatePercentage(PlanMgmtConstants.STRZERO);
					
					// HIX-65077, fetch ehb portion from plan table. no more use of URRT template
					if(tempObject[PlanMgmtConstants.TWENTY] != null){
						planResponse.setEhbPercentage(tempObject[PlanMgmtConstants.TWENTY].toString());
					}					
				}

				planResponsesList.add(planResponse);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getPlanInfoMap: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("healthPlanIdBuilder: " + healthPlanIdBuilder);
			LOGGER.debug("dentalPlanIdBuilder: " + dentalPlanIdBuilder);
			LOGGER.debug("stmPlanIdBuilder: " + stmPlanIdBuilder);
		}
		planResponsesList = getDataOfPlanIds(healthPlanIdBuilder, dentalPlanIdBuilder, stmPlanIdBuilder, planResponsesList);
		return planResponsesList;
	}
	
	private List<PlanResponse> getDataOfPlanIds(StringBuilder healthPlanIdBuilder, StringBuilder dentalPlanIdBuilder, StringBuilder stmPlanIdBuilder, List<PlanResponse> temp_planResponsesList){
		List<PlanResponse> finalPlanResponsesList = new ArrayList<PlanResponse>();
		
		Map<String, Map<String, Map<String, String>>> planHealthBenefitData = new HashMap<String, Map<String, Map<String, String>>>();
		Map<String, Map<String, Map<String, String>>> planDentalBenefitData = new HashMap<String, Map<String, Map<String, String>>>();
		Map<String, Map<String, Map<String, String>>> planHealthCostData = new HashMap<String, Map<String, Map<String, String>>>();
		Map<String, Map<String, Map<String, String>>> planDentalCostData = new HashMap<String, Map<String, Map<String, String>>>();
		Map<String, Map<String, String>> stmPlanData = new HashMap<String, Map<String, String>>();
		StringBuilder stmidBuilder = new StringBuilder();
		String considerEHBPortion = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CONSIDER_EHB_PORTION);
		
		try{
			Map<String, Map<String, Map<String, String>>> stmPlanBenefit = null; 
			Map<String, Map<String, Map<String, String>>> stmPlanCost = null;
			
			if(healthPlanIdBuilder != null && !(healthPlanIdBuilder.toString().isEmpty())){
				planHealthCostData = getHealthPlanCosts(healthPlanIdBuilder.toString());
				planHealthBenefitData = getHealthPlanBenefits(healthPlanIdBuilder.toString());
				
			}
			if(dentalPlanIdBuilder != null && !(dentalPlanIdBuilder.toString().isEmpty())){
				planDentalBenefitData = getDentalPlanBenefits(dentalPlanIdBuilder.toString());
				planDentalCostData =  getDentalPlanCosts(dentalPlanIdBuilder.toString());
			}
			if(stmPlanIdBuilder != null && !(stmPlanIdBuilder.toString().isEmpty())){
				stmPlanData = getSTMPlanInfo(stmPlanIdBuilder.toString());
				String[] tempArr = stmPlanIdBuilder.toString().split(PlanMgmtConstants.COMMA);
				for(String s : tempArr){
					Map<String, String> tempMap = stmPlanData.get(s.toString());
					if(stmidBuilder.toString().isEmpty()){
						stmidBuilder.append(tempMap.get(PlanMgmtConstants.PLAN_STM_ID));
					}else{
						stmidBuilder.append(PlanMgmtConstants.COMMA + tempMap.get(PlanMgmtConstants.PLAN_STM_ID));
					}
				}
			}
	
			if(!(stmidBuilder.toString().isEmpty())){
				stmPlanBenefit = stmPlanRateBenefitService.getStmPlanBenefits(stmidBuilder.toString());
				stmPlanCost = stmPlanRateBenefitService.getStmPlanCosts(stmidBuilder.toString());
			}
	
			for(PlanResponse planResponse : temp_planResponsesList){
				String deductibleCost = PlanMgmtConstants.NA;
				String deductibleCostFamily = PlanMgmtConstants.NA;
				
				if(planResponse.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())){
					planResponse.setPlanBenefits(planHealthBenefitData.get(String.valueOf(planResponse.getPlanId())));
					
					Map<String, Map<String, String>> healthBenefitData = planHealthBenefitData.get(String.valueOf(planResponse.getPlanId()));
					if(healthBenefitData.get(PlanMgmtConstants.GENERIC) != null){
						planResponse.setGenericMedications(healthBenefitData.get(PlanMgmtConstants.GENERIC).get(PlanMgmtConstants.NETWORK_T1_DISPLAY));
						planResponse.setGenericDrugs(healthBenefitData.get(PlanMgmtConstants.GENERIC).get(PlanMgmtConstants.TIER1_COPAY_VALUE));
					}
					if(healthBenefitData.get(PlanMgmtConstants.PRIMARY_VISIT) != null){
						planResponse.setOfficeVisit(healthBenefitData.get(PlanMgmtConstants.PRIMARY_VISIT).get(PlanMgmtConstants.NETWORK_T1_DISPLAY));
						planResponse.setOfficeVisitCost(healthBenefitData.get(PlanMgmtConstants.PRIMARY_VISIT).get(PlanMgmtConstants.TIER1_COPAY_VALUE));
					}
					
					Map<String, Map<String, Map<String,String>>> healthcostMap = getPlanCostMap(planHealthCostData.get(String.valueOf(planResponse.getPlanId())));
					planResponse.setPlanCosts(healthcostMap.get(PlanMgmtConstants.PLAN_COST_MAP));
					planResponse.setOptionalDeductible(healthcostMap.get(PlanMgmtConstants.OPTIONAL_COST_MAP));
					
					Map<String, Map<String, String>> planCosts = planHealthBenefitData.get(String.valueOf(planResponse.getPlanId()));
					if(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL) != null){  
						deductibleCost = planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL).get(PlanMgmtConstants.IN_NETWORK_IND) ;
						deductibleCostFamily = planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL).get(PlanMgmtConstants.IN_NETWORK_FLY) ;
					}
					else if(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG) != null){
						deductibleCost = planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG).get(PlanMgmtConstants.IN_NETWORK_IND) ;
						deductibleCostFamily = planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG).get(PlanMgmtConstants.IN_NETWORK_FLY) ;
					}
					
					planResponse.setDeductible(deductibleCost);
					planResponse.setDeductibleFamily(deductibleCostFamily);
					finalPlanResponsesList.add(planResponse);
				}
				if(planResponse.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
					
					
					String ehb_appt_for_pediatric_dental = planDentalBenefitData.get(String.valueOf(planResponse.getPlanId())).get(PlanMgmtConstants.EHB_APPT_FOR_PEDIATRIC_DENTAL).get(PlanMgmtConstants.EHB_APPT_FOR_PEDIATRIC_DENTAL_VALUE);
					planDentalBenefitData.get(String.valueOf(planResponse.getPlanId())).remove(PlanMgmtConstants.EHB_APPT_FOR_PEDIATRIC_DENTAL);
					
					planResponse.setPlanBenefits(planDentalBenefitData.get(String.valueOf(planResponse.getPlanId())));
					
					Map<String, Map<String, String>> dentalBenefitData = planDentalBenefitData.get(String.valueOf(planResponse.getPlanId()));
					if(dentalBenefitData.get(PlanMgmtConstants.GENERIC) != null){
						planResponse.setGenericMedications(dentalBenefitData.get(PlanMgmtConstants.GENERIC).get(PlanMgmtConstants.NETWORK_T1_DISPLAY));
						planResponse.setGenericDrugs(dentalBenefitData.get(PlanMgmtConstants.GENERIC).get(PlanMgmtConstants.TIER1_COPAY_VALUE));
					}
					
					if(dentalBenefitData.get(PlanMgmtConstants.PRIMARY_VISIT) != null){
						planResponse.setOfficeVisit(dentalBenefitData.get(PlanMgmtConstants.PRIMARY_VISIT).get(PlanMgmtConstants.NETWORK_T1_DISPLAY));
						planResponse.setOfficeVisitCost(dentalBenefitData.get(PlanMgmtConstants.PRIMARY_VISIT).get(PlanMgmtConstants.TIER1_COPAY_VALUE));
					}
					
					Map<String, Map<String, Map<String,String>>> dentalcostMap = getPlanCostMap(planDentalCostData.get(String.valueOf(planResponse.getPlanId())));
					planResponse.setPlanCosts(dentalcostMap.get(PlanMgmtConstants.PLAN_COST_MAP));
					planResponse.setOptionalDeductible(dentalcostMap.get(PlanMgmtConstants.OPTIONAL_COST_MAP));
					
					Map<String, Map<String, String>> planCosts = planDentalCostData.get(String.valueOf(planResponse.getPlanId()));
					if(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL) != null){  
						deductibleCost = planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL).get(PlanMgmtConstants.IN_NETWORK_IND) ;
						deductibleCostFamily = planCosts.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL).get(PlanMgmtConstants.IN_NETWORK_FLY) ;
					}
					else if(planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG) != null){
						deductibleCost = planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG).get(PlanMgmtConstants.IN_NETWORK_IND) ;
						deductibleCostFamily = planCosts.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG).get(PlanMgmtConstants.IN_NETWORK_FLY) ;
					}
					
					if(considerEHBPortion.equalsIgnoreCase(PlanMgmtConstants.Y)){	
						// send Pediatric Dental component if exists
						if(ehb_appt_for_pediatric_dental != null && !(ehb_appt_for_pediatric_dental.isEmpty())){
							planResponse.setPediatricDentalComponent(ehb_appt_for_pediatric_dental.replace("$", ""));
						}else{
							planResponse.setPediatricDentalComponent(PlanMgmtConstants.EMPTY_STRING);
						}
					}
					
					planResponse.setDeductible(deductibleCost);
					planResponse.setDeductibleFamily(deductibleCostFamily);
					finalPlanResponsesList.add(planResponse);
				}
				if(planResponse.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.STM.toString()) && stmPlanData.containsKey(String.valueOf(planResponse.getPlanId()))){
					
						Map<String, String> stmData = stmPlanData.get(String.valueOf(planResponse.getPlanId()));
						planResponse.setOopMax(stmData.get(PlanMgmtConstants.OOP_MAX_VAL));
						planResponse.setOopMaxFamily(stmData.get(PlanMgmtConstants.OOP_MAX_FAMILY));
						planResponse.setCoinsurance(stmData.get(PlanMgmtConstants.COINSURANCE));
						planResponse.setPolicyLength(stmData.get(PlanMgmtConstants.MAX_DURATION));
						planResponse.setPolicyLengthUnit(stmData.get(PlanMgmtConstants.DURATION_UNIT));
						planResponse.setPolicyLimit(stmData.get(PlanMgmtConstants.POLICY_MAX));
						
						if((stmData.get(PlanMgmtConstants.OUT_OF_NETWORK_COVERAGE) != null && !(stmData.get(PlanMgmtConstants.OUT_OF_NETWORK_COVERAGE).isEmpty())) && (stmData.get(PlanMgmtConstants.OUT_OF_NETWORK_COVERAGE).equalsIgnoreCase(PlanMgmtConstants.Y))){
							planResponse.setOutOfNetwkCoverage(PlanMgmtConstants.YES);
						}else if((stmData.get(PlanMgmtConstants.OUT_OF_NETWORK_COVERAGE) != null && !(stmData.get(PlanMgmtConstants.OUT_OF_NETWORK_COVERAGE).isEmpty())) && (stmData.get(PlanMgmtConstants.OUT_OF_NETWORK_COVERAGE).equalsIgnoreCase(PlanMgmtConstants.N))){
							planResponse.setOutOfNetwkCoverage(PlanMgmtConstants.NO);
						}
	
						if((stmData.get(PlanMgmtConstants.OUT_OF_COUNTRY_COVERAGE) != null && !(stmData.get(PlanMgmtConstants.OUT_OF_COUNTRY_COVERAGE).isEmpty())) && (stmData.get(PlanMgmtConstants.OUT_OF_COUNTRY_COVERAGE).equalsIgnoreCase(PlanMgmtConstants.Y))){
							planResponse.setOutOfCountyCoverage(PlanMgmtConstants.YES);
						}else if((stmData.get(PlanMgmtConstants.OUT_OF_COUNTRY_COVERAGE) != null && !(stmData.get(PlanMgmtConstants.OUT_OF_COUNTRY_COVERAGE).isEmpty())) && (stmData.get(PlanMgmtConstants.OUT_OF_COUNTRY_COVERAGE).equalsIgnoreCase(PlanMgmtConstants.N))){
							planResponse.setOutOfCountyCoverage(PlanMgmtConstants.NO);
						}
						
						planResponse.setSeparateDrugDeductible(stmData.get(PlanMgmtConstants.SEPARATE_DEDT_PRESCPT_DRUG));
	
						//Fixed the HP FOD Null Dereference issue
						if(stmPlanBenefit != null  && !(stmPlanBenefit.toString().isEmpty())){
							planResponse.setPlanBenefits(stmPlanBenefit.get(stmData.get(PlanMgmtConstants.PLAN_STM_ID).toString()));
						} else {
							Map<String, Map<String, String>> emptyStmPlanBenefit = new HashMap<String, Map<String, String>>();
							planResponse.setPlanBenefits(emptyStmPlanBenefit);
						}
						
						//Fixed the HP FOD Null Dereference issue
						if(stmPlanCost != null && !(stmPlanCost.toString().isEmpty())){
							planResponse.setPlanCosts(stmPlanCost.get(stmData.get(PlanMgmtConstants.PLAN_STM_ID).toString()));
						}else{
							Map<String, Map<String, String>> emptyStmPlanCost = new HashMap<String, Map<String, String>>();
							planResponse.setPlanCosts(emptyStmPlanCost);						
						}
						finalPlanResponsesList.add(planResponse);
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getDataOfPlanIds: ", ex);
		}
		return finalPlanResponsesList;
	}
	
	private Map<String, Map<String, Map<String,String>>> getPlanCostMap(Map<String, Map<String,String>> planCostData){
		Map<String, Map<String, Map<String,String>>> finalCostMap = new HashMap<String, Map<String, Map<String,String>>>();
		try{
			Map<String, Map<String,String>> costMap = planCostData;
			Set<String> costNameSet = costMap.keySet();
			Map<String, Map<String,String>> planCostMap = new HashMap<String, Map<String,String>>();
			Map<String, Map<String,String>> optionalPlanCostMap = new HashMap<String, Map<String,String>>();
	
			List<String> costList = getCostNames();
			for (String costName: costNameSet){
				if(costList.contains(costName)){
					planCostMap.put(costName, costMap.get(costName));
				}else{
					optionalPlanCostMap.put(costName, costMap.get(costName));
				}
			}
			
			finalCostMap.put(PlanMgmtConstants.PLAN_COST_MAP, planCostData);
			finalCostMap.put(PlanMgmtConstants.OPTIONAL_COST_MAP, optionalPlanCostMap);
		}catch(Exception ex){
			LOGGER.error("Exception occured in getPlanCostMap: ", ex);
		}
		
		return finalCostMap;
	}
	
	@Transactional(readOnly = true)
	private Map<String, Map<String, String>> getSTMPlanInfo(String stmPlanIdBuilder){
		StringBuilder buildquery = new StringBuilder();
		StringBuilder stmPlanIdBuilderStr = new StringBuilder();
		String[] planStmIdStrArr =  stmPlanIdBuilder.replace("'", "").split(",");
		buildquery.append("SELECT P.ID, PS.ID AS PLAN_STM_ID, PS.PLAN_ID, PS.OOP_MAX_VAL, PS.OOP_MAX_FAMILY, PS.COINSURANCE, PS.MAX_DURATION, PS.DURATION_UNIT, ");
		buildquery.append("PS.POLICY_MAX, PS.OUT_OF_NETWORK_COVERAGE, PS.OUT_OF_COUNTRY_COVERAGE, PSC.SEPARATE_DEDT_PRESCPT_DRUG ");
		buildquery.append("FROM PLAN P, PLAN_STM PS, PLAN_STM_COST PSC ");
		buildquery.append("WHERE P.ID = PS.PLAN_ID AND PS.ID = PSC.PLAN_STM_ID AND P.ID IN (");
		for( String stmPlanId : planStmIdStrArr ){
			stmPlanIdBuilderStr.append((stmPlanIdBuilderStr.length() == PlanMgmtConstants.ZERO ) ? PlanMgmtConstants.QUESTION_MARK : PlanMgmtConstants.COMA_QUESTION_MARK);
		}
		buildquery.append(stmPlanIdBuilderStr);
		buildquery.append(") ORDER BY P.ID ASC");
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT buildquery :: " + buildquery);
		}
		
		EntityManager entityManager = null;
		Map<String, Map<String, String>> stmPlanMap = new HashMap<String, Map<String, String>>();
		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildquery.toString());
			int itemIndex = 1;    		
    		for( String stmPlanId : planStmIdStrArr){
    			query.setParameter(itemIndex, stmPlanId.trim());
    			itemIndex++;
    		}
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();

			while (rsIterator.hasNext()) {
				Map<String, String> stmInfoMap = new HashMap<String, String>();
				Object[] fieldsArray = (Object[]) rsIterator.next();
				String rsPlanId = fieldsArray[PlanMgmtConstants.ZERO].toString();
				stmInfoMap.put("PLAN_ID", fieldsArray[PlanMgmtConstants.ZERO].toString());
				stmInfoMap.put("PLAN_STM_ID", fieldsArray[PlanMgmtConstants.ONE].toString());
				stmInfoMap.put("OOP_MAX_VAL", fieldsArray[PlanMgmtConstants.THREE] == null ? PlanMgmtConstants.EMPTY_STRING : fieldsArray[PlanMgmtConstants.THREE].toString());
				stmInfoMap.put("OOP_MAX_FAMILY", fieldsArray[PlanMgmtConstants.FOUR] == null ? PlanMgmtConstants.EMPTY_STRING :fieldsArray[PlanMgmtConstants.FOUR].toString());
				stmInfoMap.put("COINSURANCE", fieldsArray[PlanMgmtConstants.FIVE] == null ? PlanMgmtConstants.EMPTY_STRING :fieldsArray[PlanMgmtConstants.FIVE].toString());
				stmInfoMap.put("MAX_DURATION", fieldsArray[PlanMgmtConstants.SIX] == null ? PlanMgmtConstants.EMPTY_STRING :fieldsArray[PlanMgmtConstants.SIX].toString());
				stmInfoMap.put("DURATION_UNIT", fieldsArray[PlanMgmtConstants.SEVEN] == null ? PlanMgmtConstants.EMPTY_STRING :fieldsArray[PlanMgmtConstants.SEVEN].toString());
				stmInfoMap.put("POLICY_MAX", fieldsArray[PlanMgmtConstants.EIGHT] == null ? PlanMgmtConstants.EMPTY_STRING :fieldsArray[PlanMgmtConstants.EIGHT].toString());
				stmInfoMap.put("OUT_OF_NETWORK_COVERAGE", fieldsArray[PlanMgmtConstants.NINE] == null ? PlanMgmtConstants.EMPTY_STRING :fieldsArray[PlanMgmtConstants.NINE].toString());
				stmInfoMap.put("OUT_OF_COUNTRY_COVERAGE", fieldsArray[PlanMgmtConstants.TEN] == null ? PlanMgmtConstants.EMPTY_STRING :fieldsArray[PlanMgmtConstants.TEN].toString());
				stmInfoMap.put("SEPARATE_DEDT_PRESCPT_DRUG", fieldsArray[PlanMgmtConstants.ELEVEN] == null ? PlanMgmtConstants.EMPTY_STRING :fieldsArray[PlanMgmtConstants.ELEVEN].toString());
				stmPlanMap.put(rsPlanId, stmInfoMap);
			}
		} catch(Exception ex) {
			LOGGER.error("Exception occured in getSTMPlanInfo: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return stmPlanMap;
	}
	
	@Transactional(readOnly = true)
	private Map<String, Map<String, Map<String, String>>> getHealthPlanBenefits(String healthPlanIdStr) {
		// pull require health plan benefits only	
		String[] helathBenefitListArr = GhixConstants.helathBenefitList;
		String helathBenefitListStr = PlanMgmtConstants.EMPTY_STRING;

		for (String helathBenefit : helathBenefitListArr) {
			helathBenefitListStr += helathBenefitListStr.length() == 0 ? "'" + helathBenefit + "'" : "," + "'" + helathBenefit + "'";
		}

		StringBuilder buildquery = new StringBuilder();
		String[] healthIdStrArr =  healthPlanIdStr.replace("'", "").split(",");
		buildquery.append("SELECT PHB.limitation, PHB.limitation_attr, PHB.network_exceptions, ");
		buildquery.append("PHB.NETWORK_T1_COPAY_VAL, PHB.NETWORK_T1_COPAY_ATTR, PHB.NETWORK_T1_COINSURANCE_VAL, PHB.NETWORK_T1_COINSURANCE_ATTR, ");
		buildquery.append("PHB.NETWORK_T2_COPAY_VAL, PHB.NETWORK_T2_COPAY_ATTR, PHB.NETWORK_T2_COINSURANCE_VAL, PHB.NETWORK_T2_COINSURANCE_ATTR, ");
		buildquery.append("PHB.OUTNETWORK_COPAY_VAL, PHB.OUTNETWORK_COPAY_ATTR, PHB.OUTNETWORK_COINSURANCE_VAL, PHB.OUTNETWORK_COINSURANCE_ATTR, ");
		buildquery.append("PHB.subject_to_in_net_deductible, PHB.subject_to_out_net_deductible, PHB.name, PHB.PLAN_HEALTH_ID, ");
		buildquery.append("PHB.NETWORK_T1_DISPLAY, PHB.NETWORK_T2_DISPLAY, PHB.OUTNETWORK_DISPLAY, PHB.LIMIT_EXCEP_DISPLAY, PHB.IS_COVERED, PHB.NETWORK_T1_TILE_DISPLAY, P.ID, PHB.EXPLANATION ");
		buildquery.append("FROM ");
		buildquery.append("PLAN P, PLAN_HEALTH PH, PLAN_HEALTH_BENEFIT PHB ");
		buildquery.append("WHERE ");
		buildquery.append("P.ID = PH.PLAN_ID AND PH.ID = PHB.PLAN_HEALTH_ID AND P.ID IN (:paramIds) ");
		buildquery.append("AND PHB.name IN (");
		buildquery.append(helathBenefitListStr);
		buildquery.append(")");
		
		List<Integer> idList = new ArrayList<Integer>();
		if (healthIdStrArr != null && healthIdStrArr.length > PlanMgmtConstants.ZERO ) {
			for( String planId : healthIdStrArr ){
				idList.add(Integer.parseInt(planId));
			}
		}
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT buildquery :: " + buildquery);
		}
		Map<String, Map<String, Map<String, String>>> benefitDataList = new HashMap<String, Map<String, Map<String, String>>>();
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildquery.toString());
			if(idList.size() != 0){
				query.setParameter("paramIds",idList);
    		}
			
			Iterator<?> rsIterator = query.getResultList().iterator();

			while (rsIterator.hasNext()) {
				Object[] benefitFileds = (Object[]) rsIterator.next();
				String benefitName = benefitFileds[PlanMgmtConstants.SEVENTEEN].toString();
				String rshealthPlanId = benefitFileds[PlanMgmtConstants.TWENTYFIVE].toString();
				Map<String, String> benefitAttrib = new HashMap<String, String>();

				benefitAttrib.put("netWkT1Disp", (benefitFileds[PlanMgmtConstants.NINTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.NINTEEN].toString());
				benefitAttrib.put("netWkT2Disp", (benefitFileds[PlanMgmtConstants.TWENTY] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTY].toString());
				benefitAttrib.put("outNetWkDisp", (benefitFileds[PlanMgmtConstants.TWENTYONE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYONE].toString());
				benefitAttrib.put("limitExcepDisp", (benefitFileds[PlanMgmtConstants.TWENTYTWO] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYTWO].toString());
				benefitAttrib.put("netWkT1TileDisp", (benefitFileds[PlanMgmtConstants.TWENTYFOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYFOUR].toString());
				

				//We will decommissioned following attributes when plan display starts using above attributes
				benefitAttrib.put("netwkLimit", (benefitFileds[PlanMgmtConstants.ZERO] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.ZERO].toString());
				benefitAttrib.put("netwkLimitAttrib", (benefitFileds[PlanMgmtConstants.ONE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.ONE].toString());
				benefitAttrib.put("netwkExce", (benefitFileds[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWO].toString());
				benefitAttrib.put("tier1CopayVal", (benefitFileds[PlanMgmtConstants.THREE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.THREE].toString());
				benefitAttrib.put("tier1CopayAttrib", (benefitFileds[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.FOUR].toString());
				benefitAttrib.put("tier1CoinsVal", (benefitFileds[PlanMgmtConstants.FIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.FIVE].toString());
				benefitAttrib.put("tier1CoinsAttrib", (benefitFileds[PlanMgmtConstants.SIX] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.SIX].toString());
				benefitAttrib.put("tier2CopayVal", (benefitFileds[PlanMgmtConstants.SEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.SEVEN].toString());
				benefitAttrib.put("tier2CopayAttrib", (benefitFileds[PlanMgmtConstants.EIGHT] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.EIGHT].toString());
				benefitAttrib.put("tier2CoinsVal", (benefitFileds[PlanMgmtConstants.NINE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.NINE].toString());
				benefitAttrib.put("tier2CoinsAttrib", (benefitFileds[PlanMgmtConstants.TEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TEN].toString());
				benefitAttrib.put("outNetWkCopayVal", (benefitFileds[PlanMgmtConstants.ELEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.ELEVEN].toString());
				benefitAttrib.put("outNetWkCopayAttrib", (benefitFileds[PlanMgmtConstants.TWELVE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWELVE].toString());
				benefitAttrib.put("outNetWkCoinsVal", (benefitFileds[PlanMgmtConstants.THIRTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.THIRTEEN].toString());
				benefitAttrib.put("outNetWkCoinsAttrib", (benefitFileds[PlanMgmtConstants.FOURTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.FOURTEEN].toString());
				benefitAttrib.put("subToNetDeduct", (benefitFileds[PlanMgmtConstants.FIFTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.FIFTEEN].toString());
				benefitAttrib.put("subToNonNetDeduct", (benefitFileds[PlanMgmtConstants.SIXTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.SIXTEEN].toString());
				benefitAttrib.put("isCovered", (benefitFileds[PlanMgmtConstants.TWENTYTHREE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYTHREE].toString());
				benefitAttrib.put("explanation", (benefitFileds[PlanMgmtConstants.TWENTYSIX] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYSIX].toString());

				if (benefitDataList.get(rshealthPlanId) == null) {
					benefitDataList.put(rshealthPlanId, new HashMap<String, Map<String, String>>());
				}
				benefitDataList.get(rshealthPlanId).put(benefitName, benefitAttrib);

			}
		} catch(Exception ex) {
			LOGGER.error("Exception occured in getHealthPlanBenefits : ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return benefitDataList;
	}
	
	@Transactional(readOnly = true)
	private Map<String, Map<String, Map<String, String>>> getHealthPlanCosts(String healthplanIdStr) {
		StringBuilder buildquery = new StringBuilder();
		String[] healthIdStrArr =  healthplanIdStr.replace("'", "").split(",");
		buildquery.append(" SELECT phc.in_network_ind, phc.in_network_fly, phc.in_network_tier2_ind, phc.in_network_tier2_fly, phc.out_network_ind, ");
		buildquery.append(" phc.out_network_fly, phc.combined_in_out_network_ind, phc.combined_in_out_network_fly, phc.comb_def_coins_network_tier1, ");
		buildquery.append(" phc.comb_def_coins_network_tier2, phc.name, phc.plan_health_id, phc.limit_excep_display, p.id, phc.in_network_fly_pp, phc.out_network_fly_pp, phc.in_network_tier2_fly_pp, phc.combined_in_out_network_fly_pp ");
		buildquery.append(" from plan_health_cost phc,plan_health ph, plan p WHERE p.id = ph.plan_id and ph.id = phc.PLAN_HEALTH_ID");
		buildquery.append(" and p.id IN (:paramIds )");
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT buildquery :: " + buildquery);
		}
		List<Integer> idList = new ArrayList<Integer>();
		if (healthIdStrArr != null && healthIdStrArr.length > PlanMgmtConstants.ZERO) {
			for(String id : healthIdStrArr){
				idList.add(Integer.parseInt(id));
			}
		}

		Map<String, Map<String, Map<String, String>>> costDataList = new HashMap<String, Map<String, Map<String, String>>>();
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildquery.toString());
			if(idList.size() != 0){
				query.setParameter("paramIds",idList);
    		}
			
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();

			while (rsIterator.hasNext()) {
				Map<String, String> costAttrib = new HashMap<String, String>();
				Object[] costFields = (Object[]) rsIterator.next();
				String name = costFields[PlanMgmtConstants.TEN].toString();
				String rsPlanId = costFields[PlanMgmtConstants.THIRTEEN].toString();

				costAttrib.put("inNetworkInd", (costFields[PlanMgmtConstants.ZERO] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.ZERO].toString());
				costAttrib.put("inNetworkFly", (costFields[PlanMgmtConstants.ONE] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.ONE].toString());
				costAttrib.put("inNetworkTier2Ind", (costFields[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.TWO].toString());
				costAttrib.put("inNetworkTier2Fly", (costFields[PlanMgmtConstants.THREE] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.THREE].toString());
				costAttrib.put("outNetworkInd", (costFields[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.FOUR].toString());
				costAttrib.put("outNetworkFly", (costFields[PlanMgmtConstants.FIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.FIVE].toString());
				costAttrib.put("combinedInOutNetworkInd", (costFields[PlanMgmtConstants.SIX] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.SIX].toString());
				costAttrib.put("combinedInOutNetworkFly", (costFields[PlanMgmtConstants.SEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.SEVEN].toString());
				costAttrib.put("combinedDefCoinsNetworkTier1", (costFields[PlanMgmtConstants.EIGHT] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.EIGHT].toString());
				costAttrib.put("combinedDefCoinsNetworkTier2", (costFields[PlanMgmtConstants.NINE] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.NINE].toString());
				costAttrib.put("limitExcepDisplay", (costFields[PlanMgmtConstants.TWELVE] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.TWELVE].toString());
				costAttrib.put("inNetworkFlyPerPerson", (costFields[PlanMgmtConstants.FOURTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.FOURTEEN].toString());
				costAttrib.put("outNetworkFlyPerPerson", (costFields[PlanMgmtConstants.FIFTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.FIFTEEN].toString());
				costAttrib.put("inNetworkTier2FlyPerPerson", (costFields[PlanMgmtConstants.SIXTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.SIXTEEN].toString());
				costAttrib.put("combinedInOutNetworkFlyPerPerson", (costFields[PlanMgmtConstants.SEVENTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.SEVENTEEN].toString());
				
				if (costDataList.get(rsPlanId) == null) {
					costDataList.put(rsPlanId, new HashMap<String, Map<String, String>>());
				}
				costDataList.get(rsPlanId).put(name, costAttrib);
			}
		} catch(Exception ex) {
			LOGGER.error("Exception occured in getHealthPlanCosts: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return costDataList;
	}
	
	@Transactional(readOnly = true)
	private Map<String, Map<String, Map<String, String>>> getDentalPlanBenefits(String dentalplanIdStr) {
		// pull require dental benefits only not all all benefits
		String[] dentalBenefitListArr = GhixConstants.dentalBenefitList;
		String dentalBenefitListStr = PlanMgmtConstants.EMPTY_STRING;
		String ehb_appt_for_pediatric_dental_value = PlanMgmtConstants.EMPTY_STRING;

		for (String dentalBenefit : dentalBenefitListArr) {
			dentalBenefitListStr += dentalBenefitListStr.length() == 0 ? "'" + dentalBenefit + "'" : "," + "'" + dentalBenefit + "'";
		}

		StringBuilder buildquery = new StringBuilder();
		StringBuilder dentalIdStr = new StringBuilder();
		String[] dentalIdStrArr =  dentalplanIdStr.replace("'", "").split(",");
		buildquery.append("SELECT PDB.limitation, PDB.limitation_attr, PDB.network_exceptions, ");
		buildquery.append("PDB.NETWORK_T1_COPAY_VAL, PDB.NETWORK_T1_COPAY_ATTR, PDB.NETWORK_T1_COINSURANCE_VAL, PDB.NETWORK_T1_COINSURANCE_ATTR, ");
		buildquery.append("PDB.NETWORK_T2_COPAY_VAL, PDB.NETWORK_T2_COPAY_ATTR, PDB.NETWORK_T2_COINSURANCE_VAL, PDB.NETWORK_T2_COINSURANCE_ATTR, ");
		buildquery.append("PDB.OUTNETWORK_COPAY_VAL, PDB.OUTNETWORK_COPAY_ATTR, PDB.OUTNETWORK_COINSURANCE_VAL, PDB.OUTNETWORK_COINSURANCE_ATTR, ");
		buildquery.append("PDB.subject_to_in_net_deductible, PDB.subject_to_out_net_deductible, PDB.name, PDB.plan_dental_id, ");
		buildquery.append("PDB.NETWORK_T1_DISPLAY, PDB.NETWORK_T2_DISPLAY, PDB.OUTNETWORK_DISPLAY, PDB.LIMIT_EXCEP_DISPLAY, PDB.IS_COVERED, PDB.NETWORK_T1_TILE_DISPLAY, P.ID, PD.ehb_appt_for_pediatric_dental, PDB.EXPLANATION ");
		buildquery.append("FROM ");
		buildquery.append("PLAN P, PLAN_DENTAL PD, PLAN_DENTAL_BENEFIT PDB ");
		buildquery.append("WHERE P.ID = PD.PLAN_ID AND PD.ID = PDB.PLAN_DENTAL_ID AND P.ID IN (");
		for( String planId : dentalIdStrArr ){
			dentalIdStr.append((dentalIdStr.length() == PlanMgmtConstants.ZERO ) ? PlanMgmtConstants.QUESTION_MARK : PlanMgmtConstants.COMA_QUESTION_MARK);
		}
		buildquery.append(dentalIdStr);
		buildquery.append(") AND PDB.name IN (");
		buildquery.append(dentalBenefitListStr);
		buildquery.append(")");

		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT buildquery :: " + buildquery);
		}
		Map<String, Map<String, Map<String, String>>> benefitDataList = new HashMap<String, Map<String, Map<String, String>>>();
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildquery.toString());
			int itemIndex = 1;    		
    		for( String planId : dentalIdStrArr){
    			query.setParameter(itemIndex, planId.trim());
    			itemIndex++;
    		}
			Iterator<?> rsIterator = query.getResultList().iterator();

			while (rsIterator.hasNext()) {
				Object[] benefitFileds = (Object[]) rsIterator.next();
				String benefitName = benefitFileds[PlanMgmtConstants.SEVENTEEN].toString();
				String rsDentalPlanId = benefitFileds[PlanMgmtConstants.TWENTYFIVE].toString();
				Map<String, String> benefitAttrib = new HashMap<String, String>();

				benefitAttrib.put("netWkT1Disp", (benefitFileds[PlanMgmtConstants.NINTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.NINTEEN].toString());
				benefitAttrib.put("netWkT2Disp", (benefitFileds[PlanMgmtConstants.TWENTY] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTY].toString());
				benefitAttrib.put("outNetWkDisp", (benefitFileds[PlanMgmtConstants.TWENTYONE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYONE].toString());
				benefitAttrib.put("limitExcepDisp", (benefitFileds[PlanMgmtConstants.TWENTYTWO] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYTWO].toString());
				benefitAttrib.put("netWkT1TileDisp", (benefitFileds[PlanMgmtConstants.TWENTYFOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYFOUR].toString());
				
				//We will decommissioned following attributes when plan display starts using above attributes  
				benefitAttrib.put("netwkLimit", (benefitFileds[PlanMgmtConstants.ZERO] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.ZERO].toString());
				benefitAttrib.put("netwkLimitAttrib", (benefitFileds[PlanMgmtConstants.ONE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.ONE].toString());
				benefitAttrib.put("netwkExce", (benefitFileds[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWO].toString());
				benefitAttrib.put("tier1CopayVal", (benefitFileds[PlanMgmtConstants.THREE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.THREE].toString());
				benefitAttrib.put("tier1CopayAttrib", (benefitFileds[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.FOUR].toString());
				benefitAttrib.put("tier1CoinsVal", (benefitFileds[PlanMgmtConstants.FIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.FIVE].toString());
				benefitAttrib.put("tier1CoinsAttrib", (benefitFileds[PlanMgmtConstants.SIX] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.SIX].toString());
				benefitAttrib.put("tier2CopayVal", (benefitFileds[PlanMgmtConstants.SEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.SEVEN].toString());
				benefitAttrib.put("tier2CopayAttrib", (benefitFileds[PlanMgmtConstants.EIGHT] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.EIGHT].toString());
				benefitAttrib.put("tier2CoinsVal", (benefitFileds[PlanMgmtConstants.NINE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.NINE].toString());
				benefitAttrib.put("tier2CoinsAttrib", (benefitFileds[PlanMgmtConstants.TEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TEN].toString());
				benefitAttrib.put("outNetWkCopayVal", (benefitFileds[PlanMgmtConstants.ELEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.ELEVEN].toString());
				benefitAttrib.put("outNetWkCopayAttrib", (benefitFileds[PlanMgmtConstants.TWELVE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWELVE].toString());
				benefitAttrib.put("outNetWkCoinsVal", (benefitFileds[PlanMgmtConstants.THIRTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.THIRTEEN].toString());
				benefitAttrib.put("outNetWkCoinsAttrib", (benefitFileds[PlanMgmtConstants.FOURTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.FOURTEEN].toString());
				benefitAttrib.put("subToNetDeduct", (benefitFileds[PlanMgmtConstants.FIFTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.FIFTEEN].toString());
				benefitAttrib.put("subToNonNetDeduct", (benefitFileds[PlanMgmtConstants.SIXTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.SIXTEEN].toString());
				benefitAttrib.put("isCovered", (benefitFileds[PlanMgmtConstants.TWENTYTHREE] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYTHREE].toString());
				ehb_appt_for_pediatric_dental_value = (benefitFileds[PlanMgmtConstants.TWENTYSIX] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYSIX].toString();
				benefitAttrib.put("explanation", (benefitFileds[PlanMgmtConstants.TWENTYSEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWENTYSEVEN].toString());
				
				if (benefitDataList.get(rsDentalPlanId) == null) {
					benefitDataList.put(rsDentalPlanId, new HashMap<String, Map<String, String>>());
				}
				
				if(benefitDataList.get(rsDentalPlanId).get(PlanMgmtConstants.EHB_APPT_FOR_PEDIATRIC_DENTAL) == null){
					Map<String, String> tempMap = new HashMap<String, String>();
					tempMap.put(PlanMgmtConstants.EHB_APPT_FOR_PEDIATRIC_DENTAL_VALUE, ehb_appt_for_pediatric_dental_value);
					benefitDataList.get(rsDentalPlanId).put(PlanMgmtConstants.EHB_APPT_FOR_PEDIATRIC_DENTAL, tempMap);
				}
				
				benefitDataList.get(rsDentalPlanId).put(benefitName, benefitAttrib);

			}
		} catch(Exception ex) {
			LOGGER.error("Exception occured in getDentalPlanBenefits : ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return benefitDataList;
	}
	
	@Transactional(readOnly = true)
	private Map<String, Map<String, Map<String, String>>> getDentalPlanCosts(String dentalplanIdStr) {
		StringBuilder buildquery = new StringBuilder();
		StringBuilder dentalIdStr = new StringBuilder();
		String[] dentalIdStrArr =  dentalplanIdStr.replace("'", "").split(",");
		buildquery.append(" SELECT pdc.in_network_ind, pdc.in_network_fly, pdc.in_network_tier2_ind, pdc.in_network_tier2_fly, pdc.out_network_ind, ");
		buildquery.append(" pdc.out_network_fly, pdc.combined_in_out_network_ind, pdc.combined_in_out_network_fly, pdc.comb_def_coins_network_tier1, ");
		buildquery.append(" pdc.comb_def_coins_network_tier2, pdc.name, pdc.plan_dental_id, p.id, pdc.in_network_fly_pp, pdc.out_network_fly_pp, pdc.in_network_tier2_fly_pp, pdc.combined_in_out_network_fly_pp ");
		buildquery.append(" from plan_dental_cost pdc,plan_dental pd, plan p ");
		buildquery.append(" WHERE pd.id = pdc.PLAN_DENTAL_ID and p.id = pd.plan_id AND");
		buildquery.append(" p.id IN (");
		for( String planId : dentalIdStrArr ){
			dentalIdStr.append((dentalIdStr.length() == PlanMgmtConstants.ZERO ) ? PlanMgmtConstants.QUESTION_MARK : PlanMgmtConstants.COMA_QUESTION_MARK);
		}
		buildquery.append(dentalIdStr);		
		buildquery.append(")");

		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT buildquery :: " + buildquery);
		}
		
		EntityManager entityManager = null;
		Map<String, Map<String, Map<String, String>>> costDataList = new HashMap<String, Map<String, Map<String, String>>>();
		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildquery.toString());
			int itemIndex = 1;    		
    		for( String dentalPlanId : dentalIdStrArr){
    			query.setParameter(itemIndex, dentalPlanId.trim());
    			itemIndex++;
    		}
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();

			while (rsIterator.hasNext()) {
				Map<String, String> costAttrib = new HashMap<String, String>();
				Object[] costFields = (Object[]) rsIterator.next();
				String name = costFields[PlanMgmtConstants.TEN].toString();
				String rsPlanId = costFields[PlanMgmtConstants.TWELVE].toString();

				costAttrib.put("inNetworkInd", (costFields[PlanMgmtConstants.ZERO] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.ZERO].toString());
				costAttrib.put("inNetworkFly", (costFields[PlanMgmtConstants.ONE] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.ONE].toString());
				costAttrib.put("inNetworkTier2Ind", (costFields[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.TWO].toString());
				costAttrib.put("inNetworkTier2Fly", (costFields[PlanMgmtConstants.THREE] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.THREE].toString());
				costAttrib.put("outNetworkInd", (costFields[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.FOUR].toString());
				costAttrib.put("outNetworkFly", (costFields[PlanMgmtConstants.FIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.FIVE].toString());
				costAttrib.put("combinedInOutNetworkInd", (costFields[PlanMgmtConstants.SIX] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.SIX].toString());
				costAttrib.put("combinedInOutNetworkFly", (costFields[PlanMgmtConstants.SEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.SEVEN].toString());
				costAttrib.put("combinedDefCoinsNetworkTier1", (costFields[PlanMgmtConstants.EIGHT] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.EIGHT].toString());
				costAttrib.put("combinedDefCoinsNetworkTier2", (costFields[PlanMgmtConstants.NINE] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.NINE].toString());
				costAttrib.put("inNetworkFlyPerPerson", (costFields[PlanMgmtConstants.THIRTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.THIRTEEN].toString());
				costAttrib.put("outNetworkFlyPerPerson", (costFields[PlanMgmtConstants.FOURTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.FOURTEEN].toString());
				costAttrib.put("inNetworkTier2FlyPerPerson", (costFields[PlanMgmtConstants.FIFTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.FIFTEEN].toString());
				costAttrib.put("combinedInOutNetworkFlyPerPerson", (costFields[PlanMgmtConstants.SIXTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : costFields[PlanMgmtConstants.SIXTEEN].toString());

				if (costDataList.get(rsPlanId) == null) {
					costDataList.put(rsPlanId, new HashMap<String, Map<String, String>>());
				}
				costDataList.get(rsPlanId).put(name, costAttrib);
			}
		} catch(Exception ex) {
			LOGGER.error("Exception occured in getDentalPlanCosts : ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		return costDataList;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Map<String, String>> getpProviderNetwork(String planId, List<Map<String, List<String>>> providerList){
		List<Map<String, String>> providerNetworkList = new ArrayList<Map<String, String>>();
		String buildquery = PlanMgmtQueryBuilder.getProviderNetworkQuery();
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT buildquery :: " + buildquery);
		}
		if(StringUtils.isNotBlank(planId)){
			EntityManager entityManager = null;
			try {
				entityManager = emf.createEntityManager();
				Query query = entityManager.createQuery(buildquery);
				query.setParameter(PlanMgmtParamConstants.PARAM_PLAN_ID, Integer.parseInt(planId.trim()));
	
				List<?> rsList = query.getResultList();
				Iterator<?> rsIterator = rsList.iterator();
				String networKey = "";
				String hasProviderData = ""; 
				Object[] objArray = null;
				
				while (rsIterator.hasNext()) {				
					 objArray = (Object[])(rsIterator.next());
					 networKey = (objArray[PlanMgmtConstants.ZERO] != null) ? objArray[PlanMgmtConstants.ZERO].toString() : "";
					 hasProviderData = (objArray[PlanMgmtConstants.ONE] != null) ? objArray[PlanMgmtConstants.ONE].toString() : "";				
				} 	
				if(!networKey.isEmpty() && !hasProviderData.isEmpty())	{	
					providerNetworkList = providerService.isPlanBelongsToProviderNetwork(providerList, networKey, hasProviderData, PlanMgmtConstants.YES, stateCode);
				}
				
			}catch(Exception ex) {
				LOGGER.error("Exception occured in getpProviderNetwork: ", ex);
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getpProviderNetwork ", ex);
			}finally{
				 // close the entity manager
				 if(entityManager !=null && entityManager.isOpen()){
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
			}	
		}	
		return providerNetworkList;	
	}
	
	@Transactional(readOnly = true)
	public List<PlanResponse> getPlanInfoByZip(String zip, String countyFIPS, String insuranceType, String exchangeType, String effectiveDate, String tenantCode, String medicarePlanDataSource){
		List<PlanResponse> planResponseList = new ArrayList<PlanResponse>();
		HashMap<String, Object> queryParams = new HashMap<String, Object>();
		String buildquery = getPlanInfoByZipQuery(zip, countyFIPS, insuranceType, exchangeType, effectiveDate, tenantCode, queryParams, medicarePlanDataSource);
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("SQL = " + buildquery);
		}
		
		if(!(buildquery.toString().isEmpty())){
			EntityManager entityManager = null;
			try {
				entityManager = emf.createEntityManager();
				Query query = entityManager.createNativeQuery(buildquery.toString());
				query = PlanMgmtUtil.setQueryParameters(query, queryParams);
				
				List<?> rsList = query.getResultList();
				Iterator<?> rsIterator = rsList.iterator();

				Object[] objArray = null;
				PlanResponse planResponse = null;
				while (rsIterator.hasNext()) {
					objArray = (Object[])(rsIterator.next());
					planResponse = new PlanResponse();
					planResponse.setPlanId(objArray[PlanMgmtConstants.ZERO] == null ? PlanMgmtConstants.ZERO : Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
					planResponse.setIssuerPlanNumber(objArray[PlanMgmtConstants.ONE] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ONE].toString());
					planResponse.setPlanName(objArray[PlanMgmtConstants.TWO] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWO].toString());
					
					if((Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(objArray[PlanMgmtConstants.SEVEN].toString())) ||
														(Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(objArray[PlanMgmtConstants.SEVEN].toString()))){
						planResponse.setCostSharing(objArray[PlanMgmtConstants.THREE] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.THREE].toString());
						planResponse.setPlanLevel(objArray[PlanMgmtConstants.FOUR] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOUR].toString());
					}
					
					planResponse.setIssuerId(objArray[PlanMgmtConstants.FIVE] == null ? PlanMgmtConstants.ZERO : Integer.parseInt(objArray[PlanMgmtConstants.FIVE].toString()));
					planResponse.setIssuerName(objArray[PlanMgmtConstants.SIX] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SIX].toString());
					planResponse.setInsuranceType(objArray[PlanMgmtConstants.SEVEN] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SEVEN].toString());
					planResponse.setHiosIssuerId(objArray[PlanMgmtConstants.EIGHT] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.EIGHT].toString());
					planResponseList.add(planResponse);
					planResponse =  null;
				}
				
			}catch(Exception ex){
				LOGGER.error("Exception occured in getPlanInfoByZip @ ServiceImpl", ex);
			}finally{
				 // close the entity manager
				 if(entityManager !=null && entityManager.isOpen()){
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
			}
		}

		return planResponseList;
	}
	
	public Map<String, Map<String, String>> getPlansMapData(List<PlanResponse> planResponsesList){
		Map<String,Map<String, String>> plansMap = new HashMap<String, Map<String, String>>();
		Map<String, String> planInfoMap = null;
		for(PlanResponse planResponse : planResponsesList){
			planInfoMap = new HashMap<String, String>();
			planInfoMap.put("name", planResponse.getPlanName());
			planInfoMap.put("issuerId", String.valueOf(planResponse.getIssuerId()));
			planInfoMap.put("issuerName", planResponse.getIssuerName());
			planInfoMap.put("metalTier", planResponse.getPlanLevel());
			planInfoMap.put("costSharing", planResponse.getCostSharing());
			planInfoMap.put("hiosIssuerId", planResponse.getHiosIssuerId());
			plansMap.put(String.valueOf(planResponse.getPlanId()), planInfoMap);
		}		
		return plansMap;
	}
	
	
	
	@Transactional(readOnly = true)
	private String getStateListForZipCode(String zipCode, String countyFIPS) {
		StringBuilder builder = new StringBuilder(1024);
		EntityManager entityManager = null;

		try {
			String buildquery = null;
			if (StringUtils.isNotBlank(countyFIPS)) {
				buildquery = PlanMgmtQueryBuilder.getStateListByZipAndCounty(configuredDB);
			}else{
				buildquery = PlanMgmtQueryBuilder.getStateListByZip(configuredDB);
			}
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("StateListForZip_Query:- " + buildquery);
			}
			entityManager = emf.createEntityManager();
			Query query = entityManager.createQuery(buildquery.toString());
			query.setParameter(PlanMgmtParamConstants.PARAM_ZIP_CODE, zipCode);
			if (StringUtils.isNotBlank(countyFIPS)) {
				query.setParameter(PlanMgmtParamConstants.PARAM_COUNTY_CODE, countyFIPS);
			}
			Iterator<?> rsIterator = query.getResultList().iterator();
			while (rsIterator.hasNext()) {
				String tempObject = (String) (rsIterator.next());
				if (builder.toString().isEmpty()) {
					builder.append(tempObject);
				} else {
					builder.append(PlanMgmtConstants.COMMA);
					builder.append(tempObject);
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getStateListForZipCode", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		return builder.toString();
	}
	
	 
	/**
	 * HIX-50844
     * @author Shubhendu Singh (mailto: shubhendu.singh@getinsured.com)
     * Created on: Sept 29, 2014 
     * Returns List of PlanResponse objects with information about plan name, 
     * issuer name, issuer id, issuer logo required to be displayed on Universal Cart Page
     * @param List of PlanIds(Integer)
     * @return List<PlanResponse>
     */
	@Transactional(readOnly = true)
	public List<PlanResponse> getPlanResponseListForUniversalCart (List<Integer> planIdList){
		List<PlanResponse> planResponseList =null;
		if(!planIdList.isEmpty()){
			planResponseList = new ArrayList<PlanResponse>();
			PlanResponse planResponse = null;
			StringBuilder buildquery = new StringBuilder();
			EntityManager entityManager = null;

			buildquery.append("select p.id as plan_id,p.name as plan_name, p.insurance_type ,i.logo_url, i.name as issuer_name,i.id as issuer_id, i.hios_issuer_id as hios_issuer_id ");
			buildquery.append("from issuers i inner join plan p ");
			buildquery.append("on P.ISSUER_ID = I.ID ");
			buildquery.append(" and p.id in (?1) ");
			try {
				entityManager = emf.createEntityManager();
				Query query = entityManager.createNativeQuery(buildquery.toString());
				query.setParameter(1, planIdList);
				List<?> rsList = query.getResultList();
				Iterator<?> rsIterator = rsList.iterator();
				String planId,planName,insuranceType,issuerLogo,issuerName,issuerId,hiosIssuerId;

				while (rsIterator.hasNext()) {				
					String logoURL = null;
					Object[] objArray = (Object[])(rsIterator.next());
					planId = (objArray[PlanMgmtConstants.ONE] != null) ? objArray[PlanMgmtConstants.ZERO].toString() : "";
					planName = (objArray[PlanMgmtConstants.ONE] != null) ? objArray[PlanMgmtConstants.ONE].toString() : "";
					insuranceType = (objArray[PlanMgmtConstants.TWO] != null) ? objArray[PlanMgmtConstants.TWO].toString() : "";
					if(null != objArray[PlanMgmtConstants.THREE]) {
						logoURL = objArray[PlanMgmtConstants.THREE].toString();
					}
					issuerName = (objArray[PlanMgmtConstants.FOUR] != null) ? objArray[PlanMgmtConstants.FOUR].toString() : "";
					issuerId = (objArray[PlanMgmtConstants.FIVE] != null) ? objArray[PlanMgmtConstants.FIVE].toString() : "";
					hiosIssuerId = (objArray[PlanMgmtConstants.SIX] != null) ? objArray[PlanMgmtConstants.SIX].toString() : "";
					issuerLogo = PlanMgmtUtil.getIssuerLogoURL(logoURL, hiosIssuerId, null, appUrl);
				//	LOGGER.debug(planId+ ": "+planName+ ": "+insuranceType+ ": "+issuerLogo+ ": "+issuerName+ ": "+issuerId);

					planResponse = new PlanResponse();
					planResponse.setPlanId(Integer.valueOf(planId));
					planResponse.setPlanName(planName);
					planResponse.setInsuranceType(insuranceType);
					planResponse.setIssuerLogo(issuerLogo);
					planResponse.setIssuerName(issuerName);
					planResponse.setIssuerId(Integer.valueOf(issuerId));
					planResponseList.add(planResponse);
				} 	

			}catch(Exception e){
				LOGGER.error("Exception occured while fetching plans for Universal Cart",e);
			}finally{
				 // close the entity manager
				 if(entityManager !=null && entityManager.isOpen()){
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
			}
		}
		return planResponseList;	
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public Map<String, String> getMetalTierAvailability (String empPrimaryZip, String ehbCovered, String insuranceType, String effectiveDate, String empCountyCode) {		
		Map<String, String> metalTierAvailMap = new HashMap<String, String>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			metalTierAvailMap = getDefaultMetalTierList(metalTierAvailMap);
			
			ArrayList<String> paramList = new ArrayList<String>();
			StringBuilder buildQuery = new StringBuilder("SELECT phealth.plan_level AS plan_level, count(p.id) AS NO_OF_PALNS, (CASE WHEN count(p.id) > 0 THEN  'YES' ELSE 'NO' END) AS EXIST_FLAG ");
			buildQuery.append("FROM issuers i, plan p, plan_health phealth, pm_service_area psarea ");
			buildQuery.append("WHERE p.id = phealth.plan_id AND p.issuer_id = i.id AND phealth.parent_plan_id = 0 AND p.is_deleted = 'N' ");
			buildQuery.append("AND psarea.is_deleted = 'N' AND p.service_area_id = psarea.service_area_id ");
			buildQuery.append("AND psarea.zip = (:param_" + paramList.size());
			paramList.add(empPrimaryZip);
			
			buildQuery.append(") AND psarea.fips = (:param_" + paramList.size());
			paramList.add(empCountyCode);
			
			buildQuery.append(") AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date ");
			paramList.add(effectiveDate);
			buildQuery.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
			buildQuery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic("p", effectiveDate, PlanMgmtConstants.NO, empPrimaryZip, empCountyCode, null, paramList));
			buildQuery.append(" AND p.market = '");
			buildQuery.append(Plan.PlanMarket.SHOP.toString());
			buildQuery.append("'");		
			buildQuery.append(" AND p.available_for IN ('" + PlanMgmtConstants.ADULT_AND_CHILD + "') ");
			// if we are looking for Essential Health Benefits covered plans
			if (!ehbCovered.equals("")) {
				buildQuery.append(" AND phealth.ehb_covered = :param_" + paramList.size() );
				paramList.add(ehbCovered);
			}
			buildQuery.append(" AND phealth.plan_level != 'CATASTROPHIC' ");
			buildQuery.append(" GROUP BY phealth.plan_level");
	
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug(" SQL : " + buildQuery.toString());
			}
			
			
			Query query = entityManager.createNativeQuery(buildQuery.toString());			
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();		
			
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				String planLevel = objArray[PlanMgmtConstants.ZERO] != null ? objArray[PlanMgmtConstants.ZERO].toString() : PlanMgmtConstants.EMPTY_STRING;
					if((!(planLevel.isEmpty()) && metalTierAvailMap != null) && metalTierAvailMap.containsKey(planLevel) && metalTierAvailMap.get(planLevel) != null){
						String existFlag = objArray[PlanMgmtConstants.TWO] != null ? objArray[PlanMgmtConstants.TWO].toString() : PlanMgmtConstants.NO.toUpperCase();
						metalTierAvailMap.put(planLevel, existFlag);
					}
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("metalTierAvailMap : " + metalTierAvailMap);
			}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getMetalTierAvailability", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}	
		
		return metalTierAvailMap;
	}
	
	private Map<String, String> getDefaultMetalTierList(Map<String, String> temp_metalTierAvailMap){
		Map<String, String> metalTierAvailMap = temp_metalTierAvailMap;
		metalTierAvailMap.put(Plan.PlanLevel.PLATINUM.toString(), PlanMgmtConstants.NO.toUpperCase());
		metalTierAvailMap.put(Plan.PlanLevel.GOLD.toString(), PlanMgmtConstants.NO.toUpperCase());
		metalTierAvailMap.put(Plan.PlanLevel.SILVER.toString(), PlanMgmtConstants.NO.toUpperCase());
		metalTierAvailMap.put(Plan.PlanLevel.BRONZE.toString(), PlanMgmtConstants.NO.toUpperCase());
		//metalTierAvailMap.put(Plan.PlanLevel.CATASTROPHIC.toString(), PlanMgmtConstants.NO.toUpperCase());
		return metalTierAvailMap;
	}
		
	/**
	 * Return year for given date in the format of yyyy-MM-dd
	 * 
	 * @param effectiveDate
	 * @return
	 */
	private Integer getYear(String effectiveDate) {				
		Calendar cal = TSCalendar.getInstance();
		//In case of exception, we will send present year from system date.
		int year = cal.get(Calendar.YEAR); 
		try {
			Date currentDate = DATE_FORMAT_FOR_EFFECTIVE_DATE.parse(effectiveDate); 
			cal.setTime(currentDate);
			year = cal.get(Calendar.YEAR);  
		} catch (Exception e) {			
			LOGGER.error("Exception while parsing plan year date field ", e);
		}		
		return year;
	}
	
	
	// HIX-65070 CrossWalk API 
	@Override
	@Transactional(readOnly = true)
	public PlanCrossWalkResponseDTO getCrossWalkPlanId(String hiosPlanNumber, String zipCode, String countyCode, String effectiveDate, String isEligibleForCatastrophic) {
		PlanCrossWalkResponseDTO planCrossWalkResponse = new PlanCrossWalkResponseDTO();		
		planCrossWalkResponse.setIsAvailable("N");
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			Query query = null;
			String[] effectDateArr = effectiveDate.split("-");
			
			StringBuffer buildQuery = new StringBuffer(1024);
			buildQuery.append("SELECT crs.crosswalk_HIOS_PLAN_ID, crs.IS_CATASTROPHIC_OR_CHILD_ONLY, crs.AGING_OFF_CROSSWALK_PLAN_ID  ");
			buildQuery.append("FROM ");
			buildQuery.append("pm_crosswalk crs, plan p ");
			buildQuery.append("WHERE ");
			buildQuery.append("(crs.CURRENT_HIOS_PLAN_ID=:");
			buildQuery.append(PlanMgmtConstants.HIOS_PLAN_NUMBER);
			buildQuery.append(" OR crs.CURRENT_HIOS_PLAN_ID = substr(:");
			buildQuery.append(PlanMgmtConstants.HIOS_PLAN_NUMBER);
			buildQuery.append(", 1, 14)) ");
			buildQuery.append("AND ");
			buildQuery.append("( (crs.county_code=:");
			buildQuery.append(COUNTYCODE);
			buildQuery.append(" AND crs.zip_list IS NULL AND crs.crosswalk_tier='COUNTY') ");
			buildQuery.append("OR ");
			buildQuery.append("(crs.county_code=:");
			buildQuery.append(COUNTYCODE);
			buildQuery.append(" AND crs.zip_list LIKE :");
			buildQuery.append(ZIPCODE);	
			buildQuery.append(" AND crs.crosswalk_tier='ZIP' ) ");
			buildQuery.append("OR ");
			buildQuery.append("(crs.county_code IS NULL AND crs.zip_list IS NULL AND crs.crosswalk_tier='PLAN' ) ) ");
			buildQuery.append("AND crs.applicable_year=:");
			buildQuery.append(PlanMgmtConstants.EFFECTIVE_YEAR);
			buildQuery.append(" AND crs.crosswalk_HIOS_PLAN_ID=substr(p.issuer_plan_number, 1, 14) ");
			buildQuery.append("AND p.applicable_year=:");
			buildQuery.append(PlanMgmtConstants.EFFECTIVE_YEAR);
			buildQuery.append(" AND p.is_deleted='N' ");
			buildQuery.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
			buildQuery.append(" AND crs.IS_DELETED='N' ");	
			buildQuery.append(" ORDER by crs.id ASC ");
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug(" CROSSWALK QUERY == " +buildQuery.toString());
			}
			query = entityManager.createNativeQuery(buildQuery.toString());
			query.setParameter(PlanMgmtConstants.HIOS_PLAN_NUMBER, hiosPlanNumber);
			query.setParameter(COUNTYCODE, countyCode);
			query.setParameter(ZIPCODE, '%'+zipCode+'%');
			query.setParameter(PlanMgmtConstants.EFFECTIVE_YEAR, Integer.valueOf(effectDateArr[0]));		
		
			// if new cross walk HIOS plan Id exists in cross walk template return new cross walk HIOS plan Id   
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();		
			Object[] objArray = null;
			if(rsList.size() > 0){
				objArray = (Object[]) rsIterator.next();
				String outHiosPlanNumber = PlanMgmtConstants.EMPTY_STRING;
				// HIX-69392 :: 
				/*a. If isEligibleForCatastrophic" == Y, then send value of "F" column
				b. If isEligibleForCatastrophic" == N, and value of column "G" == 'NO', then send value of "F" column of 2016 crosswalk template.
				c. If isEligibleForCatastrophic" == N, and value of column "G" == 'YES', then send value of "H" column of 2016 crosswalk template. " +
				*/		 
				if(PlanMgmtConstants.Y.equalsIgnoreCase(isEligibleForCatastrophic)){
					outHiosPlanNumber = objArray[0].toString();	
				}else if(PlanMgmtConstants.N.equalsIgnoreCase(isEligibleForCatastrophic)){
					if(objArray[1].toString() != null && PlanMgmtConstants.Y.equalsIgnoreCase(objArray[1].toString())){
						outHiosPlanNumber = objArray[2].toString();
					}else{
						outHiosPlanNumber = objArray[0].toString();
					}	
				}
				
				if(StringUtils.isNotBlank(outHiosPlanNumber)){
					planCrossWalkResponse.setIsAvailable(PlanMgmtConstants.Y);
					planCrossWalkResponse.setHiosPlanNumber(outHiosPlanNumber);
				}	
			}else{
				// if given HIOS plan Id does not exists in cross walk template then look plan table for availability
				try{						
					Query planQuery = null;
										
					StringBuffer buildQuery2 = new StringBuffer();
					buildQuery2.append("SELECT substr(p.issuer_plan_number, 1, 14) ");
					buildQuery2.append("FROM ");
					buildQuery2.append("plan p ");
					buildQuery2.append("WHERE ");
					buildQuery2.append("p.issuer_plan_number=:");
					buildQuery2.append(PlanMgmtConstants.HIOS_PLAN_NUMBER);
					buildQuery2.append(" AND p.applicable_year=:");
					buildQuery2.append(PlanMgmtConstants.EFFECTIVE_YEAR);
					buildQuery2.append(" AND p.is_deleted='N' ");
					buildQuery2.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug(" CROSSWALK QUERY IN PLAN TABLE == " +buildQuery2.toString());
					}	
					planQuery = entityManager.createNativeQuery(buildQuery2.toString());
					planQuery.setParameter(PlanMgmtConstants.HIOS_PLAN_NUMBER, hiosPlanNumber);					
					planQuery.setParameter(PlanMgmtConstants.EFFECTIVE_YEAR, Integer.valueOf(effectDateArr[0]));		
					// if new cross walk HIOS plan Id exists in plan table then send HIOS plan number   
					List<?> rsList2 = planQuery.getResultList();		
					
					if(rsList2.size() > 0){
						String outHiosPlanNumber = rsList2.get(0).toString();			
						planCrossWalkResponse.setIsAvailable(PlanMgmtConstants.Y);
						planCrossWalkResponse.setHiosPlanNumber(outHiosPlanNumber);
					}
				}	
				catch (Exception e) {
				       	LOGGER.error("Got an exception in getCrosswalkHiosPlanId(). Exception ==> " + e );           
				}
			}
	   } catch (Exception e) {
       	LOGGER.error("Got an exception in getCrosswalkHiosPlanId(). Exception ==> " + e );           
       }finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
			 LOGGER.info("getCrossWalkPlanId() end with Hios Plan Number: " + planCrossWalkResponse.getHiosPlanNumber());
		}		
		
		return planCrossWalkResponse;
	}
	
		
	private String getPlanInfoByZipQuery(String zip, String countyFIPS, String insuranceType, String exchangeType, String effectiveDate, String tenantCode,
			HashMap<String, Object> queryParams, String medicarePlanDataSource) {
		String query = PlanMgmtConstants.EMPTY_STRING;
		int planYear = 0;
		
		// ## compute plan year start ##
		if (null != effectiveDate && !(PlanMgmtConstants.EMPTY_STRING.equalsIgnoreCase(effectiveDate))) {
			planYear = getYear(effectiveDate);
			queryParams.put(PlanMgmtParamConstants.PARAM_PLAN_YEAR, planYear);
		} else {
			planYear = TSCalendar.getInstance().get(Calendar.YEAR);
			queryParams.put(PlanMgmtParamConstants.PARAM_PLAN_YEAR, planYear);
		}
		// ## compute plan year end ##
		
		queryParams.put(PlanMgmtParamConstants.PARAM_ZIP_CODE, zip);
		if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
			queryParams.put(PlanMgmtParamConstants.PARAM_TENANT_CODE, tenantCode);
		}

		String currentExchangeType = exchangeType.equalsIgnoreCase(PlanMgmtConstants.ON) ? PlanMgmtConstants.ON : PlanMgmtConstants.OFF;
		queryParams.put(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE, currentExchangeType);
		                 

		if (Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(insuranceType)) {
			if (!StringUtils.isEmpty(countyFIPS)) {			
				queryParams.put(PlanMgmtParamConstants.PARAM_COUNTY_CODE, countyFIPS);
				query = PlanMgmtQueryBuilder.getHealthPlanInfoByZipWithCountyFips(tenantCode, configuredDB);
			} else {
				query = PlanMgmtQueryBuilder.getHealthPlanInfoByZipWithoutCountyFips(tenantCode, configuredDB);
			}
		}

		if (Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(insuranceType)) {
			if (!StringUtils.isEmpty(countyFIPS)) {
				queryParams.put(PlanMgmtParamConstants.PARAM_COUNTY_CODE, countyFIPS);
				query = PlanMgmtQueryBuilder.getDentalPlanInfoByZipWithCountyFips(tenantCode, configuredDB);
			} else {
				query = PlanMgmtQueryBuilder.getDentalPlanInfoByZipWithoutCountyFips(tenantCode, configuredDB);
			}
		}

		if (Plan.PlanInsuranceType.STM.toString().equalsIgnoreCase(insuranceType)
				|| Plan.PlanInsuranceType.AME.toString().equalsIgnoreCase(insuranceType)) {
			String statelist = getStateListForZipCode(zip, countyFIPS);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("statelist : " + statelist);
			}
			if (!(statelist.isEmpty())) {
				String[] stateNameArray = statelist.split(PlanMgmtConstants.COMMA);
				ArrayList<String> stateListArray = new ArrayList<String>();
				
				for(String stateNameString : stateNameArray){
					if(StringUtils.isNotEmpty(stateNameString)){
						stateListArray.add(stateNameString);
					}
				}
				
				queryParams.clear();
				
				if(Plan.PlanInsuranceType.STM.toString().equalsIgnoreCase(insuranceType)){
					query = PlanMgmtQueryBuilder.getStmPlanQuery(tenantCode);		
				}
				if(Plan.PlanInsuranceType.AME.toString().equalsIgnoreCase(insuranceType)){
					query = PlanMgmtQueryBuilder.getAmePlanQuery(tenantCode);		
				}
				
				if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
					queryParams.put(PlanMgmtParamConstants.PARAM_TENANT_CODE, tenantCode);
				}
				queryParams.put(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE, currentExchangeType);
				queryParams.put(PlanMgmtParamConstants.PARAM_STATE_LIST, stateListArray);
								
			}
		}
		
		// HIX-97592 :: enhance getPlaninfoByZip API to support Medicare plans
		if (Plan.PlanInsuranceType.MC_ADV.toString().equalsIgnoreCase(insuranceType)
				|| Plan.PlanInsuranceType.MC_RX.toString().equalsIgnoreCase(insuranceType)
				|| Plan.PlanInsuranceType.MC_SUP.toString().equalsIgnoreCase(insuranceType)
				|| "MC_DEN".equalsIgnoreCase(insuranceType)
				|| "MC_VISION".equalsIgnoreCase(insuranceType))
		{
			ZipCode zipCode = zipCodeService.findByZipCode(zip);
			queryParams.clear();
			
			if(null != zipCode){
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug(zip + " belongs to state " + zipCode.getState());
					LOGGER.debug("Medicare plan data source : "+ medicarePlanDataSource);
				}	
				
				if(medicarePlanDataSource.equalsIgnoreCase(PlanMgmtConstants.ALL)){
					query = PlanMgmtQueryBuilder.getMedicarePlanForAllQuery(tenantCode);
				}else{	
					query = PlanMgmtQueryBuilder.getMedicarePlanQuery(tenantCode);
				}
				queryParams.put(PlanMgmtParamConstants.PARAM_APPLICABLE_YEAR, planYear);
				queryParams.put(PlanMgmtParamConstants.PARAM_INSURANCE_TYPE, insuranceType);
				queryParams.put(PlanMgmtParamConstants.PARAM_STATE_CODE, zipCode.getState());
				if(!medicarePlanDataSource.equalsIgnoreCase(PlanMgmtConstants.ALL)){
					queryParams.put(PlanMgmtParamConstants.PARAM_DATA_SOURCE, medicarePlanDataSource);
				}
				
				if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
					queryParams.put(PlanMgmtParamConstants.PARAM_TENANT_CODE, tenantCode);
				}
			}
		}
				
		return query;
	}

	@Override
	@Transactional
	public boolean reapplyBenefitEditsToPlan(Integer planId, StringBuilder uiMessage) {
		Plan activePlan = iPlanRepository.findOne(planId);
		boolean status = false;
		if(null == activePlan){
			//Return error
			LOGGER.error("reapplyBenefitEditsToPlan() No Plan found for plan id " + planId );
			uiMessage.append(PlanMgmtConstants.ERROR_RE_APPLY_BENEFITS).append(PlanMgmtConstants.NO_VALID_PLAN_FOUND);
		} else {
			String issuerPlanNumber = activePlan.getIssuerPlanNumber();
			//Find edited benefits for this plan using hios plan id and applicable year.
			List<PlanBenefitEdit> editedPlanBenefitsList = iPlanBenefitEditRepository.getBenefitsEditedByPlanHiosId(Integer.toString(activePlan.getApplicableYear()), issuerPlanNumber);
			List<PlanCostEdit> editedPlanCostsList = iPlanCostEditRepository.getCostEditedByIssuerPlanNumber(Integer.toString(activePlan.getApplicableYear()), issuerPlanNumber);
			
			if(CollectionUtils.isEmpty(editedPlanBenefitsList) && CollectionUtils.isEmpty(editedPlanCostsList)){
				LOGGER.error("reapplyBenefitEditsToPlan() No benefits or cost edited for plan number " + issuerPlanNumber + " ! Should have found something!");
				uiMessage.append(PlanMgmtConstants.ERROR_RE_APPLY_BENEFITS).append(PlanMgmtConstants.NO_BENEFIT_EDITED);
			} else {
				if(Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(activePlan.getInsuranceType()) && null != activePlan.getPlanHealth()){
					
					LOGGER.debug("reapplyBenefitEditsToPlan() --Health Plan. " + issuerPlanNumber);
					//Get benefits for this health plan.
					List<PlanHealthBenefit> activePlanHealthBenefits = activePlan.getPlanHealth().getPlanHealthBenefitVO();
					//Get deductibles for this health plan.
					List<PlanHealthCost> activePlanHealthCosts = activePlan.getPlanHealth().getPlanHealthCostsList();
					
					if(CollectionUtils.isEmpty(activePlanHealthBenefits)){
						LOGGER.error("reapplyBenefitEditsToPlan() No benefits found for health plan." + issuerPlanNumber + " PHId:" + activePlan.getPlanHealth().getId());
						uiMessage.append(PlanMgmtConstants.ERROR_RE_APPLY_BENEFITS).append(PlanMgmtConstants.BENEFIT_NOT_LOADED);
					} else {
						Map<String, PlanHealthBenefit> activePlanHealthBenefitsMap = new HashMap<String, PlanHealthBenefit>();
						Map<String, PlanHealthCost> activePlanHealthCostsMap = new HashMap<String, PlanHealthCost>();
						for(PlanHealthBenefit planHealthBenefit : activePlanHealthBenefits){
							activePlanHealthBenefitsMap.put(planHealthBenefit.getName(), planHealthBenefit);
						}
						for(PlanHealthCost planHealthCost : activePlanHealthCosts){
							activePlanHealthCostsMap.put(planHealthCost.getName(), planHealthCost);
						}
						status = compareAndApplyPlanHealthBenefitEdits(activePlan, editedPlanBenefitsList, activePlanHealthBenefitsMap, uiMessage);
						if(status) {
							status = applyPlanHealthCostEdits(activePlan, editedPlanCostsList, activePlanHealthCostsMap, uiMessage);
						}
						if(status) {
							refreshPlansInSOLR(PlanMgmtConstants.HEALTH, String.valueOf(activePlan.getId()));
						}
					}
				} else if(Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(activePlan.getInsuranceType()) && null != activePlan.getPlanDental()) {
					LOGGER.debug("reapplyBenefitEditsToPlan() --Dental Plan found.");
					//Get benefits for this dental plan.
					List<PlanDentalBenefit> activePlanDentalBenefits = activePlan.getPlanDental().getPlanDentalBenefitVO();
					List<PlanDentalCost> activePlanDentalCosts = activePlan.getPlanDental().getPlanDentalCostsList();

					if(CollectionUtils.isEmpty(activePlanDentalBenefits)){
						LOGGER.error("reapplyBenefitEditsToPlan() No benefits found for dental plan.");
						uiMessage.append(PlanMgmtConstants.ERROR_RE_APPLY_BENEFITS).append(PlanMgmtConstants.BENEFIT_NOT_LOADED);
					} else {
						Map<String, PlanDentalBenefit> activePlanDentalBenefitsMap = new HashMap<String, PlanDentalBenefit>();
						Map<String, PlanDentalCost> activePlanDentalCostsMap = new HashMap<String, PlanDentalCost>();
						for(PlanDentalBenefit planDentalBenefit : activePlanDentalBenefits){
							activePlanDentalBenefitsMap.put(planDentalBenefit.getName(), planDentalBenefit);
						}
						for(PlanDentalCost planDentalCost : activePlanDentalCosts){
							activePlanDentalCostsMap.put(planDentalCost.getName(), planDentalCost);
						}
						
						status = compareAndApplyPlanDentalBenefitEdits(activePlan, editedPlanBenefitsList, activePlanDentalBenefitsMap, uiMessage);
						if(status) {
							status = applyPlanDentalCostEdits(activePlan, editedPlanCostsList, activePlanDentalCostsMap, uiMessage);
						}
						if(status) {
							refreshPlansInSOLR(PlanMgmtConstants.DENTAL, String.valueOf(activePlan.getId()));
						}
					}
					
				}else{
					LOGGER.error("applyBenefitEditsToPlan() Invalid plan type found : " + activePlan.getInsuranceType());
					uiMessage.append(PlanMgmtConstants.ERROR_RE_APPLY_BENEFITS).append(PlanMgmtConstants.INVALID_PLAN_TYPE);
				}
			}
		}
		return status;
	}
	
	private boolean applyPlanHealthCostEdits(Plan activePlan, List<PlanCostEdit> editedPlanCostsList, Map<String, PlanHealthCost> activePlanHealthCostsMap, StringBuilder uiMessage) {
		boolean costEditsReApplied = false;
		boolean anyCostEditNotApplied = false;

		try{
			
			for(PlanCostEdit editedPlanHealthCost : editedPlanCostsList){
				PlanHealthCost activePlanHealthCost = activePlanHealthCostsMap.get(editedPlanHealthCost.getName());
					
				if(null == activePlanHealthCost) {
					LOGGER.warn("Skipping Cost: " + editedPlanHealthCost.getName() + " as cost not found in reloaded list");
					anyCostEditNotApplied = true;
				} else {						
					LOGGER.debug("applyPlanHealthCostEdits() Match found for cost name : " + editedPlanHealthCost.getName() + " Cost id : " + editedPlanHealthCost.getId());

					activePlanHealthCost.setInNetWorkInd(editedPlanHealthCost.getInNetworkInd());
					activePlanHealthCost.setInNetWorkFly(editedPlanHealthCost.getInNetworkFamily());
					activePlanHealthCost.setOutNetworkInd(editedPlanHealthCost.getOutOfNetworkInd());
					activePlanHealthCost.setOutNetworkFly(editedPlanHealthCost.getOutOfNetworkFamily());
					activePlanHealthCost.setLimitExcepDisplay(editedPlanHealthCost.getLimitExceptionValue());
					iPlanHealthCostRepository.save(activePlanHealthCost);
					costEditsReApplied = (costEditsReApplied || true);
				}
			}
			//If any of the edit is re-applied, update plan-id in PlanCostEdits table, so it is not shown in UI anymore 
			if(costEditsReApplied){
				LOGGER.info("Updating cost edit plan id : " + editedPlanCostsList.get(0).getPlan().getId() + " with plan id : " + activePlan.getId());
				for(PlanCostEdit editedPlanHealthCost : editedPlanCostsList){
					//Update plan id to edited benefit.
					editedPlanHealthCost.setPlan(activePlan);
					iPlanCostEditRepository.save(editedPlanHealthCost);
				}
			}
			//If no error occurred.
			if(anyCostEditNotApplied){ 
				uiMessage.append(PlanMgmtConstants.COST_REAPPLY_COMPLETED_WITH_PARTIAL_SUCCESS);
			} else {
				uiMessage.append(PlanMgmtConstants.COST_REAPPLY_COMPLETED_SUCCESS);
			}
			
		}catch(Exception e){
			LOGGER.error("applyPlanHealthCostEdits() Exception occurred : " + e.getMessage(), e);
			uiMessage.append(PlanMgmtConstants.ERROR_RE_APPLY_COSTS);
			
		}finally{
			LOGGER.info("applyPlanHealthCostEdits() End");
		}
		return costEditsReApplied;
	}

	private boolean applyPlanDentalCostEdits(Plan activePlan, List<PlanCostEdit> editedPlanCostsList, Map<String, PlanDentalCost> activePlanDentalCostsMap, StringBuilder uiMessage) {
		boolean costEditsReApplied = false;
		boolean anyCostEditNotApplied = false;

		try{
			
			for(PlanCostEdit editedPlanDentalCost : editedPlanCostsList){
				PlanDentalCost activePlanDentalCost = activePlanDentalCostsMap.get(editedPlanDentalCost.getName());
					
				if(null == activePlanDentalCost) {
					LOGGER.warn("Skipping Cost: " + editedPlanDentalCost.getName() + " as cost not found in reloaded list");
					anyCostEditNotApplied = true;
				} else {						
					LOGGER.debug("applyPlanDentalCostEdits() Match found for cost name : " + editedPlanDentalCost.getName() + " Cost id : " + editedPlanDentalCost.getId());

					activePlanDentalCost.setInNetWorkInd(editedPlanDentalCost.getInNetworkInd());
					activePlanDentalCost.setInNetWorkFly(editedPlanDentalCost.getInNetworkFamily());
					activePlanDentalCost.setOutNetworkInd(editedPlanDentalCost.getOutOfNetworkInd());
					activePlanDentalCost.setOutNetworkFly(editedPlanDentalCost.getOutOfNetworkFamily());
					activePlanDentalCost.setLimitExcepDisplay(editedPlanDentalCost.getLimitExceptionValue());
					iPlanDentalCostRepository.save(activePlanDentalCost);
					costEditsReApplied = (costEditsReApplied || true);
				}
			}
			//If any of the edit is re-applied, update plan-id in PlanCostEdits table, so it is not shown in UI anymore 
			if(costEditsReApplied){
				LOGGER.info("Updating cost edit plan id : " + editedPlanCostsList.get(0).getPlan().getId() + " with plan id : " + activePlan.getId());
				for(PlanCostEdit editedPlanDentalCost : editedPlanCostsList){
					//Update plan id to edited benefit.
					editedPlanDentalCost.setPlan(activePlan);
					iPlanCostEditRepository.save(editedPlanDentalCost);
				}
			}
			//If no error occurred.
			if(anyCostEditNotApplied){ 
				uiMessage.append(PlanMgmtConstants.COST_REAPPLY_COMPLETED_WITH_PARTIAL_SUCCESS);
			} else {
				uiMessage.append(PlanMgmtConstants.COST_REAPPLY_COMPLETED_SUCCESS);
			}
			
		}catch(Exception e){
			LOGGER.error("applyPlanDentalCostEdits() Exception occurred : " + e.getMessage(), e);
			uiMessage.append(PlanMgmtConstants.ERROR_RE_APPLY_COSTS);
			
		}finally{
			LOGGER.info("applyPlanDentalCostEdits() End");
		}
		return costEditsReApplied;
	}
	
	private void refreshPlansInSOLR(String planType, String planId) {
		try {
			LOGGER.info("Updating Plan to SOLR: PlanID: " + planId + " --- " + GhixEndPoints.PlandisplayEndpoints.UPDATE_PLAN_BENEFITS_AND_COST_IN_SOLR + "/" + planType
	        		+ "/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(planId));
			ghixRestTemplate.exchange(GhixEndPoints.PlandisplayEndpoints.UPDATE_PLAN_BENEFITS_AND_COST_IN_SOLR + "/" + planType
	        		+ "/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(planId), PlanMgmtConstants.EXCHANGE_USER, HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null);
		} catch (Exception ex) {
			LOGGER.error("Failed to update benefits to SOLR for planId: " + planId, ex);
		}
	}
	
	
	/**
	 *  Compares benefit values of edited benefit and applies to newly loaded benefits. 
	 *  
	 * @param activePlan
	 * @param editedPlanBenefits
	 * @param activePlanHealthBenefitsMap
	 * @return
	 */
	private boolean compareAndApplyPlanHealthBenefitEdits(Plan activePlan, List<PlanBenefitEdit> editedPlanBenefits, Map<String, PlanHealthBenefit> activePlanHealthBenefitsMap, StringBuilder uiMessage){
		
		boolean isBenefitEditsReApplied = false;
		boolean isAnyBenefitEditNotApplied = false;

		try{
			
			for(PlanBenefitEdit editedPlanHealthBenefit : editedPlanBenefits){
				PlanHealthBenefit activePlanHealthBenefit = activePlanHealthBenefitsMap.get(editedPlanHealthBenefit.getBenefitName());
					
				if(null == activePlanHealthBenefit) {
					LOGGER.warn("Skipping Benefit: " + editedPlanHealthBenefit.getBenefitName() + " as benefit not found in reloaded list");
					isAnyBenefitEditNotApplied = true;
				} else {						
					LOGGER.debug("compareAndApplyPlanHealthBenefitEdits() Match found for benafit name : " + editedPlanHealthBenefit.getBenefitName() + " Benefit id : " + editedPlanHealthBenefit.getId());
					StringBuilder editsNotAppliedList = new StringBuilder();

					boolean status = updateHealthBenefitText(editedPlanHealthBenefit, activePlanHealthBenefit, PlanMgmtConstants.NetworkType.IN_NETWORK_TIER_1, editsNotAppliedList);
					isBenefitEditsReApplied = (isBenefitEditsReApplied || status);
					isAnyBenefitEditNotApplied = (isAnyBenefitEditNotApplied || !status);

					status = updateHealthBenefitText(editedPlanHealthBenefit, activePlanHealthBenefit, PlanMgmtConstants.NetworkType.IN_NETWORK_TIER_2, editsNotAppliedList);
					isBenefitEditsReApplied = (isBenefitEditsReApplied || status);
					isAnyBenefitEditNotApplied = (isAnyBenefitEditNotApplied || !status);
					
					status = updateHealthBenefitText(editedPlanHealthBenefit, activePlanHealthBenefit, PlanMgmtConstants.NetworkType.OUT_OF_NETWORK, editsNotAppliedList);
					isBenefitEditsReApplied = (isBenefitEditsReApplied || status);
					isAnyBenefitEditNotApplied = (isAnyBenefitEditNotApplied || !status);
					//Create message.
					if(editsNotAppliedList.length() > 0){
						LOGGER.warn(PlanMgmtConstants.BENEFIT_EDIT_UI_MESSAGE + editedPlanHealthBenefit.getBenefitName() + "[" + editsNotAppliedList + "]" );
					}
					//Save benefit.
					activePlanHealthBenefit.setLimitExcepDisplay(editedPlanHealthBenefit.getLimitExceptionValue());
					iPlanHealthBenefitRepository.save(activePlanHealthBenefit);
				}
			}
			//If any of the edit is re-applied, update plan-id in BenefitEdits table, so it is not shown in UI anymore 
			if(isBenefitEditsReApplied){
				LOGGER.info("Updating benefit edit plan id : " + editedPlanBenefits.get(0).getId() + " with plan id : " + activePlan.getId());
				for(PlanBenefitEdit editedPlanHealthBenefit : editedPlanBenefits){
					//Update plan id to edited benefit.
					editedPlanHealthBenefit.setPlan(activePlan);
					iPlanBenefitEditRepository.save(editedPlanHealthBenefit);
				}
			}
			//If no error occurred.
			if(isAnyBenefitEditNotApplied){ 
				uiMessage.append(PlanMgmtConstants.PROCESS_COMPLETED_WITH_PARTIAL_SUCCESS);
			} else {
				uiMessage.append(PlanMgmtConstants.PROCESS_COMPLETED_SUCCESS);
			}
			
		}catch(Exception e){
			LOGGER.error("compareAndApplyPlanHealthBenefitEdits() Exception occurred : " + e.getMessage(), e);
			uiMessage.append(PlanMgmtConstants.ERROR_RE_APPLY_BENEFITS);
			
		}finally{
			LOGGER.info("compareAndApplyPlanHealthBenefitEdits() End");
		}
		return isBenefitEditsReApplied;
	}
	
	
	private boolean updateHealthBenefitText(PlanBenefitEdit editedPlanHealthBenefit, PlanHealthBenefit activePlanHealthBenefit, PlanMgmtConstants.NetworkType networkType, StringBuilder editsNotAppliedList) {
		boolean updateDone = false;
		String editedNetworkValue = null;
		String activeNetworkValue = null;
		switch(networkType) {
			case IN_NETWORK_TIER_1: {
				editedNetworkValue = editedPlanHealthBenefit.getNetworkT1CopayVal() + editedPlanHealthBenefit.getNetworkT1CopayAttr() + PlanMgmtConstants.UNDERSCORE_STRING + editedPlanHealthBenefit.getNetworkT1CoinsuranceVal() + editedPlanHealthBenefit.getNetworkT1CoinsuranceAttr();
				activeNetworkValue = activePlanHealthBenefit.getNetworkT1CopayVal() + activePlanHealthBenefit.getNetworkT1CopayAttr() + PlanMgmtConstants.UNDERSCORE_STRING + activePlanHealthBenefit.getNetworkT1CoinsurVal() + activePlanHealthBenefit.getNetworkT1CoinsurAttr();
				break;
			}
			case IN_NETWORK_TIER_2: {
				editedNetworkValue = editedPlanHealthBenefit.getNetworkT2CopayVal() + editedPlanHealthBenefit.getNetworkT2CopayAttr() + PlanMgmtConstants.UNDERSCORE_STRING + editedPlanHealthBenefit.getNetworkT2CoinsuranceVal() + editedPlanHealthBenefit.getNetworkT2CoinsuranceAttr();
				activeNetworkValue = activePlanHealthBenefit.getNetworkT2CopayVal() + activePlanHealthBenefit.getNetworkT2CopayAttr() + PlanMgmtConstants.UNDERSCORE_STRING + activePlanHealthBenefit.getNetworkT2CoinsurVal() + activePlanHealthBenefit.getNetworkT2CoinsurAttr();
				break;
			}
			case OUT_OF_NETWORK: {
				editedNetworkValue = editedPlanHealthBenefit.getOutNetworkCopayVal() + editedPlanHealthBenefit.getOutNetworkCopayAttr() + PlanMgmtConstants.UNDERSCORE_STRING + editedPlanHealthBenefit.getOutNetworkCoinsuranceVal() + editedPlanHealthBenefit.getOutNetworkCoinsuranceAttr();  
				activeNetworkValue = activePlanHealthBenefit.getOutOfNetworkCopayVal() + activePlanHealthBenefit.getOutOfNetworkCopayAttr() + PlanMgmtConstants.UNDERSCORE_STRING + activePlanHealthBenefit.getOutOfNetworkCoinsurVal() + activePlanHealthBenefit.getOutOfNetworkCoinsurAttr();  
				break;
			}
			
			default: {
				editedNetworkValue = PlanMgmtConstants.EMPTY_STRING;
				break;
			}
		}
		LOGGER.debug("compareAndApplyPlanHealthBenefitEdits() for : " + networkType.toString() + " new : old - " + editedNetworkValue + " : " + activeNetworkValue);
			
		if(editedNetworkValue.equals(activeNetworkValue)) {
			LOGGER.debug("compareAndApplyPlanHealthBenefitEdits() Match found for " + networkType.toString() + " - " + editedPlanHealthBenefit.getBenefitName());	
			switch(networkType) {
				case IN_NETWORK_TIER_1: {
					activePlanHealthBenefit.setNetworkT1display(editedPlanHealthBenefit.getNetworkT1Value());
					activePlanHealthBenefit.setNetworkT1TileDisplay(editedPlanHealthBenefit.getNetworkT1TileValue());
					break;
				}
				case IN_NETWORK_TIER_2: {
				   	activePlanHealthBenefit.setNetworkT2display(editedPlanHealthBenefit.getNetworkT2Value());
					break;
				}
				case OUT_OF_NETWORK: {
				   	activePlanHealthBenefit.setOutOfNetworkDisplay(editedPlanHealthBenefit.getOutOfNetworkValue());
					break;
				}
				default: {
					break;
				}
			}
		   	updateDone = true;
		} else {
			editsNotAppliedList.append(networkType.toString()).append(PlanMgmtConstants.COMMA);
		}
			
		return updateDone;
	}

	/**
	 * Compares benefit values of edited benefit and applies to newly loaded benefits. 
	 * 
	 * @param activePlan
	 * @param benefitsEditedForPlan
	 * @param activePlanDentalBenefitsMap
	 * @return
	 */
	private boolean compareAndApplyPlanDentalBenefitEdits(com.getinsured.hix.model.Plan activePlan,List<PlanBenefitEdit> benefitsEditedForPlan,
			Map<String, PlanDentalBenefit> activePlanDentalBenefitsMap, StringBuilder uiMessage) {

		boolean isBenefitEditsReApplied = false;
		boolean isAnyBenefitEditNotApplied = false;

		try {
			for (PlanBenefitEdit editedDentalPlanBenefit : benefitsEditedForPlan) {
				PlanDentalBenefit activePlanDentalBenefit = activePlanDentalBenefitsMap.get(editedDentalPlanBenefit.getBenefitName());

				if (null == activePlanDentalBenefit) {
					LOGGER.warn("Skipping Benefit: " + editedDentalPlanBenefit.getBenefitName() + " as benefit not found in reloaded list");
					isAnyBenefitEditNotApplied = true;
				} else {
					LOGGER.debug("compareAndApplyPlanDentalBenefitEdits() Match found for benefit name : " + editedDentalPlanBenefit.getBenefitName()
							+ " Benefit id : " + editedDentalPlanBenefit.getId());
					StringBuilder editsNotAppliedList = new StringBuilder();

					boolean status = updateDentalBenefitText(editedDentalPlanBenefit, activePlanDentalBenefit, PlanMgmtConstants.NetworkType.IN_NETWORK_TIER_1, editsNotAppliedList);
					isBenefitEditsReApplied = (isBenefitEditsReApplied || status);
					isAnyBenefitEditNotApplied = (isAnyBenefitEditNotApplied || !status);
					
					status = updateDentalBenefitText(editedDentalPlanBenefit, activePlanDentalBenefit, PlanMgmtConstants.NetworkType.IN_NETWORK_TIER_2, editsNotAppliedList);
					isBenefitEditsReApplied = (isBenefitEditsReApplied || status);
					isAnyBenefitEditNotApplied = (isAnyBenefitEditNotApplied || !status);
					
					status = updateDentalBenefitText(editedDentalPlanBenefit, activePlanDentalBenefit, PlanMgmtConstants.NetworkType.OUT_OF_NETWORK, editsNotAppliedList);
					isBenefitEditsReApplied = (isBenefitEditsReApplied || status);
					isAnyBenefitEditNotApplied = (isAnyBenefitEditNotApplied || !status);
					
					// Create message.
					if (editsNotAppliedList.length() > 0) {
						LOGGER.warn(PlanMgmtConstants.BENEFIT_EDIT_UI_MESSAGE + editedDentalPlanBenefit.getBenefitName() + "[" + editsNotAppliedList + "]" );
					}
					// Save benefit.
					activePlanDentalBenefit.setLimitExcepDisplay(editedDentalPlanBenefit.getLimitExceptionValue());
					iPlanDentalBenefitRepository.save(activePlanDentalBenefit);
				}
			}
			// If any of the edit is re-applied, update plan-id in BenefitEdits
			// table, so it is not shown in UI anymore
			if (isBenefitEditsReApplied) {
				LOGGER.info("Updating benefit edit plan id : " + benefitsEditedForPlan.get(0).getId() + " with plan id : " + activePlan.getId());
				for (PlanBenefitEdit editedPlanBenefit : benefitsEditedForPlan) {
					// Update plan id to edited benefit.
					editedPlanBenefit.setPlan(activePlan);
					iPlanBenefitEditRepository.save(editedPlanBenefit);
				}
			}
			// If no error occurred.
			if (isAnyBenefitEditNotApplied) {
				uiMessage.append(PlanMgmtConstants.PROCESS_COMPLETED_WITH_PARTIAL_SUCCESS);
			} else {
				uiMessage.append(PlanMgmtConstants.PROCESS_COMPLETED_SUCCESS);
			}

		} catch (Exception e) {

			LOGGER.error(
					"compareAndApplyPlanDentalBenefitEdits() Exception occurred : "
							+ e.getMessage(), e);
			uiMessage.append(PlanMgmtConstants.ERROR_RE_APPLY_BENEFITS);

		} finally {
			LOGGER.info("compareAndApplyPlanDentalBenefitEdits() End");
		}

		return isBenefitEditsReApplied;
	}

	private boolean updateDentalBenefitText(
			PlanBenefitEdit editedPlanDentalBenefit,
			PlanDentalBenefit activePlanDentalBenefit, PlanMgmtConstants.NetworkType networkType,
			StringBuilder editsNotAppliedList) {
		boolean updateDone = false;
		String editedNetworkValue = null;
		String activeNetworkValue = null;
		switch (networkType) {
		case IN_NETWORK_TIER_1: {
			editedNetworkValue = editedPlanDentalBenefit.getNetworkT1CopayVal() + editedPlanDentalBenefit.getNetworkT1CopayAttr()
					+ PlanMgmtConstants.UNDERSCORE_STRING + editedPlanDentalBenefit.getNetworkT1CoinsuranceVal() + editedPlanDentalBenefit.getNetworkT1CoinsuranceAttr();
			activeNetworkValue = activePlanDentalBenefit.getNetworkT1CopayVal() + activePlanDentalBenefit.getNetworkT1CopayAttr()
					+ PlanMgmtConstants.UNDERSCORE_STRING + activePlanDentalBenefit.getNetworkT1CoinsurVal() + activePlanDentalBenefit.getNetworkT1CoinsurAttr();
			break;
		}
		case IN_NETWORK_TIER_2: {
			editedNetworkValue = editedPlanDentalBenefit.getNetworkT2CopayVal() + editedPlanDentalBenefit.getNetworkT2CopayAttr()
					+ PlanMgmtConstants.UNDERSCORE_STRING + editedPlanDentalBenefit.getNetworkT2CoinsuranceVal() + editedPlanDentalBenefit.getNetworkT2CoinsuranceAttr();
			activeNetworkValue = activePlanDentalBenefit.getNetworkT2CopayVal()	+ activePlanDentalBenefit.getNetworkT2CopayAttr()
					+ PlanMgmtConstants.UNDERSCORE_STRING	+ activePlanDentalBenefit.getNetworkT2CoinsurVal() + activePlanDentalBenefit.getNetworkT2CoinsurAttr();
			break;
		}
		case OUT_OF_NETWORK: {
			editedNetworkValue = editedPlanDentalBenefit.getOutNetworkCopayVal() + editedPlanDentalBenefit.getOutNetworkCopayAttr()
					+ PlanMgmtConstants.UNDERSCORE_STRING + editedPlanDentalBenefit.getOutNetworkCoinsuranceVal() + editedPlanDentalBenefit.getOutNetworkCoinsuranceAttr();
			activeNetworkValue = activePlanDentalBenefit.getOutOfNetworkCopayVal() + activePlanDentalBenefit.getOutOfNetworkCopayAttr()
					+ PlanMgmtConstants.UNDERSCORE_STRING + activePlanDentalBenefit.getOutOfNetworkCoinsurVal() + activePlanDentalBenefit.getOutOfNetworkCoinsurAttr();
			break;
		}
		default: {
			editedNetworkValue = PlanMgmtConstants.EMPTY_STRING;
			break;
		}
		}
		LOGGER.debug("compareAndApplyPlanHealthBenefitEdits() for : " + networkType.toString() + " new : old - " + editedNetworkValue + " : " + activeNetworkValue);

		if (editedNetworkValue.equals(activeNetworkValue)) {
			LOGGER.debug("compareAndApplyPlanHealthBenefitEdits() Match found for " + networkType.toString() + " - " + editedPlanDentalBenefit.getBenefitName());
			switch (networkType) {
			case IN_NETWORK_TIER_1: {
				activePlanDentalBenefit.setNetworkT1display(editedPlanDentalBenefit.getNetworkT1Value());
				activePlanDentalBenefit.setNetworkT1TileDisplay(editedPlanDentalBenefit.getNetworkT1TileValue());
				break;
			}
			case IN_NETWORK_TIER_2: {
				activePlanDentalBenefit.setNetworkT2display(editedPlanDentalBenefit.getNetworkT2Value());
				break;
			}
			case OUT_OF_NETWORK: {
				activePlanDentalBenefit.setOutOfNetworkDisplay(editedPlanDentalBenefit.getOutOfNetworkValue());
				break;
			}
			default: {
				break;
			}
			}
			updateDone = true;
		} else {
			editsNotAppliedList.append(networkType.toString()).append(PlanMgmtConstants.COMMA);
		}

		return updateDone;
	}
	
	
	/*	 
	 * Pull pediatric plan information
	 *  
	 * @param hiosPlanNumber
	 * @param planYear
	 * 
	 * */
	@Override
	@Transactional(readOnly = true)
	public String getPediatricPlanInfo(String hiosPlanNumber, Integer planYear){
		String planAvailableFor = PlanMgmtConstants.EMPTY_STRING;
		
		List<Plan> plansData = iPlanRepository.getPlanByHIOSID(hiosPlanNumber, planYear);
		if(null != plansData && !(plansData.isEmpty())){
			// If Available_For column in plan table is not NULL 
			if(null != plansData.get(PlanMgmtConstants.ZERO).getAvailableFor()){
				planAvailableFor =  plansData.get(PlanMgmtConstants.ZERO).getAvailableFor().toString();
			}			
		}else{
			planAvailableFor = PlanMgmtConstants.DATA_NOT_AVAILABLE;
		}
		return planAvailableFor;
	}
	
	
	@Override
	public Plan getPlanInfoByHIOSPlanId(String HIOSPlanId, String effectiveDate) {
		Plan planObj = null;

		List<Plan> plansData = iPlanRepository.getPlanByHIOSID(HIOSPlanId,Integer.parseInt(effectiveDate.substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FOUR)));
		if(null != plansData && !(plansData.isEmpty())){
			planObj = plansData.get(PlanMgmtConstants.ZERO);
		}	
		return planObj;
	}
	//Web
	@Override
	public List<String> getDistinctPlanYearsByInsuranceType(String insuranceType){
		List<String> applicableYearList = new ArrayList<>();
		
		applicableYearList = iPlanRepository.getDistinctPlanYearsByInsuranceType(insuranceType);
		return applicableYearList;
	}
	
	@Override
	public List<String> getDistinctPlanYearsByInsuranceTypeAndIssuerId(String insuranceType, int issuerId){
        List<String> applicableYearList = new ArrayList<>();
		
		applicableYearList = iPlanRepository.getDistinctPlanYearsByInsuranceTypeAndIssuerId(insuranceType, issuerId);
		return applicableYearList;
	}
	

	@Override
	public com.getinsured.hix.dto.planmgmt.PlanDTO getPlanDetailsById(int planId) {
		com.getinsured.hix.dto.planmgmt.PlanDTO planDto = null;
		Plan plan = iPlanRepository.findOne(planId);
		if (null != plan) {
			planDto = new com.getinsured.hix.dto.planmgmt.PlanDTO();
			Issuer issuer = plan.getIssuer();
			planDto.setPlanId(planId);
			if (issuer != null) {
				planDto.setHiosIssuerId(plan.getIssuer().getHiosIssuerId());
				planDto.setIssuerId(plan.getIssuer().getId());
				planDto.setIssuerLegalName(plan.getIssuer().getCompanyLegalName());
				planDto.setIssuerName(plan.getIssuer().getName());
				planDto.setIssuerLogoUrl(plan.getIssuer().getLogoURL());
				planDto.setIssuerStatus(plan.getIssuer().getCertificationStatus());
			}
			planDto.setIssuerVerificationStatus(plan.getIssuerVerificationStatus());
			planDto.setIssuerPlanNumber(plan.getIssuerPlanNumber());
			// planDto.setAvailable(plan.get);
			planDto.setPlanName(plan.getName());
			planDto.setPlanState(plan.getState());
			planDto.setPlanStartDate(plan.getStartDate());
			planDto.setPlanEndDate(plan.getEndDate());
			planDto.setPlanMarket(plan.getMarket());
			planDto.setPlanStatus(plan.getStatus());
			planDto.setPlanYear(plan.getApplicableYear());
			planDto.setInsuranceType(plan.getInsuranceType());
			planDto.setHsa(plan.getHsa());
			planDto.setSupportFile(plan.getSupportFile());
			planDto.setBrochureUrl(plan.getBrochure());
			planDto.setNetworkType(plan.getNetworkType());
			if (null != plan.getNetwork()) {
				planDto.setNetworkName(plan.getNetwork().getName());
				planDto.setNetworkUrl(plan.getNetwork().getNetworkURL());
			}
			if (null != plan.getFormularlyId()) {
				Formulary formulary = iFormularyRepository.findById(Integer.parseInt(plan.getFormularlyId()));
				if(null != formulary) {
					if(null != formulary.getDrugList() && "N".equalsIgnoreCase(formulary.getDrugList().getIsDeleted())) {
						planDto.setFormularyId(formulary.getFormularyId());
					} 
					planDto.setFormularyUrl(formulary.getFormularyUrl());
				}
			}
			if(null != plan.getServiceAreaId()) {
				IssuerServiceArea issuerServiceArea = pmIssuerServiceAreaRepository.findById(plan.getServiceAreaId());
				if(null != issuerServiceArea) {
					planDto.setServiceAreaId(issuerServiceArea.getSerffServiceAreaId());
				}
			}
			planDto.setEnrollAvailability(plan.getEnrollmentAvail());
			if(null != plan.getEnrollmentAvailEffDate()) {
				String enrlAvaildate = DATE_FORMAT_FOR_EFFECTIVE_DATE.format(plan.getEnrollmentAvailEffDate());
				planDto.setEnrollAvailEffectiveDate(enrlAvaildate);
			}
			planDto.setNonCommissionFlag(plan.getNonCommissionFlag());
			if (plan.getPlanHealth() != null) {
				planDto.setPlanLevel(plan.getPlanHealth().getPlanLevel());
				planDto.setPlanDesignType(plan.getPlanHealth().getPlanDesignType());
				planDto.setEhbCovered(plan.getPlanHealth().getEhbCovered());
				if(plan.getPlanHealth().getBenefitsUrl() != null) {
					planDto.setSbcDocUrl(plan.getPlanHealth().getBenefitsUrl());
				}
			} else if(plan.getPlanDental() != null) {
				planDto.setPlanLevel(plan.getPlanDental().getPlanLevel());
				planDto.setPlanDesignType(plan.getPlanDental().getPlanDesignType());
				if(plan.getPlanDental().getBenefitsUrl() != null) {
					planDto.setSbcDocUrl(plan.getPlanDental().getBenefitsUrl());
				}
			} else if(plan.getPlanStm() != null) {
				planDto.setExclusionUrl(plan.getPlanStm().getExclusions());
				planDto.setChildRecordId(plan.getPlanStm().getId());
			} else if(plan.getPlanAme() != null) {
				planDto.setExclusionUrl(plan.getPlanAme().getExclusionURL());
				planDto.setChildRecordId(plan.getPlanAme().getId());
			}
		}
		return planDto;
	}
	@Transactional(readOnly = true)
	public PlanSearchResponseDTO getPlans(PlanSearchRequestDTO request) {
	
	//	List<Plan> planList = null;
		List<PlanResponseDTO> planDTOList = new ArrayList<PlanResponseDTO>();
		PlanSearchResponseDTO  planSearchResponseDTO = new PlanSearchResponseDTO();
		

		StringBuilder queryObj;
		StringBuilder queryForCount = new StringBuilder();
		
		Map<String, StringBuilder> map = buildQueryBasedOnFilterCriteria(request);
		queryObj = map.get("queryObj");
		queryForCount = map.get("queryForCount");
		
		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Build query : \n" + queryObj);
		}

		EntityManager entityManager = null;

		try {
			entityManager = emf.createEntityManager();

			List<?> rsList = entityManager.createNativeQuery(queryObj.toString()).getResultList();
			Iterator<?> rsIterator = rsList.iterator();			

			//planList = new ArrayList<Plan>();
			planDTOList = new ArrayList<PlanResponseDTO>();

			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				
				PlanResponseDTO plan = new PlanResponseDTO(); 
				plan.setPlanId(Integer.parseInt(objArray[PlanMgmtConstants.ONE].toString()));
				plan.setIssuerPlanNumber(objArray[PlanMgmtConstants.TWO].toString());
				plan.setPlanName(objArray[PlanMgmtConstants.THREE].toString());

				//Issuer issuer = new Issuer();
				//issuer.setId(Integer.parseInt(objArray[PlanMgmtConstants.FOUR].toString()));
				//issuer.setName(objArray[PlanMgmtConstants.FIVE].toString());
				plan.setIssuerId(Integer.parseInt(objArray[PlanMgmtConstants.FOUR].toString()));
				plan.setIssuerName(objArray[PlanMgmtConstants.FIVE].toString());
			//	plan.setIssuer(issuer);
				
				plan.setPlanMarket(objArray[PlanMgmtConstants.SIX] == null ? "" : String.valueOf(objArray[PlanMgmtConstants.SIX]));

			   Date date = DateUtil.StringToDate((objArray[PlanMgmtConstants.SEVEN].toString()), PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT);
			
				plan.setLastUpdateTimestamp(date);
				plan.setPlanStatus(objArray[PlanMgmtConstants.EIGHT] == null ? "" : String.valueOf(objArray[PlanMgmtConstants.EIGHT]));
				plan.setPlanState(objArray[PlanMgmtConstants.NINE] == null ? "" : String.valueOf(objArray[PlanMgmtConstants.NINE]));
				plan.setIssuerVerificationStatus(objArray[PlanMgmtConstants.TEN].toString());
				plan.setEnrollAvailability(objArray[PlanMgmtConstants.ELEVEN] == null ? "" : String.valueOf(objArray[PlanMgmtConstants.ELEVEN]));
				plan.setNetworkType(objArray[PlanMgmtConstants.TWELVE] == null ? "" : String.valueOf(objArray[PlanMgmtConstants.TWELVE]));
				
				String insuranceType = request.getInsuranceType();
				if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())) {
					//PlanHealth planHealth = new PlanHealth();
					if (objArray[PlanMgmtConstants.THIRTEEN] != null) {
						plan.setPlanLevel(objArray[PlanMgmtConstants.THIRTEEN].toString());
					} else {
						plan.setPlanLevel("");
					}
					//plan.setPlanHealth(planHealth);
				}
				// Apply inner join for plan dental
				if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
					//PlanDental planDental = new PlanDental();
					if (objArray[PlanMgmtConstants.THIRTEEN] != null) {
						plan.setPlanLevel(objArray[PlanMgmtConstants.THIRTEEN].toString());
					} else {
						plan.setPlanLevel("");
					}
					//plan.setPlanDental(planDental);
				}
				
				if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.STM.toString())) {
					if (null != objArray[PlanMgmtConstants.THIRTEEN]) {
						Integer planSTMId = Integer.parseInt(objArray[PlanMgmtConstants.THIRTEEN].toString());
						//PlanSTM planSTM = new PlanSTM();
						//planSTM.setId(planSTMId);
						//plan.setPlanStm(planSTM);
						plan.setStmPlanId(planSTMId);
					}
				}
				
				if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.AME.toString())) {
					if (null != objArray[PlanMgmtConstants.THIRTEEN]) {
						Integer planAMEId = Integer.parseInt(objArray[PlanMgmtConstants.THIRTEEN].toString());
						//PlanAME planAME = new PlanAME();
					//	planAME.setId(planAMEId);
					//	plan.setPlanAme(planAME);
						plan.setAmePlanId(planAMEId);
					}
				}
				
				if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.VISION.toString())) {
					if (null != objArray[PlanMgmtConstants.THIRTEEN]) {
						Integer planVisionId = Integer.parseInt(objArray[PlanMgmtConstants.THIRTEEN].toString());
						//PlanVision planVision = new PlanVision();
						//planVision.setId(planVisionId);
						//plan.setPlanVision(planVision);
						plan.setVisionPlanId(planVisionId.intValue());
					}
				}
				
				if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.MEDICARE.toString())) {
					if (null != objArray[PlanMgmtConstants.THIRTEEN]) {
						Integer planMedicareId = Integer.parseInt(objArray[PlanMgmtConstants.THIRTEEN].toString());
						//PlanMedicare planMedicare = new PlanMedicare();
						//planMedicare.setId(planMedicareId);
						//plan.setPlanMedicare(planMedicare);
					}
				}
				plan.setNonCommissionFlag(objArray[PlanMgmtConstants.FOURTEEN] == null ? "" : String.valueOf(objArray[PlanMgmtConstants.FOURTEEN]));
				planDTOList.add(plan);
			}
			
			if(request.getIssuerId() > 0) {
				Issuer issuer = issuerRepository.findById(request.getIssuerId());
				if(null != issuer) {
					planSearchResponseDTO.setIssuerLogoURL(issuer.getLogoURL());
				}
			} 
			
			int resultCount = 0;
			Object objCount = entityManager.createNativeQuery(queryForCount.toString()).getSingleResult();

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Build Count query : " + queryForCount);
			}

			if (objCount != null) {
				resultCount = Integer.parseInt(objCount.toString());
			}
			
			planSearchResponseDTO.setTotalNumOfPlans(resultCount);
			planSearchResponseDTO.setPlanResponseDTOList(planDTOList);

		} catch (Exception e) {
			LOGGER.error("PlanMgmtService.getPlans - Error While Fetching the Plans : " , e);
		}
		finally
		{
			if(entityManager != null && entityManager.isOpen())
			{
				try {
					entityManager.close();
				}
				catch(IllegalStateException ex)
				{
					LOGGER.error("EntityManager is managed by application server", ex);
					
				}
			}
		}

		return planSearchResponseDTO;
	}
	
	private Map<String,StringBuilder> buildQueryBasedOnFilterCriteria(PlanSearchRequestDTO request){
		
		StringBuilder queryObj = new StringBuilder();
		StringBuilder queryForCount = new StringBuilder();
		
		Map<String, StringBuilder> map = new HashMap<String, StringBuilder>();
		
		
		
		queryObj.append(" SELECT t.* FROM ( ");
		queryObj.append(" SELECT ROWNUM AS rn , t.* FROM (SELECT p.id, p.issuer_plan_number, p.name as planName, p.issuer_id, i.name as issuerName, ");
		
		String insuranceType = request.getInsuranceType();
		queryObj.append(" p.market, to_char(p.last_update_timestamp, 'MM/DD/yyyy'), p.status, p.state, p.issuer_verification_status, p.ENROLLMENT_AVAIL, p.NETWORK_TYPE ");
		if (insuranceType != null && (insuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString()) ||
				insuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString()))) {
			queryObj.append(", childTable.plan_level ");
		}

		if (insuranceType != null && (insuranceType.equalsIgnoreCase(PlanInsuranceType.STM.toString()) || insuranceType.equalsIgnoreCase(PlanInsuranceType.AME.toString()) ||
				insuranceType.equalsIgnoreCase(PlanInsuranceType.VISION.toString()) || insuranceType.equalsIgnoreCase(PlanInsuranceType.MEDICARE.toString()))) {
			queryObj.append(", childTable.id as CHILD_PLAN_ID");
		}

		queryObj.append(", p.non_commission_flag FROM plan p INNER JOIN issuers i ON  p.issuer_id = i.id ");
		queryForCount.append(" SELECT count(*) FROM plan p INNER JOIN issuers i ON  p.issuer_id = i.id ");

		// for plan levels search
		String selectedLevels = "";
		Boolean applyPlanLevel = false;
		
		String selectedPlanLevel = request.getSelectedPlanLevel();
		if (StringUtils.isNotBlank(selectedPlanLevel) && !(selectedPlanLevel.equalsIgnoreCase("DEFAULT"))) {
			applyPlanLevel = true;
		}
		// Apply inner join for plan health
		if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.HEALTH.toString())) {
			queryObj.append(" INNER JOIN plan_health childTable ON p.id = childTable.plan_id ");
			queryForCount.append(" INNER JOIN plan_health childTable ON p.id = childTable.plan_id ");
		}

		// Apply inner join for plan dental
		if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
			queryObj.append(" INNER JOIN plan_dental childTable ON p.id = childTable.plan_id ");
			queryForCount.append(" INNER JOIN plan_dental childTable ON p.id = childTable.plan_id ");
		}

		if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.STM.toString())) {
			queryObj.append(" INNER JOIN plan_stm childTable ON p.id = childTable.plan_id ");
			queryForCount.append(" INNER JOIN plan_stm childTable ON p.id = childTable.plan_id ");
		}

		if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.AME.toString())) {
			queryObj.append(" INNER JOIN plan_ame childTable ON p.id = childTable.plan_id ");
			queryForCount.append(" INNER JOIN plan_ame childTable ON p.id = childTable.plan_id ");
		}

		if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.VISION.toString())) {
			queryObj.append(" INNER JOIN plan_vision childTable ON p.id = childTable.plan_id ");
			queryForCount.append(" INNER JOIN plan_vision childTable ON p.id = childTable.plan_id ");
		}

		if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.MEDICARE.toString())) {
			queryObj.append(" INNER JOIN PLAN_MEDICARE childTable ON p.id = childTable.PLAN_ID ");
			queryForCount.append(" INNER JOIN PLAN_MEDICARE childTable ON p.id = childTable.PLAN_ID ");
		}

		String tenant = request.getTenant();
		if (StringUtils.isNotBlank(tenant) && !tenant.equalsIgnoreCase(PlanMgmtConstants.ANY)) {
				if(!tenant.equalsIgnoreCase("NONE")){
					queryObj.append(" INNER JOIN pm_tenant_plan tp ON p.id = tp.plan_id ");
					queryForCount.append(" INNER JOIN pm_tenant_plan tp ON p.id = tp.plan_id ");
				}
		}

		// where condition starts here
		queryObj.append(" WHERE ");
		queryForCount.append(" WHERE ");

		// if any one/multiple plan levels are selected
		if (applyPlanLevel) {
			queryObj.append(" childTable.plan_level = '" + selectedPlanLevel + "' AND ");
			queryForCount.append(" childTable.plan_level = '" + selectedPlanLevel + "' AND ");
		}

		if (insuranceType != null && insuranceType.equalsIgnoreCase(PlanInsuranceType.DENTAL.toString())) {
			if (applyPlanLevel) {
				queryObj.append(" childTable.plan_level = '" + selectedPlanLevel + "' AND ");
				queryForCount.append(" childTable.plan_level = '" + selectedPlanLevel + "' AND ");
			}
			queryObj.append(" childTable.parent_plan_id = " + 0 + " AND ");
			queryForCount.append(" childTable.parent_plan_id = " + 0 + " AND ");
		}

		// to avoid deleted plans
		queryObj.append(" p.is_deleted='" + Plan.IS_DELETED.N.toString() + "' ");
		queryForCount.append(" p.is_deleted='" + Plan.IS_DELETED.N.toString() + "' ");

		// Search criteria
		if (insuranceType != null && !insuranceType.equals("") && insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.MEDICARE.toString())) {
			queryObj.append(" AND p.insurance_type IN ('MC_ADV','MC_SUP','MC_RX') AND i.hios_issuer_id not like 'DRX_%' ");
			queryForCount.append(" AND p.insurance_type IN ('MC_ADV','MC_SUP','MC_RX') AND i.hios_issuer_id not like 'DRX_%' ");
		}else{
			queryObj.append(" AND p.insurance_type = '" + insuranceType + "' ");
			queryForCount.append(" AND p.insurance_type = '" + insuranceType + "' ");
		}

		String selectedPlanYear = request.getPlanYear();
		if (StringUtils.isNotBlank(selectedPlanYear)) {
			queryObj.append(" AND p.applicable_year = '" + selectedPlanYear + "' ");
			queryForCount.append(" AND p.applicable_year = '" + selectedPlanYear + "' ");
		}

		String issuerName = request.getIssuerName();
		if (StringUtils.isNotBlank(issuerName) && issuerName.equalsIgnoreCase("Select Issuer")){
			queryObj.append(" AND i.name = '" + issuerName + "' ");
			queryForCount.append(" AND i.name = '" + issuerName + "' ");
		}

		int issuerId = request.getIssuerId();
		if (issuerId > 0) {
			queryObj.append(" AND p.issuer_id = " + issuerId + " ");
			queryForCount.append(" AND p.issuer_id = " + issuerId + " ");
		}

		String issuerPlanNumber = request.getIssuerPlanNumber();
		if (StringUtils.isNotBlank(issuerPlanNumber)) {
			queryObj.append(" AND p.issuer_plan_number like '" + issuerPlanNumber + "%' ");
			queryForCount.append(" AND p.issuer_plan_number like '" + issuerPlanNumber + "%' ");
		}

		String market = request.getMarket();
		if (StringUtils.isNotBlank(market) && !(market.equalsIgnoreCase(PlanMgmtConstants.ANY))){
			queryObj.append(" AND p.market = '" + market + "' ");
			queryForCount.append(" AND p.market = '" + market + "' ");
		}

		String status = request.getStatus();
		if ((StringUtils.isNotBlank(status) && !(status.equalsIgnoreCase(PlanMgmtConstants.ANY)))) {
			queryObj.append(" AND p.status = '" + status + "' ");
			queryForCount.append(" AND p.status = '" + status + "' ");
		}

		String verificationStatus = request.getIssuerVerificationStatus();
		if ((StringUtils.isNotBlank(verificationStatus) && !(verificationStatus.equalsIgnoreCase(PlanMgmtConstants.ANY)))) {
			queryObj.append(" AND p.issuer_verification_status = '" + verificationStatus + "' ");
			queryForCount.append(" AND p.issuer_verification_status = '" + verificationStatus + "' ");
		}
		
		String state = request.getState();
		String exchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
		if (GhixConstants.PHIX.equals(exchangeType) &&  (StringUtils.isNotBlank(state) && !(state.equalsIgnoreCase("NA")))) {
				queryObj.append(" AND p.state = '" + state + "' ");
				queryForCount.append(" AND p.state = '" + state + "' ");
		}
		
		String selectedExchangeType = request.getExchangeType();
		if (StringUtils.isNotBlank(selectedExchangeType) &&  !(selectedExchangeType.equalsIgnoreCase(PlanMgmtConstants.ANY))) {
			queryObj.append(" AND p.EXCHANGE_TYPE = '" + selectedExchangeType + "' ");
			queryForCount.append(" AND p.EXCHANGE_TYPE = '" + selectedExchangeType + "' ");
		}
		
		String issuerBrandNameId = request.getIssuerBrandNameId();
		if ((StringUtils.isNotBlank(issuerBrandNameId) && !(issuerBrandNameId.equalsIgnoreCase("-111")))) {
			queryObj.append(" AND i.BRAND_NAME_ID = '" + issuerBrandNameId + "' ");
			queryForCount.append(" AND i.BRAND_NAME_ID = '" + issuerBrandNameId + "' ");
		}
		
		if ((StringUtils.isNotBlank(tenant) && !(tenant.equalsIgnoreCase(PlanMgmtConstants.ANY)))) {
			if(tenant.equalsIgnoreCase("NONE")){
				queryObj.append(" AND p.id NOT IN (SELECT DISTINCT plan_id from PM_TENANT_PLAN tp) ");
				queryForCount.append(" AND p.id NOT IN (SELECT DISTINCT plan_id from PM_TENANT_PLAN tp) ");
			} else{
				queryObj.append(" AND tp.tenant_id IN (select id from tenant where code = '" + tenant + "' ) ");
				queryForCount.append(" AND tp.tenant_id IN (select id from tenant where code = '" + tenant + "' ) ");
			}
		}

		String csrVariation = request.getCsrVariation();
		
		if (StringUtils.isNotBlank(selectedExchangeType) &&  !(selectedExchangeType.equalsIgnoreCase(PlanMgmtConstants.ANY))){
			if(StringUtils.isNotBlank(csrVariation)){
				if(PlanMgmtConstants.ALL.equalsIgnoreCase(csrVariation)){
					queryObj.append(" AND SUBSTR(p.issuer_plan_number, 15, 2) IN ('01','02','03','04','05','06')");
					queryForCount.append(" AND SUBSTR(p.issuer_plan_number, 15, 2) IN ('01','02','03','04','05','06')");
				}else{
					queryObj.append(" AND SUBSTR(p.issuer_plan_number, 15, 2) = '" + csrVariation + "' ");
					queryForCount.append(" AND SUBSTR(p.issuer_plan_number, 15, 2) = '" + csrVariation + "' ");
				}
			}
		}
		
		// when enable for off exchange flow filed submitted
		String enableForOffExchangeFlow = request.getEnableForOffExchangeFlow();
		if (StringUtils.isNotBlank(enableForOffExchangeFlow) && !(enableForOffExchangeFlow.equalsIgnoreCase(PlanMgmtConstants.ANY)) ) {
			queryObj.append(" AND p.enable_for_offexch_flow = '" + enableForOffExchangeFlow + "' ");
			queryForCount.append(" AND p.enable_for_offexch_flow = '" + enableForOffExchangeFlow + "' ");
		}
		
		// when enable for non commission flag selected
		String nonCommissionFlag = request.getNonCommissionFlag();
		if (StringUtils.isNotBlank(nonCommissionFlag)) {
			if (nonCommissionFlag.equalsIgnoreCase("NULL")) {
				queryObj.append(" AND p.non_commission_flag is null ");
				queryForCount.append(" AND p.non_commission_flag is null ");
			}
			else if (nonCommissionFlag.equals(Plan.NON_COMMISSION_FLAG.Y.toString())
				  || nonCommissionFlag.equals(Plan.NON_COMMISSION_FLAG.N.toString())) {
				queryObj.append(" AND p.non_commission_flag = '" + nonCommissionFlag + "' ");
				queryForCount.append(" AND p.non_commission_flag = '" + nonCommissionFlag + "' ");
			}
		}

		String selectedPlanName = request.getSelectedPlanName();
		if (StringUtils.isNotBlank(selectedPlanName)) {
			queryObj.append(" AND UPPER(p.name) LIKE UPPER('%" + selectedPlanName + "%') ");
			queryForCount.append(" AND UPPER(p.name) LIKE UPPER('%" + selectedPlanName + "%') ");
		}

		// Sort criteria
	
		String sortBy = request.getSortBy();
		SortOrder sortOrder = request.getSortOrder();
		
		if (StringUtils.isNotBlank(sortBy)) {
				if (sortBy.equalsIgnoreCase("level")) {
					queryObj.append(" order by childTable.plan_level " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("plannumber")) {
					queryObj.append(" order by p.issuer_plan_number " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("issuer.name")) {
					queryObj.append(" order by i.name " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("submitedon") || sortBy.equalsIgnoreCase("createdOn")) {
					queryObj.append(" order by p.last_update_timestamp " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("certifiedon")) {
					queryObj.append(" order by p.certified_on " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("planmarket")) {
					queryObj.append(" order by p.market " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("planStatus")) {
					queryObj.append(" order by p.status " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("enrollment")) {
					queryObj.append(" order by p.enrollment_avail " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("verified")) {
					queryObj.append(" order by p.issuer_verification_status " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("state")) {
					queryObj.append(" order by p.state " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("name")) {
					queryObj.append(" order by p.name " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("network")) {
					queryObj.append(" order by p.NETWORK_TYPE " + sortOrder);

				} else if (sortBy.equalsIgnoreCase("nonCommissionFlag")) {
					queryObj.append(" order by p.non_commission_flag " + sortOrder);

				} else {
					LOGGER.warn("Plans [type=".intern() + insuranceType + "] Sort order ignored for column ".intern() + sortBy);
				}
		} 
		else {
				queryObj.append(" order by p.last_update_timestamp " + SortOrder.DESC);
		}
		

		queryObj.append(" ) t where rownum <= " + (request.getStartRecord() + request.getPageSize()) + " ) t ");
		queryObj.append(" WHERE rn > " + request.getStartRecord());
		
		map.put("queryObj", queryObj);
		map.put("queryForCount", queryForCount);
		return map;
	}
	
	public GHIXResponse updatePlanDetailsById(PlanRequestDTO planReqDTO) {
		GHIXResponse response = new GHIXResponse();
		boolean allowPartialUpdate = true;
		int passCount = 0, failCount = 0;
		int errorCode = 0;
		String errorMsg = null;
		if(null != planReqDTO && planReqDTO.getPlanId() > 0 && planReqDTO.getUpdatedBy() > 0 ) {
			Plan plan = iPlanRepository.findOne(planReqDTO.getPlanId());
			if (null != plan) {
				if(StringUtils.isNotBlank(planReqDTO.getPlanName())) {
					plan.setName(planReqDTO.getPlanName());
					passCount++;
				}
				if(StringUtils.isNotBlank(planReqDTO.getPlanStatus())) {
					plan.setStatus(planReqDTO.getPlanStatus());
					if (Plan.PlanStatus.CERTIFIED.name().equalsIgnoreCase(planReqDTO.getPlanStatus())) {
						plan.setCertifiedby(String.valueOf(planReqDTO.getUpdatedBy()));
						plan.setCertifiedOn(new TSDate());
					}
					if(planReqDTO.getCommentId() > 0) {
						plan.setCommentId(planReqDTO.getCommentId());
					}else{
						plan.setCommentId(null);
					}
					
					if(StringUtils.isNotBlank(planReqDTO.getSupportFile())) {
						plan.setSupportFile(planReqDTO.getSupportFile());
					}else{
						plan.setSupportFile(null);
					}
					
					passCount++;
				}
				if(StringUtils.isNotBlank(planReqDTO.getIssuerVerificationStatus())) {
					plan.setIssuerVerificationStatus(planReqDTO.getIssuerVerificationStatus());
					passCount++;
				}
				if(StringUtils.isNotBlank(planReqDTO.getNetworkUrl())) {
					plan.getNetwork().setNetworkURL(planReqDTO.getNetworkUrl());
					passCount++;
				}
				if(StringUtils.isNotBlank(planReqDTO.getEnrollAvailability())) {
					plan.setEnrollmentAvail(planReqDTO.getEnrollAvailability());

					if (planReqDTO.getCommentId() > 0) {
						plan.setCommentId(planReqDTO.getCommentId());
					}
					else {
						plan.setCommentId(null);
					}
					passCount++;
				}
				if (StringUtils.isNotBlank(planReqDTO.getEnrollAvailEffectiveDate())) {
					SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

					try {
						Date currentDate = new TSDate();
						Date effDate = dateFormat.parse(planReqDTO.getEnrollAvailEffectiveDate());
						boolean validEnrollmentDate = true;

						if (!DateUtils.isSameDay(currentDate, effDate)) {

							if (currentDate.after(effDate) || (plan.getFutureErlAvailEffDate() != null
									&& !effDate.before(plan.getFutureErlAvailEffDate()))) {
								validEnrollmentDate = false;
							}
						}

						if (validEnrollmentDate) {
							plan.setEnrollmentAvailEffDate(effDate);
							passCount++;
						}
						else {
							allowPartialUpdate = false;
							failCount++;
							LOGGER.error("Mismatched in Current Date or Future Enrollment Date with Enrollment Effective Date.");
						}
					}
					catch (ParseException e) {
						LOGGER.error("Failed to parse EnrollmentEffective Date " + planReqDTO.getEnrollAvailEffectiveDate(), e);
						failCount++;
					}
				}
				if(StringUtils.isNotBlank(planReqDTO.getFormularyUrl())) {
					plan.setFormularlyUrl(planReqDTO.getFormularyUrl());
					passCount++;
				}
				if(StringUtils.isNotBlank(planReqDTO.getSbcDocUrl())) {
					if(null != plan.getPlanHealth()) {
					plan.getPlanHealth().setBenefitsUrl(planReqDTO.getSbcDocUrl());
					} else if(null != plan.getPlanDental()) {
						plan.getPlanDental().setBenefitsUrl(planReqDTO.getSbcDocUrl());
					}
					passCount++;
				}
				if(StringUtils.isNotBlank(planReqDTO.getNonCommissionFlag())) {
					if("N".equalsIgnoreCase(planReqDTO.getNonCommissionFlag()) || "Y".equalsIgnoreCase(planReqDTO.getNonCommissionFlag())) {
						plan.setNonCommissionFlag(planReqDTO.getNonCommissionFlag().toUpperCase());
						passCount++;
					} else {
						failCount++;
						LOGGER.error("Invalid value for NonCommissionFlag: " + planReqDTO.getNonCommissionFlag());
					}
				}

				if (!allowPartialUpdate && failCount > 0) {
					passCount = 0;
				}

				if(passCount > 0) {
					plan.setLastUpdatedBy(planReqDTO.getUpdatedBy());
					iPlanRepository.save(plan);
				}
			}
		}
		if(passCount > 0 && failCount == 0) {
			response.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} else {
			if(passCount > 0 && failCount > 0) {
				response.setStatus(GhixConstants.RESPONSE_PARTIAL_SUCCESS);
				errorMsg = "Failed to update some attributes of Plan";
			} else {
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				if(passCount == 0 && failCount == 0) {
					errorCode = PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode();
					errorMsg = PlanMgmtConstants.INVALID_REQUEST;
				} else {
					errorMsg = "Failed to update Plan";
				}
			}
			response.setErrMsg(errorMsg);
			response.setErrCode(errorCode);
		} 
		return response;
	}

	@Override
	public ServiceAreaDetailsDTO getServiceAreaDetailsByPlanId(int planId) {
		DateFormat inputDate = new SimpleDateFormat("yyyy-mm-DD");
		DateFormat outputDate = new SimpleDateFormat("mm/DD/yyyy");
		
		StringBuilder buildQuery = new StringBuilder();
		buildQuery.append("SELECT P.name, p.issuer_plan_number, p.start_date, p.end_date, pisa.serff_service_area_id, psa.ZIP, psa.county ");
		buildQuery.append("FROM ");
		buildQuery.append("plan p, pm_service_area psa, pm_issuer_service_area pisa");
		buildQuery.append(" WHERE");
		buildQuery.append(" p.service_area_id = psa.service_area_id AND pisa.id =  p.service_area_id AND psa.is_deleted= 'N' AND p.id="+planId);
		
		EntityManager entityManager = null;
		ServiceAreaDetailsDTO serviceArea= new ServiceAreaDetailsDTO();
		try {
			entityManager = emf.createEntityManager();
			List<?> resultList = entityManager.createNativeQuery(buildQuery.toString()).getResultList();
			List<ZipCounty> zipCountyList = new ArrayList<ZipCounty>();
			Object[] objArray = null;
			ZipCounty zipCounty = null;

			if(resultList.size() > PlanMgmtConstants.ZERO)
			{
				for (int i = 0; i < resultList.size(); i++)
				{
					objArray = (Object[]) resultList.get(i);
					if (i == 0)
					{
						try
						{
							zipCounty = new ZipCounty();
							serviceArea.setName((objArray[PlanMgmtConstants.ZERO] != null) ? objArray[PlanMgmtConstants.ZERO].toString() : "");
							serviceArea.setIssuerPlanNumber((objArray[PlanMgmtConstants.ONE] != null) ? objArray[PlanMgmtConstants.ONE].toString() : "");
							String[] startDate = (String[]) ((objArray[PlanMgmtConstants.TWO] != null) ? objArray[PlanMgmtConstants.TWO].toString().split(" ") : "");
							if (StringUtils.isNotBlank(startDate[PlanMgmtConstants.ZERO]))
							{
								Date date = inputDate.parse(startDate[PlanMgmtConstants.ZERO]);
								serviceArea.setStartDate(outputDate.format(date));
							}
							else
							{
								serviceArea.setStartDate("");
							}

							String[] endDate = (String[]) ((objArray[PlanMgmtConstants.THREE] != null) ? objArray[PlanMgmtConstants.THREE].toString().split(" ") : "");
							if (StringUtils.isNotBlank(endDate[PlanMgmtConstants.ZERO]))
							{
								Date date = inputDate.parse(endDate[PlanMgmtConstants.ZERO]);
								serviceArea.setEndDate(outputDate.format(date));
							}
							else
							{
								serviceArea.setEndDate("");
							}
							serviceArea.setSerfServiceAreaId((objArray[PlanMgmtConstants.FOUR] != null) ? objArray[PlanMgmtConstants.FOUR].toString() : "");
							zipCounty.setZipCode((objArray[PlanMgmtConstants.FIVE] != null) ? objArray[PlanMgmtConstants.FIVE].toString() : "0");
							zipCounty.setCounty((objArray[PlanMgmtConstants.SIX] != null) ? objArray[PlanMgmtConstants.SIX].toString() : "");
							zipCountyList.add(zipCounty);
							zipCounty = null;
						}
						catch (Exception e)
						{
							LOGGER.error("Exception Occured while parsing date: ", e);
						}

					}
					else
					{
						zipCounty = new ZipCounty();
						zipCounty.setZipCode((objArray[PlanMgmtConstants.FIVE] != null) ? objArray[PlanMgmtConstants.FIVE].toString() : "0");
						zipCounty.setCounty((objArray[PlanMgmtConstants.SIX] != null) ? objArray[PlanMgmtConstants.SIX].toString() : "");
						zipCountyList.add(zipCounty);
						zipCounty = null;
					}
				}
			}

			serviceArea.setZipCounty(zipCountyList);
			serviceArea.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} catch (Exception e) {
			serviceArea.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		finally
		{
			if(entityManager != null && entityManager.isOpen())
			{
				try {
					entityManager.close();
				}
				catch(IllegalStateException ex)
				{
					LOGGER.error("EntityManager is managed by application server", ex);
				}
			}
		}
		return serviceArea;
	}


	/**
	 * Method is used to get Plan Count By HIOS Issuer Id
	 */
	@Override
	public Long getPlanCountByHiosIssuerId(String hiosIssuerId) {

		try {
			if (StringUtils.isNotBlank(hiosIssuerId)) {
				return iPlanRepository.getPlanCountByHiosIssuerId(hiosIssuerId);
			}
		}
		catch (Exception ex) {
			LOGGER.error("Exceptin Occurred in getPlanCountByHiosIssuerId(): ", ex);
		}
		return null;
	}

	@Override
	public ListOfStringArrayResponseDTO getPlanBenefitsAndCostData(int planId) {
		ListOfStringArrayResponseDTO response = new ListOfStringArrayResponseDTO();
		int errorCode = 0;
		String errorMsg = null;
		if(planId > 0 ) {
			Plan plan = iPlanRepository.findOne(planId);
			if (null != plan) {
				List<String[]> planBenefitsList = null;
				int costCount = 0;
				if (Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(plan.getInsuranceType())) {
					List<PlanHealthBenefit> plansData = plan.getPlanHealth().getPlanHealthBenefitVO();
					List<PlanHealthCost> planHealthCostData = plan.getPlanHealth().getPlanHealthCostsList();
					costCount = planHealthCostData.size();
					String constname;
					String name;
					Map<String, String> healthBenefitMap = new HashMap<String, String>();
					for (PlanmgmtConfiguration.PlanmgmtConfigurationEnum pcE : PlanmgmtConfiguration.PlanmgmtConfigurationEnum.values()) {
						constname = pcE.name().endsWith(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY) ? pcE.name()
								.replace(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY, "") : pcE.name();
						name = pcE.getValue();
						if (name.contains(PlanMgmtConstants.PLANMGMT_HEALTHBENEFITS)) {
							healthBenefitMap.put(constname, DynamicPropertiesUtil.getPropertyValue(name));
						}
					}
					planBenefitsList = PlanHealthMapper.convertObjectListToStringListFromMap(plansData, healthBenefitMap, plan, planHealthCostData);
				}
				if (Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(plan.getInsuranceType())) {
					List<PlanDentalBenefit> plansData = plan.getPlanDental().getPlanDentalBenefitVO();
					List<PlanDentalCost> planDentalCostData = plan.getPlanDental().getPlanDentalCostsList();
					costCount = planDentalCostData.size();
					String constname;
					String name;
					Map<String, String> dentalBenefitMap = new HashMap<String, String>();

					for (PlanmgmtConfiguration.PlanmgmtConfigurationEnum pcE : PlanmgmtConfiguration.PlanmgmtConfigurationEnum.values()) {
						constname = pcE.name().endsWith(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY) ? pcE.name()
								.replace(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY, "") : pcE.name();
						name = pcE.getValue();
						if (name.contains(PlanMgmtConstants.PLANMGMT_DENTALBENEFITS)) {
							dentalBenefitMap.put(constname, DynamicPropertiesUtil.getPropertyValue(name));
						}
					}
					planBenefitsList = PlanHealthMapper.convertsQDPBenefitFromMap(plansData, dentalBenefitMap, plan, planDentalCostData);
				}
				if (planBenefitsList == null) {
					planBenefitsList = PlanHealthMapper.getFileHeader();
				}
				response.setCount(costCount);
				response.setName(plan.getHiosProductId());
				response.setStringArrayList(planBenefitsList);

			} else {
				errorCode = PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode();
				errorMsg = "Invalid Plan ID";
			}
		} else {
			errorCode = PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode();
			errorMsg = PlanMgmtConstants.INVALID_REQUEST;
		}
		response.setErrMsg(errorMsg);
		response.setErrCode(errorCode);
		if(0 == errorCode) {
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("getPlanBenefitsAndCostData completed succesfully for plan id: " + planId);
			}
			response.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} else {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("getPlanBenefitsAndCostData failed for plan id: " + planId + " Error:" + errorMsg);
			}
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return response;
	}

	
	//web

	private String generateNativeWhereClauseForSelectedFilters(PlanSearchRequestDTO request){
		StringBuilder sqlDesigner = new StringBuilder();
			
			int applicableYear = Integer.parseInt(request.getPlanYear());
			int issuerBrandNameId =Integer.parseInt(request.getIssuerBrandNameId());
			
			sqlDesigner.append("WHERE P.IS_DELETED='N' ");
			String expectedInsuranceType = request.getInsuranceType();
			if(StringUtils.isNotEmpty(expectedInsuranceType) && expectedInsuranceType.contains(PlanMgmtConstants.COMMA)){
				sqlDesigner.append("AND P.INSURANCE_TYPE IN (");
				sqlDesigner.append(expectedInsuranceType);
				sqlDesigner.append(")");
			}else{
				sqlDesigner.append("AND P.INSURANCE_TYPE = '");
				sqlDesigner.append(expectedInsuranceType);
				sqlDesigner.append("'");
			}
			sqlDesigner.append(" AND (0 = ");
			sqlDesigner.append(request.getIssuerId());
			sqlDesigner.append(" OR P.ISSUER_ID = ");
			sqlDesigner.append(request.getIssuerId());
			sqlDesigner.append(") AND ('ALL' = '");
			sqlDesigner.append(request.getState());
			sqlDesigner.append("' OR P.STATE = UPPER('");
			sqlDesigner.append(request.getState());
			sqlDesigner.append("')) AND ('ALL' = '");
			sqlDesigner.append(request.getIssuerPlanNumber());
			sqlDesigner.append("' OR UPPER(P.ISSUER_PLAN_NUMBER) LIKE UPPER('");
			sqlDesigner.append(request.getIssuerPlanNumber());
			sqlDesigner.append("%')) AND ('ALL' = '");
			sqlDesigner.append(request.getSelectedPlanName());
			sqlDesigner.append("' OR UPPER(P.NAME) LIKE UPPER('%");
			sqlDesigner.append(request.getSelectedPlanName());
			sqlDesigner.append("%')) AND ('ALL' = '");
			sqlDesigner.append(request.getStatus());
			sqlDesigner.append("' OR P.STATUS = UPPER('"+request.getStatus()+"')) ");
			
			if(Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(request.getInsuranceType())){ // for Health plans
				sqlDesigner.append("AND P.APPLICABLE_YEAR = ");
				sqlDesigner.append(applicableYear);
				sqlDesigner.append(" AND ('ALL' = '");
				sqlDesigner.append(request.getSelectedPlanLevel());
				sqlDesigner.append("' OR P.ID IN (SELECT DISTINCT (PH.PLAN_ID) FROM PLAN_HEALTH PH WHERE PH.PLAN_LEVEL = '");
				sqlDesigner.append(request.getSelectedPlanLevel());
				sqlDesigner.append("')) AND ('ALL' = '");
				sqlDesigner.append(request.getMarket());
				sqlDesigner.append("' OR P.MARKET = UPPER('");
				sqlDesigner.append(request.getMarket());
				sqlDesigner.append("')) ");
				//sqlDesigner.append("AND ('ALL' = '"+status+"' OR P.STATUS = (UPPER('"+status+"'))) ");
				sqlDesigner.append("AND ('ALL' = '");
				sqlDesigner.append(request.getIssuerVerificationStatus());
				sqlDesigner.append("' OR P.VERIFIED = UPPER('");
				sqlDesigner.append(request.getIssuerVerificationStatus());
				sqlDesigner.append("')) AND ('ALL' = '");
				sqlDesigner.append(request.getEnableForOffExchangeFlow());
				sqlDesigner.append("' OR P.enable_for_offexch_flow = UPPER('");
				sqlDesigner.append(request.getEnableForOffExchangeFlow());
				sqlDesigner.append("'))	AND ('ALL' = '");
				sqlDesigner.append(request.getExchangeType());
				sqlDesigner.append("' OR P.EXCHANGE_TYPE = UPPER('");
				sqlDesigner.append(request.getExchangeType());
				sqlDesigner.append("')) AND (0 = ");
				sqlDesigner.append(issuerBrandNameId);
				sqlDesigner.append(" OR P.ISSUER_ID IN (SELECT DISTINCT (I.ID) FROM ISSUERS I WHERE (I.BRAND_NAME_ID = ");
				sqlDesigner.append(issuerBrandNameId);
				sqlDesigner.append("))) ");
				
				//  HIX-80593 when exchange type is ANY chosen then don't use CSR filter 
				if(!PlanMgmtConstants.ALL.equalsIgnoreCase(request.getExchangeType())){
					if(StringUtils.isNotEmpty(request.getCsrVariation()) && PlanMgmtConstants.ALL.equalsIgnoreCase(request.getCsrVariation())){
						sqlDesigner.append("AND SUBSTR(p.issuer_plan_number, 15, 2) IN ('01','02','03','04','05','06')");
					}else if(StringUtils.isNotEmpty(request.getCsrVariation())){
						sqlDesigner.append("AND SUBSTR(p.issuer_plan_number, 15, 2) IN ('");
						sqlDesigner.append(request.getCsrVariation());
						sqlDesigner.append("')");
					}
				}
			}else if(Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(request.getInsuranceType())){ // for Dental plans
				sqlDesigner.append("AND P.APPLICABLE_YEAR = ");
				sqlDesigner.append(applicableYear);
				sqlDesigner.append(" AND ('ALL' = '");
				sqlDesigner.append(request.getSelectedPlanLevel());
				sqlDesigner.append("' OR P.ID IN (SELECT DISTINCT (PD.PLAN_ID) FROM PLAN_DENTAL PD WHERE PD.PLAN_LEVEL = '");
				sqlDesigner.append(request.getSelectedPlanLevel());
				sqlDesigner.append("')) AND ('ALL' = '");
				sqlDesigner.append(request.getMarket());
				sqlDesigner.append("' OR P.MARKET = UPPER('");
				sqlDesigner.append(request.getMarket());
				sqlDesigner.append("')) ");
				//sqlDesigner.append("AND ('ALL' = '"+status+"' OR P.STATUS = (UPPER('"+status+"'))) ");
				sqlDesigner.append("AND ('ALL' = '");
				sqlDesigner.append(request.getIssuerVerificationStatus());
				sqlDesigner.append("' OR P.VERIFIED = UPPER('");
				sqlDesigner.append(request.getIssuerVerificationStatus());
				sqlDesigner.append("')) AND ('ALL' = '");
				sqlDesigner.append(request.getExchangeType());
				sqlDesigner.append("' OR P.EXCHANGE_TYPE = UPPER('");
				sqlDesigner.append(request.getExchangeType());
				sqlDesigner.append("')) AND (0 = ");
				sqlDesigner.append(issuerBrandNameId);
				sqlDesigner.append(" OR P.ISSUER_ID IN (SELECT DISTINCT (I.ID) FROM ISSUERS I WHERE (I.BRAND_NAME_ID = ");
				sqlDesigner.append(issuerBrandNameId);
				sqlDesigner.append("))) ");

			}else if(Plan.PlanInsuranceType.STM.toString().equalsIgnoreCase(request.getInsuranceType()) 
					|| Plan.PlanInsuranceType.AME.toString().equalsIgnoreCase(request.getInsuranceType()) 
					|| Plan.PlanInsuranceType.VISION.toString().equalsIgnoreCase(request.getInsuranceType()) 
					|| Plan.PlanInsuranceType.MEDICARE.toString().equalsIgnoreCase(request.getInsuranceType())){ // for STM/AME/VISION/MEDICARE plans
				sqlDesigner.append("AND ('ALL' = '");
				sqlDesigner.append(request.getSelectedPlanLevel());
				sqlDesigner.append("') ");
			}
			
			sqlDesigner.append("AND ('ALL' = '");
			sqlDesigner.append(request.getTenant());
			sqlDesigner.append("' OR P.ID IN (SELECT DISTINCT(PTP.PLAN_ID) FROM PM_TENANT_PLAN PTP, TENANT T WHERE T.ID = PTP.TENANT_ID AND T.CODE = UPPER('");
			sqlDesigner.append(request.getTenant());
			sqlDesigner.append("')))");
		
		return sqlDesigner.toString();
	}
	
	private String generateWhereClauseForSelectedFilters(PlanSearchRequestDTO request){
		StringBuilder sqlDesigner = new StringBuilder();
			
			int applicableYear = Integer.parseInt(request.getPlanYear());
			int issuerBrandNameId =Integer.parseInt(request.getIssuerBrandNameId());
			
			sqlDesigner.append("WHERE p.isDeleted='N' ");
			String expectedInsuranceType = request.getInsuranceType();
			if(StringUtils.isNotEmpty(expectedInsuranceType) && expectedInsuranceType.contains(PlanMgmtConstants.COMMA)){
				sqlDesigner.append("AND p.insuranceType IN (");
				sqlDesigner.append(expectedInsuranceType);
				sqlDesigner.append(")");
			}else{
				sqlDesigner.append("AND p.insuranceType = '");
				sqlDesigner.append(expectedInsuranceType);
				sqlDesigner.append("'");
			}
			sqlDesigner.append(" AND (0 = ");
			sqlDesigner.append(request.getIssuerId());
			sqlDesigner.append(" OR p.issuer.id = ");
			sqlDesigner.append(request.getIssuerId());
			sqlDesigner.append(") AND ('ALL' = '");
			sqlDesigner.append(request.getState());
			sqlDesigner.append("' OR p.state = UPPER('");
			sqlDesigner.append(request.getState());
			sqlDesigner.append("')) AND ('ALL' = '");
			sqlDesigner.append(request.getIssuerPlanNumber());
			sqlDesigner.append("' OR UPPER(p.issuerPlanNumber) LIKE UPPER('");
			sqlDesigner.append(request.getIssuerPlanNumber());
			sqlDesigner.append("%')) AND ('ALL' = '");
			sqlDesigner.append(request.getSelectedPlanName());
			sqlDesigner.append("' OR UPPER(p.name) LIKE UPPER('%");
			sqlDesigner.append(request.getSelectedPlanName());
			sqlDesigner.append("%')) AND ('ALL' = '");
			sqlDesigner.append(request.getStatus());
			sqlDesigner.append("' OR p.status = UPPER('"+request.getStatus()+"')) ");
			
			if(Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(request.getInsuranceType())){ // for Health plans
				sqlDesigner.append("AND p.applicableYear = ");
				sqlDesigner.append(applicableYear);
				sqlDesigner.append(" AND ('ALL' = '");
				sqlDesigner.append(request.getSelectedPlanLevel());
				sqlDesigner.append("' OR p.planHealth.planLevel = '");
				sqlDesigner.append(request.getSelectedPlanLevel());
				sqlDesigner.append("') AND ('ALL' = '");
				sqlDesigner.append(request.getMarket());
				sqlDesigner.append("' OR p.market = UPPER('");
				sqlDesigner.append(request.getMarket());
				sqlDesigner.append("')) ");
				//sqlDesigner.append("AND ('ALL' = '"+status+"' OR p.STATUS = (UPPER('"+status+"'))) ");
				sqlDesigner.append("AND ('ALL' = '");
				sqlDesigner.append(request.getIssuerVerificationStatus());
				sqlDesigner.append("' OR p.verified = UPPER('");
				sqlDesigner.append(request.getIssuerVerificationStatus());
				sqlDesigner.append("')) AND ('ALL' = '");
				sqlDesigner.append(request.getEnableForOffExchangeFlow());
				sqlDesigner.append("' OR p.enableForOffExchFlow = UPPER('");
				sqlDesigner.append(request.getEnableForOffExchangeFlow());
				sqlDesigner.append("'))	AND ('ALL' = '");
				sqlDesigner.append(request.getExchangeType());
				sqlDesigner.append("' OR p.exchangeType = UPPER('");
				sqlDesigner.append(request.getExchangeType());
				sqlDesigner.append("')) AND (0 = ");
				sqlDesigner.append(issuerBrandNameId);
				sqlDesigner.append(" OR p.issuer.issuerBrandName.id = ");
				sqlDesigner.append(issuerBrandNameId);
				sqlDesigner.append(") ");
				
				//  HIX-80593 when exchange type is ANY chosen then don't use CSR filter 
				if(!PlanMgmtConstants.ALL.equalsIgnoreCase(request.getExchangeType())){
					if(StringUtils.isNotEmpty(request.getCsrVariation()) && PlanMgmtConstants.ALL.equalsIgnoreCase(request.getCsrVariation())){
						sqlDesigner.append("AND SUBSTR(p.issuerPlanNumber, 15, 2) IN ('01','02','03','04','05','06')");
					}else if(StringUtils.isNotEmpty(request.getCsrVariation())){
						sqlDesigner.append("AND SUBSTR(p.issuerPlanNumber, 15, 2) IN ('");
						sqlDesigner.append(request.getCsrVariation());
						sqlDesigner.append("')");
					}
				}
			}else if(Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(request.getInsuranceType())){ // for Dental plans
				sqlDesigner.append("AND p.applicableYear = ");
				sqlDesigner.append(applicableYear);
				sqlDesigner.append(" AND ('ALL' = '");
				sqlDesigner.append(request.getSelectedPlanLevel());
				sqlDesigner.append("' OR p.planDental.planLevel = '");
				sqlDesigner.append(request.getSelectedPlanLevel());
				sqlDesigner.append("') AND ('ALL' = '");
				sqlDesigner.append(request.getMarket());
				sqlDesigner.append("' OR p.market = UPPER('");
				sqlDesigner.append(request.getMarket());
				sqlDesigner.append("')) ");
				//sqlDesigner.append("AND ('ALL' = '"+status+"' OR p.STATUS = (UPPER('"+status+"'))) ");
				sqlDesigner.append("AND ('ALL' = '");
				sqlDesigner.append(request.getIssuerVerificationStatus());
				sqlDesigner.append("' OR p.verified = UPPER('");
				sqlDesigner.append(request.getIssuerVerificationStatus());
				sqlDesigner.append("')) AND ('ALL' = '");
				sqlDesigner.append(request.getExchangeType());
				sqlDesigner.append("' OR p.exchangeType = UPPER('");
				sqlDesigner.append(request.getExchangeType());
				sqlDesigner.append("')) AND (0 = ");
				sqlDesigner.append(issuerBrandNameId);
				sqlDesigner.append(" OR p.issuer.issuerBrandName.id = ");
				sqlDesigner.append(issuerBrandNameId);
				sqlDesigner.append(") ");

			}/*else if(Plan.PlanInsuranceType.STM.toString().equalsIgnoreCase(request.getInsuranceType()) 
					|| Plan.PlanInsuranceType.AME.toString().equalsIgnoreCase(request.getInsuranceType()) 
					|| Plan.PlanInsuranceType.VISION.toString().equalsIgnoreCase(request.getInsuranceType()) 
					|| Plan.PlanInsuranceType.MEDICARE.toString().equalsIgnoreCase(request.getInsuranceType())){ // for STM/AME/VISION/MEDICARE plans
				sqlDesigner.append("AND ('ALL' = '");
				sqlDesigner.append(request.getSelectedPlanLevel());
				sqlDesigner.append("') ");
			}*/
			
			if(!"ALL".equals(request.getTenant())) {
				sqlDesigner.append("AND (");
				sqlDesigner.append(" p.id IN (SELECT DISTINCT(PTP.plan.id) FROM TenantPlan PTP WHERE PTP.tenant.code = UPPER('");
				sqlDesigner.append(request.getTenant());
				sqlDesigner.append("')))");
			}
		
		return sqlDesigner.toString();
	}
	
	@Transactional(readOnly = true)
	private String deleteTenantAssociationForSelectedPlans(String whereClauseForPlans, String tenantsCSV) {
		if(StringUtils.isNotBlank(whereClauseForPlans) &&  StringUtils.isNotBlank(tenantsCSV) ) {
			EntityManager em = null;
			try {
				
				StringBuilder buildquery = new StringBuilder("DELETE FROM PM_TENANT_PLAN WHERE PLAN_ID IN ( SELECT P.ID FROM PLAN P ");
				buildquery.append(whereClauseForPlans);
				buildquery.append(") AND TENANT_ID IN ((SELECT ID FROM TENANT WHERE CODE IN ("+tenantsCSV+")))");
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("deleteTenantDataForFilters : " + buildquery.toString());
				}
				em = emf.createEntityManager();
				if(!em.getTransaction().isActive()){
					em.getTransaction().begin();
				}
				
				Query query = em.createNativeQuery(buildquery.toString());
				int deleteQueryResult = query.executeUpdate();
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("deleteQueryResult: " + deleteQueryResult);
				}
				em.getTransaction().commit();
			} catch (Exception e) {
				if(em.getTransaction().isActive()){
					em.getTransaction().rollback();
				}
				LOGGER.error("Error in deleteAllTenantsDataForFilters() : ", e);
				throw new GIRuntimeException(e);
				//return PlanMgmtConstants.ERROR;
			}
			finally
			{
			  if(em !=null && em.isOpen())
	          {
	            em.clear();
	            
	            try
	            {
	              em.close();
	            }
	            catch (IllegalStateException ex)
	            {
	              LOGGER.error("Unable to close entity manager factory, it's managed by application container", ex);
	            }
	          }
			}
			return PlanMgmtConstants.SUCCESS;
		} else {
            LOGGER.error("deleteTenantAssociationForSelectedPlans: Invalid parameters generatedSQLForTenant: " + whereClauseForPlans + " tenantsCSV: " + tenantsCSV);
			return PlanMgmtConstants.FAILURE;
		}
	}
	

	@Transactional(readOnly = true)
	private String associatePlansWithTenants(String whereClauseForPlans, String tenantsCSV) {
		if(StringUtils.isNotBlank(whereClauseForPlans) &&  StringUtils.isNotBlank(tenantsCSV) ) {
			EntityManager em = null;
			try {
				
				StringBuilder buildquery = new StringBuilder("INSERT INTO pm_tenant_plan(id, tenant_id, created_date, updated_date, plan_id) ");
				buildquery.append("(select PM_TENANT_PLAN_SEQ.nextval, ");
				buildquery.append("(select id from tenant where code in ("+tenantsCSV+")), SYSTIMESTAMP, SYSTIMESTAMP, P.ID FROM PLAN P ");
				buildquery.append(whereClauseForPlans);
				buildquery.append(")");
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("insertTenantData : " + buildquery.toString());
				}
				em = emf.createEntityManager();
				if(!em.getTransaction().isActive()){
					em.getTransaction().begin();
				}
				
				Query query = em.createNativeQuery(buildquery.toString());
				int insertQueryResult = query.executeUpdate();
				//LOGGER.info("insertQueryResult: " + insertQueryResult);
				em.getTransaction().commit();
			} catch (Exception e) {
				LOGGER.error("Error in insertAllTenantsData() : ", e);
				
				if(em.getTransaction().isActive()){
					em.getTransaction().rollback();
				}
				//return PlanMgmtConstants.ERROR;
				throw new GIRuntimeException(e);
			}
			finally{
			  if(em !=null && em.isOpen())
	          {
	            em.clear();
	            
	            try
	            {
	              em.close();
	            }
	            catch (IllegalStateException ex)
	            {
	              LOGGER.error("Unable to close entity manager factory, it's managed by application container", ex);
	            }
	          }
			}
			return PlanMgmtConstants.SUCCESS;
		} else {
	        LOGGER.error("associatePlansWithTenants: Invalid parameters generatedSQLForTenant: " + whereClauseForPlans + " tenantsCSV: " + tenantsCSV);
			return PlanMgmtConstants.FAILURE;
		}
	}
	
	@Override
	public void addTenants(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO){
		String generatedSQLForAddTenants = null;
		if(CollectionUtils.isEmpty(planBulkUpdateRequestDTO.getPlanIdList())) {
			generatedSQLForAddTenants = generateNativeWhereClauseForSelectedFilters(planBulkUpdateRequestDTO.getPlanSearchRequestDTO());
		} else {
			generatedSQLForAddTenants = generateSQLForSelectedPlans(planBulkUpdateRequestDTO.getPlanIdList() );
		}
		String tenantsCSV = getCSVFromList(planBulkUpdateRequestDTO.getSelectedTenantList());
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("generatedSQLForAddTenants : " + generatedSQLForAddTenants);
			LOGGER.debug("tenantsCSV : " + tenantsCSV);
		}
		
		String deleteStatus = deleteTenantAssociationForSelectedPlans(generatedSQLForAddTenants, tenantsCSV);
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("deleteStatus : " + deleteStatus);
		}
		
		String associatePlansStatus = associatePlansWithTenants(generatedSQLForAddTenants, tenantsCSV);
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("associatePlansStatus : " + associatePlansStatus);
		}
		
	}

	private String getCSVFromList(List<String> strList) {
		if(!CollectionUtils.isEmpty(strList)) {
			StringBuffer strBuffer = new StringBuffer();	
			for (String strValue : strList) {
				if(strBuffer.length() > 0) {
					strBuffer.append(",'");	
				} else {
					strBuffer.append("'");
				}
				strBuffer.append(strValue.trim());
				strBuffer.append("'");
			}
			return strBuffer.toString(); 
		}
		return null;
	}
	
	private String generateSQLForSelectedPlans(List<Integer> planIdList) {
		StringBuffer strBuffer = new StringBuffer();
		
		for (Integer planId : planIdList) {
			if(strBuffer.length() > 0) {
				strBuffer.append(",");	
			} 
			strBuffer.append(planId);
		}
		StringBuilder sqlDesigner = new StringBuilder();
		
		//sqlDesigner.append(" P.ID FROM PLAN P ");
		sqlDesigner.append("WHERE ");
		sqlDesigner.append("P.IS_DELETED='N' ");
		sqlDesigner.append("AND P.ID IN (");
		sqlDesigner.append(strBuffer.toString());
		sqlDesigner.append(") ");
		
		return sqlDesigner.toString();
	}

	@Override
	@Transactional
	public void addTenantsForSelectedPlans(PlanTenantRequestDTO request) {

		    List<PlanTenantDTO> planTenantDTOList = request.getPlanTenantList();
		    if (planTenantDTOList == null)
		    	return;
			
			TenantPlan tenantPlan = null;
			Plan plan = new Plan();
			Tenant tenant = new Tenant();
			for (PlanTenantDTO planTenant : planTenantDTOList) {
				plan = iPlanRepository.findOne(planTenant.getPlanId());
				tenant = pmmsITenantRepository.getTenantByCode(request.getSelectedTenants());
				tenantPlan = pmmsITenantPlanRepository.getTenantPlanByTenantIdAndPlanId(tenant.getId(), plan.getId());
				if (null == tenantPlan) {
					tenantPlan = new TenantPlan();
					tenantPlan.setPlan(plan);
					tenantPlan.setTenant(tenant);
					tenantPlan.setCreatedDate(new Timestamp(new TSDate().getTime()));
					tenantPlan.setUpdatedDate(new Timestamp(new TSDate().getTime()));
					pmmsITenantPlanRepository.save(tenantPlan);
				} 

			}
		
	}

	@Override
	public PlanComponentHistoryDTO getPlanRateHistoryByRoleAndPlanId(String roleType, int planId) {
		PlanComponentHistoryDTO planRateHistoryDTO = new PlanComponentHistoryDTO();
		int errorCode = 0;
		String errorMsg = null;
		if(planId > 0) {
			Plan plan = iPlanRepository.findOne(planId);
			if(null != plan) {
				List<Map<String, Object>> planRateHistoryList = getPlanRateHistoryList(planId, roleType);
				planRateHistoryDTO.setHistoryList(planRateHistoryList);
				planRateHistoryDTO.setHiosIssuerId(plan.getIssuer().getHiosIssuerId());
				planRateHistoryDTO.setInsuranceType(plan.getInsuranceType());
				planRateHistoryDTO.setIssuerId(plan.getIssuer().getId());
				planRateHistoryDTO.setIssuerLogoUrl(plan.getIssuer().getLogoURL());
				planRateHistoryDTO.setIssuerName(plan.getIssuer().getName());
				planRateHistoryDTO.setIssuerPlanNumber(plan.getIssuerPlanNumber());
				planRateHistoryDTO.setPlanId(plan.getId());
				planRateHistoryDTO.setPlanName(plan.getName());
				planRateHistoryDTO.setPlanStatus(plan.getStatus());
				planRateHistoryDTO.setPlanYear(plan.getApplicableYear());
				planRateHistoryDTO.setPlanStartDate(plan.getStartDate());
				planRateHistoryDTO.setPlanEndDate(plan.getEndDate());
				planRateHistoryDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				return planRateHistoryDTO;
			}
		} else {
			errorCode = PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode();
			errorMsg = PlanMgmtConstants.INVALID_REQUEST;
		}
		planRateHistoryDTO.setErrMsg(errorMsg);
		planRateHistoryDTO.setErrCode(errorCode);
		planRateHistoryDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
		return planRateHistoryDTO;
	}

	private List<Map<String, Object>> getPlanRateHistoryList(int planId, String role) {
		List<Map<String, Object>> ratesList = new ArrayList<Map<String, Object>>();		

		String buildquery = "SELECT trunc(pr.last_update_timestamp) as update_dt, trunc(pr.effective_start_date) as start_dt, trunc(pr.effective_end_date) as end_dt FROM pm_plan_rate pr WHERE "
				+ "pr.plan_id =:planId "					
				+ " GROUP BY trunc(pr.last_update_timestamp), trunc(pr.effective_start_date), trunc(pr.effective_end_date) ORDER BY trunc(pr.last_update_timestamp)";

		EntityManager entityManager = null;

		try
		{
			entityManager = emf.createEntityManager();

			Query query = entityManager.createNativeQuery(buildquery);
			query.setParameter("planId", planId);
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();

			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Build query " + buildquery);
			}
			StringBuilder downloadLink = null;

			while (rsIterator.hasNext())
			{
				Object[] objArray = (Object[]) rsIterator.next();
				Map<String, Object> rateMap = new HashMap<String, Object>();
				rateMap.put("lastUpdateTimestamp", objArray[0]);
				rateMap.put("rateEffectiveDate", objArray[1]);
				rateMap.put("rateEffectiveEndDate", objArray[2]);
				rateMap.put("userName", "");
				rateMap.put("commentId", "");

				if (PlanMgmtConstants.ADMIN_ROLE.equalsIgnoreCase(role)) {
					downloadLink = new StringBuilder("<a href='/hix/admin/planmgmt/downloadrates/"); 
					downloadLink.append(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(planId)));
				}
				else {
					downloadLink = new StringBuilder("<a href='/hix/issuer/planmgmt/downloadrates/"); 
					downloadLink.append(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(planId)));
				}
				downloadLink.append("?");
				downloadLink.append("effective_start_date=");
				downloadLink.append(StringUtils.replace(objArray[1].toString(), " ", "@"));
				downloadLink.append("&");
				downloadLink.append("effective_end_date=");
				downloadLink.append(StringUtils.replace(objArray[2].toString(), " ", "@"));
				downloadLink.append("'>Download File</a>");
				rateMap.put("downloadLink", downloadLink);
				ratesList.add(rateMap);
			}
		}
		finally
		{
			if(entityManager != null && entityManager.isOpen())
			{
				try {
					entityManager.close();
				}
				catch(IllegalStateException ex)
				{
					LOGGER.error("EntityManager is managed by application server", ex);
				}
			}
		}
		return ratesList;
	}

	@Override
	public PlanRateListResponseDTO getPlanRateList(int planId, String effectiveStartDate, String effectiveEndDate) {
		PlanRateListResponseDTO planRateListDTO = new PlanRateListResponseDTO();
		int errorCode = 0;
		String errorMsg = null;
		if(planId > 0) {
			EntityManager entityManager = null;
			try {
				String buildquery = buildRateListQuery(planId, effectiveStartDate, effectiveEndDate);
				entityManager = emf.createEntityManager();
				List<?> rsList = entityManager.createNativeQuery(buildquery).getResultList();
				Iterator<?> rsIterator = rsList.iterator();
				LOGGER.info("Data is fetched from db result size is " + rsList.size());
				while (rsIterator.hasNext()) {
					Object[] objArray = (Object[]) rsIterator.next();
					PlanRateDTO planRate = new PlanRateDTO();
					planRate.setMaxAge(((BigDecimal) objArray[PlanMgmtConstants.TWO]).intValue());
					planRate.setMinAge(((BigDecimal) objArray[PlanMgmtConstants.ONE]).intValue());
					planRate.setRatingArea((String) objArray[PlanMgmtConstants.ZERO]);
					planRate.setRate((Float) objArray[PlanMgmtConstants.FOUR]);
					planRate.setTabacco(objArray[PlanMgmtConstants.THREE] == null ? "" : String.valueOf(objArray[PlanMgmtConstants.THREE]));
					planRateListDTO.addPlanRate(planRate);
				}
				planRateListDTO.setPlanId(planId);
				planRateListDTO.setTotalRecCount(rsList.size());
			} catch (Exception e) {
				errorCode = PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_PLAN_RATE.getCode();
				errorMsg = "Failed to create Rate List data.";
				LOGGER.error("Failed to create Rate List data. Exception: ", e);
			}
			finally
			{
				if(entityManager != null && entityManager.isOpen())
				{
					try {
						entityManager.close();
					}
					catch(IllegalStateException ex)
					{
						LOGGER.error("EntityManager is managed by application server", ex);
					}
				}
			}
		} else {
			errorCode = PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode();
			errorMsg = PlanMgmtConstants.INVALID_REQUEST;
		}
		planRateListDTO.setErrMsg(errorMsg);
		planRateListDTO.setErrCode(errorCode);
		if(errorCode == 0) {
			planRateListDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} else {
			planRateListDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return planRateListDTO;
	}
	
	
	private String buildRateListQuery(int planId, String effectiveStartDate, String effectiveEndDate) throws ParseException {
		SimpleDateFormat sdfSource = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdfDestination = new SimpleDateFormat("MM/dd/yyyy");
		StringBuilder buildquery = new StringBuilder(); 
		buildquery.append("SELECT pra.rating_area, pr.min_age, pr.max_age, pr.tobacco, pr.rate FROM pm_plan_rate pr,pm_rating_area pra where pra.id=pr.rating_area_id and pr.plan_id=");
		buildquery.append(planId);

		if (!StringUtils.isBlank(effectiveStartDate)) {
			//String[] startDatArr = effectiveStartDate.split("@");
			Date startDate = sdfSource.parse(effectiveStartDate);
			String effectStartDate = sdfDestination.format(startDate);
			buildquery.append(" AND pr.effective_start_date >= to_date('" + effectStartDate + "','MM/dd/yyyy')");
		}

		if (!StringUtils.isBlank(effectiveEndDate)) {
			//String[] endDatArr = effectiveEndDate.split("@");
			Date endDate = sdfSource.parse(effectiveEndDate);
			String effectEndDate = sdfDestination.format(endDate);
			buildquery.append(" AND pr.effective_end_date <= to_date('" + effectEndDate + "','MM/dd/yyyy')");
		}

		buildquery.append("AND pr.is_deleted = 'N'");
		buildquery.append(" GROUP BY pra.rating_area, pr.min_age, pr.max_age, pr.tobacco, pr.rate ORDER BY pra.rating_area");
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("build query : " + buildquery.toString());
		}
		return buildquery.toString();
	}

	/**
	 * Method is used to update Plan Documents Data(Brochure, SBC) in PLAN table.
	 */
	@Override
	public PlanDocumentResponseDTO updatePlanDocuments(PlanDocumentRequestDTO requestDTO) {

		LOGGER.debug("updatePlanDocuments Begin");
		PlanDocumentResponseDTO responseDTO = new PlanDocumentResponseDTO();
		final String loggerData = " for plan with id : ";

		try {

			if (null == requestDTO || CollectionUtils.isEmpty(requestDTO.getPlanIdAndDocumentsMap())) {
				LOGGER.error("Plan Document Request DTO is empty");
				return responseDTO;
			}

			for (String mapKey : requestDTO.getPlanIdAndDocumentsMap().keySet()) {

				String[] planDocumentValues = requestDTO.getPlanIdAndDocumentsMap().get(mapKey);
				LOGGER.debug("Issuer planNumber to Update in database : " + mapKey);

				if (null == planDocumentValues || 0 == planDocumentValues.length) {
					responseDTO.setFailedPlanIdsAndError(mapKey, "Plan Document data is empty.");
					continue;
				}
				// Update Plan Documents Data(Brochure, SBC) in PLAN table
				updatePlanDocumentsInDB(loggerData, planDocumentValues, mapKey, responseDTO);
			}
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while updating Plan Documents: " + ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("updatePlanDocuments End");
		}
		return responseDTO;
	}

	/**
	 * Method is used to update Plan Documents Data(Brochure, SBC) in PLAN table.
	 */
	private void updatePlanDocumentsInDB(final String loggerData, String[] planDocumentValues, String mapKey, 
			PlanDocumentResponseDTO responseDTO) {

		LOGGER.debug("updatePlanDocumentsInDB Begin");

		try {

			List<Plan> planList = iPlanRepository.findByIsDeletedAndIssuerPlanNumberStartingWithAndApplicableYear(Plan.IS_DELETED.N.toString(), planDocumentValues[POS_PLAN_ISSUER_NUM] , Integer.parseInt(planDocumentValues[POS_APPLICABLE_YEAR]));
			LOGGER.debug("Fetched " +  ((null != planList) ? planList.size() : null) + " plans for id " + mapKey);

			if (CollectionUtils.isEmpty(planList)) {
				responseDTO.setFailedPlanIdsAndError(mapKey, "Document entry not found in the database.");
				return;
			}

			for (Plan plan : planList) {

				if (null != planDocumentValues[POS_BROCHURE_ECM] && null != planDocumentValues[POS_BROCHURE_FILE]) {
					plan.setBrochureUCmId(planDocumentValues[POS_BROCHURE_ECM]);
					LOGGER.debug("Setting brochure id " + planDocumentValues[POS_BROCHURE_ECM] + loggerData + plan.getId());
					plan.setBrochureDocName(planDocumentValues[POS_BROCHURE_FILE]);
					LOGGER.debug("Setting brochure name " + planDocumentValues[POS_BROCHURE_FILE] + loggerData + plan.getId());
				}

				// Null check for plan health object.
				if (null != planDocumentValues[POS_SBC_ECM] && null != planDocumentValues[POS_SBC_FILE]) {

					if (null != plan.getPlanHealth()) {
						plan.getPlanHealth().setSbcUcmId(planDocumentValues[POS_SBC_ECM]);
						LOGGER.debug("Setting sbc id for plan health " + planDocumentValues[POS_SBC_ECM] + loggerData + plan.getId());
						plan.getPlanHealth().setSbcDocName(planDocumentValues[POS_SBC_FILE]);
						LOGGER.debug("Setting sbc name for plan health " + planDocumentValues[POS_SBC_FILE] + loggerData + plan.getId());
					}
					else if(null != plan.getPlanDental()) {
						plan.getPlanDental().setSbcUcmId(planDocumentValues[POS_SBC_ECM]);
						LOGGER.debug("Setting sbc id for plan dental " + planDocumentValues[POS_SBC_ECM] + loggerData + plan.getId());
						plan.getPlanDental().setSbcDocName(planDocumentValues[POS_SBC_FILE]);
						LOGGER.debug("Setting sbc name for plan dental " + planDocumentValues[POS_SBC_FILE] + loggerData + plan.getId());
					}
					else {
						LOGGER.debug("No matching health or dental plan found for " + mapKey);
					}
				}
			}
			// Save whole updated list.
			iPlanRepository.save(planList);
		}
		catch (ArrayIndexOutOfBoundsException ex) {
			responseDTO.setFailedPlanIdsAndError(mapKey, "Mismatched data in Array Index: " + ex.getMessage());
			LOGGER.error(responseDTO.getFailedPlanIdsAndErrorMap().get(mapKey), ex);
		}
		catch (Exception ex) {
			LOGGER.error("Error occurred while updating plan. Adding to failed plan of MAP-KEY: " + mapKey, ex);
			responseDTO.setFailedPlanIdsAndError(mapKey, ex.getMessage());
		}
		finally {
			LOGGER.debug("updatePlanDocumentsInDB End");
		}
	}
	
	

	@Override
	public List<Object[]> getPlanIdsByHiosIds(List<String> hiosPlanIdList, String coverageStartDate, String insuranceType){
		List<Object[]> PlanIdAndHiosPlanIDList = null;
		
		long t1 = TimeShifterUtil.currentTimeMillis();
		
		try
		{
			Integer planYear = PlanMgmtUtil.getCurrentYear(); 
			if(StringUtils.isNumeric(coverageStartDate.substring(0, 4))){
				planYear = Integer.parseInt(coverageStartDate.substring(0, 4));
			}
			long s1 = TimeShifterUtil.currentTimeMillis();
			PlanIdAndHiosPlanIDList = iPlanRepository.getPlanIdsAndNetworkNamesByHiosPlanIds(hiosPlanIdList, planYear, insuranceType);
			long s2 = TimeShifterUtil.currentTimeMillis();
			LOGGER.info("time taken for getting the plan ids for hiosId in getPlanIdsByHiosIds ==> " + (s2-s1));
		}
		catch(Exception ex){
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute getPlanIdsByHiosIds ", ex);
			LOGGER.error("Failed to execute getPlanIdsByHiosIds. Exception: ", ex);
		}
		long t2 = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("time taken for getting the plan ids for hiosId in getPlanIdsByHiosIds ==> " + (t2-t1));
		
		
		return PlanIdAndHiosPlanIDList;
	}
	
	@Override
	public List<Object[]> getPlanIdsAndNetowrkNamesByHiosIds(List<String> hiosPlanIdList, String coverageStartDate, String insuranceType){
		List<Object[]> PlanIdAndHiosPlanIDList = null;
		
		long t1 = TimeShifterUtil.currentTimeMillis();
		
		try
		{
			Integer planYear = PlanMgmtUtil.getCurrentYear(); 
			if(StringUtils.isNumeric(coverageStartDate.substring(0, 4))){
				planYear = Integer.parseInt(coverageStartDate.substring(0, 4));
			}
			long s1 = TimeShifterUtil.currentTimeMillis();
			PlanIdAndHiosPlanIDList = iPlanRepository.getPlanIdsAndNetworkNamesByHiosPlanIds(hiosPlanIdList, planYear, insuranceType);
			long s2 = TimeShifterUtil.currentTimeMillis();
			LOGGER.info("time taken for getting the plan ids for hiosId in getPlanIdsByHiosIds ==> " + (s2-s1));
		}
		catch(Exception ex){
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute getPlanIdsByHiosIds ", ex);
			LOGGER.error("Failed to execute getPlanIdsByHiosIds. Exception: ", ex);
		}
		long t2 = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("time taken for getting the plan ids for hiosId in getPlanIdsByHiosIds ==> " + (t2-t1));
		
		
		return PlanIdAndHiosPlanIDList;
	}
	
	@Override
	public Map<String, SbcScenarioDTO> getSbcScenarioIdsByHiosPlanIds(List<String> hiosPlanIdList, Integer applicableYear){
		List<Object[]> sbcScenarioIdObjectList = null;
		Map<String, SbcScenarioDTO> sbcScenario = new HashMap<String, SbcScenarioDTO>();
		
		try
		{
			sbcScenarioIdObjectList = iPlanHealthRepository.getSbcScenarioIdsByHiosPlanIds(hiosPlanIdList, applicableYear);
			PlanSbcScenario planSbcScenario = null;
			SbcScenarioDTO sbcScenarioDTO = null;
			for(Object[] sbcScenarioIdObject : sbcScenarioIdObjectList){
				planSbcScenario =  (PlanSbcScenario) sbcScenarioIdObject[PlanMgmtConstants.ZERO];
				sbcScenario.put(sbcScenarioIdObject[PlanMgmtConstants.ONE].toString(), planSbcScenarioService.setSbcScenario(planSbcScenario, sbcScenarioDTO));
			}
		}
		catch(Exception ex){
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute getSbcScenarioIdsByHiosPlanIds() ", ex);
			LOGGER.error("Failed to execute getSbcScenarioIdsByHiosPlanIds(). Exception: ", ex);
		}
		
		return sbcScenario;
	}
	
	@Override
	public void removeTenants(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO){
		String generatedSQLForRemoveTenants = null;
		if(CollectionUtils.isEmpty(planBulkUpdateRequestDTO.getPlanIdList())) {
			generatedSQLForRemoveTenants = generateNativeWhereClauseForSelectedFilters(planBulkUpdateRequestDTO.getPlanSearchRequestDTO());
		} else {
			generatedSQLForRemoveTenants = generateSQLForSelectedPlans(planBulkUpdateRequestDTO.getPlanIdList() );
		}
		String tenantsCSV = getCSVFromList(planBulkUpdateRequestDTO.getSelectedTenantList());

		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("generatedSQLForAddTenants : " + generatedSQLForRemoveTenants);
		}

		String deleteTenantStatus = deleteTenantAssociationForSelectedPlans(generatedSQLForRemoveTenants, tenantsCSV);
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("deleteTenantStatus : " + deleteTenantStatus);
		}
	}
	
	@Override
	@Transactional(readOnly = true)
	public void removeTenantsForSelectedPlans(PlanTenantRequestDTO request) {
		EntityManager em = null;
		try {
			   List<Integer> planIdList = request.getPlanIdList();
			   String selectedTenants = request.getSelectedTenants();
			  
			    
				 if (planIdList == null || selectedTenants == null)
				    	return;
				
				StringBuilder buildquery = new StringBuilder("delete from PM_TENANT_PLAN where tenant_id in (select id from tenant where code in (:tenantCodes)) and plan_id in (:planIds)");
			
				em = emf.createEntityManager();
				if(!em.getTransaction().isActive()){
					em.getTransaction().begin();
				}
				Query query = em.createNativeQuery(buildquery.toString());
				query.setParameter("tenantCodes", selectedTenants);
				query.setParameter("planIds",planIdList);
				int queryResult = query.executeUpdate();
				em.getTransaction().commit();
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("queryResult : " + queryResult);
				}
		} catch (Exception e) {
			LOGGER.error("removeTenantsForSelectedPlans:Failed to delete from PM_TENANT_PLAN table ",e);
			if(em.getTransaction().isActive()){
				em.getTransaction().rollback();
			}
			throw new GIRuntimeException(e);
		}
		finally{
			if(em !=null && em.isOpen())
			{
			  em.clear();
              
			  try
              {
                em.close();
              }
              catch (IllegalStateException ex)
              {
                LOGGER.error("Unable to close entity manager factory, it's managed by application container", ex);
              }
			}
		}
		return;
	}

	/**
	 * Method is used to modify Medicare Plan Status in Bulk or selected plans.
	 */
	@Override
	public boolean modifyMedicarePlanStatus(PlanBulkUpdateRequestDTO requestDTO) {

		LOGGER.debug("modifyMedicarePlanStatus() Start");
		boolean updateStatus = false;

		try {

			if (null == requestDTO || null == requestDTO.getPlanSearchRequestDTO()) {
				LOGGER.error("Invalid request parameters");
				return updateStatus;
			}

			String filterIssuerPlanNumber = "ALL";
			String filterState = "ALL";
			String filterStatus = "ALL";
			int filterIssuerId = 0;
			long filterTenantId = 0l;

			int updatedCount = 0;

			if (CollectionUtils.isEmpty(requestDTO.getPlanIdList())) {

				if (org.apache.commons.lang3.StringUtils.isNotBlank(requestDTO.getPlanSearchRequestDTO().getIssuerPlanNumber())) {
					filterIssuerPlanNumber = requestDTO.getPlanSearchRequestDTO().getIssuerPlanNumber();
				}

				if (org.apache.commons.lang3.StringUtils.isNotBlank(requestDTO.getPlanSearchRequestDTO().getState())) {
					filterState = requestDTO.getPlanSearchRequestDTO().getState();
				}

				if (org.apache.commons.lang3.StringUtils.isNotBlank(requestDTO.getPlanSearchRequestDTO().getStatus())) {
					filterStatus = requestDTO.getPlanSearchRequestDTO().getStatus();
				}

				if (0 < requestDTO.getPlanSearchRequestDTO().getIssuerId()) {
					filterIssuerId = requestDTO.getPlanSearchRequestDTO().getIssuerId();
				}

				if (org.apache.commons.lang3.StringUtils.isNumeric(requestDTO.getPlanSearchRequestDTO().getTenant())) {
					filterTenantId = Long.valueOf(requestDTO.getPlanSearchRequestDTO().getTenant());
				}

				updatedCount = iPlanRepository.modifyMedicarePlanStatusInBulk(requestDTO.getSelectedPlanStatus(),
						requestDTO.getSelectedIssuerVerificationStatus(), requestDTO.getLastUpdatedBy(), filterIssuerPlanNumber,
						filterState, filterStatus, filterIssuerId, filterTenantId);
			}
			else {
				updatedCount = iPlanRepository.modifyMedicarePlansForSelectedPlans(requestDTO.getSelectedPlanStatus(),
						requestDTO.getSelectedIssuerVerificationStatus(), requestDTO.getLastUpdatedBy(), requestDTO.getPlanIdList());
			}

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Bulk updating Medicare Plan status complete, total updated records : " + updatedCount);
			}

			if (0 < updatedCount) {
				updateStatus = true;
			}
		}
		finally {
			LOGGER.debug("modifyMedicarePlanStatus() End");
		}
		return updateStatus;
	}

	@Override
	public boolean updatePlanStatus(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO) {

		return updatePlanFields(planBulkUpdateRequestDTO, UPDATE_PLAN_FIELD.PLAN_STATUS);
	}

	/**
	 * Method is used to update Non-Commission Flag for Plans
	 */
	@Override
	public boolean updateNonCommissionFlagForPlans(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO) {

		return updatePlanFields(planBulkUpdateRequestDTO, UPDATE_PLAN_FIELD.NON_COMMISSION_FLAG);
	}

	private boolean updatePlanFields(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO, UPDATE_PLAN_FIELD updatePlanField) {

		boolean updateStatus = false;
		if (!validatePlansFieldData(planBulkUpdateRequestDTO, updatePlanField)) {
			return updateStatus;
		}

		EntityManager em = null;

		try {

			em = emf.createEntityManager();

			List<Plan> planList = getPlansForBulkUpdate(planBulkUpdateRequestDTO, em, false);

			if (null != planList) {

				Date lastUpdated = new TSDate();
				boolean updateEnrollAvail = false;

				if (UPDATE_PLAN_FIELD.PLAN_STATUS.equals(updatePlanField)
						&& StringUtils.isNotBlank(planBulkUpdateRequestDTO.getSelectedEnrollAvailability())) {
					updateEnrollAvail = true;
				}

				if (!em.getTransaction().isActive()) {
					em.getTransaction().begin();
				}

				for (Plan plan : planList) {

					if (UPDATE_PLAN_FIELD.NON_COMMISSION_FLAG.equals(updatePlanField)) {
						plan.setNonCommissionFlag(planBulkUpdateRequestDTO.getSelectedPlanStatus());
					}
					else if (UPDATE_PLAN_FIELD.IS_DELETED.equals(updatePlanField)) {
						plan.setIsDeleted(Plan.IS_DELETED.Y.toString());
					}
					else if (UPDATE_PLAN_FIELD.PLAN_STATUS.equals(updatePlanField)) {
						plan.setIssuerVerificationStatus(planBulkUpdateRequestDTO.getSelectedIssuerVerificationStatus());
						plan.setStatus(planBulkUpdateRequestDTO.getSelectedPlanStatus());

						if (updateEnrollAvail) {
							plan.setEnrollmentAvail(planBulkUpdateRequestDTO.getSelectedEnrollAvailability());
						}

						if (PlanMgmtConstants.CERTIFIED.equalsIgnoreCase(planBulkUpdateRequestDTO.getSelectedPlanStatus())) {
							plan.setCertifiedby(String.valueOf(planBulkUpdateRequestDTO.getLastUpdatedBy()));
							plan.setCertifiedOn(lastUpdated);
						}
					}
					plan.setLastUpdatedBy(planBulkUpdateRequestDTO.getLastUpdatedBy());
					plan.setLastUpdateTimestamp(lastUpdated);
					em.merge(plan);
				}

				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Plan Count of updated Non-Commission Flag: " + planList.size());
				}
				em.getTransaction().commit();
				updateStatus = true;
			}
			else {
				LOGGER.warn("There is no plan selected for update.");
			}
		}
		catch (Exception e) {
			LOGGER.error("Error in updating Non-Commission Flag for Plans: ", e);

			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			throw new GIRuntimeException(e);
		}
		finally {

			if (em != null && em.isOpen()) {
				em.clear();

				try {
					em.close();
				}
				catch (IllegalStateException ex) {
					LOGGER.error("Unable to close entity manager factory, it's managed by application container", ex);
				}
			}
		}
		return updateStatus;
	}

	private boolean validatePlansFieldData(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO, UPDATE_PLAN_FIELD updatePlanField) {

		boolean hasValidData = false;

		if (null == updatePlanField) {
			LOGGER.error("Please define which field to update.");
			return hasValidData;
		}

		if (null == planBulkUpdateRequestDTO) {
			LOGGER.error("Request DTO for bulk update is empty.");
			hasValidData = false;
		}
		else if (UPDATE_PLAN_FIELD.NON_COMMISSION_FLAG.equals(updatePlanField)
				&& StringUtils.isBlank(planBulkUpdateRequestDTO.getSelectedPlanStatus())) {
			LOGGER.error("Request parameters should not be empty for updating Non-Commission Flag for Plans");
			hasValidData = false;
		}
		else if (UPDATE_PLAN_FIELD.PLAN_STATUS.equals(updatePlanField)
				&& (StringUtils.isBlank(planBulkUpdateRequestDTO.getSelectedIssuerVerificationStatus())
					|| StringUtils.isBlank(planBulkUpdateRequestDTO.getSelectedPlanStatus()))) {
			LOGGER.error("Request parameters should not be empty for updating Plans status");
			hasValidData = false;
		}
		else {
			hasValidData = true;
		}
		return hasValidData;
	}

	private List<Plan> getPlansForBulkUpdate(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO, EntityManager em, boolean forAllIssuer) {
		String whereClauseForPlans = null;
		StringBuilder buildquery = null;
		if (CollectionUtils.isEmpty(planBulkUpdateRequestDTO.getPlanIdList())) {
			whereClauseForPlans = generateWhereClauseForSelectedFilters(planBulkUpdateRequestDTO.getPlanSearchRequestDTO());
		} else {
			whereClauseForPlans = " WHERE p.id IN (" + getPlanIdCSVFromList(planBulkUpdateRequestDTO.getPlanIdList()) + ")";
		}
		buildquery = new StringBuilder("SELECT p FROM Plan p ");
		buildquery.append(whereClauseForPlans);
		if(!forAllIssuer) {
			buildquery.append(" AND (p.issuer.id IN (select id from Issuer i where i.certificationStatus IN ('CERTIFIED', 'RECERTIFIED'))) ");
		}
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("getPlansForBulkUpdate : " + buildquery.toString());
		}
		Query query = em.createQuery(buildquery.toString());
		return query.getResultList();
	}

	@Override
	public boolean softDeletePlans(PlanBulkUpdateRequestDTO planBulkUpdateRequestDTO) {

		return updatePlanFields(planBulkUpdateRequestDTO, UPDATE_PLAN_FIELD.IS_DELETED);
	}
	
	private String getPlanIdCSVFromList(List<Integer> planIdList) {
		if(!CollectionUtils.isEmpty(planIdList)) {
			StringBuffer strBuffer = new StringBuffer();	
			for (Integer planId : planIdList) {
				if(strBuffer.length() > 0) {
					strBuffer.append(",");	
				} 
				strBuffer.append(planId);
			}
			return strBuffer.toString(); 
		}
		return null;
	}
	
	@Override
	public int bulkEnableSelectedOnExchangePlansForOffExchange(PlanSearchRequestDTO request){
		
		int updatedCount = 0;
		
		if (request == null || request.getSelectedPlanIds() == null){
			return updatedCount;
		}
			
		
		List<Plan> planList = iPlanRepository.getPlanListByPlanIds(request.getSelectedPlanIds());
		if (planList == null){
			return updatedCount;
		}
		
		List<Plan> plansWith01Variation = planList.stream()
			                                  .filter(p-> (p.getIssuerPlanNumber().substring(14, 16)).equals(PlanMgmtConstants.CSR_VARIATION_01))
			                                  .collect(Collectors.toList());
		
		String csrOffExchangeFlag = request.getCsrOffExchangeFlag();
		if(planList.size() == plansWith01Variation.size()){
			for(Plan planObj : plansWith01Variation){
				if(StringUtils.isNotBlank(csrOffExchangeFlag) && PlanMgmtConstants.YES_CAPS.equalsIgnoreCase(csrOffExchangeFlag)){
						planObj.setEnableForOffExchFlow(PlanMgmtConstants.YES_CAPS);
				}else if(StringUtils.isNotBlank(csrOffExchangeFlag) && PlanMgmtConstants.NO_CAPS.equalsIgnoreCase(csrOffExchangeFlag)){
						planObj.setEnableForOffExchFlow(PlanMgmtConstants.NO_CAPS);
					
				}
			}
			List<Plan> savedPlans = iPlanRepository.save(plansWith01Variation);
			updatedCount = savedPlans.size();
		}
		return updatedCount;
	}
	
	@Override
	public PlanSearchResponseDTO bulkUpdatePlanStatusForSelectedPlans(PlanSearchRequestDTO request){
		
			PlanSearchResponseDTO response = new PlanSearchResponseDTO();
		    List<PlanLightDTO> failedPlans = new ArrayList<PlanLightDTO>();
		    List<PlanLightDTO> updatedPlans = new ArrayList<PlanLightDTO>();
		    PlanLightDTO planLightDTO;
		    
			if (request == null || request.getSelectedPlanIds() == null){
				return response;
			}
			
	    
			List<Plan> planList = iPlanRepository.getPlanListByPlanIds(request.getSelectedPlanIds());
			if (planList == null){
				return response;
			}
			
			for(Plan plan:planList){
				plan.setStatus(request.getPlanStatusForUpdate());
				plan.setIssuerVerificationStatus(request.getIssuerVerificationStatusForUpdate());
				plan.setEnrollmentAvail(request.getEnrollmentAvailForUpdate());
				plan.setLastUpdatedBy(request.getLastUpdatedBy());
				plan.setLastUpdateTimestamp(new TSDate());
				if(request.getPlanStatusForUpdate().equalsIgnoreCase(PlanMgmtConstants.CERTIFIED)){
					plan.setCertifiedby(Integer.toString(request.getLastUpdatedBy()));
					plan.setCertifiedOn(new TSDate());
				}
				Plan savedPlan = iPlanRepository.save(plan);
				
				planLightDTO = new PlanLightDTO();
				planLightDTO.setHiosProductId(plan.getHiosProductId());
				planLightDTO.setIssuerShortName(plan.getIssuer().getShortName());
				planLightDTO.setPlanId(plan.getId());
				
				if (savedPlan == null) {
					failedPlans.add(planLightDTO);
				} 
				else{
					updatedPlans.add(planLightDTO);
				}
				
		  }
		  response.setUpdatedPlans(updatedPlans);
		  response.setFailedPlans(failedPlans);
	 
		return response;
	}

	@Override
	public List<TenantPlan> getPlanTenants(int planId) {
		return pmmsITenantPlanRepository.findByPlan_Id(planId);
	}
	
	@Override
	@Transactional
	public void decertifyPlans(int issuerId) {
		iPlanRepository.decertifyPlans(issuerId);
	}

	@Override
	public FormularyDrugListResponseDTO getFormularyDrugList(int applicableYear, String issuerHiosId, String formularyId) {
		FormularyDrugListResponseDTO formularyDrugListDTO = new FormularyDrugListResponseDTO();
		int errorCode = 0;
		String errorMsg = null;
		if(applicableYear > 0 && null != issuerHiosId && null != formularyId) {
			formularyDrugListDTO.setApplicableYear(String.valueOf(applicableYear));
			formularyDrugListDTO.setFormularyId(formularyId);
			formularyDrugListDTO.setIssuerHIOSId(issuerHiosId);
			EntityManager entityManager = null;
			try {
				String buildquery = buildFormularyDrugListQuery();
				entityManager = emf.createEntityManager();
				Query query = entityManager.createQuery(buildquery.toString());
				query.setParameter("param_formulary_id", formularyId);
				query.setParameter("param_applicable_year", applicableYear);
				query.setParameter("param_issuer_id", issuerHiosId);
				List<?> rsList = query.getResultList();
				Iterator<?> rsIterator = rsList.iterator();
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Formulary Drug Data is fetched from db result size is " + rsList.size());
				}
				while (rsIterator.hasNext()) {
					Object[] objArray = (Object[]) rsIterator.next();
					FormularyDrugItemDTO formularyDrugItem = new FormularyDrugItemDTO();
					formularyDrugItem.setFormularyId(String.valueOf(objArray[PlanMgmtConstants.ZERO]));
					formularyDrugItem.setDrugTierLevel((Integer) objArray[PlanMgmtConstants.ONE]);
					formularyDrugItem.setDrugTierType1(String.valueOf(objArray[PlanMgmtConstants.TWO]));
					formularyDrugItem.setDrugTierType2(objArray[PlanMgmtConstants.THREE] == null ? "" : String.valueOf(objArray[PlanMgmtConstants.THREE]));
					formularyDrugItem.setRxcuiCode(String.valueOf( objArray[PlanMgmtConstants.FOUR]));
					formularyDrugItem.setAuthRequired(String.valueOf(objArray[PlanMgmtConstants.FIVE]));
					formularyDrugItem.setStepTherapyRequired(String.valueOf(objArray[PlanMgmtConstants.SIX]));
					formularyDrugListDTO.addFormularyDrugItemToList(formularyDrugItem);
				}
				buildquery = buildFormularyPlanListQuery();
				Query plansQuery = entityManager.createQuery(buildquery.toString());
				plansQuery.setParameter("param_formulary_id", formularyId);
				plansQuery.setParameter("param_applicable_year", applicableYear);
				plansQuery.setParameter("param_issuer_id", issuerHiosId);
				List<?> rsPlanList = plansQuery.getResultList();
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Formulary Plans Data is fetched from db result size is " + rsList.size());
				}
				rsIterator = rsPlanList.iterator();
				while (rsIterator.hasNext()) {
					Object[] objArray = (Object[]) rsIterator.next();
					if(null == formularyDrugListDTO.getIssuerName()) {
						formularyDrugListDTO.setIssuerName((String) objArray[PlanMgmtConstants.ZERO]);
					}
					FormularyPlanDTO formularyPlan = new FormularyPlanDTO();
					formularyPlan.setFormularyId(String.valueOf(objArray[PlanMgmtConstants.THREE]));
					formularyPlan.setIssuerPlanNumber(String.valueOf(objArray[PlanMgmtConstants.TWO]));
					formularyPlan.setPlanName(String.valueOf(objArray[PlanMgmtConstants.ONE]));
					formularyDrugListDTO.addFormularyPlanToList(formularyPlan);
				}
			} catch (Exception e) {
				errorCode = PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_PLAN_RATE.getCode();
				errorMsg = "Failed to Formulary Drug List data.";
				LOGGER.error("Failed to create Formulary Drug List data. Exception: ", e);
			}
			finally
			{
				if(entityManager != null && entityManager.isOpen())
				{
					try {
						entityManager.close();
					}
					catch(IllegalStateException ex)
					{
						LOGGER.error("EntityManager is managed by application server", ex);
					}
				}
			}
		} else {
			errorCode = PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode();
			errorMsg = PlanMgmtConstants.INVALID_REQUEST;
		}
		formularyDrugListDTO.setErrMsg(errorMsg);
		formularyDrugListDTO.setErrCode(errorCode);
		if(errorCode == 0) {
			formularyDrugListDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
		} else {
			formularyDrugListDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return formularyDrugListDTO;	
	}

	private String buildFormularyPlanListQuery() {
		StringBuilder buildquery = new StringBuilder(); 
		buildquery.append("SELECT i.name, p.name, p.issuerPlanNumber, f.formularyId "
				+ " FROM Plan p, Formulary f, Issuer i"
				+ " where i.id = p.issuer.id "
				+ " and p.formularlyId = f.id"
				+ " and f.formularyId = :param_formulary_id"
				+ " and f.applicableYear = :param_applicable_year"
				+ " and i.hiosIssuerId = :param_issuer_id "
				+ " ORDER BY p.id");
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("build query : " + buildquery.toString());
		}
		return buildquery.toString();
	}

	private String buildFormularyDrugListQuery() {
		StringBuilder buildquery = new StringBuilder(); 
		buildquery.append("SELECT f.formularyId, pdt.drugTierLevel, pdt.drugTierType1, pdt.drugTierType2, pd.rxcui, pd.authRequired, pd.stepTherapyRequired "
				+ " FROM Drug pd, DrugTier pdt, Formulary f, Issuer i"
				+ " where i.id = f.issuer.id "
				+ " and f.drugList.id = pdt.drugList.id"
				+ " and pdt.id=pd.drugTier.id"
				+ " and f.formularyId = :param_formulary_id"
				+ " and f.applicableYear = :param_applicable_year"
				+ " and i.hiosIssuerId = :param_issuer_id "
				+ " ORDER BY pd.id");
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("build query : " + buildquery.toString());
		}
		return buildquery.toString();
	}

	/**
	 * Method is used to get Plan info by Plan IDs.
	 */
	@Override
	public void getPlanInfoDataByPlanIds(PlanRequest request, PlanListResponse response) {

		LOGGER.info("Begin execution of getPlanInfoDataByPlanIds API.");

		try {

			if (null == request || CollectionUtils.isEmpty(request.getPlanIds())) {
				response.setStatusCode(HttpStatus.PRECONDITION_REQUIRED);
				response.setMessage(PlanMgmtConstants.INVALID_REQUEST);
				return;
			}

			Set<Integer> uniquePlanIdSet = new HashSet<Integer>(request.getPlanIds());
			List<PlanResponse> planResponseList = iPlanRepository.getHealthPlansByIdIn(uniquePlanIdSet);

			if (null == planResponseList) {
				planResponseList = new ArrayList<PlanResponse>();
			}

			if (uniquePlanIdSet.size() > planResponseList.size()) {
				List<PlanResponse> dentalPlanResponseList = iPlanRepository.getDentalPlansByIdIn(uniquePlanIdSet);

				if (!CollectionUtils.isEmpty(dentalPlanResponseList)) {
					planResponseList.addAll(dentalPlanResponseList);
				}
			}

			if (CollectionUtils.isEmpty(planResponseList)) {
				response.setStatusCode(HttpStatus.NO_CONTENT);
				response.setMessage(PlanMgmtConstants.RECORD_NOT_FOUND);
			}
			else {
				generatePlanResponse(uniquePlanIdSet, planResponseList, response);
			}
		}
		catch (Exception ex) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
			response.setMessage(ex.getMessage());
			LOGGER.error("Exception occurred while get Plan info by IDs:", ex);
		}
		finally {
			LOGGER.info("End execution of getPlanInfoDataByPlanIds API.");
		}
	}

	/**
	 * Method is used to generate Plan Response by Plan IDs.
	 */
	private void generatePlanResponse(Set<Integer> uniquePlanIdSet, List<PlanResponse> planResponseList, PlanListResponse response) {

		// Validate Requested Plan IDs with existing Plan IDs
		validateRequestedIdsWithExistingPlanIds(uniquePlanIdSet, planResponseList, response);

		if (HttpStatus.NO_CONTENT.equals(response.getStatusCode())) {
			return;
		}

		String considerEHBPortion = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CONSIDER_EHB_PORTION);
		String pediatricDentalComponent, ehbPercentage;

		for (PlanResponse planResponse : planResponseList) {
			// CSR Amount is effective onwards 2015.
			planResponse.setCsrAmount(getCSRMultiplier(planResponse.getCostSharing(), planResponse.getPlanLevel(), planResponse.getPlanYear()));
			planResponse.setTaxIdNumber((planResponse.getTaxIdNumber() != null) ? planResponse.getTaxIdNumber().replace("-", StringUtils.EMPTY) : StringUtils.EMPTY);

			pediatricDentalComponent = ehbPercentage = null;

			if (considerEHBPortion.equalsIgnoreCase(PlanMgmtConstants.Y)) {

				if (Plan.PlanInsuranceType.DENTAL.toString().equals(planResponse.getInsuranceType())) {

					if (null != planResponse.getPediatricDentalComponent()) {
						pediatricDentalComponent = planResponse.getPediatricDentalComponent().replace("$", StringUtils.EMPTY);
					}
				}
				else if (Plan.PlanInsuranceType.HEALTH.toString().equals(planResponse.getInsuranceType())) {	
					planResponse.setStateEhbMandatePercentage(PlanMgmtConstants.STRZERO);
					ehbPercentage = planResponse.getEhbPercentage();

					if (null == ehbPercentage) {
						ehbPercentage = PlanMgmtConstants.ONE_STR;
					}
				}
			}
			planResponse.setPediatricDentalComponent(pediatricDentalComponent);
			planResponse.setEhbPercentage(ehbPercentage);

			planResponse.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(planResponse.getIssuerLogo(), planResponse.getHiosIssuerId(), null, appUrl));
			response.setPlanInList(planResponse);
		}

		if (!CollectionUtils.isEmpty(response.getPlanList())) {
			response.setStatusCode(HttpStatus.OK);
			response.setMessage("API has been executed successfully.");
		}
	}

	/**
	 * Method is used validate Requested Plan IDs with existing Plan IDs.
	 */
	private void validateRequestedIdsWithExistingPlanIds(Set<Integer> uniquePlanIdSet, List<PlanResponse> planResponseList, PlanListResponse response) {

		if (uniquePlanIdSet.size() != planResponseList.size()) {

			List<Integer> existPlanIdList = new ArrayList<Integer>();
			List<Integer> nonExistPlanIdList = null;

			// Exist Plan IDs in Database.
			planResponseList.forEach(planResponse -> existPlanIdList.add(planResponse.getPlanId()));
			// Get non exist Plan IDs.
			nonExistPlanIdList = uniquePlanIdSet.stream().filter(planId -> !existPlanIdList.contains(planId)).collect(Collectors.toList());

			StringBuffer errorMessage = new StringBuffer();
			errorMessage.append(PlanMgmtConstants.RECORD_NOT_FOUND).append(" ").append(nonExistPlanIdList);

			response.setMessage(errorMessage.toString());
			response.setStatusCode(HttpStatus.NO_CONTENT);
		}
	}
}
