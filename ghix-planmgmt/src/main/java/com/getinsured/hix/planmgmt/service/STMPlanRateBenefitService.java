/**
 * STM plan Service interface.
 * 
 * @author santanu
 * @version 1.0
 * @since Apr 24, 2014
 * 
 */
package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;
import com.getinsured.hix.model.PlanRateBenefit;


public interface STMPlanRateBenefitService {
	
	List<PlanRateBenefit> getStmPlanRateBenefits(List<Map<String, String>> memberList, String effectiveDate, String tenantCode);
	
	Map<String, Map<String, Map<String, String>>> getStmPlanBenefits(String planStmIdStr);
	
	Map<String, Map<String, Map<String, String>>> getStmPlanCosts(String planStmIdStr);	
	
}