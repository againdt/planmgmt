package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

public interface SyncPlanWithAHBXService {

	public void syncPlanWithAHBX(List<Map<String, String>> listOfPlans);
	
}
