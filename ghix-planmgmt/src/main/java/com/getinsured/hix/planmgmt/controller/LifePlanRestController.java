/**
 * 
 * @author santanu
 * @version 1.0
 * @since May 02, 2016 
 * 
 * This controller for Life plan  
 */
package com.getinsured.hix.planmgmt.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Produces;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.LifePlanDTO;
import com.getinsured.hix.dto.planmgmt.LifePlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.LifePlanResponseDTO;
import com.getinsured.hix.dto.planmgmt.Member;
import com.getinsured.hix.planmgmt.service.LifePlanRateBenefitService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;

@Controller
@RequestMapping(value = "/life")
public class LifePlanRestController {

	private static final Logger LOGGER = Logger.getLogger(LifePlanRestController.class);
	public static final String REQUIRED_DATE_FORMAT = "yyyy-MM-dd";
	
	@Autowired private LifePlanRateBenefitService lifePlanRateBenefitService;
	@Autowired private GIExceptionHandler giExceptionHandler;
	
	
	//This operation is for life plan quoting 
	@RequestMapping(value = "/getPlans", method = RequestMethod.POST) // for testing purpose
	@Produces("application/json")
	@ResponseBody public LifePlanResponseDTO getPlans(@RequestBody LifePlanRequestDTO lifePlanRequestDTO )
	{
		LifePlanResponseDTO lifePlanResponseDTO = new LifePlanResponseDTO();
		String tenantCode = PlanMgmtConstants.TENANT_GINS;
		if(!StringUtils.isBlank(lifePlanRequestDTO.getTenantCode())) {
			tenantCode = lifePlanRequestDTO.getTenantCode().trim();
		}
		
		if(LOGGER.isDebugEnabled()){
			StringBuilder requestDataStr = new StringBuilder(512);
			requestDataStr.append("Request data ===>");			
			requestDataStr.append(" Effective date : ");
			requestDataStr.append(lifePlanRequestDTO.getEffectiveDate());
			requestDataStr.append(", State Code : ");
			requestDataStr.append(lifePlanRequestDTO.getStateCode());
			requestDataStr.append(", Tenant Code : ");
			requestDataStr.append(tenantCode);
			requestDataStr.append(", memberInfo :: age: ");
			requestDataStr.append(lifePlanRequestDTO.getMemberInfo().getAge());
			requestDataStr.append(", gender: ");
			requestDataStr.append(lifePlanRequestDTO.getMemberInfo().getGender());
			requestDataStr.append(", tobacco: ");
			requestDataStr.append(lifePlanRequestDTO.getMemberInfo().getTobacco());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(requestDataStr.toString()));	
		}
		
		if(null == lifePlanRequestDTO.getEffectiveDate() || lifePlanRequestDTO.getEffectiveDate().trim().isEmpty()				
				|| null == lifePlanRequestDTO.getMemberInfo() || null == lifePlanRequestDTO.getStateCode() || lifePlanRequestDTO.getStateCode().isEmpty()
				|| null == lifePlanRequestDTO.getMemberInfo().getGender() || lifePlanRequestDTO.getMemberInfo().getGender().trim().isEmpty()
				|| null == lifePlanRequestDTO.getMemberInfo().getTobacco() || lifePlanRequestDTO.getMemberInfo().getTobacco().trim().isEmpty()
				|| lifePlanRequestDTO.getMemberInfo().getAge() <= 0	) {
			lifePlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			lifePlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			lifePlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			
		} else {
				Member memberInfo = (Member)lifePlanRequestDTO.getMemberInfo();			
				String effectiveDate = lifePlanRequestDTO.getEffectiveDate().trim();
				String stateCode = lifePlanRequestDTO.getStateCode().trim();
				SimpleDateFormat sdf = new SimpleDateFormat(REQUIRED_DATE_FORMAT); // expect coverage date in yyyy-MM-dd format
				Date vaidateDate = null;
				try{
					sdf.setLenient(false);
					vaidateDate = sdf.parse(lifePlanRequestDTO.getEffectiveDate().trim());

					try{		
						// call life plan quoting service
						List<LifePlanDTO> lifePlanList = lifePlanRateBenefitService.getPlans(effectiveDate, memberInfo, stateCode, tenantCode);								
			
						lifePlanResponseDTO.setLifePlanList(lifePlanList);	
						lifePlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
						lifePlanResponseDTO.endResponse();
					}catch(Exception ex) {
						giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch Life plans.",ex);
						LOGGER.error("Failed to fetch Life plans. Exception:", ex);					
						lifePlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
						lifePlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_LIFE_PLANS.code);
						lifePlanResponseDTO.setErrMsg(Arrays.toString(ex.getStackTrace()));
					}					
				
				}catch (ParseException  e){
					lifePlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					lifePlanResponseDTO.setErrCode(GhixErrors.INVALID_COVERAGE_DATE.getId());
					lifePlanResponseDTO.setErrMsg(GhixErrors.INVALID_COVERAGE_DATE.getMessage());
				}
		}
		return lifePlanResponseDTO;
	}

}
