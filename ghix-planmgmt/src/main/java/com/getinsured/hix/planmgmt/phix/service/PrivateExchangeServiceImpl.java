package com.getinsured.hix.planmgmt.phix.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.planmgmt.EligibilityRequest;
import com.getinsured.hix.model.planmgmt.EligibilityResponse;
import com.getinsured.hix.model.planmgmt.Member;
import com.getinsured.hix.model.planmgmt.TeaserPlanRequest;
import com.getinsured.hix.model.planmgmt.TeaserPlanResponse;
import com.getinsured.hix.planmgmt.model.PhixConstants;
import com.getinsured.hix.planmgmt.service.PlanCostAndBenefitsService;
import com.getinsured.hix.planmgmt.service.PlanRateBenefitService;
import com.getinsured.hix.planmgmt.service.QuotingBusinessLogic;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("privateExchangeService")
@Transactional
@DependsOn("dynamicPropertiesUtil")
@Repository
public class PrivateExchangeServiceImpl implements PrivateExchangeService {

	private static final Logger LOGGER = Logger.getLogger(PrivateExchangeServiceImpl.class);

	@Value("#{configProp['database.type']}")
	private String configuredDB;
	
	@Autowired
	private LookupService lookupService;
	@Autowired
	private PlanCostAndBenefitsService planCostAndBenefitsService;
	@Autowired
	private QuotingBusinessLogic quotingBusinessLogic; 
	@Autowired
	private PlanRateBenefitService planRateBenefitService;

	private static final String SPOUSE = "spouse";
	private static final String CHILD = "child";
	private static final String SELF = "self";
	private static final String MEMBER = "member";
	private static final String DEPENDENT = "dependent";
	private static final String FAMILY_TIER_PRIMARY = "Individual Rate";
	private static final String FAMILY_TIER_COUPLE = "Couple";
	private static final String FAMILY_TIER_PRIMARY_ONE_DEPENDENT = "Primary Subscriber and One Dependent";
	private static final String FAMILY_TIER_PRIMARY_TWO_DEPENDENT = "Primary Subscriber and Two Dependents";
	private static final String FAMILY_TIER_PRIMARY_MANY_DEPENDENT = "Primary Subscriber and Three or More Dependents";
	private static final String FAMILY_TIER_COUPLE_ONE_DEPENDENT = "Couple and One Dependent";
	private static final String FAMILY_TIER_COUPLE_TWO_DEPENDENT = "Couple and Two Dependents";
	private static final String FAMILY_TIER_COUPLE_MANY_DEPENDENT = "Couple and Three or More Dependents";
	private static final String FAMILY_LOOKUP_NAME = "SERFF_FAMILY_OPTION";
	private static final String CHILD_ONLY = "CHILDONLY";
	private static final String ADULT_ONLY = "ADULTONLY";
	private static final String ADULT_AND_CHILD = "ADULTANDCHILD";
	private boolean isAllAvailablePlansSilver = true;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("YYYY-MM-dd");

	@PersistenceUnit 
	private EntityManagerFactory emf;
	
		
	@Override
	public TeaserPlanResponse getSilverPlan(TeaserPlanRequest teaserPlanRequest, boolean isLowest)throws GIException {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("TeaserPlanRequest ==> " + SecurityUtil.sanitizeForLogging(teaserPlanRequest.toString()));
		}
		TeaserPlanResponse teaserPlanResponse = null;
		List<Map<String, String>> unprocessedCensusData = getCensusData(teaserPlanRequest.getMembers(), teaserPlanRequest.getZipCode(), teaserPlanRequest.getCounty(), null);
		Calendar effectiveDate = teaserPlanRequest.getEffectiveDate();
		Date effctiveUtilDate = effectiveDate.getTime();
		String strEffectiveDate = simpleDateFormat.format(effctiveUtilDate);

		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Fetching benchmark plan for having family size ==> " + unprocessedCensusData.size() + " Coverage Date: " + strEffectiveDate);
		}
		try {
			Map <String, String> benchMarkPlanIdPremiumMap = planRateBenefitService.getBenchmarkRateForEnrollment(unprocessedCensusData, strEffectiveDate, 
					teaserPlanRequest.getCostSharingVariation(), null, null, null);
			Object[] plan = new Object[] { new BigDecimal(benchMarkPlanIdPremiumMap.get(PlanMgmtConstants.INTERNAL_PLAN_ID)), 
											benchMarkPlanIdPremiumMap.get(PlanMgmtConstants.PLAN_NAME),
											benchMarkPlanIdPremiumMap.get(PlanMgmtConstants.TOTALPREMIUM)};
			teaserPlanResponse = updateTeaserPlanResponse(plan);
		} catch (Exception e) {
			LOGGER.error("Exception occurred while processing TeaserPlanRequest getSilverPlan request. Exception:",e);
			teaserPlanResponse = new TeaserPlanResponse();
			teaserPlanResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			teaserPlanResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_BENCHMARK_RATE.code);
			teaserPlanResponse.setErrMsg(e.getMessage());
		}
		return teaserPlanResponse;
	}

	private List<Map<String, String>> getCensusData(List<Member> membersList, String zipcode, String countyCode, String tobaccoValue) {
		List<Map<String, String>> censusData = new ArrayList<Map<String, String>>();
		if(!CollectionUtils.isEmpty(membersList)) {
			for (Member member : membersList) {
				if (member.getMemberType() != null) {
					Map<String, String> memberCensusMap = new HashMap<String, String>();
					memberCensusMap.put(PlanMgmtConstants.RELATION, member.getMemberType());
					memberCensusMap.put(PlanMgmtConstants.ZIP, zipcode);
					memberCensusMap.put(PlanMgmtConstants.COUNTYCODE, countyCode);
					memberCensusMap.put(PlanMgmtConstants.AGE, String.valueOf(member.getAge()));
					memberCensusMap.put(PlanMgmtConstants.GENDER, member.getGender());
					if(StringUtils.isNotBlank(tobaccoValue)) {
						memberCensusMap.put(PlanMgmtConstants.TOBACCO, tobaccoValue);
					} else {
						if(member.isTobaccoUser()) {
							memberCensusMap.put(PlanMgmtConstants.TOBACCO, PlanMgmtConstants.Y);
						} else {
							memberCensusMap.put(PlanMgmtConstants.TOBACCO, PlanMgmtConstants.N);
						}
					}
					censusData.add(memberCensusMap);
				}
			}
		}
		return censusData;
	}

	private List<Object[]> getSilverPlans(List<Member> modifiedMembers,
			EligibilityRequest eligibilityRequest, boolean isNonTobacco)
			throws GIException {
		
		List<Map<String, String>> unprocessedCensusData = null;
		List<Object[]> resultList = new ArrayList<Object[]>();
		String tobaccoValue = null;
		if(isNonTobacco) {
			tobaccoValue = PlanMgmtConstants.N;
		} 
			
		try{
			Calendar effectiveDate = eligibilityRequest.getEffectiveDate();
			Date effctiveUtilDate = effectiveDate.getTime();
			String strEffectiveDate = simpleDateFormat.format(effctiveUtilDate);
			unprocessedCensusData = getCensusData(modifiedMembers, eligibilityRequest.getZipCode(), eligibilityRequest.getCounty(), tobaccoValue);

			List<Map <String, String>> silverPlansDetailMapList = 
					planRateBenefitService.getAvailableSilverPlans(unprocessedCensusData, strEffectiveDate, eligibilityRequest.getCostSharingVariation(), eligibilityRequest.getTenantCode(), null, null);
			
			for (Map <String, String> silverplan : silverPlansDetailMapList) {
				Object[] plan = new Object[] { 
						new BigDecimal(silverplan.get(PlanMgmtConstants.INTERNAL_PLAN_ID)), 
						silverplan.get(PlanMgmtConstants.PLAN_NAME),
						silverplan.get(PlanMgmtConstants.TOTALPREMIUM)};
				resultList.add(plan);
			}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getSilverPlans", ex);
		}
		return resultList;
	}

	private TeaserPlanResponse createTeaserPlanResponse(List<Object[]> plans,
			EligibilityRequest eligibilityRequest, boolean isLowest) {
		TeaserPlanResponse teaserPlanResponse = null;
		Object[] plan = null;
		if (plans == null || (plans.isEmpty())) {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("No plans found for Eligibility calculator");
			}
			return teaserPlanResponse;

		} else if (plans.size() == 1 || isLowest) // if only more than one silver plan available then consider second plan as benchmark plan
		{
			plan = plans.get(0);
			teaserPlanResponse = updateTeaserPlanResponse(plan);
			teaserPlanResponse.setTotalPlans(plans.size());
			return teaserPlanResponse;
		} else if (plans.size() > 1 && !isLowest) // if only more than one silver plan available then consider second plan as benchmark plan
		{
			plan = plans.get(1);
			teaserPlanResponse = updateTeaserPlanResponse(plan);
			teaserPlanResponse.setTotalPlans(plans.size());
			return teaserPlanResponse;
		}

		return teaserPlanResponse;

	}

	@Transactional(readOnly = true)
	private List<Object[]> getBronzePlans(List<Member> modifiedMembers,
			EligibilityRequest eligibilityRequest, boolean isNonTobacco)
			throws GIException {
		List<Object[]> resultList = new ArrayList<Object[]>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			Calendar effectiveDate = eligibilityRequest.getEffectiveDate();
			Date effctiveUtilDate = effectiveDate.getTime();
			String strEffectiveDate = simpleDateFormat.format(effctiveUtilDate);
			String exchangeType = eligibilityRequest.getExchangeFlowType();
			String familyTier = getFamilyTier(modifiedMembers); // compute family tier
			String familyTierLookupCode = lookupService.getLookupValueCode(FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
			String houseHoldType = getHouseHoldType(modifiedMembers);
			String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
			String tenantCode = (eligibilityRequest.getTenantCode() == null || eligibilityRequest.getTenantCode().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.TENANT_GINS : eligibilityRequest.getTenantCode().trim() ; // default value is GINS
			String applicableYear = strEffectiveDate.substring(0,4);
	
			ArrayList<String> paramList = new ArrayList<String>();
			StringBuilder buildquery = new StringBuilder("SELECT temp.plan_id, temp.plan_health_id, p.name, p.issuer_id, ( CASE WHEN UPPER(rate_option) = 'F' THEN (SUM(pre) / " + modifiedMembers.size() + ") ELSE SUM(pre) END) AS total_premium ");		
			buildquery.append("FROM plan p, ");
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildquery.append(" pm_tenant_plan tp, tenant t, ");
			}
			buildquery.append(" ( ");
			int memberCount = 0;
			for (Member member : modifiedMembers) {
				buildquery.append("( SELECT phealth.id AS plan_health_id, phealth.plan_id, prate.rate AS pre, prate.rate_option AS rate_option, 1 AS memberctr ");
				buildquery.append(" FROM ");
				buildquery.append(" plan_health phealth, pm_service_area psarea, pm_plan_rate prate, plan p2 ");
				buildquery.append(" WHERE ");
				buildquery.append(" phealth.plan_level = 'BRONZE' ");
				buildquery.append(" AND p2.id = phealth.plan_id ");
				buildquery.append(" AND p2.service_area_id = psarea.service_area_id ");
	
				if (isNonTobacco) {
					buildquery.append(" AND (prate.tobacco = 'N' OR prate.tobacco IS NULL )");
				} else {
					if (member.isTobaccoUser()) {
						buildquery.append(" AND (prate.tobacco = 'Y' OR prate.tobacco IS NULL )");
					} else {
						buildquery.append("  AND (prate.tobacco = 'N' OR prate.tobacco IS NULL )");
					}
				}
				buildquery.append(" AND prate.rate IS NOT NULL ");
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append(" AND psarea.zip = to_number(trim(:param_" + paramList.size());
				paramList.add(eligibilityRequest.getZipCode());
					buildquery.append(")) ");
				}else{
					buildquery.append(" AND psarea.zip = :param_" + paramList.size());
					paramList.add(eligibilityRequest.getZipCode());
				}
				
				buildquery.append(" AND psarea.fips = :param_" + paramList.size() + " ");
				paramList.add(eligibilityRequest.getCounty());
				
				buildquery.append(" AND p2.id = prate.plan_id ");
				buildquery.append(" AND p2.is_deleted = 'N' ");
				buildquery.append(" AND prate.is_deleted = 'N' ");
				buildquery.append(" AND psarea.is_deleted = 'N' ");
				buildquery.append(" AND prate.rating_area_id = ");
				
				// -------- changes for HIX-100092 starts --------
				buildquery.append(buildSearchRatingAreaLogic(paramList, eligibilityRequest.getZipCode(), eligibilityRequest.getCounty(), applicableYear));
				// -------- changes for HIX-100092 ends --------
				
					buildquery.append(" AND ( (CAST (:param_" + paramList.size() + "  AS INTEGER) >= prate.min_age AND ");
					paramList.add(String.valueOf(member.getAge()));
					buildquery.append(" CAST (:param_" + paramList.size() + " AS INTEGER) <= prate.max_age) ");
					paramList.add(String.valueOf(member.getAge()));
					buildquery.append(" OR (CAST (:param_" + paramList.size() + " AS INTEGER) >= prate.min_age AND ");
					paramList.add(familyTierLookupCode);

					buildquery.append(" CAST (:param_" + paramList.size() + " AS INTEGER) <= prate.max_age) ) ");
					paramList.add(familyTierLookupCode);
				
				buildquery.append(" AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date ");
				paramList.add(strEffectiveDate);
				
				if (exchangeType.equalsIgnoreCase(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF)) {
					buildquery.append(" AND phealth.cost_sharing = 'CS0'");
				} else {
					buildquery.append(" AND phealth.cost_sharing = 'CS1'");
				}
				buildquery.append(" )");
				if (memberCount + 1 != modifiedMembers.size()) {
					buildquery.append(" UNION ALL ");
				}
				memberCount++;
			}
	
			buildquery.append(") temp WHERE p.id = temp.plan_id AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') between p.start_date and p.end_date ");
			paramList.add(strEffectiveDate);
			
			buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
			buildquery.append(" AND p.market = '" + PlanMgmtConstants.PLAN_MARKET_INDIVIDUAL + "' ");
			buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic("p", strEffectiveDate, PlanMgmtConstants.NO, eligibilityRequest.getZipCode(), eligibilityRequest.getCounty(), null, paramList));
			buildquery.append(" AND p.insurance_type = 'HEALTH' AND p.exchange_type = :param_" + paramList.size());
			paramList.add(exchangeType.toUpperCase());
	
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildquery.append(" AND p.id = tp.plan_id");
				buildquery.append(" AND tp.tenant_id = t.id");
				buildquery.append(" AND t.code = :param_" + paramList.size() );
				paramList.add(tenantCode.toUpperCase());
			}
			
			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic("p", houseHoldType));
			buildquery.append(" GROUP BY temp.plan_id, temp.plan_health_id, p.name, p.issuer_id, rate_option ");
			buildquery.append("HAVING sum(memberctr) = " + modifiedMembers.size());
			buildquery.append(" ORDER BY total_premium asc");
	
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Query for Bronze plans ==> " + buildquery);
			}
			
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			 resultList = query.getResultList();
			 
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getBronzePlans", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return resultList;
	}

	private TeaserPlanResponse updateTeaserPlanResponse(Object[] plan) {
		DecimalFormat f = new DecimalFormat("##.00");
		TeaserPlanResponse teaserPlanResponse = new TeaserPlanResponse();
		BigDecimal planId = (BigDecimal) plan[PlanMgmtConstants.ZERO];
		String planName = (String) plan[PlanMgmtConstants.ONE];
		Double totalPremium =  Double.parseDouble((String) plan[PlanMgmtConstants.TWO]);
				
		teaserPlanResponse.setBenchmarkPlanId(String.valueOf(planId.intValue()));
		teaserPlanResponse.setPlanName(planName);
		teaserPlanResponse.setBenchmarkPremium(totalPremium.doubleValue());

		return teaserPlanResponse;
	}


	private String getFamilyTier(List<Member> memberList) {

		Boolean isSelfQuoted = false;
		Boolean isSpouseQuoted = false;
		Boolean isDependentQuoted = false;

		for (Member member : memberList) {
			if (member.getMemberType() != null) {
				if (member.getMemberType().equalsIgnoreCase(MEMBER)
						|| member.getMemberType().equalsIgnoreCase(SELF)) {
					isSelfQuoted = true;
				}
				if (member.getMemberType().equalsIgnoreCase(SPOUSE)) {
					isSpouseQuoted = true;
				}
				if (member.getMemberType().equalsIgnoreCase(CHILD)
						|| member.getMemberType().equalsIgnoreCase(DEPENDENT)) {
					isDependentQuoted = true;
				}
			}
		}

		if (isSelfQuoted && memberList.size() == 1) { // when only self quoted
			return FAMILY_TIER_PRIMARY;
		} else if (isSelfQuoted && isSpouseQuoted && memberList.size() == 2) { // when
																				// couple
																				// quoted
			return FAMILY_TIER_COUPLE;
		} else if (isSelfQuoted && isSpouseQuoted && isDependentQuoted
				&& memberList.size() == PlanMgmtConstants.THREE) { // when
																	// couple +
																	// 1
																	// dependent
																	// quoted
			return FAMILY_TIER_COUPLE_ONE_DEPENDENT;
		} else if (isSelfQuoted && isSpouseQuoted && isDependentQuoted
				&& memberList.size() == PlanMgmtConstants.FOUR) { // when couple
																	// + 2
																	// dependents
																	// quoted
			return FAMILY_TIER_COUPLE_TWO_DEPENDENT;
		} else if (isSelfQuoted && isSpouseQuoted && isDependentQuoted
				&& memberList.size() > PlanMgmtConstants.FOUR) { // when couple
																	// and more
																	// than 2
																	// dependents
																	// quoted
			return FAMILY_TIER_COUPLE_MANY_DEPENDENT;
		} else if (!isSpouseQuoted && isSelfQuoted && isDependentQuoted
				&& memberList.size() == PlanMgmtConstants.TWO) { // when self
																	// and 1
																	// dependent
																	// (not
																	// spouse)
																	// quoted
			return FAMILY_TIER_PRIMARY_ONE_DEPENDENT;
		} else if (!isSpouseQuoted && isSelfQuoted && isDependentQuoted
				&& memberList.size() == PlanMgmtConstants.THREE) { // when self
																	// and 2
																	// dependents
																	// (not
																	// spouse)
																	// quoted
			return FAMILY_TIER_PRIMARY_TWO_DEPENDENT;
		} else if (!isSpouseQuoted && isSelfQuoted && isDependentQuoted
				&& memberList.size() > PlanMgmtConstants.THREE) { // when self
																	// and more
																	// than 2
																	// dependent
																	// (not
																	// spouse)
																	// quoted
			return FAMILY_TIER_PRIMARY_MANY_DEPENDENT;
		} else {
			return FAMILY_TIER_PRIMARY;
		}
	}

	class CompareAge implements Comparator<Member> {
		public int compare(Member member1, Member member2) {
			return (member1.getAge() > member1.getAge() ? -1 : (member1
					.getAge() < member1.getAge() ? 1 : 0));
		}
	}

	

	// Teaser API
	@Override
	public EligibilityResponse getEligibilityPremium(EligibilityRequest eligibilityRequest) throws GIException {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Eligibility Calculator: getEligibilityPremium started");
		}
		EligibilityResponse eligibilityResponse = new EligibilityResponse();
		TeaserPlanResponse slsntpReponse = null;
		TeaserPlanResponse slstpReponse = null;
		TeaserPlanResponse lsReponse = null;
		TeaserPlanResponse lbReponse = null;

		List<Member> processedMemberData = eligibilityRequest.getMembers();
		List<BigDecimal> listOfSilverPlanIds = getPlanLevelSpecificPlanIds(processedMemberData, eligibilityRequest, PhixConstants.SILVER);
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Eligibility Calculator: listOfSilverPlanIds ::" + listOfSilverPlanIds.toString());
		}

		if ((!(listOfSilverPlanIds.isEmpty()))) {
			boolean isLowest = false;
			List<Object[]> nonTobaccoSilverPlans = getSilverNonTobaccoPremiumPlans(
					eligibilityRequest, processedMemberData);
			slsntpReponse = createTeaserPlanResponse(nonTobaccoSilverPlans,
					eligibilityRequest, isLowest);

			List<Object[]> tobaccoSilverPlans = getSilverTobaccoPlans(
					eligibilityRequest, processedMemberData);
			slstpReponse = createTeaserPlanResponse(tobaccoSilverPlans,
					eligibilityRequest, isLowest);

			isLowest = true;
			lsReponse = createTeaserPlanResponse(tobaccoSilverPlans,
					eligibilityRequest, isLowest);

			updateSilverPlansAvailableInfo(listOfSilverPlanIds,
					tobaccoSilverPlans, eligibilityResponse);

		} else {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Eligibility Calculator: No silver plans found ");
			}
		}

		eligibilityResponse = buildEligibilityReponse(eligibilityResponse,
				slsntpReponse, slstpReponse, lbReponse, lsReponse);
		return eligibilityResponse;

	}

	private void updateSilverPlansAvailableInfo(
			List<BigDecimal> listOfSilverPlanIds,
			List<Object[]> tobaccoSilverPlans,
			EligibilityResponse eligibilityResponse) {

		List<BigDecimal> listOfAvailableSilverPlanIds = new ArrayList<BigDecimal>();

		for (int i = 0; i <= tobaccoSilverPlans.size() - 1; i++) {
			Object[] plan = tobaccoSilverPlans.get(i);
			BigDecimal planId = (BigDecimal) plan[0];
			listOfAvailableSilverPlanIds.add(planId);

		}

		if ((!(listOfAvailableSilverPlanIds.isEmpty()))) {
			eligibilityResponse.setAllPlansAvailableSilver(true);
			this.isAllAvailablePlansSilver = true;
			for (BigDecimal planId : listOfAvailableSilverPlanIds) {
				if (!listOfSilverPlanIds.contains(planId)) {
					eligibilityResponse.setAllPlansAvailableSilver(false);
					this.isAllAvailablePlansSilver = false;
					break;
				}
			}

			Set<BigDecimal> availableSilverPlans = new HashSet<BigDecimal>();
			availableSilverPlans.addAll(listOfAvailableSilverPlanIds);
			eligibilityResponse.setNumberSilverAvailable(availableSilverPlans
					.size());

		} else {
			eligibilityResponse.setAllPlansAvailableSilver(false);
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("No Silver Plans Available for Eligibility Calculator");
			}
		}

	}

	private List<Object[]> getSilverNonTobaccoPremiumPlans(EligibilityRequest eligibilityRequest, List<Member> modifiedMembers) throws GIException {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("EligibilityRequest getSilverNonTobaccoPremiumPlans ==> " + SecurityUtil.sanitizeForLogging(eligibilityRequest.toString()));
		}
		boolean isNonTobacco = true;

		return getSilverPlans(modifiedMembers, eligibilityRequest, isNonTobacco);
	}

	private List<Object[]> getSilverTobaccoPlans(EligibilityRequest eligibilityRequest, List<Member> modifiedMembers) throws GIException {
		boolean isNonTobacco = false;
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("EligibilityRequest for getSilverTobaccoPlans ==> " + SecurityUtil.sanitizeForLogging(eligibilityRequest.toString()));
			LOGGER.debug("Fetching silver plans for having family data ==> " + SecurityUtil.sanitizeForLogging(modifiedMembers.toString()));
		}
		return getSilverPlans(modifiedMembers, eligibilityRequest, isNonTobacco);

	}

	private EligibilityResponse buildEligibilityReponse(
			EligibilityResponse eligibilityResponse,
			TeaserPlanResponse slsntpReponse, TeaserPlanResponse slstpReponse,
			TeaserPlanResponse lbReponse, TeaserPlanResponse lsReponse) {
		boolean isAllPlansAvailableSilver = eligibilityResponse
				.isAllPlansAvailableSilver();
		boolean isAllPlansAvailableBronze = eligibilityResponse
				.isAllPlansAvailableBronze();

		Map<String, HashMap<String, HashMap<String, String>>> plansInformation = new HashMap<String, HashMap<String, HashMap<String, String>>>();

		if (slstpReponse != null && isAllPlansAvailableSilver) {
			eligibilityResponse.setBenchmarkPremiumTobacco(slstpReponse
					.getBenchmarkPremium());
			updateEligibiltyResponse(slstpReponse, PhixConstants.SLSP,
					plansInformation);
		}

		if (slsntpReponse != null) {
			eligibilityResponse.setBenchmarkPremium(slsntpReponse
					.getBenchmarkPremium());
		}

		if (isAllPlansAvailableSilver && lsReponse != null) {
			updateEligibiltyResponse(lsReponse, PhixConstants.LSP,
					plansInformation);
		}

		if (isAllPlansAvailableBronze && lbReponse != null) {

			updateEligibiltyResponse(lbReponse, PhixConstants.LBP,
					plansInformation);
			updateEligibiltyResponse(lbReponse, PhixConstants.GILBP,
					plansInformation);

		} else if (!isAllPlansAvailableBronze && lbReponse != null) {

			updateEligibiltyResponse(lbReponse, PhixConstants.GILBP,
					plansInformation);
		}

		eligibilityResponse.setPlansInformation(plansInformation);
		return eligibilityResponse;
	}

	private void updateEligibiltyResponse(TeaserPlanResponse teaserReponse,
			String planName,
			Map<String, HashMap<String, HashMap<String, String>>> plansInfo) {

		Map<String, HashMap<String, String>> innerMap1 = new HashMap<String, HashMap<String, String>>(); // teaserReponse.getBenefits();
		HashMap<String, String> innerMap2 = new HashMap<String, String>();
		innerMap2.put(PlanMgmtConstants.PLAN_NAME_KEY,
				teaserReponse.getPlanName());
		innerMap1.put(PlanMgmtConstants.PLANNAME, innerMap2);
		innerMap2 = new HashMap<String, String>();
		innerMap2.put(PlanMgmtConstants.MONTHLY_PREMIUM_KEY,
				Double.toString(teaserReponse.getBenchmarkPremium()));
		innerMap1.put(PlanMgmtConstants.MONTHLY_PREMIUM, innerMap2);

		plansInfo.put(planName,
				(HashMap<String, HashMap<String, String>>) innerMap1);

	}

	@Transactional(readOnly = true)
	private List<BigDecimal> getPlanLevelSpecificPlanIds(
			List<Member> modifiedMembers,
			EligibilityRequest eligibilityRequest, String planLevel) {

		List<BigDecimal> resultList = new ArrayList<BigDecimal>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			Calendar effectiveDate = eligibilityRequest.getEffectiveDate();
			String houseHoldType = getHouseHoldType(modifiedMembers);
			String exchangeType = eligibilityRequest.getExchangeFlowType();
			Date effctiveUtilDate = effectiveDate.getTime();
			String strEffectiveDate = simpleDateFormat.format(effctiveUtilDate);
			String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
			String tenantCode = (eligibilityRequest.getTenantCode() == null || eligibilityRequest.getTenantCode().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.TENANT_GINS : eligibilityRequest.getTenantCode().trim() ; // default value is GINS
			
			ArrayList<String> paramList = new ArrayList<String>();
			StringBuilder buildquery = new StringBuilder("SELECT p.id FROM plan p, ");
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildquery.append(" pm_tenant_plan tp, tenant t, ");
			}
			buildquery.append(" ( ");
	
			
			for (int memberCount = 1; memberCount<= modifiedMembers.size(); memberCount++) {
				buildquery.append("( SELECT  phealth.plan_id ");
				buildquery.append(" FROM ");
				buildquery.append("  pm_service_area psarea, plan p2, ");
				buildquery.append("(SELECT * from plan_health where  (plan_health.plan_level = :param_" + paramList.size() + " ");
				paramList.add(planLevel);
				
				if (planLevel.equalsIgnoreCase("BRONZE")) {
					// HIX-35997
					// For Bronze plans, if exchange type is ON then use CS1 as cost
					// sharing value else CS0
					if (exchangeType.equalsIgnoreCase(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_ON)) {
						buildquery.append("AND plan_health.cost_sharing = 'CS1' )");
					} else {
						buildquery.append("AND plan_health.cost_sharing = 'CS0' )");
					}
				} else {
					buildquery.append("AND plan_health.cost_sharing = :param_" + paramList.size() + " )");
					paramList.add(eligibilityRequest.getCostSharingVariation());				
				}
	
				buildquery.append(") phealth");
				buildquery.append(" WHERE ");
				buildquery.append(" p2.id = phealth.plan_id ");
				buildquery.append(" AND psarea.is_deleted = 'N' ");
				buildquery.append(" AND p2.service_area_id = psarea.service_area_id ");
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append(" AND psarea.zip = to_number(trim(:param_" + paramList.size());
				paramList.add(eligibilityRequest.getZipCode());			
					buildquery.append("))");
				}else{
					buildquery.append(" AND psarea.zip = :param_" + paramList.size());
					paramList.add(eligibilityRequest.getZipCode());			
				}
				
				buildquery.append(" AND psarea.fips = :param_" + paramList.size());
				paramList.add(eligibilityRequest.getCounty());			
				
				buildquery.append(" AND p2.is_deleted = 'N' ");
				buildquery.append(" )");
	
				if (memberCount != modifiedMembers.size()) {
					buildquery.append(" UNION ALL ");
				}
				
			}
	
			buildquery.append(") temp WHERE p.id = temp.plan_id AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') between p.start_date and p.end_date ");
			paramList.add(strEffectiveDate);
			
			buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
			buildquery.append(" AND p.insurance_type = 'HEALTH'  AND p.market = '" + PlanMgmtConstants.PLAN_MARKET_INDIVIDUAL + "' ");
			buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic("p", strEffectiveDate, PlanMgmtConstants.NO, eligibilityRequest.getZipCode(), eligibilityRequest.getCounty(), null, paramList));
			buildquery.append(" AND p.exchange_type = :param_" + paramList.size());
			paramList.add(exchangeType);
	
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildquery.append(" AND p.id = tp.plan_id");
				buildquery.append(" AND tp.tenant_id = t.id");
				buildquery.append(" AND t.code = :param_" + paramList.size() );
				paramList.add(tenantCode.toUpperCase());
			}
			
			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic("p", houseHoldType));
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Query for Plan Ids " + buildquery);
			}
	
			
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
		
			resultList = query.getResultList();
			
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getLowestDentalPlanPremium", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return resultList;
	}

/**
	 * If you created entityManager using EntityManagerFactory you will have
	 * to close it no matter what framework you use. Frameworks have no idea how
	 * you intend to use the EM, so they cannot close it (except, may be, on
	 * finalization, which is not guaranteed). Yes, not closing them would
	 * create a resource leak. The idea is the same as
	 * "always close java.sql.Connection" (despite some data sources have
	 * settings to close them automatically by inactivity) or
	 * "always close Hibernate session". Besides, if you plan to write portable
	 * code, you shouldn't rely on specific JPA provider "being smart" -- other
	 * may fail to close the EM in time.
	 */
	
	private String getHouseHoldType(List<Member> processedMemberData) {
		String houseHoldType = "";
		int childCount = 0;
		int adultCount = 0;
		int totalMemCount = processedMemberData.size();
		for (int i = 0; i < totalMemCount; i++) {
			int memberAge = Integer
					.valueOf(processedMemberData.get(i).getAge());
			if (memberAge < PlanMgmtConstants.TWENTYONE) {
				childCount++;
			} else if (memberAge >= PlanMgmtConstants.TWENTYONE) {
				adultCount++;
			}
		}
		if (childCount == totalMemCount) {
			houseHoldType = CHILD_ONLY;
		} else if (adultCount == totalMemCount) {
			houseHoldType = ADULT_ONLY;
		} else {
			houseHoldType = ADULT_AND_CHILD;
		}
		return houseHoldType;
	}

	@Override
	public EligibilityResponse getEligibilityPremiumAllForTiers(EligibilityRequest eligibilityRequest, String sendAllMetalTier) throws GIException {
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Eligibility Calculator: getEligibilityPremiumForAllMetalTier started");
		}
		EligibilityResponse eligibilityResponse = new EligibilityResponse();
		List<Map<String, String>> requiredPlatinumPlanList = new ArrayList<Map<String, String>>();
		List<Map<String, String>> requiredGoldPlanList = new ArrayList<Map<String, String>>();
		List<Map<String, String>> requiredSilverPlanList = null;
		List<Map<String, String>> requiredBronzePlanList = null;
		List<Member> processedMemberData = eligibilityRequest.getMembers();

		try {
			Map<String, List<Map<String, String>>> planInfoMapWithLevelKey = getMapOfPlanInfoForLevel(eligibilityRequest, sendAllMetalTier);
			Map<Integer, Map<String, Map<String, String>>> benefitsofAllPlans = new HashMap<Integer, Map<String, Map<String, String>>>();
			Map<Integer, Map<String, Map<String, String>>> costofAllPlans = new HashMap<Integer, Map<String, Map<String, String>>> ();
			
			List<Integer> planHealthIdList = getPlanHealthIdString(planInfoMapWithLevelKey);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("planHealthIdString : " + planHealthIdList);
			}
			
			Calendar effectiveDate = eligibilityRequest.getEffectiveDate();
			String strEffectiveDate = DateUtil.getDateAsString(effectiveDate, PlanMgmtConstants.REQUIRED_FORMAT_YYYYMMDD);
			if(!planHealthIdList.isEmpty()){
				// pull plan benefits from DB
				benefitsofAllPlans = planCostAndBenefitsService.getHealthPlanBenefitsForTeaserPlan(planHealthIdList,(strEffectiveDate.substring(0, 10).trim()));
				
				// pull plan cost from DB
				 costofAllPlans = planCostAndBenefitsService.getOptimizedHealthPlanCosts(planHealthIdList);
			}
		
			// if sendAllMetalTierv != Y, then send response for SILVER and
			// BRONZE plans only
			if (sendAllMetalTier != null && sendAllMetalTier.equalsIgnoreCase(PlanMgmtConstants.Y)) {
				requiredPlatinumPlanList = getRequiredPlan(planInfoMapWithLevelKey.get(com.getinsured.hix.model.Plan.PlanLevel.PLATINUM.toString()), benefitsofAllPlans,
											costofAllPlans);
				requiredGoldPlanList = getRequiredPlan(planInfoMapWithLevelKey.get(com.getinsured.hix.model.Plan.PlanLevel.GOLD.toString()), benefitsofAllPlans,
											costofAllPlans);
			}
			requiredSilverPlanList = getRequiredPlan(planInfoMapWithLevelKey.get(com.getinsured.hix.model.Plan.PlanLevel.SILVER.toString()), benefitsofAllPlans, 
											costofAllPlans);
			requiredBronzePlanList = getRequiredPlan(planInfoMapWithLevelKey.get(com.getinsured.hix.model.Plan.PlanLevel.BRONZE.toString()), benefitsofAllPlans, 
											costofAllPlans);
			boolean isLowest = false;
			List<Object[]> nonTobaccoSilverPlans = getSilverNonTobaccoPremiumPlans(eligibilityRequest, processedMemberData);
			TeaserPlanResponse slsntpReponse = createTeaserPlanResponse(nonTobaccoSilverPlans, eligibilityRequest, isLowest);

			List<Object[]> tobaccoSilverPlans = getSilverTobaccoPlans(eligibilityRequest, processedMemberData);
			TeaserPlanResponse slstpReponse = createTeaserPlanResponse(tobaccoSilverPlans, eligibilityRequest, isLowest);

			List<BigDecimal> listOfSilverPlanIds = getPlanLevelSpecificPlanIds(processedMemberData, eligibilityRequest,PhixConstants.SILVER);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Eligibility : listOfSilverPlanIds ::" + listOfSilverPlanIds.toString());
			}

			eligibilityResponse = setDataToResponse(requiredGoldPlanList,requiredSilverPlanList, requiredPlatinumPlanList,requiredBronzePlanList, planInfoMapWithLevelKey,
					slsntpReponse, slstpReponse, listOfSilverPlanIds,tobaccoSilverPlans);
		} catch (Exception ex) {
			LOGGER.error("Exception occured in getEligibilityPremiumForAllMetalTier: ",ex);
		}
		return eligibilityResponse;
	}

	@Transactional(readOnly = true)
	public Map<String, List<Map<String, String>>> getMapOfPlanInfoForLevel(
			EligibilityRequest eligibilityRequest, String sendAllMetalTier) {
		List<Map<String, String>> platinumPlanInfoMap = new ArrayList<Map<String, String>>();
		List<Map<String, String>> goldPlanInfoMap = new ArrayList<Map<String, String>>();
		List<Map<String, String>> bronzePlanInfoMap = new ArrayList<Map<String, String>>();
		List<Map<String, String>> silverPlanInfoMap = new ArrayList<Map<String, String>>();

		Map<String, List<Map<String, String>>> finalPlanInfoMap = new HashMap<String, List<Map<String, String>>>();
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			List<Member> modifiedMembers = eligibilityRequest.getMembers();
			Calendar effectiveDate = eligibilityRequest.getEffectiveDate();
			Date effctiveUtilDate = effectiveDate.getTime();
			String strEffectiveDate = simpleDateFormat.format(effctiveUtilDate);
			String exchangeType = eligibilityRequest.getExchangeFlowType();
			String familyTier = getFamilyTier(modifiedMembers); // compute family tier 
			String familyTierLookupCode = lookupService.getLookupValueCode(FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
			String houseHoldType = getHouseHoldType(modifiedMembers);
			String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
			String tenantCode = (eligibilityRequest.getTenantCode() == null || eligibilityRequest.getTenantCode().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.TENANT_GINS : eligibilityRequest.getTenantCode().trim() ; // default value is GINS

			ArrayList<String> paramList = new ArrayList<String>();
			String applicableYear = strEffectiveDate.substring(0,4);
			
			StringBuilder buildquery = new StringBuilder("SELECT p.id, p.name, ( CASE WHEN UPPER(rate_option) = 'F' THEN (SUM(pre) / " + modifiedMembers.size() + ") ELSE SUM(pre) END) AS total_premium, plan_health_id, plan_level ");			
			buildquery.append("FROM plan p, ");
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildquery.append(" pm_tenant_plan tp, tenant t, ");
			}
			buildquery.append(" ( ");

			int memberCount = 0;
			for (Member member : modifiedMembers) {
				buildquery.append("( SELECT phealth.id AS plan_health_id, phealth.plan_id, prate.rate AS pre, phealth.plan_level AS plan_level, prate.rate_option AS rate_option, 1 AS memberctr ");
				buildquery.append(" FROM ");
				buildquery.append(" plan_health phealth, pm_service_area psarea, pm_plan_rate prate, plan p2 ");

				buildquery.append(" WHERE ");

				// if sendAllMetalTierv != Y, then send response for SILVER and
				// BRONZE plans only
				if (sendAllMetalTier != null && sendAllMetalTier.equalsIgnoreCase(PlanMgmtConstants.Y)) {
					buildquery.append(" (phealth.plan_level IN ('PLATINUM', 'GOLD', 'SILVER', 'BRONZE') AND phealth.cost_sharing = ");
				} else {
					buildquery.append(" (phealth.plan_level IN ('SILVER', 'BRONZE') AND phealth.cost_sharing = ");
				}

				buildquery.append(" CASE WHEN (phealth.plan_level != 'SILVER' AND p2.exchange_type = 'ON') THEN 'CS1' WHEN (phealth.plan_level != 'SILVER' AND p2.exchange_type = 'OFF') THEN 'CS0' ELSE  :param_" + paramList.size() + " END)");
				paramList.add(eligibilityRequest.getCostSharingVariation());
				
				buildquery.append(" ");
				buildquery.append(" AND p2.id = phealth.plan_id ");
				buildquery.append(" AND p2.service_area_id = psarea.service_area_id ");

				boolean isNonTobacco = member.isTobaccoUser();
				if (isNonTobacco) {
					buildquery.append(" AND (prate.tobacco = 'Y' OR prate.tobacco IS NULL )");
				} else {
					buildquery.append("  AND (prate.tobacco = 'N' OR prate.tobacco IS NULL )");
				}
				buildquery.append(" AND prate.rate IS NOT NULL ");
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append(" AND psarea.zip = to_number(trim(:param_" + paramList.size());
				paramList.add(eligibilityRequest.getZipCode());				
					buildquery.append("))");
				}else{
					buildquery.append(" AND psarea.zip = :param_" + paramList.size());
					paramList.add(eligibilityRequest.getZipCode());
				}
				
				buildquery.append(" AND psarea.fips = :param_" + paramList.size());
				paramList.add(eligibilityRequest.getCounty());				
				
				buildquery.append(" AND p2.id = prate.plan_id ");
				//***fixing the performance issue
				//buildquery.append(" AND pzcrarea.rating_area_id = prate.rating_area_id ");
				buildquery.append("  AND prate.rating_area_id = ");
				
				// -------- changes for HIX-100092 starts --------
				buildquery.append(buildSearchRatingAreaLogic(paramList, eligibilityRequest.getZipCode(), eligibilityRequest.getCounty(), applicableYear));
				// -------- changes for HIX-100092 ends --------
				
					buildquery.append(" AND ( (CAST (:param_" + paramList.size() + " AS INTEGER) >= prate.min_age AND ");
					paramList.add(String.valueOf(member.getAge()));

					buildquery.append(" CAST (:param_" + paramList.size() + " AS INTEGER) <= prate.max_age) ");
					paramList.add(String.valueOf(member.getAge()));

					buildquery.append(" OR (CAST (:param_" + paramList.size() + " AS INTEGER) >=  prate.min_age AND ");
					paramList.add(familyTierLookupCode);

					buildquery.append(" CAST (:param_" + paramList.size() + " AS INTEGER) <= prate.max_age) ) ");
					paramList.add(familyTierLookupCode);

				buildquery.append(" AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date ");
				paramList.add(strEffectiveDate);				
				
				buildquery.append(" AND p2.is_deleted = 'N' ");
				buildquery.append(" AND prate.is_deleted = 'N' ");
				buildquery.append(" AND psarea.is_deleted = 'N' ");
				buildquery.append(" )");

				if (memberCount + 1 != modifiedMembers.size()) {
					buildquery.append(" UNION ALL ");
				}
				memberCount++;
			}
			
			buildquery.append(") temp WHERE p.id = temp.plan_id AND TO_DATE (:param_" + paramList.size()  + ", 'YYYY-MM-DD') between p.start_date and p.end_date ");
			paramList.add(strEffectiveDate);
			
			buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
			buildquery.append(" AND p.insurance_type='HEALTH' AND p.market = '" + PlanMgmtConstants.PLAN_MARKET_INDIVIDUAL + "' ");
			buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic("p", strEffectiveDate, PlanMgmtConstants.NO, eligibilityRequest.getZipCode(), eligibilityRequest.getCounty(), null, paramList));
			buildquery.append(" AND p.exchange_type = :param_" + paramList.size());
			paramList.add(exchangeType);

			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildquery.append(" AND p.id = tp.plan_id");
				buildquery.append(" AND tp.tenant_id = t.id");
				buildquery.append(" AND t.code = :param_" + paramList.size() );
				paramList.add(tenantCode.toUpperCase());
			}

			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic("p", houseHoldType));
			buildquery.append(" GROUP BY p.id, p.name, plan_health_id, plan_level, rate_option ");
			buildquery.append("HAVING sum(memberctr) = " + modifiedMembers.size());
			
			buildquery.append(" ORDER BY total_premium asc");
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("getMapOfPlanInfoForLevel_Query: " + buildquery);
			}
			
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
			Object[] planInfoArray = null;
			Map<String, String> planInfoMap = null;
			
			while (rsIterator.hasNext()) {
				planInfoArray = (Object[]) rsIterator.next();
				planInfoMap = new HashMap<String, String>();
				planInfoMap
						.put(PlanMgmtConstants.PLANID,
								planInfoArray[PlanMgmtConstants.ZERO]
										.toString() == null ? PlanMgmtConstants.ZERO_STR
										: planInfoArray[PlanMgmtConstants.ZERO]
												.toString());
				 planInfoMap.
				 			put(PlanMgmtConstants.PLANHEALTHID,
				 					planInfoArray[PlanMgmtConstants.THREE]
				 							.toString() == null ? PlanMgmtConstants.ZERO_STR 
				 							: planInfoArray[PlanMgmtConstants.THREE]
				 									.toString());
				planInfoMap
						.put(PlanMgmtConstants.PLANNAME,
								planInfoArray[PlanMgmtConstants.ONE].toString() == null ? PlanMgmtConstants.EMPTY_STRING
										: planInfoArray[PlanMgmtConstants.ONE]
												.toString());
				planInfoMap
						.put(PlanMgmtConstants.TOTALPREMIUM,
								planInfoArray[PlanMgmtConstants.TWO]
										.toString() == null ? PlanMgmtConstants.EMPTY_STRING
										: planInfoArray[PlanMgmtConstants.TWO]
												.toString());
				planInfoMap
						.put(PlanMgmtConstants.PLANLEVEL,
								planInfoArray[PlanMgmtConstants.FOUR]
										.toString() == null ? PlanMgmtConstants.EMPTY_STRING
										: planInfoArray[PlanMgmtConstants.FOUR]
												.toString());
				if (com.getinsured.hix.model.Plan.PlanLevel.PLATINUM.toString()
						.equalsIgnoreCase(
								planInfoMap.get(PlanMgmtConstants.PLANLEVEL)
										.toString())) {
					platinumPlanInfoMap.add(planInfoMap);

				}

				if (com.getinsured.hix.model.Plan.PlanLevel.GOLD.toString()
						.equalsIgnoreCase(
								planInfoMap.get(PlanMgmtConstants.PLANLEVEL)
										.toString())) {
					goldPlanInfoMap.add(planInfoMap);

				}
				if (com.getinsured.hix.model.Plan.PlanLevel.SILVER.toString()
						.equalsIgnoreCase(
								planInfoMap.get(PlanMgmtConstants.PLANLEVEL)
										.toString())) {
					silverPlanInfoMap.add(planInfoMap);

				}
				if (com.getinsured.hix.model.Plan.PlanLevel.BRONZE.toString()
						.equalsIgnoreCase(
								planInfoMap.get(PlanMgmtConstants.PLANLEVEL)
										.toString())) {
					bronzePlanInfoMap.add(planInfoMap);

				}
			}
			finalPlanInfoMap
					.put(com.getinsured.hix.model.Plan.PlanLevel.PLATINUM
							.toString(), platinumPlanInfoMap);
			finalPlanInfoMap.put(
					com.getinsured.hix.model.Plan.PlanLevel.GOLD.toString(),
					goldPlanInfoMap);
			finalPlanInfoMap.put(
					com.getinsured.hix.model.Plan.PlanLevel.SILVER.toString(),
					silverPlanInfoMap);
			finalPlanInfoMap.put(
					com.getinsured.hix.model.Plan.PlanLevel.BRONZE.toString(),
					bronzePlanInfoMap);

		} catch (Exception ex) {
			LOGGER.error("Exception Occured in getMapOfPlanInfoForLevel: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}

		return finalPlanInfoMap;
	}

	private List<Map<String, String>> getRequiredPlan(
			List<Map<String, String>> planList,
			Map<Integer, Map<String, Map<String, String>>> temp_benefitsofAllPlans,
			Map<Integer, Map<String, Map<String, String>>> temp_costofAllPlans) {
		List<Map<String, String>> requiredPlan = new ArrayList<Map<String, String>>();
		try {
			if (planList != null && !(planList.isEmpty())) {
				requiredPlan.add(planList.get(PlanMgmtConstants.ZERO));
				requiredPlan.add(planList.get((planList.size() - 1)));
			}
			if (requiredPlan != null && !(requiredPlan.isEmpty())) {
				requiredPlan = addBenefitsToPlan(requiredPlan,temp_benefitsofAllPlans);
				requiredPlan = addCostToPlan(requiredPlan, temp_costofAllPlans);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception Occured in getRequiredPlan: ", ex);
		}
		return requiredPlan;
	}

	private List<Map<String, String>> addCostToPlan(List<Map<String, String>> planMapList,Map<Integer, Map<String, Map<String, String>>> temp_costofAllPlans) {
		Map<Integer, Map<String, Map<String, String>>> costofAllPlans = temp_costofAllPlans;
		List<Map<String, String>> modifiedPlanMapList = new ArrayList<Map<String, String>>();

		String deductibleCost = PlanMgmtConstants.EMPTY_STRING;
		String deductibleCostFamily = PlanMgmtConstants.EMPTY_STRING;
		String maxOopCost = PlanMgmtConstants.EMPTY_STRING;
		String maxOopCostFamily = PlanMgmtConstants.EMPTY_STRING;
		try {
			for (Map<String, String> planMap : planMapList) {
				if (costofAllPlans.containsKey(Integer.parseInt(planMap.get(PlanMgmtConstants.PLANHEALTHID)))) {
					Map<String, Map<String, String>> costMapForPlanHealth = costofAllPlans.get(Integer.parseInt(planMap.get(PlanMgmtConstants.PLANHEALTHID)));
					if (costMapForPlanHealth.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL) != null) {
						deductibleCost = costMapForPlanHealth.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL).get(PlanMgmtConstants.IN_NETWORK_IND);
						deductibleCostFamily = costMapForPlanHealth.get(PlanMgmtConstants.DEDUCTIBLE_MEDICAL).get(PlanMgmtConstants.IN_NETWORK_FLY);
					} else if (costMapForPlanHealth.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG) != null) {
						deductibleCost = costMapForPlanHealth.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG).get(PlanMgmtConstants.IN_NETWORK_IND);
						deductibleCostFamily = costMapForPlanHealth.get(PlanMgmtConstants.DEDUCTIBLE_INTG_MED_DRUG).get(PlanMgmtConstants.IN_NETWORK_FLY);
					}

					// if MAX_OOP_MEDICAL benefit does not exists look for MAX_OOP_INTG_MED_DRUG
					if (costMapForPlanHealth.get(PlanMgmtConstants.MAX_OOP_MEDICAL) != null) {
						maxOopCost = costMapForPlanHealth.get(PlanMgmtConstants.MAX_OOP_MEDICAL).get(PlanMgmtConstants.IN_NETWORK_IND);
						maxOopCostFamily = costMapForPlanHealth.get(PlanMgmtConstants.MAX_OOP_MEDICAL).get(PlanMgmtConstants.IN_NETWORK_FLY);
					} else if (costMapForPlanHealth.get(PlanMgmtConstants.MAX_OOP_INTG_MED_DRUG) != null) {
						maxOopCost = costMapForPlanHealth.get(PlanMgmtConstants.MAX_OOP_INTG_MED_DRUG).get(PlanMgmtConstants.IN_NETWORK_IND);
						maxOopCostFamily = costMapForPlanHealth.get(PlanMgmtConstants.MAX_OOP_INTG_MED_DRUG).get(PlanMgmtConstants.IN_NETWORK_FLY);
					}
				}
				planMap.put(PlanMgmtConstants.OOP_MAX_IND, maxOopCost);
				planMap.put(PlanMgmtConstants.OOP_MAX_FLY, maxOopCostFamily);
				planMap.put(PlanMgmtConstants.DEDUCTIBLE_IND, deductibleCost);
				planMap.put(PlanMgmtConstants.DEDUCTIBLE_FLY,
						deductibleCostFamily);
				modifiedPlanMapList.add(planMap);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception Occured in addCostToPlan: ", ex);
		}

		return modifiedPlanMapList;
	}

	private List<Map<String, String>> addBenefitsToPlan(List<Map<String, String>> planMapList,Map<Integer, Map<String, Map<String, String>>> temp_benefitsofAllPlans) {
		
		Map<Integer, Map<String, Map<String, String>>> benefitsofAllPlans = temp_benefitsofAllPlans;
		List<Map<String, String>> modifiedPlanMapList = new ArrayList<Map<String, String>>();
		try {
			if (!(planMapList.isEmpty())) {
				for (Map<String, String> planMap : planMapList) {
					if (benefitsofAllPlans.containsKey(Integer.parseInt(planMap.get(PlanMgmtConstants.PLANHEALTHID)))) {
						Map<String, Map<String, String>> benefitMapForPlanHealth = benefitsofAllPlans.get(Integer.parseInt(planMap.get(PlanMgmtConstants.PLANHEALTHID)));
						if (benefitMapForPlanHealth.get(PlanMgmtConstants.PRIMARY_VISIT) != null) {
							planMap.put(PlanMgmtConstants.PRIMARY_VISIT,benefitMapForPlanHealth.get(PlanMgmtConstants.PRIMARY_VISIT).get(PlanMgmtConstants.NETWORK_T1_DISPLAY));
						}
						if (benefitMapForPlanHealth.get(PlanMgmtConstants.GENERIC) != null) {
							planMap.put(PlanMgmtConstants.GENERIC,benefitMapForPlanHealth.get(PlanMgmtConstants.GENERIC).get(PlanMgmtConstants.NETWORK_T1_DISPLAY));
						}
					}
					modifiedPlanMapList.add(planMap);
				}
			}
		} catch (Exception ex) {
			LOGGER.info("Exception occured in addBenefitsToPlan: ", ex);
		}
		return modifiedPlanMapList;
	}

	private ArrayList<Integer> getPlanHealthIdString( Map<String, List<Map<String, String>>> planInfoMapWithLevelKey) {
		ArrayList<Integer> planHealthIdList = new ArrayList<Integer>();
		try {
			List<Map<String, String>> platinumPlanList = planInfoMapWithLevelKey .get(com.getinsured.hix.model.Plan.PlanLevel.PLATINUM.toString());
			List<Map<String, String>> goldPlanList = planInfoMapWithLevelKey.get(com.getinsured.hix.model.Plan.PlanLevel.GOLD.toString());
			List<Map<String, String>> silverPlanList = planInfoMapWithLevelKey.get(com.getinsured.hix.model.Plan.PlanLevel.SILVER.toString());
			List<Map<String, String>> bronzePlanList = planInfoMapWithLevelKey.get(com.getinsured.hix.model.Plan.PlanLevel.BRONZE.toString());

			if (!(platinumPlanList.isEmpty())) {
				List<Map<String, String>> requiredPlatinumPlanList = new ArrayList<Map<String, String>>();
				requiredPlatinumPlanList.add(platinumPlanList.get(PlanMgmtConstants.ZERO));
				requiredPlatinumPlanList.add(platinumPlanList.get((platinumPlanList.size() - 1)));
				List<Integer> temp_planHealthIdList = createPlanHealthIDString(requiredPlatinumPlanList);
				planHealthIdList.addAll(temp_planHealthIdList);
			}
			if (!(goldPlanList.isEmpty())) {
				List<Map<String, String>> requiredGoldList = new ArrayList<Map<String, String>>();
				requiredGoldList.add(goldPlanList.get(PlanMgmtConstants.ZERO));
				requiredGoldList.add(goldPlanList.get((goldPlanList.size() - 1)));
				List<Integer> temp_planHealthIdList = createPlanHealthIDString(requiredGoldList);
				planHealthIdList.addAll(temp_planHealthIdList);
			}
			if (!(silverPlanList.isEmpty())) {
				List<Map<String, String>> requiredSilverList = new ArrayList<Map<String, String>>();
				requiredSilverList.add(silverPlanList.get(PlanMgmtConstants.ZERO));
				requiredSilverList.add(silverPlanList.get((silverPlanList.size() - 1)));
				List<Integer> temp_planHealthIdList = createPlanHealthIDString(requiredSilverList);
				planHealthIdList.addAll(temp_planHealthIdList);
			}
			if (!(bronzePlanList.isEmpty())) {
				List<Map<String, String>> requiredBronzeList = new ArrayList<Map<String, String>>();
				requiredBronzeList.add(bronzePlanList.get(PlanMgmtConstants.ZERO));
				requiredBronzeList.add(bronzePlanList.get((bronzePlanList.size() - 1)));
				List<Integer> temp_planHealthIdList = createPlanHealthIDString(requiredBronzeList);
				planHealthIdList.addAll(temp_planHealthIdList);
			}
		} catch (Exception ex) {
			LOGGER.error("Exceptin occured in getPlanHealthIdString: ", ex);
		}
		return planHealthIdList;
	}

	private List<Integer> createPlanHealthIDString(
			List<Map<String, String>> testList) {
		ArrayList<Integer> planHealthIdString = new ArrayList<Integer>();
		try {
			for (Map<String, String> tempMap : testList) {
					planHealthIdString.add((Integer.parseInt(tempMap.get(PlanMgmtConstants.PLANHEALTHID))));
			}
		} catch (Exception ex) {
			LOGGER.error("Exceptin occured in createPlanHealthIDString: ", ex);
		}
		return planHealthIdString;
	}

	private EligibilityResponse setDataToResponse(
			List<Map<String, String>> temp_requiredGoldPlanList,
			List<Map<String, String>> temp_requiredSilverPlanList,
			List<Map<String, String>> temp_requiredPlatinumPlanList,
			List<Map<String, String>> temp_requiredBronzePlanList,
			Map<String, List<Map<String, String>>> temp_planInfoMapWithLevelKey,
			TeaserPlanResponse temp_slsntpReponse,
			TeaserPlanResponse temp_slstpReponse,
			List<BigDecimal> listOfSilverPlanIds,
			List<Object[]> tobaccoSilverPlans) {
		EligibilityResponse eligibilityResponse = new EligibilityResponse();
		Map<String, String> lowestMap = null;
		Map<String, String> highestMap = null;
		Map<String, HashMap<String, String>> lowestInfoMap = null;
		Map<String, HashMap<String, String>> highestInfoMap = null;

		Map<String, HashMap<String, HashMap<String, String>>> plansInformation = null;
		List<Map<String, String>> requiredGoldPlanList = temp_requiredGoldPlanList;
		List<Map<String, String>> requiredSilverPlanList = temp_requiredSilverPlanList;
		List<Map<String, String>> requiredPlatinumPlanList = temp_requiredPlatinumPlanList;
		List<Map<String, String>> requiredBronzePlanList = temp_requiredBronzePlanList;

		Map<String, List<Map<String, String>>> planInfoMapWithLevelKey = temp_planInfoMapWithLevelKey;
		TeaserPlanResponse slsntpReponse = temp_slsntpReponse;
		TeaserPlanResponse slstpReponse = temp_slstpReponse;

		try {
		
			List<Map<String, String>> bronzePlanList = planInfoMapWithLevelKey
					.get(com.getinsured.hix.model.Plan.PlanLevel.BRONZE
							.toString());
			plansInformation = new HashMap<String, HashMap<String, HashMap<String, String>>>();

			if (slsntpReponse != null) {
				updateSilverPlansAvailableInfo(listOfSilverPlanIds,
						tobaccoSilverPlans, eligibilityResponse);
				eligibilityResponse.setBenchmarkPremium(slsntpReponse
						.getBenchmarkPremium());
				eligibilityResponse.setBenchmarkPremiumTobacco(slstpReponse
						.getBenchmarkPremium());
			} else {
				eligibilityResponse
						.setBenchmarkPremium(PlanMgmtConstants.ZERO_PREMIUM);
				eligibilityResponse
						.setBenchmarkPremiumTobacco(PlanMgmtConstants.ZERO_PREMIUM);
			}

			
			if (!(listOfSilverPlanIds.isEmpty())) {
				updateSilverPlansAvailableInfo(listOfSilverPlanIds,
						tobaccoSilverPlans, eligibilityResponse);
			}
			
			// ref HIX-56505 
			// if isAllAvailablePlansSilver == true, then send response 
			if(this.isAllAvailablePlansSilver){

				if (bronzePlanList != null && !(bronzePlanList.isEmpty())) {
					eligibilityResponse.setAllPlansAvailableBronze(true);
					eligibilityResponse.setNumberBronzeAvailable(bronzePlanList
							.size());
				} else {
					eligibilityResponse.setAllPlansAvailableBronze(false);
					eligibilityResponse
							.setNumberBronzeAvailable(PlanMgmtConstants.ZERO);
				}
	
				if (requiredPlatinumPlanList != null && !(requiredPlatinumPlanList.isEmpty())) {
					/* Lowest Premium for Platinum Plan, GILPP */
					lowestMap = new HashMap<String, String>();
					lowestInfoMap = new HashMap<String, HashMap<String, String>>();
					lowestMap = requiredPlatinumPlanList.get(0);
					lowestInfoMap = getHashMapValue(lowestMap);
					plansInformation
							.put(PlanMgmtConstants.GILPP,
									(HashMap<String, HashMap<String, String>>) lowestInfoMap);
	
					/* Highest Premium for Platinum Plan, GIHPP */
					highestMap = new HashMap<String, String>();
					highestInfoMap = new HashMap<String, HashMap<String, String>>();
					highestMap = requiredPlatinumPlanList.get(1);
					highestInfoMap = getHashMapValue(highestMap);
					plansInformation
							.put(PlanMgmtConstants.GIHPP,
									(HashMap<String, HashMap<String, String>>) highestInfoMap);
				}
	
				if (requiredGoldPlanList != null && !(requiredGoldPlanList.isEmpty())) {
					/* Lowest Premium for Gold Plan, GILGP */
					lowestMap = new HashMap<String, String>();
					lowestInfoMap = new HashMap<String, HashMap<String, String>>();
					lowestMap = requiredGoldPlanList.get(0);
					lowestInfoMap = getHashMapValue(lowestMap);
					plansInformation
							.put(PlanMgmtConstants.GILGP,
									(HashMap<String, HashMap<String, String>>) lowestInfoMap);
	
					/* Highest Premium for Gold Plan, GIHGP */
					highestMap = new HashMap<String, String>();
					highestInfoMap = new HashMap<String, HashMap<String, String>>();
					highestMap = requiredGoldPlanList.get(1);
					highestInfoMap = getHashMapValue(highestMap);
					plansInformation
							.put(PlanMgmtConstants.GIHGP,
									(HashMap<String, HashMap<String, String>>) highestInfoMap);
				}
	
				if (requiredSilverPlanList != null && !(requiredSilverPlanList.isEmpty())) {
					/* Lowest Premium for Silver Plan, GILSP */
					lowestMap = new HashMap<String, String>();
					lowestInfoMap = new HashMap<String, HashMap<String, String>>();
					lowestMap = requiredSilverPlanList.get(0);
					lowestInfoMap = getHashMapValue(lowestMap);
					plansInformation
							.put(PlanMgmtConstants.GILSP,
									(HashMap<String, HashMap<String, String>>) lowestInfoMap);
	
					/* Highest Premium for Silver Plan, GIHSP */
					highestMap = new HashMap<String, String>();
					highestInfoMap = new HashMap<String, HashMap<String, String>>();
					highestMap = requiredSilverPlanList.get(1);
					highestInfoMap = getHashMapValue(highestMap);
					plansInformation
							.put(PlanMgmtConstants.GIHSP,
									(HashMap<String, HashMap<String, String>>) highestInfoMap);
				}
	
				if (requiredBronzePlanList != null && !(requiredBronzePlanList.isEmpty())) {
					/* Lowest Premium for Bronze Plan, GILBP */
					lowestMap = new HashMap<String, String>();
					lowestInfoMap = new HashMap<String, HashMap<String, String>>();
					lowestMap = requiredBronzePlanList.get(0);
					lowestInfoMap = getHashMapValue(lowestMap);
					plansInformation
							.put(PlanMgmtConstants.GILBP,
									(HashMap<String, HashMap<String, String>>) lowestInfoMap);
	
					/* Highest Premium for Bronze Plan, GIHBP */
					highestMap = new HashMap<String, String>();
					highestInfoMap = new HashMap<String, HashMap<String, String>>();
					highestMap = requiredBronzePlanList.get(1);
					highestInfoMap = getHashMapValue(highestMap);
					plansInformation
							.put(PlanMgmtConstants.GIHBP,
									(HashMap<String, HashMap<String, String>>) highestInfoMap);
				}
				
			}	
			eligibilityResponse.setPlansInformation(plansInformation);
		} catch (Exception ex) {
			LOGGER.error("Exception Occurred in setDataToResponse: ", ex);
		}

		return eligibilityResponse;
	}

	private Map<String, HashMap<String, String>> getHashMapValue(
			Map<String, String> temp_PlanMap) {
		Map<String, String> lowestMap = temp_PlanMap;
		Map<String, HashMap<String, String>> lowestInfoMap = new HashMap<String, HashMap<String, String>>();
		try {
			Map<String, String> innerMap1 = new HashMap<String, String>();
			innerMap1
					.put(PlanMgmtConstants.PLAN_NAME_KEY,
							lowestMap.get(PlanMgmtConstants.PLANNAME) == null ? PlanMgmtConstants.EMPTY_STRING
									: lowestMap.get(PlanMgmtConstants.PLANNAME));
			lowestInfoMap.put(PlanMgmtConstants.PLANNAME,
					(HashMap<String, String>) innerMap1);

			Map<String, String> innerMap2 = new HashMap<String, String>();
			innerMap2
					.put(PlanMgmtConstants.MONTHLY_PREMIUM_KEY,
							lowestMap.get(PlanMgmtConstants.TOTALPREMIUM) == null ? PlanMgmtConstants.EMPTY_STRING
									: lowestMap
											.get(PlanMgmtConstants.TOTALPREMIUM));
			lowestInfoMap.put(PlanMgmtConstants.MONTHLY_PREMIUM,
					(HashMap<String, String>) innerMap2);

			Map<String, String> innerMap3 = new HashMap<String, String>();
			innerMap3
					.put(PlanMgmtConstants.OOP_MAX_IND_KEY,
							lowestMap.get(PlanMgmtConstants.OOP_MAX_IND) == null ? PlanMgmtConstants.EMPTY_STRING
									: lowestMap
											.get(PlanMgmtConstants.OOP_MAX_IND));
			lowestInfoMap.put(PlanMgmtConstants.OOP_MAX_IND,
					(HashMap<String, String>) innerMap3);

			Map<String, String> innerMap4 = new HashMap<String, String>();
			innerMap4
					.put(PlanMgmtConstants.OOP_MAX_FLY_KEY,
							lowestMap.get(PlanMgmtConstants.OOP_MAX_FLY) == null ? PlanMgmtConstants.EMPTY_STRING
									: lowestMap
											.get(PlanMgmtConstants.OOP_MAX_FLY));
			lowestInfoMap.put(PlanMgmtConstants.OOP_MAX_FLY,
					(HashMap<String, String>) innerMap4);

			Map<String, String> innerMap5 = new HashMap<String, String>();
			innerMap5
					.put(PlanMgmtConstants.DEDUCTIBLE_IND_KEY,
							lowestMap.get(PlanMgmtConstants.DEDUCTIBLE_IND) == null ? PlanMgmtConstants.EMPTY_STRING
									: lowestMap
											.get(PlanMgmtConstants.DEDUCTIBLE_IND));
			lowestInfoMap.put(PlanMgmtConstants.DEDUCTIBLE_IND,
					(HashMap<String, String>) innerMap5);

			Map<String, String> innerMap6 = new HashMap<String, String>();
			innerMap6
					.put(PlanMgmtConstants.DEDUCTIBLE_FLY_KEY,
							lowestMap.get(PlanMgmtConstants.DEDUCTIBLE_FLY) == null ? PlanMgmtConstants.EMPTY_STRING
									: lowestMap
											.get(PlanMgmtConstants.DEDUCTIBLE_FLY));
			lowestInfoMap.put(PlanMgmtConstants.DEDUCTIBLE_FLY,
					(HashMap<String, String>) innerMap6);

			Map<String, String> innerMap7 = new HashMap<String, String>();
			innerMap7
					.put(PlanMgmtConstants.GENERIC_KEY,
							lowestMap.get(PlanMgmtConstants.GENERIC) == null ? PlanMgmtConstants.EMPTY_STRING
									: lowestMap.get(PlanMgmtConstants.GENERIC));
			lowestInfoMap.put(PlanMgmtConstants.GENERIC,
					(HashMap<String, String>) innerMap7);

			Map<String, String> innerMap8 = new HashMap<String, String>();
			innerMap8
					.put(PlanMgmtConstants.PRIMARY_VISIT_KEY,
							lowestMap.get(PlanMgmtConstants.PRIMARY_VISIT) == null ? PlanMgmtConstants.EMPTY_STRING
									: lowestMap
											.get(PlanMgmtConstants.PRIMARY_VISIT));
			lowestInfoMap.put(PlanMgmtConstants.PRIMARY_VISIT,
					(HashMap<String, String>) innerMap8);
		} catch (Exception ex) {
			LOGGER.error("Exception Occurred in getHashMapValue: ", ex);
		}
		return lowestInfoMap;
	}

	
	private String buildSearchRatingAreaLogic(ArrayList<String> paramList, String zipCode, String countyCode, String applicableYear){
		StringBuilder buildquery = new StringBuilder(1024);
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		LOGGER.info("applicableYear " + applicableYear);
		
		// if state exchange is ID, then apply applicable year filter on 'pm_zip_county_rating_area' table  
		if(PlanMgmtConstants.STATE_CODE_ID.equalsIgnoreCase(stateCode))
		{
			buildquery.append(" (SELECT rAreaId ");
			buildquery.append("FROM ");
			buildquery.append("(SELECT prarea2.id AS rAreaId, row_number() OVER (ORDER by pzcrarea.applicable_year DESC) AS seqnum ");
			buildquery.append("FROM ");
			buildquery.append("pm_zip_county_rating_area pzcrarea ");
			buildquery.append("LEFT JOIN pm_rating_area prarea2 ");
			buildquery.append("ON pzcrarea.rating_area_id = prarea2.id ");
			buildquery.append("WHERE ");

			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("(pzcrarea.zip = to_number(trim(:param_" + paramList.size() + ")) OR pzcrarea.zip IS NULL) ");
				paramList.add(zipCode);
			}else{
				buildquery.append("(pzcrarea.zip = :param_" + paramList.size() + " OR pzcrarea.zip IS NULL) ");
				paramList.add(zipCode);
			}
			
			if (!StringUtils.isBlank(countyCode)) {
				buildquery.append("AND pzcrarea.county_fips = :param_" + paramList.size());
				paramList.add(countyCode);
			}	

			buildquery.append(" AND pzcrarea.applicable_year <= :param_" + paramList.size());
			paramList.add(applicableYear);
			
			buildquery.append(") rowline WHERE seqnum=1 ) ");
		}
		else
		{
			buildquery.append(" (SELECT prarea2.id ");
			buildquery.append("FROM ");
			buildquery.append("pm_zip_county_rating_area pzcrarea ");
			buildquery.append("LEFT JOIN pm_rating_area prarea2 ");
			buildquery.append("ON pzcrarea.rating_area_id = prarea2.id ");
			buildquery.append("WHERE ");

			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("(pzcrarea.zip = to_number(trim(:param_" + paramList.size() + ")) OR pzcrarea.zip IS NULL) ");
				paramList.add(zipCode);
			}else{
				buildquery.append("(pzcrarea.zip = :param_" + paramList.size() + " OR pzcrarea.zip IS NULL) ");
				paramList.add(zipCode);
			}
			
			if (!StringUtils.isBlank(countyCode)) {
				buildquery.append("AND pzcrarea.county_fips = :param_" + paramList.size());
				paramList.add(countyCode);
			}	
			
			buildquery.append(") ");
		}
		
		return buildquery.toString();
	}
	
}
