package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.RatingArea;

public interface RatingAreaRepository extends JpaRepository<RatingArea, Integer> {
	@Query("SELECT ra FROM RatingArea ra WHERE ra.ratingArea=:strRatingArea")
	RatingArea getRatingAreaByName(@Param("strRatingArea") String strRatingArea);
	
	@Query("SELECT ra FROM RatingArea ra, ZipCountyRatingArea zipcra WHERE zipcra.ratingArea = ra.id AND zipcra.zip=:strZip AND zipcra.countyFips=:countyFips")	
	RatingArea getRatingAreaByZip(@Param("strZip") String strZip, @Param("countyFips") String countyFips);
	
	@Query("SELECT DISTINCT ra FROM RatingArea ra, ZipCountyRatingArea zipcra WHERE zipcra.ratingArea = ra.id AND ra.state=:stateCode AND zipcra.countyFips=:countyFips")	
	RatingArea getRatingAreaByStateCodeAndCountyFips(@Param("stateCode") String stateCode, @Param("countyFips") String countyFips);
	
}
