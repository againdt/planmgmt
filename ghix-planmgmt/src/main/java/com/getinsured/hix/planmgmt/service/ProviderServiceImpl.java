package com.getinsured.hix.planmgmt.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;

//import javax.persistence.PersistenceUnit;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;



@Service("ProviderService")
@Repository
public class ProviderServiceImpl implements ProviderService{

	private static final Logger LOGGER = Logger.getLogger(ProviderServiceImpl.class);
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	@Override
	public List<Map<String, String>> isPlanBelongsToProviderNetwork(List<Map<String, List<String>>> providerList, String planNetworkKey, String hasProviderData, String networkVerificationStatus, String stateCode) {
		List<Map<String, String>> modifiedProviders = new ArrayList<Map<String, String>>();	
		Map<String, String> providerMapTemp  = null;
		List<String> providerNetworkKeyList =null;
		String networkStatus = null;
		
		try{
			for (Map<String, List<String>> providerMap : providerList) {		
				for (String providerId: providerMap.keySet()) { // parsing provider list 
					providerMapTemp = new HashMap<String, String>();			
					providerNetworkKeyList = providerMap.get(providerId); // get provider network list of each provider.   
					networkStatus = PlanMgmtConstants.OUTNETWORK; // default network status is OUT_OF_NETWORK
					
					// Ref jira HIX-87747
					if(PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode)){
						// if state exchange is CA then use different logic. 
						// don't use hasProviderData, networkVerificationStatus bits
						if(null != providerNetworkKeyList){
						    for(String providerNetworkKey : providerNetworkKeyList){	
						    	// if plan's network key match with provider network key, set  network status is IN_NETWORK
						    	if(providerNetworkKey.equalsIgnoreCase(planNetworkKey)){ 
						    		networkStatus =  PlanMgmtConstants.INNETWORK;
						    		break;
						    	}
						    }
						}    
						
					}else{
						// if exchange is PHIX 
						// single provider could belongs to multiple network but single plan always belongs to only one network.
					    // if provider network key match with plan network key and network have provider data 
					    // then set plan belongs to in-network
					    // if provider network key  match with plan network key and network doesn't have provider data 
					    // then set plan belongs to unknown affiliation 	
						if(providerNetworkKeyList != null){
						    for(String providerNetworkKey : providerNetworkKeyList){			    	
						    	if(providerNetworkKey.equalsIgnoreCase(planNetworkKey)){
						    		 // HIX-76044, if provider network verified/certified by admin
								    // then set network status as computed "networkStatus" 
								    // else set network status as "UNKNOWN_AFFILIATION"
						    		if(PlanMgmtConstants.YES.equalsIgnoreCase(networkVerificationStatus)){
						    			networkStatus = (hasProviderData.equalsIgnoreCase(PlanMgmtConstants.YES)) ? PlanMgmtConstants.INNETWORK : PlanMgmtConstants.UNKNOWN_AFFILIATION;
						    		}else{
						    			networkStatus = PlanMgmtConstants.UNKNOWN_AFFILIATION;
						    		}
						    	}
						    }
						}  
					    
					    // ** info ** -> if requested provider network key doesn't match with plan's network key, network status remains default i.e OUT_OF_NETWORK
						// if computed "networkStatus" is OUT_OF_NETWORK
					    if(networkStatus.equalsIgnoreCase(PlanMgmtConstants.OUTNETWORK)){
					    	// if plan's network have hasProviderData = NO, then set network status as "UNKNOWN_AFFILIATION"
					    	if(hasProviderData.equalsIgnoreCase(PlanMgmtConstants.NO)){
					    		networkStatus = PlanMgmtConstants.UNKNOWN_AFFILIATION;
					    	}// if plan's network have hasProviderData = YES and networkVerificationStatus = NO then set network status as "UNKNOWN_AFFILIATION", ref HIX-81033
					    	else if(hasProviderData.equalsIgnoreCase(PlanMgmtConstants.YES) && PlanMgmtConstants.NO.equalsIgnoreCase(networkVerificationStatus)){
					    		networkStatus = PlanMgmtConstants.UNKNOWN_AFFILIATION;
					    	}
					    }
					}
					
				    providerMapTemp.put(PlanMgmtConstants.PROVIDER_ID, providerId);
				    providerMapTemp.put(PlanMgmtConstants.NETWORK_STATUS, networkStatus);
				    modifiedProviders.add(providerMapTemp);
				}
			}
		}catch(Exception ex){
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in isPlanBelongsToProviderNetwork ", ex);
			LOGGER.error("Exception occured in isPlanBelongsToProviderNetwork: ", ex);
		}
		
		return modifiedProviders;
	}
	
	
	// get list of network keys for each strenuss id
	@Override
	@Transactional(readOnly = true)
	public List<Map<String, List<String>>>  getNetworkListByStrenussId(List<String> strenussIdList){		
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Map<String, List<String>>> providerNetworkList =  new ArrayList<Map<String, List<String>>>();
		
		
		for(String strenussId : strenussIdList){						
			Map<String, List<String>> providerIdList = new HashMap<String, List<String>>();
			List<String> networkList =  new ArrayList<String>();
				
			StringBuilder buildquery = new StringBuilder();
			buildquery.append("SELECT DISTINCT pm.network_key ");
			buildquery.append("FROM ");
			buildquery.append("provider_product pp, provider_mapping pm ");
			buildquery.append("WHERE ");
			buildquery.append("CAST(pp.product_id AS NUMERIC) = pm.product_id ");
			buildquery.append("AND pp.strenuus_id = CAST(? AS NUMERIC) ");
			buildquery.append("ORDER BY ");
			buildquery.append("pm.network_key ");	
		
			LOGGER.debug("SQL===" + buildquery.toString());
		
			try {
	            
	            Context initialContext = new InitialContext();
			    DataSource datasource = (DataSource)initialContext.lookup(PlanMgmtConstants.DATASRC);
			    
			    if (datasource != null) {
			        conn = datasource.getConnection();
			        stmt = conn.prepareStatement(buildquery.toString());	
			        stmt.setString(1, strenussId);
			        LOGGER.info("strenussId  = " +strenussId);
			        stmt.setFetchSize(PlanMgmtConstants.FETCHSIZE);	    		
		    		rs = stmt.executeQuery();
		    				    		
		            while ( rs.next() ) {		
		            	networkList.add(rs.getString("network_key") );		            	
		            }
		            providerIdList.put(strenussId, networkList);
		            providerNetworkList.add(providerIdList);
		           
			    }
			} catch (Exception e) {
	        	LOGGER.error("Got an exception to execute getNetworkListByStrenussId()! Exception ==> " , e );           
	        }finally{
	        	PlanMgmtUtil.closeResultSet(rs);
				PlanMgmtUtil.closePreparedStatement(stmt);
				PlanMgmtUtil.closeDBConnection(conn);	
	        }
		}	
				
		return providerNetworkList;		
	}
	
}
