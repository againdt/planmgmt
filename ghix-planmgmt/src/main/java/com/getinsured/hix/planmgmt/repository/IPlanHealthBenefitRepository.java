package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanHealthBenefit;

public interface IPlanHealthBenefitRepository extends JpaRepository<PlanHealthBenefit, Integer> {
	
	@Query("SELECT pb.networkLimitation, pb.networkLimitationAttribute, pb.networkExceptions, " +
			"pb.networkT1CopayVal, pb.networkT1CopayAttr, pb.networkT1CoinsurVal, pb.networkT1CoinsurAttr, " +
			"pb.networkT2CopayVal, pb.networkT2CopayAttr, pb.networkT2CoinsurVal, pb.networkT2CoinsurAttr, " +
			"pb.outOfNetworkCopayVal, pb.outOfNetworkCopayAttr, pb.outOfNetworkCoinsurVal, pb.outOfNetworkCoinsurAttr, " +
			"pb.subjectToInNetworkDuductible, pb.subjectToOutNetworkDeductible, pb.name, "+
			"pb.networkT1display, pb.networkT2display, pb.outOfNetworkDisplay, pb.limitExcepDisplay, pb.networkT1TileDisplay, pb.isCovered, pb.explanation  " +
			"from PlanHealthBenefit pb WHERE (pb.plan.id = :planId) and (TO_DATE (:effectiveDate, 'YYYY-MM-DD') BETWEEN pb.effStartDate AND pb.effEndDate)")
	List<Object[]> getPlanHealthBenefit(@Param("planId") Integer planId, @Param("effectiveDate") String effectiveDate);
	
	@Query("SELECT planHealthBenefit from PlanHealthBenefit planHealthBenefit where planHealthBenefit.plan.plan.id IN (:planId )")
	List<PlanHealthBenefit> getPlanHealthBenefitByPlanId(@Param("planId") List<Integer> planId);
	
	@Query("SELECT phb from PlanHealthBenefit phb WHERE phb.plan.id = :planHealthId")
	List<PlanHealthBenefit> getBenefitByPlanHealthId(@Param("planHealthId") Integer planHealthId);
	
}




