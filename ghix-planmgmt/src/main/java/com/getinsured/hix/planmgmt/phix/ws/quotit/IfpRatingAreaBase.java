//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.14 at 11:56:49 AM PST 
//


package com.getinsured.hix.planmgmt.phix.ws.quotit;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IfpRatingAreaBase.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IfpRatingAreaBase">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FamilyResidence"/>
 *     &lt;enumeration value="MemberResidence"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "IfpRatingAreaBase")
@XmlEnum
public enum IfpRatingAreaBase {

    @XmlEnumValue("FamilyResidence")
    FAMILY_RESIDENCE("FamilyResidence"),
    @XmlEnumValue("MemberResidence")
    MEMBER_RESIDENCE("MemberResidence");
    private final String value;

    IfpRatingAreaBase(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static IfpRatingAreaBase fromValue(String v) {
        for (IfpRatingAreaBase c: IfpRatingAreaBase.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
