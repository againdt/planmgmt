/**
 * 
 */
package com.getinsured.hix.planmgmt.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.feeds.IssuerInfoDTO;
import com.getinsured.hix.dto.planmgmt.IssuerBrandNameDTO;
import com.getinsured.hix.dto.planmgmt.IssuerByHIOSIdResponseDTO;
import com.getinsured.hix.dto.planmgmt.IssuerListRequestDTO;
import com.getinsured.hix.dto.planmgmt.IssuerPaymentInfoResponse;
import com.getinsured.hix.dto.planmgmt.SingleIssuerResponse;
import com.getinsured.hix.dto.tenant.IssuerBrandSpecificDTO;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerPaymentInformation;
import com.getinsured.hix.model.TenantDTO;
import com.getinsured.hix.planmgmt.querybuilder.IssuerQueryBuilder;
import com.getinsured.hix.planmgmt.repository.PMIssuerRepository;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.planmgmt.util.PlanMgmtParamConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;

/**
 * This is the implementation class of issuerService
 * 
 * @author gajulapalli_k
 * 
 */
@Service("pmIssuerService")
@Repository
@Transactional
public class IssuerServiceImpl implements IssuerService {
	private static final Logger LOGGER = Logger.getLogger(IssuerServiceImpl.class);
	private static final String ZIPCODE = "zipCode";
	private static final String COUNTYCODE = "countyCode";
	
	@PersistenceUnit 
	private EntityManagerFactory emf;

	@Autowired
	PMIssuerRepository  issuerRepository;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	@Autowired 
	private TenantService tenantService;
	
	@Value(PlanMgmtConstants.CONFIG_APP_URL)
	private String appUrl;

	@Override
	public String getDisclaimerInfo(String issuerId, String exchangeType) {		
		String disclaimerInfoString = null;
		if (!issuerId.equals("")) {			
			List<Object[]> disclaimerInfo = issuerRepository.getDisclaimerInfo(Integer.parseInt(issuerId));			
			if (disclaimerInfo != null && (!(disclaimerInfo.isEmpty())) && exchangeType != null) {	
				// for on/off exchange return on/off exchange disclaimer only
				if (exchangeType.equalsIgnoreCase("ON")) {
					disclaimerInfoString = (String) disclaimerInfo.get(0)[0];
				} else if (exchangeType.equalsIgnoreCase("OFF")) {
					disclaimerInfoString = (String) disclaimerInfo.get(0)[1];
				} else {
					disclaimerInfoString = PlanMgmtConstants.EMPTY_STRING;
				}			
			}
			else{
				disclaimerInfoString = PlanMgmtConstants.EMPTY_STRING;
			}
			
		} else {
			disclaimerInfoString = PlanMgmtConstants.EMPTY_STRING;
		}
		return disclaimerInfoString;
	}


	@Override
	@Transactional(readOnly = true)
	public List getIssuersByZip(String zipCode, String countyCode) {
		List issuerList = new ArrayList();
		Query query = null;
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			StringBuilder buildquery = new StringBuilder();
			buildquery.append("SELECT DISTINCT i.name ");
			buildquery.append("FROM ");
			buildquery.append("pm_service_area ps, pm_issuer_service_area isa, issuers i ");
			buildquery.append("WHERE ");
			buildquery.append("i.hios_issuer_id = isa.hios_issuer_id ");
			buildquery.append("AND isa.id = ps.service_area_id ");
			buildquery.append("AND ps.is_deleted='N' ");
			buildquery.append("AND ps.zip = (:" + ZIPCODE + ") ");
			if (!countyCode.equals("")) {
				buildquery.append("AND ps.fips = (:" + COUNTYCODE + ") ");
			}
	
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL == " + buildquery.toString());
			}
	
			query = entityManager.createNativeQuery(buildquery.toString());
			query.setParameter(ZIPCODE, zipCode);
			if (!countyCode.equals("")) {
				query.setParameter(COUNTYCODE, countyCode);
			}
	
			List rsList = query.getResultList();
			Iterator rsIterator = rsList.iterator();
	
			while (rsIterator.hasNext()) {
				String issuerName = rsIterator.next().toString();
				issuerList.add(issuerName);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getIssuersByZip", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}

		return issuerList;
	}
	
	@Override
	public Issuer getIssuerbyId(Integer id){
		return issuerRepository.findOne(id);
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<String> getIssuerD2CList() {
		List<String> issuerD2csList = new ArrayList<String>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			Query query = null;
	
			StringBuilder buildquery = new StringBuilder();
			buildquery.append("SELECT HIOS_ISSUER_ID, APPLICABLE_YEAR, D2C_FLOW_TYPE FROM ISSUER_D2C ");
			buildquery.append("WHERE D2CFLAG ='Y'");
			
			query = entityManager.createNativeQuery(buildquery.toString());
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL == " + buildquery.toString());
			}
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
	
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				StringBuilder issuerD2CBuilder = new StringBuilder();
				issuerD2CBuilder.append(objArray[PlanMgmtConstants.ZERO] == null ?  0 : objArray[PlanMgmtConstants.ZERO].toString());
				issuerD2CBuilder.append(PlanMgmtConstants.SEMICOLON_SEPERATOR);
				issuerD2CBuilder.append(objArray[PlanMgmtConstants.ONE] == null ?  0 : objArray[PlanMgmtConstants.ONE].toString());
				issuerD2CBuilder.append(PlanMgmtConstants.SEMICOLON_SEPERATOR);
				issuerD2CBuilder.append(objArray[PlanMgmtConstants.TWO] == null ?  0 : objArray[PlanMgmtConstants.TWO].toString());
				issuerD2csList.add(issuerD2CBuilder.toString());
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getIssuerD2CList: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		/*if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("issuerD2csList : " + SecurityUtil.sanitizeForLogging(issuerD2csList.toString()));
		}*/
		return issuerD2csList;
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public IssuerPaymentInfoResponse getIssuerPaymentInfoByHiosId(String hiosIssuerId) {
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("getPaymentInfoById() : Issuer hiosIssuerId : " + SecurityUtil.sanitizeForLogging(hiosIssuerId));
		}
		IssuerPaymentInfoResponse issuerPaymentInfoResponse = null;
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			if(null != entityManager){
				Query queryForIssuerPaymentInfo = entityManager.createQuery("select issuerPaymentInfo from IssuerPaymentInformation "
						+ "issuerPaymentInfo where issuerPaymentInfo.issuer.hiosIssuerId=:hiosIssuerId");
				queryForIssuerPaymentInfo.setParameter("hiosIssuerId", hiosIssuerId);
				Object paymentInfo = queryForIssuerPaymentInfo.getSingleResult();
				if(null != paymentInfo && paymentInfo instanceof IssuerPaymentInformation){
					IssuerPaymentInformation issuerPaymentInformation = (IssuerPaymentInformation)paymentInfo;
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Returning payment information for issuer hiosIssuerId : " + SecurityUtil.sanitizeForLogging(hiosIssuerId));
					}
					issuerPaymentInfoResponse = new IssuerPaymentInfoResponse();
					issuerPaymentInfoResponse.setHiosId(hiosIssuerId);
					issuerPaymentInfoResponse.setIssuerAuthURL(issuerPaymentInformation.getIssuerAuthURL());
					issuerPaymentInfoResponse.setKeyStoreFileLocation(issuerPaymentInformation.getKeyStoreFileLocation());
					issuerPaymentInfoResponse.setPassword(issuerPaymentInformation.getPassword());
					issuerPaymentInfoResponse.setPasswordSecuredKey(issuerPaymentInformation.getPasswordSecuredKey());
					issuerPaymentInfoResponse.setPrivateKeyName(issuerPaymentInformation.getPrivateKeyName());
					issuerPaymentInfoResponse.setSecurityAddress(issuerPaymentInformation.getSecurityAddress());
					issuerPaymentInfoResponse.setSecurityDnsName(issuerPaymentInformation.getSecurityDnsName());
					issuerPaymentInfoResponse.setSecurityKeyInfo(issuerPaymentInformation.getSecurityKeyInfo());
					issuerPaymentInfoResponse.setSecurityCertName(issuerPaymentInformation.getSecurityCertName());
				}else{
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Payment information not found for issuer hiosIssuerId : " +SecurityUtil.sanitizeForLogging( hiosIssuerId));
					}
				}
			}
		} catch(NoResultException e){
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Payment information not found for issuer hiosIssuerId : " +SecurityUtil.sanitizeForLogging( hiosIssuerId));
			}
		} catch(Exception ex){
			LOGGER.error("Exception occured in getIssuerPaymentInfoById()", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return issuerPaymentInfoResponse;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<IssuerInfoDTO> getIssuerList(String tenantCode, String stateCode, String insuranceType, int applicableYear) {
		 List<IssuerInfoDTO> issuerList = new ArrayList<IssuerInfoDTO>();
		 Query query = null;
		 EntityManager entityManager = null;
		 
		 try{	
			entityManager = emf.createEntityManager();
			String queryStr = PlanMgmtConstants.EMPTY_STRING;
			// HIX-72898, when tenant code is ALL, then don't use tenant filter
			if("ALL".equalsIgnoreCase(tenantCode)){				
				// HIX-72898, when insurance type is ALL, then don't use insurance type filter
				if("ALL".equalsIgnoreCase(insuranceType)){
					queryStr = IssuerQueryBuilder.getIssuerListWithoutTenantAndInsTypeQuery();
				}else{
					queryStr = IssuerQueryBuilder.getIssuerListWithoutTenantQuery();
				}
				
			}else{
				queryStr = IssuerQueryBuilder.getIssuerListWithTenantQuery();
			}
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("SQL == " + queryStr.toString());
			}	
			
			query = entityManager.createQuery(queryStr.toString());
			query.setParameter(PlanMgmtParamConstants.PARAM_STATE_CODE, stateCode.toUpperCase());
								
			if("ALL".equalsIgnoreCase(tenantCode)){
				// HIX-72898, when insurance type is ALL, then don't use insurance type filter
				if(!"ALL".equalsIgnoreCase(insuranceType)){
					query.setParameter(PlanMgmtParamConstants.PARAM_INSURANCE_TYPE, insuranceType.toUpperCase());
				}					
			}else{
				query.setParameter(PlanMgmtParamConstants.PARAM_INSURANCE_TYPE, insuranceType.toUpperCase());
				query.setParameter(PlanMgmtParamConstants.PARAM_TENANT_CODE, tenantCode.toUpperCase());
			}
			//query.setParameter(PlanMgmtParamConstants.PARAM_APPLICABLE_YEAR, applicableYear);
						
			List rsList = query.getResultList();
			Iterator rsIterator = rsList.iterator();
			Object[] objArray = null;
			while (rsIterator.hasNext()) {
				objArray = (Object[]) rsIterator.next();				
				if(null != objArray[PlanMgmtConstants.ONE]){
					IssuerInfoDTO issuerInfoDTO  = new IssuerInfoDTO();
					issuerInfoDTO.setIssuerId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
					issuerInfoDTO.setIssuerName(objArray[PlanMgmtConstants.ONE].toString());					
					issuerInfoDTO.setHiosIssuerId(objArray[PlanMgmtConstants.TWO].toString());
					issuerList.add(issuerInfoDTO);
					issuerInfoDTO = null;
				}	
				objArray =  null;
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getIssuerList", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}

		return issuerList;
	}
	
	@Override
	public List<IssuerBrandNameDTO> getIssuerAndIssuerBrandNameByUserId(Integer userId){
		String queryString = PlanMgmtConstants.EMPTY_STRING;
		Query query = null;
		List<IssuerBrandNameDTO> issuerBrandNameDtoList = null;
		IssuerBrandNameDTO brandNameDTO = null;
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			queryString = IssuerQueryBuilder.getIssuerAndIssuerBrandNameByUserId();
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("SQL === " + queryString);
			}
			
			issuerBrandNameDtoList = new ArrayList<IssuerBrandNameDTO>();			
			
			query = entityManager.createQuery(queryString.toString());
			query.setParameter(PlanMgmtParamConstants.PARAM_USER_ID, userId);
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
			Object[] objArray = null;
			while (rsIterator.hasNext()) {
				objArray = (Object[])rsIterator.next();
				if(null != objArray[PlanMgmtConstants.ZERO]){
					brandNameDTO = new IssuerBrandNameDTO();
					brandNameDTO.setIssuerId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
					if(null != objArray[PlanMgmtConstants.ONE]){
						brandNameDTO.setIssuerBrandName(objArray[PlanMgmtConstants.ONE].toString());
					}else{
						brandNameDTO.setIssuerBrandName(PlanMgmtConstants.EMPTY_STRING);
					}
					issuerBrandNameDtoList.add(brandNameDTO);
				}
				objArray =  null;
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getIssuerAndIssuerBrandNameByUserId: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return issuerBrandNameDtoList;
	}
	
	@Override
	public List<IssuerBrandNameDTO> getIssuerBrandNameListByIssuerList(IssuerListRequestDTO issuerListRequestDTO){
		String queryString = PlanMgmtConstants.EMPTY_STRING;
		Query query = null;
		List<IssuerBrandNameDTO> issuerBrandNameDtoList = null;
		IssuerBrandNameDTO brandNameDTO = null;
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			queryString = IssuerQueryBuilder.getIssuerBrandNameListByIssuerList();
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("SQL === " + queryString);
			}
			
			issuerBrandNameDtoList = new ArrayList<IssuerBrandNameDTO>();			
			
			query = entityManager.createQuery(queryString.toString());
			query.setParameter(PlanMgmtParamConstants.PARAM_ISSUER_LIST, issuerListRequestDTO.getIssuerIds());
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
			Object[] objArray = null;
			while (rsIterator.hasNext()) {
				objArray = (Object[])rsIterator.next();
				if(null != objArray[PlanMgmtConstants.ZERO]){
					brandNameDTO = new IssuerBrandNameDTO();
					brandNameDTO.setIssuerId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
					if(null != objArray[PlanMgmtConstants.ONE]){
						brandNameDTO.setIssuerBrandName(objArray[PlanMgmtConstants.ONE].toString());
					}else{
						brandNameDTO.setIssuerBrandName(PlanMgmtConstants.EMPTY_STRING);
					}
					issuerBrandNameDtoList.add(brandNameDTO);
				}
				objArray =  null;
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getIssuerBrandNameListByIssuerList: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return issuerBrandNameDtoList;
	}
	
	
	@Override
	public Issuer getIssuerByHIOSId(String hiosIssuerId){
		Issuer issuer =  new Issuer();
		try{
			issuer = issuerRepository.getIssuerByHiosID(hiosIssuerId);
		}catch(Exception ex){
			LOGGER.error("Exception occured in getIssuerbyHIOSId: ", ex);
		}
		return issuer;
	}
	
	@Override
	public List<Integer> getIssuerByName(String issuerName){
		List<Integer> issuerList =  new ArrayList<Integer>();
		try{
			issuerList = issuerRepository.getIssuerByName(issuerName);
		}catch(Exception ex){
			LOGGER.error("Exception occured in getIssuerByName: ", ex);
		}
		return issuerList;
	}
	
	
	@Override
	public SingleIssuerResponse getIssuerInfoById(Integer issuerId, boolean minimizeIssuerData, boolean includeLogo) {
		SingleIssuerResponse singleIssuerResponse = new SingleIssuerResponse();
		Issuer issuer = this.getIssuerbyId(issuerId);
		
		// if record found then set issuer object to SingleIssuerResponseDTO
		if(null != issuer){
			IssuerResponseDTOHelper issuerResponseDTOHelper = new IssuerResponseDTOHelper();
			singleIssuerResponse = issuerResponseDTOHelper.setObjectToDTO(issuer, minimizeIssuerData, includeLogo);
		}
		
		return singleIssuerResponse;
	}

	@Override
	public IssuerByHIOSIdResponseDTO getIssuerInfoByHIOSId(String hiosIssuerId, boolean includeLogo) {
		IssuerByHIOSIdResponseDTO issuerByHIOSIdResponseDTO = new IssuerByHIOSIdResponseDTO();
		try {
			// find issuer data by HIOS issuer id 
			Issuer issuer = null;

			if (StringUtils.isNotBlank(hiosIssuerId)) {
				issuer = this.getIssuerByHIOSId(hiosIssuerId);
			}

			if (null != issuer) {
				issuerByHIOSIdResponseDTO.setId(issuer.getId());
				issuerByHIOSIdResponseDTO.setName(issuer.getName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getName());
				issuerByHIOSIdResponseDTO.setHiosIssuerId(issuer.getHiosIssuerId() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getHiosIssuerId());
				issuerByHIOSIdResponseDTO.setCompanyLogo(PlanMgmtUtil.getIssuerLogoURL(issuer.getLogoURL() ,  issuer.getHiosIssuerId(), null, appUrl));

				if (issuer.getIssuerBrandName() != null) {
					issuerByHIOSIdResponseDTO.setBrandName(issuer.getIssuerBrandName().getBrandName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIssuerBrandName().getBrandName());
				}

				if (includeLogo) {
					issuerByHIOSIdResponseDTO.setLogo(issuer.getLogo());
				}
				issuerByHIOSIdResponseDTO.setMarketingName(issuer.getMarketingName());
				issuerByHIOSIdResponseDTO.setCompanyLegalName(issuer.getCompanyLegalName());
			}
			else {
				LOGGER.warn("Issuer Record Not Found.");
			}
		}
		catch (Exception ex) {
			giExceptionHandler.recordCriticalException(Component.PLANMGMT, String.valueOf(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_BY_HIOS_ID.code), "Exception occured in getIssuerInfoByHIOSId service: ",ex);
			LOGGER.error("Exception occured in getIssuerInfoByHIOSId service : ", ex);
		}
		return issuerByHIOSIdResponseDTO;
	}

	@Override
	public String getApplicationUrl(Issuer issuerObj, String tenantCode){
		String applicationUrl = PlanMgmtConstants.EMPTY_STRING;
		try{	
			TenantDTO tenantDto = tenantService.getTenant(tenantCode);
			
			// if tenant code is GINS, then return application Url from issuers table
			if(PlanMgmtConstants.TENANT_GINS.equalsIgnoreCase(tenantCode) && null != issuerObj.getApplicationUrl() )
			{
				applicationUrl = issuerObj.getApplicationUrl();
			}		
			else if(!PlanMgmtConstants.TENANT_GINS.equalsIgnoreCase(tenantCode))  // if tenant code is other than GINS
			{
				boolean isApplicationUrlExists = false;
				
				if(null != tenantDto && null != tenantDto.getConfiguration() && null != tenantDto.getConfiguration().getEappConfiguration() 
						&& null != tenantDto.getConfiguration().getEappConfiguration().getIssuerBrand())
				{
					String issuerBrandId = (null != issuerObj.getIssuerBrandName()) ? issuerObj.getIssuerBrandName().getBrandId() : PlanMgmtConstants.EMPTY_STRING;
					String hiosIssuerId = issuerObj.getHiosIssuerId();
					String issuerStateCode = issuerObj.getStateOfDomicile();
					
					// pull issuer brand specific tenant configuration 
					Map<String, Map<String, IssuerBrandSpecificDTO>> issuerBrandSpecificDTOMap = tenantDto.getConfiguration().getEappConfiguration().getIssuerBrand();
					
					// search application url by issuerBrandId or HIOS Issuer Id
					String[] keyElements = {issuerBrandId, hiosIssuerId};
					
					for(String key : keyElements)
					{
						if(issuerBrandSpecificDTOMap.containsKey(key))	// search issuerBrandId or HIOS Issuer Id exists in tenant configuration
						{
							Map<String, IssuerBrandSpecificDTO> issuerBrandMap = issuerBrandSpecificDTOMap.get(key); // get issuer brand configuration
							
							if(issuerBrandMap.containsKey(issuerStateCode)) // search application url against sate code
							{
								IssuerBrandSpecificDTO issuerBrandSpecificDTO = issuerBrandMap.get(issuerStateCode);
								applicationUrl = issuerBrandSpecificDTO.getApplicationURL();
								isApplicationUrlExists =  true;
							}
							else if(issuerBrandMap.containsKey("default")) // search default application url
							{
								IssuerBrandSpecificDTO issuerBrandSpecificDTO = issuerBrandMap.get("default");
								applicationUrl = issuerBrandSpecificDTO.getApplicationURL();
								isApplicationUrlExists =  true;
								
							}	
						}
						
						if(isApplicationUrlExists == true)
						{
							break;
						}
					}
					
				}
				
				// if Application Url does not exists in tenant configuration then return producer Url as application Url from issuers table
				if(isApplicationUrlExists == false && null != issuerObj.getProducerUrl())
				{
					applicationUrl = issuerObj.getProducerUrl();
				}	
			}
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception inside getApplicationUrl() : " + ex);
		}
		
		return applicationUrl;
	}


	@Override
	public Map<Integer, String> getIssuersLegalNameMapByIssuerIdList(List<Integer> issuerIdList) {
		List<Object[]> issuersLegalName =  null;
		Map<Integer, String> issuerLegalNamesMap =  null;
		if(null != issuerIdList && !issuerIdList.isEmpty()) {
			try{
				issuersLegalName = issuerRepository.getIssuersLegalName(issuerIdList);
				if(null != issuersLegalName && !issuersLegalName.isEmpty()) {
					issuerLegalNamesMap = new HashMap<Integer, String>();
					for (Object[] objects : issuersLegalName) {
						issuerLegalNamesMap.put((Integer) objects[0], (String) objects[1]);
					}
				} else {
					LOGGER.error("No Issuer record found for passed in IssuerIdList : " + Arrays.toString(issuerIdList.toArray()));
				}
			}catch(Exception ex){
				LOGGER.error("Exception occured in getIssuersLegalNameMapByIssuerIdList: ", ex);
			}
		} else {
			LOGGER.error("Invalid params, Issuer list is passed as empty for call to getIssuersLegalNameMapByIssuerIdList()");
		}
		return issuerLegalNamesMap;	
	}
	
}
