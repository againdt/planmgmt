//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.12 at 02:20:37 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.ihc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HipQuoteOptions complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HipQuoteOptions">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="BloodPressures" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfKeyValueOfstringint" minOccurs="0"/>
 *         &lt;element name="Cholesterols" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfKeyValueOfstringint" minOccurs="0"/>
 *         &lt;element name="FemaleHeights" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfKeyValueOfstringint" minOccurs="0"/>
 *         &lt;element name="MaleHeights" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfKeyValueOfstringint" minOccurs="0"/>
 *         &lt;element name="Occupations" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfKeyValueOfstringint" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HipQuoteOptions", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", propOrder = {
    "bloodPressures",
    "cholesterols",
    "femaleHeights",
    "maleHeights",
    "occupations"
})
public class HipQuoteOptions {

    @XmlElementRef(name = "BloodPressures", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfKeyValueOfstringint> bloodPressures;
    @XmlElementRef(name = "Cholesterols", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfKeyValueOfstringint> cholesterols;
    @XmlElementRef(name = "FemaleHeights", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfKeyValueOfstringint> femaleHeights;
    @XmlElementRef(name = "MaleHeights", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfKeyValueOfstringint> maleHeights;
    @XmlElementRef(name = "Occupations", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfKeyValueOfstringint> occupations;

    /**
     * Gets the value of the bloodPressures property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringint }{@code >}
     *     
     */
    public JAXBElement<ArrayOfKeyValueOfstringint> getBloodPressures() {
        return bloodPressures;
    }

    /**
     * Sets the value of the bloodPressures property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringint }{@code >}
     *     
     */
    public void setBloodPressures(JAXBElement<ArrayOfKeyValueOfstringint> value) {
        this.bloodPressures = value;
    }

    /**
     * Gets the value of the cholesterols property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringint }{@code >}
     *     
     */
    public JAXBElement<ArrayOfKeyValueOfstringint> getCholesterols() {
        return cholesterols;
    }

    /**
     * Sets the value of the cholesterols property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringint }{@code >}
     *     
     */
    public void setCholesterols(JAXBElement<ArrayOfKeyValueOfstringint> value) {
        this.cholesterols = value;
    }

    /**
     * Gets the value of the femaleHeights property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringint }{@code >}
     *     
     */
    public JAXBElement<ArrayOfKeyValueOfstringint> getFemaleHeights() {
        return femaleHeights;
    }

    /**
     * Sets the value of the femaleHeights property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringint }{@code >}
     *     
     */
    public void setFemaleHeights(JAXBElement<ArrayOfKeyValueOfstringint> value) {
        this.femaleHeights = value;
    }

    /**
     * Gets the value of the maleHeights property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringint }{@code >}
     *     
     */
    public JAXBElement<ArrayOfKeyValueOfstringint> getMaleHeights() {
        return maleHeights;
    }

    /**
     * Sets the value of the maleHeights property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringint }{@code >}
     *     
     */
    public void setMaleHeights(JAXBElement<ArrayOfKeyValueOfstringint> value) {
        this.maleHeights = value;
    }

    /**
     * Gets the value of the occupations property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringint }{@code >}
     *     
     */
    public JAXBElement<ArrayOfKeyValueOfstringint> getOccupations() {
        return occupations;
    }

    /**
     * Sets the value of the occupations property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfKeyValueOfstringint }{@code >}
     *     
     */
    public void setOccupations(JAXBElement<ArrayOfKeyValueOfstringint> value) {
        this.occupations = value;
    }

}
