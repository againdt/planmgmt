package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanSTMCost;

public interface IPlanSTMCostRepository extends JpaRepository<PlanSTMCost, Integer>, RevisionRepository<PlanSTMCost, Integer, Integer> {

    @Query("SELECT separateDedtPrescptDrug FROM PlanSTMCost psc WHERE psc.planSTM.id = :planStmId")
    String getSeparateDrugDeductibleForPlanSTMCost(@Param("planStmId") Integer planStmId);

}
