package com.getinsured.hix.planmgmt.util;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;

import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;

public class ReportExcelCellStyle {
	
	public CellStyle getCellStyleForAgeRange(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cellStyle.setBorderLeft((short) PlanMgmtConstants.ONE); // single line border
        cellStyle.setBorderRight((short) PlanMgmtConstants.ONE); // single line border
        return cellStyle;
	}
	
	public CellStyle getCellStyleForAgeRange65Plus(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cellStyle.setBorderLeft((short) PlanMgmtConstants.ONE); // single line border
        cellStyle.setBorderRight((short) PlanMgmtConstants.ONE); // single line border
        cellStyle.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
        return cellStyle;
	}
	
	public CellStyle getCellStyleForLastNonTobacco(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
        cellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        cellStyle.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
        return cellStyle;
	}
	
	public CellStyle getCellStyleForLastTobacco(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle lastTobacco = wb.createCellStyle();
		lastTobacco.setAlignment(CellStyle.ALIGN_CENTER);
		lastTobacco.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		lastTobacco.setBorderRight((short) PlanMgmtConstants.ONE); // single line border
		lastTobacco.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
        return lastTobacco;
	}
	
	public CellStyle getCellStyleForAge(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle ageCellStyle = wb.createCellStyle();
		ageCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		ageCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.PINK.index, (byte) PlanMgmtConstants.TWO_FIVE_THREE,(byte) PlanMgmtConstants.TWO_THREE_THREE, (byte) PlanMgmtConstants.TWO_ONE_SEVEN);
		ageCellStyle.setFillForegroundColor(HSSFColor.PINK.index);
		//ageCellStyle.setFillForegroundColor(HSSFColor.PINK.index);
		ageCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		ageCellStyle.setBorderLeft((short) PlanMgmtConstants.ONE); // single line border
		ageCellStyle.setBorderRight((short) PlanMgmtConstants.ONE); // single line border
		ageCellStyle.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
		ageCellStyle.setBorderTop((short) PlanMgmtConstants.ONE);
		return ageCellStyle;
	}
	
	public CellStyle getCellStyleForAgeEnd(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle ageCellStyle = wb.createCellStyle();
		ageCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		ageCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.YELLOW.index, (byte) PlanMgmtConstants.TWO_FIVE_THREE,(byte) PlanMgmtConstants.TWO_THREE_THREE, (byte) PlanMgmtConstants.TWO_ONE_SEVEN);
		ageCellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		//ageCellStyle.setFillForegroundColor(HSSFColor.PINK.index);
		ageCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		ageCellStyle.setBorderLeft((short) PlanMgmtConstants.ONE); // single line border
		ageCellStyle.setBorderRight((short) PlanMgmtConstants.ONE); // single line border
		ageCellStyle.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
		ageCellStyle.setBorderTop((short) PlanMgmtConstants.ONE);
		return ageCellStyle;
	}
	
	public CellStyle getCellStyleForLastAge(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle lastAgeCellStyle = wb.createCellStyle();
		lastAgeCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		lastAgeCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		lastAgeCellStyle.setBorderTop((short) PlanMgmtConstants.ONE);
		return lastAgeCellStyle;
	}
	
	public CellStyle getDataCellStyleForRatingArea(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle dataCellStyleForRatingArea = wb.createCellStyle();
		dataCellStyleForRatingArea.setAlignment(CellStyle.ALIGN_CENTER);
		dataCellStyleForRatingArea.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		dataCellStyleForRatingArea.setBorderRight((short) PlanMgmtConstants.ONE); // single line border
		return dataCellStyleForRatingArea;
	}

	public CellStyle getDataCellStyleForPlanInfo(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle dataCellStyle = wb.createCellStyle();
		dataCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		dataCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		dataCellStyle.setBorderRight((short) PlanMgmtConstants.ONE); // single line border
		dataCellStyle.setBorderLeft((short) PlanMgmtConstants.ONE);
		return dataCellStyle;
	}
	
	public CellStyle getDataCellStyleForPlanID(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle planIDCellStyle = wb.createCellStyle();
		planIDCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		planIDCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		Font font = wb.createFont();
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.AQUA.index, (byte) PlanMgmtConstants.ZERO,(byte) PlanMgmtConstants.ONE_ONE_ZERO, (byte) PlanMgmtConstants.ONE_NINE_TWO);
        font.setColor(HSSFColor.AQUA.index);
        planIDCellStyle.setFont(font);
        planIDCellStyle.setBorderRight((short) PlanMgmtConstants.ONE); // single line border
        planIDCellStyle.setBorderLeft((short) PlanMgmtConstants.ONE);
		return planIDCellStyle;
	}
	
	public CellStyle getCellStyleForNonTobacco(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle nonTobaccoCellStyle = wb.createCellStyle();
		nonTobaccoCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		nonTobaccoCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		nonTobaccoCellStyle.setBorderBottom((short) PlanMgmtConstants.ONE);
		return nonTobaccoCellStyle;
	}
	
	public CellStyle getCellStyleForLastCell(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle nonTobaccoCellStyle = wb.createCellStyle();
		nonTobaccoCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		nonTobaccoCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		nonTobaccoCellStyle.setBorderTop((short) PlanMgmtConstants.ONE);
		return nonTobaccoCellStyle;
	}
	
	public CellStyle getCellStyleForBenefit(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle datacellStyle = wb.createCellStyle();
		datacellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		datacellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.BLUE.index, (byte) PlanMgmtConstants.ONE_NINE_SEVEN,(byte) PlanMgmtConstants.TWO_ONE_SEVEN, (byte) PlanMgmtConstants.TWO_FOUR_ONE);
		datacellStyle.setFillForegroundColor(HSSFColor.BLUE.index);
		
		datacellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		datacellStyle.setBorderLeft((short) PlanMgmtConstants.ONE); // single line border
		datacellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		datacellStyle.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
		return datacellStyle;
	}
	
	public CellStyle getCellStyleForNextBenefit(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle datacellStyle = wb.createCellStyle();
		datacellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		datacellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.BLUE_GREY.index, (byte) PlanMgmtConstants.TWO_ONE_NINE,(byte) PlanMgmtConstants.TWO_TWO_NINE, (byte) PlanMgmtConstants.TWO_FOUR_ONE);
		datacellStyle.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
		//datacellStyle.setFillForegroundColor(HSSFColor.RED.index);
		
		datacellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		datacellStyle.setBorderLeft((short) PlanMgmtConstants.ONE); // single line border
		datacellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		datacellStyle.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
		return datacellStyle;
	}
	
	public CellStyle getCellStyleForEndCell(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle dataEndcellStyle = wb.createCellStyle();
		dataEndcellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		dataEndcellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.BLUE.index, (byte) PlanMgmtConstants.ONE_NINE_SEVEN,(byte) PlanMgmtConstants.TWO_ONE_SEVEN, (byte) PlanMgmtConstants.TWO_FOUR_ONE);
		dataEndcellStyle.setFillForegroundColor(HSSFColor.BLUE.index);
		//dataEndcellStyle.setFillForegroundColor(HSSFColor.GREEN.index);
		
		dataEndcellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		dataEndcellStyle.setBorderLeft((short) PlanMgmtConstants.ONE); // single line border
		dataEndcellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		dataEndcellStyle.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
		dataEndcellStyle.setBorderTop((short) PlanMgmtConstants.ONE);
		return dataEndcellStyle;
	}
	
	public CellStyle getCellStyleForEndCellLast(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle dataEndcellStyle = wb.createCellStyle();
		dataEndcellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		dataEndcellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.BLUE_GREY.index, (byte) PlanMgmtConstants.TWO_ONE_NINE,(byte) PlanMgmtConstants.TWO_TWO_NINE, (byte) PlanMgmtConstants.TWO_FOUR_ONE);
		dataEndcellStyle.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
		
		//dataEndcellStyle.setFillForegroundColor(HSSFColor.RED.index);
		
		dataEndcellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		dataEndcellStyle.setBorderLeft((short) PlanMgmtConstants.ONE); // single line border
		dataEndcellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		dataEndcellStyle.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
		dataEndcellStyle.setBorderTop((short) PlanMgmtConstants.ONE);
		return dataEndcellStyle;
	}
	
	public CellStyle getCellStyleForbenefitscell(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle dataEndcellStyle = wb.createCellStyle();
		dataEndcellStyle.setAlignment(CellStyle.ALIGN_LEFT);
		dataEndcellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.YELLOW.index, (byte) PlanMgmtConstants.TWO_FIVE_FIVE,(byte) PlanMgmtConstants.TWO_FIVE_FIVE, (byte) PlanMgmtConstants.ONE_FOUR_SIX);
		dataEndcellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		//dataEndcellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
		dataEndcellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		dataEndcellStyle.setBorderLeft((short) PlanMgmtConstants.ONE); // single line border
		dataEndcellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		dataEndcellStyle.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
		dataEndcellStyle.setBorderTop((short) PlanMgmtConstants.ONE);
		return dataEndcellStyle;
	}
	
	public CellStyle getCellStyleForBenefitEnd(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle lastBenefitCellStyle = wb.createCellStyle();
		lastBenefitCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		lastBenefitCellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		lastBenefitCellStyle.setBorderTop((short) PlanMgmtConstants.FIVE);
		return lastBenefitCellStyle;
	}
	
	public CellStyle getCellStyleForBenefitHeading(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle benefitscellStyle = wb.createCellStyle();
		benefitscellStyle.setAlignment(CellStyle.ALIGN_LEFT);
		benefitscellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.YELLOW.index, (byte) PlanMgmtConstants.TWO_FIVE_FIVE,(byte) PlanMgmtConstants.TWO_FIVE_FIVE, (byte) PlanMgmtConstants.ONE_FOUR_SIX);
		benefitscellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		//benefitscellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
		benefitscellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		benefitscellStyle.setBorderLeft((short) PlanMgmtConstants.FIVE); // single line border
		benefitscellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		benefitscellStyle.setBorderBottom((short) PlanMgmtConstants.FIVE); // single line border
		benefitscellStyle.setBorderTop((short) PlanMgmtConstants.FIVE);
		return benefitscellStyle;
	}
	
	public CellStyle getCellStyleForsubHeading(HSSFWorkbook temp_wb, int planCount){
		HSSFWorkbook wb = temp_wb;
		CellStyle subHeadingcellStyle = wb.createCellStyle();
		subHeadingcellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		subHeadingcellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		HSSFPalette palette = wb.getCustomPalette();
		if(planCount % PlanMgmtConstants.TWO == PlanMgmtConstants.ZERO){
			palette.setColorAtIndex(HSSFColor.BLUE_GREY.index, (byte) PlanMgmtConstants.TWO_ONE_NINE,(byte) PlanMgmtConstants.TWO_TWO_NINE, (byte) PlanMgmtConstants.TWO_FOUR_ONE);
			subHeadingcellStyle.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
		}else{
    		palette.setColorAtIndex(HSSFColor.BLUE.index, (byte) PlanMgmtConstants.ONE_NINE_SEVEN,(byte) PlanMgmtConstants.TWO_ONE_SEVEN, (byte) PlanMgmtConstants.TWO_FOUR_ONE);
    		subHeadingcellStyle.setFillForegroundColor(HSSFColor.BLUE.index);
		}

		subHeadingcellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		subHeadingcellStyle.setBorderLeft((short) PlanMgmtConstants.FIVE); // single line border
		subHeadingcellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		subHeadingcellStyle.setBorderBottom((short) PlanMgmtConstants.FIVE); // single line border
		subHeadingcellStyle.setBorderTop((short) PlanMgmtConstants.FIVE); // single line border
		return subHeadingcellStyle;
	}
	
	public CellStyle getCellStyleOfDeductibleCellForCostShare(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle debuctiblecellStyle = wb.createCellStyle();
		debuctiblecellStyle.setAlignment(CellStyle.ALIGN_LEFT);
		debuctiblecellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.YELLOW.index, (byte) PlanMgmtConstants.TWO_FIVE_FIVE,(byte) PlanMgmtConstants.TWO_FIVE_FIVE, (byte) PlanMgmtConstants.ONE_FOUR_SIX);
		debuctiblecellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		
		debuctiblecellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		debuctiblecellStyle.setBorderLeft((short) PlanMgmtConstants.ONE); // single line border
		debuctiblecellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		debuctiblecellStyle.setBorderBottom((short) PlanMgmtConstants.ONE); // single line border
		debuctiblecellStyle.setBorderTop((short) PlanMgmtConstants.ONE);
		return debuctiblecellStyle;
	}
	
	public CellStyle getCellStyleForDefaultSubheading(HSSFWorkbook temp_wb, int temp_planCount){
		int planCount = temp_planCount;
		HSSFWorkbook wb = temp_wb;
		CellStyle subHeadingcellStyle = temp_wb.createCellStyle();
		subHeadingcellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		subHeadingcellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		HSSFPalette palette = wb.getCustomPalette();
		if(planCount % PlanMgmtConstants.TWO == PlanMgmtConstants.ZERO){
			palette.setColorAtIndex(HSSFColor.BLUE_GREY.index, (byte) PlanMgmtConstants.TWO_ONE_NINE,(byte) PlanMgmtConstants.TWO_TWO_NINE, (byte) PlanMgmtConstants.TWO_FOUR_ONE);
			subHeadingcellStyle.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
		}else{
    		palette.setColorAtIndex(HSSFColor.BLUE.index, (byte) PlanMgmtConstants.ONE_NINE_SEVEN,(byte) PlanMgmtConstants.TWO_ONE_SEVEN, (byte) PlanMgmtConstants.TWO_FOUR_ONE);
    		subHeadingcellStyle.setFillForegroundColor(HSSFColor.BLUE.index);

		}
		
		subHeadingcellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		subHeadingcellStyle.setBorderLeft((short) PlanMgmtConstants.FIVE); // single line border
		subHeadingcellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		subHeadingcellStyle.setBorderBottom((short) PlanMgmtConstants.FIVE); // single line border
		return subHeadingcellStyle;
	}
	
	public CellStyle getCellStyleForDefaultFamilyHeading(HSSFWorkbook temp_wb, int temp_planCount){
		int planCount = temp_planCount;
		HSSFWorkbook wb = temp_wb;
		CellStyle familycellStyle = temp_wb.createCellStyle();
		familycellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		familycellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		
		HSSFPalette palette = wb.getCustomPalette();
		if(planCount % PlanMgmtConstants.TWO == PlanMgmtConstants.ZERO){
			palette.setColorAtIndex(HSSFColor.BLUE_GREY.index, (byte) PlanMgmtConstants.TWO_ONE_NINE,(byte) PlanMgmtConstants.TWO_TWO_NINE, (byte) PlanMgmtConstants.TWO_FOUR_ONE);
			familycellStyle.setFillForegroundColor(HSSFColor.BLUE_GREY.index);
		}else{
    		palette.setColorAtIndex(HSSFColor.BLUE.index, (byte) PlanMgmtConstants.ONE_NINE_SEVEN,(byte) PlanMgmtConstants.TWO_ONE_SEVEN, (byte) PlanMgmtConstants.TWO_FOUR_ONE);
    		familycellStyle.setFillForegroundColor(HSSFColor.BLUE.index);

		}
		
		familycellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		familycellStyle.setBorderLeft((short) PlanMgmtConstants.FIVE); // single line border
		familycellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		familycellStyle.setBorderBottom((short) PlanMgmtConstants.FIVE); // single line border
		familycellStyle.setBorderTop((short) PlanMgmtConstants.FIVE);
		return familycellStyle;
	}
	
	public CellStyle getCellStyleForDefaultDeductible(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle debuctiblecellStyle = temp_wb.createCellStyle();
		debuctiblecellStyle.setAlignment(CellStyle.ALIGN_LEFT);
		debuctiblecellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.YELLOW.index, (byte) PlanMgmtConstants.TWO_FIVE_FIVE,(byte) PlanMgmtConstants.TWO_FIVE_FIVE, (byte) PlanMgmtConstants.ONE_FOUR_SIX);
		debuctiblecellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		//debuctiblecellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
		
		debuctiblecellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		debuctiblecellStyle.setBorderLeft((short) PlanMgmtConstants.FIVE); // single line border
		debuctiblecellStyle.setBorderRight((short) PlanMgmtConstants.FIVE); // single line border
		debuctiblecellStyle.setBorderBottom((short) PlanMgmtConstants.FIVE); // single line border
		debuctiblecellStyle.setBorderTop((short) PlanMgmtConstants.FIVE);
		return debuctiblecellStyle;
	}
	
	public CellStyle getCellStyleForRatingAreaCell(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		CellStyle ratingAreaNamecellStyle = wb.createCellStyle();
		ratingAreaNamecellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		ratingAreaNamecellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		HSSFPalette palette = wb.getCustomPalette();
		palette.setColorAtIndex(HSSFColor.YELLOW.index, (byte) PlanMgmtConstants.TWO_FIVE_FIVE,(byte) PlanMgmtConstants.TWO_FIVE_FIVE, (byte) PlanMgmtConstants.ONE_FOUR_SIX);
		ratingAreaNamecellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		//ratingAreaNamecellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
		ratingAreaNamecellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		ratingAreaNamecellStyle.setBorderLeft((short) PlanMgmtConstants.FIVE); // single line border
		ratingAreaNamecellStyle.setBorderBottom((short) PlanMgmtConstants.FIVE); // single line border
		ratingAreaNamecellStyle.setBorderTop((short) PlanMgmtConstants.FIVE);
		ratingAreaNamecellStyle.setBorderRight((short) PlanMgmtConstants.FIVE);
		return ratingAreaNamecellStyle;
	}
	
	public CellStyle getCellStyleForPlanInfoCell(HSSFWorkbook temp_wb){
		HSSFWorkbook wb = temp_wb;
		HSSFPalette palette = wb.getCustomPalette();
		CellStyle planInfoForRatescellStyle = wb.createCellStyle();
		planInfoForRatescellStyle.setAlignment(CellStyle.ALIGN_LEFT);
		planInfoForRatescellStyle.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		palette.setColorAtIndex(HSSFColor.YELLOW.index, (byte) PlanMgmtConstants.TWO_FIVE_FIVE,(byte) PlanMgmtConstants.TWO_FIVE_FIVE, (byte) PlanMgmtConstants.ONE_FOUR_SIX);
		planInfoForRatescellStyle.setFillForegroundColor(HSSFColor.YELLOW.index);
		//planInfoForRatescellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
		planInfoForRatescellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		planInfoForRatescellStyle.setBorderLeft((short) PlanMgmtConstants.FIVE); // single line border
		planInfoForRatescellStyle.setBorderBottom((short) PlanMgmtConstants.FIVE); // single line border
		planInfoForRatescellStyle.setBorderTop((short) PlanMgmtConstants.FIVE);
		planInfoForRatescellStyle.setBorderRight((short) PlanMgmtConstants.ONE);
		return planInfoForRatescellStyle;
	}
}
