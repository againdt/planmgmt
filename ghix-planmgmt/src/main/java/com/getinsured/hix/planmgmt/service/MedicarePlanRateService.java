 /* @version 1.0
 * @since Feb 2, 2015 
 * Use MedicarePlanRateService class for Medicare Plan Rates
 */

package com.getinsured.hix.planmgmt.service;

import java.util.List;

import com.getinsured.hix.dto.planmgmt.MedicarePlan;


public interface MedicarePlanRateService {
	public List<MedicarePlan> getPlanResponseListForMedicare (List<Integer> planIdList);
	
}
