package com.getinsured.hix.planmgmt.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.AmePlan;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;

/**
 * @author Krishna Gajulapalli
 * 
 */
@Service("amePlanBenefitService")
@Repository
@Transactional
public class AMEPlanBenefitServiceImpl implements AMEPlanBenefitService {

	private static final Logger LOGGER = Logger.getLogger(AMEPlanBenefitServiceImpl.class);
	
	private static final String DOCUMENT_URL = "download/document?documentId="; // document download url
		
	@PersistenceUnit private EntityManagerFactory emf;
	
	@Autowired
	private QuotingBusinessLogic quotingBusinessLogic;
	
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	@Value(PlanMgmtConstants.CONFIG_APP_URL)
	private String appUrl;

	private static final String PLANAMEIDSTR ="planAmeIdStr";
	
	@Override
	@Transactional(readOnly = true)
	public List<AmePlan> getAmePlanDetails(String effectiveDate, List<Map<String, String>> memberList, String tenantCode) {
		List<AmePlan> amePlanList = new ArrayList<AmePlan>();
	
		// HIX-88279 :: If all the members of the household are below 19 then don't quote AME plans
		if(PlanMgmtUtil.isAllFamilyMembersAbove18years(memberList)){
			
			List<Map<String, String>> processedMemberData = quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.AME.toString()); // filter member data					
			String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
			String familyTierLookupCode = quotingBusinessLogic.getFamilyTierLookupCode(familyTier); // compute family tier lookup code		
			String stateName =  quotingBusinessLogic.getApplicantStateCode(processedMemberData);
			
			boolean doQuoteIssuerIHC  = true;
			
			if(!processedMemberData.isEmpty() ){
				for(Map<String, String> applicantData: processedMemberData)
				{
					if(applicantData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.SELF) || applicantData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.MEMBER)){					
						int applicantAge = Integer.parseInt(applicantData.get(PlanMgmtConstants.AGE));
						doQuoteIssuerIHC = quotingBusinessLogic.isAgeAllowForIHCQuoting(applicantAge);					
						break;
					}				
				}					
			}
	
			String relation = PlanMgmtConstants.EMPTY_STRING;
			List<Map<String,Object>> outDataList = getAMEAndCrosssellHelper(processedMemberData, effectiveDate, relation, familyTierLookupCode, doQuoteIssuerIHC, stateName, tenantCode);
			try{
				for(Map<String,Object> ameMap : outDataList){
					AmePlan amePlan = new AmePlan();
					amePlan.setPlanId((Integer)ameMap.get(PlanMgmtConstants.PLANID));
					amePlan.setName((String)ameMap.get(PlanMgmtConstants.PLAN_NAME));
					amePlan.setIssuerLogo((String)ameMap.get(PlanMgmtConstants.COMPANY_LOGO));
					amePlan.setPremium((Float)ameMap.get(PlanMgmtConstants.TOTALPREMIUM));
					
					amePlan.setDeductibleVal((String)ameMap.get(PlanMgmtConstants.DEDUCTIBLE_VAL));
					amePlan.setMaxBenefitVal((String)ameMap.get(PlanMgmtConstants.MAX_BENEFIT_VAL));
					amePlan.setId((Integer)ameMap.get(PlanMgmtConstants.PLAN_AME_ID));
					
					amePlan.setNetworkType((String)ameMap.get(PlanMgmtConstants.NETWORK_TYPE));
					amePlan.setBrochure((String)ameMap.get(PlanMgmtConstants.PLAN_BROCHURE));
					amePlan.setIssuerName((String)ameMap.get(PlanMgmtConstants.ISSUER_NAME));
					amePlan.setIssuerId((Integer)ameMap.get(PlanMgmtConstants.ISSUER_ID));
					amePlan.setDeductibleAttrib((String)ameMap.get(PlanMgmtConstants.DEDUCTIBLE_ATTR));
					amePlan.setMaxBenefitAttrib((String)ameMap.get(PlanMgmtConstants.BENEFIT_MAX_ATTR));
					
					Map<String, Map<String, String>> planBenefits = getAmePlanBenefits(amePlan.getId());
					if(null != planBenefits && !planBenefits.isEmpty()) {
						amePlan.setPlanBenefits(planBenefits);
					}
					amePlan.setLmitationAndExclusions((String)ameMap.get(PlanMgmtConstants.EXCLUSIONS_URL));
					amePlanList.add(amePlan);
				}
			} catch (Exception ex) {
				LOGGER.error("Exception occured in getAmePlanDetails() ", ex);
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getAmePlanDetails() ", ex);
			}
		
		}
		return amePlanList;
	}

	@Override
	@Transactional(readOnly = true)
	public Map<String, Map<String, String>> getAmePlanBenefits(Integer planAmeId) {
		Map<String, Map<String, String>> benefitDataList = new HashMap<String, Map<String, String>>();
		StringBuilder buildQuery = new StringBuilder();
		buildQuery.append("SELECT PLAN_AME_ID, BENEFIT_NAME, BENEFIT_VALUE FROM PLAN_AME_BENEFITS WHERE PLAN_AME_ID IN (:");
		buildQuery.append(PLANAMEIDSTR);
		buildQuery.append(") ORDER BY PLAN_AME_ID");
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT buildQuery :: " + buildQuery);
		}
		
		EntityManager entityManager = null;
		try {	
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildQuery.toString());
			query.setParameter(PLANAMEIDSTR, planAmeId);
			Iterator rsIterator = query.getResultList().iterator();
			
			while (rsIterator.hasNext()) {
				Object[] benefitFileds = (Object[]) rsIterator.next();
				Map<String, String> benefitData = new HashMap<String, String>();				
				String benefitName = (benefitFileds[PlanMgmtConstants.ONE] == null ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.ONE].toString());
				String benefitValue = (benefitFileds[PlanMgmtConstants.TWO] == null ? PlanMgmtConstants.EMPTY_STRING : benefitFileds[PlanMgmtConstants.TWO].toString());			
				benefitData.put("displayVal", benefitValue);
				
				benefitDataList.put(benefitName, benefitData);
			}
		
		} catch (Exception ex) {
			LOGGER.error("Exception occured in getAmePlanBenefits() ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getAmePlanBenefits() ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}

		return benefitDataList;
	}	
	
	
	@Override
	@Transactional(readOnly = true)
	public List<AmePlan> getMaxBenefitAmePlan(String effectiveDate, List<Map<String, String>> memberList, String tenantCode) {
		List<AmePlan> amePlanList = new ArrayList<AmePlan>();
		
		// HIX-88279 :: If all the members of the household are below 19 then don't quote AME plans
		if(PlanMgmtUtil.isAllFamilyMembersAbove18years(memberList)){
			
			List<Map<String, String>> processedMemberData = quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.AME.toString()); // filter member data					
			String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
			String familyTierLookupCode = quotingBusinessLogic.getFamilyTierLookupCode(familyTier); // compute family tier lookup code		
			String stateName =  quotingBusinessLogic.getApplicantStateCode(processedMemberData);
			
			boolean doQuoteIssuerIHC  = true;
			
			if(!processedMemberData.isEmpty() ){
				for(Map<String, String> applicantData: processedMemberData)
				{
					if(applicantData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.SELF) || applicantData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.MEMBER)){					
						int applicantAge = Integer.parseInt(applicantData.get(PlanMgmtConstants.AGE));
						doQuoteIssuerIHC = quotingBusinessLogic.isAgeAllowForIHCQuoting(applicantAge);					
						break;
					}				
				}					
			}
				
			ArrayList<String> paramList = new ArrayList<String>();
			int paramCount = 0; 
			StringBuilder buildQuery = new StringBuilder();
			String relation = PlanMgmtConstants.EMPTY_STRING;
			buildQuery.append("SELECT P.ID, P.NAME AS PLAN_NAME, sum(pre) AS total_premium, BENEFIT_MAX_VAL, rate_option, I.hios_issuer_id, I.logo_url ");
			buildQuery.append("FROM ");
			buildQuery.append("PLAN P, ISSUERS I, pm_tenant_plan tp, tenant t,");
			buildQuery.append(" ( ");
			for (int i = 0; i < processedMemberData.size(); i++) {
				// replace relation self/member with "applicant" 
				relation = StringUtils.replaceEach(processedMemberData.get(i).get(PlanMgmtConstants.RELATION).toLowerCase(), new String[]{PlanMgmtConstants.MEMBER,PlanMgmtConstants.SELF}, new String[]{PlanMgmtConstants.APPLICANT, PlanMgmtConstants.APPLICANT})  ;
				
				buildQuery.append(" ( SELECT p2.id AS plan_id, prate.RATE AS pre, 1 as memberctr, ");
				buildQuery.append("pame.BENEFIT_MAX_VAL as BENEFIT_MAX_VAL, prate.rate_option as rate_option ");
				buildQuery.append("FROM ");
				buildQuery.append("PLAN_AME pame, PM_PLAN_RATE prate, PLAN p2 ");
				buildQuery.append("WHERE ");
				buildQuery.append("p2.ID = pame.PLAN_ID ");
				buildQuery.append("AND p2.id = prate.plan_id ");
				buildQuery.append("AND TO_DATE (:param_" + paramCount + ", 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date ");
				paramList.add(effectiveDate);
				paramCount++;
				buildQuery.append("AND ( ( CAST (");
				buildQuery.append(":param_" + paramCount);
				paramList.add(familyTierLookupCode);
				paramCount++;
				buildQuery.append(" AS INTEGER) >= prate.min_age AND CAST (");
				buildQuery.append(":param_" + paramCount);
				paramList.add(familyTierLookupCode);
				paramCount++;
				buildQuery.append(" AS INTEGER) <= prate.max_age) OR (CAST (");
				buildQuery.append(":param_" + paramCount);
				paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.AGE));
				paramCount++;
	
				buildQuery.append(" AS INTEGER) >= prate.min_age AND CAST (");
				buildQuery.append(":param_" + paramCount);
				paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.AGE));
				paramCount++;
	
				buildQuery.append(" AS INTEGER) <= prate.max_age AND prate.relation = UPPER(");
				buildQuery.append(":param_" + paramCount);
				paramList.add(relation);
				paramCount++;
				buildQuery.append(" ) ) ) AND (prate.GENDER IS NULL");
				if(null != processedMemberData.get(i).get(PlanMgmtConstants.GENDER)){
					buildQuery.append(" OR prate.GENDER = UPPER( ");
	
					buildQuery.append(":param_" + paramCount);
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.GENDER));
					paramCount++;
					buildQuery.append(" ) ");
				}
				buildQuery.append(") AND p2.state=");
				buildQuery.append(":param_" + paramCount);
				paramList.add(stateName);
				paramCount++;
				buildQuery.append(" AND prate.is_deleted='N' ");
				buildQuery.append(" AND pame.BENEFIT_MAX_VAL IS NOT NULL ");
				buildQuery.append(" ) ");
	
				if (i + 1 != processedMemberData.size()) {
					buildQuery.append(" UNION ALL ");
				}
			}
			
			buildQuery.append(") temp WHERE P.id = temp.plan_id ");	
			buildQuery.append("AND I.ID = P.ISSUER_ID ");		
			buildQuery.append("AND P.INSURANCE_TYPE = 'AME' ");
			buildQuery.append(quotingBusinessLogic.buildPlanCertificationLogic("P", true));
			if(!doQuoteIssuerIHC ){
				buildQuery.append("AND I.name not like '%IHC%'");
			}		
			buildQuery.append("AND p.is_deleted='N' ");
			buildQuery.append(" AND p.id = tp.plan_id");
			buildQuery.append(" AND tp.tenant_id = t.id");
			buildQuery.append(" AND t.code = UPPER(:param_" + paramCount +") ");
			paramCount++;
			paramList.add(tenantCode);
			buildQuery.append("GROUP BY p.id, p.NAME, BENEFIT_MAX_VAL, rate_option, I.hios_issuer_id, I.logo_url ");
			buildQuery.append("HAVING sum(memberctr) = " + processedMemberData.size());
			buildQuery.append(" ORDER BY BENEFIT_MAX_VAL DESC ");
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug(" buildQuery :: " + buildQuery);
			}
			
			DecimalFormat df= new DecimalFormat("#.##");
			EntityManager entityManager = null;
			
			try {	
				entityManager = emf.createEntityManager();
				Query query = entityManager.createNativeQuery(buildQuery.toString());
				for (int i = 0; i < paramCount; i++) {
					query.setParameter("param_" + i, paramList.get(i));
				}
				Iterator rsIterator = query.getResultList().iterator();		
	
				while (rsIterator.hasNext()) {
					String logoURL = null;
					Object[] objArray = (Object[]) rsIterator.next();
					AmePlan amePlan = new AmePlan();
					amePlan.setPlanId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
					amePlan.setName((objArray[PlanMgmtConstants.ONE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ONE].toString());					
					Float householdPremium = Float.parseFloat((objArray[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWO].toString());
					amePlan.setMaxBenefitVal((objArray[PlanMgmtConstants.THREE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.THREE].toString());
					String rateOption = objArray[PlanMgmtConstants.FOUR].toString();
					String hiosIssuerId = objArray[PlanMgmtConstants.FIVE].toString();
					householdPremium = quotingBusinessLogic.calculatePremiumForAMEPlan(rateOption, hiosIssuerId, householdPremium, processedMemberData.size(), stateName, df);
					amePlan.setPremium(householdPremium);
					if(null != objArray[PlanMgmtConstants.SIX]) {
						logoURL = objArray[PlanMgmtConstants.SIX].toString();
					}
					//amePlan.setIssuerLogo((objArray[PlanMgmtConstants.SIX] == null) ? PlanMgmtConstants.EMPTY_STRING : appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.SIX].toString()));
					amePlan.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, hiosIssuerId, null, appUrl));
					amePlanList.add(amePlan);
					amePlan = null;
					break; // get only one record
				}			
			} catch (Exception ex) {
				LOGGER.error("Exception occured in getMaxBenefitAmePlan() ", ex);
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getMaxBenefitAmePlan() ", ex);
			}finally{
				 // close the entity manager
				 if(entityManager !=null && entityManager.isOpen()){
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
			}
			
		}
		
		return amePlanList;
	}
	
	
	@Override
	public List<Map<String,Object>> getAMEAndCrosssellHelper(List<Map<String, String>> processedMemberData, String effectiveDate, String relation,  String familyTierLookupCode, boolean doQuoteIssuerIHC, String stateName, String tenantCode){
		ArrayList<String> paramList = new ArrayList<String>();
		int paramCount = 0; 
		StringBuilder buildQuery = new StringBuilder();
		buildQuery.append("SELECT P.ID, P.NAME AS PLAN_NAME, I.LOGO_URL, sum(pre) AS total_premium, DEDUCTIBLE_VAL, BENEFIT_MAX_VAL, RATE_OPTION, I.hios_issuer_id, ");
		buildQuery.append("PLAN_AME_ID,P.NETWORK_TYPE, P.BROCHURE, I.NAME AS ISSUE_NAME, P.ISSUER_ID, DEDUCTIBLE_ATTR, BENEFIT_MAX_ATTR, EXCLUSIONS_URL");
		buildQuery.append(" FROM ");
		buildQuery.append("PLAN P, ISSUERS I, pm_tenant_plan tp, tenant t,");
		buildQuery.append(" ( ");
		for (int i = 0; i < processedMemberData.size(); i++) {
			// replace relation self/member with "applicant" 
			relation = StringUtils.replaceEach(processedMemberData.get(i).get(PlanMgmtConstants.RELATION).toLowerCase(), new String[]{PlanMgmtConstants.MEMBER,PlanMgmtConstants.SELF}, new String[]{PlanMgmtConstants.APPLICANT, PlanMgmtConstants.APPLICANT})  ;
			
			buildQuery.append(" ( SELECT p2.id AS plan_id, prate.RATE AS pre, 1 as memberctr, ");
			buildQuery.append(" pame.DEDUCTIBLE_VAL as DEDUCTIBLE_VAL, pame.BENEFIT_MAX_VAL as BENEFIT_MAX_VAL, prate.RATE_OPTION AS RATE_OPTION, ");
			buildQuery.append(" pame.ID AS PLAN_AME_ID, pame.DEDUCTIBLE_ATTR as DEDUCTIBLE_ATTR, pame.BENEFIT_MAX_ATTR as BENEFIT_MAX_ATTR, pame.EXCLUSIONS_URL as EXCLUSIONS_URL");
			buildQuery.append(" FROM ");
			buildQuery.append("PLAN_AME pame, PM_PLAN_RATE prate, PLAN p2 ");
			buildQuery.append("WHERE ");
			buildQuery.append("p2.ID = pame.PLAN_ID ");
			buildQuery.append("AND p2.id = prate.plan_id ");
			buildQuery.append("AND TO_DATE (");
			buildQuery.append(" :param_" + paramCount);
			paramList.add(effectiveDate);
			paramCount++;
			buildQuery.append(" , 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date ");
			buildQuery.append(" AND ( ( ");
			buildQuery.append(" CAST (:param_" + paramCount);
			paramList.add(familyTierLookupCode);
			paramCount++;
			buildQuery.append(" AS INTEGER) >= prate.min_age  AND ");
			buildQuery.append(" CAST (:param_" + paramCount);
			paramList.add(familyTierLookupCode);
			paramCount++;
			buildQuery.append(" AS INTEGER) <= prate.max_age) OR (");
			buildQuery.append(" CAST (:param_" + paramCount);
			paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.AGE));
			paramCount++;

			buildQuery.append(" AS INTEGER) >= prate.min_age AND ");
			buildQuery.append(" CAST (:param_" + paramCount);
			paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.AGE));
			paramCount++;

			buildQuery.append(" AS INTEGER) <= prate.max_age AND prate.relation = UPPER('");
			buildQuery.append(relation);
			buildQuery.append("') ) ) AND (prate.GENDER IS NULL");
			if(null != processedMemberData.get(i).get(PlanMgmtConstants.GENDER)){
				buildQuery.append(" OR prate.GENDER = UPPER(");
				buildQuery.append(":param_" + paramCount);
				paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.GENDER));
				paramCount++;
				buildQuery.append(") ");
			}
			buildQuery.append(") AND p2.state= ");
			buildQuery.append(":param_" + paramCount);
			paramList.add(stateName);
			paramCount++;
			buildQuery.append(" ");
			buildQuery.append("AND prate.is_deleted='N' ");			
			buildQuery.append(" ) ");

			if (i + 1 != processedMemberData.size()) {
				buildQuery.append(" UNION ALL ");
			}
		}
		
		buildQuery.append(") temp WHERE P.id = temp.plan_id ");	
		buildQuery.append("AND I.ID = P.ISSUER_ID ");		
		buildQuery.append("AND P.INSURANCE_TYPE = '"+Plan.PlanInsuranceType.AME.toString()+"' ");
		buildQuery.append(quotingBusinessLogic.buildPlanCertificationLogic("P", true));
		if(!doQuoteIssuerIHC ){
			buildQuery.append("AND I.name not like '%IHC%'");
		}		
		buildQuery.append("AND p.is_deleted='N' ");
		buildQuery.append(" AND p.id = tp.plan_id");
		buildQuery.append(" AND tp.tenant_id = t.id");
		buildQuery.append(" AND t.code = UPPER(:param_" + paramCount +") ");
		paramCount++;
		paramList.add(tenantCode);
		buildQuery.append("GROUP BY p.id, p.NAME, I.LOGO_URL, ");
		buildQuery.append("DEDUCTIBLE_VAL, BENEFIT_MAX_VAL, RATE_OPTION, I.hios_issuer_id, ");
		buildQuery.append("PLAN_AME_ID,P.NETWORK_TYPE, P.BROCHURE, I.NAME, P.ISSUER_ID, DEDUCTIBLE_ATTR, BENEFIT_MAX_ATTR, EXCLUSIONS_URL");
		buildQuery.append(" HAVING sum(memberctr) = " + processedMemberData.size());
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("buildQuery :: "  + buildQuery);
		}
		
		List<Map<String,Object>> outDataList = new ArrayList<Map<String,Object>>();
		
		DecimalFormat df= new DecimalFormat("#.##");
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildQuery.toString());
			for (int i = 0; i < paramCount; i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			Iterator<?> rsIterator = query.getResultList().iterator();		
			while (rsIterator.hasNext()) {
				String logoURL = null;
				Object[] objArray = (Object[]) rsIterator.next();
				Map<String,Object> dataMap = new HashMap<String,Object>();
				String hiosIssuerId = objArray[PlanMgmtConstants.SEVEN].toString() == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SEVEN].toString(); 
				
				dataMap.put(PlanMgmtConstants.PLANID, (objArray[PlanMgmtConstants.ZERO] == null ? PlanMgmtConstants.ZERO: Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString())));
				dataMap.put(PlanMgmtConstants.PLAN_NAME, (objArray[PlanMgmtConstants.ONE] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ONE].toString()));
				if(null != objArray[PlanMgmtConstants.TWO]) {
					logoURL = objArray[PlanMgmtConstants.TWO].toString();
				}

				dataMap.put(PlanMgmtConstants.COMPANY_LOGO, (PlanMgmtUtil.getIssuerLogoURL(logoURL, hiosIssuerId, null, appUrl)));

				Float householdPremium = Float.parseFloat((objArray[PlanMgmtConstants.THREE]==null) ? PlanMgmtConstants.EMPTY_STRING:objArray[PlanMgmtConstants.THREE].toString());
				String rateOption = objArray[PlanMgmtConstants.SIX].toString();
				householdPremium = quotingBusinessLogic.calculatePremiumForAMEPlan(rateOption, hiosIssuerId, householdPremium, processedMemberData.size(), stateName, df);
				dataMap.put(PlanMgmtConstants.TOTALPREMIUM, householdPremium);
				
				Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();
				Map<String, String> planDeductible = new HashMap<String, String>();
				Map<String, String> planMaxbenefit = new HashMap<String, String>();
				planDeductible.put(PlanMgmtConstants.DISPLAY_VAL, (objArray[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOUR].toString());
				planMaxbenefit.put(PlanMgmtConstants.DISPLAY_VAL, (objArray[PlanMgmtConstants.FIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FIVE].toString());
				planCosts.put(PlanMgmtConstants.DEDUCTIBLE, planDeductible);
				planCosts.put(PlanMgmtConstants.MAX_BENEFIT, planMaxbenefit);
				dataMap.put(PlanMgmtConstants.PLAN_COST_MAP, planCosts);

				dataMap.put(PlanMgmtConstants.DEDUCTIBLE_VAL, (objArray[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOUR].toString());
				dataMap.put(PlanMgmtConstants.MAX_BENEFIT_VAL, (objArray[PlanMgmtConstants.FIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FIVE].toString());					
				
				dataMap.put(PlanMgmtConstants.PLAN_AME_ID, (objArray[PlanMgmtConstants.EIGHT] == null ? PlanMgmtConstants.ZERO : Integer.parseInt(objArray[PlanMgmtConstants.EIGHT].toString())));
				dataMap.put(PlanMgmtConstants.NETWORK_TYPE, (objArray[PlanMgmtConstants.NINE] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.NINE].toString()));
				
				String brochureUrl = ((objArray[PlanMgmtConstants.TEN])==null?PlanMgmtConstants.EMPTY_STRING:objArray[PlanMgmtConstants.TEN].toString());
				if (StringUtils.isNotBlank(brochureUrl)) {				     
					 if (brochureUrl.matches(PlanMgmtConstants.ECM_REGEX)) {
					  brochureUrl = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(brochureUrl);    
					 }
					 else if (!brochureUrl.matches(PlanMgmtConstants.URL_REGEX)) {
					  brochureUrl = "http://" + brochureUrl;    
					 }				    
				} else {
					brochureUrl = StringUtils.EMPTY;
				}
				dataMap.put(PlanMgmtConstants.PLAN_BROCHURE, brochureUrl);
				
				dataMap.put(PlanMgmtConstants.ISSUER_NAME, (objArray[PlanMgmtConstants.ELEVEN] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ELEVEN].toString()));
				dataMap.put(PlanMgmtConstants.ISSUER_ID, (objArray[PlanMgmtConstants.TWELVE] == null ? PlanMgmtConstants.ZERO : Integer.parseInt(objArray[PlanMgmtConstants.TWELVE].toString())));
				dataMap.put(PlanMgmtConstants.DEDUCTIBLE_ATTR, (objArray[PlanMgmtConstants.THIRTEEN] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.THIRTEEN].toString()));
				dataMap.put(PlanMgmtConstants.BENEFIT_MAX_ATTR, (objArray[PlanMgmtConstants.FOURTEEN] == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOURTEEN].toString()));
				
				String exclusionsAndLimitations = ((objArray[PlanMgmtConstants.FIFTEEN])==null?PlanMgmtConstants.EMPTY_STRING:objArray[PlanMgmtConstants.FIFTEEN].toString());
				if(StringUtils.isNotBlank(exclusionsAndLimitations)) {
					 if (exclusionsAndLimitations.matches(PlanMgmtConstants.ECM_REGEX)) {
					  exclusionsAndLimitations = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(exclusionsAndLimitations);    
					 } else if (!exclusionsAndLimitations.matches(PlanMgmtConstants.URL_REGEX)) {
					  exclusionsAndLimitations = PlanMgmtConstants.HTTP_PROTOCOL + exclusionsAndLimitations;    
					 }				     
				}else{
					exclusionsAndLimitations = StringUtils.EMPTY;
				}				

				dataMap.put(PlanMgmtConstants.EXCLUSIONS_URL, exclusionsAndLimitations);
				
				outDataList.add(dataMap);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured in getAMEAndCrosssellHelper() ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getAMEAndCrosssellHelper() ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		//LOGGER.info("outDataList: " + outDataList);
		return outDataList;
	}

}
