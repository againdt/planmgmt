package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingRequestDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingResponseDTO;
import com.getinsured.hix.dto.planmgmt.Member;
import com.getinsured.hix.dto.prescription.PrescriptionSearchRequest;
import com.getinsured.hix.dto.shop.employer.eps.EmployerQuotingDTO;
import com.getinsured.hix.dto.shop.mlec.MLECRestRequest;
import com.getinsured.hix.dto.shop.mlec.MLECRestResponse;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.platform.util.exception.GIException;


public interface PlanRateBenefitService {
	
	double getBenchmarkRate(List<Map<String, String>> censusData, String coverageStartDate, String externalCaseId) throws GIException;
	
	Map<String, String> getBenchmarkRateForEnrollment(List<Map<String, String>> censusData, String coverageStartDate, String costShareLevel, String tenantCode, String externalCaseId, String currentHiosIssuerID) throws Exception;

	double getLowestPremiumPlanRate(String dob, String zip,
			String coverageStartDate, String tobacco, String planLevel,
			String planMarket, String countyCode);

	List<PlanRateBenefit> getHouseholdPlanRateBenefits(
			List<Map<String, String>> memberList, String insType,
			String effectiveDate, String costSharing, int groupId,
			String planIdStr, boolean showCatastrophicPlan, String planLevel,
			String ehbCovered, List<Map<String, List<String>>> providerList,
			String marketType, String isSpecialEnrollment, int issuerId, boolean issuerVerifiedFlag, 
			String exchangeType, String tenant, boolean minimizePlanData, String hiosPlanNumber, 
			String keepOnly, List<PrescriptionSearchRequest> prescriptionSearchRequstList,
			String eliminatePlanIds, String restrictHiosIds, String showPUFPlans,
			String currentHiosIssuerID, String isPlanChange);

	Map<String, Float> getEmployeePlanRate(
			List<Map<String, String>> employeeCensusData,
			String empPrimaryZip, String empCountyCode, String planId,
			String effectiveDate, boolean quoteByEmpPrimaryZip);
	
	MLECRestResponse getMemberLevelEmployeeRate(MLECRestRequest request);
	
	List<String> getCostNames();
	
	Map<String, Float> getEmployeeMinMaxRate(List<Map<String, String>> employeeCensusData, String empPrimaryZip, String empCountyCode, String effectiveDate, String insuranceType);
	
	List<PlanRateBenefit> getMemberLevelPlanRate(String hiosPlanNumber, List<Map<String, String>> memberList, String effectiveDate);
	
	EmployerQuotingDTO getEmployerPlanRateHelper(EmployerQuotingDTO employerQuotingDTO);
	
	List<PlanRateBenefit> getLowestDentalPlan(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenantCode);
	
	LifeSpanEnrollmentQuotingResponseDTO lifeSpanEnrollmentQuoting(LifeSpanEnrollmentQuotingRequestDTO lifeSpanEnrollmentQuotingRequestDTO, LifeSpanEnrollmentQuotingResponseDTO lifeSpanEnrollmentQuotingResponseDTO, String insuranceType);
	
	float getLowestPremium(String insuranceType, String exchangeType, String tenantCode, String effectiveDate, List<Member> memberList, boolean showCatastrophicPlan, String restrictHiosIds);

	List<Map<String, String>> getAvailableSilverPlans(List<Map<String, String>> censusData, String inputCoverageStartDate, String costShareLevel, String tenantCode, String externalCaseId, String currentHiosIssuerID) throws Exception;

}