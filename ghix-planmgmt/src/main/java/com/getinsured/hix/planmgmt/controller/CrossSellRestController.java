/**
 * CrossSellRestController.java
 * @author santanu
 * @version 1.0
 * @since July 24, 2014 
 * This REST controller would be use to handle Cross Sell request and response
 */

package com.getinsured.hix.planmgmt.controller;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.CrossSellPlan;
import com.getinsured.hix.dto.planmgmt.CrossSellPlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.CrossSellPlanResponseDTO;
import com.getinsured.hix.planmgmt.service.CrossSellService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;


//@Api(value="Cross Sell plan information", description ="Returning Cross Sell Plans benefit and rate information depends on zip, county, monthly income and census information")
@Controller
@RequestMapping(value = "/crosssell")
public class CrossSellRestController {
	private static final Logger LOGGER = Logger.getLogger(CrossSellRestController.class);
	
	@Autowired
	private CrossSellService crossSellService;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;

	/* test controller */	
//	@RequestMapping(value = "/welcome", method = RequestMethod.GET) // for testing purpose
//	@ResponseBody public String welcome()
//	{
//		LOGGER.info("Welcome to GHIX-Planmgmt module, invoke CrossSellRestController()");		
//		return "Welcome to GHIX-Planmgmt module, invoke CrossSellRestController";
//	}
	
	/* return lowest dental plan premium */
//	@ApiOperation(value="getlowestdentalpremium", notes="This API takes effective date, exchange type and census information and retriving cross sell lowest premium dental plan")
//	@ApiResponse(value="Return Cross Sell lowest premium dental plan")
	@RequestMapping(value = "/getlowestdentalpremium", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public CrossSellPlanResponseDTO getLowestDentalPremium(@RequestBody CrossSellPlanRequestDTO crossSellPlanRequestDTO )
	{
		LOGGER.info("inside getLowestDentalPremium");	

		if(LOGGER.isDebugEnabled()){
			StringBuilder buildStr = new StringBuilder(512);
			buildStr.append("Request data ===>");			
			buildStr.append(" Effective date : ");
			buildStr.append(crossSellPlanRequestDTO.getEffectiveDate());
			buildStr.append(", exchangeType : ");
			buildStr.append(crossSellPlanRequestDTO.getExchangeType());
			buildStr.append(", memberList : ");
			buildStr.append(crossSellPlanRequestDTO.getMemberList());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));	
		}
		CrossSellPlanResponseDTO crossSellPlanResponseDTO = new CrossSellPlanResponseDTO();
		
		if(null == crossSellPlanRequestDTO || null == crossSellPlanRequestDTO.getEffectiveDate() || crossSellPlanRequestDTO.getEffectiveDate().trim().isEmpty()				
			|| null == crossSellPlanRequestDTO.getMemberList() || crossSellPlanRequestDTO.getMemberList().isEmpty()) {
			crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		
		} else {
			List<Map<String, String>> memberList = (List<Map<String, String>>)crossSellPlanRequestDTO.getMemberList();			
			String effectiveDate = crossSellPlanRequestDTO.getEffectiveDate().trim();
			String exchangeType = (crossSellPlanRequestDTO.getExchangeType() == null || PlanMgmtConstants.EMPTY_STRING.equals(crossSellPlanRequestDTO.getExchangeType().trim())) ? PlanMgmtConstants.ON : crossSellPlanRequestDTO.getExchangeType(); // default value is ON
			String tenant =  (StringUtils.isNotEmpty(crossSellPlanRequestDTO.getTenant())) ? crossSellPlanRequestDTO.getTenant() : PlanMgmtConstants.TENANT_GINS;

			try{			
				Float lowestDentalPlanPremium = Float.parseFloat("0");
				lowestDentalPlanPremium = crossSellService.getLowestDentalPlanPremium(memberList, effectiveDate, exchangeType, tenant);			
				crossSellPlanResponseDTO.setLowestPremium(lowestDentalPlanPremium.toString());		
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				crossSellPlanResponseDTO.endResponse();
			}catch(Exception ex) {
				LOGGER.error("Failed to execute CrossSellRestController :: getLowestDentalPremium(). Exception:", ex);		
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute CrossSellRestController :: getLowestDentalPremium() ",ex);
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}		
			
		}
		
		return crossSellPlanResponseDTO;
	}
	
	/* return lowest ame plan premium */
//	@ApiOperation(value="getlowestamepremium", notes="This API takes effective date, exchange type and census information retriving cross sell lowest premium AME plan")
//	@ApiResponse(value="return cross sell  lowest premium ame plans")
	@RequestMapping(value = "/getlowestamepremium", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public CrossSellPlanResponseDTO getLowestAmePremium(@RequestBody CrossSellPlanRequestDTO crossSellPlanRequestDTO )
	{
		LOGGER.info("inside getLowestAmePremium");	
	
		if(LOGGER.isDebugEnabled()){
			StringBuilder buildStr = new StringBuilder(512);
			buildStr.append("Request data ===>");
			buildStr.append(" Effective date : ");
			buildStr.append(crossSellPlanRequestDTO.getEffectiveDate());
			buildStr.append(", exchangeType : ");
			buildStr.append(crossSellPlanRequestDTO.getExchangeType());
			buildStr.append(", memberList : ");
			buildStr.append(crossSellPlanRequestDTO.getMemberList());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));	
		}
		CrossSellPlanResponseDTO crossSellPlanResponseDTO = new CrossSellPlanResponseDTO();
		
		if(null == crossSellPlanRequestDTO || null == crossSellPlanRequestDTO.getMemberList() || crossSellPlanRequestDTO.getMemberList().isEmpty()
				|| null == crossSellPlanRequestDTO.getEffectiveDate() || crossSellPlanRequestDTO.getEffectiveDate().trim().isEmpty()	) {
			crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		} else {
			List<Map<String, String>> memberList = (List<Map<String, String>>)crossSellPlanRequestDTO.getMemberList();			
			String exchangeType = (crossSellPlanRequestDTO.getExchangeType() == null || PlanMgmtConstants.EMPTY_STRING.equals(crossSellPlanRequestDTO.getExchangeType().trim())) ? PlanMgmtConstants.ON : crossSellPlanRequestDTO.getExchangeType(); // default value is ON 
			String effectiveDate = crossSellPlanRequestDTO.getEffectiveDate().trim();
			String tenant =  (StringUtils.isNotEmpty(crossSellPlanRequestDTO.getTenant())) ? crossSellPlanRequestDTO.getTenant().trim() : PlanMgmtConstants.TENANT_GINS.trim();
			try{			
				BigDecimal lowestAmePlanPremium = BigDecimal.ZERO;
				lowestAmePlanPremium = crossSellService.getLowestAMEPlanPremium(memberList, effectiveDate, exchangeType, tenant);			
				crossSellPlanResponseDTO.setLowestPremium(lowestAmePlanPremium.toString());			
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				crossSellPlanResponseDTO.endResponse();
			}catch(Exception ex) {
				LOGGER.error("Failed to execute CrossSellRestController :: getLowestAmePremium(). Exception:", ex);				
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute CrossSellRestController :: getLowestAmePremium() ",ex);
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}		
			
		}
		
		return crossSellPlanResponseDTO;
	}
	
	// HIX-61441 compute lowest vision plan premium
//	@ApiOperation(value="getlowestvisionpremium", notes="This API takes effective date, exchange type and census information retriving cross sell lowest premium vision plan")
//	@ApiResponse(value="return cross sell lowest premium vision plans")
	@RequestMapping(value = "/getlowestvisionpremium", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public CrossSellPlanResponseDTO getLowestVisionPremium(@RequestBody CrossSellPlanRequestDTO crossSellPlanRequestDTO )
	{
		LOGGER.info("inside getLowestVisionPremium");	
	
		if(LOGGER.isDebugEnabled()){
			StringBuilder buildStr = new StringBuilder(512);
			buildStr.append("Request data ===>");
			buildStr.append(" Effective date : ");
			buildStr.append(crossSellPlanRequestDTO.getEffectiveDate());
			buildStr.append(", exchangeType : ");
			buildStr.append(crossSellPlanRequestDTO.getExchangeType());
			buildStr.append(", memberList : ");
			buildStr.append(crossSellPlanRequestDTO.getMemberList());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));	
		}
		CrossSellPlanResponseDTO crossSellPlanResponseDTO = new CrossSellPlanResponseDTO();
		
		if(null == crossSellPlanRequestDTO || null == crossSellPlanRequestDTO.getMemberList() || crossSellPlanRequestDTO.getMemberList().isEmpty()
				|| null == crossSellPlanRequestDTO.getEffectiveDate() || crossSellPlanRequestDTO.getEffectiveDate().trim().isEmpty()	) {
			crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		} else {
			List<Map<String, String>> memberList = (List<Map<String, String>>)crossSellPlanRequestDTO.getMemberList();			
			String exchangeType = (crossSellPlanRequestDTO.getExchangeType() == null || PlanMgmtConstants.EMPTY_STRING.equals(crossSellPlanRequestDTO.getExchangeType().trim())) ? PlanMgmtConstants.ON : crossSellPlanRequestDTO.getExchangeType(); // default value is ON 
			String effectiveDate = crossSellPlanRequestDTO.getEffectiveDate().trim();
			String tenant =  (StringUtils.isNotEmpty(crossSellPlanRequestDTO.getTenant())) ? crossSellPlanRequestDTO.getTenant().trim() : PlanMgmtConstants.TENANT_GINS.trim();
			
			try{			
				Float lowestVisionPlanPremium = (float)0;
				lowestVisionPlanPremium = crossSellService.getLowestVisionPlanPremium(memberList, effectiveDate, exchangeType, tenant);								
				crossSellPlanResponseDTO.setLowestPremium(lowestVisionPlanPremium.toString());
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				crossSellPlanResponseDTO.endResponse();
			}catch(Exception ex) {
				LOGGER.error("Failed to execute CrossSellRestController :: getLowestVisionPremium(). Exception:", ex);	
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute CrossSellRestController :: getLowestVisionPremium() ",ex);
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}					
		}
		
		return crossSellPlanResponseDTO;		
	}
	
	
	/* return dental plans for carousel */
//	@ApiOperation(value="getdentalplans", notes="This API takes effective date, exchange type and census information for retriving dental Plans")
//	@ApiResponse(value="return the Issuer Info such as Name, Premium, Logo, Plan Costs and Benefits")
	@RequestMapping(value = "/getdentalplans", method = RequestMethod.POST) 
	@Produces("application/json")
	@ResponseBody public CrossSellPlanResponseDTO getDentalPlans(@RequestBody CrossSellPlanRequestDTO crossSellPlanRequestDTO )
	{
		LOGGER.info("inside getDentalPlans");	
		
		if(LOGGER.isDebugEnabled()){
			StringBuilder buildStr = new StringBuilder(512);
			buildStr.append("Request data ===>");			
			buildStr.append(" Effective date : ");
			buildStr.append(crossSellPlanRequestDTO.getEffectiveDate());
			buildStr.append(", exchangeType : ");
			buildStr.append(crossSellPlanRequestDTO.getExchangeType());
			buildStr.append(", memberList : ");
			buildStr.append(crossSellPlanRequestDTO.getMemberList());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));	
		}
		
		CrossSellPlanResponseDTO crossSellPlanResponseDTO = new CrossSellPlanResponseDTO();
		
		if(null == crossSellPlanRequestDTO || null == crossSellPlanRequestDTO.getEffectiveDate() || crossSellPlanRequestDTO.getEffectiveDate().trim().isEmpty()				
			|| null == crossSellPlanRequestDTO.getMemberList() || crossSellPlanRequestDTO.getMemberList().isEmpty()) {
			crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		
		} else {
			List<Map<String, String>> memberList = (List<Map<String, String>>)crossSellPlanRequestDTO.getMemberList();			
			String effectiveDate = crossSellPlanRequestDTO.getEffectiveDate().trim();
			String exchangeType = (crossSellPlanRequestDTO.getExchangeType() == null || PlanMgmtConstants.EMPTY_STRING.equals(crossSellPlanRequestDTO.getExchangeType().trim())) ? PlanMgmtConstants.ON : crossSellPlanRequestDTO.getExchangeType(); // default value is ON 
			String tenant =  (StringUtils.isNotEmpty(crossSellPlanRequestDTO.getTenant())) ? crossSellPlanRequestDTO.getTenant().trim() : PlanMgmtConstants.TENANT_GINS.trim();
				
			try{			
				List<CrossSellPlan> planList = crossSellService.getDentalPlans(memberList, effectiveDate, exchangeType, tenant);	
				
				crossSellPlanResponseDTO.setCrossSellPlanList(planList);
				crossSellPlanResponseDTO.setInsuranceType(PlanMgmtConstants.DENTAL);
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				crossSellPlanResponseDTO.endResponse();
			}catch(Exception ex) {
				LOGGER.error("Failed to execute CrossSellRestController :: getDentalPlans(). Exception:", ex);		
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute CrossSellRestController :: getDentalPlans() ",ex);
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}		
			
		}
		
		return crossSellPlanResponseDTO;
	}
	
	/* return ame plans for carousel */
//	@ApiOperation(value="getameplans", notes="This API takes effective date, exchange type and census information and retriving cross sell AME Plans")
//	@ApiResponse(value="Return list of Cross Sell AME Plans having insurance type AME")
	@RequestMapping(value = "/getameplans", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public CrossSellPlanResponseDTO getAMEPlans(@RequestBody CrossSellPlanRequestDTO crossSellPlanRequestDTO )
	{
		LOGGER.info("inside getAMEPlans");	
		
		if(LOGGER.isDebugEnabled()){
			StringBuilder buildStr = new StringBuilder(512);
			buildStr.append("Request data ===>");			
			buildStr.append(" Effective date : ");
			buildStr.append(crossSellPlanRequestDTO.getEffectiveDate());
			buildStr.append(", exchangeType : ");
			buildStr.append(crossSellPlanRequestDTO.getExchangeType());
			buildStr.append(", memberList : ");
			buildStr.append(crossSellPlanRequestDTO.getMemberList());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));	
		}
		CrossSellPlanResponseDTO crossSellPlanResponseDTO = new CrossSellPlanResponseDTO();
		
		if(null == crossSellPlanRequestDTO || null == crossSellPlanRequestDTO.getEffectiveDate() || crossSellPlanRequestDTO.getEffectiveDate().trim().isEmpty()				
			|| null == crossSellPlanRequestDTO.getMemberList() || crossSellPlanRequestDTO.getMemberList().isEmpty()) {
			crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		
		} else {
			List<Map<String, String>> memberList = (List<Map<String, String>>)crossSellPlanRequestDTO.getMemberList();			
			String effectiveDate = crossSellPlanRequestDTO.getEffectiveDate().trim();
			String exchangeType = (crossSellPlanRequestDTO.getExchangeType() == null || PlanMgmtConstants.EMPTY_STRING.equals(crossSellPlanRequestDTO.getExchangeType().trim())) ? PlanMgmtConstants.ON : crossSellPlanRequestDTO.getExchangeType(); // default value is ON
			String tenant =  (StringUtils.isNotEmpty(crossSellPlanRequestDTO.getTenant())) ? crossSellPlanRequestDTO.getTenant().trim() : PlanMgmtConstants.TENANT_GINS.trim();
			
			try{			
				List<CrossSellPlan> planList = crossSellService.getAMEPlans(memberList, effectiveDate, exchangeType, tenant);	
			
				crossSellPlanResponseDTO.setCrossSellPlanList(planList);
				crossSellPlanResponseDTO.setInsuranceType(PlanMgmtConstants.AME);
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				crossSellPlanResponseDTO.endResponse();
			}catch(Exception ex) {
				LOGGER.error("Failed to execute CrossSellRestController :: getAMEPlans(). Exception:", ex);	
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute CrossSellRestController :: getAMEPlans() ",ex);
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}		
			
		}
		
		return crossSellPlanResponseDTO;
	}
	
	
	//HIX-61442 
	/* return vision plans for carousel */
//	@ApiOperation(value="getvisionplans", notes="This API takes effective date, exchange type and census information retriving cross sell vision plans list")
//	@ApiResponse(value="return cross sell vision plans list")
	@RequestMapping(value = "/getvisionplans", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public CrossSellPlanResponseDTO getVisionPlans(@RequestBody CrossSellPlanRequestDTO crossSellPlanRequestDTO )
	{		
		LOGGER.info("inside getVisionPlans");	
	
		if(LOGGER.isDebugEnabled()){
			StringBuilder buildStr = new StringBuilder(512);
			buildStr.append("Request data ===>");			
			buildStr.append(" Effective date : ");
			buildStr.append(crossSellPlanRequestDTO.getEffectiveDate());
			buildStr.append(", exchangeType : ");
			buildStr.append(crossSellPlanRequestDTO.getExchangeType());
			buildStr.append(", memberList : ");
			buildStr.append(crossSellPlanRequestDTO.getMemberList());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));	
		}
		
		CrossSellPlanResponseDTO crossSellPlanResponseDTO = new CrossSellPlanResponseDTO();
		
		if(null == crossSellPlanRequestDTO || null == crossSellPlanRequestDTO.getEffectiveDate() || crossSellPlanRequestDTO.getEffectiveDate().trim().isEmpty()				
			|| null == crossSellPlanRequestDTO.getMemberList() || crossSellPlanRequestDTO.getMemberList().isEmpty()) {
			crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		
		} else {
			List<Map<String, String>> memberList = (List<Map<String, String>>)crossSellPlanRequestDTO.getMemberList();			
			String effectiveDate = crossSellPlanRequestDTO.getEffectiveDate().trim();
			String exchangeType = (crossSellPlanRequestDTO.getExchangeType() == null || PlanMgmtConstants.EMPTY_STRING.equals(crossSellPlanRequestDTO.getExchangeType().trim()) ) ? PlanMgmtConstants.ON : crossSellPlanRequestDTO.getExchangeType(); // default value is ON
			String tenant =  (StringUtils.isNotEmpty(crossSellPlanRequestDTO.getTenant())) ? crossSellPlanRequestDTO.getTenant().trim() : PlanMgmtConstants.TENANT_GINS.trim();
			try{			
				List<CrossSellPlan> planList = crossSellService.getVisionPlans(memberList, effectiveDate, exchangeType, tenant);	
			
				crossSellPlanResponseDTO.setCrossSellPlanList(planList);
				crossSellPlanResponseDTO.setInsuranceType(PlanMgmtConstants.VISION);
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				crossSellPlanResponseDTO.endResponse();
			}catch(Exception ex) {
				LOGGER.error("Failed to execute CrossSellRestController :: getVisionPlans(). Exception:", ex);		
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute CrossSellRestController :: getVisionPlans() ",ex);
				crossSellPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				crossSellPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				crossSellPlanResponseDTO.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}		
			
		}
		
		return crossSellPlanResponseDTO;
	}	
	
}
