//
// This file was com.getinsured.hix.planmgmt.phix.ws.quotit by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// com.getinsured.hix.planmgmt.phix.ws.quotit on: 2015.01.14 at 11:56:49 AM PST 
//


package com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.getinsured.hix.planmgmt.phix.ws.quotit.FetchCustomProductsRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.FormularyLookupRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarrierLoadHistoryRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarriersPlansBenefitsRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetCountiesRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetCustomProductsQuoteRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupQuoteRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpShoppingCartsRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetPlansbyRxRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.GetZipCodeInfoRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.ListRxRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitCustomProductsRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitFamilyRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupShoppingCartRequest;
import com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitIfpShoppingCartRequest;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * com.getinsured.hix.planmgmt.phix.ws.quotit in the com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {
	private final static String NAMESPACE = "http://www.quotit.com/Services/ActWS/ACA/2";
    private final static QName _GetCountiesResponseGetCountiesResult_QNAME = new QName(NAMESPACE, "GetCountiesResult");
    private final static QName _ListRxResponseListRxResult_QNAME = new QName(NAMESPACE, "ListRxResult");
    private final static QName _SubmitFamilyResponseSubmitFamilyResult_QNAME = new QName(NAMESPACE, "SubmitFamilyResult");
    private final static QName _ListRxRequest_QNAME = new QName(NAMESPACE, "request");
    private final static QName _FetchCustomProductsRequest_QNAME = new QName(NAMESPACE, "Request");
    private final static QName _GetGroupQuoteQuoteRequest_QNAME = new QName(NAMESPACE, "QuoteRequest");
    private final static QName _GetFamilyResponseGetFamilyResult_QNAME = new QName(NAMESPACE, "GetFamilyResult");
    private final static QName _GetGroupResponseGetGroupResult_QNAME = new QName(NAMESPACE, "GetGroupResult");
    private final static QName _FetchCustomProductsResponseFetchCustomProductsResult_QNAME = new QName(NAMESPACE, "FetchCustomProductsResult");
    private final static QName _FormularyLookupResponseFormularyLookupResult_QNAME = new QName(NAMESPACE, "FormularyLookupResult");
    private final static QName _GetCustomProductsQuoteResponseGetCustomProductsQuoteResult_QNAME = new QName(NAMESPACE, "GetCustomProductsQuoteResult");
    private final static QName _SubmitGroupShoppingCartResponseSubmitGroupShoppingCartResult_QNAME = new QName(NAMESPACE, "SubmitGroupShoppingCartResult");
    private final static QName _SubmitIfpShoppingCartResponseSubmitIfpShoppingCartResult_QNAME = new QName(NAMESPACE, "SubmitIfpShoppingCartResult");
    private final static QName _GetCarriersPlansBenefitsResponseGetCarriersPlansBenefitsResult_QNAME = new QName(NAMESPACE, "GetCarriersPlansBenefitsResult");
    private final static QName _SubmitCustomProductsResponseSubmitCustomProductsResult_QNAME = new QName(NAMESPACE, "SubmitCustomProductsResult");
    private final static QName _GetCountiesGetCountiesRequest_QNAME = new QName(NAMESPACE, "GetCountiesRequest");
    private final static QName _GetPlansbyRxResponseGetPlansbyRxResult_QNAME = new QName(NAMESPACE, "GetPlansbyRxResult");
    private final static QName _SubmitGroupResponseSubmitGroupResult_QNAME = new QName(NAMESPACE, "SubmitGroupResult");
    private final static QName _GetIfpShoppingCartsResponseGetIfpShoppingCartsResult_QNAME = new QName(NAMESPACE, "GetIfpShoppingCartsResult");
    private final static QName _GetCarrierLoadHistoryResponseGetCarrierLoadHistoryResult_QNAME = new QName(NAMESPACE, "GetCarrierLoadHistoryResult");
    private final static QName _GetIfpQuoteResponseGetIfpQuoteResult_QNAME = new QName(NAMESPACE, "GetIfpQuoteResult");
    private final static QName _GetGroupQuoteResponseGetGroupQuoteResult_QNAME = new QName(NAMESPACE, "GetGroupQuoteResult");
    private final static QName _GetZipCodeInfoResponseGetZipCodeInfoResult_QNAME = new QName(NAMESPACE, "GetZipCodeInfoResult");
    private final static QName _GetZipCodeInfoGetZipCodeInfoRequest_QNAME = new QName(NAMESPACE, "GetZipCodeInfoRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpQuoteResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpQuoteResponse createGetIfpQuoteResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpQuoteResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCustomProductsQuoteResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCustomProductsQuoteResponse createGetCustomProductsQuoteResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCustomProductsQuoteResponse();
    }

    /**
     * Create an instance of {@link SubmitCustomProducts }
     * 
     */
    public SubmitCustomProducts createSubmitCustomProducts() {
        return new SubmitCustomProducts();
    }

    /**
     * Create an instance of {@link GetPlansbyRx }
     * 
     */
    public GetPlansbyRx createGetPlansbyRx() {
        return new GetPlansbyRx();
    }

    /**
     * Create an instance of {@link GetGroupQuote }
     * 
     */
    public GetGroupQuote createGetGroupQuote() {
        return new GetGroupQuote();
    }

    /**
     * Create an instance of {@link GetIfpShoppingCarts }
     * 
     */
    public GetIfpShoppingCarts createGetIfpShoppingCarts() {
        return new GetIfpShoppingCarts();
    }

    /**
     * Create an instance of {@link GetCounties }
     * 
     */
    public GetCounties createGetCounties() {
        return new GetCounties();
    }

    /**
     * Create an instance of {@link FormularyLookup }
     * 
     */
    public FormularyLookup createFormularyLookup() {
        return new FormularyLookup();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitFamilyResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitFamilyResponse createSubmitFamilyResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitFamilyResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.ListRxResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.ListRxResponse createListRxResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.ListRxResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse createGetCarriersPlansBenefitsResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetZipCodeInfoResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetZipCodeInfoResponse createGetZipCodeInfoResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetZipCodeInfoResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitGroupShoppingCartResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitGroupShoppingCartResponse createSubmitGroupShoppingCartResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitGroupShoppingCartResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitCustomProductsResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitCustomProductsResponse createSubmitCustomProductsResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitCustomProductsResponse();
    }

    /**
     * Create an instance of {@link GetGroup }
     * 
     */
    public GetGroup createGetGroup() {
        return new GetGroup();
    }

    /**
     * Create an instance of {@link GetFamily }
     * 
     */
    public GetFamily createGetFamily() {
        return new GetFamily();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.FormularyLookupResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.FormularyLookupResponse createFormularyLookupResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.FormularyLookupResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCarrierLoadHistoryResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCarrierLoadHistoryResponse createGetCarrierLoadHistoryResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCarrierLoadHistoryResponse();
    }

    /**
     * Create an instance of {@link ListRx }
     * 
     */
    public ListRx createListRx() {
        return new ListRx();
    }

    /**
     * Create an instance of {@link GetCustomProductsQuote }
     * 
     */
    public GetCustomProductsQuote createGetCustomProductsQuote() {
        return new GetCustomProductsQuote();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetPlansbyRxResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetPlansbyRxResponse createGetPlansbyRxResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetPlansbyRxResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetGroupResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetGroupResponse createGetGroupResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetGroupResponse();
    }

    /**
     * Create an instance of {@link SubmitFamily }
     * 
     */
    public SubmitFamily createSubmitFamily() {
        return new SubmitFamily();
    }

    /**
     * Create an instance of {@link GetCarrierLoadHistory }
     * 
     */
    public GetCarrierLoadHistory createGetCarrierLoadHistory() {
        return new GetCarrierLoadHistory();
    }

    /**
     * Create an instance of {@link GetCarriersPlansBenefits }
     * 
     */
    public GetCarriersPlansBenefits createGetCarriersPlansBenefits() {
        return new GetCarriersPlansBenefits();
    }

    /**
     * Create an instance of {@link SubmitGroup }
     * 
     */
    public SubmitGroup createSubmitGroup() {
        return new SubmitGroup();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitIfpShoppingCartResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitIfpShoppingCartResponse createSubmitIfpShoppingCartResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitIfpShoppingCartResponse();
    }

    /**
     * Create an instance of {@link SubmitIfpShoppingCart }
     * 
     */
    public SubmitIfpShoppingCart createSubmitIfpShoppingCart() {
        return new SubmitIfpShoppingCart();
    }

    /**
     * Create an instance of {@link GetIfpQuote }
     * 
     */
    public GetIfpQuote createGetIfpQuote() {
        return new GetIfpQuote();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitGroupResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitGroupResponse createSubmitGroupResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitGroupResponse();
    }

    /**
     * Create an instance of {@link GetZipCodeInfo }
     * 
     */
    public GetZipCodeInfo createGetZipCodeInfo() {
        return new GetZipCodeInfo();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetGroupQuoteResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetGroupQuoteResponse createGetGroupQuoteResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetGroupQuoteResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetFamilyResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetFamilyResponse createGetFamilyResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetFamilyResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCountiesResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCountiesResponse createGetCountiesResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCountiesResponse();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.FetchCustomProductsResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.FetchCustomProductsResponse createFetchCustomProductsResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.FetchCustomProductsResponse();
    }

    /**
     * Create an instance of {@link FetchCustomProducts }
     * 
     */
    public FetchCustomProducts createFetchCustomProducts() {
        return new FetchCustomProducts();
    }

    /**
     * Create an instance of {@link SubmitGroupShoppingCart }
     * 
     */
    public SubmitGroupShoppingCart createSubmitGroupShoppingCart() {
        return new SubmitGroupShoppingCart();
    }

    /**
     * Create an instance of {@link com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpShoppingCartsResponse }
     * 
     */
    public com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpShoppingCartsResponse createGetIfpShoppingCartsResponse() {
        return new com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpShoppingCartsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetCountiesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetCountiesResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCountiesResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetCountiesResponse> createGetCountiesResponseGetCountiesResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetCountiesResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetCountiesResponse>(_GetCountiesResponseGetCountiesResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetCountiesResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCountiesResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.ListRxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "ListRxResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.ListRxResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.ListRxResponse> createListRxResponseListRxResult(com.getinsured.hix.planmgmt.phix.ws.quotit.ListRxResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.ListRxResponse>(_ListRxResponseListRxResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.ListRxResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.ListRxResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitFamilyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "SubmitFamilyResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitFamilyResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitFamilyResponse> createSubmitFamilyResponseSubmitFamilyResult(com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitFamilyResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitFamilyResponse>(_SubmitFamilyResponseSubmitFamilyResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitFamilyResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitFamilyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListRxRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = ListRx.class)
    public JAXBElement<ListRxRequest> createListRxRequest(ListRxRequest value) {
        return new JAXBElement<ListRxRequest>(_ListRxRequest_QNAME, ListRxRequest.class, ListRx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitGroupShoppingCartRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = SubmitGroupShoppingCart.class)
    public JAXBElement<SubmitGroupShoppingCartRequest> createSubmitGroupShoppingCartRequest(SubmitGroupShoppingCartRequest value) {
        return new JAXBElement<SubmitGroupShoppingCartRequest>(_ListRxRequest_QNAME, SubmitGroupShoppingCartRequest.class, SubmitGroupShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FormularyLookupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = FormularyLookup.class)
    public JAXBElement<FormularyLookupRequest> createFormularyLookupRequest(FormularyLookupRequest value) {
        return new JAXBElement<FormularyLookupRequest>(_ListRxRequest_QNAME, FormularyLookupRequest.class, FormularyLookup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FetchCustomProductsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "Request", scope = FetchCustomProducts.class)
    public JAXBElement<FetchCustomProductsRequest> createFetchCustomProductsRequest(FetchCustomProductsRequest value) {
        return new JAXBElement<FetchCustomProductsRequest>(_FetchCustomProductsRequest_QNAME, FetchCustomProductsRequest.class, FetchCustomProducts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupQuoteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "QuoteRequest", scope = GetGroupQuote.class)
    public JAXBElement<GetGroupQuoteRequest> createGetGroupQuoteQuoteRequest(GetGroupQuoteRequest value) {
        return new JAXBElement<GetGroupQuoteRequest>(_GetGroupQuoteQuoteRequest_QNAME, GetGroupQuoteRequest.class, GetGroupQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetFamilyResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetFamilyResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyResponse> createGetFamilyResponseGetFamilyResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyResponse>(_GetFamilyResponseGetFamilyResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetFamilyResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetGroupResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetGroupResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupResponse> createGetGroupResponseGetGroupResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupResponse>(_GetGroupResponseGetGroupResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.FetchCustomProductsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "FetchCustomProductsResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.FetchCustomProductsResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.FetchCustomProductsResponse> createFetchCustomProductsResponseFetchCustomProductsResult(com.getinsured.hix.planmgmt.phix.ws.quotit.FetchCustomProductsResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.FetchCustomProductsResponse>(_FetchCustomProductsResponseFetchCustomProductsResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.FetchCustomProductsResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.FetchCustomProductsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetGroupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = GetGroup.class)
    public JAXBElement<GetGroupRequest> createGetGroupRequest(GetGroupRequest value) {
        return new JAXBElement<GetGroupRequest>(_ListRxRequest_QNAME, GetGroupRequest.class, GetGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.FormularyLookupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "FormularyLookupResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.FormularyLookupResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.FormularyLookupResponse> createFormularyLookupResponseFormularyLookupResult(com.getinsured.hix.planmgmt.phix.ws.quotit.FormularyLookupResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.FormularyLookupResponse>(_FormularyLookupResponseFormularyLookupResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.FormularyLookupResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.FormularyLookupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetCustomProductsQuoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetCustomProductsQuoteResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCustomProductsQuoteResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetCustomProductsQuoteResponse> createGetCustomProductsQuoteResponseGetCustomProductsQuoteResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetCustomProductsQuoteResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetCustomProductsQuoteResponse>(_GetCustomProductsQuoteResponseGetCustomProductsQuoteResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetCustomProductsQuoteResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCustomProductsQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupShoppingCartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "SubmitGroupShoppingCartResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitGroupShoppingCartResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupShoppingCartResponse> createSubmitGroupShoppingCartResponseSubmitGroupShoppingCartResult(com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupShoppingCartResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupShoppingCartResponse>(_SubmitGroupShoppingCartResponseSubmitGroupShoppingCartResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupShoppingCartResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitGroupShoppingCartResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitIfpShoppingCartResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "SubmitIfpShoppingCartResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitIfpShoppingCartResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitIfpShoppingCartResponse> createSubmitIfpShoppingCartResponseSubmitIfpShoppingCartResult(com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitIfpShoppingCartResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitIfpShoppingCartResponse>(_SubmitIfpShoppingCartResponseSubmitIfpShoppingCartResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitIfpShoppingCartResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitIfpShoppingCartResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarrierLoadHistoryRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = GetCarrierLoadHistory.class)
    public JAXBElement<GetCarrierLoadHistoryRequest> createGetCarrierLoadHistoryRequest(GetCarrierLoadHistoryRequest value) {
        return new JAXBElement<GetCarrierLoadHistoryRequest>(_ListRxRequest_QNAME, GetCarrierLoadHistoryRequest.class, GetCarrierLoadHistory.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFamilyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = GetFamily.class)
    public JAXBElement<GetFamilyRequest> createGetFamilyRequest(GetFamilyRequest value) {
        return new JAXBElement<GetFamilyRequest>(_ListRxRequest_QNAME, GetFamilyRequest.class, GetFamily.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpShoppingCartsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = GetIfpShoppingCarts.class)
    public JAXBElement<GetIfpShoppingCartsRequest> createGetIfpShoppingCartsRequest(GetIfpShoppingCartsRequest value) {
        return new JAXBElement<GetIfpShoppingCartsRequest>(_ListRxRequest_QNAME, GetIfpShoppingCartsRequest.class, GetIfpShoppingCarts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarriersPlansBenefitsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetCarriersPlansBenefitsResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarriersPlansBenefitsResponse> createGetCarriersPlansBenefitsResponseGetCarriersPlansBenefitsResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarriersPlansBenefitsResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarriersPlansBenefitsResponse>(_GetCarriersPlansBenefitsResponseGetCarriersPlansBenefitsResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarriersPlansBenefitsResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCarriersPlansBenefitsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitGroupRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = SubmitGroup.class)
    public JAXBElement<SubmitGroupRequest> createSubmitGroupRequest(SubmitGroupRequest value) {
        return new JAXBElement<SubmitGroupRequest>(_ListRxRequest_QNAME, SubmitGroupRequest.class, SubmitGroup.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitCustomProductsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "SubmitCustomProductsResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitCustomProductsResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitCustomProductsResponse> createSubmitCustomProductsResponseSubmitCustomProductsResult(com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitCustomProductsResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitCustomProductsResponse>(_SubmitCustomProductsResponseSubmitCustomProductsResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitCustomProductsResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitCustomProductsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCountiesRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetCountiesRequest", scope = GetCounties.class)
    public JAXBElement<GetCountiesRequest> createGetCountiesGetCountiesRequest(GetCountiesRequest value) {
        return new JAXBElement<GetCountiesRequest>(_GetCountiesGetCountiesRequest_QNAME, GetCountiesRequest.class, GetCounties.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitIfpShoppingCartRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = SubmitIfpShoppingCart.class)
    public JAXBElement<SubmitIfpShoppingCartRequest> createSubmitIfpShoppingCartRequest(SubmitIfpShoppingCartRequest value) {
        return new JAXBElement<SubmitIfpShoppingCartRequest>(_ListRxRequest_QNAME, SubmitIfpShoppingCartRequest.class, SubmitIfpShoppingCart.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetPlansbyRxResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetPlansbyRxResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetPlansbyRxResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetPlansbyRxResponse> createGetPlansbyRxResponseGetPlansbyRxResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetPlansbyRxResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetPlansbyRxResponse>(_GetPlansbyRxResponseGetPlansbyRxResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetPlansbyRxResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetPlansbyRxResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "SubmitGroupResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitGroupResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupResponse> createSubmitGroupResponseSubmitGroupResult(com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupResponse>(_SubmitGroupResponseSubmitGroupResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.SubmitGroupResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.SubmitGroupResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpShoppingCartsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetIfpShoppingCartsResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpShoppingCartsResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpShoppingCartsResponse> createGetIfpShoppingCartsResponseGetIfpShoppingCartsResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpShoppingCartsResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpShoppingCartsResponse>(_GetIfpShoppingCartsResponseGetIfpShoppingCartsResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpShoppingCartsResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpShoppingCartsResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitCustomProductsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "Request", scope = SubmitCustomProducts.class)
    public JAXBElement<SubmitCustomProductsRequest> createSubmitCustomProductsRequest(SubmitCustomProductsRequest value) {
        return new JAXBElement<SubmitCustomProductsRequest>(_FetchCustomProductsRequest_QNAME, SubmitCustomProductsRequest.class, SubmitCustomProducts.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarrierLoadHistoryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetCarrierLoadHistoryResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCarrierLoadHistoryResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarrierLoadHistoryResponse> createGetCarrierLoadHistoryResponseGetCarrierLoadHistoryResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarrierLoadHistoryResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarrierLoadHistoryResponse>(_GetCarrierLoadHistoryResponseGetCarrierLoadHistoryResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetCarrierLoadHistoryResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetCarrierLoadHistoryResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitFamilyRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = SubmitFamily.class)
    public JAXBElement<SubmitFamilyRequest> createSubmitFamilyRequest(SubmitFamilyRequest value) {
        return new JAXBElement<SubmitFamilyRequest>(_ListRxRequest_QNAME, SubmitFamilyRequest.class, SubmitFamily.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetIfpQuoteResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpQuoteResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteResponse> createGetIfpQuoteResponseGetIfpQuoteResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteResponse>(_GetIfpQuoteResponseGetIfpQuoteResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetIfpQuoteResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetIfpQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomProductsQuoteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "QuoteRequest", scope = GetCustomProductsQuote.class)
    public JAXBElement<GetCustomProductsQuoteRequest> createGetCustomProductsQuoteQuoteRequest(GetCustomProductsQuoteRequest value) {
        return new JAXBElement<GetCustomProductsQuoteRequest>(_GetGroupQuoteQuoteRequest_QNAME, GetCustomProductsQuoteRequest.class, GetCustomProductsQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlansbyRxRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = GetPlansbyRx.class)
    public JAXBElement<GetPlansbyRxRequest> createGetPlansbyRxRequest(GetPlansbyRxRequest value) {
        return new JAXBElement<GetPlansbyRxRequest>(_ListRxRequest_QNAME, GetPlansbyRxRequest.class, GetPlansbyRx.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupQuoteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetGroupQuoteResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetGroupQuoteResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupQuoteResponse> createGetGroupQuoteResponseGetGroupQuoteResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupQuoteResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupQuoteResponse>(_GetGroupQuoteResponseGetGroupQuoteResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetGroupQuoteResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetGroupQuoteResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetZipCodeInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetZipCodeInfoResult", scope = com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetZipCodeInfoResponse.class)
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetZipCodeInfoResponse> createGetZipCodeInfoResponseGetZipCodeInfoResult(com.getinsured.hix.planmgmt.phix.ws.quotit.GetZipCodeInfoResponse value) {
        return new JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetZipCodeInfoResponse>(_GetZipCodeInfoResponseGetZipCodeInfoResult_QNAME, com.getinsured.hix.planmgmt.phix.ws.quotit.GetZipCodeInfoResponse.class, com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2.GetZipCodeInfoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetIfpQuoteRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "QuoteRequest", scope = GetIfpQuote.class)
    public JAXBElement<GetIfpQuoteRequest> createGetIfpQuoteQuoteRequest(GetIfpQuoteRequest value) {
        return new JAXBElement<GetIfpQuoteRequest>(_GetGroupQuoteQuoteRequest_QNAME, GetIfpQuoteRequest.class, GetIfpQuote.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetZipCodeInfoRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "GetZipCodeInfoRequest", scope = GetZipCodeInfo.class)
    public JAXBElement<GetZipCodeInfoRequest> createGetZipCodeInfoGetZipCodeInfoRequest(GetZipCodeInfoRequest value) {
        return new JAXBElement<GetZipCodeInfoRequest>(_GetZipCodeInfoGetZipCodeInfoRequest_QNAME, GetZipCodeInfoRequest.class, GetZipCodeInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCarriersPlansBenefitsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = NAMESPACE, name = "request", scope = GetCarriersPlansBenefits.class)
    public JAXBElement<GetCarriersPlansBenefitsRequest> createGetCarriersPlansBenefitsRequest(GetCarriersPlansBenefitsRequest value) {
        return new JAXBElement<GetCarriersPlansBenefitsRequest>(_ListRxRequest_QNAME, GetCarriersPlansBenefitsRequest.class, GetCarriersPlansBenefits.class, value);
    }

}
