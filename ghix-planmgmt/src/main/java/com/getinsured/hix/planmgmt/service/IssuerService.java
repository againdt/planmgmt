/**
 * 
 */
package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.feeds.IssuerInfoDTO;
import com.getinsured.hix.dto.planmgmt.IssuerBrandNameDTO;
import com.getinsured.hix.dto.planmgmt.IssuerByHIOSIdResponseDTO;
import com.getinsured.hix.dto.planmgmt.IssuerListRequestDTO;
import com.getinsured.hix.dto.planmgmt.IssuerPaymentInfoResponse;
import com.getinsured.hix.dto.planmgmt.SingleIssuerResponse;
import com.getinsured.hix.model.Issuer;

/**
 * @author gajulapalli_k
 * 
 */
public interface IssuerService {
	/**
	 * Issuers will have only one disclaimer at any time
	 * 
	 * @param issuerId
	 * @return
	 */
	String getDisclaimerInfo(String issuerId, String exchangeType);

	/**
	 * returns list of issuers of certain zipcode and county code
	 * @param zipCode
	 * @param countyCode
	 * @return
	 */
	List getIssuersByZip(String zipCode, String countyCode);
	
	Issuer getIssuerbyId(Integer id);
	
	List<String> getIssuerD2CList();
	
	/**
	 * Returns Payment Information for given issuer.
	 * @param hiosId : Primary key from Issuers table.
	 * @return instance of IssuerPaymentInformation
	 */
	IssuerPaymentInfoResponse getIssuerPaymentInfoByHiosId(String hiosId);
	
	/**
	 * returns list of issuers of certain tenantCode, stateCode and insuranceType
	 * @param tenantCode
	 * @param stateCode
	 * @param insuranceType
	 * @return
	 */
	List<IssuerInfoDTO> getIssuerList(String tenantCode, String stateCode, String insuranceType, int applicableYear);
	
	/**
	 * return list of issuer id and issuer brand name depending on userId
	 * @param userId
	 * @return
	 */
	List<IssuerBrandNameDTO> getIssuerAndIssuerBrandNameByUserId(Integer userId);
	
	/**
	 * Returns list of issuer brand names and id by IssuerId's list
	 * @param issuerListDetailsDTO
	 * @return
	 */
	List<IssuerBrandNameDTO> getIssuerBrandNameListByIssuerList(IssuerListRequestDTO issuerListRequestDTO);
	
	/**
	 * Returns issuer object on given HIOS issuer id
	 * @param hiosIssuerId
	 * @return
	 */
	Issuer getIssuerByHIOSId(String hiosIssuerId);
	
	/**
	 * Returns issuer object on given issuer name
	 * @param issuerName
	 * @return
	 */
	List<Integer> getIssuerByName(String issuerName);
	
	/**
	 * Returns SingleIssuerResponse DTO on given issuer id
	 * @param issuerId
	 * @param minimizeIssuerData
	 * @return
	 */
	SingleIssuerResponse getIssuerInfoById(Integer id, boolean minimizeIssuerData, boolean includeLogo);
	
	/**
	 * Returns IssuerByHIOSIdResponseDTO DTO on given HIOS issuer id
	 * @param hiosIssuerId
	 * @return
	 */
	IssuerByHIOSIdResponseDTO getIssuerInfoByHIOSId(String hiosIssuerId, boolean includeLogo);
	
	String getApplicationUrl(Issuer issuerObj, String tenantCode);

	Map<Integer, String> getIssuersLegalNameMapByIssuerIdList(List<Integer> issuerIdList);
}
