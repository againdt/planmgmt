package com.getinsured.hix.planmgmt.querybuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.service.QuotingBusinessLogic;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.GhixConstants;

@Component
public class LowestPremiumQueryBuilder {

	@Autowired 
	private LookupService lookupService;
	
	@Autowired
	private QuotingBusinessLogic quotingBusinessLogic; 
	
	private static final Logger LOGGER = Logger.getLogger(LowestPremiumQueryBuilder.class);
	
	public StringBuilder buildQuery(ArrayList<String> paramList, List<Map<String, String>> processedHouseholdData, 
			String insuranceType, String exchangeType, String tenantCode, String effectiveDate, String configuredDB, boolean showCatastrophicPlan, String restrictHiosIds, boolean joinToIssuers){
		
		StringBuilder buildQuery = new StringBuilder(2048);
		
		try{
			String applicantStateCode =  quotingBusinessLogic.getApplicantStateCode(processedHouseholdData);							// get applicant state code
			String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedHouseholdData, insuranceType);						// get household type
			String familyTier = quotingBusinessLogic.getFamilyTier(processedHouseholdData); 											// compute family tier
			String familyTierLookupCode = quotingBusinessLogic.getFamilyTierLookupCode(familyTier); 								    // compute family tier lookup code
			String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);					// get exchange type
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
			String applicableYear = effectiveDate.substring(0,4);
			
			buildQuery.append("SELECT p.id, sum(pre) AS total_premium, rate_option, p.ISSUER_PLAN_NUMBER ");
			buildQuery.append("FROM ");
			buildQuery.append("plan p, ");
			
			if(joinToIssuers){
				buildQuery.append("issuers i, ");
			}
			
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildQuery.append(" pm_tenant_plan tp, tenant t, ");
			}
			buildQuery.append(" ( ");

			String applicantZip = null;
			String applicantCounty = null;
	
			for (int i = 0; i < processedHouseholdData.size(); i++) {
				buildQuery.append("( SELECT prate.rate AS pre, prate.rate_option AS rate_option, 1 AS memberctr, childtable.plan_id ");
				buildQuery.append("FROM ");
				
				if(PlanMgmtConstants.HEALTH.equalsIgnoreCase(insuranceType)){
					buildQuery.append("plan_health childtable, ");
				}else{
					buildQuery.append("plan_dental childtable, ");
				}
				
				buildQuery.append("pm_service_area psarea, pm_plan_rate prate, plan p2 "); 
				buildQuery.append("WHERE ");
				buildQuery.append("p2.id = childtable.plan_id ");
				buildQuery.append("AND childtable.parent_plan_id = 0 "); // pick all base plan of all metal tier
				buildQuery.append("AND p2.service_area_id = psarea.service_area_id ");
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildQuery.append("AND psarea.zip = to_number(trim(:param_" + paramList.size() + "))");			
					paramList.add(processedHouseholdData.get(i).get(PlanMgmtConstants.ZIP));
				}else{
					buildQuery.append("AND psarea.zip = :param_" + paramList.size());			
					paramList.add(processedHouseholdData.get(i).get(PlanMgmtConstants.ZIP));
				}

				if (null == applicantZip) {
					applicantZip = processedHouseholdData.get(i).get(PlanMgmtConstants.ZIP);
				}
				buildQuery.append(" AND p2.id = prate.plan_id ");
				buildQuery.append("AND p2.is_deleted = 'N' ");
				buildQuery.append("AND psarea.is_deleted = 'N' ");
				buildQuery.append("AND prate.rating_area_id = ");
				
				// -------- changes for HIX-100092 starts --------
				buildQuery.append(quotingBusinessLogic.buildSearchRatingAreaLogic(stateCode, paramList, processedHouseholdData, i, applicableYear, configuredDB));
				// -------- changes for HIX-100092 ends --------
				
				// prepare rate query
				buildQuery.append(quotingBusinessLogic.buildRateLogic("prate", applicantStateCode, houseHoldType, familyTierLookupCode, processedHouseholdData.get(i).get(PlanMgmtConstants.AGE), processedHouseholdData.get(i).get(PlanMgmtConstants.TOBACCO), effectiveDate, paramList));
				
				// HIX-90037 : if showCatastrophicPlan flag value is false then consider all metal tier plans except CATASTROPHIC
				if(!showCatastrophicPlan){
					buildQuery.append(" AND childtable.plan_level != '");
					buildQuery.append(Plan.PlanLevel.CATASTROPHIC.toString());
					buildQuery.append("'");
				}
				
				if (!StringUtils.isBlank(processedHouseholdData.get(i).get(PlanMgmtConstants.COUNTYCODE))) {
					buildQuery.append(" AND psarea.fips = :param_" + paramList.size());
					paramList.add(processedHouseholdData.get(i).get(PlanMgmtConstants.COUNTYCODE));

					if (null == applicantCounty) {
						applicantCounty = processedHouseholdData.get(i).get(PlanMgmtConstants.COUNTYCODE);
					}
				}
				buildQuery.append(" ) ");
	
				if (i + 1 != processedHouseholdData.size()) {
					buildQuery.append(" UNION ALL ");
				}
	
			}
	
			buildQuery.append(") temp WHERE p.id = temp.plan_id AND TO_DATE (:param_");
			buildQuery.append(paramList.size());
			buildQuery.append(", 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date AND p.insurance_type='");
			buildQuery.append(insuranceType);
			buildQuery.append("' ");
			paramList.add(effectiveDate);
			
			// build plan certification logic 
			buildQuery.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
			
			// build plan enrollment availability logic
			buildQuery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic("p", effectiveDate, PlanMgmtConstants.NO, applicantZip, applicantCounty, null, paramList));
	
			buildQuery.append(" AND p.market = '");
			buildQuery.append(Plan.PlanMarket.INDIVIDUAL);
			buildQuery.append("' ");
			
			// HIX-79254 - On OFF exchange flow, show ON exchange plans having enable_for_offexch_flow flag as YES along with OFF exchange plans
			if(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF.equalsIgnoreCase(exchangeType)){	 // OFF exchange flow
				buildQuery.append(" AND (p.exchange_type = 'OFF' OR (p.exchange_type = 'ON' AND UPPER(p.enable_for_offexch_flow) = 'YES')) ");
			}else{
				buildQuery.append(" AND p.exchange_type = 'ON' "); // ON exchange flow
			}
			
			if(joinToIssuers){
				buildQuery.append(" AND i.hios_issuer_id NOT IN ( :paramRestrictHiosIdList ) AND p.issuer_id = i.id ") ;
			}
			
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildQuery.append(" AND p.id = tp.plan_id");
				buildQuery.append(" AND tp.tenant_id = t.id");
				buildQuery.append(" AND t.code = :param_" + paramList.size());
				paramList.add(tenantCode.toUpperCase());
			}
			
			// build plan household availability logic
			buildQuery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic("p", houseHoldType));
			buildQuery.append(" GROUP BY ");
			buildQuery.append("p.id, rate_option, p.ISSUER_PLAN_NUMBER ");
			buildQuery.append("HAVING sum(memberctr) = " + processedHouseholdData.size());
			
			// HIX-79254 - On OFF exchange flow, use order by to sort OFF exchange plans as top in result set
			if(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF.equalsIgnoreCase(exchangeType)){ 
				buildQuery.append(" ORDER BY p.exchange_type ASC "); 
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in LowestPremiumQueryBuilder.buildQuery() : ", ex);
		}

		
		return buildQuery;
		
	}
}
