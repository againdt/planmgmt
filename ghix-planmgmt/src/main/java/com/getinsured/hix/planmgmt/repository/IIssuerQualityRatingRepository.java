package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.IssuerQualityRating;

public interface IIssuerQualityRatingRepository extends JpaRepository<IssuerQualityRating, Integer>{
	
	@Query("SELECT iqr FROM IssuerQualityRating iqr WHERE iqr.planHiosId IN (:HIOSIdList) AND isDeleted = 'N' AND to_date(:effectiveDate, 'YYYY-MM-DD') BETWEEN iqr.effectiveDate AND iqr.effectiveEndDate ORDER BY iqr.id DESC")	
	List<IssuerQualityRating> findRatingByHIOSPlanIdList(@Param("HIOSIdList") List<String> HIOSPlanIdList, @Param("effectiveDate") String effectiveDate);
}
