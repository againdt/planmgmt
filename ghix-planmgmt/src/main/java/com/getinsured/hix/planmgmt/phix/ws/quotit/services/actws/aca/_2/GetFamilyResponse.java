//
// This file was com.getinsured.hix.planmgmt.phix.ws.quotit by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// com.getinsured.hix.planmgmt.phix.ws.quotit on: 2015.01.14 at 11:56:49 AM PST 
//


package com.getinsured.hix.planmgmt.phix.ws.quotit.services.actws.aca._2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetFamilyResult" type="{}GetFamily.Response" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getFamilyResult"
})
@XmlRootElement(name = "GetFamilyResponse")
public class GetFamilyResponse {

    @XmlElementRef(name = "GetFamilyResult", namespace = "http://www.quotit.com/Services/ActWS/ACA/2", type = JAXBElement.class, required = false)
    protected JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyResponse> getFamilyResult;

    /**
     * Gets the value of the getFamilyResult property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyResponse }{@code >}
     *     
     */
    public JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyResponse> getGetFamilyResult() {
        return getFamilyResult;
    }

    /**
     * Sets the value of the getFamilyResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyResponse }{@code >}
     *     
     */
    public void setGetFamilyResult(JAXBElement<com.getinsured.hix.planmgmt.phix.ws.quotit.GetFamilyResponse> value) {
        this.getFamilyResult = value;
    }

}
