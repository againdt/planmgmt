//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.14 at 11:56:49 AM PST 
//


package com.getinsured.hix.planmgmt.phix.ws.quotit;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NetworkCostType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DrugPrescriptionPeriodType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CostSharingType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CoPayment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CoInsurance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPlansbyRx.Response.Carrier.Plan.PlanFormulary.FormularyTier.CostSharingTypeVO", propOrder = {
    "networkCostType",
    "drugPrescriptionPeriodType",
    "costSharingType",
    "coPayment",
    "coInsurance"
})
public class GetPlansbyRxResponseCarrierPlanPlanFormularyFormularyTierCostSharingTypeVO {

    @XmlElementRef(name = "NetworkCostType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> networkCostType;
    @XmlElementRef(name = "DrugPrescriptionPeriodType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> drugPrescriptionPeriodType;
    @XmlElementRef(name = "CostSharingType", type = JAXBElement.class, required = false)
    protected JAXBElement<String> costSharingType;
    @XmlElementRef(name = "CoPayment", type = JAXBElement.class, required = false)
    protected JAXBElement<String> coPayment;
    @XmlElementRef(name = "CoInsurance", type = JAXBElement.class, required = false)
    protected JAXBElement<String> coInsurance;

    /**
     * Gets the value of the networkCostType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNetworkCostType() {
        return networkCostType;
    }

    /**
     * Sets the value of the networkCostType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNetworkCostType(JAXBElement<String> value) {
        this.networkCostType = value;
    }

    /**
     * Gets the value of the drugPrescriptionPeriodType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getDrugPrescriptionPeriodType() {
        return drugPrescriptionPeriodType;
    }

    /**
     * Sets the value of the drugPrescriptionPeriodType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setDrugPrescriptionPeriodType(JAXBElement<String> value) {
        this.drugPrescriptionPeriodType = value;
    }

    /**
     * Gets the value of the costSharingType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCostSharingType() {
        return costSharingType;
    }

    /**
     * Sets the value of the costSharingType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCostSharingType(JAXBElement<String> value) {
        this.costSharingType = value;
    }

    /**
     * Gets the value of the coPayment property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCoPayment() {
        return coPayment;
    }

    /**
     * Sets the value of the coPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCoPayment(JAXBElement<String> value) {
        this.coPayment = value;
    }

    /**
     * Gets the value of the coInsurance property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCoInsurance() {
        return coInsurance;
    }

    /**
     * Sets the value of the coInsurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCoInsurance(JAXBElement<String> value) {
        this.coInsurance = value;
    }

}
