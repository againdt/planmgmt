/**
 * 
 */
package com.getinsured.hix.planmgmt.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.feeds.IssuerInfoDTO;
import com.getinsured.hix.dto.feeds.IssuerListDetailsDTO;
import com.getinsured.hix.dto.planmgmt.IssuerBrandNameDTO;
import com.getinsured.hix.dto.planmgmt.IssuerBrandNameResponseDTO;
import com.getinsured.hix.dto.planmgmt.IssuerByHIOSIdRequestDTO;
import com.getinsured.hix.dto.planmgmt.IssuerByHIOSIdResponseDTO;
import com.getinsured.hix.dto.planmgmt.IssuerEnrollmentEndDateRequestDTO;
import com.getinsured.hix.dto.planmgmt.IssuerEnrollmentEndDateResponseDTO;
import com.getinsured.hix.dto.planmgmt.IssuerListRequestDTO;
import com.getinsured.hix.dto.planmgmt.IssuerPaymentInfoResponse;
import com.getinsured.hix.dto.planmgmt.IssuerRequest;
import com.getinsured.hix.dto.planmgmt.IssuerResponse;
import com.getinsured.hix.dto.planmgmt.IssuersLegalNameResponseDTO;
import com.getinsured.hix.dto.planmgmt.SingleIssuerResponse;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;

/**
 * The REST service handler Class for issuerService
 * 
 * @author gajulapalli_k
 * 
 */
//@Api(value="Issuer information Controller", description ="Returning Issuer information depends on provided information")
@Controller
@RequestMapping(value = "/issuerInfo")
public class IssuerRestController {

	private static final Logger LOGGER = Logger
			.getLogger(IssuerRestController.class);

	private static final String INVALIDREQUEST = "Invalid request parameters.";
	@Autowired
	private IssuerService issuerService;

	public IssuerRestController() {
	}

			
//	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
//	// for testing purpose
//	@ResponseBody
//	public String welcome() {
//		LOGGER.info("Welcome to GHIX-Planmgmt module, invoke IssuerController()");
//		return "Welcome to GHIX-Planmgmt module, invoke IssuerController";
//	}

//	@ApiOperation(value="disclaimerInfo", notes="This API takes Issuer Id and Exchange Type for retriving disclaimer text")
//	@ApiResponse(value="return Disclaimer Information text of the Issuer depends on Exchanges Type")
	@RequestMapping(value = "/disclaimerInfo", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody
	IssuerResponse getDisclaimerInfo(@RequestBody IssuerRequest issuerRequest) {
		IssuerResponse issuerResponse = new IssuerResponse();
		String disclaimerInfo = null;
		LOGGER.info("Beginning to process getDisclaimerInfo().");
		
	
		// if request data elements are null throw error
		if(null == issuerRequest ||  issuerRequest.getId() == null || issuerRequest.getExchangeType() == null || issuerRequest.getExchangeType().equals("") ) {
			issuerResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			issuerResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			issuerResponse.setErrMsg(INVALIDREQUEST);
		}else{		
			try {			
				disclaimerInfo = issuerService.getDisclaimerInfo(issuerRequest.getId(),issuerRequest.getExchangeType());
			
				if (disclaimerInfo != null) {			
					issuerResponse.setDisclaimerInfo(disclaimerInfo);
				} else{
					issuerResponse.setDisclaimerInfo("");
				}
			
			} catch (Exception e) {
				LOGGER.error("Exception occured while getting DisclaimerInfo",e);
				issuerResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_DISCLAIMER_INFO.code);
				issuerResponse.setErrMsg(Arrays.toString(e.getStackTrace()));
				issuerResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}	
		
		return issuerResponse;
	}

//	@ApiOperation(value="byzipcode", notes="This API takes zip and county for retriving information of issuers")
//	@ApiResponse(value="return list of issuers information for that zip and county")
	@RequestMapping(value = "/byzipcode", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody
	IssuerResponse getIssuersByZip(@RequestBody IssuerRequest issuerRequest) {
		IssuerResponse issuerResponse = new IssuerResponse();
		LOGGER.info("Beginning to process getIssuersByZip().");
		
		if(null == issuerRequest || null == issuerRequest.getZipCode() || issuerRequest.getZipCode().equals("")) {
			issuerResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			issuerResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			issuerResponse.setErrMsg(INVALIDREQUEST);
		}else{
			String zipCode = issuerRequest.getZipCode();
			String countyCode = (issuerRequest.getCountyCode() == null) ? "" : issuerRequest.getCountyCode();
			try {
				List issuerList = issuerService.getIssuersByZip(zipCode, countyCode);
				issuerResponse.setIssuerList(issuerList);
				issuerResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			} catch (Exception e) {
				LOGGER.error("Exception occured while getting byzipcode",e);
				issuerResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUERS_BY_ZIP.code);
				issuerResponse.setErrMsg(Arrays.toString(e.getStackTrace()));
				issuerResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}	

		return issuerResponse;
	}
//	
//	@ApiOperation(value="byId", notes="This API takes Issuer Id for retriving issuer information")
//	@ApiResponse(value="return complete issuer information for provided issuer Id")
	@RequestMapping(value = "/byId", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody
	SingleIssuerResponse getIssuerById(@RequestBody IssuerRequest issuerRequest) {

		LOGGER.info("Beginning to process getIssuerById().");
		SingleIssuerResponse singleIssuerResponse = new SingleIssuerResponse();
		try {
			if (null == issuerRequest || StringUtils.isEmpty(issuerRequest.getId())) {
				singleIssuerResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				singleIssuerResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				singleIssuerResponse.setErrMsg(INVALIDREQUEST);
			} else {
				boolean minimizeIssuerData = issuerRequest.isMinimizeIssuerData();
				// call getIssuerInfoById service to invoke issuer data
				singleIssuerResponse = issuerService.getIssuerInfoById(Integer.parseInt(issuerRequest.getId()), minimizeIssuerData, issuerRequest.getIncludeLogo());
				
				if(singleIssuerResponse.getId() > 0){
					singleIssuerResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					singleIssuerResponse.endResponse();
				}else{
					singleIssuerResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					singleIssuerResponse.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
					singleIssuerResponse.endResponse();
				}
				
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured in getIssuerById()", e);
			singleIssuerResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_BY_ID.code);
			singleIssuerResponse.setErrMsg(Arrays.toString(e.getStackTrace()));
			singleIssuerResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return singleIssuerResponse;
	}

//	@ApiOperation(value="getIssuerD2CInfo", notes="This operation retrieves issuers direct to customer information")
//	@ApiResponse(value="return list of issuers direct to customer information")
	@RequestMapping(value = "/getIssuerD2CInfo", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public List<String> getIssuerD2CInfo() {
		LOGGER.info("Beginning to process getIssuerD2CInfo()");
		List<String> issuerd2csList = new ArrayList<String>();
		try {
			issuerd2csList = issuerService.getIssuerD2CList();
		} catch (Exception e) {
			LOGGER.error("Exception occured in getIssuerById()", e);
		}
		return issuerd2csList;
	}
	

//	@ApiOperation(value="paymentInfoByID", notes="This operation retrieves issuer payment information by id")
//	@ApiResponse(value="return payment information of issuer for provided issuer Id")
	@RequestMapping(value = "/paymentInfoByID", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public IssuerPaymentInfoResponse getIssuerPaymentInfoById(@RequestBody IssuerRequest issuerRequest) {
		LOGGER.info("Beginning to process getPaymentInfoByID()");
		IssuerPaymentInfoResponse issuerPaymentInfoResponse = null;
		try {
			if(!StringUtils.isEmpty(issuerRequest.getId()) && issuerRequest.getId().length() == PlanMgmtConstants.FIVE){
				issuerPaymentInfoResponse = issuerService.getIssuerPaymentInfoByHiosId(issuerRequest.getId());
			}else{
				LOGGER.error("Hios id is invalid : " + SecurityUtil.sanitizeForLogging(issuerRequest.getId()) + " Returning null API response.");
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured in getIssuerPaymentInfoById()", e);
		}
		return issuerPaymentInfoResponse;
	}
	
	
//	@ApiOperation(value="getissuerlist", notes="This API takes tenent code, insurance type and state code to return issuer list")
//	@ApiResponse(value="return list of issuers for given tenent code, insurance type and state code")
	@RequestMapping(value = "/getissuerlist", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public IssuerListDetailsDTO getIssuerList(@RequestBody IssuerListDetailsDTO issuerListDetailsDTO) {
		LOGGER.info("Beginning to process getIssuerList().");
		
		if(null != issuerListDetailsDTO ) {
			if(StringUtils.isBlank(issuerListDetailsDTO.getTenantCode().trim()) 
			|| StringUtils.isBlank(issuerListDetailsDTO.getStateCode().trim())
			|| StringUtils.isBlank(issuerListDetailsDTO.getInsuranceType().trim())) {
				issuerListDetailsDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				issuerListDetailsDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				issuerListDetailsDTO.setErrMsg(INVALIDREQUEST);
			}else{
				String tenantCode = issuerListDetailsDTO.getTenantCode().trim();
				String stateCode = issuerListDetailsDTO.getStateCode().trim();
				String insuranceType = issuerListDetailsDTO.getInsuranceType().trim();
				int applicableYear = TSCalendar.getInstance().get(Calendar.YEAR);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("tenantCode = " + SecurityUtil.sanitizeForLogging(tenantCode) + ", stateCode = " + SecurityUtil.sanitizeForLogging(stateCode) + ", insuranceType = " + SecurityUtil.sanitizeForLogging(insuranceType) + ", applicableYear =" + applicableYear);			
				}
				try {
					List<IssuerInfoDTO> issuerList = issuerService.getIssuerList(tenantCode, stateCode, insuranceType, applicableYear);
					issuerListDetailsDTO.setIssuersList(issuerList);
					issuerListDetailsDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				} catch (Exception e) {
					LOGGER.error("Exception occured while getting getIssuerList",e);
					issuerListDetailsDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUERS_LIST.code);
					issuerListDetailsDTO.setErrMsg(Arrays.toString(e.getStackTrace()));
					issuerListDetailsDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			}
		}	

		return issuerListDetailsDTO;
	}

	
//	@ApiOperation(value="getenrollmentenddateflag", notes="This API takes issuer id as request and return enrollment end date flag value")
//	@ApiResponse(value="return enrollment end date flag value for given issuer id")
	@RequestMapping(value = "/getenrollmentenddateflag", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public IssuerEnrollmentEndDateResponseDTO getenrollmentenddateflag(@RequestBody IssuerEnrollmentEndDateRequestDTO issuerEnrollmentEndDateRequestDTO) {
		LOGGER.info("Beginning to process getenrollmentenddateflag().");
		
		IssuerEnrollmentEndDateResponseDTO issuerEnrollmentEndDateResponseDTO = new IssuerEnrollmentEndDateResponseDTO();
		if(null == issuerEnrollmentEndDateRequestDTO 
			|| StringUtils.isBlank(issuerEnrollmentEndDateRequestDTO.getIssuerId().toString()) 
			) {
			issuerEnrollmentEndDateResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			issuerEnrollmentEndDateResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			issuerEnrollmentEndDateResponseDTO.setErrMsg(INVALIDREQUEST);
			
		}else{
			Integer issuerId = issuerEnrollmentEndDateRequestDTO.getIssuerId();

			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("issuer Id = " + SecurityUtil.sanitizeForLogging(issuerEnrollmentEndDateRequestDTO.getIssuerId().toString()));			
			}
			try {
				
				Issuer issuerObj = issuerService.getIssuerbyId(issuerId);
				if(issuerObj != null){
					boolean flagVal = false;
					if(issuerObj.getSendEnrollmentEndDateFlag().equalsIgnoreCase("true")){
						flagVal = true;
					}
					issuerEnrollmentEndDateResponseDTO.setEnrollmentEndDateFlag(flagVal);
					issuerEnrollmentEndDateResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
					issuerEnrollmentEndDateResponseDTO.endResponse();
				}
				else{
					issuerEnrollmentEndDateResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_SEND_ENROLLMENT_END_DATE.code);
					issuerEnrollmentEndDateResponseDTO.setErrMsg("Invalid Issuer Id");
					issuerEnrollmentEndDateResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
			} catch (Exception e) {
				LOGGER.error("Exception occured while getting getenrollmentenddateflag()",e);
				issuerEnrollmentEndDateResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_SEND_ENROLLMENT_END_DATE.code);
				issuerEnrollmentEndDateResponseDTO.setErrMsg(Arrays.toString(e.getStackTrace()));
				issuerEnrollmentEndDateResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}	

		return issuerEnrollmentEndDateResponseDTO;
	}

//	@ApiOperation(value="findIssuerDetailsByUserId/{userId}", notes="This API takes user id return list of issuer id and issuer brand name")
//	@ApiResponse(value="return list of issuer id and issuer brand name for given user id")
	@RequestMapping(value = "/findIssuerDetailsByUserId/{userId}", method = RequestMethod.GET)
	@Produces("application/json")
	@ResponseBody
	public IssuerBrandNameResponseDTO findIssuerAndBrandNameByUserId(@PathVariable("userId") String userId){
		List<IssuerBrandNameDTO> issuerBrandNameDtoList = null;
		IssuerBrandNameResponseDTO issuerBrandNameResponseDTO = new IssuerBrandNameResponseDTO();
		issuerBrandNameResponseDTO.startResponse();
		if(LOGGER.isInfoEnabled()){
			LOGGER.info("Beginning to process findIssuerAndBrandNameByUserId().");
		}
		try{
			if(StringUtils.isNotBlank(userId) && StringUtils.isNumeric(userId) && PlanMgmtConstants.ZERO < Integer.parseInt(userId)){
				issuerBrandNameDtoList = issuerService.getIssuerAndIssuerBrandNameByUserId(Integer.parseInt(userId));
				if(issuerBrandNameDtoList.isEmpty()){
					issuerBrandNameResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.FIND_ISSUER_DETAILS_BY_USER_ID.code);
					issuerBrandNameResponseDTO.setErrMsg(PlanMgmtConstants.DATA_NOT_AVAILABLE);
				}else{
					issuerBrandNameResponseDTO.setIssuerBrandNameDtoList(issuerBrandNameDtoList);
					issuerBrandNameResponseDTO.setStatus(PlanMgmtConstants.SUCCESS);
				}
			}else{
				issuerBrandNameResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.FIND_ISSUER_DETAILS_BY_USER_ID.code);
				issuerBrandNameResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_USER_ID);				
			}
		}catch(Exception ex){
			issuerBrandNameResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.FIND_ISSUER_DETAILS_BY_USER_ID.code);
			issuerBrandNameResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_USER_ID);
			issuerBrandNameResponseDTO.setStatus(PlanMgmtConstants.FAILURE);
			LOGGER.error("Exception occured in findIssuerAndBrandNameByUserId: ", ex);
		}finally{
			issuerBrandNameResponseDTO.endResponse();
		}
		return issuerBrandNameResponseDTO;
	}
	
	@RequestMapping(value = "/issuerBrandNameByIssuerIdList", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public IssuerBrandNameResponseDTO issuerBrandNameByIssuerIdList(@RequestBody IssuerListRequestDTO issuerListRequestDTO){
		List<IssuerBrandNameDTO> issuerBrandNameDtoList = null;
		IssuerBrandNameResponseDTO issuerBrandNameResponseDTO = new IssuerBrandNameResponseDTO();
		issuerBrandNameResponseDTO.startResponse();
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Beginning to process issuerBrandNameByIssuerIdList().");
		}
		try{
			if(null != issuerListRequestDTO.getIssuerIds() && issuerListRequestDTO.getIssuerIds().size() > 0){
				issuerBrandNameDtoList = issuerService.getIssuerBrandNameListByIssuerList(issuerListRequestDTO);
				if(issuerBrandNameDtoList.isEmpty()){
					issuerBrandNameResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_BY_ID.code);
					issuerBrandNameResponseDTO.setErrMsg(PlanMgmtConstants.DATA_NOT_AVAILABLE);
				}else{
					issuerBrandNameResponseDTO.setIssuerBrandNameDtoList(issuerBrandNameDtoList);
					issuerBrandNameResponseDTO.setStatus(PlanMgmtConstants.SUCCESS);
				}
			}else{
				issuerBrandNameResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_BY_ID.code);
				issuerBrandNameResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_ISSUER_LIST);				
			}
		}catch(Exception ex){
			issuerBrandNameResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_BY_ID.code);
			issuerBrandNameResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_ISSUER_LIST);
			issuerBrandNameResponseDTO.setStatus(PlanMgmtConstants.FAILURE);
			LOGGER.error("Exception occured in issuerBrandNameByIssuerIdList: ", ex);
		}finally{
			issuerBrandNameResponseDTO.endResponse();
		}
		return issuerBrandNameResponseDTO;
	}

	
	@RequestMapping(value = "/issuersLegalNameByIssuerIdList", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody
	public IssuersLegalNameResponseDTO issuersLegalNameByIssuerIdList(@RequestBody List<Integer> issuerIdListRequest){
		IssuersLegalNameResponseDTO issuerslegalNameResponseDTO = new IssuersLegalNameResponseDTO();
		issuerslegalNameResponseDTO.setStatus(PlanMgmtConstants.FAILURE); //Set status as failure until success
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Beginning to process issuersLegalNameByIssuerIdList().");
		}
		issuerslegalNameResponseDTO.startResponse();
		try{
			if(null != issuerIdListRequest && issuerIdListRequest.size() > 0){
				Map<Integer, String> issuersLegalNameMap = issuerService.getIssuersLegalNameMapByIssuerIdList(issuerIdListRequest);
				if(null == issuersLegalNameMap || issuersLegalNameMap.isEmpty()){
					issuerslegalNameResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUERS_LEGAL_NAME_BY_ID.code);
					issuerslegalNameResponseDTO.setErrMsg(PlanMgmtConstants.DATA_NOT_AVAILABLE);
				}else{
					issuerslegalNameResponseDTO.setIssuersLegalNameMap(issuersLegalNameMap);
					issuerslegalNameResponseDTO.setStatus(PlanMgmtConstants.SUCCESS);
				}
			}else{
				issuerslegalNameResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUERS_LEGAL_NAME_BY_ID.code);
				issuerslegalNameResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_ISSUER_LIST);				
			}
		}catch(Exception ex){
			issuerslegalNameResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUERS_LEGAL_NAME_BY_ID.code);
			issuerslegalNameResponseDTO.setErrMsg("Exception occured while getting Issuer Legal Name: " + ex.getMessage());
			issuerslegalNameResponseDTO.setStatus(PlanMgmtConstants.FAILURE);
			LOGGER.error("Exception occured in issuersLegalNameByIssuerIdList: ", ex);
		}finally{
			issuerslegalNameResponseDTO.endResponse();
		}
		return issuerslegalNameResponseDTO;
	}

	
	// HIX-83378 get issuer by HIOS id 
	@RequestMapping(value = "/byHIOSId", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody
	IssuerByHIOSIdResponseDTO getIssuerByHIOSId(@RequestBody IssuerByHIOSIdRequestDTO issuerByHIOSIdRequestDTO) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Beginning to process getIssuerByHIOSId().");
		}
		IssuerByHIOSIdResponseDTO issuerByHIOSIdResponseDTO = new IssuerByHIOSIdResponseDTO();

		try {

			if (null == issuerByHIOSIdRequestDTO || StringUtils.isEmpty(issuerByHIOSIdRequestDTO.getHiosId())
					|| issuerByHIOSIdRequestDTO.getHiosId().length() != PlanMgmtConstants.FIVE) {
				issuerByHIOSIdResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				issuerByHIOSIdResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				issuerByHIOSIdResponseDTO.setErrMsg(INVALIDREQUEST);
			}
			else {

				issuerByHIOSIdResponseDTO = issuerService.getIssuerInfoByHIOSId(issuerByHIOSIdRequestDTO.getHiosId(), issuerByHIOSIdRequestDTO.getIncludeLogo());

				if (issuerByHIOSIdResponseDTO.getId() > 0) {
					issuerByHIOSIdResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
				else {
					issuerByHIOSIdResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					issuerByHIOSIdResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_BY_HIOS_ID.code);
					issuerByHIOSIdResponseDTO.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
				}
				issuerByHIOSIdResponseDTO.endResponse();
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception occured in getIssuerByHIOSId() ", e);
			issuerByHIOSIdResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_BY_HIOS_ID.code);
			issuerByHIOSIdResponseDTO.setErrMsg(Arrays.toString(e.getStackTrace()));
			issuerByHIOSIdResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return issuerByHIOSIdResponseDTO;
	}

	
	
	    // HIX-83451 get issuer by name 
		@RequestMapping(value = "/byName", method = RequestMethod.POST)
		@Produces("application/json")
		public @ResponseBody
		SingleIssuerResponse getIssuerByName(@RequestBody IssuerRequest issuerRequest) {
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Beginning to process getIssuerByName() ");
			}	
			SingleIssuerResponse singleIssuer = new SingleIssuerResponse();
			try {
				if (null == issuerRequest 
					|| StringUtils.isEmpty(issuerRequest.getName())
					) {
					singleIssuer.setStatus(GhixConstants.RESPONSE_FAILURE);
					singleIssuer.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
					singleIssuer.setErrMsg(INVALIDREQUEST);
				} else {
					List<Integer> issuerIdList = issuerService.getIssuerByName(issuerRequest.getName());
					if(issuerIdList.size() > 0 ){
						singleIssuer.setIssuerIdList(issuerIdList);
						singleIssuer.setStatus(GhixConstants.RESPONSE_SUCCESS);
						singleIssuer.endResponse();
					}else{
						singleIssuer.setStatus(GhixConstants.RESPONSE_FAILURE);
						singleIssuer.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_BY_NAME.code);
						singleIssuer.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
						singleIssuer.endResponse();
					}
				}
					
			} catch (Exception e) {
				LOGGER.error("Exception occured in getIssuerByHIOSId ()", e);
				singleIssuer.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUER_BY_HIOS_ID.code);
				singleIssuer.setErrMsg(Arrays.toString(e.getStackTrace()));
				singleIssuer.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
			return singleIssuer;
		}

}
