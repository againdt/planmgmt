/* 
* VisionPlanRestController.java
 * @author santanu
 * @version 1.0
 * @since Jan 14, 2015 
 * This REST controller would be use to handle Vision plan request and response
 */

package com.getinsured.hix.planmgmt.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Produces;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.WebServiceIOException;

import com.getinsured.hix.dto.planmgmt.VisionPlan;
import com.getinsured.hix.dto.planmgmt.VisionPlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.VisionPlanResponseDTO;
import com.getinsured.hix.planmgmt.service.VisionPlanRateBenefitService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;


//@Api(value="Vision Plan Controller", description ="returning vision plan information based on the provided information")
@Controller
@RequestMapping(value = "/visionplan")
public class VisionPlanRestController {

	
	private static final Logger LOGGER = Logger.getLogger(VisionPlanRestController.class);
	
	@Autowired 
	VisionPlanRateBenefitService visionPlanRateBenefitService;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	/* test controller */	
//	@RequestMapping(value = "/welcome", method = RequestMethod.GET) // for testing purpose
//	@ResponseBody public String welcome()
//	{
//		LOGGER.info("Welcome to GHIX-Planmgmt module, invoke VisionPlanRestController()");		
//		return "Welcome to GHIX-Planmgmt module, invoke VisionPlanRestController";
//	}
	
	/* return Vision plans */
//	@ApiOperation(value="getvisionplans", notes="This operation retrieves vision plans. ")
//	@ApiResponse(value="returns vision plan information")
	@RequestMapping(value = "/getvisionplans", method = RequestMethod.POST) 
	@Produces("application/json")
	@ResponseBody public VisionPlanResponseDTO getVisionPlans(@RequestBody VisionPlanRequestDTO visionPlanRequestDTO )
	{
		LOGGER.info("inside getVisionPlans()");	
		if(LOGGER.isDebugEnabled()) {
			StringBuilder buildStr = new StringBuilder();
			buildStr.append("Request data ===>");	
			buildStr.append(" member data : ");
			buildStr.append(visionPlanRequestDTO.getMemberList());
			buildStr.append(" Effective date : ");
			buildStr.append(visionPlanRequestDTO.getEffectiveDate());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));
		}
				
		VisionPlanResponseDTO visionPlanResponseDTO = new VisionPlanResponseDTO();
		
		if(null == visionPlanResponseDTO 
				|| null == visionPlanRequestDTO.getEffectiveDate() || visionPlanRequestDTO.getEffectiveDate().trim().isEmpty()				
				|| null == visionPlanRequestDTO.getMemberList() || visionPlanRequestDTO.getMemberList().size() == 0						
				) {
					visionPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					visionPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
					visionPlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			
			} else {
				try{
					List<VisionPlan> visionPlanList =  getVisionPlanHelper(visionPlanRequestDTO);
					
					visionPlanResponseDTO.setVisionPlanList(visionPlanList);
					visionPlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);					
				}catch (WebServiceIOException webServiceIOException) {
					giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch vision plans.",webServiceIOException);
					LOGGER.error("Exception occurrecd while processing request. Exception:",webServiceIOException);	
					visionPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					visionPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_VISION_PLANS.code);
					visionPlanResponseDTO.setErrMsg(Arrays.toString(webServiceIOException.getStackTrace()));					
				}
				catch(Exception ex) {
					giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch VISION plans.",ex);
					LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);				
					visionPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					visionPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_VISION_PLANS.code);
					visionPlanResponseDTO.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}	
			}
		return visionPlanResponseDTO;
		
	}	
	
	
	public List<VisionPlan> getVisionPlanHelper(VisionPlanRequestDTO visionPlanRequestDTO){
		List<VisionPlan> visionPlanList =  new ArrayList<VisionPlan>();

		if(null == visionPlanRequestDTO 
				|| null == visionPlanRequestDTO.getEffectiveDate() || visionPlanRequestDTO.getEffectiveDate().trim().isEmpty()				
				|| null == visionPlanRequestDTO.getMemberList() || visionPlanRequestDTO.getMemberList().size() == 0) {
			return visionPlanList;		
		}else {
			String tenant =  (StringUtils.isNotEmpty(visionPlanRequestDTO.getTenant())) ? visionPlanRequestDTO.getTenant().trim() : PlanMgmtConstants.TENANT_GINS.trim();
			visionPlanList = visionPlanRateBenefitService.getVisionPlans(visionPlanRequestDTO.getMemberList(), visionPlanRequestDTO.getEffectiveDate(), tenant);		
			return visionPlanList;		
	    }
   }
	
	
	
//	@ApiOperation(value="getlowestvisionplan", notes="This operation retrieves lowest vision plan. ")
//	@ApiResponse(value="returns lowest vision plan information")
	@RequestMapping(value = "/getlowestvisionplan", method = RequestMethod.POST) 
	@Produces("application/json")
	@ResponseBody public VisionPlanResponseDTO getLowestVisionPlan(@RequestBody VisionPlanRequestDTO visionPlanRequestDTO )
	{
		LOGGER.info("inside getLowestVisionPlan()");	
		if(LOGGER.isDebugEnabled()) {
			StringBuilder buildStr = new StringBuilder();
			buildStr.append("Request data ===>");	
			buildStr.append(" member data : ");
			buildStr.append(visionPlanRequestDTO.getMemberList());
			buildStr.append(" Effective date : ");
			buildStr.append(visionPlanRequestDTO.getEffectiveDate());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));
		}
				
		VisionPlanResponseDTO visionPlanResponseDTO = new VisionPlanResponseDTO();
		
		if(null == visionPlanResponseDTO 
				|| null == visionPlanRequestDTO.getEffectiveDate() || visionPlanRequestDTO.getEffectiveDate().trim().isEmpty()				
				|| null == visionPlanRequestDTO.getMemberList() || visionPlanRequestDTO.getMemberList().size() == 0						
				) {
					visionPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					visionPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
					visionPlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			
			} else {
				try{
					String tenant =  (StringUtils.isNotEmpty(visionPlanRequestDTO.getTenant())) ? visionPlanRequestDTO.getTenant().trim() : PlanMgmtConstants.TENANT_GINS.trim();
					List<VisionPlan> visionPlanList =  visionPlanRateBenefitService.getLowestVisionPlan(visionPlanRequestDTO.getMemberList(), visionPlanRequestDTO.getEffectiveDate(), tenant);
										
					visionPlanResponseDTO.setVisionPlanList(visionPlanList);
					visionPlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);					
				}catch (WebServiceIOException webServiceIOException) {
					LOGGER.error("Exception occurrecd while processing request. Exception:",webServiceIOException);	
					visionPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					visionPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_VISION_PLANS.code);
					visionPlanResponseDTO.setErrMsg(Arrays.toString(webServiceIOException.getStackTrace()));					
				}
				catch(Exception ex) {
					LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);				
					visionPlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					visionPlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_VISION_PLANS.code);
					visionPlanResponseDTO.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}	
			}
		return visionPlanResponseDTO;
		
	}	
}	
