package com.getinsured.hix.planmgmt.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.planmgmt.model.PufMultiplierData;
import com.getinsured.hix.planmgmt.querybuilder.PufPlanQueryBuilder;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtParamConstants;
import com.getinsured.hix.platform.util.SecurityUtil;

@Service("pufPlanService")
@Repository
@Transactional
public class PufPlanServiceImpl implements PufPlanService {

	@PersistenceUnit 
	private EntityManagerFactory emf;
	
	@Value(PlanMgmtConstants.CONFIG_DB_TYPE)
	private String configuredDB;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PufPlanServiceImpl.class);
	private static final String AGE = "age";
	
	/* getPufPlanData() to pull PUF plan data */
	@Transactional(readOnly = true)
	public List<PlanRateBenefit> getPufPlanData(String zip, String countyCode, String hiosPlanIds, List<Map<String, String>> memberDataList, String stateName, String houseHoldType, String insuranceType, String applicableYear){
		/*	Contains list of plans respective to county		*/		
		List<PlanRateBenefit> pufPlanListRespectiveToCounty = new ArrayList<PlanRateBenefit>();
		/*	Contains list of plans irrespective to county	*/
		Map<String, PlanRateBenefit> pufPlanMapIrrespectiveToCounty = new HashMap<String, PlanRateBenefit>();
		boolean countySpecificPUFPlanFound = false;
		
		// calculate multiplier value for each member
		for (int i = 0; i < memberDataList.size(); i++) {
			int memberAge = Integer.valueOf(memberDataList.get(i).get(AGE));
			 Double multiplier = PufMultiplierData.getPremiumMultiplier(stateName, memberAge);
			 if(LOGGER.isDebugEnabled()) {
				 LOGGER.debug("Age: " + SecurityUtil.sanitizeForLogging(String.valueOf(memberAge)) + " Multiplier:"+ multiplier);
			 }
             memberDataList.get(i).put("multiplier", multiplier.toString());
		}
		
		//	Fetch county Name from zipcodes table with help of zip code and county code	
		String countyName = getCountyNameUsingZipAndCountyFIPS(zip, countyCode);
	
		Map<String, Map<String, String>> benefitData = new HashMap<String, Map<String, String>>();
		Map<String, Map<String, String>> planCosts = new HashMap<String, Map<String, String>>();
		List<Map<String, String>> providers = new ArrayList<Map<String, String>>();	
		List<Map<String, String>> memberInfo = new ArrayList<Map<String, String>>();
		DecimalFormat df = new DecimalFormat("0.00");
		
		String queryStr = null;
		EntityManager entityManager = null;
		List<String> listOfHIOSIdsToExclude = new ArrayList<String>();
		Object[] objArray = null;
		
		try {
			entityManager = emf.createEntityManager();
		        // excludes puff plans already exists in on/off exchange plan list 
				if(hiosPlanIds != null && hiosPlanIds.length() > 0){
					queryStr = PufPlanQueryBuilder.getPufPlansExcludesHiosPlanIdsQuery(insuranceType, houseHoldType, configuredDB);
				}else{
					queryStr = PufPlanQueryBuilder.getPufPlansIncludesHiosPlanIdsQuery(insuranceType, houseHoldType, configuredDB);
				}

				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("PUF Query :: " + queryStr);
				}
				
				if(hiosPlanIds != null && hiosPlanIds.length() > 0){
					for(String hiosPlanId : hiosPlanIds.split(",")){
						listOfHIOSIdsToExclude.add(hiosPlanId.trim());
					}
				}
				
				Query query = entityManager.createNativeQuery(queryStr);
				query.setParameter("paramZip", zip);
				query.setParameter("paramCountyFips", countyCode);
				query.setParameter("paramYear", applicableYear);
				
				if(listOfHIOSIdsToExclude.size() != 0){
					query.setParameter("paramHiosIdList", listOfHIOSIdsToExclude);
				}
				
				double totalPremium = PlanMgmtConstants.DOUBLE_ZERO;
				double basePremium = PlanMgmtConstants.DOUBLE_ZERO;
				double multiplier = PlanMgmtConstants.DOUBLE_ZERO;
				
				List rsList = query.getResultList();	
				Iterator rsIterator = rsList.iterator();
				
				while (rsIterator.hasNext()) {
					 PlanRateBenefit pufPlanMap = new PlanRateBenefit();
					 initializePufPlanMap(pufPlanMap);
					 totalPremium = PlanMgmtConstants.DOUBLE_ZERO;
					 objArray = (Object[]) rsIterator.next();
						
					//pufPlanMap.setId(Integer.parseInt(pufdPlanFileds[PlanMgmtConstants.ZERO].toString()));
					// if premium_a_i_age_21 column value is not null in puf_qhp/puf_sdp table then compute rate 
					if(objArray[PlanMgmtConstants.EIGHT]!= null){
							basePremium = Double.parseDouble(objArray[PlanMgmtConstants.EIGHT].toString());	
							for (int i = 0; i < memberDataList.size(); i++) {					
								 multiplier = Double.valueOf(memberDataList.get(i).get("multiplier"));					
								// each member premium = base premium (adult premium) * age wise multiplier 
								totalPremium  = totalPremium + (basePremium * multiplier) ; 					
							}
						 }
						
					    if(objArray[PlanMgmtConstants.THREE] != null){
						   pufPlanMap.setLevel(objArray[PlanMgmtConstants.THREE].toString().toUpperCase());
					    } 
					    
					    if(objArray[PlanMgmtConstants.ZERO] != null){
						   pufPlanMap.setIssuerId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString().substring(0,PlanMgmtConstants.FIVE)));
					    }
					   
					    if(objArray[PlanMgmtConstants.FOUR] != null){
					       pufPlanMap.setIssuer(objArray[PlanMgmtConstants.FOUR].toString());
					       pufPlanMap.setIssuerText(objArray[PlanMgmtConstants.FOUR].toString());
					    }
					    
						pufPlanMap.setPremium(Float.parseFloat(df.format(totalPremium)));
						
						if (objArray[PlanMgmtConstants.FIVE] != null){
							pufPlanMap.setName(objArray[PlanMgmtConstants.FIVE].toString());
						}
					
						pufPlanMap.setIsPuf(PlanMgmtConstants.Y);
						
						if (objArray[PlanMgmtConstants.SIX] != null){
						    pufPlanMap.setNetworkType(objArray[PlanMgmtConstants.SIX].toString());
						}
						pufPlanMap.setPlanBaseBenefits(benefitData);
						pufPlanMap.setPlanBaseCosts(planCosts);
						pufPlanMap.setPlanBenefits(benefitData);
						pufPlanMap.setPlanCosts(planCosts);
						pufPlanMap.setPlanBrochureUrl(PlanMgmtConstants.EMPTY_STRING);
						pufPlanMap.setPlanDetailsByMember(memberInfo);
						pufPlanMap.setProviderLink(PlanMgmtConstants.EMPTY_STRING);
						pufPlanMap.setProviders(providers);
						pufPlanMap.setSbcDocUrl(PlanMgmtConstants.EMPTY_STRING);
						pufPlanMap.setPrimaryCareDeductOrCoinsAfterSetNumberCopays(PlanMgmtConstants.EMPTY_STRING);
						pufPlanMap.setPrimaryCareCostSharingAfterSetNumberVisits(PlanMgmtConstants.EMPTY_STRING);
						pufPlanMap.setHsa("No");
						
						/*	Check county name from PUF_QHP table and PM_ZIP_COUNTY_RATING_AREA	
						 * if county names are equal then add to pufPlanListRespectiveToCounty	
						 * else add to pufPlanMapIrrespectiveToCounty	*/
						if(countyName != null && objArray[PlanMgmtConstants.SEVEN] != null && 
								objArray[PlanMgmtConstants.SEVEN].toString().equalsIgnoreCase(countyName)){
							pufPlanListRespectiveToCounty.add(pufPlanMap);
							countySpecificPUFPlanFound = true;
						}else if (!countySpecificPUFPlanFound){
							PlanRateBenefit tempPlanRateBenefit = pufPlanMapIrrespectiveToCounty.get(objArray[PlanMgmtConstants.ZERO]);
							if(tempPlanRateBenefit == null) {
								//We might get multiple record for same plan, one for each county, set it only once if map does not already have it
								if(LOGGER.isDebugEnabled()) {
									LOGGER.debug("Setting PUF Plan - IrrespectiveToCounty :: " + objArray[PlanMgmtConstants.ZERO]);
								}
								pufPlanMapIrrespectiveToCounty.put(objArray[PlanMgmtConstants.ZERO].toString(), pufPlanMap);
							}
						}
				 }
			
		     //} 
		}catch(Exception ex){
				LOGGER.error("Exception Occured @getPufPlanData : ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }    
		}
		
		/*	if pufPlanListRespectiveToCounty is empty then return  pufPlanListIrrespectiveToCounty	
		 * 	else return	pufPlanListRespectiveToCounty	*/
		if(pufPlanListRespectiveToCounty.isEmpty()){
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("pufPlanListRespectiveToCounty :: " + SecurityUtil.sanitizeForLogging(countyName) + "  does not exists.");
			}
			if(!pufPlanMapIrrespectiveToCounty.isEmpty()) {
				return new ArrayList<PlanRateBenefit>(pufPlanMapIrrespectiveToCounty.values());
			} else {
				return new ArrayList<PlanRateBenefit>();
			}
		}else{
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("pufPlanListRespectiveToCounty :: " + SecurityUtil.sanitizeForLogging(countyName) + " exists.");
			}
			return pufPlanListRespectiveToCounty;
		}
	}
	
	private void initializePufPlanMap(PlanRateBenefit pufPlanMap) {
		pufPlanMap.setIssuer(PlanMgmtConstants.EMPTY_STRING);
		pufPlanMap.setIssuerText(PlanMgmtConstants.EMPTY_STRING);
		pufPlanMap.setName(PlanMgmtConstants.EMPTY_STRING);
		pufPlanMap.setNetworkType(PlanMgmtConstants.EMPTY_STRING);
	}

	/*	getting county name using zip and county_fips from zipcodes table	*/
	@Transactional(readOnly = true)
	public String getCountyNameUsingZipAndCountyFIPS(String zip, String county_fips){
		String countyName = null;
		String queryStr = PlanMgmtConstants.EMPTY_STRING;
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			queryStr = PufPlanQueryBuilder.getZipCountyQuery();
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Zip, County Query :: " + queryStr.toString());
			}
		
			Query query = entityManager.createNativeQuery(queryStr.toString());
			query.setParameter(PlanMgmtParamConstants.PARAM_ZIP_CODE, zip);
			query.setParameter(PlanMgmtParamConstants.PARAM_COUNTY_CODE, county_fips);
			Iterator rsIterator = query.getResultList().iterator();
			
			if (rsIterator.hasNext()) {
				Object[] countyRelatedFileds = (Object[]) rsIterator.next();
				countyName = countyRelatedFileds[PlanMgmtConstants.ONE] == null ? null : countyRelatedFileds[PlanMgmtConstants.ONE].toString(); 
			}
		}catch(Exception ex){
			LOGGER.error("Error Occured in getCountyNameUsingZipAndCountyFIPS", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("County Name :: " + SecurityUtil.sanitizeForLogging(countyName));
		}
		return countyName;
	}
	
}
