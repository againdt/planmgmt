/**
 * File.
 */
package com.getinsured.hix.planmgmt.service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.platform.util.exception.GIException;


/**
 * Class 'ReSTServiceImpl' for implementing ReST caller methods.
 *
 * @author chalse_v
 * @version 1.0
 * @since May 10, 2013
 *
 */
@Service
public class ReSTServiceImpl implements ReSTService {
	/**
	 * Attribute.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ReSTServiceImpl.class);
	
	/**
	 * Attribute.
	 */
	private static final long serialVersionUID = 2773515940000578740L;
	
	/**
	 * Attribute String 'url'.
	 */
	private String url;
	
	/**
	 * Attribute RequestMethod requestMethod
	 */
	private RequestMethod requestMethod;
	
	/**
	 * Attribute Map<String,String> requestParameters
	 */
	private Map<String, Object> parameters = new HashMap<String, Object>();

	/**
	 * Attribute Class String.class.
	 */
	protected final Class responseClassDefault = String.class; 

	/**
	 * Attribute Class {X}.class.
	 */
	private Class responseClass;
	
	/**
	 * Attribute RestTemplate restTemplate
	 */
	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Constructor.
	 */

	public ReSTServiceImpl() {}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#getUrl()
	 */
	@Override
	public String getUrl() {
		return this.url;
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#setUrl(java.lang.String)
	 */
	@Override
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#getRequestMethod()
	 */
	@Override
	public RequestMethod getRequestMethod() {
		return this.requestMethod;
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#setRequestMethod(com.getinsured.hix.planmgmt.service.ReSTService.RequestMethod)
	 */
	@Override
	public void setRequestMethod(RequestMethod requestMethod) {
		this.requestMethod = requestMethod;
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#getParameters()
	 */
	public Map<String, Object> getParameters() {
		return this.parameters;
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#setParameters(java.util.Map)
	 */
	public void setParameters(Map<String, Object> requestParameters) {
		if(null == requestParameters) {
			throw new IllegalArgumentException();
		}
		else {
			this.parameters = requestParameters;
		}
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#addParameters(java.lang.String, java.lang.Object)
	 */
	public void addParameters(String key, Object value) {
		if(null == key || null == value || key.isEmpty()) {
			throw new IllegalArgumentException();
		}
		else {
			this.parameters.put(key, value);
		}
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#addAllParameters(java.util.Map)
	 */
	public void addAllParameters(Map<String, Object> requestParameters) {
		if(null == requestParameters || requestParameters.isEmpty()) {
			throw new IllegalArgumentException();
		}
		else {
			this.parameters.putAll(requestParameters);
		}
	}
	
	/**
	 * Getter for 'responseClassDefault'.
	 * 
	 * @return Class The specified default Class instance.
	 */
	public Class getResponseClassDefault() {
		return responseClassDefault;
	}

	
	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#getResponseClass()
	 */
	public Class getResponseClass() {
		return responseClass;
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#setResponseClass(java.lang.Class)
	 */
	public void setResponseClass(Class responseClass) {
		if(null == responseClass) {
			throw new IllegalArgumentException();
		}
		else {
			this.responseClass = responseClass;
		}
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#resetRequest()
	 */
	@Override
	public void resetRequest() {
		this.setUrl(null);
		this.setRequestMethod(null);
		this.setParameters(new HashMap<String, Object>());
	}

	/**
	 * @see com.getinsured.hix.planmgmt.service.ReSTService#sendRequest()
	 */
	@Override
	public Object sendRequest() throws GIException {
		if(url == null || url.length() == 0) {
			throw new GIException("Request Url not found.");
		}
		else if(requestMethod == null) {
			throw new GIException("Request Method not found.");
		}
		
		try {
			if(RequestMethod.GET == requestMethod) {
				LOGGER.info("Beginning to send ReST request.");
				String pathVars = generateQueryString(parameters);
				return restTemplate.getForObject(url+pathVars, this.retrieveAssociatedResponseClass());
			}
			else if(RequestMethod.POST == requestMethod) {
				LOGGER.info("Beginning to send rest request.");
				if(null == this.parameters || this.parameters.isEmpty()) {
					throw new GIException("No request parameters found for current request.");
				}
				else {
					return restTemplate.postForObject(url, this.parameters.get(this.getParameters().keySet().iterator().next().toString()), this.retrieveAssociatedResponseClass());
				}
			}
		}
		catch(RestClientException re) {
			LOGGER.error("Exception occurred while sending ReST request. Exception:" + re.getMessage());
			throw new GIException(re);
		}
		finally {
			/** Reset values of planRateBenefitRequest */ 
			resetRequest();
		}
		
		return null;
	}
	
	/**
	 * Method to create query string for request from request parameter collection.
	 *
	 * @return String the query string.
	 */
	private String generateQueryString(Map<String, Object> requestParameters) throws GIException{
		StringBuilder sb = new StringBuilder();
	
		try {
		  LOGGER.info("Beginning to create query string for current request.");
		  for(Map.Entry<String, Object> e : requestParameters.entrySet()){
		      if(sb.length() > 0){
		          sb.append('&');
		      }
		      sb.append(URLEncoder.encode(e.getKey(), "UTF-8")).append('=').append(URLEncoder.encode(e.getValue().toString(), "UTF-8"));
		  }
		}
		catch(UnsupportedEncodingException uee){
			LOGGER.error("Exception occurred while generating query string. Exception:",uee);
			throw new GIException("Unable to generate Query String using Map.",uee);
		}
		
		if(sb.length() > 0){
			sb.insert(0, "?");
		}
		
		LOGGER.info("Query string created for current request.");
		return sb.toString();
	}

	/**
	 * Method to retrieve associated response class. 
	 *
	 * @return Class the response Class instance.
	 */
	private Class retrieveAssociatedResponseClass() {
		if(null == this.getResponseClass()) {
			return this.getResponseClassDefault();
		}
		return this.getResponseClass();
	}
}
