package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.CostShare;

/**
 * Repository class 'ICostShareRepository' to handle persistence of 'CostShare' records.
 *
 */
public interface IPMCostShareRepository extends JpaRepository<CostShare, Integer> {
	
	@Query("SELECT formulary.id, costSharingType, coPaymentAmount, coInsurancePercent, drugTierLevel "+
			"FROM " +
			"CostShare " +
			"WHERE " +
			"formulary.id IN (:formularyId) "+
			"AND drugTierLevel IN (:drugTierLevel) "+
			"AND networkCostType = 'In Network' AND drugPrescriptionPeriod = 1 AND isDeleted = 'N'")
	List<Object[]> getCostShareByDrugTier(@Param("formularyId") List<Integer> formularyIdList, @Param("drugTierLevel") List<Integer> drugTierLevelList);

	@Query("SELECT id, costSharingType, coPaymentAmount, coInsurancePercent, drugTierLevel, copaymentAttribute, coinsuranceAttribute "+
			"FROM CostShare " +
			"WHERE id IN (:costShareId) "+
			"AND networkCostType = 'In Network' AND drugPrescriptionPeriod = 1 AND isDeleted = 'N'")
	List<Object[]> getCostShareByIdList(@Param("costShareId") List<Integer> costShareIdList);
			
}
