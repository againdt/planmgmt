/* 
* MedidarePlanRestController.java
 * @author santanu
 * @version 1.0
 * @since Feb 2, 2015 
 * This REST controller would be use to handle Medicare plan request and response
 */

package com.getinsured.hix.planmgmt.controller;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.Produces;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.WebServiceIOException;

import com.getinsured.hix.dto.planmgmt.MedicarePlan;
import com.getinsured.hix.dto.planmgmt.MedicarePlanDetailsDTO;
import com.getinsured.hix.dto.planmgmt.MedicarePlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.MedicarePlanResponseDTO;
import com.getinsured.hix.dto.planmgmt.MedicareZipRequestDTO;
import com.getinsured.hix.dto.planmgmt.MedicareZipResponseDTO;
import com.getinsured.hix.dto.planmgmt.Member;
import com.getinsured.hix.dto.planmgmt.PlanListRequest;
import com.getinsured.hix.dto.planmgmt.PlanRequest;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.IssuerSearchResponseDTO;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.service.MedicarePlanRateBenefitService;
import com.getinsured.hix.planmgmt.service.MedicarePlanRateService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.pmms.service.IssuerService;

//@Api(value="Medicare Plan Information", description ="Returning Medicare Plan information depends on provided information")
@Controller
@DependsOn("dynamicPropertiesUtil")
@RequestMapping(value = "/medicare")
public class MedicarePlanRestController {
	
	@Autowired
	private MedicarePlanRateService medicarePlanRateService;
	@Autowired
	private MedicarePlanRateBenefitService medicarePlanRateBenefitService;	
	@Autowired 
	private PlanMgmtService planMgmtService;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	@Autowired
	private IssuerService pmmsIssuerService;
	
	@Value("#{configProp['appUrl']}") 
	private String appUrl;

	private static final String DATE_PATTERN =  "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d$";
	
	private static final String DOCUMENT_URL= "download/document?documentId="; // document download url
		
	private static final Logger LOGGER = Logger.getLogger(MedicarePlanRestController.class);
	
	/* test controller */	
//	@RequestMapping(value = "/welcome", method = RequestMethod.GET) // for testing purpose
//	@ResponseBody public String welcome()
//	{
//		LOGGER.info("Welcome to GHIX-Planmgmt module, invoke MedicarePlanRestController()");		
//		return "Welcome to GHIX-Planmgmt module, invoke MedicarePlanRestController";
//	}
	
	
//	@ApiOperation(value="getplans", notes="This operation retrieves medicare plans based on household information and effective date")
//	@ApiResponse(value="return list of Medicaid plans for provided household information and effective date")
	@RequestMapping(value = "/getplans", method = RequestMethod.POST) 
	@Produces("application/json")
	@ResponseBody public MedicarePlanResponseDTO getMedicarePlans(@RequestBody MedicarePlanRequestDTO medicarePlanRequestDTO )
	{
		LOGGER.info("inside getMedicarePlans()");	
		StringBuilder buildStr = new StringBuilder();
		buildStr.append("Request data ===>");			
		buildStr.append(" Effective date : ");
		buildStr.append(medicarePlanRequestDTO.getEffectiveDate());
		buildStr.append(" Insurance Type : ");
		buildStr.append(medicarePlanRequestDTO.getInsuranceType());
		buildStr.append(" Tenant : ");
		buildStr.append(medicarePlanRequestDTO.getTenant());
		buildStr.append(" Zipcode : ");
		buildStr.append(medicarePlanRequestDTO.getZipCode());
		buildStr.append(" County name : ");
		buildStr.append(medicarePlanRequestDTO.getCountyName());
		buildStr.append(" member data : ");
		buildStr.append(medicarePlanRequestDTO.getMemberList());
		
		
		MedicarePlanResponseDTO medicarePlanResponseDTO = new MedicarePlanResponseDTO();
		
		if(null == medicarePlanResponseDTO 
				|| StringUtils.isBlank(medicarePlanRequestDTO.getEffectiveDate()) 	
				|| StringUtils.isBlank(medicarePlanRequestDTO.getInsuranceType())
				|| StringUtils.isBlank(medicarePlanRequestDTO.getZipCode())
				|| StringUtils.isBlank(medicarePlanRequestDTO.getCountyName())				
				|| CollectionUtils.isEmpty(medicarePlanRequestDTO.getMemberList())) {
					medicarePlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					medicarePlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
					medicarePlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			
			} else {
				try{					
					Pattern pattern = Pattern.compile(DATE_PATTERN);
					 
					Matcher matcher = pattern.matcher(medicarePlanRequestDTO.getEffectiveDate());
					if(!matcher.matches()){
						throw new Exception("Invalid effective date");
					}
					
					for (Member member : medicarePlanRequestDTO.getMemberList()) {
						matcher.reset();
						matcher = pattern.matcher(member.getDob());
						if(!matcher.matches()){
							throw new Exception("Invalid date of birth");
						}
					}
					
					List<MedicarePlan> responseData =  getMedicarePlanRateBenefitsHelper(medicarePlanRequestDTO);					
					medicarePlanResponseDTO.setPlanList(responseData);
					medicarePlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
					
				}catch (WebServiceIOException webServiceIOException) {
					giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch Medicare plans.",webServiceIOException);
					LOGGER.error("Exception occurrecd while processing request. Exception:",webServiceIOException);	
					medicarePlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					medicarePlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_MEDICARE_PLANS.code);
					medicarePlanResponseDTO.setErrMsg(Arrays.toString(webServiceIOException.getStackTrace()));					
				}
				catch(Exception ex) {
					giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch Medicare plans.",ex);
					LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);				
					medicarePlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					medicarePlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_MEDICARE_PLANS.code);
					medicarePlanResponseDTO.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}	
			}
		return medicarePlanResponseDTO;
		
	}	
	
				
	// create helper class to call directly to avoid REST call
    public List<MedicarePlan> getMedicarePlanRateBenefitsHelper(MedicarePlanRequestDTO medicarePlanRequestDTO){
    	List<MedicarePlan> responseData = new ArrayList();    	
    		
	    	try {
				
	    		List<Member> memberList = medicarePlanRequestDTO.getMemberList();
				String effectiveDate = medicarePlanRequestDTO.getEffectiveDate().trim();
				String insuranceType = medicarePlanRequestDTO.getInsuranceType().trim();
				String tenant = medicarePlanRequestDTO.getTenant().trim();
				if(StringUtils.isBlank(tenant) || PlanMgmtConstants.EMPTY_STRING.equals(tenant)) {
					tenant = PlanMgmtConstants.TENANT_GINS;
				}
				String zipCode = medicarePlanRequestDTO.getZipCode().trim();
				String countyName = medicarePlanRequestDTO.getCountyName().trim();
				String planYear = "";
				String medicareSource = "";
				if(null != medicarePlanRequestDTO.getPlanYear() && !"".equalsIgnoreCase(medicarePlanRequestDTO.getPlanYear())) {
					planYear = medicarePlanRequestDTO.getPlanYear().trim();
				}
				
				if(null != medicarePlanRequestDTO.getPlanSource()) {
					medicareSource = medicarePlanRequestDTO.getPlanSource();
				} else {
					medicareSource = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.MEDICARE_PLANS_SOURCE);
				}
				
				if(null != medicareSource) {
					if(PlanMgmtConstants.QUOTIT.equalsIgnoreCase(medicareSource)) {
						responseData = medicarePlanRateBenefitService.getMedicarePlanRateBenefits(memberList, effectiveDate, insuranceType, tenant, zipCode, countyName, planYear);
					} else if(PlanMgmtConstants.PUF.equalsIgnoreCase(medicareSource)) {
						responseData = medicarePlanRateBenefitService.getPUFPlanRateBenefits(memberList, effectiveDate, insuranceType, tenant, zipCode, countyName, planYear);
					}
				}		       
	    	}
	    	catch(Exception ex) {
				LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);	
				return responseData;
			}
		
    	
        return responseData;
    	
    }
    
//    @ApiOperation(value="getplandetails", notes="This operation retrieves details of Specific Medicare Plan")
//	@ApiResponse(value="Returning PlanResonse with Plan Information, Benefits Information and Cost Information")
	@RequestMapping(value = "/getplandetails", method = RequestMethod.POST)
	@Produces("application/json;charset=UTF-8")
    @ResponseBody public MedicarePlan getPlanDetails(@RequestBody PlanRequest planRequest)throws  GIException
	{
    	LOGGER.info("=========  Inside getPlanDetails() ============ ");
		MedicarePlan planResponse = new MedicarePlan();
		// if request data elements are null throw error
				if(null == planRequest || null == planRequest.getPlanId()) 
				{
					LOGGER.error("Invalid Input Parameters");
//					planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
//					planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
//					planResponse.setErrMsg("Invalid request parameters.");
				}
				else
				{
					planResponse = getPlanDetailsHelper(planRequest);
				}
				
				return planResponse;
	}
    
    public MedicarePlan getPlanDetailsHelper(PlanRequest planRequest)throws  GIException	
	{
    	if(LOGGER.isDebugEnabled()) {
    		LOGGER.debug("Request data :: plan Id : " + SecurityUtil.sanitizeForLogging(planRequest.getPlanId()));		
    	}
		MedicarePlan planResponse = new MedicarePlan();
		
		String planId = planRequest.getPlanId();
				
		Plan planData = planMgmtService.getPlanData(planId);
				
		planResponse.setId(planData.getId());
		planResponse.setName(planData.getName());					
		planResponse.setIssuerId(planData.getIssuer().getId());
		planResponse.setIssuerName(planData.getIssuer().getName());		
		planResponse.setInsuranceType(planData.getPlanMedicare().getPlan().getInsuranceType());
		planResponse.setNetworkType(planData.getNetworkType());
		planResponse.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(planData.getIssuer().getLogoURL(), null, 
				ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(planData.getIssuer().getId())), appUrl));
		planResponse.setPlanYear(planData.getApplicableYear().toString());
		planResponse.setPlanHiosId(planData.getHiosProductId());
		
		if(planData.getBrochure() != null) {
			String brochureUrl = planData.getBrochure();
			if (brochureUrl.matches(PlanMgmtConstants.ECM_REGEX)) {
				brochureUrl = appUrl +  DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(brochureUrl);    
			}
			else if (!brochureUrl.matches(PlanMgmtConstants.URL_REGEX)) {
				brochureUrl = PlanMgmtConstants.HTTP_PROTOCOL + brochureUrl;    
			}
			planResponse.setBrochureUrl(brochureUrl);
		}else{
			planResponse.setBrochureUrl(PlanMgmtConstants.EMPTY_STRING);
		}
				
		
		Map<String, Map<String, String>> medicareBenefits = medicarePlanRateBenefitService.getMedicarePlanBenefits(planData.getPlanMedicare().getId());		
		if(null != medicareBenefits && !medicareBenefits.isEmpty()) {
			planResponse.setBenefits(medicareBenefits);
		}						
		

		return planResponse;
		
	}

//    @ApiOperation(value="/validatezip", notes="This operation validates zip code and retrieves county code and county name for input zip code")
//	@ApiResponse(value="Return list of county code and county name for provided zip code")
	@RequestMapping(value = "/validatezip", method = RequestMethod.POST)
	@Produces("application/json;charset=UTF-8")
    @ResponseBody public MedicareZipResponseDTO getZipInfoMedicare(@RequestBody MedicareZipRequestDTO medicareZipRequest)throws  GIException{
    	MedicareZipResponseDTO medicareZipResponse = new MedicareZipResponseDTO();
    	if(null == medicareZipRequest || null == medicareZipRequest.getZip() || medicareZipRequest.getZip().equals("")) {
    		medicareZipResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
    		medicareZipResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
    		medicareZipResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}else{
			String zipCode = medicareZipRequest.getZip();
			try {
				List<Map<String, String>> countyCodeAndName = medicarePlanRateBenefitService.getCountyByZip(zipCode);
				if(!countyCodeAndName.isEmpty()){
					medicareZipResponse.setZipCode(zipCode);
					medicareZipResponse.setCountyList(countyCodeAndName);
					medicareZipResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				} else{
					medicareZipResponse.setCountyList(countyCodeAndName);
					medicareZipResponse.setErrMsg("Invalid ZIP code");
					medicareZipResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				}
				
			} catch (Exception e) {
				LOGGER.error("Exception occured while getting byzipcode",e);
				medicareZipResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ISSUERS_BY_ZIP.code);
				medicareZipResponse.setErrMsg(Arrays.toString(e.getStackTrace()));
				medicareZipResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			}
		}	

		return medicareZipResponse;
    }
    
//    @ApiOperation(value="/getplandetailsv1", notes="This operation retrieves details of Specific Medicare Plan")
//	@ApiResponse(value="Returning PlanResonse with Plan Information, Benefits Information and Cost Information")
	@RequestMapping(value = "/getplandetailsv1", method = RequestMethod.POST)
	@Produces("application/json;charset=UTF-8")
    @ResponseBody public MedicarePlanDetailsDTO getPlanDetailsV1(@RequestBody PlanListRequest planListRequest)throws  GIException
	{
    	LOGGER.info("=========  Inside getPlanDetails() ============ ");
    	MedicarePlanDetailsDTO planResponse = new MedicarePlanDetailsDTO();  	
		
		// if request data elements are null throw error
		if(null == planListRequest || planListRequest.getPlanIds().size() == 0) 
		{
			LOGGER.error("Invalid Input Parameters");
			planResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
			planResponse.setErrMsg("Invalid request parameters.");
		}
		else
		{
			List<MedicarePlan> mcPlansList = medicarePlanRateService.getPlanResponseListForMedicare(planListRequest.getPlanIds());
			if(null == mcPlansList || mcPlansList.size() == 0) {				
				planResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				planResponse.setErrMsg("No plan(s) available for the given input request, they are invalid plan ids or not medicare plan ids.");
			} else {			
				Map<Integer, String> requestPlanIds = new HashMap<>();					
				for(Integer reqId: planListRequest.getPlanIds()) {
					requestPlanIds.put(reqId,"");
				}				
				for(MedicarePlan responsePlan:mcPlansList) {								
					if(requestPlanIds.containsKey(responsePlan.getId())) {
						requestPlanIds.remove(responsePlan.getId());
					}
				}		
				if(null != requestPlanIds.keySet()  && requestPlanIds.keySet().size() > 0) {
					planResponse.setErrMsg("These are Invalid Plan Id's or not medicare plans " + requestPlanIds.keySet());
					planResponse.setPlanDetails(mcPlansList);
					planResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				} else {
					planResponse.setPlanDetails(mcPlansList);
					planResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
			}			
		}							
		return planResponse;
	}
	
	@RequestMapping(value = "/issuers", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody IssuerSearchResponseDTO getMedicareIssuerList(@RequestBody IssuerSearchRequestDTO  issuerSearchRequestDTO) {
		return pmmsIssuerService.getMedicareIssuerList(issuerSearchRequestDTO);
	}
}
