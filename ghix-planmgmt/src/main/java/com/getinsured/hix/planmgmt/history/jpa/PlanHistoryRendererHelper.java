package com.getinsured.hix.planmgmt.history.jpa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.Plan;
//import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;

/**
 * This class is used to populate the List<Map<String, Object>> base on the data got from REV_history or AUD tables.
 * Popultaed List<Map<String, Object>> can be used to display the corresponding data as History pages in UI
 * @author save_h
 */

@Service
//@Qualifier("planHistoryRendererHelper")
public class PlanHistoryRendererHelper  {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanHistoryRendererHelper.class);
	
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private UserService userService;
	
	@Value("#{configProp['appUrl']}") 
	private String appUrl;

		
	/**
	 * Populates the List<Map<String, Object>> which can be used to display the corresponding data as History pages in UI
	 * @param data
	 * 			List<Map<String, String>> input of the entire revision history data.
	 * @param compareColumns
	 * 			List<String> of column names based on which the filteration of @param data should be done.
	 * @param columnsToDisplay
	 * 			List<Integer> of column ids specified in corresponding HistoryService, which will indicate newly prepared map should contain what all keys.
	 * @return
	 * 			List<Map<String, Object>> Filtered data based on @param data, @param compareColumns and @param columnsToDisplay.
	 */
	//@Override
	public List<Map<String, Object>> processHistoryData(List<Map<String, String>> data, List<String> compareColumns, List<Integer> columnsToDisplay) 
	{
		List<Map<String, String>> orderedData = new ArrayList<Map<String,String>>(data);
		int size = orderedData.size();
		Map<String, String> firstElement = null;
		Map<String, String> secondElement = null;
		List<Map<String, Object>> processedData = new ArrayList<Map<String,Object>>();
		try {
			if (StringUtils.isNumeric(compareColumns.get(compareColumns.size()-1))) {
				compareColumns.remove(compareColumns.size()-1);
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while correcting compare columns", e);
		}
		try {
			for(int i=0; i<size-1;i++){
				if(i == 0 ){
					firstElement = orderedData.get(i);
					secondElement = orderedData.get(i+1);
					for(String keyColumn : compareColumns){
						boolean valueChanged;
						if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.ENROLLMENT_AVAIL_COL) || keyColumn.equalsIgnoreCase(PlanMgmtConstants.EFFECTIVE_DATE_COL)){
							valueChanged = isEnrollmentChange(firstElement,secondElement,PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
						}else if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.BROCHURE_COL)) {
							// We need to also compare brochureUCmId column so that we have added following method for comparison
							valueChanged = isBroucherAdded(firstElement,secondElement,keyColumn);
						} else if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL)) {
							valueChanged = isBroucherAdded(firstElement,secondElement,keyColumn);
						} else {
							valueChanged = isEqual(firstElement,secondElement,keyColumn);
						}
						if(!valueChanged){
							Map<String, Object> processedMap1 = formMap(firstElement,keyColumn, columnsToDisplay);
							processedData.add(processedMap1);
							Map<String, Object> processedMap2 = formMap(secondElement,keyColumn, columnsToDisplay);
							processedData.add(processedMap2);
						}else {
							if(compareColumns.size() == 2 && StringUtils.equals(PlanMgmtConstants.STATUS_COL, compareColumns.get(0)) && StringUtils.equals(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, compareColumns.get(1))){
								LOGGER.info("Enrollments are available.");
							}else{ 
								Map<String, Object> processedMap = formMap(firstElement,keyColumn, columnsToDisplay);
								processedData.add(processedMap);
							}
						}
					}
				}else {
					firstElement = orderedData.get(i);
					secondElement = orderedData.get(i+1);
					for (String keyColumn : compareColumns){
						boolean valueChanged;
						if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.ENROLLMENT_AVAIL_COL) || keyColumn.equalsIgnoreCase(PlanMgmtConstants.EFFECTIVE_DATE_COL)){
							valueChanged = isEnrollmentChange(firstElement,secondElement,PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
						}else if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.BROCHURE_COL)) {
							valueChanged = isBroucherAdded(firstElement,secondElement,keyColumn);
						} else if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL)) {
							valueChanged = isBroucherAdded(firstElement,secondElement,keyColumn);
						} else {
							valueChanged = isEqual(firstElement,secondElement,keyColumn);
						}
						if(!valueChanged){
							Map<String, Object> processedMap = formMap(secondElement,keyColumn, columnsToDisplay);
							processedData.add(processedMap);
						}
					}
				}
			}
			if(size == 1){
				firstElement = orderedData.get(0);
				if(compareColumns.size() == 2 && StringUtils.equals(PlanMgmtConstants.STATUS_COL, compareColumns.get(0)) && StringUtils.equals(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, compareColumns.get(1))){
					Map<String, Object> processedMap = formMap(firstElement,compareColumns.get(0), columnsToDisplay);
					processedData.add(processedMap);
				}else {
					for(String keyColumn : compareColumns){
						Map<String, Object> processedMap = formMap(firstElement,keyColumn, columnsToDisplay);
						processedData.add(processedMap);
					}
				}
			}
		} catch (Exception e) {LOGGER.error("Error occured",e);}
		Collections.reverse(processedData);
		return processedData;
	}
	
	/**
	 * Compares values of maps firstElement and secondElement for key keyColumn.
	 */
	private boolean isEqual(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) 
	{
		if(null != firstElement.get(keyColumn) && null != secondElement.get(keyColumn)){
			return firstElement.get(keyColumn).equals(secondElement.get(keyColumn));
		}else{
			return false;
		}
	}
	
	/**
	 * Returns boolean value whether broucher added or not.
	 * @param firstElement of type Map<String, String> 
	 * @param secondElement of type Map<String, String> 
	 * @param keyColumn of type String
	 * @return of type boolean
	 */
	
	private boolean isBroucherAdded(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) {
		boolean flag = true;
		if(!firstElement.get(keyColumn).equals(secondElement.get(keyColumn))){
			flag = false;
		}
		// Here we are comparing brochureUCmId column so when we manually upload brochure then we store DMS id in brochureUCmId column
		if(firstElement.get(PlanMgmtConstants.BROCHURE_UCM_ID_CAL) != null && secondElement.get(PlanMgmtConstants.BROCHURE_UCM_ID_CAL) != null && !firstElement.get(PlanMgmtConstants.BROCHURE_UCM_ID_CAL).equals(secondElement.get(PlanMgmtConstants.BROCHURE_UCM_ID_CAL))){
			flag = false;
		}
		return flag;
	}
	
	/**
	 * Returns boolean value whether enrollment changed or not
	 * @param firstElement of type Map<String, String> 
	 * @param secondElement of type Map<String, String> 
	 * @param keyColumn of type String
	 * @return of type boolean
	 */
	private boolean isEnrollmentChange(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) {
		boolean flag = true;
		if(!firstElement.get(keyColumn).equals(secondElement.get(keyColumn))){
			flag = false;
		}
		if(firstElement.get(PlanMgmtConstants.EFFECTIVE_DATE_COL) != null && secondElement.get(PlanMgmtConstants.EFFECTIVE_DATE_COL) != null 
				&& !(firstElement.get(PlanMgmtConstants.EFFECTIVE_DATE_COL).contains(secondElement.get(PlanMgmtConstants.EFFECTIVE_DATE_COL)) || secondElement.get(PlanMgmtConstants.EFFECTIVE_DATE_COL).contains(firstElement.get(PlanMgmtConstants.EFFECTIVE_DATE_COL)))){
			flag = false;
		}
			
		return flag;
	}
	
	/**
	 * Forms map for given key.
	 * The newly formed map contains key-value pairs based on the list provided as columnsToDisplay.
	 */
	private Map<String,Object> formMap(Map<String,String> firstMap,String key, List<Integer> columnsToDisplay)
	{
		Map<String, Object> values = new HashMap<String, Object>();
		String fileUrl= null;
		for (int colId : columnsToDisplay)
		{
			switch (colId) 
			{
			case PlanMgmtConstants.ENROLLMENT_AVAIL_INT : 
				String enrollAvail = firstMap.get(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
				enrollAvail = getEnrollAvail(enrollAvail);
				values.put(PlanMgmtConstants.ENROLLMENT_AVAIL_COL, enrollAvail);
				break;
				
			case PlanMgmtConstants.UPDATE_DATE_INT: 
				String dt = firstMap.get(PlanMgmtConstants.UPDATED_DATE_COL);
				SimpleDateFormat sdf = new SimpleDateFormat();
				sdf.applyPattern(PlanMgmtConstants.TIMESTAMP_PATTERN1);
				try 
				{
					values.put(PlanMgmtConstants.UPDATED_DATE_COL, sdf.parse(dt));
				}
				catch (ParseException e) 
				{
					LOGGER.error("Unable to parse Updated Date "+ dt + " ", e);
				}
				break;
				
			case PlanMgmtConstants.EFFECTIVE_DATE_INT:
				String effDate = null;
				effDate = firstMap.get(PlanMgmtConstants.EFFECTIVE_DATE_COL);
				 if(StringUtils.isBlank(effDate)){
					 values.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, PlanMgmtConstants.EMPTY_STRING);
				 }else{
					SimpleDateFormat sdf1 = new SimpleDateFormat();
					sdf1.applyPattern(PlanMgmtConstants.TIMESTAMP_PATTERN2);
					try {
						values.put(PlanMgmtConstants.EFFECTIVE_DATE_COL, sdf1.parse(effDate));
					}
					catch (Exception e) {
						LOGGER.error("Unable to parse Effective Date "+ effDate + " ", e);
					}
				 }
				 break;
				
			case PlanMgmtConstants.USER_NAME_INT:
				String userId = firstMap.get(PlanMgmtConstants.UPDATED_BY_COL);
				if(StringUtils.isNotEmpty(userId) && StringUtils.isNumeric(userId)) {
					values.put(PlanMgmtConstants.USER_NAME_COL,  userService.getUserName(Integer.parseInt(userId)));
				} else {
					values.put(PlanMgmtConstants.USER_NAME_COL,  "");
				}
				break;
				
			case PlanMgmtConstants.COMMENT_INT:
				String commentId = firstMap.get(PlanMgmtConstants.COMMENT_COL);
				if(commentId != null && commentId.length()>0)
				{
					values.put(PlanMgmtConstants.COMMENT_COL, PlanMgmtConstants.COMMENT_TEXT_PRE+commentId+PlanMgmtConstants.COMMENT_TEXT_POST);
				}
				else
				{
					values.put(PlanMgmtConstants.COMMENT_COL, "");
				}
				break;

			case PlanMgmtConstants.STATUS_FILE_INT: 
				String statusFile = null;
				statusFile = firstMap.get(PlanMgmtConstants.STATUS_FILE_COL);
				if(statusFile != null && statusFile.length()>0)
				{
					fileUrl = PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT1 + appUrl + PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT2 + ghixJasyptEncrytorUtil.encryptStringByJasypt(statusFile) + PlanMgmtConstants.DOWNLOAD_FILE_POSTTEXT_CERTI;		
					values.put(PlanMgmtConstants.STATUS_FILE_COL, fileUrl);
				}
				else
				{
					values.put(PlanMgmtConstants.STATUS_FILE_COL, fileUrl);
				}
				break;
			case PlanMgmtConstants.STATUS_INT: 
				String status = firstMap.get(PlanMgmtConstants.STATUS_COL);
				status = getStatus(status);
				values.put(PlanMgmtConstants.STATUS_COL, status);
				break;
			case PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_VAL_INT:	
			case PlanMgmtConstants.FN_ENROLLMENT_AVAIL_VAL_INT: 
			case PlanMgmtConstants.FN_STATUS_VAL_INT:
			case PlanMgmtConstants.FN_EFFECTIVE_START_DATE_VAL_INT: 
			case PlanMgmtConstants.FN_BROCHURE_VAL_INT: 
				String  value = getValue(firstMap,key, colId);
				if (value != null)
				{
					values.put(PlanMgmtConstants.DISPLAYVAL_COL, value);
				}
				break;
			case PlanMgmtConstants.FN_DISPLAYVAL_INT:
				String displayVal = getDisplayVal(key);
				values.put(PlanMgmtConstants.DISPLAYFIELD_COL, displayVal);
				break;
				
			case PlanMgmtConstants.PROVIDER_NETWORK_NAME_INT:
				/*String providerId = firstMap.get(PlanMgmtConstants.PROVIDER_NETWORK_ID_COL);
				String name = ""; //getProviderDetail(providerId, PlanMgmtConstants.PROVIDER_NETWORK_NAME_INT);
				values.put(PlanMgmtConstants.PROVIDER_NETWORK_NAME_COL, name);*/
				break;

			case PlanMgmtConstants.PROVIDER_NETWORK_TYPE_INT:
			/*	String providerId1 = firstMap.get(PlanMgmtConstants.PROVIDER_NETWORK_ID_COL);
				String type = getProviderDetail(providerId1, PlanMgmtConstants.PROVIDER_NETWORK_TYPE_INT);
				values.put(PlanMgmtConstants.PROVIDER_NETWORK_TYPE_COL, type);*/
				break;
				
			case PlanMgmtConstants.START_DATE_INT:
				String effStartDate = null;
				effStartDate = firstMap.get(PlanMgmtConstants.START_DATE_COL);
				if(StringUtils.isBlank(effStartDate)){
					values.put(PlanMgmtConstants.START_DATE_COL, PlanMgmtConstants.EMPTY_STRING);
				}else{
					SimpleDateFormat sdf2 = new SimpleDateFormat();
					sdf2.applyPattern(PlanMgmtConstants.TIMESTAMP_PATTERN2);
					try{
						values.put(PlanMgmtConstants.START_DATE_COL, sdf2.parse(effStartDate));
					}
					catch (ParseException e){
						LOGGER.error("Effective Start Data parsing error", e);
					}
				}
				break;
			}
		}
		return values;
	}
	
	/**
	 * 
	 * @param providerNetworkId 
	 * 			String of providerNetworkId
	 * @return Object[] of provider data
	 */
	/*public Object[] getProvider(String providerNetworkId) {
		Object[] providerData = null;
		Integer provNetId = Integer.parseInt(providerNetworkId);
		if (providers.containsKey(provNetId))
		{
			providerData = providers.get(provNetId);
		}
		else
		{
			providerData = getProviderFromDB(provNetId);
			providers.put(provNetId, providerData);
		}
		return providerData;
	}*/

	/**
	 * Returns provider network details
	 * @param providerNetworkId of type Integer
	 * @return of type Object[]
	 */
	/*private Object[] getProviderFromDB(Integer providerNetworkId) {
		return providerNetworkService.getProvNetDetailsById(providerNetworkId);
	}*/

	/**
	 * Returns Network Type based on provider NetworkId
	 * @param providerNetworkId of type String
	 * @param providerColKey of type int
	 * @return of type String
	 */
	/*private String getProviderDetail(String providerNetworkId, int providerColKey) {
		
		String value = null;
		if(providerNetworkId.length()>0)
		{
			Object[] providerData = getProvider(providerNetworkId);
			if (providerColKey == PlanMgmtConstants.PROVIDER_NETWORK_NAME_INT)
			{
				value = (String) providerData[0];
			}
			else if (providerColKey == PlanMgmtConstants.PROVIDER_NETWORK_TYPE_INT)
			{
				value = ((NetworkType)providerData[1]).name();
			}
		}
		return value;
	}*/

	/**
	 * Returns enrollment available status
	 * @param enrollAvail of type String
	 * @return of type String
	 */
	private String getEnrollAvail(String enrollAvail) {
		String avail = null;
		if (Plan.EnrollmentAvail.AVAILABLE.toString().equals(enrollAvail))
		{
			avail = PlanMgmtConstants.AVAILABLE; 
		}
		else if (Plan.EnrollmentAvail.DEPENDENTSONLY.toString().equals(enrollAvail))
		{
			avail = PlanMgmtConstants.DEPENDENTS_ONLY; 
		}
		else if (Plan.EnrollmentAvail.NOTAVAILABLE.toString().equals(enrollAvail))
		{
			avail = PlanMgmtConstants.NOT_AVAILABLE; 
		}
		return avail;
	}
	
	/**
	 * Returns Issuer Verification status
	 * @param issuerVerificationStatus of type String
	 * @return of type String
	 */
	private String getIssuerVerificationStatus(String issuerVerificationStatus) {
		String status = null;
		if (Plan.IssuerVerificationStatus.PENDING.toString().equals(issuerVerificationStatus))
		{
			status = PlanMgmtConstants.NO; 
		}
		else if (Plan.IssuerVerificationStatus.VERIFIED.toString().equals(issuerVerificationStatus))
		{
			status = PlanMgmtConstants.YES; 
		}		
		return status;
	}

	/**
	 * Returns certification status
	 * @param status of type String
	 * @return String
	 */
	private String getStatus(String status) {
		String sts = null;
		if (Plan.PlanStatus.CERTIFIED.toString().equals(status))
		{
			sts = PlanMgmtConstants.CERTIFIED; 
		}
		else if (Plan.PlanStatus.DECERTIFIED.toString().equals(status))
		{
			sts = PlanMgmtConstants.DE_CERTIFIED; 
		}
		else if (Plan.PlanStatus.INCOMPLETE.toString().equals(status))
		{
			sts = PlanMgmtConstants.INCOMPLETE; 
		}
		else if (Plan.PlanStatus.LOADED.toString().equals(status))
		{
			sts = PlanMgmtConstants.LOADED; 
		}
		return sts;
	}
	
	/**
	 * 
	 * @param firstMap of type Map<String,String>
	 * @param key of type String
	 * @param colId of type int
	 * @return String
	 */
	
	private String getValue(Map<String,String> firstMap,String key, int colId) 
	{
		String value = null;
		switch (colId) 
		{
		case PlanMgmtConstants.FN_ENROLLMENT_AVAIL_VAL_INT: 
			if (PlanMgmtConstants.ENROLLMENT_AVAIL_COL.equals(key))
			{
				String enrollAvail = firstMap.get(PlanMgmtConstants.ENROLLMENT_AVAIL_COL);
				value = getEnrollAvail(enrollAvail);
			}
			break;
		case PlanMgmtConstants.FN_EFFECTIVE_START_DATE_VAL_INT:
			if (PlanMgmtConstants.EFFECTIVE_DATE_COL.equals(key))
			{
				Date dateValue = null;
				String effectiveDate = firstMap.get(PlanMgmtConstants.EFFECTIVE_DATE_COL);			
				dateValue = DateUtil.StringToDate(effectiveDate, PlanMgmtConstants.TIMESTAMP_PATTERN2);
				value = DateUtil.dateToString(dateValue, PlanMgmtConstants.TIMESTAMP_PATTERN3);
			}
			break;
		case PlanMgmtConstants.FN_STATUS_VAL_INT:
			if (PlanMgmtConstants.STATUS_COL.equals(key))
			{
				String status = firstMap.get(PlanMgmtConstants.STATUS_COL);
				value = getStatus(status);
			}
			break;
		case PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_VAL_INT:	
			if (PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL.equals(key))
			{
				String issuerVerificationStatus = firstMap.get(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL);
				value = getIssuerVerificationStatus(issuerVerificationStatus);
			}
			break;
		case PlanMgmtConstants.FN_BROCHURE_VAL_INT: 
			if (PlanMgmtConstants.BROCHURE_COL.equals(key)){
				
				// Here We are displaying download link based on brochure column here sequence is necessary because
				// When we get plan from SERFF we are getting brochure link or file and that link is gets stored in brochure. And after that we are storing DMS id in
				// brochureUCmId column so that afterwards we are ignoring brochure column value and we are displaying brochureUCmId value
				String brochure = firstMap.get(PlanMgmtConstants.BROCHURE_COL);
				
				String manualUploadedBroucger = null;
				
				try {
					manualUploadedBroucger = firstMap.get(PlanMgmtConstants.BROCHURE_UCM_ID_CAL);
				} catch (Exception e) {
					manualUploadedBroucger = null;
				}
				
				value = "";
				
				if(StringUtils.isNotBlank(brochure)){
					if(brochure.matches("(?i).*http.*")){
						value = PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT1 +  brochure + PlanMgmtConstants.DOWNLOAD_FILE_POSTTEXT_BROCHURE;						
					}else{
						value = PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT1 + "http://" + brochure + PlanMgmtConstants.DOWNLOAD_FILE_POSTTEXT_BROCHURE;
					}										
				}
				
				if(StringUtils.isNotBlank(manualUploadedBroucger)) {
					value = PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT1 + appUrl + PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT2 + ghixJasyptEncrytorUtil.encryptStringByJasypt(manualUploadedBroucger) + PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT3;
					LOGGER.debug("Download file path :== "+value);
				}
				
			}			
	
		}
		return value;
	}

	/**
	 * @param key of type String
	 * @return  String
	 */
	private String getDisplayVal(String key) 
	{
		String displayVal = null;
		if (key.equals(PlanMgmtConstants.STATUS_COL))
		{
			displayVal = PlanMgmtConstants.PLAN_STATUS; 
		}
		else if (key.equals(PlanMgmtConstants.ENROLLMENT_AVAIL_COL))
		{
			displayVal = PlanMgmtConstants.ENROLLMENT_AVAILABILITY_COL; 
		}
		else if (key.equals(PlanMgmtConstants.BROCHURE_COL))
		{
			displayVal = PlanMgmtConstants.PLAN_BROUCHURE; 
		}
		else if (key.equals(PlanMgmtConstants.EFFECTIVE_DATE_COL))
		{
			displayVal= PlanMgmtConstants.EFFECTIVE_DATE_COLUMN;
		}
		else if (key.equals(PlanMgmtConstants.ISSUER_VERIFICATION_STATUS_COL))
		{
			displayVal= PlanMgmtConstants.LEVEL_VERIFIED;
		}
		else if (key.equals(PlanMgmtConstants.BENEFITFILE_COL))
		{
			displayVal = PlanMgmtConstants.PLAN_BENEFITS; 
		}
		else if (key.equals(PlanMgmtConstants.RATEFILE_COL))
		{
			displayVal = PlanMgmtConstants.PLAN_RATES;
		}
		else if(key.equals(PlanMgmtConstants.PLAN_SBC_COL)) {
			
			displayVal = PlanMgmtConstants.PLAN_SBC;
		}
		return displayVal;
	}
	
	
}
