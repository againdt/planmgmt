/**
 * File 'IFormularyRepository' to .
 *
 */
package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Formulary;

/**
 * Repository class 'IFormularyRepository' to handle persistence of 'Formulary' records.
 *
 */
public interface IFormularyRepository extends JpaRepository<Formulary, Integer> {
	
	Formulary findById(int id);
	
	@Query("SELECT formulary.formularyUrl  FROM Formulary formulary WHERE formulary.id=:id")
	String findFormularyUrlById(@Param("id") Integer id);
	
	@Query("SELECT D.rxcui, DT.drugTierLevel, F.issuer.id, F.id, CS.id, D.authRequired, D.stepTherapyRequired  " +
			"FROM " +
			"Formulary F, DrugList DL, DrugTier DT, Drug D, CostShare CS "+
			"WHERE "+
			"F.drugList.id = DL.id	"+
			"AND DT.drugList.id = DL.id "+ 
			"AND D.drugTier.id = DT.id "+
			"AND DL.isDeleted = 'N' " +
			"AND CS.formulary.id = F.id " + 
			"AND CS.drugTierLevel = DT.drugTierLevel "+
			"AND CS.isDeleted='N' " +
			"AND CS.networkCostType = 'In Network' " +
			"AND CS.drugPrescriptionPeriod = 1 AND D.rxcui IN (:rxCode) AND F.issuer.id IN(:issuerId) AND F.id IN(:id) ")
	List<Object[]> searchByRxCode(@Param("rxCode") List<String> rxCode, @Param("issuerId") List<Integer> issuerId,  @Param("id") List<Integer> formularyId);
}
