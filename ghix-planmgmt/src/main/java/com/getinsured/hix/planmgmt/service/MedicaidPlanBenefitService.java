package com.getinsured.hix.planmgmt.service;

import java.util.List;

import com.getinsured.hix.dto.planmgmt.MedicaidPlan;

public interface MedicaidPlanBenefitService {

	List<MedicaidPlan> getMedicaidPlans(String zipCode, String countyCode, String effectiveDate);
}
