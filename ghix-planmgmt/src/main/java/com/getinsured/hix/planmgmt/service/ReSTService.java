/**
 * File.
 */
package com.getinsured.hix.planmgmt.service;

import java.io.Serializable;
import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;


/**
 * Interface 'ReSTService' for declaring ReST call methods.
 *
 * @author chalse_v
 * @version 1.0
 * @since May 10, 2013
 *
 */
public interface ReSTService extends Serializable {
	/** Attributes */
	static enum RequestMethod { GET, POST}
	/**
	 * Getter for 'url'.
	 *
	 * @return the url.
	 */
	public String getUrl();

	/**
	 * Setter for 'url'.
	 *
	 * @param url the url to set.
	 */
	public void setUrl(String url);

	/**
	 * Getter for 'requestMethod'.
	 *
	 * @return the requestMethod.
	 */
	public RequestMethod getRequestMethod();

	/**
	 * Setter for 'requestMethod'.
	 *
	 * @param requestMethod the requestMethod to set.
	 */
	public void setRequestMethod(RequestMethod requestMethod);
	
	/**
	 * Getter for 'parameters'.
	 *
	 * @return the parameters.
	 */
	public Map<String, Object> getParameters();
	
	/**
	 * Setter for 'parameters'.
	 *
	 * @param parameters the parameters to set.
	 * @throws IllegalArgumentException.
	 */
	public void setParameters(Map<String, Object> requestParameters);

	/**
	 * Method to add entries into parameters's collection.
	 *
	 * @param key The key for request data.
	 * @param data The request data.
	 * 
	 * @return void
	 * @throws IllegalArgumentException.
	 */
	public void addParameters(String key, Object value);

	/**
	 * Getter for 'responseClass'.
	 * 
	 * @return Class The specified Class instance.
	 */
	public Class getResponseClass();

	/**
	 * Setter for 'responseClass'.
	 * 
	 * @return Class The specified Class instance.
	 * @throws IllegalArgumentException.
	 */
	public void setResponseClass(Class responseClass);

	/**
	 * AddAll for 'requestParameters'.
	 *
	 * @param requestParameters the requestParameters to set.
	 * @throws IllegalArgumentException.
	 */
	public void addAllParameters(Map<String, Object> requestParameters);
	/**
	 * Method to reset attributes of instance.
	 *
	 * @return void
	 */
	public void resetRequest();
	/**
	 * Method to make ReST webservice call.
	 *
	 * @return response response instance.
	 */
	public Object sendRequest() throws GIException;
}
