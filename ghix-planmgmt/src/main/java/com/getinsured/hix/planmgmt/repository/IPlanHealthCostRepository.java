package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanHealthCost;

public interface IPlanHealthCostRepository extends JpaRepository<PlanHealthCost, Integer>{
	@Query("SELECT phc.inNetWorkInd, phc.inNetWorkFly, phc.inNetworkTier2Ind, phc.inNetworkTier2Fly, phc.outNetworkInd, phc.outNetworkFly, " +
			"phc.combinedInOutNetworkInd, phc.combinedInOutNetworkFly, phc.combDefCoinsNetworkTier1, phc.combDefCoinsNetworkTier2, phc.name, phc.limitExcepDisplay  from PlanHealthCost phc WHERE phc.planHealth.id = :planId")
	List<Object[]> getPlanHealthCostByPlanId(@Param("planId") Integer planId);
	
	@Query("SELECT planHealthCost from PlanHealthCost planHealthCost where planHealthCost.planHealth.plan.id IN (:planId )")
	List<PlanHealthCost> getPlanHealthCostsByPlanId(@Param("planId") List<Integer> planId);
	
	@Query("SELECT phc from PlanHealthCost phc WHERE phc.planHealth.id = :planHealthId")
	List<PlanHealthCost> getCostByPlanHealthId(@Param("planHealthId") Integer planHealthId);
	
}

