package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.PlanBenefitAndCostRequestDTO;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.PlanRateBenefit;

// This helper class to pull benefits and costs

public interface PlanCostAndBenefitsService {
	
	 //Map<String, Map<String, Map<String, String>>> getOptimizedHealthPlanBenefits(String planHealthIdStr, String effectiveDate);
	 
	 //Map<String, Map<String, Map<String, String>>>  getOptimizedHealthPlanCosts(String planHealthIdStr);
	 
	 //Map<String, Map<String, Map<String, String>>> getOptimizedDentalPlanBenefits(String planDentalIdStr, String effectiveDate);
	 
	 //Map<String, Map<String, Map<String, String>>> getOptimizedDentalPlanCosts(String planDentalIdStr, boolean sendInNetworkOnly);
	 
	 //Map<String, Map<String, Map<String, String>>> getHealthPlanBenefitsForTeaserPlan(String planHealthIdStr, String effectiveDate);
	 
	 Map<String, Map<String, String>> getHealthPlanCosts(int planHealthId);
	 
	 Map<String, Map<String, String>> getDentalPlanCosts(int planDentalId);	 
	
	 Map<String, Map<String, String>> getDentalPlanBenefits(int planHealthId, String effectiveDate);
	 
	 Map<String, Map<String, String>> getHealthPlanBenefits(int planHealthId, String effectiveDate);
	 
	 Map<String, Object>getBenefitsAndCostByPlanId(List<Integer> planIds , String insuranceType );
	 
	 List<PlanRateBenefit> getIndividualPlanBenefitsAndCost(List<Integer> planIds, Map<Integer, String> planIdAndHiosIdMap, String insuranceType, String coverageStartDate);

	 /*Newly added interface for performance improvement*/
	 Map<Integer, Map<String, Map<String, String>>> getOptimizedHealthPlanBenefits(List<Integer> healthOrDentalIdsList, String effectiveDate);
	 
	 Map<Integer, Map<String, Map<String, String>>> getOptimizedHealthPlanCosts(List<Integer> healthOrDentalIdsList);
	 
	 Map<String, Map<String, Map<String, String>>> getOptimizedHealthPlanCosts(Map<Integer,PlanRateBenefit> planBenefitsMap);

	 Map<Integer, Map<String, Map<String, String>>> getOptimizedDentalPlanBenefits(List<Integer> planIdList, String effectiveDate);
	 
	 Map<Integer, Map<String, Map<String, String>>> getOptimizedDentalPlanCosts(List<Integer> planDentalIdList, boolean sendInNetworkOnly);

	 Map<Integer, Map<String, Map<String, String>>> getHealthPlanBenefitsForTeaserPlan(List<Integer> planHealthIdList, String effectiveDate);
	 
	 boolean anyUpdatesInBenefits(String issuerPlanNumber, Integer applicableYear);

	 List<PlanRateBenefit> getIndividualPlanBenefitsAndCostNew(List<Integer> planIds , String insuranceType , String coverageStartDate);
	 
	 GHIXResponse updatePlanBenefitAndCost(PlanBenefitAndCostRequestDTO planBenefitAndCostRequestDTO);
}
