package com.getinsured.hix.planmgmt.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.LifePlanDTO;
import com.getinsured.hix.dto.planmgmt.Member;
import com.getinsured.hix.planmgmt.querybuilder.PlanMgmtQueryBuilder;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtParamConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;



@Service("LifePlanRateBenefitService")
public class LifePlanRateBenefitServiceImpl implements LifePlanRateBenefitService{
	private static final Logger LOGGER = Logger.getLogger(LifePlanRateBenefitServiceImpl.class);
	@Value("#{configProp['appUrl']}")
	private String appUrl;
	@PersistenceUnit 
	private EntityManagerFactory emf;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	
	@Override
	@Transactional(readOnly = true)
	public List<LifePlanDTO> getPlans(String effectiveDate, Member memberInfo, String stateCode, String tenantCode){
		List<LifePlanDTO> LifePlanList =  new ArrayList<LifePlanDTO>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			LifePlanDTO lifePlan = null;
			String queryBuilder = PlanMgmtQueryBuilder.getLifePlanQuotingQuery();
			//LOGGER.info("QUERY = " + queryBuilder);
			
			Query query = entityManager.createQuery(queryBuilder.toString());
			query.setParameter(PlanMgmtParamConstants.PARAM_STATE_CODE, stateCode);
			query.setParameter(PlanMgmtParamConstants.PARAM_EFFECTIVE_DATE, effectiveDate);
			query.setParameter(PlanMgmtParamConstants.PARAM_GENDER, memberInfo.getGender());
			query.setParameter(PlanMgmtParamConstants.PARAM_AGE, memberInfo.getAge());
			query.setParameter(PlanMgmtParamConstants.PARAM_TOBACCO, memberInfo.getTobacco());
			query.setParameter(PlanMgmtParamConstants.PARAM_TENANT_CODE, tenantCode);
				
			Iterator<?> rsIterator = query.getResultList().iterator();
			Object[] tempObject = null;
			int coverageAmt = 0;
			float premium = 0;
			String logoURL = null;
			DecimalFormat df = new DecimalFormat("####0.00");
			while (rsIterator.hasNext()) {
				lifePlan = new LifePlanDTO();
				
				tempObject = (Object[]) (rsIterator.next());
				coverageAmt = Integer.parseInt(tempObject[PlanMgmtConstants.SIX].toString());
				lifePlan.setPlanId(Integer.parseInt(tempObject[PlanMgmtConstants.ZERO].toString()));
				lifePlan.setName(tempObject[PlanMgmtConstants.ONE].toString());
				lifePlan.setIssuerId(Integer.parseInt(tempObject[PlanMgmtConstants.TWO].toString()));
				lifePlan.setIssuerName(tempObject[PlanMgmtConstants.THREE].toString());
				if(null != tempObject[PlanMgmtConstants.FOUR]){
					logoURL = tempObject[PlanMgmtConstants.FOUR].toString();
				}
				lifePlan.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, null, ghixJasyptEncrytorUtil.encryptStringByJasypt(tempObject[PlanMgmtConstants.TWO].toString()), appUrl));
				premium = Float.parseFloat(tempObject[PlanMgmtConstants.FIVE].toString());
			
				if(coverageAmt > 0){
					coverageAmt = coverageAmt/1000; // multiply premium per 1k of coverage amount. if coverage amount is $250,000 then multiply rate with 250 
					premium = (premium * coverageAmt) / 12; // Divide total premium by 12 to compute monthly premium because we insert yearly rate in rate table
				}
				
				lifePlan.setPremium(Float.parseFloat(df.format(premium)));
				lifePlan.setCoverageAmount(tempObject[PlanMgmtConstants.SIX].toString());
				lifePlan.setPolicyTerm(tempObject[PlanMgmtConstants.SEVEN].toString());
			    lifePlan.setBenefitUrl((null != tempObject[PlanMgmtConstants.EIGHT]) ? tempObject[PlanMgmtConstants.EIGHT].toString() : PlanMgmtConstants.EMPTY_STRING);
				LifePlanList.add(lifePlan);
			}
		}
		catch(Exception ex){
			LOGGER.error("Exceptin Occured in getPlans", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}

		 
		 return LifePlanList;
	}

}
