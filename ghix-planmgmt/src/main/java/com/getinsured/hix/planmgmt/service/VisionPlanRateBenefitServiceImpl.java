package com.getinsured.hix.planmgmt.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.VisionPlan;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;

@Service("VisionPlanRateBenefitService")
@Repository
@Transactional
public class VisionPlanRateBenefitServiceImpl implements VisionPlanRateBenefitService{
	
	@Value("#{configProp['appUrl']}")
	private String appUrl;
	
	private static final String DOCUMENT_URL = "download/document?documentId="; // document download url
	private static final Logger LOGGER = Logger.getLogger(VisionPlanRateBenefitServiceImpl.class);
	
	@PersistenceUnit 
	private EntityManagerFactory emf;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired
	private QuotingBusinessLogic quotingBusinessLogic;
	
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Override
	@Transactional(readOnly = true)
	public List<VisionPlan> getVisionPlans(List<Map<String, String>> memberList, String effectiveDate, String tenant){		
		List<VisionPlan> visionPlanList = new ArrayList<VisionPlan>(); 
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			List<Map<String, String>> processedMemberData = quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.VISION.toString()); // filter member data					
			String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
			String familyTierLookupCode = quotingBusinessLogic.getFamilyTierLookupCode(familyTier); // compute family tier lookup code		
			String stateName =  quotingBusinessLogic.getApplicantStateCode(processedMemberData);
						
			StringBuilder buildQuery = new StringBuilder();
			ArrayList<String> paramList = new ArrayList<String>();
			//Integer paramCount = 0;
			buildQuery.append("SELECT P.ID, PLAN_VISION_ID, p.ISSUER_PLAN_NUMBER, P.ISSUER_ID, P.NAME AS PLAN_NAME, SUM(PRE) AS total_premium,  P.BROCHURE, I.NAME AS ISSUER_NAME, I.LOGO_URL, ");
			buildQuery.append("OUT_OF_NETWK_AVAILABILITY, OUT_OF_NETWK_COVERAGE, PROVIDER_NETWORK_URL, PREMIUM_PAYMENT, RATE_OPTION ");
			buildQuery.append("FROM ");
			buildQuery.append("PLAN P, ISSUERS I, PM_TENANT_PLAN tp, TENANT t,");
			buildQuery.append(" ( ");
			for (int i = 0; i < processedMemberData.size(); i++) {			
				buildQuery.append(" ( SELECT p2.id AS plan_id, prate.RATE AS PRE, pvision.ID AS PLAN_VISION_ID, 1 as memberctr, ");
				buildQuery.append("pvision.OON_AVAILABILITY AS OUT_OF_NETWK_AVAILABILITY, pvision.OON_COVERAGE as OUT_OF_NETWK_COVERAGE, ");
				buildQuery.append("pvision.PROVIDER_NETWORK_URL AS PROVIDER_NETWORK_URL, pvision.PREMIUM_PAYMENT AS PREMIUM_PAYMENT, prate.rate_option AS RATE_OPTION ");
				buildQuery.append("FROM ");
				buildQuery.append("PLAN_VISION pvision, PM_PLAN_RATE prate, PLAN p2 ");
				buildQuery.append("WHERE ");
				buildQuery.append("p2.ID = pvision.PLAN_ID ");
				buildQuery.append("AND p2.id = prate.plan_id ");
				buildQuery.append(quotingBusinessLogic.buildRateLogic("prate", stateName, "", familyTierLookupCode, processedMemberData.get(i).get(PlanMgmtConstants.AGE), "", effectiveDate, paramList));
				buildQuery.append("AND p2.state= '" + stateName + "' ");			
				//paramCount++;
				buildQuery.append(" ) ");
	
				if (i + 1 != processedMemberData.size()) {
					buildQuery.append(" UNION ALL ");
				}
			}
			
			buildQuery.append(") temp WHERE P.id = temp.plan_id ");	
			buildQuery.append("AND I.ID = P.ISSUER_ID ");		
			buildQuery.append("AND P.INSURANCE_TYPE = 'VISION' ");
			buildQuery.append(quotingBusinessLogic.buildPlanCertificationLogic("P", true));
			
			buildQuery.append(" AND p.id = tp.plan_id");
			buildQuery.append(" AND tp.tenant_id = t.id");
			buildQuery.append(" AND t.code = UPPER(:param_" + paramList.size() +") ");
			paramList.add(tenant);
			
			buildQuery.append("AND p.is_deleted='N' ");
			buildQuery.append("GROUP BY ");
			buildQuery.append("P.id, PLAN_VISION_ID, p.ISSUER_PLAN_NUMBER, P.ISSUER_ID,  P.NAME, P.BROCHURE, I.NAME, I.LOGO_URL, ");
			buildQuery.append("OUT_OF_NETWK_AVAILABILITY, OUT_OF_NETWK_COVERAGE, PROVIDER_NETWORK_URL, PREMIUM_PAYMENT, RATE_OPTION  ");
			buildQuery.append("HAVING sum(memberctr) = " + processedMemberData.size());
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug(" buildQuery :: " + buildQuery.toString());
			}
							
			StringBuilder planVisionIdStr = new StringBuilder(PlanMgmtConstants.EMPTY_STRING);
			
		
			String brochureUrl = PlanMgmtConstants.EMPTY_STRING;	
			Float premium = (float) 0;
			
			Query query = entityManager.createNativeQuery(buildQuery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
			Iterator<?> rsIterator2 = rsList.iterator();
			
			while (rsIterator.hasNext()) {
				String logoURL = null;
				Object[] objArray = (Object[]) rsIterator.next();
				VisionPlan visionPlan = new VisionPlan();
				visionPlan.setId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
				// concatenating plan_vision_ids to generate a single query
				planVisionIdStr.append((planVisionIdStr.length() == PlanMgmtConstants.ZERO) ? "'" + objArray[PlanMgmtConstants.ONE].toString() + "'" : ", '" + objArray[PlanMgmtConstants.ONE].toString() + "'");

				visionPlan.setHiosPlanId((objArray[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWO].toString());		
				visionPlan.setIssuerId(Integer.parseInt(objArray[PlanMgmtConstants.THREE].toString()));
				visionPlan.setName((objArray[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOUR].toString());
				premium = Float.parseFloat(objArray[PlanMgmtConstants.FIVE].toString());
				brochureUrl = ((objArray[PlanMgmtConstants.SIX]) == null ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SIX].toString());
				if (StringUtils.isNotBlank(brochureUrl)) {				     
				     if (brochureUrl.matches(PlanMgmtConstants.ECM_REGEX)) {
				      brochureUrl = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(brochureUrl);    
				     }
				     else if (!brochureUrl.matches(PlanMgmtConstants.URL_REGEX)) {
				      brochureUrl = PlanMgmtConstants.HTTP_PROTOCOL + brochureUrl;    
				     }				    
				} 
				visionPlan.setBrochureUrl(brochureUrl);
				visionPlan.setIssuerName((objArray[PlanMgmtConstants.SEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SEVEN].toString());
				//visionPlan.setIssuerLogo((objArray[PlanMgmtConstants.EIGHT] !=null) ? appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.EIGHT].toString()) : PlanMgmtConstants.EMPTY_STRING);
				if(null != objArray[PlanMgmtConstants.EIGHT]) {
					logoURL = objArray[PlanMgmtConstants.EIGHT].toString();
				}
				visionPlan.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, null, ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.THREE].toString()), appUrl));
				visionPlan.setOutOfNetwkAvailability((objArray[PlanMgmtConstants.NINE] !=null) ? objArray[PlanMgmtConstants.NINE].toString() : PlanMgmtConstants.EMPTY_STRING);
				visionPlan.setOutOfNetwkCoverage((objArray[PlanMgmtConstants.TEN] !=null) ? objArray[PlanMgmtConstants.TEN].toString() : PlanMgmtConstants.EMPTY_STRING);
				visionPlan.setProviderNetworkUrl((objArray[PlanMgmtConstants.ELEVEN] !=null) ?  (!objArray[PlanMgmtConstants.ELEVEN].toString().matches(PlanMgmtConstants.URL_REGEX)) ? PlanMgmtConstants.HTTP_PROTOCOL + objArray[PlanMgmtConstants.ELEVEN].toString() : objArray[PlanMgmtConstants.ELEVEN].toString() : PlanMgmtConstants.EMPTY_STRING);
				visionPlan.setPremiumPayment((objArray[PlanMgmtConstants.TWELVE] !=null) ? objArray[PlanMgmtConstants.TWELVE].toString() : PlanMgmtConstants.EMPTY_STRING);
				
				String rateOption = objArray[PlanMgmtConstants.THIRTEEN].toString();
				
				// if rate option is Family, then premium would be on family level, don't consider each quoted member's premium
				// if rate option is Age band, then premium would be for each member and aggregate for whole household
				if ("F".equalsIgnoreCase(rateOption)) {  // family quoting 
					premium = (premium / processedMemberData.size());
				}
				
				visionPlan.setPremium(premium);
				
				visionPlanList.add(visionPlan);
				visionPlan = null;
			}			
			
			// pull plan benefits in a single shot
			Map<String, Map<String, Map<String, String>>> benefitDataList = new HashMap<String, Map<String, Map<String, String>>>();
			if(planVisionIdStr.length() > PlanMgmtConstants.ZERO){
				benefitDataList = getVisionPlanBenefits(planVisionIdStr.toString(), PlanMgmtConstants.EMPTY_STRING);
			}
			
			
			while (rsIterator2.hasNext()) {
				Object[] objArray = (Object[]) rsIterator2.next();
				String planVisionId = objArray[PlanMgmtConstants.ONE].toString();
				Integer planId = Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString());
				// set plan benefits
				for (VisionPlan visionPlanObj : visionPlanList) {
						if (planId.equals(visionPlanObj.getId()) && benefitDataList.containsKey(planVisionId)) {
							visionPlanObj.setBenefits(benefitDataList.get(planVisionId));						
						}
					}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured in getvisionPlanDetails", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return visionPlanList;
	}

	
	// fetch Vision plan benefits
	@Override
	@Transactional(readOnly = true)
	public Map<String, Map<String, Map<String, String>>> getVisionPlanBenefits(String planVisionIdStr, String filterBenefits) {
		Map<String, Map<String, Map<String, String>>> benefitDataList = new HashMap<String, Map<String, Map<String, String>>>();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;		
		
		try {            
            Context initialContext = new InitialContext();
		    DataSource datasource = (DataSource)initialContext.lookup(PlanMgmtConstants.DATASRC);
		    
		    if (datasource != null) {
		        conn = datasource.getConnection();		       
	            StringBuilder buildquery = new StringBuilder();
	    		buildquery.append("SELECT plan_vision_id, benefit_name, benefit_details, copay_val, copay_attr " );	    		
	    		buildquery.append("FROM ");
	    		buildquery.append("plan_vision_benefits ");
	    		buildquery.append("WHERE ");
	    		buildquery.append("plan_vision_id IN (");
	    		buildquery.append(planVisionIdStr);
	    		buildquery.append(")");
	    		
	    		// when looking for specific plan benefits
	    		if(filterBenefits.length() > PlanMgmtConstants.ZERO){
	    			buildquery.append(" AND benefit_name in(");
	    			buildquery.append(filterBenefits);
	    			buildquery.append(")");
	    		}
	    		
	    		LOGGER.debug("SQL = " + buildquery.toString());	    				    		
	    		stmt = conn.prepareStatement(buildquery.toString());	    	
			    stmt.setFetchSize(PlanMgmtConstants.FETCHSIZE);	    		
	    		rs = stmt.executeQuery();	    		
	          
	    		while ( rs.next() ) {
					String benefitName = rs.getString("benefit_name");
					String rsPlanVisionId = rs.getString("plan_vision_id");
					Map<String, String> benefitAttrib = new HashMap<String, String>();				
	
					benefitAttrib.put(PlanMgmtConstants.BENEFIT_DETAILS, (rs.getString("benefit_details") == null) ? "" : rs.getString("benefit_details"));
					benefitAttrib.put(PlanMgmtConstants.COPAY_VALUE, (rs.getString("copay_val") == null) ? "" : rs.getString("copay_val"));
					benefitAttrib.put(PlanMgmtConstants.COPAY_ATTRIBUTE, (rs.getString("copay_attr") == null) ? "" : rs.getString("copay_attr"));
					
					if(benefitDataList.get(rsPlanVisionId) == null){
						benefitDataList.put(rsPlanVisionId,new HashMap<String, Map<String, String>>() );
					}
					benefitDataList.get(rsPlanVisionId).put(benefitName, benefitAttrib);
	            }    
		    } 
        } catch (Exception e) {
        	LOGGER.error("Got an exception to execute getvisionPlanBenefits()! Exception ==> " , e );           
        }finally{
        	PlanMgmtUtil.closeResultSet(rs);
			PlanMgmtUtil.closePreparedStatement(stmt);
			PlanMgmtUtil.closeDBConnection(conn);	
        }			
		
		return benefitDataList;
	}
	
	
		// compute lowest vision plan HIX-62501
		@Override
		@Transactional(readOnly = true)
		public List<VisionPlan> getLowestVisionPlan(List<Map<String, String>> memberList, String effectiveDate, String tenant) {
			List<VisionPlan> visionPlanList = new ArrayList<VisionPlan>(); 
			EntityManager entityManager = null;
			
			try{
				entityManager = emf.createEntityManager();
				if(LOGGER.isInfoEnabled()){
					LOGGER.info("-------- Inside getLowestVisionPlan --------");
				}
				List<Map<String, String>> processedMemberData =  quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.VISION.toString());
				String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
				String familyTierLookupCode = lookupService.getLookupValueCode(PlanMgmtConstants.FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
				String stateName = quotingBusinessLogic.getApplicantStateCode(processedMemberData);	
							
				StringBuilder buildQuery = new StringBuilder();
				ArrayList<String> paramList = new ArrayList<String>();				
				buildQuery.append("SELECT p.id, (CASE WHEN RATE_OPTION = 'F' THEN SUM(PRE)/");
				buildQuery.append(processedMemberData.size());
				buildQuery.append(" ELSE SUM(PRE) END) AS total_premium, ");
				buildQuery.append("p.name, I.logo_url, I.id AS issuerId ");
				buildQuery.append("FROM ");
				buildQuery.append("PLAN p, ISSUERS I, PM_TENANT_PLAN tp, TENANT t,");
				buildQuery.append(" ( ");
				for (int i = 0; i < processedMemberData.size(); i++) {			
					buildQuery.append(" ( SELECT p2.id AS plan_id, prate.RATE AS PRE, prate.rate_option AS RATE_OPTION, 1 as memberctr ");
					buildQuery.append("FROM ");
					buildQuery.append("PLAN_VISION pvision, PM_PLAN_RATE prate, PLAN p2 ");
					buildQuery.append("WHERE ");
					buildQuery.append("p2.ID = pvision.PLAN_ID ");
					buildQuery.append("AND p2.id = prate.plan_id ");				
					buildQuery.append(quotingBusinessLogic.buildRateLogic("prate", stateName, "", familyTierLookupCode, processedMemberData.get(i).get(PlanMgmtConstants.AGE), "", effectiveDate, paramList));
					buildQuery.append("AND p2.state='" + stateName + "' ");
					buildQuery.append(" ) ");

					if (i + 1 != processedMemberData.size()) {
						buildQuery.append(" UNION ALL ");
					}
				}
				
				buildQuery.append(") temp WHERE p.id = temp.plan_id ");	
				buildQuery.append("AND I.ID = p.ISSUER_ID ");		
				buildQuery.append("AND p.INSURANCE_TYPE = '");
				buildQuery.append(Plan.PlanInsuranceType.VISION);
				buildQuery.append("'");
				buildQuery.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
				 
				buildQuery.append(" AND p.id = tp.plan_id");
				buildQuery.append(" AND tp.tenant_id = t.id");
				buildQuery.append(" AND t.code = UPPER(:param_" + paramList.size() +") ");
				paramList.add(tenant);

				buildQuery.append(" AND p.is_deleted='N' ");
				buildQuery.append("GROUP BY ");
				buildQuery.append("p.id, RATE_OPTION, p.name, I.logo_url, I.id ");					
				buildQuery.append("HAVING sum(memberctr) = ");
				buildQuery.append(processedMemberData.size());
				buildQuery.append(" ORDER BY total_premium ");	
				
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("SQL == " + buildQuery.toString());
				}
				
				Query query = entityManager.createNativeQuery(buildQuery.toString());
				for (int i = 0; i < paramList.size(); i++) {
					query.setParameter("param_" + i, paramList.get(i));
				}
				
				List<?> rsList = query.getResultList();
				Iterator<?> rsIterator = rsList.iterator();
				
				while (rsIterator.hasNext()) {
					Object[] objArray = (Object[]) rsIterator.next();
					String logoURL = null;
					VisionPlan visionPlan = new VisionPlan();
					visionPlan.setId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
					visionPlan.setPremium(Float.parseFloat(objArray[PlanMgmtConstants.ONE].toString()));
					visionPlan.setName((objArray[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWO].toString());
					if(null != objArray[PlanMgmtConstants.THREE]) {
						logoURL = objArray[PlanMgmtConstants.THREE].toString();
					}
					visionPlan.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, null, ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.FOUR].toString()), appUrl));

					visionPlanList.add(visionPlan);
					break;
				}	
			}catch(Exception ex){		
				LOGGER.error("Exceptin Occured in getLowestVisionPlanPremium", ex);
			}finally{
				 // close the entity manager
				 if(entityManager !=null && entityManager.isOpen()){
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
			}
			
			return visionPlanList;
		}

}
