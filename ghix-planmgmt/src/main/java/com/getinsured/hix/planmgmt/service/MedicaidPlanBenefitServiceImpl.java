package com.getinsured.hix.planmgmt.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.MedicaidPlan;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;

@Service("MedicaidPlanBenefitService")
@Repository
@Transactional
public class MedicaidPlanBenefitServiceImpl implements MedicaidPlanBenefitService{
	
	@Value("#{configProp['appUrl']}")
	private String appUrl;
	
	//private static final String DOCUMENT_URL = "download/document?documentId="; // document download url
	private static final Logger LOGGER = Logger.getLogger(MedicaidPlanBenefitServiceImpl.class);
	@PersistenceUnit 
	private EntityManagerFactory emf;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	private static final String EFFECTIVEDATE = "effectiveDate";
	private static final String ZIPCODE = "zipCode";
	private static final String COUNTYCODE = "countyCode";
	
	@Override
	@Transactional(readOnly = true)
	public List<MedicaidPlan> getMedicaidPlans(String zipCode, String countyCode, String effectiveDate){
		EntityManager entityManager = null;
		List<MedicaidPlan> medicaidPlanList = new ArrayList<MedicaidPlan>(); 
		
		try{
			entityManager = emf.createEntityManager();
			StringBuilder buildquery = new StringBuilder();
			buildquery.append("SELECT p.id, p.name, i.id AS issuerid, i.name AS issuername, i.logo_url, pmedicaid.id AS medicaidid, pmedicaid.member_number, pmedicaid.website_url, pmedicaid.quality_rating ");
			buildquery.append("FROM ");
			buildquery.append("plan p, plan_medicaid pmedicaid, issuers i, pm_service_area psarea ");
			buildquery.append("WHERE ");
			buildquery.append("i.id = p.issuer_id ");
			buildquery.append("AND p.id = pmedicaid.plan_id ");
			buildquery.append("AND p.service_area_id = psarea.service_area_id ");
			buildquery.append("AND TO_DATE (:" + EFFECTIVEDATE + ", 'YYYY-MM-DD') between p.start_date and p.end_date ");
			buildquery.append("AND p.status IN ('" + Plan.PlanStatus.CERTIFIED.toString() + "') ");
			buildquery.append("AND p.issuer_verification_status = '" + Plan.IssuerVerificationStatus.VERIFIED.toString() + "' ");		
			buildquery.append("AND psarea.zip = :" + ZIPCODE + " ");
			buildquery.append("AND psarea.fips = :" + COUNTYCODE + " ");
			buildquery.append("AND p.is_deleted = 'N' ");
			buildquery.append("AND psarea.is_deleted = 'N' ");
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Medicaid SQL == " + buildquery.toString());
			}
			Query query = entityManager.createNativeQuery(buildquery.toString());
			query.setParameter(EFFECTIVEDATE, effectiveDate);
			query.setParameter(ZIPCODE, zipCode);
			query.setParameter(COUNTYCODE, countyCode);
			
			List<?> rsList = query.getResultList();
			Iterator rsIterator = rsList.iterator();					
			Iterator rsIterator2 = rsList.iterator();
			
			StringBuilder planMedicaidIdStr = new StringBuilder(PlanMgmtConstants.EMPTY_STRING);
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				MedicaidPlan medicaidPlan = new MedicaidPlan();
				String logoURL = null;
				if(null != objArray[PlanMgmtConstants.FOUR]) {
					logoURL = objArray[PlanMgmtConstants.FOUR].toString();
				}
	
				medicaidPlan.setId((objArray[PlanMgmtConstants.ZERO] != null) ?  objArray[PlanMgmtConstants.ZERO].toString() : PlanMgmtConstants.EMPTY_STRING );
				medicaidPlan.setName((objArray[PlanMgmtConstants.ONE] != null) ?  objArray[PlanMgmtConstants.ONE].toString() : PlanMgmtConstants.EMPTY_STRING );
				medicaidPlan.setIssuerId((objArray[PlanMgmtConstants.TWO] != null) ?  objArray[PlanMgmtConstants.TWO].toString() : PlanMgmtConstants.EMPTY_STRING );
				medicaidPlan.setIssuerName((objArray[PlanMgmtConstants.THREE] != null) ?  objArray[PlanMgmtConstants.THREE].toString() : PlanMgmtConstants.EMPTY_STRING );
				medicaidPlan.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, null, ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.TWO].toString()), appUrl));
				planMedicaidIdStr.append((planMedicaidIdStr.length() == PlanMgmtConstants.ZERO) ? "'" + objArray[PlanMgmtConstants.FIVE].toString() + "'" : ", '" + objArray[PlanMgmtConstants.FIVE].toString() + "'");
				medicaidPlan.setMemberNumber((objArray[PlanMgmtConstants.SIX] != null) ?  objArray[PlanMgmtConstants.SIX].toString() : PlanMgmtConstants.EMPTY_STRING);
				medicaidPlan.setWebsiteURL((objArray[PlanMgmtConstants.SEVEN] != null) ?  objArray[PlanMgmtConstants.SEVEN].toString() : PlanMgmtConstants.EMPTY_STRING);
				medicaidPlan.setQualityRating((objArray[PlanMgmtConstants.EIGHT] != null) ?  objArray[PlanMgmtConstants.EIGHT].toString() : PlanMgmtConstants.EMPTY_STRING);
				
				medicaidPlanList.add(medicaidPlan);			
			}
			
			if (planMedicaidIdStr.length() > PlanMgmtConstants.ZERO) {
				Map<String, Map<String, Map<String, String>>> benefitData = getMedicaidPlanBenefits(planMedicaidIdStr.toString()); // get medicaid plan benefits
				Map<String, Map<String, Map<String, String>>> serviceData = getMedicaidPlanServices(planMedicaidIdStr.toString()); // get medicaid plan services
							
				while (rsIterator2.hasNext()) {
					Object[] objArray = (Object[]) rsIterator2.next();
					String planMedicaidId = objArray[PlanMgmtConstants.FIVE].toString();
					String planId = objArray[PlanMgmtConstants.ZERO].toString();
					
					for (MedicaidPlan planObj : medicaidPlanList) {					
						if (planId.equals(planObj.getId())) {
							if (benefitData.containsKey(planMedicaidId)) {					
								planObj.setBenefits(benefitData.get(planMedicaidId));
							}
							if (serviceData.containsKey(planMedicaidId)) {					
								planObj.setServices(serviceData.get(planMedicaidId));
							}
						}
					}
				}	
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getMedicaidPlans: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return medicaidPlanList;
	}

	
	// fetch Medicaid plan benefits
	@Transactional(readOnly = true)
	private Map<String, Map<String, Map<String, String>>> getMedicaidPlanBenefits(String planMedicaidIdStr) {
		Map<String, Map<String, Map<String, String>>> benefitDataList = new HashMap<String, Map<String, Map<String, String>>>();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;		
		
		try {            
            Context initialContext = new InitialContext();
		    DataSource datasource = (DataSource)initialContext.lookup(PlanMgmtConstants.DATASRC);
		    
		    if (datasource != null) {
		        conn = datasource.getConnection();		       
	            StringBuilder buildquery = new StringBuilder();
	    		buildquery.append("SELECT medicaid_id, benefit_name, benefit_attr, age_min, age_max " );	    		
	    		buildquery.append("FROM ");
	    		buildquery.append("plan_medicaid_benefits ");
	    		buildquery.append("WHERE ");
	    		buildquery.append("medicaid_id IN (");
	    		buildquery.append(planMedicaidIdStr);
	    		buildquery.append(")");
	    		
	    		LOGGER.debug("SQL = " + buildquery.toString());	    				    		
	    		stmt = conn.prepareStatement(buildquery.toString());	    	
			    stmt.setFetchSize(PlanMgmtConstants.FETCHSIZE);	    		
	    		rs = stmt.executeQuery();	    		
	          
	    		while ( rs.next() ) {
					String benefitName = rs.getString("benefit_name");
					String rsPlanMedicaidId = rs.getString("medicaid_id");
					Map<String, String> benefitAttrib = new HashMap<String, String>();				
	
					benefitAttrib.put("benefitAttribute", (rs.getString("benefit_attr") == null) ? "" : rs.getString("benefit_attr"));
					benefitAttrib.put("minAge", (rs.getString("age_min") == null) ? "" : rs.getString("age_min"));
					benefitAttrib.put("maxAge", (rs.getString("age_max") == null) ? "" : rs.getString("age_max"));
					
					if(benefitDataList.get(rsPlanMedicaidId) == null){
						benefitDataList.put(rsPlanMedicaidId,new HashMap<String, Map<String, String>>() );
					}
					benefitDataList.get(rsPlanMedicaidId).put(benefitName, benefitAttrib);
	            }    
		    } 
        } catch (Exception e) {
        	LOGGER.error("Got an exception to execute getMedicaidPlanBenefits()! Exception ==> " , e );           
        }finally{
        	PlanMgmtUtil.closeResultSet(rs);
        	PlanMgmtUtil.closePreparedStatement(stmt);
        	PlanMgmtUtil.closeDBConnection(conn);	
        }			
		
		return benefitDataList;
	}

	
	// fetch Medicaid plan services
	@Transactional(readOnly = true)
	private Map<String, Map<String, Map<String, String>>> getMedicaidPlanServices(String planMedicaidIdStr) {
		Map<String, Map<String, Map<String, String>>> serviceDataList = new HashMap<String, Map<String, Map<String, String>>>();
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;		
		
		try {            
            Context initialContext = new InitialContext();
		    DataSource datasource = (DataSource)initialContext.lookup(PlanMgmtConstants.DATASRC);
		    
		    if (datasource != null) {
		        conn = datasource.getConnection();		       
	            StringBuilder buildquery = new StringBuilder();
	    		buildquery.append("SELECT medicaid_id, service_name, service_type, service_attr " );	    		
	    		buildquery.append("FROM ");
	    		buildquery.append("plan_medicaid_services ");
	    		buildquery.append("WHERE ");
	    		buildquery.append("medicaid_id IN (");
	    		buildquery.append(planMedicaidIdStr);
	    		buildquery.append(")");
	    		
	    		LOGGER.debug("SQL = " + buildquery.toString());	    				    		
	    		stmt = conn.prepareStatement(buildquery.toString());	    	
			    stmt.setFetchSize(PlanMgmtConstants.FETCHSIZE);	    		
	    		rs = stmt.executeQuery();	    		
	          
	    		while ( rs.next() ) {
					String serviceName = rs.getString("service_name");
					String rsPlanMedicaidId = rs.getString("medicaid_id");
					Map<String, String> benefitAttrib = new HashMap<String, String>();				
	
					benefitAttrib.put("serviceType", (rs.getString("service_type") == null) ? "" : rs.getString("service_type"));
					benefitAttrib.put("serviceAttribute", (rs.getString("service_attr") == null) ? "" : rs.getString("service_attr"));
					
					if(serviceDataList.get(rsPlanMedicaidId) == null){
						serviceDataList.put(rsPlanMedicaidId,new HashMap<String, Map<String, String>>() );
					}
					serviceDataList.get(rsPlanMedicaidId).put(serviceName, benefitAttrib);
	            }    
		    } 
        } catch (Exception e) {
        	LOGGER.error("Got an exception to execute getMedicaidPlanServices()! Exception ==> " , e );           
        }finally{
        	PlanMgmtUtil.closeResultSet(rs);
        	PlanMgmtUtil.closePreparedStatement(stmt);
        	PlanMgmtUtil.closeDBConnection(conn);	
        }			
		
		return serviceDataList;
	}		
}
