package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Issuer;

public interface PMIssuerRepository extends JpaRepository<Issuer, Integer>{
	
	@Query("SELECT issuer.name FROM Issuer issuer  where issuer.id = :id")
	String getIssuerName(@Param("id") Integer id);
	
	@Query("SELECT issuer.onExchangeDisclaimer,issuer.offExchangeDisclaimer FROM Issuer issuer  where issuer.id = :id")
	List<Object[]> getDisclaimerInfo(@Param("id") Integer id);
	
	@Query("SELECT i FROM Issuer i WHERE (i.hiosIssuerId = :hiosIssuerID)")
	Issuer getIssuerByHiosID(@Param("hiosIssuerID") String hiosIssuerID);

	@Query("SELECT i.id FROM Issuer i WHERE (LOWER(i.name) LIKE LOWER(:issuerName) || '%') ")
	List<Integer> getIssuerByName(@Param("issuerName") String issuerName);

	@Query("SELECT i FROM Issuer i WHERE i.id = :id")
	Issuer findById(@Param("id") Integer id);

	@Query("SELECT issuer.id, issuer.companyLegalName FROM Issuer issuer  where issuer.id IN (:issuerIdList) ")
	List<Object[]> getIssuersLegalName(@Param("issuerIdList") List<Integer> issuerIdList);

}
