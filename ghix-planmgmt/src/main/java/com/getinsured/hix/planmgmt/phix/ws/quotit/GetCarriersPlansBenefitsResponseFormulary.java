//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.14 at 11:56:49 AM PST 
//


package com.getinsured.hix.planmgmt.phix.ws.quotit;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GetCarriersPlansBenefits.Response.Formulary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GetCarriersPlansBenefits.Response.Formulary">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FormularyId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FormularyURL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DrugListId" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NumberOfTiers" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DrugTiers" type="{}ArrayOfGetCarriersPlansBenefits.Response.Formulary.DrugTier" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCarriersPlansBenefits.Response.Formulary", propOrder = {
    "formularyId",
    "formularyURL",
    "drugListId",
    "numberOfTiers",
    "drugTiers"
})
public class GetCarriersPlansBenefitsResponseFormulary {

    @XmlElement(name = "FormularyId", required = true, nillable = true)
    protected String formularyId;
    @XmlElementRef(name = "FormularyURL", type = JAXBElement.class, required = false)
    protected JAXBElement<String> formularyURL;
    @XmlElement(name = "DrugListId")
    protected int drugListId;
    @XmlElementRef(name = "NumberOfTiers", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numberOfTiers;
    @XmlElementRef(name = "DrugTiers", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier> drugTiers;

    /**
     * Gets the value of the formularyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFormularyId() {
        return formularyId;
    }

    /**
     * Sets the value of the formularyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFormularyId(String value) {
        this.formularyId = value;
    }

    /**
     * Gets the value of the formularyURL property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getFormularyURL() {
        return formularyURL;
    }

    /**
     * Sets the value of the formularyURL property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setFormularyURL(JAXBElement<String> value) {
        this.formularyURL = value;
    }

    /**
     * Gets the value of the drugListId property.
     * 
     */
    public int getDrugListId() {
        return drugListId;
    }

    /**
     * Sets the value of the drugListId property.
     * 
     */
    public void setDrugListId(int value) {
        this.drugListId = value;
    }

    /**
     * Gets the value of the numberOfTiers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumberOfTiers() {
        return numberOfTiers;
    }

    /**
     * Sets the value of the numberOfTiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumberOfTiers(JAXBElement<String> value) {
        this.numberOfTiers = value;
    }

    /**
     * Gets the value of the drugTiers property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier }{@code >}
     *     
     */
    public JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier> getDrugTiers() {
        return drugTiers;
    }

    /**
     * Sets the value of the drugTiers property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier }{@code >}
     *     
     */
    public void setDrugTiers(JAXBElement<ArrayOfGetCarriersPlansBenefitsResponseFormularyDrugTier> value) {
        this.drugTiers = value;
    }

}
