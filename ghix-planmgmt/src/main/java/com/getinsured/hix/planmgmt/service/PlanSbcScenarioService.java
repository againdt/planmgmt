package com.getinsured.hix.planmgmt.service;

import java.util.List;

import com.getinsured.hix.dto.planmgmt.SbcScenarioDTO;
import com.getinsured.hix.model.PlanSbcScenario;


/**
 * Any "field" you declare in an interface will be a static final field
 *
 */
public interface PlanSbcScenarioService {
	
	SbcScenarioDTO setSbcScenario(PlanSbcScenario planSbcScenario, SbcScenarioDTO sbcScenarioDTO);
	
	List<PlanSbcScenario> getSbcScenarioIdList(List<Integer> sbcScenarioIdList);
}
