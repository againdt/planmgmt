package com.getinsured.hix.planmgmt.querybuilder;

import org.apache.log4j.Logger;

import com.getinsured.hix.model.Plan.PlanInsuranceType;

public class ProviderReportQueryBuilder {
	private static final Logger LOGGER = Logger.getLogger(ProviderReportQueryBuilder.class);
	
	private static String providerPlanNetworkReportSQL;
	
	public static final String getproviderPlanNetworkReportSQL(){
		try{
			if(null != providerPlanNetworkReportSQL){
				return providerPlanNetworkReportSQL;
			}
			
			StringBuilder sqlDesigner = new StringBuilder(2048);
			sqlDesigner.append("SELECT DISTINCT P.ID, P.NAME, I.NAME AS CARRIER_NAME, P.NETWORK_TYPE, P.STATE, P.STATUS, P.ISSUER_VERIFICATION_STATUS, P.EXCHANGE_TYPE, N.NETWORK_NAME, ");
			sqlDesigner.append("  N.NETWORK_ID AS NETWORK_ID, N.NETWORK_URL, P.issuer_plan_number, H.plan_level, I.hios_issuer_id, P.APPLICABLE_YEAR, temp.t2_copay, temp.t2_coins, ");
			sqlDesigner.append("CASE  WHEN temp.t2_copay IS NULL AND temp.t2_coins IS NULL THEN 'NO' WHEN temp.t2_copay = 0 AND temp.t2_coins  = 0 THEN 'MAYBE' ELSE 'YES' END AS Tier_2_present, N.PRODUCT_ID, N.PRODUCT_NAME, N.NETWORK_KEY ");
			sqlDesigner.append("FROM ");
			sqlDesigner.append("PLAN P JOIN ISSUERS I ON P.ISSUER_ID = I.id ");
			sqlDesigner.append("JOIN( ");
			sqlDesigner.append(" SELECT PM.PRODUCT_ID,  N.NETWORK_KEY, N.ID, PPD.PRODUCT_NAME, N.NAME AS NETWORK_NAME, N.NETWORK_ID AS NETWORK_ID, N.NETWORK_URL ");
			sqlDesigner.append("FROM ");
			sqlDesigner.append("NETWORK N LEFT OUTER JOIN PROVIDER_MAPPING PM ON N.NETWORK_KEY = PM.NETWORK_KEY LEFT OUTER JOIN provider_product_domain ppd ON PM.PRODUCT_ID = PPD.PRODUCT_ID ");
			sqlDesigner.append(") N ");
			sqlDesigner.append("ON P.PROVIDER_NETWORK_ID = N.ID ");
			sqlDesigner.append("JOIN plan_health H ");
			sqlDesigner.append("ON P.id = H.plan_id AND P.IS_DELETED = 'N' AND P.INSURANCE_TYPE = 'HEALTH' AND "); 
			sqlDesigner.append("P.APPLICABLE_YEAR IN ((SELECT EXTRACT(YEAR FROM SYSDATE) FROM DUAL), (SELECT EXTRACT(YEAR FROM ADD_MONTHS(SYSDATE,12)) FROM DUAL)) ");
			sqlDesigner.append("LEFT JOIN ");
			sqlDesigner.append("(SELECT PHB.plan_health_id, PHB.network_t2_copay_val AS t2_copay, PHB.network_t2_coinsurance_val AS t2_coins ");
			sqlDesigner.append("FROM ");
			sqlDesigner.append("PLAN_HEALTH_BENEFIT PHB ");
			sqlDesigner.append("WHERE "); 
			sqlDesigner.append("PHB.name = 'SPECIAL_VISIT' ");
			sqlDesigner.append(") temp ");
			sqlDesigner.append("ON H.id = temp.plan_health_id ");
			sqlDesigner.append("ORDER BY P.STATE ");
			
			providerPlanNetworkReportSQL = sqlDesigner.toString();
		}catch(Exception ex){
			LOGGER.error("Exception occured in getproviderPlanNetworkReportSQL : ", ex);
		}				
		return providerPlanNetworkReportSQL;
	}
}
