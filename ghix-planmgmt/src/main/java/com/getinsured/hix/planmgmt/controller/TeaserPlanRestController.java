package com.getinsured.hix.planmgmt.controller;

import java.util.Arrays;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.model.planmgmt.EligibilityRequest;
import com.getinsured.hix.model.planmgmt.EligibilityResponse;
import com.getinsured.hix.model.planmgmt.TeaserPlanRequest;
import com.getinsured.hix.model.planmgmt.TeaserPlanResponse;
import com.getinsured.hix.planmgmt.phix.service.PrivateExchangeService;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;

//@Api(value="Teaser Plan Information controller", description ="Returning teaser plan information based on provided information")
@Controller
@RequestMapping(value = "/teaserPlanRestService")
public class TeaserPlanRestController {

	private static final Logger LOGGER = Logger.getLogger(TeaserPlanRestController.class);

	@Autowired
	private PrivateExchangeService privateExchangeService;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;

//	@ApiOperation(value="/teaserPlan/secondPlan", notes="This API returns the second sliver plan details")
//	@ApiResponse(value="return second silver plan")
	@RequestMapping(value = "/teaserPlan/secondPlan", method = RequestMethod.POST)
	public @ResponseBody TeaserPlanResponse getSecondSilverPlan(@RequestBody TeaserPlanRequest teaserPlanRequest){
		boolean isLowest = false;
		TeaserPlanResponse teaserPlanResponse = new TeaserPlanResponse();
		try{
		return privateExchangeService.getSilverPlan(teaserPlanRequest, isLowest);
		
		}catch(Exception e){
			LOGGER.error("Error in getEligibilityPremium() ",e);
			return teaserPlanResponse;
		}	
	}

//	@ApiOperation(value="/teaserPlan/eligiblityRequest", notes="This API takes members information, zip, county, exchange flow type etc. and retrive sliver and bronz plan availability, number of plans in each category and benchmark permium")
//	@ApiResponse(value="returns silver and bronze plans availability, number of plans in each category, benchmark premium for tobacco and non-tobacco, and Plans Information")
	@RequestMapping(value = "/teaserPlan/eligiblityRequest", method = RequestMethod.POST)
	public  @ResponseBody EligibilityResponse getEligibilityPremium(@RequestBody EligibilityRequest eligibilityRequest)
	{	
		LOGGER.info("Eligibility Calculator: getEligibilityPremium started");
		return getEligibilityPremiumHelper(eligibilityRequest);
	
	}
	
	public EligibilityResponse getEligibilityPremiumHelper( EligibilityRequest eligibilityRequest)
	{	
		LOGGER.info("Eligibility Calculator: getEligibilityPremium started");
		EligibilityResponse eligibilityResponse = new EligibilityResponse();
		String sendAllMetalTier = eligibilityRequest.getSendAllMetalTier();	
		if(null == eligibilityRequest.getMembers() ||
		   null	== eligibilityRequest.getZipCode() ||
		   null == eligibilityRequest.getCounty() ||
		   null == eligibilityRequest.getEffectiveDate()){
			eligibilityResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			eligibilityResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			eligibilityResponse.setErrMsg("Invalid request parameters.");
		}else{
			
			 // if exchange type is null in request then set default exchange type as ON 
	 	 	 if(eligibilityRequest.getExchangeFlowType() == null){
	 	 	     eligibilityRequest.setExchangeFlowType("ON");
	 	 	 }
	 	 	
	 	     // if exchange type is OFF then set CS0 as cost sharing value 
	 	 	 if(eligibilityRequest.getExchangeFlowType().equalsIgnoreCase(com.getinsured.hix.model.Plan.EXCHANGE_TYPE.OFF.toString())){
 	 	 	 	 eligibilityRequest.setCostSharingVariation("CS0");
 	 	 	 }else if(eligibilityRequest.getCostSharingVariation() == null || eligibilityRequest.getCostSharingVariation().equalsIgnoreCase("null")){
 	 	 		 // in certain case cost sharing value comes as NULL. in that case consider CS1 as cost sharing
 	 	 		 eligibilityRequest.setCostSharingVariation("CS1");
	 	 	 }
	 	 	 
			try{
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("sendAllMetalTier ========" + SecurityUtil.sanitizeForLogging(sendAllMetalTier));
					LOGGER.debug("COST SHARING = " +  SecurityUtil.sanitizeForLogging(eligibilityRequest.getCostSharingVariation()));
				}	
				if(eligibilityRequest.getSendOnlySilverPlan()){
					eligibilityResponse = privateExchangeService.getEligibilityPremium(eligibilityRequest);
				}else{
					eligibilityResponse = privateExchangeService.getEligibilityPremiumAllForTiers(eligibilityRequest, sendAllMetalTier);					
				}
				
			}catch(Exception e){
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch silver and bronze plans availability.",e);
				LOGGER.error("Error in getEligibilityPremium() ",e);
				eligibilityResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_ELIGIBILITY_PREMIUM.code);
				eligibilityResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				eligibilityResponse.setErrMsg(Arrays.toString(e.getStackTrace()));
				return eligibilityResponse;
			}
			//LOGGER.info("Eligibility Calculator: EligibilityResponse :"+SecurityUtil.sanitizeForLogging(eligibilityResponse.toString()));
		}
	
		return eligibilityResponse;
	
	}
	
}
