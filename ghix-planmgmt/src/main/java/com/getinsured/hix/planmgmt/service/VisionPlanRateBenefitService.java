package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.VisionPlan;

public interface VisionPlanRateBenefitService {

	List<VisionPlan> getVisionPlans(List<Map<String, String>> memberList, String effectiveDate, String tenant);
	
	Map<String, Map<String, Map<String, String>>> getVisionPlanBenefits(String planVisionIdStr, String filterBenefits);
	
	List<VisionPlan> getLowestVisionPlan(List<Map<String, String>> memberList, String effectiveDate, String tenant);
	
}

