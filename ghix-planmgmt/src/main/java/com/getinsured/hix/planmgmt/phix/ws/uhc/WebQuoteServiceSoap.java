/**
 * WebQuoteServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.getinsured.hix.planmgmt.phix.ws.uhc;

public interface WebQuoteServiceSoap extends java.rmi.Remote {
    public com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteReturn getQuote(com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteRequest value) throws java.rmi.RemoteException;
}
