package com.getinsured.hix.planmgmt.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.platform.feature.GiFeature;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints.AHBXEndPoints;

@Service("SyncPlanWithAHBXService")
public class SyncPlanWithAHBXServiceImpl implements SyncPlanWithAHBXService{

		private static final Logger LOGGER = Logger.getLogger(SyncPlanWithAHBXServiceImpl.class);
		
		@Autowired
		private GhixRestTemplate ghixRestTemplate;
		
		// call plansync REST API of AHBX module to invoke IND04
		@Override
		@GiFeature("ahbx.planmgmt.ind04.enabled")
		public void syncPlanWithAHBX(List<Map<String, String>> listOfPlans){
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("==== Inside syncPlanWithAHBX() ====");
				LOGGER.info(" # of plan to sync : " +listOfPlans.size());
			}
			
			if(listOfPlans.size() > 0){
				try{
					List<Map<String, String>> processedListOfPlans = processPlanDataForAHBXCall(listOfPlans); // process plan data before AHBX call
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("Call AHBXEndPoints.SYNC_PLAN_DATA =========" + AHBXEndPoints.SYNC_PLAN_DATA);
					}	
					String apiResponse = null; 
					apiResponse = ghixRestTemplate.postForObject(AHBXEndPoints.SYNC_PLAN_DATA, processedListOfPlans, String.class);
						
					if(apiResponse != null && !"".equals(apiResponse)){ // if response is not empty
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug( "Plan sync response status " + apiResponse.toString());
						}	
					}
				}	
				catch(Exception ex){
					LOGGER.error("Fail to call AHBXEndPoints.SYNC_PLAN_DATA ", ex);
				}
			}
		}
		
		
		// call processPlanDataForAHBXCall() to process request data as per IND04 
		private List<Map<String, String>> processPlanDataForAHBXCall(List<Map<String, String>> planDataList){
			List<Map<String, String>> listOfPlans = new ArrayList<Map<String, String>>();
			Map<String, String> requestData = null;
			String planLevel = null;
			String planMarket = null;
			String planType = null;
			
			for(Map<String, String> planData : planDataList){					
				planLevel = planData.get(PlanMgmtConstants.AHBX_PLAN_LEVEL);
				planLevel = planLevel.substring(0, 1).toUpperCase()	+ planLevel.substring(1).toLowerCase(); // example : <planLevel>Silver</planLevel>
				planMarket = planData.get(PlanMgmtConstants.AHBX_PLAN_MARKET);
				planMarket = (planMarket.equalsIgnoreCase(Plan.PlanMarket.INDIVIDUAL.toString())) ? "Individual" : "Group";
				planType = planData.get(PlanMgmtConstants.AHBX_PLAN_TYPE);
				planType = planType.substring(0, 1).toUpperCase() + planType.substring(1).toLowerCase(); 
				
				requestData = new HashMap<String, String>();
				requestData.put(PlanMgmtConstants.AHBX_PLAN_ID, planData.get(PlanMgmtConstants.AHBX_PLAN_ID));
				requestData.put(PlanMgmtConstants.AHBX_ISSUER_ID, planData.get(PlanMgmtConstants.AHBX_ISSUER_ID));
				requestData.put(PlanMgmtConstants.AHBX_PLAN_LEVEL, planLevel);
				requestData.put(PlanMgmtConstants.AHBX_PLAN_MARKET, planMarket.toUpperCase());
				requestData.put(PlanMgmtConstants.AHBX_PLAN_NAME, planData.get(PlanMgmtConstants.AHBX_PLAN_NAME));
				requestData.put(PlanMgmtConstants.AHBX_PLAN_NETWORK_TYPE, planData.get(PlanMgmtConstants.AHBX_PLAN_NETWORK_TYPE).toUpperCase());
				requestData.put(PlanMgmtConstants.AHBX_PLAN_RECORD_INDICATOR, planData.get(PlanMgmtConstants.AHBX_PLAN_RECORD_INDICATOR));
				requestData.put(PlanMgmtConstants.AHBX_PLAN_TYPE, (planType.equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())) ? "Medical" : planType);
				requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_START_DATE, planData.get(PlanMgmtConstants.AHBX_PLAN_EFFCT_START_DATE));
				requestData.put(PlanMgmtConstants.AHBX_PLAN_EFFCT_END_DATE, planData.get(PlanMgmtConstants.AHBX_PLAN_EFFCT_END_DATE));
				requestData.put(PlanMgmtConstants.AHBX_PLAN_HIOS_ID, planData.get(PlanMgmtConstants.AHBX_PLAN_HIOS_ID));
				requestData.put(PlanMgmtConstants.AHBX_PLAN_STATUS, planData.get(PlanMgmtConstants.AHBX_PLAN_STATUS));
				requestData.put(PlanMgmtConstants.AHBX_PLAN_YEAR, planData.get(PlanMgmtConstants.AHBX_PLAN_YEAR));
				listOfPlans.add(requestData);
			}
			return listOfPlans;
		}
		
}
