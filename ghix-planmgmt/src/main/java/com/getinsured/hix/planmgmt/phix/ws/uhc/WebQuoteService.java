/**
 * WebQuoteService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.getinsured.hix.planmgmt.phix.ws.uhc;

public interface WebQuoteService extends javax.xml.rpc.Service {
    public java.lang.String getWebQuoteServiceSoapAddress();

    public com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteServiceSoap getWebQuoteServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteServiceSoap getWebQuoteServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
