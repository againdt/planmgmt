/**
 * AmePlanRestController.java
 * @author santanu
 * @version 1.0
 * @since Aug 29, 2014 
 * This REST controller would be use to handle AME plan request and response
 */

package com.getinsured.hix.planmgmt.controller;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.AmePlan;
import com.getinsured.hix.dto.planmgmt.AmePlanRequestDTO;
import com.getinsured.hix.dto.planmgmt.AmePlanResponseDTO;
import com.getinsured.hix.planmgmt.service.AMEPlanBenefitService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
//import com.wordnik.swagger.annotations.ApiOperation;
//import com.wordnik.swagger.annotations.ApiResponse;

//@Api(value="AME Plans rate and benefit information", description ="Returning AME Plans benefit and rate information depends on zip, county, monthly income and census information")
@Controller
@RequestMapping(value = "/ameplan")
public class AmePlanRestController {
	private static final Logger LOGGER = Logger.getLogger(AmePlanRestController.class);
	
	@Autowired
	private AMEPlanBenefitService amePlanBenefitService;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
		
	/* test controller */
//	@ApiOperation(value="welcome", notes="Welcome to AME Plan rest controller")
//	@ApiResponse(value="Welcome to STM Plan rest controller, invoke AMEPlanRestController")
//	@RequestMapping(value = "/welcome", method = RequestMethod.GET) // for testing purpose
//	@ResponseBody public String welcome()
//	{
//		LOGGER.info("Welcome to GHIX-Planmgmt module, invoke AmePlanRestController()");		
//		return "Welcome to GHIX-Planmgmt module, invoke AmePlanRestController";
//	}
	
	
	/* return AME plans */
//	@ApiOperation(value="getameplans", notes="This operation retrieves AME Plans using census information and effective date")
//	@ApiResponse(value="Returning AME Plan Information")
	@RequestMapping(value = "/getameplans", method = RequestMethod.POST) // for testing purpose
	@Produces("application/json")
	@ResponseBody public AmePlanResponseDTO getAmePlans(@RequestBody AmePlanRequestDTO amePlanRequestDTO )
	{
		LOGGER.info("inside getAmePlans()");	
		if(LOGGER.isDebugEnabled()){
			StringBuilder buildStr = new StringBuilder(512);
			buildStr.append("Request data ===>");			
			buildStr.append(" Effective date : ");
			buildStr.append(amePlanRequestDTO.getEffectiveDate());
			buildStr.append(", memberList : ");
			buildStr.append(amePlanRequestDTO.getMemberList());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));	
		}
		
		AmePlanResponseDTO amePlanResponseDTO = new AmePlanResponseDTO();
		
		if(null == amePlanRequestDTO.getEffectiveDate() || amePlanRequestDTO.getEffectiveDate().trim().isEmpty()				
			|| null == amePlanRequestDTO.getMemberList() || amePlanRequestDTO.getMemberList().isEmpty()) {
			amePlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			amePlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			amePlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		
		} else {
			List<Map<String, String>> memberList = (List<Map<String, String>>)amePlanRequestDTO.getMemberList();			
			String effectiveDate = amePlanRequestDTO.getEffectiveDate().trim();
			String tenantCode = PlanMgmtConstants.TENANT_GINS;
			if(!StringUtils.isBlank(amePlanRequestDTO.getTenantCode())) {
				tenantCode = amePlanRequestDTO.getTenantCode().trim();
			}
			
			try{		
				List<AmePlan> amePlanList = amePlanBenefitService.getAmePlanDetails(effectiveDate, memberList, tenantCode);								
	
				amePlanResponseDTO.setAmePlanList(amePlanList);	
				amePlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				amePlanResponseDTO.endResponse();
			}catch(Exception ex) {
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute getAmePlans() ",ex);
				LOGGER.error("Failed to execute getAmePlans(). Exception:", ex);					
				amePlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				amePlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				amePlanResponseDTO.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}		
			
		}
		
		return amePlanResponseDTO;
	}
	
	
	/* return AME plans */
//	@ApiOperation(value="getmaxbenefitameplan", notes="This operation retrieves Max benefits of AME Plans")
//	@ApiResponse(value="Returning Benefits of AME Plans")
	@RequestMapping(value = "/getmaxbenefitameplan", method = RequestMethod.POST) // for testing purpose
	@Produces("application/json")
	@ResponseBody public AmePlanResponseDTO getMaxBenefitAmePlan(@RequestBody AmePlanRequestDTO amePlanRequestDTO )
	{
		LOGGER.info("inside getMaxBenefitAmePlan()");	
		if(LOGGER.isDebugEnabled()){
			StringBuilder buildStr = new StringBuilder(512);
			buildStr.append("Request data ===>");			
			buildStr.append(" Effective date : ");
			buildStr.append(amePlanRequestDTO.getEffectiveDate());
			buildStr.append(", memberList : ");
			buildStr.append(amePlanRequestDTO.getMemberList());
			LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));	
		}
		
		AmePlanResponseDTO amePlanResponseDTO = new AmePlanResponseDTO();
		
		if(null == amePlanRequestDTO.getEffectiveDate() || amePlanRequestDTO.getEffectiveDate().trim().isEmpty()				
			|| null == amePlanRequestDTO.getMemberList() || amePlanRequestDTO.getMemberList().isEmpty()) {
			amePlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
			amePlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			amePlanResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		
		} else {
			List<Map<String, String>> memberList = (List<Map<String, String>>)amePlanRequestDTO.getMemberList();			
			String effectiveDate = amePlanRequestDTO.getEffectiveDate().trim();
			String tenantCode = PlanMgmtConstants.TENANT_GINS;
			if(!StringUtils.isBlank(amePlanRequestDTO.getTenantCode())) {
				tenantCode = amePlanRequestDTO.getTenantCode().trim();
			}			
			try{		
				List<AmePlan> amePlanList = amePlanBenefitService.getMaxBenefitAmePlan(effectiveDate, memberList, tenantCode);								
	
				amePlanResponseDTO.setAmePlanList(amePlanList);	
				amePlanResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
				amePlanResponseDTO.endResponse();
			}catch(Exception ex) {
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to execute getMaxBenefitAmePlan() ",ex);
				LOGGER.error("Failed to execute getMaxBenefitAmePlan(). Exception:", ex);					
				amePlanResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				amePlanResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				amePlanResponseDTO.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}		
			
		}
		
		return amePlanResponseDTO;
	}
	
}
