package com.getinsured.hix.planmgmt.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.CrossSellPlan;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
//import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;


@Service("crossSellService")
@Repository
@Transactional
@DependsOn("dynamicPropertiesUtil")
public class CrossSellServiceImpl implements CrossSellService {
	private static final Logger LOGGER = Logger.getLogger(CrossSellServiceImpl.class);
	@PersistenceUnit 
	private EntityManagerFactory emf;
	@Autowired
	private QuotingBusinessLogic quotingBusinessLogic; 
	@Autowired
	private LookupService lookupService;
	@Autowired
	private PlanCostAndBenefitsService planCostAndBenefitsService;
	@Autowired 
	private VisionPlanRateBenefitService visionPlanRateBenefitService;
	@Autowired 
	private AMEPlanBenefitService amePlanBenefitService;
	//@Autowired
	//private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	@Value(PlanMgmtConstants.CONFIG_APP_URL)
	private String appUrl;
	
	@Value(PlanMgmtConstants.CONFIG_DB_TYPE)
	private String configuredDB;
	
//	private static final String DOCUMENT_URL = "download/document?documentId="; // document download url
	private static final String AGE = "age";
	
	
	@Override
	@Transactional(readOnly = true)
	public Float getLowestDentalPlanPremium(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenant) {
		Float premium = Float.parseFloat("0");
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("-------- Inside getLowestDentalPlanPremium --------");
			}
			
			List<Map<String, String>> processedMemberData =  quotingBusinessLogic.processMemberData(memberList, Plan.PlanInsuranceType.DENTAL.toString()); // process member data
			String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedMemberData, Plan.PlanInsuranceType.DENTAL.toString());	// get household type		
			String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
			String familyTierLookupCode = lookupService.getLookupValueCode(PlanMgmtConstants.FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
			String stateName = quotingBusinessLogic.getApplicantStateCode(processedMemberData);			
			String IHCfamilyTierLookupCode = PlanMgmtConstants.EMPTY_STRING;
			String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);

			int applicantAge = quotingBusinessLogic.getApplicantAge(processedMemberData);
		 	boolean doQuoteIssuerIHC = quotingBusinessLogic.isAgeAllowForIHCQuoting(applicantAge);
			
			// compute family tier look up value for carrier IHC, when exchange is PHIX and quoting for OFF exchange plans 
			// HIX-53639
			if(exchangeType.equalsIgnoreCase(Plan.EXCHANGE_TYPE.OFF.toString()) && currentExchangeType.equals(GhixConstants.PHIX)){
				IHCfamilyTierLookupCode = quotingBusinessLogic.getIHCFamilyTierLookupCode(memberList);
			}

			StringBuilder buildquery = new StringBuilder();
			ArrayList<String> paramList = new ArrayList<String>();
			String applicantZip = null;
			String applicantCounty = null;
			
			buildquery.append("SELECT SUM(pre) AS total_premium, rate_option, ageandfamily_rate ");			
			buildquery.append(" FROM ");
			buildquery.append("plan p, pm_tenant_plan tp, tenant t,");
			
			buildquery.append(" ( ");

			for (int i = 0; i < processedMemberData.size(); i++) {
				buildquery.append("( SELECT prate.rate AS pre,  pdental.plan_id,  1 as memberctr, prate.rate_option AS rate_option, pdental.is_community_rate AS ageandfamily_rate ");
				buildquery.append("FROM ");
				buildquery.append(" plan_dental pdental, pm_service_area psarea, pm_rating_area prarea, pm_plan_rate prate, plan p2, pm_zip_county_rating_area pzcrarea, issuers i2 ");
				buildquery.append("WHERE ");
				buildquery.append("p2.id = pdental.plan_id AND p2.issuer_id = i2.id ");
				buildquery.append("AND p2.service_area_id = psarea.service_area_id ");
				buildquery.append("AND p2.id = prate.plan_id ");
				buildquery.append("AND p2.is_deleted = 'N' ");
				if(Plan.EXCHANGE_TYPE.ON.toString().equalsIgnoreCase(exchangeType)){
					buildquery.append("AND pdental.cost_sharing='CS1' ");
				}else{
					buildquery.append("AND pdental.cost_sharing='CS0' ");
				}
				buildquery.append("AND prarea.id = prate.rating_area_id ");
				buildquery.append("AND pzcrarea.rating_area_id = prate.rating_area_id ");
				
				buildquery.append(quotingBusinessLogic.buildRateLogicForDentalQuoting("prate", "i2", stateName, houseHoldType, familyTierLookupCode, processedMemberData.get(i).get(AGE), processedMemberData.get(i).get(PlanMgmtConstants.TOBACCO), effectiveDate, exchangeType, doQuoteIssuerIHC, IHCfamilyTierLookupCode, paramList));
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND (pzcrarea.zip = TO_NUMBER(trim(:param_" + paramList.size() + ")) OR pzcrarea.zip IS NULL) ");
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.ZIP));
				}else{
					buildquery.append("AND (pzcrarea.zip = :param_" + paramList.size() + " OR pzcrarea.zip IS NULL) ");
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.ZIP));
				}

				if (null == applicantZip) {
					applicantZip = processedMemberData.get(i).get(PlanMgmtConstants.ZIP);
				}
				
				buildquery.append("AND pzcrarea.county_fips = :param_" + paramList.size() + " ");
				paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.COUNTYCODE));
				buildquery.append("AND psarea.fips = :param_" + paramList.size() + " ");
				paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.COUNTYCODE));

				if (null == applicantCounty) {
					applicantCounty = processedMemberData.get(i).get(PlanMgmtConstants.COUNTYCODE);
				}

				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND psarea.zip = TO_NUMBER(trim(:param_" + paramList.size() + ")) ");
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.ZIP));
				}else{
					buildquery.append("AND psarea.zip = :param_" + paramList.size() + " ");
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.ZIP));
				}
				
				buildquery.append("AND psarea.is_deleted = 'N' ");
				buildquery.append(" ) ");

				if (i + 1 != processedMemberData.size()) {
					buildquery.append(" UNION ALL ");
				}
			}

			buildquery.append(") temp ");
			buildquery.append(" WHERE ");
			buildquery.append(" p.id = temp.plan_id ");
			buildquery.append(" AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date ");
			paramList.add(effectiveDate);
			
			buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
			buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic("p", effectiveDate, PlanMgmtConstants.NO, applicantZip, applicantCounty, null, paramList));
			buildquery.append(" AND p.market = '");
			buildquery.append(Plan.PlanMarket.INDIVIDUAL.toString());
			buildquery.append("' AND p.insurance_type='DENTAL' AND p.is_deleted= 'N' AND p.exchange_type = UPPER(:param_" + paramList.size() +") ");
			paramList.add(exchangeType);
			
			buildquery.append(" AND p.id = tp.plan_id");
			buildquery.append(" AND tp.tenant_id = t.id");
			buildquery.append(" AND t.code = UPPER(:param_" + paramList.size() +") ");
			paramList.add(tenant);
			  
			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic("p", houseHoldType));					
			buildquery.append(" GROUP BY  p.id, rate_option, ageandfamily_rate ");
			buildquery.append("HAVING sum(memberctr) = " + processedMemberData.size());
			buildquery.append(" ORDER BY total_premium ASC ");
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL == " + buildquery.toString());
			}
			
			
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			List<?> rsList = query.getResultList();
			
			Iterator<?> rsIterator = rsList.iterator();
			DecimalFormat f = new DecimalFormat("##.00");
			
			List<Float> premiumArrayList = new ArrayList<Float>();
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				
				String rateOption = objArray[PlanMgmtConstants.ONE].toString();
				Float householdPremium = Float.parseFloat(objArray[PlanMgmtConstants.ZERO].toString());
				// if rate option is Family, then premium would be on family level, don't consider each quoted member's premium
				if (rateOption.equalsIgnoreCase(PlanMgmtConstants.FAMILY_RATE_OPTION)) {
					householdPremium = Float.parseFloat(f.format((householdPremium / processedMemberData.size())));
				}
				//HIX-62493,
				// if plan have age band rate and family rate both  
				if((objArray[PlanMgmtConstants.TWO] != null) && objArray[PlanMgmtConstants.TWO].toString().equalsIgnoreCase(PlanMgmtConstants.Y)  ){
					//	if number of quoted member is more than 1 then add those plans having family rate
					if (rateOption.equalsIgnoreCase(PlanMgmtConstants.FAMILY_RATE_OPTION) && processedMemberData.size() > 1) {
						premiumArrayList.add(householdPremium);
					}
					//	if number of quoted member is 1 then add those plans having age band rate
					if (rateOption.toString().equalsIgnoreCase(PlanMgmtConstants.AGE_RATE_OPTION) && processedMemberData.size() == 1) {
						premiumArrayList.add(householdPremium);
					}
				}else{
					premiumArrayList.add(householdPremium);
				}
				
			}	
			
			if(premiumArrayList.size() > PlanMgmtConstants.ZERO){
				// sort premium			
				Collections.sort(premiumArrayList);	
				premium =  premiumArrayList.get(0);
			}
			
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getLowestDentalPlanPremium() ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getLowestDentalPlanPremium() ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return premium;
	}
	
	
	@SuppressWarnings("rawtypes")
	@Override
	@Transactional(readOnly = true)
	public List<CrossSellPlan> getDentalPlans(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenant) {
		List<CrossSellPlan> planList = new ArrayList<CrossSellPlan>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("-------- Inside getDentalPlans --------");
			}
			
			List<Map<String, String>> processedMemberData =  quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.DENTAL.toString());
			String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedMemberData, Plan.PlanInsuranceType.DENTAL.toString());
			String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
			String familyTierLookupCode = lookupService.getLookupValueCode(PlanMgmtConstants.FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
			String stateName = quotingBusinessLogic.getApplicantStateCode(processedMemberData);
			String IHCfamilyTierLookupCode = PlanMgmtConstants.EMPTY_STRING;
			String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
			int applicantAge = quotingBusinessLogic.getApplicantAge(processedMemberData);
		 	boolean doQuoteIssuerIHC = quotingBusinessLogic.isAgeAllowForIHCQuoting(applicantAge);
			
			// compute family tier look up value for carrier IHC, when exchange is PHIX and quoting for OFF exchange plans 
			// HIX-53639
			if(exchangeType.equalsIgnoreCase(Plan.EXCHANGE_TYPE.OFF.toString()) && currentExchangeType.equals(GhixConstants.PHIX)){
				IHCfamilyTierLookupCode = quotingBusinessLogic.getIHCFamilyTierLookupCode(memberList);
			}

			StringBuilder buildquery = new StringBuilder();
			ArrayList<String> paramList = new ArrayList<String>();
			String applicantZip = null;
			String applicantCounty = null;
			
			buildquery.append("SELECT p.id AS plan_id, p.name AS plan_name, SUM(pre) AS total_premium, i.logo_url as issuer_logo, pdental_id, rate_option, ageandfamily_rate, i.hios_issuer_id as hios_issuer_id ");		
			buildquery.append(" FROM ");
			buildquery.append("plan p, issuers i, pm_tenant_plan tp, tenant t,");
			buildquery.append(" ( ");

			for (int i = 0; i < processedMemberData.size(); i++) {
				buildquery.append("( SELECT prate.rate AS pre, pdental.plan_id, pdental.id AS pdental_id, 1 as memberctr, prate.rate_option AS rate_option, pdental.is_community_rate AS ageandfamily_rate ");
				buildquery.append("FROM ");
				buildquery.append(" plan_dental pdental, pm_service_area psarea, pm_rating_area prarea, pm_plan_rate prate, plan p2, pm_zip_county_rating_area pzcrarea, issuers i2 ");
				buildquery.append("WHERE ");
				buildquery.append("p2.id = pdental.plan_id AND p2.issuer_id = i2.id ");
				buildquery.append("AND p2.service_area_id = psarea.service_area_id ");
				buildquery.append("AND p2.id = prate.plan_id ");
				buildquery.append("AND p2.is_deleted = 'N' ");
				buildquery.append("AND psarea.is_deleted = 'N' ");
				buildquery.append("AND prarea.id = prate.rating_area_id ");
				buildquery.append("AND pzcrarea.rating_area_id = prate.rating_area_id ");
				buildquery.append(quotingBusinessLogic.buildRateLogicForDentalQuoting("prate", "i2", stateName, houseHoldType, familyTierLookupCode, processedMemberData.get(i).get(AGE), processedMemberData.get(i).get(PlanMgmtConstants.TOBACCO), effectiveDate, exchangeType, doQuoteIssuerIHC, IHCfamilyTierLookupCode, paramList));
				buildquery.append("AND psarea.fips = :param_" + paramList.size() + " ");
				paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.COUNTYCODE));
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND psarea.zip = TO_NUMBER(trim(:param_" + paramList.size() + ")) ");
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.ZIP));
					buildquery.append("AND (pzcrarea.zip = TO_NUMBER(trim(:param_" + paramList.size() + ")) OR pzcrarea.zip IS NULL) ");
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.ZIP));
				}else{
					buildquery.append("AND psarea.zip = :param_" + paramList.size());
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.ZIP));
					buildquery.append(" AND (pzcrarea.zip = :param_" + paramList.size() + " OR pzcrarea.zip IS NULL) ");
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.ZIP));
				}

				if (null == applicantZip) {
					applicantZip = processedMemberData.get(i).get(PlanMgmtConstants.ZIP);
				}

				buildquery.append("AND pzcrarea.county_fips = :param_" + paramList.size() + " ");
				paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.COUNTYCODE));
				buildquery.append(" ) ");

				if (null == applicantCounty) {
					applicantCounty = processedMemberData.get(i).get(PlanMgmtConstants.COUNTYCODE);
				}

				if (i + 1 != processedMemberData.size()) {
					buildquery.append(" UNION ALL ");
				}
			}

			buildquery.append(") temp ");
			buildquery.append(" WHERE ");
			buildquery.append(" p.id = temp.plan_id ");
			buildquery.append(" AND p.issuer_id = i.id ");
			buildquery.append(" AND TO_DATE (:param_" + paramList.size()	+ ", 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date ");
			paramList.add(effectiveDate);
			
			buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
			buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic("p", effectiveDate, PlanMgmtConstants.NO, applicantZip, applicantCounty, null, paramList));
			buildquery.append(" AND p.market = '");
			buildquery.append(Plan.PlanMarket.INDIVIDUAL.toString());
			buildquery.append("' AND p.insurance_type='DENTAL' ");
			
			buildquery.append(" AND p.exchange_type = UPPER(:param_" + paramList.size() +") "); 
			paramList.add(exchangeType);
			
			buildquery.append(" AND p.id = tp.plan_id");
			buildquery.append(" AND tp.tenant_id = t.id");
			buildquery.append(" AND t.code = UPPER(:param_" + paramList.size() +") ");
			paramList.add(tenant);
			
			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic("p", houseHoldType));				
			buildquery.append(" GROUP BY p.id, p.name, i.logo_url, pdental_id, rate_option, ageandfamily_rate, hios_issuer_id ");
			buildquery.append("HAVING sum(memberctr) = " + processedMemberData.size());

			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL == " + buildquery.toString());
			}
			
			
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			
			List rsList = query.getResultList();
			
			Iterator rsIterator = rsList.iterator();
			Iterator rsIterator2 = rsList.iterator();
			DecimalFormat f = new DecimalFormat("##.00");
			List<Integer> planDentalIdList = new ArrayList<Integer>();
			Object[] objArray = null;
			CrossSellPlan crossSellPlan =  null;
			String rateOption = null;
			Float householdPremium = null;
			
			while (rsIterator.hasNext()) {		
				objArray = (Object[]) rsIterator.next();
				crossSellPlan = new CrossSellPlan();

				rateOption = objArray[PlanMgmtConstants.FIVE].toString();
				householdPremium = Float.parseFloat(objArray[PlanMgmtConstants.TWO].toString());
				// if rate option is Family, then premium would be on family level, don't consider each quoted member's premium
				if (rateOption.equalsIgnoreCase(PlanMgmtConstants.FAMILY_RATE_OPTION)) {
					householdPremium = (householdPremium / processedMemberData.size());
				}
				String logoURL = null;
				if(null != objArray[PlanMgmtConstants.THREE]) {
					logoURL = objArray[PlanMgmtConstants.THREE].toString();
				}
				
				crossSellPlan.setId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
				crossSellPlan.setName(objArray[PlanMgmtConstants.ONE].toString());
				crossSellPlan.setPremium(Float.parseFloat(f.format(householdPremium)));
				crossSellPlan.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, objArray[PlanMgmtConstants.SEVEN].toString(), null, appUrl));
				// concatenating plan_dental_ids to generate a single query
				
				BigDecimal planDentalId = (BigDecimal)objArray[PlanMgmtConstants.FOUR];
				planDentalIdList.add(planDentalId.intValue());
				//planDentalIdList.add((Integer)objArray[PlanMgmtConstants.FOUR]);
				
				//HIX-62493,
				// if plan have age band rate and family rate both  
				if((objArray[PlanMgmtConstants.SIX] != null) && objArray[PlanMgmtConstants.SIX].toString().equalsIgnoreCase("Y")  ){
					//	if number of quoted member is more than 1 then add those plans having family rate
					if (rateOption.equalsIgnoreCase(PlanMgmtConstants.FAMILY_RATE_OPTION) && processedMemberData.size() > 1) {
						planList.add(crossSellPlan);
					}
					//	if number of quoted member is 1 then add those plans having age band rate
					if (rateOption.equalsIgnoreCase(PlanMgmtConstants.AGE_RATE_OPTION) && processedMemberData.size() == 1) {
						planList.add(crossSellPlan);
					}
				}else{
					planList.add(crossSellPlan);
				}
				
				crossSellPlan = null;
			}	
			
			objArray = null;
			Object[] objArray2 = null;
			Integer planDentalId = null;
			Integer planId = null;
			
			// fire single query to pull plan benefits and costs
			if (!(planDentalIdList.isEmpty())){
				Map<Integer, Map<String, Map<String, String>>> planCosts = planCostAndBenefitsService.getOptimizedDentalPlanCosts(planDentalIdList, true);
				while (rsIterator2.hasNext()) {
					objArray2 = (Object[]) rsIterator2.next();
					planDentalId = Integer.parseInt(objArray2[PlanMgmtConstants.FOUR].toString());
					planId = Integer.parseInt(objArray2[PlanMgmtConstants.ZERO].toString());	
					
					for (CrossSellPlan planObj : planList) {							
						if (planId.equals(planObj.getId()) && planCosts.containsKey(planDentalId)) {
							Map<String, Map<String,String>> costMap = planCosts.get(planDentalId);								
							planObj.setPlanCosts(costMap);
						}
					}
				}	
			}
			
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getDentalPlans() ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getDentalPlans() ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return planList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<CrossSellPlan> getAMEPlans(List<Map<String, String>> memberList, String effectiveDate, String exchangeType,String tenant) {
		List<CrossSellPlan> planList = new ArrayList<CrossSellPlan>();	

		// HIX-88279 :: If all the members of the household are below 19 then don't quote AME plans
		if(PlanMgmtUtil.isAllFamilyMembersAbove18years(memberList)){	
		
			List<Map<String, String>> processedMemberData =  quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.AME.toString()); // process member data				
			String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
			String familyTierLookupCode = lookupService.getLookupValueCode(PlanMgmtConstants.FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
			String stateName = quotingBusinessLogic.getApplicantStateCode(processedMemberData); // get applicant sate name
			boolean doQuoteIssuerIHC  = true;
			
			if(!processedMemberData.isEmpty() ){
				for(Map<String, String> applicantData: processedMemberData)
				{
					if(applicantData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.SELF) || applicantData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.MEMBER)){					
						int applicantAge = Integer.parseInt(applicantData.get(PlanMgmtConstants.AGE));
						doQuoteIssuerIHC = quotingBusinessLogic.isAgeAllowForIHCQuoting(applicantAge);					
						break;
					}				
				}					
			}
			String relation = PlanMgmtConstants.EMPTY_STRING;
			List<Map<String,Object>> outDataList = amePlanBenefitService.getAMEAndCrosssellHelper(processedMemberData, effectiveDate, relation, familyTierLookupCode, doQuoteIssuerIHC, stateName, tenant);
			try{
				for(Map<String,Object> crossSellMap : outDataList){
					CrossSellPlan crossSellPlan = new CrossSellPlan();
					crossSellPlan.setId((Integer)crossSellMap.get(PlanMgmtConstants.PLANID));
					crossSellPlan.setName((String)crossSellMap.get(PlanMgmtConstants.PLAN_NAME));
					crossSellPlan.setIssuerLogo((String)crossSellMap.get(PlanMgmtConstants.COMPANY_LOGO));
					crossSellPlan.setPlanCosts((Map<String, Map<String, String>>)crossSellMap.get(PlanMgmtConstants.PLAN_COST_MAP));
					crossSellPlan.setPremium((Float)crossSellMap.get(PlanMgmtConstants.TOTALPREMIUM));
					planList.add(crossSellPlan);
				}
			} catch (Exception ex) {
				LOGGER.error("Exception occured in getAMEPlans() ", ex);
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getAMEPlans() ", ex);
			}
		
		}
		
		return planList;		
	}


	@Override
	@Transactional(readOnly = true)
	public BigDecimal getLowestAMEPlanPremium(List<Map<String, String>> memberList, String effectiveDate, String exchangeTypeString, String tenant) {
		BigDecimal premium = BigDecimal.ZERO;
		Float total_premium = 0.0F;
		DecimalFormat df= new DecimalFormat("#.##");
		EntityManager entityManager = null;
		
		// HIX-88279 :: If all the members of the household are below 19 then don't quote AME plans
		if(PlanMgmtUtil.isAllFamilyMembersAbove18years(memberList)){
			
			try{
				entityManager = emf.createEntityManager();
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("-------- Inside getLowestAMEPlanPremium --------");
				}
				
				List<Map<String, String>> processedMemberData =  quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.AME.toString());
				String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
				String familyTierLookupCode = lookupService.getLookupValueCode(PlanMgmtConstants.FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
				String stateName = quotingBusinessLogic.getApplicantStateCode(processedMemberData);
				List<Float> premiumArrayList = new ArrayList<Float>();
				boolean doQuoteIssuerIHC  = true;
				
				if(!processedMemberData.isEmpty() ){
					for(Map<String, String> applicantData: processedMemberData)
					{
						if(applicantData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.SELF) || applicantData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.MEMBER)){					
							int applicantAge = Integer.parseInt(applicantData.get(PlanMgmtConstants.AGE));
							doQuoteIssuerIHC = quotingBusinessLogic.isAgeAllowForIHCQuoting(applicantAge);					
							break;
						}				
					}					
				}
					
				StringBuilder buildQuery = new StringBuilder();
				ArrayList<String> paramList = new ArrayList<String>();
				int paramCount = 0;
				
				String relation = PlanMgmtConstants.EMPTY_STRING;
				buildQuery.append("SELECT p.id as PLAN_ID, sum(pre) AS total_premium, rate_option, I.hios_issuer_id ");
				buildQuery.append("FROM ");
				buildQuery.append("PLAN P, ISSUERS I, PM_TENANT_PLAN tp, TENANT t,");
				buildQuery.append(" ( ");
				
				for (int i = 0; i < processedMemberData.size(); i++) {
					// replace relation self/member with "applicant" 
					relation = StringUtils.replaceEach(processedMemberData.get(i).get(PlanMgmtConstants.RELATION).toLowerCase(), new String[]{PlanMgmtConstants.MEMBER,PlanMgmtConstants.SELF}, new String[]{PlanMgmtConstants.APPLICANT, PlanMgmtConstants.APPLICANT})  ;
					
					buildQuery.append(" ( SELECT p2.id AS plan_id, prate.RATE AS pre, 1 as memberctr, ");
					buildQuery.append("prate.rate_option as rate_option ");
					buildQuery.append("FROM ");
					buildQuery.append("PLAN_AME pame, PM_PLAN_RATE prate, PLAN p2 ");
					buildQuery.append("WHERE ");
					buildQuery.append("p2.ID = pame.PLAN_ID ");
					buildQuery.append("AND p2.id = prate.plan_id ");
					buildQuery.append("AND TO_DATE (:param_" + paramCount + ", 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date ");
					paramList.add(effectiveDate);
					paramCount++;
					
					buildQuery.append("AND ( (CAST(:param_" + paramCount);
					paramList.add(familyTierLookupCode);
					paramCount++;
					
					buildQuery.append(" AS INTEGER) >= prate.min_age AND CAST(:param_" + paramCount);
					paramList.add(familyTierLookupCode);
					paramCount++;
					
					buildQuery.append(" AS INTEGER) <= prate.max_age) OR (CAST(:param_" + paramCount);
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.AGE));
					paramCount++;
					
					buildQuery.append(" AS INTEGER) >= prate.min_age AND CAST(:param_" + paramCount);
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.AGE));
					paramCount++;
					
					buildQuery.append(" AS INTEGER) <= prate.max_age AND prate.relation = UPPER( :param_" + paramCount );
					paramList.add(relation);
					paramCount++;
					
					buildQuery.append(") ) ) AND (prate.GENDER IS NULL");
					if(null != processedMemberData.get(i).get(PlanMgmtConstants.GENDER)){
						buildQuery.append(" OR prate.GENDER = UPPER( :param_" + paramCount);
						paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.GENDER));
						paramCount++;
						buildQuery.append(") ");
					}
					buildQuery.append(") AND p2.state= :param_" + paramCount);
					paramList.add(stateName);
					paramCount++;
					buildQuery.append(" ");
					buildQuery.append("AND prate.is_deleted='N' ");			
					buildQuery.append(" ) ");
	
					if (i + 1 != processedMemberData.size()) {
						buildQuery.append(" UNION ALL ");
					}
				}
				
				buildQuery.append(") temp WHERE P.id = temp.plan_id ");	
				buildQuery.append("AND I.ID = P.ISSUER_ID ");		
				buildQuery.append("AND P.INSURANCE_TYPE = '"+Plan.PlanInsuranceType.AME.toString()+"' ");
				buildQuery.append(quotingBusinessLogic.buildPlanCertificationLogic("P", true));
				if(!doQuoteIssuerIHC ){
					buildQuery.append("AND I.name not like '%IHC%'");
				}
				
				// AME plans are always OFF exchange. No need to add exchange type filter 
				/*buildQuery.append(" AND p.exchange_type = UPPER(:param_" + paramList.size() +") ");
				paramList.add(exchangeTypeString);
				paramCount++;*/
				
				buildQuery.append(" AND p.id = tp.plan_id");
				buildQuery.append(" AND tp.tenant_id = t.id");
				buildQuery.append(" AND t.code = UPPER(:param_" + paramList.size() +") ");
				paramList.add(tenant);
				paramCount++;
				
				buildQuery.append("AND P.is_deleted='N' ");
				buildQuery.append("GROUP BY p.id, rate_option, I.hios_issuer_id ");
				buildQuery.append("HAVING sum(memberctr) = " + processedMemberData.size());
				buildQuery.append(" ORDER BY total_premium ASC ");
				
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("SQL for getLowestAMEPlanPremium:: " + buildQuery.toString());
				}
				
				
				Query query = entityManager.createNativeQuery(buildQuery.toString());
				for (int i = 0; i < paramCount; i++) {
					query.setParameter("param_" + i, paramList.get(i));
				}		
				Iterator<?> rsIterator = query.getResultList().iterator();
				if(rsIterator != null){
					while(rsIterator.hasNext()){
						Object[] objArray = (Object[]) rsIterator.next();
						total_premium = null != objArray[1].toString() ? Float.parseFloat(objArray[1].toString()) : 0.0F;
						String rateOption = objArray[2].toString();
						String hiosIssuerId = objArray[3].toString();
						total_premium = quotingBusinessLogic.calculatePremiumForAMEPlan(rateOption, hiosIssuerId, total_premium, processedMemberData.size(), stateName, df);
						premiumArrayList.add(total_premium);
					}
				}
				if(!(premiumArrayList.isEmpty())){
					Collections.sort(premiumArrayList);
					total_premium = premiumArrayList.get(PlanMgmtConstants.ZERO);
					total_premium = Float.parseFloat(df.format(total_premium));
				}
					
			}catch(Exception ex){		
				LOGGER.error("Exceptin Occured in getLowestAMEPlanPremium() ", ex);
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getLowestAMEPlanPremium() ", ex);
			}finally{
				 // close the entity manager
				 if(entityManager !=null && entityManager.isOpen()){
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
			}
			
			premium = BigDecimal.valueOf(Double.parseDouble(df.format(total_premium)));
		}
		
		return premium;
	}
	
	// compute lowest vision plan HIX-61441
	@Override
	@Transactional(readOnly = true)
	public Float getLowestVisionPlanPremium(List<Map<String, String>> memberList, String effectiveDate, String exchangeTypeString, String tenant) {
		Float premium = (float) 0;
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("-------- Inside getLowestVisionPlanPremium --------");
			}
			
			List<Map<String, String>> processedMemberData =  quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.VISION.toString());
			String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
			String familyTierLookupCode = lookupService.getLookupValueCode(PlanMgmtConstants.FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
			String stateName = quotingBusinessLogic.getApplicantStateCode(processedMemberData);	
						
			StringBuilder buildQuery = new StringBuilder();
			ArrayList<String> paramList = new ArrayList<String>();
			buildQuery.append("SELECT (CASE WHEN RATE_OPTION = 'F' THEN SUM(PRE)/" + processedMemberData.size() + " ELSE SUM(PRE) END) AS total_premium ");
			buildQuery.append("FROM ");
			buildQuery.append("PLAN P, ISSUERS I, PM_TENANT_PLAN tp, TENANT t,");
			
			buildQuery.append(" ( ");
			for (int i = 0; i < processedMemberData.size(); i++) {			
				buildQuery.append(" ( SELECT p2.id AS plan_id, prate.RATE AS PRE, prate.rate_option AS RATE_OPTION, 1 as memberctr ");
				buildQuery.append("FROM ");
				buildQuery.append("PLAN_VISION pvision, PM_PLAN_RATE prate, PLAN p2 ");
				buildQuery.append("WHERE ");
				buildQuery.append("p2.ID = pvision.PLAN_ID ");
				buildQuery.append("AND p2.id = prate.plan_id ");
				buildQuery.append(quotingBusinessLogic.buildRateLogic("prate", stateName, "", familyTierLookupCode, processedMemberData.get(i).get(PlanMgmtConstants.AGE), "", effectiveDate, paramList));
				buildQuery.append("AND p2.state= '" + stateName + "' ");				
				buildQuery.append(" ) ");

				if (i + 1 != processedMemberData.size()) {
					buildQuery.append(" UNION ALL ");
				}
			}
			
			buildQuery.append(") temp WHERE P.id = temp.plan_id ");	
			buildQuery.append("AND I.ID = P.ISSUER_ID ");		
			buildQuery.append("AND P.INSURANCE_TYPE = 'VISION' ");
			buildQuery.append(quotingBusinessLogic.buildPlanCertificationLogic("P", true));
			
			buildQuery.append(" AND p.exchange_type = UPPER(:param_" + paramList.size() +") "); 
			paramList.add(exchangeTypeString);
			
			buildQuery.append(" AND p.id = tp.plan_id");
			buildQuery.append(" AND tp.tenant_id = t.id");
			buildQuery.append(" AND t.code = UPPER(:param_" + paramList.size() +") ");
			paramList.add(tenant);
			
			buildQuery.append("AND P.is_deleted='N' ");
			buildQuery.append("GROUP BY ");
			buildQuery.append("P.id, RATE_OPTION ");					
			buildQuery.append("HAVING sum(memberctr) = " + processedMemberData.size());
			buildQuery.append(" ORDER BY total_premium ");	
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL == " + buildQuery.toString());
			}
			
			
			Query query = entityManager.createNativeQuery(buildQuery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}	
			List<?> rsList = query.getResultList();
			if (rsList != null && !rsList.isEmpty()) {
				premium = Float.parseFloat(rsList.get(0).toString());				
			}	
		}catch(Exception ex){		
			LOGGER.error("Exceptin Occured in getLowestVisionPlanPremium()", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getLowestVisionPlanPremium() ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		return premium;
	}
	
	
	//HIX-61442, get vision plans for cross sell 
	@Override
	@Transactional(readOnly = true)
	public List<CrossSellPlan> getVisionPlans(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenant){
		List<CrossSellPlan> crosssellPlanList = new ArrayList<CrossSellPlan>(); 		
		List<Map<String, String>> processedMemberData = quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.VISION.toString()); // filter member data					
		String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
		String familyTierLookupCode = quotingBusinessLogic.getFamilyTierLookupCode(familyTier); // compute family tier lookup code		
		String stateName =  quotingBusinessLogic.getApplicantStateCode(processedMemberData);
					
		StringBuilder buildQuery = new StringBuilder();
		ArrayList<String> paramList = new ArrayList<String>();
		buildQuery.append("SELECT P.ID, PLAN_VISION_ID, P.NAME AS PLAN_NAME, SUM(PRE) AS total_premium, I.LOGO_URL, RATE_OPTION, I.HIOS_ISSUER_ID  ");
		buildQuery.append("FROM ");
		buildQuery.append("PLAN P, ISSUERS I, PM_TENANT_PLAN tp, TENANT t,");
		buildQuery.append(" ( ");
		for (int i = 0; i < processedMemberData.size(); i++) {			
			buildQuery.append(" ( SELECT p2.id AS plan_id, prate.RATE AS PRE, pvision.ID AS PLAN_VISION_ID, 1 as memberctr, ");
			buildQuery.append(" prate.rate_option AS RATE_OPTION ");
			buildQuery.append("FROM ");
			buildQuery.append("PLAN_VISION pvision, PM_PLAN_RATE prate, PLAN p2 ");
			buildQuery.append("WHERE ");
			buildQuery.append("p2.ID = pvision.PLAN_ID ");
			buildQuery.append("AND p2.id = prate.plan_id ");
			buildQuery.append(quotingBusinessLogic.buildRateLogic("prate", stateName, "", familyTierLookupCode, processedMemberData.get(i).get(PlanMgmtConstants.AGE), "", effectiveDate, paramList));
			buildQuery.append("AND p2.state='" + stateName + "' ");
			buildQuery.append(") ");
			if (i + 1 != processedMemberData.size()) {
				buildQuery.append(" UNION ALL ");
			}
		}
		
		buildQuery.append(") temp WHERE P.id = temp.plan_id ");	
		buildQuery.append("AND I.ID = P.ISSUER_ID ");		
		buildQuery.append("AND P.INSURANCE_TYPE = 'VISION' ");
		buildQuery.append(quotingBusinessLogic.buildPlanCertificationLogic("P", true));
		
		buildQuery.append(" AND p.exchange_type = UPPER(:param_" + paramList.size() +") ");
		paramList.add(exchangeType);
		
		buildQuery.append(" AND p.id = tp.plan_id");
		buildQuery.append(" AND tp.tenant_id = t.id");
		buildQuery.append(" AND t.code = UPPER(:param_" + paramList.size() +") ");
		paramList.add(tenant);

		buildQuery.append("AND P.is_deleted='N' ");
		buildQuery.append("GROUP BY ");
		buildQuery.append("P.id, PLAN_VISION_ID, P.NAME, I.LOGO_URL, RATE_OPTION, I.HIOS_ISSUER_ID  ");
		buildQuery.append("HAVING sum(memberctr) = " + processedMemberData.size());
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug(" buildQuery :: " + buildQuery);
		}
				
		
		StringBuilder planVisionIdStr = new StringBuilder(PlanMgmtConstants.EMPTY_STRING);
		
		EntityManager entityManager = null;
		
		try {				
			entityManager = emf.createEntityManager();
			Float premium = (float) 0;
			Query query = entityManager.createNativeQuery(buildQuery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}	
			List<?> rsList = query.getResultList();
			Iterator<?> rsIterator = rsList.iterator();
			Iterator<?> rsIterator2 = rsList.iterator();
			
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				CrossSellPlan crossSellPlan = new CrossSellPlan();
				String logoURL = null;
				if(null != objArray[PlanMgmtConstants.FOUR]) {
					logoURL = objArray[PlanMgmtConstants.FOUR].toString();
				}
				crossSellPlan.setId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
				// concatenating plan_vision_ids to generate a single query
				planVisionIdStr.append((planVisionIdStr.length() == PlanMgmtConstants.ZERO) ? "'" + objArray[PlanMgmtConstants.ONE].toString() + "'" : ", '" + objArray[PlanMgmtConstants.ONE].toString() + "'");

				crossSellPlan.setName((objArray[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWO].toString());
				premium = Float.parseFloat(objArray[PlanMgmtConstants.THREE].toString());	
				//crossSellPlan.setIssuerLogo((objArray[PlanMgmtConstants.FOUR] !=null) ? appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.FOUR].toString()) : PlanMgmtConstants.EMPTY_STRING);
				crossSellPlan.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, objArray[PlanMgmtConstants.SIX].toString(), null, appUrl));
				String rateOption = objArray[PlanMgmtConstants.FIVE].toString();
				
				// if rate option is Family, then premium would be on family level, don't consider each quoted member's premium
				// if rate option is Age band, then premium would be for each member and aggregate for whole household
				if ("F".equalsIgnoreCase(rateOption)) {  // family quoting 
					premium = (premium / processedMemberData.size());
				}				
				crossSellPlan.setPremium(premium);
				
				crosssellPlanList.add(crossSellPlan);
				crossSellPlan = null;
			}			
			
			// pull plan benefits in a single shot
			 Map<String, Map<String, Map<String, String>>> benefitDataList = new HashMap<String, Map<String, Map<String, String>>>();
			 if(planVisionIdStr.length() > PlanMgmtConstants.ZERO){
				String filterBenefits = "'EYE_EXAM', 'GLASSES'"; 
				 benefitDataList = visionPlanRateBenefitService.getVisionPlanBenefits(planVisionIdStr.toString(), filterBenefits);
			 }
						
						
			 while (rsIterator2.hasNext()) {
				Object[] objArray = (Object[]) rsIterator2.next();
				String planVisionId = objArray[PlanMgmtConstants.ONE].toString();
				Integer planId = Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString());
				// set plan benefits
				for (CrossSellPlan crosssellPlan : crosssellPlanList) {
					if (planId.equals(crosssellPlan.getId()) && benefitDataList.containsKey(planVisionId)) {
						crosssellPlan.setPlanBenefits(benefitDataList.get(planVisionId));						
					}
				}
			}
			
		} catch (Exception ex) {
			LOGGER.error("Exception occured in getVisionPlans() ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getVisionPlans() ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		 
		 return crosssellPlanList;
	}
	
}
