package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.Plan;

@Repository
public interface IURRTPremiumClaimRepository extends JpaRepository<Plan, Integer> {	
    
    @Query("SELECT fullPortionOrEhbBasisOfTPQuantity, smbpThatAreOtherThanEHBQuantity FROM URRTPRCPremiumClaimInfo URRT WHERE URRT.id=:urrtRateChangeId AND LOWER(URRT.definingInsuranceRateCostPeriodType) = LOWER('plan-section4') ")
    List<Object[]> getEhbPercentage(@Param("urrtRateChangeId") Integer urrtRateChangeId);
       
}
