/**
 * WebQuoteServiceSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.getinsured.hix.planmgmt.phix.ws.uhc;

public class WebQuoteServiceSoapStub extends org.apache.axis.client.Stub implements com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteServiceSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[1];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetQuote");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "value"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "WebQuoteRequest"), com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteRequest.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "WebQuoteReturn"));
        oper.setReturnClass(com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteReturn.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "GetQuoteResult"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

    }

    public WebQuoteServiceSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public WebQuoteServiceSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public WebQuoteServiceSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfBenefit");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Benefit[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Benefit");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Benefit");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfChild");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Child[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Child");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Child");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfEnhancement");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Enhancement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Enhancement");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Enhancement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfEnhancementRate");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.EnhancementRate[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "EnhancementRate");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "EnhancementRate");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfFee");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Fee[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Fee");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Fee");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfFeeType");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.FeeType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "FeeType");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "FeeType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfNetwork");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Network[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Network");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Network");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfOption");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Option[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Option");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Option");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfOptionRate");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.OptionRate[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "OptionRate");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "OptionRate");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfPlan");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Plan[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Plan");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Plan");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfPlanRate");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.PlanRate[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "PlanRate");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "PlanRate");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ArrayOfString");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "string");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "AuthorizationType");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.AuthorizationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Benefit");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Benefit.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Child");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Child.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Enhancement");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Enhancement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "EnhancementRate");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.EnhancementRate.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "ExtensionDataObject");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.ExtensionDataObject.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Fee");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Fee.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "FeeType");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.FeeType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Network");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Network.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Option");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Option.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "OptionRate");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.OptionRate.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "Plan");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.Plan.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "PlanRate");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.PlanRate.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "PrimaryInsuredType");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.PrimaryInsuredType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "QuoteRequestType");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.QuoteRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "SpouseInsuredType");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.SpouseInsuredType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "WebQuoteRequest");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "WebQuoteReturn");
            cachedSerQNames.add(qName);
            cls = com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteReturn.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteReturn getQuote(com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteRequest value) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.goldenrulehealthws.com/IWebQuoteService/Latest/WebQuoteServiceSoap/GetQuote");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.goldenrulehealthws.com/IWebQuoteService/Latest", "GetQuote"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {value});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteReturn) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteReturn) org.apache.axis.utils.JavaUtils.convert(_resp, com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteReturn.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
