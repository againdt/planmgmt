package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.PlanRateBenefit;

public interface PufPlanService {

	 List<PlanRateBenefit> getPufPlanData(String zip, String countyCode, String hiosPlanIds, List<Map<String, String>> memberDataList, String stateName, String houseHoldType, String insuranceType, String applicableYear);
	 
	 String getCountyNameUsingZipAndCountyFIPS(String zip, String county_fips);	 
}
