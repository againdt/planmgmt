/* 
@author Santanu 
@Version 1.0
@Date 25 March 2016 
*/

package com.getinsured.hix.planmgmt.service;

import com.getinsured.hix.dto.planmgmt.SingleIssuerResponse;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;

public class IssuerResponseDTOHelper {

	// setObjectToDTO method set Object to SingleIssuerResponse DTO
	public SingleIssuerResponse setObjectToDTO(Issuer issuer, boolean minimizeIssuerData, boolean includeLogo) {

		SingleIssuerResponse singleIssuerResponse = new SingleIssuerResponse();
		singleIssuerResponse.setId(issuer.getId());
		singleIssuerResponse.setName(issuer.getName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getName());
		singleIssuerResponse.setHiosIssuerId(issuer.getHiosIssuerId() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getHiosIssuerId());
		singleIssuerResponse.setCompanyLogo(issuer.getLogoURL() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLogoURL());
		singleIssuerResponse.setApplicationUrl(issuer.getApplicationUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getApplicationUrl());
		singleIssuerResponse.setProducerUserName(issuer.getProducerUserName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getProducerUserName());
		singleIssuerResponse.setProducerPassword(issuer.getProducerPassword() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getProducerPassword());
		singleIssuerResponse.setPaymentUrl(issuer.getPaymentUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getPaymentUrl());

		if (issuer.getIssuerBrandName() != null) {
			singleIssuerResponse.setBrandNameId(issuer.getIssuerBrandName().getId());
			singleIssuerResponse.setBrandName(issuer.getIssuerBrandName().getBrandName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIssuerBrandName().getBrandName());
			singleIssuerResponse.setIsDeleted(issuer.getIssuerBrandName().getIsDeleted() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIssuerBrandName().getIsDeleted());
			singleIssuerResponse.setBrandUrl(issuer.getIssuerBrandName().getBrandUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIssuerBrandName().getBrandUrl());
		}
		else {
			singleIssuerResponse.setBrandNameId(PlanMgmtConstants.ZERO);
			singleIssuerResponse.setBrandName(PlanMgmtConstants.EMPTY_STRING);
			singleIssuerResponse.setIsDeleted(PlanMgmtConstants.EMPTY_STRING);
			singleIssuerResponse.setBrandUrl(PlanMgmtConstants.EMPTY_STRING);					
		}

		if (includeLogo) {
			singleIssuerResponse.setLogo(issuer.getLogo());
		}

		if (!minimizeIssuerData) {
			singleIssuerResponse.setCompanyLegalName(issuer.getCompanyLegalName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyLegalName());
			singleIssuerResponse.setCertificationStatus(issuer.getCertificationStatus() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCertificationStatus());
			singleIssuerResponse.setFederalEin(issuer.getFederalEin() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getFederalEin());
			singleIssuerResponse.setNaicCompanyCode(issuer.getNaicCompanyCode() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getNaicCompanyCode());
			singleIssuerResponse.setNaicGroupCode(issuer.getNaicGroupCode() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getNaicGroupCode());
			singleIssuerResponse.setAccreditingEntity(issuer.getAccreditingEntity() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAccreditingEntity());
			singleIssuerResponse.setLicenseNumber(issuer.getLicenseNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLicenseNumber());
			singleIssuerResponse.setLicenseStatus(issuer.getLicenseStatus() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getLicenseStatus());
			singleIssuerResponse.setAddressLine1(issuer.getAddressLine1() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAddressLine1());
			singleIssuerResponse.setAddressLine2(issuer.getAddressLine2() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAddressLine2());
			singleIssuerResponse.setCity(issuer.getCity() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCity());
			singleIssuerResponse.setState(issuer.getState() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getState());
			singleIssuerResponse.setZip(issuer.getZip() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getZip());
			singleIssuerResponse.setPhoneNumber(issuer.getPhoneNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getPhoneNumber());
			singleIssuerResponse.setEmailAddress(issuer.getEmailAddress() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getEmailAddress());
			singleIssuerResponse.setEnrollmentUrl(issuer.getEnrollmentUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getEnrollmentUrl());
			singleIssuerResponse.setContactPerson(issuer.getContactPerson() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getContactPerson());
			singleIssuerResponse.setInitialPayment(issuer.getInitialPayment() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getInitialPayment());
			singleIssuerResponse.setRecurringPayment(issuer.getRecurringPayment() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getRecurringPayment());
			singleIssuerResponse.setSiteUrl(issuer.getSiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getSiteUrl());
			singleIssuerResponse.setCompanySiteUrl(issuer.getCompanySiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanySiteUrl());
			singleIssuerResponse.setStateOfDomicile(issuer.getStateOfDomicile() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getStateOfDomicile());
			singleIssuerResponse.setCompanyAddressLine1(issuer.getCompanyAddressLine1() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyAddressLine1());
			singleIssuerResponse.setCompanyAddressLine2(issuer.getCompanyAddressLine2() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyAddressLine2());
			singleIssuerResponse.setCompanyCity(issuer.getCompanyCity() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyCity());
			singleIssuerResponse.setCompanyState(issuer.getCompanyState() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyState());
			singleIssuerResponse.setCompanyZip(issuer.getCompanyZip() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanyZip());
			singleIssuerResponse.setCompanySiteUrl(issuer.getCompanySiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCompanySiteUrl());
			singleIssuerResponse.setIndvCustServicePhone(issuer.getIndvCustServicePhone() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIndvCustServicePhone());
			singleIssuerResponse.setIndvCustServicePhoneExt(issuer.getIndvCustServicePhoneExt() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIndvCustServicePhoneExt());
			singleIssuerResponse.setIndvCustServiceTollFree(issuer.getIndvCustServiceTollFree() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIndvCustServiceTollFree());
			singleIssuerResponse.setIndvCustServiceTTY(issuer.getIndvCustServiceTTY() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIndvCustServiceTTY());
			singleIssuerResponse.setIndvSiteUrl(issuer.getIndvSiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getIndvSiteUrl());
			singleIssuerResponse.setShopCustServicePhone(issuer.getShopCustServicePhone() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShopCustServicePhone());
			singleIssuerResponse.setShopCustServicePhoneExt(issuer.getShopCustServicePhoneExt() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShopCustServicePhoneExt());
			singleIssuerResponse.setShopCustServiceTollFree(issuer.getShopCustServiceTollFree() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShopCustServiceTollFree());
			singleIssuerResponse.setShopCustServiceTTY(issuer.getShopCustServiceTTY() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShopCustServiceTTY());
			singleIssuerResponse.setShopSiteUrl(issuer.getShopSiteUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShopSiteUrl());
			singleIssuerResponse.setEffectiveStartDate(issuer.getEffectiveStartDate());
			singleIssuerResponse.setEffectiveEndDate(issuer.getEffectiveEndDate());
			singleIssuerResponse.setCreationTimestamp(issuer.getCreationTimestamp());
			singleIssuerResponse.setLastUpdateTimestamp(issuer.getLastUpdateTimestamp());
			singleIssuerResponse.setCertificationDoc(issuer.getCertificationDoc() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getCertificationDoc());
			singleIssuerResponse.setShortName(issuer.getShortName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getShortName());
			singleIssuerResponse.setTxn820Version(issuer.getTxn820Version() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getTxn820Version());
			singleIssuerResponse.setTxn834Version(issuer.getTxn834Version() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getTxn834Version());
			singleIssuerResponse.setMarketingName(issuer.getMarketingName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getMarketingName());
			singleIssuerResponse.setLastUpdatedBy(issuer.getLastUpdatedBy() == null ? PlanMgmtConstants.ZERO : issuer.getLastUpdatedBy());
			singleIssuerResponse.setNationalProducerNumber(issuer.getNationalProducerNumber() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getNationalProducerNumber());
			singleIssuerResponse.setAgentFirstName(issuer.getAgentFirstName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAgentFirstName());
			singleIssuerResponse.setAgentLastName(issuer.getAgentLastName() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getAgentLastName());
			singleIssuerResponse.setOnExchangeDisclaimer(issuer.getOnExchangeDisclaimer() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getOnExchangeDisclaimer());
			singleIssuerResponse.setOffExchangeDisclaimer(issuer.getOffExchangeDisclaimer() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getOffExchangeDisclaimer());
			singleIssuerResponse.setBrokerId(issuer.getBrokerId() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerId());
			singleIssuerResponse.setWritingAgent(issuer.getWritingAgent() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getWritingAgent());
			singleIssuerResponse.setBrokerPhone(issuer.getBrokerPhone() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerPhone());
			singleIssuerResponse.setBrokerFax(issuer.getBrokerFax() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerFax());
			singleIssuerResponse.setBrokerEmail(issuer.getBrokerEmail() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getBrokerEmail());
			singleIssuerResponse.setProducerUrl(issuer.getProducerUrl() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getProducerUrl());
			singleIssuerResponse.setD2C(issuer.getD2C() == null ? PlanMgmtConstants.EMPTY_STRING : issuer.getD2C());
			singleIssuerResponse.setOrgChart(issuer.getOrgChart());
			singleIssuerResponse.setCertifiedOn(issuer.getCertifiedOn());
			singleIssuerResponse.setDecertifiedOn(issuer.getDecertifiedOn());
		}
		
		return  singleIssuerResponse;
	}
	
	
}
