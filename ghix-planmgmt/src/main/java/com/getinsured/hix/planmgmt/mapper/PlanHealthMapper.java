package com.getinsured.hix.planmgmt.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanDentalCost;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.PlanHealthCost;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;

public class PlanHealthMapper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanHealthMapper.class);
	
	private static Map<String, Integer> columnKeys = null;
	
	private static final  String BENEFIT_NAME = "Benefits";
	public static final String DB_DATE_FORMAT = "yyyy-MM-dd";
	public static final String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";
	
	/*	Renamed Fields Constants	 */
	private static final  String LIMITATION = "Limitation( Numeric Value)";
	private static final  String LIMITATIONATTRIBUTE = "Limitation Attribute";
	private static final  String EXCEPTIONS = "Exceptions";
	
	private static final  String SUBJECTTOINNETWORKDEDUCTIBLE= "Subject to Deductible";
	private static final  String EXCLUDEFROMINNETWORKMOOP= "Excluded from MOOP";
	private static final  String SUBJECTTOOUTOFNETWORKDEDUCTIBLE= "Subject to Deductible";
	private static final  String EXCLUDEDFROMOUTOFNETWORKMOOP= "Excluded from MOOP";
	private static final  String COMBINEDINANDOUTOFNETWORK= "Combined In and Out of Network";
	private static final  String COMBINEDINANDOUTOFNETWORKATTRIBUTE= "Combined In and Out of Network Attribute";
	private static final  String INNETWORKINDIVIDUAL= "In Network - Individual";
	private static final  String INNETWORKFAMILY= "In Network - Family";
	private static final  String INNETWORKTIERTWOKINDIVIDUAL= "In Network (Tier 2) - Individual";
	private static final  String INNETWORKTIERTWOKFAMILY= "In Network (Tier 2) - Family ";
	private static final  String OUTNETWORKINDIVIDUAL= "Out of Network - Individual";
	private static final  String OUTNETWORKFAMILY= "Out of Network - Family";
	private static final  String COMBINEDINANDOUTOFNETWORKINDIVIDUAL= "Combined In/Out Network - Individual";
	private static final  String COMBINEDINANDOUTOFNETWORKFAMILY= "Combined In/Out Network - Family";
	
	/*	New Fields Constants start here	 */
	private static final  String NETWORKT1COPAYATTR= "Copay Attribute";
	private static final  String NETWORKT1COPAYVAL= "Copay Value";
	private static final  String NETWORKT1COINSURANCEATTR= "Coinsurance Attribute";
	private static final  String NETWORKT1COINSURANCEVAL= "Coinsurance Value";

	private static final  String NETWORKT2COPAYATTR= "Copay Attribute";
	private static final  String NETWORKT2COPAYVAL= "Copay Value";
	private static final  String NETWORKT2COINSURANCEATTR= "Coinsurance Attribute";
	private static final  String NETWORKT2COINSURANCEVAL= "Coinsurance Value";

	private static final  String OUTNETWORKCOPAYVAL= "Copay Value";
	private static final  String OUTNETWORKCOPAYATTR= "Copay Attribute";
	private static final  String OUTNETWORKCOINSURANCEVAL= "Coinsurance Value";
	private static final  String OUTNETWORKCOINSURANCEATTR= "Coinsurance Attribute";
	/*	New Fields Constants end here	 */
	
	// for new Report Format
	
	private static final  String PLANNAMEANDNUMBER = "Plan Name and Number";
	private static final  String PLANSTARTDATE = "Plan Start Date";
	private static final  String PLANENDDATE = "Plan End Date";
	private static final  String INNETWORKTIER1 = "In Network Tier 1";
	private static final  String INNETWORKTIER2 = "In Network Tier 2";
	private static final  String OUTOFNETWORK = "Out of Network";
	private static final  String COMBINEDINOUTNETWORK = "Combined In/Out Network";
	private static final  String DEDUCTIBLES = "Deductibles";
	private static final  String INDIVIDUAL = "Individual";
	private static final  String FAMILY = "Family";
	private static final  String DEFAULTCOINSURANCE = "Default Coinsurance";
	private static final  String EMPTYSTRING = " ";
	private static final  String INNETWORK = "In Network";
	private static final  String TIER1 = "Tier 1";
	private static final  String TIER2 = "Tier 2";
	
	/*	Renamed values for Benefit Attribute */
	private static final  int LIMITATION_VAL = 1;
	private static final  int LIMITATIONATTRIBUTE_VAL = 2;
	private static final  int EXCEPTIONS_VAL = 3;
	
	private static final  int SUBJECTTOINNETWORKDEDUCTIBLE_VAL= 4;
	private static final  int EXCLUDEFROMINNETWORKMOOP_VAL= 5;

	private static final  int BENEFIT_NAME_VAL = 6;
	private static final  int SUBJECTTOOUTOFNETWORKDEDUCTIBLE_VAL= 7;
	private static final  int EXCLUDEDFROMOUTOFNETWORKMOOP_VAL= 8;
	private static final  int COMBINEDINANDOUTOFNETWORK_VAL= 9;
	private static final  int COMBINEDINANDOUTOFNETWORKATTRIBUTE_VAL= 10;
	private static final int INNETWORKINDIVIDUAL_VAL= 11;
	private static final int INNETWORKFAMILY_VAL= 12;
	private static final int INNETWORKTIERTWOKINDIVIDUAL_VAL= 13;
	private static final int INNETWORKTIERTWOKFAMILY_VAL= 14;
	private static final int OUTNETWORKINDIVIDUAL_VAL= 15;
	private static final int OUTNETWORKFAMILY_VAL= 16;
	private static final int COMBINEDINANDOUTOFNETWORKINDIVIDUAL_VAL= 17;
	private static final int COMBINEDINANDOUTOFNETWORKFAMILY_VAL= 18;
	
	/*	New Fields Constants value start here	 */
	private static final  int NETWORKT1COPAYATTR_VAL= 19;
	private static final  int NETWORKT1COPAYVAL_VAL= 20;
	private static final  int NETWORKT1COINSURANCEATTR_VAL= 21;
	private static final  int NETWORKT1COINSURANCEVAL_VAL= 22;

	private static final  int NETWORKT2COPAYATTR_VAL= 23;
	private static final  int NETWORKT2COPAYVAL_VAL= 24;
	private static final  int NETWORKT2COINSURANCEATTR_VAL= 25;
	private static final  int NETWORKT2COINSURANCEVAL_VAL= 26;

	private static final  int OUTNETWORKCOPAYATTR_VAL= 27;
	private static final  int OUTNETWORKCOPAYVAL_VAL= 28;
	private static final  int OUTNETWORKCOINSURANCEATTR_VAL= 29;
	private static final  int OUTNETWORKCOINSURANCEVAL_VAL= 30;
	/*	New Fields Constants value end here	 */

	static {
		columnKeys = new HashMap<String, Integer>();
		
		columnKeys.put(BENEFIT_NAME, BENEFIT_NAME_VAL);
				
		/*	Renamed Attribute chagnes */
		columnKeys.put(LIMITATION, LIMITATION_VAL);
		columnKeys.put(LIMITATIONATTRIBUTE, LIMITATIONATTRIBUTE_VAL);
		columnKeys.put(EXCEPTIONS, EXCEPTIONS_VAL);

		columnKeys.put(SUBJECTTOINNETWORKDEDUCTIBLE, SUBJECTTOINNETWORKDEDUCTIBLE_VAL);
		columnKeys.put(EXCLUDEFROMINNETWORKMOOP, EXCLUDEFROMINNETWORKMOOP_VAL);
		columnKeys.put(SUBJECTTOOUTOFNETWORKDEDUCTIBLE, SUBJECTTOOUTOFNETWORKDEDUCTIBLE_VAL);
		columnKeys.put(EXCLUDEDFROMOUTOFNETWORKMOOP, EXCLUDEDFROMOUTOFNETWORKMOOP_VAL);
		columnKeys.put(COMBINEDINANDOUTOFNETWORK, COMBINEDINANDOUTOFNETWORK_VAL);
		columnKeys.put(COMBINEDINANDOUTOFNETWORKATTRIBUTE, COMBINEDINANDOUTOFNETWORKATTRIBUTE_VAL);
		columnKeys.put(INNETWORKINDIVIDUAL, INNETWORKINDIVIDUAL_VAL);
		columnKeys.put(INNETWORKFAMILY, INNETWORKFAMILY_VAL);
		columnKeys.put(INNETWORKTIERTWOKINDIVIDUAL, INNETWORKTIERTWOKINDIVIDUAL_VAL);
		columnKeys.put(INNETWORKTIERTWOKFAMILY,INNETWORKTIERTWOKFAMILY_VAL);
		columnKeys.put(OUTNETWORKINDIVIDUAL, OUTNETWORKINDIVIDUAL_VAL);
		columnKeys.put(OUTNETWORKFAMILY, OUTNETWORKFAMILY_VAL);
		columnKeys.put(COMBINEDINANDOUTOFNETWORKINDIVIDUAL, COMBINEDINANDOUTOFNETWORKINDIVIDUAL_VAL);
		columnKeys.put(COMBINEDINANDOUTOFNETWORKFAMILY, COMBINEDINANDOUTOFNETWORKFAMILY_VAL);
		
		/* New Benefit Attribute Start here	*/
		columnKeys.put(NETWORKT1COPAYATTR, NETWORKT1COPAYATTR_VAL);
		columnKeys.put(NETWORKT1COPAYVAL, NETWORKT1COPAYVAL_VAL);
		columnKeys.put(NETWORKT1COINSURANCEATTR, NETWORKT1COINSURANCEATTR_VAL);
		columnKeys.put(NETWORKT1COINSURANCEVAL, NETWORKT1COINSURANCEVAL_VAL);

		columnKeys.put(NETWORKT2COPAYATTR, NETWORKT2COPAYATTR_VAL);
		columnKeys.put(NETWORKT2COPAYVAL, NETWORKT2COPAYVAL_VAL);
		columnKeys.put(NETWORKT2COINSURANCEATTR, NETWORKT2COINSURANCEATTR_VAL);
		columnKeys.put(NETWORKT2COINSURANCEVAL, NETWORKT2COINSURANCEVAL_VAL);
		
		columnKeys.put(OUTNETWORKCOPAYATTR, OUTNETWORKCOPAYATTR_VAL);
		columnKeys.put(OUTNETWORKCOPAYVAL, OUTNETWORKCOPAYVAL_VAL);
		columnKeys.put(OUTNETWORKCOINSURANCEATTR, OUTNETWORKCOINSURANCEATTR_VAL);		
		columnKeys.put(OUTNETWORKCOINSURANCEVAL, OUTNETWORKCOINSURANCEVAL_VAL);
		/* New Benefit Attribute End here	*/
	}
	
	public PlanHealth mapData(List<List<String>> data, final List<String> columns,Map<String,String> benefitMap){	
		PlanHealth planHealth = new PlanHealth();
		try {
			long startTime = TimeShifterUtil.currentTimeMillis();
			
			LOGGER.info("Started mapping the data file for Plan benefits.");
			String colName = null;
			String key = null;
			PlanHealthBenefit planHealthBenefit = new PlanHealthBenefit();;
			int keyIndex = columns.indexOf(BENEFIT_NAME);
			HashMap<String, PlanHealthBenefit> benefitsObjs1 = new HashMap<String, PlanHealthBenefit>();
			planHealth.setPlanHealthBenefits(benefitsObjs1);
			LOGGER.info("PlanHealth mapdata");
			for(List<String> line : data)
			{
				key = line.get(keyIndex);
				LOGGER.info("Key Value from Mapper: " + key);
				if(key != null && key.length()>0)
				{
					planHealthBenefit = new PlanHealthBenefit();
					if (benefitMap.containsKey(key))
					{
						String keyConst = benefitMap.get(key); 
						planHealthBenefit.setName(keyConst);
						benefitsObjs1.put(keyConst, planHealthBenefit);					
					}
					else
					{
						planHealthBenefit.setName(key);
						benefitsObjs1.put(key, planHealthBenefit);		
					}
					planHealthBenefit.setPlanHealth(planHealth);
				}
				int colIdx = 0;
				for (String columnData : line)
				{
					colName = columns.get(colIdx);
					LOGGER.info("Mapping data for " + key);
					mapData(columnData, colName, planHealthBenefit);
					colIdx++;
				}
				key = null;
				planHealthBenefit = null;
			}
			long endTime = TimeShifterUtil.currentTimeMillis();
			LOGGER.info("Mapping the data file for Plan benefits completed in." + (endTime - startTime) + " seconds.");
			
		} catch (Exception e) {
			LOGGER.error("Error occured while mapping to constant",e.getMessage(),e);
		}
		return planHealth;
	}
	
	private void mapData(String data, String column,PlanHealthBenefit planHealthBenefit){
		
		Integer colVal = columnKeys.get(column);
		if (colVal == null){
			colVal = 0;
		}
		switch (colVal) {
					
			/*	Renamed Benefit Attribute Start here	*/
			case LIMITATION_VAL :
				planHealthBenefit.setNetworkLimitation(data);
				break;
			case LIMITATIONATTRIBUTE_VAL :
				planHealthBenefit.setNetworkLimitationAttribute(data);
				break;
			case EXCEPTIONS_VAL :
				planHealthBenefit.setNetworkExceptions(data);
				break;
			/*	Renamed Benefit Attribute End here	*/
				
			case SUBJECTTOINNETWORKDEDUCTIBLE_VAL:
				planHealthBenefit.setSubjectToInNetworkDuductible(data);
				break; 			
			case EXCLUDEFROMINNETWORKMOOP_VAL:
				planHealthBenefit.setExcludedFromInNetworkMoop(data);
				break;
			case SUBJECTTOOUTOFNETWORKDEDUCTIBLE_VAL:
				planHealthBenefit.setSubjectToOutNetworkDeductible(data);
				break;
			case EXCLUDEDFROMOUTOFNETWORKMOOP_VAL:
				planHealthBenefit.setExcludedFromOutOfNetworkMoop(data);
				break;
			case COMBINEDINANDOUTOFNETWORK_VAL:
				planHealthBenefit.setCombinedInAndOutOfNetwork(data);				
				break; 			
			case COMBINEDINANDOUTOFNETWORKATTRIBUTE_VAL:
				planHealthBenefit.setCombinedInAndOutNetworkAttribute(data);
				break; 
			case INNETWORKINDIVIDUAL_VAL:
				try {
					planHealthBenefit.setInNetworkIndividual(Double.parseDouble(data));
				}catch (Exception e) {planHealthBenefit.setInNetworkIndividual(0.0);}
				break; 
			case INNETWORKFAMILY_VAL:
				try {
					planHealthBenefit.setInNetworkFamily(Double.parseDouble(data));
				}catch (Exception e) {planHealthBenefit.setInNetworkFamily(0.0);}
				break;
			case INNETWORKTIERTWOKINDIVIDUAL_VAL:
				try {
					planHealthBenefit.setInNetworkTierTwoIndividual(Double.parseDouble(data));
				}catch (Exception e) {planHealthBenefit.setInNetworkTierTwoIndividual(0.0);}
				break;
			case INNETWORKTIERTWOKFAMILY_VAL:
				try {
					planHealthBenefit.setInNetworkTierTwoFamily(Double.parseDouble(data));
				}catch (Exception e) {planHealthBenefit.setInNetworkTierTwoFamily(0.0);}
				break;
			case COMBINEDINANDOUTOFNETWORKINDIVIDUAL_VAL:
				try {
					planHealthBenefit.setCombinedInOutNetworkIndividual(Double.parseDouble(data));
				}catch (Exception e) {planHealthBenefit.setCombinedInOutNetworkIndividual(0.0);}
				break;
			case COMBINEDINANDOUTOFNETWORKFAMILY_VAL:
				try {
					planHealthBenefit.setCombinedInOutNetworkFamily(Double.parseDouble(data));
				}catch (Exception e) {planHealthBenefit.setCombinedInOutNetworkFamily(0.0);}
				break;
			
			/*	New Benefit Attribute Start here */
			case NETWORKT1COPAYATTR_VAL:
				planHealthBenefit.setNetworkT1CopayAttr(data);				
				break;
			case NETWORKT1COPAYVAL_VAL:
				planHealthBenefit.setNetworkT1CopayVal(data);				
				break;
			case NETWORKT1COINSURANCEATTR_VAL:
				planHealthBenefit.setNetworkT1CoinsurAttr(data);				
				break;
			case NETWORKT1COINSURANCEVAL_VAL:
				planHealthBenefit.setNetworkT1CoinsurVal(data);				
				break;
			
			case NETWORKT2COPAYATTR_VAL:
				planHealthBenefit.setNetworkT2CopayAttr(data);				
				break;
			case NETWORKT2COPAYVAL_VAL:
				planHealthBenefit.setNetworkT2CopayVal(data);				
				break;
			case NETWORKT2COINSURANCEATTR_VAL:
				planHealthBenefit.setNetworkT2CoinsurAttr(data);				
				break;
			case NETWORKT2COINSURANCEVAL_VAL:
				planHealthBenefit.setNetworkT2CoinsurVal(data);				
				break;	

			case OUTNETWORKCOPAYATTR_VAL:
				planHealthBenefit.setOutOfNetworkCopayAttr(data);				
				break;
			case OUTNETWORKCOPAYVAL_VAL:
				planHealthBenefit.setOutOfNetworkCopayVal(data);				
				break;
			case OUTNETWORKCOINSURANCEATTR_VAL:
				planHealthBenefit.setOutOfNetworkCoinsurAttr(data);				
				break;
			case OUTNETWORKCOINSURANCEVAL_VAL:
				planHealthBenefit.setOutOfNetworkCoinsurVal(data);				
				break;	

			/*	New Benefit Attribute End here	*/
			default:
				LOGGER.info("Invalid option : "+colVal);
			break;
		}
	}
	
	
	public static List<String[]> convertObjectListToStringList(List<PlanHealthBenefit> healthBenefit,List<HierarchicalConfiguration> benefitList){
		
		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders = new String[PlanMgmtConstants.TWENTY];
		columnHeaders[PlanMgmtConstants.ZERO] = BENEFIT_NAME;

		/* Renamed Benefit Attributes */

		columnHeaders[PlanMgmtConstants.ONE] = NETWORKT1COPAYVAL;
		columnHeaders[PlanMgmtConstants.TWO] = NETWORKT1COPAYATTR;
		columnHeaders[PlanMgmtConstants.THREE] = NETWORKT1COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.FOUR] = NETWORKT1COINSURANCEATTR;
		columnHeaders[PlanMgmtConstants.FIVE] = SUBJECTTOINNETWORKDEDUCTIBLE;

		columnHeaders[PlanMgmtConstants.SIX] = NETWORKT2COPAYVAL;
		columnHeaders[PlanMgmtConstants.SEVEN] = NETWORKT2COPAYATTR;
		columnHeaders[PlanMgmtConstants.EIGHT] = NETWORKT2COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.NINE] = NETWORKT2COINSURANCEATTR;
		columnHeaders[PlanMgmtConstants.TEN] = SUBJECTTOOUTOFNETWORKDEDUCTIBLE;
		columnHeaders[PlanMgmtConstants.ELEVEN] = EXCLUDEFROMINNETWORKMOOP;

		columnHeaders[PlanMgmtConstants.TWELVE] = OUTNETWORKCOPAYVAL;
		columnHeaders[PlanMgmtConstants.THIRTEEN] = OUTNETWORKCOPAYATTR;
		columnHeaders[PlanMgmtConstants.FOURTEEN] = OUTNETWORKCOINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.FIFTEEN] = OUTNETWORKCOINSURANCEATTR;
		columnHeaders[PlanMgmtConstants.SIXTEEN] = EXCLUDEDFROMOUTOFNETWORKMOOP;

		columnHeaders[PlanMgmtConstants.SEVENTEEN] = LIMITATION;
		columnHeaders[PlanMgmtConstants.EIGHTEEN] = LIMITATIONATTRIBUTE;
		columnHeaders[PlanMgmtConstants.NINTEEN] = EXCEPTIONS;

		/* New Benefit Attributes */
				
		dataList.add(columnHeaders);
		
		for (int i = 0; i < healthBenefit.size(); i++) {
			String[] benefits = new String[PlanMgmtConstants.TWENTY];
						
			benefits[PlanMgmtConstants.ZERO] = getBenefitName(healthBenefit.get(i).getName(),benefitList);
			benefits[PlanMgmtConstants.ONE] = healthBenefit.get(i).getNetworkT1CopayVal();
			benefits[PlanMgmtConstants.TWO] = healthBenefit.get(i).getNetworkT1CopayAttr();
			benefits[PlanMgmtConstants.THREE] = healthBenefit.get(i).getNetworkT1CoinsurVal();			
			benefits[PlanMgmtConstants.FOUR] = healthBenefit.get(i).getNetworkT1CoinsurAttr();
			benefits[PlanMgmtConstants.FIVE] = healthBenefit.get(i).getSubjectToInNetworkDuductible();
			
			benefits[PlanMgmtConstants.SIX] = healthBenefit.get(i).getNetworkT2CopayVal();
			benefits[PlanMgmtConstants.SEVEN] = healthBenefit.get(i).getNetworkT2CopayAttr();
			benefits[PlanMgmtConstants.EIGHT] = healthBenefit.get(i).getNetworkT2CoinsurVal();			
			benefits[PlanMgmtConstants.NINE] = healthBenefit.get(i).getNetworkT2CoinsurAttr();
			benefits[PlanMgmtConstants.TEN] = healthBenefit.get(i).getSubjectToOutNetworkDeductible();
			benefits[PlanMgmtConstants.ELEVEN] = healthBenefit.get(i).getExcludedFromInNetworkMoop();
			
			benefits[PlanMgmtConstants.TWELVE] = healthBenefit.get(i).getOutOfNetworkCopayVal();
			benefits[PlanMgmtConstants.THIRTEEN] = healthBenefit.get(i).getOutOfNetworkCopayAttr();
			benefits[PlanMgmtConstants.FOURTEEN] = healthBenefit.get(i).getOutOfNetworkCoinsurVal();
			benefits[PlanMgmtConstants.FIFTEEN] = healthBenefit.get(i).getOutOfNetworkCoinsurAttr();
			benefits[PlanMgmtConstants.SIXTEEN] = healthBenefit.get(i).getExcludedFromOutOfNetworkMoop();
			
			benefits[PlanMgmtConstants.SEVENTEEN] = healthBenefit.get(i).getNetworkLimitation();
			benefits[PlanMgmtConstants.EIGHTEEN] = healthBenefit.get(i).getNetworkLimitationAttribute();
			benefits[PlanMgmtConstants.NINTEEN] = healthBenefit.get(i).getNetworkExceptions();
			
			
			
			
			
			dataList.add(benefits);
		}
		return dataList;
	}
	
public static List<String[]> convertObjectListToStringListFromMap(List<PlanHealthBenefit> healthBenefit,Map<String, String> healthBenefitMap,  Plan plan, List<PlanHealthCost> planHealthCostData){
		
		
		
		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders1 = new String[PlanMgmtConstants.THREE];
		
		String[] columnHeaders2 = new String[PlanMgmtConstants.TWO];
		String[] columnHeaders3 = new String[PlanMgmtConstants.TWO];
		
		String[] columnHeaders4 = new String[PlanMgmtConstants.FIVE];
		String[] columnHeaders5 = new String[PlanMgmtConstants.ELEVEN];
		String[] columnHeaders12 = new String[PlanMgmtConstants.THREE];
		String[] columnHeaders13 = new String[PlanMgmtConstants.THREE];
		String[] columnHeaders = new String[PlanMgmtConstants.TWENTY];
		
		columnHeaders1[0]=PLANNAMEANDNUMBER;
		
		columnHeaders2[0]=PLANSTARTDATE;
		columnHeaders3[0]=PLANENDDATE;
		
		columnHeaders4[0] = EMPTYSTRING;
		columnHeaders4[PlanMgmtConstants.ONE] = INNETWORKTIER1;
		columnHeaders4[PlanMgmtConstants.TWO] = INNETWORKTIER2;
		columnHeaders4[PlanMgmtConstants.THREE] = OUTOFNETWORK;
		columnHeaders4[PlanMgmtConstants.FOUR] = COMBINEDINOUTNETWORK;
		
		
		columnHeaders5[0] = DEDUCTIBLES;
		columnHeaders5[PlanMgmtConstants.ONE] = INDIVIDUAL;
		columnHeaders5[PlanMgmtConstants.TWO] = FAMILY;
		columnHeaders5[PlanMgmtConstants.THREE] = DEFAULTCOINSURANCE;
		columnHeaders5[PlanMgmtConstants.FOUR] = INDIVIDUAL;
		columnHeaders5[PlanMgmtConstants.FIVE] = FAMILY;
		columnHeaders5[PlanMgmtConstants.SIX] = "Default Coinsurance";
		columnHeaders5[PlanMgmtConstants.SEVEN] = INDIVIDUAL;
		columnHeaders5[PlanMgmtConstants.EIGHT] = FAMILY;
		columnHeaders5[PlanMgmtConstants.NINE] = INDIVIDUAL;
		columnHeaders5[PlanMgmtConstants.TEN] = FAMILY;
		
		dataList.add(columnHeaders1);
		dataList.add(columnHeaders2);
		dataList.add(columnHeaders3);
		dataList.add(columnHeaders4);
		dataList.add(columnHeaders5);
		
		for(int i = 0; i < planHealthCostData.size(); i++){
			String[] planDentalCosts = new String[PlanMgmtConstants.ELEVEN];
			PlanHealthCost planHealthCost = (PlanHealthCost) planHealthCostData.get(i);
			
			planDentalCosts[0] = getBenefitNameFromMap(planHealthCost.getName(),healthBenefitMap);
			planDentalCosts[PlanMgmtConstants.ONE] = planHealthCost.getInNetWorkInd()!= null?planHealthCost.getInNetWorkInd().toString():"";
			planDentalCosts[PlanMgmtConstants.TWO] = planHealthCost.getInNetWorkFly()!= null?planHealthCost.getInNetWorkFly().toString():"";
			planDentalCosts[PlanMgmtConstants.THREE] = planHealthCost.getCombDefCoinsNetworkTier1()!= null?planHealthCost.getCombDefCoinsNetworkTier1().toString():"";
			planDentalCosts[PlanMgmtConstants.FOUR] = planHealthCost.getInNetworkTier2Ind()!= null?planHealthCost.getInNetworkTier2Ind().toString():"";
			planDentalCosts[PlanMgmtConstants.FIVE] = planHealthCost.getInNetworkTier2Fly()!= null?planHealthCost.getInNetworkTier2Fly().toString():"";
			planDentalCosts[PlanMgmtConstants.SIX] = planHealthCost.getCombDefCoinsNetworkTier2()!= null?planHealthCost.getCombDefCoinsNetworkTier2().toString():"";
			planDentalCosts[PlanMgmtConstants.SEVEN] = planHealthCost.getOutNetworkInd()!= null?planHealthCost.getOutNetworkInd().toString():"";
			planDentalCosts[PlanMgmtConstants.EIGHT] = planHealthCost.getOutNetworkFly()!= null?planHealthCost.getOutNetworkFly().toString():"";
			planDentalCosts[PlanMgmtConstants.NINE] = planHealthCost.getCombinedInOutNetworkInd()!= null?planHealthCost.getCombinedInOutNetworkInd().toString():"";
			planDentalCosts[PlanMgmtConstants.TEN] = planHealthCost.getCombinedInOutNetworkFly()!= null?planHealthCost.getCombinedInOutNetworkFly().toString():"";
			
			dataList.add(planDentalCosts);
		}
		columnHeaders[0]=BENEFIT_NAME;
		
		/*	Renamed Benefit Attributes*/
		
		columnHeaders[PlanMgmtConstants.ONE]=NETWORKT1COPAYVAL;
		columnHeaders[PlanMgmtConstants.TWO]=NETWORKT1COPAYATTR;
		columnHeaders[PlanMgmtConstants.THREE]=NETWORKT1COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.FOUR]=NETWORKT1COINSURANCEATTR;
		columnHeaders[PlanMgmtConstants.FIVE]=SUBJECTTOINNETWORKDEDUCTIBLE;
		
		columnHeaders[PlanMgmtConstants.SIX]=NETWORKT2COPAYVAL;
		columnHeaders[PlanMgmtConstants.SEVEN]=NETWORKT2COPAYATTR;
		columnHeaders[PlanMgmtConstants.EIGHT]=NETWORKT2COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.NINE]=NETWORKT2COINSURANCEATTR;
		columnHeaders[PlanMgmtConstants.TEN]=SUBJECTTOOUTOFNETWORKDEDUCTIBLE;

		columnHeaders[PlanMgmtConstants.ELEVEN]=EXCLUDEFROMINNETWORKMOOP;
		columnHeaders[PlanMgmtConstants.TWELVE]=OUTNETWORKCOPAYVAL;
		columnHeaders[PlanMgmtConstants.THIRTEEN]=OUTNETWORKCOPAYATTR;
		columnHeaders[PlanMgmtConstants.FOURTEEN]=OUTNETWORKCOINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.FIFTEEN]=OUTNETWORKCOINSURANCEATTR;
		columnHeaders[PlanMgmtConstants.SIXTEEN]=EXCLUDEDFROMOUTOFNETWORKMOOP;

		columnHeaders[PlanMgmtConstants.SEVENTEEN]=LIMITATION;
		columnHeaders[PlanMgmtConstants.EIGHTEEN]=LIMITATIONATTRIBUTE;
		columnHeaders[PlanMgmtConstants.NINTEEN]=EXCEPTIONS;
		
		columnHeaders12[0] = EMPTYSTRING;
		columnHeaders12[PlanMgmtConstants.ONE] = INNETWORK;
		columnHeaders12[PlanMgmtConstants.TWO] = OUTOFNETWORK;
		
		columnHeaders13[0] = EMPTYSTRING;
		columnHeaders13[PlanMgmtConstants.ONE] = TIER1;
		columnHeaders13[PlanMgmtConstants.TWO] = TIER2;
		/*	Renamed Benefit Attributes*/
		
		
		
		
		/*	New Benefit Attributes	*/
		String[] plans = new String[PlanMgmtConstants.TWO];
		columnHeaders1[PlanMgmtConstants.ONE] = plan.getName();
		columnHeaders1[PlanMgmtConstants.TWO] = plan.getIssuerPlanNumber();
		
		columnHeaders2[PlanMgmtConstants.ONE] = getRequiredDateInFormat(String.valueOf(plan.getStartDate()));
		columnHeaders3[PlanMgmtConstants.ONE] = getRequiredDateInFormat(String.valueOf(plan.getEndDate()));
				
		dataList.add(columnHeaders12);
		dataList.add(columnHeaders13);		
		dataList.add(columnHeaders);
		
		for (int i = 0; i < healthBenefit.size(); i++) {
			String[] benefits = new String[PlanMgmtConstants.TWENTY];
						
			benefits[0] = getBenefitNameFromMap(healthBenefit.get(i).getName(),healthBenefitMap);
			benefits[PlanMgmtConstants.ONE] = healthBenefit.get(i).getNetworkT1CopayVal();
			benefits[PlanMgmtConstants.TWO] = healthBenefit.get(i).getNetworkT1CopayAttr();
			benefits[PlanMgmtConstants.THREE] = healthBenefit.get(i).getNetworkT1CoinsurVal();			
			benefits[PlanMgmtConstants.FOUR] = healthBenefit.get(i).getNetworkT1CoinsurAttr();
			benefits[PlanMgmtConstants.FIVE] = healthBenefit.get(i).getSubjectToInNetworkDuductible();
			
			benefits[PlanMgmtConstants.SIX] = healthBenefit.get(i).getNetworkT2CopayVal();
			benefits[PlanMgmtConstants.SEVEN] = healthBenefit.get(i).getNetworkT2CopayAttr();
			benefits[PlanMgmtConstants.EIGHT] = healthBenefit.get(i).getNetworkT2CoinsurVal();			
			benefits[PlanMgmtConstants.NINE] = healthBenefit.get(i).getNetworkT2CoinsurAttr();
			benefits[PlanMgmtConstants.TEN] = healthBenefit.get(i).getSubjectToOutNetworkDeductible();
			
			benefits[PlanMgmtConstants.ELEVEN] = healthBenefit.get(i).getExcludedFromInNetworkMoop();

			benefits[PlanMgmtConstants.TWELVE] = healthBenefit.get(i).getOutOfNetworkCopayVal();
			benefits[PlanMgmtConstants.THIRTEEN] = healthBenefit.get(i).getOutOfNetworkCopayAttr();
			benefits[PlanMgmtConstants.FOURTEEN] = healthBenefit.get(i).getOutOfNetworkCoinsurVal();
			benefits[PlanMgmtConstants.FIFTEEN] = healthBenefit.get(i).getOutOfNetworkCoinsurAttr();
			benefits[PlanMgmtConstants.SIXTEEN] = healthBenefit.get(i).getExcludedFromOutOfNetworkMoop();
			
			benefits[PlanMgmtConstants.SEVENTEEN] = healthBenefit.get(i).getNetworkLimitation();
			benefits[PlanMgmtConstants.EIGHTEEN] = healthBenefit.get(i).getNetworkLimitationAttribute();
			benefits[PlanMgmtConstants.NINTEEN] = healthBenefit.get(i).getNetworkExceptions();
			
			dataList.add(benefits);
		}
		return dataList;
	}
	
	public static List<String[]> convertsQDPBenefit(List<PlanDentalBenefit> dentalBenefit,List<HierarchicalConfiguration> benefitList){
		
		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders = new String[PlanMgmtConstants.TWENTY];
		columnHeaders[0]=BENEFIT_NAME;
		
		/*	Renamed Benefit Attributes*/
		
		columnHeaders[PlanMgmtConstants.ONE]=NETWORKT1COPAYVAL;
		columnHeaders[PlanMgmtConstants.TWO]=NETWORKT1COPAYATTR;
		columnHeaders[PlanMgmtConstants.THREE]=NETWORKT1COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.FOUR]=NETWORKT1COINSURANCEATTR;

		columnHeaders[PlanMgmtConstants.FIVE]=NETWORKT2COPAYVAL;
		columnHeaders[PlanMgmtConstants.SIX]=NETWORKT2COPAYATTR;
		columnHeaders[PlanMgmtConstants.SEVEN]=NETWORKT2COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.EIGHT]=NETWORKT2COINSURANCEATTR;

		columnHeaders[PlanMgmtConstants.NINE]=OUTNETWORKCOPAYVAL;
		columnHeaders[PlanMgmtConstants.TEN]=OUTNETWORKCOPAYATTR;
		columnHeaders[PlanMgmtConstants.ELEVEN]=OUTNETWORKCOINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.TWELVE]=OUTNETWORKCOINSURANCEATTR;

		columnHeaders[PlanMgmtConstants.THIRTEEN]=LIMITATION;
		columnHeaders[PlanMgmtConstants.FOURTEEN]=LIMITATIONATTRIBUTE;
		columnHeaders[PlanMgmtConstants.FIFTEEN]=EXCEPTIONS;
		
		columnHeaders[PlanMgmtConstants.SIXTEEN]=SUBJECTTOINNETWORKDEDUCTIBLE;
		columnHeaders[PlanMgmtConstants.SEVENTEEN]=SUBJECTTOOUTOFNETWORKDEDUCTIBLE;
		columnHeaders[PlanMgmtConstants.EIGHTEEN]=EXCLUDEFROMINNETWORKMOOP;
		columnHeaders[PlanMgmtConstants.NINTEEN]=EXCLUDEDFROMOUTOFNETWORKMOOP;
		
		/*	New Benefit Attributes	*/
				
		dataList.add(columnHeaders);
		
		for (int i = 0; i < dentalBenefit.size(); i++) {
			String[] benefits = new String[PlanMgmtConstants.TWENTY];
						
			
			benefits[0] = getBenefitName(dentalBenefit.get(i).getName(),benefitList);
			benefits[PlanMgmtConstants.ONE] = dentalBenefit.get(i).getNetworkT1CopayVal();
			benefits[PlanMgmtConstants.TWO] = dentalBenefit.get(i).getNetworkT1CopayAttr();
			benefits[PlanMgmtConstants.THREE] = dentalBenefit.get(i).getNetworkT1CoinsurVal();			
			benefits[PlanMgmtConstants.FOUR] = dentalBenefit.get(i).getNetworkT1CoinsurAttr();
			
			benefits[PlanMgmtConstants.FIVE] = dentalBenefit.get(i).getNetworkT2CopayVal();
			benefits[PlanMgmtConstants.SIX] = dentalBenefit.get(i).getNetworkT2CopayAttr();
			benefits[PlanMgmtConstants.SEVEN] = dentalBenefit.get(i).getNetworkT2CoinsurVal();			
			benefits[PlanMgmtConstants.EIGHT] = dentalBenefit.get(i).getNetworkT2CoinsurAttr();

			benefits[PlanMgmtConstants.NINE] = dentalBenefit.get(i).getOutOfNetworkCopayVal();
			benefits[PlanMgmtConstants.TEN] = dentalBenefit.get(i).getOutOfNetworkCopayAttr();
			benefits[PlanMgmtConstants.ELEVEN] = dentalBenefit.get(i).getOutOfNetworkCoinsurVal();
			benefits[PlanMgmtConstants.TWELVE] = dentalBenefit.get(i).getOutOfNetworkCoinsurAttr();
			
			benefits[PlanMgmtConstants.THIRTEEN] = dentalBenefit.get(i).getNetworkLimitation();
			benefits[PlanMgmtConstants.FOURTEEN] = dentalBenefit.get(i).getNetworkLimitationAttribute();
			benefits[PlanMgmtConstants.FIFTEEN] = dentalBenefit.get(i).getNetworkExceptions();
			
			benefits[PlanMgmtConstants.SIXTEEN] = dentalBenefit.get(i).getSubjectToInNetworkDuductible();
			benefits[PlanMgmtConstants.SEVENTEEN] = dentalBenefit.get(i).getSubjectToOutNetworkDeductible();
			benefits[PlanMgmtConstants.EIGHTEEN] = dentalBenefit.get(i).getExcludedFromInNetworkMoop();
			benefits[PlanMgmtConstants.NINTEEN] = dentalBenefit.get(i).getExcludedFromOutOfNetworkMoop();
			
			dataList.add(benefits);
		}
		return dataList;
	}

public static List<String[]> convertsQDPBenefitFromMap(List<PlanDentalBenefit> dentalBenefit,Map<String, String> dentalBenefitMap, Plan plan, List<PlanDentalCost> planDentalCostData){
		
		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders1 = new String[PlanMgmtConstants.THREE];
		
		String[] columnHeaders2 = new String[PlanMgmtConstants.TWO];
		String[] columnHeaders3 = new String[PlanMgmtConstants.TWO];
		
		String[] columnHeaders4 = new String[PlanMgmtConstants.FIVE];
		String[] columnHeaders5 = new String[PlanMgmtConstants.ELEVEN];
		String[] columnHeaders12 = new String[PlanMgmtConstants.THREE];
		String[] columnHeaders13 = new String[PlanMgmtConstants.THREE];
		String[] columnHeaders = new String[PlanMgmtConstants.TWENTY];
		
		
		columnHeaders1[0]=PLANNAMEANDNUMBER;
		
		columnHeaders2[0]=PLANSTARTDATE;
		columnHeaders3[0]=PLANENDDATE;
		
		columnHeaders4[0] = EMPTYSTRING;
		columnHeaders4[PlanMgmtConstants.ONE] = INNETWORKTIER1;
		columnHeaders4[PlanMgmtConstants.TWO] = INNETWORKTIER2;
		columnHeaders4[PlanMgmtConstants.THREE] = OUTOFNETWORK;
		columnHeaders4[PlanMgmtConstants.FOUR] = COMBINEDINOUTNETWORK;
		
		
		columnHeaders5[0] = DEDUCTIBLES;
		columnHeaders5[PlanMgmtConstants.ONE] = INDIVIDUAL;
		columnHeaders5[PlanMgmtConstants.TWO] = FAMILY;
		columnHeaders5[PlanMgmtConstants.THREE] = DEFAULTCOINSURANCE;
		columnHeaders5[PlanMgmtConstants.FOUR] = INDIVIDUAL;
		columnHeaders5[PlanMgmtConstants.FIVE] = FAMILY;
		columnHeaders5[PlanMgmtConstants.SIX] = DEFAULTCOINSURANCE;
		columnHeaders5[PlanMgmtConstants.SEVEN] = INDIVIDUAL;
		columnHeaders5[PlanMgmtConstants.EIGHT] = FAMILY;
		columnHeaders5[PlanMgmtConstants.NINE] = INDIVIDUAL;
		columnHeaders5[PlanMgmtConstants.TEN] = FAMILY;
		
		dataList.add(columnHeaders1);
		dataList.add(columnHeaders2);
		dataList.add(columnHeaders3);
		dataList.add(columnHeaders4);
		dataList.add(columnHeaders5);
		
		for(int i = 0; i < planDentalCostData.size(); i++){
			String[] planDentalCosts = new String[PlanMgmtConstants.ELEVEN];
			PlanDentalCost planDentalCost = (PlanDentalCost) planDentalCostData.get(i);
			
			planDentalCosts[0] = getBenefitNameFromMap(planDentalCost.getName(),dentalBenefitMap);
			planDentalCosts[PlanMgmtConstants.ONE] = planDentalCost.getInNetWorkInd()!= null?planDentalCost.getInNetWorkInd().toString():"";
			planDentalCosts[PlanMgmtConstants.TWO] = planDentalCost.getInNetWorkFly()!= null?planDentalCost.getInNetWorkFly().toString():"";
			planDentalCosts[PlanMgmtConstants.THREE] = planDentalCost.getCombDefCoinsNetworkTier1()!= null?planDentalCost.getCombDefCoinsNetworkTier1().toString():"";
			planDentalCosts[PlanMgmtConstants.FOUR] = planDentalCost.getInNetworkTier2Ind()!= null?planDentalCost.getInNetworkTier2Ind().toString():"";
			planDentalCosts[PlanMgmtConstants.FIVE] = planDentalCost.getInNetworkTier2Fly()!= null?planDentalCost.getInNetworkTier2Fly().toString():"";
			planDentalCosts[PlanMgmtConstants.SIX] = planDentalCost.getCombDefCoinsNetworkTier2()!= null?planDentalCost.getCombDefCoinsNetworkTier2().toString():"";
			planDentalCosts[PlanMgmtConstants.SEVEN] = planDentalCost.getOutNetworkInd()!= null?planDentalCost.getOutNetworkInd().toString():"";
			planDentalCosts[PlanMgmtConstants.EIGHT] = planDentalCost.getOutNetworkFly()!= null?planDentalCost.getOutNetworkFly().toString():"";
			planDentalCosts[PlanMgmtConstants.NINE] = planDentalCost.getCombinedInOutNetworkInd()!= null?planDentalCost.getCombinedInOutNetworkInd().toString():"";
			planDentalCosts[PlanMgmtConstants.TEN] = planDentalCost.getCombinedInOutNetworkFly()!= null?planDentalCost.getCombinedInOutNetworkFly().toString():"";
			/*
			planDentalCosts[1] = planDentalCost.getInNetWorkInd()!= null?DOLLAR+planDentalCost.getInNetWorkInd().toString():"";
			planDentalCosts[PlanMgmtConstants.TWO] = planDentalCost.getInNetWorkFly()!= null?DOLLAR+planDentalCost.getInNetWorkFly().toString():"";
			planDentalCosts[PlanMgmtConstants.THREE] = planDentalCost.getCombDefCoinsNetworkTier1()!= null?DOLLAR+planDentalCost.getCombDefCoinsNetworkTier1().toString():"";
			planDentalCosts[PlanMgmtConstants.FOUR] = planDentalCost.getInNetworkTier2Ind()!= null?DOLLAR+planDentalCost.getInNetworkTier2Ind().toString():"";
			planDentalCosts[PlanMgmtConstants.FIVE] = planDentalCost.getInNetworkTier2Fly()!= null?DOLLAR+planDentalCost.getInNetworkTier2Fly().toString():"";
			planDentalCosts[PlanMgmtConstants.SIX] = planDentalCost.getCombDefCoinsNetworkTier2()!= null?DOLLAR+planDentalCost.getCombDefCoinsNetworkTier2().toString():"";
			planDentalCosts[PlanMgmtConstants.SEVEN] = planDentalCost.getOutNetworkInd()!= null?DOLLAR+planDentalCost.getOutNetworkInd().toString():"";
			planDentalCosts[8] = planDentalCost.getCombinedInOutNetworkInd()!= null?DOLLAR+planDentalCost.getCombinedInOutNetworkInd().toString():"";
			planDentalCosts[PlanMgmtConstants.NINE] = planDentalCost.getCombinedInOutNetworkFly()!= null?DOLLAR+planDentalCost.getCombinedInOutNetworkFly().toString():"";*/
			
			dataList.add(planDentalCosts);
		}
		columnHeaders12[0] = EMPTYSTRING;
		columnHeaders12[PlanMgmtConstants.ONE] = INNETWORK;
		columnHeaders12[PlanMgmtConstants.TWO] = OUTOFNETWORK;
		
		columnHeaders[0]=BENEFIT_NAME;
		
		columnHeaders13[0] = EMPTYSTRING;
		columnHeaders13[PlanMgmtConstants.ONE] = TIER1;
		columnHeaders13[PlanMgmtConstants.TWO] = TIER2;
		/*	Renamed Benefit Attributes*/
		
		columnHeaders[PlanMgmtConstants.ONE]=NETWORKT1COPAYVAL;
		columnHeaders[PlanMgmtConstants.TWO]=NETWORKT1COPAYATTR;
		columnHeaders[PlanMgmtConstants.THREE]=NETWORKT1COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.FOUR]=NETWORKT1COINSURANCEATTR;
		columnHeaders[PlanMgmtConstants.FIVE]=SUBJECTTOINNETWORKDEDUCTIBLE;

		columnHeaders[PlanMgmtConstants.SIX]=NETWORKT2COPAYVAL;
		columnHeaders[PlanMgmtConstants.SEVEN]=NETWORKT2COPAYATTR;
		columnHeaders[PlanMgmtConstants.EIGHT]=NETWORKT2COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.NINE]=NETWORKT2COINSURANCEATTR;
		columnHeaders[PlanMgmtConstants.TEN]=SUBJECTTOOUTOFNETWORKDEDUCTIBLE;
		
		columnHeaders[PlanMgmtConstants.ELEVEN]=EXCLUDEFROMINNETWORKMOOP;
		columnHeaders[PlanMgmtConstants.TWELVE]=OUTNETWORKCOPAYVAL;
		columnHeaders[PlanMgmtConstants.THIRTEEN]=OUTNETWORKCOPAYATTR;
		columnHeaders[PlanMgmtConstants.FOURTEEN]=OUTNETWORKCOINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.FIFTEEN]=OUTNETWORKCOINSURANCEATTR;
		columnHeaders[PlanMgmtConstants.SIXTEEN]=EXCLUDEDFROMOUTOFNETWORKMOOP;

		columnHeaders[PlanMgmtConstants.SEVENTEEN]=LIMITATION;
		columnHeaders[PlanMgmtConstants.EIGHTEEN]=LIMITATIONATTRIBUTE;
		columnHeaders[PlanMgmtConstants.NINTEEN]=EXCEPTIONS;
		
		
		/*	New Benefit Attributes	*/
		
		String[] plans = new String[PlanMgmtConstants.TWO];
		columnHeaders1[PlanMgmtConstants.ONE] = plan.getName();
		columnHeaders1[PlanMgmtConstants.TWO] = plan.getIssuerPlanNumber();
		
		columnHeaders2[PlanMgmtConstants.ONE] = getRequiredDateInFormat(String.valueOf(plan.getStartDate()));
		columnHeaders3[PlanMgmtConstants.ONE] = getRequiredDateInFormat(String.valueOf(plan.getEndDate()));
		
		dataList.add(columnHeaders12);
		dataList.add(columnHeaders13);
		dataList.add(columnHeaders);
		
		for (int i = 0; i < dentalBenefit.size(); i++) {
			String[] benefits = new String[PlanMgmtConstants.TWENTY];
						
			
			benefits[0] = getBenefitNameFromMap(dentalBenefit.get(i).getName(),dentalBenefitMap);
			benefits[PlanMgmtConstants.ONE] = dentalBenefit.get(i).getNetworkT1CopayVal();
			benefits[PlanMgmtConstants.TWO] = dentalBenefit.get(i).getNetworkT1CopayAttr();
			benefits[PlanMgmtConstants.THREE] = dentalBenefit.get(i).getNetworkT1CoinsurVal();			
			benefits[PlanMgmtConstants.FOUR] = dentalBenefit.get(i).getNetworkT1CoinsurAttr();
			benefits[PlanMgmtConstants.FIVE] = dentalBenefit.get(i).getSubjectToInNetworkDuductible();
			
			benefits[PlanMgmtConstants.SIX] = dentalBenefit.get(i).getNetworkT2CopayVal();
			benefits[PlanMgmtConstants.SEVEN] = dentalBenefit.get(i).getNetworkT2CopayAttr();
			benefits[PlanMgmtConstants.EIGHT] = dentalBenefit.get(i).getNetworkT2CoinsurVal();			
			benefits[PlanMgmtConstants.NINE] = dentalBenefit.get(i).getNetworkT2CoinsurAttr();
			benefits[PlanMgmtConstants.TEN] = dentalBenefit.get(i).getSubjectToOutNetworkDeductible();
			benefits[PlanMgmtConstants.ELEVEN] = dentalBenefit.get(i).getExcludedFromInNetworkMoop();

			benefits[PlanMgmtConstants.TWELVE] = dentalBenefit.get(i).getOutOfNetworkCopayVal();
			benefits[PlanMgmtConstants.THIRTEEN] = dentalBenefit.get(i).getOutOfNetworkCopayAttr();
			benefits[PlanMgmtConstants.FOURTEEN] = dentalBenefit.get(i).getOutOfNetworkCoinsurVal();
			benefits[PlanMgmtConstants.FIFTEEN] = dentalBenefit.get(i).getOutOfNetworkCoinsurAttr();
			benefits[PlanMgmtConstants.SIXTEEN] = dentalBenefit.get(i).getExcludedFromOutOfNetworkMoop();
			
			benefits[PlanMgmtConstants.SEVENTEEN] = dentalBenefit.get(i).getNetworkLimitation();
			benefits[PlanMgmtConstants.EIGHTEEN] = dentalBenefit.get(i).getNetworkLimitationAttribute();
			benefits[PlanMgmtConstants.NINTEEN] = dentalBenefit.get(i).getNetworkExceptions();
			
			dataList.add(benefits);
		}
		return dataList;
	}
	
	private static String getBenefitName(String benefitDBConstant,List<HierarchicalConfiguration> benefitList) {
		try {
			for(HierarchicalConfiguration sub : benefitList){
				String ConstName = sub.getString("ConstName");
				if (ConstName.equalsIgnoreCase(benefitDBConstant)) {
					return sub.getString("Name");
				}
			} 
		} catch (Exception e) {
			LOGGER.error("Benefit name not found ",e);
		}
		return benefitDBConstant;
	}
	
	//kunal's
	private static String getBenefitNameFromMap(String benefitDBConstant,Map<String, String> benefits) {
		
			String name = benefits.get(benefitDBConstant);
			
				if (name != null) {
					return name;
				}else {
		
		return benefitDBConstant;
				}
	}
	
	public static List<String[]> getFileHeader() {
		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders = new String[PlanMgmtConstants.TWENTY];
		
		columnHeaders[0]=BENEFIT_NAME;
		columnHeaders[PlanMgmtConstants.ONE]=NETWORKT1COPAYVAL;
		columnHeaders[PlanMgmtConstants.TWO]=NETWORKT1COPAYATTR;
		columnHeaders[PlanMgmtConstants.THREE]=NETWORKT1COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.FOUR]=NETWORKT1COINSURANCEATTR;

		columnHeaders[PlanMgmtConstants.FIVE]=NETWORKT2COPAYVAL;
		columnHeaders[PlanMgmtConstants.SIX]=NETWORKT2COPAYATTR;
		columnHeaders[PlanMgmtConstants.SEVEN]=NETWORKT2COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.EIGHT]=NETWORKT2COINSURANCEATTR;

		columnHeaders[PlanMgmtConstants.NINE]=OUTNETWORKCOPAYVAL;
		columnHeaders[PlanMgmtConstants.TEN]=OUTNETWORKCOPAYATTR;
		columnHeaders[PlanMgmtConstants.ELEVEN]=OUTNETWORKCOINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.TWELVE]=OUTNETWORKCOINSURANCEATTR;

		columnHeaders[PlanMgmtConstants.THIRTEEN]=LIMITATION;
		columnHeaders[PlanMgmtConstants.FOURTEEN]=LIMITATIONATTRIBUTE;
		columnHeaders[PlanMgmtConstants.FIFTEEN]=EXCEPTIONS;
		
		columnHeaders[PlanMgmtConstants.SIXTEEN]=SUBJECTTOINNETWORKDEDUCTIBLE;
		columnHeaders[PlanMgmtConstants.SEVENTEEN]=SUBJECTTOOUTOFNETWORKDEDUCTIBLE;
		columnHeaders[PlanMgmtConstants.EIGHTEEN]=EXCLUDEFROMINNETWORKMOOP;
		columnHeaders[PlanMgmtConstants.NINTEEN]=EXCLUDEDFROMOUTOFNETWORKMOOP;
		
		dataList.add(columnHeaders);
		
		return dataList;
	}
	
	
	// for issuercontroller
	public static List<String[]> convertObjectListToStringListFromMap(
			List<PlanHealthBenefit> healthBenefit,
			Map<String, String> healthBenefitMap) {

		List<String[]> dataList = new ArrayList<String[]>();
		String[] columnHeaders = new String[PlanMgmtConstants.TWENTY];
		columnHeaders[0] = BENEFIT_NAME;

		/* Renamed Benefit Attributes */

		columnHeaders[PlanMgmtConstants.ONE] = NETWORKT1COPAYVAL;
		columnHeaders[PlanMgmtConstants.TWO] = NETWORKT1COPAYATTR;
		columnHeaders[PlanMgmtConstants.THREE] = NETWORKT1COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.FOUR] = NETWORKT1COINSURANCEATTR;

		columnHeaders[PlanMgmtConstants.FIVE] = NETWORKT2COPAYVAL;
		columnHeaders[PlanMgmtConstants.SIX] = NETWORKT2COPAYATTR;
		columnHeaders[PlanMgmtConstants.SEVEN] = NETWORKT2COINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.EIGHT] = NETWORKT2COINSURANCEATTR;

		columnHeaders[PlanMgmtConstants.NINE] = OUTNETWORKCOPAYVAL;
		columnHeaders[PlanMgmtConstants.TEN] = OUTNETWORKCOPAYATTR;
		columnHeaders[PlanMgmtConstants.ELEVEN] = OUTNETWORKCOINSURANCEVAL;
		columnHeaders[PlanMgmtConstants.TWELVE] = OUTNETWORKCOINSURANCEATTR;

		columnHeaders[PlanMgmtConstants.THIRTEEN] = LIMITATION;
		columnHeaders[PlanMgmtConstants.FOURTEEN] = LIMITATIONATTRIBUTE;
		columnHeaders[PlanMgmtConstants.FIFTEEN] = EXCEPTIONS;

		columnHeaders[PlanMgmtConstants.SIXTEEN] = SUBJECTTOINNETWORKDEDUCTIBLE;
		columnHeaders[PlanMgmtConstants.SEVENTEEN] = SUBJECTTOOUTOFNETWORKDEDUCTIBLE;
		columnHeaders[PlanMgmtConstants.EIGHTEEN] = EXCLUDEFROMINNETWORKMOOP;
		columnHeaders[PlanMgmtConstants.NINTEEN] = EXCLUDEDFROMOUTOFNETWORKMOOP;

		/* New Benefit Attributes */

		dataList.add(columnHeaders);

		for (int i = 0; i < healthBenefit.size(); i++) {
			String[] benefits = new String[PlanMgmtConstants.TWENTY];

			benefits[0] = getBenefitNameFromMap(healthBenefit.get(i).getName(),
					healthBenefitMap);
			benefits[PlanMgmtConstants.ONE] = healthBenefit.get(i).getNetworkT1CopayVal();
			benefits[PlanMgmtConstants.TWO] = healthBenefit.get(i).getNetworkT1CopayAttr();
			benefits[PlanMgmtConstants.THREE] = healthBenefit.get(i).getNetworkT1CoinsurVal();
			benefits[PlanMgmtConstants.FOUR] = healthBenefit.get(i).getNetworkT1CoinsurAttr();

			benefits[PlanMgmtConstants.FIVE] = healthBenefit.get(i).getNetworkT2CopayVal();
			benefits[PlanMgmtConstants.SIX] = healthBenefit.get(i).getNetworkT2CopayAttr();
			benefits[PlanMgmtConstants.SEVEN] = healthBenefit.get(i).getNetworkT2CoinsurVal();
			benefits[PlanMgmtConstants.EIGHT] = healthBenefit.get(i).getNetworkT2CoinsurAttr();

			benefits[PlanMgmtConstants.NINE] = healthBenefit.get(i).getOutOfNetworkCopayVal();
			benefits[PlanMgmtConstants.TEN] = healthBenefit.get(i).getOutOfNetworkCopayAttr();
			benefits[PlanMgmtConstants.ELEVEN] = healthBenefit.get(i).getOutOfNetworkCoinsurVal();
			benefits[PlanMgmtConstants.TWELVE] = healthBenefit.get(i).getOutOfNetworkCoinsurAttr();

			benefits[PlanMgmtConstants.THIRTEEN] = healthBenefit.get(i).getNetworkLimitation();
			benefits[PlanMgmtConstants.FOURTEEN] = healthBenefit.get(i).getNetworkLimitationAttribute();
			benefits[PlanMgmtConstants.FIFTEEN] = healthBenefit.get(i).getNetworkExceptions();

			benefits[PlanMgmtConstants.SIXTEEN] = healthBenefit.get(i).getSubjectToInNetworkDuductible();
			benefits[PlanMgmtConstants.SEVENTEEN] = healthBenefit.get(i).getSubjectToOutNetworkDeductible();
			benefits[PlanMgmtConstants.EIGHTEEN] = healthBenefit.get(i).getExcludedFromInNetworkMoop();
			benefits[PlanMgmtConstants.NINTEEN] = healthBenefit.get(i).getExcludedFromOutOfNetworkMoop();

			dataList.add(benefits);
		}
		return dataList;
	}

	private static String getRequiredDateInFormat(String strDate){
		String requiredDate = null;
		if(strDate != null){
			requiredDate = DateUtil.changeFormat(strDate, DB_DATE_FORMAT, REQUIRED_DATE_FORMAT);
		}else{
			requiredDate = PlanMgmtConstants.EMPTY_STRING;
		}
		return requiredDate;
	}
}
