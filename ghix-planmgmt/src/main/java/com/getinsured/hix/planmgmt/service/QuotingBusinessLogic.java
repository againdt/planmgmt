package com.getinsured.hix.planmgmt.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface QuotingBusinessLogic {

	 List<Map<String, String>> processMemberData(List<Map<String, String>> memberList, String insuranceType);

	 List<Map<String, String>> processMemberData(List<Map<String, String>> memberList, String insuranceType, boolean keepChildOnly);
	 
	 String getHouseHoldType(List<Map<String, String>> processedMemberData, String insuranceType);
	 
	 String getFamilyTier(List<Map<String, String>> memberList);
	 
	 String getApplicantStateCode(List<Map<String, String>> houseHoldData);
	 	 
	 String buildHouseHoldAvailabilityLogic(String tableName, String houseHoldType);
	 
	 String buildEnrollmentAvailabilityLogic(String tableName, String effectiveDate, String isSpecialEnrollment, ArrayList<String> paramList);
	 
	 String buildEnrollmentAvailabilityLogic(String aliasOfPlanTable, String effectiveDate, String isSpecialEnrollment,
			String applicantZip, String applicantCounty, List<String> currentHiosIdList, List<String> paramList);
	 
	 String buildRateLogic(String inputTableName, String stateName, String houseHoldType, String familyTierLookupCode, String memberAge, String memberTobaccoUsage, String effectiveDate,
			 ArrayList<String> paramList);
		 
	 String buildCostSharingLogic(String inputTableName, String CsrVal);
	 
	 String getIHCFamilyTierLookupCode(List<Map<String, String>> memberList);
	 
	 boolean isAgeAllowForIHCQuoting(int age);
	 
	 int getApplicantAge(List<Map<String, String>> modifiedMemberList);
	 
	 String getFamilyTierLookupCode(String familyTier);
	 
	 Float calculatePremiumForAMEPlan(String rateOption, String hiosIssuerId, Float houseHoldPremium, int memberdataSize, String stateName, DecimalFormat df);
	 
	 String buildPlanCertificationLogic(String inputTableName, boolean issuerVerifiedFlag);
	 
	 String buildRateLogicForDentalQuoting(String rateTable, String issuerTable, String stateName, String houseHoldType, String familyTierLookupCode, String memberAge, String memberTobaccoUsage, String effectiveDate, String exchangeType, boolean doQuoteIssuerIHC, String IHCfamilyTierLookupCode, ArrayList<String> paramList);
	 
	 String buildPlanCertificationLogicJPA(String inputTableName, boolean issuerVerifiedFlag);
	 
	 void buildEnrollmentAvailabilityLogic(StringBuilder parentBuilder, String inputTableName,String isSpecialEnrollment);
	 
	 String getApplicantZipCode(List<Map<String, String>> memberList);
	 
	 List<Map<String, String>> convertRelationshipIdToString(List<Map<String, String>> memberList);
	 
	 String buildSearchRatingAreaLogic(String stateCode, ArrayList<String> paramList, List<Map<String, String>> processedMemberData, int ctr, String applicableYear, String configuredDB);
		
}
