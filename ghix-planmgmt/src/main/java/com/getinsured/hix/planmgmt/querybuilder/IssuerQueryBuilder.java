package com.getinsured.hix.planmgmt.querybuilder;

import com.getinsured.hix.planmgmt.util.PlanMgmtParamConstants;


public class IssuerQueryBuilder {

	private static String issuerListWithTenantQuery;
	private static String issuerListWithoutTenantQuery;
	private static String issuerListWithoutTenantAndInsTypeQuery;
	private static String getIssuerAndIssuerBrandNameByUserId;
	private static String getIssuerBrandNameListByIssuerList;
	
	public final static String getIssuerListWithTenantQuery(){
		if(IssuerQueryBuilder.issuerListWithTenantQuery != null){
			return IssuerQueryBuilder.issuerListWithTenantQuery;
		}
		StringBuilder buildquery = new StringBuilder(250);
		buildquery.append("SELECT DISTINCT i.id, i.name, i.hiosIssuerId ");
		buildquery.append("FROM ");
		buildquery.append("Plan p, Issuer i, TenantPlan tp, Tenant t ");
		buildquery.append("WHERE ");
		buildquery.append("i.id = p.issuer.id ");
		buildquery.append("AND t.id = tp.tenant.id ");
		buildquery.append("AND tp.plan.id = p.id ");			
		buildquery.append("AND p.state = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_STATE_CODE);
		buildquery.append(" AND p.insuranceType = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_INSURANCE_TYPE);
		buildquery.append(" AND t.code = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);
		//buildquery.append(" AND p.applicable_year = (:" + APPLICABLEYEAR + ") ");
		buildquery.append(" AND p.isDeleted = 'N' ");
		buildquery.append(" AND i.certificationStatus in ('CERTIFIED', 'RECERTIFIED' ) ");

		IssuerQueryBuilder.issuerListWithTenantQuery = buildquery.toString().trim();
   		return IssuerQueryBuilder.issuerListWithTenantQuery;
	}
	
	
	public final static String getIssuerListWithoutTenantQuery(){
		if(IssuerQueryBuilder.issuerListWithoutTenantQuery != null){
			return IssuerQueryBuilder.issuerListWithoutTenantQuery;
		}
		StringBuilder buildquery = new StringBuilder(250);
		buildquery.append("SELECT DISTINCT i.id, i.name, i.hiosIssuerId ");
		buildquery.append("FROM ");
		buildquery.append("Plan p, Issuer i ");
		buildquery.append("WHERE ");
		buildquery.append("i.id = p.issuer.id ");
		buildquery.append("AND p.state = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_STATE_CODE);
		buildquery.append(" AND p.insuranceType = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_INSURANCE_TYPE);
		//buildquery.append(" AND p.applicable_year = (:" + APPLICABLEYEAR + ") ");
		buildquery.append(" AND p.isDeleted = 'N' ");
		buildquery.append(" AND i.certificationStatus in ('CERTIFIED', 'RECERTIFIED' ) ");
		
		IssuerQueryBuilder.issuerListWithoutTenantQuery = buildquery.toString().trim();
   		return IssuerQueryBuilder.issuerListWithoutTenantQuery;
	}
	
	public final static String getIssuerListWithoutTenantAndInsTypeQuery(){
		if(IssuerQueryBuilder.issuerListWithoutTenantAndInsTypeQuery != null){
			return IssuerQueryBuilder.issuerListWithoutTenantAndInsTypeQuery;
		}
		StringBuilder buildquery = new StringBuilder(250);
		buildquery.append("SELECT DISTINCT i.id, i.name, i.hiosIssuerId ");
		buildquery.append("FROM ");
		buildquery.append("Plan p, Issuer i ");
		buildquery.append("WHERE ");
		buildquery.append("i.id = p.issuer.id ");
		buildquery.append("AND p.state = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_STATE_CODE);
		//buildquery.append(" AND p.applicable_year = (:" + APPLICABLEYEAR + ") ");
		buildquery.append(" AND p.isDeleted = 'N' ");
		buildquery.append(" AND i.certificationStatus in ('CERTIFIED', 'RECERTIFIED' ) ");
		
		IssuerQueryBuilder.issuerListWithoutTenantAndInsTypeQuery = buildquery.toString().trim();
   		return IssuerQueryBuilder.issuerListWithoutTenantAndInsTypeQuery;
	}
	
	public final static String getIssuerAndIssuerBrandNameByUserId(){
		if(null != IssuerQueryBuilder.getIssuerAndIssuerBrandNameByUserId){
			return IssuerQueryBuilder.getIssuerAndIssuerBrandNameByUserId;
		}
		StringBuilder buildquery = new StringBuilder(250);
		buildquery.append("SELECT i.id, ibn.brandName FROM Issuer i, IssuerIssuerRepresentativeMapping issuerrepmap, IssuerRepresentative issuerrep, IssuerBrandName ibn ");
		buildquery.append("WHERE ");
		buildquery.append("i.issuerBrandName.id = ibn.id ");
		buildquery.append("AND i.id = issuerrepmap.issuer.id ");
		buildquery.append("AND issuerrepmap.issuerRepresentative.id = issuerrep.id ");
		buildquery.append("AND issuerrep.userRecord.id = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_USER_ID);
		
		IssuerQueryBuilder.getIssuerAndIssuerBrandNameByUserId = buildquery.toString().trim();
		return IssuerQueryBuilder.getIssuerAndIssuerBrandNameByUserId;
	}
	
	public final static String getIssuerBrandNameListByIssuerList(){
		if(null != IssuerQueryBuilder.getIssuerBrandNameListByIssuerList){
			return IssuerQueryBuilder.getIssuerBrandNameListByIssuerList;
		}
		StringBuilder buildquery = new StringBuilder(250);
		buildquery.append("SELECT i.id, ibn.brandName FROM Issuer i, IssuerBrandName ibn ");
		buildquery.append("WHERE ");
		buildquery.append("i.issuerBrandName.id = ibn.id ");	
		buildquery.append("AND i.id in(:");
		buildquery.append(PlanMgmtParamConstants.PARAM_ISSUER_LIST);
		buildquery.append(")");
				
		IssuerQueryBuilder.getIssuerBrandNameListByIssuerList = buildquery.toString().trim();
		return IssuerQueryBuilder.getIssuerBrandNameListByIssuerList;
	}
	
}
