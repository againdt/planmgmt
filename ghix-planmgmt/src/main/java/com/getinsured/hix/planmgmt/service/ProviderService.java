package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

public interface ProviderService {

	 
	List<Map<String, String>> isPlanBelongsToProviderNetwork(List<Map<String, List<String>>> providerList, String planNetworkKey, String hasProviderData, String networkVeficationStatus, String stateCode);
	 
	List<Map<String, List<String>>>  getNetworkListByStrenussId(List<String> strenussIdList);
}
