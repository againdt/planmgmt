/*
 @Author Santanu De
 @created 10, June, 2015
 */
package com.getinsured.hix.planmgmt.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.DependsOn;


@DependsOn("dynamicPropertiesUtil")
public class PlanMgmtUtil {
	private static final Logger LOGGER = Logger.getLogger(PlanMgmtUtil.class);
	
	// close DB connection
	public static void closeDBConnection(Connection connection){			
	if (null != connection) {
		try {
			connection.close();
			connection = null;
			} catch (Exception sqlException) {
			LOGGER.error(sqlException.getMessage(),sqlException);
			}
		}
	}
		
	// close Resultset
	public static void closeResultSet(ResultSet resultSet){			
		if (null != resultSet) {
			try {
			    resultSet.close();
				resultSet = null;
			} catch (Exception sqlException) {
				LOGGER.error(sqlException.getMessage(),sqlException);
			}
		}
	}
			
	// close DB PreparedStatement
	public static void closePreparedStatement(PreparedStatement preparedStatement){			
		if (null != preparedStatement) {
			try {
				preparedStatement.close();
				preparedStatement = null;
			} catch (Exception sqlException) {
				LOGGER.error(sqlException.getMessage(),sqlException);
			}
		}
	}
	
	public static  Query setQueryParameters(Query query,
			HashMap<String, Object> parameterMap) {
		Set<Entry<String, Object>> parameterSet = parameterMap.entrySet();
		Iterator<Entry<String, Object>> parameterCursor = parameterSet
				.iterator();
		Entry<String, Object> parameterEntry = null;
		while (parameterCursor.hasNext()) {
			parameterEntry = parameterCursor.next();
			query.setParameter(parameterEntry.getKey(),	parameterEntry.getValue());
		}
		return query;
	}

	public static String removeDecimal(String decimalVal){
		if(StringUtils.isNotBlank(decimalVal) &&  decimalVal.contains(".0")) {
			return String.valueOf(decimalVal).split("\\.")[0];
		}else{
			return decimalVal;
		}			
	}
	
	public static String returnOneString(String sbcDocURL, String benefitURL) {
		String returnString = PlanMgmtConstants.EMPTY_STRING;
		if (!(sbcDocURL.isEmpty()) && !(benefitURL.isEmpty())) {
			returnString = sbcDocURL;
		} else if ((sbcDocURL.isEmpty()) && !(benefitURL.isEmpty())) {
			returnString = benefitURL;
		} else if (!(sbcDocURL.isEmpty()) && (benefitURL.isEmpty())) {
			returnString = sbcDocURL;
		} else {
			returnString = PlanMgmtConstants.EMPTY_STRING;
		}
		return returnString;
	}
	
	public static String getIssuerLogoURL(String logoURL, String hiosIssuerId, String encIssuerId, String appURL) {
		String issuerLogoURL = null;
		if(org.apache.commons.lang3.StringUtils.isNotBlank(logoURL)) {
			issuerLogoURL = logoURL;
		} else if(org.apache.commons.lang3.StringUtils.isNotBlank(hiosIssuerId)) {
			issuerLogoURL = appURL + PlanMgmtConstants.LOGO_BY_HIOSID_URL_PATH + hiosIssuerId;
		} else {
			issuerLogoURL = appURL + PlanMgmtConstants.LOGO_BY_ID_URL_PATH + encIssuerId;
		}
		return issuerLogoURL;
	}

	public static boolean isURLValid(String checkURL) {
		try {
			if (checkURL.matches("(?i).*https.*") || checkURL.matches("(?i).*http.*") || checkURL.matches("(?i).*www.*") || checkURL.matches("(?i).*ftp.*")) {
				return true;
			} 
			return false;
		} catch (RuntimeException e) {
			LOGGER.error("Runtime exception occurred while url validation", e);
			return false;
		}
	}
	
	public static String formatNetworkKey(String networkKey, String hiosIssuerId, String applicableYear, String stateCode){
		// Ref jira HIX-87747
		// If state code is CA then add HIOS Issuer Id as prefix, applicable year as suffix with network key. i.e 45672-CAN003-2016
		if(PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode)){
			return hiosIssuerId + "-" + networkKey + "-" + applicableYear;
		}else{
			return networkKey;
		}
		
	}
	
	
	public static boolean isAllFamilyMembersAbove18years(List<Map<String, String>> memberList){
		int lessThan19YearsOldCtr = 0;
		for (Map<String, String> member : memberList) {
			if(Integer.parseInt(member.get(PlanMgmtConstants.AGE)) <= PlanMgmtConstants.EIGHTEEN){
				lessThan19YearsOldCtr ++;
			}
		}
		
		// HIX-88279 :: If all the members of the household are below 19 then don't quote AME plans
		if(memberList.size() != lessThan19YearsOldCtr){	
			return true;
		}else{
			return false;
		}
	}
	
	
	public static Integer getCurrentYear(){
		int currnetYear = 0;
		try{
	      	currnetYear = TSCalendar.getInstance().get(Calendar.YEAR); 
		}catch(Exception ex){
			LOGGER.error("Exception occured in getCurrentYear: ", ex);
		}
		return currnetYear;
	}

}
