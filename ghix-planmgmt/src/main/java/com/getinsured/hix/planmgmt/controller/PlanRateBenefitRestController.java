/**
 * PlanRateBenefitRestController.java
 * @author chalse_v
 * @version 1.0
 * @since Mar 20, 2013 
 */
package com.getinsured.hix.planmgmt.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.Produces;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.WebServiceIOException;

import com.getinsured.hix.dto.plandisplay.PlanDentalBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanDentalCostDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthCostDTO;
import com.getinsured.hix.dto.planmgmt.EmployerRequest;
import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingRequestDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingResponseDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanMemberRequestDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanPeriodLoopDTO;
import com.getinsured.hix.dto.planmgmt.LowestPremiumRequestDTO;
import com.getinsured.hix.dto.planmgmt.LowestPremiumResponseDTO;
import com.getinsured.hix.dto.planmgmt.Member;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitResponse;
import com.getinsured.hix.dto.prescription.PrescriptionSearchRequest;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.dto.shop.ShopResponse.ResponseType;
import com.getinsured.hix.dto.shop.employer.eps.EmployeePlanDTO;
import com.getinsured.hix.dto.shop.employer.eps.EmployerQuotingDTO;
import com.getinsured.hix.dto.shop.employer.eps.PlanDTO;
import com.getinsured.hix.dto.shop.mlec.MLECRestRequest;
import com.getinsured.hix.dto.shop.mlec.MLECRestResponse;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.service.PlanCostAndBenefitsService;
import com.getinsured.hix.planmgmt.service.PlanDataDTOHelper;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.planmgmt.service.PlanRateBenefitService;
import com.getinsured.hix.planmgmt.service.ProviderService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.planmgmt.util.TaskAsCallable;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
//import com.wordnik.swagger.annotations.ApiOperation;
//import com.wordnik.swagger.annotations.ApiResponse;
import com.getinsured.timeshift.TimeShifterUtil;




@Controller
@DependsOn("dynamicPropertiesUtil")
@RequestMapping(value = "/planratebenefit")
public class PlanRateBenefitRestController {	
	private static final Logger LOGGER = Logger.getLogger(PlanRateBenefitRestController.class);
	
	private static final String RESPONSE_DATA = "responseData"; 
	private static final String REQUIRED_DATE_FORMAT = "yyyy-MM-dd";
	@Value("#{configProp['appUrl']}") 
	private String appUrl;
	
	@Autowired
	private PlanRateBenefitService planRateBenefitService;
	@Autowired
	private PlanCostAndBenefitsService planCostAndBenefitsService;
	@Autowired 
	private ProviderService providerService;
	@Autowired
	private PlanMgmtService planMgmtService;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	
	/**
	 * Constructor.
	 */
	public PlanRateBenefitRestController() {}
			
//	@RequestMapping(value = "/welcome", method = RequestMethod.GET) // for testing purpose
//	@ResponseBody public String welcome()
//	{
//		LOGGER.info("Welcome to GHIX-Planmgmt module, invoke PlanRateBenefitRestController()");		
//		
//		return "Welcome to GHIX-Planmgmt module, invoke PlanRateBenefitRestController";
//	}
	
		
	/**
	 * Method to handle ReST web service request of 'getEmployerPlanRate' for given criteria.
	 *
	 * @param employerReq The EmployerRequest instance.
	 * @return String The ShopResponse as stringified XML.
	 */
	// This API is used for Employer Plan selection. We are computing rate for each reference plan for employee list
//	@ApiOperation(value="getemployerplanrate", notes="This API takes Census Data as input e.g. Age, Relation, Employer Primary ZIP and County and Plan ID for retriving employer plan rate")
//	@ApiResponse(value="returns Employer quoting information")
	@RequestMapping(value = "/getemployerplanrate", method = RequestMethod.POST)
	public @ResponseBody String getEmployerPlanRate(@RequestBody EmployerQuotingDTO employerQuotingDTO) {
		LOGGER.info("Beginning to process getEmployerPlanRate().");
		if(LOGGER.isDebugEnabled()) {
			if(null != employerQuotingDTO) {
				StringBuilder buildStr = new StringBuilder();
				buildStr.append("Request data ===>");		
				buildStr.append(" Effective date : ");
				buildStr.append(employerQuotingDTO.getEffectiveDate());
				buildStr.append(" empPrimaryZip : ");
				buildStr.append(employerQuotingDTO.getEmpPrimaryZip());
				buildStr.append(" empCountyCode : ");
				buildStr.append(employerQuotingDTO.getEmpCountyCode());				
				buildStr.append(" censusList : ");
				buildStr.append(employerQuotingDTO.getCensusList().toString());				
				LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));
			} else {
				LOGGER.debug("getEmployerPlanRate: input params is null");
			}
		}
		
		ShopResponse shopResponse = new ShopResponse();
		
		if(null == employerQuotingDTO 					
				|| null == employerQuotingDTO.getEffectiveDate()
				|| 0 == employerQuotingDTO.getEffectiveDate().trim().length()
				|| null == employerQuotingDTO.getEmpPrimaryZip()
				|| 0 == employerQuotingDTO.getEmpPrimaryZip().trim().length()
				|| null == employerQuotingDTO.getEmpCountyCode()
				|| 0 == employerQuotingDTO.getEmpCountyCode().trim().length()				
				|| null == employerQuotingDTO.getPlanDTOList()
				|| employerQuotingDTO.getPlanDTOList().isEmpty()
				|| null == employerQuotingDTO.getCensusList()
				|| employerQuotingDTO.getCensusList().isEmpty()	
				) {
			shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			shopResponse.setErrMsg("Invalid request parameters.");
		}
		else {
			try {				
				getEmployerPlanRateHelper(employerQuotingDTO);
				shopResponse.addResponseData(EmployerQuotingDTO.class.getName(), employerQuotingDTO);
				shopResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
			catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("Exception occurrecd while processing request. Exception:",webServiceIOException);	
				shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_EMPLOYEE_PLAN_RATE.code);
				shopResponse.setErrMsg(webServiceIOException.getStackTrace().toString());					
			}
			catch(Exception ex) {
				LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);				
				shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_EMPLOYEE_PLAN_RATE.code);
				shopResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
			}
		}

		LOGGER.info("Processing current request of getEmployerPlanRate() is done.");
		
		return shopResponse.sendResponse(ResponseType.XML);	
	}
	
	
	public EmployerQuotingDTO getEmployerPlanRateHelper(EmployerQuotingDTO employerQuotingDTO){
		try{
			if(null == employerQuotingDTO 					
				|| null == employerQuotingDTO.getEffectiveDate()
				|| 0 == employerQuotingDTO.getEffectiveDate().trim().length()
				|| null == employerQuotingDTO.getEmpPrimaryZip()
				|| 0 == employerQuotingDTO.getEmpPrimaryZip().trim().length()
				|| null == employerQuotingDTO.getEmpCountyCode()
				|| 0 == employerQuotingDTO.getEmpCountyCode().trim().length()				
				|| null == employerQuotingDTO.getPlanDTOList()
				|| employerQuotingDTO.getPlanDTOList().isEmpty()
				|| null == employerQuotingDTO.getCensusList()
				|| employerQuotingDTO.getCensusList().isEmpty()	
				) {
				return employerQuotingDTO;
			}
			else {
			
				List<ArrayList<ArrayList<Map<String, String>>>> employeeList = employerQuotingDTO.getCensusList();				
				String empPrimaryZip = employerQuotingDTO.getEmpPrimaryZip().trim(); // employer primary zipcode
				String empCountyCode = employerQuotingDTO.getEmpCountyCode().trim(); // employer primary county code
				List<PlanDTO> planList = employerQuotingDTO.getPlanDTOList(); // reference plan id list
				String effectiveDate = employerQuotingDTO.getEffectiveDate().trim(); // effective date	
				List<PlanDTO> newPlanDTOList = new ArrayList<PlanDTO>();
				
				// process each reference plan 
				for(PlanDTO planDTO : planList){
					String planId =  planDTO.getPlanId().toString();
			    	if(LOGGER.isDebugEnabled()) {
			    		LOGGER.debug("Compute rate for Plan id : " + SecurityUtil.sanitizeForLogging(planId));
			    	}
					List<EmployeePlanDTO> employeePlanDTOs = new ArrayList<EmployeePlanDTO>();					 
					float totalemployeePremium = 0;
					
					// compute rate for each employee on household basis i.e employee + his/her family
					for (List<ArrayList<Map<String, String>>> employee: employeeList) {					
						int empId = Integer.parseInt(employee.get(0).get(0).get("id"));						
						List<Map<String, String>> employeeCensusData = employee.get(0);
						Map<String,Float> responseData ;						
						float employeePremium = 0;	
						EmployeePlanDTO employeePlanDTO = new EmployeePlanDTO();
					
						// use employee's zipcode, county code for quoting
						responseData = planRateBenefitService.getEmployeePlanRate(employeeCensusData, empPrimaryZip, empCountyCode, planId, effectiveDate, false);
						
						// if plans not found using employee's own zipcode, county code then use employer's zipcode, county code for quoting
						if(responseData.isEmpty()){
							responseData = planRateBenefitService.getEmployeePlanRate(employeeCensusData, empPrimaryZip, empCountyCode, planId, effectiveDate, true);
							if(!responseData.isEmpty()){	
								employeePremium = responseData.get("indvPremium");	 //  each employee's own premium						
							}
						}
						else{
							employeePremium = responseData.get("indvPremium");	 //  each employee's own premium					
						}
						
						totalemployeePremium = totalemployeePremium + employeePremium;		// Adding each employee's own premium				
						
						employeePlanDTO.setEmployeeId(empId);
							
						if(!responseData.isEmpty()){
							employeePlanDTO.setTotalPremium(responseData.get("totalPremium")); // each employee's household premium	
							employeePlanDTO.setIndividualPremium(employeePremium); //  each employee's own premium	
							employeePlanDTO.setDependentPremium(responseData.get("depenPremium")); //  each employee's dependents premium	
						}
						employeePlanDTOs.add(employeePlanDTO);
					}
					
					// if any plan does not have rate, then don't add that plan in response DTO list
					if(Float.floatToRawIntBits(totalemployeePremium) != 0){
						PlanDTO newplanDTO = new PlanDTO();
						newplanDTO.setPlanId(planDTO.getPlanId());	// reset plan id
						newplanDTO.setIssuerPlanNumber(planDTO.getIssuerPlanNumber()); // reset hios plan number
						newplanDTO.setTierName(planDTO.getTierName()); // reset plan tier name
						newplanDTO.setSumEmployeesPremium(totalemployeePremium); // sum of all employee's own premium	
						newplanDTO.setEmployeePlanDTOs(employeePlanDTOs); // set employee plan DTO 
						newPlanDTOList.add(newplanDTO);
						newplanDTO = null;	
					}	
					
				}				
				
				employerQuotingDTO.setPlanDTOList(newPlanDTOList);	
				newPlanDTOList = null;
			}
			
			return employerQuotingDTO;
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception occurrecd inside getEmployerPlanRateHelper(). Exception:", ex);	
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occurrecd inside getEmployerPlanRateHelper()", ex);
			return employerQuotingDTO;
		}
		
	}
	/**
	 * Method to handle ReST web service request of 'getHouseholdPlanRateBenefits' for given criteria. 
	 *
	 * @param planRateBenefitRequest The PlanRateBenefitRequest instance.
	 * @return String The PlanRateBenefitResponse as stringified JSON.
	 */
//	@ApiOperation(value="household", notes="This API takes Insurance Type, Drug Names List, Issuer Verification Status, members details e.g. (age, relation, zip and county), ehb covered and market type etc… and retrive household plan rate benefit information")
//	@ApiResponse(value="returns Plan Information, Issuer Information, Plan Benefits, Plan Costs and Provider Information")
	@RequestMapping(value = "/household", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody PlanRateBenefitResponse getHouseholdPlanRateBenefits(@RequestBody PlanRateBenefitRequest planRateBenefitRequest)
    {
		long time = System.currentTimeMillis();
    	PlanRateBenefitResponse planRateBenefitResponse = new PlanRateBenefitResponse();
    	Map<String, Object> cRequestParams = planRateBenefitRequest.getRequestParameters();
    	if(null == cRequestParams 
				|| null == cRequestParams.get("costSharing")
				|| null == cRequestParams.get("effectiveDate")
				|| null == cRequestParams.get("groupId")
				|| null == cRequestParams.get("insType")
				|| null == cRequestParams.get("memberList")
				|| null == cRequestParams.get("planIdStr")
				|| null == cRequestParams.get("showCatastrophicPlan")
				|| null == cRequestParams.get("planLevel")
				|| null == cRequestParams.get("ehbCovered")
				|| null == cRequestParams.get("providers")
				|| null == cRequestParams.get("marketType")
				|| null == cRequestParams.get("isSpecialEnrollment")) {
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}
    	else 
    	{
    		SimpleDateFormat sdf = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
    		try{
    			Date effectiveDate = null; 
    			sdf.setLenient(false);
    			effectiveDate = sdf.parse(cRequestParams.get("effectiveDate").toString().trim());
    		List<PlanRateBenefit> planRateBenefitList = getHouseholdPlanRateBenefitsHelper(planRateBenefitRequest);
	    		
    			if( planRateBenefitList.size() ==0){
    			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
	    			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_HOUSEHOLD_PLAN_RATE_BENEFITS.code);
		    		planRateBenefitResponse.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
    		}
	    		else{
    			planRateBenefitResponse.setPlanRateBenefitList(planRateBenefitList);
    			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
	    		}
    		}	
	    	catch (ParseException  e){
	    		LOGGER.error("Error while parsing coverage start date ",e);
	    		planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_EFFECTIVE_DATE_FORMAT);
    		}
    		LOGGER.warn("Received household-API, response for Plan ID " + cRequestParams.get("planIdStr") + " in  " + (System.currentTimeMillis() - time) + " ms");
    	}

    	return planRateBenefitResponse;
    }
	
	/**
	 * Method to handle non ReST web service request of 'getHouseholdPlanRateBenefits' for given criteria. 
	 *
	 */
	
	public  List<PlanRateBenefit> getHouseholdPlanRateBenefitsHelper( PlanRateBenefitRequest planRateBenefitRequest) {
		LOGGER.info("Beginning to process current request of getHouseholdPlanRateBenefits().");
		List<PlanRateBenefit> responseData = new ArrayList<PlanRateBenefit>();
		
		Map<String, Object> cRequestParams = planRateBenefitRequest.getRequestParameters();
		List<Map<String, String>> memberList = (ArrayList<Map<String, String>>) cRequestParams.get("memberList");
		if(null == cRequestParams.get("costSharing")
				|| null == cRequestParams.get("effectiveDate")
				|| null == cRequestParams.get("groupId")
				|| null == cRequestParams.get("insType")
				|| null == cRequestParams.get("memberList")
				|| null == cRequestParams.get("planIdStr")
				|| null == cRequestParams.get("showCatastrophicPlan")
				|| null == cRequestParams.get("planLevel")
				|| null == cRequestParams.get("ehbCovered")
				|| null == cRequestParams.get("providers")
				|| null == cRequestParams.get("marketType")
				|| null == cRequestParams.get("isSpecialEnrollment")
				|| memberList.isEmpty()) {
			return responseData;
		}
		else {
			try {
				Validate.notNull(planRateBenefitService, "PlanRateBenefitService is improperly configured.");
								
				String insType = cRequestParams.get("insType").toString().trim();
				String effectiveDate = cRequestParams.get("effectiveDate").toString().trim();
				String costSharing = cRequestParams.get("costSharing").toString().trim();
				int groupId = (cRequestParams.get("groupId")==null || cRequestParams.get("groupId").toString().isEmpty()) ? 0 : Integer.parseInt(cRequestParams.get("groupId").toString());
				String planIdStr = cRequestParams.get("planIdStr").toString().trim();
				boolean showCatastrophicPlan = (Boolean) cRequestParams.get("showCatastrophicPlan");
				String planLevel= (String) cRequestParams.get("planLevel");
				String ehbCovered= (String) cRequestParams.get("ehbCovered");				
				List<Map<String, List<String>>> providers = (List<Map<String, List<String>>>) cRequestParams.get("providers");
				String marketType=(String) cRequestParams.get("marketType");
				String isSpecialEnrollment=(String) cRequestParams.get("isSpecialEnrollment");
				int issuerId = (cRequestParams.get("issuerId") == null || cRequestParams.get("issuerId").toString().isEmpty()) ? 0 : Integer.parseInt(cRequestParams.get("issuerId").toString()); // default issuer id is 0
				boolean issuerVerifiedFlag = (cRequestParams.get("issuerVerifiedFlag") == null) ? true : (Boolean) cRequestParams.get("issuerVerifiedFlag");  // default issuerVerifiedFlag is true
				String exchangeType = (cRequestParams.get("exchangeType") == null || cRequestParams.get("exchangeType").toString().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.ON : cRequestParams.get("exchangeType").toString().trim(); // default value is ON 
				String tenant = (cRequestParams.get("tenant") == null || cRequestParams.get("tenant").toString().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.TENANT_GINS : cRequestParams.get("tenant").toString().trim(); // default value is GINS
				String hiosPlanNumber = (cRequestParams.get("hiosPlanNumber") == null) ? PlanMgmtConstants.EMPTY_STRING : cRequestParams.get("hiosPlanNumber").toString().trim(); // default value is empty string
				String keepOnly =  (cRequestParams.get("keepOnly") == null) ? "N" : cRequestParams.get("keepOnly").toString().trim(); // default value is N 
				String eliminatePlanIds = (String) cRequestParams.get("eliminatePlanIds");					
				List<PrescriptionSearchRequest> prescriptionSearchRequstList = (null != planRateBenefitRequest.getPrescriptionRequestList() ? planRateBenefitRequest.getPrescriptionRequestList() : new ArrayList<PrescriptionSearchRequest>());
				boolean minimizePlanData = (cRequestParams.get("minimizePlanData") == null) ? false : (Boolean) cRequestParams.get("minimizePlanData");  // default issuerVerifiedFlag is true
				String restrictHiosIds = (cRequestParams.get("restrictHiosIds") == null) ? PlanMgmtConstants.ON : cRequestParams.get("restrictHiosIds").toString().trim();  // Default value is ON
				String showPUFPlans = (cRequestParams.get("showPufPlans") == null) ? PlanMgmtConstants.EMPTY_STRING : cRequestParams.get("showPufPlans").toString().trim();	// Default value is NO
				String currentHiosIssuerID = (cRequestParams.get("currentHiosIssuerID") == null) ? null : cRequestParams.get("currentHiosIssuerID").toString().trim();
				String isPlanChange = (cRequestParams.get("isPlanChange") == null) ? PlanMgmtConstants.N : cRequestParams.get("isPlanChange").toString().trim(); // Default value is N (No)

				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
				// HIX-56820 if strenuss id passed in request, in that case pull network keys for each strenuss id
				List<String> strenussIdList = (List<String>) cRequestParams.get("strenussIdList");
				
				
				if ((!PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode)) && (strenussIdList != null && !strenussIdList.isEmpty())){
					providers = providerService.getNetworkListByStrenussId(strenussIdList);
				}
				
		    	if(LOGGER.isDebugEnabled()) {
		    		LOGGER.debug("MEMBER INFO ==== "+ SecurityUtil.sanitizeForLogging(memberList.toString()));
		    	}
				responseData = planRateBenefitService.getHouseholdPlanRateBenefits(memberList, insType, effectiveDate,
						costSharing, groupId, planIdStr, showCatastrophicPlan, planLevel, ehbCovered, providers,
						marketType, isSpecialEnrollment, issuerId, issuerVerifiedFlag, exchangeType, tenant,
						minimizePlanData, hiosPlanNumber, keepOnly, prescriptionSearchRequstList, eliminatePlanIds,
						restrictHiosIds, showPUFPlans, currentHiosIssuerID, isPlanChange);

				Validate.notNull(responseData, "Invalid response returned from business method.");
			}
			catch(Exception ex) {
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch plans for the household.",ex);
				LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);
				return responseData;
			}
		}

		LOGGER.info("Processing current request of getHouseholdPlanRateBenefits() is done.");
		return responseData;
	}
	/**
	 * Method to handle ReST web service request of 'getBenchmarkRate' for given criteria. 
	 *
	 * @param benchmarkRateReq The BenchmarkRateRequest instance.
	 * @return String The PlanRateBenefitResponse as stringified JSON.
	 */
//	@ApiOperation(value="/rate/benchmark", notes="This API retrive the benchmark plan information")
//	@ApiResponse(value="returns the plan rate benefit of benchmark plan")
	@RequestMapping(value = "/rate/benchmark", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody PlanRateBenefitResponse getBenchmarkRate(@RequestBody PlanRateBenefitRequest planRateBenefitRequest) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Beginning to process current request of getBenchmarkRate().");
		}
		PlanRateBenefitResponse planRateBenefitResponse = new PlanRateBenefitResponse();

		if(null == planRateBenefitRequest || null == planRateBenefitRequest.getRequestParameters()) {
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}
		else {
			Map<String, Object> cRequestParams = planRateBenefitRequest.getRequestParameters();
			List<Map<String, String>> censusData = (ArrayList<Map<String, String>>) cRequestParams.get("censusData");		
			if(null == cRequestParams 
					|| null == cRequestParams.get("censusData")
					|| null == cRequestParams.get("coverageStartDate") 
					|| censusData.isEmpty()) {
				planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			}else {
				// benchmark helper class
				planRateBenefitResponse= this.getBenchmarkRateHelper(planRateBenefitRequest);
			}	
		}

		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Processing current request of getBenchmarkRate() is done.");
		}	
		return planRateBenefitResponse;
	}	
	
	
	// HIX-79776, publish benchmark plan API as service. Enrollment will call this service directly
	public PlanRateBenefitResponse getBenchmarkRateHelper(PlanRateBenefitRequest planRateBenefitRequest){
		PlanRateBenefitResponse planRateBenefitResponse = new PlanRateBenefitResponse();
		
		if(null == planRateBenefitRequest || null == planRateBenefitRequest.getRequestParameters()) {
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}else {
			Map<String, Object> cRequestParams = planRateBenefitRequest.getRequestParameters();
			List<Map<String, String>> censusData = (ArrayList<Map<String, String>>) cRequestParams.get("censusData");
			if(null == cRequestParams 
					|| null == cRequestParams.get("censusData")
					|| null == cRequestParams.get("coverageStartDate") 
					|| censusData.isEmpty()) {
				planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			}else {
				String coverageStartDate = cRequestParams.get("coverageStartDate").toString().trim();
								
				try {
					//double cResponseData = planRateBenefitService.getBenchmarkRateForEnrollment(censusData, coverageStartDate);			
					Map <String, String> benchMarkPlanIdPremiumMap = new HashMap <String, String>(); 

					String externalCaseId = null != cRequestParams.get("externalCaseId") && StringUtils.isNotBlank(cRequestParams.get("externalCaseId").toString().trim())
									? cRequestParams.get("externalCaseId").toString().trim()
									: null;
					LOGGER.debug("External Case ID: " + externalCaseId);
					benchMarkPlanIdPremiumMap = planRateBenefitService.getBenchmarkRateForEnrollment(censusData, coverageStartDate, null, null, externalCaseId, null);	
					//LOGGER.info ("benchMarkPlanIdPremiumMap " + benchMarkPlanIdPremiumMap);
					
					planRateBenefitResponse.addResponseData("benchMarkPre", benchMarkPlanIdPremiumMap.get(PlanMgmtConstants.TOTALPREMIUM));
					planRateBenefitResponse.addResponseData("benchMarkHiosPlanId", benchMarkPlanIdPremiumMap.get(PlanMgmtConstants.PLANID));
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}catch(Exception ex) {
					LOGGER.error("Exception occurred while processing request. Exception:",ex);
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_BENCHMARK_RATE.code);
					planRateBenefitResponse.setErrMsg(ex.getMessage());
				}	
			}
		}
		
		return planRateBenefitResponse;
	}		

	/**
	 * Method to handle ReST web service request of 'getLowestPremiumPlanRate' for given criteria. 
	 *
	 * @param lowestPremiumPlanRateReq The LowestPremiumPlanRateRequest instance.
	 * @return String The PlanRateBenefitResponse as stringified JSON.
	 */
//	@ApiOperation(value="findLowestPremiumPlanRate", notes="This API retrive Lowest Premium Plan Rate for given ZIP, County, employer zip, Plan Market e.g. Individual etc.")
//	@ApiResponse(value="return Lowest Premium Plan Rate for given information")
	@RequestMapping(value = "/findLowestPremiumPlanRate", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody PlanRateBenefitResponse getLowestPremiumPlanRate(@RequestBody PlanRateBenefitRequest planRateBenefitRequest) {
		LOGGER.info("============ inside getLowestPremiumPlanRate ============");			
		PlanRateBenefitResponse planRateBenefitResponse = new PlanRateBenefitResponse();

		if(null == planRateBenefitRequest || null == planRateBenefitRequest.getRequestParameters()) {
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			planRateBenefitResponse.setErrMsg("Invalid request parameters.");
		}
		else {
			Map<String, Object> cRequestParams = planRateBenefitRequest.getRequestParameters();
			if(null == cRequestParams 
					|| null == cRequestParams.get("dob")
					|| null == cRequestParams.get("zip")
					|| null == cRequestParams.get("employerPrimaryZip")
					|| null == cRequestParams.get("coverageStartDate")
					|| null == cRequestParams.get("tobacco")
					|| null == cRequestParams.get("planLevel")
					|| null == cRequestParams.get("planMarket")
					|| null == cRequestParams.get("countyCode")) {
				planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				planRateBenefitResponse.setErrMsg("Invalid request parameters.");
			}
			else {
				try {
					Validate.notNull(planRateBenefitService, "PlanRateBenefitService is improperly configured.");

					String dob = cRequestParams.get("dob").toString().trim();
					String zip = cRequestParams.get("zip").toString().trim();
					String employerPrimaryZip = cRequestParams.get("employerPrimaryZip").toString().trim();
					String coverageStartDate = cRequestParams.get("coverageStartDate").toString().trim();
					String tobacco = cRequestParams.get("tobacco").toString().trim();
					String planLevel = cRequestParams.get("planLevel").toString().trim();
					String planMarket = cRequestParams.get("planMarket").toString().trim();
					String countyCode = cRequestParams.get("countyCode").toString().trim();
					
					double cResponseData = planRateBenefitService.getLowestPremiumPlanRate(dob, zip, coverageStartDate, tobacco, planLevel, planMarket, countyCode);
					// if plan does not found in employee's own zip code then use employer's primary zip code for quoting
					if(cResponseData <= 0 ){
						cResponseData = planRateBenefitService.getLowestPremiumPlanRate(dob, employerPrimaryZip, coverageStartDate, tobacco, planLevel, planMarket, countyCode);
					}
					Validate.notNull(cResponseData, "Invalid response returned from business method.");
					
					planRateBenefitResponse.addResponseData(RESPONSE_DATA, cResponseData);
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
				catch(Exception ex) {
					LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_LOWEST_PREMIUM_RATE.code);
					planRateBenefitResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}
			}
		}

		LOGGER.info("Processing current request of getLowestPremiumPlanRate() is done.");
		return planRateBenefitResponse;
	
	}
	
//	@ApiOperation(value="memberlevelemployeerate", notes="This API takes employer zip, county, employee zip, county and members information and retrive member level employee rate")
//	@ApiResponse(value="returns bench mark premium for each member entered")
	@RequestMapping(value = "/memberlevelemployeerate", method = RequestMethod.POST)
	public @ResponseBody MLECRestResponse getMemberLevelEmployeeRate(@RequestBody MLECRestRequest request) {		
		MLECRestResponse response = null;
		try {
			LOGGER.debug("MLECRestRequest is null: " + (null == request));
			
			if (null != request) {
				response = planRateBenefitService.getMemberLevelEmployeeRate(request);
			}
			LOGGER.debug("MLECRestRequest is null: " + (null == response));			
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurrecd while processing request. Exception:", ex);
		}
		return response;
	}
	
	/**
	 * Method to handle REST web service request of 'getEmployerMinMaxRate' for given criteria.
	 *
	 * @param employerReq The EmployerRequest instance.
	 * @return String The ShopResponse as stringified XML.
	 * 
	 * This API will compute Min, Max plan rate for employee lists
	 */
//	@ApiOperation(value="getemployerminmaxrate", notes="This API takes employer zip, county, employee zip, county and members information and retrive employer min, max rate information")
//	@ApiResponse(value="return employer min and max rate information")
	@RequestMapping(value = "/getemployerminmaxrate", method = RequestMethod.POST)	
	public @ResponseBody String getEmployerMinMaxRate(@RequestBody EmployerRequest employerReq) {
    	if(LOGGER.isInfoEnabled()) {
    		LOGGER.info("Beginning to process current request of getEmployerMinMaxRate().");
    		LOGGER.info("Request data ===> empPrimaryZip:" + SecurityUtil.sanitizeForLogging(employerReq.getEmpPrimaryZip()) + ",  empCountyCode: " + SecurityUtil.sanitizeForLogging(employerReq.getEmpCountyCode()) +"  Effective date: " + SecurityUtil.sanitizeForLogging(employerReq.getEffectiveDate()) + ", Employee roster:" + SecurityUtil.sanitizeForLogging(employerReq.getEmployeeList().toString()) +", insurance type:" + SecurityUtil.sanitizeForLogging(employerReq.getInsType()));
    	}
		
		ShopResponse shopResponse = new ShopResponse();
		
		if(null == employerReq.getEmployeeList()
				|| null == employerReq.getEmpPrimaryZip()
				|| null == employerReq.getEmpCountyCode()
				|| null == employerReq.getEffectiveDate()
				|| null == employerReq.getInsType()) {
			shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			shopResponse.setErrMsg("Invalid request parameters.");
		}
		else {
			try {				
				List<ArrayList<ArrayList<Map<String, String>>>> employeeList = employerReq.getEmployeeList();
				String empPrimaryZip = employerReq.getEmpPrimaryZip().trim();	
				String empCountyCode = employerReq.getEmpCountyCode().trim();				
				String effectiveDate = employerReq.getEffectiveDate();	
				String insuranceType = employerReq.getInsType().toString();					
				
				Map<String, Float> rateData = new HashMap<String, Float>();
				rateData.put("maxRate", (float)0.00);
				rateData.put("minRate", (float)100000.00);
				
				
				for (ArrayList<ArrayList<Map<String, String>>> employee: employeeList) {	
					List<Map<String, String>> employeeCensusData = employee.get(0);
					Map<String, Float> responseData = planRateBenefitService.getEmployeeMinMaxRate(employeeCensusData, empPrimaryZip, empCountyCode,  effectiveDate, insuranceType);
					
					if((!(responseData.isEmpty()))){
					   if(responseData.get("maxRate") > rateData.get("maxRate")){
							 rateData.put("maxRate", responseData.get("maxRate"));
					     }	
					   if(responseData.get("minRate") < rateData.get("minRate")){
							 rateData.put("minRate", responseData.get("minRate"));
						}
					}else{
						rateData.put("maxRate", (float)0.00);
						rateData.put("minRate", (float)0.00);							
					}						
				}
				
				shopResponse.addResponseData(RESPONSE_DATA, rateData);
				shopResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
			catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("Exception occurrecd while processing request. Exception:",webServiceIOException);	
				shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_EMPLOYER_MIN_MAX_PLAN_RATE.code);
				shopResponse.setErrMsg(webServiceIOException.getStackTrace().toString());					
			}
			catch(Exception ex) {
				LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);				
				shopResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				shopResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_EMPLOYER_MIN_MAX_PLAN_RATE.code);
				shopResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
			}
		}

		LOGGER.info("Processing current request of getEmployerMinMaxRate() is done.");
		
		return shopResponse.sendResponse(ResponseType.XML);
	}
	
	
	/**
	 * Method to handle ReST web service request of 'getMemberLevelPremium' for given criteria. 
	 *
	 * @param planRateBenefitRequest The PlanRateBenefitRequest instance.
	 * @return String The PlanRateBenefitResponse as stringified JSON.
	 * 
	 * This API compute member level premium, rating area 
	 */
//	@ApiOperation(value="memberlevelpremium", notes="This API takes Effective Date, HIOS Plan Number and Member details and retrive member level premium")
//	@ApiResponse(value="returns Premium details by each member")
	@RequestMapping(value = "/memberlevelpremium", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody PlanRateBenefitResponse getMemberLevelPremium(@RequestBody PlanRateBenefitRequest planRateBenefitRequest) {
		LOGGER.info("Beginning to process current request of getMemberLevelPremium().");
		PlanRateBenefitResponse planRateBenefitResponse = new PlanRateBenefitResponse();

		if(null == planRateBenefitRequest || null == planRateBenefitRequest.getRequestParameters()) {
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			planRateBenefitResponse.setErrMsg("Invalid request parameters.");
		}
		else {
			Map<String, Object> cRequestParams = planRateBenefitRequest.getRequestParameters();
			if(null == cRequestParams 
					|| null == cRequestParams.get("hiosPlanNumber")
					|| null == cRequestParams.get("effectiveDate")					
					|| null == cRequestParams.get("memberList")			
					) {
				planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				planRateBenefitResponse.setErrMsg("Invalid request parameters.");
			}
			else {
				try {
					Validate.notNull(planRateBenefitService, "PlanRateBenefitService is improperly configured.");
					
					List<Map<String, String>> memberList = (ArrayList<Map<String, String>>) cRequestParams.get("memberList");				
					String effectiveDate = cRequestParams.get("effectiveDate").toString().trim();					
					String hiosPlanNumber = cRequestParams.get("hiosPlanNumber").toString().trim();	
								
			    	if(LOGGER.isDebugEnabled()) {
			    		LOGGER.debug("MEMBER INFO ==== "+ SecurityUtil.sanitizeForLogging(memberList.toString()));
			    	}
					List<PlanRateBenefit> responseData = planRateBenefitService.getMemberLevelPlanRate(hiosPlanNumber, memberList, effectiveDate);
					
					Validate.notNull(responseData, "Invalid response returned from business method.");							
					planRateBenefitResponse.setPlanRateBenefitList(responseData);
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
					LOGGER.debug("Response: " + GhixConstants.RESPONSE_SUCCESS);
				}
				catch(Exception ex) {
					LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_HOUSEHOLD_PLAN_RATE_BENEFITS.code);
					planRateBenefitResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}
			}

		}

		LOGGER.info("Processing current request of getHouseholdPlanRateBenefits() is done.");
		return planRateBenefitResponse;
	}
	
//	@ApiOperation(value="getBenefitsAndCost", notes="This API takes Plan ID's, Coverage Start Date and Insurance Type as input and retrive Plan Benefits and Costs.")
//	@ApiResponse(value="returns Plan Benefits and Costs based on provided Plan Ids, Coverage Start Date and Insurance Type")
	@RequestMapping(value = "/getBenefitsAndCost", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody PlanRateBenefitResponse getBenefitsAndCost(@RequestBody PlanRateBenefitRequest planRateBenefitRequest) 
	{
		LOGGER.info("Inside PlanRateBenefitRestController.getBenefitsAndCost()");
		PlanRateBenefitResponse planRateBenefitResponse = new PlanRateBenefitResponse();
		
		if( planRateBenefitRequest != null &&  planRateBenefitRequest.getRequestParameters() != null) 
		{
			Map<String, Object> requestParams = planRateBenefitRequest.getRequestParameters();
			if(requestParams.get("insuranceType") != null 
				&& requestParams.get("coverageStartDate") != null
				&& (requestParams.get("planIds") != null ||  requestParams.get("hiosPlanIds") != null)
				&& (Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(requestParams.get("insuranceType").toString()) 
					|| Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(requestParams.get("insuranceType").toString()) )){
				
				List<Integer> planIds = null;
				List<String> hiosPlanIds = null;
				
				if(requestParams.get("planIds") != null){
					planIds = (List<Integer>) requestParams.get("planIds");
				}
				if(requestParams.get("hiosPlanIds") != null){
					hiosPlanIds = (List<String>) requestParams.get("hiosPlanIds");
				}
				
				// expect plan id or Hios plan id in request not both at same time
				if((planIds != null && planIds.size() > PlanMgmtConstants.ZERO) && (hiosPlanIds != null && hiosPlanIds.size() > PlanMgmtConstants.ZERO)
					|| (planIds == null && hiosPlanIds != null && hiosPlanIds.size() == PlanMgmtConstants.ZERO)
					|| (hiosPlanIds == null && planIds != null && planIds.size() == PlanMgmtConstants.ZERO)
					|| (planIds == null && hiosPlanIds == null) 
					|| ((planIds != null && planIds.size() == PlanMgmtConstants.ZERO) && (hiosPlanIds != null && hiosPlanIds.size() == PlanMgmtConstants.ZERO))
						) {
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
					planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				}
				else{	
				List<PlanRateBenefit> responseData = getBenefitsAndCostHelper(planRateBenefitRequest);
				
				if(responseData.size() > PlanMgmtConstants.ZERO){
				planRateBenefitResponse.setPlanRateBenefitList(responseData);
				planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}else{
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.RECORD_NOT_FOUND.code);
					planRateBenefitResponse.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
				}
				}
				
			}else 
			{
				planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			}	
		}
		else 
		{
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}
		return planRateBenefitResponse;
	}
	
	@RequestMapping(value = "/getBenefitsAndCostData", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody PlanRateBenefitResponse getBenefitsAndCostData(@RequestBody PlanRateBenefitRequest planRateBenefitRequest) 
	{
		long t1 = TimeShifterUtil.currentTimeMillis();
		
		LOGGER.debug("Inside PlanRateBenefitRestController.getBenefitsAndCost()");
		PlanRateBenefitResponse planRateBenefitResponse = new PlanRateBenefitResponse();
		
		if( planRateBenefitRequest != null &&  planRateBenefitRequest.getRequestParameters() != null) 
		{
			Map<String, Object> requestParams = planRateBenefitRequest.getRequestParameters();
			if(requestParams.get("insuranceType") != null 
				&& requestParams.get("coverageStartDate") != null
				&& (requestParams.get("planIds") != null ||  requestParams.get("hiosPlanIds") != null)
				&& (Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(requestParams.get("insuranceType").toString()) 
					|| Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(requestParams.get("insuranceType").toString()) )){
				
				List<Integer> planIds = null;
				List<String> hiosPlanIds = null;
				
				if(requestParams.get("planIds") != null){
					planIds = (List<Integer>) requestParams.get("planIds");
				}
				if(requestParams.get("hiosPlanIds") != null){
					hiosPlanIds = (List<String>) requestParams.get("hiosPlanIds");
				}
				
				// expect plan id or Hios plan id in request not both at same time
				if((planIds != null && planIds.size() > PlanMgmtConstants.ZERO) && (hiosPlanIds != null && hiosPlanIds.size() > PlanMgmtConstants.ZERO)
					|| (planIds == null && hiosPlanIds != null && hiosPlanIds.size() == PlanMgmtConstants.ZERO)
					|| (hiosPlanIds == null && planIds != null && planIds.size() == PlanMgmtConstants.ZERO)
					|| (planIds == null && hiosPlanIds == null) 
					|| ((planIds != null && planIds.size() == PlanMgmtConstants.ZERO) && (hiosPlanIds != null && hiosPlanIds.size() == PlanMgmtConstants.ZERO))
						) {
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
					planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				}
				else{	
				List<PlanRateBenefit> responseData = getBenefitsAndCostHelperNew(planRateBenefitRequest);
				
				if(responseData.size() > PlanMgmtConstants.ZERO){
				planRateBenefitResponse.setPlanRateBenefitList(responseData);
				planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}else{
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.RECORD_NOT_FOUND.code);
					planRateBenefitResponse.setErrMsg(PlanMgmtConstants.RECORD_NOT_FOUND);
				}
				}
				
			}else 
			{
				planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			}	
		}
		else 
		{
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}
		
		long t2 = TimeShifterUtil.currentTimeMillis();
		
		LOGGER.info("time taken for total rest api ==> " + (t2-t1));
		
		return planRateBenefitResponse;
	}
	
	public List<PlanRateBenefit> getBenefitsAndCostHelper(PlanRateBenefitRequest planRateBenefitRequest) 
	{
		Map<String, Object> requestParams = planRateBenefitRequest.getRequestParameters();
		List<Integer> planIds = null;
		List<String> hiosPlanIds = null;
		List<PlanRateBenefit> responseData = new ArrayList<PlanRateBenefit>();
		
		try
		{
		if(requestParams.get("planIds") != null){
			planIds = (List<Integer>) requestParams.get("planIds");
		}
		if(requestParams.get("hiosPlanIds") != null){
			hiosPlanIds = (List<String>) requestParams.get("hiosPlanIds");
		}
		
		String insuranceType = (String) requestParams.get("insuranceType");	
		String coverageStartDate = (String) requestParams.get("coverageStartDate");
		
			int ctr= 1;
			List<Integer> planIdThreadList = new ArrayList<Integer>();
			Map<Integer, String> planIdAndHiosIdMap = new HashMap<Integer, String>();
			
			//long startTime = TimeShifterUtil.currentTimeMillis();
	
			try{
			
				 if(hiosPlanIds != null && hiosPlanIds.size() > PlanMgmtConstants.ZERO)
				 {
					 List<Object[]> PlanIdAndHiosPlanIDList  =  planMgmtService.getPlanIdsByHiosIds(hiosPlanIds, coverageStartDate, insuranceType);
					 if(null != PlanIdAndHiosPlanIDList && PlanIdAndHiosPlanIDList.size() > PlanMgmtConstants.ZERO){
						 planIds = new ArrayList<Integer>();
						 
						 for(Object[] object : PlanIdAndHiosPlanIDList){
							planIds.add(Integer.parseInt(object[ PlanMgmtConstants.ZERO].toString()));
							planIdAndHiosIdMap.put(Integer.parseInt(object[ PlanMgmtConstants.ZERO].toString()), object[ PlanMgmtConstants.ONE].toString());
						}
					}
				 }	 
					
			 // create thread pool of 10 threads 
				 int numOfProcessors = Runtime.getRuntime().availableProcessors();
		     LOGGER.info("creating threads equal to number of processors ==> " + numOfProcessors);
		     
			 ExecutorService executorService = Executors.newFixedThreadPool(numOfProcessors);
	
			 List<Callable<List<PlanRateBenefit>>> lst = new ArrayList<Callable<List<PlanRateBenefit>>>();
			 if(planIds != null && planIds.size() > PlanMgmtConstants.ZERO)
			 {
				 int numOfPlansPerThread = planIds.size()/numOfProcessors;
				  LOGGER.info("number of plans per thread ==> " + numOfPlansPerThread);
				  
				 for(Integer planId: planIds){
					planIdThreadList.add(planId);
					
					// create thread for set of 5 plans
				//	if(planIdThreadList.size() == PlanMgmtConstants.FIVE){
					if(planIdThreadList.size() == numOfPlansPerThread){
							lst.add(new TaskAsCallable(planIdThreadList, planIdAndHiosIdMap, insuranceType, coverageStartDate, planCostAndBenefitsService));
						ctr=1;
						planIdThreadList = new ArrayList<Integer>();
					}
					ctr++;
				}
				
				// create thread for rest of plans
				if(planIdThreadList.size() > 0){
						lst.add(new TaskAsCallable(planIdThreadList, planIdAndHiosIdMap, insuranceType, coverageStartDate, planCostAndBenefitsService));
				}
			 }
					
		
		     // returns a list of Futures holding their status and results when all complete
		        List<Future<List<PlanRateBenefit>>> tasks = executorService.invokeAll(lst);
		         
		        for(Future<List<PlanRateBenefit>> task : tasks)
		        {
		           try{
		        	   responseData.addAll(task.get());
		           }
		           catch(ExecutionException ex2){
		        	   LOGGER.error(ex2);
		           }
		        }
		         
		        executorService.shutdown();
			
			}
			catch(InterruptedException ex){
				LOGGER.error(ex);
			}
			
			/*long endTime   = TimeShifterUtil.currentTimeMillis();
			long totalTime = endTime - startTime;
			LOGGER.info("totalTime == "+ totalTime);*/
			
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception occured in getBenefitsAndCostHelper: ", ex);
		}
		
		return responseData;
	}
    
	
	public List<PlanRateBenefit> getBenefitsAndCostHelperNew(PlanRateBenefitRequest planRateBenefitRequest) 
	{
		
		long t1 = TimeShifterUtil.currentTimeMillis();
		
		Map<String, Object> requestParams = planRateBenefitRequest.getRequestParameters();

		List<Integer> planIds = null;
		List<String> hiosPlanIds = null;
		List<PlanRateBenefit> responseData = new ArrayList<PlanRateBenefit>();
		
		
		//
		
		if(requestParams.get("planIds") != null){
			planIds = (List<Integer>) requestParams.get("planIds");
		}
		if(requestParams.get("hiosPlanIds") != null){
			hiosPlanIds = (List<String>) requestParams.get("hiosPlanIds");
		}
		
		String insuranceType = (String) requestParams.get("insuranceType");	
		String coverageStartDate = (String) requestParams.get("coverageStartDate");
	
			Map<Integer, String> planIdAndNetworkNameMap = new HashMap<Integer, String>();
			Map<Integer, String> planIdAndIssuerPlanNumberMap = new HashMap<Integer, String>();
			
			//long startTime = TimeShifterUtil.currentTimeMillis();
	
			
				 if(hiosPlanIds != null && hiosPlanIds.size() > PlanMgmtConstants.ZERO)
				 {
					 List<Object[]> PlanIdAndHiosPlanIDList  =  planMgmtService.getPlanIdsAndNetowrkNamesByHiosIds(hiosPlanIds, coverageStartDate, insuranceType);
					 if(null != PlanIdAndHiosPlanIDList && PlanIdAndHiosPlanIDList.size() > PlanMgmtConstants.ZERO){
						 planIds = new ArrayList<Integer>();
						 
						 for(Object[] object : PlanIdAndHiosPlanIDList){
							planIds.add(Integer.parseInt(object[ PlanMgmtConstants.ZERO].toString()));
							planIdAndIssuerPlanNumberMap.put(Integer.parseInt(object[ PlanMgmtConstants.ZERO].toString()), object[ PlanMgmtConstants.ONE].toString());
							planIdAndNetworkNameMap.put(Integer.parseInt(object[ PlanMgmtConstants.ZERO].toString()), object[ PlanMgmtConstants.TWO].toString());
						}
					}
				 }	 
		
	 long s1 = TimeShifterUtil.currentTimeMillis();
	 responseData = planCostAndBenefitsService.getIndividualPlanBenefitsAndCostNew(planIds , insuranceType , coverageStartDate);
	 //populate networkName
	 if (responseData != null){
		 for(PlanRateBenefit prb : responseData){
			 prb.setNetworkName(planIdAndNetworkNameMap.get(prb.getId()));
			 prb.setHiosPlanNumber(planIdAndIssuerPlanNumberMap.get(prb.getId()));
		 }
	 }
	 long s2 = TimeShifterUtil.currentTimeMillis();
	 LOGGER.info("total time taken for planCostAndBenefitsService.getIndividualPlanBenefitsAndCostNew ==> " + (s2-s1));
	 
		
		Validate.notNull(responseData, "Invalid response returned from business method.");	
		
		long t2 = TimeShifterUtil.currentTimeMillis();
		
		LOGGER.info("total time taken for getBenefitsAndCostHelperNew ==> " + (t2-t1));
		return responseData;
	}
//	@ApiOperation(value="getBenefitsAndCostByPlanId", notes="This API takes Plan ID's and Insurance Type and retrive Plan Benefits and Costs for given plan Ids")
//	@ApiResponse(value="return Plan Benefits and Costs information for given plan Ids")
	@RequestMapping(value = "/getBenefitsAndCostByPlanId", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody PlanRateBenefitResponse getBenefitsAndCostByPlanId(@RequestBody PlanRateBenefitRequest planRateBenefitRequest) 
	{
		LOGGER.info("Inside PlanRateBenefitRestController.getBenefitsAndCostByPlanId()");
		PlanRateBenefitResponse planRateBenefitResponse = new PlanRateBenefitResponse();
		
		if( planRateBenefitRequest != null &&  planRateBenefitRequest.getRequestParameters() != null) 
		{
			Map<String, Object> requestParams = planRateBenefitRequest.getRequestParameters();
			String insuranceType = (String) requestParams.get("insuranceType");	
			List<Integer> planIds = (List<Integer>) requestParams.get("planIds");
			
			if((PlanMgmtConstants.HEALTH.equalsIgnoreCase(insuranceType) || PlanMgmtConstants.DENTAL.equalsIgnoreCase(insuranceType)) && planIds !=null && planIds.size() != PlanMgmtConstants.ZERO){
			Map<String, Object> responseData = getBenefitsAndCostByPlanIdHelper(planRateBenefitRequest);
			Validate.notNull(responseData, "Invalid response returned from business method.");
				
				if(PlanMgmtConstants.HEALTH.equalsIgnoreCase(insuranceType) )
			{
				planRateBenefitResponse.setPlanHealthBenefitDTOList((List<PlanHealthBenefitDTO>) responseData.get("healthBenefits"));
				planRateBenefitResponse.setPlanHealthCostDTOList((List<PlanHealthCostDTO>) responseData.get("healthCosts"));
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
			}
				else if (PlanMgmtConstants.DENTAL.equalsIgnoreCase(insuranceType) )
			{
				planRateBenefitResponse.setPlanDentalBenefitDTOList((List<PlanDentalBenefitDTO>) responseData.get("dentalBenefits"));
				planRateBenefitResponse.setPlanDentalCostDTOList((List<PlanDentalCostDTO>) responseData.get("dentalCosts"));
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
		}
				
				// HIX-101385 : if callFromWeb bit is true in request then send planinfo and applyeditbenefit flag in response
				if(requestParams.get("callFromWeb") != null && (boolean) requestParams.get("callFromWeb") == true){
					Integer planId = planIds.get(0);
					if (0 < planId) {
						com.getinsured.hix.dto.planmgmt.PlanDTO planDto = planMgmtService.getPlanDetailsById(planId);
						if(null != planDto){
							planRateBenefitResponse.setPlanInfoDTO(planDto);
							planRateBenefitResponse.setBenefitsModified(planCostAndBenefitsService.anyUpdatesInBenefits(planDto.getIssuerPlanNumber(), planDto.getPlanYear()));
						}
					}	
				}
				
			}	
			else{
				planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			}
		}
		else 
		{
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}
		
		planRateBenefitResponse.endResponse();
		
		return planRateBenefitResponse;
	}
	
	public Map<String, Object> getBenefitsAndCostByPlanIdHelper(PlanRateBenefitRequest planRateBenefitRequest) 
	{
		Map<String, Object> requestParams = planRateBenefitRequest.getRequestParameters();
		List<Integer> planIds = (List<Integer>) requestParams.get("planIds");
		String insuranceType = (String) requestParams.get("insuranceType");	
		Map<String, Object> responseData = planCostAndBenefitsService.getBenefitsAndCostByPlanId(planIds , insuranceType );
		Validate.notNull(responseData, "Invalid response returned from business method.");
		
		return responseData;
	}
	
    
	/* return lowest dental plan */
//	@ApiOperation(value="getlowestdentalplan", notes="THis operation retrieves plans details of plan with lowest dental premium")
//	@ApiResponse(value="return lowest dental premium plan for give information")
	@RequestMapping(value = "/getlowestdentalplan", method = RequestMethod.POST)
	@Produces("application/json")
	@ResponseBody public PlanRateBenefitResponse getLowestDentalPremium(@RequestBody PlanRateBenefitRequest planRateBenefitRequest )
	{
		PlanRateBenefitResponse planRateBenefitResponse = new PlanRateBenefitResponse();
		Map<String, Object> requestParams = planRateBenefitRequest.getRequestParameters();
		List<Map<String, String>> memberList = (List<Map<String, String>>) requestParams.get("memberList");	
		
	   	if(null == requestParams.get("effectiveDate")					
				|| null == requestParams.get("memberList")
				|| memberList.isEmpty()
		) {
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			planRateBenefitResponse.setErrMsg("Invalid request parameters.");
		}
	   	else 
	   	{	
			LOGGER.info("inside getLowestDentalPremium");	
			String exchangeType = (requestParams.get("exchangeType") == null || requestParams.get("exchangeType").toString().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.ON : requestParams.get("exchangeType").toString().trim(); // default value is ON
			String effectiveDate = requestParams.get("effectiveDate").toString().trim();
			String tenantCode = (requestParams.get("tenant") == null || requestParams.get("tenant").toString().trim().equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.TENANT_GINS : requestParams.get("tenant").toString().trim(); // default value is GINS
			
	    	if(LOGGER.isDebugEnabled()) {
				StringBuilder buildStr = new StringBuilder();
				buildStr.append("Request data ===>");			
				buildStr.append(" Effective date : ");
				buildStr.append(effectiveDate);
				buildStr.append(", exchangeType : ");
				buildStr.append(exchangeType);
				buildStr.append(", tenantCode : ");
				buildStr.append(tenantCode);
				buildStr.append(", memberList : ");
				buildStr.append(requestParams.get("memberList"));
				LOGGER.debug(SecurityUtil.sanitizeForLogging(buildStr.toString()));
	    	}
					
				try{
					List<PlanRateBenefit> lowestDentalPlan = planRateBenefitService.getLowestDentalPlan(memberList, effectiveDate, exchangeType, tenantCode);			
					planRateBenefitResponse.setPlanRateBenefitList(lowestDentalPlan);
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);						
				}catch(Exception ex) {
					LOGGER.error("Fail to fetch lowest dental plans. Exception:", ex);					
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.GET_PLAN_DETAILS.code);
					planRateBenefitResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}							
			}
			
		return planRateBenefitResponse;
	}
	
	
	
	// LIFE SPAN ENROLLMENT QUOTING API WILL QUOTE HOUSEHOLD FOR MULTIPLE PERIODS (UPTO 12 TIMES) 
		@RequestMapping(value = "/lifeSpanEnrollmentQuoting", method = RequestMethod.POST)
		@Produces("application/json")
		@ResponseBody public LifeSpanEnrollmentQuotingResponseDTO doLifeSpanEnrollmentQuoting(@RequestBody LifeSpanEnrollmentQuotingRequestDTO lifeSpanEnrollmentQuotingRequestDTO )
		{
			LifeSpanEnrollmentQuotingResponseDTO  lifeSpanEnrollmentQuotingResponseDTO = new LifeSpanEnrollmentQuotingResponseDTO();  
			String planId = lifeSpanEnrollmentQuotingRequestDTO.getPlanId().trim();
			List<LifeSpanPeriodLoopDTO> periodLoopList = lifeSpanEnrollmentQuotingRequestDTO.getPeriodLoopList();
			String considerEHBPortion = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CONSIDER_EHB_PORTION);
			boolean validPeriodLoop = true;
			SimpleDateFormat sdf = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
			
			try{			
				// ======================= VALIDATE REQUEST STARTS HERE	========================================	
				if( periodLoopList.size() > PlanMgmtConstants.ZERO){
					for(LifeSpanPeriodLoopDTO periodLoopDTO : periodLoopList){
						if( StringUtils.isBlank(periodLoopDTO.getPeriodId()) ||
						   StringUtils.isBlank(periodLoopDTO.getPeriodEffectiveDate()) ||
						   StringUtils.isBlank(periodLoopDTO.getSubscriberCountyCode()) ||
						   periodLoopDTO.getSubscriberCountyCode().length() != PlanMgmtConstants.FIVE ||
						   StringUtils.isBlank(periodLoopDTO.getSubscriberZipCode()) ||
						   periodLoopDTO.getSubscriberZipCode().length() != PlanMgmtConstants.FIVE ||
						   null == periodLoopDTO.getRequestMemberList() ||						   
						   periodLoopDTO.getRequestMemberList().size() == PlanMgmtConstants.ZERO){
						   validPeriodLoop = false;
						   break;
						}else{
							try{
				    			sdf.setLenient(false);
				    			sdf.parse(periodLoopDTO.getPeriodEffectiveDate().toString());
				    			
							for(LifeSpanMemberRequestDTO lifeSpanMemberRequestDTO : periodLoopDTO.getRequestMemberList()){
								if(StringUtils.isBlank(lifeSpanMemberRequestDTO.getCoverageStartDate()) ||
								   StringUtils.isBlank(lifeSpanMemberRequestDTO.getRelation()) ||
								   StringUtils.isBlank(lifeSpanMemberRequestDTO.getTobacco()) ||
								   StringUtils.isBlank(lifeSpanMemberRequestDTO.getDaysActive()) ||
								   StringUtils.isBlank(lifeSpanMemberRequestDTO.getDob())
								 ) {
									validPeriodLoop = false;
									break;
									}else{
										try{
							    			sdf.setLenient(false);
							    			sdf.parse(lifeSpanMemberRequestDTO.getCoverageStartDate().toString());
							    			sdf.parse(lifeSpanMemberRequestDTO.getDob().toString());
										}catch(Exception ex){
											validPeriodLoop = false;
											break;
										}
								}
							}
							}catch(Exception ex){
								validPeriodLoop = false;
								break;
							}
						}
					}
				}else{
					 validPeriodLoop = false;
				}
				// ===================== VALIDATE REQUEST ENDS HERE ====================================
				
				// IF REQUESTED PLAN ID IS NOT BLANK, NUMERIC AND validPeriodLoop == true THEN PROCESS REQUEST 
				if(StringUtils.isNumeric(planId) && StringUtils.isNotBlank(planId) && validPeriodLoop == true ){
					Plan planObj = planMgmtService.getPlanData(planId);	
					
					// CHECK RECORD EXISTS IN DB FOR REQUESTED PLAN ID, IF PLAN ID DOES NOT EXISTS THEN SEND LIFE_SPLAN_ENROLLMENT_QUOTING ERROR CODE
					if(null != planObj){
						String insuranceType = planObj.getInsuranceType();
						PlanDataDTOHelper planDataDTOHelper = new PlanDataDTOHelper();
						// SET PLAN DATA IN RESPONSE DTO 
						planDataDTOHelper.setLifeSpanEnrollmentQuotingResponseDTO(lifeSpanEnrollmentQuotingResponseDTO, planObj, insuranceType, considerEHBPortion, appUrl, planMgmtService);
						
						// QUOTE FOR PERIODS 
						lifeSpanEnrollmentQuotingResponseDTO = planRateBenefitService.lifeSpanEnrollmentQuoting(lifeSpanEnrollmentQuotingRequestDTO, lifeSpanEnrollmentQuotingResponseDTO, insuranceType);
						
						lifeSpanEnrollmentQuotingResponseDTO.setStatus(GhixConstants.RESPONSE_SUCCESS);
						
					}else{
						lifeSpanEnrollmentQuotingResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
						lifeSpanEnrollmentQuotingResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.LIFE_SPLAN_ENROLLMENT_QUOTING.getCode());
						lifeSpanEnrollmentQuotingResponseDTO.setErrMsg(PlanMgmtConstants.PLAN_NOT_FOUND);
					}
	
					lifeSpanEnrollmentQuotingResponseDTO.endResponse();
					
				}else{
					lifeSpanEnrollmentQuotingResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					lifeSpanEnrollmentQuotingResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
					lifeSpanEnrollmentQuotingResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				}
				
			}catch(Exception ex) {
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to do lifeSpanEnrollmentQuoting ",ex);
				LOGGER.error("Failed to do lifeSpanEnrollmentQuoting: ", ex);					
				lifeSpanEnrollmentQuotingResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				lifeSpanEnrollmentQuotingResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				lifeSpanEnrollmentQuotingResponseDTO.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}	

			return lifeSpanEnrollmentQuotingResponseDTO;
		}
		
		
	
		// HIX-88249 :: API to get the Lowest Plan Premium available for household
		@RequestMapping(value = "/getLowestPremium", method = RequestMethod.POST)
		@Produces("application/json")
		@ResponseBody public LowestPremiumResponseDTO getLowestPremium(@RequestBody LowestPremiumRequestDTO lowestPremiumRequestDTO )
		{
			LowestPremiumResponseDTO  lowestPremiumResponseDTO = new LowestPremiumResponseDTO();
			try{	
				SimpleDateFormat sdf = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
				boolean validRequestData = true;
				String exchangeType = PlanMgmtConstants.BOTH; 		// default value is BOTH
				String insuranceType = PlanMgmtConstants.HEALTH; 	// default value is HEALTH
				String tenantCode = PlanMgmtConstants.TENANT_GINS;	// default value is GINS
				
				// validate mandatory request
				if(StringUtils.isBlank(lowestPremiumRequestDTO.getEffectiveDate()) ||
					lowestPremiumRequestDTO.getMemberList().size() == 0) {
					validRequestData = false;
				}else{				
					for(Member memberdata : lowestPremiumRequestDTO.getMemberList()){
						if(memberdata.getId() <= 0 || memberdata.getAge() < 0 || 
								StringUtils.isBlank(memberdata.getRelation()) ||
								StringUtils.isBlank(memberdata.getZipCode()) ||
								StringUtils.isBlank(memberdata.getCountyCode()) ||
								StringUtils.isBlank(memberdata.getTobacco()) ){
								validRequestData = false;
								break;
						}
					}
				}	
				
				// validate requested exchange type
				if(StringUtils.isNotBlank(lowestPremiumRequestDTO.getExchangeType())){
					exchangeType = lowestPremiumRequestDTO.getExchangeType().trim();
					if(!PlanMgmtConstants.PLAN_EXCHANGE_TYPE_ON.equalsIgnoreCase(exchangeType) && !PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF.equalsIgnoreCase(exchangeType)){
						validRequestData = false;
					}						
				}
				
				// validate requested insurance type
				if(StringUtils.isNotBlank(lowestPremiumRequestDTO.getInsuranceType())){
					insuranceType = lowestPremiumRequestDTO.getInsuranceType().trim();
					if(!PlanMgmtConstants.HEALTH.equalsIgnoreCase(insuranceType) && !PlanMgmtConstants.DENTAL.equalsIgnoreCase(insuranceType)){
						validRequestData = false;
					}						
				}
				
				// validate effective date
				try{
	    			sdf.setLenient(false);
	    			sdf.parse(lowestPremiumRequestDTO.getEffectiveDate().trim());
				}catch(Exception ex){
					validRequestData = false;
				}
				
				if(StringUtils.isNotBlank(lowestPremiumRequestDTO.getTenantCode()) ){
					tenantCode = lowestPremiumRequestDTO.getTenantCode().trim();
				}
				
				if(validRequestData){
					float onExchangePremium = 0;
					float offExchangePremium = 0;
					boolean showCatastrophicPlan = lowestPremiumRequestDTO.isShowCatastrophicPlan();
					String restrictHiosIds = (lowestPremiumRequestDTO.getRestrictHiosIds() == null) ? PlanMgmtConstants.ON : lowestPremiumRequestDTO.getRestrictHiosIds().toString().trim();  // Default value is ON
					
					if(PlanMgmtConstants.BOTH.equalsIgnoreCase(exchangeType)) // if exchangeType does not pass explicitly then compute ON and OFF exchange lowest premium
					{
						onExchangePremium = planRateBenefitService.getLowestPremium(insuranceType, PlanMgmtConstants.PLAN_EXCHANGE_TYPE_ON, tenantCode, lowestPremiumRequestDTO.getEffectiveDate().trim(), lowestPremiumRequestDTO.getMemberList(), false, restrictHiosIds);
						offExchangePremium = planRateBenefitService.getLowestPremium(insuranceType, PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF, tenantCode, lowestPremiumRequestDTO.getEffectiveDate().trim(), lowestPremiumRequestDTO.getMemberList(), showCatastrophicPlan, restrictHiosIds);
					}
					else if(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_ON.equalsIgnoreCase(exchangeType)) // if exchangeType is ON then compute ON exchange lowest premium
					{
						onExchangePremium = planRateBenefitService.getLowestPremium(insuranceType, PlanMgmtConstants.PLAN_EXCHANGE_TYPE_ON, tenantCode, lowestPremiumRequestDTO.getEffectiveDate().trim(), lowestPremiumRequestDTO.getMemberList(), false, restrictHiosIds);
					}
					else if(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF.equalsIgnoreCase(exchangeType)) // if exchangeType is OFF then compute OFF exchange lowest premium
					{
						offExchangePremium = planRateBenefitService.getLowestPremium(insuranceType, PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF, tenantCode, lowestPremiumRequestDTO.getEffectiveDate().trim(), lowestPremiumRequestDTO.getMemberList(), showCatastrophicPlan, restrictHiosIds);
					}
					
					lowestPremiumResponseDTO.setOffExchangePremium(offExchangePremium);
					lowestPremiumResponseDTO.setOnExchangePremium(onExchangePremium);
					
				}else{
					lowestPremiumResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
					lowestPremiumResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
					lowestPremiumResponseDTO.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
				}
				
				
			}catch(Exception ex) {
				giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to do getLowestPremium ",ex);
				LOGGER.error("Failed to do getLowestPremium: ", ex);					
				lowestPremiumResponseDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
				lowestPremiumResponseDTO.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.code);
				lowestPremiumResponseDTO.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
			}	
			
			return lowestPremiumResponseDTO;
		}
		
		
		
}
		




