package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanSbcScenario;

public interface ISbcSecnarioRepository extends JpaRepository<PlanSbcScenario, Integer>{
	
	@Query("SELECT sbc FROM PlanSbcScenario sbc WHERE sbc.id IN (:idList) ")	
	List<PlanSbcScenario> getSbcSecnarioByIdList(@Param("idList") List<Integer> sbcScenarioIdList);
}
