//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.12 at 02:20:37 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.ihc;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfIhcDentalQuote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfIhcDentalQuote">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IhcDentalQuote" type="{http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.IhcDental}IhcDentalQuote" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfIhcDentalQuote", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.IhcDental", propOrder = {
    "ihcDentalQuote"
})
public class ArrayOfIhcDentalQuote {

    @XmlElement(name = "IhcDentalQuote", nillable = true)
    protected List<IhcDentalQuote> ihcDentalQuote;

    /**
     * Gets the value of the ihcDentalQuote property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ihcDentalQuote property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIhcDentalQuote().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IhcDentalQuote }
     * 
     * 
     */
    public List<IhcDentalQuote> getIhcDentalQuote() {
        if (ihcDentalQuote == null) {
            ihcDentalQuote = new ArrayList<IhcDentalQuote>();
        }
        return this.ihcDentalQuote;
    }

}
