package com.getinsured.hix.planmgmt.querybuilder;

import java.util.HashMap;


import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtParamConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public class PlanMgmtQueryBuilder {
	private static final Logger LOGGER = Logger.getLogger(PlanMgmtQueryBuilder.class);
	
	private static String updatePlanStatusQuery;
	private static String updateEnrollmentAvailabilityQuery;
	private static String csrMUltiplierQuery;
	private static String planNetworkQuery;
	private static HashMap<String, String> indvHealthPlanAvailabilityQueriesMap;
	private static HashMap<String, String> regionNameQueryMap;
	private static String crossWalkPlanIdQuery;
	private static String crosswalkQueryInPlanTable;
	private static HashMap<Integer, String> healthPlansByZipAndIssuerNameQueryMap;
	private static HashMap<Integer, String> dentalPlansByZipAndIssuerNameQueryMap;
	private static String referenceHealthPlanWithEhbQuery;
	private static String referenceHealthPlanWithoutEhbQuery;
	private static String planAvailabilityForEmployeeQuery;
	private static String providerNetworkQuery;
	private static String planDataBenefitQueryForHealthInsurance;
	private static String planDataBenefitQueryForDentalInsurance;
	private static String dentalPlanCostQuery;
	private static HashMap<String, String> healthPlanInfoByZipWithoutCountyFipsQuery;
	private static HashMap<String, String> healthPlanInfoByZipWithCountyFipsQuery;
	private static HashMap<String, String> dentalPlanInfoByZipWithoutCountyFipsQuery;
	private static HashMap<String, String> dentalPlanInfoByZipWithCountyFipsQuery;
	private static String healthPlanCostsQuery;
	private static HashMap<String, String> stmPlanQuery;
	private static HashMap<String, String> amePlanQuery;
	private static String metalTierQueryWithoutEhbQuery;
	private static String metalTierQueryWithEhbQuery;
	private static String healthPlanBenefitQuery;
	private static String dentalPlanBenefitsQuery;
	private static String stateListbyZipQuery;
	private static String stateListbyZipAndCountyQuery;
	private static String planBenefitForHealthInsuranceQuery;
	private static String planBenefitForDentalInsuranceQuery;
	private static String lifePlanQuetingQuery;
	private static HashMap<String, String> medicarePlanQuery;
	private static HashMap<String, String> medicarePlanForAllQuery;
	private static final int PLAN_LEVEL = 1;
	private static final int COUNTY_CODE = 3;
	private static final int ISSUER_NAME = 5;
	private static String ratingAreaByYearQuery;
	

	public static final String updatePlanStatusQuery(){
		if(updatePlanStatusQuery != null){
			return updatePlanStatusQuery;
		}
		
		StringBuilder buildQuery = new StringBuilder(512);
		buildQuery.append("SELECT p ");
		buildQuery.append("FROM ");
		buildQuery.append("Plan p ");
		buildQuery.append("WHERE ");
		//buildQuery.append("TRIM(SUBSTRING(p.deCertificationEffDate,0,9)) <= TO_DATE(:");
		buildQuery.append("to_date(to_char(p.deCertificationEffDate, 'MM-DD-YYYY'), 'MM-DD-YYYY') <= TO_DATE(:");
		buildQuery.append(PlanMgmtParamConstants.PARAM_TODAY_DATE);
		buildQuery.append(", 'MM-DD-YYYY')");
		
		updatePlanStatusQuery = buildQuery.toString();				

		return updatePlanStatusQuery;
	}
	
	public static final String getUpdateEnrollmentAvailabilityQuery(){
		if(updateEnrollmentAvailabilityQuery != null){
			return updateEnrollmentAvailabilityQuery;
		}		

		StringBuilder buildQuery = new StringBuilder(512);		
		buildQuery.append("SELECT p ");
		buildQuery.append("FROM ");
		buildQuery.append("Plan p ");
		buildQuery.append("WHERE ");
		//buildQuery.append("TRIM(SUBSTRING(p.futureErlAvailEffDate,0,9)) <= TO_DATE(:");
		buildQuery.append("to_date(to_char(p.futureErlAvailEffDate, 'MM-DD-YYYY'), 'MM-DD-YYYY') <= TO_DATE(:");
		buildQuery.append(PlanMgmtParamConstants.PARAM_TODAY_DATE);
		buildQuery.append(", 'MM-DD-YYYY')");
		
		updateEnrollmentAvailabilityQuery = buildQuery.toString().trim();
		
		return updateEnrollmentAvailabilityQuery;
	}
	
	
	public static final String getCSRMUltiplierQuery(){
		if(csrMUltiplierQuery != null){
			return csrMUltiplierQuery;
		}
		
		StringBuilder buildquery = new StringBuilder(512);
		/*buildquery.append("SELECT multiplier FROM ( ");
		buildquery.append("SELECT multiplier, CSRVariation, planLevel, planYear, row_number() OVER (ORDER by planYear desc) AS seqnum ");
		buildquery.append("FROM ");
		buildquery.append("PlanCsrMultiplier ");
		buildquery.append("WHERE ");
		buildquery.append("CSRVariation = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_CSR_VARIATION);
		buildquery.append(" AND UPPER(planLevel) = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_LEVEL);
		buildquery.append(" AND planYear <=  :");
		buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
		buildquery.append(" ) rowline ");
		buildquery.append("WHERE ");
		buildquery.append("seqnum = 1 ");*/
		
		buildquery.append("SELECT MULTIPLIER FROM ( ");
		buildquery.append("SELECT MULTIPLIER, CSR_VARIATION, plan_level, PLAN_YEAR, row_number() OVER (ORDER by PLAN_YEAR desc) AS seqnum ");
		buildquery.append("FROM ");
		buildquery.append("PM_CSR_MULTIPLIER ");
		buildquery.append("WHERE ");
		buildquery.append("CSR_VARIATION = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_CSR_VARIATION);
		buildquery.append(" AND UPPER(PLAN_LEVEL) = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_LEVEL);
		buildquery.append(" AND PLAN_YEAR <=  :");
		buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
		buildquery.append(" ) rowline ");
		buildquery.append("WHERE ");
		buildquery.append("seqnum = 1 ");
		
		csrMUltiplierQuery = buildquery.toString();

		return csrMUltiplierQuery;
	}
	
	public static final String getPlanNetworkQuery(){
		if(planNetworkQuery != null){
			return planNetworkQuery;
		}

		StringBuilder buildquery = new StringBuilder(512);
		buildquery.append("SELECT networkID, networkURL, applicableYear ");
		buildquery.append("FROM ");
		buildquery.append("Network ");
		buildquery.append("WHERE ");
		buildquery.append("id = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_NETWORK_ID);
		
		planNetworkQuery = buildquery.toString();
		
		return planNetworkQuery;
	}
	
	// prepare OFF EXCHANGE HEALTH plan availability
	public static final String getIndvHealthPlanAvailabilityQuery(String csrValue, String exchangeType, String configuredDB) {
		if (indvHealthPlanAvailabilityQueriesMap == null) {
			indvHealthPlanAvailabilityQueriesMap = new HashMap<String, String>(5);
		}
		String query = indvHealthPlanAvailabilityQueriesMap.get(csrValue);
		if (query != null) {
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Recrieved cached OffExchangeIndvHealthPlanAvailability query template for csrValue"+csrValue);
			}
			return query;
		}
		
		StringBuilder buildquery = new StringBuilder(1024);
		buildquery.append("SELECT count(distinct p.id ) ");
		buildquery.append("FROM ");
		buildquery.append("Plan p, ServiceArea psarea, PlanHealth phealth, ZipCountyRatingArea pzcrarea, PlanRate prate, ");
		buildquery.append("TenantPlan tp, Tenant t ");
		buildquery.append("WHERE ");
		buildquery.append("p.serviceAreaId = psarea.serviceAreaId.id ");
		buildquery.append(" AND p.id = phealth.plan.id ");
		buildquery.append(" AND pzcrarea.ratingArea.id = prate.ratingArea.id ");
		buildquery.append(" AND p.id = prate.plan.id ");
		buildquery.append(" AND p.insuranceType = '");
		buildquery.append(Plan.PlanInsuranceType.HEALTH.toString());

		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			buildquery.append("' AND psarea.zip = to_number(trim(:");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			buildquery.append(")) AND (pzcrarea.zip = to_number(trim(:");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			buildquery.append(")) OR pzcrarea.zip IS NULL) ");
		}else{
			buildquery.append("' AND psarea.zip = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			buildquery.append(" AND (pzcrarea.zip = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			buildquery.append(" OR pzcrarea.zip IS NULL) ");
		}
		
		buildquery.append(" AND psarea.fips = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
		buildquery.append(" AND pzcrarea.countyFips = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
		buildquery.append(" AND p.isDeleted = 'N' ");
		buildquery.append(" AND psarea.isDeleted = 'N' ");
		buildquery.append(" AND prate.isDeleted = 'N' ");
		buildquery.append(" AND TO_DATE (( :param_effectiveDate_0");
		buildquery.append(" ), 'YYYY-MM-DD') BETWEEN p.startDate AND p.endDate ");
		buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("p", true));
		QuotingBusinessLogicQueryBuilder.buildEnrollmentAvailabilityLogic(buildquery, "p", PlanMgmtConstants.NO);
		buildquery.append(" AND p.market = '");
		buildquery.append(Plan.PlanMarket.INDIVIDUAL.toString());
		buildquery.append("' ");
		/*buildquery.append(" AND p.exchangeType = :");
		buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);*/
		buildquery.append(" AND p.id = tp.plan.id");
		buildquery.append(" AND tp.tenant.id = t.id");
		buildquery.append(" AND t.code = :"); 
		buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);

		// HIX-79254 - On OFF exchange flow 
		// show ON exchange plan having enable_for_offexch_flow flag as YES along with OFF exchange plan
		if(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF.equalsIgnoreCase(exchangeType)){ // OFF exchange flow
			buildquery.append(" AND (p.exchangeType = 'OFF' OR (p.exchangeType = 'ON' AND UPPER(p.enableForOffExchFlow) = 'YES')) ");
		}else{
			buildquery.append(" AND p.exchangeType = 'ON' "); // ON exchange flow
		}

		// add cost sharing filter
		QuotingBusinessLogicQueryBuilder.buildCostSharingLogic(buildquery, "phealth", csrValue);
		buildquery.append(" GROUP BY (pzcrarea.ratingArea.id)");
		
		query = buildquery.toString().toString();
		indvHealthPlanAvailabilityQueriesMap.put(csrValue, query);
		
		return query;
	}
		
	
	public static final String getRegionNameQueryBuilder(PlanInsuranceType insuranceType) {
		if (regionNameQueryMap == null) {
			regionNameQueryMap = new HashMap<String, String>();
		}
		String query;
		query = regionNameQueryMap.get(insuranceType.toString());
		if (query != null) {
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Retrieved region names cache query for insurance type:"+insuranceType );
			}
			return query;
		}
		
		StringBuilder buildQuery = new StringBuilder(1024);
		switch (insuranceType) {
		case HEALTH: {
			buildQuery.append("SELECT DISTINCT prarea.ratingArea ");
			buildQuery.append("FROM ");
			buildQuery.append("RatingArea prarea, PlanRate prate ");
			buildQuery.append("WHERE ");
			buildQuery.append("prate.ratingArea = prarea.id AND prate.plan.id = (:");
			buildQuery.append(PlanMgmtParamConstants.PARAM_PLAN_ID);
			buildQuery.append(")");
			break;
		}
		case DENTAL: {
			buildQuery.append("SELECT DISTINCT prarea.rating_area ");
			buildQuery.append("FROM ");
			buildQuery.append("pm_rating_area prarea, pm_plan_rate prate ");
			buildQuery.append("WHERE ");
			buildQuery.append("prate.rating_area_id = prarea.id AND prate.plan_id = (:");
			buildQuery.append(PlanMgmtParamConstants.PARAM_PLAN_ID);
			buildQuery.append(")");
			break;
		}
		default:
			LOGGER.error("Invalid Insurance type [" + insuranceType + "] received, not supported");
		}
		
		query = buildQuery.toString().trim();
		regionNameQueryMap.put(insuranceType.toString(), query);
		
		return query;
	}
	
	public static final String getCrossWalkPlanIdQuery() {
		if (crossWalkPlanIdQuery != null) {
			return crossWalkPlanIdQuery;
		}
		
		StringBuffer buildQuery = new StringBuffer();
		buildQuery.append("SELECT crs.crosswalk_HIOS_PLAN_ID ");
		buildQuery.append("FROM ");
		buildQuery.append("pm_crosswalk crs, plan p ");
		buildQuery.append("WHERE ");
		buildQuery.append("(crs.CURRENT_HIOS_PLAN_ID=:");
		buildQuery.append(PlanMgmtConstants.HIOS_PLAN_NUMBER);
		buildQuery.append(" OR crs.CURRENT_HIOS_PLAN_ID = substr(:");
		buildQuery.append(PlanMgmtConstants.HIOS_PLAN_NUMBER);
		buildQuery.append(", 0,14)) ");
		buildQuery.append("AND ");
		buildQuery.append("( (crs.county_code=:");
		buildQuery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
		buildQuery.append(" AND crs.zip_list IS NULL AND crs.crosswalk_tier='COUNTY') ");
		buildQuery.append("OR ");
		buildQuery.append("(crs.county_code=:");
		buildQuery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
		buildQuery.append(" AND crs.zip_list LIKE :");
		buildQuery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
		buildQuery.append(" AND crs.crosswalk_tier='ZIP' ) ");
		buildQuery.append("OR ");
		buildQuery.append("(crs.county_code IS NULL AND crs.zip_list IS NULL AND crs.crosswalk_tier='PLAN' ) ) ");
		buildQuery.append("AND crs.applicable_year=:");
		buildQuery.append(PlanMgmtConstants.EFFECTIVE_YEAR);
		buildQuery.append(" AND crs.crosswalk_HIOS_PLAN_ID=substr(p.issuer_plan_number, 0, 14) ");
		buildQuery.append("AND p.applicable_year=:");
		buildQuery.append(PlanMgmtConstants.EFFECTIVE_YEAR);
		buildQuery.append(" AND p.is_deleted='N' ");
		buildQuery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("p", true));
		buildQuery.append(" AND crs.IS_DELETED='N' ");
		
		crossWalkPlanIdQuery = buildQuery.toString();
		
		return crossWalkPlanIdQuery;
	}
	
	
	public static final String getCrosswalkQueryInPlanTable() {
		if (crosswalkQueryInPlanTable != null) {
			return crosswalkQueryInPlanTable;
		}
		
		StringBuffer buildQuery = new StringBuffer(512);
		buildQuery.append("SELECT substr(p.issuer_plan_number, 0, 14) ");
		buildQuery.append("FROM ");
		buildQuery.append("plan p ");
		buildQuery.append("WHERE ");
		buildQuery.append("p.issuer_plan_number=:");
		buildQuery.append(PlanMgmtConstants.HIOS_PLAN_NUMBER);
		buildQuery.append(" AND p.applicable_year=:");
		buildQuery.append(PlanMgmtConstants.EFFECTIVE_YEAR);
		buildQuery.append(" AND p.is_deleted='N' ");
		buildQuery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("p", true));
		
		crosswalkQueryInPlanTable = buildQuery.toString();
		
		return crosswalkQueryInPlanTable;
	}
	

	public static String getPlansByZipAndIssuerName(String countyCode, String insuranceType, String issuerName, String planLevel, PlanInsuranceType planType, String configuredDB) {
		int queryCode = getQueryCode(planLevel, countyCode, issuerName);
		String sb = null;
		
		switch (planType) {
		case HEALTH: {
			sb = getHealthPlansByZipAndIssuerNameQuery(queryCode, configuredDB);
			break;
		}
		case DENTAL: {
			sb = getDentalPlansByZipAndIssuerNameQuery(queryCode, configuredDB);
			break;
		}
		default:
			throw new GIRuntimeException("Invalid plan type " + planType.toString() + ", not supported");
		}
		return sb;

	}
	
	public static final String getHealthPlansByZipAndIssuerNameQuery(int queryCode, String configuredDB) {
		String query = null;
		if(healthPlansByZipAndIssuerNameQueryMap == null){
			healthPlansByZipAndIssuerNameQueryMap = new HashMap<Integer, String>();
			
		}
		
		if(healthPlansByZipAndIssuerNameQueryMap.containsKey(queryCode)){
			query = healthPlansByZipAndIssuerNameQueryMap.get(queryCode);
			if(query != null){
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Retrieved cached query code for health plans by Zip abd Issuer name Query code:"+queryCode);
				}
				return query;
			}
		}		
		
		StringBuilder buildquery = new StringBuilder(1024);
		buildquery.append("SELECT DISTINCT p.name ");
		buildquery.append("FROM ");
		buildquery.append("ServiceArea ps, Issuer i, Plan p, PlanHealth ph, ");
		buildquery.append("TenantPlan tp, Tenant t ");
		buildquery.append("WHERE ");
		buildquery.append("p.issuer.id = i.id ");
		buildquery.append("AND p.serviceAreaId = ps.serviceAreaId.id ");
		buildquery.append("AND p.isDeleted='N' ");
		buildquery.append("AND ph.plan.id = p.id ");
		addSearchFilters(buildquery, queryCode, "ph");
		buildquery.append("AND ps.isDeleted='N' ");

		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			buildquery.append("AND ps.zip = to_number(trim(:");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			buildquery.append(")) ");
		}else{
			buildquery.append("AND ps.zip = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
		}
			
		buildquery.append(" AND p.id = tp.plan.id");
		buildquery.append(" AND tp.tenant.id = t.id");
		buildquery.append(" AND t.code = :"); 
		buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);

		query = buildquery.toString().trim();
		healthPlansByZipAndIssuerNameQueryMap.put(queryCode, query);
		
		buildquery = null;
		return query;
	}

	public static final int getQueryCode(String planLevel, String countyCode, String issuerName) {
		int queryCode = 0;
		if (!StringUtils.isEmpty(planLevel)) {
			queryCode += PLAN_LEVEL;
		}
		if (!StringUtils.isEmpty(countyCode)) {
			queryCode += COUNTY_CODE;
		}
		if (!StringUtils.isEmpty(issuerName)) {
			queryCode += ISSUER_NAME;
		}
		return queryCode;
	}


	public static final String getDentalPlansByZipAndIssuerNameQuery(int queryCode, String configuredDB) {
		String query = null;
		if(dentalPlansByZipAndIssuerNameQueryMap == null){
			dentalPlansByZipAndIssuerNameQueryMap = new HashMap<Integer, String>();
		}
		
		if(dentalPlansByZipAndIssuerNameQueryMap.containsKey(queryCode)){
			query = dentalPlansByZipAndIssuerNameQueryMap.get(queryCode);
			if(query != null){
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Retrieved cached query code for dental plans by Zip abd Issuer name Query code:"+queryCode);
				}
				return query;
			}
		}	
		
		StringBuilder buildquery = new StringBuilder(1024);
		buildquery.append("SELECT DISTINCT p.name ");
		buildquery.append("FROM ");
		buildquery.append("ServiceArea ps, Issuer i, Plan p, PlanDental pd, ");
		buildquery.append("TenantPlan tp, Tenant t ");
		buildquery.append("WHERE ");
		buildquery.append("p.issuer.id = i.id ");
		buildquery.append("AND p.serviceAreaId = ps.serviceAreaId.id ");
		buildquery.append("AND p.isDeleted='N' ");
		buildquery.append("AND pd.plan.id = p.id ");
		addSearchFilters(buildquery, queryCode, "pd");
		buildquery.append("AND ps.isDeleted='N' ");

		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			buildquery.append("AND ps.zip = to_number(trim(:");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			buildquery.append(")) ");
		}else{
			buildquery.append("AND ps.zip = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
		}
		
		buildquery.append(" AND p.id = tp.plan.id");
		buildquery.append(" AND tp.tenant.id = t.id");
		buildquery.append(" AND t.code = :"); 
		buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);

		
		query = buildquery.toString().trim();
		dentalPlansByZipAndIssuerNameQueryMap.put(queryCode, query);
		buildquery = null;
		
		return query;
	}
	

	public static final void addSearchFilters(StringBuilder buildquery, int queryCode, String childTable) {
		switch (queryCode) {
		case (PLAN_LEVEL): {
			buildquery.append("AND ");
			buildquery.append(childTable);
			buildquery.append(".planLevel = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_LEVEL);
			buildquery.append(") ");
			break;
		}
		case (COUNTY_CODE): {
			buildquery.append("AND ps.fips = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
			buildquery.append(") ");
			break;
		}
		case (ISSUER_NAME): {
			buildquery.append("AND i.name = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_ISSUER_NAME);
			buildquery.append(") ");
			break;
		}
		case (PLAN_LEVEL + COUNTY_CODE): {
			buildquery.append("AND ");
			buildquery.append(childTable);
			buildquery.append(".planLevel = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_LEVEL);
			buildquery.append(") ");

			buildquery.append("AND ps.fips = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
			buildquery.append(") ");
			break;
		}
		case (PLAN_LEVEL + ISSUER_NAME): {
			buildquery.append("AND ");
			buildquery.append(childTable);
			buildquery.append(".planLevel = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_LEVEL);
			buildquery.append(") ");

			buildquery.append("AND i.name = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_ISSUER_NAME);
			buildquery.append(") ");
			break;
		}
		case (ISSUER_NAME + COUNTY_CODE): {
			buildquery.append("AND i.name = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_ISSUER_NAME);
			buildquery.append(") ");

			buildquery.append("AND ps.fips = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
			buildquery.append(") ");
			break;
		}
		case (PLAN_LEVEL + COUNTY_CODE + ISSUER_NAME): {
			buildquery.append("AND ");
			buildquery.append(childTable);
			buildquery.append(".planLevel = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_LEVEL);
			buildquery.append(") ");

			buildquery.append("AND i.name = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_ISSUER_NAME);
			buildquery.append(") ");

			buildquery.append("AND ps.fips = (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
			buildquery.append(") ");
			break;
		}
		}
	}

	
	public static final String getProviderNetworkQuery() {
		if (providerNetworkQuery != null) {
			return providerNetworkQuery;
		}
		
		StringBuilder buildquery = new StringBuilder(512);
		buildquery.append("SELECT net.networkKey, net.hasProviderData ");
		buildquery.append("FROM ");
		buildquery.append("Plan p, Network net ");
		buildquery.append("WHERE ");
		buildquery.append(" net.id = p.providerNetworkId ");
		buildquery.append("AND p.id = (:");
		buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_ID);
		buildquery.append(") AND net.certified='YES' and net.verified='YES' ");
		
		providerNetworkQuery = buildquery.toString().trim();
		
		return providerNetworkQuery;
	}


	public static final String getReferenceHealthPlanWithEhbBuilderQuery(String configuredDB) {
		if (referenceHealthPlanWithEhbQuery != null) {
			return referenceHealthPlanWithEhbQuery;
		}
		
		StringBuilder referenceHealthPlanWithEhb = new StringBuilder(2048);
		referenceHealthPlanWithEhb.append("SELECT p.id As plan_id, phealth.planLevel AS plan_level, p.issuerPlanNumber ");
		referenceHealthPlanWithEhb.append(" FROM ");
		referenceHealthPlanWithEhb.append(" Issuer i, Plan p, PlanHealth phealth, ServiceArea psarea ");
		referenceHealthPlanWithEhb.append(" WHERE ");
		referenceHealthPlanWithEhb.append(" p.id = phealth.plan.id ");
		referenceHealthPlanWithEhb.append(" AND p.issuer.id = i.id ");
		referenceHealthPlanWithEhb.append(" AND phealth.parentPlanId = 0 ");
		referenceHealthPlanWithEhb.append(" AND p.isDeleted = 'N' ");
		referenceHealthPlanWithEhb.append(" AND psarea.isDeleted = 'N' ");
		referenceHealthPlanWithEhb.append(" AND p.serviceAreaId = psarea.serviceAreaId.id ");
		

		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			referenceHealthPlanWithEhb.append(" AND psarea.zip = to_number(trim(:");
			referenceHealthPlanWithEhb.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			referenceHealthPlanWithEhb.append("))");
		}else{
			referenceHealthPlanWithEhb.append(" AND psarea.zip = :");
			referenceHealthPlanWithEhb.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
		}
		
		referenceHealthPlanWithEhb.append(" AND psarea.fips = :");
		referenceHealthPlanWithEhb.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
		referenceHealthPlanWithEhb.append(" AND TO_DATE (" + ":param_effectiveDate_0");
		referenceHealthPlanWithEhb.append(", 'YYYY-MM-DD') BETWEEN p.startDate AND p.endDate ");
		referenceHealthPlanWithEhb.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("p", true));
		QuotingBusinessLogicQueryBuilder.buildEnrollmentAvailabilityLogic(referenceHealthPlanWithEhb, "p", PlanMgmtConstants.NO);
		referenceHealthPlanWithEhb.append(" AND p.market = '");
		referenceHealthPlanWithEhb.append(Plan.PlanMarket.SHOP.toString());
		referenceHealthPlanWithEhb.append("'");
		referenceHealthPlanWithEhb.append(" AND p.availableFor IN ('");
		referenceHealthPlanWithEhb.append(PlanMgmtConstants.ADULT_AND_CHILD);
		referenceHealthPlanWithEhb.append("') ");
		referenceHealthPlanWithEhb.append(" AND phealth.ehbCovered = :");
		referenceHealthPlanWithEhb.append(PlanMgmtParamConstants.PARAM_EHB_COVERED);
		
		referenceHealthPlanWithEhbQuery = referenceHealthPlanWithEhb.toString().trim();
		
		return referenceHealthPlanWithEhbQuery;
	}

	public static final String getReferenceHealthPlanWithoutEhbBuilderQuery(String configuredDB) {
		if (referenceHealthPlanWithoutEhbQuery != null) {
			return referenceHealthPlanWithoutEhbQuery;
		}
		
		StringBuilder referenceHealthPlanWithoutEhb = new StringBuilder(2048);
		referenceHealthPlanWithoutEhb.append("SELECT p.id As plan_id, phealth.planLevel AS plan_level, p.issuerPlanNumber ");
		referenceHealthPlanWithoutEhb.append(" FROM ");
		referenceHealthPlanWithoutEhb.append(" Issuer i, Plan p, PlanHealth phealth, ServiceArea psarea ");
		referenceHealthPlanWithoutEhb.append(" WHERE ");
		referenceHealthPlanWithoutEhb.append(" p.id = phealth.plan.id ");
		referenceHealthPlanWithoutEhb.append(" AND p.issuer.id = i.id ");
		referenceHealthPlanWithoutEhb.append(" AND phealth.parentPlanId = 0 ");
		referenceHealthPlanWithoutEhb.append(" AND p.isDeleted = 'N' ");
		referenceHealthPlanWithoutEhb.append(" AND psarea.isDeleted = 'N' ");
		referenceHealthPlanWithoutEhb.append(" AND p.serviceAreaId = psarea.serviceAreaId.id ");

		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			referenceHealthPlanWithoutEhb.append(" AND psarea.zip = to_number(trim(:");
			referenceHealthPlanWithoutEhb.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			referenceHealthPlanWithoutEhb.append("))");
		}else{
			referenceHealthPlanWithoutEhb.append(" AND psarea.zip = :");
			referenceHealthPlanWithoutEhb.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
		}
		
		referenceHealthPlanWithoutEhb.append(" AND psarea.fips = :");
		referenceHealthPlanWithoutEhb.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
		referenceHealthPlanWithoutEhb.append(" AND TO_DATE (" + ":param_effectiveDate_0");
		referenceHealthPlanWithoutEhb.append(", 'YYYY-MM-DD') BETWEEN p.startDate AND p.endDate ");
		referenceHealthPlanWithoutEhb.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("p", true));
		QuotingBusinessLogicQueryBuilder.buildEnrollmentAvailabilityLogic(referenceHealthPlanWithoutEhb, "p", PlanMgmtConstants.NO);
		referenceHealthPlanWithoutEhb.append(" AND p.market = '");
		referenceHealthPlanWithoutEhb.append(Plan.PlanMarket.SHOP.toString());
		referenceHealthPlanWithoutEhb.append("'");
		referenceHealthPlanWithoutEhb.append(" AND p.availableFor IN ('");
		referenceHealthPlanWithoutEhb.append(PlanMgmtConstants.ADULT_AND_CHILD);
		referenceHealthPlanWithoutEhb.append("') ");
		
		referenceHealthPlanWithoutEhbQuery = referenceHealthPlanWithoutEhb.toString().trim();
		
		return referenceHealthPlanWithoutEhbQuery;
	}

	
	public static final String getPlanAvailabilityForEmployeeQuery(String configuredDB) {
		if (planAvailabilityForEmployeeQuery != null) {
			return planAvailabilityForEmployeeQuery;
		}
		
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("SELECT p.id,p.insurance_type ");
		queryBuilder.append("FROM ");
		queryBuilder.append("plan p, pm_service_area psarea ");
		queryBuilder.append("WHERE ");
		queryBuilder.append("p.service_area_id = psarea.service_area_id AND p.issuer_Plan_number IN (?,?) ");
		

		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			queryBuilder.append("AND psarea.zip = to_number(trim(?))");
		}else{
			queryBuilder.append("AND psarea.zip = ?");
		}
		
		queryBuilder.append(" AND psarea.fips = ? AND p.applicable_Year = ? AND p.is_deleted = 'N' AND psarea.is_deleted = 'N' ");
		queryBuilder.append("AND TO_DATE (? , 'DD-MM-YYYY') BETWEEN p.start_date AND p.end_date ");
		queryBuilder.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("p", true));
		queryBuilder.append("AND ((p.enrollment_avail='");
		queryBuilder.append(Plan.EnrollmentAvail.AVAILABLE.toString());
		queryBuilder.append("' AND TO_DATE (?, 'DD-MM-YYYY') >= p.enrollment_avail_effdate  AND ( TO_DATE (?, 'DD-MM-YYYY') < p.future_erl_avail_effdate OR  p.future_erl_avail_effdate is null)) ");
		queryBuilder.append("OR (p.future_enrollment_avail='");
		queryBuilder.append(Plan.EnrollmentAvail.AVAILABLE.toString());
		queryBuilder.append("' AND TO_DATE (?, 'DD-MM-YYYY') >= p.future_erl_avail_effdate)) ");
		queryBuilder.append("AND TO_DATE (?, 'DD-MM-YYYY') >= p.enrollment_avail_effdate");
		
		planAvailabilityForEmployeeQuery = queryBuilder.toString().trim();
		
		return planAvailabilityForEmployeeQuery;
	}
	
	public static final String getHealthPlanDataByPlanIds() {
		if (planDataBenefitQueryForHealthInsurance != null) {
			return planDataBenefitQueryForHealthInsurance;
		}
		
		StringBuilder healthPlansCostQuery = new StringBuilder(2048);
		healthPlansCostQuery
				.append("SELECT P.id AS PLAN_ID, PH.id AS PLAN_HEALTH_ID, TO_CHAR(P.startDate,'YYYY-MM-DD') AS EFFECTIVE_DATE, P.name AS PLAN_NAME, P.insuranceType, P.networkType, P.providerNetworkId, P.issuerPlanNumber, P.issuer.id, P.market, ");
		healthPlansCostQuery.append("I.name AS ISSUER_NAME, I.logoURL, I.hiosIssuerId ");
		healthPlansCostQuery.append("FROM ");
		healthPlansCostQuery.append("PlanHealth PH, Plan P JOIN P.issuer I ");
		healthPlansCostQuery.append("WHERE ");
		healthPlansCostQuery.append("P.isDeleted = 'N' AND P.id = PH.plan.id AND PH.plan.id IN (:param_planIdInputList)");
		healthPlansCostQuery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("P", true));
		healthPlansCostQuery.append("AND TO_DATE (:param_effectiveDate_0"
				+ ", 'YYYY-MM-DD') BETWEEN P.startDate AND P.endDate ");
		QuotingBusinessLogicQueryBuilder.buildEnrollmentAvailabilityLogic(healthPlansCostQuery, "P", PlanMgmtConstants.NO);
		healthPlansCostQuery.append("ORDER BY PH.plan.id ASC");
		
		planDataBenefitQueryForHealthInsurance = healthPlansCostQuery.toString().trim();
		
		return planDataBenefitQueryForHealthInsurance;
	}

	public static final String getDentalPlanDataByPlanIds() {
		if (planDataBenefitQueryForDentalInsurance != null) {
			return planDataBenefitQueryForDentalInsurance;
		}
		
		StringBuilder DentalPlanBenefitQuery = new StringBuilder(2048);
		DentalPlanBenefitQuery
				.append("SELECT P.id AS PLAN_ID, PD.id AS PLAN_DENTAL_ID, TO_CHAR(P.startDate,'YYYY-MM-DD') AS EFFECTIVE_DATE, P.name AS PLAN_NAME, P.insuranceType, P.networkType, P.providerNetworkId, P.issuerPlanNumber, P.issuer.id, P.market, ");
		DentalPlanBenefitQuery.append("I.name AS ISSUER_NAME, I.logoURL, I.hiosIssuerId ");
		DentalPlanBenefitQuery.append("FROM ");
		DentalPlanBenefitQuery.append("Plan P INNER JOIN P.issuer I, PlanDental PD ");
		DentalPlanBenefitQuery.append("WHERE ");
		DentalPlanBenefitQuery.append("P.isDeleted = 'N' AND P.id = PD.plan.id AND PD.plan.id IN (:param_planIdInputList)");
		DentalPlanBenefitQuery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("P", true));
		DentalPlanBenefitQuery.append("AND TO_DATE (:param_effectiveDate_0"
				+ ", 'YYYY-MM-DD') BETWEEN P.startDate AND P.endDate ");
		QuotingBusinessLogicQueryBuilder.buildEnrollmentAvailabilityLogic(DentalPlanBenefitQuery, "P", PlanMgmtConstants.NO);
		DentalPlanBenefitQuery.append("ORDER BY PD.plan.id ASC");
		
		planDataBenefitQueryForDentalInsurance = DentalPlanBenefitQuery.toString().trim();
		
		return planDataBenefitQueryForDentalInsurance;
	}
	
	public static final String getDentalPlanCostsQuery() {
		if (dentalPlanCostQuery != null) {
			return dentalPlanCostQuery;
		}
		
		StringBuilder buildquery = new StringBuilder(512);
		buildquery.append(" SELECT pdc.in_network_ind, pdc.in_network_fly, pdc.in_network_tier2_ind, pdc.in_network_tier2_fly, pdc.out_network_ind, ");
		buildquery.append(" pdc.out_network_fly, pdc.combined_in_out_network_ind, pdc.combined_in_out_network_fly, pdc.comb_def_coins_network_tier1, ");
		buildquery.append(" pdc.comb_def_coins_network_tier2, pdc.name, pdc.plan_dental_id, p.id ");
		buildquery.append(" from plan_dental_cost pdc,plan_dental pd, plan p ");
		buildquery.append(" WHERE pd.id = pdc.PLAN_DENTAL_ID and p.id = pd.plan_id AND");
		buildquery.append(" p.id IN (:param_dentalIdStr)");
		
		dentalPlanCostQuery = buildquery.toString().trim();
		
		return dentalPlanCostQuery;
	}
	
	public static final String getHealthPlanCostsQuery() {
		if (healthPlanCostsQuery != null) {
			return healthPlanCostsQuery;
		}
		
		StringBuilder buildquery = new StringBuilder(1024);
		buildquery.append(" SELECT phc.in_network_ind, phc.in_network_fly, phc.in_network_tier2_ind, phc.in_network_tier2_fly, phc.out_network_ind, ");
		buildquery.append(" phc.out_network_fly, phc.combined_in_out_network_ind, phc.combined_in_out_network_fly, phc.comb_def_coins_network_tier1, ");
		buildquery.append(" phc.comb_def_coins_network_tier2, phc.name, phc.plan_health_id, phc.limit_excep_display, p.id ");
		buildquery.append(" from plan_health_cost phc,plan_health ph, plan p WHERE p.id = ph.plan_id and ph.id = phc.PLAN_HEALTH_ID");
		buildquery.append(" and p.id IN (");
		buildquery.append(":param_healthIdStr)");
		
		healthPlanCostsQuery = buildquery.toString().trim();
		
		return healthPlanCostsQuery;
	}

	public static final String getHealthPlanInfoByZipWithoutCountyFips(String tenantCode, String configuredDB) {
		if(healthPlanInfoByZipWithoutCountyFipsQuery == null){
			healthPlanInfoByZipWithoutCountyFipsQuery = new HashMap<String, String>();
		}else if (healthPlanInfoByZipWithoutCountyFipsQuery.containsKey(tenantCode)) {
			return healthPlanInfoByZipWithoutCountyFipsQuery.get(tenantCode);
		}

		StringBuilder buildquery = new StringBuilder(1024);
		if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
			buildquery.append("SELECT P3.id, P3.issuer_Plan_Number, P3.PLAN_NAME, PH.cost_Sharing, PH.plan_Level, I.id AS ISSUER_ID, I.name AS ISSUER_NAME, P3.INSURANCE_TYPE, I.hios_issuer_id, P3.enrollment_Avail_EffDate, P3.PLAN_YEAR ");
			buildquery.append("FROM ");
			buildquery.append("(SELECT P.id, P.issuer_Plan_Number, P.name AS PLAN_NAME, P.insurance_Type AS INSURANCE_TYPE, P.enrollment_Avail_EffDate, P.applicable_Year AS PLAN_YEAR, P.issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P ");
			buildquery.append("WHERE ");
			buildquery.append("P.id IN ");
			buildquery.append("(SELECT id ");
			buildquery.append(" FROM ");
			buildquery.append(" (SELECT P_PSA.ID from ");
			buildquery.append(" (SELECT P2.id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P2, PM_Service_Area PSA2 ");
			buildquery.append("WHERE ");
			buildquery.append("P2.SERVICE_AREA_ID = PSA2.service_area_id ");
			buildquery.append("AND PSA2.is_Deleted = 'N' ");
			
			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("AND PSA2.zip = TO_NUMBER(trim(:");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
				buildquery.append(")) ");
			}else{
				buildquery.append("AND PSA2.zip = :");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			}
			
			buildquery.append(" ) P_PSA ");
			buildquery.append(" , pm_tenant_plan tp, tenant t ");
			buildquery.append("WHERE ");
			buildquery.append("P_PSA.id = tp.plan_id ");
			buildquery.append("AND tp.tenant_id = t.id ");
			buildquery.append("AND t.code = :" );
			buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);
			buildquery.append(" ) ");
			buildquery.append(") ");
			buildquery.append("AND P.applicable_Year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
			buildquery.append(" AND P.is_Deleted  = 'N' ");
			buildquery.append("AND P.exchange_Type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append("AND P.enrollment_Avail = 'AVAILABLE' AND P.insurance_type ='HEALTH' ");
			buildquery.append(") P3, Issuers I, plan_health PH ");
			buildquery.append("WHERE ");
			buildquery.append("P3.issuer_id = I.id ");
			buildquery.append("AND P3.id = PH.plan_id ");
			buildquery.append("ORDER BY P3.ID ");
		}
		else if(tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){ // when tenant code ALL don't use tenant filter
			buildquery.append("SELECT P3.id, P3.issuer_Plan_Number, P3.PLAN_NAME, PH.cost_Sharing,  PH.plan_Level, I.id AS ISSUER_ID, I.name AS ISSUER_NAME, P3.INSURANCE_TYPE, I.hios_issuer_id, P3.enrollment_Avail_EffDate, P3.PLAN_YEAR ");
			buildquery.append("FROM ");
			buildquery.append("(SELECT P.id, P.issuer_Plan_Number, P.name AS PLAN_NAME, P.insurance_Type AS INSURANCE_TYPE, P.enrollment_Avail_EffDate, P.applicable_Year AS PLAN_YEAR, P.issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P ");
			buildquery.append("WHERE ");
			buildquery.append("P.id IN ");
			buildquery.append("(SELECT id ");
			buildquery.append(" FROM ");
			buildquery.append(" (SELECT P2.id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P2, PM_Service_Area PSA2 ");
			buildquery.append("WHERE ");
			buildquery.append("P2.SERVICE_AREA_ID = PSA2.service_area_id ");
			buildquery.append("AND PSA2.is_Deleted = 'N' ");

			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("AND PSA2.zip = TO_NUMBER(trim(:");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
				buildquery.append(")) ");
			}else{
				buildquery.append("AND PSA2.zip = :");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			}
			
			buildquery.append(" ) P_PSA ");
			buildquery.append(") ");
			buildquery.append("AND P.applicable_Year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
			buildquery.append(" AND P.is_Deleted  = 'N' ");
			buildquery.append("AND P.exchange_Type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append("AND P.enrollment_Avail = 'AVAILABLE' AND P.insurance_type ='HEALTH' ");
			buildquery.append(") P3, Issuers I, plan_health PH ");
			buildquery.append("WHERE ");
			buildquery.append("P3.issuer_id = I.id ");
			buildquery.append("AND P3.id = PH.plan_id ");
			buildquery.append("ORDER BY P3.ID ");
		}
		
		healthPlanInfoByZipWithoutCountyFipsQuery.put(tenantCode, buildquery.toString().trim());
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT_buildquery for Health :: " + healthPlanInfoByZipWithoutCountyFipsQuery.get(tenantCode));
		}
		
		return healthPlanInfoByZipWithoutCountyFipsQuery.get(tenantCode);
	}

	public static final String getHealthPlanInfoByZipWithCountyFips(String tenantCode, String configuredDB) {
		if(healthPlanInfoByZipWithCountyFipsQuery == null){
			healthPlanInfoByZipWithCountyFipsQuery = new HashMap<String, String>();
		}else if (healthPlanInfoByZipWithCountyFipsQuery.containsKey(tenantCode)) {
			return healthPlanInfoByZipWithCountyFipsQuery.get(tenantCode);
		}
		
		StringBuilder buildquery = new StringBuilder(1024);
		if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
			buildquery.append("SELECT P3.id, P3.issuer_Plan_Number, P3.PLAN_NAME, PH.cost_Sharing, PH.plan_Level, I.id AS ISSUER_ID, I.name AS ISSUER_NAME, P3.INSURANCE_TYPE, I.hios_issuer_id, P3.enrollment_Avail_EffDate,	P3.PLAN_YEAR ");
			buildquery.append("FROM ");
			buildquery.append("(SELECT P.id, P.issuer_Plan_Number, P.name AS PLAN_NAME, P.insurance_Type AS INSURANCE_TYPE, P.enrollment_Avail_EffDate, P.applicable_Year AS PLAN_YEAR, P.issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P ");
			buildquery.append("WHERE ");
			buildquery.append("P.id IN ");
			buildquery.append("(SELECT id ");
			buildquery.append(" FROM ");
			buildquery.append(" (SELECT P_PSA.ID from ");
			buildquery.append(" (SELECT P2.id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P2, PM_Service_Area PSA2 ");
			buildquery.append("WHERE ");
			buildquery.append("P2.SERVICE_AREA_ID = PSA2.service_area_id ");
			buildquery.append("AND PSA2.is_Deleted = 'N' ");

			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("AND PSA2.zip = TO_NUMBER(trim(:");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
				buildquery.append(")) ");
			}else{
				buildquery.append("AND PSA2.zip = :");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			}
			
			buildquery.append(" AND PSA2.fips = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);	
			buildquery.append(" ) P_PSA ");
			buildquery.append(" , pm_tenant_plan tp, tenant t ");
			buildquery.append("WHERE ");
			buildquery.append("P_PSA.id = tp.plan_id ");
			buildquery.append("AND tp.tenant_id = t.id ");
			buildquery.append("AND t.code = :" );
			buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);
			buildquery.append(" ) ");
			buildquery.append(") ");
			buildquery.append("AND P.applicable_Year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
			buildquery.append(" AND P.is_Deleted  = 'N' ");
			buildquery.append("AND P.exchange_Type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append("AND P.enrollment_Avail = 'AVAILABLE' AND P.insurance_type ='HEALTH' ");
			buildquery.append(") P3, Issuers I, Plan_Health PH ");
			buildquery.append("WHERE ");
			buildquery.append("P3.issuer_id = I.id ");
			buildquery.append("AND P3.id = PH.plan_id ");
			buildquery.append("ORDER BY P3.ID ");

		}
		else if(tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){ // when tenant code ALL don't use tenant filter
			buildquery.append("SELECT P3.id, P3.issuer_Plan_Number, P3.PLAN_NAME, PH.cost_Sharing, PH.plan_Level, I.id AS ISSUER_ID, I.name AS ISSUER_NAME, P3.INSURANCE_TYPE, I.hios_issuer_id, P3.enrollment_Avail_EffDate, P3.PLAN_YEAR ");
			buildquery.append("FROM ");
			buildquery.append("(SELECT P.id, P.issuer_Plan_Number, P.name AS PLAN_NAME, P.insurance_Type AS INSURANCE_TYPE, P.enrollment_Avail_EffDate, P.applicable_Year AS PLAN_YEAR, P.issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P ");
			buildquery.append("WHERE ");
			buildquery.append("P.id IN ");
			buildquery.append("(SELECT id ");
			buildquery.append(" FROM ");
			buildquery.append(" (SELECT P2.id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P2, PM_Service_Area PSA2 ");
			buildquery.append("WHERE ");
			buildquery.append("P2.SERVICE_AREA_ID = PSA2.service_area_id ");
			buildquery.append("AND PSA2.is_Deleted = 'N' ");
			
			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("AND PSA2.zip = TO_NUMBER(trim(:");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
				buildquery.append("))");
			}else{
				buildquery.append("AND PSA2.zip = :");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			}
			
			buildquery.append(" AND PSA2.fips = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);	
			buildquery.append(" ) P_PSA ");
			buildquery.append(") ");
			buildquery.append("AND P.applicable_Year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
			buildquery.append(" AND P.is_Deleted  = 'N' ");
			buildquery.append("AND P.exchange_Type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append("AND P.enrollment_Avail = 'AVAILABLE' AND P.insurance_type ='HEALTH' ");
			buildquery.append(") P3, Issuers I, Plan_Health PH ");
			buildquery.append("WHERE ");
			buildquery.append("P3.issuer_id = I.id ");
			buildquery.append("AND P3.id = PH.plan_id ");
			buildquery.append("ORDER BY P3.ID ");

			}	
			
		healthPlanInfoByZipWithCountyFipsQuery.put(tenantCode, buildquery.toString().trim());		
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT_buildquery for Health :: " + healthPlanInfoByZipWithCountyFipsQuery.get(tenantCode));
		}
		
		return healthPlanInfoByZipWithCountyFipsQuery.get(tenantCode);
	}

	public static final String getDentalPlanInfoByZipWithoutCountyFips(String tenantCode, String configuredDB) {
		if(dentalPlanInfoByZipWithoutCountyFipsQuery == null){
			dentalPlanInfoByZipWithoutCountyFipsQuery = new HashMap<String, String>();
		}else if (dentalPlanInfoByZipWithoutCountyFipsQuery.containsKey(tenantCode)) {
			return dentalPlanInfoByZipWithoutCountyFipsQuery.get(tenantCode);
		}
		
		StringBuilder buildquery = new StringBuilder(1024);
		
		if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
			buildquery.append("SELECT P3.id, P3.issuer_Plan_Number, P3.PLAN_NAME, PD.cost_Sharing, PD.plan_Level, I.id AS ISSUER_ID, I.name AS ISSUER_NAME, P3.INSURANCE_TYPE, I.hios_issuer_id, P3.enrollment_Avail_EffDate,	P3.PLAN_YEAR ");
			buildquery.append("FROM ");
			buildquery.append("(SELECT P.id, P.issuer_Plan_Number, P.name AS PLAN_NAME, P.insurance_Type AS INSURANCE_TYPE, P.enrollment_Avail_EffDate, P.applicable_Year AS PLAN_YEAR, P.issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P ");
			buildquery.append("WHERE ");
			buildquery.append("P.id IN ");
			buildquery.append("(SELECT id ");
			buildquery.append(" FROM ");
			buildquery.append(" (SELECT P_PSA.ID from ");
			buildquery.append(" (SELECT P2.id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P2, PM_Service_Area PSA2 ");
			buildquery.append("WHERE ");
			buildquery.append("P2.SERVICE_AREA_ID = PSA2.service_area_id ");
			buildquery.append("AND PSA2.is_Deleted = 'N' ");
			
			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("AND PSA2.zip = TO_NUMBER(trim(:");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
				buildquery.append(")) ");
			}else{
				buildquery.append("AND PSA2.zip = :");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			}
			
			buildquery.append(" ) P_PSA ");
			buildquery.append(" , pm_tenant_plan tp, tenant t ");
			buildquery.append("WHERE ");
			buildquery.append("P_PSA.id = tp.plan_id ");
			buildquery.append("AND tp.tenant_id = t.id ");
			buildquery.append("AND t.code = :" );
			buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);
			buildquery.append(" ) ");
			buildquery.append(") ");
			buildquery.append("AND P.applicable_Year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
			buildquery.append(" AND P.is_Deleted  = 'N' ");
			buildquery.append("AND P.exchange_Type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append("AND P.enrollment_Avail = 'AVAILABLE' AND P.insurance_type ='DENTAL' ");
			buildquery.append(") P3, Issuers I, plan_dental PD ");
			buildquery.append("WHERE ");
			buildquery.append("P3.issuer_id = I.id ");
			buildquery.append("AND P3.id = PD.plan_id ");
			buildquery.append("ORDER BY P3.ID ");
		}
		else if(tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){ // when tenant code ALL don't use tenant filter
			buildquery.append("SELECT P3.id, P3.issuer_Plan_Number, P3.PLAN_NAME, PD.cost_Sharing, PD.plan_Level, I.id AS ISSUER_ID, I.name AS ISSUER_NAME, P3.INSURANCE_TYPE, I.hios_issuer_id, P3.enrollment_Avail_EffDate, P3.PLAN_YEAR ");
			buildquery.append("FROM ");
			buildquery.append("(SELECT P.id, P.issuer_Plan_Number, P.name AS PLAN_NAME,  P.insurance_Type AS INSURANCE_TYPE, P.enrollment_Avail_EffDate, P.applicable_Year AS PLAN_YEAR, P.issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P ");
			buildquery.append("WHERE ");
			buildquery.append("P.id IN ");
			buildquery.append("(SELECT id ");
			buildquery.append(" FROM ");
			buildquery.append(" (SELECT P2.id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P2, PM_Service_Area PSA2 ");
			buildquery.append("WHERE ");
			buildquery.append("P2.SERVICE_AREA_ID = PSA2.service_area_id ");
			buildquery.append("AND PSA2.is_Deleted = 'N' ");
			
			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("AND PSA2.zip = TO_NUMBER(trim(:");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
				buildquery.append(")) ");
			}else{
				buildquery.append("AND PSA2.zip = :");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			}
			
			buildquery.append(" ) P_PSA ");
			buildquery.append(") ");
			buildquery.append("AND P.applicable_Year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
			buildquery.append(" AND P.is_Deleted  = 'N' ");
			buildquery.append("AND P.exchange_Type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append("AND P.enrollment_Avail = 'AVAILABLE' AND P.insurance_type ='DENTAL' ");
			buildquery.append(") P3, Issuers I, plan_dental PD ");
			buildquery.append("WHERE ");
			buildquery.append("P3.issuer_id = I.id ");
			buildquery.append("AND P3.id = PD.plan_id ");
			buildquery.append("ORDER BY P3.ID ");
		}
		
		dentalPlanInfoByZipWithoutCountyFipsQuery.put(tenantCode, buildquery.toString().trim());
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT buildquery for dental  :: " + dentalPlanInfoByZipWithoutCountyFipsQuery.get(tenantCode));
		}
		
		return dentalPlanInfoByZipWithoutCountyFipsQuery.get(tenantCode);
	}

	public static final String getDentalPlanInfoByZipWithCountyFips(String tenantCode, String configuredDB) {
		if(dentalPlanInfoByZipWithCountyFipsQuery == null){
			dentalPlanInfoByZipWithCountyFipsQuery = new HashMap<String, String>();
		}else if (dentalPlanInfoByZipWithCountyFipsQuery.containsKey(tenantCode)) {
			return dentalPlanInfoByZipWithCountyFipsQuery.get(tenantCode);
		}
		
		StringBuilder buildquery = new StringBuilder(1024);
		
		if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
			buildquery.append("SELECT P3.id, P3.issuer_Plan_Number, P3.PLAN_NAME, PD.cost_Sharing, PD.plan_Level, I.id AS ISSUER_ID, I.name AS ISSUER_NAME, P3.INSURANCE_TYPE, I.hios_issuer_id, P3.enrollment_Avail_EffDate,	P3.PLAN_YEAR ");
			buildquery.append("FROM ");
			buildquery.append("(SELECT P.id, P.issuer_Plan_Number, P.name AS PLAN_NAME, P.insurance_Type AS INSURANCE_TYPE, P.enrollment_Avail_EffDate, P.applicable_Year AS PLAN_YEAR, P.issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P ");
			buildquery.append("WHERE ");
			buildquery.append("P.id IN ");
			buildquery.append("(SELECT id ");
			buildquery.append(" FROM ");
			buildquery.append(" (SELECT P_PSA.ID from ");
			buildquery.append(" (SELECT P2.id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P2, PM_Service_Area PSA2 ");
			buildquery.append("WHERE ");
			buildquery.append("P2.SERVICE_AREA_ID = PSA2.service_area_id ");
			buildquery.append("AND PSA2.is_Deleted = 'N' ");
			
			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("AND PSA2.zip = TO_NUMBER(trim(:");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
				buildquery.append("))");
			}else{
				buildquery.append("AND PSA2.zip = :");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			}
			
			buildquery.append(" AND PSA2.fips = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);	
			buildquery.append(" ) P_PSA ");
			buildquery.append(" , pm_tenant_plan tp, tenant t ");
			buildquery.append("WHERE ");
			buildquery.append("P_PSA.id = tp.plan_id ");
			buildquery.append("AND tp.tenant_id = t.id ");
			buildquery.append("AND t.code = :" );
			buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);
			buildquery.append(" ) ");
			buildquery.append(") ");
			buildquery.append("AND P.applicable_Year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
			buildquery.append(" AND P.is_Deleted  = 'N' ");
			buildquery.append("AND P.exchange_Type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append("AND P.enrollment_Avail = 'AVAILABLE' AND P.insurance_type ='DENTAL' ");
			buildquery.append(") P3, Issuers I, plan_dental PD ");
			buildquery.append("WHERE ");
			buildquery.append("P3.issuer_id = I.id ");
			buildquery.append("AND P3.id = PD.plan_id ");
			buildquery.append("ORDER BY P3.ID ");
		}
		else if(tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){ // when tenant code ALL don't use tenant filter
			buildquery.append("SELECT P3.id, P3.issuer_Plan_Number, P3.PLAN_NAME, PD.cost_Sharing, PD.plan_Level, I.id AS ISSUER_ID, I.name AS ISSUER_NAME, P3.INSURANCE_TYPE, I.hios_issuer_id, P3.enrollment_Avail_EffDate, P3.PLAN_YEAR ");
			buildquery.append("FROM ");
			buildquery.append("(SELECT P.id, P.issuer_Plan_Number, P.name AS PLAN_NAME,  P.insurance_Type AS INSURANCE_TYPE, P.enrollment_Avail_EffDate, P.applicable_Year AS PLAN_YEAR, P.issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P ");
			buildquery.append("WHERE ");
			buildquery.append("P.id IN ");
			buildquery.append("(SELECT id ");
			buildquery.append(" FROM ");
			buildquery.append(" (SELECT P2.id ");
			buildquery.append("FROM ");
			buildquery.append("Plan P2, PM_Service_Area PSA2 ");
			buildquery.append("WHERE ");
			buildquery.append("P2.SERVICE_AREA_ID = PSA2.service_area_id ");
			buildquery.append("AND PSA2.is_Deleted = 'N' ");
			
			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("AND PSA2.zip = TO_NUMBER(trim(:");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
				buildquery.append("))");
			}else{
				buildquery.append("AND PSA2.zip = :");
				buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			}
			
			buildquery.append(" AND PSA2.fips = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);	
			buildquery.append(" ) P_PSA ");
			buildquery.append(") ");
			buildquery.append("AND P.applicable_Year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
			buildquery.append(" AND P.is_Deleted  = 'N' ");
			buildquery.append("AND P.exchange_Type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append("AND P.enrollment_Avail = 'AVAILABLE' AND P.insurance_type ='DENTAL' ");
			buildquery.append(") P3, Issuers I, plan_dental PD ");
			buildquery.append("WHERE ");
			buildquery.append("P3.issuer_id = I.id ");
			buildquery.append("AND P3.id = PD.plan_id ");
			buildquery.append("ORDER BY P3.ID ");
		}
		
		dentalPlanInfoByZipWithCountyFipsQuery.put(tenantCode, buildquery.toString().trim());
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("INPUT_buildquery for dental :: " + dentalPlanInfoByZipWithCountyFipsQuery.get(tenantCode));
		}
		
		return dentalPlanInfoByZipWithCountyFipsQuery.get(tenantCode);
	}
		
	public static final String getStmPlanQuery(String tenantCode) {
		if(stmPlanQuery == null){
			 stmPlanQuery = new HashMap<String, String>();
		}else if (stmPlanQuery.containsKey(tenantCode)) {
			return stmPlanQuery.get(tenantCode);
		}
		StringBuilder buildquery = new StringBuilder(1024);
		
		// pull records irrespective of tenant code
		if(tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){ // when tenant code ALL don't use tenant filter
			buildquery.append("SELECT P.id, P.issuer_plan_number, P.name, P.state, P.enrollment_Avail_EffDate, I.id AS ISSUER_ID , I.name AS ISSUER_NAME, P.insurance_type, I.hios_issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("plan P, issuers I ");
			buildquery.append("WHERE ");
			buildquery.append("P.issuer_id = I.id ");
			/* For STM Applicable year does not exist it is always be 0 */
			buildquery.append(" AND P.is_deleted = 'N' AND P.insurance_type = '");
			buildquery.append(Plan.PlanInsuranceType.STM);
			buildquery.append("' AND P.state IN (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_STATE_LIST);
			buildquery.append(") AND P.exchange_type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append(" ORDER BY P.id");
		}
		else if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
			buildquery.append("SELECT P.id, P.issuer_plan_number, P.name, P.state, P.enrollment_Avail_EffDate, I.id AS ISSUER_ID , I.name AS ISSUER_NAME, P.insurance_type, I.hios_issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("plan P, issuers I, pm_tenant_plan tp, tenant t ");
			buildquery.append("WHERE ");
			buildquery.append("P.issuer_id = I.id ");
			/* For STM Applicable year does not exist it is always be 0 */
			buildquery.append(" AND P.is_deleted = 'N' AND P.insurance_type = '");
			buildquery.append(Plan.PlanInsuranceType.STM);
			buildquery.append("' AND P.state IN (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_STATE_LIST);
			buildquery.append(") AND P.exchange_type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(" AND P.id = tp.plan_id");
			buildquery.append(" AND tp.tenant_id = t.id");
			buildquery.append(" AND t.code = :"); 
			buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append(" ORDER BY P.id");
		}
		
		stmPlanQuery.put(tenantCode, buildquery.toString().trim());
		
		return stmPlanQuery.get(tenantCode);
	}
	
	public static final String getAmePlanQuery(String tenantCode) {
		if(amePlanQuery == null){
			amePlanQuery = new HashMap<String, String>();
		}else if (amePlanQuery.containsKey(tenantCode)) {
			return amePlanQuery.get(tenantCode);
		}
		
		StringBuilder buildquery = new StringBuilder(1024);
		
		// pull records irrespective of tenant code
		if(tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){ // when tenant code ALL don't use tenant filter
			buildquery.append("SELECT P.id, P.issuer_plan_number, P.name, P.state, P.enrollment_Avail_EffDate, I.id AS ISSUER_ID , I.name AS ISSUER_NAME, P.insurance_type, I.hios_issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("plan P, issuers I ");
			buildquery.append("WHERE ");
			buildquery.append("P.issuer_id = I.id ");
			/* For STM Applicable year does not exist it is always be 0 */
			buildquery.append(" AND P.is_deleted = 'N' AND P.insurance_type = '");
			buildquery.append(Plan.PlanInsuranceType.AME);
			buildquery.append("' AND P.state IN (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_STATE_LIST);
			buildquery.append(") AND P.exchange_type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append(" ORDER BY P.id");
		}
		else if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
			buildquery.append("SELECT P.id, P.issuer_plan_number, P.name, P.state, P.enrollment_Avail_EffDate, I.id AS ISSUER_ID , I.name AS ISSUER_NAME, P.insurance_type, I.hios_issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("plan P, issuers I, pm_tenant_plan tp, tenant t ");
			buildquery.append("WHERE ");
			buildquery.append("P.issuer_id = I.id ");
			/* For STM Applicable year does not exist it is always be 0 */
			buildquery.append(" AND P.is_deleted = 'N' AND P.insurance_type = '");
			buildquery.append(Plan.PlanInsuranceType.AME);
			buildquery.append("' AND P.state IN (:");
			buildquery.append(PlanMgmtParamConstants.PARAM_STATE_LIST);
			buildquery.append(") AND P.exchange_type = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_EXCHANGE_TYPE);
			buildquery.append(" AND P.id = tp.plan_id");
			buildquery.append(" AND tp.tenant_id = t.id");
			buildquery.append(" AND t.code = :"); 
			buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append(" ORDER BY P.id");
		}
		
		amePlanQuery.put(tenantCode, buildquery.toString().trim());
		
		return amePlanQuery.get(tenantCode);
	}
		
	public static final String getDentalPlanBenefitsQuery() {
		if (dentalPlanBenefitsQuery != null) {
			return dentalPlanBenefitsQuery;
		}

		StringBuilder buildquery = new StringBuilder(1024);
		buildquery.append("SELECT PDB.limitation, PDB.limitation_attr, PDB.network_exceptions, ");
		buildquery.append("PDB.NETWORK_T1_COPAY_VAL, PDB.NETWORK_T1_COPAY_ATTR, PDB.NETWORK_T1_COINSURANCE_VAL, PDB.NETWORK_T1_COINSURANCE_ATTR, ");
		buildquery.append("PDB.NETWORK_T2_COPAY_VAL, PDB.NETWORK_T2_COPAY_ATTR, PDB.NETWORK_T2_COINSURANCE_VAL, PDB.NETWORK_T2_COINSURANCE_ATTR, ");
		buildquery.append("PDB.OUTNETWORK_COPAY_VAL, PDB.OUTNETWORK_COPAY_ATTR, PDB.OUTNETWORK_COINSURANCE_VAL, PDB.OUTNETWORK_COINSURANCE_ATTR, ");
		buildquery.append("PDB.subject_to_in_net_deductible, PDB.subject_to_out_net_deductible, PDB.name, PDB.plan_dental_id, ");
		buildquery.append("PDB.NETWORK_T1_DISPLAY, PDB.NETWORK_T2_DISPLAY, PDB.OUTNETWORK_DISPLAY, PDB.LIMIT_EXCEP_DISPLAY, PDB.IS_COVERED, PDB.NETWORK_T1_TILE_DISPLAY, P.ID, PD.ehb_appt_for_pediatric_dental ");
		buildquery.append("FROM ");
		buildquery.append("PLAN P, PLAN_DENTAL PD, PLAN_DENTAL_BENEFIT PDB ");
		buildquery.append("WHERE P.ID = PD.PLAN_ID AND PD.ID = PDB.PLAN_DENTAL_ID AND P.ID IN (:param_dentalIdStr");
		buildquery.append(") AND PDB.name IN (:param_dentalBenefitListStr");
		buildquery.append(")");
		
		dentalPlanBenefitsQuery = buildquery.toString().trim();
		
		return dentalPlanBenefitsQuery;
	}
	
	public static final String getHealthPlanBenefitQuery() {
		if (healthPlanBenefitQuery != null) {
			return healthPlanBenefitQuery;
		}
		
		StringBuilder buildquery = new StringBuilder();
		buildquery.append("SELECT PHB.limitation, PHB.limitation_attr, PHB.network_exceptions, ");
		buildquery.append("PHB.NETWORK_T1_COPAY_VAL, PHB.NETWORK_T1_COPAY_ATTR, PHB.NETWORK_T1_COINSURANCE_VAL, PHB.NETWORK_T1_COINSURANCE_ATTR, ");
		buildquery.append("PHB.NETWORK_T2_COPAY_VAL, PHB.NETWORK_T2_COPAY_ATTR, PHB.NETWORK_T2_COINSURANCE_VAL, PHB.NETWORK_T2_COINSURANCE_ATTR, ");
		buildquery.append("PHB.OUTNETWORK_COPAY_VAL, PHB.OUTNETWORK_COPAY_ATTR, PHB.OUTNETWORK_COINSURANCE_VAL, PHB.OUTNETWORK_COINSURANCE_ATTR, ");
		buildquery.append("PHB.subject_to_in_net_deductible, PHB.subject_to_out_net_deductible, PHB.name, PHB.PLAN_HEALTH_ID, ");
		buildquery.append("PHB.NETWORK_T1_DISPLAY, PHB.NETWORK_T2_DISPLAY, PHB.OUTNETWORK_DISPLAY, PHB.LIMIT_EXCEP_DISPLAY, PHB.IS_COVERED, PHB.NETWORK_T1_TILE_DISPLAY, P.ID ");
		buildquery.append("FROM ");
		buildquery.append("PLAN P, PLAN_HEALTH PH, PLAN_HEALTH_BENEFIT PHB ");
		buildquery.append("WHERE ");
		buildquery.append("P.ID = PH.PLAN_ID AND PH.ID = PHB.PLAN_HEALTH_ID AND P.ID IN (:param_healthIdStr) AND PHB.name IN (:param_healthBenefitListStr)");
		
		healthPlanBenefitQuery = buildquery.toString().trim();
		
		return healthPlanBenefitQuery;
	}
	
	public static final String getMetalTierAvailBuiderQueryWithEhbCovered(String configuredDB) {
		if (metalTierQueryWithEhbQuery != null) {
			return metalTierQueryWithEhbQuery;
		}
		
		StringBuilder metalTierQueryWithEhb = new StringBuilder(2048);
		metalTierQueryWithEhb.append("SELECT phealth.plan_level AS plan_level, count(p.id) AS NO_OF_PALNS, (CASE WHEN count(p.id) > 0 THEN  'YES' ELSE 'NO' END) AS EXIST_FLAG ");
		metalTierQueryWithEhb.append("FROM issuers i, plan p, plan_health phealth, pm_service_area psarea ");
		metalTierQueryWithEhb.append("WHERE p.id = phealth.plan_id AND p.issuer_id = i.id AND phealth.parent_plan_id = 0 AND p.is_deleted = 'N' ");
		metalTierQueryWithEhb.append("AND psarea.is_deleted = 'N' AND p.service_area_id = psarea.service_area_id ");
		
		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			metalTierQueryWithEhb.append("AND psarea.zip = to_number(trim(:param_empPrimaryZip))");
		}else{
			metalTierQueryWithEhb.append("AND psarea.zip = :param_empPrimaryZip");
		}
		metalTierQueryWithEhb.append(" AND psarea.fips = (:param_empCountyCode");
		metalTierQueryWithEhb.append(") AND TO_DATE (:param_effectiveDate_0" + ", 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date ");
		metalTierQueryWithEhb.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("p", true));
		QuotingBusinessLogicQueryBuilder.buildEnrollmentAvailabilityLogic(metalTierQueryWithEhb, "p", PlanMgmtConstants.NO);
		metalTierQueryWithEhb.append(" AND p.market = '");
		metalTierQueryWithEhb.append(Plan.PlanMarket.SHOP.toString());
		metalTierQueryWithEhb.append("'");
		metalTierQueryWithEhb.append(" AND p.available_for IN ('" + PlanMgmtConstants.ADULT_AND_CHILD + "') ");
		metalTierQueryWithEhb.append(" AND phealth.ehb_covered = :param_ehbCovered");
		metalTierQueryWithEhb.append(" AND phealth.plan_level != 'CATASTROPHIC' ");
		metalTierQueryWithEhb.append(" GROUP BY phealth.plan_level");
		
		metalTierQueryWithEhbQuery = metalTierQueryWithEhb.toString().trim();
		
		return metalTierQueryWithEhbQuery;
	}

	public static final String getMetalTierAvailBuiderQueryWithoutEhbCovered(String configuredDB) {
		if (metalTierQueryWithoutEhbQuery != null) {
			return metalTierQueryWithoutEhbQuery;
		}
		
		StringBuilder metalTierQueryWithoutEhb = new StringBuilder(2048);
		metalTierQueryWithoutEhb.append("SELECT phealth.plan_level AS plan_level, count(p.id) AS NO_OF_PALNS, (CASE WHEN count(p.id) > 0 THEN  'YES' ELSE 'NO' END) AS EXIST_FLAG ");
		metalTierQueryWithoutEhb.append("FROM issuers i, plan p, plan_health phealth, pm_service_area psarea ");
		metalTierQueryWithoutEhb.append("WHERE p.id = phealth.plan_id AND p.issuer_id = i.id AND phealth.parent_plan_id = 0 AND p.is_deleted = 'N' ");
		metalTierQueryWithoutEhb.append("AND psarea.is_deleted = 'N' AND p.service_area_id = psarea.service_area_id ");
		
		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			metalTierQueryWithoutEhb.append("AND psarea.zip = to_number(trim(:param_empPrimaryZip ))");
		}else{
			metalTierQueryWithoutEhb.append("AND psarea.zip = :param_empPrimaryZip");
		}
		
		metalTierQueryWithoutEhb.append(" AND psarea.fips = (:param_empCountyCode");
		metalTierQueryWithoutEhb.append(") AND TO_DATE (:param_effectiveDate_0" + ", 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date ");
		metalTierQueryWithoutEhb.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("p", true));
		QuotingBusinessLogicQueryBuilder.buildEnrollmentAvailabilityLogic(metalTierQueryWithoutEhb, "p", PlanMgmtConstants.NO);
		metalTierQueryWithoutEhb.append(" AND p.market = '");
		metalTierQueryWithoutEhb.append(Plan.PlanMarket.SHOP.toString());
		metalTierQueryWithoutEhb.append("'");
		metalTierQueryWithoutEhb.append(" AND p.available_for IN ('");
		metalTierQueryWithoutEhb.append(PlanMgmtConstants.ADULT_AND_CHILD);
		metalTierQueryWithoutEhb.append("') ");
		metalTierQueryWithoutEhb.append(" AND phealth.plan_level != 'CATASTROPHIC' ");
		metalTierQueryWithoutEhb.append(" GROUP BY phealth.plan_level");
		
		metalTierQueryWithoutEhbQuery = metalTierQueryWithoutEhb.toString().trim();
		
		return metalTierQueryWithoutEhbQuery;
	}

	public static final String getStateListByZip(String configuredDB){
		if(stateListbyZipQuery != null){
			return stateListbyZipQuery;
		}		
		
		StringBuilder buildquery = new StringBuilder(512);
		buildquery.append("SELECT DISTINCT(state) ");
		buildquery.append("FROM ");
		buildquery.append("ServiceArea PSA ");
		buildquery.append("WHERE ");
		
		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			buildquery.append("PSA.zip = to_number(trim(:");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			buildquery.append(")) ");
		}else{
			buildquery.append("PSA.zip = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
		}
		
		stateListbyZipQuery = buildquery.toString();		
		
		return stateListbyZipQuery;
	}
	
	public static final String getStateListByZipAndCounty(String configuredDB){
		if(stateListbyZipAndCountyQuery != null){
			return stateListbyZipAndCountyQuery;
		}
		
		StringBuilder buildquery = new StringBuilder(512);
		buildquery.append("SELECT DISTINCT(state) ");
		buildquery.append("FROM ");
		buildquery.append("ServiceArea PSA ");
		buildquery.append("WHERE ");
		
		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			buildquery.append("PSA.zip = to_number(trim(:");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
			buildquery.append("))");
		}else{
			buildquery.append("PSA.zip = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
		}
		
		buildquery.append(" AND PSA.fips = (:");
		buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
		buildquery.append(")");
		
		stateListbyZipAndCountyQuery = buildquery.toString();
		
		return stateListbyZipAndCountyQuery;
	}
	
	
	public static String getPlanDataBenefitQueryForDentalInsurance() {
		if (planBenefitForDentalInsuranceQuery != null) {
			return planBenefitForDentalInsuranceQuery;
		}
		
		StringBuilder dentalPlanBenefitQueryBuilder = new StringBuilder(2048);
		dentalPlanBenefitQueryBuilder.append("SELECT P.id AS PLAN_ID, PD.id AS PLAN_HEALTH_ID, TO_CHAR(P.startDate,'YYYY-MM-DD') AS EFFECTIVE_DATE, P.name AS PLAN_NAME, P.insuranceType, P.networkType, P.providerNetworkId, P.issuerPlanNumber, P.issuer.id, P.market, ");
		dentalPlanBenefitQueryBuilder.append("I.name AS ISSUER_NAME, I.logoURL, I.hiosIssuerId, P.hsa ");
		dentalPlanBenefitQueryBuilder.append("FROM ");
		dentalPlanBenefitQueryBuilder.append("Plan P INNER JOIN P.issuer I, PlanDental PD ");
		dentalPlanBenefitQueryBuilder.append("WHERE ");
		dentalPlanBenefitQueryBuilder.append("P.isDeleted = 'N' AND P.id = PD.plan.id AND PD.plan.id IN (:param_planIdInputList)");
		dentalPlanBenefitQueryBuilder.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("P", true));
		dentalPlanBenefitQueryBuilder.append("AND TO_DATE (:param_effectiveDate_0"
				+ ", 'YYYY-MM-DD') BETWEEN P.startDate AND P.endDate ");
		QuotingBusinessLogicQueryBuilder.buildEnrollmentAvailabilityLogic(dentalPlanBenefitQueryBuilder, "P", PlanMgmtConstants.NO);
		dentalPlanBenefitQueryBuilder.append("ORDER BY PD.plan.id ASC");
		
		planBenefitForDentalInsuranceQuery = dentalPlanBenefitQueryBuilder.toString().trim();
		
		return planBenefitForDentalInsuranceQuery;
	}
	
	public static String getPlanBenefitQueryForHealthInsurance() {
		if (planBenefitForHealthInsuranceQuery != null) {
			return planBenefitForHealthInsuranceQuery;
		}
		
		StringBuilder healthPlanBenefitQueryBuilder = new StringBuilder(2048);
		healthPlanBenefitQueryBuilder
				.append("SELECT P.id AS PLAN_ID, PH.id AS PLAN_HEALTH_ID, TO_CHAR(P.startDate,'YYYY-MM-DD') AS EFFECTIVE_DATE, P.name AS PLAN_NAME, P.insuranceType, P.networkType, P.providerNetworkId, P.issuerPlanNumber, P.issuer.id, P.market, ");
		healthPlanBenefitQueryBuilder.append("I.name AS ISSUER_NAME, I.logoURL, I.hiosIssuerId, P.hsa ");
		healthPlanBenefitQueryBuilder.append("FROM ");
		healthPlanBenefitQueryBuilder.append("Plan P INNER JOIN P.issuer I, PlanHealth PH ");
		healthPlanBenefitQueryBuilder.append("WHERE ");
		healthPlanBenefitQueryBuilder.append("P.isDeleted = 'N' AND P.id = PH.plan.id AND PH.plan.id IN (:param_planIdInputList)");
		healthPlanBenefitQueryBuilder.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogicJPA("P", true));
		healthPlanBenefitQueryBuilder.append("AND TO_DATE (:param_effectiveDate_0"
				+ ", 'YYYY-MM-DD') BETWEEN P.startDate AND P.endDate ");
		QuotingBusinessLogicQueryBuilder.buildEnrollmentAvailabilityLogic(healthPlanBenefitQueryBuilder, "P", PlanMgmtConstants.NO);
		healthPlanBenefitQueryBuilder.append("ORDER BY PH.plan.id ASC");
		
		planBenefitForHealthInsuranceQuery = healthPlanBenefitQueryBuilder.toString().trim();

		return planBenefitForHealthInsuranceQuery;
	}
	
	
	public static String getLifePlanQuotingQuery(){
		if(null != lifePlanQuetingQuery){
			return lifePlanQuetingQuery;
		}
		
		StringBuilder lifePlanQuotingQueryBuilder = new StringBuilder(2048);
		lifePlanQuotingQueryBuilder.append("SELECT p.id, p.name, i.id, i.name, i.logoURL, pr.rate, life.coverageMax, life.planDuration, life.benefitURL ");
		lifePlanQuotingQueryBuilder.append("FROM ");
		lifePlanQuotingQueryBuilder.append("Plan p, PlanLife life, Issuer i, PlanRate pr, TenantPlan tp, Tenant t ");
		lifePlanQuotingQueryBuilder.append("WHERE ");
		lifePlanQuotingQueryBuilder.append("p.issuer.id = i.id AND life.plan.id=p.id AND p.isDeleted = 'N' AND pr.plan.id = p.id AND pr.isDeleted = 'N'  AND p.insuranceType = 'LIFE' " );
		lifePlanQuotingQueryBuilder.append("AND p.state =:");
		lifePlanQuotingQueryBuilder.append(PlanMgmtParamConstants.PARAM_STATE_CODE);
		lifePlanQuotingQueryBuilder.append(" AND TO_DATE (:");
		lifePlanQuotingQueryBuilder.append(PlanMgmtParamConstants.PARAM_EFFECTIVE_DATE);
		lifePlanQuotingQueryBuilder.append(", 'YYYY-MM-DD') BETWEEN p.startDate AND p.endDate ");
		lifePlanQuotingQueryBuilder.append(" AND TO_DATE (:");
		lifePlanQuotingQueryBuilder.append(PlanMgmtParamConstants.PARAM_EFFECTIVE_DATE);
		lifePlanQuotingQueryBuilder.append(", 'YYYY-MM-DD') BETWEEN pr.effectiveStartDate AND pr.effectiveEndDate AND :");
		lifePlanQuotingQueryBuilder.append(PlanMgmtParamConstants.PARAM_AGE);
		lifePlanQuotingQueryBuilder.append(" >= pr.minAge AND :");
		lifePlanQuotingQueryBuilder.append(PlanMgmtParamConstants.PARAM_AGE);
		lifePlanQuotingQueryBuilder.append(" <= pr.maxAge AND pr.gender = :");
		lifePlanQuotingQueryBuilder.append(PlanMgmtParamConstants.PARAM_GENDER);
		lifePlanQuotingQueryBuilder.append(" AND pr.tobacco = :");
		lifePlanQuotingQueryBuilder.append(PlanMgmtParamConstants.PARAM_TOBACCO);
		lifePlanQuotingQueryBuilder.append(" AND p.id = tp.plan.id ");
		lifePlanQuotingQueryBuilder.append("AND tp.tenant.id = t.id ");
		lifePlanQuotingQueryBuilder.append("AND t.code = :");
		lifePlanQuotingQueryBuilder.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);
		
		lifePlanQuetingQuery = lifePlanQuotingQueryBuilder.toString().trim();
		
		return lifePlanQuetingQuery;
	}
	
	
	public static final String getMedicarePlanQuery(String tenantCode) {
		if(medicarePlanQuery == null){
			medicarePlanQuery = new HashMap<String, String>();
		}else if (medicarePlanQuery.containsKey(tenantCode)) {
			return medicarePlanQuery.get(tenantCode);
		}
		StringBuilder buildquery = new StringBuilder(1024);
		
		// pull records irrespective of tenant code
		if(tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){ // when tenant code ALL don't use tenant filter
			buildquery.append("SELECT P.id, P.issuer_plan_number, P.name, P.state, P.enrollment_Avail_EffDate, I.id AS ISSUER_ID, I.name AS ISSUER_NAME, P.insurance_type, I.hios_issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("plan P, issuers I, plan_medicare PM ");
			buildquery.append("WHERE ");
			buildquery.append("P.issuer_id = I.id ");
			buildquery.append("AND PM.plan_id = P.id ");
			buildquery.append("AND UPPER(PM.data_source) =:");
			buildquery.append(PlanMgmtParamConstants.PARAM_DATA_SOURCE);
			buildquery.append(" AND P.applicable_year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_APPLICABLE_YEAR);
			buildquery.append(" AND P.is_deleted = 'N' AND P.insurance_type =:");
			buildquery.append(PlanMgmtParamConstants.PARAM_INSURANCE_TYPE);
			buildquery.append(" AND P.state =:");
			buildquery.append(PlanMgmtParamConstants.PARAM_STATE_CODE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append(" ORDER BY P.id");
		}
		else if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
			buildquery.append("SELECT P.id, P.issuer_plan_number, P.name, P.state, P.enrollment_Avail_EffDate, I.id AS ISSUER_ID , I.name AS ISSUER_NAME, P.insurance_type, I.hios_issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("plan P, issuers I, pm_tenant_plan tp, tenant t, plan_medicare PM ");
			buildquery.append("WHERE ");
			buildquery.append("P.issuer_id = I.id ");
			buildquery.append("AND PM.plan_id = P.id ");
			buildquery.append("AND UPPER(PM.data_source) =:");
			buildquery.append(PlanMgmtParamConstants.PARAM_DATA_SOURCE);
			buildquery.append(" AND P.applicable_year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_APPLICABLE_YEAR);
			buildquery.append(" AND P.is_deleted = 'N' AND P.insurance_type =:");
			buildquery.append(PlanMgmtParamConstants.PARAM_INSURANCE_TYPE);
			buildquery.append(" AND P.state =:");
			buildquery.append(PlanMgmtParamConstants.PARAM_STATE_CODE);
			buildquery.append(" AND P.id = tp.plan_id");
			buildquery.append(" AND tp.tenant_id = t.id");
			buildquery.append(" AND t.code = :"); 
			buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append(" ORDER BY P.id");
		}
		
		medicarePlanQuery.put(tenantCode, buildquery.toString().trim());
		
		return medicarePlanQuery.get(tenantCode);
	}
	
	
	public static final String getMedicarePlanForAllQuery(String tenantCode) {
		if(medicarePlanForAllQuery == null){
			medicarePlanForAllQuery = new HashMap<String, String>();
		}else if (medicarePlanForAllQuery.containsKey(tenantCode)) {
			return medicarePlanForAllQuery.get(tenantCode);
		}
		StringBuilder buildquery = new StringBuilder(1024);
		
		// pull records irrespective of tenant code
		if(tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){ // when tenant code ALL don't use tenant filter
			buildquery.append("SELECT P.id, P.issuer_plan_number, P.name, P.state, P.enrollment_Avail_EffDate, I.id AS ISSUER_ID, I.name AS ISSUER_NAME, P.insurance_type, I.hios_issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("plan P, issuers I ");
			buildquery.append("WHERE ");
			buildquery.append("P.issuer_id = I.id ");
			buildquery.append("AND P.applicable_year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_APPLICABLE_YEAR);
			buildquery.append(" AND P.is_deleted = 'N' AND P.insurance_type =:");
			buildquery.append(PlanMgmtParamConstants.PARAM_INSURANCE_TYPE);
			buildquery.append(" AND P.state =:");
			buildquery.append(PlanMgmtParamConstants.PARAM_STATE_CODE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append(" ORDER BY P.id");
		}
		else if(!tenantCode.equalsIgnoreCase(PlanMgmtConstants.ALL)){
			buildquery.append("SELECT P.id, P.issuer_plan_number, P.name, P.state, P.enrollment_Avail_EffDate, I.id AS ISSUER_ID , I.name AS ISSUER_NAME, P.insurance_type, I.hios_issuer_id ");
			buildquery.append("FROM ");
			buildquery.append("plan P, issuers I, pm_tenant_plan tp, tenant t ");
			buildquery.append("WHERE ");
			buildquery.append("P.issuer_id = I.id ");
			buildquery.append("AND P.applicable_year = :");
			buildquery.append(PlanMgmtParamConstants.PARAM_APPLICABLE_YEAR);
			buildquery.append(" AND P.is_deleted = 'N' AND P.insurance_type =:");
			buildquery.append(PlanMgmtParamConstants.PARAM_INSURANCE_TYPE);
			buildquery.append(" AND P.state =:");
			buildquery.append(PlanMgmtParamConstants.PARAM_STATE_CODE);
			buildquery.append(" AND P.id = tp.plan_id");
			buildquery.append(" AND tp.tenant_id = t.id");
			buildquery.append(" AND t.code = :"); 
			buildquery.append(PlanMgmtParamConstants.PARAM_TENANT_CODE);
			buildquery.append(QuotingBusinessLogicQueryBuilder.buildPlanCertificationLogic("P", true));
			buildquery.append(" ORDER BY P.id");
		}
		
		medicarePlanForAllQuery.put(tenantCode, buildquery.toString().trim());
		
		return medicarePlanForAllQuery.get(tenantCode);
	}
	
	public static final String getRatingAreaByYearQuery(){
		if(ratingAreaByYearQuery != null){
			return ratingAreaByYearQuery;
		}
		
		StringBuilder buildquery = new StringBuilder(512);
		buildquery.append("SELECT id, state, rarea FROM ( ");
		buildquery.append("SELECT rarea.id AS id, rarea.state AS state, rarea.rating_area AS rarea, row_number() OVER (ORDER by zipcra.applicable_Year DESC) AS seqnum ");
		buildquery.append("FROM ");
		buildquery.append("pm_rating_area rarea, pm_zip_county_rating_area zipcra ");
		buildquery.append("WHERE ");
		buildquery.append("zipcra.rating_Area_id  = rarea.id ");
		buildquery.append("AND (zipcra.zip =:"); 
		buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
		buildquery.append(" OR zipcra.zip IS NULL) ");
		buildquery.append("AND zipcra.county_Fips =:");
		buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);
		buildquery.append(" AND zipcra.applicable_Year <=:");
		buildquery.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
		buildquery.append(" ) rowline ");
		buildquery.append("WHERE ");
		buildquery.append("seqnum = 1 ");
		
		ratingAreaByYearQuery = buildquery.toString();

		return ratingAreaByYearQuery;
	}
}
