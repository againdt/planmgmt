package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.IssuerServiceArea;

@Repository("pmIssuerServiceAreaRepository")
public interface IIssuerServiceAreaRepository extends JpaRepository<IssuerServiceArea, Integer> {
	IssuerServiceArea findById(int id);					  
}

