/**
 * STM plan Service interface.
 * 
 * @author santanu
 * @version 1.0
 * @since Apr 24, 2014
 * 
 */
package com.getinsured.hix.planmgmt.service;


import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.model.ZipCode;
/*import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.phix.ws.assurant.Demographics;
import com.getinsured.hix.planmgmt.phix.ws.assurant.GetPlans;
import com.getinsured.hix.planmgmt.phix.ws.assurant.GetPlansRequest;
import com.getinsured.hix.planmgmt.phix.ws.assurant.GetPlansResponse;*/
import com.getinsured.hix.planmgmt.phix.ws.ihc.ArrayOfParticipant;
import com.getinsured.hix.planmgmt.phix.ws.ihc.Bundle;
import com.getinsured.hix.planmgmt.phix.ws.ihc.BundleGroupQuoteRequest;
import com.getinsured.hix.planmgmt.phix.ws.ihc.BundleQuoteResponse;
import com.getinsured.hix.planmgmt.phix.ws.ihc.Gender;
import com.getinsured.hix.planmgmt.phix.ws.ihc.GenericBenefit;
import com.getinsured.hix.planmgmt.phix.ws.ihc.GetMarketplaceBundleQuoteResponse;
import com.getinsured.hix.planmgmt.phix.ws.ihc.GetMarketplaceBundleQuoteResponseResponse;
import com.getinsured.hix.planmgmt.phix.ws.ihc.Participant;
import com.getinsured.hix.planmgmt.phix.ws.ihc.PaymentFrequency;
import com.getinsured.hix.planmgmt.phix.ws.ihc.ProductType;
import com.getinsured.hix.planmgmt.phix.ws.ihc.Quote;
import com.getinsured.hix.planmgmt.phix.ws.uhc.ArrayOfChild;
import com.getinsured.hix.planmgmt.phix.ws.uhc.ArrayOfPlanRate;
import com.getinsured.hix.planmgmt.phix.ws.uhc.AuthorizationType;
import com.getinsured.hix.planmgmt.phix.ws.uhc.Child;
import com.getinsured.hix.planmgmt.phix.ws.uhc.GetQuote;
import com.getinsured.hix.planmgmt.phix.ws.uhc.GetQuoteResponse;
import com.getinsured.hix.planmgmt.phix.ws.uhc.Plan;
import com.getinsured.hix.planmgmt.phix.ws.uhc.PlanRate;
import com.getinsured.hix.planmgmt.phix.ws.uhc.PrimaryInsuredType;
import com.getinsured.hix.planmgmt.phix.ws.uhc.QuoteRequestType;
import com.getinsured.hix.planmgmt.phix.ws.uhc.SpouseInsuredType;
import com.getinsured.hix.planmgmt.phix.ws.uhc.WebQuoteRequest;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
/*import com.getinsured.hix.platform.config.DynamicPropertiesUtil;*/
import com.getinsured.hix.platform.repository.IZipCodeRepository;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;


@Service("STMPlanRateBenefitService")
@Repository
@Transactional
@DependsOn("dynamicPropertiesUtil")
public class STMPlanRateBenefitServiceImpl implements STMPlanRateBenefitService {

	private static final Logger LOGGER = Logger.getLogger(STMPlanRateBenefitServiceImpl.class);
	private static final String RELATION = "relation";
	private static final String ZIP = "zip";
	private static final String AGE = "age";
	private static final String CHILD = "child";	
	private static final String COUNTY_FIPS = "countycode";
//	private static final Integer NUMBER_ZERO = PlanMgmtConstants.ZERO;
	private static final String SELF = "self";
	private static final String GENDER = "gender";
	private static final String SPOUSE = "spouse";		
	private static final String TOBACCO = "tobacco";
	private static final String IHC_HIOS_ISSUER_ID = "00001";
	private static final String UHC_HIOS_ISSUER_ID = "00002";
//	private static final String ASSURANT_HIOS_ISSUER_ID = "00003";	
	//private static final String IHCREFERALCODE= "HED";
//	private String TIC_CHANNEL = "TIC";
//	private String ALL = "All";
//	private String PAYMENT_MODE = "M";
	public static final String DASH = "-";
	private String ZIP_CODE_LOG = "Zip code ";
	private String BELONGS_TO_LOG = " belongs to state ";
	private static final String DOCUMENT_URL = "download/document?documentId=";
	private static final BigDecimal FACT_FEES = new BigDecimal(4);
	private static final List<String> UHC_NON_FACT_FEES_STATE = Arrays.asList("GA","KY","SC","WY","NV","OR");
	private static final String DATE_OF_BIRTH = "dateOfBirth";
	
	//private final String ihcHpaCode = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.IHC_IHCHPACODE);
//	private final String iMQuotingTest = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.ASSURANT_I_MQUOTING_TEST);
//	private final String password = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.ASSURANT_PASSWORD);
//	private final String agentNumber = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.ASSURANT_AGENT_NUMBER);
	
	
	@Value("#{configProp['stm.IHC.WsdlUrl']}")
	private String ihcWsdlUrl;

	@Value("#{configProp['stm.UHC.WsdlUrl']}")
	private String uhcWsdlUrl;

//	@Value("#{configProp['stm.ASSURANT.WsdlUrl']}")
//	private String assurantWsdlUrl;

	@Value("#{configProp['stm.IHC.SoapActionUrl']}")
	private String ihcSoapActionUrl;

	private String UHCApplicationID = "75CCBA97-4151-4571-820C-DE2DA8407483";
	@Value("#{configProp['stm.UHC.SoapActionUrl']}")
	private String uhcSoapActionUrl;
	
//	@Value("#{configProp['stm.ASSURANT.SoapActionUrl']}")
//	private String assurantSoapActionUrl;

	@Value("#{configProp['appUrl']}")
	private String appUrl;

	@PersistenceUnit private EntityManagerFactory emf;
	
	@Autowired private WebServiceTemplate webServiceTemplate;
	
	@Autowired private WebServiceTemplate webServiceTemplateXMLIHC;
	
	@Autowired private IZipCodeRepository zipCodeRepository;	 
	@Autowired private QuotingBusinessLogic quotingBusinessLogic; 
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
		
	//private static final String PLANSTMIDSTR = "planStmIdStr";
	
	@Override
	public List<PlanRateBenefit> getStmPlanRateBenefits(List<Map<String, String>> memberList, String effectiveDate, String tenantCode) {
		
				
		List<PlanRateBenefit> plan = new ArrayList<PlanRateBenefit>();			
			
		try{
			// call quoteIHCWebService to get plan premium from IHC web service
			LOGGER.info("Calling IHC web service.....");
			List<PlanRateBenefit> ihcPlan = new ArrayList<PlanRateBenefit>();	
			ihcPlan  = quoteIHCWebService(memberList,  effectiveDate, tenantCode);
			if(!(ihcPlan.isEmpty())){
				plan.addAll(ihcPlan);
			}
			LOGGER.debug(ihcPlan.size() + " of IHC STM plans match with our data");	
		}catch(Exception ex){			
			LOGGER.error("Exception occured in quoteIHCWebService: ", ex);
		}
		
		try{
			// call quoteIHCWebService to get plan premium from UHC web service
			LOGGER.info("Calling UHC web service.....");
			List<PlanRateBenefit> uhcPlan = new ArrayList<PlanRateBenefit>();	
			uhcPlan  = quoteUHCWebService( memberList,  effectiveDate, tenantCode);			
			LOGGER.debug(uhcPlan.size() + " of UHC STM plans match with our data");
			if(!(uhcPlan.isEmpty())){
				plan.addAll(uhcPlan);
			}
		}catch(Exception ex){			
			LOGGER.error("Exception occured in quoteUHCWebService: ", ex);
		}
		// Below code is commented out as part of HIX-71154
		/*try{
			// call quoteAssurantWebService to get plan premium from Assurant webService
			LOGGER.info("Calling Assurant web Service.....");
			List<PlanRateBenefit> assurantPlan = new ArrayList<PlanRateBenefit>();	
			assurantPlan  = quoteAssurantWebService( memberList,  effectiveDate);			
			LOGGER.debug(assurantPlan.size() + " of Assurant plans match with our data");
			if(!(assurantPlan.isEmpty())){
				plan.addAll(assurantPlan);
			}
		}catch(Exception ex){			
			LOGGER.error("Exception occured in quoteAssurantWebService: ", ex);
		}*/
		
		int applicantAge = 0;
		for (Map<String, String> member : memberList) {					
			if (member.get(RELATION) != null) {								
				if(member.get(RELATION).equalsIgnoreCase(SELF)){							
					applicantAge = Integer.parseInt(member.get(AGE));					
				}						
			}
		}
		// quote HHC carrier when applicant's age > 1 else do not
		if(applicantAge > 1){
			try{
				// call quoteHCCWebService to get plan premium 
				LOGGER.info("Calling HCC web service.....");
				List<PlanRateBenefit> hccPlan = null;	
				hccPlan  = quoteHCCWebService( memberList, effectiveDate, tenantCode);			
				
				if(!CollectionUtils.isEmpty(hccPlan)){
					plan.addAll(hccPlan);
				}
			}catch(Exception ex){			
				LOGGER.error("Exception occured in quoteHCCWebService: ", ex);
			}
		}	
		
		return plan;
	}
	
	// Call ICH web service to get plan rate on run time on basis of census information and effective date.
	private List<PlanRateBenefit> quoteIHCWebService(List<Map<String, String>> memberList, String effectiveDateStr, String tenantCode) {		
		Map<String, String> planPremiumArr = new HashMap<String, String>();
		StringBuilder query_Value_Builder = new StringBuilder();
		List<PlanRateBenefit> ihcPlan  = new ArrayList<PlanRateBenefit>();
		
		com.getinsured.hix.planmgmt.phix.ws.ihc.ObjectFactory objectFactory = new com.getinsured.hix.planmgmt.phix.ws.ihc.ObjectFactory();	
		GetMarketplaceBundleQuoteResponse request = new GetMarketplaceBundleQuoteResponse();
		BundleGroupQuoteRequest bundleGrpQuotReq =	processIHCQuoteRequest(memberList, effectiveDateStr, objectFactory);
		
		
		JAXBElement<BundleGroupQuoteRequest> soapRequest = objectFactory.createGetMarketplaceBundleQuoteResponseQuoteRequest(bundleGrpQuotReq);
		request.setQuoteRequest(soapRequest);
		
		// prepare request data	end
		String stateCode = quotingBusinessLogic.getApplicantStateCode(memberList);
		GetMarketplaceBundleQuoteResponseResponse response = null;
		BundleQuoteResponse bundleQuoteResp =  null;
		
		try{
			// set WSDL url
			webServiceTemplateXMLIHC.setDefaultUri(ihcWsdlUrl);
			LOGGER.info("Calling IHC WSDL... Submiting request...");
			// send request			
			response =  (GetMarketplaceBundleQuoteResponseResponse) webServiceTemplateXMLIHC.marshalSendAndReceive(request,  new SoapActionCallback(ihcSoapActionUrl));
			
			if(response != null){
				LOGGER.info("Get response from IHC web service. Parsing SOAP response...");
				// parse SOAP response
				bundleQuoteResp = response.getGetMarketplaceBundleQuoteResponseResult().getValue();	
				if(bundleQuoteResp != null){
					List<Bundle> bundleLists = bundleQuoteResp.getBundleGroup().getValue().getBundles().getValue().getBundle();
					
					if(bundleLists != null){
						StringBuilder key = new StringBuilder();
						for(Bundle bundle : bundleLists){
							List<com.getinsured.hix.planmgmt.phix.ws.ihc.Plan> planList = bundle.getPrimaryProduct().getValue().getPlans().getValue().getPlan();
							for(com.getinsured.hix.planmgmt.phix.ws.ihc.Plan ihcStmPlan : planList){
								key.setLength(0);
								String planName = ihcStmPlan.getName().getValue();
								
								List<Quote> quoteList = ihcStmPlan.getQuotes().getValue().getQuote();
								
								for(Quote quote : quoteList){
									BigDecimal premium = quote.getPremium().getValue().getMonthlyPayment();								
									Integer coinsurance = null;
									String deductible = PlanMgmtConstants.EMPTY_STRING;
									String duration = PlanMgmtConstants.EMPTY_STRING;
									String oopMax =  PlanMgmtConstants.EMPTY_STRING;
									
									key.setLength(0); 
									key.append("name-");
									key.append(planName);
									
									List<GenericBenefit>  genericBenefitList = quote.getGenericBenefits().getValue().getGenericBenefit();
									if(genericBenefitList != null){
									//	String key = "name-" + planName + "|co-"+ coinsurance + "|de-" + deductible + "|du-"+ duration + "|oop-" + oopMax; 
										// parsing generic benefits
										for (GenericBenefit genericBenefit : genericBenefitList){											
											if(genericBenefit.getName().getValue().equalsIgnoreCase("CoinsuranceName")){
												coinsurance =  PlanMgmtConstants.HUNDRED - Integer.parseInt(genericBenefit.getValue().getValue());
											}
											if(genericBenefit.getName().getValue().equalsIgnoreCase("DeductibleName")){
												deductible = genericBenefit.getValue().getValue();
											}
											if(genericBenefit.getName().getValue().equalsIgnoreCase("BenefitPeriodInMonths")){
												duration = genericBenefit.getValue().getValue();
											}
											if(genericBenefit.getName().getValue().equalsIgnoreCase("OutOfPocketName")){
												oopMax = genericBenefit.getValue().getValue();
											}
											//HIX-107088 if OutOfPocketINN field is received instead of OutOfPocketName benefit field
											if(StringUtils.isBlank(oopMax) && genericBenefit.getName().getValue().equalsIgnoreCase("OutOfPocketINN")){
												oopMax = genericBenefit.getValue().getValue();
											}
										}
										
										key.append("|co-");
										key.append(coinsurance);
										key.append("|de-");
										key.append(deductible);
										key.append("|du-");
										key.append(duration);
										key.append("|oop-");
										key.append(oopMax);
										
										//LOGGER.info(key);
										//LOGGER.info("Coinsurance = " + coinsurance +": Deductible = " + deductible + ": BenefitPeriod = " + duration + ": OutOfPocket = " + oopMax + ": Premium = " + premium);
										
									}
									
									if(null != premium && StringUtils.isNotBlank(planName) && coinsurance !=null && StringUtils.isNotBlank(deductible) 
												&& StringUtils.isNotBlank(duration) && StringUtils.isNotBlank(oopMax)) {
											planPremiumArr.put(key.toString(), premium.toString());
											StringBuilder strValueBuilder = new StringBuilder();
											strValueBuilder.append(PlanMgmtConstants.OPENING_BRACKET + PlanMgmtConstants.SINGLE_QUOTE +planName+ PlanMgmtConstants.SINGLE_QUOTE + PlanMgmtConstants.COMMA + coinsurance + PlanMgmtConstants.COMMA + deductible + PlanMgmtConstants.COMMA + duration + PlanMgmtConstants.COMMA + oopMax + PlanMgmtConstants.CLOSING_BRACKET);
											if(query_Value_Builder.toString().isEmpty()){
												query_Value_Builder.append(strValueBuilder);
											}else{
												query_Value_Builder.append(PlanMgmtConstants.COMMA + strValueBuilder);
											}
									}else {
										LOGGER.info("Skipped querying for IHC STM plan as one of the value received is null");
										LOGGER.info("STM plan values received from IHC, premium:" + premium + " planName:" + planName +
												" Coinsurance:" + coinsurance + " Deductible:" + deductible + " Duration:" + duration + " OOPMax:" + oopMax);
									}
									
								}								
								
							}
						}
					}	
				}				
				
				if(query_Value_Builder.length() > PlanMgmtConstants.ZERO){
					ihcPlan = getListOfPlanIDAndPlanSTMID(query_Value_Builder, planPremiumArr, stateCode, IHC_HIOS_ISSUER_ID, tenantCode);
				} 
				
			}
			else{
				LOGGER.info("Response not found from IHC web service");
			}	
			
		}catch(WebServiceIOException wsIOE){
			LOGGER.error(wsIOE);
		}catch(SoapFaultClientException sfce){
			sfce.printStackTrace();
			LOGGER.error(sfce.getMessage() + PlanMgmtConstants.TRIPLE_DASH_STRING + sfce.toString());
		}catch(Exception e) {
			LOGGER.error(e);
		}
		
		return ihcPlan;		
	}
	
	@Transactional(readOnly = true)
	private List<PlanRateBenefit> getListOfPlanIDAndPlanSTMID(StringBuilder temp_Builder, Map<String, String> premiumListArr, String stateCode, String hiosIssuerId, String tenantCode){
		List<PlanRateBenefit> plan = new ArrayList<PlanRateBenefit>();			
		String key = null;
		EntityManager entityManager = null;
		StringBuilder buildquery = new StringBuilder();
		
		/*	Need to check Plan Certified and Re-certified Condition	*/
		buildquery.append("SELECT P.ID, PS.ID as PLAN_STM_ID, P.NAME AS PLAN_NAME, I.NAME AS ISSUE_NAME, P.ISSUER_ID, P.BROCHURE, I.LOGO_URL, PS.MAX_DURATION, PS.OOP_MAX_VAL, PS.COINSURANCE, PSC.IN_NETWORK_IND AS DEDUCTIBLE, P.NETWORK_TYPE, PS.DURATION_UNIT, PS.POLICY_MAX, PS.OUT_OF_NETWORK_COVERAGE, PS.OUT_OF_COUNTRY_COVERAGE, PSC.SEPARATE_DEDT_PRESCPT_DRUG AS SEPARATE_DRUG_DEDUCTIBLE, PS.OOP_MAX_ATTR, PS.OOP_MAX_FAMILY, PS.PLAN_RESP_NAME, PS.EXCLUSIONS, PS.POLICY_MAX_ATTR ");
		buildquery.append("FROM ");
		buildquery.append("PLAN P, ISSUERS I, PLAN_STM PS, PLAN_STM_COST PSC, pm_tenant_plan tp, tenant t ");
		buildquery.append("WHERE ");
		buildquery.append("I.ID = P.ISSUER_ID AND P.ID = PS.PLAN_ID AND P.INSURANCE_TYPE = 'STM' AND PS.ID  = PSC.PLAN_STM_ID ");
		buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic("P", true));
		buildquery.append("AND (PS.PLAN_RESP_NAME, PS.COINSURANCE, PSC.IN_NETWORK_IND, PS.MAX_DURATION, PS.OOP_MAX_VAL) IN (");
		buildquery.append(temp_Builder);
		buildquery.append(") ");
		buildquery.append(" AND p.state='" + stateCode + "'");
		buildquery.append(" AND p.is_deleted='N'");	
		buildquery.append(" AND i.hios_issuer_id='" + hiosIssuerId +"'");
		buildquery.append(" AND p.id = tp.plan_id");
		buildquery.append(" AND tp.tenant_id = t.id");
		buildquery.append(" AND t.code = UPPER('" + tenantCode +"') ");		
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("STM QUERY :: " + buildquery);
		}
		
		try {
			entityManager = emf.createEntityManager();
			List rsList = entityManager.createNativeQuery(buildquery.toString()).getResultList();
			Iterator rsIterator = rsList.iterator();
			Iterator rsIterator2 = rsList.iterator();
			String planStmIdStr = StringUtils.EMPTY;
			String brochureUrl = null;
			String exclusionsAndLimitations = null;
			Object[] rsObjectArray = null;
			
			while (rsIterator.hasNext()) {
				String logoURL = null;

				rsObjectArray = (Object[]) rsIterator.next();
				brochureUrl = rsObjectArray[PlanMgmtConstants.FIVE] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.FIVE]).trim());
				exclusionsAndLimitations = rsObjectArray[PlanMgmtConstants.TWENTY] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.TWENTY]).trim());
				
				if (StringUtils.isNotBlank(brochureUrl)) {
					
					if (brochureUrl.matches(PlanMgmtConstants.ECM_REGEX)) {
						brochureUrl = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(brochureUrl);				
					}
					else if (!brochureUrl.matches(PlanMgmtConstants.URL_REGEX)) {
						brochureUrl = PlanMgmtConstants.HTTP_PROTOCOL + brochureUrl;				
					}
				}
				
				if (StringUtils.isNotBlank(exclusionsAndLimitations)) {
					
					if (exclusionsAndLimitations.matches(PlanMgmtConstants.ECM_REGEX)) {
						exclusionsAndLimitations = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(exclusionsAndLimitations);				
					}
					else if (!exclusionsAndLimitations.matches(PlanMgmtConstants.URL_REGEX)) {
						exclusionsAndLimitations = PlanMgmtConstants.HTTP_PROTOCOL + exclusionsAndLimitations;				
					}
				}
				
				PlanRateBenefit planMap = new PlanRateBenefit();
				planMap.setId(Integer.parseInt(rsObjectArray[PlanMgmtConstants.ZERO].toString()));
				planMap.setName((String.valueOf(rsObjectArray[PlanMgmtConstants.TWO])));
				planMap.setIssuer((String.valueOf(rsObjectArray[PlanMgmtConstants.THREE])));
				planMap.setIssuerText((String.valueOf(rsObjectArray[PlanMgmtConstants.THREE])));
				planMap.setIssuerId(Integer.parseInt(rsObjectArray[PlanMgmtConstants.FOUR].toString()));
				planMap.setPlanBrochureUrl(brochureUrl);
								
				if(null != rsObjectArray[PlanMgmtConstants.SIX]) {
					logoURL = rsObjectArray[PlanMgmtConstants.SIX].toString();
				}
				planMap.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, null, ghixJasyptEncrytorUtil.encryptStringByJasypt(rsObjectArray[PlanMgmtConstants.FOUR].toString()), appUrl));
								
				planMap.setPolicyLength(rsObjectArray[PlanMgmtConstants.SEVEN] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.SEVEN])));
			    planMap.setOopMax(rsObjectArray[PlanMgmtConstants.EIGHT] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.EIGHT])));
				planMap.setCoinsurance(rsObjectArray[PlanMgmtConstants.NINE] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.NINE])));				
				planMap.setNetworkType(rsObjectArray[PlanMgmtConstants.ELEVEN] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.ELEVEN])));
				planMap.setPolicyLengthUnit(rsObjectArray[PlanMgmtConstants.TWELVE] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.TWELVE])));
				planMap.setPolicyLimit(rsObjectArray[PlanMgmtConstants.THIRTEEN] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.THIRTEEN])));
				if(rsObjectArray[PlanMgmtConstants.TWENTYONE] != null){
					planMap.setPolicyLimit(planMap.getPolicyLimit()+" "+String.valueOf(rsObjectArray[PlanMgmtConstants.TWENTYONE]));
				}
				planMap.setOutOfNetwkCoverage(rsObjectArray[PlanMgmtConstants.FOURTEEN] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.FOURTEEN])));
				planMap.setOutOfCountyCoverage(rsObjectArray[PlanMgmtConstants.FIFTEEN] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.FIFTEEN])));
				planMap.setSeparateDrugDeductible(rsObjectArray[PlanMgmtConstants.SIXTEEN] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.SIXTEEN])));
				planMap.setOopMaxAttr(rsObjectArray[PlanMgmtConstants.SEVENTEEN] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.SEVENTEEN])));
				planMap.setOopMaxFamily(rsObjectArray[PlanMgmtConstants.EIGHTEEN] == null ? StringUtils.EMPTY : (String.valueOf(rsObjectArray[PlanMgmtConstants.EIGHTEEN])));
				planMap.setExclusionsAndLimitations(exclusionsAndLimitations);
				
				// concatenating plan_health_ids to generate a single query
				planStmIdStr += (planStmIdStr.length() == 0) ? "'" + rsObjectArray[PlanMgmtConstants.ONE].toString() + "'" : ", '" + rsObjectArray[PlanMgmtConstants.ONE].toString() + "'";

				
				if(rsObjectArray[PlanMgmtConstants.NINE] != null && rsObjectArray[PlanMgmtConstants.TEN] != null && rsObjectArray[PlanMgmtConstants.SEVEN] != null && rsObjectArray[PlanMgmtConstants.EIGHT] != null){
					key = "name-" + String.valueOf(rsObjectArray[PlanMgmtConstants.NINTEEN]) + "|co-"+ String.valueOf(rsObjectArray[PlanMgmtConstants.NINE]) + "|de-" + rsObjectArray[PlanMgmtConstants.TEN] + "|du-"+ String.valueOf(rsObjectArray[PlanMgmtConstants.SEVEN])+ "|oop-" + String.valueOf(rsObjectArray[PlanMgmtConstants.EIGHT]);
					
					String monthlypremium = premiumListArr.get(key);
					//LOGGER.info("PermiumForKey: " + key +"|" + monthlypremium);
					if(monthlypremium != null){
						planMap.setPremium(Float.parseFloat(monthlypremium)) ;
						plan.add(planMap);
					}
				}
			}
			
			
			// fire single query to fetch plan benefits and plan costs
			if (planStmIdStr.length() > PlanMgmtConstants.ZERO) {
				Map<String, Map<String, Map<String, String>>> benefitData = null;
				Map<String, Map<String, Map<String, String>>> planCosts = null;
				
				benefitData = getStmPlanBenefits(planStmIdStr); // fetch stm plan benefits from db 
				planCosts = getStmPlanCosts(planStmIdStr);  // fetch stm plan cost from db 
				
				while (rsIterator2.hasNext()) {
					Object[] objArray = (Object[]) rsIterator2.next();					
					Integer planId = Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString());
					String planStmId = objArray[PlanMgmtConstants.ONE].toString();
					
					for (PlanRateBenefit planObj : plan) {						
						if (planId.equals(planObj.getId()) && benefitData.containsKey(planStmId)) {							
								planObj.setPlanBenefits(benefitData.get(planStmId));
								planObj.setPlanCosts(planCosts.get(planStmId));
						}
					}
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getListOfPlanIDAndPlanSTMID", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return plan;
	}

	@Transactional(readOnly = true)
	public Map<String, Map<String, Map<String, String>>> getStmPlanBenefits(String planStmIdStr) {
		// pull require health plan benefits only
		Map<String, Map<String, Map<String, String>>> benefitDataList = new HashMap<String, Map<String, Map<String, String>>>();
		EntityManager entityManager = null;
		
		try {
			StringBuilder buildquery = new StringBuilder();
			List<Integer> planIdList = new ArrayList<Integer>();
			
			buildquery.append("SELECT PLAN_STM_ID, NAME, NETWORK_T1_DISPLAY, NETWORK_T1_COPAY_VAL, NETWORK_T1_COPAY_ATTR, NETWORK_T1_COINSURANCE_VAL, NETWORK_T1_COINSURANCE_ATTR ");
			buildquery.append("FROM PLAN_STM_BENEFITS WHERE PLAN_STM_ID IN (:paramIds) ");
			buildquery.append("ORDER BY PLAN_STM_ID");
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("INPUT buildquery :: " + buildquery);
			}
			
			if ((planStmIdStr != null) && !planStmIdStr.equals(PlanMgmtConstants.EMPTY_STRING)) {
				for(String id :  planStmIdStr.trim().replace("'", "").split(",")){
					planIdList.add(Integer.parseInt(id.trim()));
				}
			}
			
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(buildquery.toString());
    		query.setParameter("paramIds", planIdList);
    		
			Iterator rsIterator = query.getResultList().iterator();
			Object[] benefitFileds  = null;
			Map<String, String> benefitAttrib = null;
			String planStmId = null;
			String benefitName = null;
			
			while (rsIterator.hasNext()) {
				benefitFileds = (Object[]) rsIterator.next();
				benefitAttrib = new HashMap<String, String>();
				
				planStmId = benefitFileds[PlanMgmtConstants.ZERO].toString();
				benefitName = (benefitFileds[PlanMgmtConstants.ONE] == null ? "" : benefitFileds[PlanMgmtConstants.ONE].toString());
				
				benefitAttrib.put("netWkT1Disp", (benefitFileds[PlanMgmtConstants.TWO] == null) ? "" : benefitFileds[PlanMgmtConstants.TWO].toString());
				benefitAttrib.put("tier1CopayVal", (benefitFileds[PlanMgmtConstants.THREE] == null) ? "" : benefitFileds[PlanMgmtConstants.THREE].toString());
				benefitAttrib.put("tier1CopayAttrib", (benefitFileds[PlanMgmtConstants.FOUR] == null) ? "" : benefitFileds[PlanMgmtConstants.FOUR].toString());
				benefitAttrib.put("tier1CoinsVal", (benefitFileds[PlanMgmtConstants.FIVE] == null) ? "" : benefitFileds[PlanMgmtConstants.FIVE].toString());
				benefitAttrib.put("tier1CoinsAttrib", (benefitFileds[PlanMgmtConstants.SIX] == null) ? "" : benefitFileds[PlanMgmtConstants.SIX].toString());
				benefitAttrib.put("netWkT1TileDisp", PlanMgmtConstants.EMPTY_STRING);
				
				if(benefitName.equalsIgnoreCase("OFFICE_VISIT")) { // set tile display either copay or coinsurance for OFFICE VISIT benefits
					if(!benefitAttrib.get("tier1CopayVal").equalsIgnoreCase(PlanMgmtConstants.EMPTY_STRING)){
						benefitAttrib.put("netWkT1TileDisp", PlanMgmtConstants.DOLLAR_SIGN + benefitAttrib.get("tier1CopayVal"));
					}
					if(!benefitAttrib.get("tier1CoinsVal").equalsIgnoreCase(PlanMgmtConstants.EMPTY_STRING)){
						benefitAttrib.put("netWkT1TileDisp", benefitAttrib.get("tier1CoinsVal") + PlanMgmtConstants.PERCENT_SIGN);
					}
				}
				
				if (benefitDataList.get(planStmId) == null) {
					benefitDataList.put(planStmId, new HashMap<String, Map<String, String>>());
				}
				
				benefitDataList.get(planStmId).put(benefitName, benefitAttrib);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getStmPlanBenefits", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return benefitDataList;
	}
	
	@Transactional(readOnly = true)
	public Map<String, Map<String, Map<String, String>>> getStmPlanCosts(String planStmIdStr){
		Map<String, Map<String, Map<String, String>>> costDataList = new HashMap<String, Map<String, Map<String, String>>>();
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			StringBuilder buildquery = new StringBuilder();
			List<Integer> planIdList = new ArrayList<Integer>();
			
			buildquery.append("SELECT PLAN_STM_ID, NAME, IN_NETWORK_IND, IN_NETWORK_FAMILY, FAMILY_DESCRIPTION, INDV_DESCRIPTION ");
			buildquery.append("FROM PLAN_STM_COST WHERE PLAN_STM_ID IN (:paramIds) ");		
			buildquery.append("ORDER BY PLAN_STM_ID");
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("INPUT buildquery :: " + buildquery);
			}
		
			if ((planStmIdStr != null) && !planStmIdStr.equals(PlanMgmtConstants.EMPTY_STRING)) {
				for(String id : planStmIdStr.trim().replace("'", "").split(",")){
					planIdList.add(Integer.parseInt(id.trim()));
    		}
    		}
    		
			Query query = entityManager.createNativeQuery(buildquery.toString());
			query.setParameter("paramIds", planIdList);
    		
			Iterator rsIterator = query.getResultList().iterator();
			Object[] costFields = null;
			Map<String, String> costAttrib = null;
			String planStmId = null;
			String costName = null;
			
			while (rsIterator.hasNext()) {
				costFields = (Object[]) rsIterator.next();
				costAttrib = new HashMap<String, String>();
				
				planStmId = costFields[PlanMgmtConstants.ZERO].toString();
				costName = (costFields[PlanMgmtConstants.ONE] == null ? "" : costFields[PlanMgmtConstants.ONE].toString());
				
				costAttrib.put("IN_NETWORK_IND", (costFields[PlanMgmtConstants.TWO] == null) ? "" : costFields[PlanMgmtConstants.TWO].toString());
				costAttrib.put("IN_NETWORK_FLY", (costFields[PlanMgmtConstants.THREE] == null) ? "" : costFields[PlanMgmtConstants.THREE].toString());
				/*	adding new fields in costMap after suggestion from planDisplay Team	*/
				costAttrib.put("FAMILY_DESCRIPTION", (costFields[PlanMgmtConstants.FOUR] == null) ? "" : costFields[PlanMgmtConstants.FOUR].toString());
				costAttrib.put("INDV_DESCRIPTION", (costFields[PlanMgmtConstants.FIVE] == null) ? "" : costFields[PlanMgmtConstants.FIVE].toString());

				if (costDataList.get(planStmId) == null) {
					costDataList.put(planStmId, new HashMap<String, Map<String, String>>());
				}
				
				costDataList.get(planStmId).put(costName, costAttrib);
			}
		}catch(Exception ex){
			LOGGER.error("Exception Occured in getStmPlanCosts_test", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return costDataList;
	}
	
	
	// Call UHC web service to get plan rate on run time on basis of census information and effective date. 
		private List<PlanRateBenefit> quoteUHCWebService(List<Map<String, String>> memberList, String effectiveDateStr, String tenantCode) {		
			List<PlanRateBenefit> uhcPlan  = new ArrayList<PlanRateBenefit>();
			String applicantAge = PlanMgmtConstants.EMPTY_STRING;
			String applicantGender = PlanMgmtConstants.EMPTY_STRING;
			String applicantZip = PlanMgmtConstants.EMPTY_STRING;
			Boolean applicantTobaccoUsage = false;
			String spouseAge = PlanMgmtConstants.STR_ZERO;
			String spouseGender = PlanMgmtConstants.EMPTY_STRING;
			Boolean spouseTobaccoUsage = false;
			ArrayOfChild arrayOfChild = new ArrayOfChild();

			String[] splitedEffectiveDate = effectiveDateStr.split(PlanMgmtConstants.SINGLE_DASH_STRING);
			String effectiveDateYear = splitedEffectiveDate[PlanMgmtConstants.ZERO];
			String effectiveDateMonth = splitedEffectiveDate[PlanMgmtConstants.ONE];
			String effectiveDateDay = splitedEffectiveDate[PlanMgmtConstants.TWO];
			StringBuilder query_Builder_Value = new StringBuilder();
			
			
			LOGGER.info("Preparing UHC SOAP Request...");
			//process household information
			
			for (Map<String, String> member : memberList) {
				if (member.get(RELATION) != null) {							
					if(member.get(RELATION).equalsIgnoreCase(SELF)){
						applicantAge = member.get(AGE);
						applicantGender = member.get(GENDER);
						applicantZip = member.get(ZIP);
						applicantTobaccoUsage = (member.get(TOBACCO).equalsIgnoreCase("y")) ? true : false  ;
					}
					if(member.get(RELATION).equalsIgnoreCase(SPOUSE)){
						spouseAge = member.get(AGE);
						spouseGender = member.get(GENDER);
						spouseTobaccoUsage = (member.get(TOBACCO).equalsIgnoreCase("y")) ? true : false  ;
					}				
					if (member.get(RELATION).equalsIgnoreCase(CHILD)) {
						Child childInfo = new Child();
						childInfo.setAge(Short.parseShort(member.get(AGE)));
						childInfo.setGender(member.get(GENDER).substring(0,1));
						childInfo.setTobacco((member.get(TOBACCO).equalsIgnoreCase("y")) ? true : false);
						arrayOfChild.getChild().add(childInfo);
					}
				}
			}
			com.getinsured.hix.planmgmt.phix.ws.uhc.ObjectFactory objectFactory = new com.getinsured.hix.planmgmt.phix.ws.uhc.ObjectFactory();
			GetQuote stmQuoteParameters = objectFactory.createGetQuote();
		
			
			// prepare request data	start 			
						
			WebQuoteRequest webQuoteRequest = new WebQuoteRequest();
			AuthorizationType authorizationType = new AuthorizationType();
			QuoteRequestType quoteRequestType = new QuoteRequestType();
			PrimaryInsuredType primaryInsuredType = new PrimaryInsuredType();
			SpouseInsuredType spouseInsuredType = new SpouseInsuredType();
			
					
			authorizationType.setApplicationID(UHCApplicationID);
			webQuoteRequest.setAuthorization(authorizationType);
			
			String stateCode = "";
			String county = "";
			List<ZipCode> zipcodesList = zipCodeRepository.findByZip(applicantZip);				
			if (!CollectionUtils.isEmpty(zipcodesList)) {			
				for (int i = 0; i < zipcodesList.size(); i++) {
					stateCode = zipcodesList.get(i).getState();	
					county = zipcodesList.get(i).getCounty();
				}
				LOGGER.debug(ZIP_CODE_LOG+ applicantZip + BELONGS_TO_LOG + stateCode);				
			} else {
				LOGGER.debug(ZIP_CODE_LOG+ applicantZip + " does not exists in zipcodes table ");
			}		
			
			quoteRequestType.setCoverage((short) 0);
			quoteRequestType.setState(stateCode);
			quoteRequestType.setZipCode(applicantZip);
			quoteRequestType.setCounty(county);
			
			// prepare applicant request
			primaryInsuredType.setAge(Short.parseShort(applicantAge));
			primaryInsuredType.setGender(applicantGender.substring(0,1));
			primaryInsuredType.setHealthClass("1");
			primaryInsuredType.setTobacco(applicantTobaccoUsage);
			quoteRequestType.setPrimaryInsured(primaryInsuredType);
			
			// if spouse quoted, prepare spouse request
			if(spouseAge != PlanMgmtConstants.STR_ZERO){				
				spouseInsuredType.setAge(Short.parseShort(spouseAge));
				spouseInsuredType.setGender(spouseGender.substring(0,1));
				spouseInsuredType.setHealthClass("1");
				spouseInsuredType.setTobacco(spouseTobaccoUsage);
				quoteRequestType.setSpouseInsured(spouseInsuredType);
			}
			
			// if child info exist in census data, quote for child
			if(arrayOfChild.getChild().size() > PlanMgmtConstants.ZERO){
				quoteRequestType.setChildren(arrayOfChild);
			}
			
			quoteRequestType.setPaymentMode("M");
			quoteRequestType.setEffectiveDate(effectiveDateMonth + "/" + effectiveDateDay + "/" + effectiveDateYear);
			
			webQuoteRequest.setQuoteRequest(quoteRequestType);
			
			stmQuoteParameters.setValue(webQuoteRequest);
			
			// prepare request data	end 	
			
			LOGGER.info("Request prepartion done.");
			
			GetQuoteResponse response = null;		
			
			try{
				// set WSDL url	
				webServiceTemplate.setDefaultUri(uhcWsdlUrl);
				
				LOGGER.info("Calling UHC WSDL... Submiting request...");
				// send request
				response =  (GetQuoteResponse) webServiceTemplate.marshalSendAndReceive(stmQuoteParameters, new SoapActionCallback(uhcSoapActionUrl));
				
				if(response != null){
					List<Plan> planList =  null;	
					Map<String, String> planPremiumArr = new HashMap<String, String>();
					LOGGER.info("Get response from UHC web service. Parsing SOAP response...");
					// parse SOAP response
					LOGGER.debug( " ## plans found = " + response.getGetQuoteResult().getPlans().getPlan().size());
					planList = response.getGetQuoteResult().getPlans().getPlan();
					
					
					// TBD use Co-insurance, Deductible, duration, OutOfPocket to pull plan data from plan and plan_stm tables 
					for(Plan planInfo : planList){						
						// consider only STM products
						if(planInfo.getProductType().getValue().equalsIgnoreCase("SHORT TERM")){
							ArrayOfPlanRate arrayOfPlanRate = planInfo.getPlanRates();
						
							for(PlanRate rates : arrayOfPlanRate.getPlanRate()){								
								
								if(rates.getPayment().equalsIgnoreCase("Monthly")){
									String planName = planInfo.getName();
									String coinsurance = rates.getCustomerCoinsurance().substring(rates.getCustomerCoinsurance().indexOf("/")+1);
									long deductible =  rates.getDeductible();
									//String coverageDaysStart = rates.getCoverageDaysStart();
									int duration = 0;
									if (rates.getCoverageDaysEnd() != null){
									   duration = Integer.parseInt(rates.getCoverageDaysEnd());
									}
									//short duration = rates.getMonths();
								/*	if(null != coverageDaysStart &&  coverageDaysStart.equalsIgnoreCase(PlanMgmtConstants.THIRTY_DAYS)) {
										duration = 6;
									} else if(null != coverageDaysStart &&  coverageDaysStart.equalsIgnoreCase(PlanMgmtConstants.ONE_EIGHTY_FIVE_DAYS)) {
										duration = 11;
									}*/
									
									String oopMax = rates.getOutOfPocketCoinsuranceMax().substring(0, rates.getOutOfPocketCoinsuranceMax().indexOf("."));
									BigDecimal premium = rates.getRate();
									
									if(null != premium && planInfo.isDailyRated()) {
										premium = premium.multiply(new BigDecimal(30));	
									}
									//if quoting state does not belong to non FACT FEE state,  Add $4 as FACT fees with premium
									if(!UHC_NON_FACT_FEES_STATE.contains(stateCode) && premium != null){																		
										premium = premium.add(FACT_FEES);																	
									}
									
									
									String key = "name-" + planName + "|co-"+ coinsurance + "|de-" + deductible + "|du-"+ duration + "|oop-" + oopMax; 
									
									planPremiumArr.put(key, String.valueOf(premium));
									if(null != premium && StringUtils.isNotBlank(planName) && StringUtils.isNotBlank(coinsurance) 
											&& duration!=0 && StringUtils.isNotBlank(oopMax)) {
									
										StringBuilder strValueBuilder = new StringBuilder();
										strValueBuilder.append(PlanMgmtConstants.OPENING_BRACKET + PlanMgmtConstants.SINGLE_QUOTE + planName + PlanMgmtConstants.SINGLE_QUOTE + PlanMgmtConstants.COMMA + coinsurance + PlanMgmtConstants.COMMA + deductible + PlanMgmtConstants.COMMA + duration + PlanMgmtConstants.COMMA + oopMax + PlanMgmtConstants.CLOSING_BRACKET);
									
										if(query_Builder_Value.toString().isEmpty()){
											query_Builder_Value.append(strValueBuilder);
										}else{
											query_Builder_Value.append(PlanMgmtConstants.COMMA + strValueBuilder);
										}
									}else {
										LOGGER.info("Skipped querying for UHC STM plan as one of the value received is null");
										LOGGER.info("STM plan values received from UHC, premium:" + premium + " planName:" + planName +
												" Coinsurance:" + coinsurance + " Duration:" + duration + " OOPMax:" + oopMax);
									}
								}
							}
							
						}
					}
					if(query_Builder_Value.length() > PlanMgmtConstants.ZERO){
						uhcPlan = getListOfPlanIDAndPlanSTMID(query_Builder_Value, planPremiumArr, stateCode, UHC_HIOS_ISSUER_ID,tenantCode);
					}
				}
				else{
					LOGGER.info("Response not found from UHC web service");
				}	
				
			}catch(WebServiceIOException wsIOE){
				LOGGER.error(wsIOE);
			}catch(SoapFaultClientException sfce){
				LOGGER.error(sfce.getMessage() + PlanMgmtConstants.TRIPLE_DASH_STRING + sfce.toString());
			}catch(Exception e) {
				LOGGER.error(e);
			}
			
			return uhcPlan;		
		}
//		Below code is commented out as part of HIX-71154		
	/*	private List<PlanRateBenefit> quoteAssurantWebService(List<Map<String, String>> memberList, String effectiveDateStr) throws DatatypeConfigurationException {
			List<PlanRateBenefit> assurantPlan  = new ArrayList<PlanRateBenefit>();
			com.getinsured.hix.planmgmt.phix.ws.assurant.ObjectFactory objectFactory = new com.getinsured.hix.planmgmt.phix.ws.assurant.ObjectFactory();
			String intVal = null;
			
			String applicantDOB = PlanMgmtConstants.EMPTY_STRING;
			String applicantGender = PlanMgmtConstants.EMPTY_STRING;
			String applicantZip = PlanMgmtConstants.EMPTY_STRING;			
			String spouseDOB = PlanMgmtConstants.STR_ZERO;
			String spouseGender = PlanMgmtConstants.EMPTY_STRING;			
			ArrayOfChild arrayOfChild = new ArrayOfChild();
			StringBuilder query_Builder_Value = new StringBuilder();
			Integer intCoinsurance = 0;
			int numberOfChildren = 0;
			
			try{
				for (Map<String, String> member : memberList) {
					if (member.get(RELATION) != null) {							
						if(member.get(RELATION).equalsIgnoreCase(SELF)){
							applicantDOB = member.get(DATE_OF_BIRTH);
							applicantGender = member.get(GENDER);
							applicantZip = member.get(ZIP);
							//applicantTobaccoUsage = (member.get(TOBACCO).equalsIgnoreCase("y")) ? true : false  ;
						}
						if(member.get(RELATION).equalsIgnoreCase(SPOUSE)){
							spouseDOB = member.get(DATE_OF_BIRTH);
							spouseGender = member.get(GENDER);
							//spouseTobaccoUsage = (member.get(TOBACCO).equalsIgnoreCase("y")) ? true : false  ;
						}				
						if (member.get(RELATION).equalsIgnoreCase(CHILD)) {
							Child childInfo = new Child();
							childInfo.setAge(Short.parseShort(member.get(AGE)));
							childInfo.setGender(member.get(GENDER).substring(0,1));
							childInfo.setTobacco((member.get(TOBACCO).equalsIgnoreCase("y")) ? true : false);
							arrayOfChild.getChild().add(childInfo);
							numberOfChildren++;
						}
					}
				}
				
				String[] effectiveDate = effectiveDateStr.split(DASH);
				
				GetPlans stmQuoteParameters = objectFactory.createGetPlans();
				GetPlansRequest getPlansRequest = new GetPlansRequest();
				Demographics demographics =  new Demographics();
				
				DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();

				XMLGregorianCalendar effDateXMLGregorianCalendar = null;
				GregorianCalendar gc = new GregorianCalendar();
				gc.set(Integer.parseInt(effectiveDate[PlanMgmtConstants.ZERO]), Integer.parseInt(effectiveDate[PlanMgmtConstants.ONE]), Integer.parseInt(effectiveDate[PlanMgmtConstants.TWO]));
				//gc.set(2014, 10, 10);
				effDateXMLGregorianCalendar = datatypeFactory.newXMLGregorianCalendar(gc);
				demographics.setEffectiveDate(effDateXMLGregorianCalendar);
				
				getPlansRequest.setUserName(DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.ASSURANT_I_MQUOTING_TEST));
				getPlansRequest.setPassword(DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.ASSURANT_PASSWORD));
				getPlansRequest.setChannel(TIC_CHANNEL);
				getPlansRequest.setSelectedPlan(ALL);
				
				//applicantZip = "30321";
				demographics.setAgentNumber(DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.ASSURANT_AGENT_NUMBER));
				demographics.setZipCode(applicantZip);
				demographics.setPaymentMode(PAYMENT_MODE);
				demographics.setNumberOfDays(PlanMgmtConstants.ZERO);
			
				demographics.setPrimaryBirthDate(getDOBFortted(applicantDOB));				
				demographics.setPrimaryGender(applicantGender);
				
				if(!(null != spouseDOB) && !(spouseGender.isEmpty())){
					demographics.setSpouseBirthDate(getDOBFortted(spouseDOB));			
					demographics.setSpouseGender(spouseGender);
				}
				demographics.setNumberOfChildren(numberOfChildren);

				String stateCode = "";
				String county = "";
				List<ZipCode> zipcodesList = zipCodeRepository.findByZip(applicantZip);				
				if (!CollectionUtils.isEmpty(zipcodesList)) {			
					for (int i = 0; i < zipcodesList.size(); i++) {
						stateCode = zipcodesList.get(i).getState();	
						county = zipcodesList.get(i).getCounty();
						LOGGER.debug(ZIP_CODE_LOG+ applicantZip + BELONGS_TO_LOG + stateCode +" with county"+ county);
					}
					LOGGER.debug(ZIP_CODE_LOG+ applicantZip + BELONGS_TO_LOG + stateCode);				
				} else {
					LOGGER.debug(ZIP_CODE_LOG+ applicantZip + " does not exists in zipcodes table ");
				}
				
				getPlansRequest.setDemographicsInfo(demographics);
				
				stmQuoteParameters.setRequest(getPlansRequest);
				
				GetPlansResponse response = null;
				
				LOGGER.info("Calling ASSURANT WSDL... Submiting request...");
				webServiceTemplate.setDefaultUri(assurantWsdlUrl);
				// send request
				response =  (GetPlansResponse) webServiceTemplate.marshalSendAndReceive(stmQuoteParameters, new SoapActionCallback(assurantSoapActionUrl));
				
				if(response != null){
					Map<String, String> planPremiumArr = new HashMap<String, String>();
					LOGGER.debug( " ========== # of plan "+ response.getGetPlansResult().getRatePlans().getPlan().size());
					List<com.getinsured.hix.planmgmt.phix.ws.assurant.Plan> planList =  new ArrayList<com.getinsured.hix.planmgmt.phix.ws.assurant.Plan>();	
					planList = response.getGetPlansResult().getRatePlans().getPlan();
					for(com.getinsured.hix.planmgmt.phix.ws.assurant.Plan plan : planList){
						
						LOGGER.debug( "==Plan Name==> "+ plan.getPlanID() + " >>" + plan.getPlanName() + " >> "+ plan.getMonthlyPremium());
						
						String planName = plan.getPlanName();
						String coinsurance = getCoinsuranceValue(plan.getRateOfPayment());
						long deductible =  plan.getDeductible();
						short duration = Short.parseShort(plan.getPolicyDuration());
						Double oopMax = plan.getIndividualOOP() + deductible; // adding individual oop + deductible to match oop max value 
						Double premium = plan.getMonthlyPremium();
						
						LOGGER.debug("==planRate ==>: Co-insurance == " + coinsurance + "  >> Deductible == " + deductible +
								" >> duration == " + duration + "  >> OutOfPocket == " + oopMax + " >> Monthly premium == " + premium);

						if(oopMax.intValue() > PlanMgmtConstants.ZERO){
							intVal = String.valueOf(oopMax.intValue());
						}else{
							intVal = PlanMgmtConstants.STR_ZERO+PlanMgmtConstants.STR_ZERO;
						}
						
						if(coinsurance != null){
							intCoinsurance = Integer.parseInt(coinsurance);
						}
						
						
						String key = "name-" + planName + "|co-"+ intCoinsurance.toString() + "|de-" + deductible + "|du-"+ duration + "|oop-" + intVal; 
						planPremiumArr.put(key, String.valueOf(premium));

						StringBuilder strValueBuilder = new StringBuilder();
						strValueBuilder.append(PlanMgmtConstants.OPENING_BRACKET + PlanMgmtConstants.SINGLE_QUOTE + planName + PlanMgmtConstants.SINGLE_QUOTE + PlanMgmtConstants.COMMA + intCoinsurance + PlanMgmtConstants.COMMA + deductible + PlanMgmtConstants.COMMA + duration + PlanMgmtConstants.COMMA + intVal + PlanMgmtConstants.CLOSING_BRACKET);
						
						if(query_Builder_Value.toString().isEmpty()){
							query_Builder_Value.append(strValueBuilder);
						}else{
							query_Builder_Value.append(PlanMgmtConstants.COMMA + strValueBuilder);
						}
					}
					
					LOGGER.debug("Assurant Query Builder" + query_Builder_Value);
					if(query_Builder_Value.length() > PlanMgmtConstants.ZERO){
						LOGGER.debug("Assurant_planPremiumArr : " + planPremiumArr);
						assurantPlan = getListOfPlanIDAndPlanSTMID(query_Builder_Value, planPremiumArr, stateCode, ASSURANT_HIOS_ISSUER_ID);
					}
				}
			}catch(Exception ex){
				LOGGER.error("Exception Occured in quoteAssurantWebService", ex);
			}
			
			return assurantPlan;		
		}*/
		
		public static String getDate(Calendar cal){
	        return "" + cal.get(Calendar.YEAR) +DASH+ (cal.get(Calendar.MONTH)+1)+DASH+cal.get(Calendar.DATE);
	    }
		
//		Below code is commented out as part of HIX-71154
		/*
		private String getCoinsuranceValue(String coinsurance){
			String subString = null;
			if(!(coinsurance.isEmpty())){
				int coninsuranceLength = coinsurance.length();
				subString = coinsurance.substring((coninsuranceLength - 2), coninsuranceLength);
			}
			if(subString != null){
				return subString;
			}else{
				return PlanMgmtConstants.EMPTY_STRING;
			}
		}*/
		
		@Transactional(readOnly = true)
		private List<PlanRateBenefit> quoteHCCWebService(List<Map<String, String>> memberList, String effectiveDate, String tenantCode) throws DatatypeConfigurationException {
			List<PlanRateBenefit> hccPlan  = new ArrayList<PlanRateBenefit>();
			ArrayList<String> paramList = new ArrayList<String>();
			int paramCount = 0; 
			EntityManager entityManager = null;
			
			try{
				entityManager = emf.createEntityManager();
				StringBuilder queryBuilder = new StringBuilder(2048);
				String stateCode = quotingBusinessLogic.getApplicantStateCode(memberList);			
				String applicantZip = quotingBusinessLogic.getApplicantZipCode(memberList);
				
				queryBuilder.append("SELECT p.id AS plan_id, ps.id as PLAN_STM_ID, p.name AS plan_name, i.name AS ISSUE_NAME, p.ISSUER_ID, p.BROCHURE, i.LOGO_URL, ");
				queryBuilder.append("ps.MAX_DURATION, ps.OOP_MAX_VAL, ps.COINSURANCE, p.NETWORK_TYPE, ps.DURATION_UNIT, ps.POLICY_MAX, ps.OUT_OF_NETWORK_COVERAGE, ");
				queryBuilder.append("ps.OUT_OF_COUNTRY_COVERAGE, psc.SEPARATE_DEDT_PRESCPT_DRUG AS SEPARATE_DRUG_DEDUCTIBLE, ps.OOP_MAX_ATTR, ps.OOP_MAX_FAMILY, SUM(pre) AS premium, SUM(pre) + COALESCE(saf.admin_elec_fee,0) + COALESCE(saf.assoc_fee, 0) AS PREMIUM_WITH_FEES, ps.POLICY_MAX_ATTR ");
				queryBuilder.append("FROM ");
				queryBuilder.append("plan p left join pm_stm_hcc_admin_fees saf on p.STATE = saf.STATE and saf.is_deleted = 'N', issuers i, plan_stm ps, plan_stm_cost psc, pm_tenant_plan tp, tenant t, ( ");
				
				int i = 0;
				for (Map<String, String> member : memberList) {					
					if (member.get(RELATION) != null) {	
									
						queryBuilder.append("( SELECT p.id, ");
					    // if member is applicant/spouse's gender is male, then compute premium as premium = male rate * area factorial value
						if(member.get(GENDER).equalsIgnoreCase("Male") && !member.get(RELATION).equalsIgnoreCase(CHILD)){
							queryBuilder.append(" hccrate.male_rate, hccareafact.area_factor_val, round(CAST(hccrate.male_rate * hccareafact.area_factor_val AS NUMERIC), 2) AS pre ");
						}
						// if member is applicant/spouse's gender is female, then compute premium as premium = female rate * area factorial value
						if(member.get(GENDER).equalsIgnoreCase("Female") && !member.get(RELATION).equalsIgnoreCase(CHILD)){
							queryBuilder.append(" hccrate.female_rate, hccareafact.area_factor_val, round(CAST(hccrate.female_rate * hccareafact.area_factor_val AS NUMERIC), 2) AS pre ");
						}
						
						// if member is child and AGE is less than 20 years then compute premium as premium = child rate * area factorial value 
						// otherwise compute premium as premium = male rate/female rate * area factorial value
			 	 	 	if(member.get(RELATION).equalsIgnoreCase(CHILD)){
			 	 	 		if (Integer.parseInt(member.get(AGE)) < PlanMgmtConstants.TWENTY ){			 	 	 	
			 	 	 			queryBuilder.append(" hccrate.child_rate, hccareafact.area_factor_val, round(CAST(hccrate.child_rate * hccareafact.area_factor_val AS NUMERIC), 2) AS pre ");
			 	 	 		}else if(member.get(GENDER).equalsIgnoreCase("Male")){
			 	 	 			// if child's gender is male, then compute premium as premium = male rate * area factorial value
			 	 	 			queryBuilder.append(" hccrate.male_rate, hccareafact.area_factor_val, round(CAST(hccrate.male_rate * hccareafact.area_factor_val AS NUMERIC), 2) AS pre ");
			 	 	 		}	
			 	 	 		else if(member.get(GENDER).equalsIgnoreCase("Female") ){
			 	 	 				// if child's gender is female, then compute premium as premium = female rate * area factorial value
			 	 	 				queryBuilder.append(" hccrate.female_rate, hccareafact.area_factor_val, round(CAST(hccrate.female_rate * hccareafact.area_factor_val AS NUMERIC), 2) AS pre ");
							}			 	 	 		
			 	 	 	}
						
			 	 	 	
						queryBuilder.append(" FROM ");
						queryBuilder.append(" plan p, pm_stm_hcc_rate hccrate, pm_stm_hcc_area_factor hccareafact, issuers i2 ");
						queryBuilder.append(" WHERE "); 
						queryBuilder.append(" hccrate.plan_id= p.id ");
						queryBuilder.append(" AND p.is_deleted='N' ");
						queryBuilder.append(" AND p.INSURANCE_TYPE = 'STM' ");
						queryBuilder.append(" AND hccrate.is_deleted='N' ");
						queryBuilder.append(" AND p.state= ");
						queryBuilder.append(" :param_" + paramCount);
						paramList.add(stateCode);
						paramCount++;						
						queryBuilder.append(" AND ");
						
						queryBuilder.append(" CAST(:param_" + paramCount);
						paramList.add(member.get(AGE));
						paramCount++;
						
						queryBuilder.append(" AS NUMERIC) >= hccrate.min_age AND ");
						queryBuilder.append(" CAST (:param_" + paramCount);
						paramList.add(member.get(AGE));
						paramCount++;
						
						queryBuilder.append(" AS NUMERIC) <= hccrate.max_age ");
						queryBuilder.append(" AND TO_DATE (:param_" + paramCount + " , 'YYYY-MM-DD') BETWEEN hccrate.start_date AND hccrate.end_date ");
						paramList.add(effectiveDate);
						paramCount++;
						
						queryBuilder.append(" AND hccareafact.state=p.state ");
						queryBuilder.append(" AND ( ( SUBSTR(");
						queryBuilder.append(" :param_" + paramCount);
						paramList.add(applicantZip);
						paramCount++;
						
						queryBuilder.append(",0,3) >=hccareafact.min_zip AND SUBSTR(");
						queryBuilder.append(" :param_" + paramCount);
						paramList.add(applicantZip);
						paramCount++;
						
						queryBuilder.append(",0,3)<= hccareafact.max_zip and hccareafact.support_full_state='N') ");
						queryBuilder.append(" OR ( hccareafact.min_zip IS NULL and hccareafact.support_full_state='Y') ) ");
						queryBuilder.append(" AND p.ISSUER_ID = i2.ID ");
						queryBuilder.append(" AND hccareafact.carrier_hios_id = i2.hios_issuer_id ");
						queryBuilder.append(" AND TO_DATE (:param_" + paramCount + ", 'YYYY-MM-DD') BETWEEN hccareafact.effective_start_date AND hccareafact.effective_end_date ");
						paramList.add(effectiveDate);
						paramCount++;
						queryBuilder.append(" AND hccareafact.is_deleted='N' ");
						queryBuilder.append(") ");
						
						if (i + 1 != memberList.size()) {
							queryBuilder.append(" UNION ALL ");
						}
						 i++;						
					}					 
				}						
				
				queryBuilder.append(") temp WHERE p.id = temp.id ");
				queryBuilder.append("AND i.ID = p.ISSUER_ID AND p.ID = ps.PLAN_ID AND ps.id = psc.plan_stm_id ");
				queryBuilder.append(quotingBusinessLogic.buildPlanCertificationLogic("p", true));
				queryBuilder.append(" AND p.id = tp.plan_id");
				queryBuilder.append(" AND tp.tenant_id = t.id");
				queryBuilder.append(" AND t.code = UPPER(:param_" + paramCount +") ");
				paramCount++;
				paramList.add(tenantCode);
				queryBuilder.append("GROUP BY  p.id, ps.id, p.name, i.name, p.ISSUER_ID, p.BROCHURE, i.LOGO_URL, ps.MAX_DURATION, ps.OOP_MAX_VAL, ps.COINSURANCE, p.NETWORK_TYPE, ps.DURATION_UNIT, ");
				queryBuilder.append("ps.POLICY_MAX, ps.OUT_OF_NETWORK_COVERAGE, ps.OUT_OF_COUNTRY_COVERAGE, psc.SEPARATE_DEDT_PRESCPT_DRUG , ps.OOP_MAX_ATTR, ps.OOP_MAX_FAMILY, saf.ADMIN_ELEC_FEE, saf.assoc_fee, ps.POLICY_MAX_ATTR");
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("HCC SQL========"+ queryBuilder.toString());
				}
				String planStmIdStr = PlanMgmtConstants.EMPTY_STRING;
				
				
				
				Query query = entityManager.createNativeQuery(queryBuilder.toString());
				for (int x = 0; x < paramCount; x++) {
					query.setParameter("param_" + x, paramList.get(x));
				}
								
				List<?> rsList = query.getResultList();
				Iterator rsIterator = rsList.iterator();					
				Iterator rsIterator2 = rsList.iterator();
				
				while (rsIterator.hasNext()) {
					String logoURL = null;
					Object[] rsObjectArray = (Object[]) rsIterator.next();
					String brochureUrl = rsObjectArray[PlanMgmtConstants.FIVE] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.FIVE]));
					if (StringUtils.isNotBlank(brochureUrl)) {						
						if (brochureUrl.matches(PlanMgmtConstants.ECM_REGEX)) {
							brochureUrl = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(brochureUrl);				
						}
						else if (!brochureUrl.matches(PlanMgmtConstants.URL_REGEX)) {
							brochureUrl = PlanMgmtConstants.HTTP_PROTOCOL + brochureUrl;				
						}
					}
					PlanRateBenefit planMap = new PlanRateBenefit();
					planMap.setId(Integer.parseInt(rsObjectArray[PlanMgmtConstants.ZERO].toString()));
					planMap.setName((String.valueOf(rsObjectArray[PlanMgmtConstants.TWO])));
					planMap.setIssuer((String.valueOf(rsObjectArray[PlanMgmtConstants.THREE])));
					planMap.setIssuerText((String.valueOf(rsObjectArray[PlanMgmtConstants.THREE])));
					planMap.setIssuerId(Integer.parseInt(rsObjectArray[PlanMgmtConstants.FOUR].toString()));
					planMap.setPlanBrochureUrl(brochureUrl);
					if(null != rsObjectArray[PlanMgmtConstants.SIX]) {
						logoURL = rsObjectArray[PlanMgmtConstants.SIX].toString();
					}
					planMap.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, null, ghixJasyptEncrytorUtil.encryptStringByJasypt(rsObjectArray[PlanMgmtConstants.FOUR].toString()), appUrl));
					
					planMap.setPolicyLength(rsObjectArray[PlanMgmtConstants.SEVEN] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.SEVEN])));
				    planMap.setOopMax(rsObjectArray[PlanMgmtConstants.EIGHT] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.EIGHT])));
					planMap.setCoinsurance(rsObjectArray[PlanMgmtConstants.NINE] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.NINE])));				
					planMap.setNetworkType(rsObjectArray[PlanMgmtConstants.TEN] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.TEN])));
					planMap.setPolicyLengthUnit(rsObjectArray[PlanMgmtConstants.ELEVEN] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.ELEVEN])));
					planMap.setPolicyLimit(rsObjectArray[PlanMgmtConstants.TWELVE] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.TWELVE])));
					if(rsObjectArray[PlanMgmtConstants.TWENTY] != null){
						planMap.setPolicyLimit(planMap.getPolicyLimit()+" "+String.valueOf(rsObjectArray[PlanMgmtConstants.TWENTY]));
					}
					planMap.setOutOfNetwkCoverage(rsObjectArray[PlanMgmtConstants.THIRTEEN] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.THIRTEEN])));
					planMap.setOutOfCountyCoverage(rsObjectArray[PlanMgmtConstants.FOURTEEN] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.FOURTEEN])));
					planMap.setSeparateDrugDeductible(rsObjectArray[PlanMgmtConstants.FIFTEEN] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.FIFTEEN])));
					planMap.setOopMaxAttr(rsObjectArray[PlanMgmtConstants.SIXTEEN] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.SIXTEEN])));
					planMap.setOopMaxFamily(rsObjectArray[PlanMgmtConstants.SEVENTEEN] == null ? "" : (String.valueOf(rsObjectArray[PlanMgmtConstants.SEVENTEEN])));
					planMap.setPremium(rsObjectArray[PlanMgmtConstants.NINTEEN] == null ? 0 : Float.parseFloat(rsObjectArray[PlanMgmtConstants.NINTEEN].toString()));
					
					hccPlan.add(planMap);
					// concatenating plan_health_ids to generate a single query
					planStmIdStr += (planStmIdStr.length() == 0) ? "'" + rsObjectArray[PlanMgmtConstants.ONE].toString() + "'" : ", '" + rsObjectArray[PlanMgmtConstants.ONE].toString() + "'";

					
				}
				
				// fire single query to fetch plan benefits and plan costs
				if (planStmIdStr.length() > PlanMgmtConstants.ZERO) {
					Map<String, Map<String, Map<String, String>>> benefitData = null;
					Map<String, Map<String, Map<String, String>>> planCosts = null;
					
					benefitData = getStmPlanBenefits(planStmIdStr); // fetch stm plan benefits from db 
					planCosts = getStmPlanCosts(planStmIdStr);  // fetch stm plan cost from db 
					
					while (rsIterator2.hasNext()) {
						Object[] objArray = (Object[]) rsIterator2.next();					
						Integer planId = Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString());
						String planStmId = objArray[PlanMgmtConstants.ONE].toString();
						
						for (PlanRateBenefit planObj : hccPlan) {						
							if (planId.equals(planObj.getId()) && benefitData.containsKey(planStmId)) {							
									planObj.setPlanBenefits(benefitData.get(planStmId));
									planObj.setPlanCosts(planCosts.get(planStmId));
							}
						}
					}
				}
			
			}catch(Exception ex){
				LOGGER.error("Exception Occured in quoteHCCWebService", ex);
			}finally{
				 // close the entity manager
				 if(entityManager !=null && entityManager.isOpen()){
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
			}
			
			return hccPlan;		
		}
		
		private BundleGroupQuoteRequest processIHCQuoteRequest(List<Map<String, String>> memberList, String effectiveDateStr, com.getinsured.hix.planmgmt.phix.ws.ihc.ObjectFactory objectFactory) {
			BundleGroupQuoteRequest bundleGrpQuotReq = null;			
			String applicantDOB = PlanMgmtConstants.EMPTY_STRING;
			String applicantGender = PlanMgmtConstants.EMPTY_STRING;
			String applicantZip = PlanMgmtConstants.EMPTY_STRING;
			String spouseDOB = PlanMgmtConstants.STR_ZERO;
			String spouseGender = PlanMgmtConstants.EMPTY_STRING;
			String dependentDOB = PlanMgmtConstants.EMPTY_STRING;
			String dependentGender = PlanMgmtConstants.EMPTY_STRING;
			String applicantCountyFips = PlanMgmtConstants.EMPTY_STRING;
			String applicantTobaccoUsage = PlanMgmtConstants.EMPTY_STRING;
			String spouseTobaccoUsage = PlanMgmtConstants.EMPTY_STRING;
			String dependentsTobaccoUsage = PlanMgmtConstants.EMPTY_STRING;
			String[] splitedEffectiveDate = effectiveDateStr.split(PlanMgmtConstants.SINGLE_DASH_STRING);//"-"
					
			ArrayOfParticipant participantList = new ArrayOfParticipant();
			Participant dependentParticipant = null;			
			JAXBElement<Boolean> tobaccoUsage = objectFactory.createBoolean(true);
			JAXBElement<Boolean> noTobaccoUsage = objectFactory.createBoolean(false);
			
			LOGGER.info("Preparing IHC SOAP Request...");
			try {
				DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
				bundleGrpQuotReq = new BundleGroupQuoteRequest();
				//process household information
				for (Map<String, String> member : memberList) {
					if (member.get(RELATION) != null) {							
						if(member.get(RELATION).equalsIgnoreCase(SELF)){
							applicantDOB = member.get(DATE_OF_BIRTH);
							applicantGender = member.get(GENDER);
							applicantZip = member.get(ZIP);		
							applicantCountyFips = member.get(COUNTY_FIPS);
							applicantTobaccoUsage = member.get(TOBACCO);
							
						}
						if(member.get(RELATION).equalsIgnoreCase(SPOUSE)){
							spouseDOB = member.get(DATE_OF_BIRTH);
							spouseGender = member.get(GENDER);
							spouseTobaccoUsage = member.get(TOBACCO);
						}				
						if (member.get(RELATION).equalsIgnoreCase(CHILD)) {
							dependentParticipant = new Participant();
							dependentGender = member.get(GENDER);				
							dependentDOB = member.get(DATE_OF_BIRTH);
							dependentsTobaccoUsage = member.get(TOBACCO);						
							dependentParticipant.setDateOfBirth(getDOBFortted(dependentDOB));
							if(dependentGender.equalsIgnoreCase(PlanMgmtConstants.MALE)) {
								dependentParticipant.setGender(Gender.MALE);
							} else if(dependentGender.equalsIgnoreCase(PlanMgmtConstants.FEMALE)) {
								dependentParticipant.setGender(Gender.FEMALE);			
							}
							if(dependentsTobaccoUsage.equalsIgnoreCase(PlanMgmtConstants.Y)) {
								dependentParticipant.setIsTobaccoUser(tobaccoUsage);
							} else {
								dependentParticipant.setIsTobaccoUser(noTobaccoUsage);
							}						
							participantList.getParticipant().add(dependentParticipant);
						}
						
					}
				}
				
				// prepare request data	start 					
				LOGGER.info("Request prepartion done.");							
		
				/* -------- set applicant's information start ----------- */
				Participant participant = new Participant();				
				participant.setDateOfBirth(getDOBFortted(applicantDOB));
				if(applicantTobaccoUsage.equalsIgnoreCase(PlanMgmtConstants.Y)) {
					participant.setIsTobaccoUser(tobaccoUsage);
				} else {
					participant.setIsTobaccoUser(noTobaccoUsage);
				}
				if(applicantGender.equalsIgnoreCase(PlanMgmtConstants.MALE)) {
					participant.setGender(Gender.MALE);
				} else if(applicantGender.equalsIgnoreCase(PlanMgmtConstants.FEMALE)) {
					participant.setGender(Gender.FEMALE);			}
				JAXBElement<Participant> applicant = objectFactory.createDemographicsApplicant(participant);
				bundleGrpQuotReq.setApplicant(applicant);	
				/* -------- set applicant's information end ----------- */

				/* -------- set county fips information start ----------- */
				JAXBElement<Integer> countyFips = objectFactory.createDemographicsCountyFips(Integer.parseInt(applicantCountyFips));
				bundleGrpQuotReq.setCountyFips(countyFips);
				/* -------- set county fips information end ----------- */
				
				/* -------- set dependent's information start ----------- */
				if(StringUtils.isNotBlank(dependentDOB) && StringUtils.isNotBlank(dependentGender)){	
					JAXBElement<ArrayOfParticipant> dependentList = objectFactory.createDemographicsDependents(participantList);			
					bundleGrpQuotReq.setDependents(dependentList);	
				}
				/* -------- set dependent's information end ----------- */
				
				/* -------- set postal code information start ----------- */
				JAXBElement<String> postalCode = objectFactory.createDemographicsPostalCode(applicantZip);
				bundleGrpQuotReq.setPostalCode(postalCode);
				/* -------- set postal code information end ----------- */
				
				/* -------- set spouse's information start ----------- */
				if(StringUtils.isNotBlank(spouseDOB) && StringUtils.isNotBlank(spouseGender)){
					Participant spouseParticipant = new Participant();				
					spouseParticipant.setDateOfBirth(getDOBFortted(spouseDOB));
					if(spouseTobaccoUsage.equalsIgnoreCase(PlanMgmtConstants.Y)) {
						spouseParticipant.setIsTobaccoUser(tobaccoUsage);
					} else {
						spouseParticipant.setIsTobaccoUser(noTobaccoUsage);
					}
					if(spouseGender.equalsIgnoreCase(PlanMgmtConstants.MALE)) {
						spouseParticipant.setGender(Gender.MALE);
					} else if(spouseGender.equalsIgnoreCase(PlanMgmtConstants.FEMALE)) {
						spouseParticipant.setGender(Gender.FEMALE);			}
					JAXBElement<Participant> spouse = objectFactory.createDemographicsSpouse(spouseParticipant);
					bundleGrpQuotReq.setSpouse(spouse);
				}				
				/* -------- set spouse's information end ----------- */
				
				
				JAXBElement<String> agentNumber = objectFactory.createBundleGroupQuoteRequestAgentNumber("2930164");
				bundleGrpQuotReq.setAgentNumber(agentNumber);
				bundleGrpQuotReq.setBenefitDuration(0);			
				
//				XMLGregorianCalendar effDateXMLGregorianCalendar = null;
//				GregorianCalendar gc = new GregorianCalendar();
//				gc.set(Integer.parseInt(splitedEffectiveDate[PlanMgmtConstants.ZERO]), Integer.parseInt(splitedEffectiveDate[PlanMgmtConstants.ONE]), Integer.parseInt(splitedEffectiveDate[PlanMgmtConstants.TWO]));			
//				effDateXMLGregorianCalendar = datatypeFactory.newXMLGregorianCalendar(gc);
				GregorianCalendar gc = GregorianCalendar.from((LocalDate.parse(effectiveDateStr)).atStartOfDay(ZoneId.systemDefault()));
				XMLGregorianCalendar effDateXMLGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
				bundleGrpQuotReq.setEffectiveDate(effDateXMLGregorianCalendar);
				bundleGrpQuotReq.setPaymentFrequency(PaymentFrequency.MONTHLY);	
				bundleGrpQuotReq.setPrimaryProductType(ProductType.SHORT_TERM_MEDICAL);				
				
			}catch(Exception e) {
				LOGGER.error("Error occurred in STMPlanRateBenefitServiceImpl.processIHCQuoteRequest() ", e);
			}
			
			
			return bundleGrpQuotReq;
		}
		
		private XMLGregorianCalendar getDOBFortted(String dateOfBirth) {
			XMLGregorianCalendar formattedDateOfBirth = null;
			try{
				String newDateOfBirth = null;
				if(dateOfBirth != null && dateOfBirth.length() == 10){
					newDateOfBirth = dateOfBirth.substring(6,10)+"-"+dateOfBirth.substring(0,2)+"-"+dateOfBirth.substring(3,5);			 
				}
				formattedDateOfBirth = getXMLGregorianCalendar("yyyy-MM-dd", newDateOfBirth);			
			}catch(Exception e){
				LOGGER.error("Exception occurrect while formatting date in STMPlanRateBenefitServiceImpl.getDOBFortted() ", e);
			}	
			return formattedDateOfBirth;
		}
		
		private XMLGregorianCalendar getXMLGregorianCalendar(String formatStr, String dateValue) throws Exception {
			final DateFormat format = new SimpleDateFormat(formatStr);
			GregorianCalendar gregory = new GregorianCalendar();
			gregory.setTime(format.parse(dateValue));
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);			
		}
}
