package com.getinsured.hix.planmgmt.querybuilder;

import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtParamConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public class PufPlanQueryBuilder {

	private static String zipCountyQuery;	
	private static HashMap<String,String> pufPlansExcludesHiosPlanIdsQuery;
	private static HashMap<String,String> pufPlansIncludesHiosPlanIdsQuery;
	
	public final static String getZipCountyQuery(){
		if(PufPlanQueryBuilder.zipCountyQuery != null){
			return PufPlanQueryBuilder.zipCountyQuery;
		}
		StringBuilder buildquery = new StringBuilder(250);
		buildquery.append("SELECT DISTINCT(ZIPCODE), COUNTY, COUNTY_FIPS FROM zipcodes ");
		buildquery.append("WHERE zipcode =:");
		buildquery.append(PlanMgmtParamConstants.PARAM_ZIP_CODE);
		buildquery.append(" AND state_fips||county_fips= :");
		buildquery.append(PlanMgmtParamConstants.PARAM_COUNTY_CODE);

		PufPlanQueryBuilder.zipCountyQuery = buildquery.toString().trim();
   		return PufPlanQueryBuilder.zipCountyQuery;
	}
	
	
	public final static String getPufPlansExcludesHiosPlanIdsQuery(String insuranceType, String houseHoldType, String configuredDB){
		//String query = null;
		if(PufPlanQueryBuilder.pufPlansExcludesHiosPlanIdsQuery == null){
			PufPlanQueryBuilder.pufPlansExcludesHiosPlanIdsQuery = new HashMap<String, String>();
		}
		//query = PufPlanQueryBuilder.pufPlansExcludesHiosPlanIdsQuery.get(insuranceType);
		if( PufPlanQueryBuilder.pufPlansExcludesHiosPlanIdsQuery.get(insuranceType) != null){
			return PufPlanQueryBuilder.pufPlansExcludesHiosPlanIdsQuery.get(insuranceType);
		}
		
		String tableName = null;		
		switch(insuranceType.toUpperCase()){
		case "HEALTH" :{
			tableName = "puf_qhp ";
			break;
		}
		case "DENTAL":{
			tableName = "puf_sdp ";
			break;
		}
		default:
			throw new GIRuntimeException("insuranceType type:"+insuranceType+" not supported for getPufPlanData");
		}
		
		StringBuilder queryBuilder = new StringBuilder(1024);
		queryBuilder.append("SELECT DISTINCT puf.PLAN_ID_STANDARD_COMPONENT, puf.state, puf.rating_area, puf.metal_level, puf.issuer_name, puf.plan_marketing_name, puf.plan_type, puf.county, puf.premium_a_i_age_21 ");
		queryBuilder.append("FROM ");
		queryBuilder.append(tableName);
		queryBuilder.append("puf, pm_rating_area pra, pm_zip_county_rating_area pzarea ");
		queryBuilder.append("WHERE ");
		queryBuilder.append("pra.state = puf.state ");
		queryBuilder.append("AND pra.rating_area = puf.rating_area ");
		queryBuilder.append("AND LOWER(pzarea.county) = LOWER(puf.county) ");
		queryBuilder.append("AND  pzarea.rating_area_id = pra.id ");
		
		if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
			queryBuilder.append("AND (pzarea.zip = to_number(trim(:paramZip))");
		}else{
			queryBuilder.append("AND (pzarea.zip = :paramZip");
		}
		
		queryBuilder.append(" OR pzarea.zip IS NULL) AND pzarea.county_fips= :paramCountyFips ");
		queryBuilder.append("AND puf.IS_DELETE ='N' ");
		queryBuilder.append("AND lower(puf.metal_level) != 'catastrophic' "); // HIX-40645 don't pull catastrophic puf plans 
		queryBuilder.append("AND puf.applicable_year = CAST (:paramYear AS INTEGER) ");
		
		// excludes puff plans already exists in on/off exchange plan list 
		queryBuilder.append(" AND puf.PLAN_ID_STANDARD_COMPONENT NOT IN(:paramHiosIdList) ");
			
		// if house hold type is CHILD ONLY then pull 'Allows Child-Only', 'Allows Adult and Child-Only' plans
		// if house hold type is not CHILD ONLY then pull 'Allows Adult-Only', 'Allows Adult and Child-Only' plans
		if(PlanMgmtConstants.CHILD_ONLY.equalsIgnoreCase(houseHoldType)){
			queryBuilder.append(" AND puf.child_only_offering IN ('Allows Child-Only', 'Allows Adult and Child-Only') ");
		}else {
			queryBuilder.append(" AND puf.child_only_offering IN ('Allows Adult-Only', 'Allows Adult and Child-Only') ");
		}
		queryBuilder.append(" ORDER BY puf.plan_id_standard_component");

		//query = queryBuilder.toString().trim();
		PufPlanQueryBuilder.pufPlansExcludesHiosPlanIdsQuery.put(insuranceType,  queryBuilder.toString().trim());
		return PufPlanQueryBuilder.pufPlansExcludesHiosPlanIdsQuery.get(insuranceType);
	}
		
		public final static String getPufPlansIncludesHiosPlanIdsQuery(String insuranceType, String houseHoldType, String configuredDB){
			//String query = null;
			if(PufPlanQueryBuilder.pufPlansIncludesHiosPlanIdsQuery == null){
				PufPlanQueryBuilder.pufPlansIncludesHiosPlanIdsQuery = new HashMap<String, String>();
			}
			//query = PufPlanQueryBuilder.pufPlansIncludesHiosPlanIdsQuery.get(insuranceType);
			if(PufPlanQueryBuilder.pufPlansIncludesHiosPlanIdsQuery.get(insuranceType) != null){
				return PufPlanQueryBuilder.pufPlansIncludesHiosPlanIdsQuery.get(insuranceType);
			}
			
			String tableName = null;		
			switch(insuranceType.toUpperCase()){
			case "HEALTH" :{
				tableName = "puf_qhp ";
				break;
			}
			case "DENTAL":{
				tableName = "puf_sdp ";
				break;
			}
			default:
				throw new GIRuntimeException("insuranceType type:"+insuranceType+" not supported for getPufPlanData");
			}
			
			StringBuilder queryBuilder = new StringBuilder(1024);
			queryBuilder.append("SELECT DISTINCT puf.PLAN_ID_STANDARD_COMPONENT, puf.state, puf.rating_area, puf.metal_level, puf.issuer_name, puf.plan_marketing_name, puf.plan_type, puf.county, puf.premium_a_i_age_21 ");
			queryBuilder.append("FROM ");
			queryBuilder.append(tableName);
			queryBuilder.append("puf, pm_rating_area pra, pm_zip_county_rating_area pzarea ");
			queryBuilder.append("WHERE ");
			queryBuilder.append("pra.state = puf.state ");
			queryBuilder.append("AND pra.rating_area = puf.rating_area ");
			queryBuilder.append("AND LOWER(pzarea.county) = LOWER(puf.county) ");
			queryBuilder.append("AND  pzarea.rating_area_id = pra.id ");
			
			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				queryBuilder.append("AND (pzarea.zip = to_number(trim(:paramZip))");	
			}else{
				queryBuilder.append("AND (pzarea.zip = :paramZip ");
			}
			
			queryBuilder.append(" OR pzarea.zip IS NULL) AND pzarea.county_fips= :paramCountyFips ");
			queryBuilder.append("AND puf.IS_DELETE ='N' ");
			queryBuilder.append("AND lower(puf.metal_level) != 'catastrophic' "); // HIX-40645 don't pull catastrophic puf plans 
			queryBuilder.append("AND puf.applicable_year = CAST (:paramYear AS INTEGER) ");
				
			// if house hold type is CHILD ONLY then pull 'Allows Child-Only', 'Allows Adult and Child-Only' plans
			// if house hold type is not CHILD ONLY then pull 'Allows Adult-Only', 'Allows Adult and Child-Only' plans
			if(PlanMgmtConstants.CHILD_ONLY.equalsIgnoreCase(houseHoldType)){
				queryBuilder.append(" AND puf.child_only_offering IN ('Allows Child-Only', 'Allows Adult and Child-Only') ");
			}else {
				queryBuilder.append(" AND puf.child_only_offering IN ('Allows Adult-Only', 'Allows Adult and Child-Only') ");
			}
			queryBuilder.append(" ORDER BY puf.plan_id_standard_component");
				
		//	query = queryBuilder.toString().trim();
			PufPlanQueryBuilder.pufPlansIncludesHiosPlanIdsQuery.put(insuranceType, queryBuilder.toString().trim());
			return PufPlanQueryBuilder.pufPlansIncludesHiosPlanIdsQuery.get(insuranceType);
		}	
	
}
