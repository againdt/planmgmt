package com.getinsured.hix.planmgmt.phix.service;

import com.getinsured.hix.model.planmgmt.EligibilityRequest;
import com.getinsured.hix.model.planmgmt.EligibilityResponse;
import com.getinsured.hix.model.planmgmt.TeaserPlanRequest;
import com.getinsured.hix.model.planmgmt.TeaserPlanResponse;
import com.getinsured.hix.platform.util.exception.GIException;

public interface PrivateExchangeService {
	
	EligibilityResponse getEligibilityPremium(EligibilityRequest eligibilityRequest) throws GIException; // New 
	
	TeaserPlanResponse getSilverPlan(TeaserPlanRequest teaserPlanRequest, boolean isLowest) throws GIException;
	
	EligibilityResponse getEligibilityPremiumAllForTiers(EligibilityRequest eligibilityRequest, String sendAllMetalTier) throws GIException; // New
}
