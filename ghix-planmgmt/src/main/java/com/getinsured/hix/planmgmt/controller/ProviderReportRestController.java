/**
 * ProviderReportRestController.java
 * @author Sandip Shengole
 * @version 1.0
 * @since September 07, 2015 
 */

package com.getinsured.hix.planmgmt.controller;

import static org.apache.log4j.Logger.getLogger;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.planmgmt.batch.service.provider.ProviderPlanNetworkReportService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.platform.util.exception.GIException;

//@Api(value="Provider Report Information Operation", description ="Returning provider report Information")
@Controller
@DependsOn("dynamicPropertiesUtil")
@RequestMapping(value = "/providerReport")
public class ProviderReportRestController {	
	private static final Logger LOGGER = getLogger(ProviderReportRestController.class);

	@Autowired
	private ProviderPlanNetworkReportService providerPlanNetworkReportService;
		
//	@ApiOperation(value="welcome", notes="Welcome to Provider Report Information")
//	@ApiResponse(value="Welcome to provider report rest controller, invoke ProviderReportRestController")
//	@RequestMapping(value = "/welcome", method = RequestMethod.GET) // for testing purpose
//	@ResponseBody public String welcome(){
//		if(LOGGER.isInfoEnabled()){
//			LOGGER.info("Welcome to GHIX-Planmgmt module, invoke ProviderReportRestController");
//		}
//		return "Welcome to GHIX-Planmgmt module, invoke ProviderReportRestController";
//	}
		
//	@ApiOperation(value="generateProviderPlanNetworkReport", notes="This API generates provider plan network report in xls format and returing only success or failure response")
//	@ApiResponse(value="Return report generation response in success or failure")
	@RequestMapping(value = "/generateProviderPlanNetworkReport", method = RequestMethod.POST)
	@ResponseBody public String generateProviderPlanNetworkReport()throws  GIException{
		String returnResponseString = PlanMgmtConstants.FALSE_STRING;
		try{
			returnResponseString = providerPlanNetworkReportService.generatePlanNetworkReport();
		}catch(Exception ex){
			LOGGER.error("Exception occured in generateProviderPlanNetworkReport : ", ex);
		}
		return returnResponseString;
	}
	
}
