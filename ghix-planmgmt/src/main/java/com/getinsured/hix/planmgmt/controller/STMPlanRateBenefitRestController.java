/**
 * STMPlanRateBenefitRestController.java
 * @author santanu
 * @version 1.0
 * @since Apr 24, 2014 
 */
package com.getinsured.hix.planmgmt.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Produces;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.dto.planmgmt.PlanRateBenefitResponse;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.planmgmt.service.STMPlanRateBenefitService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;


//@Api(value="STM Plans rate and benefit information", description ="Returning STM Plans benefit and rate information depends on zip, county, monthly income and census information")
@Controller
@RequestMapping(value = "/stmplanratebenefit")
@DependsOn("dynamicPropertiesUtil")
public class STMPlanRateBenefitRestController {	
	private static final Logger LOGGER = Logger.getLogger(STMPlanRateBenefitRestController.class);
	
	@Autowired
	private STMPlanRateBenefitService sTMPlanRateBenefitService;
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	/**
	 * Constructor.
	 */
	public STMPlanRateBenefitRestController() {}
	
	/* test controller */	
//	@ApiOperation(value="welcome", notes="Welcome to STM Plan rate benefit rest controller")
//	@ApiResponse(value="Welcome to STM Plan rate benefit rest controller, invoke STMPlanRateBenefitRestController")
//	@RequestMapping(value = "/welcome", method = RequestMethod.GET) // for testing purpose
//	@ResponseBody public String welcome()
//	{
//		LOGGER.info("Welcome to GHIX-Planmgmt module, invoke STMPlanRateBenefitRestController()");		
//		return "Welcome to GHIX-Planmgmt module, invoke STMPlanRateBenefitRestController";
//	}
		

	/* For STM plans */
	/**
	 * Method to handle ReST web service request of stm plans
	 *
	 * @param planRateBenefitRequest The PlanRateBenefitRequest instance.
	 * @return String The PlanRateBenefitResponse as stringified JSON.
	 */
//	@ApiOperation(value="stmplan", notes="This API retrive STM plans with rate and benefits")
//	@ApiResponse(value="Returning STM plan with rate and benefits depends on zip, county, monthly income and census information")
	@RequestMapping(value = "/stmplan", method = RequestMethod.POST)
	@Produces("application/json")
	public @ResponseBody PlanRateBenefitResponse getStmPlanRateBenefits(@RequestBody PlanRateBenefitRequest planRateBenefitRequest) {
		LOGGER.info("Beginning to process current request of getStmPlanRateBenefits().");		
		PlanRateBenefitResponse planRateBenefitResponse = new PlanRateBenefitResponse();

		if(null == planRateBenefitRequest || null == planRateBenefitRequest.getRequestParameters()) {
			planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
			planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
			planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
		}
		else {
			Map<String, Object> cRequestParams = planRateBenefitRequest.getRequestParameters();
			if(null == cRequestParams 					
					|| null == cRequestParams.get(PlanMgmtConstants.EFFECTIVEDATE)				
					|| null == cRequestParams.get(PlanMgmtConstants.MEMBERLIST)
					) {
				planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.code);
				planRateBenefitResponse.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			}
			else {
				try {
					Validate.notNull(sTMPlanRateBenefitService, PlanMgmtConstants.PLANRATESERVICE_CONFIGURE_ERROR);				
					List<PlanRateBenefit> responseData =  getStmPlanRateBenefitsHelper(planRateBenefitRequest);
					planRateBenefitResponse.setPlanRateBenefitList(responseData);
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
				catch(Exception ex) {
					giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Failed to fetch STM plans.",ex);
					LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);
					planRateBenefitResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
					planRateBenefitResponse.setErrCode(PlanMgmtErrorCodes.ErrorCode.STM_PLAN_RATE_BENEFITS.code);
					planRateBenefitResponse.setErrMsg(Arrays.toString(ex.getStackTrace()));
				}
			}

		}

		LOGGER.info("Processing current request of getStmPlanRateBenefits() is done.");
		return planRateBenefitResponse;
	}
	
	// create helper class to call directly to avoid REST call
    public List<PlanRateBenefit> getStmPlanRateBenefitsHelper(PlanRateBenefitRequest planRateBenefitRequest){
    	List<PlanRateBenefit> responseData = new ArrayList<PlanRateBenefit>();
    	Map<String, Object> cRequestParams = planRateBenefitRequest.getRequestParameters();
    	
    	if(null == cRequestParams 					
				|| null == cRequestParams.get(PlanMgmtConstants.EFFECTIVEDATE)				
				|| null == cRequestParams.get(PlanMgmtConstants.MEMBERLIST)
				) {    		
			return responseData;
		}
		else {		
	    	try {
				Validate.notNull(sTMPlanRateBenefitService, PlanMgmtConstants.PLANRATESERVICE_CONFIGURE_ERROR);
				
		    	List<Map<String, String>> memberList = (List<Map<String, String>>) cRequestParams.get(PlanMgmtConstants.MEMBERLIST);
				String effectiveDate = cRequestParams.get(PlanMgmtConstants.EFFECTIVEDATE).toString().trim();	
				String tenantCode = "";
				if(null != cRequestParams.get(PlanMgmtConstants.TENANT) && !"".equalsIgnoreCase(cRequestParams.get(PlanMgmtConstants.TENANT).toString())) {
					tenantCode = cRequestParams.get(PlanMgmtConstants.TENANT).toString().trim();					
				} else {
					tenantCode = PlanMgmtConstants.TENANT_GINS.toString(); 					
				}
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("MEMBER INFO == "+ SecurityUtil.sanitizeForLogging(memberList.toString()) + " >> effectiveDate ==" + SecurityUtil.sanitizeForLogging(effectiveDate));
				}
				
		        responseData = sTMPlanRateBenefitService.getStmPlanRateBenefits(memberList, effectiveDate, tenantCode);
		        Validate.notNull(responseData, "Invalid response returned from business method.");
	    	}
	    	catch(Exception ex) {
				LOGGER.error("Exception occurrecd while processing request. Exception:" ,ex);	
				return responseData;
			}
		}
    	
        return responseData;
    	
    }
    
}
