//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.12 at 02:20:37 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.ihc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="parameters" type="{http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.MajorMedical}MajorMedBundleQuoteParameters" minOccurs="0"/>
 *         &lt;element name="planIDs" type="{http://schemas.microsoft.com/2003/10/Serialization/Arrays}ArrayOfstring" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "parameters",
    "planIDs"
})
@XmlRootElement(name = "GetMajorMedSupplementalApplyUrl")
public class GetMajorMedSupplementalApplyUrl {

    @XmlElementRef(name = "parameters", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<MajorMedBundleQuoteParameters> parameters;
    @XmlElementRef(name = "planIDs", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfstring> planIDs;

    /**
     * Gets the value of the parameters property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link MajorMedBundleQuoteParameters }{@code >}
     *     
     */
    public JAXBElement<MajorMedBundleQuoteParameters> getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the parameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link MajorMedBundleQuoteParameters }{@code >}
     *     
     */
    public void setParameters(JAXBElement<MajorMedBundleQuoteParameters> value) {
        this.parameters = value;
    }

    /**
     * Gets the value of the planIDs property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public JAXBElement<ArrayOfstring> getPlanIDs() {
        return planIDs;
    }

    /**
     * Sets the value of the planIDs property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfstring }{@code >}
     *     
     */
    public void setPlanIDs(JAXBElement<ArrayOfstring> value) {
        this.planIDs = value;
    }

}
