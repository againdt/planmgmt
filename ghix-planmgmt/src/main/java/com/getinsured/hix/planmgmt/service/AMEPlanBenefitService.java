/**
 * 
 */
package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.planmgmt.AmePlan;

/**
 * @author Krishna Gajulapalli
 * 
 */
public interface AMEPlanBenefitService {
	List<AmePlan> getAmePlanDetails(String effectiveDate, List<Map<String, String>> memberList, String tenantCode);
	
	Map<String, Map<String, String>> getAmePlanBenefits(Integer planAmeIdStr);
	
	List<AmePlan> getMaxBenefitAmePlan(String effectiveDate, List<Map<String, String>> memberList, String tenantCode);
	
	List<Map<String,Object>> getAMEAndCrosssellHelper(List<Map<String, String>> processedMemberData, String effectiveDate, String relation,  String familyTierLookupCode, boolean doQuoteIssuerIHC, String stateName, String tenantCode);
}
