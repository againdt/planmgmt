package com.getinsured.hix.planmgmt.history.jpa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.getinsured.hix.planmgmt.service.RegionService;
import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
/**
 * This class is used to populate the List<Map<String, Object>> base on the data got from REV_history or AUD tables.
 * Popultaed List<Map<String, Object>> can be used to display the corresponding data as History pages in UI
 * @author save_h
 */
@Service
public class PlanChildHistoryRendererHelper implements HistoryRendererService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanChildHistoryRendererHelper.class);	
	private String appUrl = GhixConstants.APP_URL;
	@Autowired private UserService userService;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	public static final String REPOSITORY_NAME = "iPlanHealthRepository";
	public static final String MODEL_NAME = "com.getinsured.hix.model.PlanHealth";
	public static final String DENTAL_REPOSITORY_NAME = "iPlanDentalRepository";
	public static final String DENTAL_MODEL_NAME = "com.getinsured.hix.model.PlanDental";
	
	/*private static final String COMMENT_TEXT_PRE = "<a onclick=\"getComment('";
	private static final String COMMENT_TEXT_POST = "');\" href='#modalBox' data-toggle='modal'>Comment</a>";
	public static final String TIMESTAMP_PATTERN1 = "yyyy-MM-dd HH:mm:ss.SSS";
	private static final String TIMESTAMP_PATTERN2 = "yyyy-MM-dd";
	private static final String DOWNLOAD_FILE_PRETEXT1 = "<a href='";
	private static final String DOWNLOAD_FILE_PRETEXT2 =  "download/document?documentId=";
	private static final String DOWNLOAD_FILE_POSTTEXT_BENEFITS = "'>Download File</a>";
	private static final String CREATE_AND_DOWNLOAD_BENEFIT_FILE1 = "<a href='/hix/admin/planmgmt/dowloadbenefits/";
	private static final String CREATE_AND_DOWNLOAD_BENEFIT_FILE2 =	"'>Download File</a>";
	private static final String CREATE_AND_DOWNLOAD_RATE_FILE1 = "<a href='/hix/issuer/qhp/dowloadqhprates/";
	private static final String CREATE_AND_DOWNLOAD_RATE_FILE2 =	"'>Download File</a>";*/

	/*public static final int RATEFILE = 1;
	public static final int UPDATE_DATE = 2;
	public static final int RATE_EFFECTIVE_DATE = 3;
	public static final int USER_NAME = 4;
	public static final int COMMENT = 5;
	public static final int BENEFITFILE = 6;
	public static final int FN_RATEFILE_VAL = 7;
	public static final int FN_BENEFITFILE_VAL = 8;
	public static final int FN_DISPLAYVAL = 9;
	public static final int BENEFIT_EFFECTIVE_DATE = 10;
	public static final int EFFECTIVE_DATE_COMBINED = 11;
	public static final int PLAN_SBC_VAL = 12;*/
	
	/*public static final String RATEFILE_COL = "rateFile";
	public static final String UPDATE_DATE_COL = "lastUpdateTimestamp";
	public static final String RATE_EFFECTIVE_DATE_COL = "rateEffectiveDate";
	public static final String USER_NAME_COL = "userName";
	public static final String USER_NAME_ALIAS = "lastUpdatedBy";
	public static final String COMMENT_COL = "commentId";
	public static final String BENEFITFILE_COL = "benefitFile";
	public static final String DISPLAYVAL_COL = "displayVal";
	public static final String DISPLAYFIELD_COL = "displayField";
	public static final String BENEFIT_EFFECTIVE_DATE_COL = "benefitEffectiveDate";
	public static final String EFFECTIVE_DATE_COMB_COL = "effectiveDateCombined";
	public static final String PLAN_SBC_COL = "sbcUcmId";
	private static final String PLAN_BENEFITS = "Plan Benefits";
	private static final String PLAN_RATES = "Plan Rates";
	private static final String PLAN_SBC = "Plan SBC";*/
	
	/**
	 * Populates the List<Map<String, Object>> which can be used to display the corresponding data as History pages in UI
	 * @param data
	 * 			List<Map<String, String>> input of the entire revision history data.
	 * @param compareColumns
	 * 			List<String> of column names based on which the filteration of @param data should be done.
	 * @param columnsToDisplay
	 * 			List<Integer> of column ids specified in corresponding HistoryService, which will indicate newly prepared map should contain what all keys.
	 * @return
	 * 			List<Map<String, Object>> Filtered data based on @param data, @param compareColumns and @param columnsToDisplay.
	 */
	public List<Map<String, Object>> processData(List<Map<String, String>> data, List<String> compareColumns, List<Integer> columnsToDisplay) 
	{
		List<Map<String, String>> orderedData = new ArrayList<Map<String,String>>(data);
		Collections.reverse(orderedData);
		int size = orderedData.size();
		Map<String, String> firstElement = null;
		Map<String, String> secondElement = null;
		
		Integer planId = null;
		String role = null;
		try {
			// This piece of code need review
			if (StringUtils.isNumeric(compareColumns.get(compareColumns.size()-2))) {
				planId = Integer.parseInt(compareColumns.get(compareColumns.size()-2));
				compareColumns.remove(compareColumns.size()-2);
			}

			// This piece of code need review
			if (StringUtils.isNotEmpty(compareColumns.get(compareColumns.size() - 1)) &&
					(compareColumns.get(compareColumns.size() - 1).equalsIgnoreCase(PlanMgmtConstants.ADMIN_ROLE) ||
					compareColumns.get(compareColumns.size() - 1).equalsIgnoreCase(PlanMgmtConstants.ISSUER_ROLE))) {
				role = (compareColumns.get(compareColumns.size()- 1));
				compareColumns.remove(compareColumns.size()- 1);
			}
		} catch (Exception e) {
			planId = null;
			role = null;
			LOGGER.warn(e.getMessage());
		}
		
		List<Map<String, Object>> processedData = new ArrayList<Map<String,Object>>();
		boolean isNullSbcBenifits = false;
		try {
			for (int i=0; i<size-1;i++){
				if(i == 0 ){
					firstElement = orderedData.get(i);
					secondElement = orderedData.get(i+1);
					
					for (String keyColumn : compareColumns){
						boolean valueChanged = isEqual(firstElement,secondElement,keyColumn);
						isNullSbcBenifits = isNullSbcBenifits(firstElement,keyColumn);
						if(!valueChanged){							
							if(!isNullSbcBenifits){
								Map<String, Object> processedMap1 = formMap(firstElement,keyColumn, columnsToDisplay,planId, role);
								processedData.add(processedMap1);
							}
							isNullSbcBenifits = isNullSbcBenifits(secondElement,keyColumn);
							if(!isNullSbcBenifits){
								Map<String, Object> processedMap2 = formMap(secondElement,keyColumn, columnsToDisplay,planId, role);
								processedData.add(processedMap2);
							}
						}else {
							if(!isNullSbcBenifits){
								Map<String, Object> processedMap = formMap(firstElement,keyColumn, columnsToDisplay,planId, role);
								processedData.add(processedMap);
							}
						}
					}
				}else {
					firstElement = orderedData.get(i);
					secondElement = orderedData.get(i+1);
					for (String keyColumn : compareColumns){
						boolean valueChanged = isEqual(firstElement,secondElement,keyColumn);
						if(!valueChanged){
							isNullSbcBenifits = isNullSbcBenifits(secondElement,keyColumn);
							if(!isNullSbcBenifits){
								Map<String, Object> processedMap = formMap(secondElement, keyColumn, columnsToDisplay,planId, role);
								processedData.add(processedMap);
							}
						}
					}
				}
			}
			if(size == 1){
				for (String keyColumn : compareColumns){
					firstElement = orderedData.get(0);
					isNullSbcBenifits = isNullSbcBenifits(firstElement,keyColumn);
					if(!isNullSbcBenifits){
						Map<String, Object> processedMap = formMap(firstElement, keyColumn, columnsToDisplay,planId, role);
						if(StringUtils.isNotBlank(processedMap.get(PlanMgmtConstants.DISPLAYFIELD_COL).toString())){
							processedData.add(processedMap);
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error Processing Data : " + e);
		}
		
		return processedData;
	}
	
	/**
	 * Checks whether map contains null SBC Benefits
	 * @param element of type Map<String, String>
	 * @param keyColumn of type String
	 * @return boolean
	 */
	private boolean isNullSbcBenifits(Map<String, String> element,  String keyColumn) {
		if(keyColumn.equalsIgnoreCase(PlanMgmtConstants.PLAN_SBC_COL)){
			if(StringUtils.isBlank(element.get(keyColumn)) ){
				return true;
			}
			
		}
		return false;
	}
	
	/**
	 * Compares first element data and second element data
	 * @param firstElement of type Map<String, String>
	 * @param secondElement of type Map<String, String>
	 * @param keyColumn of type String
	 * @return boolean
	 */
	private boolean isEqual(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) {
		return firstElement.get(keyColumn).equals(secondElement.get(keyColumn));
	}
	
	/**
	 * Forms map for given key.
	 * The newly formed map contains key-value pairs based on the list provided as columnsToDisplay.
	 * 
	 * @param firstMap of type Map<String,String>
	 * @param key of type String
	 * @param columnsToDisplay of type List<Integer>
	 * @param planId of type Integer
	 * @return Map<String,Object>
	 */
	private Map<String,Object> formMap(Map<String,String> firstMap,String key, List<Integer> columnsToDisplay,Integer planId, String role)
	{
		Map<String, Object> values = new HashMap<String, Object>();
		String fileUrl = null;
		String value = null;
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern(PlanMgmtConstants.TIMESTAMP_PATTERN1);

		SimpleDateFormat sdf2 = new SimpleDateFormat();
		sdf2.applyPattern(PlanMgmtConstants.TIMESTAMP_PATTERN2);
		for (int colId : columnsToDisplay)
		{
			switch (colId) 
			{
			case PlanMgmtConstants.RATEFILE_INT: 
				String rateFile = null;
				rateFile = firstMap.get(PlanMgmtConstants.RATEFILE_COL);
				if(rateFile != null && rateFile.length()>0){
					fileUrl = PlanMgmtConstants.CREATE_AND_DOWNLOAD_RATE_FILE1 + planId + PlanMgmtConstants.CREATE_AND_DOWNLOAD_RATE_FILE2;
					values.put(PlanMgmtConstants.RATEFILE_COL, fileUrl);
				}else{
					values.put(PlanMgmtConstants.RATEFILE_COL, "");
				}
			break;
			case PlanMgmtConstants.UPDATE_DATE_INT: 
				String dt = firstMap.get(PlanMgmtConstants.UPDATE_DATE_COL);
				try 
				{
					values.put(PlanMgmtConstants.UPDATE_DATE_COL, sdf.parse(dt));
				}
				catch (ParseException e) 
				{
					LOGGER.error("Unable to parse Udated Date "+ dt + " ", e);
				}
				break;
				
			case PlanMgmtConstants.RATE_EFFECTIVE_DATE_INT:
				String effDate = firstMap.get(PlanMgmtConstants.RATE_EFFECTIVE_DATE_COL);
				try 
				{
					Date effectivedate = null;
					if (effDate != null && effDate.length() > 0)
					{
						effectivedate = sdf2.parse(effDate);
					}	
					values.put(PlanMgmtConstants.RATE_EFFECTIVE_DATE_COL, effectivedate);
				}
				catch (ParseException e) 
				{
					LOGGER.error("Unable to parse Rates effective Date "+ effDate + " ", e);
				}
				break;
				
			case PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_INT:
				String effDate1 = firstMap.get(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_COL);
				try 
				{
					Date benefitEffectivedate = null;
					if (effDate1 != null && effDate1.length() > 0)
					{
						benefitEffectivedate = sdf2.parse(effDate1);
					}	
					values.put(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_COL, benefitEffectivedate);
				}
				catch (ParseException e) 
				{
					LOGGER.error("Unable to parse Benefits effective Date "+ effDate1 + " ", e);
				}
				break;
				
			case PlanMgmtConstants.USER_NAME_INT:
				String userId = firstMap.get(PlanMgmtConstants.USER_NAME_ALIAS);
				values.put(PlanMgmtConstants.USER_NAME_COL, userService.getUserName(Integer.parseInt(userId)));
				break;
				
			case PlanMgmtConstants.COMMENT_INT:
				String commentId = firstMap.get(PlanMgmtConstants.COMMENT_COL);
				if(commentId != null && commentId.length()>0)
				{
					values.put(PlanMgmtConstants.COMMENT_COL, PlanMgmtConstants.COMMENT_TEXT_PRE+commentId+PlanMgmtConstants.COMMENT_TEXT_POST);
				}
				else
				{
					values.put(PlanMgmtConstants.COMMENT_COL, "");
				}
				break;
				
			case PlanMgmtConstants.BENEFITFILE_INT: 
				String benefitFile = null;
				benefitFile = firstMap.get(PlanMgmtConstants.BENEFITFILE_COL);
				values.put(PlanMgmtConstants.BENEFITFILE_COL, fileUrl);
				if(benefitFile != null && benefitFile.length()>0){

					if(role.equalsIgnoreCase(PlanMgmtConstants.ADMIN_ROLE)){
						fileUrl = PlanMgmtConstants.CREATE_AND_DOWNLOAD_BENEFIT_FILE1 + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(planId)) + PlanMgmtConstants.CREATE_AND_DOWNLOAD_BENEFIT_FILE2;
					}
					else {
						fileUrl = PlanMgmtConstants.CREATE_AND_DOWNLOAD_BENEFIT_FILE3 + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(planId)) + PlanMgmtConstants.CREATE_AND_DOWNLOAD_BENEFIT_FILE2;
					}
					values.put(PlanMgmtConstants.BENEFITFILE_COL, fileUrl);
				}else{
					values.put(PlanMgmtConstants.BENEFITFILE_COL, "");
				}
				break;
			case PlanMgmtConstants.PLAN_SBC_VAL_INT:
			case PlanMgmtConstants.FN_RATEFILE_VAL_INT: 
			case PlanMgmtConstants.FN_BENEFITFILE_VAL_INT:
				value = getValue(firstMap, key, colId,planId, role);
				if (value != null)
				{
					values.put(PlanMgmtConstants.DISPLAYVAL_COL, value);
				}
				break;
			case PlanMgmtConstants.FN_DISPLAYVAL_RATEHISTORY_INT:
				String displayVal = getDisplayVal(key);
				values.put(PlanMgmtConstants.DISPLAYFIELD_COL, displayVal);
				break;
			case PlanMgmtConstants.EFFECTIVE_DATE_COMBINED_INT:
				Date effectiveDateCombined = getDisplayDate(firstMap, key, sdf2);
				values.put(PlanMgmtConstants.EFFECTIVE_DATE_COMB_COL, effectiveDateCombined);
				break;
			}
		}
		return values;
	}

	/**
	 * Returns display date
	 * @param firstMap of type Map<String,String>
	 * @param key of type String
	 * @param sdf of type SimpleDateFormat
	 * @return Date
	 */
	private Date getDisplayDate(Map<String,String> firstMap, String key, SimpleDateFormat sdf) {
		Date effectiveDate = null;
		if(PlanMgmtConstants.BENEFITFILE_COL.equals(key))
		{
			String effDate = firstMap.get(PlanMgmtConstants.BENEFIT_EFFECTIVE_DATE_COL);
			try 
			{
				if (effDate != null && effDate.length() > 0)
				{
					effectiveDate = sdf.parse(effDate);
				}	
			}
			catch (ParseException e) 
			{
				LOGGER.error("Unable to parse Benefit effective Date "+ effDate + " ", e);
			}
		}
		else if (PlanMgmtConstants.RATEFILE_COL.equals(key))
		{
			String effDate = firstMap.get(PlanMgmtConstants.RATE_EFFECTIVE_DATE_COL);
			try 
			{
				if (effDate != null && effDate.length() > 0)
				{
					effectiveDate = sdf.parse(effDate);
				}	
			}
			catch (ParseException e) 
			{
				LOGGER.error("Unable to parse Rates effective Date "+ effDate + " ", e);
			}
			
		}
		return effectiveDate;
	}

	/**
	 * 
	 * @param firstMap of type Map<String,String>
	 * @param key of type String
	 * @param colId of type int
	 * @param planId of type Integer
	 * @return String
	 */
	private String getValue(Map<String,String> firstMap, String key, int colId, Integer planId, String role) 
	{
		String value = null;
		switch (colId) 
		{
		case PlanMgmtConstants.FN_RATEFILE_VAL_INT: 
			if (PlanMgmtConstants.RATEFILE_COL.equals(key))
			{
				String rateFile1 = firstMap.get(PlanMgmtConstants.RATEFILE_COL);
				if(rateFile1 != null && rateFile1.length()>0){
					value = PlanMgmtConstants.CREATE_AND_DOWNLOAD_RATE_FILE1 + planId + PlanMgmtConstants.CREATE_AND_DOWNLOAD_RATE_FILE2;
				}else{
					value = "";
				}
			}
			break;
		case PlanMgmtConstants.FN_BENEFITFILE_VAL_INT:
			if (PlanMgmtConstants.BENEFITFILE_COL.equals(key))
			{
				String benefitFile1 = firstMap.get(PlanMgmtConstants.BENEFITFILE_COL);
				if(benefitFile1 != null && benefitFile1.length()>0){

					if(role.equalsIgnoreCase(PlanMgmtConstants.ADMIN_ROLE)){
						value = PlanMgmtConstants.CREATE_AND_DOWNLOAD_BENEFIT_FILE1 + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(planId)) + PlanMgmtConstants.CREATE_AND_DOWNLOAD_BENEFIT_FILE2;
					}
					else {
						value = PlanMgmtConstants.CREATE_AND_DOWNLOAD_BENEFIT_FILE3 + ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(planId)) + PlanMgmtConstants.CREATE_AND_DOWNLOAD_BENEFIT_FILE2;
					}
				}else{
					value = "";
				}
			}
			break;
		case PlanMgmtConstants.PLAN_SBC_VAL_INT:
			if (PlanMgmtConstants.PLAN_SBC_COL.equals(key))
			{
				String plansbcurl = firstMap.get(PlanMgmtConstants.PLAN_SBC_COL);
				if(plansbcurl != null && plansbcurl.length()>0)
				{
					value = PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT1 + appUrl + PlanMgmtConstants.DOWNLOAD_FILE_PRETEXT2 + ghixJasyptEncrytorUtil.encryptStringByJasypt(plansbcurl) + PlanMgmtConstants.DOWNLOAD_FILE_POSTTEXT_BENEFITS;
				}
				else
				{
					value = "";
				}
			}
			break;
			
		}
		return value;
	}

	/**
	 * 
	 * @param key of type String
	 * @return String
	 */
	private String getDisplayVal(String key) 
	{
		String displayVal = null;
		if (key.equals(PlanMgmtConstants.BENEFITFILE_COL))
		{
			displayVal = PlanMgmtConstants.PLAN_BENEFITS; 
		}
		else if (key.equals(PlanMgmtConstants.RATEFILE_COL))
		{
			displayVal = PlanMgmtConstants.PLAN_RATES;
		}
		else if(key.equals(PlanMgmtConstants.PLAN_SBC_COL)) {
			
			displayVal = PlanMgmtConstants.PLAN_SBC;
		}
		return displayVal;
	}
	
}
