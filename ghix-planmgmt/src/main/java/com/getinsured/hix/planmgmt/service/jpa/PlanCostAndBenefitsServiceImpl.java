/* This class is used to pull plan benefits and cost from database  */

package com.getinsured.hix.planmgmt.service.jpa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.plandisplay.PlanDentalBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanDentalCostDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthBenefitDTO;
import com.getinsured.hix.dto.plandisplay.PlanHealthCostDTO;
import com.getinsured.hix.dto.planmgmt.PlanBenefitAndCostRequestDTO;
import com.getinsured.hix.model.GHIXResponse;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanBenefitEdit;
import com.getinsured.hix.model.PlanCostEdit;
import com.getinsured.hix.model.PlanDentalBenefit;
import com.getinsured.hix.model.PlanDentalCost;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.PlanHealthCost;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.planmgmt.querybuilder.PlanCostAndBenefitQueryBuilder;
import com.getinsured.hix.planmgmt.repository.IPlanBenefitEditRepository;
import com.getinsured.hix.planmgmt.repository.IPlanCostEditRepository;
import com.getinsured.hix.planmgmt.repository.IPlanDentalBenefitRepository;
import com.getinsured.hix.planmgmt.repository.IPlanDentalCostRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthBenefitRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthCostRepository;
import com.getinsured.hix.planmgmt.repository.IPlanRepository;
import com.getinsured.hix.planmgmt.service.PlanCostAndBenefitsService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.planmgmt.util.PlanMgmtParamConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.planmgmt.util.ProcessDisplayDataFromMap;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.timeshift.TimeShifterUtil;


@Service("PlanCostAndBenefitsService")
@Repository
@Transactional
public class PlanCostAndBenefitsServiceImpl implements PlanCostAndBenefitsService {


	@Autowired
	private IPlanHealthCostRepository iPlanHealthCostRepository;
	@Autowired
	private IPlanDentalCostRepository iPlanDentalCostRepository;
	@Autowired
	private IPlanDentalBenefitRepository iPlanDentalBenefitRepository;
	@Autowired
	private IPlanHealthBenefitRepository iPlanHealthBenefitRepository;
	@Autowired IPlanBenefitEditRepository iPlanBenefitEditRepository;
	@PersistenceUnit
	private EntityManagerFactory emf;
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	@Autowired
	private IPlanRepository iPlanRepository;
	@Autowired 
	IPlanCostEditRepository iPlanCostEditRepository;

	private ProcessDisplayDataFromMap displayDataFromMap = new ProcessDisplayDataFromMap();
	private static final Logger LOGGER = Logger.getLogger(PlanCostAndBenefitsServiceImpl.class);
	DateFormat simpleDf = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT);	
	
	public static String[] helathBenefitListForWAState = {"PRIMARY_VISIT", "SPECIAL_VISIT", "LABORATORY_SERVICES", "IMAGING_XRAY",
        "IMAGING_SCAN", "OUTPATIENT_SERVICES_OFFICE_VISIT", "OUTPATIENT_FACILITY_FEE", "OUTPATIENT_SURGERY_SERVICES", "OUTPATIENT_REHAB_SERVICES",
        "INPATIENT_PHY_SURGICAL_SERVICE", "INPATIENT_HOSPITAL_SERVICE", "DURABLE_MEDICAL_EQUIP","MAJOR_DENTAL_CARE_CHILD", "ACUPUNCTURE", "CHIROPRACTIC", "WEIGHT_LOSS", "GENERIC", "PREFERRED_BRAND", "NON_PREFERRED_BRAND", "SPECIALTY_DRUGS"};
	
	
	public Map<Integer, Map<String, Map<String, String>>> getOptimizedHealthPlanBenefits(List<Integer> planIds, String effectiveDate) {
		// pull require health plan benefits only	
		String[] healthBenefitListArr = GhixConstants.helathBenefitList;				
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		
		// if state is exchange is WA, then pull WA specific benefits 
		if(PlanMgmtConstants.STATE_CODE_WA.equalsIgnoreCase(stateCode)){
			healthBenefitListArr = helathBenefitListForWAState;
		}
		
		Map<Integer, Map<String, Map<String, String>>> benefitDataList = new HashMap<Integer, Map<String, Map<String, String>>>();
		ArrayList<String> healthBenefitList = new ArrayList<String>(healthBenefitListArr.length);
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
			String buildquery = PlanCostAndBenefitQueryBuilder.getHealthPlanBenefitsQuery();
			for(int i = 0;i < healthBenefitListArr.length; i++){
				healthBenefitList.add(healthBenefitListArr[i]);
			}
			HashMap<String, Object> parameterMap = new HashMap<String, Object>(3);
			parameterMap.put("param_planHealthIdListInput", planIds);
			parameterMap.put("param_helathBenefitNameInputStr", healthBenefitList);
			//parameterMap.put("param_effectiveDate", effectiveDate);
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("SQL = " + buildquery.toString());
			}
			
			Query query = entityManager.createQuery(buildquery.toString());
			query = PlanMgmtUtil.setQueryParameters(query, parameterMap);
			List<?> planHealthList = query.getResultList();
			Iterator<?> rsIterator = planHealthList.iterator();
			Object[] objArray = null;
			Map<String, String> benefitAttrib = null;
			String benefitName = PlanMgmtConstants.EMPTY_STRING;
			Integer rsPlanHealthId = 0;
			String explanation =  PlanMgmtConstants.EMPTY_STRING;
			String limitExcepDisplay =  PlanMgmtConstants.EMPTY_STRING;
			
			while (rsIterator.hasNext()) {
	        	objArray = (Object[]) rsIterator.next();
	        	benefitName = objArray[PlanMgmtConstants.SEVENTEEN].toString();
				rsPlanHealthId = (Integer)objArray[PlanMgmtConstants.EIGHTEEN];
				benefitAttrib = new HashMap<String, String>();		
				limitExcepDisplay =  (objArray[PlanMgmtConstants.TWENTYTWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYTWO].toString().trim();
				explanation =  (objArray[PlanMgmtConstants.TWENTYFIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYFIVE].toString().trim();
				
				//HIX-91011 Revert HIX-89636 changes
				//HIX-89636 :: if exchange type is PHIX and explanation is not empty, append EXPLANATION with LIMIT_EXCEP_DISPLAY
				/*if(GhixConstants.PHIX.equalsIgnoreCase(exchangeType) && StringUtils.isNotBlank(explanation)){
					 if(StringUtils.isBlank(limitExcepDisplay)){
						 limitExcepDisplay += explanation;
					 }else{
						 limitExcepDisplay += "<BR>" + explanation;
					 }
				}*/
				
				benefitAttrib.put(PlanMgmtConstants.NETWORK_LIMIT, (objArray[PlanMgmtConstants.ZERO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ZERO].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_LIMIT_ATTRIBUTE, (objArray[PlanMgmtConstants.ONE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ONE].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_EXEC, (objArray[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWO].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COPAY_VALUE, (objArray[PlanMgmtConstants.THREE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.THREE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COPAY_ATTRIBUTE, (objArray[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOUR].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COINSVAL, (objArray[PlanMgmtConstants.FIVE] == null) ? PlanMgmtConstants.EMPTY_STRING: objArray[PlanMgmtConstants.FIVE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COINS_ATTRIBUTE, (objArray[PlanMgmtConstants.SIX] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SIX].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COPAY_VALUE, (objArray[PlanMgmtConstants.SEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SEVEN].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COPAY_ATTRIBUTE, (objArray[PlanMgmtConstants.EIGHT] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.EIGHT].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COINSVAL, (objArray[PlanMgmtConstants.NINE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.NINE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COINS_ATTRIBUTE, (objArray[PlanMgmtConstants.TEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COPAY_VALUE, (objArray[PlanMgmtConstants.ELEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ELEVEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COPAY_ATTRIBUTE, (objArray[PlanMgmtConstants.TWELVE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWELVE].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COINSVAL, (objArray[PlanMgmtConstants.THIRTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.THIRTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COINS_ATTRIBUTE, (objArray[PlanMgmtConstants.FOURTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOURTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.SUBTONETDEDUCT, (objArray[PlanMgmtConstants.FIFTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FIFTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.SUBTONONNETDEDUCT, (objArray[PlanMgmtConstants.SIXTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SIXTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T1_DISPLAY, (objArray[PlanMgmtConstants.NINTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.NINTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T2_DISPLAY, (objArray[PlanMgmtConstants.TWENTY] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTY].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_DISPLAY, (objArray[PlanMgmtConstants.TWENTYONE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYONE].toString());
				//benefitAttrib.put(PlanMgmtConstants.LIMITEXCEPDISPLAY, (objArray[22] == null) ? "" : objArray[22].toString());
				benefitAttrib.put(PlanMgmtConstants.LIMITEXCEPDISPLAY, limitExcepDisplay);
				benefitAttrib.put(PlanMgmtConstants.ISCOVERED, (objArray[PlanMgmtConstants.TWENTYTHREE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYTHREE].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORKTILEDISPLAY, (objArray[PlanMgmtConstants.TWENTYFOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYFOUR].toString());
				//benefitAttrib.put(PlanMgmtConstants.EXPLANATION, (objArray[25] == null) ? "" : objArray[25].toString());
				benefitAttrib.put(PlanMgmtConstants.EXPLANATION, explanation);
				
				if(benefitDataList.get(rsPlanHealthId) == null){
					benefitDataList.put(rsPlanHealthId,new HashMap<String, Map<String, String>>() );
				}
				benefitDataList.get(rsPlanHealthId).put(benefitName, benefitAttrib);
	        }   
	        objArray = null;
        } catch (Exception e) {
        	LOGGER.error("Got an exception to execute getOptimizedHealthPlanBenefits()! Exception ==> " , e );           
        }finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return benefitDataList;
	}
	
	public Map<Integer, Map<String, Map<String, String>>> getOptimizedHealthPlanBenefitsNew(List<Integer> planIds, String effectiveDate) {
		// pull require health plan benefits only	
		//String[] healthBenefitListArr = GhixConstants.helathBenefitList;				
		//String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		
		long t1 = TimeShifterUtil.currentTimeMillis();
		// if state is exchange is WA, then pull WA specific benefits 
		//	if(PlanMgmtConstants.STATE_CODE_WA.equalsIgnoreCase(stateCode)){
		String[] healthBenefitListArr = helathBenefitListForWAState;
		//	}
		
		Map<Integer, Map<String, Map<String, String>>> benefitDataList = new HashMap<Integer, Map<String, Map<String, String>>>();
		ArrayList<String> healthBenefitList = new ArrayList<String>(healthBenefitListArr.length);
		EntityManager entityManager = null;
		long p1 = 0;
		
		try {
			entityManager = emf.createEntityManager();
			String buildquery = PlanCostAndBenefitQueryBuilder.getHealthPlanBenefitsQuery();
			for(int i = 0;i < healthBenefitListArr.length; i++){
				healthBenefitList.add(healthBenefitListArr[i]);
			}
			HashMap<String, Object> parameterMap = new HashMap<String, Object>(3);
			parameterMap.put("param_planHealthIdListInput", planIds);
			parameterMap.put("param_helathBenefitNameInputStr", healthBenefitList);
			//parameterMap.put("param_effectiveDate", effectiveDate);
			
			
			Query query = entityManager.createQuery(buildquery.toString());
			query = PlanMgmtUtil.setQueryParameters(query, parameterMap);
			
			long q1 = TimeShifterUtil.currentTimeMillis();
			List<?> planHealthList = query.getResultList();
			long q2 = TimeShifterUtil.currentTimeMillis();
			
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("SQL = " + buildquery.toString());
				LOGGER.debug("time taken for executing benefits sql ==> " + (q2-q1));
			}
			 
		     p1 = TimeShifterUtil.currentTimeMillis();
			 
			Iterator<?> rsIterator = planHealthList.iterator();
			Object[] objArray = null;
			Map<String, String> benefitAttrib = null;
			String benefitName = PlanMgmtConstants.EMPTY_STRING;
			Integer rsPlanHealthId = 0;
			String explanation =  PlanMgmtConstants.EMPTY_STRING;
			String limitExcepDisplay =  PlanMgmtConstants.EMPTY_STRING;
			
			while (rsIterator.hasNext()) {
	        	objArray = (Object[]) rsIterator.next();
	        	benefitName = objArray[PlanMgmtConstants.SEVENTEEN].toString();
				rsPlanHealthId = (Integer)objArray[PlanMgmtConstants.EIGHTEEN];
				benefitAttrib = new HashMap<String, String>();		
				limitExcepDisplay =  (objArray[PlanMgmtConstants.TWENTYTWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYTWO].toString().trim();
				explanation =  (objArray[PlanMgmtConstants.TWENTYFIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYFIVE].toString().trim();
				
				//HIX-91011 Revert HIX-89636 changes
				//HIX-89636 :: if exchange type is PHIX and explanation is not empty, append EXPLANATION with LIMIT_EXCEP_DISPLAY
				/*if(GhixConstants.PHIX.equalsIgnoreCase(exchangeType) && StringUtils.isNotBlank(explanation)){
					 if(StringUtils.isBlank(limitExcepDisplay)){
						 limitExcepDisplay += explanation;
					 }else{
						 limitExcepDisplay += "<BR>" + explanation;
					 }
				}*/
				
				benefitAttrib.put(PlanMgmtConstants.NETWORK_LIMIT, (objArray[PlanMgmtConstants.ZERO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ZERO].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_LIMIT_ATTRIBUTE, (objArray[PlanMgmtConstants.ONE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ONE].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_EXEC, (objArray[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWO].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COPAY_VALUE, (objArray[PlanMgmtConstants.THREE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.THREE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COPAY_ATTRIBUTE, (objArray[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOUR].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COINSVAL, (objArray[PlanMgmtConstants.FIVE] == null) ? PlanMgmtConstants.EMPTY_STRING: objArray[PlanMgmtConstants.FIVE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COINS_ATTRIBUTE, (objArray[PlanMgmtConstants.SIX] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SIX].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COPAY_VALUE, (objArray[PlanMgmtConstants.SEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SEVEN].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COPAY_ATTRIBUTE, (objArray[PlanMgmtConstants.EIGHT] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.EIGHT].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COINSVAL, (objArray[PlanMgmtConstants.NINE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.NINE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COINS_ATTRIBUTE, (objArray[PlanMgmtConstants.TEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COPAY_VALUE, (objArray[PlanMgmtConstants.ELEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ELEVEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COPAY_ATTRIBUTE, (objArray[PlanMgmtConstants.TWELVE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWELVE].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COINSVAL, (objArray[PlanMgmtConstants.THIRTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.THIRTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COINS_ATTRIBUTE, (objArray[PlanMgmtConstants.FOURTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOURTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.SUBTONETDEDUCT, (objArray[PlanMgmtConstants.FIFTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FIFTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.SUBTONONNETDEDUCT, (objArray[PlanMgmtConstants.SIXTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SIXTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T1_DISPLAY, (objArray[PlanMgmtConstants.NINTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.NINTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T2_DISPLAY, (objArray[PlanMgmtConstants.TWENTY] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTY].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_DISPLAY, (objArray[PlanMgmtConstants.TWENTYONE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYONE].toString());
				//benefitAttrib.put(PlanMgmtConstants.LIMITEXCEPDISPLAY, (objArray[22] == null) ? "" : objArray[22].toString());
				benefitAttrib.put(PlanMgmtConstants.LIMITEXCEPDISPLAY, limitExcepDisplay);
				benefitAttrib.put(PlanMgmtConstants.ISCOVERED, (objArray[PlanMgmtConstants.TWENTYTHREE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYTHREE].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORKTILEDISPLAY, (objArray[PlanMgmtConstants.TWENTYFOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYFOUR].toString());
				//benefitAttrib.put(PlanMgmtConstants.EXPLANATION, (objArray[25] == null) ? "" : objArray[25].toString());
				benefitAttrib.put(PlanMgmtConstants.EXPLANATION, explanation);
				
				if(benefitDataList.get(rsPlanHealthId) == null){
					benefitDataList.put(rsPlanHealthId,new HashMap<String, Map<String, String>>() );
				}
				benefitDataList.get(rsPlanHealthId).put(benefitName, benefitAttrib);
	        }   
	        objArray = null;
        } catch (Exception e) {
        	LOGGER.error("Got an exception to execute getOptimizedHealthPlanBenefits()! Exception ==> " , e );           
        }finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		long p2 = TimeShifterUtil.currentTimeMillis();
		long t2 = TimeShifterUtil.currentTimeMillis();
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("time taken for health benefits api processing the results " + (p2-p1));
			LOGGER.debug(" total time taken for health benefits api " + (t2-t1));
		}
		 
		return benefitDataList;
	}
		
		
	
	public Map<Integer, Map<String, Map<String, String>>> getOptimizedHealthPlanCosts(List<Integer> planHealthIdList) {
		Map<Integer, Map<String, Map<String, String>>> costDataList = new HashMap<Integer, Map<String, Map<String, String>>>();	
		EntityManager entityManager = null;
		
		long t1 = TimeShifterUtil.currentTimeMillis();
		long p1 = 0;
		
	    try {            
	    	entityManager = emf.createEntityManager();
	 		String queryStr = PlanCostAndBenefitQueryBuilder.getHealthPlanCostsQuery();
    		if(LOGGER.isDebugEnabled()){
    			LOGGER.debug("SQL :: " + queryStr);
    		}
    		Query query = entityManager.createQuery(queryStr);	
    		query.setParameter("param_planHealthIdInputList", planHealthIdList);
    		
    		long q1 = TimeShifterUtil.currentTimeMillis();
    		List<?> healthCostList = query.getResultList();
    		long q2 = TimeShifterUtil.currentTimeMillis();
    		
    		LOGGER.info("time taken for executing the cost sql " + (q2-q1));
    		Map<String, String> costAttrib = null;
    		
    		p1 = TimeShifterUtil.currentTimeMillis();
    		Iterator<?> rsIterator = healthCostList.iterator();
    		Object[] objArray = null;
	        while (rsIterator.hasNext()) {	
	        	objArray = (Object[]) rsIterator.next();
            	costAttrib = new HashMap<String, String>();
            	String name = objArray[10].toString();
				int rsPlanHealthId = (int)objArray[11]; // planHealth id
				costAttrib.put("inNetworkInd", (objArray[0] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[0].toString()));
				costAttrib.put("inNetworkFly", (objArray[1] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[1].toString()));		
				costAttrib.put("inNetworkTier2Ind", (objArray[2] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[2].toString()));
				costAttrib.put("inNetworkTier2Fly", (objArray[3] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[3].toString()));
				costAttrib.put("outNetworkInd", (objArray[4] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[4].toString()));
				costAttrib.put("outNetworkFly", (objArray[5] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[5].toString()));
				costAttrib.put("combinedInOutNetworkInd", (objArray[6] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[6].toString()));
				costAttrib.put("combinedInOutNetworkFly", (objArray[7] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[7].toString()));
				costAttrib.put("combinedDefCoinsNetworkTier1", (objArray[8] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[8].toString()));
				costAttrib.put("combinedDefCoinsNetworkTier2", (objArray[9] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[9].toString()));				
				costAttrib.put("limitExcepDisplay", (objArray[12] == null) ? "" : objArray[12].toString());			
				costAttrib.put("inNetworkFlyPerPerson", (objArray[13] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[13].toString()));
				costAttrib.put("inNetworkTier2FlyPerPerson", (objArray[14] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[14].toString()));
				costAttrib.put("outNetworkFlyPerPerson", (objArray[15] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[15].toString()));
				costAttrib.put("combinedInOutNetworkFlyPerPerson", (objArray[16] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[16].toString()));

				if(costDataList.get(rsPlanHealthId) == null){
					costDataList.put(rsPlanHealthId, new HashMap<String, Map<String,String>>());
				}
				costDataList.get(rsPlanHealthId).put(name, costAttrib);
            }       
	        objArray = null;
        } catch (Exception e) {
        	LOGGER.error("Got an exception to execute getOptimizedHealthPlanCosts()! Exception ==> " , e );           
        }finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}	
	    long p2 = TimeShifterUtil.currentTimeMillis();
	    long p3 = p2-p1;
	    LOGGER.info("time taken for cost api " + p3);
	    
	    
	    long t2 = TimeShifterUtil.currentTimeMillis();
	    long t3 = t2-t1;
	    LOGGER.info("time taken for cost api " + t3);
		return costDataList;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Map<String, Map<String, Map<String, String>>> getOptimizedHealthPlanCosts(Map<Integer,PlanRateBenefit> planBenefitsMap) {
		
	
		Map<String, Map<String, Map<String, String>>> costDataList = new HashMap<String, Map<String, Map<String, String>>>();			
	    Set<Integer> planHealthIdInputList = planBenefitsMap.keySet();
	    EntityManager entityManager = null;
		
	    try {            
	    		entityManager = emf.createEntityManager();
	    		String queryStr = PlanCostAndBenefitQueryBuilder.getHealthPlanCostsQuery();
						    		
	    		if(LOGGER.isDebugEnabled()){
	    			LOGGER.debug("SQL :: " + queryStr);
	    		}
	    		Query query = entityManager.createQuery(queryStr);	
	    		query.setParameter("param_planHealthIdInputList", planHealthIdInputList);
    		
	    		List<?> healthCostList = query.getResultList();
	    		Map<String, String> costAttrib = null;
	    		Iterator<?> rsIterator = healthCostList.iterator();
	    		PlanRateBenefit planRateBenefit = null;
	    		Object[] objArray = null;
		        while (rsIterator.hasNext()) {	
		        	objArray = (Object[]) rsIterator.next();
	            	costAttrib = new HashMap<String, String>();
	            	String name = objArray[10].toString();
					int rsPlanHealthId = (int)objArray[11]; // planHealth id
					planRateBenefit = planBenefitsMap.get(rsPlanHealthId);
					if(planRateBenefit == null){
						LOGGER.error("Un expected error: failed to get the plan rate benefit structure from input");
						continue;
					}
					costAttrib.put("inNetworkInd", (objArray[0] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[0].toString()));
					costAttrib.put("inNetworkFly", (objArray[1] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[1].toString()));		
					costAttrib.put("inNetworkTier2Ind", (objArray[2] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[2].toString()));
					costAttrib.put("inNetworkTier2Fly", (objArray[3] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[3].toString()));
					costAttrib.put("outNetworkInd", (objArray[4] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[4].toString()));
					costAttrib.put("outNetworkFly", (objArray[5] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[5].toString()));
					costAttrib.put("combinedInOutNetworkInd", (objArray[6] == null) ? "" :PlanMgmtUtil.removeDecimal(objArray[6].toString()));
					costAttrib.put("combinedInOutNetworkFly", (objArray[7] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[7].toString()));
					costAttrib.put("combinedDefCoinsNetworkTier1", (objArray[8] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[8].toString()));
					costAttrib.put("combinedDefCoinsNetworkTier2", (objArray[9] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[9].toString()));				
					costAttrib.put("limitExcepDisplay", (objArray[12] == null) ? "" : objArray[12].toString());
					costAttrib.put("inNetworkFlyPerPerson", (objArray[13] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[13].toString()));
					costAttrib.put("inNetworkTier2FlyPerPerson", (objArray[14] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[14].toString()));
					costAttrib.put("outNetworkFlyPerPerson", (objArray[15] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[15].toString()));
					costAttrib.put("combinedInOutNetworkFlyPerPerson", (objArray[16] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[16].toString()));
					planRateBenefit.getPlanCosts().put(name, costAttrib);
	            }       
		        objArray = null;
	        } catch (Exception e) {
	        	LOGGER.error("Got an exception to execute getOptimizedHealthPlanCosts()! Exception ==> " , e );           
	        }finally{
				 // close the entity manager
				 if(entityManager !=null && entityManager.isOpen()){
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
			}
	    
		return costDataList;
	}
	
	private Query setQueryParameters(Query query, HashMap<String, Object> parameterMap){
		try{
			Set<Entry<String, Object>> parameterSet = parameterMap.entrySet();
			Iterator<Entry<String, Object>> parameterCursor = parameterSet.iterator();
			Entry<String, Object> parameterEntry = null;
			while (parameterCursor.hasNext()) {
				parameterEntry = parameterCursor.next();
				query.setParameter(parameterEntry.getKey(),parameterEntry.getValue());
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in setQueryParameters: ", ex);
		}
		return query;
	}
	
	@Transactional(readOnly = true)
	public Map<Integer, Map<String, Map<String, String>>> getOptimizedDentalPlanBenefits(List<Integer> planIdsLit, String effectiveDate) {
		// pull require dental benefits only not all all benefits
		String[] dentalBenefitListArr = GhixConstants.dentalBenefitList;
		ArrayList<String> dentalBenefitNameInputList = new ArrayList<String>(dentalBenefitListArr.length);
		
		for (String dentalBenefit : dentalBenefitListArr) {
			dentalBenefitNameInputList.add(dentalBenefit);
		}
		HashMap<String, Object> paramList = new HashMap<String, Object>(3);
		paramList.put("param_planDentalListInput", planIdsLit);
		paramList.put("param_dentalBenefitNameInputList", dentalBenefitNameInputList);
		paramList.put("param_effectiveDate", effectiveDate);

		List<?> rs = null;
		Map<Integer, Map<String, Map<String, String>>> benefitDataList = new HashMap<Integer, Map<String, Map<String, String>>>();
		
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
	        String queryStr = PlanCostAndBenefitQueryBuilder.getDentalPlanBenefitsQuery();
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("INPUT buildquery :: " + queryStr);
			}
			Query query = entityManager.createQuery(queryStr);
			this.setQueryParameters(query, paramList);
    		rs = query.getResultList();
    		Iterator<?> rsIterator = rs.iterator();
    		String benefitName = PlanMgmtConstants.EMPTY_STRING;	
    		Integer rsPlanDentalId  = -1;
    		Map<String, String> benefitAttrib = null;
    		Object[] objArray = null;
    		String explanation = PlanMgmtConstants.EMPTY_STRING;
    		String limitExcepDisplay = PlanMgmtConstants.EMPTY_STRING;
    		
            while ( rsIterator.hasNext() ) {
            	objArray = (Object[]) rsIterator.next();
            	benefitName = objArray[PlanMgmtConstants.SEVENTEEN].toString();
            	rsPlanDentalId = (Integer)objArray[PlanMgmtConstants.EIGHTEEN];//Abhai
            	limitExcepDisplay =  (objArray[PlanMgmtConstants.TWENTYTWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYTWO].toString().trim();
				explanation =  (objArray[PlanMgmtConstants.TWENTYFIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYFIVE].toString().trim();
				
				//HIX-91011 Revert HIX-89636 changes
				//HIX-89636 :: if exchange type is PHIX and explanation is not empty, append EXPLANATION with LIMIT_EXCEP_DISPLAY
				/*if(GhixConstants.PHIX.equalsIgnoreCase(exchangeType) && StringUtils.isNotBlank(explanation)){
					 if(StringUtils.isBlank(limitExcepDisplay)){
						 limitExcepDisplay += explanation;
					 }else{
						 limitExcepDisplay += "<BR>" + explanation;
					 }
				}*/
            	
            	benefitAttrib = new HashMap<String, String>();	
            	benefitAttrib.put(PlanMgmtConstants.NETWORK_LIMIT, (objArray[PlanMgmtConstants.ZERO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ZERO].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_LIMIT_ATTRIBUTE, (objArray[PlanMgmtConstants.ONE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ONE].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_EXEC, (objArray[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWO].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COPAY_VALUE, (objArray[PlanMgmtConstants.THREE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.THREE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COPAY_ATTRIBUTE, (objArray[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOUR].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COINSVAL, (objArray[PlanMgmtConstants.FIVE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FIVE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COINS_ATTRIBUTE, (objArray[PlanMgmtConstants.SIX] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SIX].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COPAY_VALUE, (objArray[PlanMgmtConstants.SEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SEVEN].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COPAY_ATTRIBUTE, (objArray[PlanMgmtConstants.EIGHT] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.EIGHT].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COINSVAL, (objArray[PlanMgmtConstants.NINE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.NINE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COINS_ATTRIBUTE, (objArray[PlanMgmtConstants.TEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COPAY_VALUE, (objArray[PlanMgmtConstants.ELEVEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.ELEVEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COPAY_ATTRIBUTE, (objArray[PlanMgmtConstants.TWELVE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWELVE].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COINSVAL, (objArray[PlanMgmtConstants.THIRTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.THIRTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COINS_ATTRIBUTE, (objArray[PlanMgmtConstants.FOURTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOURTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.SUBTONETDEDUCT, (objArray[PlanMgmtConstants.FIFTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FIFTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.SUBTONONNETDEDUCT, (objArray[PlanMgmtConstants.SIXTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.SIXTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T1_DISPLAY, (objArray[PlanMgmtConstants.NINTEEN] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.NINTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T2_DISPLAY, (objArray[PlanMgmtConstants.TWENTY] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTY].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_DISPLAY, (objArray[PlanMgmtConstants.TWENTYONE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYONE].toString());
				//benefitAttrib.put(PlanMgmtConstants.LIMITEXCEPDISPLAY, (objArray[22] == null) ? "" : objArray[22].toString());
				benefitAttrib.put(PlanMgmtConstants.LIMITEXCEPDISPLAY, limitExcepDisplay);
				benefitAttrib.put(PlanMgmtConstants.ISCOVERED, (objArray[PlanMgmtConstants.TWENTYTHREE] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYTHREE].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORKTILEDISPLAY, (objArray[PlanMgmtConstants.TWENTYFOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWENTYFOUR].toString());
				//benefitAttrib.put(PlanMgmtConstants.EXPLANATION, (objArray[25] == null) ? "" : objArray[25].toString());
				benefitAttrib.put(PlanMgmtConstants.EXPLANATION, explanation);
				
				if(benefitDataList.get(rsPlanDentalId) == null){
					benefitDataList.put(rsPlanDentalId,new HashMap<String, Map<String, String>>() );
				}
				benefitDataList.get(rsPlanDentalId).put(benefitName, benefitAttrib);
				
				benefitAttrib = null;
            }    
        } catch (Exception e) {
        	LOGGER.error("Got an exception to execute getOptimizedDentalPlanBenefits()! Exception ==> " , e );           
        }finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
        return benefitDataList;
	}
	
	
	
	@Transactional(readOnly = true)
	public Map<Integer, Map<String, Map<String, String>>> getOptimizedDentalPlanCosts(List<Integer> planDentalIdList, boolean sendInNetworkOnly) {
		Map<Integer, Map<String, Map<String, String>>> costDataList = new HashMap<Integer, Map<String, Map<String, String>>>();		
		EntityManager entityManager = null;
		
	    try {            
	    	entityManager = emf.createEntityManager();
    		String queryStr = PlanMgmtConstants.EMPTY_STRING;
    		if(sendInNetworkOnly){
    			queryStr = PlanCostAndBenefitQueryBuilder.getDentalPlanCostsNetworkOnlyQuery();
    		}else{
    			queryStr = PlanCostAndBenefitQueryBuilder.getDentalPlanCostsQuery();
    		}
    		
    		if(LOGGER.isDebugEnabled()){
    			LOGGER.debug("SQL :: " + queryStr);
    		}
    		Query query = entityManager.createQuery(queryStr);	
    		query.setParameter("param_planDentalIdInputList", planDentalIdList);
		
    		List<?> dentalCostList = query.getResultList();
    		Map<String, String> costAttrib = null;
    		Iterator<?> rsIterator = dentalCostList.iterator();
    		Object[] objArray = null;
	        while (rsIterator.hasNext()) {	
	        	objArray = (Object[]) rsIterator.next();
            	costAttrib = new HashMap<String, String>();
            	String name = objArray[2].toString();
				int rsPlanDentalId = (int)objArray[3];// Plan Id
				
				costAttrib.put("inNetworkInd", (objArray[0] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[0].toString()));
				costAttrib.put("inNetworkFly", (objArray[1] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[1].toString()));		
				if(!sendInNetworkOnly){
					costAttrib.put("inNetworkTier2Ind", (objArray[4] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[4].toString()));
					costAttrib.put("inNetworkTier2Fly", (objArray[5] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[5].toString()));
					costAttrib.put("outNetworkInd", (objArray[6] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[6].toString()));
					costAttrib.put("outNetworkFly", (objArray[7] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[7].toString()));
					costAttrib.put("combinedInOutNetworkInd", (objArray[8] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[8].toString()));
					costAttrib.put("combinedInOutNetworkFly", (objArray[9] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[9].toString()));
					costAttrib.put("combinedDefCoinsNetworkTier1", (objArray[10] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[10].toString()));
					costAttrib.put("combinedDefCoinsNetworkTier2", (objArray[11] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[11].toString()));				
					costAttrib.put("limitExcepDisplay", (objArray[12] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[12].toString()));
					costAttrib.put("inNetworkFlyPerPerson", (objArray[13] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[13].toString()));
					costAttrib.put("inNetworkTier2FlyPerPerson", (objArray[14] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[14].toString()));
					costAttrib.put("outNetworkFlyPerPerson", (objArray[15] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[15].toString()));
					costAttrib.put("combinedInOutNetworkFlyPerPerson", (objArray[16] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[16].toString()));
				}
				else {
					costAttrib.put("inNetworkFlyPerPerson", (objArray[4] == null) ? "" : PlanMgmtUtil.removeDecimal(objArray[4].toString()));
				}
				if(costDataList.get(rsPlanDentalId) == null){
					costDataList.put(rsPlanDentalId, new HashMap<String,  Map<String, String>>() );
				}
				costDataList.get(rsPlanDentalId).put(name, costAttrib);		
            }       
	        objArray = null;
    	} catch (Exception e) {
    		LOGGER.error("Got an exception to execute getOptimizedDentalPlanCosts()! Exception ==> " , e );           
    	}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		} 
		return costDataList;
	}
	
	// PERF-92 create new method to pull limited health plan benefits
	@Transactional(readOnly = true)
	public Map<Integer, Map<String, Map<String, String>>> getHealthPlanBenefitsForTeaserPlan(List<Integer> planHealthIdList, String effectiveDate) {
		// pull require health plan benefits only	
		String[] healthBenefitListArr = {"PRIMARY_VISIT","GENERIC"};			
		Map<Integer, Map<String, Map<String, String>>> benefitDataList = new HashMap<Integer, Map<String, Map<String, String>>>();
		EntityManager entityManager = null;
		
		try {
			entityManager = emf.createEntityManager();
            ArrayList<String> helathBenefitNameInputList = new ArrayList<String>(healthBenefitListArr.length);
    		for(String helathBenefitName :  healthBenefitListArr){
    			helathBenefitNameInputList.add(helathBenefitName);
    		}
    		String queryStr = PlanCostAndBenefitQueryBuilder.getHealthPlanBenefitsForTeaserPlanQuery();
    		if(LOGGER.isDebugEnabled()){
    			LOGGER.debug("SQL :: " + queryStr);
    		}
    		Query query = entityManager.createQuery(queryStr);
    		query.setParameter("param_planHealthIdInputList", planHealthIdList);
    		query.setParameter("param_helathBenefitNameInputList", helathBenefitNameInputList);
    		query.setParameter("param_effectiveDate", effectiveDate);
    		List<?> rsList = query.getResultList();	    		
    		Iterator<?> rs = rsList.iterator();
    		Object[] objArray = null;
    		
            while ( rs.hasNext() ) {
            	objArray = (Object[]) rs.next();
				String benefitName = (objArray[0] == null) ? "" : objArray[0].toString();
				int rsPlanHealthId = (int)objArray[1];
				Map<String, String> benefitAttrib = new HashMap<String, String>();			
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T1_DISPLAY, ((objArray[2] == null) ? "" : objArray[2].toString()));
				if(benefitDataList.get(rsPlanHealthId) == null){
					benefitDataList.put(rsPlanHealthId,new HashMap<String, Map<String, String>>() );
				}
				benefitDataList.get(rsPlanHealthId).put(benefitName, benefitAttrib);
            }    
        } catch (Exception e) {
        	LOGGER.error("Got an exception to execute getHealthPlanBenefitsForTeaserPlan()! Exception ==> " , e );           
        }finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return benefitDataList;
	}
	
	@Override
	@Transactional(readOnly = true)
	public Map<String, Map<String, String>> getHealthPlanCosts(int healthPlanId) {
		List<Object[]> planCostList = iPlanHealthCostRepository.getPlanHealthCostByPlanId(healthPlanId);
		Map<String, Map<String, String>> costMap = new HashMap<String, Map<String, String>>();

		for (Object[] planCost : planCostList) {
			Map<String, String> costAttrib = new HashMap<String, String>();
			Map<String, Map<String, String>> costData = new HashMap<String, Map<String, String>>();
			costAttrib.put("inNetworkInd", (planCost[PlanMgmtConstants.ZERO] == null) ? "" :  planCost[PlanMgmtConstants.ZERO].toString().substring(0, planCost[PlanMgmtConstants.ZERO].toString().indexOf(".")) );
			costAttrib.put("inNetworkFly", (planCost[PlanMgmtConstants.ONE] == null) ? "" : planCost[PlanMgmtConstants.ONE].toString().substring(0, planCost[PlanMgmtConstants.ONE].toString().indexOf(".")));
			costAttrib.put("inNetworkTier2Ind", (planCost[PlanMgmtConstants.TWO] == null) ? "" : planCost[PlanMgmtConstants.TWO].toString().substring(0, planCost[PlanMgmtConstants.TWO].toString().indexOf(".")));
			costAttrib.put("inNetworkTier2Fly", (planCost[PlanMgmtConstants.THREE] == null) ? "" : planCost[PlanMgmtConstants.THREE].toString().substring(0, planCost[PlanMgmtConstants.THREE].toString().indexOf(".")));
			costAttrib.put("outNetworkInd", (planCost[PlanMgmtConstants.FOUR] == null) ? "" : planCost[PlanMgmtConstants.FOUR].toString().substring(0, planCost[PlanMgmtConstants.FOUR].toString().indexOf(".")));
			costAttrib.put("outNetworkFly", (planCost[PlanMgmtConstants.FIVE] == null) ? "" : planCost[PlanMgmtConstants.FIVE].toString().substring(0, planCost[PlanMgmtConstants.FIVE].toString().indexOf(".")));
			costAttrib.put("combinedInOutNetworkInd", (planCost[PlanMgmtConstants.SIX] == null) ? "" : planCost[PlanMgmtConstants.SIX].toString().substring(0, planCost[PlanMgmtConstants.SIX].toString().indexOf(".")));
			costAttrib.put("combinedInOutNetworkFly", (planCost[PlanMgmtConstants.SEVEN] == null) ? "" : planCost[PlanMgmtConstants.SEVEN].toString().substring(0, planCost[PlanMgmtConstants.SEVEN].toString().indexOf(".")));
			costAttrib.put("combinedDefCoinsNetworkTier1", (planCost[PlanMgmtConstants.EIGHT] == null) ? "" : planCost[PlanMgmtConstants.EIGHT].toString().substring(0, planCost[PlanMgmtConstants.EIGHT].toString().indexOf(".")));
			costAttrib.put("combinedDefCoinsNetworkTier2", (planCost[PlanMgmtConstants.NINE] == null) ? "" : planCost[PlanMgmtConstants.NINE].toString().substring(0, planCost[PlanMgmtConstants.NINE].toString().indexOf(".")));
			costAttrib.put("limitExcepDisplay", (planCost[PlanMgmtConstants.ELEVEN] == null) ? "" : planCost[PlanMgmtConstants.ELEVEN].toString());
			costData.put(planCost[PlanMgmtConstants.TEN].toString(), costAttrib);
			costMap.putAll(costData);
		}

		return costMap;
	}
		
	@Override
	@Transactional(readOnly = true)
	public Map<String, Map<String, String>> getDentalPlanCosts(int planDentalId) {
		List<Object[]> planCostList = iPlanDentalCostRepository.getPlanDentalCostByPlanId(planDentalId);
		Map<String, Map<String, String>> costMap = new HashMap<String, Map<String, String>>();

		for (Object[] planCost : planCostList) {
			Map<String, String> costAttrib = new HashMap<String, String>();
			Map<String, Map<String, String>> costData = new HashMap<String, Map<String, String>>();
			costAttrib.put("inNetworkInd", (planCost[PlanMgmtConstants.ZERO] == null) ? "" : planCost[PlanMgmtConstants.ZERO].toString().substring(0, planCost[PlanMgmtConstants.ZERO].toString().indexOf(".")));
			costAttrib.put("inNetworkFly", (planCost[PlanMgmtConstants.ONE] == null) ? "" : planCost[PlanMgmtConstants.ONE].toString().substring(0, planCost[PlanMgmtConstants.ONE].toString().indexOf(".")));
			costAttrib.put("inNetworkTier2Ind", (planCost[PlanMgmtConstants.TWO] == null) ? "" : planCost[PlanMgmtConstants.TWO].toString().substring(0, planCost[PlanMgmtConstants.TWO].toString().indexOf(".")));
			costAttrib.put("inNetworkTier2Fly", (planCost[PlanMgmtConstants.THREE] == null) ? "" : planCost[PlanMgmtConstants.THREE].toString().substring(0, planCost[PlanMgmtConstants.THREE].toString().indexOf(".")));
			costAttrib.put("outNetworkInd", (planCost[PlanMgmtConstants.FOUR] == null) ? "" : planCost[PlanMgmtConstants.FOUR].toString().substring(0, planCost[PlanMgmtConstants.FOUR].toString().indexOf(".")));
			costAttrib.put("outNetworkFly", (planCost[PlanMgmtConstants.FIVE] == null) ? "" : planCost[PlanMgmtConstants.FIVE].toString().substring(0, planCost[PlanMgmtConstants.FIVE].toString().indexOf(".")));
			costAttrib.put("combinedInOutNetworkInd", (planCost[PlanMgmtConstants.SIX] == null) ? "" : planCost[PlanMgmtConstants.SIX].toString().substring(0, planCost[PlanMgmtConstants.SIX].toString().indexOf(".")));
			costAttrib.put("combinedInOutNetworkFly", (planCost[PlanMgmtConstants.SEVEN] == null) ? "" : planCost[PlanMgmtConstants.SEVEN].toString().substring(0, planCost[PlanMgmtConstants.SEVEN].toString().indexOf(".")));
			costAttrib.put("combinedDefCoinsNetworkTier1", (planCost[PlanMgmtConstants.EIGHT] == null) ? "" : planCost[PlanMgmtConstants.EIGHT].toString().substring(0, planCost[PlanMgmtConstants.EIGHT].toString().indexOf(".")));
			costAttrib.put("combinedDefCoinsNetworkTier2", (planCost[PlanMgmtConstants.NINE] == null) ? "" : planCost[PlanMgmtConstants.NINE].toString().substring(0, planCost[PlanMgmtConstants.NINE].toString().indexOf(".")));
			costAttrib.put("limitExcepDisplay", (planCost[PlanMgmtConstants.ELEVEN] == null) ? "" : planCost[PlanMgmtConstants.ELEVEN].toString());
			costData.put(planCost[PlanMgmtConstants.TEN].toString(), costAttrib);
			costMap.putAll(costData);
		}

		return costMap;
	}
	
		
	// fetch dental plan benefits
	@Override
	@Transactional(readOnly = true)
	public Map<String, Map<String, String>> getDentalPlanBenefits(int planDentalId, String effectiveDate) {
		List<Object[]> planbenefit = iPlanDentalBenefitRepository.getPlanDentalBenefit(planDentalId, effectiveDate);
		Map<String, Map<String, String>> benefitMap = new HashMap<String, Map<String, String>>();
		String[] dentalBenefitList = GhixConstants.dentalBenefitList;

		for (Object[] benefitFileds : planbenefit) {
			String benefitName = benefitFileds[PlanMgmtConstants.FIFTEEN].toString();
			if (Arrays.asList(dentalBenefitList).contains(benefitName)) {
				Map<String, String> benefitAttrib = new HashMap<String, String>();
				Map<String, Map<String, String>> benefitData = new HashMap<String, Map<String, String>>();

				benefitAttrib.put(PlanMgmtConstants.NETWORK_LIMIT, (benefitFileds[PlanMgmtConstants.ZERO] == null) ? "" : benefitFileds[PlanMgmtConstants.ZERO].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_LIMIT_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.ONE] == null) ? "" : benefitFileds[PlanMgmtConstants.ONE].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_EXEC, (benefitFileds[PlanMgmtConstants.TWO] == null) ? "" : benefitFileds[PlanMgmtConstants.TWO].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COPAY_VALUE, (benefitFileds[PlanMgmtConstants.THREE] == null) ? "" : benefitFileds[PlanMgmtConstants.THREE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COPAY_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.FOUR] == null) ? "" : benefitFileds[PlanMgmtConstants.FOUR].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COINSVAL, (benefitFileds[PlanMgmtConstants.FIVE] == null) ? "" : benefitFileds[PlanMgmtConstants.FIVE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COINS_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.SIX] == null) ? "" : benefitFileds[PlanMgmtConstants.SIX].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COPAY_VALUE, (benefitFileds[PlanMgmtConstants.SEVEN] == null) ? "" : benefitFileds[PlanMgmtConstants.SEVEN].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COPAY_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.EIGHT] == null) ? "" : benefitFileds[PlanMgmtConstants.EIGHT].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COINSVAL, (benefitFileds[PlanMgmtConstants.NINE] == null) ? "" : benefitFileds[PlanMgmtConstants.NINE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COINS_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.TEN] == null) ? "" : benefitFileds[PlanMgmtConstants.TEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COPAY_VALUE, (benefitFileds[PlanMgmtConstants.ELEVEN] == null) ? "" : benefitFileds[PlanMgmtConstants.ELEVEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COPAY_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.TWELVE] == null) ? "" : benefitFileds[PlanMgmtConstants.TWELVE].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COINSVAL, (benefitFileds[PlanMgmtConstants.THIRTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.THIRTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COINS_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.FOURTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.FOURTEEN].toString());
				
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T1_DISPLAY, (benefitFileds[PlanMgmtConstants.SIXTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.SIXTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T2_DISPLAY, (benefitFileds[PlanMgmtConstants.SEVENTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.SEVENTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_DISPLAY, (benefitFileds[PlanMgmtConstants.EIGHTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.EIGHTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORKTILEDISPLAY, (benefitFileds[PlanMgmtConstants.NINTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.NINTEEN].toString());
								
				benefitAttrib.put(PlanMgmtConstants.SUBTONETDEDUCT, (benefitFileds[PlanMgmtConstants.TWENTY] == null) ? "" : benefitFileds[PlanMgmtConstants.TWENTY].toString());
				benefitAttrib.put(PlanMgmtConstants.SUBTONONNETDEDUCT, (benefitFileds[PlanMgmtConstants.TWENTYONE] == null) ? "" : benefitFileds[PlanMgmtConstants.TWENTYONE].toString());
				benefitAttrib.put(PlanMgmtConstants.ISCOVERED, (benefitFileds[PlanMgmtConstants.TWENTYTWO] == null) ? "" : benefitFileds[PlanMgmtConstants.TWENTYTWO].toString());
				benefitAttrib.put(PlanMgmtConstants.EXPLANATION, (benefitFileds[PlanMgmtConstants.TWENTYTHREE] == null) ? "" : benefitFileds[PlanMgmtConstants.TWENTYTHREE].toString());
				benefitAttrib.put(PlanMgmtConstants.LIMITEXCEPDISPLAY, (benefitFileds[PlanMgmtConstants.TWENTYFOUR] == null) ? "" : benefitFileds[PlanMgmtConstants.TWENTYFOUR].toString());
						
				benefitData.put(benefitName, benefitAttrib);
				benefitMap.putAll(benefitData);
			}
		}
		return benefitMap;
	}
		
	// fetch health plan benefits
	@Override
	@Transactional(readOnly = true)
	public Map<String, Map<String, String>> getHealthPlanBenefits(int healthPlanId, String effectiveDate) {
		List<Object[]> planbenefit = iPlanHealthBenefitRepository.getPlanHealthBenefit(healthPlanId, effectiveDate);
		Map<String, Map<String, String>> benefitMap = new HashMap<String, Map<String, String>>();
		String[] helathBenefitList = GhixConstants.helathBenefitList;

		for (Object[] benefitFileds : planbenefit) {
			String benefitName = benefitFileds[PlanMgmtConstants.SEVENTEEN].toString();
			
			if (Arrays.asList(helathBenefitList).contains(benefitName)) {
				Map<String, String> benefitAttrib = new HashMap<String, String>();
				Map<String, Map<String, String>> benefitData = new HashMap<String, Map<String, String>>();

				benefitAttrib.put(PlanMgmtConstants.NETWORK_LIMIT, (benefitFileds[PlanMgmtConstants.ZERO] == null) ? "" : benefitFileds[PlanMgmtConstants.ZERO].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_LIMIT_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.ONE] == null) ? "" : benefitFileds[PlanMgmtConstants.ONE].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_EXEC, (benefitFileds[PlanMgmtConstants.TWO] == null) ? "" : benefitFileds[PlanMgmtConstants.TWO].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COPAY_VALUE, (benefitFileds[PlanMgmtConstants.THREE] == null) ? "" : benefitFileds[PlanMgmtConstants.THREE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COPAY_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.FOUR] == null) ? "" : benefitFileds[PlanMgmtConstants.FOUR].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COINSVAL, (benefitFileds[PlanMgmtConstants.FIVE] == null) ? "" : benefitFileds[PlanMgmtConstants.FIVE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER1_COINS_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.SIX] == null) ? "" : benefitFileds[PlanMgmtConstants.SIX].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COPAY_VALUE, (benefitFileds[PlanMgmtConstants.SEVEN] == null) ? "" : benefitFileds[PlanMgmtConstants.SEVEN].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COPAY_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.EIGHT] == null) ? "" : benefitFileds[PlanMgmtConstants.EIGHT].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COINSVAL, (benefitFileds[PlanMgmtConstants.NINE] == null) ? "" : benefitFileds[PlanMgmtConstants.NINE].toString());
				benefitAttrib.put(PlanMgmtConstants.TIER2_COINS_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.TEN] == null) ? "" : benefitFileds[PlanMgmtConstants.TEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COPAY_VALUE, (benefitFileds[PlanMgmtConstants.ELEVEN] == null) ? "" : benefitFileds[PlanMgmtConstants.ELEVEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COPAY_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.TWELVE] == null) ? "" : benefitFileds[PlanMgmtConstants.TWELVE].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COINSVAL, (benefitFileds[PlanMgmtConstants.THIRTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.THIRTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_COINS_ATTRIBUTE, (benefitFileds[PlanMgmtConstants.FOURTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.FOURTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.SUBTONETDEDUCT, (benefitFileds[PlanMgmtConstants.FIFTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.FIFTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.SUBTONONNETDEDUCT, (benefitFileds[PlanMgmtConstants.SIXTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.SIXTEEN].toString());
				
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T1_DISPLAY, (benefitFileds[PlanMgmtConstants.EIGHTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.EIGHTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORK_T2_DISPLAY, (benefitFileds[PlanMgmtConstants.NINTEEN] == null) ? "" : benefitFileds[PlanMgmtConstants.NINTEEN].toString());
				benefitAttrib.put(PlanMgmtConstants.OUTNETWORK_DISPLAY, (benefitFileds[PlanMgmtConstants.TWENTY] == null) ? "" : benefitFileds[PlanMgmtConstants.TWENTY].toString());
				benefitAttrib.put(PlanMgmtConstants.LIMITEXCEPDISPLAY, (benefitFileds[PlanMgmtConstants.TWENTYONE] == null) ? "" : benefitFileds[PlanMgmtConstants.TWENTYONE].toString());
				benefitAttrib.put(PlanMgmtConstants.NETWORKTILEDISPLAY, (benefitFileds[PlanMgmtConstants.TWENTYTWO] == null) ? "" : benefitFileds[PlanMgmtConstants.TWENTYTWO].toString());
				benefitAttrib.put(PlanMgmtConstants.ISCOVERED, (benefitFileds[PlanMgmtConstants.TWENTYTHREE] == null) ? "" : benefitFileds[PlanMgmtConstants.TWENTYTHREE].toString());
				benefitAttrib.put(PlanMgmtConstants.EXPLANATION, (benefitFileds[PlanMgmtConstants.TWENTYFOUR] == null) ? "" : benefitFileds[PlanMgmtConstants.TWENTYFOUR].toString());
				
				benefitData.put(benefitName, benefitAttrib);
				benefitMap.putAll(benefitData);
			}
		}
		return benefitMap;
	}
		
		
	@Override
	@Transactional(readOnly = true)
	public Map<String, Object> getBenefitsAndCostByPlanId ( List<Integer> planIds, String insuranceType) {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		
		if( "HEALTH".equals(insuranceType) ){
			List<PlanHealthBenefit> healthBenefitList = iPlanHealthBenefitRepository.getPlanHealthBenefitByPlanId(planIds);
			List<PlanHealthBenefitDTO> healthBenefitDTOList = new ArrayList<PlanHealthBenefitDTO>();
			for( PlanHealthBenefit pb : healthBenefitList){
				healthBenefitDTOList.add(copyToHealthBenefitDTO(pb));
			}
			List<PlanHealthCost> healthCostList = iPlanHealthCostRepository.getPlanHealthCostsByPlanId(planIds);
			List<PlanHealthCostDTO> healthCostDtoList = new ArrayList<PlanHealthCostDTO>();
			for( PlanHealthCost ph : healthCostList){
				healthCostDtoList.add(copyToHealthCostDTO(ph));
			}
			
			dataMap.put("healthBenefits", healthBenefitDTOList);
			dataMap.put("healthCosts", healthCostDtoList);
		}
		else if ( "DENTAL".equals(insuranceType) ){
			List<PlanDentalBenefit> dentalBenefitList = iPlanDentalBenefitRepository.getPlanDentalBenefitByPlanId(planIds);
			List<PlanDentalBenefitDTO> dentalBenefitDTOList = new ArrayList<PlanDentalBenefitDTO>();
			for( PlanDentalBenefit pdb : dentalBenefitList){
				dentalBenefitDTOList.add(copyToDentalBenefitDTO(pdb));
			}
			List<PlanDentalCost> dentalCostList = iPlanDentalCostRepository.getPlanDentalCostsByPlanId(planIds);
			List<PlanDentalCostDTO> healthCostDtoList = new ArrayList<PlanDentalCostDTO>();
			for( PlanDentalCost pdc : dentalCostList){
				healthCostDtoList.add(copyToDentalCostDTO(pdc));
			}
			
			dataMap.put("dentalBenefits", dentalBenefitDTOList);
			dataMap.put("dentalCosts", healthCostDtoList);
		}
		return dataMap;
	}
		
		
	private PlanDentalCostDTO copyToDentalCostDTO (PlanDentalCost pdc ){
		PlanDentalCostDTO planDentalCostDTO = new PlanDentalCostDTO();
		planDentalCostDTO.setId(pdc.getId());
		planDentalCostDTO.setPlanDentalId(pdc.getPlanDental().getId());
		planDentalCostDTO.setPlanId(pdc.getPlanDental().getPlan().getId());
		planDentalCostDTO.setName(pdc.getName());
		planDentalCostDTO.setInNetWorkInd(pdc.getInNetWorkInd());
		planDentalCostDTO.setInNetWorkFly(pdc.getInNetWorkFly());
		planDentalCostDTO.setInNetworkTier2Ind(pdc.getInNetworkTier2Ind());
		planDentalCostDTO.setInNetworkTier2Fly(pdc.getInNetworkTier2Fly());
		planDentalCostDTO.setOutNetworkInd(pdc.getOutNetworkInd());
		planDentalCostDTO.setOutNetworkFly(pdc.getOutNetworkFly());
		planDentalCostDTO.setCombinedInOutNetworkInd(pdc.getCombinedInOutNetworkInd());
		planDentalCostDTO.setCombinedInOutNetworkFly(pdc.getCombinedInOutNetworkFly());
		planDentalCostDTO.setCombDefCoinsNetworkTier1(pdc.getCombDefCoinsNetworkTier1());
		planDentalCostDTO.setCombDefCoinsNetworkTier2(pdc.getCombDefCoinsNetworkTier2());
		planDentalCostDTO.setLimitExcepDisplay(pdc.getLimitExcepDisplay());

		return planDentalCostDTO;
	}
		
		
	private PlanHealthBenefitDTO copyToHealthBenefitDTO (PlanHealthBenefit pb ){
		PlanHealthBenefitDTO planHealthBenefitDTO = new PlanHealthBenefitDTO();
		planHealthBenefitDTO.setId(pb.getId());
		planHealthBenefitDTO.setPlanHealthId(pb.getPlanHealth().getId());
		planHealthBenefitDTO.setPlanId(pb.getPlanHealth().getPlan().getId());
		planHealthBenefitDTO.setName(pb.getName());
		planHealthBenefitDTO.setNetworkLimitation(pb.getNetworkLimitation());
		planHealthBenefitDTO.setNetworkLimitationAttribute(pb.getNetworkLimitationAttribute());
		planHealthBenefitDTO.setNetworkExceptions(pb.getNetworkExceptions());
		planHealthBenefitDTO.setSubjectToInNetworkDuductible(pb.getSubjectToInNetworkDuductible());
		planHealthBenefitDTO.setExcludedFromInNetworkMoop(pb.getExcludedFromInNetworkMoop());
		planHealthBenefitDTO.setSubjectToOutNetworkDeductible(pb.getSubjectToOutNetworkDeductible());
		planHealthBenefitDTO.setExcludedFromOutOfNetworkMoop(pb.getExcludedFromOutOfNetworkMoop());
		planHealthBenefitDTO.setCombinedInAndOutOfNetwork(pb.getCombinedInAndOutOfNetwork());
		planHealthBenefitDTO.setCombinedInAndOutNetworkAttribute(pb.getCombinedInAndOutNetworkAttribute());
		planHealthBenefitDTO.setEffectiveStartDate(simpleDf.format(pb.getEffStartDate()));
		planHealthBenefitDTO.setEffectiveEndDate(simpleDf.format(pb.getEffEndDate()));
		planHealthBenefitDTO.setIsEHB(pb.getIsEHB());
		planHealthBenefitDTO.setIsCovered(pb.getIsCovered());
		planHealthBenefitDTO.setMinStay(pb.getMinStay());
		planHealthBenefitDTO.setExplanation(pb.getExplanation());
		planHealthBenefitDTO.setInNetworkIndividual(pb.getInNetworkIndividual());
		planHealthBenefitDTO.setInNetworkFamily(pb.getInNetworkFamily());
		planHealthBenefitDTO.setInNetworkTierTwoIndividual(pb.getInNetworkTierTwoIndividual());
		planHealthBenefitDTO.setInNetworkTierTwoFamily(pb.getInNetworkTierTwoFamily());
		planHealthBenefitDTO.setOutOfNetworkIndividual(pb.getOutOfNetworkIndividual());
		planHealthBenefitDTO.setOutOfNetworkFamily(pb.getOutOfNetworkFamily());
		planHealthBenefitDTO.setCombinedInOutNetworkIndividual(pb.getCombinedInOutNetworkIndividual());
		planHealthBenefitDTO.setCombinedInOutNetworkFamily(pb.getCombinedInOutNetworkFamily());
		planHealthBenefitDTO.setNetworkT1CopayVal(pb.getNetworkT1CopayVal());
		planHealthBenefitDTO.setNetworkT1CopayAttr(pb.getNetworkT1CopayAttr());
		planHealthBenefitDTO.setNetworkT1CoinsurVal(pb.getNetworkT1CoinsurVal());
		planHealthBenefitDTO.setNetworkT1CoinsurAttr(pb.getNetworkT1CoinsurAttr());
		planHealthBenefitDTO.setNetworkT2CopayVal(pb.getNetworkT2CopayVal());
		planHealthBenefitDTO.setNetworkT2CopayAttr(pb.getNetworkT2CopayAttr());
		planHealthBenefitDTO.setNetworkT2CoinsurVal(pb.getNetworkT2CoinsurVal());
		planHealthBenefitDTO.setNetworkT2CoinsurAttr(pb.getNetworkT2CoinsurAttr());
		planHealthBenefitDTO.setOutOfNetworkCopayVal(pb.getOutOfNetworkCopayVal());
		planHealthBenefitDTO.setOutOfNetworkCopayAttr(pb.getOutOfNetworkCopayAttr());
		planHealthBenefitDTO.setOutOfNetworkCoinsurVal(pb.getOutOfNetworkCoinsurVal());
		planHealthBenefitDTO.setOutOfNetworkCoinsurAttr(pb.getOutOfNetworkCoinsurAttr());
		planHealthBenefitDTO.setIsStateMandate(pb.getIsStateMandate());
		planHealthBenefitDTO.setServiceLimit(pb.getServiceLimit());
		planHealthBenefitDTO.setEhbVarianceReason(pb.getEhbVarianceReason());
		planHealthBenefitDTO.setNetworkT1display(pb.getNetworkT1display());
		planHealthBenefitDTO.setNetworkT2display(pb.getNetworkT2display());
		planHealthBenefitDTO.setOutOfNetworkDisplay(pb.getOutOfNetworkDisplay());
		planHealthBenefitDTO.setNetworkT1TileDisplay(pb.getNetworkT1TileDisplay());
		planHealthBenefitDTO.setLimitExcepDisplay(pb.getLimitExcepDisplay());
		
		return planHealthBenefitDTO;
	}
		
	private PlanDentalBenefitDTO copyToDentalBenefitDTO (PlanDentalBenefit pb ){
		PlanDentalBenefitDTO planDentalBenefitDTO = new PlanDentalBenefitDTO();
		planDentalBenefitDTO.setId(pb.getId());
		planDentalBenefitDTO.setPlanDentalId(pb.getPlanDental().getId());
		planDentalBenefitDTO.setPlanId(pb.getPlanDental().getPlan().getId());
		planDentalBenefitDTO.setName(pb.getName());
		planDentalBenefitDTO.setNetworkLimitation(pb.getNetworkLimitation());
		planDentalBenefitDTO.setNetworkLimitationAttribute(pb.getNetworkLimitationAttribute());
		planDentalBenefitDTO.setNetworkExceptions(pb.getNetworkExceptions());
		planDentalBenefitDTO.setSubjectToInNetworkDuductible(pb.getSubjectToInNetworkDuductible());
		planDentalBenefitDTO.setExcludedFromInNetworkMoop(pb.getExcludedFromInNetworkMoop());
		planDentalBenefitDTO.setSubjectToOutNetworkDeductible(pb.getSubjectToOutNetworkDeductible());
		planDentalBenefitDTO.setExcludedFromOutOfNetworkMoop(pb.getExcludedFromOutOfNetworkMoop());
		planDentalBenefitDTO.setEffectiveStartDate(simpleDf.format(pb.getEffStartDate()));
		planDentalBenefitDTO.setEffectiveEndDate(simpleDf.format(pb.getEffEndDate()));
		planDentalBenefitDTO.setIsEHB(pb.getIsEHB());
		planDentalBenefitDTO.setIsCovered(pb.getIsCovered());
		planDentalBenefitDTO.setMinStay(pb.getMinStay());
		planDentalBenefitDTO.setExplanation(pb.getExplanation());
		planDentalBenefitDTO.setNetworkT1CopayVal(pb.getNetworkT1CopayVal());
		planDentalBenefitDTO.setNetworkT1CopayAttr(pb.getNetworkT1CopayAttr());
		planDentalBenefitDTO.setNetworkT1CoinsurVal(pb.getNetworkT1CoinsurVal());
		planDentalBenefitDTO.setNetworkT1CoinsurAttr(pb.getNetworkT1CoinsurAttr());
		planDentalBenefitDTO.setNetworkT2CopayVal(pb.getNetworkT2CopayVal());
		planDentalBenefitDTO.setNetworkT2CopayAttr(pb.getNetworkT2CopayAttr());
		planDentalBenefitDTO.setNetworkT2CoinsurVal(pb.getNetworkT2CoinsurVal());
		planDentalBenefitDTO.setNetworkT2CoinsurAttr(pb.getNetworkT2CoinsurAttr());
		planDentalBenefitDTO.setOutOfNetworkCopayVal(pb.getOutOfNetworkCopayVal());
		planDentalBenefitDTO.setOutOfNetworkCopayAttr(pb.getOutOfNetworkCopayAttr());
		planDentalBenefitDTO.setOutOfNetworkCoinsurVal(pb.getOutOfNetworkCoinsurVal());
		planDentalBenefitDTO.setOutOfNetworkCoinsurAttr(pb.getOutOfNetworkCoinsurAttr());
		planDentalBenefitDTO.setNetworkT1display(pb.getNetworkT1display());
		planDentalBenefitDTO.setNetworkT2display(pb.getNetworkT2display());
		planDentalBenefitDTO.setOutOfNetworkDisplay(pb.getOutOfNetworkDisplay());
		planDentalBenefitDTO.setNetworkT1TileDisplay(pb.getNetworkT1TileDisplay());
		planDentalBenefitDTO.setLimitExcepDisplay(pb.getLimitExcepDisplay());
		
		return planDentalBenefitDTO;
	}
	
	
	private PlanHealthCostDTO copyToHealthCostDTO (PlanHealthCost ph ){
		PlanHealthCostDTO planHealthCostDTO = new PlanHealthCostDTO();
		planHealthCostDTO.setId(ph.getId());
		planHealthCostDTO.setPlanHealthId(ph.getPlanHealth().getId());
		planHealthCostDTO.setPlanId(ph.getPlanHealth().getPlan().getId());
		planHealthCostDTO.setName(ph.getName());
		planHealthCostDTO.setInNetWorkInd(ph.getInNetWorkInd());
		planHealthCostDTO.setInNetWorkFly(ph.getInNetWorkFly());
		planHealthCostDTO.setInNetworkTier2Ind(ph.getInNetworkTier2Ind());
		planHealthCostDTO.setInNetworkTier2Fly(ph.getInNetworkTier2Fly());
		planHealthCostDTO.setOutNetworkInd(ph.getOutNetworkInd());
		planHealthCostDTO.setOutNetworkFly(ph.getOutNetworkFly());
		planHealthCostDTO.setCombinedInOutNetworkInd(ph.getCombinedInOutNetworkInd());
		planHealthCostDTO.setCombinedInOutNetworkFly(ph.getCombinedInOutNetworkFly());
		planHealthCostDTO.setCombDefCoinsNetworkTier1(ph.getCombDefCoinsNetworkTier1());
		planHealthCostDTO.setCombDefCoinsNetworkTier2(ph.getCombDefCoinsNetworkTier2());
		planHealthCostDTO.setLimitExcepDisplay(ph.getLimitExcepDisplay());

		return planHealthCostDTO;
	}
		
	
	
	@Override
	@Transactional(readOnly = true)
	public List<PlanRateBenefit> getIndividualPlanBenefitsAndCost(List<Integer> planIds, Map<Integer, String> planIdAndHiosIdMap, String insuranceType, String coverageStartDate) {
		List<PlanRateBenefit> planBenefitsMap = new ArrayList<PlanRateBenefit>();
		EntityManager entityManager = null;

		try{
			entityManager = emf.createEntityManager();
			List<Integer> healthOrDentalIdsList = new ArrayList<Integer>();
			Map<Integer, Integer> planIdAndChildIdList = new HashMap<Integer, Integer>();
			String queryStr = PlanMgmtConstants.EMPTY_STRING;
			
			if(planIds != null && planIds.size() > PlanMgmtConstants.ZERO)
			{
				queryStr = PlanCostAndBenefitQueryBuilder.getIndividualPlanBenefitAndCostByPlanIdsQuery(insuranceType);
			}
			
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("INPUT buildquery :: " + queryStr);
			}
			
			Query query = entityManager.createQuery(queryStr);
			if(planIds != null && planIds.size() > PlanMgmtConstants.ZERO)
			{
				query.setParameter(PlanMgmtParamConstants.PARAM_PLAN_ID_LIST, planIds);
			}
			
			Iterator<?> rsIterator = query.getResultList().iterator();
			Object[] tempObject = null;
			
			while (rsIterator.hasNext()) {
				tempObject = (Object[]) (rsIterator.next());
				healthOrDentalIdsList.add((Integer) tempObject[PlanMgmtConstants.ZERO]);
				planIdAndChildIdList.put((Integer) tempObject[PlanMgmtConstants.ONE], (Integer) tempObject[PlanMgmtConstants.ZERO]);
			}	
			
			Map<Integer, Map<String, Map<String, String>>> benefitData = new HashMap<Integer, Map<String, Map<String, String>>>();
			Map<Integer, Map<String, Map<String, String>>> planCosts = new HashMap<Integer, Map<String, Map<String, String>>>();
			if(healthOrDentalIdsList.size() > 0){
				
				if (Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(insuranceType)) {
					benefitData = this.getOptimizedHealthPlanBenefits(healthOrDentalIdsList, coverageStartDate);
					planCosts = this.getOptimizedHealthPlanCosts(healthOrDentalIdsList);
				} 
				else if (Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(insuranceType)) {
					benefitData = this.getOptimizedDentalPlanBenefits(healthOrDentalIdsList, coverageStartDate);
					planCosts = this.getOptimizedDentalPlanCosts(healthOrDentalIdsList, false);
				}
				
				
				PlanRateBenefit planRateBenefitObj = null;
				Integer planId = null;
				Integer childTableId = null;
				for (Map.Entry<Integer, Integer> entry : planIdAndChildIdList.entrySet()) {
					planId =entry.getKey();
					childTableId = entry.getValue();
					
					planRateBenefitObj = new PlanRateBenefit();
					planRateBenefitObj.setId(planId);
					if(planIdAndHiosIdMap.containsKey(planId)){
						planRateBenefitObj.setHiosPlanNumber(planIdAndHiosIdMap.get(planId));
					}
					planRateBenefitObj.setPlanBenefits(benefitData.get(childTableId));
					planRateBenefitObj.setPlanCosts(planCosts.get(childTableId));
					planBenefitsMap.add(planRateBenefitObj);
				}
				
			}
			
		}catch(Exception ex){
			LOGGER.error("Exception occured in getIndividualPlanBenefitsAndCost: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}

		return planBenefitsMap;
	}
	
	
	
	/**
	 * Method is used to check any updates in existing benefits or not.
	 */
	@Override
	public boolean anyUpdatesInBenefits(String issuerPlanNumber, Integer applicableYear) {
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Execution of anyUpdatesInBenefits() Begins.");
		}
		boolean anyUpdates = false;

		try {
			Long planCount = null;

			if (StringUtils.isNotBlank(issuerPlanNumber) && null != applicableYear) {
				planCount = iPlanBenefitEditRepository.getPlanCountForIssuerPlanNumberAndApplicableYear(issuerPlanNumber, Integer.toString(applicableYear));
			}
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Plan Count for IssuerPlanNumber("+ issuerPlanNumber +") and ApplicableYear("+ applicableYear +") is " + planCount);
			}
			if (null != planCount && 0 < planCount) {
				anyUpdates = true;
			}
		}
		finally {
			LOGGER.debug("Execution of anyUpdatesInBenefits() End with return value " + anyUpdates);
		}
		return anyUpdates;
	}
	

	@Override
	@Transactional(readOnly = true)
	public List<PlanRateBenefit> getIndividualPlanBenefitsAndCostNew(List<Integer> planIds, String insuranceType, String coverageStartDate) {
		List<PlanRateBenefit> planBenefitsMap = new ArrayList<PlanRateBenefit>();
		EntityManager entityManager = null;

		try{
			entityManager = emf.createEntityManager();
			List<Integer> healthOrDentalIdsList = new ArrayList<Integer>();
			Map<Integer, Integer> planIdAndChildIdList = new HashMap<Integer, Integer>();
			String queryStr = PlanCostAndBenefitQueryBuilder.getIndividualPlanBenefitAndCostQuery(insuranceType);
	
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("INPUT buildquery :: " + queryStr);
			}
			Query query = entityManager.createQuery(queryStr);
			query.setParameter("param_planIdList", planIds);
			
			Iterator<?> rsIterator = query.getResultList().iterator();
			Object[] tempObject = null;
			
			while (rsIterator.hasNext()) {
				tempObject = (Object[]) (rsIterator.next());
				healthOrDentalIdsList.add((Integer) tempObject[PlanMgmtConstants.ZERO]);
				planIdAndChildIdList.put((Integer) tempObject[PlanMgmtConstants.ONE], (Integer) tempObject[PlanMgmtConstants.ZERO]);
			}	
			
			Map<Integer, Map<String, Map<String, String>>> benefitData = new HashMap<Integer, Map<String, Map<String, String>>>();
			Map<Integer, Map<String, Map<String, String>>> planCosts = new HashMap<Integer, Map<String, Map<String, String>>>();
			if(healthOrDentalIdsList.size() > 0){
				
				if (Plan.PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(insuranceType)) {
					benefitData = this.getOptimizedHealthPlanBenefitsNew(healthOrDentalIdsList, coverageStartDate);
					planCosts = this.getOptimizedHealthPlanCosts(healthOrDentalIdsList);
				} 
				else if (Plan.PlanInsuranceType.DENTAL.toString().equalsIgnoreCase(insuranceType)) {
					benefitData = this.getOptimizedDentalPlanBenefits(healthOrDentalIdsList, coverageStartDate);
					planCosts = this.getOptimizedDentalPlanCosts(healthOrDentalIdsList, false);
				}
				
				
				PlanRateBenefit planRateBenefitObj = null;
				for (Map.Entry<Integer, Integer> entry : planIdAndChildIdList.entrySet()) {
					Integer planId =entry.getKey();
					Integer childTableId = entry.getValue();
					
					planRateBenefitObj = new PlanRateBenefit();
					planRateBenefitObj.setId(planId);
					planRateBenefitObj.setPlanBenefits(benefitData.get(childTableId));
					planRateBenefitObj.setPlanCosts(planCosts.get(childTableId));
					planBenefitsMap.add(planRateBenefitObj);
				}
				
			}
			
		}catch(Exception ex){
			LOGGER.error("Exception occured in getIndividualPlanBenefitsAndCost: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}

		return planBenefitsMap;
	}
	
	@Override
	public GHIXResponse updatePlanBenefitAndCost(PlanBenefitAndCostRequestDTO planBenefitAndCostRequestDTO){
		GHIXResponse response = new GHIXResponse(); 
		
		try
		{
			if(null == planBenefitAndCostRequestDTO.getPlanId()
					 || null == planBenefitAndCostRequestDTO.getPlanBenefitAndCostList() 
					 || planBenefitAndCostRequestDTO.getPlanBenefitAndCostList().size() == PlanMgmtConstants.ZERO
					 || null == planBenefitAndCostRequestDTO.getUpdatedBy()
					 || planBenefitAndCostRequestDTO.getUpdatedBy() == PlanMgmtConstants.ZERO){
				
				response.setStatus(GhixConstants.RESPONSE_FAILURE);
				response.setErrCode(PlanMgmtErrorCodes.ErrorCode.INVALID_REQUEST_PARAMETER.getCode());
				response.setErrMsg(PlanMgmtConstants.INVALID_REQUEST);
			 }
			else{
				Plan planObj = iPlanRepository.findOne(planBenefitAndCostRequestDTO.getPlanId());
				if (null != planObj) {
					List<Map<String, String>> planHealthBenefitsListForUpdate = planBenefitAndCostRequestDTO.getPlanBenefitAndCostList();
							
					if(planObj.getInsuranceType().equalsIgnoreCase(PlanMgmtConstants.HEALTH))
					{
						List<PlanHealthBenefit> planHealthBenefitsList = new ArrayList<PlanHealthBenefit>();
						List<PlanHealthCost> planHealthCostsList = new ArrayList<PlanHealthCost>();
						
						planHealthBenefitsList = iPlanHealthBenefitRepository.getBenefitByPlanHealthId(planObj.getPlanHealth().getId()); 
						planHealthCostsList = iPlanHealthCostRepository.getCostByPlanHealthId(planObj.getPlanHealth().getId()); 
						updatePlanHealthForDisplayData(planHealthBenefitsList, planHealthCostsList, planHealthBenefitsListForUpdate, planBenefitAndCostRequestDTO.getUpdatedBy(), planObj);
						response.setStatus(GhixConstants.RESPONSE_SUCCESS);
					}
					else if(planObj.getInsuranceType().equalsIgnoreCase(PlanMgmtConstants.DENTAL))
					{
						List<PlanDentalBenefit> planDentalBenefitsList = new ArrayList<PlanDentalBenefit>();
						List<PlanDentalCost> planDentalCostsList = new ArrayList<PlanDentalCost>();
						
						planDentalBenefitsList = iPlanDentalBenefitRepository.getBenefitByPlanDentalId(planObj.getPlanDental().getId());
						planDentalCostsList = iPlanDentalCostRepository.getCostByPlanDentalId(planObj.getPlanDental().getId());
						updatePlanDentalForDisplayData(planDentalBenefitsList, planDentalCostsList, planHealthBenefitsListForUpdate, planBenefitAndCostRequestDTO.getUpdatedBy(), planObj);
						response.setStatus(GhixConstants.RESPONSE_SUCCESS);
					}
					
				}
				else{
					response.setStatus(GhixConstants.RESPONSE_FAILURE);
					response.setErrCode(PlanMgmtErrorCodes.ErrorCode.UPDATE_PLAN_BENEFIT_AND_COST.getCode());
					response.setErrMsg("Invalid Plan Id");
				}
			 }
		}
		catch(Exception ex)
		{
			LOGGER.error("Exception occured in updatePlanBenefitAndCost: ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute updatePlanBenefitAndCost ", ex);
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
			response.setErrCode(PlanMgmtErrorCodes.ErrorCode.TECHNICAL_ERROR.getCode());
			response.setErrMsg(PlanMgmtConstants.ERROR_TECHNICAL);
		}
		
		response.endResponse();
		return response;
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	private void updatePlanHealthForDisplayData(List<PlanHealthBenefit> planHealthBenefitsList, List<PlanHealthCost> planHealthCostsList, List<Map<String, String>> planHealthBenefitsListForUpdate, int accountUserId, Plan planObj) {
		String[] benefitDisplayDatArray = null;
		String[] costSharingDisplayDataArray = null;
		
		PlanDentalBenefit planDentalBenefit = null;
		PlanDentalCost planDentalCost = null;
		EntityManager entityManager =  null;
		String benefitdisplayDataString = null;
		List<PlanBenefitEdit> existingplanBenefitEditslist = null;
		PlanBenefitEdit planBenefitEdit = null;
		PlanCostEdit planCostEdit = null;
		List<PlanCostEdit> existingPlanCostEditsList = null;
		
		try{
			entityManager = emf.createEntityManager();
			entityManager.getTransaction().begin();
			
		
			for(PlanHealthBenefit planHealthBenefit : planHealthBenefitsList){
				benefitdisplayDataString = displayDataFromMap.getBenefitDisplayDataFromMap(planHealthBenefit.getName(), planHealthBenefitsListForUpdate, Plan.PlanInsuranceType.HEALTH.toString());
				if(benefitdisplayDataString != null){
					benefitDisplayDatArray = benefitdisplayDataString.split("##");
				}
				if(benefitDisplayDatArray.length > 0){
					//update benefits in PLAN_HEALTH_BENEFIT table
					updateHealthBenefitDisplayDataInfo(planHealthBenefit, benefitDisplayDatArray[PlanMgmtConstants.ZERO], benefitDisplayDatArray[PlanMgmtConstants.ONE], benefitDisplayDatArray[PlanMgmtConstants.TWO], benefitDisplayDatArray[PlanMgmtConstants.THREE], benefitDisplayDatArray[PlanMgmtConstants.FOUR]);
					
					// Populating cost data in PLAN_BENEFIT_EDITS table
					populateAndPersistBenefitsEdits(planHealthBenefit, planDentalBenefit, benefitDisplayDatArray, accountUserId, planObj.getIssuerPlanNumber(), String.valueOf(planObj.getApplicableYear()), entityManager, planBenefitEdit, existingplanBenefitEditslist);
				}
			}
			
			List<String> costSharingdisplayDataStringList = null;
			for(PlanHealthCost planHealthCost : planHealthCostsList){
				costSharingdisplayDataStringList = displayDataFromMap.getCostSharingDisplayDataFromMap(planHealthCost.getName(), planHealthBenefitsListForUpdate);
				
				if(costSharingdisplayDataStringList != null && !costSharingdisplayDataStringList.isEmpty() ){
					for(String costSharingdisplayDataString : costSharingdisplayDataStringList){
						if(costSharingdisplayDataString != null && costSharingdisplayDataString.length() > 0){
							costSharingDisplayDataArray = costSharingdisplayDataString.split("\\"+PlanMgmtConstants.PIPE_SIGN);
						}
						if(costSharingdisplayDataString!= null && costSharingDisplayDataArray.length > 0){
							//update benefits in PLAN_HEALTH_COST table
							updateHealthCostSharingDisplayDataInfo(planHealthCost,costSharingDisplayDataArray[PlanMgmtConstants.ZERO],costSharingDisplayDataArray[PlanMgmtConstants.ONE],costSharingDisplayDataArray[PlanMgmtConstants.TWO], costSharingDisplayDataArray[PlanMgmtConstants.THREE], costSharingDisplayDataArray[PlanMgmtConstants.FOUR]);
							
							// Populating cost data in PLAN_COST_EDITS table
							populateAndPersistCostEdits(planHealthCost, planDentalCost, accountUserId, planObj.getIssuerPlanNumber(), String.valueOf(planObj.getApplicableYear()), planCostEdit, existingPlanCostEditsList);
						}
					}
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured @updatePlanHealthForDisplayData : ", ex);	//ADDING EXCEPTION FRAMEWORK RELATED CHANGES
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute updatePlanHealthForDisplayData ", ex);
		}
		finally 
		{
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.getTransaction().commit();
				
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}
		
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	public void updatePlanDentalForDisplayData(List<PlanDentalBenefit> planDentalBenefitsList, List<PlanDentalCost> planDentalCostsList, List<Map<String, String>> planHealthOrDentalBenefitsListForUpdate, int accountUserId, Plan planObj) {
		String[] benefitDisplayDatArray = null;
		String[] costSharingDisplayDataArray = null;
		PlanHealthBenefit planHealthBenefit = null;
		PlanHealthCost planHealthCost = null;
		String benefitdisplayDataString = null;
		EntityManager entityManager = null; 
		List<PlanBenefitEdit> existingplanBenefitEditslist = null;
		PlanBenefitEdit planBenefitEdit = null;
		PlanCostEdit planCostEdit = null;
		List<PlanCostEdit> existingPlanCostEditsList = null;
		
		try{
			entityManager = emf.createEntityManager();
			entityManager.getTransaction().begin();
			
			for(PlanDentalBenefit planDentalBenefit : planDentalBenefitsList){
				benefitdisplayDataString = displayDataFromMap.getBenefitDisplayDataFromMap(planDentalBenefit.getName(), planHealthOrDentalBenefitsListForUpdate, Plan.PlanInsuranceType.DENTAL.toString());
				if(benefitdisplayDataString != null){
					benefitDisplayDatArray = benefitdisplayDataString.split("##");
				}
				if(benefitDisplayDatArray.length > 0){
					//update benefits in PLAN_DENTAL_BENEFITS table
					updateDentalBenefitDisplayDataInfo(planDentalBenefit, benefitDisplayDatArray[PlanMgmtConstants.ZERO], benefitDisplayDatArray[PlanMgmtConstants.ONE], benefitDisplayDatArray[PlanMgmtConstants.TWO], benefitDisplayDatArray[PlanMgmtConstants.THREE], benefitDisplayDatArray[PlanMgmtConstants.FOUR], benefitDisplayDatArray[PlanMgmtConstants.FIVE]);
					
					// Populating cost data in PLAN_BENEFIT_EDITS table
					populateAndPersistBenefitsEdits(planHealthBenefit, planDentalBenefit, benefitDisplayDatArray, accountUserId, planObj.getIssuerPlanNumber(), String.valueOf(planObj.getApplicableYear()), entityManager, planBenefitEdit, existingplanBenefitEditslist);
				}
			}
			
			List<String> costSharingdisplayDataStringList = null;
			for(PlanDentalCost planDentalCost : planDentalCostsList){
				costSharingdisplayDataStringList = displayDataFromMap.getCostSharingDisplayDataFromMap(planDentalCost.getName(), planHealthOrDentalBenefitsListForUpdate);
				if(costSharingdisplayDataStringList != null && !costSharingdisplayDataStringList.isEmpty() ){
					for(String costSharingdisplayDataString : costSharingdisplayDataStringList){
						if(costSharingdisplayDataString != null && costSharingdisplayDataString.length() > 0){
							costSharingDisplayDataArray = costSharingdisplayDataString.split("\\"+PlanMgmtConstants.PIPE_SIGN);
						}
						if(costSharingdisplayDataString!= null && costSharingDisplayDataArray.length > 0){
							//update benefits in PLAN_DENTAL_COST table
						    updateDentalCostSharingDisplayDataInfo(planDentalCost,costSharingDisplayDataArray[PlanMgmtConstants.ZERO],costSharingDisplayDataArray[PlanMgmtConstants.ONE],costSharingDisplayDataArray[PlanMgmtConstants.TWO], costSharingDisplayDataArray[PlanMgmtConstants.THREE], costSharingDisplayDataArray[PlanMgmtConstants.FOUR]);
						   
						    // Populating cost data in PLAN_COST_EDITS table
							populateAndPersistCostEdits(planHealthCost, planDentalCost, accountUserId, planObj.getIssuerPlanNumber(), String.valueOf(planObj.getApplicableYear()), planCostEdit, existingPlanCostEditsList);
						}
					}
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Exception occured @updatePlanDentalForDisplayData : ", ex);	//ADDING EXCEPTION FRAMEWORK RELATED CHANGES
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute updatePlanDentalForDisplayData ", ex);
		}
		finally {
			if (null != entityManager && entityManager.isOpen()) {
				entityManager.getTransaction().commit();
				entityManager.clear();
				entityManager.close();
				entityManager = null;
			}
		}
	}
	
	
	@Transactional(rollbackFor = Exception.class)
	private void updateHealthBenefitDisplayDataInfo(PlanHealthBenefit temp_planHealthBenefit, String temp_displayValue1, String temp_displayValue2, String temp_displayValue3, String temp_displayValue4, String temp_displayValue5){
		PlanHealthBenefit planHealthBenefit = temp_planHealthBenefit;
		String displayValue1 = temp_displayValue1;
		String displayValue2 = temp_displayValue2;
		String displayValue3 = temp_displayValue3; 
		String displayValue4 = temp_displayValue4;
		String displayValue5 = temp_displayValue5;
		
		try{
			if(displayValue1.equalsIgnoreCase(PlanMgmtConstants.ZERO_STR) || displayValue1.equalsIgnoreCase("N/A")){
				displayValue1 = PlanMgmtConstants.EMPTY_STRING;
			}
			if(displayValue2.equalsIgnoreCase(PlanMgmtConstants.ZERO_STR) || displayValue2.equalsIgnoreCase("N/A")){
				displayValue2 = PlanMgmtConstants.EMPTY_STRING;
			}
			if(displayValue3.equalsIgnoreCase(PlanMgmtConstants.ZERO_STR) || displayValue3.equalsIgnoreCase("N/A")){
				displayValue3 = PlanMgmtConstants.EMPTY_STRING;
			}
			if(displayValue4.equalsIgnoreCase("N/A")){
				displayValue4 = PlanMgmtConstants.EMPTY_STRING;
			}
			if(displayValue5.equalsIgnoreCase(PlanMgmtConstants.ZERO_STR) || displayValue5.equalsIgnoreCase("N/A")){
				displayValue5 = PlanMgmtConstants.EMPTY_STRING;
			}
		//	LOGGER.info("Ready to update HealthBenefit Data: " + displayValue1 +" $$ "+ displayValue2 +" $$ "+ displayValue3 +" $$ "+ displayValue4 +" $$ "+ displayValue5);
			planHealthBenefit.setNetworkT1display(displayValue1);
			planHealthBenefit.setNetworkT2display(displayValue2);
			planHealthBenefit.setOutOfNetworkDisplay(displayValue3);
			planHealthBenefit.setNetworkT1TileDisplay(displayValue5);
			planHealthBenefit.setLimitExcepDisplay(displayValue4);
			iPlanHealthBenefitRepository.save(planHealthBenefit);
		}catch(Exception ex){
			LOGGER.error("Exception occured @updateDisplayDataInfo : ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute updateDisplayDataInfo ", ex);
		}
	
	}
	
	/*
	 * Update edited cost sharing data for plan Display
	 * @param planHealthCost
	 * @param costSharingValue
	 * @param updateTo
	 * @param costSharingName
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	private boolean updateHealthCostSharingDisplayDataInfo(PlanHealthCost planHealthCost, String costSharingValue, String updateTo, String costSharingName, String limitExclusionVal, String networkType){
		boolean updateFlag = false;
		try{
			// set plan health cost IN/OUT NETWORK Individual data;
			if(planHealthCost.getName().equalsIgnoreCase(costSharingName) && PlanMgmtConstants.COST_SHARING_TYPE_IND.equalsIgnoreCase(updateTo)){
				if(costSharingValue != null && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingValue)) && PlanMgmtConstants.IN_NETWORK.equalsIgnoreCase(networkType)){
					if(StringUtils.isNotBlank(costSharingValue)){
						planHealthCost.setInNetWorkInd(Double.parseDouble(costSharingValue));
					}else{
						planHealthCost.setInNetWorkInd(null);
					}
					if(!limitExclusionVal.equalsIgnoreCase(PlanMgmtConstants.N_A)){
						planHealthCost.setLimitExcepDisplay(limitExclusionVal);
					}else{
						planHealthCost.setLimitExcepDisplay(PlanMgmtConstants.EMPTY_STRING);
					}
				}
				if(costSharingValue != null && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingValue)) && PlanMgmtConstants.OUT_NETWORK.equalsIgnoreCase(networkType)){
					if(StringUtils.isNotBlank(costSharingValue)){
						planHealthCost.setOutNetworkInd(Double.parseDouble(costSharingValue));
					}else{
						planHealthCost.setOutNetworkInd(null);
					}
				}
				iPlanHealthCostRepository.save(planHealthCost);
			}
			
			// set plan health cost IN/OUT NETWORK Family data
			if(planHealthCost.getName().equalsIgnoreCase(costSharingName) && PlanMgmtConstants.COST_SHARING_TYPE_FLY.equalsIgnoreCase(updateTo)){
				if(costSharingValue != null && costSharingValue.length() > 0 && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingValue)) && PlanMgmtConstants.IN_NETWORK.equalsIgnoreCase(networkType)){
					if(StringUtils.isNotBlank(costSharingValue)){
						planHealthCost.setInNetWorkFly(Double.parseDouble(costSharingValue));
					}else{
						planHealthCost.setInNetWorkFly(null);
					}
				}
				if(costSharingValue != null && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingValue)) && PlanMgmtConstants.OUT_NETWORK.equalsIgnoreCase(networkType)){
					if(StringUtils.isNotBlank(costSharingValue)){
						planHealthCost.setOutNetworkFly(Double.parseDouble(costSharingValue));
					}else{
						planHealthCost.setOutNetworkFly(null);
					}
				}
				
				iPlanHealthCostRepository.save(planHealthCost);
			}
			
		}catch(Exception ex){
			LOGGER.error("Exception occured @updateCostSharingDisplayDataInfo : ", ex);	//ADDING EXCEPTION FRAMEWORK RELATED CHANGES
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute updateCostSharingDisplayDataInfo ", ex);
		}
		return updateFlag;
	}
	
	
	
	/*
	 * Update Dental Plan Benefits
	 * @param planDentalBenefit
	 * @param displayValue1
	 * @param displayValue2
	 * @param displayValue3
	 * @param displayValue4
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	private boolean updateDentalBenefitDisplayDataInfo(PlanDentalBenefit temp_planDentalBenefit, String temp_displayValue1, String temp_displayValue2, String temp_displayValue3, String temp_displayValue4, String temp_displayValue5, String temp_displayValue6){
		PlanDentalBenefit planDentalBenefit = temp_planDentalBenefit; 
		String displayValue1 = temp_displayValue1; 
		String displayValue2 = temp_displayValue2; 
		String displayValue3 = temp_displayValue3;
		String displayValue4 = temp_displayValue4;
		String displayValue5 = temp_displayValue5;
		String displayValue6 = temp_displayValue6;
		boolean updateFlag = false;
		Integer updateResponse = 0;
		try{
			if(displayValue1.equalsIgnoreCase(PlanMgmtConstants.ZERO_STR) || displayValue1.equalsIgnoreCase("N/A")){
				displayValue1 = PlanMgmtConstants.EMPTY_STRING;
			}
			if(displayValue2.equalsIgnoreCase(PlanMgmtConstants.ZERO_STR) || displayValue2.equalsIgnoreCase("N/A")){
				displayValue2 = PlanMgmtConstants.EMPTY_STRING;
			}
			if(displayValue3.equalsIgnoreCase(PlanMgmtConstants.ZERO_STR) || displayValue3.equalsIgnoreCase("N/A")){
				displayValue3 = PlanMgmtConstants.EMPTY_STRING;
			}
			if(displayValue4.equalsIgnoreCase("N/A")){
				displayValue4 = PlanMgmtConstants.EMPTY_STRING;
			}
			if(displayValue5.equalsIgnoreCase(PlanMgmtConstants.ZERO_STR) || displayValue5.equalsIgnoreCase("N/A")){
				displayValue5 = PlanMgmtConstants.EMPTY_STRING;
			}
			if(displayValue6.equalsIgnoreCase(PlanMgmtConstants.ZERO_STR) || displayValue5.equalsIgnoreCase("N/A")){
				displayValue6 = PlanMgmtConstants.EMPTY_STRING;
			}
			//LOGGER.info("Ready to update DentalBenefit Data: " + displayValue1 +" $$ "+ displayValue2 +" $$ "+ displayValue3 +" $$ "+ displayValue4);
			planDentalBenefit.setNetworkT1display(displayValue1);
			planDentalBenefit.setNetworkT2display(displayValue2);
			planDentalBenefit.setOutOfNetworkDisplay(displayValue3);
			planDentalBenefit.setLimitExcepDisplay(displayValue4);
			planDentalBenefit.setNetworkT1TileDisplay(displayValue5);
			planDentalBenefit.setExplanation(displayValue6);
			iPlanDentalBenefitRepository.save(planDentalBenefit);
		}catch(Exception ex){
			LOGGER.error("Exception occured @updateDisplayDataInfo : ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute updateDisplayDataInfo ", ex);
		}
		if(updateResponse.equals(PlanMgmtConstants.ZERO)){
			updateFlag = false;
		}else{
			updateFlag = true;
		}
		return updateFlag;
	}
	
	
	/*
	 * update plan dental cost sharing information
	 * @param planDentalCost
	 * @param costSharingValue
	 * @param updateTo
	 * @param costSharingName
	 * @return
	 */
	// removing this for dental plans -  
	@Transactional(rollbackFor = Exception.class)
	private boolean updateDentalCostSharingDisplayDataInfo(PlanDentalCost planDentalCost, String costSharingValue, String updateTo, String costSharingName, String limitExclusionVal, String networkType){
		boolean updateFlag = false;
		try{
			// set plan dental cost IN/OUT NETWORK Individual data;
			if(planDentalCost.getName().equalsIgnoreCase(costSharingName) && PlanMgmtConstants.COST_SHARING_TYPE_IND.equalsIgnoreCase(updateTo)){
			    if(costSharingValue != null && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingValue)) && PlanMgmtConstants.IN_NETWORK.equalsIgnoreCase(networkType)){
			    	if(StringUtils.isNotBlank(costSharingValue)){
			    		planDentalCost.setInNetWorkInd(Double.parseDouble(costSharingValue));
			    	}else{
			    		planDentalCost.setInNetWorkInd(null);
			    	}
					// set limit and exception display text
					if(!limitExclusionVal.equalsIgnoreCase(PlanMgmtConstants.N_A)){
						planDentalCost.setLimitExcepDisplay(limitExclusionVal);
					}else{
						planDentalCost.setLimitExcepDisplay(PlanMgmtConstants.EMPTY_STRING);
					}
				}
				
				if(costSharingValue != null && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingValue)) && PlanMgmtConstants.OUT_NETWORK.equalsIgnoreCase(networkType)){
					if(StringUtils.isNotBlank(costSharingValue)){
						planDentalCost.setOutNetworkInd(Double.parseDouble(costSharingValue));
					}else{
						planDentalCost.setOutNetworkInd(null);
					}
				}
				
				iPlanDentalCostRepository.save(planDentalCost);
			}
			
			// set plan dental cost IN/OUT NETWORK Family data;
			if(planDentalCost.getName().equalsIgnoreCase(costSharingName) && PlanMgmtConstants.COST_SHARING_TYPE_FLY.equalsIgnoreCase(updateTo)){
				if(costSharingValue != null && costSharingValue.length() > 0 && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingValue)) && PlanMgmtConstants.IN_NETWORK.equalsIgnoreCase(networkType)){
					if(StringUtils.isNotBlank(costSharingValue)){
						planDentalCost.setInNetWorkFly(Double.parseDouble(costSharingValue));
					}else{
						planDentalCost.setInNetWorkFly(null);
					}
				}
				if(costSharingValue != null && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingValue)) && PlanMgmtConstants.OUT_NETWORK.equalsIgnoreCase(networkType)){
					if(StringUtils.isNotBlank(costSharingValue)){
						planDentalCost.setOutNetworkFly(Double.parseDouble(costSharingValue));
					}else{
						planDentalCost.setOutNetworkFly(null);
					}
				}
				
				iPlanDentalCostRepository.save(planDentalCost);
			}
			
		}catch(Exception ex){
			LOGGER.error("Exception occured @updateCostSharingDisplayDataInfo : ", ex);	//ADDING EXCEPTION FRAMEWORK RELATED CHANGES
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute updateCostSharingDisplayDataInfo ", ex);
		}
		return updateFlag;
	}
	
		
	
	private void populateAndPersistBenefitsEdits(PlanHealthBenefit planHealthBenefit, PlanDentalBenefit planDentalBenefit, String[] benefitDisplayDatArray, int accountUserId, String hiosPlanId,  String applicableYear, EntityManager entityManager, PlanBenefitEdit planBenefitEdit, List<PlanBenefitEdit> existingplanBenefitEditslist) {
		
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("populateAndPersistBenefitsEdits() Begins: " );
		}
		String benefitName = null;
		String networkT1Value = benefitDisplayDatArray[PlanMgmtConstants.ZERO];
		String networkT2Value = benefitDisplayDatArray[PlanMgmtConstants.ONE];
		String outNetworkValue = benefitDisplayDatArray[PlanMgmtConstants.TWO];
		String limitExceptionValue = benefitDisplayDatArray[PlanMgmtConstants.THREE];
		String networkT1Tilevalue = benefitDisplayDatArray[PlanMgmtConstants.FOUR];
			
		try{
			
			if( (null != planHealthBenefit && null == planDentalBenefit) || (null == planHealthBenefit && null != planDentalBenefit) )
			{
				if(null != planHealthBenefit){
					benefitName= planHealthBenefit.getName();
				}else if(null != planDentalBenefit){
					benefitName= planDentalBenefit.getName();
				}
				
				//fetching existing benefit edits records
				 existingplanBenefitEditslist= iPlanBenefitEditRepository.findByPlanHiosIdAndBenefitNameAndApplicableYear(hiosPlanId, benefitName, applicableYear);
				
				if(CollectionUtils.isEmpty(existingplanBenefitEditslist)){
						if(LOGGER.isDebugEnabled()){
					LOGGER.debug("No Records found for IssuerPlan number : "+ hiosPlanId +" benefit Name : "+benefitName +" and applicable year : "+  applicableYear);
					}
					planBenefitEdit = new PlanBenefitEdit();
				}else{
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("Record found for IssuerPlan number : "+ hiosPlanId +" benefit Name : "+benefitName +" and applicable year : "+  applicableYear);
					}
					planBenefitEdit = existingplanBenefitEditslist.get(0);
				}
				
				if(null!= planHealthBenefit){
					planBenefitEdit.setPlan(planHealthBenefit.getPlanHealth().getPlan());
					planBenefitEdit.setInsuranceType(PlanMgmtConstants.HEALTH);
					planBenefitEdit.setPlanBenefitId(planHealthBenefit.getId());
				}else if(null!= planDentalBenefit){
					planBenefitEdit.setPlan(planDentalBenefit.getPlanDental().getPlan());
					planBenefitEdit.setInsuranceType(PlanMgmtConstants.DENTAL);
					planBenefitEdit.setPlanBenefitId(planDentalBenefit.getId());
				}	
					
				planBenefitEdit.setPlanHiosId(hiosPlanId);
				planBenefitEdit.setBenefitName(benefitName);
				planBenefitEdit.setApplicableYear(applicableYear);
				
				planBenefitEdit.setNetworkT1CoinsuranceAttr(planHealthBenefit.getNetworkT1CoinsurAttr());
				planBenefitEdit.setNetworkT1CoinsuranceVal(planHealthBenefit.getNetworkT1CoinsurVal());
				planBenefitEdit.setNetworkT1CopayAttr(planHealthBenefit.getNetworkT1CopayAttr());
				planBenefitEdit.setNetworkT1CopayVal(planHealthBenefit.getNetworkT1CopayVal());
				planBenefitEdit.setNetworkT2CoinsuranceAttr(planHealthBenefit.getNetworkT2CoinsurAttr());
				planBenefitEdit.setNetworkT2CoinsuranceVal(planHealthBenefit.getNetworkT2CoinsurVal());
				planBenefitEdit.setNetworkT2CopayAttr(planHealthBenefit.getNetworkT2CopayAttr());
				planBenefitEdit.setNetworkT2CopayVal(planHealthBenefit.getNetworkT2CopayVal());
				planBenefitEdit.setOutNetworkCoinsuranceAttr(planHealthBenefit.getOutOfNetworkCoinsurAttr());
				planBenefitEdit.setOutNetworkCoinsuranceVal(planHealthBenefit.getOutOfNetworkCoinsurVal());
				planBenefitEdit.setOutNetworkCopayAttr(planHealthBenefit.getOutOfNetworkCopayAttr());
				planBenefitEdit.setOutNetworkCopayVal(planHealthBenefit.getOutOfNetworkCopayVal());
				
				planBenefitEdit.setNetworkT1Value(networkT1Value);
				planBenefitEdit.setNetworkT2Value(networkT2Value);
				planBenefitEdit.setOutOfNetworkValue(outNetworkValue);
				planBenefitEdit.setNetworkT1TileValue(networkT1Tilevalue);
				planBenefitEdit.setLimitExceptionValue(limitExceptionValue);
				planBenefitEdit.setLastUpdatedBy(accountUserId);
				//saving edited records 
				entityManager.merge(planBenefitEdit);
				
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Benefit edit records persisted successfully..");
				}
			}
		}
		catch (Exception ex) {
			LOGGER.error("Exception occured @populateAndPersistBenefitsEdits ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute populateAndPersistBenefitsEdits ", ex);
		}
	}
	
	
  private void populateAndPersistCostEdits(PlanHealthCost planHealthCost, PlanDentalCost planDentalCost, int accountUserId, String planHiosId, String applicableYear, PlanCostEdit planCostEdit, List<PlanCostEdit> existingPlanCostEditsList) {
	  	if(LOGGER.isDebugEnabled()){
	  		LOGGER.debug("populateAndPersistCostEdits() Begins");
	  	}
	  	
	  	String costName = null;
	  	
		try {
			
			if( (null != planHealthCost && null == planDentalCost) || (null == planHealthCost && null != planDentalCost) )
			{
				if(null != planHealthCost){
					costName= planHealthCost.getName();
				}else if(null != planDentalCost){
					costName= planDentalCost.getName();
				}
				
				//fetching existing benefit edits records
				existingPlanCostEditsList = iPlanCostEditRepository.findByIssuerPlanNumberAndNameAndApplicableYear(planHiosId, costName, applicableYear);
										
				if (CollectionUtils.isEmpty(existingPlanCostEditsList)) {

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("No Records found for IssuerPlan number: "+ planHiosId +", benefit Name: "+ costName +" and applicable year: "+ applicableYear);
					}
					planCostEdit = new PlanCostEdit();
					planCostEdit.setCreatedBy(accountUserId);
				}
				else {

					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("Record found for IssuerPlan number: "+ planHiosId +", benefit Name: "+ costName +" and applicable year: "+ applicableYear);
					}
					planCostEdit = existingPlanCostEditsList.get(0);
				}
				
				if(null != planHealthCost){
					planCostEdit.setPlan(planHealthCost.getPlanHealth().getPlan());
					planCostEdit.setPlanCostId(planHealthCost.getId());
					planCostEdit.setInsuranceType(PlanMgmtConstants.HEALTH);
				}else if(null != planDentalCost){
					planCostEdit.setPlan(planDentalCost.getPlanDental().getPlan());
					planCostEdit.setPlanCostId(planDentalCost.getId());
					planCostEdit.setInsuranceType(PlanMgmtConstants.DENTAL);
				}
				
				planCostEdit.setIssuerPlanNumber(planHiosId);
				planCostEdit.setName(costName);
				planCostEdit.setApplicableYear(applicableYear);

				planCostEdit.setInNetworkInd(planHealthCost.getInNetWorkInd());
				planCostEdit.setInNetworkFamily(planHealthCost.getInNetWorkFly());
				planCostEdit.setOutOfNetworkInd(planHealthCost.getOutNetworkInd());
				planCostEdit.setOutOfNetworkFamily(planHealthCost.getOutNetworkFly());
				planCostEdit.setLimitExceptionValue(planHealthCost.getLimitExcepDisplay());
				planCostEdit.setLastUpdatedBy(accountUserId);

				//saving edited records 
				iPlanCostEditRepository.save(planCostEdit);
								
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Cost edit records persisted successfully.");
				}	
			}
			
		}
		catch (Exception ex) {
			LOGGER.error("Exception occured @populateAndPersistCostEdits ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute populateAndPersistCostEdits ", ex);
		}
	}

}
