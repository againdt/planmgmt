package com.getinsured.hix.planmgmt.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.planmgmt.MedicarePlan;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.platform.util.SecurityUtil;

@Service("MedicarePlanRateService")
@Repository
@Transactional
public class MedicarePlanRateServiceImpl implements MedicarePlanRateService{
	
	private static final Logger LOGGER = Logger.getLogger(MedicarePlanRateServiceImpl.class);
	
	@PersistenceUnit private EntityManagerFactory emf;
	
	@Autowired
	private MedicarePlanRateBenefitService medicarePlanRateBenefitService;
	
	/**
	 * 
	 * @param planIdList
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<MedicarePlan> getPlanResponseListForMedicare (List<Integer> planIdList){
		LOGGER.debug("=======Inside getPlanResponseListForMedicare() method======"); 
		List<MedicarePlan> planResponseList =null;
		if(!planIdList.isEmpty()){
			planResponseList = new ArrayList<MedicarePlan>();
			MedicarePlan planResponse = null;
			StringBuilder buildquery = new StringBuilder();
			EntityManager entityManager = null;
			
			buildquery.append("select p.id as plan_id,p.name as plan_name, p.insurance_type, p.network_type, p.applicable_year as plan_year, i.logo_url, i.name as issuer_name,i.id as issuer_id, p.hiosproduct_id, p.brochure, pm.id as medicareId, pm.cms_is_snp ");
			buildquery.append("from issuers i, plan p, plan_medicare pm ");
			buildquery.append("where P.ISSUER_ID = I.ID  AND p.id=pm.plan_id AND P.INSURANCE_TYPE IN('MC_ADV','MC_SUP','MC_RX')");
			buildquery.append(" and p.id in (?1) ");
			
			try {
				entityManager = emf.createEntityManager();
				Query query = entityManager.createNativeQuery(buildquery.toString());
				query.setParameter(1, planIdList);
				List<?> rsList = query.getResultList();
				Iterator<?> rsIterator = rsList.iterator();
				String planId,planName,issuerId,issuerName,insuranceType,networkType,issuerLogo,planYear,planHiosId,brochureUrl, medicareId, isSNP;
					

				while (rsIterator.hasNext()) {				
					Object[] objArray = (Object[])(rsIterator.next());
					planId = (objArray[PlanMgmtConstants.ZERO] != null) ? objArray[PlanMgmtConstants.ZERO].toString() : "";
					planName = (objArray[PlanMgmtConstants.ONE] != null) ? objArray[PlanMgmtConstants.ONE].toString() : "";
					insuranceType = (objArray[PlanMgmtConstants.TWO] != null) ? objArray[PlanMgmtConstants.TWO].toString() : "";
					networkType = (objArray[PlanMgmtConstants.THREE] != null) ? objArray[PlanMgmtConstants.THREE].toString() : "";
					planYear = (objArray[PlanMgmtConstants.FOUR] != null) ? objArray[PlanMgmtConstants.FOUR].toString() : "";
					issuerLogo = (objArray[PlanMgmtConstants.FIVE] != null) ? objArray[PlanMgmtConstants.FIVE].toString() : "";
					issuerName = (objArray[PlanMgmtConstants.SIX] != null) ? objArray[PlanMgmtConstants.SIX].toString() : "";
					issuerId = (objArray[PlanMgmtConstants.SEVEN] != null) ? objArray[PlanMgmtConstants.SEVEN].toString() : "";		
					planHiosId = (objArray[PlanMgmtConstants.EIGHT] != null) ? objArray[PlanMgmtConstants.EIGHT].toString() : "";
					brochureUrl = (objArray[PlanMgmtConstants.NINE] != null) ? objArray[PlanMgmtConstants.NINE].toString() : "";
					medicareId = (objArray[PlanMgmtConstants.TEN] != null) ? objArray[PlanMgmtConstants.TEN].toString() : "";
					isSNP = (objArray[PlanMgmtConstants.ELEVEN] != null) ? objArray[PlanMgmtConstants.ELEVEN].toString() : "";
								
					

					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug(SecurityUtil.sanitizeForLogging(planId+ ": "+planName+ ": "+insuranceType+ ": "+issuerLogo+ ": "+issuerName+ ": "+issuerId));
					}
					planResponse = new MedicarePlan();
					planResponse.setId(Integer.valueOf(planId));
					planResponse.setName(planName);
					planResponse.setInsuranceType(insuranceType);
					planResponse.setIssuerLogo(issuerLogo);
					planResponse.setIssuerName(issuerName);
					planResponse.setIssuerId(Integer.valueOf(issuerId));
					planResponse.setNetworkType(networkType);
					planResponse.setPlanYear(planYear);
					planResponse.setIsSNP(isSNP);
					planResponse.setPlanHiosId(planHiosId);
					planResponse.setBrochureUrl(brochureUrl);
					
					Map<String, Map<String, String>> medicareBenefits = medicarePlanRateBenefitService.getMedicarePlanBenefits(Integer.parseInt(medicareId));		
			   		if(null != medicareBenefits && medicareBenefits.size() > 0) {
			   			planResponse.setBenefits(medicareBenefits);
			   		}		
					
					planResponseList.add(planResponse);					
					
				} 	

			}catch(Exception e){
				LOGGER.error("Exception occured while fetching Medicare plans",e);
			}finally{
				 // close the entity manager
				 if(entityManager !=null && entityManager.isOpen()){
					 entityManager.clear();
					 entityManager.close();
					 entityManager = null;
				 }
			}
		}
		return planResponseList;	
	}
		
}
