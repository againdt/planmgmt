package com.getinsured.hix.planmgmt.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.PlanNetworkReport;

@Repository()
public interface IProviderPlanNetworkReportRepository extends JpaRepository<PlanNetworkReport, Integer> {
}
