package com.getinsured.hix.planmgmt.service;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingRequestDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanEnrollmentQuotingResponseDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanMemberRequestDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanMemberResponseDTO;
import com.getinsured.hix.dto.planmgmt.LifeSpanPeriodLoopDTO;
import com.getinsured.hix.dto.planmgmt.PrescriptionSearchDTO;
import com.getinsured.hix.dto.planmgmt.SbcScenarioDTO;
import com.getinsured.hix.dto.prescription.PrescriptionSearchRequest;
import com.getinsured.hix.dto.shop.employer.eps.EmployeePlanDTO;
import com.getinsured.hix.dto.shop.employer.eps.EmployerQuotingDTO;
import com.getinsured.hix.dto.shop.employer.eps.PlanDTO;
import com.getinsured.hix.dto.shop.mlec.MLECRestRequest;
import com.getinsured.hix.dto.shop.mlec.MLECRestResponse;
import com.getinsured.hix.dto.shop.mlec.Member;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanRateBenefit;
import com.getinsured.hix.model.PlanSbcScenario;
import com.getinsured.hix.model.RatingArea;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.querybuilder.LifespanEnrollmentQuptingQueryBuilder;
import com.getinsured.hix.planmgmt.querybuilder.LowestPremiumQueryBuilder;
import com.getinsured.hix.planmgmt.repository.IFormularyRepository;
import com.getinsured.hix.planmgmt.repository.IPlanRepository;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtErrorCodes;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.service.TenantService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.timeshift.TSCalendar;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

@Service("planRateBenefitService")
@Repository
@Transactional
@DependsOn("dynamicPropertiesUtil")
public class PlanRateBenefitServiceImpl implements PlanRateBenefitService {

	private static final Logger LOGGER = Logger.getLogger(PlanRateBenefitServiceImpl.class);
	private static final String RELATION = "relation";
	private static final String ZIP = "zip";
	private static final String AGE = "age";
	private static final String TOBACCO = "tobacco";
	private static final String COUNTYCODE = "countycode";
	private static final String ID = "id";
	private static final String CS1 = "CS1"; // for base plan of any metal tier
	private static final String CS2 = "CS2"; // for any metal tier, american native FPL plan, when we pull plan from SERFF
	private static final String CS3 = "CS3"; // for any metal tier, american native FPL plan, when we pull plan from SERFF
	private static final String CS4 = "CS4"; // for silver plan, csr variation	// #1
	private static final String CS5 = "CS5"; // for silver plan, csr variation	// #2
	private static final String CS6 = "CS6"; // for silver plan, csr variation	// #3
	private static final String ALIAS_OF_PLAN_TABLE = "p";
	
	@Value(PlanMgmtConstants.CONFIG_APP_URL)
	private String appUrl;

	@Value(PlanMgmtConstants.CONFIG_DB_TYPE)
	private String configuredDB;
	
	private static final String DOCUMENT_URL = "download/document?documentId="; // document download url
	private static final String FAMILY_TIER_PRIMARY = "Individual Rate";
	private static final String FAMILY_LOOKUP_NAME = "SERFF_FAMILY_OPTION";
	private static final String CHILD = "child";
	private static final String EHB_COVERED_PARTIAL = "PARTIAL";
	private static final long NEW_BENCHMARK_EFFECTIVE_YEAR = 2019;
	// HIX-57761, list of FFM states
	//private static final String[] FFMSTATEARR = {"AK","AL","AR","AZ","DE","FL","GA","IA","IL","IN","KS","LA","ME","MI","MO","MS","MT","NC","ND","NE","NH","NJ","NM","NV","OH","OK","OR","PA","SC","SD","TN","TX","UT","VA","WI","WV","WY"}; 
	
	public enum CostNames {
		MAX_OOP_MEDICAL, MAX_OOP_DRUG, MAX_OOP_INTG_MED_DRUG, DEDUCTIBLE_MEDICAL, DEDUCTIBLE_DRUG, DEDUCTIBLE_INTG_MED_DRUG, DEDUCTIBLE_BRAND_NAME_DRUG, ANNUAL_MAXIMUM_BENEFIT;
	}
	
	@PersistenceUnit 
	private EntityManagerFactory emf;

	@Autowired
	private IssuerQualityRatingService issuerQualityRatingService;
	@Autowired
	private RatingAreaService ratingAreaService;
	/*@Autowired
	private RestTemplate restTemplate;*/
	@Autowired
	private LookupService lookupService;
	@Autowired
	private IFormularyRepository formularyRepository;
	@Autowired
	private QuotingBusinessLogic quotingBusinessLogic; 
	@Autowired
	private PlanCostAndBenefitsService planCostAndBenefitsService;
	@Autowired 
	private ProviderService providerService;
	@Autowired
	private PrescriptionSearchService prescriptionSearchService;
	@Autowired
	private IPlanRepository iPlanRepository;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private PufPlanService pufPlanService;
	@Autowired
	private TenantService tenantService;
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	@Autowired
	private NetworkTansparencyRatingService networkTansparencyRatingService;
	@Autowired
	private LifespanEnrollmentQuptingQueryBuilder lifespanEnrollmentQuptingQueryBuilder;
	@Autowired
	private LowestPremiumQueryBuilder lowestPremiumQueryBuilder;
	@Autowired
	private PlanSbcScenarioService planSbcScenarioService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private Gson platformGson;

	@Override
	public double getBenchmarkRate(List<Map<String, String>> censusData, String inputCoverageStartDate, String externalCaseId) throws GIException {
		double benchMarkPre = PlanMgmtConstants.ZERO;

		try {
			Map<String, String> benchMarkPlanIdPremiumMap = getBenchmarkRateForEnrollment(censusData, inputCoverageStartDate, null, null, externalCaseId, null);
			if(benchMarkPlanIdPremiumMap != null && benchMarkPlanIdPremiumMap.containsKey(PlanMgmtConstants.TOTALPREMIUM)){
				benchMarkPre = Double.parseDouble(benchMarkPlanIdPremiumMap.get(PlanMgmtConstants.TOTALPREMIUM));
			}
			
			// if benchmark plan not found
			if (benchMarkPre == PlanMgmtConstants.ZERO) {
				throw new GIException(GhixErrors.NO_DATA_FOUND);
			}
		} catch (Exception e) {
			LOGGER.error("Error while fetching benchmark plan for having family data ==> " + censusData.toString() + " Error: " + e.getMessage());
			throw new GIException(GhixErrors.ERROR_WHILE_PROCESSING);
		}
		
		return benchMarkPre;
	}
	
	@Override
	public Map <String, String> getBenchmarkRateForEnrollment(List<Map<String, String>> unprocessedCensusData, String inputCoverageStartDate, String costShareLevel, String tenantCode, String externalCaseId, String currentHiosIssuerID) throws Exception {
		return  getSecondLowestRate( getAvailableSilverPlans(unprocessedCensusData, inputCoverageStartDate, costShareLevel, tenantCode, externalCaseId, currentHiosIssuerID) );
	}

	private Map<String, String> getSecondLowestRate(List<Map<String, String>> silverPlanDetailsMapList) {
		Map<String, String> benchMarkPlanIdPremiumMap = new HashMap<String, String>();
		if (!CollectionUtils.isEmpty(silverPlanDetailsMapList)) {
			if (silverPlanDetailsMapList.size() == 1) { // if only single silver plan available then consider that plan as benchmark plan
				benchMarkPlanIdPremiumMap = silverPlanDetailsMapList.get(0);
			} else if (silverPlanDetailsMapList.size() >= 2) { // if more than one silver plan available then consider second plan as benchmark plan
				benchMarkPlanIdPremiumMap = silverPlanDetailsMapList.get(1);
			}
		}
		return benchMarkPlanIdPremiumMap;
	}	
	
	@Override
	public List<Map <String, String>> getAvailableSilverPlans(List<Map<String, String>> unprocessedCensusData, String inputCoverageStartDate, String costShareLevel, String tenantCode, String externalCaseId, String currentHiosIssuerID) throws Exception {
		List<Map<String, String>> silverPlansDetailsMapList = null;
		if(unprocessedCensusData != null && unprocessedCensusData.size() >= 0 && StringUtils.isNotBlank(inputCoverageStartDate) ){
	    	for(Map<String, String> memberData : unprocessedCensusData){
		    	if(!memberData.containsKey(PlanMgmtConstants.AGE) || (memberData.containsKey(PlanMgmtConstants.AGE) && memberData.get(PlanMgmtConstants.AGE) == null)){
		    	    memberData.put(PlanMgmtConstants.AGE, String.valueOf(calculateAge(memberData.get("dob"), inputCoverageStartDate, "MM/dd/yyyy")) ); 
	    		}
	    	}
	    	// HIX-100131 :: Convert requested Relationship Id to Relation String
	    	List<Map<String, String>> censusDataWithRelationShipAsString = quotingBusinessLogic.convertRelationshipIdToString(unprocessedCensusData);
			List<Map<String, String>> processedCensusData = quotingBusinessLogic.processMemberData(censusDataWithRelationShipAsString, Plan.PlanInsuranceType.HEALTH.toString());	
			String coverageStartDateArr[] = inputCoverageStartDate.split("/");
			String coverageStartDate = null;
			String coverageYear = null;

			if(coverageStartDateArr != null && coverageStartDateArr.length == 3) {
				coverageStartDate = coverageStartDateArr[2] + "-" + coverageStartDateArr[0] + "-" +coverageStartDateArr[1];
				coverageYear = coverageStartDateArr[2];
			}
			else {
				//The date is already in required format
				coverageStartDate = inputCoverageStartDate;
				coverageYear = inputCoverageStartDate.split("-")[0];
			}

			List<String> currentHiosIdList = null;

			if (StringUtils.isNotBlank(externalCaseId) && StringUtils.isBlank(currentHiosIssuerID)
					&& "MN".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))) {
				currentHiosIdList = getPlanHiosIdForExternalMemberIdAPI(externalCaseId, coverageYear);
			}
			silverPlansDetailsMapList = getAvailableSilverPlans(censusDataWithRelationShipAsString, processedCensusData, coverageStartDate, costShareLevel, tenantCode, currentHiosIdList);
		}
		else {
			LOGGER.error(PlanMgmtConstants.INVALID_REQUEST);
		}
		return silverPlansDetailsMapList;
	}

	/**
	 * Method is used to get List of HIOS Issuer IDs using - GET_PLAN_HIOS_ID_FOR_EXTERNAL_MEMBER_ID [Params: External Case ID & Coverage Year] API.
	 */
	private List<String> getPlanHiosIdForExternalMemberIdAPI(String externalCaseId, String coverageYear) {

		List<String> currentHiosIdList = null;
		final String apiName = EnrollmentEndPoints.GET_PLAN_HIOS_ID_FOR_EXTERNAL_MEMBER_ID + " API ";

		try {

			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setExternalMemberId(externalCaseId);
			enrollmentRequest.setCoverageYear(coverageYear);
			EnrollmentResponse enrollmentResponse = null;

			String apiResponse =  ghixRestTemplate.postForObject(EnrollmentEndPoints.GET_PLAN_HIOS_ID_FOR_EXTERNAL_MEMBER_ID, enrollmentRequest, String.class);

			Map<String, List<Map<String, Object>>> responseMap = null;

			if (StringUtils.isNotBlank(apiResponse)) {

				enrollmentResponse = platformGson.fromJson(apiResponse, EnrollmentResponse.class);

				if (enrollmentResponse != null) {
					currentHiosIdList = enrollmentResponse.getHiosIssuerIdList();
				}
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception Occured in IssuerRestClient.updateIssuerReps ", e);
		}
		finally {

			if (CollectionUtils.isEmpty(currentHiosIdList)) {
				LOGGER.warn("Failed to get Current Hios Id List from " + apiName);
			}
		}
		return currentHiosIdList;
	}

	@Transactional(readOnly = true)
	private List<Map <String, String>> getAvailableSilverPlans(List<Map<String, String>> unprocessedCensusData, List<Map<String, String>> processedCensusData, 
												String coverageStartDate, String costShareLevel, String tenantCode, List<String> currentHiosIdList) {
		ArrayList<String> paramList = new ArrayList<String>();
		List<Map<String, String>> premiumAndIdMapList = new ArrayList<Map<String, String>>();
		EntityManager entityManager = null;
		try{
			entityManager = emf.createEntityManager();
			String familyTier = quotingBusinessLogic.getFamilyTier(processedCensusData); // compute family tier
			String familyTierLookupCode = lookupService.getLookupValueCode(PlanMgmtConstants.FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
			String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedCensusData, Plan.PlanInsuranceType.HEALTH.toString());
			String stateName = quotingBusinessLogic.getApplicantStateCode(processedCensusData);	
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
			String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
			String applicableYear = coverageStartDate.substring(0,4);
			long lApplicableYear = Long.parseLong(applicableYear);
			StringBuilder buildquery = new StringBuilder();
			buildquery.append("SELECT sum(pre) AS total_premium, p.ehb_premium_fraction, p.issuer_plan_number, temp.ehb_covered, p.name, p.id ");
			buildquery.append("FROM plan p, ");
			if (GhixConstants.PHIX.equals(currentExchangeType)){
				buildquery.append(" pm_tenant_plan tp, tenant t, ");
			}
			buildquery.append(" ( ");

			String applicantZip = null;
			String applicantCounty = null;
	
			for (int i = 0; i < processedCensusData.size(); i++) {
				buildquery.append("( SELECT phealth.id AS phealth_id, phealth.plan_id, prate.rate AS pre, phealth.plan_level AS plan_level, phealth.ehb_covered, 1 as memberctr ");
				buildquery.append("FROM ");
				buildquery.append("plan_health phealth, pm_service_area psarea, pm_plan_rate prate, plan p2 ");
				buildquery.append("WHERE ");
				if(StringUtils.isNotBlank(costShareLevel)) {
					buildquery.append("phealth.cost_sharing = :param_" + paramList.size());
					paramList.add(costShareLevel);			
				} else {
					buildquery.append("phealth.cost_sharing = 'CS1' " );
					//buildquery.append("phealth.parent_plan_id = 0 ");
				}
				buildquery.append(" AND phealth.plan_level = 'SILVER' ");
				buildquery.append(" AND p2.id = phealth.plan_id ");
				buildquery.append(" AND p2.service_area_id = psarea.service_area_id ");
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND psarea.zip = to_number(trim(:param_" + paramList.size() + ")) ");
					paramList.add(processedCensusData.get(i).get(ZIP));
				}else{
					buildquery.append("AND psarea.zip = :param_" + paramList.size() + " ");
					paramList.add(processedCensusData.get(i).get(ZIP));
				}

				if (null == applicantZip) {
					applicantZip = processedCensusData.get(i).get(ZIP);
				}

				buildquery.append("AND psarea.fips = :param_" + paramList.size() + " ");
				paramList.add(processedCensusData.get(i).get(COUNTYCODE));

				if (null == applicantCounty) {
					applicantCounty = processedCensusData.get(i).get(COUNTYCODE);
				}
				
				buildquery.append("AND p2.id = prate.plan_id ");
				buildquery.append("AND p2.is_deleted = 'N' ");
				buildquery.append("AND p2.insurance_type = 'HEALTH' ");
				buildquery.append("AND psarea.is_deleted = 'N' ");
				buildquery.append("AND prate.rating_area_id = ");
				
				// -------- changes for HIX-100092 starts --------
				buildquery.append(quotingBusinessLogic.buildSearchRatingAreaLogic(stateCode, paramList, processedCensusData, i, applicableYear, configuredDB));
				// -------- changes for HIX-100092 ends --------
					
				
				// in private exchange rate could by age band or family tier
				buildquery.append(quotingBusinessLogic.buildRateLogic("prate", stateName, houseHoldType, familyTierLookupCode, processedCensusData.get(i).get(PlanMgmtConstants.AGE), processedCensusData.get(i).get(PlanMgmtConstants.TOBACCO), coverageStartDate, paramList));
				buildquery.append(" ) ");
				
				if (i + 1 != processedCensusData.size()) {
					buildquery.append(" UNION ALL ");
				}
			}
	
			buildquery.append(") temp WHERE p.id = temp.plan_id AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') between p.start_date and p.end_date ");
			paramList.add(coverageStartDate);		
			
			buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic(ALIAS_OF_PLAN_TABLE, true));
			buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic(ALIAS_OF_PLAN_TABLE, coverageStartDate, PlanMgmtConstants.NO, applicantZip, applicantCounty, currentHiosIdList, paramList));
			buildquery.append("AND p.market = '" + Plan.PlanMarket.INDIVIDUAL.toString() + "' ");

			if (GhixConstants.PHIX.equals(currentExchangeType)){
				buildquery.append(" AND p.id = tp.plan_id ");
				buildquery.append(" AND tp.tenant_id = t.id ");
				buildquery.append(" AND t.code = :param_" + paramList.size() );
				if(StringUtils.isBlank(tenantCode)) {
					paramList.add(PlanMgmtConstants.TENANT_GINS);
				} else {
					paramList.add(tenantCode.toUpperCase());
				}
			}
			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic(ALIAS_OF_PLAN_TABLE, houseHoldType));
			buildquery.append("GROUP BY p.id, p.name, p.ehb_premium_fraction, p.issuer_plan_number, temp.ehb_covered HAVING sum(memberctr) = " + processedCensusData.size());
			buildquery.append(" ORDER BY total_premium ASC ");
	
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Fetching Available Silver Plans (For benchmark), SQL == " + buildquery.toString());
			}
	
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			List rsList = query.getResultList();
	
			if (rsList == null || rsList.isEmpty()) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Silver Plans not found.");
				}
			} else {
				Iterator rsIterator = rsList.iterator();
				String considerEHBPortion = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CONSIDER_EHB_PORTION);
				//List<Double> premiumArrayList = new ArrayList<Double>();
				DecimalFormat f = new DecimalFormat("##.00");
				Object[] objArray = null;	
				Map<String, String>premiumAndIdMap =  null;
				double dentalPremiumForChildren = 0;
				boolean gotDentalPremium = false; 
				
				while (rsIterator.hasNext()) {
					objArray = (Object[]) rsIterator.next();
					double totalPremium = Double.parseDouble(objArray[PlanMgmtConstants.ZERO].toString());
					 
					// HIX-39219
					// EHB flag is ON, then premium would be, premium = ehb percentage * current premium  
					// HIX-65077, fetch ehb portion from plan table. no more use of URRT template
					if(considerEHBPortion.equalsIgnoreCase("Y") && objArray[PlanMgmtConstants.ONE] != null){
						double ehbPercentage = Double.parseDouble(objArray[PlanMgmtConstants.ONE].toString());
						// premium = ehb percentage * current premium
						totalPremium = totalPremium * ehbPercentage ;					
					}				
					
					premiumAndIdMap = new HashMap<String,String> ();
					premiumAndIdMap.put(PlanMgmtConstants.PLANID, objArray[PlanMgmtConstants.TWO].toString());
					String ehbCovered = objArray[PlanMgmtConstants.THREE].toString();
					if(lApplicableYear >= NEW_BENCHMARK_EFFECTIVE_YEAR && EHB_COVERED_PARTIAL.equalsIgnoreCase(ehbCovered)) {
						if(!gotDentalPremium) {
							dentalPremiumForChildren = getBenchmarkDentalRate(unprocessedCensusData, coverageStartDate, true, stateName, null, entityManager);
							gotDentalPremium = true;
						}
						totalPremium += dentalPremiumForChildren;
					}
					premiumAndIdMap.put(PlanMgmtConstants.TOTALPREMIUM, f.format(totalPremium));
					premiumAndIdMap.put(PlanMgmtConstants.PLAN_NAME, objArray[PlanMgmtConstants.FOUR].toString());
					premiumAndIdMap.put(PlanMgmtConstants.INTERNAL_PLAN_ID, objArray[PlanMgmtConstants.FIVE].toString());
					premiumAndIdMapList.add(premiumAndIdMap);
				}
				
				// sort premium			
				//Collections.sort(premiumArrayList);
				Collections.sort(premiumAndIdMapList, new ComparePremium());
			}		
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getAvailableSilverPlans", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}	
		return premiumAndIdMapList;
	}
	
	private double getBenchmarkDentalRate(List<Map<String, String>> censusData, String coverageStartDate,
			boolean childOnly, String stateName, String tenantCode, EntityManager entityManager) {

		double secoundLowestPemium = 0;
		String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
		List<Map<String, String>> childOnlyCensusData = quotingBusinessLogic.processMemberData(censusData, Plan.PlanInsuranceType.DENTAL.toString(), childOnly);

		if(CollectionUtils.isEmpty(childOnlyCensusData)) {
			LOGGER.info("No child member for dental plans in the household");
		} else {
			ArrayList<String> paramList = new ArrayList<String>();
			Map <String, String> benchMarkPlanIdPremiumMap = new HashMap <String, String>();
			EntityManager em = null;
			
			try{
				if(null == entityManager) {
					em = emf.createEntityManager();
				} else {
					em = entityManager;
				}
				
				String houseHoldType = quotingBusinessLogic.getHouseHoldType(childOnlyCensusData, Plan.PlanInsuranceType.DENTAL.toString());
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
				String applicableYear = coverageStartDate.substring(0,4);
				StringBuilder buildquery = new StringBuilder();
				buildquery.append("SELECT sum(pre) AS total_premium, p.issuer_plan_number, temp.ehb_appt_for_pediatric_dental ");
				buildquery.append("FROM plan p, ");
				if (GhixConstants.PHIX.equals(currentExchangeType)){
					buildquery.append(" pm_tenant_plan tp, tenant t, ");
				}
				buildquery.append(" ( ");

				String applicantZip = null;
				String applicantCounty = null;
		
				for (int i = 0; i < childOnlyCensusData.size(); i++) {
					buildquery.append("( SELECT pdental.id AS pdental_id, pdental.plan_id, prate.rate AS pre, pdental.plan_level AS plan_level, pdental.ehb_appt_for_pediatric_dental, 1 as memberctr ");
					buildquery.append("FROM ");
					buildquery.append("plan_dental pdental, pm_service_area psarea, pm_plan_rate prate, plan p2 ");
					buildquery.append("WHERE ");
					buildquery.append("pdental.parent_plan_id = 0 ");
					buildquery.append("AND p2.id = pdental.plan_id ");
					buildquery.append("AND p2.service_area_id = psarea.service_area_id ");
					
					if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
						buildquery.append("AND psarea.zip = to_number(trim(:param_" + paramList.size() + ")) ");
						paramList.add(childOnlyCensusData.get(i).get(ZIP));
					}else{
						buildquery.append("AND psarea.zip = :param_" + paramList.size() + " ");
						paramList.add(childOnlyCensusData.get(i).get(ZIP));
					}

					if (null == applicantZip) {
						applicantZip = childOnlyCensusData.get(i).get(ZIP);
					}

					buildquery.append("AND psarea.fips = :param_" + paramList.size() + " ");
					paramList.add(childOnlyCensusData.get(i).get(COUNTYCODE));

					if (null == applicantCounty) {
						applicantCounty = childOnlyCensusData.get(i).get(COUNTYCODE);
					}
					buildquery.append("AND p2.id = prate.plan_id ");
					buildquery.append("AND p2.is_deleted = 'N' ");
					buildquery.append("AND p2.insurance_type = 'DENTAL' ");
					buildquery.append("AND psarea.is_deleted = 'N' ");
					buildquery.append("AND prate.rating_area_id = ");
					
					// -------- reference HIX-100092 --------
					buildquery.append(quotingBusinessLogic.buildSearchRatingAreaLogic(stateCode, paramList, childOnlyCensusData, i, applicableYear, configuredDB));
					buildquery.append(quotingBusinessLogic.buildRateLogic("prate", stateName, houseHoldType, null, childOnlyCensusData.get(i).get(PlanMgmtConstants.AGE), null, coverageStartDate, paramList));
					buildquery.append(" ) ");
					
					if (i + 1 != childOnlyCensusData.size()) {
						buildquery.append(" UNION ALL ");
					}
				}
		
				buildquery.append(") temp WHERE p.id = temp.plan_id AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') between p.start_date and p.end_date ");
				paramList.add(coverageStartDate);		
				
				buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic(ALIAS_OF_PLAN_TABLE, true));
				buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic(ALIAS_OF_PLAN_TABLE, coverageStartDate, PlanMgmtConstants.NO, applicantZip, applicantCounty, null, paramList));
				buildquery.append("AND p.market = '" + Plan.PlanMarket.INDIVIDUAL.toString() + "' ");
				if (GhixConstants.PHIX.equals(currentExchangeType)){
					buildquery.append(" AND p.id = tp.plan_id ");
					buildquery.append(" AND tp.tenant_id = t.id ");
					buildquery.append(" AND t.code = :param_" + paramList.size() );
					if(StringUtils.isBlank(tenantCode)) {
						paramList.add(PlanMgmtConstants.TENANT_GINS);
					} else {
						paramList.add(tenantCode.toUpperCase());
					}
				}
				buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic(ALIAS_OF_PLAN_TABLE, houseHoldType));
				buildquery.append("GROUP BY p.id, p.issuer_plan_number, temp.ehb_appt_for_pediatric_dental HAVING sum(memberctr) = " + childOnlyCensusData.size());
				buildquery.append(" ORDER BY total_premium ASC ");
		
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Fetching benchmark dental plan, SQL == " + buildquery.toString());
				}
		
				Query query = entityManager.createNativeQuery(buildquery.toString());
				for (int i = 0; i < paramList.size(); i++) {
					query.setParameter("param_" + i, paramList.get(i));
				}
				List rsList = query.getResultList();
		
				if (CollectionUtils.isEmpty(rsList)) {
					if(LOGGER.isInfoEnabled()) {
						LOGGER.info("Benchmark dental plan not found.");
					}
				} else {
					Iterator rsIterator = rsList.iterator();
					DecimalFormat f = new DecimalFormat("##.00");
					Object[] objArray = null;	
					List<Map<String, String>> premiumAndIdMapList = new ArrayList<Map<String, String>>();
					Map<String, String>premiumAndIdMap =  null;
					
					while (rsIterator.hasNext()) {
						objArray = (Object[]) rsIterator.next();
						double totalPremium = Double.parseDouble(objArray[PlanMgmtConstants.ZERO].toString());
						 
						if(objArray[PlanMgmtConstants.TWO] != null){
							double ehbApptForPediatricDental = Double.parseDouble(objArray[PlanMgmtConstants.TWO].toString());
							totalPremium = totalPremium * ehbApptForPediatricDental ;					
						}				
						
						premiumAndIdMap = new HashMap<String,String> ();
						premiumAndIdMap.put(PlanMgmtConstants.PLANID, objArray[PlanMgmtConstants.ONE].toString());
						premiumAndIdMap.put(PlanMgmtConstants.TOTALPREMIUM, f.format(totalPremium));
						premiumAndIdMapList.add(premiumAndIdMap);
					}
					
					Collections.sort(premiumAndIdMapList, new ComparePremium());
					
					if (premiumAndIdMapList.size() == 1) { // if only single dental plan available then consider that plan as benchmark dental plan 
						benchMarkPlanIdPremiumMap =  premiumAndIdMapList.get(0);
					} else if (premiumAndIdMapList.size() >= 2) { // if more than one dental plan available then consider second plan as benchmark dental plan 
						benchMarkPlanIdPremiumMap = premiumAndIdMapList.get(1);
					}
					
					if(null != benchMarkPlanIdPremiumMap.get(PlanMgmtConstants.TOTALPREMIUM)) {
						secoundLowestPemium = Double.parseDouble(benchMarkPlanIdPremiumMap.get(PlanMgmtConstants.TOTALPREMIUM));
					}
				}		
			}catch(Exception ex){
				LOGGER.error("Exceptin Occured in getBenchmarkDentalRate", ex);
			}finally{
				 // close the entity manager
				 if(entityManager == null && em != null && em.isOpen()){
					 em.clear();
					 em.close();
					 em = null;
				 }
			}
		}
		
		return secoundLowestPemium;
	}

	@Override
	@Transactional(readOnly = true)
	public double getLowestPremiumPlanRate(String dob, String zip, String coverageStartDate, String tobacco, String planLevel, String planMarket, String countyCode) {
		double premium = PlanMgmtConstants.ZERO;
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			ArrayList<String> paramList = new ArrayList<String>();
			int paramCount = 0;
			Integer age = calculateAge(dob, coverageStartDate, GhixConstants.REQUIRED_DATE_FORMAT);
	
			String familyTierLookupCode = lookupService.getLookupValueCode(FAMILY_LOOKUP_NAME, FAMILY_TIER_PRIMARY); // compute family tier lookup code
			StringBuilder buildquery = new StringBuilder();
			buildquery.append("SELECT MIN(prate.rate) AS pre ");
			buildquery.append("FROM plan p, plan_health phealth, pm_service_area psarea, pm_plan_rate prate, pm_zip_county_rating_area pzcrarea ");
			buildquery.append("WHERE ");
			buildquery.append("p.id = phealth.plan_id ");
			buildquery.append("AND phealth.parent_plan_id = 0 ");
			buildquery.append("AND p.service_area_id = psarea.service_area_id ");
			
			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("AND psarea.zip = to_number(trim(:param_" + paramCount  + ")) ");
				paramList.add(zip);
				paramCount++;
			}else{
				buildquery.append("AND psarea.zip = :param_" + paramCount  + " ");
				paramList.add(zip);
				paramCount++;
			}
			
			buildquery.append("AND p.id = prate.plan_id ");
			buildquery.append("AND p.is_deleted = 'N' ");
			buildquery.append("AND prate.is_deleted = 'N' ");
			buildquery.append("AND psarea.is_deleted = 'N' ");
			buildquery.append("AND pzcrarea.rating_area_id = prate.rating_area_id ");
			
			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("AND (pzcrarea.zip = to_number(trim(:param_" + paramCount  + ")) OR pzcrarea.zip IS NULL) ");
				paramList.add(zip);
				paramCount++;
			}else{
				buildquery.append("AND (pzcrarea.zip = :param_" + paramCount  + " OR pzcrarea.zip IS NULL) ");
				paramList.add(zip);
				paramCount++;
			}
			
			buildquery.append("AND ( (CAST(:param_" + paramCount + " AS INTEGER) >= prate.min_age AND ");
			paramList.add(String.valueOf(age));
			paramCount++;
			
			buildquery.append("CAST(:param_" + paramCount  + " AS INTEGER) <= prate.max_age) ");
			paramList.add(String.valueOf(age));
			paramCount++;
			
			buildquery.append("OR (CAST(:param_" + paramCount + " AS INTEGER) >= prate.min_age AND ");
			paramList.add(familyTierLookupCode);
			paramCount++;
			
			buildquery.append( "CAST(:param_" + paramCount + " AS INTEGER) <= prate.max_age) ) ");
			paramList.add(familyTierLookupCode);
			paramCount++;
			
			buildquery.append("AND phealth.plan_level = :param_" + paramCount + " ");
			paramList.add(planLevel.toUpperCase());
			paramCount++;
			
			buildquery.append("AND p.market = :param_" + paramCount + " ");
			paramList.add(planMarket.toUpperCase());
			paramCount++;
			
			buildquery.append("AND TO_DATE (:param_" + paramCount + ", 'MM-DD-YYYY') between p.start_date and p.end_date ");
			paramList.add(coverageStartDate);
			paramCount++;
			
			buildquery.append("AND p.status IN ('" + Plan.PlanStatus.CERTIFIED.toString() + "') ");
			buildquery.append("AND p.issuer_verification_status='" + Plan.IssuerVerificationStatus.VERIFIED.toString() + "' ");
			if (tobacco != null && !tobacco.equals(PlanMgmtConstants.EMPTY_STRING)) {
				buildquery.append("AND prate.tobacco = '" + tobacco.toUpperCase() + "' ");
			}
			if (countyCode != null && !countyCode.equals(PlanMgmtConstants.EMPTY_STRING)) {
				buildquery.append("AND psarea.fips = :param_" + paramCount + " ");
				paramList.add(countyCode);
				paramCount++;
				
				buildquery.append("AND pzcrarea.county_fips = :param_" + paramCount + " ");
				paramList.add(countyCode);
				paramCount++;
				
			}
	
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Lowest premium  rate SQL = " + buildquery.toString());
			}
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramCount; i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			List rsList = query.getResultList();
			Iterator rsIterator = rsList.iterator();
			DecimalFormat f = new DecimalFormat("##.00");
	
			while (rsIterator.hasNext()) {
				premium = Double.parseDouble(f.format(rsIterator.next()));
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getLowestPremiumPlanRate", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		return premium;
	}

	private Integer calculateAge(String memberDob, String coverageStartDate, String dateFormat ) throws GIException {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		Date dateOfBirth = null;
		Date dateOfCoverage =  null;
		int age = 0;
		
		try {
			sdf.setLenient(false);
			dateOfBirth = sdf.parse(memberDob);
			dateOfCoverage = sdf.parse(coverageStartDate);
		
			Calendar dob = TSCalendar.getInstance();
			dob.setTime(dateOfBirth);
			Calendar today = TSCalendar.getInstance();
			today.setTime(dateOfCoverage);
			
			age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
			if (dob.get(Calendar.MONTH) > today.get(Calendar.MONTH)
					|| (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH) && dob.get(Calendar.DATE) > today.get(Calendar.DATE))) {
				age--;
			}
			if (age < 0) {
				throw new GIException(GhixErrors.INVALID_AGE);
			}
			
		} catch (ParseException e) {
			LOGGER.error("Error while parsing Date of Birth ", e);
			throw new GIException(GhixErrors.INVALID_BIRTH_DATE);
		}
		return age;
	}
	


	@Override
	@Transactional(readOnly = true)
	public Map<String, Float> getEmployeePlanRate(List<Map<String, String>> employeeCensusData, String empPrimaryZip, String empCountyCode, String planId,
			String effectiveDate, boolean quoteByEmpPrimaryZip) {		
		
		Map<String, Float> rate = new HashMap<String, Float>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			ArrayList<String> paramList = new ArrayList<String>();
			int paramCount = 0;
			StringBuilder buildquery = new StringBuilder();
			buildquery.append("SELECT p.id AS plan_id, sum(pre) AS total_premium, sum(indvpre) AS indv_premium ");
			buildquery.append("FROM plan p, ( ");
			int j = 0;
			
			// process family data
			List<Map<String, String>> processedEmployeeData = quotingBusinessLogic.processMemberData(employeeCensusData,Plan.PlanInsuranceType.HEALTH.toString());
			String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedEmployeeData, Plan.PlanInsuranceType.HEALTH.toString());		
			String familyTier = quotingBusinessLogic.getFamilyTier(processedEmployeeData); // compute family tier
			String familyTierLookupCode = lookupService.getLookupValueCode(FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
			
			// parse census data of employee's family members
			for (Map<String, String> employee : processedEmployeeData) {
				buildquery.append("( SELECT phealth.plan_id, prate.rate AS pre, 1 AS memberctr, ");
				if (employee.get(RELATION).equalsIgnoreCase("member")) {
					buildquery.append(" prate.rate AS indvpre ");
				} else {
					buildquery.append(" 0 AS indvpre ");
				}
				// if quoteByEmpPrimaryZip == true use employer primary zip to quote else employee's own zip code		
				String zip = (quoteByEmpPrimaryZip) ? empPrimaryZip : employee.get(ZIP);
				buildquery.append(" FROM plan_health phealth, pm_service_area psarea, pm_plan_rate prate, plan p2, pm_zip_county_rating_area pzcrarea ");
				buildquery.append("WHERE ");
				buildquery.append("p2.id = phealth.plan_id ");
				buildquery.append("AND phealth.parent_plan_id = 0 ");
				buildquery.append("AND p2.service_area_id = psarea.service_area_id ");
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND psarea.zip = to_number(trim(:param_" + paramCount + ")) ");
					paramList.add(zip);
					paramCount++;
				}else{
					buildquery.append("AND psarea.zip = :param_" + paramCount + " ");
					paramList.add(zip);
					paramCount++;
				}
				
				buildquery.append("AND p2.id = prate.plan_id ");
				buildquery.append("AND p2.is_deleted = 'N' ");
				buildquery.append("AND prate.is_deleted = 'N' ");
				buildquery.append("AND psarea.is_deleted = 'N' ");
				buildquery.append("AND pzcrarea.rating_area_id = prate.rating_area_id ");
				// consider tobacco usages to compute plan rate  
				if (employee.get(TOBACCO) != null && !employee.get(TOBACCO).equals(PlanMgmtConstants.EMPTY_STRING)) {
					String tobacco = employee.get(TOBACCO).toUpperCase();
					if (tobacco.equalsIgnoreCase("YES")) {
						tobacco = "Y";
					}
					if (tobacco.equalsIgnoreCase("NO")) {
						tobacco = "N";
					}
					buildquery.append("AND (prate.tobacco = :param_" + paramCount + " OR prate.tobacco is NULL) ");
					paramList.add(tobacco);
					paramCount++;
				}
				// in private exchange rate could be basis on age band or family tier
				buildquery.append(" AND ( (CAST (:param_" + paramCount + " AS INTEGER) >= prate.min_age AND ");
				paramList.add(employee.get(AGE));
				paramCount++;
				
				buildquery.append("CAST (:param_" + paramCount + " AS INTEGER) <= prate.max_age) ");
				paramList.add(employee.get(AGE));
				paramCount++;
				
				buildquery.append(" OR (CAST (:param_" + paramCount + " AS INTEGER) >= prate.min_age AND ");
				paramList.add(familyTierLookupCode);
				paramCount++;
				
				buildquery.append("CAST (:param_" + paramCount + " AS INTEGER) <= prate.max_age) ) ");
				paramList.add(familyTierLookupCode);
				paramCount++;
				
				buildquery.append("AND TO_DATE (:param_" + paramCount + ", 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date ");
				paramList.add(effectiveDate);
				paramCount++;
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND (pzcrarea.zip = to_number(trim(:param_" + paramCount + ")) OR pzcrarea.zip IS NULL) ");
					paramList.add(zip);
					paramCount++;
				}else{
					buildquery.append("AND (pzcrarea.zip = :param_" + paramCount + " OR pzcrarea.zip IS NULL) ");
					paramList.add(zip);
					paramCount++;
				}
	
				// if quoteByEmpPrimaryZip == true use employer's county code to quote else use employee's own county code							
				String countyCode = (quoteByEmpPrimaryZip) ? empCountyCode : (employee.get(COUNTYCODE) == null) ? PlanMgmtConstants.EMPTY_STRING : employee.get(COUNTYCODE);
	
				if (!countyCode.equals(PlanMgmtConstants.EMPTY_STRING)) {
					buildquery.append(" AND psarea.fips = :param_" + paramCount + " ");
					paramList.add(countyCode);
					paramCount++;
					
					buildquery.append(" AND pzcrarea.county_fips = :param_" + paramCount + " ");
					paramList.add(countyCode);
					paramCount++;
				}
				
				buildquery.append(" ) ");
	
				if (j + 1 != processedEmployeeData.size()) {
					buildquery.append(" UNION ALL ");
				}
	
				j++;
			}
	
			buildquery.append(") temp WHERE p.id = temp.plan_id AND ");
			buildquery.append(" p.id = CAST (:param_" + paramCount + " AS INTEGER) ");
			paramList.add(planId);
			paramCount++;
			
			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic(ALIAS_OF_PLAN_TABLE, houseHoldType));
			buildquery.append(" GROUP BY p.id  HAVING sum(memberctr) = " + processedEmployeeData.size());
	
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Compute Employer Plan Rate SQL = " + buildquery.toString());
			}
	
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramCount; i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			List rsList = query.getResultList();
			
			Iterator rsIterator = rsList.iterator();
			while (rsIterator.hasNext()) {
	
				Object[] objArray = (Object[]) rsIterator.next();
				Float totalPremium = Float.parseFloat(objArray[1].toString());
				Float indvPremium = (float) 0;
				indvPremium = Float.parseFloat(objArray[2].toString());
	
				rate.clear();
				rate.put("planId",  Float.parseFloat(objArray[0].toString()));
				rate.put("totalPremium", totalPremium);
				rate.put("indvPremium", indvPremium);
				rate.put("depenPremium", totalPremium - indvPremium);
			}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getEmployeePlanRate", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		
		return rate;

	}


	@Override
	public List<PlanRateBenefit> getHouseholdPlanRateBenefits(List<Map<String, String>> memberList, String insType,
			String effectiveDate, String costSharing, int groupId, String planIdStr, boolean showCatastrophicPlan,
			String planLevel, String ehbCovered, List<Map<String, List<String>>> providerList, String marketType,
			String isSpecialEnrollment, int issuerId, boolean issuerVerifiedFlag, String exchangeType, String tenant,
			boolean minimizePlanData, String hiosPlanNumber, String keepOnly,
			List<PrescriptionSearchRequest> prescriptionSearchRequstList, String eliminatePlanIds,
			String restrictHiosIds, String showPUFPlans, String currentHiosIssuerID, String isPlanChange) {

		List<PlanRateBenefit> planRateBenefitsList = new ArrayList<PlanRateBenefit>();

		if (insType.equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())) {
			planRateBenefitsList = getHouseholdHealthPlanRateBenefits(memberList, effectiveDate, costSharing, planIdStr,
					showCatastrophicPlan, planLevel, ehbCovered, providerList, marketType, isSpecialEnrollment,
					issuerId, issuerVerifiedFlag, exchangeType, tenant, minimizePlanData, hiosPlanNumber, keepOnly,
					prescriptionSearchRequstList, eliminatePlanIds, restrictHiosIds, showPUFPlans, currentHiosIssuerID,
					isPlanChange);
		}
		else if (insType.equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())) {
			planRateBenefitsList = getHouseholdDentalPlanRateBenefits(memberList, effectiveDate, planIdStr, planLevel,
					marketType, isSpecialEnrollment, issuerId, issuerVerifiedFlag, exchangeType, tenant, hiosPlanNumber,
					keepOnly, minimizePlanData, eliminatePlanIds, restrictHiosIds, showPUFPlans, currentHiosIssuerID,
					isPlanChange);
		}
		else {
			planRateBenefitsList = new ArrayList<PlanRateBenefit>();
		}
		return planRateBenefitsList;
	}

	@Transactional(readOnly = true)
	private List<PlanRateBenefit> getHouseholdHealthPlanRateBenefits(List<Map<String, String>> memberList,
			String effectiveDate, String costSharing, String planIdStr, boolean showCatastrophicPlan, String planLevel,
			String ehbCovered, List<Map<String, List<String>>> providerList, String marketType,
			String isSpecialEnrollment, int issuerId, boolean issuerVerifiedFlag, String exchangeType,
			String tenantCode, boolean minimizePlanData, String hiosPlanNumber, String keepOnly,
			List<PrescriptionSearchRequest> prescriptionSearchRequstList, String eliminatePlanIds,
			String restrictHiosIds, String showPUFPlans, String currentHiosIssuerID, String isPlanChange) {

		List<PlanRateBenefit> planRateBenefitList = new ArrayList<PlanRateBenefit>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			List<PlanRateBenefit> pufPlanData = new ArrayList<PlanRateBenefit>();
			boolean showPufPlans = false;
			List<String> restrictHiosIdListOfTenant = null;
			String considerEHBPortion = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CONSIDER_EHB_PORTION);
			String computeIssuerQualityRating = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.COMPUTEISSUERQUALITYRATING);
			String computeNetworkTransparencyRating = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.COMPUTE_NETWORK_TRANSPARENCY_RATING);
			String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		//  String turnOnSSAP = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.TURNONOROFFSSAP);
			List<Map<String, String>> processedMemberData = quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.HEALTH.toString()); // filter member data	
			String stateName =  quotingBusinessLogic.getApplicantStateCode(processedMemberData);
			String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedMemberData, Plan.PlanInsuranceType.HEALTH.toString());		
			String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
			String familyTierLookupCode = quotingBusinessLogic.getFamilyTierLookupCode(familyTier); // compute family tier lookup code
			String applicableYear = effectiveDate.substring(0,4);
			
			// If exchange type is PHIX then pull tenant configuration 
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				// pull tenant's show PUF plan configuration
				if(StringUtils.isNotBlank(showPUFPlans) && PlanMgmtConstants.YES.equalsIgnoreCase(showPUFPlans)) {
					showPufPlans = true;
				} else if(StringUtils.isNotBlank(showPUFPlans) && PlanMgmtConstants.NO.equalsIgnoreCase(showPUFPlans)) {
					showPufPlans = false;
				} else if(StringUtils.isBlank(showPUFPlans)) { 
					if(null != tenantService.getTenant(tenantCode).getConfiguration().getPlansConfiguration() 
						&& null != tenantService.getTenant(tenantCode).getConfiguration().getPlansConfiguration().getShowPufPlans() 
						&& PlanMgmtConstants.YES.equalsIgnoreCase(tenantService.getTenant(tenantCode).getConfiguration().getPlansConfiguration().getShowPufPlans().toString()) ){
						showPufPlans = true;
					}
				}
				// HIX-82090 - if affliate's restrictHiosId flag value is set as ON, then pull tenant's restricted Hios Issuer Id list 
				if(restrictHiosIds.equalsIgnoreCase(PlanMgmtConstants.ON)){
					restrictHiosIdListOfTenant = tenantService.getTenant(tenantCode).getConfiguration().getHiosIdConfiguration();
				}
			}		
			
		
			ArrayList<String> paramList = new ArrayList<String>();
			StringBuilder buildquery = new StringBuilder(2048);
			buildquery.append("SELECT p.id AS plan_id, p.name AS plan_name, plan_level, sum(pre) AS total_premium, p.network_type AS network_type, i.name AS issuer_name, phealth_id, parent_id, ehb_covered, i.id as issuer_id, ");
			if(PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB) || StringUtils.isEmpty(configuredDB)) {
				buildquery.append("LISTAGG(memberid, ',') within GROUP (order by memberid) AS memberlist ,");
				buildquery.append("LISTAGG(pre, ',') within GROUP (order by memberid) AS memberprelist,");
				buildquery.append("LISTAGG(rname, ',') within GROUP (order by memberid) AS memberrelist, ");
			} else if(PlanMgmtConstants.DB_POSTGRESQL.equalsIgnoreCase(configuredDB)) {
				buildquery.append("STRING_AGG(CAST (memberid AS VARCHAR), ',' order by memberid) AS memberlist ,");
				buildquery.append("STRING_AGG(to_char(CAST (pre AS DOUBLE PRECISION), '9999D99'), ',' order by memberid) AS memberprelist,");
				buildquery.append("STRING_AGG(rname, ',' order by memberid) AS memberrelist, ");
			}
			buildquery.append("sbc_ucm_id, p.brochure AS brochure, benefits_url, net.network_url AS network_url, i.logo_url AS issuer_logo, p.brochure_ucm_id AS brochure_ucm, max_coins_spdrug, max_chr_cpy, pmrycare_no_vsit, ");
			buildquery.append("pmrycare_after_copay, p.formularly_id, p.tp_id, p.is_puf, rate_option, p.hsa, net.network_key, net.has_provider_data, p.issuer_plan_number, ");
			buildquery.append("TIER2_UTIL, p.eoc_doc_id, cost_sharing, p.ehb_premium_fraction, net.certified, net.verified, i.hios_issuer_id, net.network_id AS networkId, p.exchange_type, sbc_scenario_id ");
			buildquery.append("FROM ");
			buildquery.append("plan p, issuers i, network net, ");
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("After first part: SQL == " + buildquery.toString());
			}
			
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildquery.append(" pm_tenant_plan tp, tenant t, ");
			}
			buildquery.append(" ( ");

			String applicantZip = null;
			String applicantCounty = null;
	
			for (int i = 0; i < processedMemberData.size(); i++) {
				buildquery.append("( SELECT phealth.id AS phealth_id, phealth.parent_plan_id AS parent_id, phealth.plan_id, prate.rate AS pre, phealth.plan_level AS plan_level, phealth.ehb_covered AS ehb_covered, :param_" + paramList.size());
				paramList.add(processedMemberData.get(i).get(ID));
			    buildquery.append(" AS memberid, prarea.rating_area AS rname, 1 AS memberctr, phealth.sbc_ucm_id AS sbc_ucm_id, phealth.benefits_url AS benefits_url, phealth.max_coinsurance_for_sp_drug AS max_coins_spdrug, ");
			    buildquery.append("phealth.max_chrging_inpatient_cpy AS max_chr_cpy, phealth.pmry_care_cs_aftr_no_visits AS pmrycare_no_vsit, phealth.pmry_care_dedtcoins_aftr_cpy AS pmrycare_after_copay, prate.rate_option AS rate_option, ");
			    buildquery.append("phealth.TIER2_UTIL AS TIER2_UTIL, phealth.cost_sharing AS cost_sharing, phealth.plan_sbc_scenario_id AS sbc_scenario_id ");
				buildquery.append("FROM ");
				buildquery.append("plan_health phealth, pm_service_area psarea, pm_plan_rate prate, plan p2, pm_rating_area prarea ");
				buildquery.append("WHERE ");
				buildquery.append("p2.id = phealth.plan_id ");
	
				if (costSharing == null || costSharing.equals(PlanMgmtConstants.EMPTY_STRING) || costSharing.equalsIgnoreCase(CS1)) {
					buildquery.append(" AND phealth.parent_plan_id = 0 "); // pick all base plan of all metal tier
				} else if (costSharing.equalsIgnoreCase(CS2) || costSharing.equalsIgnoreCase(CS3)) {
					if(PlanMgmtConstants.STATE_CODE_MN.equals(stateCode) && showCatastrophicPlan)
					{
						buildquery.append(" AND (phealth.cost_sharing = UPPER(:param_" + paramList.size() + ") OR phealth.plan_level = '" + Plan.PlanLevel.CATASTROPHIC.toString() + "')");
					}
					else {
						buildquery.append(" AND phealth.cost_sharing = UPPER(:param_" + paramList.size() + ")"); // pick all respective cost sharing plan of all metal tier
					}
					paramList.add(costSharing);
				} else if (costSharing.equalsIgnoreCase(CS4) || costSharing.equalsIgnoreCase(CS5) || costSharing.equalsIgnoreCase(CS6)) {
					buildquery.append(" AND ((phealth.parent_plan_id = 0 and phealth.plan_level !='" + Plan.PlanLevel.SILVER.toString()
							+ "')  OR phealth.cost_sharing=UPPER(:param_" + paramList.size() + "))"); // pick base plan of other metal tier except silver + respective cost sharing plan
					paramList.add(costSharing);
				}
	
				buildquery.append(" AND p2.service_area_id = psarea.service_area_id ");
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND psarea.zip = to_number(trim(:param_" + paramList.size() + "))");			
					paramList.add(processedMemberData.get(i).get(ZIP));
				}else{
					buildquery.append("AND psarea.zip = :param_" + paramList.size());			
					paramList.add(processedMemberData.get(i).get(ZIP));
				}

				if (null == applicantZip) {
					applicantZip = processedMemberData.get(i).get(ZIP);
				}
				
				buildquery.append(" AND prarea.id = prate.rating_area_id ");
				buildquery.append("AND p2.id = prate.plan_id ");
				buildquery.append("AND p2.is_deleted = 'N' ");
				buildquery.append("AND psarea.is_deleted = 'N' ");
				buildquery.append("AND prate.rating_area_id = ");
				
				// -------- changes for HIX-100092 starts --------
				buildquery.append(quotingBusinessLogic.buildSearchRatingAreaLogic(stateCode, paramList, processedMemberData, i, applicableYear, configuredDB));
				// -------- changes for HIX-100092 ends --------
						  
				// prepare rate query
				buildquery.append(quotingBusinessLogic.buildRateLogic("prate", stateName, houseHoldType, familyTierLookupCode, processedMemberData.get(i).get(PlanMgmtConstants.AGE), processedMemberData.get(i).get(PlanMgmtConstants.TOBACCO), effectiveDate, paramList));
				
				// if showCatastrophicPlan == false, show all plan tier except CATASTROPHIC
				if (!showCatastrophicPlan) {
					// if we are looking for any specific plan level like gold/silver etc
					if (!planLevel.equals(PlanMgmtConstants.EMPTY_STRING)) {
						buildquery.append(" AND phealth.plan_level = :param_" + paramList.size());
						paramList.add(planLevel.toUpperCase());
					} else {
						// any metal tier except CATASTROPHIC
						buildquery.append(" AND phealth.plan_level != '" + Plan.PlanLevel.CATASTROPHIC.toString() + "'");
					}
				} else { // if showCatastrophicPlan == true
							// if we are looking for any specific plan level like gold/silver etc
					if (!planLevel.equals(PlanMgmtConstants.EMPTY_STRING)) {
						buildquery.append(" AND (phealth.plan_level = :param_" + paramList.size() + " OR phealth.plan_level = '"
								+ Plan.PlanLevel.CATASTROPHIC.toString() + "')");
						paramList.add(planLevel.toUpperCase());
					}
				}
				// if we are looking for Essential Health Benefits covered  plans
				if (!ehbCovered.equals(PlanMgmtConstants.EMPTY_STRING)) {
					buildquery.append(" AND phealth.ehb_covered = :param_" + paramList.size());
					paramList.add(ehbCovered.toUpperCase());
				}
	
				if (!StringUtils.isBlank(processedMemberData.get(i).get(COUNTYCODE))) {
					buildquery.append(" AND psarea.fips = :param_" + paramList.size());
					paramList.add(processedMemberData.get(i).get(COUNTYCODE));

					if (null == applicantCounty) {
						applicantCounty = processedMemberData.get(i).get(COUNTYCODE);
					}
				}
				buildquery.append(" ) ");
	
				if (i + 1 != processedMemberData.size()) {
					buildquery.append(" UNION ALL ");
				}
	
			}
	
			buildquery.append(") temp WHERE p.id = temp.plan_id AND net.id = p.provider_network_id AND TO_DATE (:param_" + paramList.size()
					+ ", 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date AND p.insurance_type='HEALTH' ");
			paramList.add(effectiveDate);
			
			// if issuerVerifiedFlag = false and issuer representatives comes through consumer shopping flow then skip issuer verified plans
			if (issuerVerifiedFlag && issuerId <= 0) {
				buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic(ALIAS_OF_PLAN_TABLE, true));
			}else{
				buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic(ALIAS_OF_PLAN_TABLE, false));
			}
			
			// HIX-62057 - when issuer representatives comes through consumer shopping flow then skip EnrollmentAvailability filter
			if (StringUtils.isBlank(hiosPlanNumber) && keepOnly.equalsIgnoreCase("N") && issuerId <= 0) { // if keepOnly == N, then use enrollment availability filter. ref jira HIX-48205

				List<String> currentHiosIssuerIdList = null;

				if (StringUtils.isNotBlank(currentHiosIssuerID) && PlanMgmtConstants.Y.equalsIgnoreCase(isPlanChange)) {
					currentHiosIssuerIdList = Arrays.asList(new String[] {currentHiosIssuerID});
				}

				if (isSpecialEnrollment.equalsIgnoreCase("YES")) {
					buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic(ALIAS_OF_PLAN_TABLE, effectiveDate, PlanMgmtConstants.YES, applicantZip, applicantCounty, currentHiosIssuerIdList, paramList));
				}
				else {
					buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic(ALIAS_OF_PLAN_TABLE, effectiveDate, PlanMgmtConstants.NO, applicantZip, applicantCounty, currentHiosIssuerIdList, paramList));
				}
			}
			buildquery.append(" AND p.market = :param_" + paramList.size() + " AND p.issuer_id = i.id ");
			paramList.add(marketType.toUpperCase());
			
			List<Integer> idListToInclude = new ArrayList<Integer>();
			List<Integer> idListToExclude = new ArrayList<Integer>();
			if ((planIdStr != null) && !planIdStr.equals(PlanMgmtConstants.EMPTY_STRING)) {
				buildquery.append(" AND p.id in ( :paramIds ) ");			
				for(String id : planIdStr.split(",")){
					idListToInclude.add(Integer.parseInt(id));
				}
			}
			
			if(hiosPlanNumber != null && !hiosPlanNumber.equals(PlanMgmtConstants.EMPTY_STRING)){
				buildquery.append(buildEnrollmentAvailableLogic(ALIAS_OF_PLAN_TABLE, effectiveDate, paramList));
				buildquery.append(" AND p.issuer_Plan_number = :param_" + paramList.size());
				paramList.add(hiosPlanNumber);
			}
	
			//HIX-65755 :: Eliminate PlanIds in special enrollment
			if((eliminatePlanIds != null) && !eliminatePlanIds.equals(PlanMgmtConstants.EMPTY_STRING)){
				buildquery.append(" AND p.id NOT IN ( :paramIdseliminated ) ") ;
				for(String id : eliminatePlanIds.split(",")){
					idListToExclude.add(Integer.parseInt(id));
				}
			}	
			
			// HIX-89716 :: During Issuer Rep plan shopping, ignore the restricted HIOS IDs
			if(issuerId <= PlanMgmtConstants.ZERO){
				// if restrictHiosIdListOfTenant is not empty then excludes those HIOS Issuer ids from quoting
				if((null != restrictHiosIdListOfTenant) && (restrictHiosIdListOfTenant.size() > PlanMgmtConstants.ZERO)){
					buildquery.append(" AND i.hios_issuer_id NOT IN ( :paramRestrictHiosIdList ) ") ;
				}
			}	
			
			// if issuer id passed pick that issuer specific plans
			if (issuerId > 0) {
				buildquery.append(" AND p.issuer_id = CAST(:param_" + paramList.size() + " AS INTEGER) ");
				paramList.add(String.valueOf(issuerId));
			}
			
			// HIX-79254 - On OFF exchange flow 
			// show ON exchange plan having enable_for_offexch_flow flag as YES along with OFF exchange plan
			if(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF.equalsIgnoreCase(exchangeType)){ // OFF exchange flow
				buildquery.append(" AND (p.exchange_type = 'OFF' OR (p.exchange_type = 'ON' AND UPPER(p.enable_for_offexch_flow) = 'YES')) ");
			}else{
				buildquery.append(" AND p.exchange_type = 'ON' "); // ON exchange flow
			}
			
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildquery.append(" AND p.id = tp.plan_id");
				buildquery.append(" AND tp.tenant_id = t.id");
				buildquery.append(" AND t.code = :param_" + paramList.size());
				paramList.add(tenantCode.toUpperCase());
			}
			
			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic(ALIAS_OF_PLAN_TABLE, houseHoldType));
			buildquery.append(" GROUP BY ");
			buildquery.append("p.id, p.name, net.network_url, phealth_id, parent_id, plan_level, i.name, i.logo_url, p.network_type, ehb_covered, i.id, sbc_ucm_id, ");
			buildquery.append("p.brochure, p.brochure_ucm_id, benefits_url, max_coins_spdrug, max_chr_cpy, pmrycare_no_vsit, pmrycare_after_copay, p.formularly_id, p.tp_id, p.is_puf, ");
			buildquery.append("rate_option, p.hsa, net.network_key, net.has_provider_data, p.issuer_plan_number, p.eoc_doc_id, TIER2_UTIL, p.eoc_doc_id, cost_sharing, sbc_scenario_id, ");
			buildquery.append("p.ehb_premium_fraction, net.certified, net.verified, i.hios_issuer_id, net.network_id, p.exchange_type ");
			buildquery.append("HAVING sum(memberctr) = " + processedMemberData.size());
			// HIX-79254 - On OFF exchange flow, use order by to sort OFF exchange plans as top in result set
			if(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF.equalsIgnoreCase(exchangeType)){ 
				buildquery.append(" ORDER BY p.exchange_type ASC "); 
			}

			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL == " + buildquery.toString());
			}
			
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			
			if(idListToInclude.size() != 0){
				query.setParameter("paramIds",idListToInclude);
			}
			
			if(idListToExclude.size() != 0){
				query.setParameter("paramIdseliminated",idListToExclude);
			}
			
			// HIX-89716 :: During Issuer Rep plan shopping, ignore the restricted HIOS IDs
			if(issuerId <= PlanMgmtConstants.ZERO){
				if((null != restrictHiosIdListOfTenant) && (restrictHiosIdListOfTenant.size() > PlanMgmtConstants.ZERO)){
					query.setParameter("paramRestrictHiosIdList", restrictHiosIdListOfTenant);
				}
			}

			List rsList = query.getResultList();
			
			Iterator<?> rsIterator = rsList.iterator();
			Iterator<?> rsIterator2 = rsList.iterator();
			Iterator<?> rsIterator3 = rsList.iterator();
			Iterator<?> rsIterator4 = rsList.iterator();
			
			List<Integer> planHealthIdList = new ArrayList<Integer>();
			StringBuilder parentPlanIdStr = new StringBuilder(1024);
			StringBuilder hiosPlanIds = new StringBuilder(1024);
			Map<Integer, Integer> parrentPlanHealthIdArr = new HashMap<Integer, Integer>();
			List<String> hiosPlaIdList = new ArrayList<String>();
			List<String> hiosPlanIdListForQualityRating = new ArrayList<String>();
			List<String> hiosPlanIdListForNetworkTransparencyRating = new ArrayList<String>();
			BigDecimal planHealthId = null;
			Object[] objArray = null;
			String logoURL = null;
			String sbcDocURL = null;
			String benefitURL = null;
			PlanRateBenefit planMap = null;
			String rateOption = null;
			Float householdPremium = null;
			String returnedString = null;
			String providerUrl = null;
			String brochureUrl = null;
			String brochureUCM = null;
			List<PrescriptionSearchDTO> prescriptionSearchDTOList = null;
			Integer issuerIdForDrugName = null;
			List<Map<String, String>> memberInfo = null;
			Map<String, String> memberData = null;
			Integer formularlyId = PlanMgmtConstants.ZERO;
			String formularyUrl = null;
			String formularyIdStr = null;
			RatingArea ratingArea = null;
			String networKey = null;
			String networkVerificationStatus = null;
			String hasProviderData = null;
			List<Map<String, String>> associatedProviders = null;
			String hsaString =  null;
			String[] memberids = null;
			String[] memberpremiums = null;
			String[] memberregions = null;
			String applicantCountyFips = PlanMgmtConstants.EMPTY_STRING;
			List<Integer> sbcScenarioIdList = new ArrayList<Integer>();
			Map<String, String> memberHavingRatingAreaMap = new HashMap<String, String>();
					
			while (rsIterator.hasNext()) {
				logoURL = null;
				sbcDocURL = PlanMgmtConstants.EMPTY_STRING;
				benefitURL = PlanMgmtConstants.EMPTY_STRING;
				planMap = new PlanRateBenefit();
				objArray = (Object[]) rsIterator.next();
				rateOption = objArray[PlanMgmtConstants.TWENTYSIX].toString();
				householdPremium = Float.parseFloat(objArray[PlanMgmtConstants.THREE].toString());
				// if rate option is Family, then premium would be on family level, don't consider each quoted member's premium
				if (rateOption.equalsIgnoreCase("F")) {
					householdPremium = (householdPremium / processedMemberData.size());
				}
				planMap.setId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
				planMap.setName(objArray[PlanMgmtConstants.ONE].toString());
				planMap.setLevel(objArray[PlanMgmtConstants.TWO].toString());
				planMap.setPremium(householdPremium);
				planMap.setNetworkType((objArray[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOUR].toString());
				planMap.setIssuer(objArray[PlanMgmtConstants.FIVE].toString());
				planMap.setIssuerText(objArray[PlanMgmtConstants.FIVE].toString().toLowerCase().replaceAll("\\s|\\.", PlanMgmtConstants.EMPTY_STRING));
	
				// concatenating plan_health_ids to generate a single query
				planHealthId = (BigDecimal)objArray[PlanMgmtConstants.SIX];
				planHealthIdList.add(planHealthId.intValue());
	
				// for cost sharing child plans (CS2,CS3,CS4,CS5,CS6) concatenating parent plan_ids to generate a single query
				if (Integer.parseInt(objArray[PlanMgmtConstants.SEVEN].toString()) > PlanMgmtConstants.ZERO) {
					int parentPlanId = Integer.parseInt(objArray[PlanMgmtConstants.SEVEN].toString());
					parentPlanIdStr.append((parentPlanIdStr.length() == PlanMgmtConstants.ZERO) ? parentPlanId  : "," + parentPlanId + PlanMgmtConstants.EMPTY_STRING);
				}
	
				planMap.setEhbCovered((objArray[PlanMgmtConstants.EIGHT] != null) ? objArray[PlanMgmtConstants.EIGHT].toString() : PlanMgmtConstants.EMPTY_STRING);
				planMap.setIssuerId(Integer.parseInt(objArray[PlanMgmtConstants.NINE].toString()));
	
				memberids = objArray[PlanMgmtConstants.TEN].toString().split(",");
				memberpremiums = objArray[PlanMgmtConstants.ELEVEN].toString().split(",");
				memberregions = objArray[PlanMgmtConstants.TWELVE].toString().split(",");
	
				if (objArray[PlanMgmtConstants.THIRTEEN] != null) {
					sbcDocURL = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.THIRTEEN].toString());
				}
	
				if (objArray[PlanMgmtConstants.FIFTEEN] != null) {
	
					if (PlanMgmtUtil.isURLValid(objArray[PlanMgmtConstants.FIFTEEN].toString())) {
						benefitURL = objArray[PlanMgmtConstants.FIFTEEN].toString();
					} else {
						benefitURL = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.FIFTEEN].toString());
					}
				}
	
				// Adding Regular expression for adding HTTP for HIX-25317 
				if (!benefitURL.equals(PlanMgmtConstants.EMPTY_STRING) && !benefitURL.matches(PlanMgmtConstants.URL_REGEX)) {
					benefitURL = "http://" + benefitURL;				
				}
	
				returnedString = PlanMgmtUtil.returnOneString(sbcDocURL, benefitURL);
				if (!returnedString.isEmpty()) {
					planMap.setSbcDocUrl(returnedString);
				} else {
					planMap.setSbcDocUrl(PlanMgmtConstants.EMPTY_STRING);
				}
	
				// Append http:// in plan brochure and provider link url if does not exists
				providerUrl = (objArray[PlanMgmtConstants.SIXTEEN] != null) ? objArray[PlanMgmtConstants.SIXTEEN].toString() : PlanMgmtConstants.EMPTY_STRING;
				/* Adding Regular expression for adding HTTP for HIX-25317	 */ 
				if (!StringUtils.isBlank(providerUrl) && !providerUrl.matches(PlanMgmtConstants.URL_REGEX)) {
					providerUrl = "http://" + providerUrl;				
				}
	
				brochureUrl = (objArray[PlanMgmtConstants.FOURTEEN] != null) ? objArray[PlanMgmtConstants.FOURTEEN].toString() : PlanMgmtConstants.EMPTY_STRING;
				brochureUCM = (objArray[PlanMgmtConstants.EIGHTEEN] != null) ? objArray[PlanMgmtConstants.EIGHTEEN].toString() : PlanMgmtConstants.EMPTY_STRING;
	
				if (brochureUCM != null && !brochureUCM.equals(PlanMgmtConstants.EMPTY_STRING)) {
					brochureUrl = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.EIGHTEEN].toString());
				} else {
					// Adding Regular expression for adding HTTP for HIX-25317
					if (!brochureUrl.equals(PlanMgmtConstants.EMPTY_STRING) && !brochureUrl.matches(PlanMgmtConstants.URL_REGEX)) {
						brochureUrl = "http://" + brochureUrl;				
					}
				}
	
				planMap.setPlanBrochureUrl(brochureUrl);
				planMap.setProviderLink(providerUrl);
				if(null != objArray[PlanMgmtConstants.SEVENTEEN]) {
					logoURL = objArray[PlanMgmtConstants.SEVENTEEN].toString();
				}
				//planMap.setIssuerLogo((objArray[PlanMgmtConstants.SEVENTEEN] != null) ? appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.SEVENTEEN].toString()) : PlanMgmtConstants.EMPTY_STRING);
				planMap.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL,  objArray[PlanMgmtConstants.THIRTYSEVEN].toString(), null, appUrl));
				planMap.setMaxCoinseForSpecialtyDrugs((objArray[PlanMgmtConstants.NINTEEN] != null) ? objArray[PlanMgmtConstants.NINTEEN].toString() : PlanMgmtConstants.EMPTY_STRING);
				planMap.setMaxNumDaysForChargingInpatientCopay((objArray[PlanMgmtConstants.TWENTY] != null) ? objArray[PlanMgmtConstants.TWENTY].toString() : PlanMgmtConstants.EMPTY_STRING);
				planMap.setPrimaryCareCostSharingAfterSetNumberVisits((objArray[PlanMgmtConstants.TWENTYONE] != null) ? objArray[PlanMgmtConstants.TWENTYONE].toString() : PlanMgmtConstants.EMPTY_STRING);
				planMap.setPrimaryCareDeductOrCoinsAfterSetNumberCopays((objArray[PlanMgmtConstants.TWENTYTWO] != null) ? objArray[PlanMgmtConstants.TWENTYTWO].toString() : PlanMgmtConstants.EMPTY_STRING);
	
				if (objArray[PlanMgmtConstants.TWENTYTHREE] != null && StringUtils.isNotBlank(objArray[PlanMgmtConstants.TWENTYTHREE].toString()) ) {
					formularlyId = Integer.parseInt(objArray[PlanMgmtConstants.TWENTYTHREE].toString().trim());
					formularyIdStr = objArray[PlanMgmtConstants.TWENTYTHREE].toString().trim();
					formularyUrl = getFormularyURL(formularlyId);
					planMap.setFormularyUrl(formularyUrl);
				}else{
					planMap.setFormularyUrl(PlanMgmtConstants.EMPTY_STRING);
					formularyIdStr = PlanMgmtConstants.EMPTY_STRING;
				}
				if (objArray[PlanMgmtConstants.TWENTYFOUR] != null) {
					planMap.setTpId(objArray[PlanMgmtConstants.TWENTYFOUR].toString());
				} else {
					planMap.setTpId(PlanMgmtConstants.EMPTY_STRING);
				}
	
				if (objArray[PlanMgmtConstants.TWENTYFIVE] != null) {
					planMap.setIsPuf(objArray[PlanMgmtConstants.TWENTYFIVE].toString());
				} else {
					planMap.setIsPuf(PlanMgmtConstants.EMPTY_STRING);
				}
				
				// New service for getting drug information
				if (null != prescriptionSearchRequstList && !(prescriptionSearchRequstList.isEmpty())) {
					prescriptionSearchDTOList = new ArrayList<PrescriptionSearchDTO>();
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("-- getting Prescription Search Information --");
					}
					issuerIdForDrugName = Integer.parseInt(planMap.getIssuerId().toString());
					prescriptionSearchDTOList = prescriptionSearchService.getPrescriptionSearchDrugInformation(formularyIdStr, issuerIdForDrugName, prescriptionSearchRequstList);
					planMap.setPrescriptionSearchDTOList(prescriptionSearchDTOList);
				}
	
				// calculate cost, region for each member
				memberInfo = new ArrayList<Map<String, String>>();
				for (Map<String, String> inputMemberData : memberList) {
					memberData = new HashMap<String, String>();
	
					memberData.put("id", inputMemberData.get(ID));
					memberData.put("premium", PlanMgmtConstants.ZERO_STR);
					for (int ctr = 0; ctr < memberids.length; ctr++) {
						if (memberids[ctr].equalsIgnoreCase(inputMemberData.get(ID))) {
							// if rate option is family i.e. community rating, 
							// then set premium for applicant only and other family member's premium would be zero. 
							if (rateOption.equalsIgnoreCase("F")){
								if(inputMemberData.get(RELATION).equalsIgnoreCase(PlanMgmtConstants.MEMBER) || inputMemberData.get(RELATION).equalsIgnoreCase(PlanMgmtConstants.SELF)) {
									memberData.put("premium", memberpremiums[ctr].trim());
								}														
							}else{
								memberData.put("premium", memberpremiums[ctr].trim());
							}	
							memberData.put("region", memberregions[ctr].replaceAll("[a-zA-Z ]", PlanMgmtConstants.EMPTY_STRING));
							
							if(inputMemberData.get(RELATION).equalsIgnoreCase(PlanMgmtConstants.MEMBER) || inputMemberData.get(RELATION).equalsIgnoreCase(PlanMgmtConstants.SELF)) {
								applicantCountyFips = inputMemberData.get(PlanMgmtConstants.COUNTYCODE);
							}
						}
	
					}
					if (memberData.get("region") == null) 
					{
						// if member's rating area is already computed then don't fire query on DB to pull rating area for corresponding member
						if(null != memberHavingRatingAreaMap.get(memberData.get("id")))
						{
							memberData.put("region",  memberHavingRatingAreaMap.get(memberData.get("id")));
						}
						else
						{						
							// if state exchange is ID then, then use applicable year filter to pull rating area 
							if(PlanMgmtConstants.STATE_CODE_ID.equalsIgnoreCase(stateCode))
							{
								ratingArea = ratingAreaService.getRatingAreaByYear(inputMemberData.get(ZIP), inputMemberData.get(COUNTYCODE), applicableYear);
							}else{
								ratingArea = ratingAreaService.getRatingAreaByZip(inputMemberData.get(ZIP), inputMemberData.get(COUNTYCODE));
							}
						    
							if (ratingArea != null && ratingArea.getRatingArea() != null) {
								memberData.put("region", ratingArea.getRatingArea().replaceAll("[a-zA-Z ]", PlanMgmtConstants.EMPTY_STRING));
							} else {
								memberData.put("region", memberregions[0].replaceAll("[a-zA-Z ]", PlanMgmtConstants.EMPTY_STRING));
							}
						}
					}
					
					if(null != memberData.get("region") && null == memberHavingRatingAreaMap.get(memberData.get("id")) ){
						memberHavingRatingAreaMap.put(memberData.get("id"),  memberData.get("region"));
					}
					
					memberInfo.add(memberData);
				}
	
				planMap.setPlanDetailsByMember(memberInfo);
	
				networKey = PlanMgmtConstants.EMPTY_STRING;
				networkVerificationStatus = PlanMgmtConstants.NO;
				
				// Ref jira HIX-87747
				// set plan network key 
				// if state exchange is CA then use network.network_id as plan network key.
				if(PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode))
				{
					if(null != objArray[PlanMgmtConstants.THIRTYEIGHT] && null != objArray[PlanMgmtConstants.THIRTYSEVEN]){
						networKey = PlanMgmtUtil.formatNetworkKey(objArray[PlanMgmtConstants.THIRTYEIGHT].toString(), objArray[PlanMgmtConstants.THIRTYSEVEN].toString(), applicableYear, stateCode);
					}
				}
				else	// if state exchange other than CA then use network.network_key as plan network key.
				{	
					if(null != objArray[PlanMgmtConstants.TWENTYEIGHT] && null != objArray[PlanMgmtConstants.THIRTYSEVEN] ){
						networKey = PlanMgmtUtil.formatNetworkKey(objArray[PlanMgmtConstants.TWENTYEIGHT].toString(), objArray[PlanMgmtConstants.THIRTYSEVEN].toString(), applicableYear, stateCode);
					}
				}	
									
				//HIX-74017, consider those networks which are certified and verified both are set as YES.
				if(objArray[PlanMgmtConstants.TWENTYEIGHT] != null && objArray[PlanMgmtConstants.THIRTYFIVE] != null && objArray[PlanMgmtConstants.THIRTYSIX] != null){
					// in consumer shopping flow form issuer portal, ignore network verified status filter use only certified filter 
					if (issuerId > 0 && objArray[PlanMgmtConstants.THIRTYFIVE].toString().equalsIgnoreCase(PlanMgmtConstants.YES) ) {
						networkVerificationStatus = PlanMgmtConstants.YES; 
					}
					// in anonymous or individual flow, use network verified status and certified filter  
					else if(issuerId <= 0 && objArray[PlanMgmtConstants.THIRTYFIVE].toString().equalsIgnoreCase(PlanMgmtConstants.YES)  && objArray[PlanMgmtConstants.THIRTYSIX].toString().equalsIgnoreCase(PlanMgmtConstants.YES) ) {
						networkVerificationStatus = PlanMgmtConstants.YES; 
					}
				}
				
				
				hasProviderData = (objArray[PlanMgmtConstants.TWENTYNINE] != null) ? objArray[PlanMgmtConstants.TWENTYNINE].toString() : PlanMgmtConstants.EMPTY_STRING;				
			    associatedProviders = providerService.isPlanBelongsToProviderNetwork(providerList, networKey, hasProviderData, networkVerificationStatus, stateCode);
				planMap.setProviders(associatedProviders);
				
				//	adding new field as per JIRA: HIX-34301
				hsaString = (objArray[PlanMgmtConstants.TWENTYSEVEN] != null) ? objArray[PlanMgmtConstants.TWENTYSEVEN].toString() : PlanMgmtConstants.EMPTY_STRING;
				planMap.setHsa(hsaString);
				
				// prepare hios plan ids, 
				// take first 14 digits from plan table, because puf table have 14 digit hios plan number without standard component id			
				hiosPlanIds.append((hiosPlanIds.length() == 0) ? objArray[PlanMgmtConstants.THIRTY].toString().substring(0,14) : "," + objArray[PlanMgmtConstants.THIRTY].toString().substring(0,14));
				
				// reset networkKey
				networKey = PlanMgmtConstants.EMPTY_STRING;
				
				// ref jira HIX-98476 : send network key
				// if state exchange is CA then use network.network_id as plan network key.
				if(PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(stateCode))
				{
					if(null != objArray[PlanMgmtConstants.THIRTYEIGHT] && null != objArray[PlanMgmtConstants.THIRTYSEVEN]){
						networKey = PlanMgmtUtil.formatNetworkKey(objArray[PlanMgmtConstants.THIRTYEIGHT].toString(), objArray[PlanMgmtConstants.THIRTYSEVEN].toString(), applicableYear, stateCode);
					}
				}
				else	// if state exchange other than CA then use network.network_key as plan network key.
				{
					if(objArray[PlanMgmtConstants.TWENTYNINE] != null && objArray[PlanMgmtConstants.TWENTYNINE].toString().equalsIgnoreCase(PlanMgmtConstants.YES)){
						networKey = (objArray[PlanMgmtConstants.TWENTYEIGHT] != null) ? objArray[PlanMgmtConstants.TWENTYEIGHT].toString() : PlanMgmtConstants.EMPTY_STRING;
					}
				}	
				
				planMap.setNetworkKey(networKey);
				planMap.setTier2util((objArray[PlanMgmtConstants.THIRTYONE] != null) ? objArray[PlanMgmtConstants.THIRTYONE].toString() : PlanMgmtConstants.EMPTY_STRING);
				String eocDocURL = null;
				if(null != objArray[PlanMgmtConstants.THIRTYTWO]) {
					if (objArray[PlanMgmtConstants.THIRTYTWO].toString().matches(PlanMgmtConstants.ECM_REGEX)) {
						eocDocURL = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.THIRTYTWO].toString());    
					} else {
						eocDocURL = objArray[PlanMgmtConstants.THIRTYTWO].toString();    
					}
				} else {
					eocDocURL = PlanMgmtConstants.EMPTY_STRING;
				}
				planMap.setEocDocUrl(eocDocURL);
				planMap.setCostSharing((objArray[PlanMgmtConstants.THIRTYTHREE] != null) ? objArray[PlanMgmtConstants.THIRTYTHREE].toString().trim(): PlanMgmtConstants.EMPTY_STRING);
				
				// if considerEHB flag is ON and plan have EHB percentage HIX-65077, fetch ehb portion from plan table.
				if(considerEHBPortion.equalsIgnoreCase("Y") && objArray[PlanMgmtConstants.THIRTYFOUR] != null){				
					planMap.setEhbPercentage(objArray[PlanMgmtConstants.THIRTYFOUR].toString());	
				}
				
				// ref HIX-88018 : Return plan's exchange type
				planMap.setExchangeType((objArray[PlanMgmtConstants.THIRTYNINE] != null) ? objArray[PlanMgmtConstants.THIRTYNINE].toString() : PlanMgmtConstants.EMPTY_STRING);
				
				planMap.setHiosPlanNumber(objArray[PlanMgmtConstants.THIRTY].toString());
				if(computeIssuerQualityRating.equalsIgnoreCase(PlanMgmtConstants.YES)){
					//hiosPlaIdListForQualityRating.add(objArray[PlanMgmtConstants.THIRTY].toString().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FOURTEEN));
					hiosPlanIdListForQualityRating.add(objArray[PlanMgmtConstants.THIRTY].toString());
				}
				
				// HIX-87498, if planManagement.computeNetworkTransparencyRating flag == YES, then compute network transparency rating 
				if(PlanMgmtConstants.YES.equalsIgnoreCase(computeNetworkTransparencyRating)){
					hiosPlanIdListForNetworkTransparencyRating.add(objArray[PlanMgmtConstants.THIRTY].toString().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FOURTEEN));
				}
				
				// HIX-79254, On OFF exchange flow, when we show ON and OFF Exchange plans together, OFF exchange plan goes for first priority
				// if any ON exchange plan have same HIOS plan number (14 digit) in OFF exchange plan list then don't include that ON exchange plan in response.    
				if(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF.equalsIgnoreCase(exchangeType)){ 
					if(hiosPlaIdList != null && !hiosPlaIdList.contains(objArray[PlanMgmtConstants.THIRTY].toString().substring(0,PlanMgmtConstants.FIFTEEN))){			
						hiosPlaIdList.add(objArray[PlanMgmtConstants.THIRTY].toString().substring(0, PlanMgmtConstants.FIFTEEN));
						planRateBenefitList.add(planMap);
					}	
				}else{
					planRateBenefitList.add(planMap);
				}
				
				// HIX-99135
				sbcScenarioIdList.add(Integer.parseInt(objArray[PlanMgmtConstants.FOURTY].toString()));
				
				planMap = null;
			}
			
			// if cost sharing child plans exists in result set, pull plan_health_id of parent plan from plan_health table 
			if (parentPlanIdStr.length() > PlanMgmtConstants.ZERO) {
							
				Connection conn = null;
				ResultSet rs = null;
				PreparedStatement statement=null;
				try {
		            
		            Context initialContext = new InitialContext();
				    DataSource datasource = (DataSource)initialContext.lookup(PlanMgmtConstants.DATASRC);
				    if (datasource != null) {
				    	int parentPlanHealthId;
			    		int parentPlanId;
			    		StringBuilder buildquery2 = new StringBuilder();
			    		StringBuilder questionMark =  new StringBuilder();		    		
			    		String[] PlanIdArr = parentPlanIdStr.toString().split(",");
			    		for(String planId : PlanIdArr){
			    			questionMark.append(" ?,");
			    		}
			    		
			    		buildquery2.append("SELECT id, plan_id FROM plan_health where plan_id in (");
			    		buildquery2.append(questionMark.substring(0, questionMark.length() -1));
			    		buildquery2.append(" )");
			    		
			    		conn = datasource.getConnection();	
			    		statement = conn.prepareStatement(buildquery2.toString());
			    		int index = 1;
			    		for(String planId : PlanIdArr){
			    			statement.setInt(index, Integer.parseInt(planId));
			    			index++;
			    		}
			    		statement.setFetchSize(PlanMgmtConstants.FETCHSIZE);
			    		if(LOGGER.isDebugEnabled()) {
			    			LOGGER.debug("SQL :: " + buildquery2 );
			    		}
			    		rs = statement.executeQuery();
			    				    		
			            while ( rs.next() ) {						
							parentPlanHealthId =  Integer.parseInt(rs.getString("id"));
							parentPlanId = Integer.parseInt(rs.getString("plan_id"));	
							// concatenating plan_health_ids to generate a single query				 
							planHealthIdList.add(rs.getInt("id"));
							parrentPlanHealthIdArr.put(parentPlanId, parentPlanHealthId);    
						}	            
				    } 
		        } catch (Exception e) {
		        	LOGGER.error("Got an exception! Exception ==> " , e );           
		        }finally{
		        	PlanMgmtUtil.closeResultSet(rs);
		        	PlanMgmtUtil.closePreparedStatement(statement);
		        	PlanMgmtUtil.closeDBConnection(conn);	   
		        }	
			 }
			
			//LOGGER.info("sbcScenarioIdList " + sbcScenarioIdList.toString());
			
			List<PlanSbcScenario> planSbcScenarioList = new ArrayList<PlanSbcScenario>(); 
			
			// HIX-99135 :: pull sbc scenario from plan_sbc_scenario table
			if(!sbcScenarioIdList.isEmpty()){
				planSbcScenarioList = planSbcScenarioService.getSbcScenarioIdList(sbcScenarioIdList);
			}
			
			Object[] objArray4 = null;
			SbcScenarioDTO sbcScenarioDTO = null;
			Integer planId4 = null; 
			
			while (rsIterator4.hasNext()) {
				objArray4 = (Object[]) rsIterator4.next();
				planId4 = Integer.parseInt(objArray4[0].toString());
				for (PlanRateBenefit planObj : planRateBenefitList) {
					if (planId4.equals(planObj.getId())) {
						// HIX-99135 :: set SBC scenario information in response
						for(PlanSbcScenario planSbcScenario : planSbcScenarioList){
							if(planSbcScenario.getId() ==  Integer.parseInt(objArray4[PlanMgmtConstants.FOURTY].toString())){
								planObj.setSbcScenarioDTO(planSbcScenarioService.setSbcScenario(planSbcScenario, sbcScenarioDTO));
							}
						}
					}		
				}
			}	
			
			if (!minimizePlanData) {			
				Map<Integer, Map<String, Map<String, String>>> benefitData = new HashMap<Integer, Map<String, Map<String, String>>>();			
				Map<Integer, Map<String, Map<String, String>>> planCosts = new HashMap<Integer, Map<String, Map<String, String>>>();
				
				
				// fire single query to fetch plan benefits and plan costs
				if (!(planHealthIdList.isEmpty())){
					benefitData = planCostAndBenefitsService.getOptimizedHealthPlanBenefits(planHealthIdList, effectiveDate);
					planCosts = planCostAndBenefitsService.getOptimizedHealthPlanCosts(planHealthIdList);
				}
				
				List<String> costList = getCostNames();
				Object[] objArray1 = null;
			    BigDecimal planHealthId2 = null;
			    Integer planId = null; 
			    Integer parentPlanId = null;
			    Map<String, Map<String,String>> costMap = null;
			    Map<String, Map<String,String>> planCostMap = null;
			    Map<String, Map<String,String>> optionalPlanCostMap = null;
			    Set<String> costNameSet =null;
			    Map<String, Map<String,String>> planBaseCosts = null;
			    String parentHealthId = null;
			    
				while (rsIterator2.hasNext()) {
					objArray1 = (Object[]) rsIterator2.next();
					planHealthId2 = (BigDecimal)objArray1[PlanMgmtConstants.SIX];
					planId = Integer.parseInt(objArray1[0].toString());
					parentPlanId = Integer.parseInt(objArray1[PlanMgmtConstants.SEVEN].toString());
	
					for (PlanRateBenefit planObj : planRateBenefitList) {
						if (planId.equals(planObj.getId())) {
							
							if (benefitData.containsKey(planHealthId2.intValue())) {
								planObj.setPlanBenefits(benefitData.get(planHealthId2.intValue()));
	//							planObj.setPlanCosts(planCosts.get(planHealthId));
								costMap = planCosts.get(planHealthId2.intValue());
								costNameSet = costMap.keySet();
								planCostMap = new HashMap<String, Map<String,String>>();
								optionalPlanCostMap = new HashMap<String, Map<String,String>>();
								
								for (String costName: costNameSet){
									if(costList.contains(costName)){
										planCostMap.put(costName, costMap.get(costName));
									}else{
										optionalPlanCostMap.put(costName, costMap.get(costName));
									}
								}
								planObj.setPlanCosts(planCostMap);
								planObj.setOptionalDeductible(optionalPlanCostMap);
								planCostMap = null;
								optionalPlanCostMap = null;
							}
							
							// for cost sharing child plans (CS2,CS3,CS4,CS5,CS6) set base plan benefits and costs
							if (parentPlanId > 0 && parrentPlanHealthIdArr.containsKey(parentPlanId)) {
								parentHealthId = parrentPlanHealthIdArr.get(parentPlanId).toString();
								if (benefitData.containsKey(parentHealthId)) {
									planObj.setPlanBaseBenefits(benefitData.get(parentHealthId));
									costMap = planCosts.get(parentHealthId);
									costNameSet = costMap.keySet();
									planBaseCosts = new HashMap<String, Map<String,String>>();
									optionalPlanCostMap = new HashMap<String, Map<String,String>>();
									for (String costName: costNameSet){
										if(costList.contains(costName)){
											planBaseCosts.put(costName, costMap.get(costName));
										}else{
											optionalPlanCostMap.put(costName, costMap.get(costName));
										}
									}
									planObj.setPlanBaseCosts(planBaseCosts);
									planObj.setOptionalDeductible(optionalPlanCostMap);
									planBaseCosts = null;
									optionalPlanCostMap = null;
								}
							}
							
						}
					}
					
				}
			}
			
			// HIX-56426 : Code added to optimize Issuer Quality Rating calls
			Map<String, Map<String, String>> issuerQualityRating = new LinkedHashMap<String, Map<String, String>>();
			if (computeIssuerQualityRating.equalsIgnoreCase(PlanMgmtConstants.YES) && !hiosPlanIdListForQualityRating.isEmpty()) {
				issuerQualityRating = issuerQualityRatingService.getIssuerQualityRatingByHIOSPlanIdList(hiosPlanIdListForQualityRating, effectiveDate, stateCode);	
				Object[] objArray2 = null;
				Integer planId = null;
				while (rsIterator3.hasNext()) {
					objArray2 = (Object[]) rsIterator3.next();
					planId = Integer.parseInt(objArray2[0].toString());
					for (PlanRateBenefit planObj : planRateBenefitList) {
						if (planId.equals(planObj.getId())) {						
							//planObj.setIssuerQualityRating(issuerQualityRating.get(planObj.getHiosPlanNumber().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FOURTEEN)));
							planObj.setIssuerQualityRating(issuerQualityRating.get(planObj.getHiosPlanNumber()));
							break;
						}
					}
				}
			}
			

			// HIX-87498 : compute Network Transparency Rating  
			if(!hiosPlanIdListForNetworkTransparencyRating.isEmpty()){
				planRateBenefitList = networkTansparencyRatingService.setTansparencyRatingService(planRateBenefitList, hiosPlanIdListForNetworkTransparencyRating, applicantCountyFips, Integer.parseInt(applicableYear));
			}	
				
		
			// pull PUF plans applicable only for private exchange
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("EXCHANGETYPE: " + SecurityUtil.sanitizeForLogging(currentExchangeType) + " ::::::::  tenant =" + SecurityUtil.sanitizeForLogging(tenantCode) + "exchangeType = " + SecurityUtil.sanitizeForLogging(exchangeType));
			}
		
			// HIX-80502 show/hide PUF plans on depends on tenant configuration. Show PUF plans when showPufPlans is true
			
			if(showPufPlans){  
				// For off-exchange, we don't need to show the PUF plans : for HIX-42652
				// if plan id passes in request then don't send PUF plan in response HIX-52995
				// when issuer representative comes through consumer shopping flow then don't pull PUF plans. ref HIX-80226
				// if HIOS plan number passed through request then don't pull PUF plans. ref HIX-80662
				if (PlanMgmtConstants.EMPTY_STRING.equals(planIdStr) && issuerId <= 0 && PlanMgmtConstants.EMPTY_STRING.equalsIgnoreCase(hiosPlanNumber) ) {
	
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug(" :::::::: Trying to get PUF QHP plans ");
					}
					// pulling puff plans
					
					pufPlanData = pufPlanService.getPufPlanData(processedMemberData.get(0).get(ZIP), processedMemberData.get(0).get(COUNTYCODE), hiosPlanIds.toString(), processedMemberData, stateName, houseHoldType, Plan.PlanInsuranceType.HEALTH.toString(), applicableYear );
					
					// if we have puff plan then club puff plan with on/off exchange plans	
					if(pufPlanData != null && pufPlanData.size() > PlanMgmtConstants.ZERO){
						planRateBenefitList.addAll(pufPlanData);
						if(LOGGER.isDebugEnabled()) {
							LOGGER.debug(" :::::::: Added PUF data ... record count " + pufPlanData.size());
						}
					}
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getHouseholdhealthPlanRateBenefits", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return planRateBenefitList;
	}

	/**
	 * Method is used to build Enrollment Available Logic for HEALTH / DENTAL plans.
	 */
	private String buildEnrollmentAvailableLogic(String aliasOfTable, String effectiveDate, List<String> paramList) {

		StringBuilder buildquery = new StringBuilder();
		buildquery.append(" AND ( (");

		buildquery.append(aliasOfTable);
		buildquery.append(".enrollment_avail IN ('");
		buildquery.append(Plan.EnrollmentAvail.AVAILABLE.toString());
		buildquery.append("', '");
		buildquery.append(Plan.EnrollmentAvail.DEPENDENTSONLY.toString());
		buildquery.append("') ");

		buildquery.append(" AND TO_DATE (  :param_" + paramList.size());
		paramList.add(effectiveDate);
		buildquery.append(" , 'YYYY-MM-DD') >= ");
		buildquery.append(aliasOfTable);
		buildquery.append(".enrollment_avail_effdate  AND ( TO_DATE (  :param_" + paramList.size());
		paramList.add(effectiveDate);
		buildquery.append(", 'YYYY-MM-DD') < ");
		buildquery.append(aliasOfTable);
		buildquery.append(".future_erl_avail_effdate OR  ");
		buildquery.append(aliasOfTable);
		buildquery.append(".future_erl_avail_effdate is null) ");

		buildquery.append(" ) OR ( ");

		buildquery.append(aliasOfTable);
		buildquery.append(".future_enrollment_avail IN ('");
		buildquery.append(Plan.EnrollmentAvail.AVAILABLE.toString());
		buildquery.append("', '");
		buildquery.append(Plan.EnrollmentAvail.DEPENDENTSONLY.toString());
		buildquery.append("') ");

		buildquery.append(" AND TO_DATE (  :param_" + paramList.size());
		paramList.add(effectiveDate);
		buildquery.append( " , 'YYYY-MM-DD') >= ");
		buildquery.append(aliasOfTable);
		buildquery.append(".future_erl_avail_effdate ");

		buildquery.append(" )) ");
		return buildquery.toString();
	}

	@Transactional(readOnly = true)
	private List<PlanRateBenefit> getHouseholdDentalPlanRateBenefits(List<Map<String, String>> memberList,
			String effectiveDate, String planIdStr, String planLevel, String marketType, String isSpecialEnrollment,
			int issuerId, boolean issuerVerifiedFlag, String exchangeType, String tenantCode, String hiosPlanNumber,
			String keepOnly, boolean minimizePlanData, String eliminatePlanIds, String restrictHiosIds,
			String showPUFPlans, String currentHiosIssuerID, String isPlanChange) {
		
		List<PlanRateBenefit> plan = new ArrayList<PlanRateBenefit>();
		EntityManager entityManager = null;

		try{
			entityManager = emf.createEntityManager();
			List<PlanRateBenefit> pufPlanData = new ArrayList<PlanRateBenefit>();
			List<Map<String, String>> modifiedMemberList = quotingBusinessLogic.processMemberData(memberList,Plan.PlanInsuranceType.DENTAL.toString());
			String houseHoldType = quotingBusinessLogic.getHouseHoldType(modifiedMemberList, Plan.PlanInsuranceType.DENTAL.toString());
			String stateName = quotingBusinessLogic.getApplicantStateCode(modifiedMemberList);
			String familyTier = quotingBusinessLogic.getFamilyTier(modifiedMemberList); // compute family tier
			String familyTierLookupCode = quotingBusinessLogic.getFamilyTierLookupCode(familyTier); // compute family tier lookup code
			String considerEHBPortion = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.CONSIDER_EHB_PORTION);
			String IHCfamilyTierLookupCode = PlanMgmtConstants.EMPTY_STRING;
			String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
			int applicantAge = quotingBusinessLogic.getApplicantAge(modifiedMemberList);
		 	boolean doQuoteIssuerIHC = quotingBusinessLogic.isAgeAllowForIHCQuoting(applicantAge);
		 	List<String> restrictHiosIdListOfTenant = null;
		 	boolean showPufPlans = false;
		 	String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		 	String applicableYear = effectiveDate.substring(0,4);
		 		
		 	// if exchange type is PHIX
		 	if (currentExchangeType.equals(GhixConstants.PHIX)){
		 		//  HIX-53639 - compute family tier look up value for carrier IHC, when exchange is PHIX and quoting for OFF exchange plans 
		 		if(exchangeType.equalsIgnoreCase(Plan.EXCHANGE_TYPE.OFF.toString())){
					IHCfamilyTierLookupCode = quotingBusinessLogic.getIHCFamilyTierLookupCode(memberList);
				}
		 		// pull tenant's show PUF plan configuration
		 		if(StringUtils.isNotBlank(showPUFPlans) && PlanMgmtConstants.YES.equalsIgnoreCase(showPUFPlans)) {
		 			showPufPlans = true;	 			
		 		} else if(StringUtils.isNotBlank(showPUFPlans) && PlanMgmtConstants.NO.equalsIgnoreCase(showPUFPlans)) {
		 			showPufPlans = false;
		 		} else if(StringUtils.isBlank(showPUFPlans)){
					if(null != tenantService.getTenant(tenantCode).getConfiguration().getPlansConfiguration() && null != tenantService.getTenant(tenantCode).getConfiguration().getPlansConfiguration().getShowPufPlans() && PlanMgmtConstants.YES.equalsIgnoreCase(tenantService.getTenant(tenantCode).getConfiguration().getPlansConfiguration().getShowPufPlans().toString()) ){
						showPufPlans = true;
					}
		 		}
				// HIX-82090 - if affliate's restrictHiosId flag value is set as ON, then pull tenant's restricted Hios Issuer Id list 
				if(restrictHiosIds.equalsIgnoreCase(PlanMgmtConstants.ON)){
					restrictHiosIdListOfTenant =  tenantService.getTenant(tenantCode).getConfiguration().getHiosIdConfiguration();
				}
		 	}	
						
			ArrayList<String> paramList = new ArrayList<String>();
			StringBuilder buildquery = new StringBuilder();
			buildquery.append("SELECT p.id AS plan_id, p.name AS plan_name, plan_level, sum(pre) AS total_premium, p.network_type AS network_type, i.name AS issuer_name, pdental_id, net.network_url as network_url, i.logo_url as issuer_logo, ");
			if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
				buildquery.append("listagg(memberid, ',')  within group (order by memberid) as memberlist , listagg(pre, ',')  within group (order by memberid) as memberprelist, listagg(rname, ',')  within group (order by memberid) as memberrelist, ");
			} else if(PlanMgmtConstants.DB_POSTGRESQL.equalsIgnoreCase(configuredDB)) {	
				//buildquery.append("string_agg(to_char(memberid, '999'), ',' order by memberid) as memberlist , string_agg(to_char(pre, '999D99'), ',' order by memberid) as memberprelist, string_agg(rname, ',' order by memberid) as memberrelist, ");
				buildquery.append("string_agg(CAST (memberid AS VARCHAR), ',' order by memberid) as memberlist ,  string_agg(to_char(CAST (pre AS DOUBLE PRECISION), '9999D99'), ',' order by memberid) as memberprelist,   string_agg(rname, ',' order by memberid) as memberrelist, ");

			}
			buildquery.append("p.brochure_ucm_id as brochure_ucm, p.brochure as brochure, p.is_puf, rate_option, p.formularly_id, p.hsa, GVER, i.id as issuer_id, ");
			buildquery.append("EHB_portion, net.has_provider_data, net.network_key, TIER2_UTIL, p.eoc_doc_id, cost_sharing, ageandfamily_rate, sbc_ucm_id, benefits_url, p.issuer_plan_number, i.hios_issuer_id, p.exchange_type ");		
			buildquery.append("FROM ");
			buildquery.append("plan p, issuers i, network net, ");
			if (currentExchangeType.equals(GhixConstants.PHIX)){
				buildquery.append(" pm_tenant_plan tp, tenant t, ");
			}
			buildquery.append(" ( ");

			String applicantZip = null;
			String applicantCounty = null;
	
			for (int i = 0; i < modifiedMemberList.size(); i++) {
				buildquery.append("( SELECT pdental.id AS pdental_id, pdental.plan_id, prate.rate AS pre, pdental.plan_level AS plan_level, :param_" + paramList.size()
						+ " as memberid, prarea.rating_area as rname, 1 as memberctr, prate.rate_option as rate_option, pdental.GUARANTEED_VS_ESTIMATED_RATE AS GVER, ");
				paramList.add(modifiedMemberList.get(i).get(ID));
				
				buildquery.append("pdental.ehb_appt_for_pediatric_dental AS EHB_portion, ");
				buildquery.append("pdental.TIER2_UTIL AS TIER2_UTIL, pdental.cost_sharing AS cost_sharing, pdental.is_community_rate AS ageandfamily_rate, pdental.sbc_ucm_id AS sbc_ucm_id, pdental.benefits_url AS benefits_url ");
				buildquery.append("FROM ");
				buildquery.append("plan_dental pdental, pm_service_area psarea,  pm_rating_area prarea, pm_plan_rate prate, plan p2 ");
			    // when exchange is PHIX and quoting for OFF exchange plans  
				if(exchangeType.equalsIgnoreCase(Plan.EXCHANGE_TYPE.OFF.toString()) && currentExchangeType.equals(GhixConstants.PHIX)){
					buildquery.append(", issuers i2 ");
				}
				buildquery.append("WHERE ");
				buildquery.append("p2.id = pdental.plan_id ");
				// when exchange is PHIX and quoting for OFF exchange plans  
				if(exchangeType.equalsIgnoreCase(Plan.EXCHANGE_TYPE.OFF.toString()) && currentExchangeType.equals(GhixConstants.PHIX)){
					buildquery.append("AND p2.issuer_id = i2.id ");
				}
				buildquery.append("AND p2.service_area_id = psarea.service_area_id ");
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND psarea.zip = to_number(trim(:param_" + paramList.size() + ")) ");
					paramList.add(modifiedMemberList.get(i).get(ZIP));
				}else{
					buildquery.append("AND psarea.zip = :param_" + paramList.size() + " ");
					paramList.add(modifiedMemberList.get(i).get(ZIP));
				}

				if (null == applicantZip) {
					applicantZip = modifiedMemberList.get(i).get(ZIP);
				}
				
				buildquery.append("AND p2.id = prate.plan_id ");
				buildquery.append("AND p2.is_deleted = 'N' ");
				buildquery.append("AND psarea.is_deleted = 'N' ");
				buildquery.append("AND prarea.id = prate.rating_area_id ");
				buildquery.append("AND prate.rating_area_id = ");
				
				// -------- changes for HIX-100092 starts --------
				buildquery.append(quotingBusinessLogic.buildSearchRatingAreaLogic(stateCode, paramList, modifiedMemberList, i, applicableYear, configuredDB));
				// -------- changes for HIX-100092 ends --------
				
				buildquery.append(quotingBusinessLogic.buildRateLogicForDentalQuoting("prate", "i2", stateName, houseHoldType, familyTierLookupCode, modifiedMemberList.get(i).get(AGE), modifiedMemberList.get(i).get(TOBACCO), effectiveDate, exchangeType, doQuoteIssuerIHC, IHCfamilyTierLookupCode, paramList));
				
				// if we are looking for any specific plan level like low/high etc
				if (!planLevel.equals(PlanMgmtConstants.EMPTY_STRING)) {
					buildquery.append("AND pdental.plan_level = :param_" + paramList.size() + " ");
					paramList.add(planLevel.toUpperCase());
				}
	
				if (modifiedMemberList.get(i).get(COUNTYCODE) != null && !modifiedMemberList.get(i).get(COUNTYCODE).equals(PlanMgmtConstants.EMPTY_STRING)) {
					buildquery.append("AND psarea.fips = :param_" + paramList.size() + " ");
					paramList.add(modifiedMemberList.get(i).get(COUNTYCODE));

					if (null == applicantCounty) {
						applicantCounty = modifiedMemberList.get(i).get(COUNTYCODE);
					}
				}
				buildquery.append(" ) ");
	
				if (i + 1 != modifiedMemberList.size()) {
					buildquery.append(" UNION ALL ");
				}
			}
	
			buildquery.append(") temp WHERE p.id = temp.plan_id AND net.id  = p.provider_network_id AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date AND p.insurance_type='DENTAL' ");
			paramList.add(effectiveDate);
			buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic(ALIAS_OF_PLAN_TABLE, issuerVerifiedFlag));
	
			// HIX-77565 When the Issuer comes through consumer shopping flow, plan enrollment availability status should not be checked.
			if (StringUtils.isBlank(hiosPlanNumber) && keepOnly.equalsIgnoreCase("N") && issuerId <= 0) { // if keepOnly == N, then use enrollment availability filter. ref jira HIX-48205

				List<String> currentHiosIssuerIdList = null;

				if (StringUtils.isNotBlank(currentHiosIssuerID) && PlanMgmtConstants.Y.equalsIgnoreCase(isPlanChange)) {
					currentHiosIssuerIdList = Arrays.asList(new String[] {currentHiosIssuerID});
				}

				if (isSpecialEnrollment.equalsIgnoreCase("YES")) {
					buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic(ALIAS_OF_PLAN_TABLE, effectiveDate, PlanMgmtConstants.YES, applicantZip, applicantCounty, currentHiosIssuerIdList, paramList));
				}
				else {
					buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic(ALIAS_OF_PLAN_TABLE, effectiveDate, PlanMgmtConstants.NO, applicantZip, applicantCounty, currentHiosIssuerIdList, paramList));
				}
			}	

			buildquery.append("AND p.market = :param_" + paramList.size() + " AND p.issuer_id = i.id ");
			paramList.add(marketType.toUpperCase());
			
			List<Integer> idListToInclude = new ArrayList<Integer>();
			List<Integer> idListToExclude = new ArrayList<Integer>();
			if (planIdStr != null && !planIdStr.equals(PlanMgmtConstants.EMPTY_STRING)) {
				buildquery.append(" AND p.id in ( :paramIds ) ");
				for(String id : planIdStr.split(",")){
					idListToInclude.add(Integer.parseInt(id));
				}
			}
			
			if(hiosPlanNumber != null && !hiosPlanNumber.equals(PlanMgmtConstants.EMPTY_STRING)){
				buildquery.append(buildEnrollmentAvailableLogic(ALIAS_OF_PLAN_TABLE, effectiveDate, paramList));
				buildquery.append(" AND p.issuer_Plan_number = :param_" + paramList.size() +  " ");
				paramList.add(hiosPlanNumber);
			}
			// if issuer id passed, pick that issuer specific plans
			if (issuerId > 0) {
				buildquery.append(" AND p.issuer_id = CAST (:param_" + paramList.size() + " AS INTEGER) ");
				paramList.add(String.valueOf(issuerId));
			}
			
			//HIX-65755 :: Eliminate PlanIds in special enrollment
			if((eliminatePlanIds != null) && !eliminatePlanIds.equals(PlanMgmtConstants.EMPTY_STRING)){
				buildquery.append(" AND p.id NOT IN ( :paramIdseliminated ) ") ;
				for(String id : eliminatePlanIds.split(",")){
					idListToExclude.add(Integer.parseInt(id));
				}
			}	
			
			// HIX-89716 :: During Issuer Rep plan shopping, ignore the restricted HIOS IDs
			if(issuerId <= PlanMgmtConstants.ZERO){
				// if restrictHiosIdListOfTenant is not empty then excludes those HIOS Issuer ids from quoting
				if((null != restrictHiosIdListOfTenant) && (restrictHiosIdListOfTenant.size() > PlanMgmtConstants.ZERO)){
					buildquery.append(" AND i.hios_issuer_id NOT IN ( :paramRestrictHiosIdList ) ") ;
				}
			}
			
			buildquery.append(" AND p.exchange_type = :param_");
			buildquery.append(paramList.size());
			paramList.add(exchangeType.toUpperCase());	
			
			if (currentExchangeType.equals(GhixConstants.PHIX)){
			  buildquery.append(" AND p.id = tp.plan_id");
			  buildquery.append(" AND tp.tenant_id = t.id");
			  buildquery.append(" AND t.code = :param_");
			  buildquery.append(paramList.size());
			  paramList.add(tenantCode.toUpperCase());
			}
	
			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic(ALIAS_OF_PLAN_TABLE, houseHoldType));
			buildquery.append(" GROUP BY  p.id, p.name, net.network_url, pdental_id, plan_level, i.name, i.logo_url, p.network_type, p.brochure, p.brochure_ucm_id, p.is_puf, rate_option, p.formularly_id, p.hsa, GVER, i.id, EHB_portion, "); 
			buildquery.append("net.has_provider_data, net.network_key, TIER2_UTIL, p.eoc_doc_id, cost_sharing, ageandfamily_rate, sbc_ucm_id, benefits_url, p.issuer_plan_number, i.hios_issuer_id, p.exchange_type  ");
			buildquery.append("HAVING sum(memberctr) = " + modifiedMemberList.size());
	
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL == " + buildquery.toString());
			}
	
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			
			if(idListToInclude.size() != 0){
				query.setParameter("paramIds", idListToInclude);
			}
			
			if(idListToExclude.size() != 0){
				query.setParameter("paramIdseliminated", idListToExclude);
			}
			
			// HIX-89716 :: During Issuer Rep plan shopping, ignore the restricted HIOS IDs
			if(issuerId <= PlanMgmtConstants.ZERO){
				if((null != restrictHiosIdListOfTenant) && (restrictHiosIdListOfTenant.size() > PlanMgmtConstants.ZERO)){
					query.setParameter("paramRestrictHiosIdList", restrictHiosIdListOfTenant);
				}
			}	
				
			List rsList = query.getResultList();		
			Iterator rsIterator = rsList.iterator();
			Iterator rsIterator2 = rsList.iterator();
			List<Integer> planDentalIdStr = new ArrayList<Integer>();
			DecimalFormat f = new DecimalFormat("##.00");
			BigDecimal planDentalId  = null;
			StringBuilder hiosPlanIds = new StringBuilder(1024);
			String sbcDocURL = null;
			String benefitURL = null;
			PlanRateBenefit planMap =null;
			Object[] objArray = null;
			String logoURL = null;
			String rateOption = null;
			Float householdPremium = null;
			String brochureUCM  = null;
			String brochureUrl = null;
			String returnedString = null;
			String providerUrl = null;
			List<Map<String, String>> memberInfo = null;
			Map<String, String> memberData = null;
			String ehbPercentage = null;
			String[] memberids = null;
			String[] memberpremiums = null;
			String[] memberregions = null;
			String[] effectiveDateArr = null;
			Integer formularlyId = PlanMgmtConstants.ZERO;
			String formularyUrl = null;
			RatingArea ratingArea = null; 
			Map<String, String> memberHavingRatingAreaMap = new HashMap<String, String>();
			
			while (rsIterator.hasNext()) {
				sbcDocURL = PlanMgmtConstants.EMPTY_STRING;
				benefitURL = PlanMgmtConstants.EMPTY_STRING;
				planMap = new PlanRateBenefit();
				objArray = (Object[]) rsIterator.next();
				logoURL = null;
	
				rateOption = objArray[PlanMgmtConstants.FIFTEEN].toString();
				householdPremium = Float.parseFloat(objArray[PlanMgmtConstants.THREE].toString());
				// if rate option is Family, then premium would be on family level, don't consider each quoted member's premium
				if (rateOption.equalsIgnoreCase(PlanMgmtConstants.FAMILY_RATE_OPTION)) {
					householdPremium = (householdPremium / modifiedMemberList.size());
				}
	
				planMap.setId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
				planMap.setName(objArray[PlanMgmtConstants.ONE].toString());
				planMap.setLevel((objArray[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWO].toString());
				planMap.setPremium(Float.parseFloat(f.format(householdPremium)));
				planMap.setNetworkType((objArray[PlanMgmtConstants.FOUR] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.FOUR].toString());
				planMap.setIssuer(objArray[PlanMgmtConstants.FIVE].toString());
				planMap.setIssuerText(objArray[PlanMgmtConstants.FIVE].toString().toLowerCase().replaceAll("\\s|\\.", PlanMgmtConstants.EMPTY_STRING));
				// collection of plan_dental_ids to generate a single query	
				planDentalId = (BigDecimal)objArray[PlanMgmtConstants.SIX];
				planDentalIdStr.add(planDentalId.intValue());
	
				// Append http:// in provider link url if does not exists
				providerUrl = (objArray[PlanMgmtConstants.SEVEN] != null) ? objArray[PlanMgmtConstants.SEVEN].toString() : PlanMgmtConstants.EMPTY_STRING;
				if (!providerUrl.equals(PlanMgmtConstants.EMPTY_STRING) && !providerUrl.matches("(?i).*http.*")) {
					providerUrl = "http://" + providerUrl;
				}
	
				planMap.setProviderLink(providerUrl);
				if(null != objArray[PlanMgmtConstants.EIGHT]) {
					logoURL = objArray[PlanMgmtConstants.EIGHT].toString();
				}
				planMap.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, objArray[PlanMgmtConstants.THIRTY].toString(), null, appUrl));
	
			//	List<Map<String, String>> associatedproviders = checkProviderSupportsPlan(providerList, planMap.getId());
				//planMap.setProviders(associatedproviders);
	
				memberids = objArray[PlanMgmtConstants.NINE].toString().split(",");
				memberpremiums = objArray[PlanMgmtConstants.TEN].toString().split(",");
				memberregions = objArray[PlanMgmtConstants.ELEVEN].toString().split(",");
	
				brochureUCM = (objArray[PlanMgmtConstants.TWELVE] != null) ? objArray[PlanMgmtConstants.TWELVE].toString() : PlanMgmtConstants.EMPTY_STRING;
				brochureUrl = (objArray[PlanMgmtConstants.THIRTEEN] != null) ? objArray[PlanMgmtConstants.THIRTEEN].toString() : PlanMgmtConstants.EMPTY_STRING;
	
				if (brochureUCM != null && !brochureUCM.equals(PlanMgmtConstants.EMPTY_STRING)) {
					brochureUrl = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.TWELVE].toString());
				} else {
					if (!brochureUrl.equals(PlanMgmtConstants.EMPTY_STRING) && !brochureUrl.matches("(?i).*http.*")) {
						brochureUrl = "http://" + brochureUrl;
					}
	
				}
	
				// calculate cost, region for each member
				memberInfo = new ArrayList<Map<String, String>>();
				for (Map<String, String> inputMemberData : memberList) {
					memberData = new HashMap<String, String>();
	
					memberData.put("id", inputMemberData.get(ID));
					memberData.put("premium", PlanMgmtConstants.ZERO_STR);
					for (int ctr = 0; ctr < memberids.length; ctr++) {
						if (memberids[ctr].equalsIgnoreCase(inputMemberData.get(ID))) {
							// if rate option is family i.e. community rating, 
							// then set premium for applicant only and other family member's premium would be zero. 
							if (rateOption.equalsIgnoreCase("F")){
								if(inputMemberData.get(RELATION).equalsIgnoreCase("member") || inputMemberData.get(RELATION).equalsIgnoreCase("self")) {
									memberData.put("premium", memberpremiums[ctr].trim());
								}														
							}else{
								memberData.put("premium", memberpremiums[ctr].trim());
							}
							memberData.put("region", memberregions[ctr].replaceAll("[a-zA-Z ]", PlanMgmtConstants.EMPTY_STRING));
						}
					}
					if (memberData.get("region") == null) {
						// if member's rating area is already computed then don't fire query on DB to pull rating area for corresponding member
						if(null != memberHavingRatingAreaMap.get(memberData.get("id")))
						{
							memberData.put("region",  memberHavingRatingAreaMap.get(memberData.get("id")));
						}
						else
						{
							// if state exchange is ID then, then use applicable year filter to pull rating area 
							if(PlanMgmtConstants.STATE_CODE_ID.equalsIgnoreCase(stateCode))
							{
								ratingArea = ratingAreaService.getRatingAreaByYear(inputMemberData.get(ZIP), inputMemberData.get(COUNTYCODE), applicableYear);
							}else{
								ratingArea = ratingAreaService.getRatingAreaByZip(inputMemberData.get(ZIP), inputMemberData.get(COUNTYCODE));
							}
							
							if (ratingArea != null && ratingArea.getRatingArea() != null) {
								memberData.put("region", ratingArea.getRatingArea().replaceAll("[a-zA-Z ]", PlanMgmtConstants.EMPTY_STRING));
							} else {
								memberData.put("region", memberregions[0].replaceAll("[a-zA-Z ]", PlanMgmtConstants.EMPTY_STRING));
							}
						}	
					}
					
					if(null != memberData.get("region") && null == memberHavingRatingAreaMap.get(memberData.get("id")) ){
						memberHavingRatingAreaMap.put(memberData.get("id"),  memberData.get("region"));
					}
					
					memberInfo.add(memberData);
				}
				
				planMap.setPlanBrochureUrl(brochureUrl);
				planMap.setPlanDetailsByMember(memberInfo);
				
				if (objArray[PlanMgmtConstants.FOURTEEN] != null) {
					planMap.setIsPuf(objArray[PlanMgmtConstants.FOURTEEN].toString());
				} else {
					planMap.setIsPuf(PlanMgmtConstants.EMPTY_STRING);
				}
				
				if (objArray[PlanMgmtConstants.SIXTEEN] != null && StringUtils.isNotBlank(objArray[PlanMgmtConstants.SIXTEEN].toString())) {
					formularlyId = Integer.parseInt(objArray[PlanMgmtConstants.SIXTEEN].toString().trim());
					formularyUrl = getFormularyURL(formularlyId);
					planMap.setFormularyUrl(formularyUrl);
				}else{
					planMap.setFormularyUrl(PlanMgmtConstants.EMPTY_STRING);
				}
				
				/*	adding new field as per JIRA: HIX-34301*/
				planMap.setHsa((objArray[PlanMgmtConstants.SEVENTEEN] != null) ? objArray[PlanMgmtConstants.SEVENTEEN].toString(): PlanMgmtConstants.EMPTY_STRING);								
				planMap.setGuaranteedVsEstimatedRate( (objArray[PlanMgmtConstants.EIGHTEEN] != null) ? objArray[PlanMgmtConstants.EIGHTEEN].toString().trim(): PlanMgmtConstants.EMPTY_STRING);
				planMap.setIssuerId(Integer.parseInt(objArray[PlanMgmtConstants.NINTEEN].toString()));
			
				// changes done for HIX-47877
				// if considerEHBPortion == Y, then EHBpercentage would be as below for dental plans
				effectiveDateArr = effectiveDate.split("-");
	
				if(considerEHBPortion.equalsIgnoreCase("Y")){				
					ehbPercentage = (objArray[PlanMgmtConstants.TWENTY] != null) ? objArray[PlanMgmtConstants.TWENTY].toString() : PlanMgmtConstants.EMPTY_STRING;
					if(ehbPercentage != null && !ehbPercentage.equalsIgnoreCase(PlanMgmtConstants.EMPTY_STRING)){
						ehbPercentage = ehbPercentage.replace("$", PlanMgmtConstants.EMPTY_STRING);
						if(!ehbPercentage.equalsIgnoreCase(PlanMgmtConstants.EMPTY_STRING)){
							// ref jira HIX-85634
							// if coverage year <= 2016, ebhPercentageValue = pdental.ehb_appt_for_pediatric_dental * number of quoted member
							if(Integer.parseInt(effectiveDateArr[0]) <= 2016){
							Float ebhPercentageValue = Float.parseFloat(ehbPercentage) * modifiedMemberList.size(); // modifiedMemberList.size() = number of quoted member
							planMap.setEhbPercentage(ebhPercentageValue.toString());	
							}else{
								// if coverage year > 2016 then send coverage amount as captured in pdental.ehb_appt_for_pediatric_dental column
								planMap.setEhbPercentage(ehbPercentage);
						}			
					}				
				}
				
				}
				
				if(objArray[PlanMgmtConstants.TWENTYONE] != null && objArray[PlanMgmtConstants.TWENTYONE].toString().equalsIgnoreCase(PlanMgmtConstants.YES)){
					planMap.setNetworkKey((objArray[PlanMgmtConstants.TWENTYTWO] != null) ? objArray[PlanMgmtConstants.TWENTYTWO].toString().trim(): PlanMgmtConstants.EMPTY_STRING);
				}else{
					planMap.setNetworkKey(PlanMgmtConstants.EMPTY_STRING);
				}
				
				planMap.setTier2util((objArray[PlanMgmtConstants.TWENTYTHREE] != null) ? objArray[PlanMgmtConstants.TWENTYTHREE].toString().trim(): PlanMgmtConstants.EMPTY_STRING);
				
				String eocDocURL = null;
				if(null != objArray[PlanMgmtConstants.TWENTYFOUR]) {
					if (objArray[PlanMgmtConstants.TWENTYFOUR].toString().matches(PlanMgmtConstants.ECM_REGEX)) {
						eocDocURL = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.TWENTYFOUR].toString());    
					} else {
						eocDocURL = objArray[PlanMgmtConstants.TWENTYFOUR].toString();    
					}
				} else {
					eocDocURL = PlanMgmtConstants.EMPTY_STRING;
				}
				planMap.setEocDocUrl(eocDocURL);
				planMap.setCostSharing((objArray[PlanMgmtConstants.TWENTYFIVE] != null) ? objArray[PlanMgmtConstants.TWENTYFIVE].toString().trim(): PlanMgmtConstants.EMPTY_STRING);
				
				if (objArray[PlanMgmtConstants.TWENTYSEVEN] != null) {
					sbcDocURL = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.TWENTYSEVEN].toString());
				}
				
				if (objArray[PlanMgmtConstants.TWENTYEIGHT] != null) {
	
					if (PlanMgmtUtil.isURLValid(objArray[PlanMgmtConstants.TWENTYEIGHT].toString())) {
						benefitURL = objArray[PlanMgmtConstants.TWENTYEIGHT].toString();
					} else {
						benefitURL = appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.TWENTYEIGHT].toString());
					}
				}
				 
				if (!benefitURL.equals(PlanMgmtConstants.EMPTY_STRING) && !benefitURL.matches(PlanMgmtConstants.URL_REGEX)) {
					benefitURL = "http://" + benefitURL;				
				}
				
				returnedString = PlanMgmtUtil.returnOneString(sbcDocURL, benefitURL);
				if (!returnedString.isEmpty()) {
					planMap.setSbcDocUrl(returnedString);
				} else {
					planMap.setSbcDocUrl(PlanMgmtConstants.EMPTY_STRING);
				}
				
				planMap.setHiosPlanNumber(objArray[PlanMgmtConstants.TWENTYNINE].toString());
				// ref HIX-88018 :: Return plan's exchange type
				planMap.setExchangeType((objArray[PlanMgmtConstants.THIRTYONE] != null) ? objArray[PlanMgmtConstants.THIRTYONE].toString() : PlanMgmtConstants.EMPTY_STRING);
				
				//HIX-62493
				// if plan have age band rate and family rate both  		   
				if((objArray[PlanMgmtConstants.TWENTYSIX] != null) && objArray[PlanMgmtConstants.TWENTYSIX].toString().equalsIgnoreCase("Y")  ){
					//	if number of quoted member is more than 1 then add those plans having family rate
					if (rateOption.equalsIgnoreCase(PlanMgmtConstants.FAMILY_RATE_OPTION) && modifiedMemberList.size() > 1) {
						plan.add(planMap);
					}
					// 	if number of quoted member is 1 then add those plans having age band rate
					if (rateOption.equalsIgnoreCase(PlanMgmtConstants.AGE_RATE_OPTION) && modifiedMemberList.size() == 1) {
						plan.add(planMap);
					}
				}else{
					plan.add(planMap);
				}
				
				planMap = null;
				
				// prepare hios plan ids, 
				// take first 14 digits from plan table, because puff table have 14 digit hios plan number without standard component id			
				hiosPlanIds.append((hiosPlanIds.length() == 0) ? objArray[PlanMgmtConstants.TWENTYNINE].toString().substring(0,14) : "," + objArray[PlanMgmtConstants.TWENTYNINE].toString().substring(0,14));
							
			}
			
			Map<Integer, Map<String, Map<String, String>>> benefitData = new HashMap<Integer, Map<String, Map<String, String>>>();
			Map<Integer, Map<String, Map<String, String>>> planCosts = new HashMap<Integer, Map<String, Map<String, String>>>();
	
			// fire single query to pull plan benefits and costs
			if(!minimizePlanData){
				if (!(planDentalIdStr.isEmpty())) {
					benefitData = planCostAndBenefitsService.getOptimizedDentalPlanBenefits(planDentalIdStr, effectiveDate);
					planCosts = planCostAndBenefitsService.getOptimizedDentalPlanCosts(planDentalIdStr, false);
				}
		
				List<String> costList = getCostNames();
				BigDecimal planDentalId2 = null;
				Object[] objArray2 = null;
				Map<String, Map<String,String>> costMap = null;
				Map<String, Map<String,String>> planCostMap = null;
				Map<String, Map<String,String>> optionalPlanCostMap = null;
				Integer planId =  null;
				
				while (rsIterator2.hasNext()) {
					objArray2 = (Object[]) rsIterator2.next();
					planDentalId2 = (BigDecimal)objArray2[PlanMgmtConstants.SIX];
					planId = Integer.parseInt(objArray2[PlanMgmtConstants.ZERO].toString());
					costMap = planCosts.get(planDentalId2.intValue());
					
					for (PlanRateBenefit planObj : plan) {
							if (planId.equals(planObj.getId()) && benefitData.containsKey(planDentalId2.intValue())) {
								planObj.setPlanBenefits(benefitData.get(planDentalId2.intValue()));
								Set<String> costNameSet = costMap.keySet();
								planCostMap = new HashMap<String, Map<String,String>>();
								optionalPlanCostMap = new HashMap<String, Map<String,String>>();
								for (String costName: costNameSet){
									if(costList.contains(costName)){
											planCostMap.put(costName, costMap.get(costName));
										}else{
											optionalPlanCostMap.put(costName, costMap.get(costName));
										}
									}
								planObj.setPlanCosts(planCostMap);
								planObj.setOptionalDeductible(optionalPlanCostMap);
							}
						}
				}
			}
			
			// HIX-80502 show/hide PUF plans on depends on tenant configuration. Show PUF plans when showPufPlans is true
			if(showPufPlans){ 
				// Implement PUF dental plan quoting ref. HIX-80945
				// For off-exchange, we don't need to show the PUF plans : for HIX-42652
				// if plan id passes in request then don't send PUF plan in response HIX-52995
				// when issuer representative comes through consumer shopping flow then don't pull PUF plans. ref HIX-80226
				// if HIOS plan number passed through request then don't pull PUF plans. ref HIX-80662
				if (GhixConstants.PHIX.equalsIgnoreCase(currentExchangeType) && PlanMgmtConstants.EMPTY_STRING.equals(planIdStr) && 
						PlanMgmtConstants.PLAN_EXCHANGE_TYPE_ON.equalsIgnoreCase(exchangeType) && issuerId <= 0 && PlanMgmtConstants.EMPTY_STRING.equalsIgnoreCase(hiosPlanNumber) ) {
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug(" :::::::: Trying to get PUF SADP plans " );
					}
					// pulling puff plans
					pufPlanData = pufPlanService.getPufPlanData(modifiedMemberList.get(0).get(ZIP), modifiedMemberList.get(0).get(COUNTYCODE), hiosPlanIds.toString(), modifiedMemberList, stateName, houseHoldType, Plan.PlanInsuranceType.DENTAL.toString(), applicableYear );
					
					// if we have puff plan then club puff plan with on/off exchange plans	
					if(pufPlanData != null && pufPlanData.size() > PlanMgmtConstants.ZERO){
							plan.addAll(pufPlanData);
							if(LOGGER.isDebugEnabled()) {
								LOGGER.debug(" :::::::: Added PUF data ... record count " + pufPlanData.size());
							}
					}
					pufPlanData = null;
				}
			}	
		}catch(Exception ex){
			LOGGER.error("Exception Occured in getHouseholdDentalPlanRateBenefits", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return plan;
	}


	private String getFormularyURL(Integer formularlyId){
		String formularyURL = PlanMgmtConstants.EMPTY_STRING;
		if(null != formularlyId && formularlyId > PlanMgmtConstants.ZERO){
			formularyURL = formularyRepository.findFormularyUrlById(formularlyId);
			/* Adding Regular expression for adding HTTP  */
			if(StringUtils.isNotBlank(formularyURL)){
				if (!formularyURL.equals(PlanMgmtConstants.EMPTY_STRING) && !formularyURL.matches(PlanMgmtConstants.URL_REGEX)) {
					formularyURL = "http://" + formularyURL;			
				}
			}else{
				formularyURL = PlanMgmtConstants.EMPTY_STRING;
			}
		}	
		return formularyURL;
	}
	
	
		
	/**
	 * Method is used to get Plan Rate for each family member.
	 * @param request, Object of MLECRestRequest.
	 * @return Object of MLECRestRequest.
	 * @exception throw Exception.
	 */
	@Override
	@Transactional(readOnly = true)
	public MLECRestResponse getMemberLevelEmployeeRate(final MLECRestRequest request) {
		MLECRestResponse response = null;
		EntityManager entityManager = null;
		List rsList = null;
		Iterator rsIterator = null;
		List<Member> memberList = new ArrayList<Member>();
		
		try {
			entityManager = emf.createEntityManager();
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("MLECRestRequest is null: " + (null == request));
			}
			
			if (null == request) {
				return response;
			}
			
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("Checking Validations");
			}
			if (null != request.getBenchmarkPlanId()
					&& 0 < request.getBenchmarkPlanId()
					&& null != request.getEmployeeQuotingZip()
					&& null != request.getEmployerQuotingZip()
					&& null != request.getEmployeeQuotingCountyCode()
					&& null != request.getEmployerQuotingCountyCode()				
					&& null != request.getEffectiveDate()
					&& null != request.getMembers()
					&& !request.getMembers().isEmpty()) {
				
								
				List<Map<String, String>> processedMemberData = null;
				List<Map<String, String>> actualMemberData = new ArrayList<Map<String, String>>();
				
				// Make ArrayList with Map for processedMemberData().
				for (Member memberObj : request.getMembers()) {
					Map<String, String> memberData = new HashMap<String, String>();
					if (null != memberObj) {
						memberData.put(ID, memberObj.getMemberID());
						
						if (null != memberObj.getRelationship()
								&& memberObj.getRelationship() == Member.Relationship.CH) {
							memberData.put(RELATION, CHILD);
						}
						else {
							memberData.put(RELATION, memberObj.getRelationship().toString());
						}
						memberData.put(AGE, memberObj.getAge().toString());
						
						if (null != memberObj.getSmoker()) {							
							memberData.put(TOBACCO, (memberObj.getSmoker()) ? "Y" : "N");						
						}
						else {
							memberData.put(TOBACCO, null);
						}
						actualMemberData.add(memberData);
					}
				}
				// filter member data
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("actualMemberData size : " + actualMemberData.size());
				}
				processedMemberData = quotingBusinessLogic.processMemberData(actualMemberData,Plan.PlanInsuranceType.HEALTH.toString());					
				
				Member memberResponse = null;
				boolean quoteByEmployeeZip = false;
				String memberIDs[] = null;
				String memberRates[] = null;
				//Integer planId = null;
				
				int benchmarkPlanId = request.getBenchmarkPlanId();
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("BenchmarkPlanId: " + benchmarkPlanId);
				}
								
				//entityManager = entityManagerFactory.createEntityManager();
				
				// use employer's zipcode, county code for quoting
				StringBuilder buildQuery = getMemberLevelEmployeeRateQuery(request, quoteByEmployeeZip, processedMemberData);
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Employer Build Query:" + buildQuery);
				}
				rsList = entityManager.createNativeQuery(buildQuery.toString()).getResultList();
					
				// if plans not found using employer's  zipcode, county code then use employee's zipcode, county code for quoting
				if (rsList.isEmpty()) {
					quoteByEmployeeZip = true;
					buildQuery = getMemberLevelEmployeeRateQuery(request, quoteByEmployeeZip, processedMemberData);
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Employee Build Query:" + buildQuery);
					}
					rsList = entityManager.createNativeQuery(buildQuery.toString()).getResultList();
				}
				
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("DATA SIZE = " + rsList.size());
				}
				
				if (!rsList.isEmpty()) {						
						rsIterator = rsList.iterator();	
						
							if (rsIterator.hasNext()) {								
								Object[] objArray = (Object[]) rsIterator.next();								
								if (null != objArray && Integer.parseInt(PlanMgmtConstants.FOUR_STR) == objArray.length) {
								
									/*if (null != objArray[1]
											&& StringUtils.isNumeric(objArray[1].toString())) {
										planId = Integer.parseInt(objArray[1].toString());
									}*/
									
									if (null != objArray[Integer.parseInt(PlanMgmtConstants.TWO_STR)] && null != objArray[Integer.parseInt(PlanMgmtConstants.THREE_STR)]
											&& StringUtils.isNotBlank(objArray[Integer.parseInt(PlanMgmtConstants.TWO_STR)].toString())
											&& StringUtils.isNotBlank(objArray[Integer.parseInt(PlanMgmtConstants.THREE_STR)].toString())) {
										memberIDs = objArray[Integer.parseInt(PlanMgmtConstants.TWO_STR)].toString().split(",");
										memberRates = objArray[Integer.parseInt(PlanMgmtConstants.THREE_STR)].toString().split(",");										
									
										
										if (0 < memberIDs.length && memberIDs.length == memberRates.length) {
											for(Map<String, String> inputMemberData : actualMemberData){
												memberResponse = new Member();											
												memberResponse.setMemberID(inputMemberData.get(ID));
												memberResponse.setBenchmarkPremium(0);
												if(inputMemberData.get(RELATION).equalsIgnoreCase(CHILD)){
													memberResponse.setRelationship(Member.Relationship.CH);
												}else if (inputMemberData.get(RELATION).equalsIgnoreCase(Member.Relationship.SE.toString())){
													memberResponse.setRelationship(Member.Relationship.SE);
												}else if (inputMemberData.get(RELATION).equalsIgnoreCase(Member.Relationship.SP.toString())){
													memberResponse.setRelationship(Member.Relationship.SP);
												}else if (inputMemberData.get(RELATION).equalsIgnoreCase(Member.Relationship.DT.toString())){
													memberResponse.setRelationship(Member.Relationship.DT);
												}
											
												// set rate for quoted members
												for (int i = 0; i < memberIDs.length; i++) {		
													if(memberIDs[i].trim().equalsIgnoreCase(inputMemberData.get(ID).trim())){
														memberResponse.setMemberID(memberIDs[i].trim());													
														memberResponse.setBenchmarkPremium(Float.parseFloat(memberRates[i].trim()));
													}											
												}
												memberList.add(memberResponse);
											}
																						
										}
									}
								}
								//planId = null;
							}						
					}				
			}

			if (!memberList.isEmpty()) {				
			
				response = new MLECRestResponse();
				response.setMembers(memberList);
			}
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("MLECRestResponse: " + response);
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getMemberLevelEmployeeRate", ex);
		}finally {
			// Clear all values
			rsIterator = null;
			
			if (null != rsList) {
				rsList.clear();
				rsList = null;
			}
			// close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		return response;
	}
	
	
	/**
	 * Build Query for Lowest Premium Plan Rate
	 * 
	 * @return Build Query in StringBuffer
	 */
	private StringBuilder getMemberLevelEmployeeRateQuery(MLECRestRequest request, boolean quoteByEmployeeZip,List<Map<String, String>> processedMemberData) {
		
		String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedMemberData, Plan.PlanInsuranceType.HEALTH.toString());		
	
		SimpleDateFormat formatter = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT);				
		String effectiveDate = formatter.format(request.getEffectiveDate());
		String quotingZip = PlanMgmtConstants.EMPTY_STRING;
		String quotingCountyCode = PlanMgmtConstants.EMPTY_STRING ;
		
		// if quoteByEmployeeZip == true use employee zip to quote else employer zip code	
		if(quoteByEmployeeZip){
			quotingZip =  request.getEmployeeQuotingZip();
			quotingCountyCode = request.getEmployeeQuotingCountyCode();
		}else{
			quotingZip =  request.getEmployerQuotingZip();
			quotingCountyCode = request.getEmployerQuotingCountyCode();
		}
		
		
		StringBuilder buildQuery = new StringBuilder("SELECT SUM(RATE) AS TOTAL_PREMIUM, PLAN.ID,");
		if(PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB) || StringUtils.isEmpty(configuredDB)) {
			buildQuery.append(" LISTAGG(MEMBERID, ',') WITHIN GROUP (ORDER BY MEMBERID) AS MEMBER_LIST,");
			buildQuery.append(" LISTAGG(RATE, ',') WITHIN GROUP (ORDER BY MEMBERID) AS MEMBER_RATE_LIST");	
		} else if(PlanMgmtConstants.DB_POSTGRESQL.equalsIgnoreCase(configuredDB)) {
			buildQuery.append(" string_agg(CAST(MEMBERID AS VARCHAR), ',' ORDER BY MEMBERID) AS MEMBER_LIST,");	
			buildQuery.append(" string_agg(to_char(CAST (RATE AS DOUBLE PRECISION), '9999D99'), ',' ORDER BY MEMBERID) AS MEMBER_RATE_LIST");
		}
		buildQuery.append(" FROM PLAN  INNER JOIN ISSUERS ON PLAN.ISSUER_ID = ISSUERS.ID, (");
		
		for(int li=0; li< processedMemberData.size(); li++){	
			
			buildQuery.append("( SELECT PM_PLAN_RATE.RATE AS RATE, PLAN.ID AS PLAN_ID, ");
			buildQuery.append(processedMemberData.get(li).get(ID) +" AS MEMBERID, 1 AS MEMBER_CTR");
			buildQuery.append(" FROM PLAN INNER JOIN PLAN_HEALTH ON PLAN.ID = PLAN_HEALTH.PLAN_ID");
			buildQuery.append(" INNER JOIN PM_SERVICE_AREA ON PLAN.SERVICE_AREA_ID = PM_SERVICE_AREA.SERVICE_AREA_ID");
			buildQuery.append(" INNER JOIN PM_PLAN_RATE ON PLAN.ID = PM_PLAN_RATE.PLAN_ID");
			buildQuery.append(" INNER JOIN PM_ZIP_COUNTY_RATING_AREA ON PM_ZIP_COUNTY_RATING_AREA.RATING_AREA_ID = PM_PLAN_RATE.RATING_AREA_ID");
			buildQuery.append(" WHERE PLAN_HEALTH.PARENT_PLAN_ID = 0");
			buildQuery.append(" AND PM_PLAN_RATE.IS_DELETED = 'N'");
			buildQuery.append(" AND PM_SERVICE_AREA.IS_DELETED = 'N'");
			buildQuery.append(" AND PM_SERVICE_AREA.ZIP = ");
			buildQuery.append(quotingZip);
			buildQuery.append(" AND (PM_ZIP_COUNTY_RATING_AREA.ZIP = ");
			buildQuery.append(quotingZip);
			buildQuery.append(" OR PM_ZIP_COUNTY_RATING_AREA.zip IS NULL) ");
			buildQuery.append(" AND CAST(");
			buildQuery.append(processedMemberData.get(li).get(AGE));
			buildQuery.append(" AS INTEGER) >= PM_PLAN_RATE.MIN_AGE AND CAST(");
			buildQuery.append(processedMemberData.get(li).get(AGE));
			buildQuery.append(" AS INTEGER) <= PM_PLAN_RATE.MAX_AGE");			
			buildQuery.append(" AND (PM_PLAN_RATE.TOBACCO = UPPER('");
			buildQuery.append(processedMemberData.get(li).get(TOBACCO));
			buildQuery.append("') OR PM_PLAN_RATE.TOBACCO IS NULL)");
			buildQuery.append(" AND TO_DATE ('");
			buildQuery.append(effectiveDate);
			buildQuery.append("', 'YYYY-MM-DD') BETWEEN PM_PLAN_RATE.EFFECTIVE_START_DATE AND PM_PLAN_RATE.EFFECTIVE_END_DATE");			
			buildQuery.append(" AND PM_SERVICE_AREA.FIPS = '");
			buildQuery.append(quotingCountyCode);
			buildQuery.append("' AND PM_ZIP_COUNTY_RATING_AREA.COUNTY_FIPS = '");
			buildQuery.append(quotingCountyCode);
			buildQuery.append("' ");
			
			if (li < (processedMemberData.size() - 1)) {
				buildQuery.append(" ) UNION ALL ");
			}
			else {
				buildQuery.append(" ) ");
			}
		}
		buildQuery.append(" ) TEMP WHERE PLAN.ID = TEMP.PLAN_ID");
		
		buildQuery.append(" AND PLAN.ID = CAST(");
		buildQuery.append(request.getBenchmarkPlanId());		
		buildQuery.append(" AS INTEGER) AND PLAN.IS_DELETED = 'N' ");
	
		buildQuery.append(" AND PLAN.MARKET = '" + Plan.PlanMarket.SHOP.toString() + "'");		
		// on basis of household type pull respective plans
		buildQuery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic("PLAN", houseHoldType));		
		buildQuery.append(" GROUP BY PLAN.ID HAVING SUM(MEMBER_CTR) = ");
		buildQuery.append(processedMemberData.size());
		buildQuery.append(" ORDER BY TOTAL_PREMIUM");
		
		
		return buildQuery;
	}

	//@Transactional(readOnly = true)
	/*public String getProviderURL(String providerNetworkId) {
		String providerUrl = null;
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		try{			
			StringBuilder buildquery  = new StringBuilder();
			buildquery.append("SELECT NETWORK_ID, NETWORK_URL, APPLICABLE_YEAR FROM NETWORK WHERE ID = ");
			LOGGER.debug("***ProviderURL_Query:-***" +buildquery);
			buildquery.append(providerNetworkId);
			Query query = entityManager.createNativeQuery(buildquery.toString());
			Iterator rsIterator = query.getResultList().iterator();
			while (rsIterator.hasNext()) {
				Object[] tempObject = (Object[])(rsIterator.next());
				providerUrl = tempObject[PlanMgmtConstants.ONE].toString();
			}
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getCSRMultiplier", ex);
		}
		
		return providerUrl;
	}*/
	
	@Override
	public List<String> getCostNames(){
		List<String> deductibles = new ArrayList<String>();
		deductibles.add(CostNames.MAX_OOP_MEDICAL.toString());
		deductibles.add(CostNames.MAX_OOP_DRUG.toString());
		deductibles.add(CostNames.MAX_OOP_INTG_MED_DRUG.toString());
		deductibles.add(CostNames.DEDUCTIBLE_MEDICAL.toString());
		deductibles.add(CostNames.DEDUCTIBLE_DRUG.toString());
		deductibles.add(CostNames.DEDUCTIBLE_INTG_MED_DRUG.toString());
		deductibles.add(CostNames.DEDUCTIBLE_BRAND_NAME_DRUG.toString());
		deductibles.add(CostNames.ANNUAL_MAXIMUM_BENEFIT.toString());
		return deductibles;
		
	}

	// this method will return min, max dental premium for employee level
	@Override
	@Transactional(readOnly = true)
	public Map<String, Float> getEmployeeMinMaxRate(List<Map<String, String>> employeeCensusData, String empPrimaryZip, String empCountyCode, String effectiveDate, String insuranceType) {
		Map<String, Float> rateBenefits = new HashMap<String, Float>();
		EntityManager entityManager = null;
		try{
			entityManager = emf.createEntityManager();
			StringBuilder buildquery = new StringBuilder();		
			int j = 0;
			ArrayList<String> paramList = new ArrayList<String>();
			// process family data
			List<Map<String, String>> processedEmployeeData = quotingBusinessLogic.processMemberData(employeeCensusData,Plan.PlanInsuranceType.DENTAL.toString());
			String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedEmployeeData,Plan.PlanInsuranceType.DENTAL.toString());		
			String familyTier = quotingBusinessLogic.getFamilyTier(processedEmployeeData); // compute family tier
			String familyTierLookupCode = lookupService.getLookupValueCode(FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
			String countyCode = PlanMgmtConstants.EMPTY_STRING;
			String zip = PlanMgmtConstants.EMPTY_STRING;
			
			buildquery.append("SELECT p.id, sum(pre) AS total_premium ");
			buildquery.append("FROM plan p, ( ");
			// parse census data of employee's family members
			for (Map<String, String> employee : processedEmployeeData) {
				countyCode = empCountyCode;
				zip = empPrimaryZip;
				
				buildquery.append("( SELECT pdental.plan_id, prate.rate AS pre, 1 AS memberctr ");			
				buildquery.append(" FROM ");
				buildquery.append(" plan_dental pdental, pm_service_area psarea, pm_plan_rate prate, plan p2, pm_zip_county_rating_area pzcrarea ");
				buildquery.append("WHERE ");
				buildquery.append("p2.id = pdental.plan_id ");
				buildquery.append("AND pdental.parent_plan_id = 0 ");
				buildquery.append("AND p2.service_area_id = psarea.service_area_id ");
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND psarea.zip = to_number(trim(:param_" + paramList.size() + ")) ");
					paramList.add(zip);
				}else{
					buildquery.append("AND psarea.zip = :param_" + paramList.size() + " ");
					paramList.add(zip);
				}
				
				buildquery.append("AND p2.id = prate.plan_id ");
				buildquery.append("AND p2.is_deleted = 'N' ");
				buildquery.append("AND prate.is_deleted = 'N' ");
				buildquery.append("AND psarea.is_deleted = 'N' ");
				buildquery.append("AND pzcrarea.rating_area_id = prate.rating_area_id ");
				// consider tobacco usages to compute plan rate  
				if (employee.get(TOBACCO) != null && !employee.get(TOBACCO).equals(PlanMgmtConstants.EMPTY_STRING)) {
					String tobacco = employee.get(TOBACCO).toUpperCase();
					if (tobacco.equalsIgnoreCase("YES")) {
						tobacco = "Y";
					}
					if (tobacco.equalsIgnoreCase("NO")) {
						tobacco = "N";
					}
					buildquery.append("AND (prate.tobacco = :param_" + paramList.size() + " OR prate.tobacco is NULL) ");
					paramList.add(tobacco);
				}
				// in private exchange rate could be basis on age band or family tier
				buildquery.append(" AND ( ( CAST (:param_" + paramList.size() + " AS INTEGER) >= prate.min_age AND ");
				paramList.add(employee.get(AGE));
				
				buildquery.append(" CAST (:param_" + paramList.size() + " AS INTEGER) <= prate.max_age) ");
				paramList.add(employee.get(AGE));
				
				buildquery.append(" OR (CAST (:param_" + paramList.size() + " AS INTEGER) >= prate.min_age AND ");
				paramList.add(familyTierLookupCode);
				
				buildquery.append(" CAST (:param_" + paramList.size() + " AS INTEGER) <= prate.max_age ) ) ");
				paramList.add(familyTierLookupCode);
				
				buildquery.append("AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date ");
				paramList.add(effectiveDate);
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND (pzcrarea.zip = to_number(trim(:param_" + paramList.size() + ")) OR pzcrarea.zip IS NULL) ");
					paramList.add(zip);
				}else{
					buildquery.append("AND (pzcrarea.zip = :param_" + paramList.size() + " OR pzcrarea.zip IS NULL) ");
					paramList.add(zip);
				}
				
				if (!countyCode.equals(PlanMgmtConstants.EMPTY_STRING)) {
					buildquery.append(" AND psarea.fips = :param_" + paramList.size() + " ");
					paramList.add(countyCode);
					
					buildquery.append(" AND pzcrarea.county_fips = :param_" + paramList.size() + " ");
					paramList.add(countyCode);
				}
				
				buildquery.append(" ) ");
	
				if (j + 1 != processedEmployeeData.size()) {
					buildquery.append(" UNION ALL ");
				}
	
				j++;
			}
	
			buildquery.append(") temp WHERE p.id = temp.plan_id ");
			buildquery.append(" AND p.insurance_type = :param_" + paramList.size() + " ");
			paramList.add(insuranceType);
			
			buildquery.append(" AND p.market = '" + Plan.PlanMarket.SHOP.toString() + "' ");	
			buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic(ALIAS_OF_PLAN_TABLE, true));
			buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic(ALIAS_OF_PLAN_TABLE, effectiveDate, PlanMgmtConstants.NO, empPrimaryZip, empCountyCode, null, paramList));
			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic(ALIAS_OF_PLAN_TABLE, houseHoldType));
			buildquery.append(" GROUP BY p.id  HAVING sum(memberctr) = " + processedEmployeeData.size());
			buildquery.append(" ORDER BY total_premium DESC ");
	
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Compute Employer Min Max Dental Plan Rate SQL = " + buildquery.toString());
			}
	
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			List rsList = query.getResultList();
			Iterator rsIterator = rsList.iterator();
			
			List<Float> premiumList= new ArrayList<Float>();
			while (rsIterator.hasNext()) {
				Object[] objArray = (Object[]) rsIterator.next();
				Float totalPremium = Float.parseFloat(objArray[PlanMgmtConstants.ONE].toString());
				premiumList.add(totalPremium);
			}
			
			if(!premiumList.isEmpty()){		
				rateBenefits.put("maxRate",premiumList.get(PlanMgmtConstants.ZERO));
				rateBenefits.put("minRate",premiumList.get((premiumList.size()-1)));
			}	
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getEmployeeMinMaxRate", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}	
			
		return rateBenefits;

	}
	
	
	// this method will return member level rate/premium, rating area 
	@Override
	@Transactional(readOnly = true)
	public List<PlanRateBenefit> getMemberLevelPlanRate(String hiosPlanNumber, List<Map<String, String>> memberList, String effectiveDate){		
		List<PlanRateBenefit> plan = new ArrayList<PlanRateBenefit>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			StringBuilder buildquery = new StringBuilder();
			ArrayList<String> paramList = new ArrayList<String>();
			List<Plan> plansData = iPlanRepository.getPlanByHIOSID(hiosPlanNumber,Integer.parseInt(effectiveDate.substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FOUR)));
			// process family data
			if(null != plansData && !(plansData.isEmpty())){
				String insuranceType = plansData.get(PlanMgmtConstants.ZERO).getInsuranceType();
				List<Map<String, String>> processedMemberData = quotingBusinessLogic.processMemberData(memberList, insuranceType);
				String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedMemberData, insuranceType);		
				String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
				String familyTierLookupCode = lookupService.getLookupValueCode(FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code		
				String stateName = quotingBusinessLogic.getApplicantStateCode(processedMemberData);
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
				String applicableYear = effectiveDate.substring(0,4);
				
				buildquery.append("SELECT p.id AS plan_id, sum(pre) AS total_premium, ");
				if(PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB) || StringUtils.isEmpty(configuredDB)) {
					buildquery.append(" listagg(memberid, ',')  within group (order by memberid) as memberlist, ");
					buildquery.append(" listagg(pre, ',')  within group (order by memberid) as memberprelist, ");
					buildquery.append(" listagg(rname, ',')  within group (order by memberid) as memberrelist, rate_option ");
				} else if(PlanMgmtConstants.DB_POSTGRESQL.equalsIgnoreCase(configuredDB)) {
				buildquery.append(" string_agg(CAST (memberid AS VARCHAR), ',' order by memberid) as memberlist, ");
				buildquery.append(" string_agg(to_char(CAST (pre AS DOUBLE PRECISION), '9999D99'), ',' order by memberid) as memberprelist, ");
				buildquery.append(" string_agg(rname, ',' order by memberid) as memberrelist, rate_option ");
				}
				buildquery.append(" FROM plan p, ");		
				buildquery.append(" ( ");

				String applicantZip = null;
				String applicantCounty = null;

				for (int i = 0; i < processedMemberData.size(); i++) {
					buildquery.append("( SELECT p2.id, prate.rate AS pre, ");
					buildquery.append(":param_" + paramList.size() + " as memberid, prarea.rating_area as rname, 1 as memberctr, prate.rate_option as rate_option ");
					paramList.add(processedMemberData.get(i).get(ID));
					
					buildquery.append(" FROM ");
					buildquery.append(" pm_service_area psarea, pm_plan_rate prate, plan p2, pm_rating_area prarea ");
					buildquery.append(" WHERE ");
					buildquery.append(" p2.issuer_Plan_number = :param_" + paramList.size());
					paramList.add(hiosPlanNumber);
					
					buildquery.append(" AND p2.service_area_id = psarea.service_area_id ");
					
					if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
						buildquery.append(" AND psarea.zip = to_number(trim(:param_" + paramList.size() + "))");
						paramList.add(processedMemberData.get(i).get(ZIP));
					}else{
						buildquery.append(" AND psarea.zip = :param_" + paramList.size());
						paramList.add(processedMemberData.get(i).get(ZIP));
					}

					if (null == applicantZip) {
						applicantZip = processedMemberData.get(i).get(ZIP);
					}
					
					buildquery.append(" AND prarea.id = prate.rating_area_id ");
					buildquery.append(" AND p2.id = prate.plan_id ");
					buildquery.append(" AND p2.is_deleted = 'N' ");
					buildquery.append(" AND psarea.is_deleted = 'N' ");
					buildquery.append(" AND pzcrarea.rating_area_id = prate.rating_area_id ");
					
					// prepare rate query
					buildquery.append(quotingBusinessLogic.buildRateLogic("prate", stateName, houseHoldType, familyTierLookupCode, processedMemberData.get(i).get(PlanMgmtConstants.AGE), processedMemberData.get(i).get(PlanMgmtConstants.TOBACCO), effectiveDate, paramList));
					
					buildquery.append(" AND psarea.fips = :param_" + paramList.size() + " ");
					paramList.add(processedMemberData.get(i).get(COUNTYCODE));

					if (null == applicantCounty) {
						applicantCounty = processedMemberData.get(i).get(COUNTYCODE);
					}
					buildquery.append(" ) ");

					if (i + 1 != processedMemberData.size()) {
						buildquery.append(" UNION ALL ");
					}

				}

				buildquery.append(") temp WHERE p.id = temp.id ");
				buildquery.append(" AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date ");
				paramList.add(effectiveDate);
				
				buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic(ALIAS_OF_PLAN_TABLE, true));
				buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic(ALIAS_OF_PLAN_TABLE, effectiveDate, PlanMgmtConstants.NO, applicantZip, applicantCounty, null, paramList));
				buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic(ALIAS_OF_PLAN_TABLE, houseHoldType));
				buildquery.append(" GROUP BY p.id, rate_option ");
				buildquery.append(" HAVING sum(memberctr) = ");
				buildquery.append(processedMemberData.size());

				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Compute Member Level Plan Rate SQL = " + buildquery.toString());
				}

				Query query = entityManager.createNativeQuery(buildquery.toString());
				for (int i = 0; i < paramList.size(); i++) {
					query.setParameter("param_" + i, paramList.get(i));
				}
				List rsList = query.getResultList();
				Iterator rsIterator = rsList.iterator();
				Object[] objArray = null;
				PlanRateBenefit planMap = null;
				String[] memberids = null;
				RatingArea ratingArea = null;
				Map<String, String> memberData = null;
				String[] memberpremiums = null;
				String[] memberregions = null;
				String rateOption  = null;
				List<Map<String, String>> memberInfo = null;
				
				while (rsIterator.hasNext()) {
					objArray= (Object[]) rsIterator.next();
					planMap = new PlanRateBenefit();
					
					memberids = objArray[PlanMgmtConstants.TWO].toString().split(",");
					memberpremiums = objArray[PlanMgmtConstants.THREE].toString().split(",");
					memberregions = objArray[PlanMgmtConstants.FOUR].toString().split(",");
					rateOption = objArray[PlanMgmtConstants.FIVE].toString();
							
					// calculate cost, region for each member
					memberInfo = new ArrayList<Map<String, String>>();
					for (Map<String, String> inputMemberData : memberList) {
						memberData = new HashMap<String, String>();
						
						memberData.put("id", inputMemberData.get(ID));
						memberData.put("premium", PlanMgmtConstants.ZERO_STR);
						for (int ctr = 0; ctr < memberids.length; ctr++) {
							if (memberids[ctr].equalsIgnoreCase(inputMemberData.get(ID))) {
								// if rate option is family i.e. community rating, 
								// then set premium for applicant only and other family member's premium would be zero. 
								if (rateOption.equalsIgnoreCase("F")){
									if(inputMemberData.get(RELATION).equalsIgnoreCase("member") || inputMemberData.get(RELATION).equalsIgnoreCase("self")) {
										memberData.put("premium", memberpremiums[ctr].trim());
									}														
									}else{
										memberData.put("premium", memberpremiums[ctr].trim());
									}	
									memberData.put("region", memberregions[ctr].replaceAll("[a-zA-Z ]", PlanMgmtConstants.EMPTY_STRING));
							 }

						}
						if (memberData.get("region") == null) {
							ratingArea = ratingAreaService.getRatingAreaByZip(inputMemberData.get(ZIP), inputMemberData.get(COUNTYCODE));
							if (ratingArea != null && ratingArea.getRatingArea() !=null) {
								memberData.put("region", ratingArea.getRatingArea().replaceAll("[a-zA-Z ]", PlanMgmtConstants.EMPTY_STRING));
							} else {
								memberData.put("region", memberregions[0].replaceAll("[a-zA-Z ]", PlanMgmtConstants.EMPTY_STRING));
							}
						}
						memberInfo.add(memberData);
					}
					
					planMap.setId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
					planMap.setHiosPlanNumber(hiosPlanNumber);
					planMap.setPlanDetailsByMember(memberInfo);
					
					plan.add(planMap);
				}
				rsIterator = null;
				
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getMemberLevelPlanRate: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return plan;
	}
	
	public EmployerQuotingDTO getEmployerPlanRateHelper(EmployerQuotingDTO employerQuotingDTO){
		if(null == employerQuotingDTO 					
			|| null == employerQuotingDTO.getEffectiveDate()
			|| 0 == employerQuotingDTO.getEffectiveDate().trim().length()
			|| null == employerQuotingDTO.getEmpPrimaryZip()
			|| 0 == employerQuotingDTO.getEmpPrimaryZip().trim().length()
			|| null == employerQuotingDTO.getEmpCountyCode()
			|| 0 == employerQuotingDTO.getEmpCountyCode().trim().length()				
			|| null == employerQuotingDTO.getPlanDTOList()
			|| employerQuotingDTO.getPlanDTOList().isEmpty()
			|| null == employerQuotingDTO.getCensusList()
			|| employerQuotingDTO.getCensusList().isEmpty()	
			) {
			return employerQuotingDTO;
		}
		else {
		
			List<ArrayList<ArrayList<Map<String, String>>>> employeeList = employerQuotingDTO.getCensusList();				
			String empPrimaryZip = employerQuotingDTO.getEmpPrimaryZip().trim(); // employer primary zipcode
			String empCountyCode = employerQuotingDTO.getEmpCountyCode().trim(); // employer primary county code
			List<PlanDTO> planList = employerQuotingDTO.getPlanDTOList(); // reference plan id list
			String effectiveDate = employerQuotingDTO.getEffectiveDate().trim(); // effective date	
			List<PlanDTO> newPlanDTOList = new ArrayList();
			
			// process each reference plan 
			for(PlanDTO planDTO : planList){
				String planId =  planDTO.getPlanId().toString();
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Compute rate for Plan id : " + SecurityUtil.sanitizeForLogging(planId));
				}
				List<EmployeePlanDTO> employeePlanDTOs = new ArrayList();					 
				float totalemployeePremium = 0;
				
				// compute rate for each employee on household basis i.e employee + his/her family
				for (List<ArrayList<Map<String, String>>> employee: employeeList) {					
					int empId = Integer.parseInt(employee.get(0).get(0).get("id"));						
					List<Map<String, String>> employeeCensusData = employee.get(0);
					Map<String,Float> responseData ;						
					float employeePremium = 0;	
					EmployeePlanDTO employeePlanDTO = new EmployeePlanDTO();
				
					// use employer's zipcode, county code for quoting
					responseData = this.getEmployeePlanRate(employeeCensusData, empPrimaryZip, empCountyCode, planId, effectiveDate, false);
					
					// if plans not found using employee's own zipcode, county code then use employer's zipcode, county code for quoting
					if(responseData.isEmpty()){
						responseData = this.getEmployeePlanRate(employeeCensusData, empPrimaryZip, empCountyCode, planId, effectiveDate, true);
						if(!responseData.isEmpty()){	
							employeePremium = responseData.get("indvPremium");	 //  each employee's own premium						
						}
					}
					else{
						employeePremium = responseData.get("indvPremium");	 //  each employee's own premium					
					}
					
					totalemployeePremium = totalemployeePremium + employeePremium;		// Adding each employee's own premium				
					
					employeePlanDTO.setEmployeeId(empId);
						
					if(!responseData.isEmpty()){
						employeePlanDTO.setTotalPremium(responseData.get("totalPremium")); // each employee's household premium	
						employeePlanDTO.setIndividualPremium(employeePremium); //  each employee's own premium	
						employeePlanDTO.setDependentPremium(responseData.get("depenPremium")); //  each employee's dependents premium	
					}
					employeePlanDTOs.add(employeePlanDTO);
				}
				
				// if any plan does not have rate, then don't add that plan in response DTO list
				if(Float.floatToRawIntBits(totalemployeePremium) != 0){
					PlanDTO newplanDTO = new PlanDTO();
					newplanDTO.setPlanId(planDTO.getPlanId());	// reset plan id
					newplanDTO.setIssuerPlanNumber(planDTO.getIssuerPlanNumber()); // reset hios plan number
					newplanDTO.setTierName(planDTO.getTierName()); // reset plan tier name
					newplanDTO.setSumEmployeesPremium(totalemployeePremium); // sum of all employee's own premium	
					newplanDTO.setEmployeePlanDTOs(employeePlanDTOs); // set employee plan DTO 
					newPlanDTOList.add(newplanDTO);
					newplanDTO = null;	
				}	
				
			}				
			
			employerQuotingDTO.setPlanDTOList(newPlanDTOList);	
			newPlanDTOList = null;
		}
		
		return employerQuotingDTO;
	}
		
	
	@Override
	@Transactional(readOnly = true)
	public List<PlanRateBenefit> getLowestDentalPlan(List<Map<String, String>> memberList, String effectiveDate, String exchangeType, String tenantCode) {
		List<PlanRateBenefit> planRateBenefitsList = new ArrayList<PlanRateBenefit>();
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("-------- Inside getLowestDentalPlan --------");
			}
			
			List<Map<String, String>> processedMemberData =  quotingBusinessLogic.processMemberData(memberList, Plan.PlanInsuranceType.DENTAL.toString());
			String houseHoldType = quotingBusinessLogic.getHouseHoldType(processedMemberData, Plan.PlanInsuranceType.DENTAL.toString());			
			String familyTier = quotingBusinessLogic.getFamilyTier(processedMemberData); // compute family tier
			String familyTierLookupCode = lookupService.getLookupValueCode(PlanMgmtConstants.FAMILY_LOOKUP_NAME, familyTier); // compute family tier lookup code
			String stateName = quotingBusinessLogic.getApplicantStateCode(processedMemberData);
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
			String IHCfamilyTierLookupCode = PlanMgmtConstants.EMPTY_STRING;
			String currentExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
			int applicantAge = quotingBusinessLogic.getApplicantAge(processedMemberData);
		 	boolean doQuoteIssuerIHC = quotingBusinessLogic.isAgeAllowForIHCQuoting(applicantAge);
		 	String applicableYear = effectiveDate.substring(0,4);
		 	
			// compute family tier look up value for carrier IHC, when exchange is PHIX and quoting for OFF exchange plans 
			// HIX-53639
			if(exchangeType.equalsIgnoreCase(Plan.EXCHANGE_TYPE.OFF.toString()) && currentExchangeType.equals(GhixConstants.PHIX)){
				IHCfamilyTierLookupCode = quotingBusinessLogic.getIHCFamilyTierLookupCode(memberList);
			}

			StringBuilder buildquery = new StringBuilder();
			ArrayList<String> paramList = new ArrayList<String>();
			
			buildquery.append("SELECT p.id, (CASE WHEN rate_option = 'F' THEN pre ELSE SUM(pre) END) AS total_premium, p.name, logo_url, p.ISSUER_ID, rate_option, ageandfamily_rate, hios_issuer_id ");			
			buildquery.append(" FROM ");
			buildquery.append("plan p, pm_tenant_plan tp, tenant t, ");
			buildquery.append(" ( ");

			String applicantZip = null;
			String applicantCounty = null;

			for (int i = 0; i < processedMemberData.size(); i++) {
				buildquery.append("( SELECT prate.rate AS pre,  pdental.plan_id,  1 as memberctr, prate.rate_option AS rate_option, i2.logo_url, i2.hios_issuer_id, pdental.is_community_rate AS ageandfamily_rate ");
				buildquery.append("FROM ");
				buildquery.append(" plan_dental pdental, pm_service_area psarea, pm_rating_area prarea, pm_plan_rate prate, plan p2, issuers i2 ");
				buildquery.append("WHERE ");
				buildquery.append("p2.id = pdental.plan_id AND p2.issuer_id = i2.id ");
				buildquery.append("AND p2.service_area_id = psarea.service_area_id ");
				
				if(StringUtils.isBlank(configuredDB) || PlanMgmtConstants.DB_ORACLE.equalsIgnoreCase(configuredDB)) {
					buildquery.append("AND psarea.zip = to_number(trim(:param_" + paramList.size() + ")) ");
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.ZIP));
				}else{
					buildquery.append("AND psarea.zip = :param_" + paramList.size() + " ");
					paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.ZIP));
				}

				if (null == applicantZip) {
					applicantZip = processedMemberData.get(i).get(PlanMgmtConstants.ZIP);
				}
				
				buildquery.append("AND p2.id = prate.plan_id ");
				buildquery.append("AND p2.is_deleted = 'N' ");
				buildquery.append("AND prate.is_deleted = 'N' ");
				buildquery.append("AND psarea.is_deleted = 'N' ");
				buildquery.append("AND prarea.id = prate.rating_area_id ");
				buildquery.append("AND prate.rating_area_id = ");
				
				// -------- changes for HIX-100092 starts --------
				buildquery.append(quotingBusinessLogic.buildSearchRatingAreaLogic(stateCode, paramList, processedMemberData, i, applicableYear, configuredDB));
				// -------- changes for HIX-100092 ends --------
				
				buildquery.append(quotingBusinessLogic.buildRateLogicForDentalQuoting("prate", "i2", stateName, houseHoldType, familyTierLookupCode, processedMemberData.get(i).get(AGE), processedMemberData.get(i).get(PlanMgmtConstants.TOBACCO), effectiveDate, exchangeType, doQuoteIssuerIHC, IHCfamilyTierLookupCode, paramList));
				
				buildquery.append(" AND psarea.fips = :param_" + paramList.size() + " ");
				paramList.add(processedMemberData.get(i).get(PlanMgmtConstants.COUNTYCODE));
				buildquery.append(" ) ");

				if (null == applicantCounty) {
					applicantCounty = processedMemberData.get(i).get(PlanMgmtConstants.COUNTYCODE);
				}
				
				if (i + 1 != processedMemberData.size()) {
					buildquery.append(" UNION ALL ");
				}
			}

			buildquery.append(") temp ");
			buildquery.append(" WHERE ");
			buildquery.append(" p.id = temp.plan_id ");
			buildquery.append(" AND TO_DATE (:param_" + paramList.size() + ", 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date ");
			paramList.add(effectiveDate);			
			buildquery.append(quotingBusinessLogic.buildPlanCertificationLogic(ALIAS_OF_PLAN_TABLE, true));
			buildquery.append(quotingBusinessLogic.buildEnrollmentAvailabilityLogic(ALIAS_OF_PLAN_TABLE, effectiveDate, PlanMgmtConstants.NO, applicantZip, applicantCounty, null, paramList));
			buildquery.append(" AND p.market = '");
			buildquery.append(Plan.PlanMarket.INDIVIDUAL.toString());
			buildquery.append("' AND p.insurance_type='DENTAL' AND p.exchange_type = :param_");
			buildquery.append(paramList.size());
			paramList.add(exchangeType.toUpperCase());	
  		    buildquery.append(" AND p.id = tp.plan_id");
			buildquery.append(" AND tp.tenant_id = t.id");
			buildquery.append(" AND t.code = :param_");
			buildquery.append(paramList.size());
			paramList.add(tenantCode.toUpperCase());
			buildquery.append(quotingBusinessLogic.buildHouseHoldAvailabilityLogic(ALIAS_OF_PLAN_TABLE, houseHoldType));					
			buildquery.append(" GROUP BY p.id, rate_option, pre, p.name, logo_url, p.ISSUER_ID, ageandfamily_rate, hios_issuer_id ");
			buildquery.append("HAVING sum(memberctr) = ");
			buildquery.append(processedMemberData.size());
			buildquery.append(" ORDER BY total_premium ASC ");
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL == " + buildquery.toString());
			}
			
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			List rsList = query.getResultList();
			Iterator rsIterator = rsList.iterator();
			String rateOption = null;
			
			while (rsIterator.hasNext()) {
				String logoURL = null;
				Object[] objArray = (Object[]) rsIterator.next();
				PlanRateBenefit planMap = new PlanRateBenefit();
				planMap.setId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
				planMap.setPremium(Float.parseFloat(objArray[PlanMgmtConstants.ONE].toString()));
				planMap.setName((objArray[PlanMgmtConstants.TWO] == null) ? PlanMgmtConstants.EMPTY_STRING : objArray[PlanMgmtConstants.TWO].toString());
				//planMap.setIssuerLogo((objArray[PlanMgmtConstants.THREE] == null) ? PlanMgmtConstants.EMPTY_STRING : appUrl + DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(objArray[PlanMgmtConstants.THREE].toString()) );
				if(null != objArray[PlanMgmtConstants.THREE]) {
					logoURL = objArray[PlanMgmtConstants.THREE].toString();
				}
				planMap.setIssuerLogo(PlanMgmtUtil.getIssuerLogoURL(logoURL, objArray[PlanMgmtConstants.SEVEN].toString(), null, appUrl));
				
				
				//HIX-62493,
				// if plan have age band rate and family rate both  
				rateOption = objArray[PlanMgmtConstants.FIVE].toString();
				if((objArray[PlanMgmtConstants.SIX] != null) && objArray[PlanMgmtConstants.SIX].toString().equalsIgnoreCase("Y")  ){
					//	if number of quoted member is more than 1 then add those plans having family rate
					if (rateOption.equalsIgnoreCase(PlanMgmtConstants.FAMILY_RATE_OPTION) && processedMemberData.size() > 1) {
						planRateBenefitsList.add(planMap);
						break;
					}
					//	if number of quoted member is 1 then add those plans having age band rate
					if (rateOption.equalsIgnoreCase(PlanMgmtConstants.AGE_RATE_OPTION) && processedMemberData.size() == 1) {
						planRateBenefitsList.add(planMap);
						break;
					}
				}else{
					planRateBenefitsList.add(planMap);
					break;
				}
												
			}	
			
		}catch(Exception ex){
			LOGGER.error("Exceptin Occured in getLowestDentalPlan", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
				
		return planRateBenefitsList;
	}
	
	
	

	@Override
	@Transactional(readOnly = true)
	public LifeSpanEnrollmentQuotingResponseDTO lifeSpanEnrollmentQuoting(LifeSpanEnrollmentQuotingRequestDTO lifeSpanEnrollmentQuotingRequestDTO, LifeSpanEnrollmentQuotingResponseDTO lifeSpanEnrollmentQuotingResponseDTO, String insuranceType){
		double benchMarkPremium = PlanMgmtConstants.ZERO;
		String benchMarkHiosPlanId = PlanMgmtConstants.EMPTY_STRING;
		List<Map<String, String>> unprocessedCensusData = null;
		Map<String, String> member = null;
		LifeSpanPeriodLoopDTO periodLoopDTOSetter = null;
		List<LifeSpanMemberResponseDTO> memberDTOList = null; 
		List<LifeSpanPeriodLoopDTO> periodLoopDTOList = new ArrayList<LifeSpanPeriodLoopDTO>();
		
		try{
			for(LifeSpanPeriodLoopDTO periodLoopDTO : lifeSpanEnrollmentQuotingRequestDTO.getPeriodLoopList()){
				periodLoopDTOSetter = new LifeSpanPeriodLoopDTO();
				benchMarkPremium = PlanMgmtConstants.ZERO;
				unprocessedCensusData = new ArrayList<Map<String, String>>();
				memberDTOList = new ArrayList<LifeSpanMemberResponseDTO>();
				
				for(LifeSpanMemberRequestDTO memberDTO : periodLoopDTO.getRequestMemberList()){
					member = new HashMap<String, String>();
					member.put(PlanMgmtConstants.ID, String.valueOf(memberDTO.getId()) );
					member.put(PlanMgmtConstants.RELATION, memberDTO.getRelation().trim());
					member.put(PlanMgmtConstants.ZIP, periodLoopDTO.getSubscriberZipCode().trim());
					member.put(PlanMgmtConstants.COUNTYCODE, periodLoopDTO.getSubscriberCountyCode().trim());
					member.put(PlanMgmtConstants.AGE, String.valueOf(memberDTO.getAge() > 0 ? memberDTO.getAge() :
						calculateAge(memberDTO.getDob().trim(), memberDTO.getCoverageStartDate().trim(), PlanMgmtConstants.REQUIRED_DATE_FORMAT )));
					member.put(PlanMgmtConstants.TOBACCO, memberDTO.getTobacco().trim());
					member.put(PlanMgmtConstants.MEMBER_COVERAGE_DATE, memberDTO.getCoverageStartDate().trim());
					member.put(PlanMgmtConstants.DAYS_ACTIVE, memberDTO.getDaysActive());
					unprocessedCensusData.add(member);
				}
	
				// HIX-100131 :: Convert requested Relationship Id to Relation String
				unprocessedCensusData = quotingBusinessLogic.convertRelationshipIdToString(unprocessedCensusData);
		    	
				List<Map<String, String>> processedCensusData = quotingBusinessLogic.processMemberData(unprocessedCensusData, insuranceType);
				
				// COMPUTE BENCHMARK PREMIUM ONLY FOR HEALTH PLAN
				if(PlanMgmtConstants.HEALTH.equalsIgnoreCase(insuranceType)){
					Map <String, String> benchMarkPlanIdPremiumMap = getBenchmarkRateForEnrollment(unprocessedCensusData, periodLoopDTO.getPeriodEffectiveDate().trim(), null, null, null, lifeSpanEnrollmentQuotingResponseDTO.getHiosIssuerId());
					
					if(benchMarkPlanIdPremiumMap != null && benchMarkPlanIdPremiumMap.containsKey(PlanMgmtConstants.TOTALPREMIUM)){
						benchMarkHiosPlanId = benchMarkPlanIdPremiumMap.get(PlanMgmtConstants.PLANID);
						benchMarkPremium = Double.parseDouble( benchMarkPlanIdPremiumMap.get(PlanMgmtConstants.TOTALPREMIUM));
					}	
				}	
				
				// QUOTE FOR HOUSEHOLD DATA FOR EACH PERIOD
				memberDTOList = lifeSpanEnrollmentQuotingHelper(processedCensusData, unprocessedCensusData, lifeSpanEnrollmentQuotingRequestDTO.getPlanId());
				
				// IF PLAN IS NOT AVAILABLE FOR QUOTING THEN SEND ERROR MESSAGE
				if(memberDTOList.size() == 0 ){
					periodLoopDTOSetter.setStatus(GhixConstants.RESPONSE_FAILURE);
					periodLoopDTOSetter.setErrorCode(PlanMgmtErrorCodes.ErrorCode.PLAN_NOT_lIVE.getCode());
					periodLoopDTOSetter.setErrorMsg("Plan is not available for given household data");
				}else{
					periodLoopDTOSetter.setStatus(GhixConstants.RESPONSE_SUCCESS);
				}
					
				periodLoopDTOSetter.setPeriodId(periodLoopDTO.getPeriodId());
				periodLoopDTOSetter.setSlspPremium(benchMarkPremium);
				periodLoopDTOSetter.setSlspHiosPlanId(benchMarkHiosPlanId);
				periodLoopDTOSetter.setPeriodEffectiveDate(periodLoopDTO.getPeriodEffectiveDate());
				periodLoopDTOSetter.setSubscriberCountyCode(periodLoopDTO.getSubscriberCountyCode());
				periodLoopDTOSetter.setSubscriberZipCode(periodLoopDTO.getSubscriberZipCode());
				
				periodLoopDTOSetter.setResponseMemberList(memberDTOList);
				
				periodLoopDTOList.add(periodLoopDTOSetter);
				lifeSpanEnrollmentQuotingResponseDTO.setPeriodLoopList(periodLoopDTOList);
			}
		}catch(Exception ex){
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in lifeSpanEnrollmentQuoting service: ",ex);
			LOGGER.error("Exception occured in lifeSpanEnrollmentQuoting service : ", ex);
		}	
		
		return lifeSpanEnrollmentQuotingResponseDTO;
	}
	
	
	private List<LifeSpanMemberResponseDTO> lifeSpanEnrollmentQuotingHelper(List<Map<String, String>> processedHouseholdData, List<Map<String, String>> unprocessedHouseholdData, String planId){
		List<LifeSpanMemberResponseDTO> memberDTOList = new  ArrayList<LifeSpanMemberResponseDTO>();
		LifeSpanMemberResponseDTO memberResponseDTO = null; 
		String[] memberids = null;
		String[] memberpremiums = null;
		String rateOption = "";
		EntityManager entityManager = null;
		
		try{
			entityManager = emf.createEntityManager();
			ArrayList<String> paramList = new ArrayList<String>();
			
			StringBuilder buildQuery = lifespanEnrollmentQuptingQueryBuilder.buildQuery(paramList, processedHouseholdData, configuredDB, planId);
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("Fetching life span plan, SQL == " + buildQuery.toString());
			}
	
			Query query = entityManager.createNativeQuery(buildQuery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
			}
			
			List rsList = query.getResultList();
			if (rsList == null || rsList.isEmpty()) {
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("Record not found for Plan Id = " + planId);
				}
			} else {
				Iterator rsIterator = rsList.iterator();
				Object[] objArray = null;						
				while (rsIterator.hasNext()) {
					objArray = (Object[]) rsIterator.next();
					memberids = objArray[PlanMgmtConstants.ONE].toString().split(",");
					memberpremiums = objArray[PlanMgmtConstants.TWO].toString().split(",");
					rateOption = (objArray[PlanMgmtConstants.THREE] != null ) ? objArray[PlanMgmtConstants.THREE].toString() : PlanMgmtConstants.EMPTY_STRING;
					
					for (Map<String, String> inputMemberData : unprocessedHouseholdData) {
						memberResponseDTO = new LifeSpanMemberResponseDTO();
						memberResponseDTO.setMemberLevelPreContribution(PlanMgmtConstants.ZERO_STR);
						memberResponseDTO.setId(Integer.parseInt(inputMemberData.get(PlanMgmtConstants.ID)));
						memberResponseDTO.setDaysActive(inputMemberData.get(PlanMgmtConstants.DAYS_ACTIVE));
						memberResponseDTO.setAge(Integer.parseInt(inputMemberData.get(PlanMgmtConstants.AGE)));
						memberResponseDTO.setRatingArea((objArray[PlanMgmtConstants.FOUR] != null ) ? objArray[PlanMgmtConstants.FOUR].toString().replaceAll("[a-zA-Z ]", PlanMgmtConstants.EMPTY_STRING) : PlanMgmtConstants.EMPTY_STRING);
						
						for (int ctr = 0; ctr < memberids.length; ctr++) {
							if (memberids[ctr].equalsIgnoreCase(inputMemberData.get(PlanMgmtConstants.ID))) {
								// if rate option is family i.e. community rating, 
								// then set premium for applicant only and other family member's premium would be zero. 
								if (rateOption.equalsIgnoreCase("F")){
									if(inputMemberData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.MEMBER) || inputMemberData.get(PlanMgmtConstants.RELATION).equalsIgnoreCase(PlanMgmtConstants.SELF)) {
										memberResponseDTO.setMemberLevelPreContribution(memberpremiums[ctr].trim());
									}														
								}else{
									memberResponseDTO.setMemberLevelPreContribution(memberpremiums[ctr].trim());
								}	
							}
						}
						memberDTOList.add(memberResponseDTO);
					}
				}	
			}
		}catch(Exception ex){
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in lifeSpanEnrollmentQuotingHelper ",ex);
			LOGGER.error("Exception occured in lifeSpanEnrollmentQuotingHelper: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		
		return memberDTOList;
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public float getLowestPremium(String insuranceType, String exchangeType, String tenantCode, String effectiveDate, List<com.getinsured.hix.dto.planmgmt.Member> memberListDTO, boolean showCatastrophicPlan, String restrictHiosIds){
		EntityManager entityManager = null;
		float lowestPremium = 0;
		List<Map<String, String>> memberList = null;
		Map<String, String> member = null;
		List<String> restrictHiosIdListOfTenant = null;
		boolean joinToIssuers = false;
		
		try{
			entityManager = emf.createEntityManager();
			memberList = new ArrayList<Map<String, String>>();
			for(com.getinsured.hix.dto.planmgmt.Member memberDTO : memberListDTO){
				member = new HashMap<String, String>();
				member.put(PlanMgmtConstants.ID, String.valueOf(memberDTO.getId()) );
				member.put(PlanMgmtConstants.RELATION, memberDTO.getRelation().trim());
				member.put(PlanMgmtConstants.ZIP, memberDTO.getZipCode().trim());
				member.put(PlanMgmtConstants.COUNTYCODE, memberDTO.getCountyCode().trim());
				member.put(PlanMgmtConstants.AGE, String.valueOf(memberDTO.getAge()).trim());
				member.put(PlanMgmtConstants.TOBACCO, memberDTO.getTobacco().trim());
				memberList.add(member);
			}
			
			List<Map<String, String>> processedHouseholdData = quotingBusinessLogic.processMemberData(memberList, insuranceType);			// process member data	
			ArrayList<String> paramList = new ArrayList<String>();
			
			// HIX-82090 - if affliate's restrictHiosId flag value is set as ON, then pull tenant's restricted Hios Issuer Id list 
			if(restrictHiosIds.equalsIgnoreCase(PlanMgmtConstants.ON)){
				restrictHiosIdListOfTenant = tenantService.getTenant(tenantCode).getConfiguration().getHiosIdConfiguration();
			}
			if((restrictHiosIds != null && restrictHiosIds.equals(PlanMgmtConstants.ON)) &&
			   (restrictHiosIdListOfTenant != null && restrictHiosIdListOfTenant.size() > 0)){
						joinToIssuers = true;
			}
			
			// prepare SQL
			StringBuilder buildquery = lowestPremiumQueryBuilder.buildQuery(paramList, processedHouseholdData, insuranceType, exchangeType, tenantCode, effectiveDate, configuredDB, showCatastrophicPlan, restrictHiosIds, joinToIssuers);
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("SQL == " + buildquery.toString());
			}
			
			Query query = entityManager.createNativeQuery(buildquery.toString());
			for (int i = 0; i < paramList.size(); i++) {
				query.setParameter("param_" + i, paramList.get(i));
				//LOGGER.info("param_" + i + " >> " + paramList.get(i));
			}
			
			if(joinToIssuers){
				query.setParameter("paramRestrictHiosIdList", restrictHiosIdListOfTenant);
			}
			
			List rsList = query.getResultList();
			if (null != rsList && rsList.size() > 0 ) {
				Iterator rsIterator = rsList.iterator();
				Object[] objArray = null;		
				String rateOption = null;
				float householdPremium = 0;
				List <Float> premiumArrayList = new ArrayList<Float>();
				List<String> hiosPlaIdList = new ArrayList<String>();
				
				while (rsIterator.hasNext()) {
					objArray = (Object[]) rsIterator.next();
					householdPremium = Float.parseFloat(objArray[PlanMgmtConstants.ONE].toString());
					rateOption = objArray[PlanMgmtConstants.TWO].toString();
					if (rateOption.equalsIgnoreCase("F")) {
						householdPremium = (householdPremium / processedHouseholdData.size());
					}
					
					// HIX-79254, On OFF exchange flow, when we show ON and OFF Exchange plans together, OFF exchange plan goes for first priority
					// if any ON exchange plan have same HIOS plan number (14 digit) in OFF exchange plan list then don't include that ON exchange plan in response. 
					if(PlanMgmtConstants.PLAN_EXCHANGE_TYPE_OFF.equalsIgnoreCase(exchangeType)){ 
						if(hiosPlaIdList != null && !hiosPlaIdList.contains(objArray[PlanMgmtConstants.THREE].toString().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FOURTEEN))){			
							hiosPlaIdList.add(objArray[PlanMgmtConstants.THREE].toString().substring(PlanMgmtConstants.ZERO, PlanMgmtConstants.FOURTEEN));
							premiumArrayList.add(householdPremium);
						}
					}else{
						premiumArrayList.add(householdPremium);
					}
				}	
				
				// sort premium			
				Collections.sort(premiumArrayList);
				lowestPremium = premiumArrayList.get(0);
			}		
					
		}catch(Exception ex){
			giExceptionHandler.recordFatalException(Component.PLANMGMT, null, "Exception occured in getLowestPremium ",ex);
			LOGGER.error("Exception occured in getLowestPremium: ", ex);
		}finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
			 }
		}
		return lowestPremium;
	}
}



class CompareAge implements Comparator<Map<String, String>> {
	private static final String AGE = "age";

	@Override
	public int compare(Map<String, String> census1, Map<String, String> census2) {
		return (Integer.parseInt(census1.get(AGE)) > Integer.parseInt(census2.get(AGE))) ? -1 : (Integer.parseInt(census1.get(AGE)) < Integer.parseInt(census2
				.get(AGE))) ? 1 : 0;
	}
		
}

class ComparePremium implements Comparator<Map<String, String>> {
	@Override
	public int compare(Map<String, String> premiumAndIdMap1, Map<String, String> premiumAndIdMap2) {
		return (Double.parseDouble(premiumAndIdMap1.get(PlanMgmtConstants.TOTALPREMIUM)) < Double.parseDouble(premiumAndIdMap2.get(PlanMgmtConstants.TOTALPREMIUM))) ? -1 : (Double.parseDouble(premiumAndIdMap1.get(PlanMgmtConstants.TOTALPREMIUM)) > Double.parseDouble(premiumAndIdMap2
				.get(PlanMgmtConstants.TOTALPREMIUM))) ? 1 : 0;
	}
		
}
