package com.getinsured.hix.planmgmt.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.util.exception.GIExceptionHandler;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;

public class ProcessDisplayDataFromMap {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProcessDisplayDataFromMap.class);
	
	@Autowired
	private GIExceptionHandler giExceptionHandler;
	
	
	public String getBenefitDisplayDataFromMap(String benefitName, List<Map<String, String>> planHealthBenefitsListForUpdate, String insuranceType){
		//final String[] benefitArray = allBenefitsArray;
		String benefitString = null;
		try{
			String mapData1 = PlanMgmtConstants.N_A;
			String mapData2 = PlanMgmtConstants.N_A;
			String mapData3 = PlanMgmtConstants.N_A;
			String mapData4 = PlanMgmtConstants.N_A;
			String mapData5 = PlanMgmtConstants.N_A;
			String mapData6 =PlanMgmtConstants.N_A;
			
			//if(benefitName.equalsIgnoreCase(benefitArray[PlanMgmtConstants.ZERO])){
				for(Map<String,String> displayMap : planHealthBenefitsListForUpdate){
				
					if(StringUtils.isNotBlank(displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName + ".newtworkT1Display"))){
						mapData1 = displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName +".newtworkT1Display");
					}/*else{
						mapData1 = PlanMgmtConstants.N_A;
					}*/
					
					if(StringUtils.isNotBlank(displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName + ".newtworkT2Display"))){
						mapData2 = displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName +".newtworkT2Display");
					}/*else{
						mapData2 = PlanMgmtConstants.N_A;
					}*/
					
					if(StringUtils.isNotBlank(displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName + ".outnetworkDisplay"))){
						mapData3 = displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName +".outnetworkDisplay");
					}/*else{
						mapData3 = PlanMgmtConstants.N_A;
					}*/
					
					if(StringUtils.isNotBlank(displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName + ".limitExcepDisplay"))){
						mapData4 = displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName +".limitExcepDisplay");
					}/*else{
						mapData4 = PlanMgmtConstants.N_A;
					}*/
					
					if(StringUtils.isNotBlank(displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName + ".networkt1tiledisplay"))){
						mapData5 = displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName +".networkt1tiledisplay");
					}/*else{
						mapData5 = PlanMgmtConstants.N_A;
					}*/
					
					if(StringUtils.isNotBlank(displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName + ".explanation"))){
						mapData6 = displayMap.get(PlanMgmtConstants.ELEMENT_DISP_BENEFIT_DATA + benefitName +".explanation");
					}/*else{
						mapData6 = PlanMgmtConstants.N_A;
					}*/
					
				}		
							
			if(insuranceType.equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())){
				benefitString = mapData1 +"##"+ mapData2 +"##"+ mapData3 +"##"+ mapData4 + "##" + mapData5 + "##" + mapData6;
			}else{
				benefitString = mapData1 +"##"+ mapData2 +"##"+ mapData3 +"##"+ mapData4 + "##" + mapData5;
			}
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("benefitString: " + benefitString);//Enable this while debugging for issue
			}	
		}catch(Exception ex){
			LOGGER.error("Exception occured @getDisplayDataFromMap : ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute getDisplayDataFromMap ", ex);
		}
		return benefitString;
	}
	
	public List<String> getCostSharingDisplayDataFromMap(String costSharingKey, List<Map<String, String>> planHealthBenefitsListForUpdate){
		List<String> costSharingStringList = new ArrayList<String>();
		String costSharingString = PlanMgmtConstants.EMPTY_STRING;
		
		try{
			String limitExclusionVal = null;
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("costSharingKey = "+ costSharingKey);
			}
				for(Map<String,String> costSharingMap : planHealthBenefitsListForUpdate){
					costSharingString = PlanMgmtConstants.EMPTY_STRING;
					// process in network cost data 
					if(costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".inNetWorkInd") != null && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".inNetWorkInd"))) ){
						// retrieve exception display text
						limitExclusionVal = getLimitExclusionValue(planHealthBenefitsListForUpdate, costSharingMap, costSharingKey);
						
						costSharingString = costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".inNetWorkInd") + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.COST_SHARING_TYPE_IND + PlanMgmtConstants.PIPE_SIGN + costSharingKey + PlanMgmtConstants.PIPE_SIGN + limitExclusionVal + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.IN_NETWORK;
						costSharingStringList.add(costSharingString);
					}
					if(costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".inNetWorkFly") != null && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".inNetWorkFly"))) ){
						costSharingString = costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".inNetWorkFly") + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.COST_SHARING_TYPE_FLY + PlanMgmtConstants.PIPE_SIGN + costSharingKey + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.N_A + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.IN_NETWORK;
						costSharingStringList.add(costSharingString);
					}
					
					// process out network cost data
					if(costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA  + costSharingKey + ".outNetWorkInd") != null && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".outNetWorkInd"))) ){
						costSharingString = costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".outNetWorkInd") + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.COST_SHARING_TYPE_IND + PlanMgmtConstants.PIPE_SIGN + costSharingKey + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.N_A + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.OUT_NETWORK;
						costSharingStringList.add(costSharingString);
					}
					if(costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".outNetWorkFly") != null && !(PlanMgmtConstants.N_A.equalsIgnoreCase(costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".outNetWorkFly"))) ){
						costSharingString = costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costSharingKey + ".outNetWorkFly") + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.COST_SHARING_TYPE_FLY + PlanMgmtConstants.PIPE_SIGN + costSharingKey + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.N_A + PlanMgmtConstants.PIPE_SIGN + PlanMgmtConstants.OUT_NETWORK;
						costSharingStringList.add(costSharingString);
					}
					
				}
		
		}catch(Exception ex){
			LOGGER.error("Exception occured @getCostSharingDisplayDataFromMap : ", ex);
			giExceptionHandler.recordFatalException(Component.PLANMGMT,	null, "Failed to execute getCostSharingDisplayDataFromMap ", ex);
		}
		
		costSharingString = null;
		
		if(LOGGER.isDebugEnabled()){
			if(costSharingStringList != null && costSharingStringList.size() > 0){
				LOGGER.debug("costSharingStringList: " + costSharingStringList.toString() +costSharingStringList.size());
			}
		}	
		return costSharingStringList;
	} 
	
	private String getLimitExclusionValue(List<Map<String, String>> planHealthBenefitsListForUpdate, Map<String, String> temp_costSharingMap, String costName){
		String limitExclusionVal = PlanMgmtConstants.N_A;
		try{
			for(Map<String,String> costSharingMap : planHealthBenefitsListForUpdate){
				
				if(costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costName + ".limitExcepDisplay") != null){
						limitExclusionVal = costSharingMap.get(PlanMgmtConstants.ELEMENT_DISP_COST_DATA + costName + ".limitExcepDisplay");
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception in getLimitExclusionValue", ex);
		}
		if(limitExclusionVal.equals(PlanMgmtConstants.EMPTY_STRING)){
			return PlanMgmtConstants.N_A;
		}else{
			return limitExclusionVal;
		}
	}
}
