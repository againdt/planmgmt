package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;


/**
 * 
 * @version 1.0
 * @since Mar 25, 2013 
 *
 */
public interface IssuerQualityRatingService {
	
	Map<String, Map<String, String>> getIssuerQualityRatingByHIOSPlanIdList(List<String> HIOSPlanIdList, String effectiveDate, String stateCode);
	 
}