package com.getinsured.hix.planmgmt.querybuilder;

import java.util.HashMap;

import com.getinsured.hix.planmgmt.util.PlanMgmtParamConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public class PlanCostAndBenefitQueryBuilder {
	private static String dentalPlanCostsNetworkOnlyQuery;
	private static String dentalPlanCostsQuery;
	private static String healthPlanBenefitsQuery;
	private static String healthPlanCostsQuery;
	private static String dentalPlanBenefitsQuery;
	private static String planBenefitsForTeaserPlanQuery;
	private static HashMap<String,String> individualPlanBenefitAndCostByPlanIdQueryMap;
	private static HashMap<String,String> individualPlanBenefitAndCostByHiosPlanIdQueryMap;
	
	private static HashMap<String,String> individualPlanBenefitAndCostQueryMap;
	
	public final static String getDentalPlanCostsNetworkOnlyQuery(){
		if(PlanCostAndBenefitQueryBuilder.dentalPlanCostsNetworkOnlyQuery != null){
			return PlanCostAndBenefitQueryBuilder.dentalPlanCostsNetworkOnlyQuery;
		}
		StringBuilder buildquery = new StringBuilder(250);
		buildquery.append(" SELECT pdc.inNetWorkInd, pdc.inNetWorkFly, pdc.name, pdc.planDental.id, pdc.inNetworkFlyPerPerson ");
		buildquery.append(" FROM");
		buildquery.append(" PlanDentalCost pdc");
		buildquery.append(" WHERE");
		buildquery.append(" pdc.planDental.id IN (:param_planDentalIdInputList)");

		PlanCostAndBenefitQueryBuilder.dentalPlanCostsNetworkOnlyQuery = buildquery.toString().trim();
   		return PlanCostAndBenefitQueryBuilder.dentalPlanCostsNetworkOnlyQuery;
	}
	
	
	public final static String getDentalPlanCostsQuery(){
		if(PlanCostAndBenefitQueryBuilder.dentalPlanCostsQuery != null){
			return PlanCostAndBenefitQueryBuilder.dentalPlanCostsQuery;
		}
		StringBuilder buildquery = new StringBuilder(250);
		buildquery.append(" SELECT pdc.inNetWorkInd, pdc.inNetWorkFly, pdc.name, pdc.planDental.id, ");
		buildquery.append(" pdc.inNetworkTier2Ind, pdc.inNetworkTier2Fly, pdc.outNetworkInd, ");
		buildquery.append(" pdc.outNetworkFly, pdc.combinedInOutNetworkInd, pdc.combinedInOutNetworkFly, pdc.combDefCoinsNetworkTier1, ");	
		buildquery.append(" pdc.combDefCoinsNetworkTier2, pdc.limitExcepDisplay, pdc.inNetworkFlyPerPerson, pdc.inNetworkTier2FlyPerPerson, ");
		buildquery.append(" pdc.outNetworkFlyPerPerson, pdc.combinedInOutNetworkFlyPerPerson ");
		buildquery.append(" FROM");
		buildquery.append(" PlanDentalCost pdc");
		buildquery.append(" WHERE");
		buildquery.append(" pdc.planDental.id IN (:param_planDentalIdInputList)");

		PlanCostAndBenefitQueryBuilder.dentalPlanCostsQuery = buildquery.toString().trim();
   		return PlanCostAndBenefitQueryBuilder.dentalPlanCostsQuery;
	}
	
	
	public final static String getHealthPlanBenefitsQuery(){
		if(PlanCostAndBenefitQueryBuilder.healthPlanBenefitsQuery != null){
			return PlanCostAndBenefitQueryBuilder.healthPlanBenefitsQuery;
		}
		StringBuilder buildquery= new StringBuilder(1024);
		buildquery.append("SELECT planHealthBenefit.networkLimitation, planHealthBenefit.networkLimitationAttribute, planHealthBenefit.networkExceptions, " );
		buildquery.append("planHealthBenefit.networkT1CopayVal, planHealthBenefit.networkT1CopayAttr, planHealthBenefit.networkT1CoinsurVal, planHealthBenefit.networkT1CoinsurAttr, ");
		buildquery.append("planHealthBenefit.networkT2CopayVal, planHealthBenefit.networkT2CopayAttr, planHealthBenefit.networkT2CoinsurVal, planHealthBenefit.networkT2CoinsurAttr, ");
		buildquery.append("planHealthBenefit.outOfNetworkCopayVal, planHealthBenefit.outOfNetworkCopayAttr, planHealthBenefit.outOfNetworkCoinsurVal, planHealthBenefit.outOfNetworkCoinsurAttr, ");
		buildquery.append("planHealthBenefit.subjectToInNetworkDuductible, planHealthBenefit.subjectToOutNetworkDeductible, planHealthBenefit.name, planHealthBenefit.plan.id, ");
		buildquery.append("planHealthBenefit.networkT1display, planHealthBenefit.networkT2display, planHealthBenefit.outOfNetworkDisplay, planHealthBenefit.limitExcepDisplay, planHealthBenefit.isCovered, planHealthBenefit.networkT1TileDisplay, planHealthBenefit.explanation ");
		buildquery.append("FROM ");
		buildquery.append("PlanHealthBenefit planHealthBenefit ");
		buildquery.append("WHERE ");
		buildquery.append("planHealthBenefit.plan.id IN (:param_planHealthIdListInput");
		buildquery.append(") AND name IN (:param_helathBenefitNameInputStr) ");
		//buildquery.append(") AND TO_DATE(:param_effectiveDate");
		//buildquery.append(",'YYYY-MM-DD') BETWEEN planHealthBenefit.effStartDate AND planHealthBenefit.effEndDate"); 
		PlanCostAndBenefitQueryBuilder.healthPlanBenefitsQuery = buildquery.toString().trim();
		return PlanCostAndBenefitQueryBuilder.healthPlanBenefitsQuery;
	}
	
	
	public final static String getHealthPlanCostsQuery(){
		if(PlanCostAndBenefitQueryBuilder.healthPlanCostsQuery != null){
			return PlanCostAndBenefitQueryBuilder.healthPlanCostsQuery;
		}
		StringBuilder buildquery = new StringBuilder(250);
		buildquery.append(" SELECT phc.inNetWorkInd, phc.inNetWorkFly, phc.inNetworkTier2Ind, phc.inNetworkTier2Fly, phc.outNetworkInd, ");
		buildquery.append(" phc.outNetworkFly, phc.combinedInOutNetworkInd, phc.combinedInOutNetworkFly, phc.combDefCoinsNetworkTier1, ");	
		buildquery.append(" phc.combDefCoinsNetworkTier2, phc.name, phc.planHealth.id, phc.limitExcepDisplay, phc.inNetworkFlyPerPerson, ");
		buildquery.append(" phc.inNetworkTier2FlyPerPerson, phc.outNetworkFlyPerPerson, phc.combinedInOutNetworkFlyPerPerson ");
		buildquery.append(" FROM");
		buildquery.append(" PlanHealthCost phc");
		buildquery.append(" WHERE");
		buildquery.append(" phc.planHealth.id IN (:param_planHealthIdInputList");
    	buildquery.append(")");
    	PlanCostAndBenefitQueryBuilder.healthPlanCostsQuery = buildquery.toString().trim();
   		return PlanCostAndBenefitQueryBuilder.healthPlanCostsQuery;
	}
	
	public final static String getDentalPlanBenefitsQuery(){
		if(PlanCostAndBenefitQueryBuilder.dentalPlanBenefitsQuery != null){
			return PlanCostAndBenefitQueryBuilder.dentalPlanBenefitsQuery;
		}
		StringBuilder buildquery= new StringBuilder(1024);
		buildquery.append("SELECT planDentalBenefit.networkLimitation, planDentalBenefit.networkLimitationAttribute, planDentalBenefit.networkExceptions, " );
		buildquery.append("planDentalBenefit.networkT1CopayVal, planDentalBenefit.networkT1CopayAttr, planDentalBenefit.networkT1CoinsurVal, planDentalBenefit.networkT1CoinsurAttr, ");
		buildquery.append("planDentalBenefit.networkT2CopayVal, planDentalBenefit.networkT2CopayAttr, planDentalBenefit.networkT2CoinsurVal, planDentalBenefit.networkT2CoinsurAttr, ");
		buildquery.append("planDentalBenefit.outOfNetworkCopayVal, planDentalBenefit.outOfNetworkCopayAttr, planDentalBenefit.outOfNetworkCoinsurVal, planDentalBenefit.outOfNetworkCoinsurAttr, ");
		buildquery.append("planDentalBenefit.subjectToInNetworkDuductible, planDentalBenefit.subjectToOutNetworkDeductible, planDentalBenefit.name, planDentalBenefit.plan.id, ");
		buildquery.append("planDentalBenefit.networkT1display, planDentalBenefit.networkT2display, planDentalBenefit.outOfNetworkDisplay, planDentalBenefit.limitExcepDisplay, planDentalBenefit.isCovered, planDentalBenefit.networkT1TileDisplay, planDentalBenefit.explanation ");
		buildquery.append("FROM ");
		buildquery.append("PlanDentalBenefit planDentalBenefit ");
		buildquery.append("WHERE ");
		buildquery.append("planDentalBenefit.plan.id IN (:param_planDentalListInput");
		buildquery.append(") AND planDentalBenefit.name IN (:param_dentalBenefitNameInputList");
		buildquery.append(") AND TO_DATE(:param_effectiveDate");
		buildquery.append(",'YYYY-MM-DD') BETWEEN planDentalBenefit.effStartDate AND planDentalBenefit.effEndDate"); 
		
		PlanCostAndBenefitQueryBuilder.dentalPlanBenefitsQuery = buildquery.toString().trim();
		return PlanCostAndBenefitQueryBuilder.dentalPlanBenefitsQuery;
	}
	
	public final static String getHealthPlanBenefitsForTeaserPlanQuery(){
		if(PlanCostAndBenefitQueryBuilder.planBenefitsForTeaserPlanQuery != null){
			return PlanCostAndBenefitQueryBuilder.planBenefitsForTeaserPlanQuery;
		}
		StringBuilder buildquery = new StringBuilder(250);
		buildquery.append("SELECT planHealthBenefit.name, planHealthBenefit.plan.id, planHealthBenefit.networkT1display " );	    		
		buildquery.append("FROM ");
		buildquery.append("PlanHealthBenefit planHealthBenefit ");
		buildquery.append("WHERE ");
		buildquery.append("planHealthBenefit.plan.id IN (:param_planHealthIdInputList");
		buildquery.append(") AND name IN (:param_helathBenefitNameInputList");
		buildquery.append(") AND (TO_DATE (:param_effectiveDate,'YYYY-MM-DD') BETWEEN planHealthBenefit.effStartDate AND planHealthBenefit.effEndDate)");
		PlanCostAndBenefitQueryBuilder.planBenefitsForTeaserPlanQuery = buildquery.toString().trim();
		return PlanCostAndBenefitQueryBuilder.planBenefitsForTeaserPlanQuery;
	}
	
	public final static String getIndividualPlanBenefitAndCostByPlanIdsQuery(String insuranceType){
			String query = null;		
			if(PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostByPlanIdQueryMap == null){
				PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostByPlanIdQueryMap = new HashMap<String, String>();
			}
			query = PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostByPlanIdQueryMap.get(insuranceType);
			if(query != null){
				return query;
			}
			String tableName = null;
			
			switch(insuranceType.toUpperCase()){
			case "HEALTH" :{
				tableName = "PlanHealth childTable";
				break;
			}
			case "DENTAL":{
				tableName = "PlanDental childTable";
				break;
			}
			
			default:
				throw new GIRuntimeException("Plan type:"+insuranceType+" not supported for getIndividualPlanBenefitAndCostByPlanIdsQuery() ");
			}
			
			StringBuilder queryBuilder = new StringBuilder(128);
			queryBuilder.append("SELECT id , childTable.plan.id ");
			queryBuilder.append("FROM ");
			queryBuilder.append(tableName);
			queryBuilder.append(" WHERE ");
			queryBuilder.append("childTable.plan.id IN (:");
			queryBuilder.append(PlanMgmtParamConstants.PARAM_PLAN_ID_LIST);
			queryBuilder.append(")");
			
			query = queryBuilder.toString().trim();
			PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostByPlanIdQueryMap.put(insuranceType, query);
		
		return query;
		
	}
	
	
	public final static String getIndividualPlanBenefitAndCostByHiosPlanIdsQuery(String insuranceType){
		String query = null;		
		if(PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostByHiosPlanIdQueryMap == null){
			PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostByHiosPlanIdQueryMap = new HashMap<String, String>();
		}
		query = PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostByHiosPlanIdQueryMap.get(insuranceType);
		if(query != null){
			return query;
		}
		String tableName = null;
		
		switch(insuranceType.toUpperCase()){
		case "HEALTH" :{
			tableName = "PlanHealth childTable";
			break;
		}
		case "DENTAL":{
			tableName = "PlanDental childTable";
			break;
		}
		
		default:
			throw new GIRuntimeException("Plan type:"+insuranceType+" not supported for getIndividualPlanBenefitAndCostByHiosPlanIdsQuery() ");
		}
		
		StringBuilder queryBuilder = new StringBuilder(128);
		queryBuilder.append("SELECT id , childTable.plan.id ");
		queryBuilder.append("FROM ");
		queryBuilder.append(tableName);
		queryBuilder.append(" WHERE ");
		queryBuilder.append("childTable.plan.id IN (SELECT id FROM Plan p WHERE p.issuerPlanNumber IN(:");
		queryBuilder.append(PlanMgmtParamConstants.PARAM_HIOS_PLAN_ID_LIST);
		queryBuilder.append(") AND p.isDeleted='N' AND p.applicableYear =:");
		queryBuilder.append(PlanMgmtParamConstants.PARAM_PLAN_YEAR);
		queryBuilder.append(" AND p.insuranceType = '");
		queryBuilder.append(insuranceType.toUpperCase());
		queryBuilder.append("') ");
		
		query = queryBuilder.toString().trim();
		PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostByHiosPlanIdQueryMap.put(insuranceType, query);
	
	return query;
	
 }
	
	public final static String getIndividualPlanBenefitAndCostQuery(String insuranceType){
		String query = null;		
		if(PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostQueryMap == null){
			PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostQueryMap = new HashMap<String, String>();
		}
		query = PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostQueryMap.get(insuranceType);
		if(query != null){
			return query;
		}
		String tableName = null;
		
		switch(insuranceType.toUpperCase()){
		case "HEALTH" :{
			tableName = "PlanHealth childTable";
			break;
		}
		case "DENTAL":{
			tableName = "PlanDental childTable";
			break;
		}
		
		default:
			throw new GIRuntimeException("Plan type:"+insuranceType+" not supported for IndividualPlanBenefitAndCost");
		}
		
		StringBuilder queryBuilder = new StringBuilder(128);
		queryBuilder.append("SELECT id , childTable.plan.id ");
		queryBuilder.append("FROM ");
		queryBuilder.append(tableName);
		queryBuilder.append(" WHERE ");
		queryBuilder.append("childTable.plan.id IN (:param_planIdList)");
		
		query = queryBuilder.toString().trim();
		PlanCostAndBenefitQueryBuilder.individualPlanBenefitAndCostQueryMap.put(insuranceType, query);
	
	return query;
	
}
	
}
