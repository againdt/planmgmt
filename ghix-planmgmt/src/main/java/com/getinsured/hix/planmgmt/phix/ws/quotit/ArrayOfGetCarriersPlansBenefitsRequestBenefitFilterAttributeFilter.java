//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.01.14 at 11:56:49 AM PST 
//


package com.getinsured.hix.planmgmt.phix.ws.quotit;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter.AttributeFilter complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter.AttributeFilter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetCarriersPlansBenefits.Request.BenefitFilter.AttributeFilter" type="{}GetCarriersPlansBenefits.Request.BenefitFilter.AttributeFilter" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfGetCarriersPlansBenefits.Request.BenefitFilter.AttributeFilter", propOrder = {
    "getCarriersPlansBenefitsRequestBenefitFilterAttributeFilter"
})
public class ArrayOfGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter {

    @XmlElement(name = "GetCarriersPlansBenefits.Request.BenefitFilter.AttributeFilter", nillable = true)
    protected List<GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter> getCarriersPlansBenefitsRequestBenefitFilterAttributeFilter;

    /**
     * Gets the value of the getCarriersPlansBenefitsRequestBenefitFilterAttributeFilter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the getCarriersPlansBenefitsRequestBenefitFilterAttributeFilter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter }
     * 
     * 
     */
    public List<GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter> getGetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter() {
        if (getCarriersPlansBenefitsRequestBenefitFilterAttributeFilter == null) {
            getCarriersPlansBenefitsRequestBenefitFilterAttributeFilter = new ArrayList<GetCarriersPlansBenefitsRequestBenefitFilterAttributeFilter>();
        }
        return this.getCarriersPlansBenefitsRequestBenefitFilterAttributeFilter;
    }

}
