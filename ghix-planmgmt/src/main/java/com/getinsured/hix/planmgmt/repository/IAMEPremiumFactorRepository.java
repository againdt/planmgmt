package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.PlanAMEPremiumFactor;

@Repository
public interface IAMEPremiumFactorRepository extends JpaRepository<PlanAMEPremiumFactor, Integer>, RevisionRepository<PlanAMEPremiumFactor, Integer, Integer> {
		
	 @Query("SELECT monthlyFactor FROM PlanAMEPremiumFactor pamef where pamef.issuerHiosId=:hiosIssuerId and pamef.state=:stateCode and pamef.isDeleted='N'")
	    Double getMonthlyFactor(@Param("hiosIssuerId") String hiosIssuerId, @Param("stateCode") String stateCode);
}
