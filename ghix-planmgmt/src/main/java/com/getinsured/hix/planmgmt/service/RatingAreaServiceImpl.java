package com.getinsured.hix.planmgmt.service;

import java.util.Iterator;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.RatingArea;
import com.getinsured.hix.planmgmt.querybuilder.PlanMgmtQueryBuilder;
import com.getinsured.hix.planmgmt.repository.RatingAreaRepository;
import com.getinsured.hix.planmgmt.service.RatingAreaService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.planmgmt.util.PlanMgmtParamConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;


@Service("ratingAreaService")
@Repository
@Transactional
public class RatingAreaServiceImpl implements RatingAreaService{
	private static final Logger LOGGER = LoggerFactory.getLogger(RatingAreaServiceImpl.class);
	@Autowired private RatingAreaRepository ratingAreaRepository;
	@PersistenceUnit 
	private EntityManagerFactory emf;
	
	@Override
	public RatingArea getByRatingAreaID(String ratingArea){
		RatingArea returningRatingArea=null;
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("ratingAreaIDForRatingArea: " + SecurityUtil.sanitizeForLogging(ratingArea));
		}
		returningRatingArea=ratingAreaRepository.getRatingAreaByName(ratingArea.trim());
		if(returningRatingArea!=null){
			if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("returningRatingAreaFound: " + returningRatingArea.getRatingArea());
			}	
			return returningRatingArea;
		}else{
			return null;
		}
	}

	@Override
	public RatingArea getRatingAreaByZip(String zip, String countyFips) {
		RatingArea ra = new RatingArea();
		try
		{
			ra = ratingAreaRepository.getRatingAreaByZip(zip, countyFips);
		}
		catch(Exception ex)
		{
			LOGGER.error("Error inside getRatingAreaByZip(). Exception:", ex);		
		}
		
			return ra;
		}
	
	@Override
	public RatingArea getRatingAreaByYear(String zipCode, String countyCode, String applicableYear) {
		EntityManager entityManager = null;
		RatingArea ra = new RatingArea();
		try{
			entityManager = emf.createEntityManager();
			
			String buildquery = PlanMgmtQueryBuilder.getRatingAreaByYearQuery();
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("build query for rating area by year:- " +buildquery);
			}
			
			Query query = entityManager.createNativeQuery(buildquery);
			query.setParameter(PlanMgmtParamConstants.PARAM_ZIP_CODE, zipCode);
			query.setParameter(PlanMgmtParamConstants.PARAM_COUNTY_CODE, countyCode);
			query.setParameter(PlanMgmtParamConstants.PARAM_PLAN_YEAR, applicableYear);
			
			Iterator<?> rsIterator = query.getResultList().iterator();
			Object[] objArray = null;
			while (rsIterator.hasNext()) {
				objArray = (Object[]) rsIterator.next();
				ra.setId(Integer.parseInt(objArray[PlanMgmtConstants.ZERO].toString()));
				ra.setState(objArray[PlanMgmtConstants.ONE].toString());
				ra.setRatingArea(objArray[PlanMgmtConstants.TWO].toString());
			}
		}
		catch(Exception ex)
		{
			LOGGER.error("Error inside getRatingAreaByYear(). Exception:", ex);		
		}
		finally{
			 // close the entity manager
			 if(entityManager !=null && entityManager.isOpen()){
				 entityManager.clear();
				 entityManager.close();
				 entityManager = null;
	}
		}
		
		return ra;
	}
	
	
	// pull rating area using State code and county FIPS
	@Override
	public String getRatingAreaByStateCodeAndCountyFips(String stateCode, String countyFips){
		
		String ratingArea= PlanMgmtConstants.EMPTY_STRING;
		RatingArea ratingAreaObj = ratingAreaRepository.getRatingAreaByStateCodeAndCountyFips(stateCode, countyFips);
		if(null != ratingAreaObj){
			ratingArea = ratingAreaObj.getRatingArea();
		}
		
		return ratingArea;
	}
}