package com.getinsured.hix.planmgmt.service;

import com.getinsured.hix.model.RatingArea;

public interface RatingAreaService {
	
	RatingArea getByRatingAreaID(String ratingArea);
	
	RatingArea getRatingAreaByZip(String zip, String countyFips);
	
	RatingArea getRatingAreaByYear(String zipCode, String countyCode, String applicableYear);
	
	String getRatingAreaByStateCodeAndCountyFips(String stateCode, String countyFips);
}
