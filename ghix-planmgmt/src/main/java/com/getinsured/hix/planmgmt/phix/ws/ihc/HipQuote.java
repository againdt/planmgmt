//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.12 at 02:20:37 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.ihc;

import java.math.BigDecimal;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HipQuote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HipQuote">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplyUrl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MembershipBrochure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MonthlyCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="PlanFees" type="{http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip}HipPlanFees" minOccurs="0"/>
 *         &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PlanName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HipQuote", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", propOrder = {
    "applyUrl",
    "membershipBrochure",
    "monthlyCost",
    "planFees",
    "planID",
    "planName"
})
public class HipQuote {

    @XmlElementRef(name = "ApplyUrl", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", type = JAXBElement.class, required = false)
    protected JAXBElement<String> applyUrl;
    @XmlElementRef(name = "MembershipBrochure", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", type = JAXBElement.class, required = false)
    protected JAXBElement<String> membershipBrochure;
    @XmlElement(name = "MonthlyCost")
    protected BigDecimal monthlyCost;
    @XmlElementRef(name = "PlanFees", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", type = JAXBElement.class, required = false)
    protected JAXBElement<HipPlanFees> planFees;
    @XmlElement(name = "PlanID")
    protected Integer planID;
    @XmlElementRef(name = "PlanName", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", type = JAXBElement.class, required = false)
    protected JAXBElement<String> planName;

    /**
     * Gets the value of the applyUrl property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getApplyUrl() {
        return applyUrl;
    }

    /**
     * Sets the value of the applyUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setApplyUrl(JAXBElement<String> value) {
        this.applyUrl = value;
    }

    /**
     * Gets the value of the membershipBrochure property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMembershipBrochure() {
        return membershipBrochure;
    }

    /**
     * Sets the value of the membershipBrochure property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMembershipBrochure(JAXBElement<String> value) {
        this.membershipBrochure = value;
    }

    /**
     * Gets the value of the monthlyCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMonthlyCost() {
        return monthlyCost;
    }

    /**
     * Sets the value of the monthlyCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMonthlyCost(BigDecimal value) {
        this.monthlyCost = value;
    }

    /**
     * Gets the value of the planFees property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link HipPlanFees }{@code >}
     *     
     */
    public JAXBElement<HipPlanFees> getPlanFees() {
        return planFees;
    }

    /**
     * Sets the value of the planFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link HipPlanFees }{@code >}
     *     
     */
    public void setPlanFees(JAXBElement<HipPlanFees> value) {
        this.planFees = value;
    }

    /**
     * Gets the value of the planID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPlanID() {
        return planID;
    }

    /**
     * Sets the value of the planID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPlanID(Integer value) {
        this.planID = value;
    }

    /**
     * Gets the value of the planName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getPlanName() {
        return planName;
    }

    /**
     * Sets the value of the planName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setPlanName(JAXBElement<String> value) {
        this.planName = value;
    }

}
