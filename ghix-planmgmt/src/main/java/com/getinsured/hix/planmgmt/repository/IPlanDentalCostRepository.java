package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.getinsured.hix.model.PlanDentalCost;

public interface IPlanDentalCostRepository extends JpaRepository<PlanDentalCost, Integer>{
	@Query("SELECT pdc.inNetWorkInd, pdc.inNetWorkFly, pdc.inNetworkTier2Ind, pdc.inNetworkTier2Fly, pdc.outNetworkInd, pdc.outNetworkFly, " +
			"pdc.combinedInOutNetworkInd, pdc.combinedInOutNetworkFly, pdc.combDefCoinsNetworkTier1, pdc.combDefCoinsNetworkTier2, pdc.name, pdc.limitExcepDisplay FROM PlanDentalCost pdc WHERE pdc.planDental.id = :planId")
	List<Object[]> getPlanDentalCostByPlanId(@Param("planId") Integer planId);
	
	@Query("SELECT planDentalCost from PlanDentalCost planDentalCost where planDentalCost.planDental.plan.id IN (:planId )")
	List<PlanDentalCost> getPlanDentalCostsByPlanId(@Param("planId") List<Integer> planId);
	
	@Query("SELECT pdc from PlanDentalCost pdc WHERE pdc.planDental.id = :planDentalId")
	List<PlanDentalCost> getCostByPlanDentalId(@Param("planDentalId") Integer planDentalId);
}
