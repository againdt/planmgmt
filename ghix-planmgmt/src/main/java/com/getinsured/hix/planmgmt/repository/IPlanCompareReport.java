package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.PlanCompareReport;

@Repository
public interface IPlanCompareReport extends JpaRepository<PlanCompareReport, Integer> {
    @Query("SELECT p from PlanCompareReport as p where p.planReportStatus = 'INQUEUE' OR p.planReportStatus = 'ERROR' and p.isDeleted = 'N'")
    List<PlanCompareReport> getPlanIdsList();

    @Query("UPDATE PlanCompareReport set planReportStatus = 'INPROGRESS' where ID = :planReportID and isDeleted = 'N'")
    String getStatusForReport(@Param("planReportID") int planReportID);

}
