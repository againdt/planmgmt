package com.getinsured.hix.planmgmt.admin;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.GhixEndPoints;

/**
 *
 * 
 * @author Nikhil Talreja
 * @since August 06, 2013
 * 
 */
public class PlanMgmtInfo implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(PlanMgmtInfo.class);
	
	private List<PlanMgmtInfoRec> records; 
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		LOGGER.info(""+getPlanMgmtInfo());
	}

	/**
	 * <p>
	 * This method calls all the individual methods. Every method loads specific
	 * details as indicated in each method.
	 * </p>
	 * 
	 */
	public void loadPlanManagementInfo() {
		
		LOGGER.info("Loading Plan Management info");
		loadPlanMgmtconfigConfigDetails();
	}

	private void loadPlanMgmtconfigConfigDetails() {

		//if (records != null && records.size() > 0) { commenting due to sonar violation
		if (records != null && (!(records.isEmpty()))) {
			return;
		}
		else{
			records = new ArrayList<PlanMgmtInfoRec>(); 
		}

		//Endpoints
		records.add(new PlanMgmtInfoRec("Service Endpoints","Plan Management Service", GhixEndPoints.PLAN_MGMT_URL));
		
		//PlanManagement REST URLS
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_REST_URL,"Get Employer Reference plans", GhixEndPoints.PlanMgmtEndPoints.GET_EMPLOYER_REFERENCE_PLANS));
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_REST_URL,"Get Plan Data (by Id)", GhixEndPoints.PlanMgmtEndPoints.GET_PLAN_DATA_BY_ID_URL));
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_REST_URL,"Get Employer Plan Rate", GhixEndPoints.PlanMgmtEndPoints.GET_EMPLOYER_PLAN_RATE));
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_REST_URL,"Lowest Premium plan", GhixEndPoints.PlanMgmtEndPoints.GET_LOWEST_PREMIUM_URL));
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_REST_URL,"Household plan rate", GhixEndPoints.PlanMgmtEndPoints.GET_HOUSEHOLD_PLAN_RATE_URL));
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_REST_URL,"Update Plan status", GhixEndPoints.PlanMgmtEndPoints.PLAN_STATUS_UPDATE_URL));
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_REST_URL,"Get Benchmark plan rate", GhixEndPoints.PlanMgmtEndPoints.GET_BENCHMARK_PLAN_RATE_URL));
		
		//Plan management mail template configuration
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_MAIL_TEMPLATE_CONF,"Exchange Name", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)));
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_MAIL_TEMPLATE_CONF,"Exchange Addres",  DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1)));
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_MAIL_TEMPLATE_CONF,"Exchange Website", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL)));
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_MAIL_TEMPLATE_CONF,"Exchange Team", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME)));
		records.add(new PlanMgmtInfoRec(PlanMgmtConstants.PLAN_MGMT_MAIL_TEMPLATE_CONF,"Exchange Users",  DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_USERS)));
	}

	
	public List<PlanMgmtInfoRec> getPlanMgmtInfo() {
		
		loadPlanManagementInfo();
		return records;
	}

	/**
	 * This is for Quick Testing Purpose
	 * 
	 * @param str
	 */
	public static void main(String str[]) {
		
		PlanMgmtInfo pmInfo = new PlanMgmtInfo();
		pmInfo.loadPlanManagementInfo();
		
	}

}
