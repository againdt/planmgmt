package com.getinsured.hix.planmgmt.querybuilder;

import org.apache.log4j.Logger;

import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;

public class QuotingBusinessLogicQueryBuilder {
	private static final Logger LOGGER = Logger.getLogger(QuotingBusinessLogicQueryBuilder.class);
	
	public static final String buildPlanCertificationLogicJPA(String inputTableName, boolean issuerVerifiedFlag){
		StringBuilder buildQuery = new StringBuilder(100);
		try{
			String tableName = inputTableName.trim();
			buildQuery.append(" AND ");
			buildQuery.append(tableName);
			buildQuery.append(".status IN('");
			buildQuery.append(Plan.PlanStatus.CERTIFIED);
			buildQuery.append("') ");
			
			// if issuerVerifiedFlag is true then user issuer verification status filter
			if(issuerVerifiedFlag){
				buildQuery.append(" AND ");
				buildQuery.append(tableName);
				buildQuery.append(".issuerVerificationStatus = '");
				buildQuery.append(Plan.IssuerVerificationStatus.VERIFIED);				
				buildQuery.append("' ");
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in buildPlanCertificationLogicJPA: ", ex);
		}
		return buildQuery.toString();
	}
	
	
	public static final String buildPlanCertificationLogic(String inputTableName, boolean issuerVerifiedFlag){
		StringBuilder buildQuery = new StringBuilder(100); 
		String tableName = inputTableName.trim();
		buildQuery.append(" AND ");
		buildQuery.append(tableName);
		buildQuery.append(".STATUS IN('");
		buildQuery.append(Plan.PlanStatus.CERTIFIED);
		buildQuery.append("') ");
		
		// if issuerVerifiedFlag is true then user issuer verification status filter
		if(issuerVerifiedFlag){
			buildQuery.append(" AND ");
			buildQuery.append(tableName);
			buildQuery.append(".issuer_verification_status = '");
			buildQuery.append(Plan.IssuerVerificationStatus.VERIFIED);				
			buildQuery.append("' ");
		}	
		
		return buildQuery.toString();
	}
	
	public static final void buildEnrollmentAvailabilityLogic(StringBuilder parentBuilder, String inputTableName,String isSpecialEnrollment){
		try{
			String tableName = inputTableName.trim();
			if(isSpecialEnrollment.equalsIgnoreCase(PlanMgmtConstants.YES)){			
				parentBuilder.append(" AND ( ( ");
				parentBuilder.append(tableName);
				parentBuilder.append(".enrollmentAvail IN('");
				parentBuilder.append( Plan.EnrollmentAvail.AVAILABLE.toString());
				parentBuilder.append("','");
				parentBuilder.append(Plan.EnrollmentAvail.DEPENDENTSONLY.toString());
				parentBuilder.append("') AND TO_DATE ( :param_effectiveDate_1");
				parentBuilder.append(" , 'YYYY-MM-DD') >= ");
				parentBuilder.append(tableName);
				parentBuilder.append(".enrollmentAvailEffDate  AND ( TO_DATE ( :param_effectiveDate_2");
				parentBuilder.append(" , 'YYYY-MM-DD') < ");
				parentBuilder.append(tableName);
				parentBuilder.append(".futureErlAvailEffDate OR  ");
				parentBuilder.append(tableName);
				parentBuilder.append(".futureErlAvailEffDate is null) ) ");
				parentBuilder.append(" OR ");	
				parentBuilder.append(" ( ");
				parentBuilder.append(tableName);
				parentBuilder.append(".futureEnrollmentAvail IN('");
				parentBuilder.append(Plan.EnrollmentAvail.AVAILABLE.toString());
				parentBuilder.append( "','");
				parentBuilder.append(Plan.EnrollmentAvail.DEPENDENTSONLY.toString());
				parentBuilder.append("') AND TO_DATE (  :param_effectiveDate_3");
				parentBuilder.append(" , 'YYYY-MM-DD') >= ");
				parentBuilder.append(tableName);
				parentBuilder.append(".futureErlAvailEffDate) )");			
			}else{
				parentBuilder.append(" AND ( ( ");
				parentBuilder.append(tableName);
				parentBuilder.append(".enrollmentAvail='");
				parentBuilder.append(Plan.EnrollmentAvail.AVAILABLE.toString());
				parentBuilder.append("' AND TO_DATE (  :param_effectiveDate_1");
				parentBuilder.append(" , 'YYYY-MM-DD') >= ");
				parentBuilder.append(tableName);
				parentBuilder.append(".enrollmentAvailEffDate  AND ( TO_DATE (  :param_effectiveDate_2");
				parentBuilder.append(", 'YYYY-MM-DD') < ");
				parentBuilder.append(tableName);
				parentBuilder.append(".futureErlAvailEffDate OR  ");
				parentBuilder.append(tableName);
				parentBuilder.append(".futureErlAvailEffDate is null) ) ");
				parentBuilder.append(" OR ");
				parentBuilder.append(" ( ");
				parentBuilder.append(tableName);
				parentBuilder.append(".futureEnrollmentAvail = '");
				parentBuilder.append(Plan.EnrollmentAvail.AVAILABLE.toString());
				parentBuilder.append("' AND TO_DATE (  :param_effectiveDate_3");
				parentBuilder.append( " , 'YYYY-MM-DD') >= ");
				parentBuilder.append(tableName);
				parentBuilder.append(".futureErlAvailEffDate) ) ");
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in buildEnrollmentAvailabilityLogic: ", ex);
		}
	}
	
	public static final void buildCostSharingLogic(StringBuilder parentBuilder, String inputTableName, String csrValue) {
		try{
			String tableName = inputTableName.trim();
			
			if (csrValue.equals("") || csrValue.equalsIgnoreCase("CS1")) {
				// pick all base plan of all metal tier
				parentBuilder.append(" AND ");
				parentBuilder.append(tableName);
				parentBuilder.append(".parentPlanId = 0 "); 
			} else if (csrValue.equalsIgnoreCase("CS2") || csrValue.equalsIgnoreCase("CS3")) {
				 // pick all respective cost sharing plan of all metal tier
				parentBuilder.append(" AND ");
				parentBuilder.append(tableName);
				parentBuilder.append(".costSharing = UPPER('");
				parentBuilder.append(csrValue);
				parentBuilder.append("')");
			} else if (csrValue.equalsIgnoreCase("CS4") || csrValue.equalsIgnoreCase("CS5") || csrValue.equalsIgnoreCase("CS6")) {
				// pick base plan of other metal tiers except silver + respective cost sharing plan
				parentBuilder.append(" AND ((");
				parentBuilder.append(tableName);
				parentBuilder.append(".parentPlanId = 0 AND ");
				parentBuilder.append(tableName);
				parentBuilder.append(".planLevel !='");
				parentBuilder.append(Plan.PlanLevel.SILVER.toString());
				parentBuilder.append("')  OR ");
				parentBuilder.append(tableName);
				parentBuilder.append(".costSharing=UPPER('");
				parentBuilder.append(csrValue);
				parentBuilder.append("'))"); 
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in buildCostSharingLogic: ", ex);
		}
	}
}
