package com.getinsured.hix.planmgmt.model;

public interface PhixConstants {

	public static final String SLSP = "SLSP";
	
	public static final String LSP = "LSP";
	
	public static final String LBP = "LBP";
	
	public static final String GILSP = "GILSP";
	
	public static final String GILBP = "GILBP";
	
	public static final String SILVER = "SILVER";
	
	public static final String BRONZE = "BRONZE";
	
	
}
