package com.getinsured.hix.planmgmt.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.PlanRateBenefit;

public interface NetworkTansparencyRatingService {

	// return network transparency rating for HIOS Plan Ids
	Map<String, String> getNetworkTansparencyRatingByHIOSPlanIdList(List<String> HIOSPlanIdList, String countyFips, Integer applicableYear);
	
	List<PlanRateBenefit> setTansparencyRatingService(List<PlanRateBenefit> planRateBenefitList, List<String> HIOSPlanIdList, String countyFips, Integer applicableYear);
	
	
}
