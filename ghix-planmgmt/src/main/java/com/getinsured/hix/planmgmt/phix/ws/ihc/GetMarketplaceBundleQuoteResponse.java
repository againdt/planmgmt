//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.12 at 02:20:37 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.ihc;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="quoteRequest" type="{http://schemas.datacontract.org/2004/07/SB.Marketplace.Quoting.Models}BundleGroupQuoteRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "quoteRequest"
})
@XmlRootElement(name = "GetMarketplaceBundleQuoteResponse")
public class GetMarketplaceBundleQuoteResponse {

    @XmlElementRef(name = "quoteRequest", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<BundleGroupQuoteRequest> quoteRequest;

    /**
     * Gets the value of the quoteRequest property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BundleGroupQuoteRequest }{@code >}
     *     
     */
    public JAXBElement<BundleGroupQuoteRequest> getQuoteRequest() {
        return quoteRequest;
    }

    /**
     * Sets the value of the quoteRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BundleGroupQuoteRequest }{@code >}
     *     
     */
    public void setQuoteRequest(JAXBElement<BundleGroupQuoteRequest> value) {
        this.quoteRequest = value;
    }

}
