package com.getinsured.hix.planmgmt.repository;
/**
 * Repository class for PlanCostEdit
 */
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanCostEdit;
	
public interface IPlanCostEditRepository extends JpaRepository<PlanCostEdit, Integer>, RevisionRepository<PlanCostEdit, Integer, Integer> {
	
	@Query("SELECT pce FROM PlanCostEdit pce WHERE pce.applicableYear=:applicableYear and pce.issuerPlanNumber=:issuerPlanNumber")
	List<PlanCostEdit> getCostEditedByIssuerPlanNumber(@Param("applicableYear") String applicableYear, @Param("issuerPlanNumber") String issuerPlanNumber);
	
	List<PlanCostEdit> findByIssuerPlanNumberAndNameAndApplicableYear(String issuerPlanNumber, String name, String applicableYear);
	
}
