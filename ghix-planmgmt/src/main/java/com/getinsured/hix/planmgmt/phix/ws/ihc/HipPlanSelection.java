//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2015.06.12 at 02:20:37 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.ihc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HipPlanSelection complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="HipPlanSelection">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicantCriticalIllnessID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="BillingMethodID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ChildCriticalIllnessID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DeductibleID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="HealthBundleID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="MembershipLevelID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PlanID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="SpouseCriticallIllnessID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TestingBundleID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HipPlanSelection", namespace = "http://schemas.datacontract.org/2004/07/SB.QuotingService.Models.Hip", propOrder = {
    "applicantCriticalIllnessID",
    "billingMethodID",
    "childCriticalIllnessID",
    "deductibleID",
    "healthBundleID",
    "membershipLevelID",
    "planID",
    "spouseCriticallIllnessID",
    "testingBundleID"
})
public class HipPlanSelection {

    @XmlElement(name = "ApplicantCriticalIllnessID")
    protected Integer applicantCriticalIllnessID;
    @XmlElement(name = "BillingMethodID")
    protected Integer billingMethodID;
    @XmlElement(name = "ChildCriticalIllnessID")
    protected Integer childCriticalIllnessID;
    @XmlElement(name = "DeductibleID")
    protected Integer deductibleID;
    @XmlElement(name = "HealthBundleID")
    protected Integer healthBundleID;
    @XmlElement(name = "MembershipLevelID")
    protected Integer membershipLevelID;
    @XmlElement(name = "PlanID")
    protected Integer planID;
    @XmlElement(name = "SpouseCriticallIllnessID")
    protected Integer spouseCriticallIllnessID;
    @XmlElement(name = "TestingBundleID")
    protected Integer testingBundleID;

    /**
     * Gets the value of the applicantCriticalIllnessID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getApplicantCriticalIllnessID() {
        return applicantCriticalIllnessID;
    }

    /**
     * Sets the value of the applicantCriticalIllnessID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setApplicantCriticalIllnessID(Integer value) {
        this.applicantCriticalIllnessID = value;
    }

    /**
     * Gets the value of the billingMethodID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBillingMethodID() {
        return billingMethodID;
    }

    /**
     * Sets the value of the billingMethodID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBillingMethodID(Integer value) {
        this.billingMethodID = value;
    }

    /**
     * Gets the value of the childCriticalIllnessID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChildCriticalIllnessID() {
        return childCriticalIllnessID;
    }

    /**
     * Sets the value of the childCriticalIllnessID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChildCriticalIllnessID(Integer value) {
        this.childCriticalIllnessID = value;
    }

    /**
     * Gets the value of the deductibleID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDeductibleID() {
        return deductibleID;
    }

    /**
     * Sets the value of the deductibleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDeductibleID(Integer value) {
        this.deductibleID = value;
    }

    /**
     * Gets the value of the healthBundleID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHealthBundleID() {
        return healthBundleID;
    }

    /**
     * Sets the value of the healthBundleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHealthBundleID(Integer value) {
        this.healthBundleID = value;
    }

    /**
     * Gets the value of the membershipLevelID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMembershipLevelID() {
        return membershipLevelID;
    }

    /**
     * Sets the value of the membershipLevelID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMembershipLevelID(Integer value) {
        this.membershipLevelID = value;
    }

    /**
     * Gets the value of the planID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPlanID() {
        return planID;
    }

    /**
     * Sets the value of the planID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPlanID(Integer value) {
        this.planID = value;
    }

    /**
     * Gets the value of the spouseCriticallIllnessID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSpouseCriticallIllnessID() {
        return spouseCriticallIllnessID;
    }

    /**
     * Sets the value of the spouseCriticallIllnessID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSpouseCriticallIllnessID(Integer value) {
        this.spouseCriticallIllnessID = value;
    }

    /**
     * Gets the value of the testingBundleID property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTestingBundleID() {
        return testingBundleID;
    }

    /**
     * Sets the value of the testingBundleID property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTestingBundleID(Integer value) {
        this.testingBundleID = value;
    }

}
