//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.06.05 at 03:36:01 PM PDT 
//


package com.getinsured.hix.planmgmt.phix.ws.assurant;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ApplicantInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ApplicantInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ApplicantEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantMiddleInitial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantBirthDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="ApplicantSSN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantDaytimePhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantEveningPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantPhysicianName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApplicantPhysicianPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ApplicantInfo", propOrder = {
    "applicantEmail",
    "applicantFirstName",
    "applicantMiddleInitial",
    "applicantLastName",
    "applicantBirthDate",
    "applicantSSN",
    "applicantAddress1",
    "applicantAddress2",
    "applicantCity",
    "applicantDaytimePhone",
    "applicantEveningPhone",
    "applicantPhysicianName",
    "applicantPhysicianPhone"
})
public class ApplicantInfo {

    @XmlElement(name = "ApplicantEmail")
    protected String applicantEmail;
    @XmlElement(name = "ApplicantFirstName")
    protected String applicantFirstName;
    @XmlElement(name = "ApplicantMiddleInitial")
    protected String applicantMiddleInitial;
    @XmlElement(name = "ApplicantLastName")
    protected String applicantLastName;
    @XmlElement(name = "ApplicantBirthDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar applicantBirthDate;
    @XmlElement(name = "ApplicantSSN")
    protected String applicantSSN;
    @XmlElement(name = "ApplicantAddress1")
    protected String applicantAddress1;
    @XmlElement(name = "ApplicantAddress2")
    protected String applicantAddress2;
    @XmlElement(name = "ApplicantCity")
    protected String applicantCity;
    @XmlElement(name = "ApplicantDaytimePhone")
    protected String applicantDaytimePhone;
    @XmlElement(name = "ApplicantEveningPhone")
    protected String applicantEveningPhone;
    @XmlElement(name = "ApplicantPhysicianName")
    protected String applicantPhysicianName;
    @XmlElement(name = "ApplicantPhysicianPhone")
    protected String applicantPhysicianPhone;

    /**
     * Gets the value of the applicantEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantEmail() {
        return applicantEmail;
    }

    /**
     * Sets the value of the applicantEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantEmail(String value) {
        this.applicantEmail = value;
    }

    /**
     * Gets the value of the applicantFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantFirstName() {
        return applicantFirstName;
    }

    /**
     * Sets the value of the applicantFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantFirstName(String value) {
        this.applicantFirstName = value;
    }

    /**
     * Gets the value of the applicantMiddleInitial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantMiddleInitial() {
        return applicantMiddleInitial;
    }

    /**
     * Sets the value of the applicantMiddleInitial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantMiddleInitial(String value) {
        this.applicantMiddleInitial = value;
    }

    /**
     * Gets the value of the applicantLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantLastName() {
        return applicantLastName;
    }

    /**
     * Sets the value of the applicantLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantLastName(String value) {
        this.applicantLastName = value;
    }

    /**
     * Gets the value of the applicantBirthDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getApplicantBirthDate() {
        return applicantBirthDate;
    }

    /**
     * Sets the value of the applicantBirthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setApplicantBirthDate(XMLGregorianCalendar value) {
        this.applicantBirthDate = value;
    }

    /**
     * Gets the value of the applicantSSN property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantSSN() {
        return applicantSSN;
    }

    /**
     * Sets the value of the applicantSSN property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantSSN(String value) {
        this.applicantSSN = value;
    }

    /**
     * Gets the value of the applicantAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantAddress1() {
        return applicantAddress1;
    }

    /**
     * Sets the value of the applicantAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantAddress1(String value) {
        this.applicantAddress1 = value;
    }

    /**
     * Gets the value of the applicantAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantAddress2() {
        return applicantAddress2;
    }

    /**
     * Sets the value of the applicantAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantAddress2(String value) {
        this.applicantAddress2 = value;
    }

    /**
     * Gets the value of the applicantCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantCity() {
        return applicantCity;
    }

    /**
     * Sets the value of the applicantCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantCity(String value) {
        this.applicantCity = value;
    }

    /**
     * Gets the value of the applicantDaytimePhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantDaytimePhone() {
        return applicantDaytimePhone;
    }

    /**
     * Sets the value of the applicantDaytimePhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantDaytimePhone(String value) {
        this.applicantDaytimePhone = value;
    }

    /**
     * Gets the value of the applicantEveningPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantEveningPhone() {
        return applicantEveningPhone;
    }

    /**
     * Sets the value of the applicantEveningPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantEveningPhone(String value) {
        this.applicantEveningPhone = value;
    }

    /**
     * Gets the value of the applicantPhysicianName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantPhysicianName() {
        return applicantPhysicianName;
    }

    /**
     * Sets the value of the applicantPhysicianName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantPhysicianName(String value) {
        this.applicantPhysicianName = value;
    }

    /**
     * Gets the value of the applicantPhysicianPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicantPhysicianPhone() {
        return applicantPhysicianPhone;
    }

    /**
     * Sets the value of the applicantPhysicianPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicantPhysicianPhone(String value) {
        this.applicantPhysicianPhone = value;
    }

}
