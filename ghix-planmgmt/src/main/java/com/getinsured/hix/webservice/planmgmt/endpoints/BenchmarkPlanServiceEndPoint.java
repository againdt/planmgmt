package com.getinsured.hix.webservice.planmgmt.endpoints;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.getinsured.hix.planmgmt.service.PlanRateBenefitService;
import com.getinsured.hix.planmgmt.util.PlanMgmtConstants;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIException.GhixErrors;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanRequest;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanRequest.Members.Member;
import com.getinsured.hix.webservice.planmgmt.benchmarkplan.GetBenchmarkPlanResponse;
import com.getinsured.timeshift.TSCalendar;



@Endpoint
public class BenchmarkPlanServiceEndPoint {
	private static final String TARGET_NAMESPACE = "http://webservice.hix.getinsured.com/planmgmt/benchMarkPlan";
	private static final int ZIPCODE_COUNTYCODE_LENGTH = 5;
	
	private static final String ELEMENT_TOBACCO = "tobacco";

	private static final String ELEMENT_SELF = "self";
	
	private static final String ELEMENT_PRIMARY = "primary";
	
	private static final String ELEMENT_SPOUSE = "spouse";
	
	private static final String ELEMENT_AGE = "age";

	private static final String ELEMENT_RELATION = "relation";

	private static final String ELEMENT_ZIP = "zip";
	
	private static final String ELEMENT_COUNTYCODE = "countycode";
	
	private static final Logger LOGGER = Logger.getLogger(BenchmarkPlanServiceEndPoint.class);
	
	public static final String NAMESPACE = GhixConstants.TNS_GHIX_EMP_PLAN_SELECTION;
	
	private static final String DOB = "dob";
	
	//private static final String ANONYMOUS = "ANONYMOUS";	
	
    public static final String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";
	
	@Autowired	private PlanRateBenefitService planRateBenefitService;
	
	@PayloadRoot(localPart = "getBenchmarkPlanRequest", namespace = TARGET_NAMESPACE)
	public @ResponsePayload GetBenchmarkPlanResponse getBenchMarkPremium(@RequestPayload GetBenchmarkPlanRequest benchmarkPlanRequest) throws GIException
	{
		int responseCode = -1;
		String responseDesc = "";		    
		double rate = PlanMgmtConstants.ZERO;

		String coverageStartDate = benchmarkPlanRequest.getCoverageStartDate();
		String householdId = benchmarkPlanRequest.getHouseholdCaseId();
		SimpleDateFormat sdf = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
		Date effectiveDate = null; 					
		try{
			sdf.setLenient(false);
			effectiveDate = sdf.parse(coverageStartDate);
			
			//check length of input YYYY
			String[] ccoverageDate = coverageStartDate.split("/");
			
			try{
				if(ccoverageDate[2].length() != PlanMgmtConstants.FOUR){
					throw new GIException(GhixErrors.INVALID_COVERAGE_DATE);
				}else{
					if (householdId == null || (householdId.length() != PlanMgmtConstants.TEN || !householdId.toString().matches("^[a-zA-Z0-9]+")) ){
						throw new GIException(GhixErrors.INVALID_HOUSEHOLD_ID);
		    		}
					
					// process request XML			
		    		List<Map<String,String>> censusData= getCensusData(benchmarkPlanRequest, effectiveDate);
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("Processed census Data successfully." + censusData.toString());
					}
					//validate data elements
					validateCensusData(censusData);
					
					// calculate premium			
					rate = planRateBenefitService.getBenchmarkRate(censusData, coverageStartDate, householdId);
					responseDesc =  GhixErrors.SUCCESS.toString();
					responseCode = PlanMgmtConstants.TWOHUNDRED;
				}
			}
			catch (GIException  e){
				LOGGER.error("Error while Processing request : " , e);
	    		responseDesc =  e.getErrorMsg();
	    		responseCode = e.getErrorCode(); 
	    	}
		}
		catch (ParseException  e){
			LOGGER.error("Error while parsing coverage start date ",e);
			responseDesc =  GhixErrors.INVALID_COVERAGE_DATE.getMessage();
    		responseCode =  GhixErrors.INVALID_COVERAGE_DATE.getId();
		}
				
		GetBenchmarkPlanResponse benchMarkPlanResponse = new GetBenchmarkPlanResponse();
		benchMarkPlanResponse.setBenchmarkPremium(BigDecimal.valueOf(rate));
		benchMarkPlanResponse.setResponseCode(Integer.toString(responseCode));
		benchMarkPlanResponse.setResponseDescription(responseDesc);
		//logRequestAndResponse(benchmarkPlanRequest, benchMarkPlanResponse);
		
		return benchMarkPlanResponse;
	}
	
/*	private void logRequestAndResponse(GetBenchmarkPlanRequest benchmarkPlanRequest, GetBenchmarkPlanResponse benchMarkPlanResponse){
		
		StringBuilder request =  new StringBuilder("<getBenchmarkPlanRequest>" +
				"<householdCaseId>" + benchmarkPlanRequest.getHouseholdCaseId() + "</householdCaseId>" +
				"<coverageStartDate>" + benchmarkPlanRequest.getCoverageStartDate() + "</coverageStartDate>" +
				"<members> ");				
				List<Member> members = benchmarkPlanRequest.getMembers().getMember();   
				for(int i=0; i< members.size(); i++){				
					request.append("<member>");
					request.append("<relation>").append(members.get(i).getRelation()).append("</relation>");
					request.append("<dob>").append(members.get(i).getDob()).append("</dob>");
					request.append("<zip>").append(members.get(i).getZip()).append("</zip>");
					request.append("<countyCode>").append(members.get(i).getCountyCode()).append("</countyCode>");
					if( members.get(i).getTobacco() != null){
						request.append("<tobacco>").append(members.get(i).getTobacco()).append("</tobacco>");
					}
					request.append("</member>");
				}
				request.append( " </members>").append("</getBenchmarkPlanRequest>");
		
				LOGGER.info("BENCHMARK PLAN REQUEST === " +request);
				LOGGER.info("BENCHMARK PLAN RESPONSE === " + "<getBenchmarkPlanResponse>" +
				"<benchmarkPremium>" + benchMarkPlanResponse.getBenchmarkPremium() + "</benchmarkPremium>" +
				"<responseCode>" + benchMarkPlanResponse.getResponseCode() + "</responseCode>" +
				"<responseDescription>" + benchMarkPlanResponse.getResponseDescription() + "</responseDescription>" +
				"</getBenchmarkPlanResponse>");
	}*/
	
	private void validateCensusData(List<Map<String, String>> censusData)
			throws GIException {
		/* Disabling Self validation Ref# HIX-113627	
		checkifSelfPresent(censusData);  */
		// check Multiple Self Present in census 
		checkMultipleSelfPresent(censusData);
		// check Multiple Spouse Present in census 
		checkMultipleSpousePresent(censusData);		
	}
	


	private void checkifSelfPresent(List<Map<String,String>> censusData) throws GIException {
		String relation = null;
    	if (censusData == null)
    	{
    		throw new GIException(GhixErrors.INVALID_REQUEST);
    	}
    	boolean isSelfPresent = false;
    	for (Map<String,String> data : censusData)
    	{
    		relation = data.get(ELEMENT_RELATION);
			if(ELEMENT_SELF.equalsIgnoreCase(relation) || ELEMENT_PRIMARY.equalsIgnoreCase(relation))
			{
				isSelfPresent = true;
				break;
			}
    	}
    	
    	if(!isSelfPresent)
    	{    		
    		throw new GIException(GhixErrors.MEMBER_SELF_NOT_SPECIFIED );
    	}
	}
	
	private void checkMultipleSelfPresent(List<Map<String,String>> censusData) throws GIException {
		String relation = null;
    	if (censusData == null)
    	{
    		throw new GIException(GhixErrors.INVALID_REQUEST);
    	}
    	int numberOfSelfPresent = 0;
    	for (Map<String,String> data : censusData)
    	{
    		relation = data.get(ELEMENT_RELATION);
			if(ELEMENT_SELF.equalsIgnoreCase(relation) || ELEMENT_PRIMARY.equalsIgnoreCase(relation))
			{
				numberOfSelfPresent++;
			}
    	}
    	
    	if(numberOfSelfPresent > 1)
    	{    		
    		throw new GIException(GhixErrors.MULTIPLE_SELF_PRESENT );
    	}
	}
	
	private void checkMultipleSpousePresent(List<Map<String,String>> censusData) throws GIException {
		String relation = null;
    	if (censusData == null)
    	{
    		throw new GIException(GhixErrors.INVALID_REQUEST);
    	}
    	int numberOfSpousePresent = 0;
    	for (Map<String,String> data : censusData)
    	{
    		relation = data.get(ELEMENT_RELATION);
			if(ELEMENT_SPOUSE.equalsIgnoreCase(relation))
			{				
				numberOfSpousePresent++;
			}
    	}
    	
    	if(numberOfSpousePresent > 1)
    	{    		
    		throw new GIException(GhixErrors.MULTIPLE_SPOUSE_PRESENT );
    	}
	}
	
	private List<Map<String, String>> getCensusData(GetBenchmarkPlanRequest requestElement, Date effectiveDate) throws GIException{  
    		
    	List<Map<String,String>> myMap = new ArrayList<Map<String,String>>();
    	
    	try{    		
    		List<Member> members = requestElement.getMembers().getMember();   		
    		
    		for(int i=0; i< members.size(); i++){
    			Map<String,String> censusData = new HashMap<String, String>();    
    			if(members.get(i).getRelation() != null){
	    			if(members.get(i).getRelation().equalsIgnoreCase(ELEMENT_PRIMARY) || (members.get(i).getRelation().equalsIgnoreCase(ELEMENT_SELF))){	    				
	    				censusData.put(ELEMENT_RELATION, ELEMENT_SELF);
					}else {						
						censusData.put(ELEMENT_RELATION, members.get(i).getRelation());
					}   	
    			}
    			else
	    		{
	    			throw new GIException(GhixErrors.INVALID_REQUEST);
	    		}
    			
    			// if DOB exists in request but not AGE
    			if(StringUtils.isNotBlank(members.get(i).getDob()) && members.get(i).getAge() == null)
    			{    				
    				censusData.put(DOB, members.get(i).getDob().toString());
    				int age = getAge(members.get(i).getDob().toString(), effectiveDate);
    				censusData.put(ELEMENT_AGE, age+"");
    			}
    			
    			// if AGE exists in request but not DOB
    			if(StringUtils.isBlank(members.get(i).getDob()) && members.get(i).getAge() != null)
    			{    	
    				if(members.get(i).getAge().toString().matches("[0-9]+")){
    					censusData.put(ELEMENT_AGE, members.get(i).getAge().toString());
    				}else{
        				throw new GIException(GhixErrors.INVALID_AGE);
        			}
    			}
    			    			
    			if( members.get(i).getZip() != null)
    			{
    				String zipCode = validateZipCode(members.get(i).getZip());
    				censusData.put(ELEMENT_ZIP, zipCode);
    			}
    			else
    			{
    				throw new GIException(GhixErrors.INVALID_ZIPCODE);
    			}
    			
    			if( members.get(i).getCountyCode() != null)
    			{
    				String countyCode = validateCountyCode(members.get(i).getCountyCode());
    				censusData.put(ELEMENT_COUNTYCODE, countyCode);
    			}
    			else
    			{
    				throw new GIException(GhixErrors.INVALID_COUNTY_CODE);
    			}
    			
    			if(members.get(i).getTobacco() != null)
    			{
    				String tobaccoVal =members.get(i).getTobacco();
    				if (PlanMgmtConstants.Y.equalsIgnoreCase(tobaccoVal) || PlanMgmtConstants.N.equalsIgnoreCase(tobaccoVal))
    				{
    					censusData.put(ELEMENT_TOBACCO, members.get(i).getTobacco());
    				}
    				else
    				{
    					throw new GIException(GhixErrors.INVALID_TOBACCO_VALUE);
    				}
    				//tobacco yes no check
    			}
    			
    			myMap.add(censusData);    			
    			
    		}
    		
    	}catch(GIException e){
    		LOGGER.error("Error while parsing Census Data ",e);
    		throw e;
		}	
    	
    	return myMap ;    	 
    }
    
	// VALIDATE ZIP CODE
    private String validateZipCode(String zipCode) throws GIException {
    	String extractedZipCode = null;
		if (zipCode.length() != ZIPCODE_COUNTYCODE_LENGTH)
		{
			throw new GIException(GhixErrors.INVALID_ZIPCODE);
		}
		extractedZipCode = zipCode.substring(0, PlanMgmtConstants.FIVE);
		try {
	        Integer.parseInt( extractedZipCode );
	    }
	    catch( Exception e ) {
	    	LOGGER.error("Error while extracting zipcode ",e);
	    	throw new GIException(GhixErrors.INVALID_ZIPCODE);
	    }
		return extractedZipCode;
	}
    
    // VALIDATE COUNTY CODE
    private String validateCountyCode(String countyCode) throws GIException {
    	String extractedCountyCode = null;
		if (countyCode.length() != ZIPCODE_COUNTYCODE_LENGTH)
		{
			throw new GIException(GhixErrors.INVALID_COUNTY_CODE);
		}
		extractedCountyCode = countyCode.substring(0, PlanMgmtConstants.FIVE);
		try {
	        Integer.parseInt( extractedCountyCode );
	    }
	    catch( Exception e ) {
	    	LOGGER.error("Error while extracting county code ",e);
	    	throw new GIException(GhixErrors.INVALID_COUNTY_CODE);
	    }
		return extractedCountyCode;
	}

    
	private int getAge(String dobElement, Date effectiveDate) throws GIException
    {
    	SimpleDateFormat sdf = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
		Date dateOfBirth = null;
		try
		{
			sdf.setLenient(false);
			dateOfBirth = sdf.parse(dobElement);
			
			//check length of input YYYY
			String[] dob = dobElement.split("/");
			if(dob[2].length() != PlanMgmtConstants.FOUR){
				throw new GIException(GhixErrors.INVALID_BIRTH_DATE);
			}	
		}
		catch (ParseException  e) 
		{
			LOGGER.error("Error while parsing Date of Birth ",e);
			throw new GIException(GhixErrors.INVALID_BIRTH_DATE);
		}	
		
    	Calendar dob = TSCalendar.getInstance();
    	dob.setTime(dateOfBirth);
    	Calendar ageOnDate = TSCalendar.getInstance();
    	ageOnDate.setTime(effectiveDate);
    	
    	Integer age = ageOnDate.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
	    if(dob.get(Calendar.MONTH) > ageOnDate.get(Calendar.MONTH) 
	       		|| (dob.get(Calendar.MONTH) == ageOnDate.get(Calendar.MONTH) && dob.get(Calendar.DATE) > ageOnDate.get(Calendar.DATE))){
	    	age--;
	    }
	    if (age < 0)
	    {
	    	throw new GIException(GhixErrors.INVALID_AGE);
	    }
    	return age;
    }

}
