package com.getinsured.hix.shop.employer.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Plans implements Comparable<Plans>, Serializable{

	private int id;
	private String name;
	private String level;
	private String coInsurance;
	private String deductible;	
	private String officeVisit;
	private String oopMax;
	private int empCount;
	
	private String employeeId;
	
	private Float premium;
	private Float indvPremium;
	private Float dependentPremium;
	private Float avgPremium;
	private String issuer;
	private String issuerText;
	private Float employerTotalCost;
	private Float employerIndvCost;
	private Float employerDependentCost;
	private Float employeeTotalCost;
	private Float taxCredit;
	private Float estimatedTaxCredit;
	private int notAffodableCount;
	private Float taxPenalty;
	private String networkType;
	private String empList;
	private List<Integer> allEmpList = new ArrayList<Integer>();
	
	private Map<String, HashMap<String, String>> planBenefits;
	
	
	
	public Plans(){
		
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	
	public String getCoInsurance() {
		return coInsurance;
	}

	public void setCoInsurance(String coInsurance) {
		this.coInsurance = coInsurance;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getOfficeVisit() {
		return officeVisit;
	}

	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}

	public String getOopMax() {
		return oopMax;
	}

	public void setOopMax(String oopMax) {
		this.oopMax = oopMax;
	}

	public int getEmpCount() {
		return empCount;
	}

	public void setEmpCount(int empCount) {
		this.empCount = empCount;
	}

	public Float getPremium() {
		return premium;
	}

	public void setPremium(Float premium) {
		this.premium = premium;
	}
	
	public Float getIndvPremium() {
		return indvPremium;
	}
	
	public void setIndvPremium(Float premium) {
		this.indvPremium = premium;
	}

	public Float getAvgPremium() {
		return avgPremium;
	}

	public void setAvgPremium(Float avgPremium) {
		this.avgPremium = avgPremium;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	public String getIssuerText() {
		return issuerText;
	}

	public void setIssuerText(String issuerText) {
		this.issuerText = issuerText;
	}

	

	public Float getEmployerTotalCost() {
		return employerTotalCost;
	}

	public void setEmployerTotalCost(Float employerTotalCost) {
		this.employerTotalCost = employerTotalCost;
	}

	public Float getEmployerIndvCost() {
		return employerIndvCost;
	}

	public void setEmployerIndvCost(Float employerIndvCost) {
		this.employerIndvCost = employerIndvCost;
	}

	public Float getEmployerDependentCost() {
		return employerDependentCost;
	}

	public void setEmployerDependentCost(Float employerDependentCost) {
		this.employerDependentCost = employerDependentCost;
	}

	
	
	public Float getTaxCredit() {
		return taxCredit;
	}

	public void setTaxCredit(Float taxCredit) {
		this.taxCredit = taxCredit;
	}

	public Float getEstimatedTaxCredit() {
		return estimatedTaxCredit;
	}

	public void setEstimatedTaxCredit(Float estimatedTaxCredit) {
		this.estimatedTaxCredit = estimatedTaxCredit;
	}

	
	
	public int getNotAffodableCount() {
		return notAffodableCount;
	}

	public void setNotAffodableCount(int notAffodableCount) {
		this.notAffodableCount = notAffodableCount;
	}

	public Float getTaxPenalty() {
		return taxPenalty;
	}

	public void setTaxPenalty(Float taxPenalty) {
		this.taxPenalty = taxPenalty;
	}

	public String getNetworkType() {
		return networkType;
	}

	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}

	
	public String getEmpList() {
		return empList;
	}

	public void setEmpList(String empList) {
		this.empList = empList;
	}

	public Float getEmployeeTotalCost() {
		return employeeTotalCost;
	}

	public void setEmployeeTotalCost(Float employeeTotalCost) {
		this.employeeTotalCost = employeeTotalCost;
	}
	
	public Map<String, HashMap<String, String>> getPlanBenefits() {
		return planBenefits;
	}

	public void setPlanBenefits(Map<String, HashMap<String, String>> planBenefits) {
		this.planBenefits = planBenefits;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	
	public List<Integer> getAllEmpList() {
		return allEmpList;
	}

	public void setAllEmpList(List<Integer> allEmpList) {
		this.allEmpList = allEmpList;
	}

	public Float getDependentPremium() {
		return dependentPremium;
	}

	public void setDependentPremium(Float dependentPremium) {
		this.dependentPremium = dependentPremium;
	}

	@Override
	public int compareTo(Plans o) {
		return premium.compareTo(o.premium);
	}

	
	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((premium == null) ? 0 : premium.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Plans other = (Plans) obj;
		if (premium == null) {
			if (other.premium != null)
				return false;
		} else if (!premium.equals(other.premium))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Plans [id=" + id + ", name=" + name + ", level=" + level
				+ ", coInsurance=" + coInsurance + ", deductible=" + deductible
				+ ", officeVisit=" + officeVisit + ", oopMax=" + oopMax
				+ ", empCount=" + empCount + ", employeeId=" + employeeId
				+ ", premium=" + premium + ", indvPremium=" + indvPremium
				+ ", dependentPremium=" + dependentPremium + ", avgPremium="
				+ avgPremium + ", issuer=" + issuer + ", issuerText="
				+ issuerText + ", employerTotalCost=" + employerTotalCost
				+ ", employerIndvCost=" + employerIndvCost
				+ ", employerDependentCost=" + employerDependentCost
				+ ", employeeTotalCost=" + employeeTotalCost + ", taxCredit="
				+ taxCredit + ", estimatedTaxCredit=" + estimatedTaxCredit
				+ ", notAffodableCount=" + notAffodableCount + ", taxPenalty="
				+ taxPenalty + ", networkType=" + networkType + ", empList="
				+ empList + ", allEmpList=" + allEmpList + ", planBenefits="
				+ planBenefits + "]";
	}

	
	
	
	
}
