/**
 * PlanRateBenefitControllerTest.java
 * @author chalse_v
 * @version 1.0
 * @since Mar 20, 2013 
 */
package com.getinsured.hix.planmgmt.restwebservice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.Validate;
//import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;
//import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

//import com.getinsured.hix.dto.planmgmt.PlanRateBenefitRequest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.planmgmt.BaseTest;
//import com.getinsured.hix.platform.util.GhixConstants;
//import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;

/**
 * The tester class for PlanRateBenefit ReST service.
 * 
 * @author chalse_v
 * @version 1.1
 * @since Mar 28, 2013 
 *
 */
public class PlanRateBenefitRestControllerTest extends BaseTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanRateBenefitRestControllerTest.class);

	@Autowired
	private RestTemplate restTemplate;
	private static final String PROVIDER_ID = "providerId";
	private static final String IS_SUPPORTED = "isSupported"; 
	private static final String PROVIDER_TYPE = "type";

	private JSONParser parser = new JSONParser();
	
	/**
	 * Constructor.
	 */
	public PlanRateBenefitRestControllerTest() {
	}

	/**
	 * Method to test ReST web service request of 'getLowestPremiumPlanRate' for given criteria. 
	 *
	 * @return void
	 * @throws IOException
	 */
	@Test
	public void testGetLowestPremiumPlanRate() throws Exception{
		LOGGER.info("Beginning execution of testGetLowestPremiumPlanRate().");
		
		/*Validate.notNull(restTemplate, "RestTemplate instance is improperly configured.");
		Validate.notNull(parser, "JSONParser instance is improperly configured.");
		
		Map<String, Object> requestParameters = new LinkedHashMap<String, Object>();
		
   		PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest(restTemplate);
   		planRateBenefitRequest.setUrl(PlanMgmtEndPoints.GET_LOWEST_PREMIUM_URL);
   		planRateBenefitRequest.setRequestMethod(PlanRateBenefitRequest.RequestMethod.POST);
   		planRateBenefitRequest.setRequestParameters(requestParameters);
		
		try {
   			String responseJSON = planRateBenefitRequest.sendRequest();
			
			Validate.notNull(responseJSON, "Response is missing for request.");
			
			JSONObject rawObject = (JSONObject) parser.parse(responseJSON);
			
			Validate.notNull(rawObject, "Invalid instance for parsed responseJSON encountered.");
			
			String status = (String) rawObject.get("status");
			
			if(null == status || status.equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
				LOGGER.info("Response of testGetLowestPremiumPlanRate() is '" + GhixConstants.RESPONSE_FAILURE + "'. Cause: errorcode= " + rawObject.get("errCode") + " message=" + rawObject.get("errMsg"));
				
				if(LOGGER.isDebugEnabled())
					LOGGER.debug("Response:" + responseJSON);
					
				Assert.fail(rawObject.get("errMsg").toString());
			}
			else {
				Assert.assertTrue(true);
			}
		}catch (ParseException e) {
			LOGGER.info("Exception occurred in test method. Exception:" + e.getMessage());
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}catch (Exception ex) {
			LOGGER.info("Exception occurred in test method. Exception:" + ex.getMessage());
			ex.printStackTrace();
			Assert.fail(ex.getMessage());
		}
   		finally {
   			LOGGER.info("Completed execution of testGetLowestPremiumPlanRate().");
   		}			*/
	}

	/**
	 * Method to test ReST web service request of 'getBenchmarkRate' for given criteria. 
	 *
	 * @return void
	 * @throws IOException
	 */ 
	@Test
	public void testGetBenchmarkRate() throws Exception {
		LOGGER.info("Beginning execution of testGetBenchmarkRate().");
		
		/*Validate.notNull(restTemplate, "RestTemplate instance is improperly configured.");
		Validate.notNull(parser, "JSONParser instance is improperly configured.");
		
		Map<String, Object> requestParameters = new LinkedHashMap<String, Object>();
		
   		PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest(restTemplate);
   		planRateBenefitRequest.setUrl(PlanMgmtEndPoints.GET_BENCHMARK_PLAN_RATE_URL);
   		planRateBenefitRequest.setRequestMethod(PlanRateBenefitRequest.RequestMethod.POST);
   		planRateBenefitRequest.setRequestParameters(requestParameters);
		
		try {
   			String responseJSON = planRateBenefitRequest.sendRequest();
			
			Validate.notNull(responseJSON, "Response is missing for request.");
			
			JSONObject rawObject = (JSONObject) parser.parse(responseJSON);
			
			Validate.notNull(rawObject, "Invalid instance for parsed responseJSON encountered.");
			
			String status = (String) rawObject.get("status");
			
			if(null == status || status.equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
				LOGGER.info("Response of testGetBenchmarkRate() is '" + GhixConstants.RESPONSE_FAILURE + "'. Cause: errorcode= " + rawObject.get("errCode") + " message=" + rawObject.get("errMsg"));
				
				if(LOGGER.isDebugEnabled())
					LOGGER.debug("Response:" + responseJSON);
					
				Assert.fail(rawObject.get("errMsg").toString());
			}
			else {
				Assert.assertTrue(true);
			}
		}catch (ParseException e) {
			LOGGER.info("Exception occurred in test method. Exception:" + e.getMessage());
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}catch (Exception ex) {
			LOGGER.info("Exception occurred in test method. Exception:" + ex.getMessage());
			ex.printStackTrace();
			Assert.fail(ex.getMessage());
		}
   		finally {
   			LOGGER.info("Completed execution of testGetBenchmarkRate().");
   		}*/			
	}


	/**
	 * Method to test ReST web service request of 'getEmployeePlanRateBenefits' for given criteria. 
	 *
	 * @return void
	 * @throws IOException
	 */
	@Test
	public void testGetEmployeePlanRateBenefits() throws Exception {
		LOGGER.info("Beginning execution of testGetEmployeePlanRateBenefits().");
		
		Validate.notNull(restTemplate, "RestTemplate instance is improperly configured.");
		Validate.notNull(parser, "JSONParser instance is improperly configured.");
		
		Map<String,String> censusData = new HashMap<String, String>();
	    ArrayList<Map<String,String>> myMap = new ArrayList<Map<String,String>>();
	      
	    ArrayList<ArrayList<Map<String,String> > > employeedata = new ArrayList< ArrayList<Map<String,String>> >();	
	    ArrayList< ArrayList< ArrayList< Map<String,String> > > >  employeeList  =  new ArrayList< ArrayList< ArrayList< Map<String,String> > > >();
	  	              
   		censusData = new HashMap<String, String>();
   		censusData.put("relation", "member");       		
   		censusData.put("id", "11");	
   		censusData.put("age", "51");            
   		censusData.put("tobacco", "n");
   		censusData.put("zip", "94087");	       		
   		myMap.add(censusData);
   
   		censusData = new HashMap<String, String>();
   		censusData.put("relation", "spouse");	       		
   		censusData.put("age", "34");            
   		censusData.put("tobacco", "n");
   		censusData.put("zip", "94087");	       		
   		myMap.add(censusData);
   		
   		censusData = new HashMap<String, String>();
   		censusData.put("relation", "child");	       		
   		censusData.put("age", "34");            
   		censusData.put("tobacco", "n");
   		censusData.put("zip", "94087");	       		
   		myMap.add(censusData);
   	
   		employeedata.add( myMap);	       		
   		employeeList.add(employeedata);
	       		
	       		
   		myMap = new ArrayList<Map<String,String>>();
   		employeedata = new ArrayList< ArrayList<Map<String,String>> >();
   		
   		censusData = new HashMap<String, String>();
   		censusData.put("relation", "member");
   		censusData.put("id", "22");	       		
   		censusData.put("age", "43");            
   		censusData.put("tobacco", "y");
   		censusData.put("zip", "92222");	       		
   		myMap.add(censusData);
   		
   		censusData = new HashMap<String, String>();
   		censusData.put("relation", "spouse");	       		
   		censusData.put("age", "34");            
   		censusData.put("tobacco", "y");
   		censusData.put("zip", "92222");	       		
   		myMap.add(censusData);
   			       		
   		employeedata.add( myMap);
   		employeeList.add(employeedata);
   		
   		Map<String, Object> requestParameters = new LinkedHashMap<String, Object>();
   		
   		requestParameters.put("employeeList", employeeList);
   		requestParameters.put("insType", Plan.PlanInsuranceType.HEALTH.toString());
   		requestParameters.put("effectiveDate", "2014-11-05");
   		requestParameters.put("planLavel", Plan.PlanLevel.GOLD.toString());
   		
   		/*PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest(restTemplate);
   		planRateBenefitRequest.setUrl(PlanMgmtEndPoints.GET_EMPLOYER_PLAN_RATE);
   		planRateBenefitRequest.setRequestMethod(PlanRateBenefitRequest.RequestMethod.POST);
   		planRateBenefitRequest.setRequestParameters(requestParameters);
   		
   		try {
   			String responseJSON = planRateBenefitRequest.sendRequest();
			
			Validate.notNull(responseJSON, "Response is missing for request.");
			
			JSONObject rawObject = (JSONObject) parser.parse(responseJSON);
			
			Validate.notNull(rawObject, "Invalid instance for parsed responseJSON encountered.");
			
			String status = (String) rawObject.get("status");
			
			if(null == status || status.equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
				LOGGER.info("Response of testGetEmployeePlanRateBenefits() is '" + GhixConstants.RESPONSE_FAILURE + "'. Cause: errorcode= " + rawObject.get("errCode") + " message=" + rawObject.get("errMsg"));
				
				if(LOGGER.isDebugEnabled())
					LOGGER.debug("Response:" + responseJSON);
					
				Assert.fail(rawObject.get("errMsg").toString());
			}
			else {
				Assert.assertTrue(true);
			}
		}catch (ParseException e) {
			LOGGER.info("Exception occurred in test method. Exception:" + e.getMessage());
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}catch (Exception ex) {
			LOGGER.info("Exception occurred in test method. Exception:" + ex.getMessage());
			ex.printStackTrace();
			Assert.fail(ex.getMessage());
		} 

   		finally {
   			LOGGER.info("Completed execution of testGetEmployeePlanRateBenefits().");
   		}		*/
	}

	/**
	 * Method to test ReST web service request of 'getHouseholdPlanRateBenefits' for given criteria. 
	 *
	 * @return void
	 * @throws IOException
	 */
	@Test
	public void testGetHouseholdPlanRateBenefits() throws Exception {
		LOGGER.info("Beginning execution of testGetHouseholdPlanRateBenefits().");
		
		Validate.notNull(restTemplate, "RestTemplate instance is improperly configured.");
		Validate.notNull(parser, "JSONParser instance is improperly configured.");
		
		Map<String,String> censusData = new HashMap<String, String>();
	    censusData.put("age", "25");            
	    censusData.put("tobacco", "y");
	    censusData.put("zip", "92222");
	    censusData.put("relation", "spouse");
		
		ArrayList<Map<String, String>> memberList = new ArrayList<Map<String,String>>();
		memberList.add(censusData);

		List<Map<String, String>> providers = new ArrayList<Map<String,String>>();
		Map<String ,String>  p1 = new HashMap<String, String>();
		p1.put(PROVIDER_ID, "19");
		p1.put(PROVIDER_TYPE, "FACILITY");
		p1.put(IS_SUPPORTED, "");
		providers.add(p1);
		
		Map<String, Object> requestParameters = new LinkedHashMap<String, Object>();
   		
   		requestParameters.put("memberList", memberList);
   		requestParameters.put("insType", Plan.PlanInsuranceType.HEALTH.toString());
   		requestParameters.put("effectiveDate", "2014-05-05");
   		requestParameters.put("costSharing", "");
   		requestParameters.put("groupId", Integer.valueOf(4));
   		requestParameters.put("planIdStr", "");
   		requestParameters.put("showCatastrophicPlan", Boolean.FALSE);
   		
   		requestParameters.put("planLevel", Plan.PlanLevel.GOLD);
   		requestParameters.put("ehbCovered", "PARTIAL");
   		requestParameters.put("providers", providers);
   		requestParameters.put("marketType", Plan.PlanMarket.INDIVIDUAL);
   		requestParameters.put("isSpecialEnrollment", "YES");
   		
   		/*
   		PlanRateBenefitRequest planRateBenefitRequest = new PlanRateBenefitRequest(restTemplate);
   		planRateBenefitRequest.setUrl(PlanMgmtEndPoints.GET_HOUSEHOLD_PLAN_RATE_URL);
   		planRateBenefitRequest.setRequestMethod(PlanRateBenefitRequest.RequestMethod.POST);
   		planRateBenefitRequest.setRequestParameters(requestParameters);
   		
   		try {
   			String responseJSON = planRateBenefitRequest.sendRequest();
			
			Validate.notNull(responseJSON, "Response is missing for request.");
			
			JSONObject rawObject = ((JSONObject) parser.parse(responseJSON));
			
			Validate.notNull(rawObject, "Invalid instance for parsed responseJSON encountered.");
			
			String status = (String) rawObject.get("status");
			
			if(null == status || status.equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
				LOGGER.info("Response of testGetHouseholdPlanRateBenefits() is '" + GhixConstants.RESPONSE_FAILURE + "'. Cause: errorcode= " + rawObject.get("errCode") + " message=" + rawObject.get("errMsg"));
				
				if(LOGGER.isDebugEnabled())
					LOGGER.debug("Response:" + responseJSON);
					
				Assert.fail(rawObject.get("errMsg").toString());
			}
			else {
				Assert.assertTrue(true);
			}
		}catch (ParseException e) {
			LOGGER.info("Exception occurred in test method. Exception:" + e.getMessage());
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}catch (Exception ex) {
			LOGGER.info("Exception occurred in test method. Exception:" + ex.getMessage());
			ex.printStackTrace();
			Assert.fail(ex.getMessage());
		} 

   		finally {
   			LOGGER.info("Completed execution of testGetHouseholdPlanRateBenefits().");
   		}	*/	
	}
}
