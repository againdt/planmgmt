package com.getinsured.hix.planmgmt.restwebservice;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.model.planmgmt.HouseHoldRequest;
import com.getinsured.hix.model.planmgmt.HouseHoldResponse;
import com.getinsured.hix.planmgmt.BaseTest;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
//TODO: Need to modify the test case
public class PremiumRestControllerTest extends BaseTest {

	private static final Logger LOGGER = Logger
			.getLogger(PremiumRestControllerTest.class);

	private static final String PREMIUM_REST_CONTROLLER_SERVICE = GhixEndPoints.PlanMgmtEndPoints.GET_BENCHMARK_PREMIUM_URL;

//	@Autowired
//	@Qualifier(value = "configProp")
//	private Properties properties;

	@Autowired
	private RestTemplate restTemplate;

	@Test
	public void testGetPremiumForHouseHoldPlan() {
		LOGGER.info("Beginning execution of getPremiumForHouseHoldPlan().");

//		Validate.notNull(properties,
//				"Properties instance is improperly configured.");
		Validate.notNull(restTemplate,
				"RestTemplate instance is improperly configured.");

		//TODO: Need to Set required values to HouseHold
		HouseHoldRequest req = new HouseHoldRequest();
		req.setState("HI");

		
		HouseHoldResponse houseHoldResponse = restTemplate.postForObject(PREMIUM_REST_CONTROLLER_SERVICE, req, 	HouseHoldResponse.class);

		//TODO: Need to change the conditions and Assertion logic after getting the real data.
		if (houseHoldResponse.getPremium() != 0.0d) {
			LOGGER.info("The premium returned successfully." +houseHoldResponse.getPremium());
			Assert.assertTrue(true);
		} else {
			LOGGER.info("Response of getPremiumForHouseHoldPlan() is '"
					+ GhixConstants.RESPONSE_FAILURE + "'");

			if (LOGGER.isDebugEnabled())
				LOGGER.debug("Response:" + houseHoldResponse.getPremium());

			Assert.fail();
		}

	}
	
	//TODO 

}
