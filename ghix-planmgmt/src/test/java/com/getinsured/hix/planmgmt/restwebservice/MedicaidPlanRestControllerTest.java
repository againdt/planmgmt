/**
 * MedicaidPlanRestControllerTest.java
 * @author santanu
 * @version 1.0
 * @since jan 23, 2015 
 */
package com.getinsured.hix.planmgmt.restwebservice;

import java.io.IOException;

import org.apache.commons.lang.Validate;
//import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;
//import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

//import com.getinsured.hix.dto.planmgmt.MedicaidPlanRequestDTO;
import com.getinsured.hix.planmgmt.BaseTest;
/*import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;*/

public class MedicaidPlanRestControllerTest extends BaseTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(MedicaidPlanRestControllerTest.class);

	@Autowired
	private RestTemplate restTemplate;


	private JSONParser parser = new JSONParser();
	
	/**
	 * Constructor.
	 */
	public MedicaidPlanRestControllerTest() {
	}

	/**
	 * Method to test ReST web service request of 'testGetMedicaidPlans' for given criteria. 
	 *
	 * @return void
	 * @throws IOException
	 */
	@Test
	public void testGetMedicaidPlans() throws Exception {
		LOGGER.info("Beginning execution of testGetMedicaidPlans().");
		
		Validate.notNull(restTemplate, "RestTemplate instance is improperly configured.");
		Validate.notNull(parser, "JSONParser instance is improperly configured.");
		
	/*		
		MedicaidPlanRequestDTO medicaidPlanRequestDTO = new MedicaidPlanRequestDTO(restTemplate);
		medicaidPlanRequestDTO.setUrl(GhixEndPoints.PlanMgmtEndPoints.GET_MEDICAID_PLAN);
		medicaidPlanRequestDTO.setRequestMethod("POST");
		medicaidPlanRequestDTO.setHouseholdCaseId("1");
		medicaidPlanRequestDTO.setZipCode("29316");
		medicaidPlanRequestDTO.setCountyCode("45083");
		medicaidPlanRequestDTO.setEffectiveDate("2015-10-10");
		
	 	try {
   			String responseJSON = medicaidPlanRequestDTO.sendRequest();
			
			Validate.notNull(responseJSON, "Response is missing for request.");
			
			JSONObject rawObject = ((JSONObject) parser.parse(responseJSON));
			
			Validate.notNull(rawObject, "Invalid instance for parsed responseJSON encountered.");
			
			String status = (String) rawObject.get("status");
			LOGGER.info( "status == " + status);
			if(null == status || status.equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {
				LOGGER.info("Response of testGetMedicaidPlans() is '" + GhixConstants.RESPONSE_FAILURE + "'. Cause: errorcode= " + rawObject.get("errCode") + " message=" + rawObject.get("errMsg"));
				
				if(LOGGER.isDebugEnabled())
					LOGGER.debug("Response:" + responseJSON);
					
				Assert.fail(rawObject.get("errMsg").toString());
			}
			else {
				Assert.assertTrue(true);
			}
		}catch (ParseException e) {
			LOGGER.info("Exception occurred in test method. Exception:" + e.getMessage());
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}catch (Exception ex) {
			LOGGER.info("Exception occurred in test method. Exception:" + ex.getMessage());
			ex.printStackTrace();
			Assert.fail(ex.getMessage());
		} 

   		finally {
   			LOGGER.info("Completed execution of testGetMedicaidPlans().");
   		}		*/
	}
}
