/**
 * PlanMgmtSerffRestController.java
 * @author chalse_v
 * @version 1.0
 * @since Apr 4, 2013 
 */
package com.getinsured.hix.planmgmt.restwebservice;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws.PlanMgmtRequest;
import com.getinsured.hix.dto.planmgmt.template.admin.hix.pm.ws.PlanMgmtResponse;
import com.getinsured.hix.planmgmt.BaseTest;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.PlanMgmtEndPoints;
import com.serff.planmanagementserffapi.serff.model.service.UpdateExchangeWorkflowStatus;
import com.serff.planmanagementserffapi.serff.model.service.UpdateExchangeWorkflowStatusResponse;

/**
 * JUnit test class for testing PlanMgmtSerff REST web service for Transfer of Plan Details.
 * 
 * @author chalse_v
 * @version 1.3
 * @since May 28, 2013 
 *
 */
public class PlanMgmtSerffRestControllerTest extends BaseTest {
	private static final Logger LOGGER = Logger.getLogger(PlanMgmtSerffRestControllerTest.class);


	@Autowired   
	private RestTemplate restTemplate;
	
	/**
	 * Constructor.
	 */
	public PlanMgmtSerffRestControllerTest() {
	}
	
	/**
	 * JUnit test method 'testProcessCSR' to test processCSR operation for given criteria.
	 *
	 * @author chalse_v 
	 */
	@Test
	public void testProcessCSR() {
		LOGGER.info("Beginning execution of testProcessCSR().");
		
		Validate.notNull(restTemplate, "RestTemplate instance is improperly configured.");
		
		PlanMgmtRequest req = new PlanMgmtRequest();

		// PlanMgmtEndPoints.PROCESS_CSR_URL removed now due to JIRA HIX-19253
		// PlanMgmtResponse response = restTemplate.postForObject(PlanMgmtEndPoints.PROCESS_CSR_URL, req, PlanMgmtResponse.class);
		PlanMgmtResponse response = null;

		if(LOGGER.isDebugEnabled()) {
			if(null != response) {
				LOGGER.debug("Response:" + response.getStatus());
			}
		}

		if(null == response || response.getStatus().trim().isEmpty() || response.getStatus().trim().equals(GhixConstants.RESPONSE_FAILURE)) {
			LOGGER.warn("Response of testProcessCSR() is '" + GhixConstants.RESPONSE_FAILURE + "'");
			
			Assert.fail();
		}
		else {
			LOGGER.info("The SERFF CSR data was successfully persisted.");
			Assert.assertTrue(true);
		}		
	}
	
	/**
	 * JUnit test method 'testUpdatePlanStatus' to test updatePlanStatus operation for given criteria.
	 *
	 * @author chalse_v 
	 * @since May 10, 2013 
	 */
	@Test
	public void testUpdatePlanStatus() {
		LOGGER.info("Beginning execution of testUpdatePlanStatus().");
		
		Validate.notNull(restTemplate, "RestTemplate instance is improperly configured.");
		
		UpdateExchangeWorkflowStatus request = new UpdateExchangeWorkflowStatus();
		request.setAppID("GHIX_PLANMGMT");
		request.setIssuerId("1");
		request.setExchangePlanId("1");
		request.setExchangeWorkflowStatus("REJECTED");
		
		UpdateExchangeWorkflowStatusResponse response = restTemplate.postForObject(PlanMgmtEndPoints.PLAN_STATUS_UPDATE_URL, request, UpdateExchangeWorkflowStatusResponse.class);

		if(null == response || response.getStatus().trim().isEmpty() || response.getStatus().trim().equals(GhixConstants.RESPONSE_FAILURE)) {
			LOGGER.warn("Response of testUpdatePlanStatus() is '" + GhixConstants.RESPONSE_FAILURE + "'");
			
			if(LOGGER.isDebugEnabled()) {
				if(null != response) {
					LOGGER.debug("Response:" + response.getStatus());
				}
			}

			Assert.fail();
		}
		else {
			LOGGER.info("SERFF was successfully updated with plan status.");
			Assert.assertTrue(true);
		}		
	}
}
