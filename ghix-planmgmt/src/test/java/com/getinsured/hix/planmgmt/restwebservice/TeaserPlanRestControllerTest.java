package com.getinsured.hix.planmgmt.restwebservice;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.model.planmgmt.TeaserPlanRequest;
import com.getinsured.hix.model.planmgmt.TeaserPlanResponse;
import com.getinsured.hix.planmgmt.BaseTest;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;

public class TeaserPlanRestControllerTest  extends BaseTest{
	
	private static final Logger LOGGER = Logger
			.getLogger(TeaserPlanRestControllerTest.class);
	
	@Autowired
	private RestTemplate restTemplate;


	// private static final String TEASER_PLAN_API_URL = GhixEndPoints.PlanMgmtEndPoints.TEASER_PLAN_API_URL;
	@Test
	public void testGetBenchMarkPlan() {
		LOGGER.info("Beginning execution of testGetBenchMarkPlan().");
		TeaserPlanRequest teaserPlanRequest= new TeaserPlanRequest();
		//TODO: Need to change the conditions and Assertion logic after getting the real data.
		// GhixEndPoints.PlanMgmtEndPoints.TEASER_PLAN_API_URL removed now due to JIRA HIX-19253 
		/*TeaserPlanResponse teaserPlanResponse = restTemplate.postForObject(TEASER_PLAN_API_URL, teaserPlanRequest, 	TeaserPlanResponse.class);
		if (teaserPlanResponse.getBenchmarkPremium() != 0.0d) {
			LOGGER.info("The premium returned successfully." +teaserPlanResponse.getBenchmarkPremium());
			Assert.assertTrue(true);
		} else {
			LOGGER.info("Response of testGetBenchMarkPlan() is '"
					+ GhixConstants.RESPONSE_FAILURE + "'");

			if (LOGGER.isDebugEnabled())
				LOGGER.debug("Response:" + teaserPlanResponse.getBenchmarkPremium());

			Assert.fail();
		}*/
		
	}

}
